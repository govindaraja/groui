import React, { Component } from 'react';
import { connect } from 'react-redux';
import { HotTable } from '@handsontable/react';
import Handsontable from 'handsontable';
import 'handsontable/dist/handsontable.full.css';
import { v1 as uuidv1 } from 'uuid';

import { commonMethods, modalService } from '../utils/services';

const Max_Rows = 1000;

var RedGreenRenderer = function(instance, td, row, col, prop, value, cellProperties) {
	while (td.firstChild) {
		td.removeChild(td.firstChild);
	}
	if (value) {
		var flagElement = document.createElement('DIV');
		flagElement.className = 'excel_error_indicator';
		flagElement.style = "width: 8px;height: 8px;margin: auto;margin-top: 7px;border-radius: 50%"
		flagElement.innerHTML = '';
		td.appendChild(flagElement);
	} else {
		var textNode = document.createTextNode(value === null ? '' : '');
		td.appendChild(textNode);
	}
};

var IndexRenderer = function(instance, td, row, col, prop, value, cellProperties) {
	while (td.firstChild) {
		td.removeChild(td.firstChild);
	}
	var textNode = document.createTextNode(value === null ? '' : Number(value) + 1);
	td.appendChild(textNode);
};

var HeaderRenderer = function(instance, td, row, col, prop, value, cellProperties) {
	while (td.firstChild) {
		td.removeChild(td.firstChild);
	}
	var textNode = document.createTextNode(value === null ? '' : value);
	td.appendChild(textNode);
};

Handsontable.renderers.registerRenderer('RedGreenRenderer', RedGreenRenderer);
Handsontable.renderers.registerRenderer('IndexRenderer', IndexRenderer);

Handsontable.renderers.registerRenderer('HeaderRenderer', HeaderRenderer);

export default class hottable extends Component {
	constructor(props) {
		super(props);

		let getInitialArray = () => {
			let initialArr = this.props.data && this.props.data.length > 0 ? this.props.data : [];
			if(initialArr.length < Max_Rows) {
				let arr = [];
				for(var i=initialArr.length;i<Max_Rows;i++)
					arr.push({});
				initialArr.push(...arr);
			}
			return initialArr;
		}

		this.state = {
			id: uuidv1(),
			settings: {
				data: getInitialArray(),
				readOnly: this.props.readOnly,
				rowHeaders: true,
				height: '' + (this.props.height ? this.props.height : 500)  + '',
				stretchH: "all",
				manualColumnResize: true,
				maxRows: Max_Rows,
				//contextMenu: ['row_above', 'row_below', 'remove_row', 'cut', 'copy'],
				contextMenu: {
					items: {
						//'row_above': {},
						//'row_below': {},
						//'remove_row': {},
						'custom_row_above': {
							name: "Insert row above",
							key: "Insert row above",
							callback: (key, options) => {
								let data = this.props.data || [];
								let errorFound = false;
								let clearArray = [];
								if(data.length == Max_Rows && Object.keys(data[Max_Rows-1]).length === 0) {
									clearArray.push([999, 1]);
									this.refs.hottable.hotInstance.alter('remove_row', clearArray, null, 'custom_row_above');
									this.refs.hottable.hotInstance.alter('insert_row', options[0].start.row, null, 'custom_row_above');
								}
							}
						},
						'custom_row_below': {
							name: "Insert row below",
							key: "Insert row below",
							callback: (key, options) => {
								let data = this.props.data || [];
								let errorFound = false;
								let clearArray = [];
								if(data.length == Max_Rows && Object.keys(data[Max_Rows-1]).length === 0) {
									clearArray.push([999, 1]);
									this.refs.hottable.hotInstance.alter('remove_row', clearArray, null, 'custom_row_below');
									this.refs.hottable.hotInstance.alter('insert_row', options[0].start.row+1, null, 'custom_row_below');
								}
							}
						},
						custom_remove_row: {
							name: "Remove Rows",
							key: "Remove Rows",
							callback: (key, options) => {
								let data = this.props.data || [];
								let errorFound = false;
								let clearArray = [];
								options.forEach((option) => {
									for(var i=option.start.row;i<=option.end.row;i++) {
										if(data[i] && data[i]._restrictDelete) {
											errorFound= true;
										}
									}
									clearArray.push([option.start.row, (option.end.row - option.start.row + 1)])
								});
								if(!errorFound)
									this.refs.hottable.hotInstance.alter('remove_row', clearArray, null, 'custom_remove_row');
							}
						},
						'cut': {},
						'copy': {}
					}
				},
				columns: this.props.columns,
				comments: true,
				cell: this.props.cellArray ? this.props.cellArray : [],
				cells: !this.props.needDelete ? null : (row, col, prop) => {
					var cellProperties = {
						editor: this.props.columns[col].type
					};

					if(!this.props.needDelete)
						return cellProperties;

					if(!this.refs.hottable)
						return cellProperties;

					if(col != 1 && !this.props.columns[col].disableOnDeleteRestriction)
						return cellProperties;

					if (this.refs.hottable.hotInstance.getDataAtRowProp(row, '_restrictDelete') === true)
						cellProperties.editor = col == 1 ? 'dropdown' : false;
					else
						cellProperties.editor = col == 1 ? false : this.props.columns[col].type;

					if(cellProperties.editor === false)
						cellProperties.readOnly = true;

					return cellProperties;
				},
				fixedColumnsLeft: this.props.fixedColumnsLeft ? this.props.fixedColumnsLeft : 1,
				invalidCellClassName: 'hottablecellinvalid',
				colHeaders: (col) => {
					if(this.state && this.state.settings && this.state.settings.columns) {
						if(!this.state.settings.columns[col].helpinfo)
							return `<span>${this.state.settings.columns[col].displayTitle}</span>`;

						return `<span className="help-info" data-container="body" data-toggle="popover" data-placement="auto" data-trigger="hover" data-content="${this.state.settings.columns[col].helpinfo}">${this.state.settings.columns[col].displayTitle}</span>`;
					}
					return '';
				},
				onAfterChange: (changes, source) => {
					if (source !== 'loadData') {
						let data = this.state.settings.data;
						for (var i = 0; i < changes.length; i++) {
							changes[i][3] = typeof(changes[i][3]) == 'string' ? changes[i][3].trim() : changes[i][3];
							if (changes[i][3] === '' || changes[i][3] === null) {
								if(this.props.needEdit)
									data[changes[i][0]][changes[i][1]] = null;
								else
									delete data[changes[i][0]][changes[i][1]];
							}
							else
								data[changes[i][0]][changes[i][1]] = changes[i][3];
						}

						this.updateData(data);
					}
				},
				afterRemoveRow: (index, amount, physicalRows, source) => {
					//console.log(index, amount, physicalRows, source);
					let data = this.state.settings.data;
					//data.splice(index, amount);

					this.updateData(data, source);
				},
				afterCreateRow: (index, amount, source) => {
					let data = this.state.settings.data;
					this.updateData(data, `create_${source}`, index);
					//data.splice(index, amount);

					//this.updateData(data, source);
				},
				beforePaste: (data, coords) => {
					let colindex = coords[0].startCol;
					let numericIndexArray = [];
					for (var i = colindex; i < this.props.columns.length; i++) {
						if(this.props.columns[i].type == 'numeric')
							numericIndexArray.push(i - colindex);
					}

					if(numericIndexArray.length > 0) {
						data.forEach((datitem) => {
							numericIndexArray.forEach(numericindex => {
								if(datitem.length > numericindex) {
									datitem[numericindex] = datitem[numericindex] !== '' && datitem[numericindex] !== null && !isNaN(Number(datitem[numericindex])) ? Number(datitem[numericindex]) : datitem[numericindex];
								}
							});
						});
					}
				}
			}
		}

		this.addRow = this.addRow.bind(this);
		this.validate = this.validate.bind(this);
		this.updateData = this.updateData.bind(this);
	}

	componentWillReceiveProps(nextprops) {
		this.state.settings.cell = nextprops.cellArray ? nextprops.cellArray : 1;;
		this.state.settings.fixedColumnsLeft = nextprops.fixedColumnsLeft ? nextprops.fixedColumnsLeft : 1;
		this.state.settings.readOnly = nextprops.readOnly ? nextprops.readOnly : false;
		this.state.settings.height = '' + (nextprops.height ? nextprops.height : 500)  + '';
		if(this.state.settings.readOnly)
			this.state.settings.data = nextprops.data && nextprops.data.length >= 0 ? nextprops.data : this.state.settings.data;

		this.setState({
			settings: this.state.settings
		});
	}

	componentDidMount(nextprops) {
		Handsontable.dom.addEvent(document.getElementById(this.state.id), 'mouseover', function (event) {
			$(function () {
				$('[data-toggle="popover"]').popover()
			});
			/*if (event.target.nodeName == 'BUTTON' && event.target.className.indexOf('help-info') >= 0) {

			}*/
		});
	}

	updateData(data, source, createindex) {
		let lastIndex = null;
		data.forEach((dataprime, index) => {
			if(Object.keys(dataprime).length == 0)
				lastIndex = lastIndex === null ? index : lastIndex;
			else
				lastIndex = null;
		});
		lastIndex = lastIndex === null ? data.length : lastIndex;

		/*if(lastIndex >= 0 && lastIndex >= data.length - 20) {
			data.push({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {});
		}*/
		if(source !== 'custom_row_above' && source !== 'custom_row_below') {
			if (data.length < Max_Rows) {
				for (var i = data.length; i < Max_Rows; i++)
					data.push({});
			}
		}

		if(source === 'create_custom_row_above' || source === 'create_custom_row_below') {
			for(var prop in data[createindex])
				delete data[createindex][prop];
		}

		if(!this.props.data || this.props.data.length == 0)
			this.props.onChange(data);
		//this.props.onChange(data);
		//setTimeout(this.validate, 0);
		//this.validate();
	}

	validate(cb) {
		this.refs.hottable.hotInstance.validateCells((valid) => {
			this.valid = valid
			if (cb)
				cb(valid);
		});
	}

	addRow() {
		/*let data = [...this.state.settings.data];
		data.push({});

		this.setState({
			settings: {
				...this.state.settings,
				data
			}
		}, () => {
			this.validate();
		});*/

	}

	render() {
		return ( 
			<HotTable id={this.state.id} ref = "hottable" settings = {this.state.settings} />
		);
	}
}