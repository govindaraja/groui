import React,{Component} from 'react';
import axios from 'axios';
import XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import { currencyFilter, dateFilter, datetimeFilter, taxFilter } from '../utils/filter';
import { excel_sheet_from_array_of_arrays, excel_Workbook, excel_s2ab } from '../utils/excelutils';

export function generateExcelforImport (reportName, colDefs, dataArray, child_colDefs, child_dataArray, ParentSheetName, ChildSheetsObj, app, callback) {
	let reportData = [[]],
		widthArray = [],
		child_reportData = {},
		child_widthArray = {};

	for (let i = 0; i < colDefs.length; i++) {
		reportData[0].push({
			v : colDefs[i].displayTitle
		});
		widthArray.push({
			wch : colDefs[i].width ? colDefs[i].width / 10 : 10
		});
	}

	for (let i = 0; i < dataArray.length; i++) {
		let tempData = [];

		for (let j = 0; j < colDefs.length; j++) {
			let value = '',type = '';

			value = dataArray[i][colDefs[j].data];

			tempData.push({
				v : value,type:type
			});
		}

		reportData.push(tempData);
	}

	if (Object.keys(child_colDefs).length > 0) {
		for (let prop in child_colDefs) {
			child_reportData[prop] = [[]];
			child_widthArray[prop] = [];

			for (let i = 0; i < child_colDefs[prop].length; i++) {
				child_reportData[prop][0].push({
					v : child_colDefs[prop][i].displayTitle
				});
				child_widthArray[prop].push({
					wch : child_colDefs[prop][i].width ? child_colDefs[prop][i].width / 10 : 10
				});
			}

			if (child_dataArray[prop] && child_dataArray[prop].length > 0) {
				for (let i = 0; i < child_dataArray[prop].length; i++) {
					let tempData = [];

					for (let j = 0; j < child_colDefs[prop].length; j++) {
						let value = '',type = '';

						value = child_dataArray[prop][i][child_colDefs[prop][j].data];

						tempData.push({
							v : value,type:type
						});
					}

					child_reportData[prop].push(tempData);
				}
			}
		}
	}

	let wb = {
		SheetNames : [],
		Sheets : {}
	};

	let ws = excel_sheet_from_array_of_arrays(reportData);
	wb.SheetNames.push(ParentSheetName);
	wb.Sheets[ParentSheetName] = ws;
	ws['!cols'] = widthArray;

	for (let prop in child_reportData) {
		let ws = excel_sheet_from_array_of_arrays(child_reportData[prop]);
		// Sheet Name Should be maximum 31 characters and should not contains these characters '\','/','*','[',']',':','?'.
		var name = ChildSheetsObj[prop].replace(/[^A-Za-z0-9_()\s-@]/g, '');
		wb.SheetNames.push(name);
		wb.Sheets[name] = ws;
		ws['!cols'] = child_widthArray[prop];
	}

	let wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
	saveAs(new Blob([excel_s2ab(wbout)],{type:"application/octet-stream"}), reportName + "_" + (dateFilter(new Date())) + ".xlsx");

	callback();
}