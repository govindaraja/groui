import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import async from 'async';
import { Prompt } from "react-router-dom";
import moment from 'moment';

import HotTable from './hottable';
import * as filter from '../utils/filter';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { updatePageJSONState, updateAppState } from '../actions/actions';
import {getPageJson, checkAccess, checkActionVerbAccess, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation } from '../utils/utils';
import { LocalSelect, SelectAsync, DateElement  } from '../components/utilcomponents';
import { generateExcelforImport } from  './excelexport';


const DATE_FORMAT = "DD-MMM-YYYY";
const TIME_FORMAT = "hh:mm a";

export default connect((state, props) => {
	return {
		app : state.app,
		pagejson : state.pagejson,
	}
}, { updatePageJSONState, updateAppState })(class DataImport extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag: true,
			resource: {
				name: '',
				companyid: this.props.app.selectedcompanyid,
				companyid_name: this.props.app.selectedCompanyDetails.name,
				companyid_legalname: this.props.app.selectedCompanyDetails.legalname
			},
			errorObj: {},
			main: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
			childData: {},
			cellArray: {
				main: [],
				childData: {}
			},
			errorColumns: [{
				type: 'text',
				width: 70,
				displayTitle: 'Sheet',
				_title: 'Sheet',
				data:'sheet'
			}, {
				renderer: "IndexRenderer",
				width: 30,
				displayTitle: 'Row No',
				_title: 'Row No',
				data:'rowno'
			}, {
				type: 'text',
				width: 400,
				displayTitle: 'Errors',
				_title: 'Errors',
				data:'error'
			}],
			additionalchargesColumns: [{
				title : "Additional Charge",
				model : "additionalchargesid",
				type : "selectauto",
				resource : 'additionalchargesmaster',
				fields : 'id,name,description,applyforvaluation,defaultpercentage,displayorder',
				required: true,
				"help" : "If any additional charges need to be added to the invoice, select them here. For E.g. Travel charges.",
				import_required: true
			}, {
				title : "Description",
				model : "description",
				type : "textarea",
				required : true,
				"help" : "Description of the Additional Charges. Will be updated from the Additional Charges Master. Update if required.",
				import_required: true
			}, {
				title : "Amount",
				model : "amount",
				type : "number",
				required : true,
				"help" : "Total value of the Additional Charges.",
				import_required: true
			}],
			billingschedulesColumns: [{
				title : "Schedule Start Date",
				model : "scheduledate",
				type : "date",
				required: true,
				import_required: true,
				"help" : "Start date of the Billing Schedule. Depends on the type of schedule. For e.g., if you will bill your customer once every six months, create two line items with the start dates of the schedule.",
				import_primary: '_parentreference,scheduledate'
			}, {
				title : "Schedule End Date",
				model : "scheduleenddate",
				type : "date",
				required: true,
				import_required: true,
				"help" : "End date of the Billing Schedule for the corresponding Start Date. Depends on the type of schedule. For e.g., if you bill your customer every six months, the dates will be end dates of the six month periods.",
				import_primary: '_parentreference,scheduleenddate'
			}, {
				title : "Is Suspended",
				model : "issuspended",
				type : "checkbox",
				required: false,
				import_required: false,
				"help" : "Select this option if you want to suspend this billing schedule for the contract."
			}, {
				title : "Suspended Remarks",
				model : "suspendremarks",
				type : "textarea",
				required: false,
				import_required: false,
				"help" : "Internal remarks for reference stating the reason for suspension of the billing schedule."
			}],
			tabActive: 'Main',
			createParam: this.props.match.params.id > 0 ? false : true,
			onLoadFinished: false,
			childData: {},
			selectOptions: {},
			selectFullOptions: {},
			importResourceArray: this.getImportResources()
		};
		this.urlObj = {};

		this.pageJSONCallback = this.pageJSONCallback.bind(this);
		this.checkCondition = this.checkCondition.bind(this);
		this.getColumns = this.getColumns.bind(this);
		this.renderTabHeader = this.renderTabHeader.bind(this);
		this.cancel = this.cancel.bind(this);
		this.resourceOnChange = this.resourceOnChange.bind(this);
		this.create = this.create.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getItemById = this.getItemById.bind(this);
		this.validate = this.validate.bind(this);
		this.save = this.save.bind(this);
		this.checkValidations = this.checkValidations.bind(this);
		this.generateArray = this.generateArray.bind(this);
		this.checkPrimaryFn = this.checkPrimaryFn.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
	}

	componentWillMount() {
		if(this.state.createParam) {
			this.setState({
				onLoadFinished: true,
				loaderflag: false
			});
		} else {
			this.getItemById();	
		}
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag) {
			this.updateLoaderFlag(false);
			this.props.updateAppState('usepageloaderFlag', false);
		}
	}

	getImportResources = () => {
		let impRes = [];

		for (let prop in this.props.app.importResources) {
			impRes.push(prop);
		}

		impRes.sort((a, b) => {
			return (a.toLowerCase() > b.toLowerCase() ? 1 : (a.toLowerCase() < b.toLowerCase() ? -1 : 0));
		});

		return impRes;
	};

	getItemById() {
		this.updateLoaderFlag(true);
		axios.get(`/api/dataimports/${this.props.match.params.id}`).then((response) => {
			if (response.data.message == 'success') {
				axios.get(`documents/data.json?url=./uploads/dataimports/${response.data.main.id}/data.json`).then((dataresponse) => {
					this.setState({
						resource: response.data.main,
						ndt: {
							notes: response.data.notes,
							documents: response.data.documents,
							tasks: response.data.tasks
						},
						main: dataresponse.data.main,
						childData: dataresponse.data.childData
					}, () => {
						getPageJson(this.props, {
							title: this.props.app.importResources[this.state.resource.resource],
							type: 'Details'
						}, this.pageJSONCallback);
					});
				});
			} else {
				this.updateLoaderFlag(false);
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	pageJSONCallback(pagejson) {
		let columnObj = this.getColumns(pagejson);
		let columnArray = [{
			data: 'isError',
			displayTitle: '',
			width: 30,
			readOnly: true,
			_customObj: {},
			renderer: "RedGreenRenderer"
		}];
		if(columnObj.childArray.length > 0) {
			columnArray.push({
				type: 'text',
				width: 150,
				required: true,
				displayTitle: 'Reference *',
				_title: 'Reference',
				_customObj: {},
				data: '_reference'
			});
		}
		columnArray.push(...columnObj.columnArray);
		this.setState({
			pagejson: pagejson,
			columns: columnArray,
			onLoadFinished: true,
			childArray: columnObj.childArray,
			childObj: columnObj.childObj,
		}, () => {
			this.getSelectOptions();
		});
	}

	checkCondition(type, condition, item, itemstr) {
		// Gloabals
		let app = this.props.app;
		let feature = this.props.app.feature;
		let user = this.props.app.user;
		//let state = this.state;
		let pagejson = this.state.pagejson;
		//let initFn = (tempObj) => tempObj ? this.props.updateFormState(this.props.form, tempObj) : null;
		//let changeFn = (tempObj) => tempObj ? this.props.updateFormState(this.props.form, tempObj) : null;
		//let {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} = filter;
		let cus_modalService = modalService;
		let cus_commonMethods = commonMethods;
		let cus_axios = axios;

		// Strict to Page
		//let controller = this.controller;
		let resource = {};

		if(type == 'if')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 || (['string', 'boolean'].indexOf(typeof(condition)) >= 0 && eval(`try{${condition}}catch(e){}`));
		if(type == 'hide')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? 'hide' : '');
		if(type == 'show')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? '' : 'hide');
		if(type == 'required')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'disabled')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'value') {
			//console.log(condition)
			if(typeof(condition) == 'string') {
				if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
					return eval(`try{${condition}}catch(e){}`);
				else
					return condition;
			}
			return condition;
		}
		return null;
	}

	getColumns(pagejson, isChild) {
		let columnArray = [];
		let childArray = [];
		let childObj = [];
		if(isChild) {
			pagejson = {
				sections: [{
					fields: pagejson
				}]
			}
		}
		pagejson.sections.forEach((section) => {
			if(!this.checkCondition('if', section.import_show))
				return null;

			section.title = section.import_title ? section.import_title : section.title;
			section.fields.forEach((field) => {
				if((['text', 'textarea', 'autosuggest', 'email', 'checkbox', 'number', 'selectasync', 'selectauto', 'repeatabletable', 'date', 'time', 'billingschedules', 'additionalcharges', 'phone', 'password', 'address', 'contact'].indexOf(field.type) >= 0 || (field.type == 'select' && !field.multiselect) || ['tax'].includes(field.import_type)) && this.checkCondition('if', field.import_show)) {
					let colTempObj = {
						'helpinfo': field.help
					};
					if(['text', 'textarea', 'autosuggest', 'email', 'phone', 'password'].indexOf(field.type) >= 0) {
						colTempObj.type = 'text';
						colTempObj.width = 200;
					}
					if(['number'].indexOf(field.type) >= 0) {
						colTempObj.type = 'numeric';
						colTempObj.width = 200;
					}
					if(['checkbox'].indexOf(field.type) >= 0) {
						colTempObj.type = 'dropdown';
						colTempObj.source = ['Yes', 'No'];
					}
					if(['selectasync', 'selectauto', 'address', 'contact'].indexOf(field.type) >= 0) {
						colTempObj.type = 'autocomplete';
						colTempObj.strict = true;
						colTempObj.width = 200;
						if(field.import_filter)
							field.import_filter = `${this.checkCondition('value', field.import_filter)}`;

						if (field.type == 'address') {
							field.resource = 'getimportaddressquery';
							field.label = 'unique_displayaddress';
							field.import_filter = `parentresource = '${this.checkCondition('value', field.parentresource)}'`;
						}

						if (field.type == 'contact') {
							field.resource = 'getimportcontactquery';
							field.label = 'unique_displaycontact';
							field.import_filter = `parentresource = '${this.checkCondition('value', field.parentresource)}'`;
						}
					}
					if(['date'].indexOf(field.type) >= 0) {
						colTempObj.type = 'date';
						colTempObj.width = 200;
						colTempObj.dateFormat = DATE_FORMAT;
					}
					if(['time'].indexOf(field.type) >= 0) {
						colTempObj.type = 'time';
						colTempObj.width = 200;
						colTempObj.timeFormat = TIME_FORMAT;
					}
					if(field.type == 'repeatabletable' && field.import_include) {
						childArray.push({
							title: section.title,
							childname: field.json,
							json: pagejson[field.json].body,
							columns: [{
								data: 'isError',
								displayTitle: '',
								width: 30,
								readOnly: true,
								_customObj: {},
								renderer: "RedGreenRenderer"
							}, {
								type: 'text',
								width: 150,
								required: true,
								displayTitle: `${pagejson.importname} Reference *`,
								_title: `${pagejson.importname} Reference`,
								_customObj: {},
								data: '_parentreference'
							}, ...this.getColumns(pagejson[field.json].body, true).columnArray]
						});
						childObj[field.json] = section.title;
					}
					if (['billingschedules', 'additionalcharges'].indexOf(field.type) >= 0) {
						let tempColArray = [],
							tempTitle = '',
							tempChildname = '';

						if (field.type == 'billingschedules') {
							tempColArray = this.state.billingschedulesColumns;
							tempTitle = 'Billing Schedules';
							tempChildname = 'contractbillingschedules';
						}

						if (field.type == 'additionalcharges') {
							tempColArray = this.state.additionalchargesColumns;
							tempTitle = 'Additional Charges';
							tempChildname = 'additionalcharges';
						}

						childArray.push({
							title: tempTitle,
							childname: tempChildname,
							json: tempColArray,
							columns: [{
								data: 'isError',
								displayTitle: '',
								width: 30,
								readOnly: true,
								_customObj: {},
								renderer: "RedGreenRenderer"
							}, {
								type: 'text',
								width: 150,
								required: true,
								displayTitle: `${pagejson.importname} Reference *`,
								_title: `${pagejson.importname} Reference`,
								_customObj: {},
								data: '_parentreference'
							}, ...this.getColumns(tempColArray, true).columnArray]
						});
						childObj[tempChildname] = tempTitle;
					}
					if(field.type == 'select' && field.import_type != 'tax') {
						colTempObj.type = 'dropdown';
						colTempObj.width = 200;
					}
					if(field.import_type == 'tax') {
						colTempObj.type = 'dropdown';
						colTempObj.width = 200;
					}
					if(['repeatabletable', 'billingschedules', 'additionalcharges'].indexOf(field.type) == -1) {
						let title = this.checkCondition('value', field.import_title ? field.import_title : field.title);
						let import_required = this.checkCondition('required', field.import_required);

						let fieldModel = field.model;

						if (['address', 'contact'].includes(field.type))
							fieldModel = this.checkCondition('value', field.model)[0];

						columnArray.push({
							...colTempObj,
							displayTitle: `${title} ${field.import_required ? ' *' : ''}`,
							_title: title,
							required: field.import_required ? true : false,
							_customObj: {
								...field
							},
							data: fieldModel
						});
					}
				}
			});
		});
		return {columnArray, childArray, childObj};
	}

	getSelectOptions() {
		let selectOptions = {};
		let selectFullOptions = {};

		let internalSelectOptions = (prime, eachCB, childname) => {
			if(!['selectasync', 'selectauto', 'address', 'contact', 'select'].includes(prime._customObj.type))
				return eachCB(null);

			this.getOptions(prime._customObj, (optArray, allArray, labelKey, valueKey) => {
				let newProp = childname ? `${childname}_${prime.data}` : prime.data;
				selectOptions[newProp] = optArray;
				selectFullOptions[newProp] = {};
				allArray.forEach((opt) => {
					selectFullOptions[newProp][opt[labelKey]] = opt[valueKey];
				});
				prime.source = selectOptions[newProp];
				eachCB(null);
			});
		}

		async.eachSeries(this.state.columns, internalSelectOptions, () => {
			async.eachSeries(this.state.childArray, (child, eachChildCB) => {
				async.eachSeries(child.columns, (prime, eachCB) => internalSelectOptions(prime, eachCB, child.childname), () =>	eachChildCB(null));
			}, () => {
				this.setState({
					selectOptions, selectFullOptions
				}, () => {
					this.setState({
						columns: this.state.columns,
						loaderflag: false,
						childArray: this.state.childArray
					}, () => {
						setTimeout(() => {
							this.refs.import_main.validate();

							if (['Cancelled', 'Imported'].indexOf(this.state.resource.status) == -1)
								this.validate(null, true);
						}, 0);
					});
				});
			});
		});
	}

	getOptions(field, cb) {
		let labelKey = field.label ? field.label : 'name';
		let valueKey = field.value ? field.value : 'id';
		if(field.type == 'select') {
			let localOptions = [];

			let tempLocalOptions = field.import_localoptions ? this.checkCondition('value', field.import_localoptions) : this.checkCondition('value', field.localoptions);

			if(Array.isArray(tempLocalOptions) && tempLocalOptions.length > 0) {
				if(typeof(tempLocalOptions[0]) == 'string')
					localOptions = tempLocalOptions.map((a) => {return {id: a, name: a}});
				else
					localOptions = tempLocalOptions;
			}

			if(field.import_localoptions == 'capacityfieldArray') {
				let itemmasterFields = this.props.app.myResources['itemmaster'].fields;
				let equipmentFields = this.props.app.myResources['equipments'].fields;
				let capacityfieldArray = [];

				Object.keys(itemmasterFields).forEach((item) => {
					if(equipmentFields[`${item}`] && itemmasterFields[`${item}`].type == 'integer' && !['id', 'createdby', 'modifiedby'].includes(item)) {
						capacityfieldArray.push({
							name: item,
							displayname: itemmasterFields[`${item}`].displayName
						});
					}
				});

				localOptions = capacityfieldArray;
			}

			return cb(localOptions.map((a) => a[labelKey]), localOptions, labelKey, valueKey)
		}
		let fields = `${field.fields}`;

		let filterCondition = field.import_filter === "" || field.import_filter ? field.import_filter : (field.filter ? field.filter : '');
		let url = `/api/${field.resource}?field=${fields}&skip=0&filtercondition=${filterCondition}`;

		if(this.props.app.myResources[field.resource].type == 'query')
			url = `/api/query/${field.resource}?${filterCondition}`;

		if(this.urlObj[url])
			return cb(this.urlObj[url].options, this.urlObj[url].all, labelKey, valueKey);

		axios.get(url).then((response) => {
			if(response.data.message == 'success') {
				response.data.main.sort((a, b) => {
					return (a[labelKey].toLowerCase() > b[labelKey].toLowerCase() ? 1 : (a[labelKey].toLowerCase() < b[labelKey].toLowerCase() ? -1 : 0));
				});

				let resultArray = response.data.main.map((a) => a[labelKey]);
				this.urlObj[url] = {
					options: resultArray,
					all: response.data.main
				};
				cb(this.urlObj[url].options, this.urlObj[url].all, labelKey, valueKey);
			} else
				cb([], [], labelKey, valueKey);
		});
	}

	cancel() {
		this.props.history.goBack();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag})
	}

	checkValidations(mainitem, col, dataObj, childOptname, rowno, cellArray) {
		let newrowno = rowno + 1;
		let intCheckValidations = () => {
			childOptname = childOptname ? childOptname : '';
			if (mainitem.hasOwnProperty(col.data)) {
				if(col.type == 'text' && typeof(mainitem[col.data]) != 'string')
					return `'${col._title}' is invalid`;

				if(col.type == 'numeric' && typeof(mainitem[col.data]) != 'number')
					return `'${col._title}' is invalid`;

				if(col.type == 'dropdown' && col._customObj.type == 'checkbox' && (mainitem[col.data] !== 'Yes' && mainitem[col.data] !== 'No'))
					return `'${col._title}' is invalid`;

				if(col.type == 'dropdown' && col._customObj.type != 'checkbox' && !this.state.selectFullOptions[`${childOptname}${col.data}`].hasOwnProperty(mainitem[col.data]))
					return `'${col._title}' is invalid`;

				if(col.type == 'autocomplete' && !this.state.selectFullOptions[`${childOptname}${col.data}`].hasOwnProperty(mainitem[col.data]))
					return `'${col._title}' is invalid`;

				if (col._customObj.type == 'email' && mainitem[col.data]) {
					let x = mainitem[col.data].split(','),
						errMail = false;

					for (let i = 0; i < x.length; i++) {
						if (!(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,})$/.test(x[i].trim()))) {
							errMail = true;
							break;
						}
					}

					if (errMail)
						return `'${col._title}' is invalid`;
				}

				if(col.type == 'date' && moment(mainitem[col.data], DATE_FORMAT, true)._d == 'Invalid Date')
					return `'${col._title}' is invalid`;

				if(col.type == 'time' && moment(mainitem[col.data], TIME_FORMAT, true)._d == 'Invalid Date')
					return `'${col._title}' is invalid`;

				if(['text', 'numeric'].indexOf(col.type) >= 0) 
					dataObj[col.data] = mainitem[col.data];

				if(col.type == 'dropdown' && col._customObj.type == 'checkbox')
					dataObj[col.data] = mainitem[col.data] === 'Yes' ? true : (mainitem[col.data] === 'No' ? false : null);

				if(col.type == 'dropdown' && col._customObj.type != 'checkbox') {
					dataObj[col.data] = this.state.selectFullOptions[`${childOptname}${col.data}`][mainitem[col.data]];
					if(col._customObj.import_type == 'tax')
						dataObj[col.data] = [dataObj[col.data]];
				}

				if(col.type == 'autocomplete')
					dataObj[col.data] = this.state.selectFullOptions[`${childOptname}${col.data}`][mainitem[col.data]];

				if(col.type == 'date')
					dataObj[col.data] = moment(mainitem[col.data], DATE_FORMAT, true);

				if(col.type == 'time')
					dataObj[col.data] = moment(mainitem[col.data], TIME_FORMAT, true);
			}
			
			if(col.required && (dataObj[col.data] === null || dataObj[col.data] === '' || !mainitem.hasOwnProperty(col.data)))
				return `'${col._title}' is required`;

			return null;
		};
		let error = intCheckValidations(mainitem, col, dataObj, childOptname);
		if(error) {
			cellArray.push(`${error}`);
			mainitem.isError = true;
			return true;
		}
		return null;
	}

	validate(param, onloadCheck) {
		this.updateLoaderFlag(true);
		var dataArray = [],
			childDataObj = {}, errorArray = [], cellArray = {
				main: {},
				childData: {}
			};

		let emptyRowArray = [];
		let masterEmptyRowArray = [];
		let uniqueRefObj = {};
		let primaryObj = {};
		let childPrimaryArray = {};

		this.state.main.forEach((mainitem, mainindex) => {
			delete mainitem.isError;
			if (Object.keys(mainitem).length > 0) {
				if(emptyRowArray.length > 0)
					masterEmptyRowArray.push(emptyRowArray), emptyRowArray = [];

				let dataObj = {
					_refRowNo: mainindex
				};
				cellArray.main[mainindex] = {};

				mainitem.isError = null;
				this.state.columns.forEach((col, colindex) => {
					cellArray.main[mainindex][colindex] = [];
					this.checkValidations(mainitem, col, dataObj, null, mainindex, cellArray.main[mainindex][colindex]);

					if (col._customObj.import_unique && mainitem[col.data]) {
						uniqueRefObj[col.data] = uniqueRefObj[col.data] ? uniqueRefObj[col.data] : {};

						if (uniqueRefObj[col.data][mainitem[col.data].toString().trim().toLocaleLowerCase()]) {
							cellArray.main[mainindex][colindex].push(`${mainitem[col.data]} - '${col._title}' is duplicated`);
							mainitem.isError = true;
						} else
							uniqueRefObj[col.data][mainitem[col.data].toString().trim().toLocaleLowerCase()] = JSON.parse(JSON.stringify(mainitem));
					}

					if (col._customObj.import_primary)
						this.checkPrimaryFn(mainitem, col, primaryObj, cellArray.main[mainindex][colindex]);
				});

				dataArray.push(dataObj);
			} else {
				cellArray.main[mainindex] = {
					0: []
				};
				mainitem.isError = true;
				emptyRowArray.push(mainindex + 1);
			}
		});

		emptyRowArray.forEach((emprowindex) => {
			delete this.state.main[emprowindex - 1].isError;
		});

		if(masterEmptyRowArray.length > 0) {
			masterEmptyRowArray.forEach((emptyrows) => {
				emptyrows.forEach((emptyindex) => {
					emptyrows.reverse();
					cellArray.main[emptyindex - 1][0].push(`Please remove inbetween Empty rows - ${emptyrows.reverse().join()}`);
				});
			});
		}

		for (var k = 0; k < this.state.childArray.length; k++) {
			let child = this.state.childArray[k];
			childDataObj[child.childname] = [];

			if (this.state.childData[child.childname]) {
				let emptyChildRowArray = [];
				let masterChildEmptyRowArray = [];
				cellArray.childData[child.childname] = {};
				this.state.childData[child.childname].forEach((childitem, childindex) => {
					delete childitem.isError;
					if (Object.keys(childitem).length > 0) {

						if(emptyChildRowArray.length > 0)
							masterChildEmptyRowArray.push(emptyChildRowArray), emptyChildRowArray = [];

						let dataChiObj = {
							_refRowNo: childindex
						};
						cellArray.childData[child.childname][childindex] = {};

						if (!childPrimaryArray[child.childname])
							childPrimaryArray[child.childname] = {};

						childitem.isError = null;
						child.columns.forEach((col, colindex) => {
							cellArray.childData[child.childname][childindex][colindex] = [];
							this.checkValidations(childitem, col, dataChiObj, `${child.childname}_`, childindex, cellArray.childData[child.childname][childindex][colindex]);

							if (col._customObj.import_primary)
								this.checkPrimaryFn(childitem, col, childPrimaryArray[child.childname], cellArray.childData[child.childname][childindex][colindex]);
						});

						childDataObj[child.childname].push(dataChiObj);
					} else {
						cellArray.childData[child.childname][childindex] = {
							0: []
						};
						childitem.isError = true;
						emptyChildRowArray.push(childindex + 1);
					}
				});

				emptyChildRowArray.forEach((emprowindex) => {
					delete this.state.childData[child.childname][emprowindex - 1].isError;
				});

				if(masterChildEmptyRowArray.length > 0) {
					masterChildEmptyRowArray.forEach((emptyrows) => {
						emptyrows.forEach((emptyindex) => {
							cellArray.childData[child.childname][emptyindex - 1][0].push(`Please remove inbetween Empty rows - ${masterChildEmptyRowArray.reverse().join()}`);
						});
					});
				}
			}
		}

		if(this.state.childArray.length > 0) {
			let uniqueRefObj = {};
			dataArray.forEach((data, index) => {
				if(!data._reference)
					return null;
				if(uniqueRefObj[data._reference.toString().toLowerCase()]) {
					cellArray.main[data._refRowNo][1].push(`Duplicate Reference`);
					this.state.main[data._refRowNo].isError = true;
				} else {
					uniqueRefObj[data._reference.toString().toLowerCase()] = true;
				}
			});

			for(var prop in childDataObj) {
				childDataObj[prop].forEach((data, index) => {
					if(!data._parentreference)
						return null;
					if(!uniqueRefObj[data._parentreference.toString().toLowerCase()]) {
						cellArray.childData[prop][data._refRowNo][1].push(`Invalid Parent Reference`);
						this.state.childData[prop][data._refRowNo].isError = true;
					}
				});
			}
		}

		let newCellArray = {
			main: [],
			childData: {},
		};

		if(this.checkForError(cellArray, errorArray, newCellArray)) {
			this.showMessage(true, errorArray, newCellArray);
			return this.updateLoaderFlag(false);
		} else {
			if (!onloadCheck)
				this.showMessage(null, errorArray, newCellArray);
		}

		if(param)
			this.generateArray(dataArray, childDataObj);
		else
			this.updateLoaderFlag(false);
	}

	checkPrimaryFn = (mainitem, col, primaryObj, cellArray) => {
		let primaryArray = col._customObj.import_primary.split(',');

		let primCondition = true;

		if (col.type == 'dropdown' && col._customObj.type == 'checkbox' && mainitem[col.data] !== 'Yes')
			primCondition = false;

		if (primCondition) {
			let valueFound = true;

			for (let i = 0; i < primaryArray.length; i++) {
				if (!mainitem[primaryArray[i]]) {
					valueFound = false;
					break;
				}
			}

			if (valueFound) {
				let tempVal = [];

				primaryArray.forEach((item) => {
					tempVal.push(mainitem[item].toString().trim().toLocaleLowerCase());
				});

				let x = `${col._title.toString().trim().toLocaleLowerCase()}_${tempVal.join('_')}`;

				if (!primaryObj[x] && tempVal.length > 0)
					primaryObj[x] = true;
				else {
					cellArray.push(`'${col._title}' is duplicated`);
					mainitem.isError = true;
				}
			}
		}
	}

	checkForError(cellArray, errorArray, newCellArray) {
		let errorFound = false;
		for(var row in cellArray.main) {
			let allErrorArray = [];
			for(var cell in cellArray.main[row]) {
				if(cellArray.main[row][cell].length > 0) {
					errorFound = true;
					newCellArray.main.push({
						row: Number(row),
						col: Number(cell),
						comment: {
							value: cellArray.main[row][cell].join("\n"),
							readOnly: true
						}
					});
					cellArray.main[row][cell].forEach((err) => {
						errorArray.push({
							sheet: this.state.pagejson.importname,
							rowno: row,
							error: err
						});
					});
					if(cell != 0)
						allErrorArray.push(cellArray.main[row][cell].join("\n"));
				}
			}
			newCellArray.main.push({
				row: row,
				col: 0,
				comment: {
					value: [...cellArray.main[row][0], ...allErrorArray].join("\n"),
					readOnly: true
				}
			});
		}

		for(var prop in cellArray.childData) {
			newCellArray.childData[prop] = [];
			for(var row in cellArray.childData[prop]) {
				let allErrorArray = [];
				for(var cell in cellArray.childData[prop][row]) {
					if(cellArray.childData[prop][row][cell].length > 0) {
						errorFound = true;
						newCellArray.childData[prop].push({
							row: Number(row),
							col: Number(cell),
							comment: {
								value: cellArray.childData[prop][row][cell].join("\n"),
								readOnly: true
							}
						});
						cellArray.childData[prop][row][cell].forEach((err) => {
							errorArray.push({
								sheet: this.state.childObj[prop],
								rowno: row,
								error: err
							});
						});
						allErrorArray.push(cellArray.childData[prop][row][cell].join("\n"));
					}
				}
				newCellArray.childData[prop].push({
					row: row,
					col: 0,
					comment: {
						value: [...cellArray.childData[prop][row][0], ...allErrorArray].join("\n"),
						readOnly: true
					}
				});
			}
		}
		return errorFound;
	}

	showMessage(err, errorArray, cellArray) {
		this.setState({
			tabActive: errorArray.length > 0 ? 'Error' : this.state.tabActive,
			main: this.state.main,
			childData: this.state.childData,
			errorArray,
			cellArray
		});
		this.props.openModal(modalService["infoMethod"]({
			header : err ? "Error" : "Success",
			body : err ? "Please check the Error Message Tab" : "Validated Successfull",
			btnArray : ["Ok"],
			isToast: true,
			toastType: err ? "Error" : ""
		}));
	}

	generateArray(main, childData) {
		let resourceObj = {}, resourceArray = [];
		main.forEach((data, dataindex) => {
			resourceObj[this.state.childArray.length > 0 ? (data._reference.toString().toLocaleLowerCase()) : dataindex] = {
				...data
			};
			this.state.childArray.forEach((child) => {
				resourceObj[data._reference.toString().toLocaleLowerCase()][child.childname] = [];
			});
		});
		for(var prop in childData) {
			childData[prop].forEach((childdata) => {
				resourceObj[childdata._parentreference.toString().toLocaleLowerCase()][prop].push(childdata);
			});
		}
		for(var prop in resourceObj)
			resourceArray.push(resourceObj[prop]);

		resourceArray.sort((a, b) => {
			return a._refRowNo - b._refRowNo;
		});

		if(resourceArray.length == 0) {
			this.props.openModal(modalService["infoMethod"]({
				header : "Error",
				body : "Please enter atlease one row",
				btnArray : ["Ok"],
				isToast: true
			}));
			return this.updateLoaderFlag(false);
		}

		let resource = {
			...this.state.resource,
			data_json: {
				main: this.state.main,
				childData: this.state.childData
			},
			resourceArray: resourceArray
		};

		axios({
			method : 'post',
			data : {
				actionverb : 'Import',
				data : resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/dataimports'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);

			if (response.data.message == 'success') {
				this.setState({
					resource: response.data.main
				});
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			} else {
				if(response.data.error && response.data.error.length > 0) {

					response.data.error.sort((a, b) => {
						return (a.rowno > b.rowno ? 1 : (a.rowno < b.rowno ? -1 : 0));
					});

					this.props.openModal(modalService["infoMethod"]({
						header : "Error",
						body : "Please check the error Tab",
						btnArray : ["Ok"],
						isToast: true,
						toastType: "Error"
					}));
					let cellArray = {
						main: [],
						childData:{}
					};
					let childnameObj = {};
					this.state.childArray.forEach((childObj) => {
						childnameObj[childObj.childname] = childObj.title
					});
					let errorArray = response.data.error.map((err) => {
						if(err.childname) {
							this.state.childData[err.childname][resourceArray[err.rowno][err.childname][err.childrowno]._refRowNo].isError = true;
							return {
								sheet: childnameObj[err.childname],
								_sheetname: err.childname,
								rowno: resourceArray[err.rowno][err.childname][err.childrowno]._refRowNo,
								error: err.error
							}
						}
						console.log(err.rowno);
						this.state.main[err.rowno].isError = true;
						return {
							sheet: this.state.pagejson.importname,
							_sheetname: 'main',
							rowno: err.rowno,
							error: err.error
						}
					});
					let errorChildObj = {};
					this.state.childArray.forEach((childObj) => {
						errorChildObj[childObj.childname] = {};
						cellArray.childData[childObj.childname] = [];
					});
					let errorObj = {
						main: {},
						...errorChildObj
					};

					errorArray.forEach((err) => {
						if(!errorObj[err._sheetname][err.rowno])
							errorObj[err._sheetname][err.rowno] = [];

						errorObj[err._sheetname][err.rowno].push(err.error);
					});
					for(var prop in errorObj) {
						for(var secprop in errorObj[prop]) {
							let arr = prop == 'main' ? cellArray.main : cellArray.childData[prop];
							arr.push({
								row: secprop,
								col: 0,
								comment: {
									value: errorObj[prop][secprop].join("\n\n"),
									readOnly: true
								}
							});
						}
					}
					this.setState({ errorArray, tabActive: 'Error', main: this.state.main, childData: this.state.childData, cellArray});
				} else {
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			}
			this.updateLoaderFlag(false);
		});
	}

	updateTabActive(tabActive) {
		this.updateLoaderFlag(true);
		let childname = '';
		for(var prop in this.state.childObj) {
			if(this.state.childObj[prop] == tabActive)
				childname = prop;
		}
		this.setState({ tabActive }, () => {
			this.updateLoaderFlag(false);
			setTimeout(() => {
				if(tabActive != 'Error')
					this.refs[tabActive == 'Main' ? 'import_main' : `import_${childname}`].validate();
			}, 0);
		});
	}

	create() {
		this.updateLoaderFlag(true);
		axios({
			method : 'post',
			data : {
				actionverb : 'Save',
				data : this.state.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/dataimports'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
				if (resparam)
					this.controller.create(param, true);
			});

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					this.props.history.replace(`/details/dataimports/${response.data.main.id}`);
				} else {
					if (param == 'Delete')
						this.props.history.replace("/list/dataimports");
					else
						this.props.initialize(response.data.main);
				}
			}
			this.updateLoaderFlag(false);
		});
	}

	save(param) {
		this.updateLoaderFlag(true);
		let data_json = {
			main: [],
			childData: {}
		};
		this.state.main.forEach((data) => {
			let tempObj = {
				...data
			};
			delete tempObj.isError;
			data_json.main.push({ 
				...tempObj
			});
		});
		for(var prop in this.state.childData) {
			data_json.childData[prop] = [];
			this.state.childData[prop].forEach((data) => {
				let tempObj = {
					...data
				};
				delete tempObj.isError;
				data_json.childData[prop].push({ 
					...tempObj
				});
			});
		}

		let resource = {
			...this.state.resource,
			data_json
		};
		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/dataimports'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

			if (response.data.message == 'success') {
				if (param == 'Delete')
					this.props.history.replace("/list/dataimports");
				else
					this.setState({
						resource: response.data.main
					});
			}
			this.updateLoaderFlag(false);
		});
	}

	resourceOnChange(name, value) {
		this.setState({
			resource: {
				...this.state.resource,
				[name]: value
			}
		});
	}

	exportExcel () {
		let excelname = `${this.state.resource.resource} ${this.state.resource.importno ? '- '+this.state.resource.importno : ''}`;
		let columns = [];
		let childColumns = {};
		let rows = [];
		let childRows = {};

		this.state.columns.forEach((item) => {
			if (item.displayTitle)
				columns.push(item);
		});

		if (this.state.childArray.length > 0) {
			this.state.childArray.forEach((item, index) => {
				childColumns[item.childname] = [];
				item.columns.map((a) => {
					if (a.displayTitle)
						childColumns[item.childname].push(a);
				});
			});
		}

		if (this.state.main.length > 0) {
			this.state.main.forEach((item) => {
				if (Object.keys(item).length > 0)
					rows.push(item);
			});
		}

		if (Object.keys(this.state.childData).length > 0) {
			for (let prop in this.state.childData) {
				childRows[prop] = [];

				this.state.childData[prop].forEach((item) => {
					if (Object.keys(item).length > 0)
						childRows[prop].push(item);
				});
			}
		}

		generateExcelforImport(excelname, columns, rows, childColumns, childRows, this.state.pagejson.importname, this.state.childObj, this.props.app, () => {});
	}

	renderCreate() {
		let seriestypeErrorClass = this.state.resource.numberingseriesmasterid ? '' : 'errorinput';
		let resourceErrorClass = this.state.resource.resource ? '' : 'errorinput';

		let buttonDisabled = seriestypeErrorClass || resourceErrorClass ? true : false;
		return (
			<div className="row">
				<div className="col-sm-12 col-md-4 offset-md-4" style={{marginTop: '100px'}}>
					<div className="card marginbottom-15 borderradius-0" style={{boxShadow: '0 0px 4px 0 rgba(0, 0, 0, 0.25)'}}>
						<div className="card-body">
							<div align="center" style={{marginBottom: '25px'}}><h6 className="gs-text-color"><b>New Data Import</b></h6></div>
							<div className="row responsive-form-element">
								<div className="form-group col-sm-12">
									<label className="labelclass">Company</label>
									<input type="text" className="form-control" value={this.state.resource.companyid_name} disabled={true} />
								</div>
								<div className="form-group col-sm-12">
									<label className="labelclass">Numbering Series</label>
									<SelectAsync
										resource="numberingseriesmaster"
										fields="id,name,format,isdefault,currentvalue"
										className={`${seriestypeErrorClass}`}
										value={this.state.resource.numberingseriesmasterid}
										label="name"
										valuename="id"
										createParamFlag={true}
										filter="numberingseriesmaster.resource='dataimports'"
										defaultValueUpdateFn={(value) => this.resourceOnChange('numberingseriesmasterid', value)}
										onChange={(value) => this.resourceOnChange('numberingseriesmasterid', value)}/>
								</div>
								<div className="form-group col-sm-12">
									<label className="labelclass">Resource</label>
									<LocalSelect className={`${resourceErrorClass}`} options={this.state.importResourceArray} value={this.state.resource.resource} onChange={(value) => this.resourceOnChange('resource', value)} />
								</div>
								<div className="form-group col-sm-12">
									<label className="labelclass">Remarks</label>
									<textarea className="form-control" value={this.state.resource.remarks} onChange={(evt) => this.resourceOnChange('remarks', evt.target.value)}></textarea>
								</div>
							</div>
							<div className="col-sm-12 text-center margintop-15">
								<button
									type="button"
									className="btn btn-sm btn-width btn-secondary"
									onClick={this.cancel}>
									<i className="fa fa-times marginright-5"></i>Close
								</button>
								<button
									type="button"
									className="btn btn-sm gs-btn-success btn-width"
									disabled={buttonDisabled}
									onClick={this.create}>
									<i className="fa fa-save"></i>Save
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	renderTabHeader() {

		let tabArray = [];
		let maintabActive = this.state.tabActive == 'Main';
		tabArray.push(<li  className={`nav-item`} key={0}><span className={`nav-link cursor-ptr ${maintabActive ? 'active' : ''}`} onClick={() => this.updateTabActive('Main')}>{this.state.pagejson.importname}</span></li>);
		this.state.childArray.forEach((child, index) => {
			let tabActive = this.state.tabActive == child.title;
			tabArray.push(<li  className={`nav-item`} key={index+1}><span className={`nav-link cursor-ptr ${tabActive ? 'active' : ''}`} onClick={() => this.updateTabActive(child.title)}>{child.title}</span></li>);
		});
		if(['Cancelled', 'Imported'].indexOf(this.state.resource.status) == -1) {
			let errortabActive = this.state.tabActive == 'Error';
			tabArray.push(<li  className={`nav-item`} key={this.state.childArray.length+2}><span className={`nav-link cursor-ptr erractive ${errortabActive ? 'active' : ''}`} onClick={() => this.updateTabActive('Error')}>Error Message</span></li>);
		}
		return(
			<>
				<div className="row importheaderrow">
					<div className="col-md-12 bg-white report-header d-flex justify-content-between">
						<div className="report-header-title" style={{color: 'inherit', textTransform: 'none'}}>
							<a className="affixanchor float-left marginright-10" onClick={this.cancel}><span className="fa fa-arrow-left "></span></a>
							<span>DATA IMPORT: {this.state.resource.importno}</span>
							<label className="badge gs-badge-success marginleft-10 font-12">{this.state.resource.status}</label>
							<label className="badge gs-badge-primary marginleft-10 font-12">{this.state.resource.resource}</label>
						</div>
						{this.state.resource && ['Cancelled', 'Imported'].indexOf(this.state.resource.status) >= 0 ? <div className="text-center margintop-15">
							<div className="gs-form-alert-success px-2" style={{fontSize: '13px'}}>Datas {this.state.resource.status} successfully. Data sheet is locked. No further changes allowed.</div>
						</div> : null}
						<div className="report-header-rightpanel">
							{this.state.resource && ['Cancelled', 'Imported'].indexOf(this.state.resource.status) == -1 ? <button
								type="button"
								className="btn gs-form-btn-success btn-sm btn-width marginleft-5"
								onClick={() => this.validate()}>
								<i className="fa fa-check-square-o"></i>Validate
							</button> : null }
							<button
								type="button"
								className="btn gs-form-btn-success btn-sm btn-width marginleft-5"
								onClick={() => this.exportExcel()}>
								<i className="fa fa-file-excel-o"></i>Export Data
							</button>
						</div>
					</div>
				</div>
				<div className="row" style={{marginBottom: '20px', height: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.importheaderrow').outerHeight() - $('.importfooterrow').outerHeight() - 60}px`}}>
					<div className="col-md-12 borderradius-0" style={{width:'100%', marginTop:'5px'}}>
						<ul className="nav nav-tabs gs-import-navtabs importtabheader">
							{tabArray}
						</ul>
						{this.renderTabBody($(window).outerHeight() - $('.navbar').outerHeight() - $('.importheaderrow').outerHeight() - $('.importtabheader').outerHeight() - 100)}
					</div>
				</div>
				{/*<div className="row">
					<div className="col-md-12 bg-white report-header">
						<div align="center">
							<button type="button" onClick={this.cancel} className="btn btn-sm btn-success">Close</button>
						</div>
					</div>
				</div>*/}
			</>
		);
	}

	renderTabBody(height) {
		let tabArray = [];
		let maintabActive = this.state.tabActive == 'Main';

		let tabIf = this.state.childArray.length > 0 ? false : true;

		if(tabIf || maintabActive) {
			tabArray.push(<div style={{marginTop: '-15px'}} hidden={maintabActive ? false : true} key={0}>
				<HotTable ref="import_main" readOnly={['Cancelled', 'Imported'].indexOf(this.state.resource.status) >= 0 ? true : false} columns={this.state.columns} data={this.state.main} height={height} cellArray={this.state.cellArray.main} fixedColumnsLeft={2} onChange={(main) => this.setState({main: main})} />
			</div>);
		}

		this.state.childArray.forEach((child, index) => {
			let tabActive = this.state.tabActive == child.title;
			if(tabIf || tabActive) {
				tabArray.push(<div style={{marginTop: '-15px'}} hidden={tabActive ? false : true} key={index+1}>
					<HotTable ref={`import_${child.childname}`} readOnly={['Cancelled', 'Imported'].indexOf(this.state.resource.status) >= 0 ? true : false} columns={child.columns} data={this.state.childData[child.childname]} cellArray={this.state.cellArray.childData[child.childname]} height={height} fixedColumnsLeft={2} onChange={(data) => this.setState({childData: {...this.state.childData, [`${child.childname}`]: data}})} />
				</div>);
			}
		});
		if(['Cancelled', 'Imported'].indexOf(this.state.resource.status) == -1) {
			let errortabActive = this.state.tabActive == 'Error';
			if(tabIf || errortabActive) {
				tabArray.push(<div style={{marginTop: '-15px'}} hidden={errortabActive ? false : true} key={this.state.childArray.length + 2}>
					<HotTable readOnly={true} columns={this.state.errorColumns} height={height} data={this.state.errorArray} />
				</div>);
			}
		}
		return (
				<div className="tab-content" style={{marginTop: '20px'}}>
					<div className="tab-pane fade show active">
						{tabArray}
					</div>
				</div>
		);
	}

	getPromptMessage() {
		let msg = {
			header: 'Alert',
			body: 'Please make sure you save all those changes',
			btnTitle: 'Do you really want to exit?'
		};
		return JSON.stringify(msg);
	}

	render() {
		return (
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<Prompt when={this.state.createParam || ['Cancelled', 'Imported'].indexOf(this.state.resource.status) >= 0 ? false : true} message={() => this.getPromptMessage()} />
				{this.state.createParam ? this.renderCreate() : <>
					{this.state.onLoadFinished ? this.renderTabHeader() : null}
				</>}
				{this.state.resource && this.state.resource.id ? <div className="row" style={{paddingTop: '40px'}}>
					<div className="col-md-12 col-sm-12 col-xs-12 importfooterrow">
						<div className="muted credit text-center sticky-footer" style={{width: '100%', zIndex: '100'}}>
							<button
								type="button"
								className="btn btn-sm btn-width btn-secondary"
								onClick = {this.cancel}>
								<i className="fa fa-times marginright-5"></i>Close
							</button>
							{ this.state.resource && ['Cancelled', 'Imported'].indexOf(this.state.resource.status) == -1 && checkActionVerbAccess(this.props.app, 'dataimports', 'Save') ? <button
								type="button"
								onClick={() => this.save('Save')}
								className="btn btn-width btn-sm gs-btn-success">
								<i className="fa fa-save"></i>Save
							</button> : null }
							{ this.state.resource && this.state.resource.status == 'Draft' && checkActionVerbAccess(this.props.app, 'dataimports', 'Import') ? <button
								type="button"
								onClick={() => this.validate(true)}
								className="btn btn-width btn-sm gs-btn-success">
								<i className="fa fa-check"></i>Import
							</button> : null }
							{ this.state.resource && this.state.resource.status == 'Draft' && checkActionVerbAccess(this.props.app, 'dataimports', 'Cancel') ? <button
								type="button"
								onClick={() => this.save('Cancel')}
								className="btn btn-width btn-sm gs-btn-warning">
								<i className="fa fa-ban"></i>Cancel
							</button> : null }
							{ this.state.resource && this.state.resource.status == 'Cancelled' && checkActionVerbAccess(this.props.app, 'dataimports', 'Delete') ? <button
								type="button"
								onClick={() => this.save('Delete')}
								className="btn btn-width btn-sm gs-btn-danger">
								<i className="fa fa-trash-o"></i>Delete
							</button> : null }
						</div>
					</div>
				</div> : null}
			</>
		);
	}
})