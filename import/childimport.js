import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import async from 'async';
import { Prompt } from "react-router-dom";
import moment from 'moment';

import HotTable from './hottable';
import * as filter from '../utils/filter';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { updatePageJSONState, updateAppState } from '../actions/actions';
import {getPageJson, checkAccess, checkActionVerbAccess, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation } from '../utils/utils';
import { LocalSelect, SelectAsync, DateElement  } from '../components/utilcomponents';
import { generateExcelforImport } from  './excelexport';


const DATE_FORMAT = "DD-MMM-YYYY";
const TIME_FORMAT = "hh:mm a";

class ChildImport extends React.Component {
	constructor(props) {
		super(props);
		let restrictestimationcommercials = false;
		this.props.app.feature.restrictprojectestimationrole.some((restrictrole) => {
			if(this.props.app.user.roleid.indexOf(restrictrole) > -1) {
				restrictestimationcommercials = true;
				return true;
			}
		});
		this.state = {
			restrictestimationcommercials: restrictestimationcommercials,
			loaderflag: true,
			errorObj: {},
			main: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
			cellArray: {},
			errorColumns: [{
				renderer: "IndexRenderer",
				width: 30,
				displayTitle: 'Row No',
				_title: 'Row No',
				data:'rowno'
			}, {
				type: 'text',
				width: 400,
				displayTitle: 'Errors',
				_title: 'Errors',
				data:'error'
			}],
			tabActive: 'Main',
			onLoadFinished: false,
			selectOptions: {},
			selectFullOptions: {}
		};
		this.urlObj = {};

		this.checkCondition = this.checkCondition.bind(this);
		this.getColumns = this.getColumns.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.validate = this.validate.bind(this);
		this.checkValidations = this.checkValidations.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
		this.generateGridData = this.generateGridData.bind(this);
	}

	componentWillMount() {
		this.generateGridData();
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag) {
			this.updateLoaderFlag(false);
			this.props.updateAppState('usepageloaderFlag', false);
		}
		if(nextProps.initialArray.length != this.props.initialArray.length) {
			this.setState({
				onLoadFinished: false
			}, () => {
				this.generateGridData();
			});
		}
	}

	generateGridData() {
		let columnArray = [{
			data: 'isError',
			displayTitle: '',
			width: 30,
			readOnly: true,
			_customObj: {},
			renderer: "RedGreenRenderer"
		}];
		if(this.props.needDelete)
			columnArray.push({
				type: 'text',
				source: ['Yes'],
				displayTitle: 'Delete row',
				_title: 'Delete row',
				_customObj: {},
				data: 'deleteRow'
			});

		columnArray.push(...this.getColumns(this.props.childjson.body));

		this.setState({
			columns: columnArray
		}, () => {
			this.getSelectOptions(() => {
				let initialArray = [];
				if(this.props.initialArray && this.props.initialArray.length > 0) {
					initialArray = JSON.parse(JSON.stringify(this.props.initialArray));
					initialArray.forEach((initialitem) => {
						this.props.formatItemForEdit(initialitem);

						columnArray.forEach((column) => {
							if(!column.data && column.data == 'isError')
								return null;

							if(column.type == 'text' && column._customObj.type == 'spaninput') {
								initialitem[column.data] = this.checkCondition('value', column.data.split('span_')[1], initialitem);
								initialitem[column.data] = initialitem[column.data] === 'null' ? null : initialitem[column.data];
							}

							if(column.type == 'autocomplete' && initialitem[column.data])
								initialitem[column.data] = this.state.selectValueOptions[column.data][initialitem[column.data]] ? this.state.selectValueOptions[column.data][initialitem[column.data]] : null;

							if(column.type == 'dropdown' && column._customObj.type == 'checkbox')
								initialitem[column.data] = initialitem[column.data] === true ? 'Yes' : (initialitem[column.data] === false ? 'No' : null);

							if(column.type == 'dropdown' && column._customObj.type != 'checkbox')
								initialitem[column.data] = this.state.selectValueOptions[column.data][initialitem[column.data]] ? this.state.selectValueOptions[column.data][initialitem[column.data]] : null;

							if(column.type == 'date' && initialitem[column.data])
								initialitem[column.data] = filter.dateFilter(initialitem[column.data]);
						});
					});
				}

				this.setState({
					main: [...initialArray, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
					onLoadFinished: true,
					loaderflag: false
				}, () => {
					setTimeout(() => {
						this.refs.import_main.validate();
						this.validate(null, true, true);
					}, 0);
				});
			});
		});
	}

	checkCondition(type, condition, item) {
		// Gloabals
		let app = this.props.app;
		let feature = this.props.app.feature;
		let user = this.props.app.user;
		let state = this.state;
		let pagejson = this.state.pagejson;
		//let initFn = (tempObj) => tempObj ? this.props.updateFormState(this.props.form, tempObj) : null;
		//let changeFn = (tempObj) => tempObj ? this.props.updateFormState(this.props.form, tempObj) : null;
		let {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} = filter;
		let cus_modalService = modalService;
		let cus_commonMethods = commonMethods;
		let cus_axios = axios;

		// Strict to Page
		//let controller = this.controller;
		let resource = this.props.resource;

		if(type == 'if')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 || (['string', 'boolean'].indexOf(typeof(condition)) >= 0 && eval(`try{${condition}}catch(e){}`));
		if(type == 'hide')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? 'hide' : '');
		if(type == 'show')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? '' : 'hide');
		if(type == 'required')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'disabled')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'value') {
			//console.log(condition)
			if(typeof(condition) == 'string') {
				if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
					return eval(`try{${condition}}catch(e){}`);
				else
					return condition;
			}
			return condition;
		}
		return null;
	}

	getColumns(fields) {
		let columnArray = [];

		fields.forEach((field) => {
			if((['text', 'textarea', 'autosuggest', 'email', 'checkbox', 'number', 'selectasync', 'selectauto', 'date', 'time', 'spaninput'].indexOf(field.type) >= 0 || (field.type == 'select' && (!field.multiselect || (field.multiselect && ['taxid', 'materialtaxid', 'labourtaxid'].indexOf(field.model) > -1)) && (Array.isArray(field.localoptions) || this.checkCondition('value', field.localoptions)))) && this.checkCondition('if', field.childimport_show)) {
				let colTempObj = {
					'helpinfo': field.help
				};
				if(['text', 'textarea', 'autosuggest', 'email'].indexOf(field.type) >= 0) {
					colTempObj.type = 'text';
					colTempObj.width = 200;
				}
				if(['number'].indexOf(field.type) >= 0) {
					colTempObj.type = 'numeric';
					colTempObj.width = 200;
				}
				if(['checkbox'].indexOf(field.type) >= 0) {
					colTempObj.type = 'dropdown';
					colTempObj.source = ['Yes', 'No'];
				}
				if(['selectasync', 'selectauto'].indexOf(field.type) >= 0) {
					colTempObj.type = 'autocomplete';
					colTempObj.strict = true;
					colTempObj.width = 200;
				}
				if(['date'].indexOf(field.type) >= 0) {
					colTempObj.type = 'date';
					colTempObj.width = 200;
					colTempObj.dateFormat = DATE_FORMAT;
				}
				if(['time'].indexOf(field.type) >= 0) {
					colTempObj.type = 'time';
					colTempObj.width = 200;
					colTempObj.timeFormat = TIME_FORMAT;
				}
				if(field.type == 'select') {
					colTempObj.type = 'dropdown';
					colTempObj.width = 200;				
					field.localoptions =  this.checkCondition('value', field.localoptions);
				}
				if(['spaninput'].indexOf(field.type) >= 0) {
					colTempObj.type= "text";
					colTempObj.width = field.childimport_idfield ? 30 : 200;
				}

				colTempObj.disableOnDeleteRestriction = field.childimport_disableOnDeleteRestriction ? true : false;

				if(field.model == 'id')
					colTempObj.width = 80;

				let title = this.checkCondition('value', field.childimport_title ? field.childimport_title : field.title);
				let childimport_required = this.checkCondition('required', field.childimport_required);
				
				columnArray.push({
					...colTempObj,
					displayTitle: `${title} ${field.childimport_required ? ' *' : ''}`,
					_title: title,
					required: field.childimport_required ? true : false,
					readOnly: field.childimport_disabled || field.type == 'spaninput' ? true : false,
					_customObj: {
						...field
					},
					data: field.type != 'spaninput' ? field.model : `span_${field.model}`
				});
			}
		});
		return columnArray;
	}

	getSelectOptions(selectCB) {
		let selectOptions = {};
		let selectFullOptions = {};
		let selectValueOptions = {};

		let internalSelectOptions = (prime, eachCB, childname) => {
			if(prime._customObj.type != 'selectasync' && prime._customObj.type != 'selectauto' && prime._customObj.type != 'select')
				return eachCB(null);

			this.getOptions(prime._customObj, (optArray, allArray, labelKey, valueKey) => {
				let newProp = childname ? `${childname}_${prime.data}` : prime.data;
				selectOptions[newProp] = optArray;
				selectFullOptions[newProp] = {};
				selectValueOptions[newProp] = {};
				allArray.forEach((opt) => {
					selectFullOptions[newProp][opt[labelKey]] = opt[valueKey];
					selectValueOptions[newProp][opt[valueKey]] = opt[labelKey];
				});
				prime.source = selectOptions[newProp];
				eachCB(null);
			});
		}

		async.eachSeries(this.state.columns, internalSelectOptions, () => {
			this.setState({
				selectOptions, selectFullOptions, selectValueOptions
			}, () => {
				this.setState({
					columns: this.state.columns,
					loaderflag: false
				}, () => {
					selectCB();
				});
			});
		});
	}

	getOptions(field, cb) {
		let labelKey = field.label ? field.label : 'name';
		let valueKey = field.value ? field.value : 'id';
		if(field.type == 'select') {
			let localOptions = [];
			if(field.localoptions.length > 0) {
				if(typeof(field.localoptions[0]) == 'string')
					localOptions = field.localoptions.map((a) => {return {id: a, name: a}});
				else
					localOptions = field.localoptions;
			}
			return cb(localOptions.map((a) => a[labelKey]), localOptions, labelKey, valueKey)
		}
		let fields = `${field.resource == 'uomquery' ? 'id,name' : field.fields}`;

		let filterCondition = field.childimport_filter === "" || field.childimport_filter ? field.childimport_filter : (field.filter ? field.filter : '');
		filterCondition = field.resource == 'uomquery' ? '' : filterCondition;
		let url = `/api/${field.resource == 'uomquery' ? 'uom' : field.resource}?field=${fields}&skip=0&filtercondition=${filterCondition}`;
		if(this.urlObj[url])
			return cb(this.urlObj[url].options, this.urlObj[url].all, labelKey, valueKey);

		axios.get(url).then((response) => {
			if(response.data.message == 'success') {
				response.data.main.sort((a, b) => {
					return (a[labelKey].toLowerCase() > b[labelKey].toLowerCase() ? 1 : (a[labelKey].toLowerCase() < b[labelKey].toLowerCase() ? -1 : 0));
				});

				let resultArray = response.data.main.map((a) => a[labelKey]);
				this.urlObj[url] = {
					options: resultArray,
					all: response.data.main
				};
				cb(this.urlObj[url].options, this.urlObj[url].all, labelKey, valueKey);
			} else
				cb([], [], labelKey, valueKey);
		});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag})
	}

	checkValidations(mainitem, col, dataObj, rowno, cellArray) {
		let newrowno = rowno + 1;
		let intCheckValidations = () => {
			if (mainitem.hasOwnProperty(col.data) && mainitem[col.data] !== null && dataObj[col.data] !== '') {
				if(col.type == 'text' && typeof(mainitem[col.data]) != 'string')
					return `'${col._title}' is invalid`;

				if(col.type == 'numeric' && typeof(mainitem[col.data]) != 'number')
					return `'${col._title}' is invalid`;

				if(col.type == 'dropdown' && col._customObj.type == 'checkbox' && (mainitem[col.data] !== 'Yes' && mainitem[col.data] !== 'No'))
					return `'${col._title}' is invalid`;

				if(col.type == 'dropdown' && col._customObj.type != 'checkbox' && !this.state.selectFullOptions[`${col.data}`].hasOwnProperty(mainitem[col.data])) {
					return `'${col._title}' is invalid`;
				}

				if(col.type == 'autocomplete' && !this.state.selectFullOptions[`${col.data}`].hasOwnProperty(mainitem[col.data]))
					return `'${col._title}' is invalid`;

				if(col.data == 'deleteRow' && !(!mainitem.hasOwnProperty(col.data) || mainitem[col.data] === null || mainitem[col.data] === 'Yes'))
					return `'${col._title}' is invalid`;

				if (col._customObj.type == 'email' && mainitem[col.data]) {
					let x = mainitem[col.data].split(','),
						errMail = false;

					for (let i = 0; i < x.length; i++) {
						if (!(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,})$/.test(x[i].trim()))) {
							errMail = true;
							break;
						}
					}

					if (errMail)
						return `'${col._title}' is invalid`;
				}

				if(col.type == 'date' && moment(mainitem[col.data], DATE_FORMAT, true)._d == 'Invalid Date')
					return `'${col._title}' is invalid`;

				if(col.type == 'time' && moment(mainitem[col.data], TIME_FORMAT, true)._d == 'Invalid Date')
					return `'${col._title}' is invalid`;

				if(['text', 'numeric'].indexOf(col.type) >= 0) 
					dataObj[col.data] = mainitem[col.data];

				if(col.type == 'dropdown' && col._customObj.type == 'checkbox')
					dataObj[col.data] = mainitem[col.data] === 'Yes' ? true : (mainitem[col.data] === 'No' ? false : null);

				if(col.type == 'dropdown' && col._customObj.type != 'checkbox') {
					dataObj[col.data] = this.state.selectFullOptions[`${col.data}`][mainitem[col.data]];
				}

				if(col.type == 'autocomplete')
					dataObj[col.data] = this.state.selectFullOptions[`${col.data}`][mainitem[col.data]];

				if(col.type == 'date')
					dataObj[col.data] = moment(mainitem[col.data], DATE_FORMAT, true);

				if(col.type == 'time')
					dataObj[col.data] = moment(mainitem[col.data], TIME_FORMAT, true);
			}
			
			if(col.required && (dataObj[col.data] === null || dataObj[col.data] === '' || !mainitem.hasOwnProperty(col.data)))
				return `'${col._title}' is required`;

			return null;
		};
		let error = intCheckValidations(mainitem, col, dataObj);
		if(error) {
			cellArray.push(`${error}`);
			mainitem.isError = true;
			return true;
		}
		return null;
	}

	validate(param, onloadCheck, notshowErrorTab) {
		this.updateLoaderFlag(true);
		var dataArray = [], errorArray = [], cellArray = {};

		let emptyRowArray = [];
		let masterEmptyRowArray = [];
		let uniqueRefObj = {};
		let primaryObj = {};

		this.state.main.forEach((mainitem, mainindex) => {
			delete mainitem.isError;
			if (Object.keys(mainitem).length === 0) {
				cellArray[mainindex] = {
					0: []
				};
				mainitem.isError = true;
				emptyRowArray.push(mainindex + 1);
				return null;
			}

			if(emptyRowArray.length > 0)
				masterEmptyRowArray.push(emptyRowArray), emptyRowArray = [];

			let dataObj = {
				_refRowNo: mainindex,
				...mainitem
			};
			cellArray[mainindex] = {};

			mainitem.isError = null;
			this.state.columns.forEach((col, colindex) => {
				cellArray[mainindex][colindex] = [];
				this.checkValidations(mainitem, col, dataObj, mainindex, cellArray[mainindex][colindex]);

				if (col._customObj.childimport_unique && mainitem[col.data]) {
					uniqueRefObj[col.data] = uniqueRefObj[col.data] ? uniqueRefObj[col.data] : {};

					if (uniqueRefObj[col.data][mainitem[col.data].toString().trim().toLocaleLowerCase()]) {
						cellArray[mainindex][colindex].push(`${mainitem[col.data]} - '${col._title}' is duplicated`);
						mainitem.isError = true;
					} else {
						uniqueRefObj[col.data][mainitem[col.data].toString().trim().toLocaleLowerCase()] = JSON.parse(JSON.stringify(mainitem));
					}
				}
			});

			dataArray.push(dataObj);
		});

		emptyRowArray.forEach((emprowindex) => {
			delete this.state.main[emprowindex - 1].isError;
		});

		if(masterEmptyRowArray.length > 0) {
			masterEmptyRowArray.forEach((emptyrows) => {
				emptyrows.forEach((emptyindex) => {
					emptyrows.reverse();
					cellArray[emptyindex - 1][0].push(`Please remove inbetween Empty rows - ${emptyrows.reverse().join()}`);
				});
			});
		}

		let newCellArray = [];

		if(this.checkForError(cellArray, errorArray, newCellArray)) {
			this.showMessage(true, errorArray, newCellArray, notshowErrorTab);
			return this.updateLoaderFlag(false);
		}

		let checkTypeValidation = (data, column, cellErrorArray) => {
			let errorColumnTypeFound = false;
			if((['text', 'textarea', 'autosuggest'].indexOf(column.type) >= 0 && typeof(data[column.data]) != 'string') || (['number'].indexOf(column.type) >= 0 && typeof(data[column.data]) != 'number') || (['checkbox'].indexOf(column.type) >= 0 && typeof(data[column.data]) != 'boolean') || (['date', 'time'].indexOf(column.type) >= 0 && new Date(data[column.data]) == 'Invalid Date'))
				errorColumnTypeFound = true;
				
			if (column.type == 'email') {
				if(typeof(data[column.data]) == 'string') {
					let x = mainitem[col.data].split(',');

					for (let i = 0; i < x.length; i++) {
						if (!(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,})$/.test(x[i].trim()))) {
							errorColumnTypeFound = true;
							break;
						}
					}
				} else {
					errorColumnTypeFound = true;
				}
			}

			if (column.type == 'dropdown' && column._customObj.type != 'checkbox') {
				if(!this.state.selectValueOptions[`${column.data}`][data[column.data]]) {
					errorColumnTypeFound = true;
				}
			}

			if(errorColumnTypeFound)
				cellErrorArray.push(`'${column._title}' is invalid`);
		}

		let checkRequiredValidation = (data, column, cellErrorArray) => {
			let errorRequiredFound = false;

			if(column.childimport_required) {
				if(data[column.data] === null || data[column.data] === '' || data[column.data] === undefined || !data.hasOwnProperty(column.data))
					errorRequiredFound = true;
			}

			if(errorRequiredFound)
				cellErrorArray.push(`'${column._title}' must have a value.`);
		}

		let checkShowValidation = (data, column, cellErrorArray) => {
			let errorShowFound = false, errorRequiredFound = false;

			let childimport_conditional_show_string = column._customObj.childimport_conditional_show ? column._customObj.childimport_conditional_show : column._customObj.if;


			let childimport_conditional_show = this.checkCondition('if', childimport_conditional_show_string, data);

			if(childimport_conditional_show === false || childimport_conditional_show === undefined) {
				if(data[column.data] !== null && data[column.data] !== '' && data[column.data] !== undefined && data.hasOwnProperty(column.data))
					errorShowFound = true;
			} else {

				let childimport_conditional_required_string = column._customObj.childimport_conditional_required ? column._customObj.childimport_conditional_required : column._customObj.required;

				let childimport_conditional_required = this.checkCondition('required', childimport_conditional_required_string, data);

				if(childimport_conditional_required) {
					if(data[column.data] === null || data[column.data] === '' || data[column.data] === undefined || !data.hasOwnProperty(column.data))
						errorRequiredFound = true;
				}
			}

			if(errorShowFound)
				cellErrorArray.push(`'${column._title}' should not have a value. For more info, please check the field help.`);

			if(errorRequiredFound) {
				let errorText = column._customObj.type == 'checkbox' ? `'${column._title}' must be "Yes" or "No".` : `'${column._title}' must have a value.`;

				cellErrorArray.push(errorText);
			}
		}

		dataArray.forEach((data, index) => {
			this.state.columns.forEach((column, colindex) => {
				cellArray[data._refRowNo][colindex] = [];
				if(data.hasOwnProperty(column.data) && data[column.data] !== null) {
					checkTypeValidation(data, column, cellArray[data._refRowNo][colindex]);
				}
				checkRequiredValidation(data, column, cellArray[data._refRowNo][colindex]);
				checkShowValidation(data, column, cellArray[data._refRowNo][colindex]);
				if(cellArray[data._refRowNo][colindex].length > 0)
					this.state.main[data._refRowNo].isError = true;
			});
		});

		if(this.checkForError(cellArray, errorArray, newCellArray)) {
			this.showMessage(true, errorArray, newCellArray, notshowErrorTab);
			return this.updateLoaderFlag(false);
		} else {
			if(this.props.parentValidation) {
				this.props.parentValidation(dataArray, cellArray, (dataArray, cellArray) => {
					if(this.checkForError(cellArray, errorArray, newCellArray)) {
						this.showMessage(true, errorArray, newCellArray, notshowErrorTab);
						return this.updateLoaderFlag(false);
					} else {
						this.updateLoaderFlag(false);
						if (!onloadCheck) {
							this.showMessage(null, errorArray, newCellArray, notshowErrorTab);
							if(param) {
								for (var i = dataArray.length - 1; i >= 0; --i) {
									if(dataArray[i].deleteRow == "Yes") {
										dataArray.splice(i, 1);
									}
								}
								this.props.importCB(dataArray);
							}
						}
					}
				});
			} else {
				if (!onloadCheck) {
					this.showMessage(null, errorArray, newCellArray, notshowErrorTab);
					if(param) {
						for (var i = dataArray.length - 1; i >= 0; --i) {
							if(dataArray[i].deleteRow == "Yes") {
								dataArray.splice(i, 1);
							}
						}
						this.props.importCB(dataArray);
					}
				}

				this.updateLoaderFlag(false);
			}
		}
	}

	checkForError(cellArray, errorArray, newCellArray) {
		let errorFound = false;
		for(var row in cellArray) {
			let allErrorArray = [];
			for(var cell in cellArray[row]) {
				if(cellArray[row][cell].length > 0) {
					errorFound = true;
					newCellArray.push({
						row: Number(row),
						col: Number(cell),
						comment: {
							value: cellArray[row][cell].join("\n"),
							readOnly: true
						}
					});
					cellArray[row][cell].forEach((err) => {
						errorArray.push({
							rowno: row,
							error: err
						});
					});
					if(cell != 0)
						allErrorArray.push(cellArray[row][cell].join("\n"));
				}
			}
			newCellArray.push({
				row: row,
				col: 0,
				comment: {
					value: [...cellArray[row][0], ...allErrorArray].join("\n"),
					readOnly: true
				}
			});
		}
		return errorFound;
	}

	showMessage(err, errorArray, cellArray, notshowErrorTab) {
		this.setState({
			tabActive: notshowErrorTab ? this.state.tabActive : (errorArray.length > 0 ? 'Error' : this.state.tabActive),
			main: this.state.main,
			errorArray,
			cellArray
		});

		this.props.openModal(modalService["infoMethod"]({
			header : err ? "Error" : "Success",
			body : err ? "Please check the Error Message Tab" : "Validated Successful",
			btnArray : ["Ok"],
			isToast: true,
			toastType: err ? "Error" : ""
		}));
	}
	
	exportExcel () {
		let excelname = `${this.props.importname}`;
		let columns = [];
		let rows = [];

		this.state.columns.forEach((item) => {
			if (item.displayTitle && item.displayTitle != 'Delete row')
				columns.push(item);
		});

		if (this.state.main.length > 0) {
			this.state.main.forEach((item) => {
				if (Object.keys(item).length > 0)
					rows.push(item);
			});
		}

		generateExcelforImport(excelname, columns, rows, {}, {}, this.props.importname, {}, this.props.app, () => {});
	}

	updateTabActive(tabActive) {
		this.updateLoaderFlag(true);
		this.setState({ tabActive }, () => {
			this.updateLoaderFlag(false);
			setTimeout(() => {
				if(tabActive != 'Error')
					this.refs['import_main'].validate();
			}, 0);
		});
	}

	renderTabHeader() {

		let tabArray = [];
		let maintabActive = this.state.tabActive == 'Main';
		tabArray.push(<li  className={`nav-item`} key={0}><span className={`nav-link ${maintabActive ? 'active' : ''}`} onClick={() => this.updateTabActive('Main')}>Main</span></li>);
		
		let errortabActive = this.state.tabActive == 'Error';
		tabArray.push(<li  className={`nav-item`} key={1}><span className={`nav-link erractive ${errortabActive ? 'active' : ''}`} onClick={() => this.updateTabActive('Error')}>Error Message</span></li>);
		return(
			<div>
				<div className="row">
					<div className="borderradius-0" style={{width:'100%', marginTop:'5px'}}>
						<ul className="nav nav-tabs gs-import-navtabs importtabheader">
							{tabArray}
						</ul>
						{this.renderTabBody()}
					</div>
				</div>
			</div>
		);
	}

	renderTabBody() {
		let tabArray = [];
		let maintabActive = this.state.tabActive == 'Main';
		let errortabActive = this.state.tabActive == 'Error';

		tabArray.push(<div style={{marginTop: '-15px'}} hidden={maintabActive ? false : true} key={0}>
			<HotTable ref="import_main" readOnly={false} columns={this.state.columns} data={this.state.main} height={this.props.height} cellArray={this.state.cellArray} fixedColumnsLeft={2} onChange={(main) => this.setState({main: main})} needEdit={true} needDelete={this.props.needDelete} />
		</div>);

		tabArray.push(<div style={{marginTop: '-15px'}} hidden={errortabActive ? false : true} key={1}>
			<HotTable readOnly={true} columns={this.state.errorColumns} height={this.props.height} data={this.state.errorArray} />
		</div>);

		return (
				<div className="tab-content" style={{marginTop: '20px'}}>
					<div className="tab-pane fade show active">
						{tabArray}
					</div>
				</div>
		);
	}

	render() {
		return (
			<div className="col-md-12">
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div>
					{this.state.onLoadFinished ? this.renderTabHeader() : null}
				</div>
			</div>
		);
	}
}

export default ChildImport;
