import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import { enableBatching } from 'redux-batched-actions';
import 'font-awesome/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/reactcustom.css';
import './css/responsive.css';
import './css/reportfilter.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import './lib/bootstrap-wysiwyg.min.js';

import App from './containers/app';
//import Modal from './containers/modal';
import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(enableBatching(reducers))}>
      <App/>
  </Provider>
  , document.querySelector('.react-container'));
