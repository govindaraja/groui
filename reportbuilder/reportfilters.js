import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { commonMethods, modalService } from '../utils/services';
import { DateEle, localSelectEle, autoSelectEle, selectAsyncEle, checkboxEle, textareaEle, NumberEle, SpanEle } from '../components/formelements';
import { currencyFilter, dateFilter, datetimeFilter, timeFilter } from '../utils/filter';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation, checkActionVerbAccess, requiredNewValidation } from '../utils/utils';
import { ReportNumberEle, ReportBooleanEle, ReportDateEle, ReportLocalSelectEle, ReportAutoSelectEle, ReportAutoMultiSelectEle } from './reportformelements';

export class ReportFilterComponent extends Component {
	constructor (props) {
		super (props);

		this.state = {
			loaderflag: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.addRow = this.addRow.bind(this);
		this.deleteRow = this.deleteRow.bind(this);
		this.renderFilters = this.renderFilters.bind(this);
		this.getFieldType = this.getFieldType.bind(this);
		this.renderOperators = this.renderOperators.bind(this);
	};

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	};

	addRow(filter_index) {
		let condition = [...this.props.resource.config.filter];

		if (filter_index || filter_index == 0)
			condition.splice(filter_index+1, 0, {
				required: this.props.resource.type == 'Performance Target' ? true : false,
				locked: this.props.resource.type == 'Performance Target' ? true : false,
				value: null
			});
		else
			condition.push({
				required: this.props.resource.type == 'Performance Target' ? true : false,
				locked: this.props.resource.type == 'Performance Target' ? true : false,
				value: null
			});

		condition = condition.map((item, index) => {
			return {...item, index}
		});

		this.props.updateFormState(this.props.form, {
			[`config.filter`]: condition
		});
	};

	deleteRow(index) {
		let condition = [...this.props.resource.config.filter];

		condition.splice(index, 1);

		this.props.updateFormState(this.props.form, {
			[`config.filter`]: condition
		});
	};

	getFieldType (item, itemval, itemstr, index) {
		let { resource, FilterObj, app } = this.props;

		let fieldValue = null;

		if (FilterObj[itemval].isForeignKey && FilterObj[itemval].foreignKeyOptions.resource == 'companymaster')
			fieldValue = 'Current Company'

		this.props.updateFormState(this.props.form, {
			[`${itemstr}.field`]: itemval,
			[`${itemstr}.required`]: this.props.resource.type == 'Performance Target' ? true : false,
			[`${itemstr}.locked`]: this.props.resource.type == 'Performance Target' ? true : false,
			[`${itemstr}.value`]: fieldValue
		});
	};

	renderFilters () {
		let resource = this.props.resource;

		return (
			<div className="form-group col-md-12">
				<table className="table gs-table gs-item-table-bordered">
					<thead>
						<tr>
							<th style = {{width: '30%'}}>Field</th>
							<th style = {{width: '40%'}}>Value</th>
							<th className = "text-center" style = {{width: '10%'}}>Mandatory</th>
							<th className = "text-center" style = {{width: '10%'}}>Locked</th>
							<th className = "text-center" style = {{width: '10%'}}></th>
						</tr>
					</thead>
					<tbody>
						{this.props.fields.map((item, index) => {
							let itemObj = eval(`try{resource.${item}}catch(e){}`);

							return (
								<tr key={index}>
									<td>
										<Field
											name = {`config.filter[${index}].field`}
											props = {{
												valuename: 'field',
												label: 'displayName',
												required: true,
												onChange: (value) => this.getFieldType(itemObj, value, `config.filter[${index}]`, index),
												options: [...this.props.FilterArray]
											}}
											component = {localSelectEle}
											validate = {[stringNewValidation({
												required: true
											})]}
										/>
									</td>
									<td>
										{ itemObj.field ? this.renderOperators (itemObj.field, index, `config.filter[${index}]`) : null}
									</td>
									<td className = "text-center">
										<Field
											name = {`config.filter[${index}].required`}
											props = {{
												disabled: this.props.resource.type == 'Performance Target'
											}}
											component = {checkboxEle} />
									</td>
									<td className = "text-center">
										<Field
											name = {`config.filter[${index}].locked`}
											props = {{
												disabled: this.props.resource.type == 'Performance Target'
											}}
											component = {checkboxEle} />
									</td>
									<td className = "text-center">
										<button
											type = "button"
											onClick = {() => {this.addRow(index)}}
											className = "btn btn-sm gs-form-btn-primary btnhide marginright-3"
											rel = "tooltip"
											title = "Add Filter">
											<span className = "fa fa-plus"></span>
										</button>
										<button
											type = "button"
											onClick = {()=>{this.deleteRow(index)}}
											className="btn btn-sm gs-form-btn-danger marginleft-3"
											rel = "tooltip"
											title = "Delete Filter">
											<span className = "fa fa-trash-o"></span>
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		);
	};

	renderOperators (itemName, index, itemstr) {
		let { resource, FilterObj, FilterArray } = this.props,
			renderType = '',
			fieldProps = {
				required: resource.config.filter[index].locked,
				placeholder: FilterObj[itemName].displayName
			},
			validateProps = {
				required: `resource.config.filter[${index}].locked`,
				title : FilterObj[itemName].displayName
			};

		let tempvalidate = [requiredNewValidation(validateProps)];

		if (FilterObj[itemName].type == 'string' && FilterObj[itemName].group == 'localselect') {
			renderType = ReportLocalSelectEle;
			fieldProps.options = FilterObj[itemName].localOptions;
		}

		if (FilterObj[itemName].type == 'string' && FilterObj[itemName].group == 'resource') {
			renderType = ReportLocalSelectEle;
			fieldProps.options = this.props.resourceArray;
		}

		if (FilterObj[itemName].type == 'boolean') {
			renderType = ReportBooleanEle;
		}

		if (FilterObj[itemName].type == 'date') {
			renderType = ReportDateEle;
		}

		if (FilterObj[itemName].type == 'integer' && !FilterObj[itemName].isForeignKey) {
			renderType = ReportNumberEle;
		}

		if (FilterObj[itemName].isForeignKey) {
			renderType = ReportAutoSelectEle;

			fieldProps.resource = FilterObj[itemName].foreignKeyOptions.resource;
			fieldProps.fields = `id,${FilterObj[itemName].foreignKeyOptions.mainField.split('.')[1]}`;
			fieldProps.label = `${FilterObj[itemName].foreignKeyOptions.mainField.split('.')[1]}`;

			if (FilterObj[itemName].foreignKeyOptions.resource == 'numberingseriesmaster' && FilterObj[itemName].numberingseries_resource)
				fieldProps.filter = `numberingseriesmaster.resource = '${FilterObj[itemName].numberingseries_resource}'`;
		}

		if (FilterObj[itemName].isArrayForeignKey) {
			renderType = ReportAutoMultiSelectEle;

			fieldProps.resource = FilterObj[itemName].foreignKeyArrayOptions.resource;
			fieldProps.fields = `id,${FilterObj[itemName].foreignKeyArrayOptions.mainField}`;
			fieldProps.label = `${FilterObj[itemName].foreignKeyArrayOptions.mainField}`;
		}

		return (
			<Field
				name = {`${itemstr}.value`}
				props = {fieldProps}
				component = {renderType}
				validate = {tempvalidate}
			/>
		);
	};

	render() {
		const { app, resource} = this.props;

		if (!resource)
			return null;

		return (
			<div className = "col-md-12">
				{this.props.fields.length > 0 ? this.renderFilters () : null}
				<div className = "form-group col-md-12 col-sm-12 col-xs-12">
					<button
						type = "button"
						onClick = {()=>{this.addRow()}}
						rel = "tooltip"
						title = "Add Filter"
						className = "btn btn-sm btn-gs btn-outline-primary filter-button">
						<span className = "fa fa-plus" ></span> Add Filter
					</button>
				</div>
			</div>
		);
	};
}