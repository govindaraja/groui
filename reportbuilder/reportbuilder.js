import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, Fields, FieldArray, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateReportBuilderJSONState, updateFormState, updateReportFilter } from '../actions/actions';
import { requiredNewValidation, getReportBuilderJson, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation, checkActionVerbAccess, checkMustArray } from '../utils/utils';
import { InputEle, SpanEle, DateEle, textareaEle, selectAsyncEle, localSelectEle, autoSelectEle, checkboxEle,  autosuggestEle, autoMultiSelectEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { currencyFilter } from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';
import { LocalSelect, Init } from '../components/utilcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

import { Reactuigrid } from '../components/reportcomponents';
import { ReportNumberEle, ReportBooleanEle, ReportDateEle, ReportLocalSelectEle, ReportAutoSelectEle, ReportAutoMultiSelectEle } from './reportformelements';

import {ChildEditModal} from '../components/utilcomponents';

import ReactBIChart from '../components/newreportbicomponents';

class ReportBuilderForm extends Component {
	constructor(props) {
		super(props);
		let resourceArray = [];

		let myResources = this.props.app.myResources;

		for (let prop in myResources) {
			if (myResources[prop].type != 'query')
				resourceArray.push({
					id : prop,
					name : myResources[prop].displayName
				});
		}

		this.state = {
			filterToggleOpen: true,
			isFiltersAvailable: false,
			resourceArray: resourceArray,
			isParameterFilterAvailable: false,
			isRequiredFilterAvailable: false,
			isAccess: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.reportBuilderJSONCallback = this.reportBuilderJSONCallback.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.renderFilterTags = this.renderFilterTags.bind(this);
		this.renderdefinedFilterTags = this.renderdefinedFilterTags.bind(this);
		this.renderCustomMessage = this.renderCustomMessage.bind(this);
		this.editFilter = this.editFilter.bind(this);
		this.reportOnChange = this.reportOnChange.bind(this);
		this.addMissedColumns = this.addMissedColumns.bind(this);
		this.renderDashboard = this.renderDashboard.bind(this);
		this.openDrillDown = this.openDrillDown.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	componentWillReceiveProps(nextprops) {
		if(this.props.isMaximize !== nextprops.isMaximize && nextprops.isMaximize == false) {
			setTimeout(() => {
				this.setState({
					filterToggleOpen: this.state.filterToggleOpen
				});
			}, 100);
		}
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		getReportBuilderJson(this.props, this.props.match.params.id, this.reportBuilderJSONCallback);
	}

	reportOnChange(id, itemObj){
		this.props.history.replace("/reportbuilder/" + id);
	}

	editFilter(){
		let { isFiltersAvailable, isParameterFilterAvailable } = this.state;

		let getBody = (closeModal) => {
			return (
				<ReportBuildFilterForm />
			)
		}
		this.props.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} app={this.props.app} updateFormState={this.props.updateFormState} getReportData={this.getReportData} openModal={this.openModal} closeModal={closeModal} resourceArray={this.state.resourceArray} getBody={getBody} isDashboard = {this.props.isDashboard} />}, className: {content: 'react-modal-custom-class-40', overlay: 'react-modal-overlay-custom-class'},confirmModal: true});
	}

	reportBuilderJSONCallback(reportjson) {
		if (!document.location.hash.includes('dashboardbuilder') && !document.location.hash.includes('dashboardwidget'))
			document.getElementById("pagetitle").innerHTML = `${reportjson.name}`;

		let tempObj={};

		if(this.props.reportdata){
			tempObj={
				...this.props.reportdata
			}
			if(this.props.isDrillDown) {
				tempObj.originalRows = [];
				tempObj.filter = JSON.parse(JSON.stringify(this.props.drillDownAppliedFilterObj ? this.props.drillDownAppliedFilterObj : {}));
				tempObj.parameterfilter = JSON.parse(JSON.stringify(this.props.drillDownAppliedParameterFilterObj ? this.props.drillDownAppliedParameterFilterObj : {}));
			}
		}else{
			tempObj = {
				filters: {},
				columnsinfo: reportjson.columnsinfo ? reportjson.columnsinfo : {},
				datasetid : reportjson.datasetid,
				freezedcolumns : reportjson.config.freezedcolumns ? reportjson.config.freezedcolumns : 0,
				reportfilter: reportjson.config.filter ? reportjson.config.filter : [],
				reportType: reportjson.type,
				reportdisplayName: reportjson.name,
				allowexportexcel : reportjson.hasexcelexport,
				foldername : reportjson.foldername,
				module : reportjson.module,
				reportid : reportjson.id,
				reportjson : reportjson,
				definedParameters: reportjson.definedParameters ? reportjson.definedParameters : [],
				parameterfilter: {},
				...this.props.reportdata
			};

			tempObj.filter = {};

			tempObj.reportfilter.map((filter,index)=>{
	 			tempObj.filter[filter.field] = filter.value;
	 		});

			tempObj.definedParameters.map((filter,index) => {
				if (filter == 'companyid')
					tempObj.parameterfilter[filter] = this.props.app.selectedcompanyid;

				if (filter == 'fromdate')
					tempObj.parameterfilter[filter] = new Date(new Date(new Date().setMonth((new Date().getMonth() - 1))).setHours(0, 0, 0, 0));

				if (filter == 'todate')
					tempObj.parameterfilter[filter] = new Date(new Date().setHours(0, 0, 0, 0));

				if (filter == 'asondate')
					tempObj.parameterfilter[filter] = new Date(new Date().setHours(0, 0, 0, 0));

				if (filter == 'stockbasedon')
					tempObj.parameterfilter[filter] = `transactiondate`;
			});

			if(this.props.isDrillDown) {
				tempObj.filter = JSON.parse(JSON.stringify(this.props.drillDownAppliedFilterObj ? this.props.drillDownAppliedFilterObj : {}));
				tempObj.parameterfilter = JSON.parse(JSON.stringify(this.props.drillDownAppliedParameterFilterObj ? this.props.drillDownAppliedParameterFilterObj : {}));
			}

			if(reportjson.type =='Normal'){
				tempObj.columns = reportjson.config.fields.map((field,index)=>{
					let columninfo = reportjson.columnsinfo[field];
					let tempColumn = {
						name : columninfo.displayName,
						key : columninfo.isForeignKey ? `${field}_dn` : (columninfo.isArrayForeignKey ? `text${field}` : field),
						cellClass :(columninfo.type == 'string') ? "text-left" : ((columninfo.type == 'integer' && !columninfo.isForeignKey) ? "text-right" : "text-center"),
						width : 150,
						locked : (index+1 <= tempObj.freezedcolumns) ? true : false
					};

					if(columninfo.filterformat == 'date')
						tempColumn.width = 150;
					else if(columninfo.filterformat == 'datetime')
						tempColumn.width = 200;
					else if(columninfo.isForeignKey)
						tempColumn.width = 200;
					else if(columninfo.isArrayForeignKey)
						tempColumn.width = 250;

					if (reportjson.config.sumaggregation.includes(field))
						tempColumn.footertype = 'sum';

					if (columninfo.isForeignKey && this.props.app.myResources[columninfo.foreignKeyOptions.resource].subgroup == 'Transactions'){
						tempColumn.format = 'anchortag';
						tempColumn.transactionname = columninfo.foreignKeyOptions.resource;
						tempColumn.transactionid = field;
					} else if (columninfo.title && this.props.app.myResources[columninfo.title_resource].subgroup == 'Transactions') {
						tempColumn.format = 'anchortag';
						tempColumn.transactionname = columninfo.title_resource;
						tempColumn.transactionid = `${field}_id`;
					} else if (columninfo.isArrayForeignKey) {
						tempColumn.format = 'multiSelectAutoFilter';
					} else if (columninfo.filterformat) {
						tempColumn.format = columninfo.filterformat == 'currency' ? 'number' : (columninfo.filterformat == 'tax' ? 'taxFilter' : (columninfo.filterformat == 'multiselectauto' ? 'multiSelectAutoFilter' : columninfo.filterformat));
					} else {
						tempColumn.format = (columninfo.type == 'integer' && !columninfo.isForeignKey) ? 'number' : null;
					}
					return tempColumn;
				});
			}
		}

		let filtersFound = false,
			parameterfilterFound = false,
			requiredfilterArray = [],
			requiredfilterFound = false,
			requiredparameterfilterFound = false;

		if (tempObj.reportfilter.length > 0) {
			filtersFound = true;

			for (let i = 0; i < tempObj.reportfilter.length; i++) {
				if (tempObj.reportfilter[i].required)
					requiredfilterArray.push(tempObj.reportfilter[i].field);
			}

			if (requiredfilterArray.length > 0)
				requiredfilterArray.some((item) => {
					if (tempObj.filter[item] == null) {
						requiredfilterFound = true;
						return true;
					} else
						return false;
				});
		}

		if (tempObj.definedParameters.length > 0)
			for (let i = 0; i < tempObj.definedParameters.length; i++) {
				if (tempObj.definedParameters[i] != 'companyid') {
					parameterfilterFound = true;
					//break;

					if (!['asondateoptional'].includes(tempObj.definedParameters[i]) && tempObj.parameterfilter[tempObj.definedParameters[i]] == null) {
						requiredparameterfilterFound = true;
						break;
					}
				}
			}

		requiredfilterFound = requiredfilterFound || requiredparameterfilterFound;

		let isAccess = checkMustArray(reportjson.allowedroles, this.props.app.user.roleid) ? true : false;

		this.setState({
			isFiltersAvailable: filtersFound,
			isParameterFilterAvailable: parameterfilterFound,
			isRequiredFilterAvailable: requiredfilterFound,
			isAccess
		});

		this.props.initialize(tempObj);

		setTimeout(() => {
			if (!isAccess)
				return null;

			if (this.props.reportdata)
				this.getReportData();
			else {
				if (this.props.isDashboard) {
					if (!requiredfilterFound)
						this.getReportData();
				} else {
					if (requiredfilterFound)
						this.editFilter();
					else
						this.getReportData();
				}
			}
		}, 0);

		this.updateLoaderFlag(false);
	}

	getReportData (filterObj) {
		this.updateLoaderFlag(true);
		//this.props.array.removeAll('originalRows');
		this.props.updateFormState(this.props.form, {
			originalRows: null
		});

		let filterString = [];
		filterString.push(`id=${this.props.match.params.id}`);
		filterString.push(`datasetid=${this.props.resource.datasetid}`);
		if(this.props.isDrillDown) {
			filterString.push(`filter=${encodeURIComponent(JSON.stringify({...this.props.resource.filter, ...this.props.drillDownFilterObj}))}`);
			filterString.push(`isdrilldown=true`);
		} else {
			filterString.push(`filter=${encodeURIComponent(JSON.stringify(this.props.resource.filter))}`);
		}
		filterString.push(`parameterfilter=${encodeURIComponent(JSON.stringify(this.props.resource.parameterfilter))}`);

		axios.get(`/api/query/reportbuilderquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let resultData = {};
				
				if(response.data.main.length == 10000) {
					let apiResponse = commonMethods.apiResult({
						data: {
							message: 'Report exceeds 10,000 lines. Currently showing first 10,000 lines. Please edit the filters and try a shorter duration'
						}
					});
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
				if (this.props.resource.reportType == 'Analytics' && this.props.resource.reportjson.config.analyticstype != 'Table')
					resultData = this.addMissedColumns(response.data.main);
				else
					resultData = response.data.main;

				this.setState({
					isRequiredFilterAvailable: false
				}, () => {
					this.props.updateFormState(this.props.form, {
						originalRows: resultData
					});

					if(this.props.isDrillDown) {
						setTimeout(() => {
							this.setState({
								refreshFlag: true
							});
						}, 0)
					}
				});

				this.props.updateReportFilter(this.props.form,this.props.resource);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	openDrillDown(filterObj) {
		if(!this.props.resource.reportjson.enabledrilldown)
			return false;

		let filterGenObj = {}, filterDisplayObj = [];
		let { resource } = this.props;

		for(var prop in filterObj) {
			let tempObj = filterObj[prop];
			filterGenObj[prop] = {
				operator: 'Equals',
				value: null
			};

			if(tempObj.value == '--Empty--')
				tempObj.value = 'No Value';

			if(tempObj.value != 'No Value') {
				for(var i=0; i<resource.originalRows.length; i++) {
					if(resource.originalRows[i][tempObj.valueProp] == tempObj.value) {
						let columnObj = this.props.resource.columnsinfo[prop];
						if(columnObj.type == 'date')
							filterGenObj[prop].value = resource.originalRows[i][prop];
						else if(columnObj.type == 'integer' && !columnObj.isForeignKey)
							filterGenObj[prop].value = resource.originalRows[i][prop];
						else
							filterGenObj[prop].value = [resource.originalRows[i][prop]];
					}
				}
			} else {
				filterGenObj[prop] = 'Is Empty';
			}

			filterDisplayObj.push({
				prop: filterObj[prop].valueDisplayProp,
				value: filterObj[prop].value
			});
		}

		//this.getReportData(filterGenObj);

		let DrillDownReportBuilderForm = require(`./reportbuilder`).default;

		this.props.openModal({render: (closeModal) => {
			return (
				<div>
					<div className="react-modal-header">
						<div style={{width: '90%', float: 'left'}}>
							<h5 className="modal-title gs-text-color" style={{marginTop: '5px'}}>{this.props.resource.reportjson.name} - Detailed View</h5>
						</div>
						<div style={{width: '10%', display: 'flex', flexDirection: 'row-reverse', paddingRight: '15px'}}>
							<div style={{fontWeight: 100, fontSize: '30px', color: 'rgb(149, 149, 149)', cursor: 'pointer'}} onClick={closeModal}>x</div>
						</div>
					</div>
					<div className={`react-modal-body`}>
						<div className="row gs-drilldown-display" style={{marginBottom: '15px', paddingLeft: '26px', paddingRight: '26px'}}>
							{filterDisplayObj.map((item, index) => {
								return(
									<div className="col-md-3 col-sm-6 col-xs-12" key={index}>
										<label className="labelclass">{item.prop}</label><br />
										<span><b>{item.value}</b></span>
									</div>
								);
							})}
						</div>
						<DrillDownReportBuilderForm
							reportid = {this.props.match.params.id}
							match = {this.props.match}
							history = {this.props.history}
							openModal = {this.props.openModal}
							isDrillDown = {true}
							drillDownCloseModal = {closeModal}
							drillDownAppliedFilterObj = {this.props.resource.filter}
							drillDownAppliedParameterFilterObj = {this.props.resource.parameterfilter}
							drillDownFilterObj = {filterGenObj}
						/>
					</div>
				</div>
			);
		}, className: {content: 'react-modal-custom-class-drilldown', overlay: 'react-modal-overlay-custom-class'}});
	}

	addMissedColumns (data) {
		let columnsinfo = this.props.resource.columnsinfo,
			fields = this.props.resource.reportjson.config.fields,
			dateformatterArray = [],
			monthObj = {
				January: 1,
				February: 2,
				March: 3,
				April: 4,
				May: 5,
				June: 6,
				July: 7,
				August: 8,
				September: 9,
				October: 10,
				November: 11,
				December: 12
			},
			monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			quarterArray = ['Q1', 'Q2', 'Q3', 'Q4'];

		for (let prop in fields) {
			if (columnsinfo[fields[prop]] && columnsinfo[fields[prop]].isConfiguredColumn && columnsinfo[fields[prop]].configuredProp.calculationtype == 'dateformatter') {
				dateformatterArray.push({
					column: fields[prop],
					formatter: columnsinfo[fields[prop]].configuredProp.formatter
				});
			}
		}

		dateformatterArray.forEach((item) => {
			let fdataObj = {
				'financialyear': [],
				'calendaryear': [],
				'month-year': [],
				'month': [],
				'financialquarter': [],
				'calendarquarter': [],
				'date': []
			}

			data.forEach((col) => {
				if (col[item.column]) {
					if (item.formatter == 'calendaryear') {
						if (!fdataObj[item.formatter].includes(Number(col[item.column])))
							fdataObj[item.formatter].push(Number(col[item.column]));
					} else if (item.formatter == 'financialyear') {
						let splitValue = Number(col[item.column].split('-')[0]);

						if (!fdataObj[item.formatter].includes(splitValue))
							fdataObj[item.formatter].push(splitValue);
					} else if (item.formatter == 'date') {
						if (!fdataObj[item.formatter].includes(new Date(col[item.column]).toDateString()))
							fdataObj[item.formatter].push(new Date(col[item.column]).toDateString());
					} else {
						if (!fdataObj[item.formatter].includes(col[item.column]))
							fdataObj[item.formatter].push(col[item.column]);
					}
				}
			});

			if (fdataObj[item.formatter].length == 0)
				return null;

			if (['financialquarter', 'calendarquarter'].includes(item.formatter)) {
				if (quarterArray.length != fdataObj[item.formatter].length)
					quarterArray.forEach((qarr) => {
						if (!fdataObj[item.formatter].includes(qarr)) {
							data.push({
								[item.column]: qarr
							});
						}
					});
			}

			if (item.formatter == 'month') {
				Object.keys(monthObj).forEach((marr) => {
					if (!fdataObj[item.formatter].includes(marr)) {
						data.push({
							[item.column]: marr
						});
					}
				});
			}

			if (['calendaryear', 'financialyear'].includes(item.formatter)) {
				fdataObj[item.formatter].sort((a, b) => {
					return (a < b) ? -1 : (a > b) ? 1 : 0;
				});

				let minValue = fdataObj[item.formatter][0];
				let maxValue = fdataObj[item.formatter][fdataObj[item.formatter].length - 1];

				for (let i = minValue+1; i < maxValue; i++) {
					if (!fdataObj[item.formatter].includes(i))
						data.push({
							[item.column]: item.formatter == 'financialyear' ? `${i}-${i+1}` : i
						});
				}
			}

			if (['month-year', 'date'].includes(item.formatter)) {
				fdataObj[item.formatter].sort((a, b) => {
					return (new Date(a) < new Date(b)) ? -1 : (new Date(a) > new Date(b)) ? 1 : 0;
				});

				if (item.formatter == 'month-year') {
					let minMonth = fdataObj[item.formatter][0].split('-')[0],
						maxMonth = fdataObj[item.formatter][fdataObj[item.formatter].length - 1].split('-')[0],
						minYear = Number(fdataObj[item.formatter][0].split('-')[1]),
						maxYear = Number(fdataObj[item.formatter][fdataObj[item.formatter].length - 1].split('-')[1]);

					for (let i = minYear; i <= maxYear; i++) {
						let j = (i == minYear) ? monthArray.indexOf(minMonth)+1 : 0;
						let jlength = (i == maxYear) ? monthArray.indexOf(maxMonth) : monthArray.length;

						for (j; j < jlength; j++) {
							if (!fdataObj[item.formatter].includes(`${monthArray[j]}-${i}`)) {
								data.push({
									[item.column]: `${monthArray[j]}-${i}`
								});
							}
						}
					}
				} else {
					let minDate = new Date(fdataObj[item.formatter][0]),
						maxDate = new Date(fdataObj[item.formatter][fdataObj[item.formatter].length - 1]);

					while (minDate <= maxDate) {
						if (!fdataObj[item.formatter].includes(minDate.toDateString()))
							data.push({
								[item.column]: `${minDate.getDate()}-${monthArray[minDate.getMonth()]}-${minDate.getFullYear()}`
							});

						minDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate() + 1);
					}
				}
			}
		});

		return data;
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);

		if(this.props.isDrillDown)
			this.props.drillDownCloseModal();
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	renderFilterTags() {
		let { isFiltersAvailable, isParameterFilterAvailable } = this.state;

		let filtersFound = false,
			parameterfilterFound = false;

		if (Object.keys(this.props.resource.filter).length > 0)
			for (let prop in this.props.resource.filter) {
				if (this.props.resource.filter[prop]) {
					filtersFound = true;
					break;
				}
			}

		if (Object.keys(this.props.resource.parameterfilter).length > 0)
			for (let prop in this.props.resource.parameterfilter) {
				if (this.props.resource.parameterfilter[prop]) {
					parameterfilterFound = true;
					break;
				}
			}

		if (!filtersFound && !parameterfilterFound)
			return (
				<div onClick={this.editFilter} style={{position: 'relative', width: 'auto'}}>
					<div className="list-filter-card">
						<span className="list-filter-card-content" title={`No filter applied`}>No filter applied</span>
					</div>
				</div>
			);

		return Object.keys(this.props.resource.filter).map((field, key) => {
			if (this.props.resource.filter[field] === null)
				return null;

			let filter = this.props.resource.filter[field],
				displayValue = null,
				displayBetValue = null,
				columnsinfo = this.props.resource.columnsinfo[field];

			if (typeof(filter) == 'object') {
				displayValue = filter.value;
				displayBetValue = filter.betweenvalue ? filter.betweenvalue : null;

				if (columnsinfo && columnsinfo.type == 'date') {
					displayValue = moment(displayValue).format("DD-MMM-YYYY");
					displayBetValue = displayBetValue ? moment(displayBetValue).format("DD-MMM-YYYY") : null;
				}
			} else {
				displayValue = filter;

				if (columnsinfo && columnsinfo.type == 'boolean')
					displayValue = typeof(displayValue) == 'boolean' ? (displayValue == true ? 'Yes' : 'No') : displayValue;
			}

			let description = `${columnsinfo.displayName} ${(filter != null && filter.operator) ? filter.operator : ' Equals '} ${displayValue}`;

			if(filter != null && filter.operator == 'Between')
				description = `${description} and ${displayBetValue}`;

			if (filter != null && columnsinfo && columnsinfo.type == 'integer' && columnsinfo.isForeignKey && typeof(filter) == 'object')
				description = `${columnsinfo.displayName} ${filter.value_dn}`;

			if (filter != null && columnsinfo && columnsinfo.type == 'array' && columnsinfo.isArrayForeignKey && typeof(filter) == 'object')
				description = `${columnsinfo.displayName} ${filter.value_dn}`;

			return (
				<div key={key} onClick={this.editFilter} style={{position: 'relative', width: 'auto'}}>
					<div className="list-filter-card">
						<span className="list-filter-card-content" title={`${description}`}>{description}</span>
					</div>
				</div>
			);
		});
	};

	renderdefinedFilterTags () {
		return Object.keys(this.props.resource.parameterfilter).map((field, key) => {
			if (this.props.resource.parameterfilter[field] === null || field === 'companyid' || field.indexOf('_dname') >= 0)
				return null;

			let displayValue = this.props.resource.parameterfilter[field],
				columnsinfo = {
					fromdate: {
						type: 'date',
						displayName: 'From Date'
					},
					todate: {
						type: 'date',
						displayName: 'To Date'
					},
					asondate: {
						type: 'date',
						displayName: 'As on Date'
					},
					"asondateoptional": {
						type: 'date',
						displayName: 'As on Date'
					},
					"stockbasedon": {
						type: 'dropdown',
						displayName: 'Stock Based On'
					},
					companyid: {
						type: 'autoselect',
						displayName: 'Company'
					},
					itemid: {
						type: 'autoselect',
						displayName: 'Item'
					},
					warehouseid: {
						type: 'automultiselect',
						displayName: 'Warehouse'
					},
					projectid: {
						type: 'autoselect',
						displayName: 'Project'
					}
				};

			if (columnsinfo[field].type == 'date')
				displayValue = moment(displayValue).format("DD-MMM-YYYY");
			else if (columnsinfo[field].type == 'boolean')
				displayValue = typeof(displayValue) == 'boolean' ? (displayValue == true ? 'Yes' : 'No') : displayValue;
			else if (columnsinfo[field].type == 'autoselect') {
				if (field == 'companyid')
					displayValue = this.props.app.selectedCompanyDetails.name;
				if (field == 'itemid')
					displayValue = this.props.resource.parameterfilter.itemid_dname ? this.props.resource.parameterfilter.itemid_dname : displayValue;
			}
			else if (columnsinfo[field].type == 'automultiselect') {
				if (field == 'warehouseid')
					displayValue = this.props.resource.parameterfilter.warehouseid_dname ? this.props.resource.parameterfilter.warehouseid_dname : displayValue;
			}

			let description = `${columnsinfo[field].displayName} Equals ${displayValue}`;

			return (
				<div key={key} onClick={this.editFilter} style={{position: 'relative', width: 'auto'}}>
					<div className="list-filter-card-warning">
						<span className="list-filter-card-warning-content" title={`${description}`}>{description}</span>
					</div>
				</div>
			);
		});
	};

	renderCustomMessage(value, report_styleProps) {
		let height = Object.keys(report_styleProps).length > 0 ? report_styleProps.maxHeight : '100%';

		return (
			<div className = 'row no-gutters' style = {{height}}>
				<div className = 'col-md-12'>
					<div className = 'd-flex flex-column align-items-center justify-content-center h-100'>
							{ value == 'Msg_No_Result' ? <img src = '../images/dashboard_noresult.png' className = 'list-error-image'/> : null }

							<div className = 'padding-10 font-12 form-group'>
								{value == 'Msg_No_Result' ? <div className = 'text-center'>
									No results found for your search!
									<p>
										<span onClick={this.editFilter}> Edit filter </span>
									</p>
								</div> : ''}

								{value == 'Msg_Required_Filter' ? <div className = 'text-center'>
									<b>Filters</b> are <b>mandatory</b> in this report.
									<p>
										Please
										<span onClick={this.editFilter}> select any filter </span> to display report data.
									</p>
								</div> : null}
							</div>
					</div>
				</div>
			</div>
		);
	};

	renderDashboard() {
		let { isFiltersAvailable, isParameterFilterAvailable, isRequiredFilterAvailable } = this.state;

		let reportHeight;
		if(this.props.isMaximize)
			reportHeight = $(window).height() - 50 - $('.dashboard_reportHeader').outerHeight() - 30;
		else
			reportHeight = $(`.widget-${this.props.match.params.id}`).outerHeight() - $('.dashboard_reportHeader').outerHeight() - 30;

		let report_styleProps = {};

		if (this.props.resource.reportjson.config.analyticstype != 'KPI' && !this.props.isMaximize)
			report_styleProps = {
				maxHeight: `${reportHeight}px`,
				height: `${reportHeight}px`,
				overflowY: 'auto',
				overflowX: 'hidden'
			};

		return(
			<div className = 'row no-gutters h-100'>

				<Loadingcontainer isloading = {this.state.loaderflag}></Loadingcontainer>

				<form className = 'col-md-12'>
					<div className = {`row ${!this.props.isMaximize ? 'no-gutters h-100' : ''}`}  style = {{marginTop: '0px'}}>
						<div className = {`col-md-12 bg-white dashboard_reportHeader`}>
							<div className = 'row no-gutters h-100' style = {{padding: `${this.props.isMaximize ? '10px 0px' : ''}`}}>
								<div className = {`form-group col-md-10 overflowtxt gs-dashboard-textColor ${this.props.isMaximize ? 'isMaximize' : ''}`}>
									{this.props.resource.reportjson.name}
								</div>
								<div className = 'form-group col-md-2 text-right'>
									<span className = 'gs-dashboard-filtericon fa fa-filter' style={{color: '#B5B5B5', cursor: 'pointer'}} onClick={this.editFilter}></span>
									{ this.props.isCreatePage || this.props.resource.reportjson.config.analyticstype != 'KPI'? <span className = {`gs-dashboard-filtericon fa ${(this.props.isCreatePage || this.props.isMaximize) ? 'fa-times' : 'fa-window-maximize'} marginleft-10`} style={{color: '#B5B5B5', cursor: 'pointer'}} onClick={()=>this.props.maximizeWidget(this.props.resource.reportid, !this.props.isMaximize)}></span> : null }
								</div>
							</div>
						</div>

						<div style={report_styleProps} className = 'col-md-12 bg-white'>
							{ isRequiredFilterAvailable ? this.renderCustomMessage('Msg_Required_Filter', report_styleProps) : null }

							{ this.props.resource.originalRows && this.props.resource.originalRows.length == 0 ? this.renderCustomMessage('Msg_No_Result', report_styleProps) : null }

							{this.props.resource.reportType != 'Normal'  && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <ReactBIChart
									chartprops = {this.props.resource}
									updateFormState = {this.props.updateFormState}
									form = {this.props.form}
									app = {this.props.app}
									ref = "pivot"
									reportHeight = {reportHeight}
									isDashboard = {this.props.isDashboard}
									isMaximize = {this.props.isMaximize}
									openDrillDown={this.openDrillDown}
							/> : null }
						</div>
					</div>
				</form>
			</div>
		);
	};

	renderDrillDown() {
		let { isFiltersAvailable, isParameterFilterAvailable, isRequiredFilterAvailable } = this.state;

		let reportHeight= ($(window).height() * 0.98) - ($('.react-modal-header').outerHeight() || 0) - ($('.gs-drilldown-display').outerHeight() || 0) - 65;

		let report_styleProps = {};

		if (this.props.resource.reportjson.config.analyticstype != 'KPI' && !this.props.isMaximize)
			report_styleProps = {
				height: `${reportHeight}px`,
				paddingLeft: '0px',
				paddingRight: '26px'
			};

		return(
			<div className = 'row'>
				<Loadingcontainer isloading = {this.state.loaderflag}></Loadingcontainer>
				<form className = 'col-md-12' style={report_styleProps}>
					{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname={this.props.resource.reportdisplayName} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} height={reportHeight} /> : null }
				</form>
			</div>
		);
	};

	render() {
		let { isFiltersAvailable, isParameterFilterAvailable, isRequiredFilterAvailable, isAccess } = this.state;

		if(!this.props.resource)
			return null;

		let reportHeight = $(window).height() - (document.getElementsByClassName('navbar')[0].clientHeight + ($('.report-filtertag-container').outerHeight() || 0) + ($('.report-header').outerHeight() || 0));

		return(
			<div>
				{ this.props.isDashboard ? this.renderDashboard() : (this.props.isDrillDown ? this.renderDrillDown() : <div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="d-flex flex-row justify-content-center float-left" style={{width: '40%'}}>
								<div style={{width: '100%', display: 'block', margin: '10px 0px 10px 10px'}}><Field name={'reportid'} props={{resource: "reports", fields: "id,name", label: "name",filter : `reports.foldername = '${this.props.resource.foldername}' and reports.module = '${this.props.resource.module}' and not(COALESCE(reports.config->>'analyticstype', 'Normal') = 'KPI')`,onChange : (item, itemObj)=>{this.reportOnChange(item, itemObj)}}} component={selectAsyncEle} /></div>
							</div>
							<div className="report-header-btnbar float-right">
								{this.props.resource.allowexportexcel && this.props.resource.reportType == "Normal" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0  ? <button type="button" className="btn gs-form-btn-success btn-sm marginleft-5" onClick={() => this.refs.grid.exportExcel()}>
									<i className="fa fa-file-excel-o"></i>Export
								</button> : null}

								{this.props.resource.allowexportexcel && this.props.resource.reportType != "Normal" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.pivot.print()} className="btn gs-form-btn-success btn-sm marginleft-5">
									<i className = {`${this.props.resource.reportjson.config.analyticstype == 'Table' ? 'fa fa-file-excel-o' : 'fa fa-print'}`}></i>{this.props.resource.reportjson.config.analyticstype == 'Table' ? 'Export' : 'Print'}
								</button> : null}
							</div>
						</div>

						{ isAccess ? <div className="col-md-12 bg-white d-lg-block report-filtertag-container">
							<div className="row" style={{margin: '0 auto',padding: '10px'}}>
								{this.props.resource.definedParameters.length > 0 ? this.renderdefinedFilterTags() : null}
								{this.renderFilterTags()}
								{(isFiltersAvailable || isParameterFilterAvailable) ? <button type="button" className="btn btn-sm gs-form-btn-primary marginleft-10" onClick={this.editFilter}>Edit Filter</button> : null}
							</div>
						</div> : null}
						{ isAccess ? <div className="col-md-12 bg-white">
							<ReportPlaceholderComponent isreportbuilder = {true} reportdata={this.props.resource.originalRows} />

							{this.props.resource.reportType == 'Normal' && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname={this.props.resource.reportdisplayName} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
							{this.props.resource.reportType != 'Normal' && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <ReactBIChart chartprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} app={this.props.app} ref="pivot" reportHeight={reportHeight} openDrillDown={this.openDrillDown} /> : null }
						</div> : <div className = 'col-md-12 bg-white'>
							<ReportPlaceholderComponent isRestrictAccess = {true} reportdata={[]} />
						</div>}
					</div>
				</form>
			</div> )}
			</div>
		);
	};
}

ReportBuilderForm = connect(
	(state, props) => {
		let formName = props.match.params.id > 0 ? (props.isDashboard ? `ReportBuilder_${props.dashboardid}_${props.match.params.id}` : `ReportBuilder_${props.match.params.id}`) : `ReportBuilder_create`;

		if(props.isDrillDown)
			formName = `${formName}_drilldown`;

		return {
			app : state.app,
			reportbuilderjson : state.reportbuilderjson,
			form : formName,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateReportBuilderJSONState, updateFormState, updateReportFilter }
)(reduxForm()(ReportBuilderForm));

export default ReportBuilderForm;


class ReportBuildFilterForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filters: [],
			definedParamFilters: {
				fromdate: {
					field: 'fromdate',
					displayName: 'From Date',
					required: true,
					disabled: false,
					placeholder: 'From Date'
				},
				todate: {
					field: 'todate',
					displayName: 'To Date',
					required: true,
					disabled: false,
					placeholder: 'To Date'
				},
				stockbasedon: {
					field: 'stockbasedon',
					displayName: 'Stock Based On',
					required: true,
					disabled: false,
					options: [{
						id: 'transactiondate',
						name: 'Transaction Date'
					}, {
						id: 'postingdate',
						name: 'Posting Date'
					}],
					placeholder: 'Stock Based On'
				},
				asondate: {
					field: 'asondate',
					displayName: 'As on Date',
					required: true,
					disabled: false,
					placeholder: 'As on Date'
				},
				asondateoptional: {
					field: 'asondateoptional',
					displayName: 'As on Date',
					required: false,
					disabled: false,
					placeholder: 'As on Date'
				},
				companyid: {
					field: 'companyid',
					displayName: 'Company',
					required: true,
					disabled: false,
					placeholder: 'Company',
					resource: `companymaster`,
					fields: `id,name,defaultcompany`,
					label: `name`
				},
				itemid: {
					field: 'itemid',
					displayName: 'Item',
					required: true,
					disabled: false,
					placeholder: 'Item',
					resource: `itemmaster`,
					fields: `id,name,displayname`,
					displaylabel: `name`,
					label: `displayname`
				},
				warehouseid: {
					field: 'warehouseid',
					displayName: 'Warehouse',
					required: true,
					disabled: false,
					placeholder: 'Warehouse',
					resource: `stocklocations`,
					fields: `id,name`,
					label: `name`,
					filter: `stocklocations.parentid IS NULL`
				},
				projectid: {
					field: 'projectid',
					displayName: 'Project',
					required: true,
					disabled: false,
					placeholder: 'Project',
					resource: `projects`,
					fields: `id,displayname`,
					label: `displayname`,
					filter: `projects.status in ('Approved', 'Completed')`
				}
			},
			filter: this.props.resource.filter,
			parameterfilter: this.props.resource.parameterfilter
		};

		this.renderFilters = this.renderFilters.bind(this);
		this.renderdefinedFilters = this.renderdefinedFilters.bind(this);
		this.closeFilter = this.closeFilter.bind(this);
		this.updateFilter = this.updateFilter.bind(this);
	}

	componentWillReceiveProps(props) {
		if (!props.resource)
			props.closeModal();
	}

	updateFilter(){
		this.props.getReportData();
		this.props.closeModal();
	}

	closeFilter() {
		let { filter, parameterfilter } = this.state;

		this.props.updateFormState(this.props.form, {
			filter,
			parameterfilter
		});

		this.props.closeModal();
	}

	parameterFilterOnChange(param, value, valueObj) {
		let tempObj = {};
		if(param == 'itemid')
			tempObj[`parameterfilter.itemid_dname`] = value ? valueObj.name : null;
		if(param == 'warehouseid')
			tempObj[`parameterfilter.warehouseid_dname`] = value ? valueObj.map(item => item.name).join(', ') : null;

		this.props.updateFormState(this.props.form, tempObj);
	}

	renderFilters(){
		return this.props.resource.reportfilter.map((filter, key) => {

			var columnsinfo = this.props.resource.columnsinfo[filter.field];

			let renderType = '',
			tempvalidate = [requiredNewValidation({
				required: filter.required,
				title : columnsinfo.displayName
			})],
			fieldProps = {
				required: filter.required,
				disabled: filter.locked,
				placeholder: columnsinfo.displayName
			};

			if (columnsinfo.type == 'string' && columnsinfo.group != 'localselect' && columnsinfo.group != 'resource') {
				renderType = ReportInputEle;
			}

			if (columnsinfo.type == 'string' && columnsinfo.group == 'resource') {
				renderType = ReportLocalSelectEle;
				fieldProps.options = this.props.resourceArray;
			}

			if (columnsinfo.type == 'string' && columnsinfo.group == 'localselect') {
				renderType = ReportLocalSelectEle;
				fieldProps.options = columnsinfo.localOptions;
			}

			if (columnsinfo.type == 'boolean') {
				renderType = ReportBooleanEle;
			}

			if (columnsinfo.type == 'date') {
				renderType = ReportDateEle;
			}

			if (columnsinfo.type == 'integer' && !columnsinfo.isForeignKey) {
				renderType = ReportNumberEle;
			}

			if (columnsinfo.isForeignKey) {
				renderType = ReportAutoSelectEle;
				fieldProps.resource = columnsinfo.foreignKeyOptions.resource;
				fieldProps.fields = `id,${columnsinfo.foreignKeyOptions.mainField.split('.')[1]}`;
				fieldProps.label = `${columnsinfo.foreignKeyOptions.mainField.split('.')[1]}`;

				if (columnsinfo.foreignKeyOptions.resource == 'numberingseriesmaster' && columnsinfo.numberingseries_resource)
					fieldProps.filter = `numberingseriesmaster.resource = '${columnsinfo.numberingseries_resource}'`;
			}

			if (columnsinfo.isArrayForeignKey) {
				renderType = ReportAutoMultiSelectEle;
				fieldProps.resource = columnsinfo.foreignKeyArrayOptions.resource;
				fieldProps.fields = `id,${columnsinfo.foreignKeyArrayOptions.mainField}`;
				fieldProps.label = `${columnsinfo.foreignKeyArrayOptions.mainField}`;
			}

			return (
				<div className="form-group col-sm-12 col-md-12" key={key} >
					<div className="row">
						<div className="col-md-4">
							<label className="labelclass">{columnsinfo.displayName}</label>
						</div>
						<div className="col-md-8">
							<Field
								name = {`filter.${filter.field}`}
								{...fieldProps}
								component = {renderType}
								validate = {tempvalidate}
							/>
						</div>
					</div>
				</div>
			);
		});
	};

	renderdefinedFilters() {
		let { definedParamFilters } = this.state;

		return this.props.resource.definedParameters.map((filter, key) => {
			if (filter == 'companyid')
				return null;

			return (
				<div className="form-group col-sm-12 col-md-12" key={key} >
					<div className="row">
						<div className="col-md-4">
							<label className="labelclass">{definedParamFilters[filter].displayName}</label>
						</div>
						<div className="col-md-8">
							{ filter == 'fromdate' ? <Field
								name = {`parameterfilter.fromdate`}
								props = {{
									required: definedParamFilters[filter].required
								}}
								component = {DateEle}
								validate = {[dateNewValidation({
									required: definedParamFilters[filter].required,
									title: definedParamFilters[filter].displayName
								})]}
							/> : null }
							{ filter == 'todate' ? <Field
								name = {`parameterfilter.todate`}
								props = {{
									required: definedParamFilters[filter].required,
									min: this.props.resource.parameterfilter.fromdate
								}}
								component = {DateEle}
								validate = {[dateNewValidation({
									required: definedParamFilters[filter].required,
									title : definedParamFilters[filter].displayName,
									min: '{resource.parameterfilter.fromdate}'
								})]}
							/> : null }
							{ filter == 'asondate' ? <Field
								name = {`parameterfilter.asondate`}
								props = {{
									required: definedParamFilters[filter].required
								}}
								component = {DateEle}
								validate = {[dateNewValidation({
									required: definedParamFilters[filter].required,
									title : definedParamFilters[filter].displayName
								})]}
							/> : null }
							{ filter == 'asondateoptional' ? <Field
								name = {`parameterfilter.asondateoptional`}
								props = {{
									required: definedParamFilters[filter].required
								}}
								component = {DateEle}
								validate = {[dateNewValidation({
									required: definedParamFilters[filter].required,
									title : definedParamFilters[filter].displayName
								})]}
							/> : null }
							{filter == 'stockbasedon' ? <Field
								name = {`parameterfilter.stockbasedon`}
								props = {{
									options: [...definedParamFilters[filter].options],
								}}
								component = {localSelectEle}
								validate = {[stringNewValidation({
									required: definedParamFilters[filter].required,
									model: 'stockbasedon'
								})]}
							/> : null }
							{filter == 'itemid' ? <Field
								name = {`parameterfilter.itemid`}
								props = {{
									resource: definedParamFilters[filter].resource,
									fields: definedParamFilters[filter].fields,
									label: definedParamFilters[filter].label,
									required : definedParamFilters[filter].required,
									onChange: (value, valueObj) => this.parameterFilterOnChange('itemid', value, valueObj)
								}}
								component = {autoSelectEle}
								validate = {[numberNewValidation({
									required: definedParamFilters[filter].required,
									model: 'itemid'
								})]}
							/> : null }
							{filter == 'warehouseid' ? <Field
								name = {`parameterfilter.warehouseid`}
								props = {{
									resource: definedParamFilters[filter].resource,
									fields: definedParamFilters[filter].fields,
									label: definedParamFilters[filter].label,
									filter: definedParamFilters[filter].filter,
									required : definedParamFilters[filter].required,
									onChange: (value, valueObj) => this.parameterFilterOnChange('warehouseid', value, valueObj)
								}}
								component = {autoMultiSelectEle}
								validate = {[multiSelectNewValidation({
									required: definedParamFilters[filter].required,
									model: 'warehouseid'
								})]}
							/> : null }
							{false && filter == 'companyid' ? <Field
								name = {`parameterfilter.companyid`}
								props = {{
									resource: "companymaster",
									fields: "id,name,defaultcompany",
									label: 'name',
									required : definedParamFilters[filter].required,
								}}
								component = {autoSelectEle}
								validate = {[numberNewValidation({
									required: definedParamFilters[filter].required,
									model: 'companyid'
								})]}
							/> : null }
							{filter == 'projectid' ? <Field
								name = {`parameterfilter.projectid`}
								props = {{
									resource: definedParamFilters[filter].resource,
									fields: definedParamFilters[filter].fields,
									label: definedParamFilters[filter].label,
									required : definedParamFilters[filter].required,
									onChange: (value, valueObj) => this.parameterFilterOnChange('projectid', value, valueObj)
								}}
								component = {autoSelectEle}
								validate = {[numberNewValidation({
									required: definedParamFilters[filter].required,
									model: 'projectid'
								})]}
							/> : null }
						</div>
					</div>
				</div>
			);
		});
	};

	render() {
		if (!this.props.resource)
			return null;

		return (
			<div>
				<form>
					<div className="react-outer-modal">
						<div className="react-modal-header">
							<h5 className="modal-title gs-text-color">Edit Filter {this.props.isDashboard ? `(${this.props.resource.reportdisplayName})` : ''}</h5>
						</div>
						<div className="react-modal-body react-modal-body-scroll-min-height-250">
							<div className="row">
								<div className="col-md-12 col-sm-12 col-xs-12">
									{ this.props.resource.definedParameters.length > 0 ? this.renderdefinedFilters() : null}
									{(this.props.resource.reportfilter.length > 0) ? <div>
										<h6 className = 'text-center'>
											Configuration Filters
										</h6>
										{this.renderFilters()}
									</div> : null}
									{(this.props.resource.definedParameters.length == 0 && this.props.resource.reportfilter.length == 0) ? <div className="alert alert-warning text-center">
										Filter Not Available
									</div> : null}
								</div>
							</div>
						</div>
						<div className="react-modal-footer">
							<div className="row">
								<div className="col-md-12 col-sm-12 col-xs-12">
									<div className="muted credit text-center">
										<button
											type = "button"
											className = "btn btn-sm btn-width btn-secondary"
											onClick = {this.closeFilter}>
											<i className = "fa fa-times"></i>Close
										</button>
										<button
											type = "button"
											className = "btn btn-sm gs-btn-success btn-width"
											disabled = {this.props.invalid}
											onClick = {this.updateFilter}>
											<i className = "fa fa-check"></i>Ok
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		);
	}
}