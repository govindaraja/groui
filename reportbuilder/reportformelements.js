import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import moment from 'moment';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { commonMethods, modalService } from '../utils/services';
import { ReportNumberElement, ReportBooleanElement, ReportDateElement, ReportLocalSelectElement, ReportAutoSelectElement, ReportAutoMultiSelectElement } from './filtercomponents';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation, checkActionVerbAccess } from '../utils/utils';
import { dateFilter, datetimeFilter, timeFilter} from '../utils/filter';

export const ReportNumberEle = (field) => {
	let { onChange } = field;

	let onChangeFun = (value) => {
		field.input.onChange(value);

		if (typeof(onChange) == 'function')
			setTimeout(() => {
				onChange(value);
			}, 0);
	};

	return(
		<ReportNumberElement
			className = {`form-control ${field.classname ? field.classname : ''} ${field.meta.error ? 'errorinput' : ''}`}
			placeholder = {field.placeholder}
			value = {field.input.value}
			onChange = {onChangeFun}
			required = {field.required}
			disabled = {field.disabled}
		/>
	);
};

export const ReportBooleanEle = (field) => {
	let { onChange } = field;

	let onChangeFun = (typeof(onChange) == 'function') ? (value) => {
		field.input.onChange(value);

		setTimeout(() => {
			onChange(value);
		}, 0);
	} : field.input.onChange;

	return (
		<ReportBooleanElement
			className = {`form-control ${field.classname ? field.classname : ''} ${field.meta.error ? 'errorinput' : ''}`}
			placeholder = {field.placeholder}
			value = {field.input.value}
			required = {field.required}
			disabled = {field.disabled}
			onChange = {onChangeFun}
		/>
	);
};

export const ReportDateEle = (field) => {
	let { onChange, onFocus, onBlur, min, max } = field;

	let onChangeFun = (typeof(onChange) == 'function') ? (value) => {
		field.input.onChange(value);

		setTimeout(() => {
			onChange(value);
		}, 0);
	} : field.input.onChange;

	let onFocusFun = (typeof(onFocus) == 'function') ? (value) => {
		field.input.onFocus(value);

		setTimeout(() => {
			onFocus(value);
		}, 0);
	} : (value) => field.input.onFocus(value);

	let onBlurFun = (typeof(onBlur) == 'function') ? (value) => {
		field.input.onBlur(value);

		setTimeout(() => {
			onBlur(value);
		}, 0);
	} : (value) => field.input.onBlur(value);

	let minDate = field.min ? moment(field.min) : undefined;
	let maxDate = field.max ? moment(field.max) : undefined;

	return (
		<ReportDateElement
			className = {`form-control ${field.classname ? field.classname : ''} ${field.meta.error ? 'errorinput' : ''}`}
			value = {field.input.value}
			onChange = {onChangeFun}
			onFocus = {onFocusFun}
			onBlur = {onBlurFun}
			required = {field.required}
			disabled = {field.disabled}
			min = {minDate}
			max = {maxDate}
			placeholder = {field.placeholder}
		/>
	);
};

export const ReportLocalSelectEle = (field) => {
	let { options, label, valuename, onChange, onFocus, onBlur } = field;

	let onChangeFun = (typeof(onChange) == 'function') ? (value, valueObj) => {
		field.input.onChange(value);

		setTimeout(() => {
			onChange(value, valueObj);
		}, 0);
	} : field.input.onChange;

	let onFocusfunction = (typeof(onFocus) == 'function') ? (value) => {
		field.input.onFocus(value);

		onFocus(value);
	} : field.input.onFocus;

	let onBlurfunction = (typeof(onBlur) == 'function') ? (value) => {
		field.input.onBlur(value);

		onBlur(value);
	} : field.input.onBlur;

	return (
		<ReportLocalSelectElement
			options = {options}
			label = {label}
			valuename = {valuename}
			{...field.input}
			onChange = {onChangeFun}
			onFocus = {onFocusfunction}
			onBlur = {onBlurfunction}
			placeholder = {field.placeholder}
			className = {`form-control ${field.classname ? field.classname : ''} ${field.meta.error ? 'errorinput' : ''}`}
			required = {field.required}
			disabled = {field.disabled}
		/>
	);
};

export const ReportAutoSelectEle = (field) => {
	let { resource, fields, displaylabel, label, valuename, filter, onChange, onFocus, onBlur } = field;

	let onChangeFun = (typeof(onChange) == 'function') ? (value, valueObj) => {
		field.input.onChange(value);

		setTimeout(() => {
			onChange(value, valueObj);
		}, 0);
	} : field.input.onChange;

	let onFocusfunction = (typeof(onFocus) == 'function') ? (value) => {
		field.input.onFocus(value);

		onFocus(value);
	} : field.input.onFocus;

	let onBlurfunction = (typeof(onBlur) == 'function') ? (value) => {
		field.input.onBlur(value);

		onBlur(value);
	} : field.input.onBlur;

	return (
		<ReportAutoSelectElement
			resource = {resource}
			fields = {fields}
			displaylabel = {displaylabel}
			label = {label}
			valuename = {valuename}
			filter = {filter}
			{...field.input}
			onChange = {onChangeFun}
			onFocus = {onFocusfunction}
			onBlur = {onBlurfunction}
			placeholder = {field.placeholder}
			className = {`form-control ${field.classname ? field.classname : ''} ${field.meta.error ? 'errorinput' : ''}`}
			required = {field.required}
			disabled = {field.disabled}
		/>
	);
};

export const ReportAutoMultiSelectEle = (field) => {
	let { resource, fields, displaylabel, label, valuename, filter, onChange, onFocus, onBlur } = field;

	let onChangeFun = (typeof(onChange) == 'function') ? (value, valueObj) => {
		field.input.onChange(value);

		setTimeout(() => {
			onChange(value, valueObj);
		}, 0);
	} : field.input.onChange;

	let onFocusfunction = (typeof(onFocus) == 'function') ? (value) => {
		field.input.onFocus(value);

		onFocus(value);
	} : field.input.onFocus;

	let onBlurfunction = (typeof(onBlur) == 'function') ? (value) => {
		field.input.onBlur(value);

		onBlur(value);
	} : field.input.onBlur;

	return (
		<ReportAutoSelectElement
			resource = {resource}
			fields = {fields}
			displaylabel = {displaylabel}
			label = {label}
			valuename = {valuename}
			filter = {filter}
			{...field.input}
			onChange = {onChangeFun}
			onFocus = {onFocusfunction}
			onBlur = {onBlurfunction}
			placeholder = {field.placeholder}
			className = {`form-control ${field.classname ? field.classname : ''} ${field.meta.error ? 'errorinput' : ''}`}
			required = {field.required}
			disabled = {field.disabled}
		/>
	);
};

