import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import moment from 'moment';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { updateFormState, updateReportFilter } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import { DateEle, localSelectEle, autoSelectEle, selectAsyncEle, checkboxEle, textareaEle, InputEle, NumberEle, SpanEle, ButtongroupEle, Buttongroup } from '../components/formelements';
import { LocalSelect, SelectAsync, AutoSelect, AutoMultiSelect, DisplayGroup, AutoSuggest, DateElement, NumberElement, DateTimeElement, TimeElement } from '../components/utilcomponents';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation, checkActionVerbAccess } from '../utils/utils';
import { dateFilter, datetimeFilter, timeFilter} from '../utils/filter';

import Popover from 'react-popover';

export const CheckMouseEventOutSide = (mouseevent) => {
	if(!$(mouseevent.path[0]).hasClass('gs-react-select-perfix__option') &&
		!$(mouseevent.path[1]).hasClass('gs-react-select-perfix__option') &&
		!$(mouseevent.path[2]).hasClass('gs-react-select-perfix__option') &&
		!$(mouseevent.path[0]).hasClass('gs-react-select-perfix__menu-notice') &&
		!$(mouseevent.path[1]).hasClass('gs-react-select-perfix__menu-notice') &&
		!$(mouseevent.path[2]).hasClass('gs-react-select-perfix__menu-notice') &&
		!$(mouseevent.path[0]).hasClass('gs-react-select-perfix__menu-list') &&
		!$(mouseevent.path[1]).hasClass('gs-react-select-perfix__menu-list') &&
		!$(mouseevent.path[2]).hasClass('gs-react-select-perfix__menu-list') &&
		!$(mouseevent.path[0]).hasClass('gs-react-select-perfix__menu') &&
		!$(mouseevent.path[1]).hasClass('gs-react-select-perfix__menu') &&
		!$(mouseevent.path[2]).hasClass('gs-react-select-perfix__menu') &&
		!$(mouseevent.path[0]).hasClass('react-datepicker') &&
		!$(mouseevent.path[1]).hasClass('react-datepicker') &&
		!$(mouseevent.path[2]).hasClass('react-datepicker') &&
		!$(mouseevent.path[3]).hasClass('react-datepicker') &&
		!$(mouseevent.path[4]).hasClass('react-datepicker') &&
		!$(mouseevent.path[5]).hasClass('react-datepicker') &&
		!$(mouseevent.path[6]).hasClass('react-datepicker') &&
		!$(mouseevent.path[7]).hasClass('react-datepicker'))
		return true;

	return false;
}

export class ReportNumberElement extends Component {
	constructor(props) {
		super(props);

		this.state = {
			operator: 'Equals',
			defaultvalue: 'Custom',
			value: null,
			betweenvalue: null,
			operatorOptions: ['Equals',
							'Not Equals',
							'Between',
							'More Than',
							'More Than or Equal',
							'Less Than',
							'Less Than or Equal'],
			defaultOptions: ['Custom',
							'Is Empty',
							'Is Not Empty'],
			toggle: false
		};

		if (this.props.value)
			this.onLoad(this.props);

		this.radioBtnOnchange = this.radioBtnOnchange.bind(this);
		this.renderNumberElement = this.renderNumberElement.bind(this);
		this.callbackFn = this.callbackFn.bind(this);
		this.valueOnChange = this.valueOnChange.bind(this);
		this.renderCustomOperator = this.renderCustomOperator.bind(this);
		this.renderPopoverBodyValue = this.renderPopoverBodyValue.bind(this);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.value) {
			if (!this.state.actualValue)
				this.onLoad(nextProps);
			else if ((typeof(nextProps.value) == 'object' ? JSON.stringify(nextProps.value) : nextProps.value) != (typeof(this.state.actualValue) == 'object' ? JSON.stringify(this.state.actualValue) : this.state.actualValue))
				this.onLoad(nextProps);
		} else {
			if (this.state.actualValue)
				this.onLoad(nextProps);
		}
	};

	onLoad(props) {
		let tempObj = {};

		if (!props.value)
			tempObj = {
				defaultvalue: 'Custom',
				operator: 'Equals',
				value: null,
				betweenvalue: null
			};
		else if (typeof(props.value) == 'object')
			tempObj = {
				operator: props.value.operator,
				value: props.value.value,
				betweenvalue: props.value.betweenvalue ? props.value.betweenvalue : null,
				defaultvalue: 'Custom'
			};
		else
			tempObj = {
				defaultvalue: props.value
			};

		tempObj.actualValue = props.value;

		for (let prop in tempObj)
			this.state[prop] = tempObj[prop];

		setTimeout(() => {
			this.setState(tempObj);
		}, 0);
	};

	radioBtnOnchange(item) {
		this.state.defaultvalue = item;
		this.state.value = null;
		this.state.betweenvalue = null;
		this.state.operator = 'Equals';

		this.callbackFn();

		this.setState({
			defaultvalue: item,
			value: null,
			betweenvalue: null,
			operator: 'Equals'
		});
	};

	valueOnChange(reference, val) {
		let tempObj = {
			defaultvalue: 'Custom'
		};

		tempObj[reference] = val;

		this.state[reference] = val;

		this.callbackFn();

		this.setState(tempObj);
	};

	callbackFn() {
		let { onChange } = this.props,
			{ operator, defaultvalue, value, betweenvalue } = this.state,
			valueObj = null;

		if (defaultvalue != 'Custom')
			valueObj = defaultvalue;
		else {
			if (value !== null && value !== undefined && value !== '') {
				if (operator == 'Between') {
					if (betweenvalue !== null && betweenvalue !== undefined && betweenvalue !== '')
						valueObj = {
							operator,
							value,
							betweenvalue
						};
				} else
					valueObj = {
						operator,
						value
					};
			}
		}

		this.state.actualValue = valueObj;

		onChange(valueObj);

		this.setState({actualValue: valueObj});
	};

	renderNumberElement(reference) {
		let { defaultvalue, betweenvalue, value, operator } = this.state,
			input_Value = (reference == 'betweenvalue' ? betweenvalue : value),
			isError = (defaultvalue == 'Custom' && (input_Value === '' || input_Value === null || input_Value === undefined)) ? true : false;

		return (
			<NumberElement
				className = {`form-control ${isError ? 'errorinput' : ''}`}
				//placeholder = {`Enter ${this.props.placeholder}`}
				value = {input_Value}
				onChange = {(val)=>{this.valueOnChange(reference, val)}}
				required = {defaultvalue == 'Custom'}
				disabled = {this.props.disabled}
			/>
		);
	};

	renderCustomOperator() {
		let { operator, operatorOptions, defaultvalue } = this.state,
			isOperatorError = (defaultvalue == 'Custom' && (operator == '' || operator == null || operator == undefined)) ? true : false;

		return (
			<div className = 'col-md-11'>
				<div className = 'row'>
					<div className = 'col-md-4 paddingleft-0 paddingright-0'>
						<LocalSelect
							options = {operatorOptions}
							value = {operator}
							onChange = {(val) => {
								this.state.defaultvalue = 'Custom';
								this.state.value = null;
								this.state.betweenvalue = null;
								this.state.operator = 'val';

								this.callbackFn();

								this.setState({
									operator: val,
									value: null,
									betweenvalue: null,
									defaultvalue: 'Custom'
								})}
							}
							placeholder = {'Select Options'}
							className = {`${isOperatorError ? 'errorinput' : ''}`}
							required = {defaultvalue == 'Custom'}
							disabled = {this.props.disabled}
						/>
					</div>
					{operator == 'Between' ? <div className = 'col-md-8'>
						<div className = 'row' style = {{marginRight: '0px'}}>
							<div className = 'col-md-6' style = {{paddingRight: '5px'}}>
								{this.renderNumberElement('value')}
							</div>
							<div className = 'col-md-6' style = {{paddingRight: '5px'}}>
								{this.renderNumberElement('betweenvalue')}
							</div>
						</div>
					</div> : <div className = 'col-md-8'>
						<div className = 'row' style = {{marginRight: '0px'}}>
							<div className = 'col-md-6 paddingright-0'>
								{this.renderNumberElement('value')}
							</div>
						</div>
					</div>}
				</div>
			</div>
		);
	};

	renderPopoverBodyValue (displayNameLabel) {
		let { defaultvalue, defaultOptions, toggle } = this.state;

		return (
			<div className = 'row' style={{marginRight: '0px', marginLeft: '0px', padding: '8px'}}>
				{defaultOptions.length > 0 ? defaultOptions.map((filter, index) => {
					return (
						<div key = {index} className = 'form-group col-md-12'>
							<div className = 'row'>
								<div className = 'col-md-1 gs-reportfilter-align'>
									<input
										type = 'radio'
										disabled = {this.props.disabled}
										checked = {defaultvalue == filter}
										onChange = {() => {this.radioBtnOnchange(filter)}}
										/>
								</div>
								{filter == 'Custom' ? this.renderCustomOperator() : <div className = 'col-md-11 paddingclass'>
									<span>{filter}</span>
								</div>}
							</div>
						</div>
					);
				}) : null}
				<button
					type = 'button'
					disabled = {!displayNameLabel}
					onClick = {()=>{
						this.setState({
							toggle: false
						});
					}}
					className = 'btn btn-sm btn-width marginleft-15 gs-btn-success'>
					<i className = 'fa fa-check'></i>Ok
				</button>
			</div>
		);
	};

	render() {
		let { operator, defaultvalue, value, betweenvalue, defaultOptions, toggle } = this.state,
			displayNameLabel = '';

		if (defaultvalue != 'Custom')
			displayNameLabel = `${defaultvalue ? defaultvalue : ''}`;
		else {
			if (operator && typeof(value) == 'number') {
				if (operator == 'Between') {
					if (typeof(betweenvalue) == 'number')
						displayNameLabel = `${operator} ${value} ${'and '+betweenvalue}`;
				} else
					displayNameLabel = `${operator} ${value}`;
			}
		}

		return (
			<Popover
				className = {'gs-reportfilter-popover'}
				preferPlace = {'below'}
				place={'below'}
				enterExitTransitionDurationMs = {0}
				offset={4}
				isOpen = {toggle}
				tipSize = {0.1}
				onOuterAction = {(mouseevent) => {
					if(CheckMouseEventOutSide(mouseevent)) {
						return this.setState({
							toggle: false
						});
					}
				}}
				body = {this.renderPopoverBodyValue(displayNameLabel)}
			>
				<div
					className = 'input-group'
					title = {`${displayNameLabel ? displayNameLabel : 'Select '+this.props.placeholder}`}
					onClick = {() => {
						this.setState({
							toggle: !toggle
						})
					}}
				>
					<input
						type = 'text'
						className = {`${this.props.className} text-ellipsis rptbuildText`}
						//placeholder = {`Select ${this.props.placeholder}`}
						required = {this.props.required}
						disabled = {true}
						autoComplete = 'off'
						value = {displayNameLabel}
					/>
					<span className = 'input-group-append'>
						{!this.props.disabled && this.props.value ? <div
							onClick = {(e) => {
								this.props.onChange(null);
								e.stopPropagation();
							}}
							rel = 'tooltip'
							title = 'Clear'
							className = 'btn btn-sm gs-btn-light disabled ndtclass'>
							<span>x</span>
						</div> : null}
						<button
							type = 'button'
							disabled = {this.props.disabled}
							className = 'btn btn-sm gs-btn-light ndtclass'>
							<span className = {`${toggle ? 'fa fa-caret-up' : 'fa fa-caret-down'}`}></span>
						</button>
					</span>
				</div>
			</Popover>
		);
	};
};

export class ReportBooleanElement extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: null,
			defaultOptions: [
				{title: 'Yes', value: true},
				{title: 'No', value: false},
				{title: 'All', value: null}
			],
			toggle: false
		};

		if (typeof(this.props.value) == 'boolean')
			this.onLoad(this.props);

		this.radioBtnOnchange = this.radioBtnOnchange.bind(this);
	};

	componentWillReceiveProps(nextProps) {
		if (typeof(nextProps.value) == 'boolean') {
			if (!this.state.value)
				this.onLoad(nextProps);
			else if (nextProps.value != this.state.value)
				this.onLoad(nextProps);
		} else {
			if (typeof(this.state.value) == 'boolean')
				this.onLoad(nextProps);
		}
	};

	onLoad(props) {
		this.state.value = typeof(props.value) == 'boolean' ? props.value : null;

		setTimeout(() => {
			this.setState({
				value: typeof(props.value) == 'boolean' ? props.value: null
			});
		}, 0);
	};

	radioBtnOnchange(item) {
		let { onChange } = this.props;

		this.state.value = item;

		onChange(item);

		this.setState({
			value: item
		});
	};

	render() {
		let { value, defaultOptions, toggle } = this.state;

		return (
			<div className = 'input-group'>
				<Buttongroup
					buttons = {defaultOptions}
					buttonclass = {`btn-sm`}
					className = {`${(typeof(value) != 'boolean' && this.props.required) ? 'errorinput' : ''}`}
					value = {value}
					disabled = {this.props.disabled}
					required = {this.props.required}
					onChange = {(val) => {this.radioBtnOnchange(val)}}
				/>
			</div>
		);
	};
};

export class ReportDateElement extends Component {
	constructor(props) {
		super(props);

		this.state = {
			operator: 'Equals',
			defaultvalue: 'Custom',
			value: null,
			betweenvalue: null,
			lastvalue: {
				operator: 'Days',
				value: null
			},
			nextvalue: {
				operator: 'Days',
				value: null
			},
			defaultdatevalue: 'Today',
			operatorOptions: ['Equals',
							'Not Equals',
							'Between',
							'More Than',
							'More Than or Equal',
							'Less Than',
							'Less Than or Equal'],
			defaultOptions: ['Custom',
							'Custom_last',
							'Custom_next',
							'Custom_defalut',
							'Is Empty',
							'Is Not Empty'],
			customOptions: ['Today',
							'Yesterday',
							'Current Week',
							'Last Week',
							'Current Month',
							'Last Month',
							'Current Quarter',
							'Last Quarter',
							'Current Year',
							'Last Year',
							'Current FY',
							'Last FY'],
			dateFilterOptions: [{title: 'Days', value: 'Days'},
							{title: 'Weeks', value: 'Weeks'},
							{title: 'Months', value: 'Months'},
							{title: 'Years', value: 'Years'}],
			toggle: false
		};

		if (this.props.value)
			this.onLoad(this.props);

		this.radioBtnOnchange = this.radioBtnOnchange.bind(this);
		this.renderDateElement = this.renderDateElement.bind(this);
		this.callbackFn = this.callbackFn.bind(this);
		this.valueOnChange = this.valueOnChange.bind(this);
		this.renderCustomOperator = this.renderCustomOperator.bind(this);
		this.renderLastAndNextOperators = this.renderLastAndNextOperators.bind(this);
		this.renderDefalutOperator = this.renderDefalutOperator.bind(this);
		this.filterValueOnchange = this.filterValueOnchange.bind(this);
		this.renderPopoverBodyValue = this.renderPopoverBodyValue.bind(this);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.value) {
			if (!this.state.actualValue)
				this.onLoad(nextProps);
			else if ((typeof(nextProps.value) == 'object' ? JSON.stringify(nextProps.value) : nextProps.value) != (typeof(this.state.actualValue) == 'object' ? JSON.stringify(this.state.actualValue) : this.state.actualValue))
				this.onLoad(nextProps);
		} else {
			if (this.state.actualValue)
				this.onLoad(nextProps);
		}
	};

	onLoad(props) {
		let tempObj = {};

		if (!props.value)
			tempObj = {
				operator: 'Equals',
				value: null,
				betweenvalue: null,
				defaultvalue: 'Custom',
				defaultdatevalue: 'Today',
				lastvalue: {
					operator: 'Days',
					value: null
				},
				nextvalue: {
					operator: 'Days',
					value: null
				}
			};
		else if (typeof(props.value) == 'object')
			tempObj = {
				operator: props.value.operator,
				value: props.value.value,
				betweenvalue: props.value.betweenvalue ? props.value.betweenvalue : null,
				defaultvalue: 'Custom'
			};
		else {
			if (/^\Last +[0-9]+\ (Days|Months|Years|Weeks)$/.test(props.value) && props.value.split(' ')[0] == 'Last')
				tempObj = {
					defaultvalue: 'Custom_last',
					lastvalue: {
						value: Number(props.value.split(' ')[1]),
						operator: props.value.split(' ')[2]
					}
				};
			else if (/^\Next +[0-9]+\ (Days|Months|Years|Weeks)$/.test(props.value) && props.value.split(' ')[0] == 'Next')
				tempObj = {
					defaultvalue: 'Custom_next',
					nextvalue: {
						value: Number(props.value.split(' ')[1]),
						operator: props.value.split(' ')[2]
					}
				};
			else if (this.state.defaultOptions.indexOf(props.value) == -1)
				tempObj = {
					defaultvalue: 'Custom_defalut',
					defaultdatevalue: props.value
				};
			else
				tempObj = {
					defaultvalue: props.value
				};
		}

		tempObj.actualValue = props.value;

		for (let prop in tempObj)
			this.state[prop] = tempObj[prop];

		setTimeout(() => {
			this.setState(tempObj);
		}, 0);
	};

	radioBtnOnchange(item) {
		this.state.defaultvalue = item;
		this.state.value = null;
		this.state.betweenvalue = null;
		this.state.defaultdatevalue = 'Today';
		this.state.lastvalue = {
			operator: 'Days',
			value: null
		};
		this.state.nextvalue = {
			operator: 'Days',
			value: null
		};

		this.state.operator = 'Equals';

		this.callbackFn();

		this.setState({
			defaultvalue: item,
			value: null,
			betweenvalue: null,
			defaultdatevalue: 'Today',
			lastvalue: {
				operator: 'Days',
				value: null
			},
			nextvalue: {
				operator: 'Days',
				value: null
			},
			operator: 'Equals'
		});
	};

	valueOnChange(reference, val) {
		let tempObj = {
			defaultvalue: 'Custom',
			defaultdatevalue: 'Today',
			lastvalue: {
				operator: 'Days',
				value: null
			},
			nextvalue: {
				operator: 'Days',
				value: null
			}
		};

		tempObj[reference] = val ? new Date(val).toISOString() : null;

		for (let prop in tempObj)
			this.state[prop] = tempObj[prop];

		this.callbackFn();

		this.setState(tempObj);
	};

	filterValueOnchange(reference, val, field) {
		let lastandnextObj = {
				operator: 'Days',
				value: null
			};

		let renderValue = (reference == 'Custom_last' ? this.state.lastvalue : (reference == 'Custom_next') ? this.state.nextvalue : lastandnextObj);

		this.state.defaultvalue = reference;
		this.state.value = null;
		this.state.betweenvalue = null;
		this.state.operator = 'Equals';

		if (reference == 'Custom_last')
			this.state.lastvalue[field] = val;
		else
			this.state.lastvalue = lastandnextObj;

		if (reference == 'Custom_next')
			this.state.nextvalue[field] = val;
		else
			this.state.nextvalue = lastandnextObj;

		if (reference == 'Custom_defalut')
			this.state.defaultdatevalue = val;
		else
			this.state.defaultdatevalue = 'Today';

		//if (reference != 'Custom_defalut' && renderValue.value && renderValue.operator)
			this.callbackFn();

		let tempObj = {
			defaultvalue: reference,
			defaultdatevalue: reference == 'Custom_defalut' ? val : 'Today',
			value: null,
			betweenvalue: null,
			operator: 'Equals'
		};

		if (reference == 'Custom_last')
			tempObj[`lastvalue.${field}`] = val;

		if (reference == 'Custom_next')
			tempObj[`nextvalue.${field}`] = val;

		this.setState(tempObj);
	};

	callbackFn() {
		let { onChange } = this.props,
			{ operator, defaultvalue, value, betweenvalue, defaultdatevalue, lastvalue, nextvalue } = this.state,
			valueObj = null;

		if (['Custom', 'Custom_last', 'Custom_next', 'Custom_defalut'].indexOf(defaultvalue) == -1)
			valueObj = defaultvalue;
		else if (defaultvalue == 'Custom_defalut')
			valueObj = defaultdatevalue;
		else if (defaultvalue == 'Custom_last')
			valueObj = `Last ${lastvalue.value} ${lastvalue.operator ? lastvalue.operator : ''}`;
		else if (defaultvalue == 'Custom_next')
			valueObj = `Next ${nextvalue.value} ${nextvalue.operator ? nextvalue.operator : ''}`;
		else {
			if (value != null && value != undefined && value != '') {
				if (operator == 'Between') {
					if (betweenvalue != null && betweenvalue != undefined && betweenvalue != '')
						valueObj = {
							operator,
							value,
							betweenvalue
						};
				} else
					valueObj = {
						operator,
						value
					};
			}
		}

		this.state.actualValue = valueObj;

		onChange(valueObj);

		this.setState({actualValue: valueObj});
	};

	renderDateElement(reference) {
		let { defaultvalue, betweenvalue, value, operator } = this.state,
			minmaxCheck = false,
			input_Value = (reference == 'betweenvalue' ? betweenvalue : value),
			minDate = this.props.min ? moment(this.props.min) : undefined,
			maxDate = this.props.max ? moment(this.props.max) : undefined;

			if (reference == 'betweenvalue') {
				minDate = value ? moment(value) : minDate;

				if (value && betweenvalue && new Date(value).setHours(0,0,0,0) > new Date(betweenvalue).setHours(0,0,0,0))
					minmaxCheck = true;
			}

			let isError = (defaultvalue == 'Custom' && (input_Value == '' || input_Value == null || input_Value == undefined || minmaxCheck)) ? true : false;

		return (
			<DateElement
				className = {`form-control ${isError ? 'errorinput' : ''}`}
				value = {input_Value}
				onChange = {(val) => {this.valueOnChange(reference, val)}}
				required = {defaultvalue == 'Custom'}
				disabled = {this.props.disabled}
				min = {minDate}
				max = {maxDate}
				//placeholder = {`Select ${this.props.placeholder}`}
			/>
		);
	};

	renderCustomOperator() {
		let { operator, operatorOptions, defaultvalue } = this.state,
			isError = (defaultvalue == 'Custom' && (operator == '' || operator == null || operator == undefined)) ? true : false;

		return (
			<div className = 'col-md-11'>
				<div className = 'row'>
					<div className = 'col-md-4 paddingleft-0 paddingright-0'>
						<LocalSelect
							options = {operatorOptions}
							value = {operator}
							onChange = {(val) => {
								this.state.defaultvalue = 'Custom';
								this.state.value = null;
								this.state.betweenvalue = null;
								this.state.defaultdatevalue = 'Today';
								this.state.lastvalue = {
									operator: 'Days',
									value: null
								};
								this.state.nextvalue = {
									operator: 'Days',
									value: null
								};

								this.state.operator = 'val';

								this.callbackFn();

								this.setState({
									operator: val,
									value: null,
									betweenvalue: null,
									defaultvalue: 'Custom',
									defaultdatevalue: 'Today',
									lastvalue: {
										operator: 'Days',
										value: null
									},
									nextvalue: {
										operator: 'Days',
										value: null
									}
								})}
							}
							placeholder = {'Select Options'}
							className = {`${isError ? 'errorinput' : ''}`}
							required = {defaultvalue == 'Custom'}
							disabled = {this.props.disabled}
						/>
					</div>
					{operator == 'Between' ? <div className = 'col-md-8'>
						<div className = 'row'>
							<div className = 'col-md-6' style = {{paddingRight: '5px'}}>
								{this.renderDateElement('value')}
							</div>
							<div className = 'col-md-6' style = {{paddingLeft: '5px'}}>
								{this.renderDateElement('betweenvalue')}
							</div>
						</div>
					</div> : <div className = 'col-md-8'>
						<div className = 'row' style = {{marginRight: '0px'}}>
							<div className = 'col-md-6 paddingright-0'>
								{this.renderDateElement('value')}
							</div>
						</div>
					</div>}
				</div>
			</div>
		);
	};

	renderLastAndNextOperators(reference) {
		let { operator, operatorOptions, defaultvalue, lastvalue, nextvalue, dateFilterOptions } = this.state,
			renderValue = (reference == 'Custom_last' ? lastvalue : nextvalue);

		let isLastError = (defaultvalue == 'Custom_last' && (lastvalue.value == '' || lastvalue.value == null || lastvalue.value == undefined)) ? true : false;

		let isNextError = (defaultvalue == 'Custom_next' && (nextvalue.value == '' || nextvalue.value == null || nextvalue.value == undefined)) ? true : false;

		let isLastOperatorError = (defaultvalue == 'Custom_last' && !lastvalue.operator) ? true : false;

		let isNextOperatorError = (defaultvalue == 'Custom_next' && !nextvalue.operator) ? true : false;

		return (
			<div className = 'col-md-11'>
				<div className = 'row'>
					<div className = 'col-md-4 paddingleft-0 paddingright-0'>
						<div className = 'row'>
							<div className = 'col-md-3 d-flex flex-column justify-content-center'>
								{reference == 'Custom_last' ? 'Last' : 'Next'}
							</div>
							<div className = 'col-md-9'>
								<NumberElement
									className = {`form-control ${reference == 'Custom_last' && isLastError ? 'errorinput' : (reference == 'Custom_next' && isNextError ? 'errorinput' : '')}`}
									value = {renderValue.value}
									onChange = {(val)=>{this.filterValueOnchange(reference, val, 'value')}}
									required = {defaultvalue == reference}
									disabled = {this.props.disabled}
									//placeholder = {'Enter Value'}
								/>
							</div>
						</div>
					</div>
					<div className = 'col-md-8 btn-group-full-width'>
						<Buttongroup
							buttons = {dateFilterOptions}
							buttonclass = {`btn-sm`}
							onChange = {(val) => {this.filterValueOnchange(reference, val, 'operator')}}
							value = {renderValue.operator}
							className = {`${reference == 'Custom_last' && isLastOperatorError ? 'errorinput' : (reference == 'Custom_next' && isNextOperatorError ? 'errorinput' : '')}`}
							required = {defaultvalue == reference}
							disabled = {this.props.disabled}
						/>
					</div>
				</div>
			</div>
		);
	};

	renderDefalutOperator() {
		let { customOptions, defaultvalue, defaultdatevalue } = this.state,
			isError = (defaultvalue == 'Custom_defalut' && (defaultdatevalue == '' || defaultdatevalue == null || defaultdatevalue == undefined)) ? true : false;
		return (
			<div className = 'col-md-11 paddingleft-0'>
				<LocalSelect
					options = {customOptions}
					value = {defaultdatevalue}
					onChange = {(val) => {this.filterValueOnchange('Custom_defalut', val)}}
					placeholder = {'Select Options'}
					className = {`${isError ? 'errorinput' : ''}`}
					required = {defaultvalue == 'Custom_defalut'}
					disabled = {this.props.disabled}
				/>
			</div>
		);
	};

	renderPopoverBodyValue (displayNameLabel) {
		let { defaultvalue, defaultOptions, toggle } = this.state;

		return (
			<div className = 'row' style={{marginRight: '0px', marginLeft: '0px', padding: '8px'}}>
				{defaultOptions.length > 0 ? defaultOptions.map((filter, index) => {
					return (
						<div key = {index} className = 'form-group col-md-12'>
							<div className = 'row'>
								<div className = 'col-md-1 gs-reportfilter-align'>
									<input
										type = 'radio'
										disabled = {this.props.disabled}
										checked = {defaultvalue == filter}
										onChange = {() => {this.radioBtnOnchange(filter)}}
										/>
								</div>
								{filter == 'Custom' ? this.renderCustomOperator() : null}
								{['Custom_last', 'Custom_next'].indexOf(filter) >= 0 ? this.renderLastAndNextOperators(filter) : null}
								{filter == 'Custom_defalut' ? this.renderDefalutOperator() : null}
								{['Custom', 'Custom_last', 'Custom_next', 'Custom_defalut'].indexOf(filter) == -1 ? <div className = 'col-md-11 paddingclass'>
									<span>{filter}</span>
								</div> : null}
							</div>
						</div>
					);
				}) : null}
				<button
					type = 'button'
					disabled = {!displayNameLabel}
					onClick = {()=>{
						this.setState({
							toggle: false
						});
					}}
					className = 'btn btn-sm btn-width marginleft-15 gs-btn-success'>
					<i className = 'fa fa-check'></i>Ok
				</button>
			</div>
		);
	};

	render() {
		let { operator, defaultvalue, value, betweenvalue, defaultOptions, toggle, defaultdatevalue, lastvalue, nextvalue } = this.state,
			displayNameLabel = '';

		if (['Custom', 'Custom_last', 'Custom_next', 'Custom_defalut'].indexOf(defaultvalue) == -1)
			displayNameLabel = `${defaultvalue}`;
		else if (defaultvalue == 'Custom_defalut')
			displayNameLabel = `${defaultdatevalue ? defaultdatevalue : ''}`;
		else if (defaultvalue == 'Custom_last') {
			if (lastvalue.value && lastvalue.operator)
				displayNameLabel = `Last ${lastvalue.value} ${lastvalue.operator}`;
		} else if (defaultvalue == 'Custom_next') {
			if (nextvalue.value && nextvalue.operator)
				displayNameLabel = `Next ${nextvalue.value} ${nextvalue.operator}`;
		} else {
			if (operator && value) {
				if (operator == 'Between') {
					if (betweenvalue)
						displayNameLabel = `${operator} ${moment(value).format('DD-MMM-YYYY')} ${'and '+moment(betweenvalue).format('DD-MMM-YYYY')}`;
				} else
					displayNameLabel = `${operator} ${moment(value).format('DD-MMM-YYYY')}`;
			}
		}

		return (
			<Popover
				className = {'gs-reportfilter-popover'}
				preferPlace = {'below'}
				place={'below'}
				enterExitTransitionDurationMs = {0}
				offset={4}
				isOpen = {toggle}
				tipSize = {0.1}
				onOuterAction = {(mouseevent) => {
					if(CheckMouseEventOutSide(mouseevent)) {
						return this.setState({
							toggle: false
						});
					}
				}}
				body = {this.renderPopoverBodyValue(displayNameLabel)}
			>
				<div
					className = 'input-group'
					title = {`${displayNameLabel ? displayNameLabel : 'Select '+this.props.placeholder}`}
					onClick = {() => {
						this.setState({
							toggle: !toggle
						})
					}}
				>
					<input
						type = 'text'
						className = {`${this.props.className} text-ellipsis rptbuildText`}
						//placeholder = {`Select ${this.props.placeholder}`}
						required = {this.props.required}
						disabled = {true}
						autoComplete = 'off'
						value = {displayNameLabel}
					/>
					<span className = 'input-group-append'>
						{!this.props.disabled && this.props.value ? <div
							onClick = {(e) => {
								this.props.onChange(null);
								e.stopPropagation();
							}}
							rel = 'tooltip'
							title = 'Clear'
							className = 'btn btn-sm gs-btn-light disabled ndtclass'>
							<span>x</span>
						</div> : null}
						<button
							type = 'button'
							disabled = {this.props.disabled}
							className = 'btn btn-sm gs-btn-light ndtclass'>
							<span className = {`${toggle ? 'fa fa-caret-up' : 'fa fa-caret-down'}`}></span>
						</button>
					</span>
				</div>
			</Popover>
		);
	};
};

export class ReportLocalSelectElement extends Component {
	constructor(props) {
		super(props);

		this.state = {
			operator: 'Equals',
			defaultvalue: 'Custom',
			value: null,
			operatorOptions: ['Equals',
							'Not Equals'],
			defaultOptions: ['Custom',
							'Is Empty',
							'Is Not Empty'],
			toggle: false
		};

		if (this.props.value)
			this.onLoad(this.props);

		this.radioBtnOnchange = this.radioBtnOnchange.bind(this);
		this.callbackFn = this.callbackFn.bind(this);
		this.valueOnChange = this.valueOnChange.bind(this);
		this.renderCustomOperator = this.renderCustomOperator.bind(this);
		this.renderPopoverBodyValue = this.renderPopoverBodyValue.bind(this);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.value) {
			if (!this.state.actualValue)
				this.onLoad(nextProps);
			else if ((typeof(nextProps.value) == 'object' ? JSON.stringify(nextProps.value) : nextProps.value) != (typeof(this.state.actualValue) == 'object' ? JSON.stringify(this.state.actualValue) : this.state.actualValue))
				this.onLoad(nextProps);
		} else {
			if (this.state.actualValue)
				this.onLoad(nextProps);
		}
	};

	onLoad(props) {
		let tempObj = {};

		if (!props.value)
			tempObj = {
				operator: 'Equals',
				value: null,
				defaultvalue: 'Custom'
			};
		else if (typeof(props.value) == 'object')
			tempObj = {
				operator: props.value.operator,
				value: props.value.value,
				defaultvalue: 'Custom'
			};
		else
			tempObj = {
				defaultvalue: props.value
			};

		tempObj.actualValue = props.value;

		for (let prop in tempObj)
			this.state[prop] = tempObj[prop];

		setTimeout(() => {
			this.setState(tempObj);
		}, 0);
	};

	radioBtnOnchange(item) {
		this.state.defaultvalue = item;
		this.state.value = null;
		this.state.operator = 'Equals';

		this.callbackFn();

		this.setState({
			defaultvalue: item,
			value: null,
			operator: 'Equals'
		});
	};

	valueOnChange(val) {
		this.state.value = val;
		this.state.defaultvalue = 'Custom';

		this.callbackFn();

		this.setState({
			value: val,
			defaultvalue: 'Custom'
		});
	};

	callbackFn() {
		let { onChange } = this.props,
			{ operator, defaultvalue, value } = this.state,
			valueObj = null;

		if (defaultvalue != 'Custom')
			valueObj = defaultvalue;
		else {
			if (value != null && value != undefined && value != '')
				valueObj = {
					operator,
					value
				};
		}

		this.state.actualValue = valueObj;

		onChange(valueObj);

		this.setState({actualValue: valueObj});
	};

	renderCustomOperator() {
		let { defaultvalue, value, operator, operatorOptions } = this.state,
			{ options, label, valuename } = this.props,
			isError = (defaultvalue == 'Custom' && (value == '' || value == null || value == undefined)) ? true : false,
			isOperatorError = (defaultvalue == 'Custom' && (operator == '' || operator == null || operator == undefined)) ? true : false;

		return (
			<div className = 'col-md-11'>
				<div className = 'row'>
					<div className = 'col-md-4 paddingleft-0 paddingright-0'>
						<LocalSelect
							options = {operatorOptions}
							value = {operator}
							onChange = {(val) => {
								this.state.defaultvalue = 'Custom';
								this.state.value = null;
								this.state.operator = 'val';

								this.callbackFn();

								this.setState({
									operator: val,
									value: null,
									defaultvalue: 'Custom'
								})}
							}
							placeholder = {'Select Options'}
							className = {`${isOperatorError ? 'errorinput' : ''}`}
							required = {defaultvalue == 'Custom'}
							disabled = {this.props.disabled}
						/>
					</div>
					<div className = 'col-md-8' style = {{marginRight: '0px'}}>
						<LocalSelect
							options = {options}
							label = {label}
							valuename = {valuename}
							value = {value}
							onChange = {(val)=>{this.valueOnChange(val)}}
							//placeholder = {`Select ${this.props.placeholder}`}
							multiselect = {true}
							className = {`${isError ? 'errorinput' : ''}`}
							required = {this.props.required || defaultvalue == 'Custom'}
							disabled = {this.props.disabled}
						/>
					</div>
				</div>
			</div>
		);
	};

	renderPopoverBodyValue (displayNameLabel) {
		let { defaultvalue, defaultOptions, toggle } = this.state;

		return (
			<div className = 'row' style={{marginRight: '0px', marginLeft: '0px', padding: '8px'}}>
				{defaultOptions.length > 0 ? defaultOptions.map((filter, index) => {
					return (
						<div key = {index} className = 'form-group col-md-12'>
							<div className = 'row'>
								<div className = 'col-md-1 gs-reportfilter-align'>
									<input
										type = 'radio'
										disabled = {this.props.disabled}
										checked = {defaultvalue == filter}
										onChange = {() => {this.radioBtnOnchange(filter)}}
										/>
								</div>
								{filter == 'Custom' ? this.renderCustomOperator() : <div className = 'col-md-11 paddingclass'>
									<span>{filter}</span>
								</div>}
							</div>
						</div>
					);
				}) : null}
				<button
					type = 'button'
					disabled = {!displayNameLabel}
					onClick = {()=>{
						this.setState({
							toggle: false
						});
					}}
					className = 'btn btn-sm btn-width marginleft-15 gs-btn-success'>
					<i className = 'fa fa-check'></i>Ok
				</button>
			</div>
		);
	};

	render() {
		let { operator, defaultvalue, value, defaultOptions, toggle } = this.state,
			displayNameLabel = '';

		if (defaultvalue != 'Custom')
			displayNameLabel = `${defaultvalue}`;
		else {
			if (operator && Array.isArray(value) && value.length > 0)
				displayNameLabel = `${operator} ${Array.isArray(value) ? value.join(', ') : ''}`;
		}

		return (
			<Popover
				className = {'gs-reportfilter-popover'}
				preferPlace = {'below'}
				place={'below'}
				enterExitTransitionDurationMs = {0}
				offset={4}
				isOpen = {toggle}
				tipSize = {0.1}
				onOuterAction = {(mouseevent) => {
					if(CheckMouseEventOutSide(mouseevent)) {
						return this.setState({
							toggle: false
						});
					}
				}}
				body = {this.renderPopoverBodyValue(displayNameLabel)}
			>
				<div
					className = 'input-group'
					title = {`${displayNameLabel ? displayNameLabel : 'Select '+this.props.placeholder}`}
					onClick = {() => {
						this.setState({
							toggle: !toggle
						})
					}}
				>
					<input
						type = 'text'
						className = {`${this.props.className} text-ellipsis rptbuildText`}
						//placeholder = {`Select ${this.props.placeholder}`}
						required = {this.props.required}
						disabled = {true}
						autoComplete = 'off'
						value = {displayNameLabel}
					/>
					<span className = 'input-group-append'>
						{!this.props.disabled && this.props.value ? <div
							onClick = {(e) => {
								this.props.onChange(null);
								e.stopPropagation();
							}}
							rel = 'tooltip'
							title = 'Clear'
							className = 'btn btn-sm gs-btn-light disabled ndtclass'>
							<span>x</span>
						</div> : null}
						<button
							type = 'button'
							disabled = {this.props.disabled}
							className = 'btn btn-sm gs-btn-light ndtclass'>
							<span className = {`${toggle ? 'fa fa-caret-up' : 'fa fa-caret-down'}`}></span>
						</button>
					</span>
				</div>
			</Popover>
		);
	};
};

export const ReportAutoSelectElement = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			operator: 'Equals',
			defaultvalue: 'Custom',
			value: null,
			teamvalue: null,
			defaultcustomvalue: null,
			operatorOptions: ['Equals',
							'Not Equals'],
			defaultOptions: ['Custom',
							'Custom_defalut',
							'Is Empty',
							'Is Not Empty'],
			customOptions: [],
			customOptionsObj: {},
			teamOptions: ['In Team',
						'Not In Team'],
			userDefaultOptions: ['In User Default'],
			toggle: false,
			labelKey: 'name',
			displayLabelKey: 'name',
			valueKey: 'id',
			collapsedName: ''
		};

		if (this.props.value)
			this.onLoad(this.props);
		else
			this.onLoadResourceProps(this.props)

		this.radioBtnOnchange = this.radioBtnOnchange.bind(this);
		this.callbackFn = this.callbackFn.bind(this);
		this.valueOnChange = this.valueOnChange.bind(this);
		this.renderCustomOperator = this.renderCustomOperator.bind(this);
		this.getStateValue = this.getStateValue.bind(this);
		this.renderPopoverBodyValue = this.renderPopoverBodyValue.bind(this);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.value) {
			if (!this.state.actualValue)
				this.onLoad(nextProps);
			else if ((typeof(nextProps.value) == 'object' ? JSON.stringify(nextProps.value) : nextProps.value) != (typeof(this.state.actualValue) == 'object' ? JSON.stringify(this.state.actualValue) : this.state.actualValue))
				this.onLoad(nextProps);
		} else {
			if (this.state.actualValue)
				this.onLoad(nextProps);
		}
	};

	onLoadResourceProps(props) {
		let { operatorOptions, defaultOptions, teamOptions } = this.state;

		if (props.resource == 'users') {
			if (defaultOptions.includes('In My Team'))
				defaultOptions.splice(defaultOptions.indexOf('In My Team'), 1);

			if (defaultOptions.includes('Is Me'))
				defaultOptions.splice(defaultOptions.indexOf('Is Me'), 1);

			if (defaultOptions.includes('CustomTeam'))
				defaultOptions.splice(defaultOptions.indexOf('CustomTeam'), 1);

			if (defaultOptions.includes('Current Company'))
				defaultOptions.splice(defaultOptions.indexOf('Current Company'), 1);

			defaultOptions.splice(defaultOptions.indexOf('Custom')+1, 0, 'CustomTeam', 'Is Me', 'In My Team')
		} else {
			if (defaultOptions.includes('In My Team'))
				defaultOptions.splice(defaultOptions.indexOf('In My Team'), 1);

			if (defaultOptions.includes('Is Me'))
				defaultOptions.splice(defaultOptions.indexOf('Is Me'), 1);

			if (defaultOptions.includes('CustomTeam'))
				defaultOptions.splice(defaultOptions.indexOf('CustomTeam'), 1);

			if (props.resource == 'companymaster') {
				if (defaultOptions.includes('Current Company'))
					defaultOptions.splice(defaultOptions.indexOf('Current Company'), 1);

				defaultOptions.splice(defaultOptions.indexOf('Custom')+1, 0, 'Current Company');
			}
		}

		let tempObj = {
			defaultOptions,
			labelKey: props.label ? props.label : `name`,
			displayLabelKey: props.displaylabel ? props.displaylabel : (props.label ? props.label : `name`),
			valueKey: props.valuename ? props.valuename : `id`,
			collapsedName: ''
		};

		if(props.resource != 'users') {
			tempObj.customOptions = [];
			tempObj.customOptionsObj = {};

			for (var prop in this.props.app.myResources.users.fields) {
				let userFieObj = this.props.app.myResources.users.fields[prop];
				if((userFieObj.isForeignKey && userFieObj.foreignKeyOptions && userFieObj.foreignKeyOptions.resource == props.resource) || (userFieObj.isArrayForeignKey && userFieObj.foreignKeyArrayOptions && userFieObj.foreignKeyArrayOptions.resource == props.resource)) {
					tempObj.customOptions.push({
						id: `userid_${prop}`,
						name: `User's ${userFieObj.displayName}`
					});
					tempObj.customOptionsObj[`userid_${prop}`] = `User's ${userFieObj.displayName}`;
				}
			}
		} else {
			tempObj.customOptions = [];
			tempObj.customOptionsObj = {};
		}

		for (let prop in tempObj)
			this.state[prop] = tempObj[prop];

		setTimeout(() => {
			this.setState(tempObj, () => {
				if (typeof(props.value) == 'object')
					this.getStateValue(props);
			});
		}, 0);
	};

	onLoad(props) {
		let { operatorOptions, defaultOptions, teamOptions, userDefaultOptions } = this.state;

		if (props.resource == 'users') {
			if (defaultOptions.includes('In My Team'))
				defaultOptions.splice(defaultOptions.indexOf('In My Team'), 1);

			if (defaultOptions.includes('Is Me'))
				defaultOptions.splice(defaultOptions.indexOf('Is Me'), 1);

			if (defaultOptions.includes('CustomTeam'))
				defaultOptions.splice(defaultOptions.indexOf('CustomTeam'), 1);

			if (defaultOptions.includes('Current Company'))
				defaultOptions.splice(defaultOptions.indexOf('Current Company'), 1);

			defaultOptions.splice(defaultOptions.indexOf('Custom')+1, 0, 'CustomTeam', 'Is Me', 'In My Team')
		} else {
			if (defaultOptions.includes('In My Team'))
				defaultOptions.splice(defaultOptions.indexOf('In My Team'), 1);

			if (defaultOptions.includes('Is Me'))
				defaultOptions.splice(defaultOptions.indexOf('Is Me'), 1);

			if (defaultOptions.includes('CustomTeam'))
				defaultOptions.splice(defaultOptions.indexOf('CustomTeam'), 1);

			if (props.resource == 'companymaster') {
				if (defaultOptions.includes('Current Company'))
					defaultOptions.splice(defaultOptions.indexOf('Current Company'), 1);

				defaultOptions.splice(defaultOptions.indexOf('Custom')+1, 0, 'Current Company');
			}
		}

		let tempObj = {}, customOptions = [], customOptionsObj = {};

		if(props.resource != 'users') {
			for (var prop in this.props.app.myResources.users.fields) {
				let userFieObj = this.props.app.myResources.users.fields[prop];
				if((userFieObj.isForeignKey && userFieObj.foreignKeyOptions && userFieObj.foreignKeyOptions.resource == props.resource) || (userFieObj.isArrayForeignKey && userFieObj.foreignKeyArrayOptions && userFieObj.foreignKeyArrayOptions.resource == props.resource)) {
					customOptions.push({
						id: `userid_${prop}`,
						name: `User's ${userFieObj.displayName}`
					});
					customOptionsObj[`userid_${prop}`] = `User's ${userFieObj.displayName}`;
				}
			}
		}

		if (!props.value)
			tempObj = {
				operator: (this.state.defaultvalue && this.state.defaultvalue == 'CustomTeam') ? 'In Team' : 'Equals',
				value: null,
				betweenvalue: null,
				teamvalue: null,
				collapsedName: '',
				defaultvalue: 'Custom',
				defaultcustomvalue: null
				//defaultvalue: (this.state.defaultvalue && this.state.defaultvalue == 'CustomTeam') ? 'CustomTeam' : 'Custom'
			};
		else if (typeof(props.value) == 'object') {
			if (operatorOptions.indexOf(props.value.operator) >= 0)
				tempObj = {
					operator: props.value.operator,
					value: props.value.value,
					defaultvalue: 'Custom'
				};

			if (teamOptions.indexOf(props.value.operator) >= 0)
				tempObj = {
					operator: props.value.operator,
					teamvalue: props.value.value,
					defaultvalue: 'CustomTeam'
				};

			if (userDefaultOptions.indexOf(props.value.operator) >= 0) {
				tempObj = {
					defaultvalue: 'Custom_defalut',
					defaultcustomvalue: props.value.value
				};
			}
		}
		else
			tempObj = {
				defaultvalue: props.value,
				collapsedName: props.value
			};

		tempObj.actualValue = props.value;
		tempObj.defaultOptions = defaultOptions;

		tempObj.customOptions = customOptions;
		tempObj.customOptionsObj = customOptionsObj;

		tempObj.labelKey = props.label ? props.label : `name`;
		tempObj.displayLabelKey = props.displaylabel ? props.displaylabel : (props.label ? props.label : `name`);
		tempObj.valueKey = props.valuename ? props.valuename : `id`;

		for (let prop in tempObj)
			this.state[prop] = tempObj[prop];

		setTimeout(() => {
			this.setState(tempObj, () => {
				if (typeof(props.value) == 'object')
					this.getStateValue(props);
			});
		}, 0);
	};

	radioBtnOnchange(item) {
		this.state.defaultvalue = item;
		this.state.value = null;
		this.state.teamvalue = null;

		this.state.collapsedName = (['Custom', 'CustomTeam'].indexOf(item) == -1) ? item : '';

		this.state.operator = (['Custom', 'CustomTeam'].indexOf(item) == -1) ? 'Equals' : ((item == 'CustomTeam') ? 'In Team' : 'Equals');

		this.callbackFn();

		this.setState({
			defaultvalue: item,
			value: null,
			teamvalue: null,
			defaultcustomvalue: null,
			operator: (['Custom', 'CustomTeam'].indexOf(item) == -1) ? 'Equals' : ((item == 'CustomTeam') ? 'In Team' : 'Equals'),
			collapsedName: (['Custom', 'CustomTeam'].indexOf(item) == -1) ? item : ''
		});
	};

	valueOnChange(reference, val, valObj) {
		let tempObj = {
			defaultvalue: reference == 'teamvalue' ? 'CustomTeam' : 'Custom',
			defaultcustomvalue: null,
			value: null,
			teamvalue: null,
			collapsedName: ''
		};

		let nameObj = {},
			tempColName = [],
			tempLabelKey = (reference == 'teamvalue') ? 'fullname' : this.state.labelKey;

		(valObj || []).forEach((item) => {
			nameObj[item[`${this.state.valueKey}`]] = item[`${tempLabelKey}`];
		});

		(val || []).forEach((item) => {
			if (nameObj[item])
				tempColName.push(nameObj[item]);
		});

		if (reference == 'teamvalue' && !this.state.teamOptions.includes(this.state.operator))
			tempObj['operator'] = 'In Team';

		if (reference == 'value' && !this.state.operatorOptions.includes(this.state.operator))
			tempObj['operator'] = 'Equals';

		tempObj[reference] = val;

		tempObj['collapsedName'] = val ? `${tempObj['operator'] ? tempObj['operator'] : this.state.operator} ${tempColName.join(', ')}` : '';

		if (reference == 'teamvalue' && !this.state.teamOptions.includes(this.state.operator))
			this.state.operator = 'In Team';

		if (reference == 'value' && !this.state.operatorOptions.includes(this.state.operator))
			this.state.operator = 'Equals';

		this.state.collapsedName = val ? `${this.state.operator} ${tempColName.join(', ')}` : '';

		this.state[reference] = val;

		this.state.defaultvalue = reference == 'teamvalue' ? 'CustomTeam' : 'Custom';

		this.callbackFn();

		this.setState(tempObj, () => {
			/*let collapsedResource = {
				resource: (reference == 'teamvalue') ? 'teamstructure' : this.props.resource,
				fields: (reference == 'teamvalue') ? 'id,fullname' : this.props.fields,
				labelKey: (reference == 'teamvalue') ? 'fullname' : this.state.labelKey,
				valueKey: (reference == 'teamvalue') ? 'id' : this.state.valueKey,
				value: {
					value: val,
					operator: this.state.operator
				}
			};

			this.getStateValue(collapsedResource)*/
		});
	};

	filterValueOnchange(reference, val, field) {
		this.state.defaultvalue = reference;
		this.state.value = null;
		this.state.operator = 'Equals';

		this.state.defaultcustomvalue = val;

		this.state.collapsedName = val ? (this.state.customOptionsObj[val] ? this.state.customOptionsObj[val] : val) : nul;

		//if (reference != 'Custom_defalut' && renderValue.value && renderValue.operator)
			this.callbackFn();

		let tempObj = {
			defaultvalue: reference,
			defaultcustomvalue: reference == 'Custom_defalut' ? val : null,
			value: null,
			collapsedName: this.state.collapsedName,
			operator: 'Equals'
		};

		this.setState(tempObj);
	};

	getStateValue(props) {
		let { operator, defaultvalue, value, teamvalue, defaultOptions, operatorOptions, teamOptions, toggle } = this.state;

		if (['Custom', 'CustomTeam', 'Custom_defalut'].indexOf(defaultvalue) == -1) {
			this.state.collapsedName = `${defaultvalue}`;

			this.setState({
				collapsedName: `${defaultvalue}`
			});
		} else if(['Custom_defalut'].includes(defaultvalue)) {
			this.state.collapsedName = props.value.value ? (this.state.customOptionsObj[props.value.value] ? this.state.customOptionsObj[props.value.value] : props.value.value) : null;

			this.setState({
				collapsedName: this.state.collapsedName
			});
		} else {
			/*if (defaultvalue == 'Custom' || operatorOptions.indexOf(operator) >= 0)
				collapsedName = `${operator} ${(typeof(value) == 'string') ? value : Array.isArray(value) ? value.join(', '): ''}`;

			if (defaultvalue == 'CustomTeam' || teamOptions.indexOf(operator) >= 0)
				collapsedName = `${operator} ${(typeof(teamvalue) == 'string') ? teamvalue : Array.isArray(teamvalue) ? teamvalue.join(', '): ''}`;
			*/
			let collapsedResource = {
				resource: (teamOptions.indexOf(props.value.operator) >= 0) ? 'teamstructure' : props.resource,
				fields: (teamOptions.indexOf(props.value.operator) >= 0) ? 'id,fullname' : props.fields,
				labelKey: (teamOptions.indexOf(props.value.operator) >= 0) ? 'fullname' : this.state.labelKey,
				valueKey: (teamOptions.indexOf(props.value.operator) >= 0) ? 'id' : this.state.valueKey
			};

			let selectedValue = props.value.value;

			let filterString = `${collapsedResource.resource}.${collapsedResource.valueKey}=ANY(array[${selectedValue}])`;

			let url = `/api/${collapsedResource.resource}?field=${collapsedResource.fields}&skip=0&pagelength=10&filtercondition=${filterString}`;

			axios.get(url).then((response) => {
				if(response.data.message == 'success') {
					if(response.data.main.length > 0) {
						let nameObj = {},
							tempColName = [];

						response.data.main.forEach((item) => {
							nameObj[item[`${collapsedResource.valueKey}`]] = item[`${collapsedResource.labelKey}`];
						});

						selectedValue.forEach((item) => {
							if (nameObj[item])
								tempColName.push(nameObj[item]);
						});

						this.state.collapsedName = `${this.state.operator} ${tempColName.join(', ')}`;

						this.setState({
							collapsedName: `${this.state.operator} ${tempColName.join(', ')}`
						});
					}
				}
			});
		}
	};

	callbackFn() {
		let { onChange } = this.props,
			{ operator, defaultvalue, defaultcustomvalue, value, teamvalue, collapsedName } = this.state,
			valueObj = null;

		if (['Custom', 'CustomTeam', 'Custom_defalut'].indexOf(defaultvalue) == -1)
			valueObj = defaultvalue;
		else if(defaultvalue == 'Custom_defalut') {
			if(defaultcustomvalue) {
				valueObj = {
					operator: 'In User Default',
					value: defaultcustomvalue,
					value_dn: collapsedName
				};
			}
		} else {
			if (defaultvalue == 'Custom' && value != null && value != undefined && value != '')
				valueObj = {
					operator: operator || 'Equals',
					value,
					value_dn: collapsedName
				};

			if (defaultvalue == 'CustomTeam' && teamvalue != null && teamvalue != undefined && teamvalue != '')
				valueObj = {
					operator: operator || 'In Team',
					value: teamvalue,
					value_dn: collapsedName
				};
		}

		this.state.actualValue = valueObj;

		onChange(valueObj);

		this.setState({actualValue: valueObj});
	};

	renderCustomOperator(reference) {
		if(reference == 'Custom_defalut')
			return this.renderDefalutOperator();

		let { defaultvalue, value, teamvalue, operator, operatorOptions, teamOptions, labelKey, displayLabelKey, valueKey } = this.state,
			{ resource, fields, displaylabel, label, valuename, filter } = this.props;

		let SelectOptions = reference == 'Custom' ? operatorOptions : teamOptions;

		let isError = (defaultvalue == 'Custom' && (value == '' || value == null || value == undefined)) ? true : false;

		let isTeamError = (defaultvalue == 'CustomTeam' && (teamvalue == '' || teamvalue == null || teamvalue == undefined)) ? true : false;

		let isOperatorError = (['Custom', 'CustomTeam'].includes(defaultvalue) && (operator == '' || operator == null || operator == undefined)) ? true : false;

		return (
			<div className = 'col-md-11'>
				<div className = 'row'>
					<div className = 'col-md-4 paddingleft-0 paddingright-0'>
						<LocalSelect
							options = {SelectOptions}
							value = {SelectOptions.includes(operator) ? operator : null}
							onChange = {(val) => {
								this.state.defaultvalue = reference;
								this.state.value = null;
								this.state.teamvalue = null;
								this.state.collapsedName = '';
								this.state.operator = val;

								this.callbackFn();

								this.setState({
									operator: val,
									value: null,
									teamvalue: null,
									collapsedName: '',
									defaultcustomvalue: null,
									defaultvalue: reference
								})}
							}
							placeholder = {'Select Options'}
							className = {`${isOperatorError ? 'errorinput' : ''}`}
							required = {defaultvalue == reference}
							disabled = {this.props.disabled}
						/>
					</div>
					<div className = 'col-md-8' style = {{marginRight: '0px'}}>
						{reference == 'Custom' ? <AutoMultiSelect
							resource = {resource}
							fields = {fields}
							displaylabel = {displayLabelKey}
							label = {labelKey}
							valuename = {valueKey}
							filter = {filter}
							value = {value}
							onChange = {(val, valObj)=>{this.valueOnChange('value', val, valObj)}}
							placeholder = {`Select ${this.props.placeholder}`}
							//multiselect = {this.props.multiselect}
							className = {`${isError ? 'errorinput' : ''}`}
							required = {this.props.required || defaultvalue == 'Custom'}
							disabled = {this.props.disabled}
						/> : <AutoMultiSelect
							resource = {'teamstructure'}
							fields = {'id,fullname'}
							label = {'fullname'}
							value = {teamvalue}
							onChange = {(val, valObj)=>{this.valueOnChange('teamvalue', val, valObj)}}
							placeholder = {'Select Team'}
							//multiselect = {this.props.multiselect}
							className = {`${isTeamError ? 'errorinput' : ''}`}
							required = {this.props.required || defaultvalue == 'CustomTeam'}
							disabled = {this.props.disabled}
						/>}
					</div>
				</div>
			</div>
		);
	};

	renderDefalutOperator() {
		let { customOptions, defaultvalue, defaultcustomvalue } = this.state,
			isError = (defaultvalue == 'Custom_defalut' && (defaultcustomvalue == '' || defaultcustomvalue == null || defaultcustomvalue == undefined)) ? true : false;

		if(customOptions.length == 0)
			return null;

		return (
			<div className = 'col-md-11 paddingleft-0'>
				<LocalSelect
					options = {customOptions}
					value = {defaultcustomvalue}
					onChange = {(val) => {this.filterValueOnchange('Custom_defalut', val)}}
					placeholder = {'Select Options'}
					className = {`${isError ? 'errorinput' : ''}`}
					required = {defaultvalue == 'Custom_defalut'}
					disabled = {this.props.disabled}
				/>
			</div>
		);
	};

	renderPopoverBodyValue () {
		let { defaultvalue, defaultOptions, toggle, collapsedName } = this.state;

		return (
			<div className = 'row' style={{marginRight: '0px', marginLeft: '0px', padding: '8px'}}>
				{defaultOptions.length > 0 ? defaultOptions.map((filter, index) => {
					if(filter == 'Custom_defalut' && this.state.customOptions.length == 0)
						return null;

					return (
						<div key = {index} className = 'form-group col-md-12'>
							<div className = 'row'>
								<div className = 'col-md-1 gs-reportfilter-align'>
									<input
										type = 'radio'
										disabled = {this.props.disabled}
										checked = {defaultvalue == filter}
										onChange = {() => {this.radioBtnOnchange(filter)}}
										/>
								</div>
								{['Custom', 'CustomTeam', 'Custom_defalut'].indexOf(filter) >= 0 ? this.renderCustomOperator(filter) : <div className = 'col-md-11 paddingclass'>
									<span>{filter}</span>
								</div>}
							</div>
						</div>
					);
				}) : null}
				<button
					type = 'button'
					disabled = {!collapsedName}
					onClick = {()=>{
						this.setState({
							toggle: false
						});
					}}
					className = 'btn btn-sm btn-width marginleft-15 gs-btn-success'>
					<i className = 'fa fa-check'></i>Ok
				</button>
			</div>
		);
	};

	render() {
		let { operator, defaultvalue, value, teamvalue, defaultOptions, operatorOptions, teamOptions, toggle, collapsedName } = this.state,
			displayNameLabel = '';

		/*if (['Custom', 'CustomTeam'].indexOf(defaultvalue) == -1)
			displayNameLabel = `${defaultvalue}`;
		else {
			if (defaultvalue == 'Custom' || operatorOptions.indexOf(operator) >= 0)
				displayNameLabel = `${operator ? operator : ''} ${(typeof(value) == 'string') ? value : Array.isArray(value) ? value.join(', '): ''}`;

			if (defaultvalue == 'CustomTeam' || teamOptions.indexOf(operator) >= 0)
				displayNameLabel = `${operator ? operator : ''} ${(typeof(teamvalue) == 'string') ? teamvalue : Array.isArray(teamvalue) ? teamvalue.join(', '): ''}`;
		}*/

		return (
			<Popover
				className = {'gs-reportfilter-popover'}
				preferPlace = {'below'}
				place={'below'}
				enterExitTransitionDurationMs = {0}
				offset={4}
				isOpen = {toggle}
				tipSize = {0.1}
				onOuterAction = {(mouseevent) => {
					if(CheckMouseEventOutSide(mouseevent)) {
						return this.setState({
							toggle: false
						});
					}
				}}
				body = {this.renderPopoverBodyValue()}
			>
				<div
					className = 'input-group'
					title = {`${collapsedName ? collapsedName : 'Select '+this.props.placeholder}`}
					onClick = {() => {
						this.setState({
							toggle: !toggle
						})
					}}
				>
					<input
						type = 'text'
						className = {`${this.props.className} text-ellipsis rptbuildText`}
						//placeholder = {`Select ${this.props.placeholder}`}
						required = {this.props.required}
						disabled = {true}
						autoComplete = 'off'
						value = {collapsedName}
					/>
					<span className = 'input-group-append'>
						{!this.props.disabled && this.props.value ? <div
							onClick = {(e) => {
								this.props.onChange(null);
								e.stopPropagation();
							}}
							rel = 'tooltip'
							title = 'Clear'
							className = 'btn btn-sm gs-btn-light disabled ndtclass'>
							<span>x</span>
						</div> : null}
						<button
							type = 'button'
							disabled = {this.props.disabled}
							className = 'btn btn-sm gs-btn-light ndtclass'>
							<span className = {`${toggle ? 'fa fa-caret-up' : 'fa fa-caret-down'}`}></span>
						</button>
					</span>
				</div>
			</Popover>
		);
	};
});