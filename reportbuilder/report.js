import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { updateFormState, updateReportBuilderJSONState, resetReportFilterByReport } from '../actions/actions';
import { WithContext as ReactTags } from 'react-tag-input';

import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import withDragDropContext from '../utils/withDragDropContext';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { DateEle, localSelectEle, autoSelectEle, selectAsyncEle, checkboxEle, textareaEle, InputEle, NumberEle, SpanEle, autosuggestEle, ButtongroupEle, autoMultiSelectEle } from '../components/formelements';
import { customfieldAssign, numberNewValidation, dateNewValidation, stringNewValidation, checkActionVerbAccess, multiSelectNewValidation, requiredNewValidation } from '../utils/utils';
import { dateFilter, datetimeFilter, timeFilter} from '../utils/filter';
import Listviewlistcontainer from '../components/list/listviewlistcontainer';
import { Prompt } from 'react-router-dom';
import { ReportFilterComponent } from './reportfilters';
import { AdditionalInformationSection } from '../containers/ndtsection1';

class ReportBuilder extends Component {
	constructor(props) {
		super(props);

		let resourceArray = [];

		let myResources = this.props.app.myResources;

		for (let prop in myResources) {
			if (myResources[prop].type != 'query')
				resourceArray.push({
					id : prop,
					name : myResources[prop].displayName
				});
		}

		this.state = {
			loaderflag: true,
			createParam: this.props.match.params.id > 0 ? false : true,
			rolesArray: [],
			searchresourcefield: '',
			resourceArray: resourceArray,
			columnsArray: [],
			columnsDisplayNameObj: {},
			FilterArray: [],
			aggregateFieldsArray: [],
			measureTypes: ['COUNT', 'SUM', 'AVG', 'MIN', 'MAX'],
			measureOptions: {
				COUNT: ['date', 'datetime', 'integer'],
				SUM: ['integer'],
				AVG: ['integer'],
				MIN: ['date', 'datetime', 'integer'],
				MAX: ['date', 'datetime', 'integer']
			}
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.initialize = this.initialize.bind(this);
		this.getItemById = this.getItemById.bind(this);
		this.getRoles = this.getRoles.bind(this);
		this.getColumns = this.getColumns.bind(this);
		this.onPagejsonfieldDrop = this.onPagejsonfieldDrop.bind(this);
		this.onDragEnd = this.onDragEnd.bind(this);
		this.copyfunction = this.copyfunction.bind(this);
		this.copyCallBack = this.copyCallBack.bind(this);
		this.typeOnChange = this.typeOnChange.bind(this);
		this.measureOnChange = this.measureOnChange.bind(this);
		this.save = this.save.bind(this);
		this.validateReport = this.validateReport.bind(this);
		this.openDataset = this.openDataset.bind(this);
		this.renderConfiguration = this.renderConfiguration.bind(this);
		this.renderDefinedParameters = this.renderDefinedParameters.bind(this);
		this.measurePTOnChange = this.measurePTOnChange.bind(this);
		this.onclickDnd = this.onclickDnd.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);

		if (this.props.match.path == '/createReport')
			this.initialize();
		else
			this.getItemById();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	initialize() {
		let tempObj = {
			hasexcelexport: true,
			type: 'Normal'
		};

		if (this.props.location.params){
			const params = this.props.location.params;

			if (params.param == 'copy')
				tempObj = params;

			if (params.param == 'DataSet') {
				tempObj['datasetid'] = params.id;

				if (params.type == 'Analytics')
					tempObj['config'] = {
						analyticstype: null,
						fields: {},
						filter: [],
						aggregation: {}
					};
				else
					tempObj['config'] = {
						freezedcolumns: 0,
						fields: [],
						filter: [],
						sumaggregation: []
					};
			}
		} else
			tempObj['config'] = {
				freezedcolumns: 0,
				fields: [],
				filter: [],
				sumaggregation: []
			};

		this.props.initialize(tempObj);
		setTimeout(this.getRoles, 0)

		this.updateLoaderFlag(false);
	}

	copyfunction() {
		this.props.openModal({
			render: (closeModal) => {
				return <CopyModal
					callback = {this.copyCallBack}
					closeModal = {closeModal} />
			}, className: {
				content: 'react-modal-custom-class',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	copyCallBack() {
		let tempObject = {},
			resource = this.props.app.myResources['reports'];

		for (let prop in resource['fields']) {
			for (let prop1 in this.props.resource) {
				if (prop == prop1) {
					if (!resource.fields[prop].restrictToCopy) {
						tempObject[prop] = this.props.resource[prop1];

						if (resource['fields'][prop]['foreignKeyOptions']) {
							if (resource.fields[prop]['foreignKeyOptions']['mainField']) {
								let tempmainfield = resource.fields[prop]['foreignKeyOptions']['mainField'].split('_')[1];
								tempmainfield = tempmainfield.split('.').join('_');
								tempObject[tempmainfield] = this.props.resource[tempmainfield];
							}
							if (resource.fields[prop]['foreignKeyOptions']['additionalField']) {
								for (let l = 0; l < resource.fields[prop]['foreignKeyOptions']['additionalField'].length; l++) {
									let tempadditionalfield = resource.fields[prop]['foreignKeyOptions']['additionalField'][l].split('_')[1];
									tempadditionalfield = tempadditionalfield.split('.').join('_');
									tempObject[tempadditionalfield] = this.props.resource[tempadditionalfield];
								}
							}
						}
					}
				}
			}
		}

		tempObject.config.filter = tempObject.config.filter ? tempObject.config.filter : [];

		tempObject.param = 'copy';

		this.props.history.push({pathname: '/createReport', params: tempObject});
	}

	getItemById() {
		axios.get(`/api/reports/${this.props.match.params.id}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.initialize({
					...response.data.main
				});

				this.setState({
					ndt: {
						notes: response.data.notes,
						documents: response.data.documents,
						tasks: response.data.tasks
					}
				});

				this.getRoles();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	getRoles() {
		let rolesArray = [];

		axios.get(`/api/roles?&field=id,name&filtercondition=`).then((response) => {
			if (response.data.message == 'success') {
				rolesArray = response.data.main;

				rolesArray.sort(
					(a, b) => {
						return (a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0;
					}
				);
			}

			this.setState({ rolesArray }, () => {
				if (this.props.resource && this.props.resource.datasetid)
					this.getColumns();
			});
		});
	};

	getColumns() {
		this.updateLoaderFlag(true);

		let columnsArray = [],
			columnsDisplayNameObj = {},
			resourceConfigColumns = [],
			FilterArray = [],
			FilterObj = {},
			aggregateFieldsArray = [];

		if (this.props.resource && this.props.resource.datasetid) {
			axios.get(`/api/dataset/info/${this.props.resource.datasetid}`).then((response) => {
				if (response.data.message == 'success') {
					for (let prop in response.data.main.fields) {
						columnsDisplayNameObj[prop] = response.data.main.fields[prop].displayName;

						columnsArray.push({
							field: prop,
							jsonname: 'resourcejson',
							...response.data.main.fields[prop]
						});

						if ((response.data.main.fields[prop].type == 'string' && (response.data.main.fields[prop].group == 'localselect' || response.data.main.fields[prop].group == 'resource')) || ['boolean', 'date', 'integer'].includes(response.data.main.fields[prop].type) || (response.data.main.fields[prop].isArrayForeignKey))
							FilterArray.push({
								field: prop,
								jsonname: 'resourcejson',
								...response.data.main.fields[prop]
							});

						if (['date', 'datetime'].includes(response.data.main.fields[prop].type) || (response.data.main.fields[prop].type == 'integer' && !response.data.main.fields[prop].isForeignKey))
							aggregateFieldsArray.push({
								field: prop,
								jsonname: 'resourcejson',
								...response.data.main.fields[prop]
							});
					}

					if (this.props.resource && this.props.resource.type == 'Normal' && this.props.resource.config.fields)
						this.props.resource.config.fields.forEach((item) => {
							if (response.data.main.fields[item])
								resourceConfigColumns.push(item)
						});
				}

				this.setState({
					columnsDisplayNameObj,
					columnsArray,
					FilterArray,
					FilterObj: response.data.main.fields,
					definedParameters: response.data.main.definedParameters,
					aggregateFieldsArray
				}, () => {
					if (this.props.resource.type == 'Normal')
						this.props.updateFormState(this.props.form, {
							[`config.fields`]: resourceConfigColumns
						});
				});

				this.updateLoaderFlag(false);
			});
		} else {
			this.setState({
				columnsDisplayNameObj,
				columnsArray,
				FilterArray,
				FilterObj: {},
				aggregateFieldsArray
			}, () => {
				let tempObj = {};

				if (this.props.resource.type == 'Analytics')
					tempObj = {
						config: {
							analyticstype: null,
							fields: {},
							filter: [],
							aggregation: {}
						}
					};
				else if (this.props.resource.type == 'Performance Target')
					tempObj = {
						config: {
							fields: {},
							filter: []
						}
					};
				else
					tempObj = {
						config: {
							freezedcolumns: 0,
							fields: [],
							filter: [],
							sumaggregation: []
						}
					};

				this.props.updateFormState(this.props.form, tempObj);
			});

			this.updateLoaderFlag(false);
		}
	};

	validateReport(actionverb) {
		if(actionverb != 'Save')
			return this.save(actionverb);

		let errorArray = [];

		let companyColumnsArray = [];
		this.state.columnsArray.forEach((column) => {
			if(column.field == 'companyid')
				companyColumnsArray.push(column.field);
			else {
				let splitbyunderscore = column.field.split('_');
				if(splitbyunderscore.length == 2) {
					if(splitbyunderscore[0].indexOf('parent') == 0 && splitbyunderscore[1] == 'companyid')
						companyColumnsArray.push(column.field);
				}
			}
		});

		let companyColumnFound = false;
		for(var i=0;i<companyColumnsArray.length;i++) {
			for(var j=0;j<this.props.resource.config.filter.length;j++) {
				if(companyColumnsArray[i] == this.props.resource.config.filter[j].field) {
					companyColumnFound = true;
					break;
				}
			}

			if(companyColumnFound)
				break;
		}

		if(companyColumnsArray.length > 0 && !companyColumnFound)
			errorArray.push(`You have not added company filter. This will show data for all companies. We strongly recommend that you add company filter.`);

		if(errorArray.length == 0)
			return this.save(actionverb);

		let message = {
			header : 'Warning',
			bodyArray: [...errorArray],
			btnTitle : ` Do you want to continue?`,
			btnArray : ['Yes','No']
		};
		this.props.openModal(modalService['confirmMethod'](message, (param) => {
			if(param) {
				this.save(actionverb);
			}
		}));
	}

	save(param, confirm) {
		this.updateLoaderFlag(true);

		axios({
			method: 'post',
			data: {
				actionverb: param,
				data: this.props.resource,
				ignoreExceptions: confirm ? true : false
			},
			url: '/api/reports'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.save(param, true);
			}));

			if (response.data.message == 'success') {
				let resource = {
					...response.data.main
				};

				if (this.props.match.path == '/createReport')
					this.props.history.replace(`/details/reports/${response.data.main.id}`);
				else if (param == 'Delete')
					this.props.history.replace('/list/reports');
				else {
					this.props.resetReportFilterByReport(`ReportBuilder_${this.props.resource.id}`);
					this.props.updateReportBuilderJSONState(this.props.resource.id, null);
					this.props.initialize(resource);
				}
			}

			this.updateLoaderFlag(false);
		});
	}

	onPagejsonfieldDrop(data, index, isdrilldown) {
		if(isdrilldown) {
			if (['pagejson', 'resourcejson'].indexOf(data.jsonname) >= 0) {
				let drilldownfields = [...this.props.resource.config.drilldownfields];

				if (data.jsonname == 'pagejson')
					drilldownfields.splice(drilldownfields.indexOf(data.field), 1);

				if (index === 0 || index > 0)
					drilldownfields.splice(index, 0, data.field);
				else
					drilldownfields.push(data.field);

				this.props.updateFormState(this.props.form, {
					[`config.drilldownfields`]: drilldownfields
				});
			}

			return true;
		}
		if (['pagejson', 'resourcejson'].indexOf(data.jsonname) >= 0) {
			let fields = [...this.props.resource.config.fields];

			if (data.jsonname == 'pagejson')
				fields.splice(fields.indexOf(data.field), 1);

			if (index === 0 || index > 0)
				fields.splice(index, 0, data.field);
			else
				fields.push(data.field);

			this.props.updateFormState(this.props.form, {
				[`config.fields`]: fields
			});
		}
	}

	onDragEnd(result, isdrilldown) {
		const { source, destination, draggableId } = result;
		let jsonNameArr = draggableId.split('-');

		if (!destination || (destination.droppableId === source.droppableId && source.droppableId == 'column-1')) {
			return;
		}

		if(isdrilldown) {
			let drilldownfields = [...this.props.resource.config.drilldownfields],
			sumaggregation = [...this.props.resource.config.sumaggregation];

			if (source.droppableId === destination.droppableId && source.droppableId != 'column-1') {
				let data = drilldownfields[source.index];
				let sumaggdata = sumaggregation[source.index];

				drilldownfields.splice(source.index, 1);
				drilldownfields.splice(destination.index, 0, data);

				sumaggregation.splice(source.index, 1);
				sumaggregation.splice(destination.index, 0, sumaggdata);
	
				this.props.updateFormState(this.props.form, {
					[`config.drilldownfields`]: drilldownfields,
					[`config.sumaggregation`]: sumaggregation
				});
				return;
			}

			if (jsonNameArr[1] == 'pagejson') {
				drilldownfields.splice(drilldownfields.indexOf(jsonNameArr[0]), 1);

				if (sumaggregation.includes(jsonNameArr[0]))
					sumaggregation.splice(sumaggregation.indexOf(jsonNameArr[0]), 1);

				this.props.updateFormState(this.props.form, {
					[`config.drilldownfields`]: drilldownfields,
					[`config.sumaggregation`]: sumaggregation
				});
			}

			if (jsonNameArr[1] == 'resourcejson') {
				drilldownfields.push(destination.index, 0, jsonNameArr[0]);

				this.props.updateFormState(this.props.form, {
					[`config.drilldownfields`]: drilldownfields
				});
			}

			return true;
		}
		let fields = [...this.props.resource.config.fields],
			sumaggregation = [...this.props.resource.config.sumaggregation];

		if (source.droppableId === destination.droppableId && source.droppableId != 'column-1') {
			let data = fields[source.index];
			let sumaggdata = sumaggregation[source.index];

			fields.splice(source.index, 1);
			fields.splice(destination.index, 0, data);

			sumaggregation.splice(source.index, 1);
			sumaggregation.splice(destination.index, 0, sumaggdata);

			this.props.updateFormState(this.props.form, {
				[`config.fields`]: fields,
				[`config.sumaggregation`]: sumaggregation
			});
			return;
		}

		if (jsonNameArr[1] == 'pagejson') {
			fields.splice(fields.indexOf(jsonNameArr[0]), 1);

			if (sumaggregation.includes(jsonNameArr[0]))
				sumaggregation.splice(sumaggregation.indexOf(jsonNameArr[0]), 1);

			this.props.updateFormState(this.props.form, {
				[`config.fields`]: fields,
				[`config.sumaggregation`]: sumaggregation
			});
		}

		if (jsonNameArr[1] == 'resourcejson') {
			fields.splice(destination.index, 0, jsonNameArr[0]);

			this.props.updateFormState(this.props.form, {
				[`config.fields`]: fields
			});
		}
	};

	onclickDnd(data, isdrilldown) {
		if (!['pagejson', 'resourcejson'].includes(data.jsonname))
			return;

		if(isdrilldown) {
			let drilldownfields = [...this.props.resource.config.drilldownfields];

			drilldownfields.push(data.field);

			this.props.updateFormState(this.props.form, {
				[`config.drilldownfields`]: drilldownfields
			});
			return true;
		}

		let fields = [...this.props.resource.config.fields];
		fields.push(data.field);

		this.props.updateFormState(this.props.form, {
			[`config.fields`]: fields
		});
	}

	typeOnChange (value, ref) {
		let tempObj = {};

		if (ref) {
			if (value == 'Analytics')
				tempObj = {
					config: {
						analyticstype: null,
						fields: {},
						filter: [],
						aggregation: {}
					}
				};
			else if (value == 'Performance Target')
				tempObj = {
					config: {
						fields: {},
						parameterfilter: {},
						filter: []
					}
				};
			else
				tempObj = {
					config: {
						freezedcolumns: 0,
						fields: [],
						filter: [],
						sumaggregation: []
					}
				};
		} else {
			tempObj[`config.fields`] = {};
			tempObj[`config.filter`] = [];
			tempObj[`config.aggregation`] = {};
		}

		tempObj.enabledrilldown = false;
		tempObj[`config.drilldownfields`] = [];
		tempObj[`config.sumaggregation`] = [];

		this.props.updateFormState(this.props.form, tempObj);
	};

	measureOnChange (ref) {
		let tempObj = {};

		if (ref == 'measure1')
			tempObj[`config.aggregation.measure1`] = null;

		if (ref == 'measure2')
			tempObj[`config.aggregation.measure2`] = null;

		if (ref == 'measure3')
			tempObj[`config.aggregation.measure3`] = null;

		this.props.updateFormState(this.props.form, tempObj);
	};

	measurePTOnChange () {
		this.props.updateFormState(this.props.form, {
			[`config.fields.measure`]: null
		});
	};

	openDataset () {
		if (this.props.resource.datasetid)
			this.props.history.push(`/details/datasets/${this.props.resource.datasetid}`);
	}

	renderDragDropContext(filteredFields, columnsDisplayNameObj, isdrilldown) {
		const {app, resource} = this.props;
		const { searchresourcefield } = this.state;
		let fieldname = isdrilldown ? 'drilldownfields' : 'fields';

		return (
			<DragDropContext onDragEnd={(result)=> this.onDragEnd(result, isdrilldown)} >
				<div style={{width:"100%"}}>
					<div className = "list-view-body-column">
						<div className="list-search-column">
							<input type="text" className="form-control" value={searchresourcefield} onChange={(e)=>{this.setState({searchresourcefield: e.target.value})}} placeholder={'Search Column'}/>
						</div>
						<Droppable droppableId={'column-1'}>
							{(DroppableProvided, DroppableSnapshot) => (
								<div className="col-md-12 list-available-column" ref={DroppableProvided.innerRef}>
									<Listviewlistcontainer  jsonfieldarray={filteredFields}  searchterm={searchresourcefield} displayNameObj = {columnsDisplayNameObj} isreadable={resource.issystemview} rootapp={app} onClick = {(value) => {this.onclickDnd(value, isdrilldown)}}/>
									{DroppableProvided.placeholder}
								</div>
							)}
						</Droppable>
					</div>
					<div className="list-view-select-cloumns">
						<div className="col-md-12" style={{paddingRight:'10px'}}>
							<div className="list-select-column-title">SELECTED COLUMNS<span style={{marginLeft:'10px',fontSize:'12px',color:'#6eb5e2',fontWeight:'normal'}}>Drag here to select Cloumns</span></div>
							<Droppable droppableId={'column-2'}>
								{(DroppableProvided, DroppableSnapshot) => (
									<div ref={DroppableProvided.innerRef}>
										<div className="listcontainer-column">
										<Listviewlistcontainer  jsonfieldarray={resource.config[fieldname] && resource.config[fieldname].length >= 0 ? resource.config[fieldname].map((field) => {return {field, jsonname:'pagejson'}}) : []} displayNameObj = {columnsDisplayNameObj}  resourcename={'reports'} rootapp={app} isreadable={resource.issystemview} />
										</div>
									</div>
								)}
							</Droppable>
						</div>
					</div>
				</div>
			</DragDropContext>
		);
	}

	renderConfiguration(aggregationArray, filteredFields, measure1Options, measure2Options, measure3Options, imagePath, measurePTOptions, isdrilldown) {
		const { app, resource} = this.props;
		const { rolesArray, searchresourcefield, columnsArray, columnsDisplayNameObj, aggregateFieldsArray, FilterObj, measureTypes, measureOptions } = this.state;

		if(resource.type == 'Normal' || isdrilldown) {
			let fieldname = isdrilldown ? 'drilldownfields' : 'fields';
			return (<div className = 'col-md-12 col-sm-12 col-xs-12 displayeven'>
				<div className = 'card' style = {{marginBottom:'10px'}}>
					<div className = 'card-header listpage-title gs-uppercase'>Selected Columns To Be Displayed
					</div>
					<div className = 'card-body' style = {{paddingTop:'15px'}}>
						<div className = 'col-md-12 col-sm-12 col-xs-12' style={{paddingTop:'0px'}}>
							<div className = 'row'>
								{this.renderDragDropContext(filteredFields, columnsDisplayNameObj, isdrilldown)}
							</div>
							<div className = 'row margintop-30'>
								<div className = 'col-md-6 col-sm-12 col-xs-12'>
									<div className = 'row'>
										<div className = 'form-group col-md-9 col-sm-6'>
											<label><b>Enter number of columns to be freezed</b></label>
										</div>
										<div className = 'form-group col-md-3 col-sm-6'>
											<Field
												name = {`config.freezedcolumns`}
												props = {{
													required: false
												}}
												component = {NumberEle}
												validate = {[numberNewValidation({
													required: false
												})]}
											/>
										</div>
									</div>
								</div>
								<div className = 'col-md-6 col-sm-12 col-xs-12'>
									<div className = 'row'>
										<div className = 'form-group col-md-6 col-sm-6'>
											<label><b>Sum aggregation columns</b></label>
										</div>
										<div className = 'form-group col-md-6 col-sm-6'>
											<Field
												name = {`config.sumaggregation`}
												props = {{
													multiselect: true,
													options: [...aggregationArray],
													label: 'label',
													valuename: 'value'
												}}
												component = {localSelectEle}
											/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>);
		}
		if(resource.type == 'Analytics') {
			return (<div className = 'col-md-12'>
				<div className = 'card' style = {{marginBottom:'10px'}}>
					<div className = 'card-header listpage-title gs-uppercase'>Analytics Configuration</div>
					<div className = 'card-body'>
						<div className='row'>
							<div className = 'col-md-7 col-sm-12 col-xs-12'>
								<div className = 'row'>
									<div className = 'col-md-12'>
										<div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
												<label>Analytics Type</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6 btn-group-full-width'>
												<Field
													name = {`config.analyticstype`}
													props = {{
														buttons: [{
															title: 'Table',
															value: 'Table',
															icon: 'fa-table'
														}, {
															title: 'Bar',
															value: 'Bar Chart',
															icon: 'fa-bar-chart'
														}, {
															title: 'Line',
															value: 'Line Chart',
															icon: 'fa-line-chart'
														}, {
															title: 'Donut',
															value: 'Donut Chart',
															icon: 'fa-pie-chart'
														}, {
															title: 'KPI',
															value: 'KPI',
															icon: 'fa-key'
														}],
														onChange: (value) => this.typeOnChange(value, false),
														required: true
													}}
													component = {ButtongroupEle}
													//validate = {[]}
													validate = {[stringNewValidation({
														required:  true,
														title : 'Analytics Type'
													})]}
												/>
											</div>
										</div>
										{ resource.config.analyticstype != 'KPI' ? <div className = 'row'>
											<div className = 'form-group s col-md-3 col-sm-6'>
												<label>{resource.config.analyticstype == 'Table' ? 'Column 1' : 'Axis'}</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.fields.axis1`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: resource.config.analyticstype != 'Table' ? true : false,
														options: [...this.state.columnsArray]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: "{resource.config.analyticstype != 'Table'}"
													})]}
												/>
											</div>
										</div> : null }
										{ resource.config.analyticstype == 'Table' ? <div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
												<label>Column 2</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.fields.axis2`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: false,
														options: [...this.state.columnsArray]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: false
													})]}
												/>
											</div>
										</div> : null}
										{ !['Donut Chart', 'KPI'].includes(resource.config.analyticstype) ? <div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
											<label>{resource.config.analyticstype == 'Table' ? 'Row 1' : 'Series'}</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.fields.series1`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: false,
														options: [...this.state.columnsArray]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: false
													})]}
												/>
											</div>
										</div> : null}
										{ resource.config.analyticstype == 'Table' ? <div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
												<label>Row 2</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.fields.series2`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: false,
														options: [...this.state.columnsArray]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: false
													})]}
												/>
											</div>
										</div> : null}
										<div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
												<label>Measure Type {resource.config.analyticstype == 'Table' ? '1' : ''}</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.aggregation.measuretype1`}
													props = {{
														required: true,
														onChange: (value) => this.measureOnChange('measure1'),
														options: [...measureTypes]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: true
													})]}
												/>
											</div>
										</div>
										{resource.config.aggregation && resource.config.aggregation.measuretype1 && resource.config.aggregation.measuretype1 != 'COUNT' ? <div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
												<label>Measure {resource.config.analyticstype == 'Table' ? '1' : ''}</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.aggregation.measure1`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: true,
														options: [...measure1Options]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: true
													})]}
												/>
											</div>
										</div> : null}
										{ resource.config.analyticstype == 'Table' ? <div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
												<label>Measure Type 2</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.aggregation.measuretype2`}
													props = {{
														required: false,
														onChange: (value) => this.measureOnChange('measure2'),
														options: [...measureTypes]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: false
													})]}
												/>
											</div>
										</div> : null}
										{resource.config.aggregation && resource.config.analyticstype == 'Table' && resource.config.aggregation.measuretype2 && resource.config.aggregation.measuretype2 != 'COUNT' ? <div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
												<label>Measure 2</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.aggregation.measure2`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: true,
														options: [...measure2Options]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: true
													})]}
												/>
											</div>
										</div> : null}

										{ resource.config.analyticstype == 'Table' ? <div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
												<label>Measure Type 3</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.aggregation.measuretype3`}
													props = {{
														required: false,
														onChange: (value) => this.measureOnChange('measure3'),
														options: [...measureTypes]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: false
													})]}
												/>
											</div>
										</div> : null}
										{resource.config.aggregation && resource.config.analyticstype == 'Table' && resource.config.aggregation.measuretype3 && resource.config.aggregation.measuretype3 != 'COUNT' ? <div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
												<label>Measure 3</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.aggregation.measure3`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: true,
														options: [...measure3Options]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: true
													})]}
												/>
											</div>
										</div> : null}
										{resource.config.analyticstype == 'Donut Chart' ? <div className = 'row'>
											<div className = 'form-group col-md-3 col-sm-6'>
												<label>Value Format</label>
											</div>
											<div className = 'form-group col-md-9 col-sm-6'>
												<Field
													name = {`config.donut_valuetype`}
													props = {{
														required: true,
														options: [{id: 'value', name: 'Value'}, {id: 'percentage', name: 'Percentage'}, {id: 'valueandpercentage', name: 'Value & Percentage'}]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: true
													})]}
												/>
											</div>
										</div> : null}
									</div>
								</div>
							</div>
							{resource.config.analyticstype ? <div className = 'col-md-5 col-sm-12 col-xs-12'>
								<div className = 'text-center'>
									<img src={`../images/${imagePath}`} style = {{width: '80%', height: '80%'}}/>
								</div>
							</div> : null}
						</div>
					</div>
				</div>
			</div>);
		}
		if(resource.type == 'Performance Target') {
			return (<div className = 'col-md-12'>
				<div className = 'card' style = {{marginBottom:'10px'}}>
					<div className = 'card-header listpage-title gs-uppercase'>Analytics Configuration</div>
					<div className = 'card-body'>
						<div className='row'>
							<div className = 'col-md-7 col-sm-12 col-xs-12'>
								<div className = 'row'>
									<div className = 'col-md-12'>
										<div className = 'row'>
											<div className = 'form-group s col-md-4 col-sm-6'>
												<label>Period</label>
											</div>
											<div className = 'form-group col-md-8 col-sm-6'>
												<Field
													name = {`config.fields.period`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: true,
														options: [...this.state.columnsArray]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: true
													})]}
												/>
											</div>
										</div>
										<div className = 'row'>
											<div className = 'form-group col-md-4 col-sm-6'>
												<label>Periodicity</label>
											</div>
											<div className = 'form-group col-md-8 col-sm-6'>
												<Field
													name = {`config.fields.periodicity`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: true,
														options: [...this.state.columnsArray]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: true
													})]}
												/>
											</div>
										</div>
										<div className = 'row'>
											<div className = 'form-group s col-md-4 col-sm-6'>
												<label>User</label>
											</div>
											<div className = 'form-group col-md-8 col-sm-6'>
												<Field
													name = {`config.fields.user`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: true,
														options: [...this.state.columnsArray]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: true
													})]}
												/>
											</div>
										</div>
										<div className = 'row'>
											<div className = 'form-group col-md-4 col-sm-6'>
												<label>Measure Type</label>
											</div>
											<div className = 'form-group col-md-8 col-sm-6'>
												<Field
													name = {`config.fields.measuretype`}
													props = {{
														required: true,
														onChange: (value) => this.measurePTOnChange('measure1'),
														options: [...measureTypes]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: true
													})]}
												/>
											</div>
										</div>
										{resource.config && resource.config.fields && resource.config.fields.measuretype && resource.config.fields.measuretype != 'COUNT' ? <div className = 'row'>
											<div className = 'form-group col-md-4 col-sm-6'>
												<label>Measure</label>
											</div>
											<div className = 'form-group col-md-8 col-sm-6'>
												<Field
													name = {`config.fields.measure`}
													props = {{
														valuename: 'field',
														label: 'displayName',
														required: true,
														options: [...measurePTOptions]
													}}
													component = {localSelectEle}
													validate = {[stringNewValidation({
														required: true
													})]}
												/>
											</div>
										</div> : null}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>);
		}
	}

	renderDefinedParameters() {
		if(this.props.resource.type != 'Performance Target' || this.state.definedParameters.length == 0)
			return null;

		const { app, resource} = this.props;
		const { rolesArray, searchresourcefield, columnsArray, columnsDisplayNameObj, aggregateFieldsArray, FilterObj, measureTypes, measureOptions } = this.state;

		let definedParamFilters = {
			fromdate: {
				field: 'fromdate',
				displayName: 'From Date',
				required: true,
				disabled: false,
				placeholder: 'From Date'
			},
			todate: {
				field: 'todate',
				displayName: 'To Date',
				required: true,
				disabled: false,
				placeholder: 'To Date'
			},
			stockbasedon: {
				field: 'stockbasedon',
				displayName: 'Stock Based On',
				required: true,
				disabled: false,
				options: [{
					id: 'transactiondate',
					name: 'Transaction Date'
				}, {
					id: 'postingdate',
					name: 'Posting Date'
				}],
				placeholder: 'Stock Based On'
			},
			asondate: {
				field: 'asondate',
				displayName: 'As on Date',
				required: true,
				disabled: false,
				placeholder: 'As on Date'
			},
			companyid: {
				field: 'companyid',
				displayName: 'Company',
				required: true,
				disabled: false,
				placeholder: 'Company',
				resource: `companymaster`,
				fields: `id,name,defaultcompany`,
				label: `name`
			},
			itemid: {
				field: 'itemid',
				displayName: 'Item',
				required: true,
				disabled: false,
				placeholder: 'Item',
				resource: `itemmaster`,
				fields: `id,name,displayname`,
				displaylabel: `name`,
				label: `displayname`
			},
			warehouseid: {
				field: 'warehouseid',
				displayName: 'Warehouse',
				required: true,
				disabled: false,
				placeholder: 'Warehouse',
				resource: `stocklocations`,
				fields: `id,name`,
				label: `name`,
				filter: `stocklocations.parentid IS NULL`
			}
		};

		return(<div className = 'col-md-12'>
			<div className = 'card' style={{marginBottom:'10px'}}>
				<div className = 'card-header listpage-title gs-uppercase'>Defined Parameters</div>
				<div className = 'card-body'>
					<div className='row responsive-form-element'>
						{this.state.definedParameters.map((filter, key) => {
							if (filter == 'companyid')
								return null;

							return (
								<div className="form-group col-sm-6 col-md-6" key={key} >
									<label className="labelclass">{definedParamFilters[filter].displayName}</label>
									{ filter == 'fromdate' ? <Field
										name = {`config.parameterfilter.fromdate`}
										props = {{
											required: definedParamFilters[filter].required
										}}
										component = {DateEle}
										validate = {[dateNewValidation({
											required: definedParamFilters[filter].required,
											title: definedParamFilters[filter].displayName
										})]}
									/> : null }
									{ filter == 'todate' ? <Field
										name = {`config.parameterfilter.todate`}
										props = {{
											required: definedParamFilters[filter].required,
											min: this.props.resource.config && this.props.resource.config.parameterfilter ? this.props.resource.config.parameterfilter.fromdate : null
										}}
										component = {DateEle}
										validate = {[dateNewValidation({
											required: definedParamFilters[filter].required,
											title : definedParamFilters[filter].displayName,
											min: '{resource.config.parameterfilter.fromdate}'
										})]}
									/> : null }
									{ filter == 'asondate' ? <Field
										name = {`config.parameterfilter.asondate`}
										props = {{
											required: definedParamFilters[filter].required
										}}
										component = {DateEle}
										validate = {[dateNewValidation({
											required: definedParamFilters[filter].required,
											title : definedParamFilters[filter].displayName
										})]}
									/> : null }
									{filter == 'stockbasedon' ? <Field
										name = {`config.parameterfilter.stockbasedon`}
										props = {{
											options: [...definedParamFilters[filter].options],
										}}
										component = {localSelectEle}
										validate = {[stringNewValidation({
											required: definedParamFilters[filter].required,
											model: 'stockbasedon'
										})]}
									/> : null }
									{filter == 'itemid' ? <Field
										name = {`config.parameterfilter.itemid`}
										props = {{
											resource: definedParamFilters[filter].resource,
											fields: definedParamFilters[filter].fields,
											label: definedParamFilters[filter].label,
											required : definedParamFilters[filter].required,
											onChange: (value, valueObj) => this.parameterFilterOnChange('itemid', value, valueObj)
										}}
										component = {autoSelectEle}
										validate = {[numberNewValidation({
											required: definedParamFilters[filter].required,
											model: 'itemid'
										})]}
									/> : null }
									{filter == 'warehouseid' ? <Field
										name = {`config.parameterfilter.warehouseid`}
										props = {{
											resource: definedParamFilters[filter].resource,
											fields: definedParamFilters[filter].fields,
											label: definedParamFilters[filter].label,
											filter: definedParamFilters[filter].filter,
											required : definedParamFilters[filter].required,
											onChange: (value, valueObj) => this.parameterFilterOnChange('warehouseid', value, valueObj)
										}}
										component = {autoMultiSelectEle}
										validate = {[multiSelectNewValidation({
											required: definedParamFilters[filter].required,
											model: 'warehouseid'
										})]}
									/> : null }
									{false && filter == 'companyid' ? <Field
										name = {`config.parameterfilter.companyid`}
										props = {{
											resource: "companymaster",
											fields: "id,name,defaultcompany",
											label: 'name',
											required : definedParamFilters[filter].required,
										}}
										component = {autoSelectEle}
										validate = {[numberNewValidation({
											required: definedParamFilters[filter].required,
											model: 'companyid'
										})]}
									/> : null }
								</div>
							);
						})}
					</div>
				</div>
			</div>
		</div>);
	}

	render() {
		const { app, resource} = this.props;
		const { rolesArray, searchresourcefield, columnsArray, columnsDisplayNameObj, aggregateFieldsArray, FilterObj, measureTypes, measureOptions } = this.state;

		let aggregationArray = [],
			filteredFields = [...columnsArray],
			drilldownFilteredfields = [...columnsArray],
			measure1Options = [],
			measure2Options = [],
			measure3Options = [],
			measurePTOptions = [],
			imagePath = '';

		if (!resource)
			return null; 

		if (resource.type == 'Normal' && resource.datasetid) {
			if (resource.config.fields && resource.config.fields.length >= 0) {
				filteredFields = columnsArray.filter((field) => {
					return resource.config.fields.indexOf(field.field) == -1 ? true : false
				});

				columnsArray.forEach((item) => {
					if (resource.config.fields.indexOf(item.field) >= 0 && item.type == 'integer' && !item.isForeignKey)
						aggregationArray.push({value: item.field, label: columnsDisplayNameObj[item.field]});
				});
			}
		} else {
			drilldownFilteredfields = columnsArray.filter((field) => {
				return (resource.config.drilldownfields || []).indexOf(field.field) == -1 ? true : false
			});

			columnsArray.forEach((item) => {
				if ((resource.config.drilldownfields || []).indexOf(item.field) >= 0 && item.type == 'integer' && !item.isForeignKey)
					aggregationArray.push({value: item.field, label: columnsDisplayNameObj[item.field]});
			});
		}

		if ((resource.type == 'Analytics') && resource.datasetid) {
			measure1Options = [];
			measure2Options = [];
			measure3Options = [];

			if (resource.config.aggregation && resource.config.aggregation.measuretype1 && aggregateFieldsArray && aggregateFieldsArray.length > 0) {
				let mtype = resource.config.aggregation.measuretype1;

				let mOptions = resource.config.analyticstype != 'Table' ? ['integer'] : measureOptions[mtype];

				measure1Options = aggregateFieldsArray.filter((item) => {
					return (mOptions.includes(item.type)) ? true : false;
				});
			}

			if (resource.config.aggregation && resource.config.aggregation.measuretype2 && aggregateFieldsArray && aggregateFieldsArray.length > 0) {
				let mtype = resource.config.aggregation.measuretype2;

				measure2Options = aggregateFieldsArray.filter((item) => {
					return (measureOptions[mtype].includes(item.type)) ? true : false;
				});
			}

			if (resource.config.aggregation && resource.config.aggregation.measuretype3 && aggregateFieldsArray && aggregateFieldsArray.length > 0) {
				let mtype = resource.config.aggregation.measuretype3;

				measure3Options = aggregateFieldsArray.filter((item) => {
					return (measureOptions[mtype].includes(item.type)) ? true : false;
				});
			}

			if (resource.config.analyticstype == 'Bar Chart')
				imagePath = 'Layout_barchart.png';
			else if (resource.config.analyticstype == 'Line Chart')
				imagePath = 'Layout_linechart.png';
			else if (resource.config.analyticstype == 'Donut Chart')
				imagePath = 'Layout_donutchart.png';
			else if (resource.config.analyticstype == 'KPI')
				imagePath = 'Layout_kpi.png';
			else
				imagePath = 'Layout_table.png';
		}

		if(resource.type == 'Performance Target' && resource.datasetid) {
			if (resource.config && resource.config.fields && resource.config.fields.measuretype && aggregateFieldsArray && aggregateFieldsArray.length > 0) {
				let mtype = resource.config.fields.measuretype;
				measurePTOptions = aggregateFieldsArray.filter((item) => {
					return (measureOptions[mtype].includes(item.type)) ? true : false;
				});
			}
		}

		let dataSetFlag = resource && resource.datasetid && columnsArray.length > 0;

		return (
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<Prompt when={!this.state.createParam && this.props.dirty && this.props.anyTouched} message={`Unsaved Changes`} />
				<div className="row">
					<div className="col-sm-12 col-md-12 col-lg-9" style={{position: 'fixed', height: '45px', backgroundColor: '#FAFAFA',  zIndex: 5}}>
						<div className="row">
							<div className="col-md-6 col-sm-12 paddingleft-md-30">
								<h6 className="margintop-15 semi-bold-custom">
									<a className="affixanchor float-left marginright-10" onClick={()=>{this.props.history.goBack()}}>
										<span className="fa fa-chevron-left"></span>
									</a> {!resource.id ? `New Report` : `REPORT: ${resource.name}`}
								</h6>
							</div>
							{this.props.resource.id ?  <div className="col-md-6 col-sm-12  margintop-6 text-right-md">
								<div className="row">
									<div className="col-md-12 col-sm-12  text-right-md">
										<div className="btn btn-sm affixmenu-btn" onClick={this.copyfunction}><i className="fa fa-files-o"></i>Copy</div>
									</div>
								</div>
							</div> : null}
						</div>
					</div>
				</div>
				<form>
					<div className = 'row'>
						<div className = 'col-sm-12 col-md-12 col-lg-9'>
							<div className = 'row' style={{marginTop:'50px'}}>
								<div className = 'col-md-12'>
									<div className = 'card' style={{marginBottom:'10px'}}>
										<div className = 'card-header listpage-title gs-uppercase'>Basic Information</div>
										<div className = 'card-body'>
											<div className = 'row'>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Report Name</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'name'}
																props = {{
																	required: true
																}}
																component = {InputEle}
																validate = {[stringNewValidation({
																	required: true,
																	title : 'Report Name'
																})]}
															/>
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Report Type</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'type'}
																props = {{
																	options: ['Normal', 'Analytics','Performance Target'],
																	onChange: (value) => this.typeOnChange(value, true),
																	required: true
																}}
																component = {localSelectEle}
																validate = {[stringNewValidation({
																	required:  true,
																	title : 'Report Type'
																})]} />
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Data Set</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<div className = 'input-group'>
																<div style={{flex: '1'}}>
																<Field
																	name = {'datasetid'}
																	props = {{
																		resource: 'datasets',
																		fields: 'id,name',
																		onChange: () => {this.getColumns()}
																	}}
																	component = {autoSelectEle}
																	validate = {[numberNewValidation({
																		required: true,
																		title : 'Data Set'
																	})]}
																/>
																</div>
																<span className = 'input-group-append'>
																	<button
																		type = 'button'
																		className = 'btn btn-sm gs-form-btn-success ndtclass'
																		title = 'Open Data Set'
																		disabled = {!this.props.resource.datasetid}
																		onClick = {() => this.openDataset()}
																	>
																		<span className = 'fa fa-external-link'></span>
																	</button>
																</span>
															</div>
														</div>
													</div>
												</div>
												{this.props.resource.type != 'Performance Target' ? <div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Module</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'module'}
																props = {{
																	options: ['Sales', 'Project', 'Purchase', 'Stock', 'Accounts', 'Service', 'Production', 'HR', 'Admin'],
																	required: true
																}}
																component = {localSelectEle}
																validate = {[stringNewValidation({
																	required: true,
																	title : 'Module'
																})]} />
														</div>
													</div>
												</div> : null}
												{this.props.resource.type != 'Performance Target' ? <div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Folder</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'foldername'}
																props = {{
																	resource : 'reports',
																	field : 'foldername'
																}}
																component = {autosuggestEle}
																validate = {[stringNewValidation({
																	required: true,
																	title : 'Folder'
																})]}/>
														</div>
													</div>
												</div> : null}
												{this.props.resource.type != 'Performance Target' ? <div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Allowed Roles</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'allowedroles'}
																props = {{
																	multiselect: true,
																	options: [...rolesArray],
																	required: true
																}}
																component = {localSelectEle}
																validate = {[multiSelectNewValidation({
																	required: true,
																	title : 'Allowed Roles'
																})]}
															/>
														</div>
													</div>
												</div> : null}
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Remarks</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'remarks'}
																component = {textareaEle}
															/>
														</div>
													</div>
												</div>
												{this.props.resource.type != 'Performance Target' ? <div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Allow Excel Export</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'hasexcelexport'}
																component = {checkboxEle}
															/>
														</div>
													</div>
												</div> : null}
												{this.props.resource.type != 'Performance Target' ? <div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Display Order</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'displayorder'}
																props = {{
																	required: true
																}}
																component = {NumberEle}
																validate = {[numberNewValidation({
																	required: true
																})]}
															/>
														</div>
													</div>
												</div> : null}
												{ resource.id ? <div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Active</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'isactive'}
																component = {checkboxEle}
															/>
														</div>
													</div>
												</div> : null}
												{this.props.resource.type == 'Analytics' ? <div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Enable Drill Down</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'enabledrilldown'}
																props = {{
																	onChange: () => {
																		this.props.updateFormState(this.props.form, {
																			[`config.drilldownfields`]: [],
																			[`config.sumaggregation`]: []
																		});
																	}
																}}
																component = {checkboxEle}
															/>
														</div>
													</div>
												</div> : null}
											</div>
										</div>
									</div>
								</div>
								{dataSetFlag ? this.renderConfiguration(aggregationArray, filteredFields, measure1Options, measure2Options, measure3Options, imagePath, measurePTOptions) : null}
								{dataSetFlag && resource.type == 'Analytics' && resource.enabledrilldown ? this.renderConfiguration(aggregationArray, drilldownFilteredfields, measure1Options, measure2Options, measure3Options, imagePath, measurePTOptions, true) : null}
								{dataSetFlag ? this.renderDefinedParameters() : null}
								{ dataSetFlag ? <div className = 'col-md-12'>
									<div className = 'card' style={{marginBottom:'3rem'}}>
										<div className = 'card-header listpage-title gs-uppercase'>Filter Conditions</div>
										<div className = 'card-body'>
											<div className='row responsive-form-element'>
												<FieldArray
													name = {`config.filter`}
													resource = {this.props.resource}
													array = {this.props.array}
													app = {this.props.app}
													updateFormState = {this.props.updateFormState}
													form = {this.props.form}
													component = {ReportFilterComponent}
													resourceArray = {this.state.resourceArray}
													FilterArray = {this.state.FilterArray}
													FilterObj = {this.state.FilterObj}/>
											</div>
										</div>
									</div>
								</div> : null}
							</div>
						</div>
						<div className={`col-sm-12 ndt-section col-md-12 col-lg-3 paddingleft-md-0 margintop-5 `} >
							{this.props.resource && this.props.resource.id && this.state.ndt ? <AdditionalInformationSection ndt={this.state.ndt} key={0} parentresource={'reports'} openModal={this.props.openModal} parentid={this.props.resource.id} relatedpath={this.props.match.path} createOrEdit = {this.props.createOrEdit} app ={this.props.app} history={this.props.history} /> : null }
						</div>
					</div>
					<div className = 'row'>
						<div className = 'col-sm-12 col-md-12 col-lg-9'>
							<div className = 'muted credit text-center sticky-footer actionbtn-bar' style={{boxShadow:'none'}}>
								<button
									type = 'button'
									className = 'btn btn-sm btn-width btn-secondary'
									onClick={() => this.props.history.goBack()}>
									<i className = 'fa fa-times marginright-5'></i>Close
								</button>
								{checkActionVerbAccess(this.props.app, 'reports', 'Save') ? <button
									type = 'button'
									className = 'btn btn-width btn-sm gs-btn-success'
									disabled = {this.props.invalid}
									onClick = {()=>{this.validateReport('Save');}}>
									<i className = 'fa fa-save'></i>Save
								</button> : null}
								{resource.id && resource.type != 'Performance Target' && resource.config.analyticstype != 'KPI' ? <button
									type = 'button'
									className = 'btn btn-width btn-sm gs-btn-primary'
									onClick = {() => {this.props.history.push(`/reportbuilder/${this.props.resource.id}`)}}
									disabled = {this.props.invalid}>
									<i className = 'fa fa-list'></i>View Report
								</button> : null}
								{resource.id && checkActionVerbAccess(this.props.app, 'reports', 'Delete') && (resource.createdby == this.props.app.user.id || this.props.app.user.roleid.indexOf(1) >= 0) ? <button
									type = 'button'
									className = 'btn btn-width btn-sm gs-btn-danger'
									onClick = {() => this.save('Delete')}
									disabled = {this.props.invalid}>
									<i className = 'fa fa-trash-o'></i>Delete
								</button> : null}
							</div>
 						</div>
					</div>
				</form>
			</>
		);
	}
}

class CopyModal extends Component {
	constructor(props) {
		super(props);
		this.copyrecord = this.copyrecord.bind(this);
	}

	copyrecord() {
		this.props.callback();
		this.props.closeModal();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Copy Record</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							<p>Do you want to copy this record?</p>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm gs-btn-warning btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Cancel</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.copyrecord}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

ReportBuilder = connect(
	(state, props) => {
		let formName = props.match.params.id > 0 ? `Report_${props.match.params.id}` : 'Report_create';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			touchOnChange: true,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			reportbuilderjson : state.reportbuilderjson,
			reportdata: state.reportfilter,
			fullstate : state
		}
	}, { updateFormState, resetReportFilterByReport, updateReportBuilderJSONState }
)(reduxForm()(withDragDropContext(ReportBuilder)));

export default ReportBuilder;
