import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		country : this.props.app.feature.Country
	};

	if(this.props.location.params) {
		let params = this.props.location.params;
		if(params.parentresource == 'partners')
			tempObj.gstregtype = params.gstregtype;
		tempObj.parentresource = params.parentresource;
		tempObj.parentid = params.parentid;
	}
	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/addresses/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function countryChange(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		countryid:  valueobj.id,
		countryid_name:  valueobj.name,
		stateid: null,
		cityid: null,
		areaid: null,
		streetid: null
	});
}

export function stateChange(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		stateid:  valueobj.id,
		stateid_name:  valueobj.name,
		cityid: null,
		areaid: null,
		streetid: null
	});
}

export function cityChange(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		cityid:  valueobj.id,
		cityid_name:  valueobj.name,
		areaid: null,
		streetid: null
	});
}

export function areaChange(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		areaid:  valueobj.id,
		areaid_name:  valueobj.name,
		areaid_pincode: valueobj.pincode,
		streetid: null
	});
}

export function streetChange(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		streetid:  valueobj.id,
		streetid_name:  valueobj.name
	});
}

export function getDisplayAddress () {
	let addressData = this.props.app.feature.displayAddressFormat || "{{First Line}},\n{{Second Line}}, {{City}},\n{{State}}, {{Postal Code}}";
	let placeholderMap = {
		'firstline' : 'First Line'
	};

	if(this.props.app.feature.useMasterForAddresses) {
		placeholderMap.areaid_pincode = 'Postal Code';
		placeholderMap.cityid_name = 'City';
		placeholderMap.streetid_name = 'Second Line';
		placeholderMap.areaid_name = 'Area';
		placeholderMap.stateid_name = 'State';
		placeholderMap.countryid_name = 'Country';
	} else {
		placeholderMap.postalcode = 'Postal Code';
		placeholderMap.secondline = 'Second Line';
		placeholderMap.city = 'City';
		placeholderMap.state = 'State';
		placeholderMap.country = 'Country';
	}

	for (var prop in placeholderMap) {
		if (this.props.resource[prop])
			addressData = addressData.replace(new RegExp('{{' + placeholderMap[prop] + '}}', "g"), this.props.resource[prop]);
		else
			addressData = addressData.replace(new RegExp('{{' + placeholderMap[prop] + '}},', "g"), '').replace(new RegExp('{{' + placeholderMap[prop] + '}}', "g"), '');
	}

	this.props.updateFormState(this.props.form, {
		displayaddress: addressData
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	axios({
		method : 'POST',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/addresses'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if (this.props.isModal) {
				this.props.closeModal();
				this.props.callback(response.data.main);
			} else {
				if (this.props.isModal)
					this.props.closeModal();
				else {
					this.props.initialize(response.data.main);
					setTimeout(this.props.history.goBack, 0);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	if (this.props.isModal) {
		this.props.closeModal();
	} else{
		this.props.history.goBack();
	}
}
