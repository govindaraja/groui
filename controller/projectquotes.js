import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { commonMethods, detailsSVC, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import { ItemRateField } from '../components/utilcomponents';
import PotentialrevenueupdateModal from '../components/details/potentialrevenueupdatemodal';
import EmailModal from '../components/details/emailmodal';
import KititemaddModal from '../components/details/kititemaddmodal';
import ProjectQuotationProfitAnalysisModal from '../components/details/projectquoteprofitanalysismodal';
import ProjectEstimationCostBreakupOverviewDetails from '../components/details/projectestimationcostbreakupoverviewdetails';
import BOQGridEditModal from '../components/details/importexportprojectcomponent';
import ProjectMarkupModal from '../components/details/projectmarkupmodal';

export function onLoad () {
	let restrictestimationcommercials = false;
	this.props.app.feature.restrictprojectestimationrole.some((restrictrole) => {
		if(this.props.app.user.roleid.indexOf(restrictrole) > -1) {
			restrictestimationcommercials = true;
			return true;
		}
	});

	this.setState({ restrictestimationcommercials });

	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		companyid: this.props.app.selectedcompanyid,
		quotedate: new Date(),
		projectquoteitems: [],
		kititemprojectquotedetails: [],
		additionalcharges: [],
		milestoneitems: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;
		let leaditemArray = params.leaditems;

		if(params.param == 'copy')
			tempObj = {...tempObj, ...params};

		if(params.param == 'Leads') {
			leaditemArray.forEach((item, i) => {
				let uomName = "";

				if (this.props.app.uomObj[item.uomid])
					uomName = this.props.app.uomObj[item.uomid].name;

				let tempChildObj = {
					index: i+1,
					itemtype: 'Item',
					alternateuom: item.uomconversiontype ? true : false,
					uomid_name : uomName
				};

				utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_itemtype', 'itemnamevariationid', 'itemnamevariationid_name', 'description', 'drawingno', 'rate', 'splitrate', 'labourrate', 'materialrate', 'taxid', 'labourtaxid', 'materialtaxid', 'itemid_uomgroupid', 'quantity', 'uomid', 'discountmode', 'discountquantity', 'itemid_keepstock', 'itemid_hasaddons', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', {'name' : 'itemid_name'}, 'itemid_itemcategorymasterid', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingquantity', 'billingconversiontype', 'billingconversionfactor', 'billingrate']);

				this.customFieldsOperation('leaditems', tempChildObj, item, 'projectquoteitems');
				tempObj.projectquoteitems.push(tempChildObj);
			});

			utils.assign(tempObj, params, [{'leadid' : 'id'}, 'currencyid', 'currencyexchangerate', {'dueon' : 'duedate'}, 'contactid', 'companyid', 'email', 'mobile', 'phone', 'deliveryaddress', 'deliveryaddressid', 'deliverydate', 'customerreference', 'billingaddress', 'billingaddressid', 'salesperson', 'priority', {'expectedclosuredate' : 'duedate'}, 'territoryid', 'taxid', {'leadid_subject' : 'subject'}, {'projectname' : 'subject'}, 'roundoffmethod', 'labourtaxid', 'materialtaxid', {'leadid_potentialrevenue' : 'potentialrevenue'}, {'leadid_currencyid' : 'currencyid'}]);

			if (params.customerid > 0) {
				tempObj.customerid = params.customerid;
				tempObj.customerid_name = params.customerid_name;
				tempObj.pricelistid = params.pricelistid;
				tempObj.creditperiod = params.customerid_salescreditperiod;
				tempObj.territoryid = params.territoryid;
			}

			this.customFieldsOperation('leads', tempObj, params, 'projectquotes');
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.getTandC();
		this.controller.callbackCustomerDetails();
		this.controller.currencyOnChange();
		if(this.props.location.params && this.props.location.params.param == 'Leads')
			this.controller.onLoadKit(0, []);
		this.controller.computeFinalRate();
	}, 0);
	this.updateLoaderFlag(false);
}

export function onLoadKit (count, kititemprojectquotedetails) {
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;
	if(count < this.props.resource.projectquoteitems.length) {
		if(this.props.resource.projectquoteitems[count].itemid_issaleskit) {
			let promise1 = commonMethods.getItemDetails(this.props.resource.projectquoteitems[count].itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales');
			promise1.then((returnObject) => {
				returnObject.kititems.forEach((kititem) => {
					kititem.rootindex = this.props.resource.projectquoteitems[count].index;
					if(!kititem.parentindex) {
						let tempQuantity = Number((kititem.conversion * this.props.resource.projectquoteitems[count].quantity).toFixed(roundOffPrecisionStock));
						calculateQuantity(kititem.index, tempQuantity);
						kititem.quantity = tempQuantity;
					}
				});
				function calculateQuantity(tempIndex, tempQuantity) {
					returnObject.kititems.forEach((kititem) => {
						if (kititem.parentindex == tempIndex) {
							let sectempQty = Number((kititem.conversion * tempQuantity).toFixed(roundOffPrecisionStock));
							kititem.quantity = sectempQty;
							calculateQuantity(kititem.index, sectempQty);
						}
					});
				}
				returnObject.kititems.forEach((kititem) => {
					kititemprojectquotedetails.push(kititem);
				});
				this.controller.onLoadKit(count+1, kititemprojectquotedetails);
			}, (reason)=> {});
		} else {
			this.controller.onLoadKit(count+1, kititemprojectquotedetails);
		}
	} else {
		this.props.updateFormState(this.props.form, {
			kititemprojectquotedetails
		});
		this.controller.computeFinalRate();
	}
}

export function getItemById () {
	axios.get(`/api/projectquotes/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = {
				...response.data.main,
				tempfinaltotal : response.data.main.finaltotal
			};

			tempObj.projectquoteitems.forEach((item) => {
				if (item.uomconversiontype)
					item.alternateuom = true;
			});

			this.props.initialize(tempObj);

			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Project Quotation' and termsandconditions.isdefault`).then((response) => {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0)
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function tandcOnchange(id, valueobj) {
	if(id) {
		this.props.updateFormState(this.props.form, {
			tandc: valueobj.terms
		});
	}
}

export function currencyOnChange () {
	if (this.props.resource.pricelistid)
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid)
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});

	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function itemTypeOnChange (item, itemstr) {
	let tempObj = {};

	for(let prop in item) {
		if(['itemid','quantity','rate','uomid','finalrate'].indexOf(prop) >= 0)
			tempObj[`${itemstr}.${prop}`] = null;
	}

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function callbackCustomerDetails() {
	if(this.props.resource.customerid > 0) {
		return axios.get(`/api/partners?field=id,paymentterms,modeofdispatch,freight,customergroups/creditperiod/partnergroupid&filtercondition=partners.iscustomer=true and partners.id=${this.props.resource.customerid}`).then((response) => {
			if (response.data.main.length > 0)
				this.props.updateFormState(this.props.form, {
					paymentterms : response.data.main[0].paymentterms,
					modeofdespatch : response.data.main[0].modeofdispatch,
					freight : response.data.main[0].freight,
					creditperiod : this.props.resource.creditperiod ? this.props.resource.creditperiod : response.data.main[0].partnergroupid_creditperiod
				});
		});
	}
}

export function estimationdetailedcallback(index) {
	return this.openModal({
		render : (closeModal) => {
			return <ProjectEstimationCostBreakupOverviewDetails resourceitems={this.props.resource.projectquoteitems} index={index} resourcename = {'projectquotes'} closeModal={closeModal} app={this.props.app} />
		}, className: {
			content : 'react-modal-custom-class-80',
			overlay : 'react-modal-overlay-custom-class'
		}
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.itemid_uomgroupid`] : valueobj.uomgroupid
	});

	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.itemid`] : valueobj.parentid
	});

	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales');

	promise.then((returnObject) => {
		let tempObj = {
			[`${itemstr}.itemid_name`] : returnObject.itemid_name,
			[`${itemstr}.description`] : returnObject.description,
			[`${itemstr}.itemid_issaleskit`] : returnObject.itemid_issaleskit,
			[`${itemstr}.itemid_itemtype`] : returnObject.itemid_itemtype,
			[`${itemstr}.rate`] : 0,
			[`${itemstr}.labourrate`] : 0,
			[`${itemstr}.materialrate`] : 0,
			[`${itemstr}.quantity`] : item.quantity ? item.quantity : 1,
			[`${itemstr}.uomid`] : returnObject.uomid,
			[`${itemstr}.uomid_name`] : returnObject.uomid_name,
			[`${itemstr}.alternateuom`] : returnObject.alternateuom,
			[`${itemstr}.itemid_uomgroupid`] : returnObject.uomgroupid,
			[`${itemstr}.splitrate`] : returnObject.splitrate,
			[`${itemstr}.taxid`] : (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid,
			[`${itemstr}.itemnamevariationid`] : returnObject.itemnamevariationid,
			[`${itemstr}.itemnamevariationid_name`] : returnObject.itemnamevariationid_name,
			[`${itemstr}.itemid_keepstock`] : returnObject.keepstock,
			[`${itemstr}.itemid_imageurl`] : returnObject.itemid_imageurl,
			[`${itemstr}.itemid_hasaddons`] : returnObject.hasaddons,
			[`${itemstr}.itemid_addonhelptext`] : returnObject.addonhelptext,
			[`${itemstr}.itemid_itemcategorymasterid`] : returnObject.itemcategorymasterid,
			[`${itemstr}.itemid_itemgroupid`] : returnObject.itemgroupid,
			[`${itemstr}.itemid_lastpurchasecost`] : returnObject.itemid_lastpurchasecost,
			[`${itemstr}.uomconversionfactor`] : returnObject.alternateuom ? returnObject.uomconversionfactor : null,
			[`${itemstr}.uomconversiontype`] : returnObject.alternateuom ? returnObject.uomconversiontype : null,
			[`${itemstr}.unitarray`] : returnObject.unitarray
		};

		if (returnObject.itemid_itemtype != 'Project')
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		else {
			tempObj[`${itemstr}.rate`] = (!tempObj[`${itemstr}.splitrate`] && returnObject.rate) ? returnObject.rate : 0;

			tempObj[`${itemstr}.labourtaxid`] = (this.props.resource.labourtaxid && this.props.resource.labourtaxid.length > 0) ? this.props.resource.labourtaxid : [];

			tempObj[`${itemstr}.materialtaxid`] = (this.props.resource.materialtaxid && this.props.resource.materialtaxid.length > 0) ? this.props.resource.materialtaxid : [];
		}

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'projectquoteitems', itemstr);

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;
		tempObj[`${itemstr}.billingquantity`] = null;
		tempObj[`${itemstr}.billingrate`] = null;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		if (item.index) {
			let itemCount = 0;
			tempObj.kititemprojectquotedetails = [...this.props.resource.kititemprojectquotedetails];
			for (var i = 0; i < tempObj.kititemprojectquotedetails.length; i++) {
				if (tempObj.kititemprojectquotedetails[i].rootindex == item.index)
					itemCount++;
			}
			for (var i = 0; i < itemCount; i++) {
				for (var j = 0; j < tempObj.kititemprojectquotedetails.length; j++) {
					if (tempObj.kititemprojectquotedetails[j].rootindex == item.index) {
						tempObj.kititemprojectquotedetails.splice(j, 1);
						break;
					}
				}
			}

			if (returnObject.itemid_issaleskit) {
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = item.index;
					tempObj.kititemprojectquotedetails.push(returnObject.kititems[i]);
				}
			}
		} else {
			let index = 0;
			for (var i = 0; i < this.props.resource.projectquoteitems.length; i++) {
				if (this.props.resource.projectquoteitems[i].index > index)
					index = this.props.resource.projectquoteitems[i].index;
			}
			tempObj[`${itemstr}.index`] = index + 1;

			if (returnObject.itemid_issaleskit) {
				tempObj.kititemprojectquotedetails = [...this.props.resource.kititemprojectquotedetails];
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = tempObj[`${itemstr}.index`];
					tempObj.kititemprojectquotedetails.push(returnObject.kititems[i]);
				}
			}
		}

		this.props.updateFormState(this.props.form, tempObj);
		var tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
	}, (reason) => {});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson : valueobj.name,
		phone : valueobj.phone,
		email : valueobj.email,
		mobile : valueobj.mobile,
		contactid_name : valueobj.name,
		contactid_phone : valueobj.phone,
		contactid_email : valueobj.email,
		contactid_mobile : valueobj.mobile
	});
}

export function computeFinalRate() {
	setTimeout(() => {
		let tempObj = {};

		this.props.resource.projectquoteitems.forEach((item, index) => {
			if (item.itemid_itemtype == 'Project' && item.splitrate)
				tempObj[`projectquoteitems[${index}].rate`] = (item.labourrate ? item.labourrate : 0) + ((item.materialrate ? item.materialrate : 0));
		});

		this.props.updateFormState(this.props.form, tempObj);

		taxEngine(this.props, 'resource', 'projectquoteitems', null, true, true);
	}, 0);
}

export function callBackCustomerName (id, valueobj) {
	let tempObj = {
		customerid_name : valueobj.name,
		contactid: valueobj.contactid,
		contactperson: valueobj.contactid_name,
		email : valueobj.contactid_email,
		phone : valueobj.contactid_phone,
		mobile : valueobj.contactid_mobile,
		deliveryaddress : valueobj.addressid_displayaddress,
		billingaddress : valueobj.addressid_displayaddress,
		billingaddressid : valueobj.addressid,
		deliveryaddressid : valueobj.addressid,
		paymentterms : valueobj.paymentterms,
		modeofdespatch : valueobj.modeofdispatch,
		freight : valueobj.freight,
		territoryid : valueobj.territory,
		salesperson : this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		pricelistid : valueobj.salespricelistid ? valueobj.salespricelistid : valueobj.partnergroupid_pricelistid,
		creditperiod : valueobj.salescreditperiod ? valueobj.salescreditperiod : valueobj.partnergroupid_creditperiod
	};

	this.customFieldsOperation('partners', tempObj, valueobj, 'projectquotes');

	if (!tempObj['pricelistid']) {
		for (let i = 0; i < this.props.app.appSettings.length; i++) {
			if (this.props.app.appSettings[i].module == 'Sales' && this.props.app.appSettings[i].name == 'Pricelistid') {
				tempObj.pricelistid = this.props.app.appSettings[i].value.value;
				break;
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function addressonChange(address, addressobj) {
	this.props.updateFormState(this.props.form, {
		[`${address}`] : addressobj.displayaddress,
		[`${address}id`] : addressobj.id
	});
}

export function deliverydateonchange() {
	let tempObj = {};

	this.props.resource.projectquoteitems.forEach((item, index) => {
		if (!item.deliverydate || item.deliverydate == null)
			tempObj[`projectquoteitems[${index}].deliverydate`] = this.props.resource.deliverydate;
	});

	this.props.updateFormState(this.props.form, tempObj);
}

export function leadnoCallback(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		leadid_potentialrevenue: valueobj.potentialrevenue,
		leadid_currencyid: valueobj.currencyid,
	});
}

export function itemLevelInitfn(itemstr, updateValue) {
	let tempItem = this.selector(this.props.fullstate, itemstr);

	if(!tempItem[`${updateValue}`])
		this.props.updateFormState(this.props.form, {
			[`${itemstr}[${updateValue}]`] : this.props.resource[`${updateValue}`]
		});
}

export function expectedclosuredateFocusfn() {
	if(this.props.resource.expectedclosuredate == null)
		this.props.updateFormState(this.props.form, {
			expectedclosuredate: this.props.resource.nextfollowup
		});
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function renderTableReffield(item) {
	return (
		<div>
			<span>{item.internalrefno}</span>
			{item.internalrefno ? <br></br> : null}
			<span className="text-muted">{item.clientrefno ? `(${item.clientrefno})` : null}</span>
		</div>
	)
}

export function renderItemDetails(item) {
	return (
		<div>
			<div className={item.itemtype == 'Section' ? 'text-ellipsis-line-2' : ''}>{item.itemtype == 'Section' ? item.name : item.itemid_name}</div>
			<div className="text-ellipsis-line-2 font-13 text-muted">{item.description}</div>
		</div>
	);
}

export function quantityOnChange(itemqty, item, itemstr) {
	let kititemprojectquotedetails = this.props.resource.kititemprojectquotedetails;
	let tempObj = {};
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;

	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
	}
	
	for (var i = 0; i < kititemprojectquotedetails.length; i++) {
		if (item.index == kititemprojectquotedetails[i].rootindex) {
			if (!kititemprojectquotedetails[i].parentindex) {
				let tempIndex = kititemprojectquotedetails[i].index;
				let tempQuantity = Number((kititemprojectquotedetails[i].conversion * itemqty).toFixed(roundOffPrecisionStock));
				tempObj[`kititemprojectquotedetails[${i}].quantity`] = tempQuantity;
				calculateQuantity(tempIndex, tempQuantity, item.index);
			}
		}
	}

	function calculateQuantity(tempIndex, tempQuantity, index) {
		for (var j = 0; j < kititemprojectquotedetails.length; j++) {
			if (tempIndex == kititemprojectquotedetails[j].parentindex && index == kititemprojectquotedetails[j].rootindex) {
				let sectempQty = Number((kititemprojectquotedetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
				tempObj[`kititemprojectquotedetails[${j}].quantity`] = sectempQty;
				calculateQuantity(kititemprojectquotedetails[j].index, sectempQty, index);
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function addKitItem() {
	this.openModal({
		render: (closeModal) => {
			return <KititemaddModal resource = {this.props.resource} child={'projectquoteitems'} kit={'kititemprojectquotedetails'} array={this.props.array} callback={()=>{
				for (var i = 0; i < this.props.resource.projectquoteitems.length; i++) {
					this.controller.quantityOnChange(this.props.resource.projectquoteitems[i].quantity, this.props.resource.projectquoteitems[i], `projectquoteitems[${i}]`);
				}
			}} closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function conversionOnChange (item) {
	for (var i = 0; i < this.props.resource.projectquoteitems.length; i++) {
		if (item.rootindex == this.props.resource.projectquoteitems[i].index) {
			this.controller.quantityOnChange(this.props.resource.projectquoteitems[i].quantity, this.props.resource.projectquoteitems[i], `projectquoteitems[${i}]`);
			break;
		}
	}
}

export function ismilestonerequiredOnChange() {
	if(!this.props.resource.ismilestonerequired) {
		let projectquoteitems = [...this.props.resource.projectquoteitems];
		projectquoteitems.forEach((item) => {
			item.milestonetemplateid = null;
		});
		this.props.updateFormState(this.props.form, {
			projectquoteitems,
			milestoneitems: []
		});
	}
}

export function getimportMilestoneItems() {
	let projectquoteitems = [...this.props.resource.projectquoteitems];
	let milestoneitems = [...this.props.resource.milestoneitems];
	let milestonetemplateidArr = [], tempmilestonetemplateidArr = [];

	if(!this.props.resource.ismilestonerequired)
		return;

	projectquoteitems.forEach((item) => {
		if(item.itemtype == 'Item' && item.milestonetemplateid && !milestonetemplateidArr.includes(item.milestonetemplateid))
			milestonetemplateidArr.push(item.milestonetemplateid);
	});

	if(milestonetemplateidArr.length == 0)
		return;

	axios.get(`/api/milestonetemplateitems?field=id,parentid,milestonestageid,percentage,milestonestages/name/milestonestageid,displayorder,milestonetemplates/name/parentid&filtercondition=milestonetemplateitems.parentid IN (${milestonetemplateidArr})`).then((response) => {
		if (response.data.message == 'success') {
			let tempMilestonetemplateitems = response.data.main.sort((a, b) => a.id - b.id);

			deleteOldMilestoneitems(() => {
				tempMilestonetemplateitems.forEach((item) => {
					let itemfound = false;
					milestoneitems.some((milestoneitem) => {
						if(item.parentid == milestoneitem.milestonetemplateid && item.milestonestageid == milestoneitem.milestonestageid) {
							itemfound = true;
							return true;
						}
					});

					if(!itemfound) {
						milestoneitems.push({
							name: item.milestonestageid_name,
							milestonetemplateid: item.parentid,
							milestonetemplateid_name: item.parentid_name,
							milestonestageid: item.milestonestageid,
							milestonestageid_name: item.milestonestageid_name,
							percentage: item.percentage
						});
					}
				});

				this.props.updateFormState(this.props.form, {
					milestoneitems
				});
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});

	function deleteOldMilestoneitems(milestonedeleteCB) {
		let deleteitemcount = 0;
		projectquoteitems.forEach((item) => {
			milestonetemplateidArr.push(item.milestonetemplateid);
		});

		milestoneitems.forEach((milestoneitem, milestoneindex) => {
			if(!milestonetemplateidArr.includes(milestoneitem.milestonetemplateid)) {
				deleteitemcount++;
			}
		});

		for (var i = 0; i < deleteitemcount; i++) {
			milestoneitems.some((milestoneitem, milestoneindex) => {
				if(!milestonetemplateidArr.includes(milestoneitem.milestonetemplateid)) {
					milestoneitems.splice(milestoneindex, 1);
					return true;
				}
			});
		}
		milestonedeleteCB();
	}
}

export function callbackMilestoneTemplate(value, valueobj, item, itemstr) {
	let milestoneitems = [...this.props.resource.milestoneitems];
	let projectquoteitems = [...this.props.resource.projectquoteitems];
	let milestonetemplateidArr = [], deleteitemcount = 0;

	if(value > 0) {
		axios.get(`/api/milestonetemplateitems?field=id,parentid,milestonestageid,percentage,milestonestages/name/milestonestageid,displayorder,milestonetemplates/name/parentid&filtercondition=milestonetemplateitems.parentid=${value}`).then((response) => {
			if (response.data.message == 'success') {
				let tempMilestonetemplateitems = response.data.main.sort((a, b) => a.displayorder - b.displayorder);

				deleteOldMilestoneitems(() => {
					let itemfound = false;
					milestoneitems.some((milestoneitem) => {
						if(milestoneitem.milestonetemplateid == value) {
							itemfound = true;
							return true;
						}
					});

					if(!itemfound) {
						tempMilestonetemplateitems.forEach((item) => {
							milestoneitems.push({
								name: item.milestonestageid_name,
								milestonetemplateid: item.parentid,
								milestonetemplateid_name: item.parentid_name,
								milestonestageid: item.milestonestageid,
								milestonestageid_name: item.milestonestageid_name,
								percentage: item.percentage
							});
						});
					}

					this.props.updateFormState(this.props.form, {
						milestoneitems
					});
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	} else {
		deleteOldMilestoneitems(() => {
			this.props.updateFormState(this.props.form, {
				milestoneitems
			});
		});
	}

	function deleteOldMilestoneitems(milestonedeleteCB) {
		projectquoteitems.forEach((item) => {
			milestonetemplateidArr.push(item.milestonetemplateid);
		});

		milestoneitems.forEach((milestoneitem, milestoneindex) => {
			if(!milestonetemplateidArr.includes(milestoneitem.milestonetemplateid)) {
				deleteitemcount++;
			}
		});

		for (var i = 0; i < deleteitemcount; i++) {
			milestoneitems.some((milestoneitem, milestoneindex) => {
				if(!milestonetemplateidArr.includes(milestoneitem.milestonetemplateid)) {
					milestoneitems.splice(milestoneindex, 1);
					return true;
				}
			});
		}
		milestonedeleteCB();
	}
}

export function deleteProjectQuoteItem (index) {
	let itemCount = 0, milestoneitemCount = 0;
	let kititemprojectquotedetails = [...this.props.resource.kititemprojectquotedetails];
	let projectquoteitems = [...this.props.resource.projectquoteitems];
	let milestoneitems = [...this.props.resource.milestoneitems];

	if(projectquoteitems[index].estimatedqty > 0) {
		return this.props.openModal(modalService['infoMethod']({
			header : 'Warning',
			body : `Item "${projectquoteitems[index].itemid_name}" is already estimated. Please delete this item estimation`,
			btnArray : ['Ok']
		}));
	}

	for (var i = 0; i < kititemprojectquotedetails.length; i++) {
		if (kititemprojectquotedetails[i].rootindex == this.props.resource.projectquoteitems[index].index)
			itemCount++;
	}
	for (var i = 0; i < itemCount; i++) {
		for (var j = 0; j < kititemprojectquotedetails.length; j++) {
			if (kititemprojectquotedetails[j].rootindex == this.props.resource.projectquoteitems[index].index) {
				kititemprojectquotedetails.splice(j, 1);
				break;
			}
		}
	}

	let deleteMilestoneitem = true;
	for (var i = 0; i < projectquoteitems.length; i++) {
		if(i != index && projectquoteitems[i].milestonetemplateid == projectquoteitems[index].milestonetemplateid) {
			deleteMilestoneitem = false;
			break;
		}
	}

	if(deleteMilestoneitem) {
		milestoneitems.forEach((item) => {
			if (item.milestonetemplateid == projectquoteitems[index].milestonetemplateid)
				milestoneitemCount++;
		});
	}

	for (var i = 0; i < milestoneitemCount; i++) {
		for (var j = 0; j < milestoneitems.length; j++) {
			if (milestoneitems[j].milestonetemplateid == projectquoteitems[index].milestonetemplateid) {
				milestoneitems.splice(j, 1);
				break;
			}
		}
	}

	projectquoteitems.splice(index, 1);

	this.props.updateFormState(this.props.form, {
		kititemprojectquotedetails,
		projectquoteitems,
		milestoneitems
	});
	this.controller.computeFinalRate();
}

export function deleteKitItem(index) {
	let errorArray = [];
	let itemfound = false;
	let kititemprojectquotedetails = [...this.props.resource.kititemprojectquotedetails];

	for (var i = 0; i < kititemprojectquotedetails.length; i++) {
		if (index != i && kititemprojectquotedetails[index].rootindex == kititemprojectquotedetails[i].rootindex && kititemprojectquotedetails[index].parentindex == kititemprojectquotedetails[i].parentindex) {
			itemfound = true;
		}
	}
	if (!itemfound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	let itemCouFound = false;
	for (var i = 0; i < kititemprojectquotedetails.length; i++) {
		if (index != i && kititemprojectquotedetails[index].rootindex == kititemprojectquotedetails[i].rootindex && !kititemprojectquotedetails[i].parentindex)
			itemCouFound = true;
	}
	if (!itemCouFound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	if (errorArray.length == 0) {
		let parentIndexArray = [kititemprojectquotedetails[index].index];
		let rootindex = kititemprojectquotedetails[index].rootindex;
		deleteArray(kititemprojectquotedetails[index].rootindex, kititemprojectquotedetails[index].index);

		function deleteArray(rootindex, index) {
			for (var i = 0; i < kititemprojectquotedetails.length; i++) {
				if (kititemprojectquotedetails[i].rootindex == rootindex && kititemprojectquotedetails[i].parentindex == index) {
					parentIndexArray.push(kititemprojectquotedetails[i].index)
					deleteArray(rootindex, kititemprojectquotedetails[i].index);
				}
			}
		}

		for (var i = 0; i < parentIndexArray.length; i++) {
			for (var j = 0; j < kititemprojectquotedetails.length; j++) {
				if (kititemprojectquotedetails[j].rootindex == rootindex && kititemprojectquotedetails[j].index == parentIndexArray[i]) {
					kititemprojectquotedetails.splice(j, 1);
					break;
				}
			}
		}

		this.props.updateFormState(this.props.form, {kititemprojectquotedetails});
	} else {
		let response = {
			data : {
				message : 'failure',
				error : utils.removeDuplicate(errorArray)
			}
		};
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
}

export function openPotentialrevenueModal(param, checkparam) {

	let leadObj = {
		potentialrevenue: this.props.resource.leadid_potentialrevenue,
		currencyid: this.props.resource.leadid_currencyid,
		quoteflag: !checkparam,
		currencydiffer: checkparam
	};

	return this.openModal({
		render: (closeModal) => {
			return <PotentialrevenueupdateModal
					resource = {this.props.resource}
					closeModal = {closeModal}
					leadobj = {leadObj}
					app = {this.props.app}
					callback = {(potenitalvalue) =>{
						this.props.change('pipelinevalue', potenitalvalue);
						setTimeout(() =>{
							this.controller.save(param, null, true)
						},0);
					}} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Customer',
			data : emailObj
		},
		url : '/api/projectquotes'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function save (param, confirm, leadidconfirm) {
	if (param == 'Send To Customer') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};

		this.openModal({
			render: (closeModal) => {
				return <EmailModal
					parentresource = {this.props.resource}
					app = {this.props.app}
					resourcename = {'projectquotes'}
					activityemail = {false}
					callback = {this.controller.emailFn}
					openModal = {this.openModal}
					closeModal = {closeModal} />
			}, className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			}, confirmModal: true
		});
	} else if((param == 'Submit' || param == 'Approve') && this.props.resource.leadid > 0 && !leadidconfirm) {
		if(this.props.resource.currencyid == this.props.resource.leadid_currencyid) {
			if(!this.props.resource.leadid_potentialrevenue) {
				this.props.resource.pipelinevalue = this.props.resource.finaltotal;
				this.props.updateFormState(this.props.form, {
					pipelinevalue: this.props.resource.finaltotal
				});
				this.controller.save(param, null, true);
			} else if(this.props.resource.leadid_potentialrevenue != this.props.resource.finaltotal && (!this.props.resource.id  || this.props.resource.status == 'Draft'))
				this.controller.openPotentialrevenueModal(param, false);
			else if(this.props.resource.finaltotal != this.props.resource.tempfinaltotal && this.props.resource.id)
				this.controller.openPotentialrevenueModal(param, false);
			else {
				this.props.resource.pipelinevalue = this.props.resource.leadid_potentialrevenue;
				this.props.updateFormState(this.props.form, {
					pipelinevalue: this.props.resource.leadid_potentialrevenue
				});
				this.controller.save(param, null, true);
			}
		} else if(this.props.resource.currencyid != this.props.resource.leadid_currencyid) {
			if(!this.props.resource.leadid_potentialrevenue || (this.props.resource.leadid_potentialrevenue != this.props.resource.finaltotal && (!this.props.resource.id || this.props.resource.status == 'Draft')))
				this.controller.openPotentialrevenueModal(param, true);
			else if(this.props.resource.finaltotal != this.props.resource.tempfinaltotal && this.props.resource.id)
				this.controller.openPotentialrevenueModal(param, false);
			else {
				this.props.resource.pipelinevalue = this.props.resource.leadid_potentialrevenue;
				this.props.updateFormState(this.props.form, {
					pipelinevalue: this.props.resource.leadid_potentialrevenue
				});
				this.controller.save(param, null, true);
			}
		} else {
			this.props.resource.pipelinevalue = this.props.resource.leadid_potentialrevenue;
			this.props.updateFormState(this.props.form, {
				pipelinevalue: this.props.resource.leadid_potentialrevenue
			});
			this.controller.save(param, null, true);
		}
	} else {
		this.updateLoaderFlag(true);

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && param !='Amend' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/projectquotes'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true, true);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam)
					this.props.history.replace(`/details/projectquotes/${response.data.main.id}`);
				else if (param == 'Delete')
					this.props.history.replace("/list/projectquotes");
				else
					this.props.initialize({
						...response.data.main,
						tempfinaltotal : response.data.main.finaltotal
					});
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function createProject() {
	this.props.history.push({
		pathname: '/createProject',
		params: {
			...this.props.resource,
			param: 'Project Quotes'
		}
	});
}

export function createEstimations() {
	this.updateLoaderFlag(true);
	checkTransactionExist('projectestimations', 'projectquotes', this.props.resource.id, this.openModal, (param) => {
		if(param)
			this.props.history.push({pathname: '/createProjectEstimation', params: {...this.props.resource, param: 'Project Quotes'}});
		this.updateLoaderFlag(false);
	});
}

export function createProformaInvoice() {
	this.updateLoaderFlag(true);
	checkTransactionExist('proformainvoices', 'projectquotes', this.props.resource.id, this.openModal, (param) => {
		if(param)
			this.props.history.push({pathname: '/createProformaInvoice', params: {...this.props.resource, param: 'Project Quotes'}});
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}

export function profitabilityAnalysis() {
	return this.openModal({
		render: (closeModal) => {
			return <ProjectQuotationProfitAnalysisModal 
					resourcename = {'projectquotes'}
					resourceid = {this.props.resource.id}
					resourceitems = {this.props.resource.projectquoteitems}
					app = {this.props.app}
					closeModal = {closeModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});	
}

export function boqGridEditView() {
	return this.props.openModal({
		render: (closeModal) => {
			return <BOQGridEditModal
				childjson = {this.state.pagejson.projectquoteitems}
				resourcename={'projectquoteitems'}
				resource = {this.props.resource}
				updateFormState = {this.props.updateFormState}
				computeFinalRate = {this.controller.computeFinalRate}
				onLoadKit = {this.controller.onLoadKit}
				getimportMilestoneItems = {this.controller.getimportMilestoneItems}
				form = {this.props.form}
				getimportMilestoneItems = {this.controller.getimportMilestoneItems}
				openModal={this.props.openModal}
				app = {this.props.app}
				closeModal = {closeModal} />
		}, className: {
			content: 'react-modal-custom-class-100',
			overlay: 'react-modal-overlay-custom-class'
		},
		confirmModal: true
	});
}

export function modifiedcallback (modifiedDate) {
	this.props.updateFormState(this.props.form, {
		modified: modifiedDate
	});
}

export function markupbtncallback() {
	return this.props.openModal({
		render: (closeModal) => {
			return <ProjectMarkupModal
				resourcename = {'projectquotes'}
				resource = {this.props.resource}
				updateFormState = {this.props.updateFormState}
				computeFinalRate = {this.controller.computeFinalRate}
				form = {this.props.form}
				openModal={this.props.openModal}
				app = {this.props.app}
				closeModal = {closeModal} />
		}, className: {
			content: 'react-modal-custom-class-100',
			overlay: 'react-modal-overlay-custom-class'
		},
		confirmModal: true
	});
}

export function israteonlyOnchange(value, item, itemstr) {
	if(value) {
		let tempObj = {
			[`${itemstr}.quantity`]: 0
		};

		if(item.usebillinguom)
			tempObj[`${itemstr}.billingquantity`] = 0;

		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}