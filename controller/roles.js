import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	this.setState({
		moduleArray : ["Sales", "Project", "Purchase", "Stock", "Accounts", "Service", "Production", "HR", "Admin"]
	});
	if(this.state.createParam) {
		this.props.initialize({});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById () {
	axios.get(`/api/roles/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == 'success') {
			let tempObj = response.data.main;
			this.props.initialize(tempObj);
			setTimeout(this.controller.getuserRoleMapping, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getuserRoleMapping () {

	axios.get(`/api/userrolemapping?field=id,roleid,userid,isactive,users/displayname/userid,users/email/userid,roles/name/roleid&filtercondition=roleid=${this.props.resource.id}`).then((response) => {
		if(response.data.message == 'success') {
			let userroledetails = response.data.main.sort((a, b) => {
				return (a.id > b.id) ? -1 : (a.id < b.id) ? 1 : 0;
			});
			this.props.updateFormState(this.props.form, {
				userroledetails
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function userDetails (item) {
	return (<a href={`#/details/users/${item.userid}`}>{item.userid_displayname}</a>);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	let tempData = {
		actionverb : param,
		data : this.props.resource,
		ignoreExceptions : confirm ? true : false
	}

	axios({
		method : 'post',
		data : tempData,
		url : '/api/roles'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/roles/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace('/list/roles');
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
