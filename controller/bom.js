import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		companyid: this.props.app.selectedcompanyid,
		bomitems: []
	};

	if(this.props.location.params && this.props.location.params.param == 'copy') {
		tempObj = this.props.location.params;
	}
	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/bom/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = id;
	tempObj[`${itemstr}.itemid_name`] = valueobj.name;
	tempObj[`${itemstr}.description`] = valueobj.description;
	tempObj[`${itemstr}.uomid`] = valueobj.stockuomid;
	tempObj[`${itemstr}.uomid_name`] = valueobj.stockuomid_name;
	tempObj[`${itemstr}.allowproduction`] = valueobj.allowproduction;

	this.customFieldsOperation('itemmaster', tempObj, valueobj, 'bomitems', itemstr);

	this.props.updateFormState(this.props.form, tempObj);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/bom'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/bom/${response.data.main.id}`);
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/bom");
				} else {
					this.props.initialize(response.data.main);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
