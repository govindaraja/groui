import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';
import NewLeadStageModal from '../components/details/newleadstagemodal';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById () {
	axios.get(`/api/leadstages/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param) {
	if(param == 'Delete') {
		this.openModal({
			render: (closeModal) => {
				return <NewLeadStageModal parentresource={this.props.resource} callback={(id) => {
					this.props.updateFormState(this.props.form, {
						stageid : id
					});
					setTimeout(()=>{this.controller.saveLeadStage(param);}, 0);
				}} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	} else {
		this.controller.saveLeadStage(param);
	}
}

export function saveLeadStage (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/leadstages'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
			if (resparam)
				this.controller.saveLeadStage(param, true);
		});

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/leadstages/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/leadstages");
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
