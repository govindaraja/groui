import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {};

	if(this.props.location.params) {
		let params = this.props.location.params;

		tempObj.parentresource = params.parentresource;
		tempObj.parentid = params.parentid;
	}
	this.props.initialize(tempObj);

	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/tasks/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save(param, confirm) {
	this.updateLoaderFlag(true);

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/tasks'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if (this.props.isModal) {
				this.props.closeModal();
				this.props.callback(response.data.main);
			} else {
				if(this.state.createParam) {
					this.props.history.replace(`/details/tasks/${response.data.main.id}`);
				} else {
					if (param == 'Delete')
						this.props.history.replace("/list/tasks");
					else
						this.props.initialize(response.data.main);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function goToResourceLink () {
	let parentid = this.props.resource.parentid;
	let parentresource = this.props.resource.parentresource;

	if (!this.props.app.myResources[`${parentresource}`].fields['companyid']) {
		this.props.history.push(`/details/${this.props.resource.parentresource}/${this.props.resource.parentid}`);
	} else {
		let queryString = `/api/common/methods/checkCompanyAccessTasks?parentresource=${parentresource}&parentid=${parentid}`;

		axios.get(`${queryString}`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main && response.data.main.companyid && response.data.main.companyid != this.props.app.selectedcompanyid) {
					let message = {
						header : "Error",
						body : `Related Transaction belongs to a different company. Please change the Company to ${response.data.main.companyid_name} to view.`,
						btnArray : ["Ok"]
					};
					this.props.openModal(modalService.infoMethod(message));
				} else {
					this.props.history.push(`/details/${response.data.main.resourcetype}/${this.props.resource.parentid}`);
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function cancel () {
	if (this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}
	
