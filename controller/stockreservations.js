import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import StockdetailsModal from '../containers/stockdetails';
import StockreservationincludeitemModal from '../components/details/stockreservationincludeitemmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		companyid: this.props.app.selectedcompanyid,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		stockreservationitems: []
	};

	this.props.initialize(tempObj);	
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/stockreservations/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callBackCustomerName (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		partnerid : valueobj.id,
		partnerid_name : valueobj.name,
	});
}

export function callbackItem (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.itemid_name`] : valueobj.name,
		[`${itemstr}.description`] : valueobj.description,
		[`${itemstr}.uomid`] : valueobj.stockuomid,
		[`${itemstr}.uomid_name`] : valueobj.stockuomid_name,
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param !='Cancel' && param !='Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/stockreservations'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/stockreservations/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/stockreservations");
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function includeItem (param) {
	this.openModal({
		render: (closeModal) => {
			return <StockreservationincludeitemModal param={param} resource={this.props.resource} array={this.props.array} openModal={this.openModal} closeModal={closeModal} updateLoaderFlag={this.updateLoaderFlag} />
		}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
	});
}

export function cancel () {
	this.props.history.goBack();
}
