import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation, checkAccountBalance } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';
import EmailModal from '../components/details/emailmodal';
import { AccountBalance } from '../components/utilcomponents';
import CostCenterPickingdetailsModal from '../components/details/costcenterpickingdetailsmodal';

let onLoadFlag = false;

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();

	this.setState({
		receiptforarray: [{
			name: 'order',
			displayname: 'Order'
		}, {
			name: 'contract',
			displayname: 'Contract'
		}, {
			name: 'servicecall',
			displayname: 'Service Call'
		}, {
			name: 'projects',
			displayname: 'Project'
		}, {
			name: 'contractenquiries',
			displayname: 'Contract Enquiry'
		}]
	});
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		voucherdate: new Date(new Date().setHours(0, 0, 0, 0)),
		vouchertype: 'Receipt',
		journalvoucheritems: [],
		invoiceitems: [],
		deductions: [],
		otheraccounts: [],
		employeeadvances: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;
		let purchaseorderitemArray = params.supplierquotationitems;

		if(params.param == 'copy') {
			tempObj = params;
		}

		if(params.param == 'Sales Orders') {
			tempObj.receiptfor = 'order';
			tempObj.isadvance = true;
			utils.assign(tempObj, params, [{'partnerid' : 'customerid'}, 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'orderid' : 'id'}, {'amount' : 'finaltotal'}, 'salesperson', 'territoryid', 'contactid', 'contactperson', 'email', 'phone', 'mobile']);
			this.customFieldsOperation('orders', tempObj, params, 'journalvouchers');
		}

		if(params.param == 'Sales Invoices') {

			utils.assign(tempObj, params, [{'partnerid' : 'customerid'}, 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'amount' : 'outstandingamount'}, 'salesperson', 'engineerid', 'territoryid', 'servicecallid', 'contactid', 'contactperson', 'email', 'phone', 'mobile']);
			this.customFieldsOperation('salesinvoices', tempObj, params, 'journalvouchers');

			tempObj.invoiceitems.push({
				xsalesinvoiceid_invoiceno : params.invoiceno,
				xsalesinvoiceid : params.id,
				accountid : params.receivableaccountid,
				xsalesinvoiceid_paymentduedate : params.paymentduedate,
				xsalesinvoiceid_invoicedate : params.invoicedate,
				xsalesinvoiceid_balance : params.outstandingamount,
				xsalesinvoiceid_outstandingamount : params.outstandingamount,
				credit : params.outstandingamount
			});
			onLoadFlag = true;
		}

		if(params.param == 'Projects') {
			tempObj.receiptfor = 'projects';
			tempObj.isadvance = true;

			utils.assign(tempObj, params, [{'partnerid' : 'customerid'}, 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'projectid' : 'id'}, 'salesperson', 'contactid', 'email', 'phone', 'mobile']);

			this.customFieldsOperation('projects', tempObj, params, 'journalvouchers');
		}

		if(params.param == 'Accounts Receivable Report') {
			tempObj.currencyid = params.receiptArray[0].currencyid ? params.receiptArray[0].currencyid : this.props.app.defaultCurrency;
			utils.assign(tempObj, params.receiptArray[0], ['partnerid', 'currencyexchangerate', {'salesperson' : 'salespersonid'}, 'contactid', 'email', 'phone', 'mobile']);

			let totalBalance=0;
			for(var i = 0; i < params.receiptArray.length; i++) {
				totalBalance += params.receiptArray[i].balance;
				tempObj.invoiceitems.push({
					xsalesinvoiceid_invoiceno : params.receiptArray[i].voucherno,
					xsalesinvoiceid : params.receiptArray[i].xsalesinvoiceid,
					accountid : params.receiptArray[i].accountid,
					xsalesinvoiceid_paymentduedate : params.receiptArray[i].duedate,
					xsalesinvoiceid_invoicedate : params.receiptArray[i].voucherdate,
					xsalesinvoiceid_balance : params.receiptArray[i].balance,
					xsalesinvoiceid_outstandingamount : params.receiptArray[i].balance,
					credit : params.receiptArray[i].balance,
					xjournalvoucherid_voucherno : params.receiptArray[i].voucherno,
					xjournalvoucherid : params.receiptArray[i].xjournalvoucherid,
					xjournalvoucherid_paymentduedate : params.receiptArray[i].voucherdate,
					xjournalvoucherid_voucherdate : params.receiptArray[i].voucherdate,
					xjournalvoucherid_finaltotal : params.receiptArray[i].balance
				});
			}
			tempObj.amount = totalBalance;
			onLoadFlag = true;
		}

		if(params.param == 'Service Calls') {
			tempObj.receiptfor = 'servicecall';
			tempObj.isadvance = true;
			tempObj.engineerid = this.props.app.user.isengineer ? this.props.app.user.id : null;

			utils.assign(tempObj, params, [{'partnerid' : 'customerid'}, 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'servicecallid' : 'id'}, 'salesperson', 'territoryid', 'contactid', 'contactperson', 'email', 'phone', 'mobile']);

			this.customFieldsOperation('servicecalls', tempObj, params, 'journalvouchers');
		}

		if(params.param == 'Contracts') {
			tempObj.receiptfor = 'contract';
			tempObj.isadvance = true;

			utils.assign(tempObj, params, [{'partnerid' : 'customerid'}, 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'contractid' : 'id'}, {'amount' : 'finaltotal'}, 'salesperson', 'territoryid', 'contactid', 'contactperson', 'email', 'phone', 'mobile']);

			this.customFieldsOperation('contracts', tempObj, params, 'journalvouchers');
		}

		if(params.param == 'contractenquiries') {
			tempObj.receiptfor = 'contractenquiries';
			tempObj.isadvance = true;

			utils.assign(tempObj, params, [{'partnerid' : 'customerid'}, 'companyid', 'currencyid', 'currencyexchangerate', {'contractenquiryid' : 'id'}, {'amount' : 'finaltotal'}, 'salesperson', 'territoryid', 'contactid', 'contactpersonChange', 'email', 'phone', 'mobile']);

			this.customFieldsOperation('contractenquiries', tempObj, params, 'journalvouchers');
		}

		if(params.param == 'Proforma Invoices') {
			tempObj.isadvance = true;

			if(params.orderid > 0)
				tempObj.receiptfor = 'order';
			if(params.projectid > 0)
				tempObj.receiptfor = 'projects';
			if(params.contractenquiryid > 0)
				tempObj.receiptfor = 'contractenquiries';
			if(params.contractid > 0)
				tempObj.receiptfor = 'contract';

			utils.assign(tempObj, params, [{'partnerid' : 'customerid'}, 'companyid','defaultcostcenter', 'currencyid', 'currencyexchangerate',{'proformainvoiceid' : 'id'},{'proformainvoiceid_invoiceno' : 'invoiceno'}, 'amount', 'salesperson', 'territoryid', 'contactid', 'contactpersonChange', 'email', 'phone', 'mobile','contractenquiryid','contractid','orderid','projectid']);

			this.customFieldsOperation('proformainvoices', tempObj, params, 'journalvouchers');
			onLoadFlag = true;
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.currencyOnChange();
	}, 0);
	
	this.updateLoaderFlag(false);
}

export function renderAccountBalance(item, itemstr) {
	/*checkAccountBalance({
		companyid: this.props.resource.companyid,
		accountid: item.accountid,
		show: 'credit',
		againstid: item.xsalesinvoiceid > 0 ? item.xsalesinvoiceid : item.xjournalvoucherid,
		type: item.xsalesinvoiceid > 0 ? 'salesinvoices' : 'journalvouchers',
		partnerid: this.props.resource.partnerid,
	}, item, itemstr, this.selector, this.props);
	return item.accountbalance;*/
	return (
		<AccountBalance app={this.props.app} updateFn={(value) => this.updateLocalCompState(`${itemstr}.accountbalance`, value)} currencyid={this.props.resource.currencyid} showAs="text" balance={item.xsalesinvoiceid ? item.xsalesinvoiceid_outstandingamount : null} item={{
			companyid: this.props.resource.companyid,
			accountid: item.accountid,
			show: 'credit',
			againstid: item.xsalesinvoiceid > 0 ? item.xsalesinvoiceid : item.xjournalvoucherid,
			type: item.xsalesinvoiceid > 0 ? 'salesinvoices' : 'journalvouchers',
			partnerid: this.props.resource.partnerid,
		}} />
	);
}

export function renderEmployeeAccountBalance(item, itemstr) {
	return (
		<AccountBalance app={this.props.app} updateFn={(value) => this.updateLocalCompState(`${itemstr}.accountbalance`, value)} currencyid={this.props.resource.currencyid} showAs="text" balance={item.xexpenserequestid ? item.xexpenserequestid_outstandingamount : (item.xpayrollid ? item.xpayrollid_outstandingamount : null)} item={{
			companyid: this.props.resource.companyid,
			accountid: item.accountid,
			show: 'credit',
			againstid: item.xexpenserequestid > 0 ? item.xexpenserequestid : (item.xpayrollid ? item.xpayrollid : item.xjournalvoucherid),
			type: item.xexpenserequestid > 0 ? 'expenserequests' : (item.xpayrollid ? 'payrolls' :  'journalvouchers'),
			employeeid: this.props.resource.employeeid,
		}} />
	);
}

export function getItemById () {
	axios.get(`/api/receiptvouchers/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;

			if(tempObj.vouchertype == 'Receipt' && this.props.match.path != '/details/receiptvouchers/:id')
				return this.props.history.replace(`/details/receiptvouchers/${tempObj.id}`);
			if(tempObj.vouchertype == 'Payment' && this.props.match.path != '/details/paymentvouchers/:id')
				return this.props.history.replace(`/details/paymentvouchers/${tempObj.id}`);
			if(tempObj.vouchertype == 'Credit Note' && this.props.match.path != '/details/creditnotes/:id')
				return this.props.history.replace(`/details/creditnotes/${tempObj.id}`);
			if(tempObj.vouchertype == 'Debit Note' && this.props.match.path != '/details/debitnotes/:id')
				return this.props.history.replace(`/details/debitnotes/${tempObj.id}`);
			if(tempObj.vouchertype == 'Journal Voucher' && this.props.match.path != '/details/journalvouchers/:id')
				return this.props.history.replace(`/details/journalvouchers/${tempObj.id}`);

			tempObj.invoiceitems = [];
			tempObj.deductions = [];
			tempObj.otheraccounts = [];
			tempObj.employeeadvances = [];

			for (var i = 0; i < tempObj.journalvoucheritems.length; i++) {
				if (tempObj.journalvoucheritems[i].xsalesinvoiceid || tempObj.journalvoucheritems[i].xjournalvoucherid)
					tempObj.invoiceitems.push(tempObj.journalvoucheritems[i]);
				else if ((tempObj.journalvoucheritems[i].xexpenserequestid || tempObj.journalvoucheritems[i].xpayrollid || tempObj.journalvoucheritems[i].xjournalvoucherid) && tempObj.employeeid > 0)
					tempObj.employeeadvances.push(tempObj.journalvoucheritems[i]);
				else if(tempObj.journalvoucheritems[i].credit > 0)
					tempObj.otheraccounts.push(tempObj.journalvoucheritems[i]);
				else
					tempObj.deductions.push(tempObj.journalvoucheritems[i]);
			}

			if(tempObj.servicecallid)
				tempObj.receiptfor = 'servicecall';
			if(tempObj.orderid)
				tempObj.receiptfor = 'order';
			if(tempObj.contractid)
				tempObj.receiptfor = 'contract';
			if(tempObj.projectid)
				tempObj.receiptfor = 'projects';
			if(tempObj.contractenquiryid)
				tempObj.receiptfor = 'contractenquiries';

			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson: valueobj.name,
		phone: valueobj.phone,
		email: valueobj.email,
		mobile: valueobj.mobile,
		contactid_name: valueobj.name,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile,
	});
}

export function receiptforOnChange() {
	this.props.updateFormState(this.props.form, {
		orderid: null,
		contractid: null,
		servicecallid: null,
		engineerid: null,
		projectid: null,
		contractenquiryid: null
	});
}

export function callbackEngineer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson: valueobj.id
	});
}

export function callBackPartner (id, valueobj) {
	let tempObj = {
		isadvance: false,
		territoryid: valueobj.territory,
		salesperson: this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		contactid: valueobj.contactid,
		contactperson: valueobj.contactid_name,
		email: valueobj.contactid_email,
		phone: valueobj.contactid_phone,
		mobile: valueobj.contactid_mobile,
	};

	this.customFieldsOperation('partners', tempObj, valueobj, 'journalvouchers');

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getSalesInvoices();
}

export function callBackSO (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		defaultcostcenter: valueobj.defaultcostcenter
	});
}

export function callBackContract (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		defaultcostcenter: valueobj.defaultcostcenter
	});
}

export function callBackServiceCall (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		defaultcostcenter: valueobj.defaultcostcenter
	});
}

export function callBackProject (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		defaultcostcenter: valueobj.defaultcostcenter
	});
}

export function callBackEmployee (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		isadvance: false
	});
	this.controller.getSalesInvoices();
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			if(onLoadFlag)
				onLoadFlag =false;
			else
				this.controller.getSalesInvoices();

		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		if(onLoadFlag)
			onLoadFlag =false;
		else
			this.controller.getSalesInvoices();
	}
}

export function callBackAccount (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.accountid_name`]: valueobj.name,
		[`${itemstr}.accountid_createfollowup`]: valueobj.createfollowup,
		[`${itemstr}.followup`]: valueobj.createfollowup,
	});
}

export function getSalesInvoices () {
	if(this.props.resource.partnerid && this.props.resource.employeeid)
		return this.props.openModal(modalService.infoMethod({
			header : "Failure",
			body : "Please Choose either Partner or Employee",
			btnArray : ["Ok"]
		}));

	let tempObj = {
		invoiceitems: [],
		amount: 0,
		employeeadvances: []
	};
	if(this.props.resource.isadvance || !this.props.resource.companyid || !this.props.resource.currencyid || (!this.props.resource.partnerid && !this.props.resource.employeeid)) {
		this.props.updateFormState(this.props.form, tempObj);
		return null;
	}
	
	this.updateLoaderFlag(true);
	if (this.props.resource.partnerid) {
		axios.get(`/api/query/getpartneroutstandingsquery?type=Receipt&companyid=${this.props.resource.companyid}&currencyid=${this.props.resource.currencyid}&partnerid=${this.props.resource.partnerid}`).then((response) => {
			if (response.data.message == 'success') {

				tempObj.amount = 0;

				response.data.main.forEach((invitem) => {
					tempObj.amount += invitem.balance;
					invitem.credit = invitem.balance;
					if(invitem.xsalesinvoiceid)
						invitem.xsalesinvoiceid_outstandingamount = invitem.balance;
					tempObj.invoiceitems.push(invitem);
				});

				tempObj.amount = Number(tempObj.amount.toFixed(this.props.app.roundOffPrecision));

				if (tempObj.invoiceitems.length == 0) {
					this.props.openModal(modalService.infoMethod({
						header : "Warning",
						body : "No outstanding found for this Customer. You can create an advance entry",
						btnArray : ["Ok"]
					}));
				}
					
				for (var i = 0; i < tempObj.invoiceitems.length; i++) {
					tempObj.invoiceitems[i].tempduedate = (tempObj.invoiceitems[i].xsalesinvoiceid ? tempObj.invoiceitems[i].xsalesinvoiceid_paymentduedate : tempObj.invoiceitems[i].xjournalvoucherid_voucherdate);
				}

				tempObj.invoiceitems.sort((a, b) => {
					let atempduedate = (a.xsalesinvoiceid_paymentduedate ||  a.xjournalvoucherid_voucherdate);
					let btempduedate = (b.xsalesinvoiceid_paymentduedate ||  b.xjournalvoucherid_voucherdate);
					return new Date(atempduedate) - new Date(btempduedate);
				});

				this.props.updateFormState(this.props.form, tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	if (this.props.resource.employeeid) {
		axios.get(`/api/query/getemployeeoutstandingsquery?type=Receipt&companyid=${this.props.resource.companyid}&currencyid= ${this.props.resource.currencyid}&employeeid=${this.props.resource.employeeid}`).then((response) => {
			if (response.data.message == 'success') {

				response.data.main.forEach((invitem) => {
					tempObj.amount += invitem.balance;
					invitem.credit = invitem.balance;
					if(invitem.xexpenserequestid)
						invitem.xexpenserequestid_outstandingamount = invitem.balance;
					if(invitem.xpayrollid)
						invitem.xpayrollid_outstandingamount = invitem.balance;
					tempObj.employeeadvances.push(invitem);
				});
				tempObj.amount = Number(tempObj.amount.toFixed(this.props.app.roundOffPrecision));

				if (tempObj.employeeadvances.length == 0) {
					this.props.openModal(modalService.infoMethod({
						header : "Warning",
						body : "No Advances found for this Employee",
						btnArray : ["Ok"]
					}));
				}

				for (var i = 0; i < tempObj.employeeadvances.length; i++) {
					tempObj.employeeadvances[i].tempduedate = (tempObj.employeeadvances[i].xexpenserequestid ? tempObj.employeeadvances[i].xexpenserequestid_expensedate : tempObj.employeeadvances[i].xjournalvoucherid_voucherdate);
				}

				tempObj.employeeadvances.sort((a, b) => {
					let aduedate = a.xexpenserequestid_expensedate || a.xjournalvoucherid_voucherdate;
					let bduedate = b.xexpenserequestid_expensedate || b.xjournalvoucherid_voucherdate;
					return new Date(aduedate) - new Date(bduedate);
				});

				this.props.updateFormState(this.props.form, tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function isAdvanceChange () {
	this.props.updateFormState(this.props.form, {
		receiptfor: null,
		invoiceitems: [],
		otheraccounts: [],
		onaccountamount: 0,
		orderid: null,
		contractid: null,
		servicecallid: null,
		engineerid: null,
		projectid: null,
		contractenquiryid: null
	});
	this.controller.getSalesInvoices(this.props.resource.partnerid, this.props.resource.companyid, this.props.resource.currencyid);
}

export function openCostCenterPickDetails (item, itemstr) {
	this.openModal({render: (closeModal) => {
		return <CostCenterPickingdetailsModal item={item} resource={this.props.resource} itemstr={itemstr} amountstr={`${item.debit ? 'debit' : 'credit'}`} app={this.props.app} form={this.props.form} updateFormState={this.props.updateFormState} openModal={this.openModal} closeModal={closeModal} />
	}, className: {
		content: 'react-modal-custom-class-60',
		overlay: 'react-modal-overlay-custom-class'
	}, confirmModal: true });
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);
	let tempresourceObj = {
		journalvoucheritems: []
	};

	tempresourceObj.amountinwords = commonMethods.numberToWord(this.props.app, this.props.resource.amount, this.props.resource.currencyid);
	if (this.props.resource.currencyid == this.props.app.defaultCurrency) {
		tempresourceObj.amountinwordslc = commonMethods.numberToWord(this.props.app, this.props.resource.amount);
		tempresourceObj.amountlc = this.props.resource.amount;
	} else {
		if (this.props.resource.currencyexchangerate) {
			tempresourceObj.amountlc = Number((this.props.resource.amount * this.props.resource.currencyexchangerate).toFixed(this.props.app.roundOffPrecision));				
			tempresourceObj.amountinwordslc = commonMethods.numberToWord(this.props.app, tempresourceObj.amountlc)
		} else {
			tempresourceObj.amountlc = 0;
		}
	}
	tempresourceObj.isadvance = this.props.resource.isadvance;
	if(tempresourceObj.isadvance && !this.props.resource.partnerid)
		tempresourceObj.isadvance = false;

	if (!tempresourceObj.isadvance) {
		for (var i = 0; i < this.props.resource.invoiceitems.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.invoiceitems[i]);

		for (var i = 0; i < this.props.resource.deductions.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.deductions[i]);

		for (var i = 0; i < this.props.resource.otheraccounts.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.otheraccounts[i]);

		for (var i = 0; i < this.props.resource.employeeadvances.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.employeeadvances[i]);

		let creditAmount = 0, debitAmount = 0;
		debitAmount += this.props.resource.amount;
		creditAmount += (this.props.resource.onaccountamount ? this.props.resource.onaccountamount : 0);

		for (var i = 0; i < tempresourceObj.journalvoucheritems.length; i++) {
			creditAmount += tempresourceObj.journalvoucheritems[i].credit ? tempresourceObj.journalvoucheritems[i].credit : 0;
			debitAmount += tempresourceObj.journalvoucheritems[i].debit ? tempresourceObj.journalvoucheritems[i].debit : 0;
		}

		this.props.updateFormState(this.props.form, tempresourceObj);

		if (Number(creditAmount.toFixed(this.props.app.roundOffPrecision)) == Number(debitAmount.toFixed(this.props.app.roundOffPrecision))) {
			setTimeout(() => {
				this.controller.saveReceiptVoucher(param);
			}, 0);
		} else {
			this.updateLoaderFlag(false);
			let message = {
				header : 'Error',
				body : 'Sum of Allocated Amount should equal to sum of Voucher amount and Deductions',
				btnArray : ['Ok']
			};
			this.props.openModal(modalService.infoMethod(message));
		}
	} else {
		for (var i = 0; i < this.props.resource.deductions.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.deductions[i]);

		for (var i = 0; i < this.props.resource.otheraccounts.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.otheraccounts[i]);

		this.props.updateFormState(this.props.form, tempresourceObj);
		setTimeout(() => {
			this.controller.saveReceiptVoucher(param);
		}, 0);
	}
}

export function saveReceiptVoucher (param, confirm,confirm2) {
	if (param == 'Send To Customer') {
		this.updateLoaderFlag(false);
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'receiptvouchers'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});

	} else {
		this.updateLoaderFlag(true);

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm2 && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
			let message = {
				header : 'Cost Center Alert',
				body : `Cost Center not selected. Do you want to ${param} ?`,
				btnArray : ['Yes','No']
			};

			return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
				if(resparam)
					this.controller.saveReceiptVoucher(param,confirm,true);
				else
					this.updateLoaderFlag(false);
			}));	
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/receiptvouchers'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam) {
					this.controller.saveReceiptVoucher(param, true,confirm2);
				}
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					this.props.history.replace(`/details/receiptvouchers/${response.data.main.id}`);
				} else {
					if (param == 'Delete') {
						this.props.history.replace("/list/receiptvouchers");
					} else {
						let tempObj = response.data.main;
						tempObj.invoiceitems = [];
						tempObj.deductions = [];
						tempObj.otheraccounts = [];
						tempObj.employeeadvances = [];

						for (var i = 0; i < tempObj.journalvoucheritems.length; i++) {
							if (tempObj.journalvoucheritems[i].xsalesinvoiceid || tempObj.journalvoucheritems[i].xjournalvoucherid)
								tempObj.invoiceitems.push(tempObj.journalvoucheritems[i]);
							else if ((tempObj.journalvoucheritems[i].xexpenserequestid || tempObj.journalvoucheritems[i].xpayrollid || tempObj.journalvoucheritems[i].xjournalvoucherid) && tempObj.employeeid)
								tempObj.employeeadvances.push(tempObj.journalvoucheritems[i]);
							else if(tempObj.journalvoucheritems[i].credit > 0)
								tempObj.otheraccounts.push(tempObj.journalvoucheritems[i]);
							else
								tempObj.deductions.push(tempObj.journalvoucheritems[i]);
						}
						this.props.initialize(tempObj);
					}
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function getAllocatedAmount (param) {
	let childNameObj = {
		totalinvoiceamount: 'invoiceitems',
		totalaccountamount: 'otheraccounts',
		totaldeductionamount: 'deductions',
		totalexpenseamount: 'employeeadvances',
	}
	let childName = childNameObj[param];
	let childFieldName = param == 'totaldeductionamount' ? 'debit' : 'credit';
	let totalamount = 0;
	
	for(var i = 0; i < this.props.resource[childName].length;i++)
		totalamount += (this.props.resource[childName][i][childFieldName] ? this.props.resource[childName][i][childFieldName] : 0);

	return Number(totalamount.toFixed(this.props.app.roundOffPrecision));
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Customer',
			data : emailObj
		},
		url : '/api/receiptvouchers'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function cancel () {
	this.props.history.goBack();
}
