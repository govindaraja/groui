import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { itemmasterDisplaynamefilter,uomFilter } from '../utils/filter';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import { RenderLostLabel } from '../utils/customfieldtypes';
import ServicecallequipmentaddModal from '../components/details/servicecallequipmentaddmodal';
import EmailModal from '../components/details/emailmodal';
import { ItemRateField } from '../components/utilcomponents';
import ContractEnquriyEquipmentCreateModal from '../components/details/contractenquiryequipmentcreatemodal';

export function onLoad () {
	this.controller.contracttypesOnLoad();
}

export function contracttypesOnLoad () {

	this.updateLoaderFlag(true);

	axios.get(`/api/contracttypes?field=id,name,description,type,duration,schedule,rate,hsncodeid,hsncodes/salestaxid/hsncodeid,hsncodes/unionsalestaxid/hsncodeid,hsncodes/intersalestaxid/hsncodeid`).then((response) => {
		if(response.data.message == 'success') {
			let contracttypeArray = {
				'AMC' : [],
				'Warranty' : []
			};
			let	contractObj = {};

			response.data.main.forEach((item) => {
				if(item.type == 'AMC')
					contracttypeArray['AMC'].push(item);
				else
					contracttypeArray['Warranty'].push(item);

				contractObj[item.id] = item;
			});

			this.setState({ contracttypeArray, contractObj }, () => {
				if(this.state.createParam)
					this.controller.initializeState();
				else
					this.controller.getItemById();
			});

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function initializeState () {
	let tempObj = {
		currencyid : this.props.app.defaultCurrency,
		contractenquirydate : new Date(new Date().setHours(0, 0, 0, 0)),
		type : "New Enquiry",
		salesperson : this.props.app.user.issalesperson ? this.props.app.user.id : null,
		companyid : this.props.app.selectedcompanyid,
		additionalcharges : [],
		contractenquiryitems : [],
		contractbillingschedules : [],
		contractenquirycovereditems: [],
		contracttype : 'AMC',
		billingschedule : 'Not Applicable',
		contractfor: 'Equipment'
	};

	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function renderTableQtyfield(item) {
	if(item.capacityfield && item.capacity > 0)
		return (
			<div>
				<span style={{color:'gray'}}>{item.qtyinnos ? item.qtyinnos : 1} Nos * </span>
				<span style={{color:'gray'}}>{item.capacity ? item.capacity : 1} {itemmasterDisplaynamefilter(item.capacityfield, this.props.app)}</span>
				<span> = {item.quantity} {`${item.capacityfield ? `${itemmasterDisplaynamefilter(item.capacityfield, this.props.app)}` : ''}`}</span>
			</div>
		);
	else
		return <span>{item.quantity} {`${item.uomid ? `${uomFilter(item.uomid, this.props.app.uomObj)}` : 'Nos'}`}</span>
}

export function getItemById () {
	axios.get(`/api/contractenquiries/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function contractforOnChange() {
	this.props.updateFormState(this.props.form, {
		contractenquiryitems : [],
		contractbillingschedules : [],
		contractenquirycovereditems: []
	});
}

export function getOldContractTypes () {
	if(!this.props.resource.contractid)
		return Object.keys(this.state.contractObj).map((a) => {
			return this.state.contractObj[a];
		});

	return this.props.resource.oldcontracttypeid.map((a) => {
		return this.state.contractObj[a];
	});
}

export function callbackCustomer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson: this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		paymentterms: valueobj.paymentterms,
		territoryid: valueobj.territory,
		customerid_gstin: valueobj.gstin,
		billingaddress: null,
		billingaddressid: null,
		installationaddressid: null,
		installationaddress: null,
		contractenquiryitems: [],
		contactid: valueobj.contactid,
		email: valueobj.contactid_email,
		mobile: valueobj.contactid_mobile,
		phone: valueobj.contactid_phone,
		contactperson: valueobj.contactid_name,
		contactid_name: valueobj.contactid_name,
		contactid_email: valueobj.contactid_email,
		contactid_mobile: valueobj.contactid_mobile,
		contactid_phone: valueobj.contactid_phone,
		installationcontactid: valueobj.contactid,
		installationcontactperson: valueobj.contactid_name,
		installationemail: valueobj.contactid_email,
		installationmobile: valueobj.contactid_mobile,
		installationphone: valueobj.contactid_phone,
		installationcontactid_name: valueobj.contactid_name,
		installationcontactid_email: valueobj.contactid_email,
		installationcontactid_mobile: valueobj.contactid_mobile,
		installationcontactid_phone: valueobj.contactid_phone
	});
}

export function callbackType () {
	this.props.updateFormState(this.props.form, {
		newcontracttypeid: []
	});
}

export function childinit (item, itemstr) {
	if((this.props.resource.newcontracttypeid && this.props.resource.newcontracttypeid.length == 1) && this.props.resource.newcontracttypeid.indexOf(item.newcontracttypeid) == -1){
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.newcontracttypeid`]: item.newcontracttypeid == null ? this.props.resource.newcontracttypeid[0] : item.newcontracttypeid,
			[`${itemstr}.rate`]: (item.rate == null) ? this.state.contractObj[this.props.resource.newcontracttypeid[0]].rate : item.rate,
			[`${itemstr}.taxid`]: (item.taxid==null || item.taxid.length==0) ? this.controller.defaultHSNCodeTax(item) : item.taxid
		});

		this.controller.computeFinalRate();
	}
}

export function childContractChange (itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);

	if(item.newcontracttypeid) {
		let tempObj = {};

		this.props.updateFormState(this.props.form, {
			[`${itemstr}.rate`]: this.state.contractObj[item.newcontracttypeid].rate,
			[`${itemstr}.taxid`]: this.controller.defaultHSNCodeTax(item)
		});
	}

	this.controller.computeFinalRate();
}

export function defaultHSNCodeTax (item) {
	if(item.newcontracttypeid) {
		if(this.state.contractObj[item.newcontracttypeid].hsncodeid > 0) {
			let companygstin = this.props.app.selectedCompanyDetails.gstin ? this.props.app.selectedCompanyDetails.gstin.substr(0,2) : '';

			let partnergstin = (this.props.resource.installationaddressid_gstin || this.props.resource.customerid_gstin) ? (this.props.resource.installationaddressid_gstin || this.props.resource.customerid_gstin).substr(0,2) : '';

			if(companygstin != '') {
				let companyUGSTApplicable = false;
				this.props.app.utildata.india_states.map(function(sta) {
					if(sta.code == Number(companygstin))
						companyUGSTApplicable = sta.UTGSTApplicable ? true : false;
				});

				let taxid;

				if(partnergstin == '' || companygstin == partnergstin)
					 taxid = companyUGSTApplicable ? this.state.contractObj[item.newcontracttypeid]['hsncodeid_unionsalestaxid'] : (this.state.contractObj[item.newcontracttypeid]['hsncodeid_salestaxid'] || null);
				else
					taxid = this.state.contractObj[item.newcontracttypeid]['hsncodeid_intersalestaxid'] > 0 ? this.state.contractObj[item.newcontracttypeid]['hsncodeid_intersalestaxid']: null;

				return taxid > 0 ? [taxid] : [];
			} else
				return [];
		} else
			return [];
	}

	this.updateLoaderFlag(false);
}

export function getContractTypes(param) {
	let array = [];
	let name = (param == 'old' ? 'oldcontracttypeid' : (param == 'new' ? 'newcontracttypeid' : ''));
	if(name != '') {
		if(this.props.resource[name] && this.props.resource[name].length > 0) {
			this.props.resource[name].forEach((contype) => {
				array.push(this.state.contractObj[contype]);
			});
		}
	}
	return array;
}

export function callBackNewContractType () {
	let contractenquiryitems = [...this.props.resource.contractenquiryitems];
	let tempObj = {};
	if (this.props.resource.newcontracttypeid && this.props.resource.newcontracttypeid.length > 0) {

		if(!this.props.resource.duration)
			tempObj['duration'] = this.state.contractObj[this.props.resource.newcontracttypeid[0]].duration;

		contractenquiryitems.map((item, index) => {
			if(this.props.resource.newcontracttypeid.indexOf(item.newcontracttypeid) == -1) {
				item.newcontracttypeid = null;
				item.rate = null;
				item.discountquantity = null;
				item.amount = null;
				item.taxid = null;
			}

			item.newcontracttypeid = item.newcontracttypeid == null ? (this.props.resource.newcontracttypeid.length == 1 ? this.props.resource.newcontracttypeid[0] : item.newcontracttypeid) : item.newcontracttypeid;

			if (item.newcontracttypeid) {
				item.rate = item.rate == null ? (this.state.contractObj[item.newcontracttypeid].type == 'Warranty' ? 0 : this.state.contractObj[item.newcontracttypeid].rate) : item.rate;

				item.taxid = (item.taxid && item.taxid.length>0)? item.taxid :  this.controller.defaultHSNCodeTax(item);
			}
		});
		tempObj.contractenquiryitems = contractenquiryitems;
		
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}

	this.updateLoaderFlag(false);
}

export function calculateExpiryDate () {
	if (this.props.resource.duration != null && this.props.resource.startdate != null) {
		let tempStartDate = new Date(this.props.resource.startdate);
		let tempexpiryDate = new Date(tempStartDate.setMonth(tempStartDate.getMonth() + this.props.resource.duration)); 

		this.props.updateFormState(this.props.form, {
			expiredate : new Date(tempexpiryDate.setDate(tempexpiryDate.getDate()-1))
		});

		this.controller.callBackBillingSchedule();
	}
}

export function callBackBillingSchedule () {
	let contractbillingschedules = [];

	if(this.props.resource.startdate && this.props.resource.duration && this.props.resource.billingschedule !='Not Applicable') {
		let times;

		switch(this.props.resource.billingschedule) {
			case 'Annual':
				times = 12;
				break;
			case 'Semi Annual':
				times = 6;
				break;
			case 'Tri Annual':
				times = 4;
				break;
			case 'Quarterly':
				times = 3;
				break;
			case 'Bi Monthly':
				times = 2;
				break;
			case 'Monthly':
				times = 1;
				break;
		}

		if (this.props.resource.duration >= times) {
			let tempStartDate = new Date(this.props.resource.startdate);
			for (let i = this.props.resource.duration ; i >= times; i = i - times) {
				let tempEndDate = new Date(new Date(tempStartDate).setMonth(new Date(tempStartDate).getMonth() + times));
				if(i-times >= times)
					tempEndDate = new Date(new Date(tempEndDate).setDate(new Date(tempEndDate).getDate()-1));
				else
					tempEndDate = new Date(this.props.resource.expiredate);

				contractbillingschedules.push({
					scheduledate : new Date(tempStartDate),
					scheduleenddate : new Date(tempEndDate)
				});

				tempStartDate = new Date(new Date(tempStartDate).setMonth(new Date(tempStartDate).getMonth() + times));
			}
		}
	}

	this.props.updateFormState(this.props.form, { contractbillingschedules });
}

export function callbackProductCoveredItem(id, valueobj, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid:this.props.resource.customerid, partneraddressid:this.props.resource.billingaddressid} , null, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.itemid_useforoandm`] = returnObject.itemid_useforoandm;
		tempObj[`${itemstr}.capacityfield`] = returnObject.itemid_capacityfield;
		tempObj[`${itemstr}.capacity`] = returnObject[returnObject.itemid_capacityfield] ? returnObject[returnObject.itemid_capacityfield] : null;
		//tempObj[`${itemstr}.quantity`] = Number(((returnObject[returnObject.itemid_capacityfield] ? returnObject[returnObject.itemid_capacityfield] : 1)).toFixed(this.props.app.roundOffPrecision));
		tempObj[`${itemstr}.qtyinnos`] = returnObject.itemid_capacityfield ? (item.qtyinnos ? item.qtyinnos : 1) : null;
		tempObj[`${itemstr}.quantity`] = returnObject.itemid_capacityfield ? Number((tempObj[`${itemstr}.qtyinnos`] * tempObj[`${itemstr}.capacity`]).toFixed(this.props.app.roundOffPrecision)) : (item.quantity ? item.quantity : 1);
		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		tempObj[`${itemstr}.uomid`] = returnObject.stockuomid;
		//tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;

		this.props.updateFormState(this.props.form, tempObj);
		this.controller.calculateCoveredItemAmount();
	}, (reason) => {});
}

export function callbackProduct(id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid:this.props.resource.customerid, partneraddressid:this.props.resource.billingaddressid} , null, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.capacityfield`] = returnObject.itemid_capacityfield;
		tempObj[`${itemstr}.capacity`] = returnObject[returnObject.itemid_capacityfield] ? returnObject[returnObject.itemid_capacityfield] : null;
		tempObj[`${itemstr}.qtyinnos`] = returnObject[returnObject.itemid_capacityfield] ? (item.qtyinnos ? item.qtyinnos : 1) : null;
		tempObj[`${itemstr}.quantity`] = returnObject[returnObject.itemid_capacityfield] ? Number((tempObj[`${itemstr}.capacity`] * tempObj[`${itemstr}.qtyinnos`]).toFixed(this.props.app.roundOffPrecision)) : (item.quantity ? item.quantity : 1);
		tempObj[`${itemstr}.rate`] = (this.props.resource.newcontracttypeid && this.props.resource.newcontracttypeid.length == 1) ? (this.state.contractObj[this.props.resource.newcontracttypeid[0]].type == 'Warranty' ? 0 : this.state.contractObj[this.props.resource.newcontracttypeid[0]].rate) : null;

		this.props.updateFormState(this.props.form, tempObj);
		setTimeout(this.controller.computeFinalRate, 0);
	}, (reason) => {});
}

export function computeFinalRate() {
	setTimeout(() => {
		taxEngine(this.props, 'resource', 'contractenquiryitems', true);
	}, 0);
}

export function calculateCoveredItemAmount() {
	setTimeout(() => {
		let tempObj = {};
		this.props.resource.contractenquirycovereditems.forEach((item, index) => {
			tempObj[`contractenquirycovereditems[${index}].quantity`] = item.capacityfield ? Number(((item.capacity ? item.capacity : 1) * (item.qtyinnos ? item.qtyinnos :  0)).toFixed(this.props.app.roundOffPrecision)) : (item.quantity ? item.quantity : 0);
			tempObj[`contractenquirycovereditems[${index}].amount`] = Number(((tempObj[`contractenquirycovereditems[${index}].quantity`]) * (item.rate ? item.rate : 0)).toFixed(this.props.app.roundOffPrecision));
		});
		this.props.updateFormState(this.props.form, tempObj);
	}, 0);
}

export function addEquipment () {
	this.openModal({
		render: (closeModal) => {
			return <ServicecallequipmentaddModal
					   resource={this.props.resource}
					   contractObj={this.state.contractObj}
					   computeFinalRate={this.controller.computeFinalRate}
					   app={this.props.app}
					   array={this.props.array}
					   closeModal={closeModal}
					   openModal={this.openModal}
					   updateFormState={this.props.updateFormState}
					   form={this.props.form}
					   resourcename= {'contractenquiries'}
					   childresourcename = {'contractenquiryitems'}
					   defaultHSNCodeTax={this.controller.defaultHSNCodeTax}
					   customFieldsOperation = {this.customFieldsOperation} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function islostChange (id, valueobj, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: 0
	});

	this.controller.computeFinalRate();
}

export function renderLostLabel(item) {
	return <RenderLostLabel item = {item} />
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Contract Enquiry' and termsandconditions.isdefault`).then((response) => {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'installationaddress') {
		tempObj.installationaddress = addressobj.displayaddress;
		tempObj.installationaddressid = addressobj.id;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function tandcOnchange(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		tandc : valueobj.terms
	});
}

export function installationcontactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		installationcontactperson : valueobj.name,
		installationphone : valueobj.phone,
		installationemail : valueobj.email,
		installationmobile : valueobj.mobile,
		installationcontactid_name : valueobj.name,
		installationcontactid_phone : valueobj.phone,
		installationcontactid_email : valueobj.email,
		installationcontactid_mobile : valueobj.mobile,
	});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson : valueobj.name,
		phone : valueobj.phone,
		email : valueobj.email,
		mobile : valueobj.mobile,
		contactid_name : valueobj.name,
		contactid_phone : valueobj.phone,
		contactid_email : valueobj.email,
		contactid_mobile : valueobj.mobile
	});
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Customer',
			data : emailObj
		},
		url : '/api/contractenquiries'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function save (param, confirm) {
	if (param == 'Send To Customer') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'contractenquiries'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
	} else {
		this.updateLoaderFlag(true);

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && param !='Amend' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/contractenquiries'
		}).then((response) => {
			if(param != 'Won' || (param == 'Won' && response.data.message !='success')) {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
					if (resparam)
						this.controller.save(param, true);
				}));
			}

			if (response.data.message == 'success') {
				if(this.state.createParam)
					this.props.history.replace(`/details/contractenquiries/${response.data.main.id}`);
				else if (param == 'Delete')
					this.props.history.replace("/list/contractenquiries");
				else if (param == 'Won') {
					this.props.initialize(response.data.main);
					let message = {
						header : 'Create Contract?',
						body : 'Contract Enquiry status is changed. Would you like to create the Contract now?',
						btnArray : ['Yes','No']
					};
					this.props.openModal(modalService['confirmMethod'](message, (param) => {
						if(param)
							this.controller.createContract();
					}));
				} else
					this.props.initialize(response.data.main);
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function createContract () {
	let itemidFound = false;
	this.props.resource.contractenquiryitems.some(item => {
		if(!item.equipmentid && item.itemid > 0) {
			itemidFound = true;
			return true;
		}
	});

	if(itemidFound) {
		this.openModal({
			render: (closeModal) => {
				return <ContractEnquriyEquipmentCreateModal
					resource={this.props.resource}
					app={this.props.app}
					closeModal={closeModal}
					openModal={this.openModal}
					updateFormState={this.props.updateFormState}
					form={this.props.form}
					createOrEdit = {this.props.createOrEdit}
					customFieldsOperation = {this.customFieldsOperation}
					getCustomFields = {this.getCustomFields}
					callback = {(value) => this.controller.checkandcreateContract(value)}
				 />
			}, className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	} else {
		checkTransactionExist('contracts', 'contractenquiries', this.props.resource.id, this.openModal, (param) => {
			if(param)
				this.props.history.push({pathname: '/createContract', params: {...this.props.resource, param: 'contractenquiries'}});
		});
	}
}

export function checkandcreateContract(equipmentArray) {
	let tempResourceObj = JSON.parse(JSON.stringify(this.props.resource));
	let contractenquiryitems = [];

	tempResourceObj.contractenquiryitems.forEach(enquiryitem => {
		if(enquiryitem.equipmentid) {
			contractenquiryitems.push(enquiryitem);
		} else {
			equipmentArray.forEach(item => {
				if(item.id == enquiryitem.id) {
					contractenquiryitems.push(item);
				}
			});
		}
	});

	tempResourceObj.contractenquiryitems = contractenquiryitems;

	checkTransactionExist('contracts', 'contractenquiries', this.props.resource.id, this.openModal, (param) => {
		if(param)
			this.props.history.push({pathname: '/createContract', params: {...tempResourceObj, param: 'contractenquiries'}});
	});
}

export function createReceipt () {
	this.props.history.push({pathname: '/createReceiptVoucher', params: {...this.props.resource, param: 'contractenquiries'}});
}

export function createProformaInvoice () {
	this.updateLoaderFlag(true);
	checkTransactionExist('proformainvoices', 'contractenquiries', this.props.resource.id, this.openModal, (param) => {
		if(param)
			this.props.history.push({pathname: '/createProformaInvoice', params: {...this.props.resource, param: 'Contract Enquiries'}});
		this.updateLoaderFlag(false);
	});
}

export function calculateCoveredItemsTotal() {
	let totalamount = 0;

	this.props.resource.contractenquirycovereditems.forEach(item => {
		totalamount += item.amount ? item.amount : 0;
	});

	return Number(totalamount.toFixed(this.props.app.roundOffPrecision));
}

export function cancel () {
	this.props.history.goBack();
}
