import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({});
		setTimeout(this.controller.getResourceName, 0);
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getResourceName () {
	let resourceArray = [];

	let myResources = this.props.app.myResources;

	for(let prop in myResources) {
		if(!myResources[prop]['hideInPermission'] && myResources[prop]&& myResources[prop].authorization && myResources[prop].authorization['Print']) {
			resourceArray.push({
				resourceName : myResources[prop].resourceName,
				displayName : myResources[prop].displayName
			});
		}
	}

	resourceArray.sort((a, b) => {
		return (a.displayName < b.displayName) ? -1 : (a.displayName > b.displayName) ? 1 : 0;
	});

	this.props.updateFormState(this.props.form, { resourceArray });

	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/printmodules/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == 'success') {
			this.props.initialize(response.data.main);
			setTimeout(this.controller.getResourceName, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/printmodules'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/printmodules/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace('/list/printmodules');
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}