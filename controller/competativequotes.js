import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		competativequoteitems: [],
		additionalcharges: [],
		competativequoteitemArray : [],
		additionalchargesArray : []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'Quotes') {

			utils.assign(tempObj, params, [{'quoteid' : 'id'}, 'leadid', 'leadid_subject', 'customerid', 'companyid', 'currencyid', 'currencyexchangerate', 'customerid_name', 'quoteno', 'quotedate', 'roundoffmethod']);
			this.customFieldsOperation('quotes', tempObj, params, 'competativequotes');

			params.quoteitems.forEach((item) => {
				let tempChildObj = {
					alternateuom: item.uomconversiontype ? true : false
				};
				utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'description', 'rate', 'quantity', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'itemid_issaleskit', 'discountmode', 'itemname', 'drawingno', 'discountquantity', 'itemid_keepstock', 'taxid', 'taxdetails', 'uomconversionfactor', 'uomconversiontype', 'remarks']);
				tempObj.competativequoteitems.push(tempChildObj);
			});

			params.additionalcharges.forEach((charge) => {
				let chargetempobj = {};
				utils.assign(chargetempobj, charge, ['additionalchargesid', 'description', 'amount', 'taxcodeid', 'defaultpercentage', 'displayorder']);
				tempObj.additionalcharges.push(chargetempobj);
			});
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.currencyOnChange();
		this.controller.computeFinalRate();
	}, 0);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/competativequotes/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.competativequoteitems.forEach((item) => {
				if(item.uomconversiontype)
					item.alternateuom = true;
			});
			this.props.initialize(tempObj);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	this.props.change(`${item}.itemid`, valueobj.parentid);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	this.props.change(`${itemstr}.rate`, null);
	let promise = commonMethods.getItemDetails(id, this.props.resource.customerid, this.props.resource.pricelistid, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.itemid_keepstock;
		tempObj[`${itemstr}.itemid_imageurl`] = returnObject.itemid_imageurl;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'competativequoteitems');

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function uomOnchange (id, valueobj, item, itemstr) {
	let tempObj = {};

	if (valueobj.id == id && valueobj.alternateuom) {
		tempObj[`${itemstr}.uomconversiontype`] = valueobj.conversiontype;
		tempObj[`${itemstr}.uomconversionfactor`] = valueobj.conversionrate;
		tempObj[`${itemstr}.alternateuom`] = valueobj.alternateuom;
	} else {
		tempObj[`${itemstr}.uomconversiontype`] = null;
		tempObj[`${itemstr}.uomconversionfactor`] = null;
		tempObj[`${itemstr}.alternateuom`] = false;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function computeFinalRate() {
	setTimeout(() => taxEngine(this.props, 'resource', 'competativequoteitems'), 0);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/competativequotes'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace (`/details/competativequotes/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/competativequotes");
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
