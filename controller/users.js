import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService, pageValidation } from '../utils/services';
import UserSuccessModal from '../components/details/usersuccessmodal';
import ResetpasswordModal from '../components/details/resetpasswordmodal';

export function onLoad () {
	axios.get(`/api/companymaster?field=id,name,legalname`).then((response) => {
		if(response.data.message == 'success') {
			this.setState({
				companiesArray: response.data.main
			});
			this.controller.getTeamStructure();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getTeamStructure () {
	axios.get(`/api/teamstructure?field=id,fullname,name`).then((response) => {
		if(response.data.message == 'success') {
			this.setState({
				teamArray: response.data.main
			});
			this.controller.initialize();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function initialize() {
	if(this.state.createParam) {
		this.props.initialize({});
		this.updateLoaderFlag(false);
	} else {
		this.controller.getItemById();
	}
}

export function getRoleIds() {
	if(!this.state.roleArray)
		return [];

	let array = [];
	this.state.roleArray.forEach((role) => {
		if(role.checked)
			array.push(role.id);
	});
	return array;
}

export function onRoleChange(value, role) {
	if(value == 'selectall' || value == 'deselectall') {
		let roleArray = [...this.state.roleArray];
		roleArray.forEach((role) => {
			role.checked = (value == 'selectall' ? true : false);
		});
		this.setState({ roleArray });
	} else {
		role.checked = value;
		this.setState({
			roleArray : this.state.roleArray
		});
	}
}

export function getItemById () {
	axios.get(`/api/users/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			setTimeout(this.controller.getuserRoleMapping, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getuserRoleMapping () {
	axios.get(`/api/query/userrolemapquery?userid=${this.props.resource.id}`).then((response) => {
		if (response.data.message == 'success') {
			let roleArray = response.data.main.map((role) => {
				role.checked = role.userrolemapid > 0 ? true : false;
				return role;
			});
			this.setState({roleArray});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function forgotPass () {
	this.props.updateFormState(this.props.form, { password : "" });
}

export function defaultCompanyChange () {
	if(this.props.resource.companies) {
		if(this.props.resource.companies.indexOf(this.props.resource.defaultcompanyid) == -1) {
			let companyArray = [...this.props.resource.companies];
			companyArray.push(this.props.resource.defaultcompanyid);
			this.props.updateFormState(this.props.form, {
				companies: companyArray
			});
		}
	} else {
		this.props.updateFormState(this.props.form, {
			companies: [this.props.resource.defaultcompanyid]
		});
	}
}


export function onChangeAPIUser () {
	if(this.props.resource.isapiuser) {
		this.props.updateFormState(this.props.form, {
			loginstatus :'Blocked',
			trackerstatus : 'Blocked',
			essstatus: 'Blocked'
		});
	}
}

export function removeImage(param) {
	this.props.updateFormState(this.props.form, {
		[param]: null
	});
}

export function imageUpload (file, model) {
	this.updateLoaderFlag(true);
	let tempResourceobj = this.props.resource;
	tempResourceobj.parentresource = "users";
	tempResourceobj.resourceName = "users";
	tempResourceobj.parentid = tempResourceobj.id;
	tempResourceobj.imagename = "";
	tempResourceobj.uploadParam = model;

	let tempData = {
		actionverb : 'Save',
		data : tempResourceobj
	};

	const formData = new FormData();
	formData.append('file', file);
	formData.append('data', JSON.stringify(tempResourceobj));
	formData.append('actionverb', 'Save');

	const config = {
		headers: {
			'content-type': 'multipart/form-data'
		}
	};
	let fileName = file.name.substr(0, file.name.lastIndexOf('.'));

	if(!(/^[a-zA-Z0-9\.\,\_\-\'\!\*\(\)\ ]*$/.test(fileName))) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Invalid file name. Please change file name. file name contain invalid specical characters. Allowed special characters are '_ - . , ! ( ) *'",
			btnArray : ["Ok"]
		}));
	}

	if (file.size > 10485760) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "File Size should not be exceed than <b class='text-danger'>10 MB</b>",
			btnArray : ["Ok"]
		}));
	}

	if (!file.type.match('image.*')) {
		this.props.updateFormState(this.props.form, {
			image: null
		});
		let message = {
			header : "Error",
			body : "Invalid Image Format.Please Choose Image Type File",
			btnArray : ["Ok"]
		};
		this.props.openModal(modalService.infoMethod(message));
		this.updateLoaderFlag(false);
	} else {
		axios.post('/upload', formData, config).then((response) => {
			if (response.data.message == 'success') {
				let tempobj = response.data.main;
				tempobj.imagename = response.data.main.filename;
				tempobj.image = '';
				this.props.updateFormState(this.props.form, tempobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function save (param) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	let errorArray = [],
	tempObj = this.props.resource;

	if (tempObj.defaultcompanyid) {
		if(tempObj.companies.indexOf(tempObj.defaultcompanyid) == -1)
			errorArray.push('Default Company assigned is not in the list of companies selected for the user');
	} else {
		errorArray.push('Please Choose Default Company');
	}

	if(errorArray.length > 0) {
		let response = {
			data : {
				message : 'failure',
				error : errorArray
			}
		};
		this.updateLoaderFlag(false);
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	} else
		this.controller.saveUser(param, tempObj);
}

export function saveUser (param, tempObj, confirm) {
	this.updateLoaderFlag(true);

	tempObj.userrolemap = this.state.roleArray;
	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : tempObj,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/users'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);

		if (response.data.message == 'success' && this.state.createParam) {
			if (tempObj.autogenpassword && !tempObj.isapiuser) {
				this.props.updateFormState(this.props.form, response.data.main);
				setTimeout(() => {
					this.controller.showSuccessModal();
				}, 0);
			} else {
				apiResponse.message.isToast = false;
				apiResponse.message.bodyArray.push('Please Select Roles for this User');
			}
		}

		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.saveUser(param, tempObj, true);
		}));

		if(response.data.message == 'success') {
			if (this.state.createParam)
				this.props.history.replace(`/details/users/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace('/list/users');
			else {
				this.props.initialize(response.data.main);
				setTimeout(this.controller.getuserRoleMapping, 0);
			}
		}

		this.updateLoaderFlag(false);
	});
}

export function showSuccessModal(data) {
	this.openModal({
		render : (closeModal) => {
			return <UserSuccessModal
				resource = {this.props.resource}
				closeModal = {closeModal}
				openModal = {this.openModal}
			/>
		}, className : {
			content: 'react-modal-custom-class-30',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

/*export function saveUser (param, tempObj, confirm) {
	this.updateLoaderFlag(true);

	tempObj.userrolemap = this.state.roleArray;
	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : tempObj,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/users'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);

		if(response.data.message == 'success' && this.state.createParam) {
			apiResponse.message.isToast = false;
			apiResponse.message.bodyArray.push('Please Select Companies & Roles for this User');
		}

		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.saveUser(param, tempObj, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/users/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace('/list/users');
			else {
				response.data.main.forgotPassword = false;
				this.props.initialize(response.data.main);
				setTimeout(this.controller.getuserRoleMapping, 0);
			}
		}

		this.updateLoaderFlag(false);
	});
}*/

export function cancel () {
	this.props.history.goBack();
}

export function resetPassword () {
	this.openModal({
		render : (closeModal) => {
			return <ResetpasswordModal 
				resource = {this.props.resource}
				updateFormState = {this.props.updateFormState}
				form = {this.props.form}
				openModal = {this.openModal}
				callback = {(data) => {
					setTimeout(() => {
						if (data)
							this.controller.showSuccessModal(data);
					}, 100);}}
				closeModal = {closeModal}
			/>
		}, className : {
			content : 'react-modal-custom-class-30',
			overlay : 'react-modal-overlay-custom-class'
		}, confirmModal: true
	});
}