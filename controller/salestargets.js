import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	this.setState({months : [{id : 0, name : "January"}, {id : 1, name : "February"}, {id : 2, name : "March"}, {id : 3, name : "April"}, {id : 4, name : "May"}, {id : 5, name : "June"}, {id : 6, name : "July"}, {id : 7, name : "August"}, {id : 8, name : "September"}, {id : 9, name : "October"}, {id : 10, name : "November"}, {id : 11, name : "December"}]})
	if(this.state.createParam) {
		this.props.initialize({
			year: new Date().getFullYear(),
			companyid: this.props.app.selectedcompanyid
		});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById () {
	axios.get(`/api/salestargets/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			setTimeout(() => {
				this.controller.getTargetFields();
			}, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function baseResourceOnChange () {
	this.props.updateFormState(this.props.form, {
		targetfield: null
	});
	this.controller.getTargetFields();
}

export function getTargetFields () {
	let targetFields = [];

	if(this.props.resource.baseresource == 'Leads') {
		for(var  prop in this.props.app.myResources.leads.fields)
			if(this.props.app.myResources.leads.fields[prop].includeInTargets)
				targetFields.push({name:'leads-'+ prop, displayname : 'Leads-'+this.props.app.myResources.leads.fields[prop].displayName});
				
		for(var prop in this.props.app.myResources.leaditems.fields)
			if(this.props.app.myResources.leaditems.fields[prop].includeInTargets)
				targetFields.push({name:'leaditems-'+prop, displayname : 'Lead Items-'+this.props.app.myResources.leaditems.fields[prop].displayName});

		targetFields.push({name:'leadscount-count',displayname : 'Leads-Count'});
	} else if(this.props.resource.baseresource == 'Orders') {
		for(var prop in this.props.app.myResources.orders.fields)
			if(this.props.app.myResources.orders.fields[prop].includeInTargets)
				targetFields.push({name:'orders-'+prop, displayname : 'Orders-'+this.props.app.myResources.orders.fields[prop].displayName});
				
		for(var prop in this.props.app.myResources.orderitems.fields)
			if(this.props.app.myResources.orderitems.fields[prop].includeInTargets)
				targetFields.push({name:'orderitems-'+prop, displayname : 'Order Items-'+this.props.app.myResources.orderitems.fields[prop].displayName});
	} else if(this.props.resource.baseresource == 'Sales Invoices') {
		for(var prop in this.props.app.myResources.salesinvoices.fields)
			if(this.props.app.myResources.salesinvoices.fields[prop].includeInTargets)
				targetFields.push({name:'salesinvoices-'+prop, displayname : 'Sales Invoices-'+this.props.app.myResources.salesinvoices.fields[prop].displayName});
				
		for(var prop in this.props.app.myResources.salesinvoiceitems.fields)
			if(this.props.app.myResources.salesinvoiceitems.fields[prop].includeInTargets)
				targetFields.push({name:'salesinvoiceitems-'+prop, displayname : 'Sales Invoice Items-'+this.props.app.myResources.salesinvoiceitems.fields[prop].displayName});
	} else if(this.props.resource.baseresource == 'Receipts') {
		for(var prop in this.props.app.myResources.journalvouchers.fields) {
			if(this.props.app.myResources.journalvouchers.fields[prop].includeInTargets){
				targetFields.push({name: 'receipt-'+prop, displayname : 'Receipts-'+this.props.app.myResources.journalvouchers.fields[prop].displayName});
			}
		}
	}
	this.props.updateFormState(this.props.form, {
		targetFields
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/salestargets'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/salestargets/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/salestargets");
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
