import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation, checkStatus } from '../utils/services';
import { ItemRateField,ChildEditModal } from '../components/utilcomponents';
import EstimationItemPickingdetailsModal from '../components/details/estimationitempickingdetailsmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		invoicedate: new Date(new Date().setHours(0, 0, 0, 0)),
		invoicetype: 'Commercial Invoice',
		purchaseinvoiceitems: [],
		kititempurchaseinvoicedetails: [],
		additionalcharges: [],
		accountingforms: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'Receipt Notes') {

			utils.assign(tempObj, params, ['partnerid', 'companyid', 'contactid', 'defaultcostcenter', 'currencyid', 'deliveryaddress', 'billingaddress', 'supplieraddress', 'deliveryaddressid', 'billingaddressid', 'supplieraddressid', 'taxid', 'remarks', 'ewaybillno']);
			tempObj.supplierstate = params.supplieraddressid_state ? params.supplieraddressid_state : params.supplierstate;
			tempObj.suppliergstin = params.supplieraddressid_gstin ? params.supplieraddressid_gstin : params.partnerid_gstin;
			tempObj.suppliergstregtype = params.partnerid_gstregtype ? params.partnerid_gstregtype : params.suppliergstregtype;

			this.customFieldsOperation('receiptnotes', tempObj, params, 'purchaseinvoices');

			params.receiptnoteitems.forEach((item, index) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				if(item.quantity > item.invoicedqty) {
					let tempChildObj = {
						index: index + 1,
						alternateuom: item.uomconversiontype ? true : false,
						quantity: (item.quantity - item.invoicedqty),
						sourceresource: 'receiptnoteitems'
					};
					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_uomgroupid', {
						'sourceid': 'id'
					}, 'description', 'specification', 'uomid', 'uomid_name', 'uomconversionfactor', 'uomconversiontype', 'purchaseorderitemsid', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingconversiontype', 'billingconversionfactor', 'remarks','itemmakeid']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('receiptnoteitems', tempChildObj, item, 'purchaseinvoiceitems');

					tempObj.purchaseinvoiceitems.push(tempChildObj);
				}
			});
		}

		if(params.param == 'Work Orders') {

			utils.assign(tempObj, params, ['partnerid', 'companyid', {'contactid' : 'suppliercontactid'},{'email' : 'supplieremail'},{'mobile' : 'suppliermobile'},{'phone' : 'supplierphone'}, 'defaultcostcenter', 'currencyid', 'deliveryaddress', 'billingaddress', 'deliveryaddressid', 'billingaddressid', 'taxid', 'roundoffmethod', 'currencyid', 'currencyexchangerate', 'remarks']);

			this.customFieldsOperation('workorders', tempObj, params, 'purchaseinvoices');

			params.workorderitems.forEach((item, index) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				item.completedqty = item.completedqty ? item.completedqty : 0;
				if(item.completedqty > item.invoicedqty) {
					let tempChildObj = {
						index: index + 1,
						alternateuom: item.uomconversiontype ? true : false,
						quantity: (item.completedqty - item.invoicedqty),
						sourceresource: 'workorderitems',
					};
					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_uomgroupid', {
						'sourceid': 'id'
					}, 'description', 'uomid', 'uomid_name', 'uomconversionfactor', 'uomconversiontype', 'rate', 'discountmode', 'discountquantity', 'taxid', 'remarks']);

					this.customFieldsOperation('workorderitems', tempChildObj, item, 'purchaseinvoiceitems');

					tempObj.purchaseinvoiceitems.push(tempChildObj);
				}
			});
		}
		if(params.param == 'Work Progress') {

			utils.assign(tempObj, params, ['partnerid', 'companyid', {'contactid' : 'suppliercontactid'},{'email' : 'supplieremail'},{'mobile' : 'suppliermobile'},{'phone' : 'supplierphone'}, {'defaultcostcenter': 'projectid_defaultcostcenter'}, 'currencyid', 'deliveryaddress', 'billingaddress', 'deliveryaddressid', 'billingaddressid', 'taxid', 'roundoffmethod', 'currencyid', 'currencyexchangerate', 'remarks']);

			this.customFieldsOperation('workprogress', tempObj, params, 'purchaseinvoices');

			params.workprogressitems.forEach((item, index) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				if(item.quantity > item.invoicedqty) {
					let tempChildObj = {
						index: index + 1,
						alternateuom: item.uomconversiontype ? true : false,
						quantity: item.workorderitemsid ? (item.workorderitemsid_completedqty - (item.workorderitemsid_invoicedqty || 0)) :  (item.quantity - item.invoicedqty),
						sourceresource: item.workorderitemsid ? 'workorderitems' : 'workprogressitems',
						sourceid: item.workorderitemsid ? item.workorderitemsid : item.id,
						taxid: item.workorderitemsid ? item.workorderitemsid_taxid : [],
						rate:  item.workorderitemsid ? item.workorderitemsid_rate : item.rate,
						discountmode:  item.workorderitemsid ? item.workorderitemsid_discountmode : item.discountmode,
						discountquantity:  item.workorderitemsid ? item.workorderitemsid_discountquantity : item.discountquantity
					};
					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_uomgroupid', 'description', 'uomid', 'uomid_name', 'uomconversionfactor', 'uomconversiontype', 'remarks']);

					this.customFieldsOperation('workprogressitems', tempChildObj, item, 'purchaseinvoiceitems');

					tempObj.purchaseinvoiceitems.push(tempChildObj);
				}
			});
		}
		if(params.param == 'Expense Requests') {
			utils.assign(tempObj, params, ['expenserequestid', 'expenserequestitemsid']);

			this.customFieldsOperation('expenserequests', tempObj, params, 'purchaseinvoices');
		}
		if(tempObj.purchaseinvoiceitems.length == 0 && params.param != 'Expense Requests') {
			let apiResponse = commonMethods.apiResult({
				data : {
					message : 'Items are Already Invoiced'
				}
			});
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			tempObj = {
				currencyid: this.props.app.defaultCurrency,
				companyid: this.props.app.selectedcompanyid,
				roundoffmethod: this.props.app.defaultRoundOffMethod,
				invoicedate: new Date(new Date().setHours(0, 0, 0, 0)),
				invoicetype: 'Commercial Invoice',
				purchaseinvoiceitems: [],
				kititempurchaseinvoicedetails: [],
				additionalcharges: [],
				accountingforms: []
			}
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.getTandC();
		if(this.props.resource.partnerid)
			this.controller.callBackCustomerNameInternal(true);

		let purchaseorderitemsidarray = [], itemsidarray = [];

		this.props.resource.purchaseinvoiceitems.forEach((item) => {
			if(item.purchaseorderitemsid > 0)
				purchaseorderitemsidarray.push(item.purchaseorderitemsid);

			if(item.itemid > 0)
				itemsidarray.push(item.itemid);
		});

		if(itemsidarray.length > 0)
			this.controller.getAccountDetails(itemsidarray);

		if(purchaseorderitemsidarray.length > 0)
			this.controller.getPurchaseOrderDetails(purchaseorderitemsidarray);

		this.controller.computeFinalRate();

	}, 0);
	this.updateLoaderFlag(false);
}

export function callbackProject(id, valueobj) {
	let tempObj = {
		projectid_estimationavailable: valueobj.estimationavailable,
		defaultcostcenter: valueobj.defaultcostcenter
	};
	this.customFieldsOperation('projects', tempObj, valueobj, 'purchaseinvoices');
	this.props.updateFormState(this.props.form, tempObj);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function getPurchaseOrderDetails (idarray) {
	let customFields = this.getCustomFields('purchaseorderitems','string', false, this.props.app);

	axios.get(`/api/purchaseorderitems?field=id,parentid,rate,billingrate,ratelc,discountmode,finalratelc,discountquantity,itemmaster/issaleskit/itemid,taxid,index${(customFields ? ','+customFields : '' )}&filtercondition=purchaseorderitems.id in (${idarray.join()})`).then((response) => {
		if (response.data.message == 'success') {
			let poIDArray = [], tempObj = {};
			this.props.resource.purchaseinvoiceitems.forEach((invitem, invindex) => {
				for (var i = 0; i < response.data.main.length; i++) {
					if(response.data.main[i].id == invitem.purchaseorderitemsid) {
						if(poIDArray.indexOf(response.data.main[i].parentid) == -1)
							poIDArray.push(response.data.main[i].parentid);

						tempObj[`purchaseinvoiceitems[${invindex}].rate`] = response.data.main[i].rate;
						tempObj[`purchaseinvoiceitems[${invindex}].billingrate`] = response.data.main[i].billingrate;
						tempObj[`purchaseinvoiceitems[${invindex}].itemid_issaleskit`] = response.data.main[i].itemid_issaleskit;
						tempObj[`purchaseinvoiceitems[${invindex}].discountmode`] = response.data.main[i].discountmode;
						tempObj[`purchaseinvoiceitems[${invindex}].discountquantity`] = response.data.main[i].discountquantity;
						tempObj[`purchaseinvoiceitems[${invindex}].purchaseorderid`] = response.data.main[i].parentid;
						tempObj[`purchaseinvoiceitems[${invindex}].order_index`] = response.data.main[i].index;
						tempObj[`purchaseinvoiceitems[${invindex}].taxid`] = response.data.main[i].taxid;
						this.customFieldsOperation('purchaseorderitems', tempObj, response.data.main[i], 'purchaseinvoiceitems', `purchaseinvoiceitems[${invindex}]`);
						break;
					}
				}
			});

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
			this.controller.getAdditionalCharges(poIDArray);
			this.controller.getPOkititems(poIDArray);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getPOkititems (poIDArray) {
	if(poIDArray.length == 0)
		return null;

	axios.get(`/api/kititempurchaseorderdetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid,rate,ratelc&filtercondition=kititempurchaseorderdetails.parentid in (${poIDArray.join()})`).then((response) => {
		if(response.data.message == 'success') {
			let kititempurchaseinvoicedetails = [];
			let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;
			response.data.main.forEach((kititem) => {
				this.props.resource.purchaseinvoiceitems.forEach((invitem) => {
					if(kititem.parentid == invitem.purchaseorderid && kititem.rootindex == invitem.order_index) {
						kititempurchaseinvoicedetails.push({
							itemid: kititem.itemid,
							itemid_name: kititem.itemid_name,
							parent: kititem.parent,
							uomid: kititem.uomid,
							uomid_name: kititem.uomid_name,
							quantity: kititem.quantity,
							conversion: kititem.conversion,
							rate: kititem.rate,
							ratelc: kititem.ratelc,
							itemid_issaleskit: kititem.itemid_issaleskit,
							itemid_hasserial: kititem.itemid_hasserial,
							itemid_hasbatch: kititem.itemid_hasbatch,
							itemid_keepstock: kititem.itemid_keepstock,
							rootindex: invitem.index,
							warehouseid: invitem.warehouseid,
							index: kititem.index,
							parentindex: kititem.parentindex
						});
					}
				});
			});
			this.props.resource.purchaseinvoiceitems.forEach((invitem) => {
				for (var i = 0; i < kititempurchaseinvoicedetails.length; i++) {
					if (invitem.index == kititempurchaseinvoicedetails[i].rootindex) {
						if (!kititempurchaseinvoicedetails[i].parentindex) {
							let tempIndex = kititempurchaseinvoicedetails[i].index;
							let tempQuantity = Number((kititempurchaseinvoicedetails[i].conversion * invitem.quantity).toFixed(roundOffPrecisionStock));
							kititempurchaseinvoicedetails[i].quantity = tempQuantity;
							calculateQuantity(tempIndex, tempQuantity, invitem.index);
						}
					}
				}
			});
			function calculateQuantity(tempIndex, tempQuantity, index) {
				for (var j = 0; j < kititempurchaseinvoicedetails.length; j++) {
					if (tempIndex == kititempurchaseinvoicedetails[j].parentindex && index == kititempurchaseinvoicedetails[j].rootindex) {
						let sectempQty = Number((kititempurchaseinvoicedetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
						kititempurchaseinvoicedetails[j].quantity = sectempQty;
						calculateQuantity(kititempurchaseinvoicedetails[j].index, sectempQty, index);
					}
				}
			}
			this.props.updateFormState(this.props.form, {
				kititempurchaseinvoicedetails
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getAdditionalCharges (poIDArray) {
	if(poIDArray.length == 0)
		return null;

	axios.get(`/api/additionalcharges?field=name,amount,applyforvaluation,parentid,parentresource,additionalchargesid,additionalchargesmaster/name/additionalchargesid,description,taxcodeid,defaultpercentage,displayorder&filtercondition=additionalcharges.parentid in (${poIDArray.join()}) and parentresource ='purchaseorders'`).then((response) => {
		if (response.data.message == 'success') {
			let additionalcharges = response.data.main.sort((a, b) => {
				return (a.displayorder) - (b.displayorder);
			});
			this.props.updateFormState(this.props.form, {
				additionalcharges
			});
			this.controller.computeFinalRate();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getItemById () {
	axios.get(`/api/purchaseinvoices/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			for (var i = 0; i < tempObj.purchaseinvoiceitems.length; i++) {
				if (tempObj.purchaseinvoiceitems[i].uomconversiontype)
					tempObj.purchaseinvoiceitems[i].alternateuom = true;
			}
			this.props.initialize(tempObj);

			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Purchase Invoice' and termsandconditions.isdefault`).then((response)=> {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function tandcOnchange(id, valueobj) {
	if(id) {
		this.props.updateFormState(this.props.form, {
			tandc: valueobj.terms
		});
	}
}

export function currencyOnChange () {
	if (this.props.resource.pricelistid) {
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid) {
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});
		}
	}
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject)=> {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason)=> {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function calculateDueDate () {
	let tempObj = {};

	let creditPeriod = this.props.resource.creditperiod > 0 ? this.props.resource.creditperiod : 0;
	if(this.props.resource.supplierinvoicedate)
		tempObj.paymentduedate = new Date(new Date(this.props.resource.supplierinvoicedate).setDate(new Date(this.props.resource.supplierinvoicedate).getDate() + creditPeriod));

	this.props.updateFormState(this.props.form, tempObj);
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson: valueobj.name,
		phone: valueobj.phone,
		email: valueobj.email,
		mobile: valueobj.mobile
	});
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
	}
	if(address == 'supplieraddress') {
		tempObj.supplieraddress = addressobj.displayaddress;
		tempObj.supplieraddressid = addressobj.id;
		tempObj.suppliergstin = addressobj.gstin ? addressobj.gstin : this.props.resource.suppliergstin;

		if(this.props.app.feature.useMasterForAddresses)
			tempObj.supplierstate = addressobj.stateid_name ? addressobj.stateid_name : this.props.resource.supplierstate;
		else
			tempObj.supplierstate = addressobj.state ? addressobj.state : this.props.resource.supplierstate;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function callBackCustomerName () {
	this.controller.callBackCustomerNameInternal();
}

export function callBackCustomerNameInternal (initializeparam) {
	axios.get(`/api/partners?field=id,name,groups,payableaccountid,natureofpurchaseid,purchasecreditperiod,addressid,addresses/displayaddress/addressid,eccnumber,importerexportercode,exciserange,excisedivision,commissionerate,gstin,gstregtype,contactid,contacts/name/contactid,contacts/phone/contactid,contacts/email/contactid,contacts/mobile/contactid&filtercondition=partners.id=${this.props.resource.partnerid}`).then((response) => {
		if (response.data.message == 'success') {
			if(response.data.main.length == 0)
				return null;

			let tempObj = {
				creditperiod: response.data.main[0].purchasecreditperiod,
				payableaccountid: response.data.main[0].payableaccountid > 0 ? response.data.main[0].payableaccountid : this.props.app.defaultpayableaccountid,
				suppliergstregtype: response.data.main[0].gstregtype,
				suppliergstin: response.data.main[0].gstin,
				contactid: response.data.main[0].contactid,
				email: response.data.main[0].contactid_email,
				mobile: response.data.main[0].contactid_mobile,
				phone: response.data.main[0].contactid_phone,
				contactperson: response.data.main[0].contactid_name
			};

			if(initializeparam) {
				if(this.props.resource.suppliergstregtype)
					delete tempObj.suppliergstregtype;
				if(this.props.resource.suppliergstin)
					delete tempObj.suppliergstin;
			}

			this.props.updateFormState(this.props.form, tempObj);
			setTimeout(this.controller.calculateDueDate, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = valueobj.parentid;
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid: this.props.resource.partnerid, partneraddressid: this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase');
	promise.then((returnObject)=> {
		let tempObj = {};
		tempObj[`${itemstr}.itemid`] = returnObject.itemid;
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.drawingno`] = returnObject.drawingno;
		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.accountid`] = returnObject.expenseaccountid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.itemid_recommendedsellingprice`] = returnObject.itemid_recommendedsellingprice;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'purchaseinvoiceitems', itemstr);

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		if (item.index) {
			let itemCount = 0;
			tempObj.kititempurchaseinvoicedetails = [...this.props.resource.kititempurchaseinvoicedetails];
			for (var i = 0; i < tempObj.kititempurchaseinvoicedetails.length; i++) {
				if (tempObj.kititempurchaseinvoicedetails[i].rootindex == item.index)
					itemCount++;
			}
			for (var i = 0; i < itemCount; i++) {
				for (var j = 0; j < tempObj.kititempurchaseinvoicedetails.length; j++) {
					if (tempObj.kititempurchaseinvoicedetails[j].rootindex == item.index) {
						tempObj.kititempurchaseinvoicedetails.splice(j, 1);
						break;
					}
				}
			}

			if (returnObject.itemid_issaleskit) {
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = item.index;
					tempObj.kititempurchaseinvoicedetails.push(returnObject.kititems[i]);
				}
			}
		} else {
			let index = 0;
			for (var i = 0; i < this.props.resource.purchaseinvoiceitems.length; i++) {
				if (this.props.resource.purchaseinvoiceitems[i].index > index)
					index = this.props.resource.purchaseinvoiceitems[i].index;
			}
			tempObj[`${itemstr}.index`] = index + 1;

			if (returnObject.itemid_issaleskit) {
				tempObj.kititempurchaseinvoicedetails = [...this.props.resource.kititempurchaseinvoicedetails];
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = tempObj[`${itemstr}.index`];
					tempObj.kititempurchaseinvoicedetails.push(returnObject.kititems[i]);
				}
			}
		}

		this.props.updateFormState(this.props.form, tempObj);

		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
	}, (reason)=> {});
}

export function quantityOnChange(itemqty, item, itemstr) {
	let kititempurchaseinvoicedetails = this.props.resource.kititempurchaseinvoicedetails;
	let tempObj = {};
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;

	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
	}

	for (var i = 0; i < kititempurchaseinvoicedetails.length; i++) {
		if (item.index == kititempurchaseinvoicedetails[i].rootindex) {
			if (!kititempurchaseinvoicedetails[i].parentindex) {
				let tempIndex = kititempurchaseinvoicedetails[i].index;
				let tempQuantity = Number((kititempurchaseinvoicedetails[i].conversion * itemqty).toFixed(roundOffPrecisionStock));
				tempObj[`kititempurchaseinvoicedetails[${i}].quantity`] = tempQuantity;
				calculateQuantity(tempIndex, tempQuantity, item.index);
			}
		}
	}

	function calculateQuantity(tempIndex, tempQuantity, index) {
		for (var j = 0; j < kititempurchaseinvoicedetails.length; j++) {
			if (tempIndex == kititempurchaseinvoicedetails[j].parentindex && index == kititempurchaseinvoicedetails[j].rootindex) {
				let sectempQty = Number((kititempurchaseinvoicedetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
				tempObj[`kititempurchaseinvoicedetails[${j}].quantity`] = sectempQty;
				calculateQuantity(kititempurchaseinvoicedetails[j].index, sectempQty, index);
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.partnerid, partneraddressid: this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.partnerid, partneraddressid: this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function computeFinalRate() {
	setTimeout(()=> {
		let tempObj = {};
		for (var i = 0; i < this.props.resource.kititempurchaseinvoicedetails.length; i++) {
			if (!this.props.resource.kititempurchaseinvoicedetails[i].itemid_issaleskit) {
				if (this.props.resource.currencyid == this.props.app.defaultCurrency) {
					tempObj[`kititempurchaseinvoicedetails[${i}].ratelc`] = this.props.resource.kititempurchaseinvoicedetails[i].rate;
				} else {
					if (this.props.resource.currencyexchangerate)
						tempObj[`kititempurchaseinvoicedetails[${i}].ratelc`] = this.props.resource.kititempurchaseinvoicedetails[i].rate * this.props.resource.currencyexchangerate;
					else
						tempObj[`kititempurchaseinvoicedetails[${i}].ratelc`] = 0;
				}
			}
		}
		this.props.updateFormState(this.props.form, tempObj);
		setTimeout(() => {
			taxEngine(this.props, 'resource', 'purchaseinvoiceitems')
		}, 0);
	}, 0);
}

export function deleteInvoiceItem (index) {
	let itemCount = 0;
	let kititempurchaseinvoicedetails = [...this.props.resource.kititempurchaseinvoicedetails];
	let purchaseinvoiceitems = [...this.props.resource.purchaseinvoiceitems];

	for (var i = 0; i < kititempurchaseinvoicedetails.length; i++) {
		if (kititempurchaseinvoicedetails[i].rootindex == this.props.resource.purchaseinvoiceitems[index].index)
			itemCount++;
	}
	for (var i = 0; i < itemCount; i++) {
		for (var j = 0; j < kititempurchaseinvoicedetails.length; j++) {
			if (kititempurchaseinvoicedetails[j].rootindex == this.props.resource.purchaseinvoiceitems[index].index) {
				kititempurchaseinvoicedetails.splice(j, 1);
				break;
			}
		}
	}
	purchaseinvoiceitems.splice(index, 1);

	this.props.updateFormState(this.props.form, {
		kititempurchaseinvoicedetails,
		purchaseinvoiceitems
	});
	this.controller.computeFinalRate();
}

export function deleteKitItem(index) {
	let errorArray = [];
	let itemfound = false;
	let kititempurchaseinvoicedetails = [...this.props.resource.kititempurchaseinvoicedetails];
	let purchaseinvoiceitems = [...this.props.resource.purchaseinvoiceitems];
	let canDelete = true;

	for (var i = 0; i < purchaseinvoiceitems.length; i++) {
		if (purchaseinvoiceitems[i].index == kititempurchaseinvoicedetails[index].rootindex && purchaseinvoiceitems[i].sourceresource && purchaseinvoiceitems[i].sourceid)
			canDelete= false;
	}

	if(!canDelete) {
		let apiResponse = commonMethods.apiResult({
			data:{
				message: "You can't delete kit items in this transaction"
			}
		});
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	} else {
		for (var i = 0; i < kititempurchaseinvoicedetails.length; i++) {
			if (index != i && kititempurchaseinvoicedetails[index].rootindex == kititempurchaseinvoicedetails[i].rootindex && kititempurchaseinvoicedetails[index].parentindex == kititempurchaseinvoicedetails[i].parentindex)
				itemfound = true;
		}
		if (!itemfound)
			errorArray.push("Item is a only child for parent. Delete the Parent item");

		let itemCouFound = false;
		for (var i = 0; i < kititempurchaseinvoicedetails.length; i++) {
			if (index != i && kititempurchaseinvoicedetails[index].rootindex == kititempurchaseinvoicedetails[i].rootindex && !kititempurchaseinvoicedetails[i].parentindex)
				itemCouFound = true;
		}
		if (!itemCouFound)
			errorArray.push("Item is a only child for parent. Delete the Parent item");

		if (errorArray.length == 0) {
			let parentIndexArray = [kititempurchaseinvoicedetails[index].index];
			let rootindex = kititempurchaseinvoicedetails[index].rootindex;
			deleteArray(kititempurchaseinvoicedetails[index].rootindex, kititempurchaseinvoicedetails[index].index);

			function deleteArray(rootindex, index) {
				for (var i = 0; i < kititempurchaseinvoicedetails.length; i++) {
					if (kititempurchaseinvoicedetails[i].rootindex == rootindex && kititempurchaseinvoicedetails[i].parentindex == index) {
						parentIndexArray.push(kititempurchaseinvoicedetails[i].index)
						deleteArray(rootindex, kititempurchaseinvoicedetails[i].index);
					}
				}
			}

			for (var i = 0; i < parentIndexArray.length; i++) {
				for (var j = 0; j < kititempurchaseinvoicedetails.length; j++) {
					if (kititempurchaseinvoicedetails[j].rootindex == rootindex && kititempurchaseinvoicedetails[j].index == parentIndexArray[i]) {
						kititempurchaseinvoicedetails.splice(j, 1);
						break;
					}
				}
			}

			this.props.updateFormState(this.props.form, {kititempurchaseinvoicedetails});
		} else {
			let response = {
				data : {
					message : 'failure',
					error : utils.removeDuplicate(errorArray)
				}
			};
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	}
}

export function getAccountDetails (itemsidarray) {
	if(itemsidarray.length == 0)
		return null;

	axios.get(`/api/common/methods/gettaxaccountdetails?itemarray=${itemsidarray.join()}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = {};
			this.props.resource.purchaseinvoiceitems.forEach((invitem, index) => {
				response.data.main.forEach((item) => {
					if(invitem.itemid == item.itemid)
						tempObj[`purchaseinvoiceitems[${index}].accountid`] = item.expenseaccountid;
				});
			});
			this.props.updateFormState(this.props.form, tempObj);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}


export function kitdisable (item) {
	for(var i = 0; i < this.props.resource.purchaseinvoiceitems.length; i++) {
		if(this.props.resource.purchaseinvoiceitems[i].index == item.rootindex) {
			if(this.props.resource.purchaseinvoiceitems[i].sourceid)
				return true;
			else
				return false;
		}
	}
	return false;
}

export function save (param, confirm,confirm2) {
	this.updateLoaderFlag(true);

	if(param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm2 && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
		let message = {
			header : 'Cost Center Alert',
			body : `Cost Center not selected. Do you want to ${param} ?`,
			btnArray : ['Yes','No']
		};

		return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
			if(resparam)
				this.controller.save(param,confirm,true);
			else
				this.updateLoaderFlag(false);
		}));	
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/purchaseinvoices'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if(resparam)
				this.controller.save(param, true,confirm2);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/purchaseinvoices/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/purchaseinvoices");
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function createPayment () {
	this.props.history.push({pathname: '/createPaymentVoucher', params: {...this.props.resource, param: 'Purchase Invoices'}});
}

export function cancel () {
	this.props.history.goBack();
}

export function openEstimationItemPickDetails (index,itemstr) {
	let getBody = (closeModal) => {
		return (
			<EstimationItemPickingdetailsModal />
		)
	}
	this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} index={index} child={'purchaseinvoiceitems'} itemstr={itemstr} app={this.props.app} updateFormState={this.props.updateFormState} createOrEdit={this.props.createOrEdit} openModal={this.openModal} closeModal={closeModal} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}
