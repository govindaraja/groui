import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		type: 'Equipment'
	};

	if(this.props.location.params && this.props.location.params.param == 'copy')
		tempObj = this.props.location.params;

	if(this.props.location.params && this.props.location.params.param == 'contractenquiries') {
		const params = this.props.location.params;

		tempObj.itemid = params.itemid;
		tempObj.itemid_name = params.itemid_name;
		tempObj.partnerid = params.partnerid;
		tempObj.type = params.type;
		tempObj.description = params.description;
		this.customFieldsOperation('contractenquiryitems', tempObj, params, 'equipments');
		this.customFieldsOperation('itemmaster', tempObj, params, 'equipments');
	}

	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function typeOnChange() {
	let tempObj = {};
	if(this.props.resource.type == 'Facility') {
		tempObj.itemid = null;
		tempObj.description = null;
		tempObj.serialno = null;
		tempObj.invoiceno = null;
		tempObj.invoicedate = null;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function displaynameFocusfn() {
	let tempObj = {};
	if(!this.props.resource.displayname) {
		tempObj.displayname = `${this.props.resource.itemid_name ? this.props.resource.itemid_name : ''}${this.props.resource.serialno ? `-${this.props.resource.serialno}` : ''}`;
		this.props.updateFormState(this.props.form, tempObj);
	}
}

export function getItemById () {
	axios.get(`/api/equipments/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackProduct (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		itemid_name: valueobj.name,
		description: valueobj.description
	});
	this.controller.getItemDetails(valueobj.id);
}

export function getItemDetails(id) {
	let promise = commonMethods.getItemDetails(id, this.props.resource.customerid, null, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'equipments');

		this.props.updateFormState(this.props.form, tempObj);
	}, (reason)=>{});
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	tempObj.equipmentaddress = addressobj.displayaddress;
	tempObj.equipmentaddressid = addressobj.id;

	this.props.updateFormState(this.props.form, tempObj);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/equipments'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				if (this.props.isModal) {
					this.props.closeModal();
					this.props.callback(response.data.main);
				} else {
					this.props.history.replace(`/details/equipments/${response.data.main.id}`);
				}
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/equipments");
				} else {
					this.props.initialize(response.data.main);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function createContract() {

	if(this.props.resource.type == 'Facility'){
		this.controller.showTypeModal();
	}else{
		this.props.history.push({pathname: '/createContract', params: {...this.props.resource,contractfor:'Equipment', param: 'equipments'}});
	}	
}

export function cancel () {
	if(this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}


export function showTypeModal() {
	this.openModal({render: (closeModal) => {
		return <EquipmentTypeModal closeModal={closeModal} callback={(val) => {
			if(val) {
				this.props.history.push({pathname: '/createContract', params: {...this.props.resource,contractfor:val, param: 'equipments'}});
			}
		}}/>
	}, className: {
		content: 'react-modal-custom-class-30', overlay: 'react-modal-overlay-custom-class'
	},
	confirmModal: true
	});
}


class EquipmentTypeModal extends Component {
	constructor(props) {
		super(props);

		this.state = {};
		this.selectOnChange = this.selectOnChange.bind(this);
		this.closeTypeModal = this.closeTypeModal.bind(this);
	};

	selectOnChange(value) {
		this.setState({
			type: value
		});
	}

	closeTypeModal(param) {
		this.props.closeModal();
		this.props.callback(`${param ? this.state.type : ''}`);
	};

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title text-center">Select Contract For</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-8 offset-md-2 text-center expensereqtypemodal">
							<div className={`border rounded float-left p-2 reqtype ${this.state.type == 'Facility' ? 'active' : ''}`} onClick={()=>this.selectOnChange('Facility')}>
								Facility
							</div>
							<div className={`border rounded float-right p-2 reqtype ${this.state.type == 'O & M' ? 'active' : ''}`} onClick={()=>this.selectOnChange('O & M')}>
								O & M
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={() => this.closeTypeModal(false)}><i className="fa fa-times"></i>Close</button>
								<button type="button" className={`btn btn-sm gs-btn-success btn-width`} onClick={() => this.closeTypeModal(true)} disabled={!this.state.type}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}