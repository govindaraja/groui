import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, detailsSVC, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import { ItemRateField, ChildEditModal } from '../components/utilcomponents';
import ProfitanalyseModal from '../containers/profitabilityanalyseforquote';
import StockdetailsModal from '../containers/stockdetails';
import PotentialrevenueupdateModal from '../components/details/potentialrevenueupdatemodal';
import KititemaddModal from '../components/details/kititemaddmodal';
import AddonModal from '../components/details/addonitem';
import AnalyseRateModal from '../containers/analyseitemrate';
import EmailModal from '../components/details/emailmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		quotedate: new Date(new Date().setHours(0, 0, 0, 0)),
		quoteitems: [],
		kititemquotedetails: [],
		additionalcharges: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'copy') {
			tempObj = params;
		}

		if(params.param == 'Leads') {

			utils.assign(tempObj, params, [{'leadid' : 'id'}, {'leadid_currencyid' : 'currencyid'}, {'leadid_potentialrevenue' : 'potentialrevenue'}, 'currencyid', 'currencyexchangerate', {'dueon' : 'duedate'}, 'contactid', 'companyid', 'competitorid',  'email', 'mobile', 'phone', 'deliveryaddress', 'deliveryaddressid', 'deliverydate', 'customerreference', 'billingaddress', 'billingaddressid', 'salesperson', 'priority', {'expectedclosuredate' : 'duedate'}, 'territoryid', 'taxid', 'roundoffmethod', {'contactperson' : 'contactid_name'}]);
			this.customFieldsOperation('leads', tempObj, params, 'quotes');

			if(params.customerid > 0) {
				utils.assign(tempObj, params, ['customerid', 'customerid_name', 'pricelistid', {'creditperiod' : 'customerid_salescreditperiod'}]);
				tempObj.paymentterms = tempObj.paymentterms ? tempObj.paymentterms : params.customerid_paymentterms;
				tempObj.modeofdespatch = tempObj.modeofdespatch ? tempObj.modeofdespatch : params.customerid_modeofdespatch;
				tempObj.freight = tempObj.freight ? tempObj.freight : params.customerid_freight;
			}

			let quoteitemArray = params.leaditems;
			for (var i = 0; i < quoteitemArray.length; i++) {
				let tempChildObj = {
					index: i+1,
					alternateuom: quoteitemArray[i].uomconversiontype ? true : false
				};
				utils.assign(tempChildObj, quoteitemArray[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'description', 'drawingno', 'rate', 'taxid', 'itemid_uomgroupid', 'quantity', 'uomid', 'uomid_name', 'discountmode', 'discountquantity', 'itemid_keepstock', 'itemid_hasaddons', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'remarks', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingquantity', 'billingconversiontype', 'billingconversionfactor', 'itemid_itemcategorymasterid']);

				this.customFieldsOperation('leaditems', tempChildObj, quoteitemArray[i], 'quoteitems');
				tempObj.quoteitems.push(tempChildObj);
			}
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.getTandC();
		if(this.props.location.params && this.props.location.params.param == 'Leads')
			this.controller.onLoadKit(0, []);
		this.controller.callbackCustomerDetails();
		this.controller.currencyOnChange();
		this.controller.computeFinalRate();
	}, 0);
	this.updateLoaderFlag(false);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Quotation' and termsandconditions.isdefault`).then((response) => {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function tandcOnchange(id, valueobj) {
	if(id) {
		this.props.updateFormState(this.props.form, {
			tandc: valueobj.terms
		});
	}
}

export function onLoadKit (count, kititemquotedetails) {
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;
	if(count < this.props.resource.quoteitems.length) {
		if(this.props.resource.quoteitems[count].itemid_issaleskit) {
			let promise1 = commonMethods.getItemDetails(this.props.resource.quoteitems[count].itemid, {customerid:this.props.resource.customerid, partneraddressid:this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales');
			promise1.then((returnObject) => {
				returnObject.kititems.forEach((kititem) => {
					kititem.rootindex = this.props.resource.quoteitems[count].index;
					if(!kititem.parentindex) {
						let tempQuantity = Number((kititem.conversion * this.props.resource.quoteitems[count].quantity).toFixed(this.props.app.roundOffPrecisionStock));
						calculateQuantity(kititem.index, tempQuantity);
						kititem.quantity = tempQuantity;
					}
				});
				function calculateQuantity(tempIndex, tempQuantity) {
					returnObject.kititems.forEach((kititem) => {
						if (kititem.parentindex == tempIndex) {
							let sectempQty = Number((kititem.conversion * tempQuantity).toFixed(roundOffPrecisionStock));
							kititem.quantity = sectempQty;
							calculateQuantity(kititem.index, sectempQty);
						}
					});
				}
				returnObject.kititems.forEach((kititem) => {
					kititemquotedetails.push(kititem);
				});
				this.controller.onLoadKit(count+1, kititemquotedetails);
			}, (reason)=> {});
		} else {
			this.controller.onLoadKit(count+1, kititemquotedetails);
		}
	} else {
		this.props.updateFormState(this.props.form, {
			kititemquotedetails
		});
		this.controller.computeFinalRate();
	}
}

export function getItemById () {
	axios.get(`/api/quotes/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.tempfinaltotal = response.data.main.finaltotal;
			for (var i = 0; i < tempObj.quoteitems.length; i++) {
				if (tempObj.quoteitems[i].uomconversiontype)
					tempObj.quoteitems[i].alternateuom = true;
			}
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function currencyOnChange () {
	if (this.props.resource.pricelistid) {
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid) {
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});
		}
	}
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function deliverydateonchange() {
	let tempObj = {};
	for (var i = 0; i < this.props.resource.quoteitems.length; i++) {
		if (!this.props.resource.quoteitems[i].deliverydate || this.props.resource.quoteitems[i].deliverydate == null) {
			tempObj[`quoteitems[${i}].deliverydate`] = this.props.resource.deliverydate;
		}
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function competativequoteLoad() {
	let queryString = `/api/competativequotes?&field=id,quoteid,quotes/quoteno/quoteid,quotes/quotedate/quoteid,partners/name/competitorid,users/displayname/createdby,users/displayname/modifiedby,created,modified,finaltotal&filtercondition=competativequotes.quoteid=${this.props.resource.id}`;

	axios.get(`${queryString}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.updateFormState(this.props.form, {
				'competativequotesArray': response.data.main
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function leadnoCallback(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		leadid_potentialrevenue: valueobj.potentialrevenue,
		leadid_currencyid: valueobj.currencyid,
	});
}

export function callBackCustomerName (id, valueobj) {
	let tempObj = {
		customerid_name: valueobj.name,
		paymentterms: valueobj.paymentterms,
		modeofdespatch: valueobj.modeofdispatch,
		freight: valueobj.freight,
		territoryid: valueobj.territory,
		salesperson: this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		contactid: valueobj.contactid,
		email: valueobj.contactid_email,
		phone: valueobj.contactid_phone,
		mobile: valueobj.contactid_mobile,
		contactperson: valueobj.contactid_name,
		contactid_name: valueobj.contactid_name,
		contactid_email: valueobj.contactid_email,
		contactid_phone: valueobj.contactid_phone,
		contactid_mobile: valueobj.contactid_mobile,
		deliveryaddress: valueobj.addressid_displayaddress,
		billingaddress: valueobj.addressid_displayaddress,
		billingaddressid: valueobj.addressid,
		deliveryaddressid: valueobj.addressid,
	};

	this.customFieldsOperation('partners', tempObj, valueobj, 'quotes');

	if (valueobj.salespricelistid) {
		tempObj.pricelistid = valueobj.salespricelistid;
	} else if (valueobj.partnergroupid_pricelistid) {
		tempObj.pricelistid = valueobj.partnergroupid_pricelistid;
	} else {
		for (var i = 0; i < this.props.app.appSettings.length; i++) {
			if (this.props.app.appSettings[i].module == 'Sales' && this.props.app.appSettings[i].name == 'Pricelistid') {
				tempObj.pricelistid = this.props.app.appSettings[i].value.value;
				break;
			}
		}
	}
	if (valueobj.salescreditperiod)
		tempObj.creditperiod = valueobj.salescreditperiod;
	else
		tempObj.creditperiod = valueobj.partnergroupid_creditperiod;

	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackCustomerDetails() {
	let tempObj = {};
	if(this.props.resource.customerid > 0) {
		return axios.get(`/api/partners?field=id,paymentterms,modeofdispatch,freight,customergroups/creditperiod/partnergroupid&filtercondition=partners.iscustomer=true and partners.id=${this.props.resource.customerid}`).then((response) => {
			if (response.data.main.length > 0) {
				tempObj.creditperiod = this.props.resource.creditperiod ? this.props.resource.creditperiod : response.data.main[0].partnergroupid_creditperiod;
				this.props.updateFormState(this.props.form, tempObj);
			}
		});
	}
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson: valueobj.name,
		phone: valueobj.phone,
		email: valueobj.email,
		mobile: valueobj.mobile,
		contactid_name: valueobj.name,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile
	});
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = valueobj.parentid;
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid:this.props.resource.customerid, partneraddressid:this.props.resource.billingaddressid} , this.props.resource.pricelistid, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.drawingno`] = returnObject.drawingno;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_imageurl`] = returnObject.itemid_imageurl;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_addonhelptext`] = returnObject.addonhelptext;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.itemid_lastpurchasecost`] = returnObject.itemid_lastpurchasecost;
		tempObj[`${itemstr}.itemid_recommendedsellingprice`] = returnObject.itemid_recommendedsellingprice;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'quoteitems', itemstr);
		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		if (item.index) {
			let itemCount = 0;
			tempObj.kititemquotedetails = [...this.props.resource.kititemquotedetails];
			for (var i = 0; i < tempObj.kititemquotedetails.length; i++) {
				if (tempObj.kititemquotedetails[i].rootindex == item.index)
					itemCount++;
			}
			for (var i = 0; i < itemCount; i++) {
				for (var j = 0; j < tempObj.kititemquotedetails.length; j++) {
					if (tempObj.kititemquotedetails[j].rootindex == item.index) {
						tempObj.kititemquotedetails.splice(j, 1);
						break;
					}
				}
			}

			if (returnObject.itemid_issaleskit) {
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = item.index;
					tempObj.kititemquotedetails.push(returnObject.kititems[i]);
				}
			}
		} else {
			let index = 0;
			for (var i = 0; i < this.props.resource.quoteitems.length; i++) {
				if (this.props.resource.quoteitems[i].index > index) {
					index = this.props.resource.quoteitems[i].index;
				}
			}
			tempObj[`${itemstr}.index`] = index + 1;

			if (returnObject.itemid_issaleskit) {
				tempObj.kititemquotedetails = [...this.props.resource.kititemquotedetails];
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = tempObj[`${itemstr}.index`];
					tempObj.kititemquotedetails.push(returnObject.kititems[i]);
				}
			}
		}

		if(returnObject.hasaddons) {
			tempObj[`${itemstr}.baseitemrate`] = tempObj[`${itemstr}.rate`];
			tempObj[`${itemstr}.addonrate`] = 0;
		}

		this.props.updateFormState(this.props.form, tempObj);
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);

		if(returnObject.hasaddons) {
			this.controller.openItemAddOn(this.selector(this.props.fullstate, itemstr).index, itemstr);
		}
	}, (reason) => {});
}

export function quantityOnChange(itemqty, item, itemstr) {
	let kititemquotedetails = this.props.resource.kititemquotedetails;
	let tempObj = {};
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;

	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
	}

	for (var i = 0; i < kititemquotedetails.length; i++) {
		if (item.index == kititemquotedetails[i].rootindex) {
			if (!kititemquotedetails[i].parentindex) {
				let tempIndex = kititemquotedetails[i].index;
				let tempQuantity = Number((kititemquotedetails[i].conversion * itemqty).toFixed(roundOffPrecisionStock));
				tempObj[`kititemquotedetails[${i}].quantity`] = tempQuantity;
				calculateQuantity(tempIndex, tempQuantity, item.index);
			}
		}
	}

	function calculateQuantity(tempIndex, tempQuantity, index) {
		for (var j = 0; j < kititemquotedetails.length; j++) {
			if (tempIndex == kititemquotedetails[j].parentindex && index == kititemquotedetails[j].rootindex) {
				let sectempQty = Number((kititemquotedetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
				tempObj[`kititemquotedetails[${j}].quantity`] = sectempQty;
				calculateQuantity(kititemquotedetails[j].index, sectempQty, index);
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid:this.props.resource.customerid, partneraddressid:this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid:this.props.resource.customerid, partneraddressid:this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function conversionOnChange (item) {
	for (var i = 0; i < this.props.resource.quoteitems.length; i++) {
		if (item.rootindex == this.props.resource.quoteitems[i].index) {
			this.controller.quantityOnChange(this.props.resource.quoteitems[i].quantity, this.props.resource.quoteitems[i], `quoteitems[${i}]`);
			break;
		}
	}
}

export function openItemAddOn(index, itemstr) {
	let getBody = (closeModal) => {
		return (
			<AddonModal />
		)
	}
	this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} child={'quoteitems'} kit={'kititemquotedetails'} index={index} itemstr={itemstr} array={this.props.array} updateFormState={this.props.updateFormState} resource={this.props.resource} app={this.props.app} callback={this.controller.itemaddoncallback} openStockDetails={this.controller.openStockDetails} computeFinalRate={this.controller.computeFinalRate} checkCondition={this.checkCondition} closeModal={closeModal} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
}

export function itemaddoncallback () {
	for (var i = 0; i < this.props.resource.quoteitems.length; i++)
		this.controller.quantityOnChange(this.props.resource.quoteitems[i].quantity, this.props.resource.quoteitems[i], `quoteitems[${i}]`);
}

export function computeFinalRate() {
	setTimeout(() =>{
		let tempObj = {};
		for (var i = 0; i < this.props.resource.quoteitems.length; i++) {
			if (this.props.resource.quoteitems[i].itemid_hasaddons) {
				tempObj[`quoteitems[${i}].rate`] = (this.props.resource.quoteitems[i].baseitemrate ? this.props.resource.quoteitems[i].baseitemrate : 0) + ((this.props.resource.quoteitems[i].addonrate ? this.props.resource.quoteitems[i].addonrate : 0));
			}
		}
		this.props.updateFormState(this.props.form, tempObj);

		taxEngine(this.props, 'resource', 'quoteitems');
	}, 0);
}

export function deleteQuoteItem (index) {
	let itemCount = 0;
	let kititemquotedetails = [...this.props.resource.kititemquotedetails];
	let quoteitems = [...this.props.resource.quoteitems];

	for (var i = 0; i < kititemquotedetails.length; i++) {
		if (kititemquotedetails[i].rootindex == this.props.resource.quoteitems[index].index)
			itemCount++;
	}
	for (var i = 0; i < itemCount; i++) {
		for (var j = 0; j < kititemquotedetails.length; j++) {
			if (kititemquotedetails[j].rootindex == this.props.resource.quoteitems[index].index) {
				kititemquotedetails.splice(j, 1);
				break;
			}
		}
	}
	quoteitems.splice(index, 1);

	this.props.updateFormState(this.props.form, {
		kititemquotedetails,
		quoteitems
	});
	this.controller.computeFinalRate();
}

export function deleteKitItem(index) {
	let errorArray = [];
	let itemfound = false;
	let kititemquotedetails = [...this.props.resource.kititemquotedetails];

	for (var i = 0; i < kititemquotedetails.length; i++) {
		if (index != i && kititemquotedetails[index].rootindex == kititemquotedetails[i].rootindex && kititemquotedetails[index].parentindex == kititemquotedetails[i].parentindex) {
			itemfound = true;
		}
	}
	if (!itemfound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	let itemCouFound = false;
	for (var i = 0; i < kititemquotedetails.length; i++) {
		if (index != i && kititemquotedetails[index].rootindex == kititemquotedetails[i].rootindex && !kititemquotedetails[i].parentindex)
			itemCouFound = true;
	}
	if (!itemCouFound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	if (errorArray.length == 0) {
		let parentIndexArray = [kititemquotedetails[index].index];
		let rootindex = kititemquotedetails[index].rootindex;
		deleteArray(kititemquotedetails[index].rootindex, kititemquotedetails[index].index);

		function deleteArray(rootindex, index) {
			for (var i = 0; i < kititemquotedetails.length; i++) {
				if (kititemquotedetails[i].rootindex == rootindex && kititemquotedetails[i].parentindex == index) {
					parentIndexArray.push(kititemquotedetails[i].index)
					deleteArray(rootindex, kititemquotedetails[i].index);
				}
			}
		}

		for (var i = 0; i < parentIndexArray.length; i++) {
			for (var j = 0; j < kititemquotedetails.length; j++) {
				if (kititemquotedetails[j].rootindex == rootindex && kititemquotedetails[j].index == parentIndexArray[i]) {
					kititemquotedetails.splice(j, 1);
					break;
				}
			}
		}

		this.props.updateFormState(this.props.form, {kititemquotedetails});
	} else {
		let response = {
			data : {
				message : 'failure',
				error : utils.removeDuplicate(errorArray)
			}
		};
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
}

export function addKitItem() {
	this.openModal({
		render: (closeModal) => {
			return <KititemaddModal resource = {this.props.resource} child={'quoteitems'} kit={'kititemquotedetails'} array={this.props.array} callback={() =>{
				for (var i = 0; i < this.props.resource.quoteitems.length; i++) {
					this.controller.quantityOnChange(this.props.resource.quoteitems[i].quantity, this.props.resource.quoteitems[i], `quoteitems[${i}]`);
				}
			}} closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function openStockDetails (item) {
	this.openModal({render: (closeModal) => {return <StockdetailsModal resource = {this.props.resource} item={item} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function analyseitemRate (item) {
	this.openModal({render: (closeModal) => {return <AnalyseRateModal item={item} param="quotes" customerid={this.props.resource.customerid} customerid_name={this.props.resource.customerid_name} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function openPotentialrevenueModal(param, checkparam) {

	let leadObj = {
		potentialrevenue: this.props.resource.leadid_potentialrevenue,
		currencyid: this.props.resource.leadid_currencyid,
		quoteflag: !checkparam,
		currencydiffer: checkparam
	};

	return this.openModal({render: (closeModal) => {return <PotentialrevenueupdateModal resource = {this.props.resource} closeModal={closeModal} leadobj={leadObj} app={this.props.app} callback={(potenitalvalue) =>{this.props.change('pipelinevalue', potenitalvalue);setTimeout(() =>{this.controller.save(param, null, true);},0);}} />}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}});
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Customer',
			data : emailObj
		},
		url : '/api/quotes'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function save (param, confirm, leadidconfirm) {
	if (param == 'Send To Customer') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'quotes'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});

	} else if((param == 'Submit' || param == 'Approve') && this.props.resource.leadid > 0 && !leadidconfirm) {
		if(this.props.resource.currencyid == this.props.resource.leadid_currencyid) {
			if(!this.props.resource.leadid_potentialrevenue) {
				this.props.resource.pipelinevalue = this.props.resource.finaltotal;
				this.props.updateFormState(this.props.form, {
					pipelinevalue: this.props.resource.finaltotal
				});
				this.controller.save(param, null, true);
			} else if(this.props.resource.leadid_potentialrevenue != this.props.resource.finaltotal && (!this.props.resource.id  || this.props.resource.status == 'Draft')) {
				this.controller.openPotentialrevenueModal(param, false);
			} else if(this.props.resource.finaltotal != this.props.resource.tempfinaltotal && this.props.resource.id) {
				this.controller.openPotentialrevenueModal(param, false);
			} else {
				this.props.resource.pipelinevalue = this.props.resource.leadid_potentialrevenue;
				this.props.updateFormState(this.props.form, {
					pipelinevalue: this.props.resource.leadid_potentialrevenue
				});
				this.controller.save(param, null, true);
			}
		} else if(this.props.resource.currencyid != this.props.resource.leadid_currencyid) {
			if(!this.props.resource.leadid_potentialrevenue || (this.props.resource.leadid_potentialrevenue != this.props.resource.finaltotal && (!this.props.resource.id || this.props.resource.status == 'Draft'))) {
				this.controller.openPotentialrevenueModal(param, true);
			} else if(this.props.resource.finaltotal != this.props.resource.tempfinaltotal && this.props.resource.id) {
				this.controller.openPotentialrevenueModal(param, false);
			} else {
				this.props.resource.pipelinevalue = this.props.resource.leadid_potentialrevenue;
				this.props.updateFormState(this.props.form, {
					pipelinevalue: this.props.resource.leadid_potentialrevenue
				});
				this.controller.save(param, null, true);
			}
		} else {
			this.props.resource.pipelinevalue = this.props.resource.leadid_potentialrevenue;
			this.props.updateFormState(this.props.form, {
				pipelinevalue: this.props.resource.leadid_potentialrevenue
			});
			this.controller.save(param, null, true);
		}
	} else {
		this.updateLoaderFlag(true);

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && param !='Amend' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions: confirm ? true : false
			},
			url : '/api/quotes'
		}).then((response) => {
			if(param != 'Won' || (param == 'Won' && response.data.message !='success')) {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
					if (resparam) {
						if (this.props.resource.status == 'To Amend')
							this.controller.save(param, true);
						else
							this.controller.save(param, true, true);
					}
				}));
			}

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					this.props.history.replace(`/details/quotes/${response.data.main.id}`);
				} else {
					if (param == 'Delete') {
						this.props.history.replace("/list/quotes");
					} else if (param == 'Won') {
						this.props.initialize(response.data.main);
						let message = {
							header : 'Create Order?',
							body : 'Quotation status is changed. Would you like to create the order now?',
							btnArray : ['Yes','No']
						};
						this.props.openModal(modalService['confirmMethod'](message, (param) => {
							if(param) {
								this.controller.createOrder();
							}
						}));
					} else {
						let tempObj = response.data.main;
						tempObj.tempfinaltotal = response.data.main.finaltotal;
						this.props.initialize(tempObj);
					}
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function createOrder () {
	this.updateLoaderFlag(true);
	checkTransactionExist('orders', 'quotes', this.props.resource.id, this.openModal, (param) => {
		if(param)
			this.props.history.push({pathname: '/createOrder', params: {...this.props.resource, param: 'Quotes'}});
		this.updateLoaderFlag(false);
	});
}

export function createCompetativeQuote() {
	this.props.history.push({pathname: '/createCompetativeQuote', params: {...this.props.resource, param: 'Quotes'}});	
}

export function createProformaInvoice() {
	this.updateLoaderFlag(true);
	checkTransactionExist('proformainvoices', 'quotes', this.props.resource.id, this.openModal, (param) => {
		if(param)
			this.props.history.push({pathname: '/createProformaInvoice', params: {...this.props.resource, param: 'Quotes'}});
		this.updateLoaderFlag(false);
	});	
}

export function profitanalysedetails() {
	this.openModal({render: (closeModal) => {
		return <ProfitanalyseModal resource = {this.props.resource} param="quotes" app={this.props.app} closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class-60',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function cancel () {
	this.props.history.goBack();
}
