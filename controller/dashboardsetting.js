import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService } from '../utils/services';

export function onLoad() {
	this.setState({
		dontDirtyCheck: true
	});
	axios.get(`/api/roles?field=id,name,description,module`).then((response) => {
		if(response.data.message == 'success') {
			this.setState({
				roleArray : response.data.main
			});
			this.controller.initializeDashboard();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function initializeDashboard(name) {
	axios.get(`/api/initialize/initializedashboard?page=dashboardsettings`).then((response) => {

		this.props.initialize({
			dashboardArray: response.data.sort((a, b) => (a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0),
			name: name,
			alert: name ? getAlert(response.data, name) : null
		});

		this.updateLoaderFlag(false);
	});
}

export function getAlert(dashboardArray, name) {
	for(var i=0; i<dashboardArray.length; i++) {
		if(name == dashboardArray[i].name)
			return dashboardArray[i];
	}
}

export function alertOnChange (value, valueObj) {
	this.props.updateFormState(this.props.form, {
		alert: value ? valueObj : null
	});
}

export function getRoleIds() {
	return this.props.resource.alert ? this.props.resource.alert.roleid : [];
}

export function onRoleChange(value, role) {
	let tempObj = {
		alert: JSON.parse(JSON.stringify(this.props.resource.alert))
	};
	if(!value)
		tempObj.alert.roleid.splice(tempObj.alert.roleid.indexOf(role.id), 1);
	else if(tempObj.alert.roleid.indexOf(role.id) == -1)
		tempObj.alert.roleid.push(role.id);

	this.props.updateFormState(this.props.form, tempObj);
}


export function save (param, confirm) {

	if(!this.props.resource.alert)
		return null;

	this.updateLoaderFlag(true);

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : {
				name : this.props.resource.alert.name,
				reportname : this.props.resource.alert.reportName,
				roleid : this.props.resource.alert.roleid
			},
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/dashboardalerts'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			let dashboardArray = [...this.props.resource.dashboardArray];

			dashboardArray.forEach(item => {
				if(item.name == this.props.resource.alert.name)
					item.roleid = this.props.resource.alert.roleid;
			});

			this.props.updateFormState(this.props.form, {
				dashboardArray: dashboardArray
			});
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}