import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { ItemRateField } from '../components/utilcomponents';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		voucherdate: new Date(new Date().setHours(0, 0, 0, 0)),
		vouchertype: 'Credit Note',
		journalvoucheritems: [],
		salesreturnitems: [],
		journalvoucherinvoiceitems: [],
		additionalcharges: [],
		otheraccounts: []
	};

	if(this.props.location.params && this.props.location.params.param == 'Sales Returns') {
		if (this.props.location.params.againstorder)
			utils.assign(tempObj, this.props.location.params, [{'salesreturnid' : 'id'}, {'partnerid' : 'customerid'}, {'territoryid' : 'customerid_territory'}, {'partnergstin' : 'customerid_gstin'}, {'partnergstregtype' : 'customerid_gstregtype'}, 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'billingaddressid' : 'orderid_billingaddressid'}, {'deliveryaddressid' : 'orderid_deliveryaddressid'}, {'billingaddress' : 'orderid_billingaddress'}, {'deliveryaddress' : 'orderid_deliveryaddress'}, 'salesperson', {'returnsoid': 'orderid'}, {'salesreturnid_againstorder': 'againstorder'}]);
		else
			utils.assign(tempObj, this.props.location.params, [{'salesreturnid' : 'id'}, {'partnerid' : 'customerid'}, {'territoryid' : 'customerid_territory'}, {'partnergstin' : 'customerid_gstin'}, {'partnergstregtype' : 'customerid_gstregtype'}, 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', 'salesperson', {'salesreturnid_againstorder': 'againstorder'}]);

		this.customFieldsOperation('salesreturns', tempObj, this.props.location.params, 'journalvouchers');
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		if (this.props.resource.salesreturnid > 0) {
			if (this.props.resource.salesreturnid_againstorder)
				this.controller.getsalesreturnitemsonchange();
			else
				this.controller.getsalesreturnitemsforWithoutOrder();

			this.controller.currencyOnChange();
		}
	}, 0);
	
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/creditnotes/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			this.props.initialize(tempObj);

			if(tempObj.vouchertype == 'Receipt' && this.props.match.path != '/details/receiptvouchers/:id')
				return this.props.history.replace(`/details/receiptvouchers/${tempObj.id}`);
			if(tempObj.vouchertype == 'Payment' && this.props.match.path != '/details/paymentvouchers/:id')
				return this.props.history.replace(`/details/paymentvouchers/${tempObj.id}`);
			if(tempObj.vouchertype == 'Credit Note' && this.props.match.path != '/details/creditnotes/:id')
				return this.props.history.replace(`/details/creditnotes/${tempObj.id}`);
			if(tempObj.vouchertype == 'Debit Note' && this.props.match.path != '/details/debitnotes/:id')
				return this.props.history.replace(`/details/debitnotes/${tempObj.id}`);
			if(tempObj.vouchertype == 'Journal Voucher' && this.props.match.path != '/details/journalvouchers/:id')
				return this.props.history.replace(`/details/journalvouchers/${tempObj.id}`);

			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				},
				returnid: tempObj.salesreturnid
			});

			setTimeout(() => {
				if(this.props.resource.salesreturnid)
					this.controller.getSalesReturnItems();
				if(['Draft', 'Submitted', 'Revise'].indexOf(this.props.resource.status) >= 0)
					this.controller.computeFinalRate();

				this.controller.checkisAgainstorder(this.props.resource.salesreturnid);
			}, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getSalesReturnItems () {
	axios.get(`api/query/getsalesreturninvoiceitems?salesreturnid=${this.props.resource.salesreturnid}`).then((response) => {
		if(response.data.message == 'success') {
			this.props.updateFormState(this.props.form, {
				salesreturnitems: response.data.salesreturnarray
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
	}
}

export function salesreturnOnChange(value, valueobj) {
	if (this.state.returnid != value) {
		if (this.props.resource.journalvoucherinvoiceitems.length > 0) {
			let message = {
				header: 'Warning',
				bodyArray: [this.state.returnid ? 'You have added items from sales returns. This action will remove the items.' : 'You have added some items without sales returns. This action will remove the items.'],
				btnTitle: ` Do you want to continue?`,
				btnArray: ['Yes', 'No']
			};

			this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if (param)
					this.setState({
						returnid: value
					}, () => {
						this.controller.getJVinvoiceitems(valueobj);
					});
				else
					this.props.updateFormState(this.props.form, {
						salesreturnid: this.state.returnid
					});
			}));
		} else
			this.setState({
				returnid: value
			}, () => {
				this.controller.getJVinvoiceitems(valueobj);
			});
	} else
		this.controller.getJVinvoiceitems(valueobj);
}

export function getJVinvoiceitems(valueobj) {
	let tempObj = {
		partnerid: this.props.resource.salesreturnid ? valueobj.customerid : null,
		territoryid: this.props.resource.salesreturnid ? valueobj.customerid_territory : null,
		partnergstin: this.props.resource.salesreturnid ? valueobj.customerid_gstin : null,
		partnergstregtype: this.props.resource.salesreturnid ? valueobj.customerid_gstregtype : null,
		companyid: this.props.resource.salesreturnid ? valueobj.companyid : this.props.app.selectedcompanyid,
		defaultcostcenter: this.props.resource.salesreturnid ? valueobj.defaultcostcenter : null,
		currencyid: this.props.resource.salesreturnid ? valueobj.currencyid : this.props.app.defaultCurrency,
		currencyexchangerate: this.props.resource.salesreturnid ? valueobj.currencyexchangerate : null,
		billingaddressid: this.props.resource.salesreturnid ? valueobj.orderid_billingaddressid : null,
		deliveryaddressid: this.props.resource.salesreturnid ? valueobj.orderid_deliveryaddressid : null,
		billingaddress: this.props.resource.salesreturnid ? valueobj.orderid_billingaddress : null,
		deliveryaddress: this.props.resource.salesreturnid ? valueobj.orderid_deliveryaddress : null,
		salesperson: this.props.resource.salesreturnid ? valueobj.salesperson : (this.props.app.user.issalesperson ? this.props.app.user.id : null),
		returnsoid: this.props.resource.salesreturnid ? valueobj.orderid : null,
		salesreturnid_againstorder: this.props.resource.salesreturnid ? valueobj.againstorder : false,
		journalvoucheritems: [],
		salesreturnitems: [],
		journalvoucherinvoiceitems: [],
		additionalcharges: [],
		otheraccounts: []
	};

	this.props.updateFormState(this.props.form, tempObj);

	setTimeout(() => {
		if (this.props.resource.salesreturnid) {
			if (valueobj.againstorder)
				this.controller.getsalesreturnitemsonchange();
			else
				this.controller.getsalesreturnitemsforWithoutOrder();
		} else
			this.controller.computeFinalRate();
	}, 0);
}

export function checkisAgainstorder(id) {
	if (!id) {
		this.props.updateFormState(this.props.form, {
			salesreturnid_againstorder: false
		});

		return null;
	}

	axios.get(`/api/salesreturns/${id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.updateFormState(this.props.form, {
				salesreturnid_againstorder: response.data.main.againstorder
			});

			setTimeout(() => {
				if (!this.props.resource.salesreturnid_againstorder)
					this.controller.getsalesreturnitemsforWithoutOrder(true);
			}, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getsalesreturnitemsonchange() {
	this.props.array.removeAll('salesreturnitems');
	this.props.array.removeAll('journalvoucherinvoiceitems');

	axios.get(`/api/query/getsalesreturninvoiceitems?salesreturnid=${this.props.resource.salesreturnid}`).then((response) => {
		if(response.data.message == 'success') {
			let errArray = [], salesreturnarray = response.data.salesreturnarray, invoicearray = response.data.invoicearray;

			for (var i = 0; i < salesreturnarray.length; i++) {
				let tempQuantity = 0;
				for (var j = 0; j < invoicearray.length; j++) {
					if (salesreturnarray[i].orderitemsid == invoicearray[j].salesinvoiceitemsid_orderitemsid)
						tempQuantity += invoicearray[j].salesinvoiceitemsid_quantity;
				}
				if (tempQuantity < salesreturnarray[i].qtyauthorized)
					errArray.push(`Invoiced quantity is less than returned quantity for item  ${salesreturnarray[i].itemid_name} Please create invoice and then create credit note.`);
			}
			if (errArray.length > 0) {
				this.props.updateFormState(this.props.form, {
					salesreturnitems: salesreturnarray,
					journalvoucherinvoiceitems: invoicearray
				});
				let errresponse = {
					data: {
						message: 'failure',
						error: errArray
					}
				}
				let apiResponse = commonMethods.apiResult(errresponse);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			} else {
				for (var i = 0; i < salesreturnarray.length; i++) {
					let tempQuantity = 0,
						timesfound = 0;
					for (var j = 0; j < invoicearray.length; j++) {
						if (salesreturnarray[i].orderitemsid == invoicearray[j].salesinvoiceitemsid_orderitemsid) {
							tempQuantity += invoicearray[j].salesinvoiceitemsid_quantity;
							timesfound++;
						}
					}
					if ((timesfound == 1) || (tempQuantity == salesreturnarray[i].qtyauthorized)) {
						let tempObj = {};
						for (var j = 0; j < invoicearray.length; j++) {
							if (salesreturnarray[i].orderitemsid == invoicearray[j].salesinvoiceitemsid_orderitemsid) {
								if (timesfound == 1)
									invoicearray[j].quantity = salesreturnarray[i].qtyauthorized;
								else
									invoicearray[j].quantity = salesreturnarray[i].salesinvoiceitemsid_quantity;
							}
						}
					}
				}
				this.props.updateFormState(this.props.form, {
					salesreturnitems: salesreturnarray,
					journalvoucherinvoiceitems: invoicearray
				});
				this.controller.computeFinalRate();
			}
		}
	});
}

export function getsalesreturnitemsforWithoutOrder (invFlag) {
	this.updateLoaderFlag(true);

	let Returnitems = [],
		itemidArray = [];

	axios.get(`/api/salesreturnitems?field=id,description,quantity,qtyauthorized,uomid,taxid,rate,costrate,finalrate,amount,warehouseid,itemid,itemmaster/name/itemid,itemmaster/uomgroupid/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/keepstock/itemid,itemmaster/issaleskit/itemid,uom/name/uomid,displayorder,uomconversionfactor,uomconversiontype&filtercondition=salesreturnitems.isdeleted=FALSE AND salesreturnitems.parentid=${this.props.resource.salesreturnid}`).then((response) => {
		if (response.data.main.length > 0) {
			response.data.main.forEach((item) => {
				itemidArray.push(item.itemid);

				let tempObj = item;

				tempObj.orderitemsid = 'Without Order';

				Returnitems.push(tempObj);
			});

			this.props.updateFormState(this.props.form, {
				salesreturnitems: Returnitems
			});

			if (!invFlag)
				this.controller.getAccountDetails(itemidArray);
			else
				this.controller.computeFinalRate();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function getAccountDetails (itemidArray) {
	if(itemidArray.length == 0)
		return null;

	axios.get(`/api/common/methods/gettaxaccountdetails?itemarray=${itemidArray.join()}`).then((response) => {
		if (response.data.message == 'success') {
			let jvInvoiceitems = [],
				accObj = {};

			response.data.main.forEach((item) => {
				accObj[item.itemid] = item.incomeaccountid;
			});

			this.props.resource.salesreturnitems.forEach((item, index) => {
				let tempObj = {
					salesinvoiceid: null,
					salesinvoiceid_invoiceno: null,
					salesinvoiceid_invoicedate: null,
					salesinvoiceitemsid: null,
					accountid: accObj[item.itemid],
					uomid: item.uomid,
					uomid_name: item.uomid_name,
					uomconversiontype: item.uomconversiontype,
					uomconversionfactor: item.uomconversionfactor,
					rate: item.rate,
					itemid: item.itemid,
					itemid_name: item.itemid_name,
					itemid_keepstock: item.itemid_keepstock,
					description: item.description,
					discount: null,
					salesinvoiceitemsid_quantity: null,
					quantity: item.quantity,
					taxid: item.taxid,
					amount: item.amount,
					salesreturnitemid: item.id,
					displayorder: item.displayorder
				};

				jvInvoiceitems.push(tempObj);
			});

			this.props.updateFormState(this.props.form, {
				journalvoucherinvoiceitems: jvInvoiceitems
			});

			this.controller.computeFinalRate();

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function callBackPartner(id, valueobj) {
	let tempObj = {
		salesperson: this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		territoryid: valueobj.territory,
		partnergstin: valueobj.gstin,
		partnergstregtype: valueobj.gstregtype,
		billingaddress: valueobj.addressid_displayaddress,
		deliveryaddress: valueobj.addressid_displayaddress,
		billingaddressid: valueobj.addressid,
		deliveryaddressid: valueobj.addressid,
		partnerstate: valueobj.addressid_state
	};

	this.customFieldsOperation('partners', tempObj, valueobj, 'journalvouchers');

	this.props.updateFormState(this.props.form, tempObj);
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
		tempObj.partnergstin = addressobj.gstin ? addressobj.gstin : this.props.resource.partnergstin;
		if(this.props.app.feature.useMasterForAddresses)
			tempObj.partnerstate = addressobj.stateid_name ? addressobj.stateid_name : this.props.resource.partnerstate;
		else
			tempObj.partnerstate = addressobj.state ? addressobj.state : this.props.resource.partnerstate;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackProduct(id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, this.props.resource.partnerid, null, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};

		tempObj[`${itemstr}.itemid`] = returnObject.itemid;
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.itemid_itemtype`] = returnObject.itemid_itemtype;
		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.accountid`] = returnObject.itemid_incomeaccountid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.itemid_recommendedsellingprice`] = returnObject.itemid_recommendedsellingprice;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'journalvoucherinvoiceitems', itemstr);

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if (tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		this.props.updateFormState(this.props.form, tempObj);

		let tempchilditem = this.selector(this.props.fullstate, itemstr);

		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
	}, (reason) => {});
}

export function quantityOnChange(itemqty, item, itemstr) {
	if (item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);

		this.props.updateFormState(this.props.form, {
			[`${itemstr}.billingquantity`]: Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))
		});
	}

	this.controller.computeFinalRate();
}

export function uomOnchange(id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, this.props.resource.partnerid, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if (item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));

			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange(value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};

	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, this.props.resource.partnerid, null, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if (item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));

				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if (item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;

		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);

	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});

	this.controller.computeFinalRate();
}

export function computeFinalRate () {
	taxEngine(this.props, 'resource', 'journalvoucherinvoiceitems');
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function save (param,confirm) {
	this.updateLoaderFlag(true);

	if(param !='Cancel' && param !='Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
		let message = {
			header : 'Cost Center Alert',
			body : `Cost Center not selected. Do you want to ${param} ?`,
			btnArray : ['Yes','No']
		};

		return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
			if(resparam)
				this.controller.save(param,true);
			else
				this.updateLoaderFlag(false);
		}));	
	}

	if(['Cancel', 'Delete', 'Update'].includes(param))
		return this.controller.saveCreditNote(param);

	let errArray = [];
	if (this.props.resource.salesreturnid > 0) {
		if (this.props.resource.salesreturnitems.length == 0)
			errArray.push("Sales Return Items have atleast one item");

		if (this.props.resource.salesreturnid_againstorder)
			for (var i = 0; i < this.props.resource.salesreturnitems.length; i++) {
				let tempQuantity = 0,
					salesReturnQty = 0;
				for (var j = 0; j < this.props.resource.salesreturnitems.length; j++) {
					if (this.props.resource.salesreturnitems[i].orderitemsid == this.props.resource.salesreturnitems[j].orderitemsid) {
						salesReturnQty += this.props.resource.salesreturnitems[j].qtyauthorized;
					}
				}
				for (var j = 0; j < this.props.resource.journalvoucherinvoiceitems.length; j++) {
					if (this.props.resource.salesreturnitems[i].orderitemsid == this.props.resource.journalvoucherinvoiceitems[j].salesinvoiceitemsid_orderitemsid) {
						tempQuantity += this.props.resource.journalvoucherinvoiceitems[j].quantity;
					}
				}
				if (tempQuantity != salesReturnQty) {
					errArray.push(`Sales Return quantity is not equal to Credit Quantity for item  ${this.props.resource.salesreturnitems[i].itemid_name} Please Give credit Quantity equal to Sales Return Quantity`);
				}
			}
		else
			this.props.resource.journalvoucherinvoiceitems.forEach((item) => {
				this.props.resource.salesreturnitems.forEach((srtnitem) => {
					if (item.salesreturnitemid == srtnitem.id) {
						if (item.quantity != srtnitem.qtyauthorized)
							errArray.push(`Sales Return quantity is not equal to Credit Quantity for item  ${srtnitem.itemid_name} Please Give credit Quantity equal to Sales Return Quantity`);
					}
				});
			});
	} else {
		/*let totalCredit = 0, totalDebit = 0;
		for (var i = 0; i < this.props.resource.journalvoucheritems.length; i++) {
			let tempCredit = this.props.resource.journalvoucheritems[i].credit ? this.props.resource.journalvoucheritems[i].credit : 0;
			let tempDebit = this.props.resource.journalvoucheritems[i].debit ? this.props.resource.journalvoucheritems[i].debit : 0;
			if (tempCredit == 0 && tempDebit == 0) {
				errArray.push("Item " + i + " Credit Or Debit amount must be greater than 0");
			}
			if (tempCredit > 0 && tempDebit > 0) {
				errArray.push("Item " + i + " must be either credit or debit");
			}
			totalCredit += tempCredit;
			totalDebit += tempDebit;
		}
		if (totalCredit != totalDebit) {
			errArray.push("Sum of Credit amount must be equal to Sum of Debit Amount");
		}*/
	}
	if (errArray.length > 0) {
		let response = {
			data: {
				message : 'failure',
				error : errArray
			}
		}
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		this.updateLoaderFlag(false);
	} else {
		this.controller.saveCreditNote(param);
	}
}

export function saveCreditNote (param, confirm) {
	this.updateLoaderFlag(true);

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/creditnotes'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.saveCreditNote(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/creditnotes/${response.data.main.id}`);
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/creditnotes");
				} else {
					this.props.initialize(response.data.main);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
