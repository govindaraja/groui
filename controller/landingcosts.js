import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';
import CostCenterPickingdetailsModal from '../components/details/costcenterpickingdetailsmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();

	this.setState({
		allocationTypeArray: [{
			name: 'Value',
			displayname: 'By Value'
		}, {
			name: 'Quantity',
			displayname: 'By Quantity'
		}]
	});
}

export function initialiseState() {
	let tempObj = {
		companyid: this.props.app.selectedcompanyid,
		date: new Date(new Date().setHours(0, 0, 0, 0)),
		ispurchasereturn: true,
		landingcostitems: [],
		landingcostreceiptnoteitems: []
	};

	if(this.props.location.params && this.props.location.params.param == 'Receipt Notes') {
		let params = this.props.location.params;
		let receiptnoteitemsArray = params.receiptnoteitems;
		let kititemreceiptdetailsArray = params.kititemreceiptdetails;

		utils.assign(tempObj, params, ['companyid', {'receiptnoteid' : 'id'}, {'receiptnoteid_receiptnotenumber' : 'receiptnotenumber'}, {'receiptnoteid_receiptnotedate' : 'receiptnotedate'}, 'defaultcostcenter']);

		for (var i = 0; i < receiptnoteitemsArray.length; i++) {
			if (receiptnoteitemsArray[i].stockqty > 0 && receiptnoteitemsArray[i].itemid_keepstock && !receiptnoteitemsArray[i].itemid_issaleskit) {
				let tempChildObj = {
					receiptnoteitemid_landingcost: receiptnoteitemsArray[i].receiptnoteitemid_landingcost ? receiptnoteitemsArray[i].receiptnoteitemid_landingcost : 0
				};

				utils.assign(tempChildObj, receiptnoteitemsArray[i], ['itemid', 'itemid_name', 'description', {'quantity' : 'stockqty'}, {'receiptnoteitemid_valuationrate' : 'valuationrate'}, {'receiptnoteitemid' : 'id'}, 'warehouseid', 'warehouseid_accountid', {'uomid' : 'itemid_stockuomid'}]);

				tempObj.landingcostreceiptnoteitems.push(tempChildObj);
			}
		}

		for (var i = 0; i < kititemreceiptdetailsArray.length; i++) {
			let warehouseid, warehouseid_accountid;
			for(var j = 0; j < receiptnoteitemsArray.length; j++){
				if(kititemreceiptdetailsArray[i].rootindex == receiptnoteitemsArray[j].index) {
					warehouseid = receiptnoteitemsArray[j].warehouseid;
					warehouseid_accountid = receiptnoteitemsArray[j].warehouseid_accountid;
					break;
				}
			}

			if (kititemreceiptdetailsArray[i].stockqty > 0 && kititemreceiptdetailsArray[i].itemid_keepstock && !kititemreceiptdetailsArray[i].itemid_issaleskit) {
				let tempKitObj = {
					receiptnotekititemid_landingcost: kititemreceiptdetailsArray[i].landingcost ? kititemreceiptdetailsArray[i].landingcost : 0,
					warehouseid: warehouseid,
					warehouseid_accountid: warehouseid_accountid
				};

				utils.assign(tempKitObj, kititemreceiptdetailsArray[i], ['itemid', 'itemid_name', 'description', {'quantity' : 'stockqty'}, {'receiptnotekititemid_valuationrate' : 'valuationrate'}, {'receiptnotekititemid' : 'id'}, {'uomid' : 'itemid_stockuomid'}]);

				tempObj.landingcostreceiptnoteitems.push(tempKitObj);
			}
		}
	}

	this.props.initialize(tempObj);
	
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/landingcosts/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function landingcostOnchange (value, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.landingcostid_name`] = valueobj.name;
	tempObj[`${itemstr}.type`] = valueobj.basedon;
	tempObj[`${itemstr}.accountid`] = valueobj.accountid;

	this.props.updateFormState(this.props.form, tempObj);
}

export function computeFinalRate () {
	let tempObj = {};

	for (var i = 0; i < this.props.resource.landingcostreceiptnoteitems.length; i++) {
		tempObj[`landingcostreceiptnoteitems[${i}].amount`] = Number(((this.props.resource.landingcostreceiptnoteitems[i].receiptnoteitemid ? this.props.resource.landingcostreceiptnoteitems[i].receiptnoteitemid_valuationrate : this.props.resource.landingcostreceiptnoteitems[i].receiptnotekititemid_valuationrate) * this.props.resource.landingcostreceiptnoteitems[i].quantity).toFixed(this.props.app.roundOffPrecision));
		tempObj[`landingcostreceiptnoteitems[${i}].landingcost`] = 0;
	}

	for (var j = 0; j < this.props.resource.landingcostitems.length; j++) {
		if (this.props.resource.landingcostitems[j].amount > 0) {
			if (this.props.resource.landingcostitems[j].type == "Value") {
				let totalamount = 0;
				for (var i = 0; i < this.props.resource.landingcostreceiptnoteitems.length; i++)
					totalamount += this.props.resource.landingcostreceiptnoteitems[i].amount;

				if (totalamount > 0) {
					for (var i = 0; i < this.props.resource.landingcostreceiptnoteitems.length; i++)
						tempObj[`landingcostreceiptnoteitems[${i}].landingcost`] = Number((tempObj[`landingcostreceiptnoteitems[${i}].landingcost`] + (this.props.resource.landingcostitems[j].amount * (this.props.resource.landingcostreceiptnoteitems[i].amount / totalamount))).toFixed(this.props.app.roundOffPrecision));
				}
			} else {
				let totalqty = 0;
				for (var i = 0; i < this.props.resource.landingcostreceiptnoteitems.length; i++)
					totalqty += this.props.resource.landingcostreceiptnoteitems[i].quantity;
				if (totalqty > 0) {
					for (var i = 0; i < this.props.resource.landingcostreceiptnoteitems.length; i++)
						tempObj[`landingcostreceiptnoteitems[${i}].landingcost`] = Number((tempObj[`landingcostreceiptnoteitems[${i}].landingcost`] + ((this.props.resource.landingcostitems[j].amount / totalqty) * this.props.resource.landingcostreceiptnoteitems[i].quantity)).toFixed(this.props.app.roundOffPrecision));
				}
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
}

export function getAllocatedAmount (param) {
	let childName = param == 'landingcostreceipttotal' ? 'landingcostreceiptnoteitems' : 'landingcostitems';
	let childFieldName = param == 'landingcostreceipttotal' ? 'landingcost' : 'amount';
	let totalamount = 0;
	
	for(var i = 0; i < this.props.resource[childName].length;i++)
		totalamount += (this.props.resource[childName][i][childFieldName] ? this.props.resource[childName][i][childFieldName] : 0);

	return Number(totalamount.toFixed(this.props.app.roundOffPrecision));
}

export function openCostCenterPickDetails (item, itemstr) {
	this.openModal({render: (closeModal) => {
		return <CostCenterPickingdetailsModal item={item} resource={this.props.resource} itemstr={itemstr} amountstr={'amount'} app={this.props.app} form={this.props.form} updateFormState={this.props.updateFormState} openModal={this.openModal} closeModal={closeModal} />
	}, className: {
		content: 'react-modal-custom-class-60',
		overlay: 'react-modal-overlay-custom-class'
	}, confirmModal: true });
}

export function save (param, confirm,confirm2) {
	this.updateLoaderFlag(true);

	if(param !='Update' && param !='Cancel' && param !='Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm2 && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
		let message = {
			header : 'Cost Center Alert',
			body : `Cost Center not selected. Do you want to ${param} ?`,
			btnArray : ['Yes','No']
		};

		return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
			if(resparam)
				this.controller.save(param, confirm,true);
			else
				this.updateLoaderFlag(false);
		}));	
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/landingcosts'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
			if (resparam)
				this.controller.save(param, true,confirm2);
		});

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/landingcosts/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/landingcosts");
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
