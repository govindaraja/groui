import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({locationidArray: []});
		this.controller.getLocationdetails();
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getLocationdetails () {
	axios.get("/api/locations?&field=id,name").then((response) => {
		if(response.data.message == 'success') {
			let locationidArray = response.data.main;
			locationidArray.sort((a, b)=> {
				return a.name - b.name;
			});
			for(var i = 0; i < locationidArray.length; i++) {
				this.props.array.push('locationidArray', locationidArray[i]);
			}
		}
	});
}

export function getItemById () {
	axios.get(`/api/holidays/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.controller.getLocationdetails();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/holidays'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		});

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/holidays/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/holidays");
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
