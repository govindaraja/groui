import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		companyid: this.props.app.selectedcompanyid,
		materialrequirementdate: new Date(new Date().setHours(0, 0, 0, 0)),
		materialrequirementitems: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;
		let projectitemArray = params.projectitems;

		if(params.param == 'copy') {
			tempObj = params;
		}

		if(params.param == 'Projects') {
			for (var i = 0; i < projectitemArray.length; i++) {
				if(projectitemArray[i].itemid_itemtype == 'Product') {

					let tempChildObj = {
						index: i+1,
						alternateuom: projectitemArray[i].uomconversiontype ? true : false
					};

					utils.assign(tempChildObj, projectitemArray[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_keepstock', 'itemid_uomgroupid', 'description', 'quantity', 'uomid', 'uomid_name', 'uomconversionfactor', 'uomconversiontype']);

					this.customFieldsOperation('projectitems', tempChildObj, projectitemArray[i], 'materialrequirementitems');
					tempObj.materialrequirementitems.push(tempChildObj);
				}
			}

			utils.assign(tempObj, params, [{'projectid' : 'id'}, 'remarks', 'customerid']);

			this.customFieldsOperation('projects', tempObj, params, 'materialrequirements');
		}
	}

	this.props.initialize(tempObj);
	
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/materialrequirements/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			for (var i = 0; i < tempObj.materialrequirementitems.length; i++) {
				if (tempObj.materialrequirementitems[i].uomconversiontype)
					tempObj.materialrequirementitems[i].alternateuom = true;
			}
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, this.props.resource.customerid, null, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.itemid_itemtype`] = returnObject.itemid_itemtype;
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.itemid_recommendedsellingprice`] = returnObject.itemid_recommendedsellingprice;
		tempObj[`${itemstr}.uomconversionfactor`] = returnObject.alternateuom ? returnObject.uomconversionfactor : null;
		tempObj[`${itemstr}.uomconversiontype`] = returnObject.alternateuom ? returnObject.uomconversiontype : null;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'materialrequirementitems', itemstr);

		this.props.updateFormState(this.props.form, tempObj);
	}, (reason) => {});
}

export function uomOnchange (id, valueobj, item, itemstr) {
	let tempObj = {};

	if (valueobj.id == id && valueobj.alternateuom) {
		tempObj[`${itemstr}.uomconversiontype`] = valueobj.conversiontype;
		tempObj[`${itemstr}.uomconversionfactor`] = valueobj.conversionrate;
		tempObj[`${itemstr}.alternateuom`] = valueobj.alternateuom;
	} else {
		tempObj[`${itemstr}.uomconversiontype`] = null;
		tempObj[`${itemstr}.uomconversionfactor`] = null;
		tempObj[`${itemstr}.alternateuom`] = false;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param !='Update' && param !='Cancel' && param !='Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/materialrequirements'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/materialrequirements/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/materialrequirements");
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
