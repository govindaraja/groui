import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, pageValidation } from '../utils/services';
import SearchForOrder from '../components/details/searchfororder';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initializeState();
	else
		this.controller.getItemById();
}

export function initializeState () {
	let tempObj = {
		isreplacement: false,
		issuecreditnote: false,
		returndate: new Date(new Date().setHours(0, 0, 0, 0)),
		companyid: this.props.app.selectedcompanyid,
		companyid_name: this.props.app.selectedCompanyDetails.name,
		currencyid: this.props.app.defaultCurrency,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		salesreturnitems: []
	};

	this.props.initialize(tempObj);

	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/salesreturns/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callAgainstOrder (val) {
	if (this.props.resource.salesreturnitems.length > 0) {
		let message = {
			header: 'Warning',
			bodyArray: [!val ? 'You have added items from an order. This action will remove the Order No and items.' : 'You have added some items without Order No. This action will remove the items.'],
			btnTitle: ` Do you want to continue?`,
			btnArray: ['Yes', 'No']
		};

		this.props.openModal(modalService['confirmMethod'](message, (param) => {
			if (param)
				this.props.updateFormState(this.props.form, {
					orderid: null,
					customerid: null,
					salesperson: null,
					defaultcostcenter: null,
					warehouseid: null,
					pricelistid: null,
					currencyid: this.props.app.defaultCurrency,
					currencyexchangerate: null,
					roundoffmethod: this.props.app.defaultRoundOffMethod,
					issuecreditnote: val ? false : true,
					salesreturnitems: []
				});
			else
				this.props.updateFormState(this.props.form, {
					againstorder: !val
				});
		}));
	} else
		this.props.updateFormState(this.props.form, {
			orderid: null,
			customerid: null,
			salesperson: null,
			defaultcostcenter: null,
			warehouseid: null,
			pricelistid: null,
			currencyid: this.props.app.defaultCurrency,
			currencyexchangerate: null,
			roundoffmethod: this.props.app.defaultRoundOffMethod,
			issuecreditnote: val ? false : true,
			salesreturnitems: []
		});
}

export function callbackOrder (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		customerid : valueobj.customerid,
		customerid_name : valueobj.customerid_name,
		companyid : valueobj.companyid,
		companyid_name : valueobj.companyid_name,
		salesperson : valueobj.salesperson,
		currencyid: valueobj.currencyid,
		currencyexchangerate: valueobj.currencyexchangerate,
		defaultcostcenter: valueobj.defaultcostcenter,
		warehouseid: valueobj.warehouseid,
		pricelistid: valueobj.pricelistid
	});

	setTimeout(this.controller.orderOnChange, 0);
}


export function orderOnChange () {
	this.props.array.removeAll('salesreturnitems');

	if(!this.props.resource.orderid)
		return null;

	this.updateLoaderFlag(true);

	axios.get(`/api/orderitems?field=id,itemid,itemmaster/name/itemid,deliveredqty,uomid,uom/name/uomid,uomconversionfactor,uomconversiontype&skip=0&filtercondition=orderitems.parentid=${this.props.resource.orderid}`).then((response) => {
		if(response.data.message == 'success') {
			let orderItemsIdArray = response.data.main.map((item) => {
				return item.id;
			});

			this.controller.getDCItems(orderItemsIdArray);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getDCItems (array) {
	this.updateLoaderFlag(true);

	axios.get(`/api/deliverynoteitems?field=id,parentid,itemid,itemmaster/name/itemid,quantity,uomid,uom/name/uomid,uomconversionfactor,uomconversiontype,orderitems/displayorder/orderitemsid&skip=0&filtercondition=deliverynotes_parentid.status IN ('Approved', 'Dispatched') AND deliverynoteitems.orderitemsid IN (${array.join(',')})`).then((response) => {
		if(response.data.message == 'success') {
			response.data.main = response.data.main.sort((a, b) => {
				return a.orderitemsid_displayorder - b.orderitemsid_displayorder
			});

			let sritemsArray = [];
			response.data.main.forEach((item) => {
				sritemsArray.push({
					deliverynoteitemsid : item.id,
					itemid : item.itemid,
					itemid_name : item.itemid_name,
					deliverynoteitemsid_parentid : item.parentid,
					deliverynoteitemsid_quantity : item.quantity,
					deliverynoteitemsid_uomid : item.uomid,
					deliverynoteitemsid_uomname : item.uomid_name,
					qtyauthorized : item.quantity,
					quantity : item.quantity,
					uomid : item.uomid,
					uomid_name : item.uomid_name,
					uomconversionfactor : item.uomconversionfactor,
					uomconversiontype : item.uomconversiontype
				});
			});

			this.props.updateFormState(this.props.form, {
				'salesreturnitems': sritemsArray
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/salesreturns'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace("/details/salesreturns/" + response.data.main.id);
			else if (param == 'Delete')
				this.props.history.replace("/list/salesreturns");
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function callBackCustomerName(id, valueobj) {
	let tempObj = {
		customerid_name: valueobj.name,
		salesperson: this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson
	};

	if (valueobj.salespricelistid)
		tempObj.pricelistid = valueobj.salespricelistid;
	else if (valueobj.partnergroupid_pricelistid)
		tempObj.pricelistid = valueobj.partnergroupid_pricelistid;
	else {
		for (let i = 0; i < this.props.app.appSettings.length; i++) {
			if (this.props.app.appSettings[i].module == 'Sales' && this.props.app.appSettings[i].name == 'Pricelistid') {
				tempObj.pricelistid = this.props.app.appSettings[i].value['value'];
				break;
			}
		}
	}

	this.customFieldsOperation('partners', tempObj, valueobj, 'salesreturns');

	this.props.updateFormState(this.props.form, tempObj);
}

export function warehouseonchange() {
	let tempObj = {};

	if (this.props.resource.salesreturnitems.length == 0)
		return null;

	this.props.resource.salesreturnitems.forEach((item, i) => {
		if (!item.warehouseid || item.warehouseid == null)
			tempObj[`salesreturnitems[${i}].warehouseid`] = this.props.resource.warehouseid;
	});

	this.props.updateFormState(this.props.form, tempObj);
}

export function currencyOnChange() {
	if (this.props.resource.pricelistid) {
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid)
			this.props.updateFormState(this.props.form, {
				pricelistid: null
			});
	}

	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate: returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate: null
		});

		this.controller.computeFinalRate();
	}
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, this.props.resource.customerid, this.props.resource.pricelistid, 'sales');

	promise.then((returnObject) => {
		let tempObj = {};

		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;

		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		tempObj[`${itemstr}.qtyauthorized`] = item.qtyauthorized ? item.qtyauthorized : 1;
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;
		tempObj[`${itemstr}.itemid_addonhelptext`] = returnObject.addonhelptext;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.itemid_recommendedsellingprice`] = returnObject.itemid_recommendedsellingprice;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'salesreturnitems', itemstr);

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if (tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		this.props.updateFormState(this.props.form, tempObj);

		let tempchilditem = this.selector(this.props.fullstate, itemstr);

		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
	}, (reason) => {});
}

export function quantityOnChange(itemqty, item, itemstr) {
	let tempObj = {
		[`${itemstr}.qtyauthorized`]: itemqty
	};

	if (item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);

		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
	}

	this.props.updateFormState(this.props.form, tempObj);

	this.controller.computeFinalRate();
}

export function qtyauthorizedOnChange(itemqty, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.quantity`]: itemqty
	});
}

export function uomOnchange(id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);

	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, this.props.resource.customerid, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if (item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange(value, itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);

	let tempObj = {};

	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, this.props.resource.customerid, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if (item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;

		if (item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;

		this.props.updateFormState(this.props.form, tempObj);

		this.controller.computeFinalRate();
	}
}

export function computeCostRateLc(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};

	if (itemstr) {
		if (item.costrate) {
			if (this.props.resource.currencyid == this.props.app.defaultCurrency)
				tempObj[`${itemstr}.costratelc`] = item.costrate;
			else {
				if (this.props.resource.currencyexchangerate)
					tempObj[`${itemstr}.costratelc`] = item.costrate * this.props.resource.currencyexchangerate;
				else
					tempObj[`${itemstr}.costratelc`] = 0;
			}
		} else
			tempObj[`${itemstr}.costratelc`] = 0;
	} else {
		for (let i = 0; i < this.props.resource.salesreturnitems.length; i++) {
			if (this.props.resource.salesreturnitems[i].costrate) {
				if (this.props.resource.currencyid == this.props.app.defaultCurrency)
					tempObj[`salesreturnitems[${i}].costratelc`] = this.props.resource.salesreturnitems[i].costrate;
				else {
					if (this.props.resource.currencyexchangerate)
						tempObj[`salesreturnitems[${i}].costratelc`] = this.props.resource.salesreturnitems[i].costrate * this.props.resource.currencyexchangerate;
					else
						tempObj[`salesreturnitems[${i}].costratelc`] = 0;
				}
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);

	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});

	this.controller.computeFinalRate();
}

export function computeFinalRate() {
	setTimeout(() => {
		taxEngine(this.props, 'resource', 'salesreturnitems');
	}, 0);

	this.controller.computeCostRateLc();
}

export function searchOrder() {
	this.openModal({
		render: (closeModal) => {
			return <SearchForOrder type = 'Sales' openModal={this.props.openModal} callback={
				(item) => {
					this.props.updateFormState(this.props.form, {
						orderid : item.id,
						customerid : item.customerid,
						customerid_name : item.customerid_name,
						companyid : item.companyid,
						companyid_name : item.companyid_name,
						currencyid: item.currencyid,
						currencyexchangerate: item.currencyexchangerate,
						defaultcostcenter: item.defaultcostcenter,
						warehouseid: item.warehouseid,
						pricelistid: item.pricelistid
					});

					setTimeout(this.controller.orderOnChange, 0);
				}
		} closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function createReceiptNote() {
	this.props.history.push({
		pathname: '/createReceiptNote',
		params: {...this.props.resource, param: 'Sales Returns'}
	});
}

export function createCreditNote() {
	this.props.history.push({
		pathname: '/createCreditNote',
		params: {...this.props.resource, param: 'Sales Returns'}
	});
}

export function cancel () {
	this.props.history.goBack();
}
