import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { modalService, commonMethods, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		let tempObj = {};

		if(this.props.location.params)
			tempObj = { ...this.props.location.params };

		this.props.initialize(tempObj);

		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById() {
	axios.get(`/api/projectschedules/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == "success")
			this.props.initialize(response.data.main);
		else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	let tempObj = this.props.resource;

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : tempObj,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/projectschedules'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == "success") {
			if(this.props.isModal) {
				if(param != 'Delete')
					this.props.callback(response.data.main);
				this.props.closeModal();
			} else {
				if(param == 'Delete')
					this.props.history.replace('/projectschedules');
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	if(this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}
