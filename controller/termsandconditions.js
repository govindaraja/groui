import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	let resourceArray = ['Quotation', 'Order', 'Rfq', 'Supplier Quotation', 'Purchase Order','Proforma Invoice', 'Sales Invoice', 'Purchase Invoice', 'Estimation', 'Contract Enquiry', 'Contract'];

	if (this.props.app.feature.useProjects)
		resourceArray.push('Project Quotation', 'Work Order');

	this.setState({ resourceArray });

	if(this.state.createParam)
		this.controller.initializeState();
	else
		this.controller.getItemById();
}

export function initializeState () {
	this.props.initialize({});
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/termsandconditions/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == 'success') {
			this.props.initialize(response.data.main);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/termsandconditions'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/termsandconditions/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace('/list/termsandconditions');
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
