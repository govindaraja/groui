import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	this.props.initialize({
		companyid: this.props.app.selectedcompanyid,
		complaintdate : new Date(new Date().setHours(0, 0, 0, 0)),
		assignedto : this.props.app.user.id
	});
	this.updateLoaderFlag(false);
}

export function customerCallBack () {
	this.props.updateFormState(this.props.form, {
		orderno : null
	});
}

export function getItemById () {
	axios.get(`/api/customercomplaints/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param == 'Resolve' && (this.props.resource.resolution == null || this.props.resource.resolution == '')) {
		this.props.openModal(modalService.infoMethod({
			"header": "Warning!",
			"body": "Field Resolution is mandatory"
		}));
		this.updateLoaderFlag(false);
	} else {
		if(pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/customercomplaints'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					this.props.history.replace(`/details/customercomplaints/${response.data.main.id}`);
				} else {
					if (param == 'Delete')
						this.props.history.replace("/list/customercomplaints");
					else
						this.props.initialize(response.data.main);
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function cancel () {
	this.props.history.goBack();
}
