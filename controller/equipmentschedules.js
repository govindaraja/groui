import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService, pageValidation } from '../utils/services';
import { SelectAsync } from '../components/utilcomponents';

export function onLoad () {
	this.controller.getItemById();
}

export function getItemById () {
	axios.get(`/api/equipmentschedules/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt : {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function createServiceCall () {
	this.props.openModal({render: (closeModal) => {
		return <CreateServiceCallOptionModal
			resource={this.props.resource}
			callback={(numberingseriesmasterid) => {
				this.updateLoaderFlag(true);
				this.props.updateFormState(this.props.form, { numberingseriesmasterid });
				setTimeout(() => {
					this.controller.saveEquipSchedule('Inprogress');
				}, 0);
			}}
			app={this.props.app}
			closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function openSuspendRemarksModal () {
	this.props.openModal({
		render: (closeModal) => {
			return <SuspendRemarksModal
				resource={this.props.resource}
				callback={(remarks) => {
					this.updateLoaderFlag(true);
					this.props.updateFormState(this.props.form, {
						suspendremarks: remarks
					});
					setTimeout(() => {
						this.controller.saveEquipSchedule('Suspend');
					}, 0);
				}}
				app={this.props.app}
				closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function saveEquipSchedule (param) {
	if(param == 'Inprogress') {
		let tempData = {
			numberingseriesmasterid: this.props.resource.numberingseriesmasterid,
			preferences: 'Schedule Level',
			equipmentschedules : [this.props.resource]
		};

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : tempData
			},
			url : '/api/equipmentschedules'
		}).then((response) => {
			if (response.data.message == 'success') {
				this.props.openModal(modalService['infoMethod']({
					header : 'Success',
					body : 'Service Call No(s) ' + response.data.main.join() + ' created successfully!!!',
					btnArray : ['Ok']
				}));
				let tempObj = this.props.resource;
				tempObj.status = 'Inprogress';
				this.props.initialize(tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	} else {
		let tempData = {
			suspendremarks : this.props.resource.suspendremarks,
			equipmentschedules : [this.props.resource]
		};

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : tempData
			},
			url : '/api/equipmentschedules'
		}).then((response) => {
			if (response.data.message == 'success') {
				let tempObj = this.props.resource;
				tempObj.status = 'Suspended';
				this.props.initialize(tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function save (param) {
	this.updateLoaderFlag(true);

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource
		},
		url : '/api/equipmentschedules'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}

class CreateServiceCallOptionModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			numberingseriesmasterid: null
		};
		this.createServiceCall = this.createServiceCall.bind(this);
	}

	createServiceCall() {
		this.props.callback(this.state.numberingseriesmasterid);
		this.props.closeModal();
	}

	filterOnchange(numberingseriesmasterid) {
		this.setState({
			numberingseriesmasterid
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">PMS Call Creation</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-12">
							<div className="form-check col-md-6 col-sm-12">
								<label>Choose Series Type</label>
								<SelectAsync 
									resource="numberingseriesmaster" 
									fields="id,name,format,isdefault,currentvalue"
									className={this.state.numberingseriesmasterid ? '' : 'errorinput'}
									value={this.state.numberingseriesmasterid}
									label="name" 
									valuename="id"
									createParamFlag={true}
									filter="numberingseriesmaster.resource='servicecalls'" 
									defaultValueUpdateFn={(value, valueobj) => this.filterOnchange(value, valueobj)}
									onChange={(value, valueobj) => this.filterOnchange(value, valueobj)}
								/>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.createServiceCall} disabled={!this.state.numberingseriesmasterid}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class SuspendRemarksModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.suspendSchedule = this.suspendSchedule.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
	}

	suspendSchedule() {
		this.props.callback(this.state.remarks);
		this.props.closeModal();
	}

	inputOnChange(evt) {
		this.setState({
			remarks: evt.target.value
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Remarks for Suspend</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-6 col-sm-12 offset-md-1">
							<textarea className={`form-control ${!this.state.remarks ? 'errorinput' : ''}`} value={this.state.remarks} onChange={this.inputOnChange} required />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.suspendSchedule} disabled={!this.state.remarks}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}