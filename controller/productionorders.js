import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, detailsSVC, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';
import { OrderPoQtyField, ItemRateField } from '../components/utilcomponents';
import StockdetailsModal from '../containers/stockdetails';
import ProductionorderitemrequestplannedqtydetailsModal from '../components/details/productionorderitemrequestplannedqtydetailsmodal';
import ConfirmValuationModal from '../components/details/confirmvaluationmodal';
import {ChildEditModal} from '../components/utilcomponents';
import async from 'async'

export function onLoad () {
	axios.get(`/api/roles?&field=id,name&filtercondition=roles.name='Production Exception Approvers'`).then((response) => {
		if (response.data.message == 'success') {
			this.setState({
				producitonExceptionApproverRoleId: response.data.main.length > 0 ? response.data.main[0].id : null
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		orderdate: new Date(new Date().setHours(0, 0, 0, 0)),
		productionbomitems: [],
		productionfinisheditems : [],
		productionschedule: [],
		bomitemsarray: [],
		confirmvaluation : {}
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'copy') {
			tempObj = params;
			tempObj.confirmvaluation = {};
		}

		if(params.param == 'Sales Orders') {
			tempObj.salesorderid = params.id;
			tempObj.customerid = params.customerid;
			tempObj.currencyid = this.props.app.defaultCurrency;
			tempObj.productionfinisheditems = [];
			tempObj.productionbomitems = [];
			tempObj.addonObj = {};

			params.orderitems.forEach((orderitem,index)=>{
				if(orderitem.checked && orderitem.pendingqty>0){
					let tempfpitem = {
						itemid : orderitem.itemid,
						itemid_name : orderitem.itemid_name,
						itemid_itemtype : orderitem.itemid_itemtype,
						itemid_keepstock : orderitem.itemid_keepstock,
						itemid_issaleskit : orderitem.itemid_issaleskit,
						itemid_hasaddons :  orderitem.itemid_hasaddons,
						description : orderitem.description,
						uomid : orderitem.uomid,
						uomid_name : orderitem.uomid_name,
						itemid_bomtype : orderitem.itemid_bomtype,
						bomid : orderitem.bomid,
						quantity : orderitem.pendingqty,
						type : 'Finished Product',
						salesorderitemsid : orderitem.id
					};
					this.customFieldsOperation('orderitems', tempfpitem,orderitem, 'productionfinisheditems');
					tempObj.productionfinisheditems.push(tempfpitem);

					if(orderitem.includeaddon){
						params.kititemorderdetails.forEach((item,index)=>{
							if(item.rootindex == orderitem.index){
								let tempitem = {
									itemid : item.itemid,
									itemid_name : item.itemid_name,
									itemid_itemtype : item.itemid_itemtype,
									itemid_keepstock : item.itemid_keepstock,
									itemid_issaleskit : item.itemid_issaleskit,
									itemid_hasaddons :  item.itemid_hasaddons,
									itemid_bomtype : item.itemid_bomtype,
									description : item.itemid_description,
									uomid : item.uomid,
									uomid_name : item.uomid_name,
									quantity : Number((orderitem.pendingqty * item.conversion).toFixed(this.props.app.roundOffPrecisionStock))
								};
								this.customFieldsOperation('kititemorderdetails', tempitem,item, 'productionbomitems');
								this.customFieldsOperation('kititemorderdetails', tempitem,item, 'productionfinisheditems');

								if(tempObj.addonObj[item.itemid]){
									tempObj.addonObj[item.itemid].quantity += item.quantity; 
								}else{	
									tempObj.addonObj[item.itemid] = tempitem;
								}
							}
						});
					}
				}
			});

			for(let props in tempObj.addonObj){
				tempObj.productionbomitems.push({
					...tempObj.addonObj[props],
					conversion : 1,
					applicablefor : 'Specific Item',
					specificitemid : tempObj.addonObj[props].itemid,
					specificitemid_bomtype : tempObj.addonObj[props].itemid_bomtype
				});

				
				tempObj.productionfinisheditems.push({
					...tempObj.addonObj[props],
					type : 'Co-Product'
				});
			}
			
			this.customFieldsOperation('orders', tempObj, params, 'productionorders');
			setTimeout(() => {
				this.controller.getAllBOMItems();
			}, 0);
		}

		if(params.param == 'Projects') {			
			tempObj.projectid = params.id;
			tempObj.customerid = params.customerid;

			this.customFieldsOperation('projects', tempObj, params, 'productionorders');
		}
	}

	this.props.initialize(tempObj);

	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/productionorders/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			this.controller.checkVisibilityForButton();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getBom (id, valueobj, item, itemstr) {
	this.updateLoaderFlag(true);
	axios.get(`/api/bom?&field=id,name,isdefault&filtercondition=bom.status='Approved' and bom.itemid=${id}&sortstring=(case when isdefault is true then 1 when isdefault is false then 2 else 3 end)`).then((response) => {
		if (response.data.message == 'success') {
			let bomitemsarray = response.data.main;
			if(bomitemsarray.length > 0 && (bomitemsarray.filter(bom=>bom.isdefault).length > 0 || bomitemsarray.length == 1)) {
				let bom = bomitemsarray.filter(bom=>bom.isdefault).length > 0 ? bomitemsarray.filter(bom=>bom.isdefault)[0] : bomitemsarray[0];
				let tempObj={};
				tempObj[`${itemstr}.bomid`] = bom.id;
				tempObj[`${itemstr}.bomid_name`] = bom.name;
				this.props.updateFormState(this.props.form, tempObj);
				setTimeout(() => {
					this.controller.getBOMItems(id,bom.id);
				}, 0);
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackBom(id, valueobj, itemstr){
	if(id > 0){
		this.controller.getBOMItems(valueobj.itemid,valueobj.id);
	}else{
		let fpitem = this.selector(this.props.fullstate, itemstr);
		let productionbomitems = this.props.resource.productionbomitems.filter(rmitem=>!(rmitem.isbomitem && rmitem.specificitemid==fpitem.itemid));
		this.props.updateFormState(this.props.form, { productionbomitems });
	}
}

export function getAllBOMItems () {
	async.eachSeries(this.props.resource.productionfinisheditems, (item, eachCB) => {
		if(item.bomid > 0)
			this.controller.getBOMItems(item.itemid,item.bomid,eachCB);
		else
			eachCB(null);
	}, () => {
	});
}

export function getBOMItems (itemid,bomid,callback) {
	if(!itemid && !bomid)
		return null;
	this.updateLoaderFlag(true);
	let productionbomitems = this.props.resource.productionbomitems ?  this.props.resource.productionbomitems.filter(bomitem=>!(bomitem.specificitemid == itemid && bomitem.isbomitem)) : [];
	let finishedproduct = this.props.resource.productionfinisheditems.filter(finishedproduct=>finishedproduct.itemid == itemid)[0];	
	axios.get(`/api/query/getmultibomitemsquery?itemid=${itemid}&bomid=${bomid}`).then((response) => {
		if (response.data.message == 'success') {
			response.data.main.forEach((item, index) => {
				item.conversion = item.quantity;
			});

			let responseArr = response.data.main.sort((a,b) => (a.id - b.id));

			responseArr.forEach((item) => {
				let tempObj = {
					itemid : item.itemid,
					itemid_name : item.itemid_name,
					itemid_type : item.itemid_type,
					itemid_keepstock : item.itemid_keepstock,
					itemid_hasaddons : item.itemid_hasaddons,
					itemid_issaleskit : item.itemid_issaleskit,
					description : item.description,
					uomid : item.uomid,
					uomid_name : item.uomid_name,
					conversion : item.conversion,
					quantity : Number((item.conversion * finishedproduct.quantity).toFixed(this.props.app.roundOffPrecisionStock)),
					applicablefor : 'Specific Item',
					specificitemid : itemid,
					specificitemid_bomtype : finishedproduct.itemid_bomtype,
					isbomitem : true 
				};
				this.customFieldsOperation('itemmaster', tempObj, item, 'productionbomitems');
				productionbomitems.push(tempObj);
			});

			this.props.updateFormState(this.props.form, {
				productionbomitems
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		if(callback)
			callback(null);
		this.updateLoaderFlag(false);
	});
}

export function getSpecificItems() {
	let array = [];
	
	this.props.resource.productionfinisheditems.forEach((fpitem) => {
		array.push({
			id : fpitem.itemid,
			name : fpitem.itemid_name
		});
	});
	
	return array;
}

export function specificItemOnChange(value, item, itemstr) {
	let tempObj = {};
	for(let i=0;i<this.props.resource.productionfinisheditems.length;i++){
		if(this.props.resource.productionfinisheditems[i].itemid == value){
			tempObj[`${itemstr}.specificitemid_bomtype`] = this.props.resource.productionfinisheditems[i].itemid_bomtype;
			break;
		}
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function checkVisibilityForButton () {
	setTimeout(() => {
		let showConfirmValuationBtn = true;
		this.props.resource.productionfinisheditems.forEach((item) => {
			if((item.quantity-(item.deliveredqty || 0))>0)
				showConfirmValuationBtn = false;
		});
		this.setState({
			showConfirmValuationBtn
		});
	}, 0);
}

export function callbackRouting () {
	if(!this.props.resource.routingid)
		return null;

	axios.get(`/api/routingitems?&field=id,processid,workcenterid,setuptime,processtime,duration,displayorder,processes/name/processid,workcenters/name/workcenterid&filtercondition=routingitems.parentid=${this.props.resource.routingid}`).then((response) => {
		if (response.data.message == 'success') {
			response.data.main = response.data.main.sort((a, b) => a.displayorder > b.displayorder ? 1 : -1);
			let productionschedule = response.data.main.map((item) => {
				return {
					processid : item.processid,
					processid_name : item.processid_name,
					workcenterid : item.workcenterid,
					workcenterid_name : item.workcenterid_name,
					processtime : item.processtime,
					setuptime : item.setuptime,
					duration : item.duration
				}
			});
			this.props.updateFormState(this.props.form, {
				productionschedule
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function quantityOnChange (itemqty, item) {
	let tempObj = {};
	this.props.resource.productionbomitems.forEach((bomitem, index) => {
		if(bomitem.specificitemid == item.itemid){
			tempObj[`productionbomitems[${index}].quantity`] = Number((bomitem.conversion * (itemqty ? itemqty : 0)).toFixed(this.props.app.roundOffPrecisionStock));
		}
	});
	this.props.updateFormState(this.props.form, tempObj);
}

export function conversionOnChange (value, item, itemstr) {
	let tempObj = {};
	let finishedproduct = this.props.resource.productionfinisheditems.filter(finishedproduct=>finishedproduct.itemid == item.specificitemid)[0];
	tempObj[`${itemstr}.quantity`] = Number((value * finishedproduct.quantity).toFixed(this.props.app.roundOffPrecisionStock));
	this.props.updateFormState(this.props.form, tempObj);
}

export function deleteBOMItem (index) {
	let productionbomitems = [...this.props.resource.productionbomitems];
	let finishedproduct = this.props.resource.productionfinisheditems.filter(finishedproduct=>finishedproduct.itemid == productionbomitems[index].specificitemid)[0];

	if(productionbomitems[index].isbomitem && finishedproduct.itemid_bomtype == 'Standard'){
		if(this.state.producitonExceptionApproverRoleId && this.props.app.user.roleid.includes(this.state.producitonExceptionApproverRoleId)) {
			let message = {
				header : 'Error',
				body : 'Do you want to delete standard bom item ?',
				btnArray : ['Yes','No']
			};
			this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if(param) {
					productionbomitems.splice(index, 1);
					this.props.updateFormState(this.props.form, {
						productionbomitems
					});
				}
			}));
		} else {
			this.props.openModal(modalService['infoMethod']({
				header : 'Error',
				body : `Only Production Exception Approver have access to delete standard bom items.`,
				btnArray : ['Ok']
			}));
		}
	} else {
		productionbomitems.splice(index, 1);
		this.props.updateFormState(this.props.form, {
			productionbomitems
		});
	}
}
export function deleteFinishedProduct (index) {
	let productionfinisheditems = [...this.props.resource.productionfinisheditems];
	let productionbomitems = this.props.resource.productionbomitems.filter(bomitem=>bomitem.specificitemid != productionfinisheditems[index].itemid);
	productionfinisheditems.splice(index,1);
	this.props.updateFormState(this.props.form, {
		productionbomitems : productionbomitems,
		productionfinisheditems : productionfinisheditems
	});
}

export function callBackOrderNo (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		customerid: valueobj.customerid
	});
}

export function callBackProjectName (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		customerid: valueobj.customerid
	});
}

export function processOnChange(value, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.processid_name`] = valueobj.name;
	this.props.updateFormState(this.props.form, tempObj);
}

export function workcenterOnChange(value, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.workcenterid_name`] = valueobj.name;
	this.props.updateFormState(this.props.form, tempObj);
}

export function operatorOnChange(value, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.operatorid_displayname`] = valueobj.displayname;
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackItem (id, valueobj, item, itemstr) {
	let itemcount = 0;
	this.props.resource.productionfinisheditems.forEach((prodfinitem) => {
		if(prodfinitem.itemid == id)
			itemcount ++;
	});
	if(itemcount > 1) {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.itemid`]: null
		});
		return this.props.openModal(modalService['infoMethod']({
			header : 'Error',
			body : `Item Cannot be Duplicated`,
			btnArray : ['Ok']
		}));
	}
	let tempObj = {};
	tempObj[`${itemstr}.itemid_name`] = valueobj.name;
	tempObj[`${itemstr}.itemid_itemtype`] = valueobj.itemtype;
	tempObj[`${itemstr}.description`] = valueobj.description;
	tempObj[`${itemstr}.uomid`] = valueobj.stockuomid;
	tempObj[`${itemstr}.uomid_name`] = valueobj.stockuomid_name;
	tempObj[`${itemstr}.itemid_bomtype`] = valueobj.bomtype;
	tempObj[`${itemstr}.quantity`] = 1;
	tempObj[`routingid`] = valueobj.routingid;
	this.customFieldsOperation('itemmaster', tempObj, valueobj, 'productionfinisheditems', itemstr);
	this.props.updateFormState(this.props.form, tempObj);
	setTimeout(() => {
		if(this.props.resource.productionschedule.length == 0)
			this.controller.callbackRouting();
		this.controller.getBom(id, valueobj, item, itemstr);
	}, 0);
}

export function callbackProduct (id, valueobj, item, itemstr) {
	let tempObj = {
		[`${itemstr}.itemid_name`]: valueobj.name,
		[`${itemstr}.itemid_itemtype`]: valueobj.itemtype,
		[`${itemstr}.itemid_keepstock`]: valueobj.keepstock,
		[`${itemstr}.itemid_issaleskit`]: valueobj.issaleskit,
		[`${itemstr}.itemid_hasaddons`]: valueobj.hasaddons,
		[`${itemstr}.description`]: valueobj.description,
		[`${itemstr}.uomid`]: valueobj.stockuomid,
		[`${itemstr}.uomid_name`]: valueobj.stockuomid_name,
		[`${itemstr}.quantity`] : (item.quantity ? item.quantity : 1)
	};
	this.customFieldsOperation('itemmaster', tempObj, valueobj, 'productionbomitems', itemstr);
	this.props.updateFormState(this.props.form, tempObj);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param !='Update' && param !='Hold' && param !='Unhold' && param !='Cancel' && param !='Delete' && param !='Amend' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/productionorders'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/productionorders/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/productionorders");
				else{
					this.props.initialize(response.data.main);
					this.controller.checkVisibilityForButton();
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function createItemRequest () {
	this.props.openModal({
		render: (closeModal) => {
			return <ProductionorderitemrequestplannedqtydetailsModal resource={this.props.resource} resourcename='productionorders' openModal={this.props.openModal} closeModal={closeModal} history={this.props.history} />
		}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
	});
}

export function createReceiptNote() {
	checkTransactionExist('receiptnotes', 'productionorders', this.props.resource.id, this.openModal, (param)=> {
		if(param)
			this.props.history.push({pathname: '/createReceiptNote', params: {...this.props.resource, param: 'Production Order'}});
	});
}

export function openStockDetails (item) {
	this.props.openModal({render: (closeModal) => {return <StockdetailsModal resource = {this.props.resource} item={item} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function cancel () {
	this.props.history.goBack();
}

export function confirmValuation(){
	let getBody = (closeModal) => {
		return (
			<ConfirmValuationModal />
		)
	}
	this.props.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} app={this.props.app} updateFormState={this.props.updateFormState} openModal={this.openModal} closeModal={closeModal} resourceName={'productionorders'} getBody={getBody} save={this.controller.save} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'},confirmModal: true});
}
