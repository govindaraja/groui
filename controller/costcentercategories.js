import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { modalService, commonMethods, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({costcenters : []});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById() {
	axios.get(`/api/costcentercategories/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == "success") {
			this.props.initialize(response.data.main);
			this.setState({
				ndt : {
					notes : response.data.notes,
					documents : response.data.documents,
					tasks : response.data.tasks,
				}
			});
			this.controller.getCostCenter();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getCostCenter (){
	axios.get(`/api/costcenters?field=id,modified,name,isactive&filtercondition=costcenters.categoryid=${this.props.match.params.id}&sortstring=costcenters.id&includeinactive=true`).then((response) => {
		if(response.data.message == "success") {
			var tempObj ={
				costcenters : response.data.main
			};
			this.props.updateFormState(this.props.form, tempObj);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
} 


export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/costcentercategories'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == "success") {
			if(this.state.createParam) {
				this.props.history.replace(`/details/costcentercategories/${response.data.main.id}`);
			} else {
				if(param == 'Delete')
					this.props.history.replace('/list/costcentercategories');
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
