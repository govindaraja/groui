import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, biService } from '../utils/services';

export function onLoad () {
	let variableObject = biService.getVariableObject(this.props.location.params.reportname, this.props.app)[this.props.location.params.resource];
	let operatorObject = biService.getOperatorObject();
	let dataObject = biService.getDataObject(this.props.location.params.reportname, this.props.app)[this.props.location.params.resource];
	let variableObjArray = [], operatorObjArray = [], dataObjArray = [];
	for(var prop in variableObject) {
		variableObjArray.push({id: prop, name: variableObject[prop]});
	}
	for(var prop in operatorObject) {
		operatorObjArray.push({id: prop, name: prop});
	}
	for(var prop in dataObject) {
		dataObjArray.push({id: prop, name: dataObject[prop]});
	}

	this.setState({ variableObject, dataObject, operatorObject, variableObjArray, operatorObjArray, dataObjArray });

	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {};
	if(this.props.location.params) {
		let params = this.props.location.params;
		tempObj.reportname = params.reportname;
		tempObj.resourcename = params.resource;
	}
	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/biconfigurations/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.config.rows1 = response.data.main.config.rows[0];
			tempObj.config.rows2 = response.data.main.config.rows[1];
			tempObj.config.columns1 = response.data.main.config.columns[0];
			tempObj.config.columns2 = response.data.main.config.columns[1];
			this.props.initialize(tempObj);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);
	let resourceObj = {...this.props.resource};

	let rows = [], cols = [], errorArray = [];
	if (resourceObj.config.rows1)
		rows.push(resourceObj.config.rows1);
	if (resourceObj.config.rows2)
		rows.push(resourceObj.config.rows2);
	if (resourceObj.config.columns1)
		cols.push(resourceObj.config.columns1);
	if (resourceObj.config.columns2)
		cols.push(resourceObj.config.columns2);

	let rowDuplicate = false, uniqueRow = {}, colDuplicate = false, uniqueColumn = {};
	for (var i = 0; i < rows.length; i++)
		if (uniqueRow[rows[i]])
			rowDuplicate = true;
		else
			uniqueRow[rows[i]] = rows[i];

	if (rowDuplicate)
		errorArray.push('Rows Axis Level-1 and Level-2 can\'t have same values\n');

	for (var i = 0; i < cols.length; i++)
		if (uniqueColumn[cols[i]])
			colDuplicate = true;
		else
			uniqueColumn[cols[i]] = cols[i];

	if (colDuplicate)
		errorArray.push('Columns Axis Level-1 and Level-2 can\'t have same values\n');

	for (var i = 0; i < rows.length; i++) {
		if (cols.indexOf(rows[i]) >= 0) {
			errorArray.push('Rows Axis Levels and Columns Axis Levels can\'t have same values');
			break;
		}
	}
	
	if (errorArray.length > 0) {
		this.props.openModal(modalService.infoMethod({
			header : 'Error',
			body : errorArray,
			btnArray : ['Ok']
		}));
		this.updateLoaderFlag(false);
	} else {
		resourceObj.config.rows = rows;
		resourceObj.config.columns = cols;
		
		axios({
			method : 'POST',
			data : {
				actionverb : param,
				data : resourceObj,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/biconfigurations'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true);
			}));

			if (response.data.message == 'success') {
				this.props.closeModal();
				this.props.callback((param == 'Delete') ? null : response.data.main.id);
			}
			this.updateLoaderFlag(false);
		});
	}
}


export function cancel () {
	this.props.closeModal();
}
