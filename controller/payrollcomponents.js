import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		roundoffmethod : this.props.app.defaultRoundOffMethod
	};

	if(this.props.location.params && this.props.location.params.param == 'copy')
		tempObj = this.props.location.params;

	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/payrollcomponents/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function typeOnChange () {
	this.props.updateFormState(this.props.form, {
		category: null,
		statutorytype: null,
		accrualaccountid: null,
		tdsapplicable: false,
		pfapplicable: false,
		esiapplicable: false,
		taxexempt: false
	});
}

export function computeOnChange () {
	this.props.updateFormState(this.props.form, {
		utilitymoduleid: null
	});
}

export function categoryOnChange() {
	this.props.updateFormState(this.props.form, {
		statutorytype: null
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/payrollcomponents'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/payrollcomponents/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace("/list/payrollcomponents");
			else
				this.props.initialize(response.data.main);
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
