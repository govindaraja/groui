import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation, accessResource } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../utils/filter';
import { ItemRateField } from '../components/utilcomponents';
import StockdetailsModal from '../containers/stockdetails';
import AnalysePurchaseRateModal from '../containers/analysepurchaseitemrate';
import EmailModal from '../components/details/emailmodal';
import AnalyseSupplierquotationModal from '../components/details/analysesupplierquotationmodal';
import { RenderItemQty } from '../utils/customfieldtypes';
import PurchaseOrderItemRequestAddModal from '../components/details/purchaseorderitemrequestaddmodal';

let roundOffPrecisionLocal = 5;

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();

	for (var i = 0; i < this.props.app.appSettings.length; i++) {
		if (this.props.app.appSettings[i].module == 'Accounts' && this.props.app.appSettings[i].name == 'Rounding Off Precision')
			roundOffPrecisionLocal = this.props.app.appSettings[i].value.value;
	}

	if(this.props.app.feature.useProduction && accessResource('productionorders', 'Read', this.props.app)) {
		this.controller.getproductionorders();
	}
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		podate: new Date(new Date().setHours(0, 0, 0, 0)),
		purchaseorderitems: [],
		kititempurchaseorderdetails: [],
		additionalcharges: []
	};

	let itemrequestidArr = [], saleskitArr = [], prodOrderidArr = [];

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'copy')
			tempObj = params;

		if(params.param == 'Supplier Quotation') {
			let purchaseorderitemArray = params.supplierquotationitems;

			for (var i = 0; i < purchaseorderitemArray.length; i++) {
				let tempChildObj = {
					index: i+1,
					alternateuom: purchaseorderitemArray[i].uomconversiontype ? true : false,
					warehouseid: params.projectid_warehouseid ? params.projectid_warehouseid : null,
					deliverydate: purchaseorderitemArray[i].deliverydate ? purchaseorderitemArray[i].deliverydate : params.deliverydate
				};
				utils.assign(tempChildObj, purchaseorderitemArray[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_keepstock', 'itemid_uomgroupid', 'description', 'rate', 'quantity', 'uomid', 'uomid_name', 'discountmode', 'discountquantity', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'remarks', 'usebillinguom', 'billinguomid', 'billinguomid_name', 'billingrate', 'billingquantity', 'billingconversiontype', 'billingconversionfactor', 'itemrequestitemsid']);

				this.customFieldsOperation('supplierquotationitems', tempChildObj, purchaseorderitemArray[i], 'purchaseorderitems');
				tempObj.purchaseorderitems.push(tempChildObj);
			}

			utils.assign(tempObj, params, ['partnerid', 'partnerid_name', {'supplierquoteid_supplierquoteno' : 'supplierquoteno'}, {'supplierquoteid_rfqid' : 'rfqid'}, {'supplierquoteid' : 'id'}, {'supplierreference' : 'vendorreference'}, 'pricelistid', 'creditperiod', 'companyid', 'currencyid', 'contactid', {'contactperson' : 'contactid_name'}, {'mobile' : 'contactid_mobile'}, {'phone' : 'contactid_phone'}, {'email' : 'contactid_email'}, 'currencyexchangerate', 'deliveryaddress', 'deliveryaddressid', 'billingaddress', 'billingaddressid', 'deliverydate', 'vendorreference', 'taxid', 'roundoffmethod', 'projectid']);

			if(purchaseorderitemArray.length > 0)
				tempObj.warehouseid = purchaseorderitemArray[0].warehouseid;
			if(params.projectid_warehouseid > 0)
				tempObj.warehouseid = params.projectid_warehouseid;

			this.customFieldsOperation('supplierquotation', tempObj, params, 'purchaseorders');
		}

		if(params.param == 'Purchase Planner' || params.param == 'Project Purchase Planner') {
			let purchaseorderitemArray = params.purchaseItemArray;

			for (var i = 0; i < purchaseorderitemArray.length; i++) {
				if(!prodOrderidArr.includes(purchaseorderitemArray[i].productionorderid))
					prodOrderidArr.push(purchaseorderitemArray[i].productionorderid);

				if(itemrequestidArr.indexOf(purchaseorderitemArray[i].itemrequestid) == -1)
					itemrequestidArr.push(purchaseorderitemArray[i].itemrequestid);

				if(purchaseorderitemArray[i].itemid_issaleskit) {
					saleskitArr.push(purchaseorderitemArray[i].itemid);
				}
				let tempChildObj = {
					index: i+1,
					alternateuom: purchaseorderitemArray[i].uomconversiontype ? true : false,
					newitem: true,
					deliverydate: purchaseorderitemArray[i].deliverydate ? purchaseorderitemArray[i].deliverydate : params.deliverydate
				};
 
 				utils.assign(tempChildObj, purchaseorderitemArray[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_keepstock', 'itemid_uomgroupid', 'description', 'specification', 'rate', 'quantity', 'uomid', 'uomid_name', 'discountmode', 'discountquantity', 'warehouseid', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'remarks', 'usebillinguom', 'billinguomid', 'billinguomid_name', 'billingconversiontype', 'billingconversionfactor', 'itemrequestitemsid', {'itemrequest_index' : 'index'}, 'itemrequestid', {'billingrate' : 0},'approvedmakes']);

 				if(params.param == 'Project Purchase Planner') {
 					tempChildObj.quantity = Number(((purchaseorderitemArray[i].quantity * (purchaseorderitemArray[i].uomconversionfactor ? purchaseorderitemArray[i].uomconversionfactor : 1)) / (purchaseorderitemArray[i].requestconversionfactor ? purchaseorderitemArray[i].requestconversionfactor : 1)).toFixed(this.props.app.roundOffPrecisionStock));
 				}

 				let purchaserate = purchaseorderitemArray[i].rate;

				if(purchaseorderitemArray[i].projectestimationitemsid) {
					let itemConversionfactor = purchaseorderitemArray[i].usebillinguom ? purchaseorderitemArray[i].billingconversionfactor : purchaseorderitemArray[i].uomconversionfactor;
					/*if(purchaseorderitemArray[i].itemrequestitemsid_usebillinguom) {
						purchaserate = Number((purchaseorderitemArray[i].projectestimationitemsid_rate * (purchaseorderitemArray[i].requestrateconversionfactor ? purchaseorderitemArray[i].requestrateconversionfactor : 1) / (itemConversionfactor ? itemConversionfactor : 1)).toFixed(this.props.app.roundOffPrecision));
					} else {*/
						purchaserate = Number((purchaseorderitemArray[i].projectestimationitemsid_rate * (purchaseorderitemArray[i].requestconversionfactor ? purchaseorderitemArray[i].requestconversionfactor : 1) / (itemConversionfactor ? itemConversionfactor : 1)).toFixed(this.props.app.roundOffPrecision));
					//}
				}

				tempChildObj.rate = purchaserate;

				if(purchaseorderitemArray[i].usebillinguom) {
					tempChildObj.billingrate = tempChildObj.rate;

					tempChildObj.rate = Number(((tempChildObj.billingrate / (purchaseorderitemArray[i].uomconversionfactor ? purchaseorderitemArray[i].uomconversionfactor : 1)) * purchaseorderitemArray[i].billingconversionfactor).toFixed(this.props.app.roundOffPrecision));

					tempChildObj.billingquantity = Number(((tempChildObj.quantity / (purchaseorderitemArray[i].uomconversionfactor ? purchaseorderitemArray[i].uomconversionfactor : 1)) * purchaseorderitemArray[i].billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
				}

				if(params.projectid > 0) {
					this.customFieldsOperation('itemrequestitems', tempChildObj, purchaseorderitemArray[i], 'purchaseorderitems');
				} else {
					this.customFieldsOperation('orderitems', tempChildObj, purchaseorderitemArray[i], 'purchaseorderitems');
				}

				tempObj.purchaseorderitems.push(tempChildObj);
			}
			utils.assign(tempObj, params, ['companyid', 'warehouseid', 'projectid', 'orderid', 'projectid_projectname',  'projectid_projectno', 'projectid_displayname', 'taxid']);
			if(params.projectid > 0) {
				utils.assign(tempObj, params, [{'deliveryaddress': 'projectid_deliveryaddress'}, {'deliveryaddressid': 'projectid_deliveryaddressid'}, {'deliverydate': 'requireddate'}, 'defaultcostcenter']);
				this.customFieldsOperation('itemrequests', tempObj, params, 'purchaseorders');
			} else if(params.param == 'Sales Order') {
				utils.assign(tempObj, params, ['deliveryaddress', 'deliveryaddressid']);
				this.customFieldsOperation('orders', tempObj, params, 'purchaseorders');
			}
			if(prodOrderidArr.length > 0)
				tempObj.productionorderid = prodOrderidArr;
		}

		if(params.param == 'Sales Order') {
			utils.assign(tempObj, params, ['companyid', 'warehouseid', {'orderid': 'id'}, 'deliveryaddress', 'deliveryaddressid']);
			this.customFieldsOperation('orders', tempObj, params, 'purchaseorders');

			let tempindex = 0;
			params.orderitems.forEach((orderitem) => {
				let tempChildObj = {
					deliverydate: orderitem.deliverydate ? orderitem.deliverydate : params.deliverydate
				};
				if(orderitem.itemid_allowpurchase && orderitem.itemid_itemtype == 'Product') {
					utils.assign(tempChildObj, orderitem, ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_keepstock', 'itemid_uomgroupid', 'description', 'quantity', 'warehouseid', 'displaygroup', 'remarks', {'requestconversionfactor' : 'uomconversionfactor'}, 'itemid_itemgroupid', 'itemid_itemcategorymasterid']);

					this.customFieldsOperation('orderitems', tempChildObj, orderitem, 'purchaseorderitems');

					if(!orderitem.itemid_hasaddons || (orderitem.itemid_hasaddons && orderitem.itemid_keepstock)) {
						tempindex++;
						tempChildObj.index = tempindex;
						tempObj.purchaseorderitems.push(tempChildObj);
					}
				}
				if(orderitem.itemid_issaleskit) {
					params.kititemorderdetails.forEach((kititem) => {
						if(orderitem.index == kititem.rootindex) {
							let tempKitChiObj = {
								rootindex: tempChildObj.index,
								warehouseid: orderitem.warehouseid
							};
							utils.assign(tempKitChiObj, kititem, ['itemid', 'itemid_name', 'parent', 'uomid', 'uomid_name', 'quantity', 'conversion', 'rate', 'ratelc', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'index', 'parentindex']);

							tempObj.kititempurchaseorderdetails.push(tempKitChiObj);
						}
					});
				}
				if(orderitem.itemid_hasaddons) {
					params.kititemorderdetails.forEach((kititem) => {
						if(orderitem.index == kititem.rootindex && kititem.itemid_allowpurchase && kititem.itemid_itemtype == 'Product') {
							let tempKitChiObj = {
								deliverydate: orderitem.deliverydate ? orderitem.deliverydate : params.deliverydate,
								warehouseid: orderitem.warehouseid
							};
							utils.assign(tempKitChiObj, kititem, ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_keepstock', 'itemid_uomgroupid', {'description': 'itemid_name'}, 'quantity']);

							this.customFieldsOperation('orderitems', tempKitChiObj, kititem, 'purchaseorderitems');

							tempindex++;
							tempKitChiObj.index = tempindex;
							tempObj.purchaseorderitems.push(tempKitChiObj);
						}
					});
				}
			});

			/*let purchaseorderitemArray = params.purchaseItemArray;
			let purchaseorderkititemArray = params.purchaseKitItemArray;

			for (var i = 0; i < purchaseorderitemArray.length; i++) {
				let tempChildObj = {
					index: i+1,
					deliverydate: purchaseorderitemArray[i].deliverydate ? purchaseorderitemArray[i].deliverydate : params.deliverydate
				};

				utils.assign(tempChildObj, purchaseorderitemArray[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_keepstock', 'itemid_uomgroupid', 'description', 'rate', 'quantity', 'discountmode', 'discountquantity', 'warehouseid', 'taxid', 'displaygroup', 'remarks', {'requestconversionfactor' : 'uomconversionfactor'}]);

				this.customFieldsOperation('orderitems', tempChildObj, purchaseorderitemArray[i], 'purchaseorderitems');

				tempObj.purchaseorderitems.push(tempChildObj);

				for (var j = 0; j < purchaseorderkititemArray.length; j++) {
					if(purchaseorderitemArray[i].orderid == purchaseorderkititemArray[j].parentid && purchaseorderitemArray[i].order_index == purchaseorderkititemArray[j].rootindex) {
						let tempKitChiObj = {
							rootindex: tempChildObj.index,
							warehouseid: purchaseorderitemArray[i].warehouseid
						};
						utils.assign(tempKitChiObj, purchaseorderkititemArray[j], ['itemid', 'itemid_name', 'parent', 'uomid', 'uomid_name', 'quantity', 'conversion', 'rate', 'ratelc', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'index', 'parentindex']);
						tempObj.kititempurchaseorderdetails.push(tempKitChiObj);
					}
				}
			}

			utils.assign(tempObj, params, ['companyid', 'warehouseid', 'orderid', 'taxid', 'deliveryaddress', 'deliveryaddressid']);

			this.customFieldsOperation('orders', tempObj, params, 'purchaseorders');*/
		}

		if(params.param == 'Item Requests for Project') {
			let purchaseorderitemArray = params.itemrequestitems;
			itemrequestidArr.push(params.id);

			for (var i = 0; i < purchaseorderitemArray.length; i++) {
				let tempChildObj = {
					index: i+1,
					deliverydate: purchaseorderitemArray[i].deliverydate ? purchaseorderitemArray[i].deliverydate : params.deliverydate,
					itemrequestitemsid : purchaseorderitemArray[i].id,
					deliverydate : purchaseorderitemArray[i].requireddate,
					itemrequestid : params.id,
					itemrequest_index : purchaseorderitemArray[i].index,
					newitem: true,
					discountmode : 'Percentage'
				};

				if(purchaseorderitemArray[i].itemid_issaleskit) {
					saleskitArr.push(purchaseorderitemArray[i].itemid);
				}

				utils.assign(tempChildObj, purchaseorderitemArray[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_keepstock', 'itemid_uomgroupid', 'description', 'specification', 'quantity', 'discountquantity', 'warehouseid', 'taxid', 'displaygroup', 'remarks', {'requestconversionfactor' : 'uomconversionfactor'}, {'requestrateconversionfactor' : 'billingconversionfactor'}, {'itemrequestitemsid_usebillinguom' : 'usebillinguom'}, 'projectestimationitemsid', 'projectestimationitemsid_rate','approvedmakes']);

				this.customFieldsOperation('itemrequestitems', tempChildObj, purchaseorderitemArray[i], 'purchaseorderitems');

				tempObj.purchaseorderitems.push(tempChildObj);
			}

			utils.assign(tempObj, params, ['companyid', 'warehouseid', 'projectid', 'projectid_projectname',  'projectid_projectno', 'projectid_displayname', 'taxid', {'deliveryaddress': 'projectid_deliveryaddress'}, {'deliveryaddressid': 'projectid_deliveryaddressid'}, {'deliverydate': 'requireddate'}, 'defaultcostcenter']);

			this.customFieldsOperation('itemrequests', tempObj, params, 'purchaseorders');		
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.getTandC();
		if(this.props.location.params && this.props.location.params.param == 'Supplier Quotation')
			this.controller.onLoadKit(0, []);

		if(this.props.location.params && ['Sales Order', 'Item Requests for Project', 'Project Purchase Planner', 'Purchase Planner'].indexOf(this.props.location.params.param) >= 0)
			this.controller.getRateFn(0, itemrequestidArr, saleskitArr, this.props.location.params.param);

		this.controller.currencyOnChange();
		this.controller.computeFinalRate();
	}, 0);

	this.updateLoaderFlag(false);
}

export function renderTableQtyfield(item) {
	return (
		<RenderItemQty status={this.props.resource.status} resource={this.props.resource} item={item} uomObj={this.props.app.uomObj} statusArray={['Approved','Sent To Supplier','Hold']} />
	);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function getItemById () {
	axios.get(`/api/purchaseorders/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			for (var i = 0; i < tempObj.purchaseorderitems.length; i++) {
				if (tempObj.purchaseorderitems[i].uomconversiontype)
					tempObj.purchaseorderitems[i].alternateuom = true;
			}
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			setTimeout(this.controller.getSupplierQuotation, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getKititem (itemreqarray, saleskitArr) {
	if(itemreqarray.length == 0)
		return null;

	let url = `/api/kititemitemrequestdetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,costratio,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid&filtercondition=kititemitemrequestdetails.parentid in (${itemreqarray.join()})`;
	let parentidfield = 'itemrequestid';
	let parentindexfield = 'itemrequest_index';

	axios.get(url).then((response) => {
		if(response.data.message == 'success') {
			let kititempurchaseorderdetails = [...this.props.resource.kititempurchaseorderdetails];
			let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;

			this.props.resource.purchaseorderitems.forEach((item) => {
				if(!item.newitem)
					return null;

				for (var j = 0; j < response.data.main.length; j++) {
					if(response.data.main[j].parentid == item[parentidfield] && item[parentindexfield] == response.data.main[j].rootindex) {
						kititempurchaseorderdetails.push({
							itemid: response.data.main[j].itemid,
							itemid_name: response.data.main[j].itemid_name,
							parent: response.data.main[j].parent,
							uomid: response.data.main[j].uomid,
							uomid_name: response.data.main[j].uomid_name,
							quantity: response.data.main[j].quantity,
							rate: response.data.main[j].rate,
							conversion: response.data.main[j].conversion,
							costratio: response.data.main[j].costratio,
							itemid_issaleskit: response.data.main[j].itemid_issaleskit,
							itemid_hasserial: response.data.main[j].itemid_hasserial,
							itemid_hasbatch: response.data.main[j].itemid_hasbatch,
							itemid_keepstock: response.data.main[j].itemid_keepstock,
							rootindex: item.index,
							warehouseid: item.warehouseid,
							index: response.data.main[j].index,
							parentindex: response.data.main[j].parentindex
						});
					}
				}

				item.newitem = false;
			});
			this.props.resource.purchaseorderitems.forEach((invitem) => {
				for (var i = 0; i < kititempurchaseorderdetails.length; i++) {
					if (invitem.index == kititempurchaseorderdetails[i].rootindex) {
						if (!kititempurchaseorderdetails[i].parentindex) {
							let tempIndex = kititempurchaseorderdetails[i].index;
							let tempQuantity = Number((kititempurchaseorderdetails[i].conversion * invitem.quantity).toFixed(roundOffPrecisionStock));
							kititempurchaseorderdetails[i].quantity = tempQuantity;

							let tempRate = Number(((invitem.rate * ((kititempurchaseorderdetails[i].costratio) / 100)) / kititempurchaseorderdetails[i].conversion).toFixed(roundOffPrecisionLocal));
							kititempurchaseorderdetails[i].rate = tempRate;

							calculateQuantity(tempIndex, tempQuantity, tempRate, invitem.index);
						}
					}
				}
			});

			function calculateQuantity(tempIndex, tempQuantity, tempRate, index) {
				for (var j = 0; j < kititempurchaseorderdetails.length; j++) {
					if (tempIndex == kititempurchaseorderdetails[j].parentindex && index == kititempurchaseorderdetails[j].rootindex) {
						let sectempQty = Number((kititempurchaseorderdetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
						kititempurchaseorderdetails[j].quantity = sectempQty;

						let subItemtemprate = Number(((tempRate * ((kititempurchaseorderdetails[j].costratio) / 100)) / kititempurchaseorderdetails[j].conversion).toFixed(roundOffPrecisionLocal));
						kititempurchaseorderdetails[j].rate = subItemtemprate;

						calculateQuantity(kititempurchaseorderdetails[j].index, sectempQty, subItemtemprate, index);
					}
				}
			}

			this.props.updateFormState(this.props.form, {
				kititempurchaseorderdetails
			});
			this.updateLoaderFlag(false);
			this.controller.computeFinalRate();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function onLoadKit (count, kititempurchaseorderdetails) {
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;
	if(count < this.props.resource.purchaseorderitems.length) {
		if(this.props.resource.purchaseorderitems[count].itemid_issaleskit) {
			let promise1 = commonMethods.getItemDetails(this.props.resource.purchaseorderitems[count].itemid, {customerid:this.props.resource.partnerid, partneraddressid:this.props.resource.supplieraddressid}, null, 'purchase');
			promise1.then((returnObject)=> {
				returnObject.kititems.forEach((kititem) => {
					kititem.rootindex = this.props.resource.purchaseorderitems[count].index;
					if(!kititem.parentindex) {
						let tempQuantity = Number((kititem.conversion * this.props.resource.purchaseorderitems[count].quantity).toFixed(roundOffPrecisionStock));
						calculateQuantity(kititem.index, tempQuantity);
						kititem.quantity = tempQuantity;
					}
				});
				function calculateQuantity(tempIndex, tempQuantity) {
					returnObject.kititems.forEach((kititem) => {
						if (kititem.parentindex == tempIndex) {
							let sectempQty = Number((kititem.conversion * tempQuantity).toFixed(roundOffPrecisionStock));
							kititem.quantity = sectempQty;
							calculateQuantity(kititem.index, sectempQty);
						}
					});
				}
				returnObject.kititems.forEach((kititem) => {
					kititempurchaseorderdetails.push(kititem);
				});
				this.controller.onLoadKit(count+1, kititempurchaseorderdetails);
			}, (reason)=> {});
		} else {
			this.controller.onLoadKit(count+1, kititempurchaseorderdetails);
		}
	} else {
		this.props.updateFormState(this.props.form, {
			kititempurchaseorderdetails
		});
		this.controller.computeFinalRate();
	}
}

export function quantityOnChange(itemqty, item, itemstr) {
	let kititempurchaseorderdetails = [...this.props.resource.kititempurchaseorderdetails];
	let tempObj = {};
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;

	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
	}

	for (var i = 0; i < kititempurchaseorderdetails.length; i++) {
		if (item.index == kititempurchaseorderdetails[i].rootindex) {
			if (!kititempurchaseorderdetails[i].parentindex) {
				let tempIndex = kititempurchaseorderdetails[i].index;
				let tempQuantity = Number((kititempurchaseorderdetails[i].conversion * itemqty).toFixed(roundOffPrecisionStock));
				tempObj[`kititempurchaseorderdetails[${i}].quantity`] = tempQuantity;
				calculateQuantity(tempIndex, tempQuantity, item.index);
			}
		}
	}

	function calculateQuantity(tempIndex, tempQuantity, index) {
		for (var j = 0; j < kititempurchaseorderdetails.length; j++) {
			if (tempIndex == kititempurchaseorderdetails[j].parentindex && index == kititempurchaseorderdetails[j].rootindex) {
				let sectempQty = Number((kititempurchaseorderdetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
				tempObj[`kititempurchaseorderdetails[${j}].quantity`] = sectempQty;
				calculateQuantity(kititempurchaseorderdetails[j].index, sectempQty, index);
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function conversionOnChange (item) {
	for (var i = 0; i < this.props.resource.purchaseorderitems.length; i++) {
		if (item.rootindex == this.props.resource.purchaseorderitems[i].index) {
			this.controller.quantityOnChange(this.props.resource.purchaseorderitems[i].quantity, this.props.resource.purchaseorderitems[i], `purchaseorderitems[${i}]`);
			break;
		}
	}
}

export function deleteOrderItem (index) {
	let itemCount = 0;
	let kititempurchaseorderdetails = [...this.props.resource.kititempurchaseorderdetails];
	let purchaseorderitems = [...this.props.resource.purchaseorderitems];

	for (var i = 0; i < kititempurchaseorderdetails.length; i++) {
		if (kititempurchaseorderdetails[i].rootindex == this.props.resource.purchaseorderitems[index].index)
			itemCount++;
	}
	for (var i = 0; i < itemCount; i++) {
		for (var j = 0; j < kititempurchaseorderdetails.length; j++) {
			if (kititempurchaseorderdetails[j].rootindex == this.props.resource.purchaseorderitems[index].index) {
				kititempurchaseorderdetails.splice(j, 1);
				break;
			}
		}
	}
	purchaseorderitems.splice(index, 1);

	this.props.updateFormState(this.props.form, {
		kititempurchaseorderdetails,
		purchaseorderitems
	});
	this.controller.computeFinalRate();
}

export function deleteKitItem(index) {
	let errorArray = [];
	let itemfound = false;
	let kititempurchaseorderdetails = [...this.props.resource.kititempurchaseorderdetails];

	for (var i = 0; i < kititempurchaseorderdetails.length; i++) {
		if (index != i && kititempurchaseorderdetails[index].rootindex == kititempurchaseorderdetails[i].rootindex && kititempurchaseorderdetails[index].parentindex == kititempurchaseorderdetails[i].parentindex) {
			itemfound = true;
		}
	}
	if (!itemfound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	let itemCouFound = false;
	for (var i = 0; i < kititempurchaseorderdetails.length; i++) {
		if (index != i && kititempurchaseorderdetails[index].rootindex == kititempurchaseorderdetails[i].rootindex && !kititempurchaseorderdetails[i].parentindex)
			itemCouFound = true;
	}
	if (!itemCouFound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	let canDelete = true;
	for (var i = 0; i < this.props.resource.purchaseorderitems.length; i++) {
		if (this.props.resource.purchaseorderitems[i].itemrequestitemsid && kititempurchaseorderdetails[index].rootindex == this.props.resource.purchaseorderitems[i].index)
			canDelete = false;
	}
	if (!canDelete) {
		errorArray.push("You can't delete kit items in this transaction");
	}

	if (errorArray.length == 0) {
		let parentIndexArray = [kititempurchaseorderdetails[index].index];
		let rootindex = kititempurchaseorderdetails[index].rootindex;
		deleteArray(kititempurchaseorderdetails[index].rootindex, kititempurchaseorderdetails[index].index);

		function deleteArray(rootindex, index) {
			for (var i = 0; i < kititempurchaseorderdetails.length; i++) {
				if (kititempurchaseorderdetails[i].rootindex == rootindex && kititempurchaseorderdetails[i].parentindex == index) {
					parentIndexArray.push(kititempurchaseorderdetails[i].index)
					deleteArray(rootindex, kititempurchaseorderdetails[i].index);
				}
			}
		}

		for (var i = 0; i < parentIndexArray.length; i++) {
			for (var j = 0; j < kititempurchaseorderdetails.length; j++) {
				if (kititempurchaseorderdetails[j].rootindex == rootindex && kititempurchaseorderdetails[j].index == parentIndexArray[i]) {
					kititempurchaseorderdetails.splice(j, 1);
					break;
				}
			}
		}

		this.props.updateFormState(this.props.form, {kititempurchaseorderdetails});
	} else {
		let response = {
			data : {
				message : 'failure',
				error : utils.removeDuplicate(errorArray)
			}
		};
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
}

export function getRateFn (count, itemrequestidArr, saleskitArr, param) {
	if (count < this.props.resource.purchaseorderitems.length && param != 'Project Purchase Planner') {
		let promise = commonMethods.getItemDetails(this.props.resource.purchaseorderitems[count].itemid, null, null, 'purchase', this.props.resource.purchaseorderitems[count].uomid, this.props.resource.purchaseorderitems[count].alternateuom, this.props.resource.purchaseorderitems[count].uomconversionfactor);
		promise.then((returnObject) => {
			let tempObj = {};
			tempObj[`purchaseorderitems[${count}].uomid`] = returnObject.uomid;
			tempObj[`purchaseorderitems[${count}].uomid_name`] = returnObject.uomid_name;
			tempObj[`purchaseorderitems[${count}].taxid`] = (this.props.resource && this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
			tempObj[`purchaseorderitems[${count}].alternateuom`] = returnObject.alternateuom;

			if (returnObject.alternateuom) {
				tempObj[`purchaseorderitems[${count}].uomconversionfactor`] = returnObject.uomconversionfactor;
				tempObj[`purchaseorderitems[${count}].uomconversiontype`] = returnObject.uomconversiontype;
			} else {
				tempObj[`purchaseorderitems[${count}].uomconversionfactor`] = null;
				tempObj[`purchaseorderitems[${count}].uomconversiontype`] = null;
			}

			tempObj[`purchaseorderitems[${count}].itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
			tempObj[`purchaseorderitems[${count}].usebillinguom`] = returnObject.usebillinguom;
			tempObj[`purchaseorderitems[${count}].billinguomid`] = returnObject.billinguomid;
			tempObj[`purchaseorderitems[${count}].billingconversiontype`] = returnObject.billingconversiontype;
			tempObj[`purchaseorderitems[${count}].billingconversionfactor`] = returnObject.billingconversionfactor;

			let purchaserate = returnObject.rate;

			if(this.props.resource.purchaseorderitems[count].projectestimationitemsid) {
				let itemConversionfactor = returnObject.usebillinguom ? returnObject.billingconversionfactor : returnObject.uomconversionfactor;
				/*if(this.props.resource.purchaseorderitems[count].itemrequestitemsid_usebillinguom) {
					purchaserate = Number((this.props.resource.purchaseorderitems[count].projectestimationitemsid_rate * (this.props.resource.purchaseorderitems[count].requestrateconversionfactor ? this.props.resource.purchaseorderitems[count].requestrateconversionfactor : 1) / (itemConversionfactor ? itemConversionfactor : 1)).toFixed(this.props.app.roundOffPrecision));
				} else {*/
					purchaserate = Number((this.props.resource.purchaseorderitems[count].projectestimationitemsid_rate * (this.props.resource.purchaseorderitems[count].requestconversionfactor ? this.props.resource.purchaseorderitems[count].requestconversionfactor : 1) / (itemConversionfactor ? itemConversionfactor : 1)).toFixed(this.props.app.roundOffPrecision));
				//}
			}

			tempObj[`purchaseorderitems[${count}].rate`] = commonMethods.getRate(purchaserate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			tempObj[`purchaseorderitems[${count}].quantity`] = Number(((this.props.resource.purchaseorderitems[count].quantity * (tempObj[`purchaseorderitems[${count}].uomconversionfactor`] ? tempObj[`purchaseorderitems[${count}].uomconversionfactor`] : 1)) / (this.props.resource.purchaseorderitems[count].requestconversionfactor ? this.props.resource.purchaseorderitems[count].requestconversionfactor : 1)).toFixed(this.props.app.roundOffPrecisionStock));

			if(tempObj[`purchaseorderitems[${count}].usebillinguom`]) {
				tempObj[`purchaseorderitems[${count}].billingrate`] = tempObj[`purchaseorderitems[${count}].rate`];

				tempObj[`purchaseorderitems[${count}].rate`] = Number(((tempObj[`purchaseorderitems[${count}].billingrate`] / (tempObj[`purchaseorderitems[${count}].uomconversionfactor`] ? tempObj[`purchaseorderitems[${count}].uomconversionfactor`] : 1)) * tempObj[`purchaseorderitems[${count}].billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));

				tempObj[`purchaseorderitems[${count}].billingquantity`] = Number(((tempObj[`purchaseorderitems[${count}].quantity`] / (tempObj[`purchaseorderitems[${count}].uomconversionfactor`] ? tempObj[`purchaseorderitems[${count}].uomconversionfactor`] : 1)) * tempObj[`purchaseorderitems[${count}].billingconversionfactor`]).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);

			this.controller.getRateFn(count+1, itemrequestidArr, saleskitArr, param);
		}, (reason) => {});
	} else {
		if(itemrequestidArr.length > 0 && saleskitArr.length >0)
			this.controller.getKititem(itemrequestidArr, saleskitArr);
		else
			this.controller.computeFinalRate();
	}
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Purchase Order' and termsandconditions.isdefault`).then((response) => {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function tandcOnchange(id, valueobj) {
	if(id) {
		this.props.updateFormState(this.props.form, {
			tandc: valueobj.terms
		});
	}
}

export function getSupplierQuotation() {
	if(!this.props.resource.supplierquoteid_rfqid || !this.props.resource.supplierquoteid)
		return null;

	axios.get(`/api/supplierquotation?field=id,supplierquoteno,rfqid,supplierid,partners/name/partnerid,finaltotal&filtercondition=supplierquotation.status='Approved' AND supplierquotation.rfqid=${this.props.resource.supplierquoteid_rfqid}`).then((response) => {
		if (response.data.message == 'success') {
			this.setState({
				supplierQuotes: response.data.main
			});
			this.controller.getRFQitems();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getRFQitems() {
	let queryString = `/api/rfqitems?field=id,itemid,itemmaster/name/itemid,uomid,uom/name/uomid,quantity&filtercondition=rfqitems.parentid=${this.props.resource.supplierquoteid_rfqid}`;

	axios.get(`${queryString}`).then((response) => {
		if (response.data.message == 'success') {
			this.setState({
				rfqitems: response.data.main
			});
			this.controller.analyseButton();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function analyseButton () {
	let {supplierQuotes} = this.state;
	axios.get(`/api/supplierquotationitems?field=id,parentid,itemid,itemmaster/name/itemid,uomid,uom/name/uomid,quantity,amount&filtercondition=supplierquotationitems.parentid in (${supplierQuotes.map((a) => a.id).join()})`).then((response)=> {
		if (response.data.message == 'success') {
			supplierQuotes.forEach((quote) => {
				quote.supplierquotationitems = {};
				response.data.main.map((quoteitem) => {
					if(quote.id == quoteitem.parentid)
						quote.supplierquotationitems[quoteitem.itemid] = quoteitem;
				});
			});

			this.setState({ supplierQuotes });
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getproductionorders () {
	axios.get(`/api/productionorders?field=id,orderno,orderdate,rawmaterialwarehouseid&filtercondition=productionorders.status='Approved'`).then((response) => {
		if (response.data.message == 'success') {
			this.setState({
				productionordersarray: response.data.main
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function productionorderOnChange () {
	for(var i = 0; i < this.state.productionordersarray.length; i++) {
		if(this.props.resource.productionorderid.indexOf(this.state.productionordersarray[i].id) > -1) {
			this.props.updateFormState(this.props.form, {
				warehouseid: this.state.productionordersarray[i].rawmaterialwarehouseid
			});
			break;
		}
	}
}

export function currencyOnChange () {
	if (this.props.resource.pricelistid) {
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid) {
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});
		}
	}
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function callBackSupplier(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		suppliercontactperson: null,
		suppliermobile: null,
		supplierphone: null,
		supplieremail: null,
		partnerid_name: valueobj.name,
		pricelistid: valueobj.purchasepricelistid,
		creditperiod: valueobj.purchasecreditperiod,
		suppliercontactid: valueobj.contactid,
		supplieremail: valueobj.contactid_email,
		suppliermobile: valueobj.contactid_mobile,
		supplierphone: valueobj.contactid_phone,
		suppliercontactperson: valueobj.contactid_name,
		supplieraddressid : valueobj.addressid,
		supplieraddress : valueobj.addressid_displayaddress,
		paymentterms : valueobj.purchasepaymentterms
	});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson: valueobj.name,
		phone: valueobj.phone,
		email: valueobj.email,
		mobile: valueobj.mobile,
		contactid_name: valueobj.name,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile
	});
}

export function suppliercontactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		suppliercontactperson: valueobj.name,
		supplieremail: valueobj.email,
		suppliermobile: valueobj.mobile,
		supplierphone: valueobj.phone
	});
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
	}
	if(address == 'supplieraddress') {
		tempObj.supplieraddress = addressobj.displayaddress;
		tempObj.supplieraddressid = addressobj.id;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function deliverydateonchange() {
	let tempObj = {};
	for (var i = 0; i < this.props.resource.purchaseorderitems.length; i++) {
		if (!this.props.resource.purchaseorderitems[i].deliverydate || this.props.resource.purchaseorderitems[i].deliverydate == null) {
			tempObj[`purchaseorderitems[${i}].deliverydate`] = this.props.resource.deliverydate;
		}
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackproject (id, valueobj) {
	let tempObj = {
		deliveryaddress: valueobj.deliveryaddress,
		deliveryaddressid: valueobj.deliveryaddressid,
		defaultcostcenter: valueobj.defaultcostcenter,
		warehouseid: valueobj.warehouseid
	};
	this.customFieldsOperation('projects', tempObj, valueobj, 'purchaseorders');
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = valueobj.parentid;
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid:this.props.resource.partnerid, partneraddressid:this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.itemid_recommendedsellingprice`] = returnObject.itemid_recommendedsellingprice;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'purchaseorderitems', itemstr);
		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		if (item.index) {
			let itemCount = 0;
			tempObj.kititempurchaseorderdetails = [...this.props.resource.kititempurchaseorderdetails];
			for (var i = 0; i < tempObj.kititempurchaseorderdetails.length; i++) {
				if (tempObj.kititempurchaseorderdetails[i].rootindex == item.index)
					itemCount++;
			}
			for (var i = 0; i < itemCount; i++) {
				for (var j = 0; j < tempObj.kititempurchaseorderdetails.length; j++) {
					if (tempObj.kititempurchaseorderdetails[j].rootindex == item.index) {
						tempObj.kititempurchaseorderdetails.splice(j, 1);
						break;
					}
				}
			}

			if (returnObject.itemid_issaleskit) {
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = item.index;
					tempObj.kititempurchaseorderdetails.push(returnObject.kititems[i]);
				}
			}
		} else {
			let index = 0;
			for (var i = 0; i < this.props.resource.purchaseorderitems.length; i++) {
				if (this.props.resource.purchaseorderitems[i].index > index)
					index = this.props.resource.purchaseorderitems[i].index;
			}
			tempObj[`${itemstr}.index`] = index + 1;

			if (returnObject.itemid_issaleskit) {
				tempObj.kititempurchaseorderdetails = [...this.props.resource.kititempurchaseorderdetails];
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = tempObj[`${itemstr}.index`];
					tempObj.kititempurchaseorderdetails.push(returnObject.kititems[i]);
				}
			}
		}

		this.props.updateFormState(this.props.form, tempObj);

		setTimeout(() => {
			let tempchilditem = this.selector(this.props.fullstate, itemstr);
			this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
			this.controller.rateOnChange(itemstr);
		}, 0);
	}, (reason) => {});
}

export function rateOnChange (itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	let finalrate = item.rate;
	let tempObj = {};
	let kititempurchaseorderdetails = this.props.resource.kititempurchaseorderdetails;

	if (item.discountquantity != null && item.discountquantity != "" && item.discountquantity != "undefined") {
		if(item.discountmode=='Percentage')
			finalrate = finalrate - ((finalrate * item.discountquantity) / 100);
		else
			finalrate = finalrate - item.discountquantity;
	}
	for (var i = 0; i < kititempurchaseorderdetails.length; i++) {
		if (item.index == kititempurchaseorderdetails[i].rootindex) {
			if (!kititempurchaseorderdetails[i].parentindex) {
				let tempRate = Number(((finalrate * ((kititempurchaseorderdetails[i].costratio) / 100)) / kititempurchaseorderdetails[i].conversion).toFixed(roundOffPrecisionLocal));
				let tempIndex = kititempurchaseorderdetails[i].index;
				tempObj[`kititempurchaseorderdetails[${i}].rate`] = tempRate;
				
				calculaterate(tempIndex, tempRate, item.index);
			}
		}
	}

	function calculaterate(tempIndex, tempRate, index) {
		for (var j = 0; j < kititempurchaseorderdetails.length; j++) {
			if (tempIndex == kititempurchaseorderdetails[j].parentindex && index == kititempurchaseorderdetails[j].rootindex) {
				let subItemtemprate = Number(((tempRate * ((kititempurchaseorderdetails[j].costratio) / 100)) / kititempurchaseorderdetails[j].conversion).toFixed(roundOffPrecisionLocal));
				tempObj[`kititempurchaseorderdetails[${j}].rate`] = subItemtemprate;
				calculaterate(kititempurchaseorderdetails[j].index, subItemtemprate, index);
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function computeFinalRate() {
	setTimeout(() => {
		let tempObj = {};
		for (var i = 0; i < this.props.resource.kititempurchaseorderdetails.length; i++) {
			if(!this.props.resource.kititempurchaseorderdetails[i].itemid_issaleskit) {
				if (this.props.resource.currencyid == this.props.app.defaultCurrency) {
					tempObj[`kititempurchaseorderdetails[${i}].ratelc`] = this.props.resource.kititempurchaseorderdetails[i].rate;
				} else {
					if (this.props.resource.currencyexchangerate)
						tempObj[`kititempurchaseorderdetails[${i}].ratelc`] = this.props.resource.kititempurchaseorderdetails[i].rate * this.props.resource.currencyexchangerate;
					else
						tempObj[`kititempurchaseorderdetails[${i}].ratelc`] = 0;
				}
			}
		}		

		this.props.updateFormState(this.props.form, tempObj);
		setTimeout(() => {
			taxEngine(this.props, 'resource', 'purchaseorderitems');
		}, 0);
	}, 0);
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);
	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid:this.props.resource.partnerid, partneraddressid:this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);

	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid:this.props.resource.partnerid, partneraddressid:this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;

		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;

		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function save (param, confirm) {
	if (param == 'Send To Supplier') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'purchaseorders'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});

	} else {
		this.updateLoaderFlag(true);

		if(param !='Update' && param !='Hold' && param !='Unhold' && param !='Cancel' && param !='Delete' && param !='Amend' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/purchaseorders'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					this.props.history.replace(`/details/purchaseorders/${response.data.main.id}`);
				} else {
					if (param == 'Delete')
						this.props.history.replace("/list/purchaseorders");
					else
						this.props.initialize(response.data.main);
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function createReceiptNote () {
	checkTransactionExist('receiptnotes', 'purchaseorders', this.props.resource.id, this.openModal, (param) => {
		if(param) {
			this.props.history.push({
				pathname: '/createReceiptNote',
				params: {
					...this.props.resource,
					param: 'Purchase Orders'
				}
			});
		}
	});
}

export function getItemRequest () {
	this.openModal({render: (closeModal) => {
		return <PurchaseOrderItemRequestAddModal resource={this.props.resource} jsonname={'purchaseorderitems'} openModal={this.props.openModal} closeModal={closeModal} callback={this.controller.itemreqCallBack} />
	}, className: {
		content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
	});
}

export function itemreqCallBack(itemRequestItemsArray) {
	let purchaseorderitems = [...this.props.resource.purchaseorderitems], kititempurchaseorderdetails = [...this.props.resource.kititempurchaseorderdetails];

	this.updateLoaderFlag(true);

	let itemrequestidArr = [], saleskitArr = [];

	let index = 0, itempushedflag = false;

	for (var k = 0; k < purchaseorderitems.length; k++) {
		if (purchaseorderitems[k].index > index)
			index = purchaseorderitems[k].index;
	}

	for(var i=0; i<itemRequestItemsArray.length; i++) {
		let itemFound=false;
		for(var j = 0; j < purchaseorderitems.length; j++) {
			if(purchaseorderitems[j].itemrequestitemsid == itemRequestItemsArray[i].itemrequestitemsid) {
				itemFound=true;
				if (!itemRequestItemsArray[i].checked) {
					let itemCount = 0;
					for (var l = 0; l < kititempurchaseorderdetails.length; l++) {
						if (kititempurchaseorderdetails[l].rootindex == purchaseorderitems[j].index) {
							kititempurchaseorderdetails.splice(l, 1);
							l=-1;
						}
					}
					purchaseorderitems.splice(j, 1);
				}
				break;
			}
		}
		if(!itemFound && itemRequestItemsArray[i].checked) {
			index++;
			itempushedflag = true;

			itemRequestItemsArray[i].quantity = itemRequestItemsArray[i].suggestedqty;
			itemRequestItemsArray[i].uomid = itemRequestItemsArray[i].purchaseuomid;
			itemRequestItemsArray[i].uomid_name = itemRequestItemsArray[i].purchaseuomid_name;

			if(itemrequestidArr.indexOf(itemRequestItemsArray[i].itemrequestid) == -1)
				itemrequestidArr.push(itemRequestItemsArray[i].itemrequestid);

			if(itemRequestItemsArray[i].itemid_issaleskit)
				saleskitArr.push(itemRequestItemsArray[i].itemid);

			let tempChildObj = {
				index: index,
				alternateuom: itemRequestItemsArray[i].uomconversiontype ? true : false,
				newitem: true,
				deliverydate: itemRequestItemsArray[i].deliverydate ? itemRequestItemsArray[i].deliverydate : this.props.resource.deliverydate
			};

			utils.assign(tempChildObj, itemRequestItemsArray[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_keepstock', 'itemid_uomgroupid', 'description', 'specification', 'rate', 'quantity', 'uomid', 'uomid_name', 'discountmode', 'discountquantity', 'warehouseid', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'remarks', 'usebillinguom', 'billinguomid', 'billinguomid_name', 'billingconversiontype', 'billingconversionfactor', 'itemrequestitemsid', {'itemrequest_index' : 'index'}, 'itemrequestid', {'billingrate' : 0},'approvedmakes']);

			tempChildObj.quantity = Number(((itemRequestItemsArray[i].quantity * (itemRequestItemsArray[i].uomconversionfactor ? itemRequestItemsArray[i].uomconversionfactor : 1)) / (itemRequestItemsArray[i].requestconversionfactor ? itemRequestItemsArray[i].requestconversionfactor : 1)).toFixed(this.props.app.roundOffPrecisionStock));

			let purchaserate = itemRequestItemsArray[i].rate;

			if(itemRequestItemsArray[i].projectestimationitemsid) {
				let itemConversionfactor = itemRequestItemsArray[i].usebillinguom ? purchaseorderitemArray[i].billingconversionfactor : itemRequestItemsArray[i].uomconversionfactor;
				/*if(purchaseorderitemArray[i].itemrequestitemsid_usebillinguom) {
					purchaserate = Number((purchaseorderitemArray[i].projectestimationitemsid_rate * (purchaseorderitemArray[i].requestrateconversionfactor ? purchaseorderitemArray[i].requestrateconversionfactor : 1) / (itemConversionfactor ? itemConversionfactor : 1)).toFixed(this.props.app.roundOffPrecision));
				} else {*/
					purchaserate = Number((itemRequestItemsArray[i].projectestimationitemsid_rate * (itemRequestItemsArray[i].requestconversionfactor ? itemRequestItemsArray[i].requestconversionfactor : 1) / (itemConversionfactor ? itemConversionfactor : 1)).toFixed(this.props.app.roundOffPrecision));
				//}
			}

			tempChildObj.rate = purchaserate;

			if(itemRequestItemsArray[i].usebillinguom) {
				tempChildObj.billingrate = tempChildObj.rate;

				tempChildObj.rate = Number(((tempChildObj.billingrate / (itemRequestItemsArray[i].uomconversionfactor ? itemRequestItemsArray[i].uomconversionfactor : 1)) * itemRequestItemsArray[i].billingconversionfactor).toFixed(this.props.app.roundOffPrecision));

				tempChildObj.billingquantity = Number(((tempChildObj.quantity / (itemRequestItemsArray[i].uomconversionfactor ? itemRequestItemsArray[i].uomconversionfactor : 1)) * itemRequestItemsArray[i].billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.customFieldsOperation('itemrequestitems', tempChildObj, itemRequestItemsArray[i], 'purchaseorderitems');

			purchaseorderitems.push(tempChildObj);
		}
	}

	this.props.updateFormState(this.props.form, { purchaseorderitems, kititempurchaseorderdetails });

	if(itempushedflag) {
		setTimeout(() => {
			if(itemrequestidArr.length > 0 && saleskitArr.length > 0) {
				this.controller.getKititem(itemrequestidArr, saleskitArr);
			} else {
				this.controller.computeFinalRate();
				this.updateLoaderFlag(false);
			}
		}, 0);
	} else {
		this.updateLoaderFlag(false);
	}
}

export function createPayment() {
	this.props.history.push({
		pathname: '/createPaymentVoucher',
		params: {
			...this.props.resource,
			param: 'Purchase Orders'
		}
	});
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Supplier',
			data : emailObj
		},
		url : '/api/purchaseorders'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.supplieremail;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					var apiResponse = commonMethods.apiResult(response);
					this.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				var apiResponse = commonMethods.apiResult(response);
				this.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			var apiResponse = commonMethods.apiResult(response);
			this.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function analyseitemRate (item) {
	let errorArray = [];
	if (!(item.itemid > 0))
		errorArray.push('Item Name is required!!!');

	if (!(this.props.resource.partnerid > 0))
		errorArray.push('Supplier Name is required!!!');

	if (!(item.warehouseid > 0))
		errorArray.push('Warehouse Name is required!!!');

	if (errorArray.length > 0)
		this.openModal(modalService.infoMethod({
			header : "Error",
			body : errorArray,
			btnArray : ["Ok"]
		}));
	else {
		this.props.openModal({render: (closeModal) => {return <AnalysePurchaseRateModal item={item} param="purchaseorders" partnerid={this.props.resource.partnerid} partnerid_name={this.props.resource.partnerid_name} closeModal={closeModal} openModal={this.props.openModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
	}
}

export function openSupplierQuotations () {
	setTimeout(() => {
		this.openModal({
			render: (closeModal) => {
				return <AnalyseSupplierquotationModal rfqitems={this.state.rfqitems} supplierquotes={this.state.supplierQuotes}  history={this.props.history} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}, 0);
}

export function openStockDetails (item) {
	this.openModal({render: (closeModal) => {return <StockdetailsModal resource = {this.props.resource} item={item} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function cancel () {
	this.props.history.goBack();
}
