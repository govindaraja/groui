import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation, checkStatus } from '../utils/services';
import StockdetailsModal from '../containers/stockdetails';
import {ChildEditModal} from '../components/utilcomponents';
import PickingdetailsModal from '../components/details/receiptpickingdetailsmodal';
import EmailModal from '../components/details/emailmodal';

let roundOffPrecisionLocal = 5;

export function onLoad () {
	roundOffPrecisionLocal = this.props.app.roundOffPrecision;

	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let orderArray = [], deliveryidArray = [], itemreqArr = [];
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		receiptnotedate: new Date(new Date().setHours(0, 0, 0, 0)),
		receiptnoteitems: [],
		kititemreceiptdetails: [],
		receiptitemvaluation: [],
		additionalcharges: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'Purchase Orders') {
			let purchaseorderitemArray = params.purchaseorderitems;
			tempObj.ispurchaseorder = 'Purchase Orders';

			utils.assign(tempObj, params, ['partnerid', 'companyid', 'defaultcostcenter', 'currencyid', {'purchaseorderid_ponumber' : 'ponumber'}, {'contactid' : 'suppliercontactid'}, {'mobile' : 'suppliermobile'}, {'email' : 'supplieremail'}, {'contactid_mobile' : 'suppliermobile'}, {'contactid_email' : 'supplieremail'}, {'contactid_phone' : 'supplierphone'}, {'billingcontactid' : 'contactid'}, {'billingcontactid_mobile' : 'mobile'}, {'billingcontactid_email' : 'email'}, {'billingcontactid_phone' : 'phone'}, 'warehouseid', 'supplieraddress', 'supplieraddressid', 'deliveryaddress', 'deliveryaddressid', 'billingaddress', 'billingaddressid', 'supplierreference', 'taxid', 'remarks', 'projectid', 'projectid_projectname', 'projectid_projectname', 'projectid_displayname']);

			this.customFieldsOperation('purchaseorders', tempObj, params, 'receiptnotes');

			params.purchaseorderitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;
				if(item.quantity > item.deliveredqty) {
					if(orderArray.indexOf(item.parentid) == -1)
						orderArray.push(item.parentid);

					if(item.itemid_itemtype != 'Service') {
						let tempChildObj = {
							index: index + 1,
							alternateuom: item.uomconversiontype ? true : false,
							quantity: (item.quantity - item.deliveredqty),
							incomingqty: (item.quantity - item.deliveredqty),
							sourceresource: 'purchaseorderitems'
						};
						utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'itemid_issaleskit', 'description', 'specification', 'uomid', 'uomid_name', 'uomconversionfactor', 'uomconversiontype', {'sourceid' : 'id'}, {'purchaseorderid' : 'parentid'}, 'warehouseid', {'purchaseorderitemsid' : 'id'}, 'remarks', {'order_index' : 'index'}, 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor','itemmakeid']);

						tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

						this.customFieldsOperation('purchaseorderitems', tempChildObj, item, 'receiptnoteitems');
						tempObj.receiptnoteitems.push(tempChildObj);
					}
				}
			});

			if(orderArray.length == 1)
				tempObj.purchaseorderid = orderArray[0];
		}

		if(params.param == 'Sales Returns') {
			tempObj.ispurchaseorder = 'Sales Returns';

			if (params.againstorder)
				tempObj['taxid'] = params.taxid;

			utils.assign(tempObj, params, [{'partnerid' : 'customerid'}, {'salesreturnid' : 'id'}, {'salesreturnid_salesreturnno' : 'salesreturnno'}, 'companyid', 'defaultcostcenter', 'currencyid', 'warehouseid', {'salesreturnid_againstorder': 'againstorder'}]);

			this.customFieldsOperation('salesreturns', tempObj, params, 'receiptnotes');

			params.salesreturnitems.forEach((item, index) => {
				item.qtyreturned = item.qtyreturned ? item.qtyreturned : 0;

				if (params.againstorder) {
					if(item.qtyauthorized > item.qtyreturned) {
						if(deliveryidArray.indexOf(item.deliverynoteitemsid_parentid) == -1)
							deliveryidArray.push(item.deliverynoteitemsid_parentid);

						let tempChildObj = {
							index: index + 1,
							alternateuom: item.uomconversiontype ? true : false,
							quantity: (item.qtyauthorized - item.qtyreturned),
							incomingqty: (item.qtyauthorized - item.qtyreturned),
							sourceresource: 'salesreturnitems'
						};

						utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_uomgroupid', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'itemid_issaleskit', {'description' : 'deliverynoteitemsid_description'}, {'uomid' : 'deliverynoteitemsid_uomid'}, {'sourceid' : 'id'}, {'deliverynoteid' : 'deliverynoteitemsid_parentid'}, 'uomconversionfactor', 'uomconversiontype', {'deliveryindex' : 'deliverynoteitemsid_index'}, {'warehouseid' : 'deliverynoteitemsid_warehouseid'}]);

						this.customFieldsOperation('salesreturnitems', tempChildObj, item, 'receiptnoteitems');

						tempObj.receiptnoteitems.push(tempChildObj);
					}
				} else {
					if (item.qtyauthorized > item.qtyreturned) {
						let tempChildObj = {
							index: index + 1,
							alternateuom: item.uomconversiontype ? true : false,
							quantity: (item.qtyauthorized - item.qtyreturned),
							incomingqty: (item.qtyauthorized - item.qtyreturned),
							sourceresource: 'salesreturnitems'
						};

						utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_uomgroupid', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'itemid_issaleskit', 'description', 'uomid', 'uomid_name', {'sourceid' : 'id'}, 'uomconversionfactor', 'uomconversiontype', 'warehouseid', 'remarks', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

						tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

						this.customFieldsOperation('salesreturnitems', tempChildObj, item, 'receiptnoteitems');

						tempObj.receiptnoteitems.push(tempChildObj);
					}
				}
			});
		}

		if(params.param == 'Stock Transfer') {
			tempObj.ispurchaseorder = 'Stock Transfer';

			utils.assign(tempObj, params, ['companyid', {'stocktransferid' : 'id'}, {'warehouseid' : 'destinationwarehouseid'}]);

			this.customFieldsOperation('stocktransfer', tempObj, params, 'receiptnotes');

			params.stocktransferitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;
				item.receivedqty = item.receivedqty ? item.receivedqty : 0;

				if(item.deliveredqty > item.receivedqty) {
					let tempChildObj = {
						index: index + 1,
						quantity: (item.deliveredqty - item.receivedqty),
						incomingqty: (item.deliveredqty - item.receivedqty),
						warehouseid: params.destinationwarehouseid,
						sourceresource: 'stocktransferitems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'itemid_issaleskit', 'description', 'uomid', 'uomid_name', {'sourceid' : 'id'}]);

					this.customFieldsOperation('stocktransferitems', tempChildObj, item, 'receiptnoteitems');

					tempObj.receiptnoteitems.push(tempChildObj);
				}
			});
		}

		if(['Item Requests for Service Call', 'Item Requests for Engineer', 'Item Requests for Project', 'Item Requests for Production Order', 'Item Requests for General', 'Item Requests for Job Work'].indexOf(params.param) >= 0) {
			tempObj.ispurchaseorder = params.param;

			utils.assign(tempObj, params, [{'partnerid' : 'customerid'}, 'companyid', 'defaultcostcenter', 'currencyid', 'warehouseid', 'projectid', 'productionorderid', 'workorderid', 'projectid_projectname', 'projectid_displayname', 'servicecallid', 'servicecallid_servicecallno', 'engineerid', 'engineerid_displayname']);

			params.itemrequestitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;
				item.usedqty = item.usedqty ? item.usedqty : 0;
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				item.returnedqty = item.returnedqty ? item.returnedqty : 0;

				let quantity = item.deliveredqty - item.usedqty - item.invoicedqty;

				if(params.param == 'Item Requests for Project' || params.param == 'Item Requests for Job Work')
					quantity = item.deliveredqty;

				if(quantity > item.returnedqty) {
					if(itemreqArr.indexOf(item.parentid) == -1)
						itemreqArr.push(item.parentid);

					let tempChildObj = {
						index: index + 1,
						quantity: (quantity - item.returnedqty),
						incomingqty: (quantity - item.returnedqty),
						sourceresource: 'itemrequestitems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'itemid_issaleskit', 'description', 'specification', 'uomid', 'uomid_name', {'sourceid' : 'id'}, 'uomconversionfactor', 'uomconversiontype', 'warehouseid', {'itemrequestid' : 'parentid'}, {'itemrequest_index' : 'index'}, 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('itemrequestitems', tempChildObj, item, 'receiptnoteitems');

					tempObj.receiptnoteitems.push(tempChildObj);	
				}
			});
		}

		if(params.param == 'Production Order') {
			tempObj.ispurchaseorder = params.param;

			utils.assign(tempObj, params, ['companyid', 'defaultcostcenter', {'warehouseid' : 'finishedgoodswarehouseid'}, {'productionorderid' : 'id'},'currencyid',{'partnerid' : 'customerid'}]);

			this.customFieldsOperation('productionorders', tempObj, params, 'receiptnotes');

			params.productionfinisheditems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;
				if(item.quantity > item.deliveredqty) {

					if(item.itemid_itemtype != 'Service') {
						let tempChildObj = {
							index: index + 1,
							alternateuom: item.uomconversiontype ? true : false,
							quantity: (item.quantity - item.deliveredqty),
							incomingqty: (item.quantity - item.deliveredqty),
							warehouseid : params.finishedgoodswarehouseid,
							sourceresource: 'productionfinisheditems'
						};
						utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'itemid_issaleskit', {'description': 'itemid_description'}, 'uomid', 'uomid_name',{'sourceid' : 'id'}]);

						tempObj.receiptnoteitems.push(tempChildObj);
					}
				}
			});
		}

		if(params.param == 'Job Work') {
			let receiptnoteitemArray = params.workorderinitems;
			tempObj.ispurchaseorder = 'Job Work Return';

			utils.assign(tempObj, params, ['partnerid', 'companyid', 'defaultcostcenter', 'currencyid', {'workorderid' : 'id'}, {'workorderid_wonumber' : 'wonumber'}, {'contactid' : 'suppliercontactid'}, {'mobile' : 'suppliermobile'}, {'email' : 'supplieremail'}, 'warehouseid', 'supplieraddress', 'supplieraddressid', 'deliveryaddress', 'deliveryaddressid', 'billingaddress', 'billingaddressid', 'supplierreference', 'taxid', 'remarks']);

			this.customFieldsOperation('workorders', tempObj, params, 'receiptnotes');

			params.workorderinitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;
				if(item.quantity > item.deliveredqty) {

					if(item.itemid_itemtype != 'Service') {
						let tempChildObj = {
							index: index + 1,
							alternateuom: item.uomconversiontype ? true : false,
							quantity: (item.quantity - item.deliveredqty),
							incomingqty: (item.quantity - item.deliveredqty),
							sourceresource: 'workorderinitems'
						};
						utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'itemid_issaleskit', 'description', 'uomid', 'uomid_name', 'uomconversionfactor', 'uomconversiontype', {'sourceid' : 'id'}, 'warehouseid', 'remarks']);

						this.customFieldsOperation('workorderinitems', tempChildObj, item, 'receiptnoteitems');
						tempObj.receiptnoteitems.push(tempChildObj);
					}
				}
			});
		}
	} else {
		tempObj.ispurchaseorder = 'Others';
	}

	this.props.initialize(tempObj);

	setTimeout(()=> {
		if(this.props.location.params) {
			if(this.props.location.params.param == 'Purchase Orders')
				this.controller.getKititems(orderArray, 'kititempurchaseorderdetails');

			if(this.props.location.params.param == 'Sales Returns')
				this.controller.getKititems(deliveryidArray, 'kititemdeliverydetails');

			if(['Item Requests for Service Call', 'Item Requests for Engineer', 'Item Requests for Project', 'Item Requests for Production Order', 'Item Requests for General', 'Item Requests for Job Work'].indexOf(this.props.location.params.param) >= 0)
				this.controller.getKititems(itemreqArr, 'kititemitemrequestdetails');

			if(this.props.location.params.param == 'Stock Transfer')
				this.controller.setSerialNumberForStockTransfer();
		}
	}, 0);
	
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/receiptnotes/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.receiptnoteitems.forEach((item) => {
				if(item.uomconversiontype)
					item.alternateuom = true;
				tempObj.kititemreceiptdetails.forEach((kititem) => {
					if(kititem.rootindex == item.index)
						kititem.warehouseid = item.warehouseid;
				});
			});
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getKititems (array, param) {
	if(array.length == 0)
		return null;

	let url, parentidfield, parentindexfield;
	if(param == 'kititempurchaseorderdetails') {
		url = `/api/kititempurchaseorderdetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid&filtercondition=kititempurchaseorderdetails.parentid in (${array.join()})`;
		parentidfield = 'purchaseorderid';
		parentindexfield = 'order_index';
	} else if(param == 'kititemdeliverydetails') {
		url = `/api/kititemdeliverydetails?&field=itemid,parentid,parent,uomid,quantity,conversion,rootindex,index,parentindex,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/issaleskit/itemid,uom/name/uomid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid&filtercondition=kititemdeliverydetails.parentid in (${array.join()})`;
		parentidfield = 'deliverynoteid';
		parentindexfield = 'deliveryindex';
	}  else if(param == 'kititemitemrequestdetails') {
		url = `/api/kititemitemrequestdetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid&filtercondition=kititemitemrequestdetails.parentid in (${array.join()})`;
		parentidfield = 'itemrequestid';
		parentindexfield = 'itemrequest_index';
	} else {
		return null;
	}

	axios.get(url).then((response) => {
		if(response.data.message == 'success') {
			let kititemreceiptdetails = [];
			let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;
			this.props.resource.receiptnoteitems.forEach((item) => {
				for (var j = 0; j < response.data.main.length; j++) {
					if(response.data.main[j].parentid == item[parentidfield] && item[parentindexfield] == response.data.main[j].rootindex) {
						kititemreceiptdetails.push({
							itemid : response.data.main[j].itemid,
							itemid_name : response.data.main[j].itemid_name,
							parent : response.data.main[j].parent,
							uomid : response.data.main[j].uomid,
							uomid_name : response.data.main[j].uomid_name,
							quantity : response.data.main[j].quantity,
							conversion : response.data.main[j].conversion,
							itemid_issaleskit : response.data.main[j].itemid_issaleskit,
							itemid_hasserial : response.data.main[j].itemid_hasserial,
							itemid_hasbatch : response.data.main[j].itemid_hasbatch,
							itemid_keepstock : response.data.main[j].itemid_keepstock,
							rootindex: item.index,
							warehouseid: item.warehouseid,
							index: response.data.main[j].index,
							parentindex: response.data.main[j].parentindex
						});
					}
				}
			});
			this.props.resource.receiptnoteitems.forEach((invitem) => {
				for (var i = 0; i < kititemreceiptdetails.length; i++) {
					if (invitem.index == kititemreceiptdetails[i].rootindex) {
						if (!kititemreceiptdetails[i].parentindex) {
							let tempIndex = kititemreceiptdetails[i].index;
							let tempQuantity = Number((kititemreceiptdetails[i].conversion * invitem.quantity).toFixed(roundOffPrecisionStock));
							kititemreceiptdetails[i].quantity = tempQuantity;
							calculateQuantity(tempIndex, tempQuantity, invitem.index);
						}
					}
				}
			});
			function calculateQuantity(tempIndex, tempQuantity, index) {
				for (var j = 0; j < kititemreceiptdetails.length; j++) {
					if (tempIndex == kititemreceiptdetails[j].parentindex && index == kititemreceiptdetails[j].rootindex) {
						let sectempQty = Number((kititemreceiptdetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
						kititemreceiptdetails[j].quantity = sectempQty;
						calculateQuantity(kititemreceiptdetails[j].index, sectempQty, index);
					}
				}
			}

			this.props.updateFormState(this.props.form, {
				kititemreceiptdetails
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function setSerialNumberForStockTransfer () {
	let itemidArray = [];

	this.props.resource.receiptnoteitems.forEach((item) => {
		if(item.itemid_hasserial || item.itemid_hasbatch)
			itemidArray.push(item.sourceid);
	});

	if(itemidArray.length == 0)
		return null;

	this.updateLoaderFlag(true);

	axios.get(`/api/deliverynoteitems?&field=itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,sourceresource,sourceid,pickingdetails&filtercondition=deliverynotes_parentid.status in ('Approved', 'Dispatched') and deliverynoteitems.sourceresource='stocktransferitems' and deliverynoteitems.sourceid in (${itemidArray.join()})`).then((response)=> {
		if(response.data.message == 'success') {
			let tempObj = {}, updateObj = {};
			response.data.main.forEach((item) => {
				if(!(item.pickingdetails && item.pickingdetails.details && item.pickingdetails.details.length > 0))
					return null;

				tempObj[item.sourceid] = {};

				item.pickingdetails.details.map((tempitem) => {
					let groupingVar = item.itemid_hasbatch ? tempitem.itembatchid : 'null';
					if(!tempObj[item.sourceid][groupingVar]) {
						tempObj[item.sourceid][groupingVar] = {
							itembatchid: item.itemid_hasbatch ? tempitem.itembatchid : null,
							itembatchid_batchnumber: item.itemid_hasbatch ? tempitem.itembatchid_batchnumber : null,
							itembatchid_expirydate: item.itemid_hasbatch ? tempitem.itembatchid_expirydate : null,
							itembatchid_manufacturingdate: item.itemid_hasbatch ? tempitem.itembatchid_manufacturingdate : null,
							quantity: 0,
							serialno: []
						};
					}
					if(item.itemid_hasserial) {
						tempObj[item.sourceid][groupingVar].quantity++;
						tempObj[item.sourceid][groupingVar].serialno.push(tempitem.serialno);
					} else {
						tempObj[item.sourceid][groupingVar].quantity += tempitem.quantity;
					}
				});
			});

			this.props.resource.receiptnoteitems.forEach((item, index) => {
				if(!tempObj[item.sourceid])
					return null;

				let pickingDetails = [];
				for(let prop in tempObj[item.sourceid]) {
					let tempSecObj;
					if(item.itemid_hasbatch) {
						tempSecObj = {
							itembatchid: tempObj[item.sourceid][prop].itembatchid,
							itembatchid_batchnumber: tempObj[item.sourceid][prop].itembatchid_batchnumber,
							expirydate: tempObj[item.sourceid][prop].itembatchid_expirydate,
							manufacturingdate: tempObj[item.sourceid][prop].itembatchid_manufacturingdate,
							quantity: tempObj[item.sourceid][prop].quantity,
							warehouseid: item.warehouseid
						}
						if(item.itemid_hasserial)
							tempSecObj.serialArray = tempObj[item.sourceid][prop].serialno.join();
					} else {
						tempSecObj = {
							quantity: tempObj[item.sourceid][prop].quantity,
							warehouseid: item.warehouseid
						}
						tempSecObj.serialArray = tempObj[item.sourceid][prop].serialno.join();
					}
					pickingDetails.push(tempSecObj);
				}
				updateObj[`receiptnoteitems[${index}].pickingdetails`] = {
					details: pickingDetails
				};
			});

			this.props.updateFormState(this.props.form, updateObj);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callBackPartnerName(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		partnerid_name: valueobj.name,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : valueobj.salesperson,
		deliveryaddress: valueobj.addressid_displayaddress,
		billingaddress: valueobj.addressid_displayaddress,
		billingaddressid: valueobj.addressid,
		deliveryaddressid: valueobj.addressid,
		contactid: valueobj.contactid,
		contactid_name: valueobj.contactid_name,
		contactid_email: valueobj.contactid_email,
		contactid_mobile: valueobj.contactid_mobile,
		contactid_phone: valueobj.contactid_phone
	});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson: valueobj.name,
		phone: valueobj.phone,
		email: valueobj.email,
		mobile: valueobj.mobile,
		contactid_name: valueobj.name,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile
	});
}

export function billingcontactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		billingcontactid_name: valueobj.name,
		billingcontactid_phone: valueobj.phone,
		billingcontactid_email: valueobj.email,
		billingcontactid_mobile: valueobj.mobile
	});
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
	}
	if(address == 'supplieraddress') {
		tempObj.supplieraddress = addressobj.displayaddress;
		tempObj.supplieraddressid = addressobj.id;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function setWareHouse () {
	let tempObj = {};
	this.props.resource.receiptnoteitems.forEach((item, index) => {
		tempObj[`receiptnoteitems[${index}].warehouseid`] = item.warehouseid ? item.warehouseid : this.props.resource.warehouseid;
		this.props.resource.kititemreceiptdetails.forEach((kititem, kitindex) => {
			if(kititem.rootindex == item.index)
				tempObj[`kititemreceiptdetails[${kitindex}].warehouseid`] = tempObj[`receiptnoteitems[${index}].warehouseid`];
		});
	});
	this.props.updateFormState(this.props.form, tempObj);
}

export function callBackAccount (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.accountid_name`]: valueobj.name
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = valueobj.parentid;
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid: this.props.resource.partnerid, partneraddressid: this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.rate`] = commonMethods.getRate((returnObject.lastpurchasecost || 0), this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app);
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = returnObject.taxid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.warehouseid`] = this.props.resource.warehouseid;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'receiptnoteitems', itemstr);
		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate((returnObject.lastpurchasecost || 0), this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		if (item.index) {
			let itemCount = 0;
			tempObj.kititemreceiptdetails = [...this.props.resource.kititemreceiptdetails];
			for (var i = 0; i < tempObj.kititemreceiptdetails.length; i++) {
				if (tempObj.kititemreceiptdetails[i].rootindex == item.index)
					itemCount++;
			}
			for (var i = 0; i < itemCount; i++) {
				for (var j = 0; j < tempObj.kititemreceiptdetails.length; j++) {
					if (tempObj.kititemreceiptdetails[j].rootindex == item.index) {
						tempObj.kititemreceiptdetails.splice(j, 1);
						break;
					}
				}
			}

			if (returnObject.itemid_issaleskit) {
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = item.index;
					tempObj.kititemreceiptdetails.push(returnObject.kititems[i]);
				}
			}
		} else {
			let index = 0;
			for (var i = 0; i < this.props.resource.receiptnoteitems.length; i++) {
				if (this.props.resource.receiptnoteitems[i].index > index)
					index = this.props.resource.receiptnoteitems[i].index;
			}
			tempObj[`${itemstr}.index`] = index + 1;

			if (returnObject.itemid_issaleskit) {
				tempObj.kititemreceiptdetails = [...this.props.resource.kititemreceiptdetails];
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = tempObj[`${itemstr}.index`];
					tempObj.kititemreceiptdetails.push(returnObject.kititems[i]);
				}
			}
		}

		this.props.updateFormState(this.props.form, tempObj);
		setTimeout(() => {
			let tempchilditem = this.selector(this.props.fullstate, itemstr);
			this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
			this.controller.warehouseonchange(tempchilditem.warehouseid, tempchilditem, itemstr);
			this.controller.rateOnChange(itemstr);
		}, 0);

	}, (reason) => {});
}

export function quantityOnChange(itemqty, item, itemstr) {
	let kititemreceiptdetails = [...this.props.resource.kititemreceiptdetails];
	let tempObj = {};
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;

	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
	}

	for (var i = 0; i < kititemreceiptdetails.length; i++) {
		if (item.index == kititemreceiptdetails[i].rootindex) {
			if (!kititemreceiptdetails[i].parentindex) {
				let tempIndex = kititemreceiptdetails[i].index;
				let tempQuantity = Number((kititemreceiptdetails[i].conversion * itemqty).toFixed(roundOffPrecisionStock));
				tempObj[`kititemreceiptdetails[${i}].quantity`] = tempQuantity;
				calculateQuantity(tempIndex, tempQuantity, item.index);
			}
		}
	}

	function calculateQuantity(tempIndex, tempQuantity, index) {
		for (var j = 0; j < kititemreceiptdetails.length; j++) {
			if (tempIndex == kititemreceiptdetails[j].parentindex && index == kititemreceiptdetails[j].rootindex) {
				let sectempQty = Number((kititemreceiptdetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
				tempObj[`kititemreceiptdetails[${j}].quantity`] = sectempQty;
				calculateQuantity(kititemreceiptdetails[j].index, sectempQty, index);
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);
}


export function conversionfactoronchange (value, itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.partnerid, partneraddressid: this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate((returnObject.lastpurchasecost || 0), this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate((returnObject.lastpurchasecost || 0), this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
	}
}

export function conversionOnChange (item) {
	for (var i = 0; i < this.props.resource.receiptnoteitems.length; i++) {
		if (item.rootindex == this.props.resource.receiptnoteitems[i].index) {
			this.controller.quantityOnChange(this.props.resource.receiptnoteitems[i].quantity, this.props.resource.receiptnoteitems[i], `receiptnoteitems[${i}]`);
			break;
		}
	}
}

export function rateOnChange (itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	let finalrate = item.rate;
	let tempObj = {};
	let kititemreceiptdetails = this.props.resource.kititemreceiptdetails;

	if (item.discountquantity != null && item.discountquantity != "" && item.discountquantity != "undefined") {
		if(item.discountmode=='Percentage')
			finalrate = finalrate - ((finalrate * item.discountquantity) / 100);
		else
			finalrate = finalrate - item.discountquantity;
	}
	for (var i = 0; i < kititemreceiptdetails.length; i++) {
		if (item.index == kititemreceiptdetails[i].rootindex) {
			if (!kititemreceiptdetails[i].parentindex) {
				let tempRate = Number(((finalrate * ((kititemreceiptdetails[i].costratio) / 100)) / kititemreceiptdetails[i].conversion).toFixed(roundOffPrecisionLocal));
				let tempIndex = kititemreceiptdetails[i].index;
				tempObj[`kititemreceiptdetails[${i}].rate`] = tempRate;
				
				calculaterate(tempIndex, tempRate, item.index);
			}
		}
	}

	function calculaterate(tempIndex, tempRate, index) {
		for (var j = 0; j < kititemreceiptdetails.length; j++) {
			if (tempIndex == kititemreceiptdetails[j].parentindex && index == kititemreceiptdetails[j].rootindex) {
				let subItemtemprate = Number(((tempRate * ((kititemreceiptdetails[j].costratio) / 100)) / kititemreceiptdetails[j].conversion).toFixed(roundOffPrecisionLocal));
				tempObj[`kititemreceiptdetails[${j}].rate`] = subItemtemprate;
				calculaterate(kititemreceiptdetails[j].index, subItemtemprate, index);
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.rateOnChange(itemstr);
}

export function warehouseonchange(id, item, itemstr) {
	let tempObj = {
		[`${itemstr}.pickingdetails`]: null
	};
	this.props.resource.kititemreceiptdetails.forEach((kititem, kitindex) => {
		if(kititem.rootindex == item.index) {
			tempObj[`kititemreceiptdetails[${kitindex}].warehouseid`] = id;
			tempObj[`kititemreceiptdetails[${kitindex}].pickingdetails`] = null;
		}
	});
	this.props.updateFormState(this.props.form, tempObj);
}

export function deleteReceiptItem (index) {
	if(this.props.resource.ispurchaseorder == 'Purchase Orders' || this.props.resource.ispurchaseorder == 'Others' || this.props.resource.ispurchaseorder == 'Item Requests for Project' || this.props.resource.ispurchaseorder == 'Item Requests for Service Call' || this.props.resource.ispurchaseorder == 'Item Requests for Engineer' || this.props.resource.ispurchaseorder == 'Item Requests for Production Order' || this.props.resource.ispurchaseorder == 'Production Order' || this.props.resource.ispurchaseorder == 'Job Work Return' || this.props.resource.ispurchaseorder == 'Item Requests for Job Work') {
		let itemCount = 0;
		let kititemreceiptdetails = [...this.props.resource.kititemreceiptdetails];
		let receiptnoteitems = [...this.props.resource.receiptnoteitems];

		for (var i = 0; i < kititemreceiptdetails.length; i++) {
			if (kititemreceiptdetails[i].rootindex == this.props.resource.receiptnoteitems[index].index)
				itemCount++;
		}
		for (var i = 0; i < itemCount; i++) {
			for (var j = 0; j < kititemreceiptdetails.length; j++) {
				if (kititemreceiptdetails[j].rootindex == this.props.resource.receiptnoteitems[index].index) {
					kititemreceiptdetails.splice(j, 1);
					break;
				}
			}
		}
		receiptnoteitems.splice(index, 1);

		this.props.updateFormState(this.props.form, {
			kititemreceiptdetails,
			receiptnoteitems
		});

	} else {
		let apiResponse = commonMethods.apiResult({
			data:{
				message: 'No Access to delete Sales Return Items or Stock Transfer Items'
			}
		});
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
}

export function deleteKitItem(index) {
	if(this.props.resource.ispurchaseorder == 'Others') {
		let errorArray = [];
		let itemfound = false;
		let kititemreceiptdetails = [...this.props.resource.kititemreceiptdetails];

		for (var i = 0; i < kititemreceiptdetails.length; i++) {
			if (index != i && kititemreceiptdetails[index].rootindex == kititemreceiptdetails[i].rootindex && kititemreceiptdetails[index].parentindex == kititemreceiptdetails[i].parentindex) {
				itemfound = true;
			}
		}
		if (!itemfound)
			errorArray.push("Item is a only child for parent. Delete the Parent item");

		let itemCouFound = false;
		for (var i = 0; i < kititemreceiptdetails.length; i++) {
			if (index != i && kititemreceiptdetails[index].rootindex == kititemreceiptdetails[i].rootindex && !kititemreceiptdetails[i].parentindex)
				itemCouFound = true;
		}
		if (!itemCouFound)
			errorArray.push("Item is a only child for parent. Delete the Parent item");

		if (errorArray.length == 0) {
			let parentIndexArray = [kititemreceiptdetails[index].index];
			let rootindex = kititemreceiptdetails[index].rootindex;
			deleteArray(kititemreceiptdetails[index].rootindex, kititemreceiptdetails[index].index);

			function deleteArray(rootindex, index) {
				for (var i = 0; i < kititemreceiptdetails.length; i++) {
					if (kititemreceiptdetails[i].rootindex == rootindex && kititemreceiptdetails[i].parentindex == index) {
						parentIndexArray.push(kititemreceiptdetails[i].index)
						deleteArray(rootindex, kititemreceiptdetails[i].index);
					}
				}
			}

			for (var i = 0; i < parentIndexArray.length; i++) {
				for (var j = 0; j < kititemreceiptdetails.length; j++) {
					if (kititemreceiptdetails[j].rootindex == rootindex && kititemreceiptdetails[j].index == parentIndexArray[i]) {
						kititemreceiptdetails.splice(j, 1);
						break;
					}
				}
			}

			this.props.updateFormState(this.props.form, {kititemreceiptdetails});
		} else {
			let response = {
				data : {
					message : 'failure',
					error : utils.removeDuplicate(errorArray)
				}
			};
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	} else {
		let response = {
			data:{
				message: "You can't delete kit items in this transaction"
			}
		};
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
}

export function openPickDetails (itemstr) {
	let getBody = (closeModal) => {
		return (
			<PickingdetailsModal />
		)
	}
	this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} itemstr={itemstr} app={this.props.app} updateFormState={this.props.updateFormState} createOrEdit={this.props.createOrEdit} openModal={this.openModal} closeModal={closeModal} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function analyseitemRate (item) {
	let errorArray = [];
	if (!(item.itemid > 0))
		errorArray.push('Item Name is required!!!');

	if (!(this.props.resource.partnerid > 0))
		errorArray.push('Supplier Name is required!!!');

	if (!(item.warehouseid > 0))
		errorArray.push('Warehouse Name is required!!!');

	if (errorArray.length > 0)
		this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : errorArray,
			btnArray : ["Ok"]
		}));
	else {
		this.props.openModal({render: (closeModal) => {return <AnalysePurchaseRateModal item={item} param="purchaseorders" partnerid={this.props.resource.partnerid} partnerid_name={this.props.resource.partnerid_name} closeModal={closeModal} openModal={this.props.openModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
	}
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Supplier',
			data : emailObj
		},
		url : '/api/receiptnotes'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function save (param, confirm,confirm2) {
	if (param == 'Send To Supplier') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'receiptnotes'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});

	} else {
		this.updateLoaderFlag(true);

		if(param !='Cancel' && param !='Delete' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm2 && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
			let message = {
				header : 'Cost Center Alert',
				body : `Cost Center not selected. Do you want to ${param} ?`,
				btnArray : ['Yes','No']
			};

			return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
				if(resparam)
					this.controller.save(param, confirm,true);
				else
					this.updateLoaderFlag(false);
			}));	
		}

		let errorArray = [];
		let tempObj = this.props.resource;
		
		
		['receiptnoteitems', 'kititemreceiptdetails'].forEach((prop) => {
			tempObj[prop].forEach((item) => {
				if(item.pickingdetails && item.pickingdetails.details) {
					item.pickingdetails.details.forEach((rowitem) => {
						if(rowitem.serialArray) {
							if(rowitem.serialArray.indexOf('\n') >= 0 && rowitem.serialArray.indexOf(',') >= 0)
								errorArray.push(`Serial No  For Item ${item.itemid_name}, Please use serialno seperator either comma or new line. Dont use both`);
							else {
								if(rowitem.serialArray.indexOf('\n') >= 0)
									rowitem.serialnoArray = rowitem.serialArray.split('\n');
								else
									rowitem.serialnoArray = rowitem.serialArray.split(',');
							}
						} else {
							if(item.itemid_hasserial)
								rowitem.serialnoArray = [];
						}
					});
				} else {
					item.pickingdetails = {
						details: []
					};
				}
			});
		});

		if(errorArray.length > 0) {
			this.updateLoaderFlag(false);
			return this.openModal(modalService.infoMethod({
				header : 'Error',
				body : errorArray,
				btnArray : ['Ok']
			}));
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : tempObj,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/receiptnotes'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true,confirm2);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					this.props.history.replace(`/details/receiptnotes/${response.data.main.id}`);
				} else {
					if (param == 'Delete')
						this.props.history.replace("/list/receiptnotes");
					else {
						let iRItemFound = false;
						this.props.initialize(response.data.main);

						this.props.resource.receiptnoteitems.forEach((item) => {
							if(item.purchaseorderitemsid_itemrequestitemsid)
								return iRItemFound=true;
						});

						if (iRItemFound && param == 'Approve' && response.data.main.ispurchaseorder=="Purchase Orders" && response.data.main.projectid > 0) {
							let message = {
								header : 'Create Delivery Note?',
								body : 'Receipt Note status is changed. Would you like to create the delivery note now?',
								btnArray : ['Yes','No']
							};
							this.props.openModal(modalService['confirmMethod'](message, (param) => {
								if(param) {
									this.controller.checkForDeliverynote();
								}
							}));
						}
					}
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function checkForDeliverynote() {
	this.updateLoaderFlag(true);
	let itemsidArray = [], itemrequestitemsidArr = [];
	this.props.resource.receiptnoteitems.forEach((item) => {
		if(item.purchaseorderitemsid)
			itemsidArray.push(item.purchaseorderitemsid);
	});

	axios.get(`/api/query/projectdeliveryplannerquery?projectid=${this.props.resource.projectid}&poitemid=${itemsidArray}`).then((response)=> {
		if(response.data.message == 'success') {
			let tempObj = {
				itemrequestitems : []
			};
			tempObj.itemrequestitems = response.data.main;

			let poitemidObj = {};
			this.props.resource.receiptnoteitems.forEach((receiptitem) => {
				poitemidObj[receiptitem.purchaseorderitemsid] = receiptitem.quantity;
			});

			tempObj.itemrequestitems.forEach((itemreqitem) => {
				let pickedqty = 0;
				if(itemreqitem.poitemsidarray && itemreqitem.poitemsidarray.length > 0) {
					itemreqitem.poitemsidarray.forEach((poitemid) => {
						pickedqty += (poitemidObj[poitemid] ? poitemidObj[poitemid] : 0);
					});
					itemreqitem._pickedQty = Number(pickedqty.toFixed(this.props.app.roundOffPrecisionStock));
				}
			});

			if(tempObj.itemrequestitems.length > 0)
				return this.props.history.push({pathname: '/createDeliveryNote', params: {...tempObj, param: 'Project Delivery Planner'}});
			else {
				let apiResponse = commonMethods.apiResult({
					data: {
						message: 'Items are Already Delivered or Item request might be pre closed'
					}
				});
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function createInvoice () {
	let promise = checkStatus.purchaseinvoice(this.props.resource);
	promise.then((returnObject) => {
		let temparray = [];
		for (var i = 0; i < returnObject.length; i++) {
			if (returnObject[i].status == 'Hold' || returnObject[i].status == 'Closed') {
				temparray.push(`Purchase Order ${returnObject[i].ponumber} for item ${returnObject[i].itemid_name} is not Sent To Supplier`);
			}
		}
		if(temparray.length > 0) {
			let message = {
				header : 'Warning',
				body : temparray,
				btnArray : ['Ok']
			};
			this.props.openModal(modalService.infoMethod(message));
		} else {
			this.updateLoaderFlag(true);
			checkTransactionExist('purchaseinvoices', 'receiptnotes', this.props.resource.id, this.openModal, (param) => {
				if(param) {
					this.props.history.push({pathname: '/createPurchaseInvoice', params: {...this.props.resource, param: 'Receipt Notes'}});
				}
				this.updateLoaderFlag(false);
			});
		}
	}, (reason)=> {});
}

export function createLandingCost() {
	this.props.history.push({pathname: '/createLandingCost', params: {...this.props.resource, param: 'Receipt Notes'}});
}

export function cancel () {
	this.props.history.goBack();
}
