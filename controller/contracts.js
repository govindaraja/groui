import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { itemmasterDisplaynamefilter,uomFilter } from '../utils/filter';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import ServicecallequipmentaddModal from '../components/details/servicecallequipmentaddmodal';
import EquipmentSchedulesTable from '../components/details/equipmentschedulestable';
import ExpireContractItems from '../components/details/expirecontractitems';
import EmailModal from '../components/details/emailmodal';
import { ItemRateField, ChildEditModal } from '../components/utilcomponents';
import UpdateBillingSchedule from '../components/details/updatecontractbillingschedules';
import SuspendBillingSchedule from '../components/details/suspendcontractbillingschedules';

export function onLoad () {
	this.controller.contracttypesOnLoad();
}

export function contracttypesOnLoad () {

	this.updateLoaderFlag(true);

	axios.get(`/api/contracttypes?field=id,name,description,type,schedule,rate,duration,hsncodeid,hsncodes/salestaxid/hsncodeid,hsncodes/unionsalestaxid/hsncodeid,hsncodes/intersalestaxid/hsncodeid,schedulemethod`).then((response) => {
		if(response.data.message == 'success') {
			let contractArray = {
				'AMC' : [],
				'Warranty' : []
			};
			let contractObj = {};

			response.data.main.forEach((item) => {
				if(item.type == 'AMC')
					contractArray['AMC'].push(item);
				else
					contractArray['Warranty'].push(item);

				contractObj[item.id] = item;
			});

			this.setState({ contractArray, contractObj }, () => {
				if(this.state.createParam)
					this.controller.initializeState();
				else
					this.controller.getItemById();
			});

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});

}

export function initializeState () {
	let tempObj = {
		currencyid : this.props.app.defaultCurrency,
		contractdate : new Date(new Date().setHours(0, 0, 0, 0)),
		type : "New Enquiry",
		salesperson : this.props.app.user.issalesperson ? this.props.app.user.id : null,
		companyid : this.props.app.selectedcompanyid,
		contractitems : [],
		additionalcharges : [],
		contractbillingschedules : [],
		contractcovereditems: [],
		contracttype : 'AMC',
		billingschedule : 'Not Applicable',
		contractfor: 'Equipment',
		schedules : {
			array : []
		}
	};

	if(this.props.location.params) {
		const params = this.props.location.params;
		let contractitemsArray = params.contractenquiryitems;
		let contractcovereditemsArray = params.contractenquirycovereditems;

		if(params.param == 'contractenquiries') {
			for (let i = 0; i < contractitemsArray.length; i++) {
				if (contractitemsArray[i].equipmentid > 0 && !contractitemsArray[i].islost) {
					let tempChildObj = {
						index: i+1
					};
					utils.assign(tempChildObj, contractitemsArray[i], ['equipmentid', {'contracttypeid' : 'newcontracttypeid'}, 'equipmentid_displayname', 'description', 'equipmentid_serialno', 'equipmentid_invoiceno', 'rate', 'discountquantity', 'discountmode', 'equipmentid_invoicedate', 'taxid', 'capacity', 'capacityfield', 'quantity']);

					this.customFieldsOperation('contractenquiryitems', tempChildObj, contractitemsArray[i], 'contractitems');
					tempObj.contractitems.push(tempChildObj);
				}
			}

			for (let i = 0; i < contractcovereditemsArray.length; i++) {
				let tempChildObj = {
					index: i+1
				};
				utils.assign(tempChildObj, contractcovereditemsArray[i], ['itemid', 'itemid_name', 'itemid_itemtype', 'itemid_useforoandm', 'description', 'quantity', 'uomid', 'uomid_name', 'rate', 'capacity', 'capacityfield','qtyinnos', 'facilityid']);

				this.customFieldsOperation('contractenquirycovereditems', tempChildObj, contractitemsArray[i], 'contractcovereditems');
				tempObj.contractcovereditems.push(tempChildObj);
			}

			utils.assign(tempObj, params, ['customerid', 'contactid', 'email', 'mobile', 'phone', 'installationcontactid', 'installationemail', 'installationmobile', 'installationphone', 'companyid', 'billingaddress', 'billingaddressid', 'installationaddress', 'installationaddressid', 'currencyid', 'currencyexchangerate', 'salesperson', 'contracttype', {'contracttypeid' : 'newcontracttypeid'}, {'contractenquiryid' : 'id'}, 'contractid', 'startdate', 'duration', 'expiredate', 'tandc', 'taxid', 'ponumber', 'podate', 'billingschedule', 'roundoffmethod', 'territoryid', 'contractfor']);

			tempObj.contractbillingschedules = [];

			params.contractbillingschedules.map((item) => {
				tempObj.contractbillingschedules.push({
					scheduleno : item.scheduleno,
					scheduledate : item.scheduledate,
					scheduleenddate : item.scheduleenddate,
					displayname : item.displayname
				});
			});

			tempObj['invoicenotapplicable'] = params.contracttype == 'Warranty' ? true : false;

			this.customFieldsOperation('contractenquiries', tempObj, params, 'contracts');

			this.props.initialize(tempObj);
			setTimeout(() => {
				this.controller.currencyOnChange();
				this.controller.generateSchedule();
			}, 0);
			this.controller.getTandC();
			this.updateLoaderFlag(false);
		} else if (params.param == 'equipments') {
			tempObj.customerid = params.partnerid;
			tempObj.installationaddressid = params.equipmentaddressid;
			tempObj.installationaddress = params.equipmentaddress;
			tempObj.contractfor = params.contractfor;
			tempObj.contracttypeid = [];
			
			let itemObj = {
				equipmentid : params.id,
				equipmentid_displayname : params.displayname,
				description : params.description,
				equipmentid_serialno : params.serialno,
				equipmentid_invoiceno : params.invoiceno,
				rate : 0,
				equipmentid_invoicedate : params.invoicedate,
				quantity : 1
			};

			if(this.props.app.feature.useCapacityInContractPricing && params.itemid_capacityfield){
				itemObj.capacityfield = params.itemid_capacityfield ;
				itemObj.capacity = params[params.itemid_capacityfield] ;
				itemObj.quantity = params[params.itemid_capacityfield] > 0 ? params[params.itemid_capacityfield] : 1 ;
			}
			this.customFieldsOperation('equipments', itemObj, params, 'contractitems');

			tempObj.contractitems.push(itemObj);

			axios.get(`/api/partners?field=id,name,gstin,gstregtype,salesperson,territory,contactid,contacts/name/contactid,contacts/phone/contactid,contacts/email/contactid,contacts/mobile/contactid&filtercondition=partners.id=${tempObj.customerid}`).then((response) => {
				if (response.data.message == 'success') {
					if(response.data.main.length > 0) {
						let contactObj = {
							salesperson : this.props.app.user.issalesperson ? this.props.app.user.id : response.data.main[0].salesperson,
							territoryid : response.data.main[0].territory,
							customerid_gstin : response.data.main[0].gstin,
							billingaddress : null,
							billingaddressid : null,
							installationaddressid : null,
							installationaddress : null,
							contactid: response.data.main[0].contactid,
							email: response.data.main[0].contactid_email,
							mobile: response.data.main[0].contactid_mobile,
							phone: response.data.main[0].contactid_phone,
							contactperson: response.data.main[0].contactid_name,
							contactid_name: response.data.main[0].contactid_name,
							contactid_email: response.data.main[0].contactid_email,
							contactid_mobile: response.data.main[0].contactid_mobile,
							contactid_phone: response.data.main[0].contactid_phone,
							installationcontactid: response.data.main[0].contactid,
							installationcontactperson: response.data.main[0].contactid_name,
							installationemail: response.data.main[0].contactid_email,
							installationmobile: response.data.main[0].contactid_mobile,
							installationphone: response.data.main[0].contactid_phone,
							installationcontactid_name: response.data.main[0].contactid_name,
							installationcontactid_email: response.data.main[0].contactid_email,
							installationcontactid_mobile: response.data.main[0].contactid_mobile,
							installationcontactid_phone: response.data.main[0].contactid_phone
						};

						tempObj = {...tempObj, ...contactObj};
					}

					this.props.initialize(tempObj);
					setTimeout(this.controller.currencyOnChange, 0);
					this.controller.getTandC();
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}

				this.updateLoaderFlag(false);
			});
		} else if(params.param == 'Service Calls') {
			utils.assign(tempObj, params, ['customerid', 'contactid', 'email', 'mobile', 'phone', 'installationcontactid', 'installationemail', 'installationmobile', 'installationphone', {'billingaddress' : 'address'}, {'billingaddressid' : 'addressid'}, 'installationaddress', 'installationaddressid', 'companyid', 'currencyid', 'currencyexchangerate', {'servicecallid' : 'id'}, 'territoryid', {'invoicenotapplicable' : true}]);

			tempObj.contracttypeid = [];

			axios.get(`/api/itemmaster?field=id,iswarrantyapplicable,warrantycontracttypeid,contracttypes/duration/warrantycontracttypeid&includeinactive=true&filtercondition=itemmaster.id in (${params.equipmentitems.map((item) => item.equipmentid_itemid).join()})`).then((response) => {
				if(response.data.message == 'success') {
					let itemmasterObj = {};
					let durationArray = [];

					response.data.main.forEach((item) => {
						itemmasterObj[item.id] = item;
					});

					params.equipmentitems.forEach((item) => {
						item.equipmentid_itemid_iswarrantyapplicable = itemmasterObj[item.equipmentid_itemid].iswarrantyapplicable;
						item.equipmentid_itemid_warrantycontracttypeid = itemmasterObj[item.equipmentid_itemid].warrantycontracttypeid;
						item.equipmentid_itemid_warrantycontracttypeid_duration = itemmasterObj[item.equipmentid_itemid].warrantycontracttypeid_duration;

						if(item.equipmentid_itemid_iswarrantyapplicable) {
							tempObj.contractitems.push({
								equipmentid : item.equipmentid,
								contracttypeid : item.equipmentid_itemid_warrantycontracttypeid,
								equipmentid_displayname : item.equipmentid_displayname,
								description : item.equipmentid_description,
								equipmentid_serialno : item.equipmentid_serialno,
								equipmentid_invoiceno : item.equipmentid_invoiceno,
								rate : 0,
								equipmentid_invoicedate : item.equipmentid_invoicedate
							});

							if(tempObj.contracttypeid.indexOf(item.equipmentid_itemid_warrantycontracttypeid) == -1) {
								tempObj.contracttypeid.push(item.equipmentid_itemid_warrantycontracttypeid);
								if(item.equipmentid_itemid_warrantycontracttypeid_duration && durationArray.indexOf(item.equipmentid_itemid_warrantycontracttypeid_duration) == -1)
									durationArray.push(item.equipmentid_itemid_warrantycontracttypeid_duration);
							}
						}
					});

					tempObj.contracttype = 'Warranty';
					tempObj.invoicenotapplicable = true;
					tempObj.startdate = new Date(new Date().setHours(0, 0, 0, 0));
					tempObj.duration = (durationArray.length == 1) ? durationArray[0] : 12;

					this.props.initialize(tempObj);
					setTimeout(() => {
						this.controller.currencyOnChange();
						this.controller.calculateExpiryDate();
					}, 0);
					this.controller.getTandC();
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}

				this.updateLoaderFlag(false);
			});
		}
	} else {
		this.props.initialize(tempObj);
		this.controller.getTandC();
		this.updateLoaderFlag(false);
	}
}

export function getItemById () {
	axios.get(`/api/contracts/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function renderTableQtyfield(item) {
	if(item.capacityfield && item.capacity > 0)
		return <span>{item.quantity} {`${itemmasterDisplaynamefilter(item.capacityfield, this.props.app)}`}</span>;
	else
		return <span>{item.quantity} Nos</span>;
}

export function renderCoveredItemTableQtyfield(item) {
	if(item.capacityfield && item.capacity > 0)
		return (
			<div>
				<span style={{color:'gray'}}>{item.qtyinnos ? item.qtyinnos : 1} Nos * </span>
				<span style={{color:'gray'}}>{item.capacity ? item.capacity : 1} {itemmasterDisplaynamefilter(item.capacityfield, this.props.app)}</span>
				<span> = {item.quantity} {`${item.capacityfield ? `${itemmasterDisplaynamefilter(item.capacityfield, this.props.app)}` : ''}`}</span>
			</div>
		);
	else
		return <span>{item.quantity} {`${item.uomid ? `${uomFilter(item.uomid, this.props.app.uomObj)}` : 'Nos'}`}</span>
}

export function contractforOnChange() {
	this.props.updateFormState(this.props.form, {
		contractitems : [],
		contractbillingschedules : [],
		contractcovereditems: []
	});
}

export function calculateExpiryDate () {
	if (this.props.resource.duration != null && this.props.resource.startdate != null) {
		let tempStartDate = new Date(this.props.resource.startdate);
		let tempexpiryDate = new Date(tempStartDate.setMonth(tempStartDate.getMonth() + this.props.resource.duration));

		this.props.updateFormState(this.props.form, {
			expiredate : new Date(tempexpiryDate.setDate(tempexpiryDate.getDate()-1))
		});

		this.controller.generateSchedule();

		if(this.props.resource.billingschedule !='Not Applicable')
			this.controller.callBackBillingSchedule();
	}
}

export function callbackCustomer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson : this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		territoryid : valueobj.territory,
		customerid_gstin : valueobj.gstin,
		billingaddress : null,
		billingaddressid : null,
		installationaddressid : null,
		installationaddress : null,
		contractitems : [],
		contactid: valueobj.contactid,
		email: valueobj.contactid_email,
		mobile: valueobj.contactid_mobile,
		phone: valueobj.contactid_phone,
		contactperson: valueobj.contactid_name,
		contactid_name: valueobj.contactid_name,
		contactid_email: valueobj.contactid_email,
		contactid_mobile: valueobj.contactid_mobile,
		contactid_phone: valueobj.contactid_phone,
		installationcontactid: valueobj.contactid,
		installationcontactperson: valueobj.contactid_name,
		installationemail: valueobj.contactid_email,
		installationmobile: valueobj.contactid_mobile,
		installationphone: valueobj.contactid_phone,
		installationcontactid_name: valueobj.contactid_name,
		installationcontactid_email: valueobj.contactid_email,
		installationcontactid_mobile: valueobj.contactid_mobile,
		installationcontactid_phone: valueobj.contactid_phone
	});
}

export function callbackType () {
	let tempObj = {};

	if(this.props.resource.contracttype == 'Warranty') {
		tempObj = {
			invoicenotapplicable : true,
			billingschedule : null,
			contractbillingschedules : [],
			contracttypeid : []
		};
	} else {
		tempObj = {
			contracttypeid : [],
			invoicenotapplicable : false
		};
	}

	this.props.updateFormState(this.props.form, tempObj);
}

export function getContractTypes(param) {
	let array = [];
	if(this.props.resource.contracttypeid && this.props.resource.contracttypeid.length > 0) {
		this.props.resource.contracttypeid.forEach((contype) => {
			array.push(this.state.contractObj[contype]);
		});
	}
	return array;
}

export function callBackContractType () {

	let contractitems = [...this.props.resource.contractitems];

	if (this.props.resource.contracttypeid && this.props.resource.contracttypeid.length > 0) {

		let tempObj = {};

		if(!this.props.resource.duration)
			tempObj['duration'] = this.state.contractObj[this.props.resource.contracttypeid[0]].duration;

		contractitems.map((item, index) => {
			if(this.props.resource.contracttypeid.indexOf(item.contracttypeid) == -1) {
				item.contracttypeid = null;
				item.rate = null;
				item.discountquantity = null;
				item.amount = null;
				item.taxid = null;
			}

			item.contracttypeid = (item.contracttypeid == null) ? (this.props.resource.contracttypeid.length == 1 ? this.props.resource.contracttypeid[0] : item.contracttypeid) : item.contracttypeid;

			if (item.contracttypeid) {
				item.rate = (item.rate == null) ? (this.state.contractObj[item.contracttypeid].type == 'Warranty' ? 0 : this.state.contractObj[item.contracttypeid].rate) : item.rate;

				item.taxid = (item.taxid && item.taxid.length>0)? item.taxid :  this.controller.defaultHSNCodeTax(item);
			}
		});

		tempObj.contractitems = contractitems;

		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
	this.controller.generateSchedule();
}

export function childinit (item, itemstr) {
	if((this.props.resource.newcontracttypeid && this.props.resource.newcontracttypeid.length == 1) && this.props.resource.newcontracttypeid.indexOf(item.newcontracttypeid) == -1){
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.contracttypeid`] : item.contracttypeid == null ? this.props.resource.contracttypeid[0] : item.contracttypeid,
			[`${itemstr}.rate`] : (item.rate == null) ? this.state.contractObj[this.props.resource.contracttypeid[0]].rate : item.rate,
			[`${itemstr}.taxid`] : (item.taxid==null || item.taxid.length==0) ? this.controller.defaultHSNCodeTax(item) : item.taxid
		});

		this.controller.computeFinalRate();
	}
	
}

export function childContractChange (itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);

	let tempObj = {};

	if(this.props.resource.contracttype == 'Warranty')
		tempObj[`${itemstr}.rate`] = 0;
	else {
		if(item.contracttypeid) {
			tempObj[`${itemstr}.rate`] = this.state.contractObj[item.contracttypeid].rate;
			tempObj[`${itemstr}.taxid`] = this.controller.defaultHSNCodeTax(item);
		}
	}

	this.props.updateFormState(this.props.form, tempObj);

	this.controller.computeFinalRate();
}

export function defaultHSNCodeTax (item) {
	if(item.contracttypeid) {
		if(this.state.contractObj[item.contracttypeid].hsncodeid > 0) {
			let companygstin = this.props.app.selectedCompanyDetails.gstin ? this.props.app.selectedCompanyDetails.gstin.substr(0,2) : '';

			let partnergstin = (this.props.resource.installationaddressid_gstin || this.props.resource.customerid_gstin) ? (this.props.resource.installationaddressid_gstin || this.props.resource.customerid_gstin).substr(0,2) : '';

			if(companygstin != '') {
				let companyUGSTApplicable = false;

				this.props.app.utildata.india_states.map(function(sta) {
					if(sta.code == Number(companygstin))
						companyUGSTApplicable = sta.UTGSTApplicable ? true : false;
				});

				let taxid;

				if(partnergstin == '' || companygstin == partnergstin)
					 taxid = companyUGSTApplicable ? this.state.contractObj[item.contracttypeid]['hsncodeid_unionsalestaxid'] : (this.state.contractObj[item.contracttypeid]['hsncodeid_salestaxid'] || null);
				else
					taxid = this.state.contractObj[item.contracttypeid]['hsncodeid_intersalestaxid'] > 0 ? this.state.contractObj[item.contracttypeid]['hsncodeid_intersalestaxid']: null;

				return taxid > 0 ? [taxid] : [];
			} else
				return [];
		} else
			return [];
	}

	this.updateLoaderFlag(false);
}

export function generateSchedule () {
	let tempObj = {
		array : {}
	};

	if(this.props.resource.startdate && this.props.resource.duration && this.props.resource.contracttypeid && this.props.resource.contracttypeid.length>0) {

		this.props.resource.contracttypeid.forEach((item) => {
			tempObj.array[item] = [];

			if(this.state.contractObj[item].schedule != 'Not Applicable') {
				let times;

				switch(this.state.contractObj[item].schedule) {
					case 'Annual':
						times = 12;
						break;
					case 'Semi Annual':
						times = 6;
						break;
					case 'Tri Annual':
						times = 4;
						break;
					case 'Quarterly':
						times = 3;
						break;
					case 'Bi Monthly':
						times = 2;
						break;
					case 'Monthly':
						times = 1;
						break;
				}

				if (this.props.resource.duration >= times) {
					let tempStartDate = new Date(this.props.resource.startdate);
					for (let i = this.props.resource.duration ; i >= times; i = i - times) {
						let tempEndDate = new Date(new Date(tempStartDate).setMonth(new Date(tempStartDate).getMonth() + times));
						if(i-times >= times)
							tempEndDate = new Date(new Date(tempEndDate).setDate(new Date(tempEndDate).getDate()-1));
						else
							tempEndDate = new Date(this.props.resource.expiredate);

						tempObj.array[item].push({
							scheduledate : new Date((this.state.contractObj[item].schedulemethod == 'Period Start') ? tempStartDate : tempEndDate)
						});

						tempStartDate = new Date(new Date(tempStartDate).setMonth(new Date(tempStartDate).getMonth() + times));
					}
				}
			}
		});

		this.props.updateFormState(this.props.form, {
			schedules : tempObj
		});
	}
};

export function callBackBillingSchedule () {
	let contractbillingschedules = [];

	if(this.props.resource.startdate && this.props.resource.duration && this.props.resource.billingschedule !='Not Applicable') {
		let times;

		switch(this.props.resource.billingschedule) {
			case 'Annual':
				times = 12;
				break;
			case 'Semi Annual':
				times = 6;
				break;
			case 'Tri Annual':
				times = 4;
				break;
			case 'Quarterly':
				times = 3;
				break;
			case 'Bi Monthly':
				times = 2;
				break;
			case 'Monthly':
				times = 1;
				break;
		}

		if (this.props.resource.duration >= times) {
			let tempStartDate = new Date(this.props.resource.startdate);
			for (let i = this.props.resource.duration ; i >= times; i = i - times) {
				let tempEndDate = new Date(new Date(tempStartDate).setMonth(new Date(tempStartDate).getMonth() + times));
				if(i-times >= times)
					tempEndDate = new Date(new Date(tempEndDate).setDate(new Date(tempEndDate).getDate()-1));
				else
					tempEndDate = new Date(this.props.resource.expiredate);

				contractbillingschedules.push({
					scheduledate : new Date(tempStartDate),
					scheduleenddate : new Date(tempEndDate)
				});

				tempStartDate = new Date(new Date(tempStartDate).setMonth(new Date(tempStartDate).getMonth() + times));
			}
		}
	}

	this.props.updateFormState(this.props.form, { contractbillingschedules });
}

export function callbackProductCoveredItem(id, valueobj, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid:this.props.resource.customerid, partneraddressid:this.props.resource.billingaddressid} , null, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.itemid_useforoandm`] = returnObject.itemid_useforoandm;
		tempObj[`${itemstr}.capacityfield`] = returnObject.itemid_capacityfield;
		tempObj[`${itemstr}.capacity`] = returnObject[returnObject.itemid_capacityfield] ? returnObject[returnObject.itemid_capacityfield] : null;
		//tempObj[`${itemstr}.quantity`] = Number(((returnObject[returnObject.itemid_capacityfield] ? returnObject[returnObject.itemid_capacityfield] : 1)).toFixed(this.props.app.roundOffPrecision));
		
		tempObj[`${itemstr}.qtyinnos`] = returnObject.itemid_capacityfield ? (item.qtyinnos ? item.qtyinnos : 1) : null;
		tempObj[`${itemstr}.quantity`] = returnObject.itemid_capacityfield ? Number((tempObj[`${itemstr}.qtyinnos`] * tempObj[`${itemstr}.capacity`]).toFixed(this.props.app.roundOffPrecision)) : (item.quantity ? item.quantity : 1);

		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		tempObj[`${itemstr}.uomid`] = returnObject.stockuomid;
		//tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;

		this.props.updateFormState(this.props.form, tempObj);
		this.controller.calculateCoveredItemAmount();
	}, (reason) => {});
}

export function calculateCoveredItemAmount() {
	setTimeout(() => {
		let tempObj = {};
		this.props.resource.contractcovereditems.forEach((item, index) => {
			tempObj[`contractcovereditems[${index}].quantity`] = item.capacityfield ? Number(((item.capacity ? item.capacity : 1) * (item.qtyinnos ? item.qtyinnos :  0)).toFixed(this.props.app.roundOffPrecision)) : (item.quantity ? item.quantity : 0);
			tempObj[`contractcovereditems[${index}].amount`] = Number(((tempObj[`contractcovereditems[${index}].quantity`]) * (item.rate ? item.rate : 0)).toFixed(this.props.app.roundOffPrecision));
		});
		this.props.updateFormState(this.props.form, tempObj);
	}, 0);
}

export function computeFinalRate() {
	setTimeout(() => {
		taxEngine(this.props, 'resource', 'contractitems', true);
	}, 0);
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Contract' and termsandconditions.isdefault`).then((response) => {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'installationaddress') {
		tempObj.installationaddress = addressobj.displayaddress;
		tempObj.installationaddressid = addressobj.id;
		tempObj.installationaddressid_gstin = addressobj.gstin;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
			this.controller.calculateCoveredItemAmount();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
		this.controller.calculateCoveredItemAmount();
	}
}

export function tandcOnchange(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		tandc : valueobj.terms
	});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactid : id,
		contactperson : valueobj.name,
		phone : valueobj.phone,
		email : valueobj.email,
		mobile : valueobj.mobile,
		contactid_name : valueobj.name,
		contactid_phone : valueobj.phone,
		contactid_email : valueobj.email,
		contactid_mobile : valueobj.mobile
	});
}

export function installationcontactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		installationcontactid : id,
		installationcontactperson : valueobj.name,
		installationphone : valueobj.phone,
		installationemail : valueobj.email,
		installationmobile : valueobj.mobile,
		installationcontactid_name : valueobj.name,
		installationcontactid_phone : valueobj.phone,
		installationcontactid_email : valueobj.email,
		installationcontactid_mobile : valueobj.mobile
	});
}

export function createContractInvoice () {
	let invoiceIdArray = [];
	this.props.resource.contractbillingschedules.forEach((schedule)=>{
		if(schedule.invoiceid > 0)
			invoiceIdArray.push(schedule.invoiceid);
	});
	
	checkTransactionExist('contractinvoices', 'contracts', this.props.resource.id, this.openModal, (param) => {
		if(param){
			if(this.props.resource.billingschedule == 'Not Applicable' || (this.props.resource.billingschedule != 'Not Applicable' && invoiceIdArray.length == 0)){
				this.props.history.push({pathname: '/createContractInvoice', params: {...this.props.resource, param: 'Contracts'}});
			}else{
				axios.get(`/api/salesinvoiceitems?field=id,rate,amount,equipmentid&filtercondition=salesinvoiceitems.parentid=ANY(ARRAY[${invoiceIdArray.join()}])`).then((response) => {
					if(response.data.message == 'success') {
						
						let equipmentObj = {};
						response.data.main.forEach((invoice)=>{
							if(equipmentObj[invoice.equipmentid]){
								equipmentObj[invoice.equipmentid].rate += invoice.rate;
								equipmentObj[invoice.equipmentid].amount += invoice.amount; 
							}else{
								equipmentObj[invoice.equipmentid] = {
									rate : invoice.rate,
									amount : invoice.amount
								};
							} 
						});
						
						this.props.resource.contractitems.forEach((item)=>{
							if(equipmentObj[item.equipmentid]){
								item.invoicerate = equipmentObj[item.equipmentid].rate;
								item.invoiceamount = equipmentObj[item.equipmentid].amount;
							}
						});
						
						this.props.history.push({pathname: '/createContractInvoice', params: {...this.props.resource, param: 'Contracts'}});
					} else {
						let apiResponse = commonMethods.apiResult(response);
						this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
					}

					this.updateLoaderFlag(false);
				});
			}
		}
	});
}

export function createProformaInvoice () {
	let invoiceIdArray = [];
	this.props.resource.contractbillingschedules.forEach((schedule)=>{
		if(schedule.invoiceid > 0)
			invoiceIdArray.push(schedule.invoiceid);
	});
	
	checkTransactionExist('proformainvoices', 'contracts', this.props.resource.id, this.openModal, (param) => {
		if(param){
			if(this.props.resource.billingschedule == 'Not Applicable' || (this.props.resource.billingschedule != 'Not Applicable' && invoiceIdArray.length == 0)){
				this.props.history.push({pathname: '/createProformaInvoice', params: {...this.props.resource, param: 'Contracts'}});
			}else{
				axios.get(`/api/salesinvoiceitems?field=id,rate,amount,equipmentid&filtercondition=salesinvoiceitems.parentid=ANY(ARRAY[${invoiceIdArray.join()}])`).then((response) => {
					if(response.data.message == 'success') {
						
						let equipmentObj = {};
						response.data.main.forEach((invoice)=>{
							if(equipmentObj[invoice.equipmentid]){
								equipmentObj[invoice.equipmentid].rate += invoice.rate;
								equipmentObj[invoice.equipmentid].amount += invoice.amount; 
							}else{
								equipmentObj[invoice.equipmentid] = {
									rate : invoice.rate,
									amount : invoice.amount
								};
							} 
						});
						
						this.props.resource.contractitems.forEach((item)=>{
							if(equipmentObj[item.equipmentid]){
								item.invoicerate = equipmentObj[item.equipmentid].rate;
								item.invoiceamount = equipmentObj[item.equipmentid].amount;
							}
						});
						
						this.props.history.push({pathname: '/createProformaInvoice', params: {...this.props.resource, param: 'Contracts'}});
					} else {
						let apiResponse = commonMethods.apiResult(response);
						this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
					}

					this.updateLoaderFlag(false);
				});
			}
		}
	});
}

export function createReceipt () {
	this.props.history.push({pathname: '/createReceiptVoucher', params: {...this.props.resource, param: 'Contracts'}});
}

export function addEquipment () {
	this.openModal({
		render: (closeModal) => {
			return <ServicecallequipmentaddModal
					   resource={this.props.resource}
					   contractObj={this.state.contractObj}
					   computeFinalRate={this.controller.computeFinalRate}
					   app={this.props.app}
					   array={this.props.array}
					   closeModal={closeModal}
					   openModal={this.openModal}
					   updateFormState={this.props.updateFormState}
					   form={this.props.form}
					   resourcename= {'contracts'}
					   childresourcename = {'contractitems'}
					   defaultHSNCodeTax={this.controller.defaultHSNCodeTax}
					   customFieldsOperation = {this.customFieldsOperation} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Customer',
			data : emailObj
		},
		url : '/api/contracts'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function expireFn () {
	this.updateLoaderFlag(true);

	axios({
		method : 'post',
		data : {
			actionverb : 'Expire',
			data : this.props.resource
		},
		url : '/api/contracts'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

		if (response.data.message == 'success')
			this.props.initialize(response.data.main);

		this.updateLoaderFlag(false);
	});
}

export function openUpdateBillingScheduleModal (){
	let getBody = (closeModal) => {
			return (
				<UpdateBillingSchedule />
			)
		}
	this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} app={this.props.app} resource={this.props.resource} updateFormState={this.props.updateFormState} closeModal={closeModal} openModal={this.openModal} action = {'update'} callback={this.controller.updatebillingscheduleFn} updateLoaderFlag = {this.updateLoaderFlag} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
}

export function openSuspendBillingScheduleModal (){
	let getBody = (closeModal) => {
			return (
				<SuspendBillingSchedule />
			)
		}
	this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} app={this.props.app} resource={this.props.resource} updateFormState={this.props.updateFormState} closeModal={closeModal} openModal={this.openModal} action = {'suspend'} callback={this.controller.updatebillingscheduleFn} updateLoaderFlag = {this.updateLoaderFlag} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
}

export function updatebillingscheduleFn (action,billingschedules,remarks, callback) {
	this.updateLoaderFlag(true);

	var tempObj = JSON.parse(JSON.stringify(this.props.resource));
	tempObj.contractbillingschedules = billingschedules;
	tempObj.suspendremarks = remarks;
	
	axios({
		method : 'post',
		data : {
			actionverb : action == 'update' ? 'Update Billing Schedule' : 'Suspend',
			data :tempObj
		},
		url : `/api/${action == 'update' ? 'contracts' : 'common/methods/updateContractBillingSchedule'}`
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

		if (response.data.message == 'success') {
			if(action != 'update') {
				response.data.main.contractbillingschedules.forEach(item => {
					item.checked = false;
				});
			}
			this.props.initialize(response.data.main);
			callback(true);
		} else {
			callback(false);
		}

		this.updateLoaderFlag(false);
	});
}

export function openSchedules (item, itemstr) {
	this.openModal({
		render: (closeModal) => {
			return <EquipmentSchedulesTable
					   item={item}
					   closeModal={closeModal}
					   openModal={this.openModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function save (param, confirm) {
	if (param == 'Send To Customer') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'contracts'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
	} else if (param == 'Expire') {
		let getBody = (closeModal) => {
			return (
				<ExpireContractItems />
			)
		}
		this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} updateFormState={this.props.updateFormState} closeModal={closeModal} openModal={this.openModal} callback={this.controller.expireFn} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
	} else {
		this.updateLoaderFlag(true);

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/contracts'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam)
					this.props.history.replace(`/details/contracts/${response.data.main.id}`);
				else if (param == 'Delete')
					this.props.history.replace("/list/contracts");
				else
					this.props.initialize(response.data.main);
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function calculateCoveredItemsTotal() {
	let totalamount = 0;

	this.props.resource.contractcovereditems.forEach(item => {
		totalamount += item.amount ? item.amount : 0;
	});

	return Number(totalamount.toFixed(this.props.app.roundOffPrecision));
}

export function cancel () {
	this.props.history.goBack();
}
