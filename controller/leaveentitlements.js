import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		leaveentitlementitems: []
	};

	if(this.props.location.params && this.props.location.params.param == 'copy')
		tempObj = this.props.location.params;

	this.props.initialize(tempObj);

	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/leaveentitlements/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackLeaveType (id, valueobj, item, itemstr) {
	var itemFoundCount = 0;
	this.props.resource.leaveentitlementitems.forEach((a) => {
		if(a.leavetypeid == id) {
			itemFoundCount++;
		}
	});

	this.props.updateFormState(this.props.form, {
		[`${itemstr}.leavetypeid_name`]: valueobj.name,
		[`${itemstr}.cancarryforward`]: valueobj.cancarryforward,
		[`${itemstr}.maxcarryforward`]: valueobj.maxcarryforward,
		[`${itemstr}.allocationtype`]: valueobj.allocationtype,
	});

	if(itemFoundCount > 1) {
		this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Leave Type already Exists"
		}));
	}
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/leaveentitlements'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		});

		if (response.data.message == 'success') {
			if (this.props.isModal) {
				if (param != 'Delete')
					this.props.callback(response.data.main);

				this.props.closeModal();
			} else {
				if(this.state.createParam)
					this.props.history.replace(`/details/leaveentitlements/${response.data.main.id}`);
				else if (param == 'Delete')
					this.props.history.replace("/list/leaveentitlements");
				else
					this.props.initialize(response.data.main);
			}
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	if(this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}