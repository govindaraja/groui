import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, detailsSVC, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import { ItemRateField } from '../components/utilcomponents';
import DuplicateLeadCheckModal from '../components/details/duplicateleadcheck';
import LeadWonLostCheckModal from '../components/details/leadwonlostcheck';
import PotentialrevenueupdateModal from '../components/details/potentialrevenueupdatemodal';
import StockdetailsModal from '../containers/stockdetails';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		leaditems: []
	};
	this.setState({duplicateLeadFlag: false});

	if(this.props.location.params) {
		let params = this.props.location.params;
		if(params.param == 'Dialer') {
			tempObj.contactid = params.contactid;
			tempObj.customerid = params.customerid;
			tempObj.mobile = params.mobile;
			tempObj.phone = params.phone;
			tempObj.email = params.email;
			tempObj.tempcontactid = params.tempcontactid;

			if(!tempObj.customerid && !tempObj.contactid){
				tempObj.contact = {
					mobile : tempObj.mobile,
					phone : tempObj.mobile 
				}
			}
		} else if(params.param == 'salesactivities') {
			tempObj.contactid = params.contactid;
			tempObj.customerid = params.customerid;
			tempObj.tempsalesactivityid = params.salesactivityid;
		} else {
			utils.assign(tempObj, params, [{'tempcontactid' : 'id'}, {'contactid' : 'id'}, 'email', 'phone', 'mobile', {'subject' : 'organization'}, {'billingaddressid' : 'addressid'}, {'billingaddress' : 'address'}, {'source' : 'sourceid'}, 'territoryid', 'industryid']);

			this.customFieldsOperation('contacts', tempObj, params, 'leads');
			if(params.parentresource == 'partners')
				tempObj.customerid = params.parentid;
		}
	}
	this.props.initialize(tempObj);

	this.updateLoaderFlag(false);
}

export function currencyOnChange () {
	if (this.props.resource.pricelistid) {
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid) {
			this.props.updateFormState(this.props.form, {
				pricelistid: null
			});
		}
	}
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject)=> {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate: returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason)=> {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate: null
		});
		this.controller.computeFinalRate();
	}
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function deliverydateonchange () {
	let tempObj = {};
	for (var i = 0; i < this.props.resource.leaditems.length; i++) {
		if (!this.props.resource.leaditems[i].deliverydate || this.props.resource.leaditems[i].deliverydate == null) {
			tempObj[`leaditems[${i}].deliverydate`] = this.props.resource.deliverydate;
		}
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function getItemById () {
	axios.get(`/api/leads/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.tempfinaltotal = response.data.main.finaltotal;
			for (var i = 0; i < tempObj.leaditems.length; i++) {
				if (tempObj.leaditems[i].uomconversiontype)
					tempObj.leaditems[i].alternateuom = true;
			}
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function customerOnchange (id, valueobj) {
	let tempObj = {
		customerid_name: valueobj.name,
		territoryid: valueobj.territory,
		salesperson: this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		industryid: valueobj.industryid,
		deliveryaddress: valueobj.addressid_displayaddress,
		billingaddress: valueobj.addressid_displayaddress,
		billingaddressid: valueobj.addressid,
		deliveryaddressid: valueobj.addressid,
		contactid: valueobj.contactid,
		email: valueobj.contactid_email,
		mobile: valueobj.contactid_mobile,
		phone: valueobj.contactid_phone,
		contactperson: valueobj.contactid_name
	};
	this.customFieldsOperation('partners', tempObj, valueobj, 'leads');

	if (valueobj.salespricelistid) {
		tempObj.pricelistid = valueobj.salespricelistid;
	} else if (valueobj.partnergroupid_pricelistid) {
		tempObj.pricelistid = valueobj.partnergroupid_pricelistid;
	} else {
		for (var i = 0; i < this.props.app.appSettings.length; i++) {
			if (this.props.app.appSettings[i].module == 'Sales' && this.props.app.appSettings[i].name == 'Pricelistid') {
				tempObj.pricelistid = this.props.app.appSettings[i].value['value'];
				break;
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = valueobj.parentid;
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid:this.props.resource.customerid, partneraddressid:this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales');
	promise.then((returnObject)=> {
		let tempObj = {};
		tempObj[`${itemstr}.itemid`] = returnObject.itemid;
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.itemid_itemtype`] = returnObject.itemid_itemtype;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.rate`] = 0;
		tempObj[`${itemstr}.labourrate`] = 0;
		tempObj[`${itemstr}.materialrate`] = 0;
		tempObj[`${itemstr}.splitrate`] = returnObject.splitrate;
		if (returnObject.itemid_itemtype != 'Project') {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		} else {
			tempObj[`${itemstr}.labourtaxid`] = (this.props.resource.labourtaxid && this.props.resource.labourtaxid.length > 0) ? this.props.resource.labourtaxid : [];
			tempObj[`${itemstr}.materialtaxid`] = (this.props.resource.materialtaxid && this.props.resource.materialtaxid.length > 0) ? this.props.resource.materialtaxid : [];
		}
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'leaditems', itemstr);

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		this.props.updateFormState(this.props.form, tempObj);

		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
	}, (reason)=> {}).catch((err) => {
		console.error(err);
	});
}

export function quantityOnChange(itemqty, item, itemstr) {
	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);

		this.props.updateFormState(this.props.form, {
			[`${itemstr}.billingquantity`]: Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))
		});
	}
	this.controller.computeFinalRate();
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid:this.props.resource.customerid, partneraddressid:this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid:this.props.resource.customerid, partneraddressid:this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson: valueobj.name,
		phone: valueobj.phone,
		email: valueobj.email,
		mobile: valueobj.mobile
	});
}

export function computeFinalRate() {
	setTimeout(() => {
		let tempObj = {};
		for (var i = 0; i < this.props.resource.leaditems.length; i++) {
			if (this.props.resource.leaditems[i].itemid_itemtype == 'Project' && this.props.resource.leaditems[i].splitrate) {
				tempObj[`leaditems[${i}].rate`] = (this.props.resource.leaditems[i].labourrate ? this.props.resource.leaditems[i].labourrate : 0) + ((this.props.resource.leaditems[i].materialrate ? this.props.resource.leaditems[i].materialrate : 0));
			}
		}
		this.props.updateFormState(this.props.form, tempObj);
		taxEngine(this.props, 'resource', 'leaditems');
	}, 0);
}

export function createQuotation (param) {
	if (this.props.resource.customerid) {
		this.controller.save(param);
	} else {
		let response = {
			data : {
				message : 'Customer is required for creating a quotation / order'
			}
		};
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
}

export function save (param, checkparam, confirm) {
	let tempresourceObj = this.props.resource;

	if (['Lost', 'Won'].indexOf(param) >= 0 && tempresourceObj.status == 'Quotation' && !tempresourceObj.markaslost) {

		return this.openModal({render: (closeModal) => {return <LeadWonLostCheckModal resource = {this.props.resource} closeModal={closeModal} actionVerb={param} callback={()=> {
			this.props.updateFormState(this.props.form, {
				markaslost: true
			});
			setTimeout(() => this.controller.save(param), 0);
		}} />}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}});

	} else if(!tempresourceObj.id && this.state.duplicateLeadFlag==false && (tempresourceObj.contact && (tempresourceObj.contact.email || tempresourceObj.contact.mobile))) {
		return this.controller.checkDuplicateLead(param);
	}

	this.updateLoaderFlag(true);

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	if(!tempresourceObj.potentialrevenue && !checkparam) {
		tempresourceObj.potentialrevenue = tempresourceObj.finaltotal;
	} else if(tempresourceObj.finaltotal != tempresourceObj.tempfinaltotal && tempresourceObj.finaltotal > 0 && !checkparam) {
		this.updateLoaderFlag(false);
		let leadObj = {
			potentialrevenue: tempresourceObj.potentialrevenue,
			currencyid: tempresourceObj.currencyid,
			quoteflag: false,
			currencydiffer: false
		};

		return this.openModal({render: (closeModal) => {return <PotentialrevenueupdateModal resource = {this.props.resource} closeModal={closeModal} leadobj={leadObj} app={this.props.app} callback={(potenitalvalue)=>{
			this.props.updateFormState(this.props.form, {
				potentialrevenue: potenitalvalue
			});
			setTimeout(() => this.controller.save(param, true), 0);
		}} />}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}});
	}

	let tempData = {
		actionverb : (param == 'createQuote' || param == 'createOrder' || param == 'createProjectQuote') ? 'Save' : param,
		data : tempresourceObj,
		ignoreExceptions : confirm ? true : false
	};

	axios({
		method : 'post',
		data : tempData,
		url : '/api/leads'
	}).then((response)=> {
		if((param != 'createQuote' && param != 'createOrder' && param != 'createProjectQuote') || response.data.message != 'success') {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
				if (resparam)
					this.controller.save(param, null, true);
			});
		}
		if (response.data.message == 'success') {
			if (this.state.createParam)  {
				this.props.history.replace(`/details/leads/${response.data.main.id}`);
			} else {
				if (param != 'Delete') {
					let tempObj = response.data.main;
					tempObj.tempfinaltotal = response.data.main.finaltotal;
					this.props.initialize(tempObj);
					if (param == 'createQuote') {
						checkTransactionExist('quotes', 'leads', response.data.main.id, this.openModal, (param)=> {
							if(param) {
								this.props.history.push({pathname: '/createQuote', params: {...this.props.resource, param: 'Leads'}});
							}
						});
					}
					if (param == 'createOrder') {
						checkTransactionExist('orders', 'leads', response.data.main.id, this.openModal, (param)=> {
							if(param) {
								this.props.history.push({pathname: '/createOrder', params: {...this.props.resource, param: 'Leads'}});
							}
						});
					}
					if (param == 'createProjectQuote') {
						this.props.history.push({pathname: '/createProjectQuote', params: {...this.props.resource, param: 'Leads'}});
					}
				} else {
					this.props.history.replace('/list/leads');
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function checkDuplicate() {
	if(this.props.resource.contact && (this.props.resource.contact.mobile || this.props.resource.contact.email) && this.state.duplicateLeadFlag==false) {
		this.controller.checkDuplicateLead();
	}
}

export function openStockDetails (item) {
	this.openModal({
		render: (closeModal) => {
			return <StockdetailsModal
					resource = {this.props.resource}
					item = {item}
					closeModal = {closeModal} />
		},
		className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function checkDuplicateLead (param) {
	this.updateLoaderFlag(true);
	let filterArray = [];
	if (this.props.resource.contact.email)
		filterArray.push("email=" + this.props.resource.contact.email);
	if (this.props.resource.contact.mobile)
		filterArray.push("mobile=" + this.props.resource.contact.mobile);

	axios.get(`/api/query/duplicateleadsquery?${filterArray.join('&')}`).then((response) =>{
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				let duplicateArray = response.data.main;

				this.openModal({render: (closeModal) => {return <DuplicateLeadCheckModal array = {duplicateArray} closeModal={closeModal} callback={()=>{
					this.setState({duplicateLeadFlag: true}, () => {
						if(param) {
							this.controller.save(param);
						} else {
							this.updateLoaderFlag(false);
						}
					});
				}} />}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}, confirmModal : true});
			} else {
				if(param) {
					this.setState({duplicateLeadFlag: true}, () => {
						this.controller.save(param);
					});
				} else {
					this.updateLoaderFlag(false);
				}
			}
		} else {
			this.updateLoaderFlag(false);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function cancel () {
	this.props.history.goBack();
}

