import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import { ItemRateField } from '../components/utilcomponents';
import StockdetailsModal from '../containers/stockdetails';
import AnalyseRateModal from '../containers/analyseitemrate';
import EmailModal from '../components/details/emailmodal';
import { ItemrequestprofitanalyseModal } from '../components/details/itemrequestprofitabilityanalyse';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		estimationdate: new Date(new Date().setHours(0, 0, 0, 0)),
		engineerid: this.props.app.user.isengineer ? this.props.app.user.id : null,
		estimationitems: [],
		equipmentitems: [],
		componentitems: [],
		serviceitems: [],
		expenseitems: [],
		additionalcharges: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;
		tempObj.equipmentitems = params.equipmentitems;

		utils.assign(tempObj, params, [{'servicecallid' : 'id'}, 'companyid', 'customerid', 'customerid_name', 'contactid', 'contactperson', 'mobile', 'phone', 'email', 'address', 'addressid',  'pricelistid', 'salesperson', 'currencyid', 'currencyexchangerate', 'territoryid', {'paymentterms' : 'customerid_paymentterms'}]);

		this.customFieldsOperation('servicecalls', tempObj, params, 'estimations');
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.getTandC();
		if(!this.props.resource.currencyexchangerate)
			this.controller.currencyOnChange();
	}, 0);
	this.updateLoaderFlag(false);
}

export function renderTableRatefield(item) {
	return <ItemRateField item = {item} currencyid = {this.props.resource.currencyid} app = {this.props.app} />;
}

export function currencyOnChange () {
	if (this.props.resource.pricelistid) {
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid)
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});
	}

	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject)=> {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason)=> {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function callBackCustomerName (id, valueobj) {
	let tempObj = {
		paymentterms : valueobj.paymentterms,
		servicecallid: null,
		servicecallid_calltype: null,
		contactid: null,
		contactperson: null,
		phone: null,
		email: null,
		phone: null,
		contactid_name : null,
		contactid_phone : null,
		contactid_email : null,
		contactid_mobile : null,
		estimationitems: [],
		serviceitems: [],
		componentitems: [],
		expenseitems: [],
		servicecallid_calltype: null,
		salesperson : this.props.app.user.issalesperson ? this.props.app.user.id : valueobj.salesperson
	};

	if (valueobj.salespricelistid)
		tempObj.pricelistid = valueobj.salespricelistid;
	else if (valueobj.partnergroupid_pricelistid)
		tempObj.pricelistid = valueobj.partnergroupid_pricelistid;
	else {
		for (let i = 0; i < this.props.app.appSettings.length; i++) {
			if (this.props.app.appSettings[i].module == 'Sales' && this.props.app.appSettings[i].name == 'Pricelistid') {
				tempObj.pricelistid = this.props.app.appSettings[i].value['value'];
				break;
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
}

export function callBackServiceCall (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		servicecallid_calltype : valueobj.calltype,
		salesperson : valueobj.salesperson,
		pricelistid : valueobj.pricelistid,
		estimationitems: [],
		componentitems : [],
		serviceitems : [],
		expenseitems : []
	});

	this.updateLoaderFlag(true);
	this.controller.getEquipmentItems();
}

export function callbackEngineer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson : valueobj.id
	});
}

export function equipmentonChange(equipmentid, equipmentObj, item, itemstr) {
	if(equipmentid > 0) {
		let tempObj = {
			[`${itemstr}.contractid`]: equipmentObj.contractid,
			[`${itemstr}.contractid_contractno`]: equipmentObj.contractid_contractno,
			[`${itemstr}.contracttypeid`]: equipmentObj.contracttypeid,
			[`${itemstr}.contracttypeid_name`]: equipmentObj.contracttypeid_name,
		};
		this.customFieldsOperation('equipmentitems', tempObj, equipmentObj, 'equipmentitems', itemstr);

		this.props.updateFormState(this.props.form, tempObj);
	} else {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.contractid`] : null,
			[`${itemstr}.contractid_contractno`] : null,
			[`${itemstr}.contracttypeid`] : null,
			[`${itemstr}.contracttypeid_name`] : null
		});
	}

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.addressid}, this.props.resource.pricelistid, 'sales', item.uomid, item.alternateuom, item.uomconversionfactor, equipmentid > 0 ? equipmentObj.contractid : null, this.props.resource.servicecallid, equipmentid > 0 ? equipmentid : null);

	promise1.then((returnObject) => {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.freeorchargeable`] : returnObject.freeorchargeable,
			[`${itemstr}.rate`] : returnObject.freeorchargeable == 'Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app)
		});

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.addressid}, this.props.resource.pricelistid, 'sales', item.uomid, item.alternateuom, item.uomconversionfactor, item.contractid, this.props.resource.servicecallid, item.equipmentid);
	promise.then((returnObject)=> {
		let tempObj = {
			[`${itemstr}.itemid_name`] : returnObject.itemid_name,
			[`${itemstr}.description`] : returnObject.description,
			[`${itemstr}.itemid_issaleskit`] : returnObject.itemid_issaleskit,
			[`${itemstr}.itemid_itemtype`] : returnObject.itemid_itemtype,
			[`${itemstr}.rate`] : returnObject.freeorchargeable=='Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app),
			[`${itemstr}.quantity`] : item.quantity ? item.quantity : 1,
			[`${itemstr}.freeorchargeable`] : returnObject.freeorchargeable,
			[`${itemstr}.uomid`] : returnObject.uomid,
			[`${itemstr}.uomid_name`] : returnObject.uomid_name,
			[`${itemstr}.alternateuom`] : returnObject.alternateuom,
			[`${itemstr}.itemid_uomgroupid`] : returnObject.uomgroupid,
			[`${itemstr}.taxid`] : (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid,
			[`${itemstr}.itemnamevariationid`] : returnObject.itemnamevariationid,
			[`${itemstr}.itemnamevariationid_name`] : returnObject.itemnamevariationid_name,
			[`${itemstr}.itemid_keepstock`] : returnObject.keepstock,
			[`${itemstr}.itemid_imageurl`] : returnObject.itemid_imageurl,
			[`${itemstr}.itemid_itemgroupid`] : returnObject.itemgroupid,
			[`${itemstr}.accountid`] : returnObject.itemid_incomeaccountid,
			[`${itemstr}.itemid_quantitytype`] : returnObject.itemid_quantitytype
		};

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'estimationitems', itemstr);

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		this.props.updateFormState(this.props.form, tempObj);

		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);

	}, (reason)=> {});
}

export function quantityOnChange(itemqty, item, itemstr) {
	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);

		this.props.updateFormState(this.props.form, {
			[`${itemstr}.billingquantity`]: Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))
		});
	}

	this.controller.computeFinalRate();
}

export function computeFinalRate() {
	setTimeout(() => {
		let estimationitems = [];

		this.props.resource.componentitems.forEach((item) => {
			estimationitems.push(item);
		});

		this.props.resource.serviceitems.forEach((item) => {
			estimationitems.push(item);
		});

		this.props.resource.expenseitems.forEach((item) => {
			estimationitems.push(item);
		});

		this.props.updateFormState(this.props.form, {
			estimationitems
		});

		setTimeout(() => {
			taxEngine(this.props, 'resource', 'estimationitems');
		}, 0);
	}, 0);
}

export function freeorchargeableOnChange(value, itemstr) {
	if(value == 'Free') {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.rate`] : 0,
			[`${itemstr}.discountquantity`] : null,
			[`${itemstr}.taxid`] : null,
			[`${itemstr}.billingrate`] : 0,
		});

		this.controller.computeFinalRate();
	}
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson : valueobj.name,
		phone : valueobj.phone,
		email : valueobj.email,
		mobile : valueobj.mobile,
		contactid_name : valueobj.name,
		contactid_phone : valueobj.phone,
		contactid_email : valueobj.email,
		contactid_mobile : valueobj.mobile
	});
}

export function addressonChange(address, addressobj) {
	this.props.updateFormState(this.props.form, {
		address : addressobj.displayaddress,
		addressid : addressobj.id
	});
}

export function getItemById () {
	axios.get(`/api/estimations/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempEstimationObj = response.data.main;
			tempEstimationObj.componentitems = [];
			tempEstimationObj.serviceitems = [];
			tempEstimationObj.expenseitems = [];
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});

			tempEstimationObj.estimationitems.forEach((item) => {
				if(item.itemid_itemtype == 'Product')
					tempEstimationObj.componentitems.push(item);
				if(item.itemid_itemtype == 'Service')
					tempEstimationObj.serviceitems.push(item);
				if(item.itemid_itemtype == 'Expense')
					tempEstimationObj.expenseitems.push(item);
			});
			this.props.initialize(tempEstimationObj);

			setTimeout(this.controller.getEquipmentItems, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			this.updateLoaderFlag(false);
		}
	});
}

export function getEquipmentItems () {
	let equipmentCustomField = this.getCustomFields('equipmentitems', 'string', false, this.props.app);

	let queryString = `/api/equipmentitems?field=id,equipmentid,equipments/displayname/equipmentid,equipments/description/equipmentid,equipments/serialno/equipmentid,contractid,contracts/contractno/contractid,contracttypeid,contracttypes/contracttypes/contracttypeid,scheduleid${(equipmentCustomField ? ','+equipmentCustomField : '' )}&filtercondition=equipmentitems.parentresource = 'servicecalls' and equipmentitems.parentid=${this.props.resource.servicecallid}`;

	return axios.get(`${queryString}`).then((response)=> {
		if (response.data.message == 'success') {
			let equipmentArray = [];

			response.data.main.map((item) => {
				let tempObj = {
					equipmentid : item.equipmentid,
					equipmentid_displayname : item.equipmentid_displayname,
					equipmentid_serialno : item.equipmentid_serialno,
					equipmentid_description : item.equipmentid_description,
					contracttypeid : item.contracttypeid,
					contracttypeid_name : item.contracttypeid_name,
					contractid : item.contractid,
					contractid_contractno : item.contractid_contractno,
					scheduleid : item.scheduleid
				};

				this.customFieldsOperation('equipmentitems', tempObj, item, 'equipmentitems');

				equipmentArray.push(tempObj);
			});

			this.props.updateFormState(this.props.form, {equipmentitems: equipmentArray});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.addressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor, item.contractid, this.props.resource.servicecallid, item.equipmentid);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = returnObject.freeorchargeable=='Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = returnObject.freeorchargeable=='Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = returnObject.freeorchargeable=='Free' ? 0 : Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = returnObject.freeorchargeable=='Free' ? 0 : Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}


export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.addressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor, item.contractid, this.props.resource.servicecallid, item.equipmentid);

		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = returnObject.freeorchargeable=='Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = returnObject.freeorchargeable=='Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = returnObject.freeorchargeable=='Free' ? 0 : Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = returnObject.freeorchargeable=='Free' ? 0 : Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Customer',
			data : emailObj
		},
		url : '/api/estimations'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function save (param, confirm) {
	if (param == 'Send To Customer') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({
			render: (closeModal) => {
				return <EmailModal
					parentresource = {this.props.resource}
					app = {this.props.app}
					resourcename = {'estimations'}
					activityemail = {false}
					callback = {this.controller.emailFn}
					openModal = {this.openModal}
					closeModal = {closeModal} />
			},className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			}, confirmModal: true
		});
	} else {
		this.updateLoaderFlag(true);

		if(param != 'Cancel' && param != 'Delete' && param !='Amend' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		let tempEstObj = this.props.resource;
		tempEstObj.estimationitems = [];

		tempEstObj.componentitems.forEach((item) => {
			tempEstObj.estimationitems.push(item);
		});

		tempEstObj.serviceitems.forEach((item) => {
			tempEstObj.estimationitems.push(item);
		});

		tempEstObj.expenseitems.forEach((item) => {
			tempEstObj.estimationitems.push(item);
		});

		setTimeout(() => {
			axios({
				method : 'post',
				data : {
					actionverb : param,
					data : tempEstObj,
					ignoreExceptions: confirm ? true : false
				},
				url : '/api/estimations'
			}).then((response)=> {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam)=> {
					if (resparam)
						this.controller.save(param, true);
				}));

				if (response.data.message == 'success') {
					if(this.state.createParam)
						this.props.history.replace(`/details/estimations/${response.data.main.id}`);
					else if (param == 'Delete')
						this.props.history.replace("/list/estimations");
					else {
						let tempObj = response.data.main;
						tempObj.componentitems = [];
						tempObj.serviceitems = [];
						tempObj.expenseitems = [];
						tempObj.estimationitems.forEach((item) => {
							if(item.itemid_itemtype == 'Product')
								tempObj.componentitems.push(item);
							if(item.itemid_itemtype == 'Service')
								tempObj.serviceitems.push(item);
							if(item.itemid_itemtype == 'Expense')
								tempObj.expenseitems.push(item);
						});
						this.props.initialize(tempObj);
					}
				}
				this.updateLoaderFlag(false);
			});
		}, 0);
	}
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Estimation' and termsandconditions.isdefault`).then((response)=> {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function tandcOnchange(id, valueobj) {
	if(id) {
		this.props.updateFormState(this.props.form, {
			tandc: valueobj.terms
		});
	}
}

export function openStockDetails (item) {
	this.openModal({
		render: (closeModal) => {
			return <StockdetailsModal
				resource = {this.props.resource}
				item = {item}
				closeModal = {closeModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function analyseitemRate (item) {
	if (item.itemid > 0) {
		this.openModal({
			render: (closeModal) => {
				return <AnalyseRateModal
					item = {item}
					param = "estimations"
					customerid = {this.props.resource.customerid}
					customerid_name = {this.props.resource.customerid_name}
					closeModal = {closeModal} />
			}, className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	} else {
		this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Item Name is required!!",
			btnArray : ["Ok"]
		}));
	}
}

export function createItemRequest () {
	checkTransactionExist('itemrequests', 'estimations', this.props.resource.id, this.openModal, (param)=> {
		if(param)
			this.props.history.push({
				pathname: '/createItemRequest',
				params: {...this.props.resource, param: 'Estimations'}
			});
	});
}

export function createProformaInvoice() {
	this.updateLoaderFlag(true);
	checkTransactionExist('proformainvoices', 'estimations', this.props.resource.id, this.openModal, (param) => {
		if(param)
			this.props.history.push({pathname: '/createProformaInvoice', params: {...this.props.resource, param: 'Estimation'}});
		this.updateLoaderFlag(false);
	});
}

export function profitanalysedetails() {
	this.openModal({
		render: (closeModal) => {
			return <ItemrequestprofitanalyseModal
				resource = {this.props.resource}
				param = "estimations"
				app = {this.props.app}
				closeModal = {closeModal} />
		}, className: {
			content: 'react-modal-custom-class-60',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function cancel () {
	this.props.history.goBack();
}
