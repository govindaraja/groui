import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import { ChildEditModal } from '../components/utilcomponents';
import WorkOrderItemAddmodal from '../components/details/workorderitemaddmodal';
import WOProjectEstimationDetailsModal from '../components/details/woprojectestimationitemdetailsmodal';
import CasualLabourEstimationDetailsModal from '../components/details/casuallabourprojectestimationitemdetailsmodal';
import CasualLabourPurchaseInvoiceOptionModal from '../components/details/casuallabourpurchaseinvoiceoptionmodal';
import InternalLabourEstimationDetailsModal from '../components/details/internallabourprojectestimationitemdetailsmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		workprogressdate: new Date(new Date().setHours(0, 0, 0, 0)),
		workprogressitems: [],
		additionalcharges: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'Work Orders') {
			let workorderitemsArray = params.workorderitems;

			tempObj.projectid = params.projectid;
			tempObj.companyid = params.companyid;
			tempObj.worktype = 'Work Order';

			this.customFieldsOperation('workorders', tempObj, params, 'workprogress');

			for (var i = 0; i < workorderitemsArray.length; i++) {
				let wpQty = workorderitemsArray[i].quantity - (workorderitemsArray[i].completedqty || 0);
				let tempChiObj = {
					index: i+1,
					contractorid: params.partnerid,
					contractorid_displayname : params.partnerid_displayname,
					quantity: wpQty > 0 ? wpQty : 0
				};
				utils.assign(tempChiObj, workorderitemsArray[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_itemtype', 'itemid_uomgroupid', 'itemid_keepstock', 'itemid_itemcategorymasterid', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'description', 'uomid', 'uomid_name', 'uomconversionfactor', 'uomconversiontype', 'alternateuom', 'boqitemsid', 'projectestimationitemsid', {'workorderitemsid' : 'id'},'rate']);

				this.customFieldsOperation('workorderitems', tempChiObj, workorderitemsArray[i], 'workprogressitems');
				tempObj.workprogressitems.push(tempChiObj);
			}
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		if(!this.props.resource.currencyexchangerate)
			this.controller.currencyOnChange();

		this.controller.computeFinalRate();
	}, 0);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/workprogress/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			for (var i = 0; i < tempObj.workprogressitems.length; i++) {
				if (tempObj.workprogressitems[i].uomconversiontype)
					tempObj.workprogressitems[i].alternateuom = true;
			}
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			this.controller.checkVisibilityForButton();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function checkVisibilityForButton () {
	setTimeout(() => {
		let showPurchaseInvoiceBtn = false;
		this.props.resource.workprogressitems.forEach((item) => {
			if(item.workorderitemsid) {
				if((item.quantity || 0) > (item.workorderitemsid_invoicedqty || 0))
					showPurchaseInvoiceBtn = true;
			} else {
				if((item.quantity || 0) > (item.invoicedqty || 0))
					showPurchaseInvoiceBtn = true;
			}
		});
		this.setState({
			showPurchaseInvoiceBtn
		});
	}, 0);
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject)=> {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

		}, (reason)=> {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
	}
}

export function dateofcompletionchange() {
	let tempObj = {};
	for (var i = 0; i < this.props.resource.workprogressitems.length; i++) {
		if (!this.props.resource.workprogressitems[i].dateofcompletion || this.props.resource.workprogressitems[i].dateofcompletion == null) {
			tempObj[`workprogressitems[${i}].dateofcompletion`] = this.props.resource.dateofcompletion;
		}
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function getWorkOrderItemDetails () {
	this.openModal({render: (closeModal) => {
		return <WorkOrderItemAddmodal
			resource={this.props.resource}
			updateFormState={this.props.updateFormState}
			form={this.props.form}
			openModal={this.props.openModal}
			computeFinalRate = {this.controller.computeFinalRate}
			closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function getProjectEstimationItems () {
	this.openModal({render: (closeModal) => {
		return <WOProjectEstimationDetailsModal
			resource={this.props.resource}
			itemarray={'workprogressitems'}
			updateFormState={this.props.updateFormState}
			form={this.props.form}
			app={this.props.app}
			openModal={this.props.openModal}
			computeFinalRate = {this.controller.computeFinalRate}
			closeModal={closeModal}/>
		},
		className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function callBackProject (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		projectid_projectname : valueobj.projectname,
		projectid_displayname : valueobj.displayname,
		salesperson : valueobj.salesperson,
		projectid_estimationavailable : valueobj.estimationavailable,
		workprogressitems: []
	});
}

export function worktypeOnchange() {
	this.props.updateFormState(this.props.form, {
		projectid : null,
		workprogressitems : []
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, null, null, 'purchase');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.rate`] = 0;
		tempObj[`${itemstr}.itemid_itemtype`] = returnObject.itemid_itemtype;
		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app);
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.uomconversionfactor`] = null;
		tempObj[`${itemstr}.uomconversiontype`] = null;
		tempObj[`${itemstr}.itemid_activitytypeid`] = returnObject.itemid_activitytypeid;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'workprogressitems', itemstr);
		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function uomOnchange (id, valueobj, item, itemstr) {
	let tempObj = {};

	if (valueobj.id == id && valueobj.alternateuom) {
		tempObj[`${itemstr}.uomconversiontype`] = valueobj.conversiontype;
		tempObj[`${itemstr}.uomconversionfactor`] = valueobj.conversionrate;
		tempObj[`${itemstr}.alternateuom`] = valueobj.alternateuom;
	} else {
		tempObj[`${itemstr}.uomconversiontype`] = null;
		tempObj[`${itemstr}.uomconversionfactor`] = null;
		tempObj[`${itemstr}.alternateuom`] = false;
	}
	this.props.updateFormState(this.props.form, tempObj);

	let promise1 = commonMethods.getItemDetails(item.itemid, null, null, 'purchase', id, tempObj[`${itemstr}.alternateuom`], tempObj[`${itemstr}.uomconversionfactor`]);
	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app);
		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, item, itemstr) {
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, null, null, 'purchase', item.uomid, item.alternateuom, value);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app);
			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function callbackEmployee(id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.employeeid_displayname`] : valueobj.displayname
	});
}

export function computeFinalRate() {
	setTimeout(() =>{
		taxEngine(this.props, 'resource', 'workprogressitems', null, null, true);
	}, 0);
}

export function openPickDetails(item, itemstr) {
	this.openModal({render: (closeModal) => {
		return <CasualLabourEstimationDetailsModal 
			resource={this.props.resource}
			item={item}
			itemstr={itemstr}
			updateFormState = {this.props.updateFormState}
			form = {this.props.form}
			app={this.props.app}
			openModal={this.props.openModal}
			closeModal={closeModal}/>
		},
		className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function openInternalLabourPickDetails(item, itemstr) {
	this.openModal({render: (closeModal) => {
		return <InternalLabourEstimationDetailsModal 
			resource={this.props.resource}
			item={item}
			itemstr={itemstr}
			updateFormState = {this.props.updateFormState}
			form = {this.props.form}
			app={this.props.app}
			openModal={this.props.openModal}
			closeModal={closeModal}/>
		},
		className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions: confirm ? true : false
		},
		url : '/api/workprogress'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam)=> {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/workprogress/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace("/list/workprogress");
			else {
				this.props.initialize(response.data.main);
				this.controller.checkVisibilityForButton();
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function createPurchaseInvoice() {
	let contractoridArr = [];
	this.props.resource.workprogressitems.forEach((item) => {
		if(item.quantity > item.invoicedqty) {
			let diffContractor = false;
			for(var i=0; i < contractoridArr.length; i++) {
				if(item.contractorid == contractoridArr[i].contractorid) {
					diffContractor= true;
					break;
				}
			}
			if(!diffContractor) {
				contractoridArr.push({
					contractorid : item.contractorid,
					contractorid_displayname : item.contractorid_displayname,
				});
			}
		}
	});
	if(contractoridArr.length > 1) {
		this.openModal({render: (closeModal) => {
			return <CasualLabourPurchaseInvoiceOptionModal 
				contractoridArr={contractoridArr}
				callback={(contractorid)=> {
					this.controller.createInvoice(contractorid);
				}}
				openModal={this.props.openModal}
				closeModal={closeModal}/>
			},
			className: {
				content: 'react-modal-custom-class',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	} else {
		this.controller.createInvoice(contractoridArr[0].contractorid);
	}
}

export function createInvoice(contractorid) {
	let tempObj = { ...this.props.resource};
	tempObj.workprogressitems = [];
	this.props.resource.workprogressitems.forEach((item) => {
		if(item.contractorid == contractorid) {
			tempObj.workprogressitems.push(item);
		}
	});
	tempObj.partnerid = tempObj.workprogressitems[0].contractorid;
	this.props.history.push({pathname: '/createPurchaseInvoice', params: {...tempObj, param: 'Work Progress'}});
}

export function cancel () {
	this.props.history.goBack();
}
