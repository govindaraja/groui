import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { commonMethods, detailsSVC, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';
import { OrderPoQtyField, ItemRateField } from '../components/utilcomponents';
import ProjectprofitdetailsModal from '../components/details/projectprofitdetailsmodal';
import { RenderItemQty } from '../utils/customfieldtypes';
import async from 'async';
import KititemaddModal from '../components/details/kititemaddmodal';
import EstimationCostBreakupDetails from '../components/details/projectestimationcostbreakupdetails';
import ProjectEstimationCostBreakupOverviewDetails from '../components/details/projectestimationcostbreakupoverviewdetails';
import ProjectQuoteitemdetailsModal from '../components/details/projectquoteitemdetailsmodal';
import ProjectProfitAnalysisModal from '../components/details/projectquoteprofitanalysismodal';
import BOQGridEditModal from '../components/details/importexportprojectcomponent';
import ProjectMarkupModal from '../components/details/projectmarkupmodal';

export function onLoad () {
	let restrictestimationcommercials = false;
	this.props.app.feature.restrictprojectestimationrole.some((restrictrole) => {
		if(this.props.app.user.roleid.indexOf(restrictrole) > -1) {
			restrictestimationcommercials = true;
			return true;
		}
	});

	this.setState({
		restrictestimationcommercials,
		showItemReqTab: utils.checkActionVerbAccess(this.props.app, 'itemrequests', 'Menu'),
		showInvoiceTab: utils.checkActionVerbAccess(this.props.app, 'salesinvoices', 'Menu'),
		showReceiptTab: utils.checkActionVerbAccess(this.props.app, 'receiptvouchers', 'Menu'),
		showWOTab: utils.checkActionVerbAccess(this.props.app, 'workorders', 'Menu'),
		showProductionTab: utils.checkActionVerbAccess(this.props.app, 'productionorders', 'Menu'),
		showExpenseTab: utils.checkActionVerbAccess(this.props.app, 'expenserequests', 'Menu'),
		showProformaTab: utils.checkActionVerbAccess(this.props.app, 'proformainvoices', 'Menu')
	});

	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		startdate: new Date(new Date().setHours(0, 0, 0, 0)),
		projectitems: [],
		projectschedules : [],
		kititemprojectdetails: [],
		additionalcharges: [],
		milestoneitems: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'copy')
			tempObj = params;

		if(params.param == 'Project Quotes') {
			let projectquoteitemArray = params.projectquoteitems;
			let kititemprojectquoteArray = params.kititemprojectquotedetails;
			let additionalchargesArray = params.additionalcharges;
			let milestoneitemsArray = params.milestoneitems;

			projectquoteitemArray.forEach((item, i) => {
				if (this.props.app.uomObj[item.uomid])
					item.uomid_name = this.props.app.uomObj[item.uomid].name;

				let tempChildObj = {
					index: i+1,
					alternateuom: item.uomconversiontype ? true : false,
				};

				utils.assign(tempChildObj, item, ['internalrefno','clientrefno','itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_itemtype', 'itemnamevariationid', 'itemnamevariationid_name', 'description', 'specification', 'drawingno', 'rate', 'splitrate', 'labourrate', 'materialrate', 'taxid', 'labourtaxid', 'materialtaxid', 'itemid_uomgroupid', 'quantity', 'uomid', 'uomid_name', 'discountmode', 'discountquantity', 'itemid_keepstock', 'itemid_hasaddons', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'name', 'itemtype', {'quoteid' : 'parentid'}, {'quote_index' : 'index'}, {'boqquoteitemsid' : 'id'}, 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingquantity', 'billingconversiontype', 'billingconversionfactor', 'billingrate', 'milestonetemplateid','approvedmakes']);

				this.customFieldsOperation('projectitems', tempChildObj, item, 'projectitems');

				tempObj.projectitems.push(tempChildObj);
			});

			for (var i = 0; i < tempObj.projectitems.length; i++) {
				for (var j = 0; j < kititemprojectquoteArray.length; j++) {
					if(tempObj.projectitems[i].quoteid == kititemprojectquoteArray[j].parentid && tempObj.projectitems[i].quote_index == kititemprojectquoteArray[j].rootindex) {
						let tempChiObj = {
							rootindex: tempObj.projectitems[i].index
						};
						utils.assign(tempChiObj, kititemprojectquoteArray[j], ['itemid', 'itemid_name', 'parent', 'uomid', 'uomid_name', 'quantity', 'conversion', 'rate', 'ratelc', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'index', 'parentindex', 'costratio']);
						tempObj.kititemprojectdetails.push(tempChiObj);
					}
				}
			}

			milestoneitemsArray.forEach((item) => {
				tempObj.milestoneitems.push({
					name : item.name,
					milestonetemplateid : item.milestonetemplateid,
					milestonetemplateid_name : item.milestonetemplateid_name,
					milestonestageid : item.milestonestageid,
					milestonestageid_name : item.milestonestageid_name,
					percentage : item.percentage,
					displayorder : item.displayorder
				});
			});

			additionalchargesArray.forEach((item) => {
				tempObj.additionalcharges.push({
					additionalchargesid : item.additionalchargesid,
					additionalchargesid_name : item.additionalchargesid_name,
					description : item.description,
					amount : item.amount,
					taxcodeid : item.taxcodeid,
					defaultpercentage : item.defaultpercentage,
					displayorder : item.displayorder
				});
			});

			utils.assign(tempObj, params, [{'projectquoteid' : 'id'}, {'projectquoteid_quoteno' : 'quoteno'}, 'currencyid', 'currencyexchangerate', 'contactid', 'companyid', 'email', 'mobile', 'phone', 'deliveryaddress', 'deliveryaddressid', 'billingaddress', 'billingaddressid', 'salesperson', 'projectname', 'taxid', 'labourtaxid', 'materialtaxid', 'modeofdespatch', 'roundoffmethod', 'ismilestonerequired', 'paymentterms']);

			if (params.customerid > 0) {
				tempObj.customerid = params.customerid;
				tempObj.customerid_name = params.customerid_name;
				tempObj.pricelistid = params.pricelistid;
				tempObj.creditperiod = params.customerid_salescreditperiod;
				tempObj.territoryid = params.territoryid;
			}

			this.customFieldsOperation('projectquotes', tempObj, params, 'projects');
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.callbackCustomerDetails();
		this.controller.currencyOnChange();
		this.controller.computeFinalRate();
	}, 0);

	this.updateLoaderFlag(false);
}

export function onLoadKit (count, kititemprojectdetails) {
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;
	if(count < this.props.resource.projectitems.length) {
		if(this.props.resource.projectitems[count].itemid_issaleskit) {
			let promise1 = commonMethods.getItemDetails(this.props.resource.projectitems[count].itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales');
			promise1.then((returnObject) => {
				returnObject.kititems.forEach((kititem) => {
					kititem.rootindex = this.props.resource.projectitems[count].index;
					if(!kititem.parentindex) {
						let tempQuantity = Number((kititem.conversion * this.props.resource.projectitems[count].quantity).toFixed(roundOffPrecisionStock));;
						calculateQuantity(kititem.index, tempQuantity);
						kititem.quantity = tempQuantity;
					}
				});
				function calculateQuantity(tempIndex, tempQuantity) {
					returnObject.kititems.forEach((kititem) => {
						if (kititem.parentindex == tempIndex) {
							let sectempQty = Number((kititem.conversion * tempQuantity).toFixed(roundOffPrecisionStock));
							kititem.quantity = sectempQty;
							calculateQuantity(kititem.index, sectempQty);
						}
					});
				}
				returnObject.kititems.forEach((kititem) => {
					kititemprojectdetails.push(kititem);
				});
				this.controller.onLoadKit(count+1, kititemprojectdetails);
			}, (reason)=> {});
		} else {
			this.controller.onLoadKit(count+1, kititemprojectdetails);
		}
	} else {
		this.props.updateFormState(this.props.form, {
			kititemprojectdetails
		});
		this.controller.computeFinalRate();
	}
}

export function currencyOnChange () {
	if (this.props.resource.pricelistid)
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid)
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});

	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});

		this.controller.computeFinalRate();
	}
}

export function itemTypeOnChange (item, itemstr) {
	let tempObj = {};

	for(let prop in item) {
		if(['itemid','quantity','rate','uomid','finalrate'].indexOf(prop) >= 0)
			tempObj[`${itemstr}.${prop}`] = null;
	}

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function callbackCustomerDetails() {
	if(this.props.resource.customerid > 0) {
		return axios.get(`/api/partners?field=id,paymentterms,modeofdispatch,freight,customergroups/creditperiod/partnergroupid&filtercondition=partners.iscustomer=true and partners.id=${this.props.resource.customerid}`).then((response) => {
			if (response.data.main.length > 0)
				this.props.updateFormState(this.props.form, {
					paymentterms : this.props.resource.paymentterms ? this.props.resource.paymentterms : response.data.main[0].paymentterms,
					modeofdespatch : this.props.resource.modeofdespatch ? this.props.resource.modeofdespatch : response.data.main[0].modeofdispatch,
					freight : response.data.main[0].freight,
					creditperiod : this.props.resource.creditperiod ? this.props.resource.creditperiod : response.data.main[0].partnergroupid_creditperiod
				});
		});
	}
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.itemid_uomgroupid`] : valueobj.uomgroupid
	});

	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.itemid`] : valueobj.parentid
	});

	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales');

	promise.then((returnObject) => {
		let tempObj = {
			[`${itemstr}.itemid_name`] : returnObject.itemid_name,
			[`${itemstr}.description`] : returnObject.description,
			[`${itemstr}.itemid_issaleskit`] : returnObject.itemid_issaleskit,
			[`${itemstr}.itemid_itemtype`] : returnObject.itemid_itemtype,
			[`${itemstr}.rate`] : 0,
			[`${itemstr}.labourrate`] : 0,
			[`${itemstr}.materialrate`] : 0,
			[`${itemstr}.quantity`] : item.quantity ? item.quantity : 1,
			[`${itemstr}.uomid`] : returnObject.uomid,
			[`${itemstr}.uomid_name`] : returnObject.uomid_name,
			[`${itemstr}.alternateuom`] : returnObject.alternateuom,
			[`${itemstr}.uomgroupid`] : returnObject.uomgroupid,
			[`${itemstr}.itemid_uomgroupid`] : returnObject.uomgroupid,
			[`${itemstr}.splitrate`] : returnObject.splitrate,
			[`${itemstr}.taxid`] : (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid,
			[`${itemstr}.itemnamevariationid`] : returnObject.itemnamevariationid,
			[`${itemstr}.itemnamevariationid_name`] : returnObject.itemnamevariationid_name,
			[`${itemstr}.itemid_keepstock`] : returnObject.keepstock,
			[`${itemstr}.itemid_imageurl`] : returnObject.itemid_imageurl,
			[`${itemstr}.itemid_hasaddons`] : returnObject.hasaddons,
			[`${itemstr}.itemid_addonhelptext`] : returnObject.addonhelptext,
			[`${itemstr}.itemid_itemcategorymasterid`] : returnObject.itemcategorymasterid,
			[`${itemstr}.itemid_itemgroupid`] : returnObject.itemgroupid,
			[`${itemstr}.itemid_hasbatch`] : returnObject.hasbatch,
			[`${itemstr}.itemid_hasserial`] : returnObject.hasserial,
			[`${itemstr}.uomconversionfactor`] : returnObject.alternateuom ? returnObject.uomconversionfactor : null,
			[`${itemstr}.uomconversiontype`] : returnObject.alternateuom ? returnObject.uomconversiontype : null
		};

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;
		tempObj[`${itemstr}.billingquantity`] = null;
		tempObj[`${itemstr}.billingrate`] = null;

		if (returnObject.itemid_itemtype != 'Project') {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			if(tempObj[`${itemstr}.usebillinguom`]) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
			}
		} else {
			if(!tempObj[`${itemstr}.splitrate`])
				tempObj[`${itemstr}.rate`] = returnObject.rate ? returnObject.rate : 0;

			tempObj[`${itemstr}.labourtaxid`] = (this.props.resource.labourtaxid && this.props.resource.labourtaxid.length > 0) ? this.props.resource.labourtaxid : [];
			tempObj[`${itemstr}.materialtaxid`] = (this.props.resource.materialtaxid && this.props.resource.materialtaxid.length > 0) ? this.props.resource.materialtaxid : [];
		}

		if (item.index) {
			let itemCount = 0;
			tempObj.kititemprojectdetails = [...this.props.resource.kititemprojectdetails];
			for (var i = 0; i < tempObj.kititemprojectdetails.length; i++) {
				if (tempObj.kititemprojectdetails[i].rootindex == item.index)
					itemCount++;
			}
			for (var i = 0; i < itemCount; i++) {
				for (var j = 0; j < tempObj.kititemprojectdetails.length; j++) {
					if (tempObj.kititemprojectdetails[j].rootindex == item.index) {
						tempObj.kititemprojectdetails.splice(j, 1);
						break;
					}
				}
			}

			if (returnObject.itemid_issaleskit) {
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = item.index;
					tempObj.kititemprojectdetails.push(returnObject.kititems[i]);
				}
			}
		} else {
			let index = 0;
			for (var i = 0; i < this.props.resource.projectitems.length; i++) {
				if (this.props.resource.projectitems[i].index > index)
					index = this.props.resource.projectitems[i].index;
			}
			tempObj[`${itemstr}.index`] = index + 1;

			if (returnObject.itemid_issaleskit) {
				tempObj.kititemprojectdetails = [...this.props.resource.kititemprojectdetails];
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = tempObj[`${itemstr}.index`];
					tempObj.kititemprojectdetails.push(returnObject.kititems[i]);
				}
			}
		}

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'projectitems', itemstr);

		this.props.updateFormState(this.props.form, tempObj);
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson : valueobj.name,
		phone : valueobj.phone,
		email : valueobj.email,
		mobile : valueobj.mobile,
		contactid_name : valueobj.name,
		contactid_phone : valueobj.phone,
		contactid_email : valueobj.email,
		contactid_mobile : valueobj.mobile
	});
}

export function computeFinalRate() {
	setTimeout(() => {
		let tempObj = {};

		this.props.resource.projectitems.forEach((item, i) => {
			if (item.itemid_itemtype == 'Project' && item.splitrate)
				tempObj[`projectitems[${i}].rate`] = (item.labourrate ? item.labourrate : 0) + ((item.materialrate ? item.materialrate : 0));
		});

		this.props.updateFormState(this.props.form, tempObj);

		taxEngine(this.props, 'resource', 'projectitems', null, true, true);
	}, 0);
}

export function callBackCustomerName (id, valueobj) {
	let tempObj = {
		customerid_name : valueobj.name,
		contactid: valueobj.contactid,
		contactperson: valueobj.contactid_name,
		email : valueobj.contactid_email,
		phone : valueobj.contactid_phone,
		mobile : valueobj.contactid_mobile,
		deliveryaddress : valueobj.addressid_displayaddress,
		billingaddress : valueobj.addressid_displayaddress,
		billingaddressid : valueobj.addressid,
		deliveryaddressid : valueobj.addressid,
		paymentterms : valueobj.paymentterms,
		customerid_name : valueobj.name,
		modeofdespatch : valueobj.modeofdispatch,
		freight : valueobj.freight,
		territoryid : valueobj.territory,
		salesperson : this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		creditperiod : valueobj.salescreditperiod ? valueobj.salescreditperiod : valueobj.partnergroupid_creditperiod,
		pricelistid : valueobj.salespricelistid ? valueobj.salespricelistid : valueobj.partnergroupid_pricelistid
	};

	this.customFieldsOperation('partners', tempObj, valueobj, 'projectquotes');

	if (!tempObj.pricelistid) {
		for (let i = 0; i < this.props.app.appSettings.length; i++) {
			if (this.props.app.appSettings[i].module == 'Sales' && this.props.app.appSettings[i].name == 'Pricelistid') {
				tempObj.pricelistid = this.props.app.appSettings[i].value.value;
				break;
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function deleteProjectItem (index) {
	let itemCount = 0, milestoneitemCount = 0;
	let kititemprojectdetails = [...this.props.resource.kititemprojectdetails];
	let projectitems = [...this.props.resource.projectitems];
	let milestoneitems = [...this.props.resource.milestoneitems];
	let errorStr = '';

	if(projectitems[index].estimatedqty > 0) {
		errorStr = `Item "${projectitems[index].itemid_name}" is already estimated. Please delete this item estimation`;
	}

	let boqmilestonestatusObj = {};
	if(this.state.boqmilestonestatusarr && this.state.boqmilestonestatusarr.length > 0) {
		this.state.boqmilestonestatusarr.forEach((boqstatusitem) => {
			if(!boqmilestonestatusObj[boqstatusitem.boqid]) {
				boqmilestonestatusObj[boqstatusitem.boqid] = {
					invoicedqty: boqstatusitem.invoicedqty,
					completedqty: boqstatusitem.completedqty
				};
			} else {
				boqmilestonestatusObj[boqstatusitem.boqid].invoicedqty += boqstatusitem.invoicedqty;
				boqmilestonestatusObj[boqstatusitem.boqid].completedqty += boqstatusitem.completedqty;
			}
		});
	}

	if(boqmilestonestatusObj[projectitems[index].id] && boqmilestonestatusObj[projectitems[index].id].completedqty > 0) {
		errorStr = `Item "${projectitems[index].itemid_name}" is already completed. Please cancel this milestone progress created against this item`;
	}

	if(errorStr) {
		return this.props.openModal(modalService['infoMethod']({
			header : 'Warning',
			body : errorStr,
			btnArray : ['Ok']
		}));
	}

	for (var i = 0; i < kititemprojectdetails.length; i++) {
		if (kititemprojectdetails[i].rootindex == this.props.resource.projectitems[index].index)
			itemCount++;
	}
	for (var i = 0; i < itemCount; i++) {
		for (var j = 0; j < kititemprojectdetails.length; j++) {
			if (kititemprojectdetails[j].rootindex == this.props.resource.projectitems[index].index) {
				kititemprojectdetails.splice(j, 1);
				break;
			}
		}
	}

	let deleteMilestoneitem = true;
	for (var i = 0; i < projectitems.length; i++) {
		if(i != index && projectitems[i].milestonetemplateid == projectitems[index].milestonetemplateid) {
			deleteMilestoneitem = false;
			break;
		}
	}

	if(deleteMilestoneitem) {
		milestoneitems.forEach((item) => {
			if (item.milestonetemplateid == projectitems[index].milestonetemplateid)
				milestoneitemCount++;
		});
	}

	for (var i = 0; i < milestoneitemCount; i++) {
		for (var j = 0; j < milestoneitems.length; j++) {
			if (milestoneitems[j].milestonetemplateid == projectitems[index].milestonetemplateid) {
				milestoneitems.splice(j, 1);
				break;
			}
		}
	}

	projectitems.splice(index, 1);

	this.props.updateFormState(this.props.form, {
		kititemprojectdetails,
		projectitems,
		milestoneitems
	});
	this.controller.computeFinalRate();
}

export function quantityOnChange(itemqty, item, itemstr) {
	let kititemprojectdetails = this.props.resource.kititemprojectdetails;
	let tempObj = {};
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;

	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
	}

	for (var i = 0; i < kititemprojectdetails.length; i++) {
		if (item.index == kititemprojectdetails[i].rootindex) {
			if (!kititemprojectdetails[i].parentindex) {
				let tempIndex = kititemprojectdetails[i].index;
				let tempQuantity = Number((kititemprojectdetails[i].conversion * itemqty).toFixed(roundOffPrecisionStock));
				tempObj[`kititemprojectdetails[${i}].quantity`] = tempQuantity;
				calculateQuantity(tempIndex, tempQuantity, item.index);
			}
		}
	}

	function calculateQuantity(tempIndex, tempQuantity, index) {
		for (var j = 0; j < kititemprojectdetails.length; j++) {
			if (tempIndex == kititemprojectdetails[j].parentindex && index == kititemprojectdetails[j].rootindex) {
				let sectempQty = Number((kititemprojectdetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
				tempObj[`kititemprojectdetails[${j}].quantity`] = sectempQty;
				calculateQuantity(kititemprojectdetails[j].index, sectempQty, index);
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function conversionOnChange (item) {
	for (var i = 0; i < this.props.resource.projectitems.length; i++) {
		if (item.rootindex == this.props.resource.projectitems[i].index) {
			this.controller.quantityOnChange(this.props.resource.projectitems[i].quantity, this.props.resource.projectitems[i], `projectitems[${i}]`);
			break;
		}
	}
}

export function addKitItem() {
	this.openModal({
		render: (closeModal) => {
			return <KititemaddModal resource = {this.props.resource} child={'projectitems'} kit={'kititemprojectdetails'} array={this.props.array} callback={()=>{
				for (var i = 0; i < this.props.resource.projectitems.length; i++) {
					this.controller.quantityOnChange(this.props.resource.projectitems[i].quantity, this.props.resource.projectitems[i], `projectitems[${i}]`);
				}
			}} closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function deleteKitItem(index) {
	let errorArray = [];
	let itemfound = false;
	let kititemprojectdetails = [...this.props.resource.kititemprojectdetails];

	for (var i = 0; i < kititemprojectdetails.length; i++) {
		if (index != i && kititemprojectdetails[index].rootindex == kititemprojectdetails[i].rootindex && kititemprojectdetails[index].parentindex == kititemprojectdetails[i].parentindex) {
			itemfound = true;
		}
	}
	if (!itemfound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	let itemCouFound = false;
	for (var i = 0; i < kititemprojectdetails.length; i++) {
		if (index != i && kititemprojectdetails[index].rootindex == kititemprojectdetails[i].rootindex && !kititemprojectdetails[i].parentindex)
			itemCouFound = true;
	}
	if (!itemCouFound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	if (errorArray.length == 0) {
		let parentIndexArray = [kititemprojectdetails[index].index];
		let rootindex = kititemprojectdetails[index].rootindex;
		deleteArray(kititemprojectdetails[index].rootindex, kititemprojectdetails[index].index);

		function deleteArray(rootindex, index) {
			for (var i = 0; i < kititemprojectdetails.length; i++) {
				if (kititemprojectdetails[i].rootindex == rootindex && kititemprojectdetails[i].parentindex == index) {
					parentIndexArray.push(kititemprojectdetails[i].index)
					deleteArray(rootindex, kititemprojectdetails[i].index);
				}
			}
		}

		for (var i = 0; i < parentIndexArray.length; i++) {
			for (var j = 0; j < kititemprojectdetails.length; j++) {
				if (kititemprojectdetails[j].rootindex == rootindex && kititemprojectdetails[j].index == parentIndexArray[i]) {
					kititemprojectdetails.splice(j, 1);
					break;
				}
			}
		}

		this.props.updateFormState(this.props.form, {kititemprojectdetails});
	} else {
		let response = {
			data : {
				message : 'failure',
				error : utils.removeDuplicate(errorArray)
			}
		};
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
}

export function estimationdetailedcallback(index) {
	return this.openModal({
		render : (closeModal) => {
			return <ProjectEstimationCostBreakupOverviewDetails resourceitems={this.props.resource.projectitems} index={index} resourcename = {'projects'} closeModal={closeModal} app={this.props.app} />
		}, className: {
			content : 'react-modal-custom-class-80',
			overlay : 'react-modal-overlay-custom-class'
		}
	});
}

export function ismilestonerequiredOnChange() {
	if(!this.props.resource.ismilestonerequired) {
		let projectitems = [...this.props.resource.projectitems];
		projectitems.forEach((item) => {
			item.milestonetemplateid = null;
		});
		this.props.updateFormState(this.props.form, {
			projectitems,
			milestoneitems: []
		});
	}
}

export function getimportMilestoneItems() {
	let projectitems = [...this.props.resource.projectitems];
	let milestoneitems = [...this.props.resource.milestoneitems];
	let milestonetemplateidArr = [], tempmilestonetemplateidArr = [];

	if(!this.props.resource.ismilestonerequired)
		return;

	projectitems.forEach((item) => {
		if(item.itemtype == 'Item' && item.milestonetemplateid && !milestonetemplateidArr.includes(item.milestonetemplateid))
			milestonetemplateidArr.push(item.milestonetemplateid);
	});

	if(milestonetemplateidArr.length == 0)
		return;

	axios.get(`/api/milestonetemplateitems?field=id,parentid,milestonestageid,percentage,milestonestages/name/milestonestageid,displayorder,milestonetemplates/name/parentid&filtercondition=milestonetemplateitems.parentid IN (${milestonetemplateidArr})`).then((response) => {
		if (response.data.message == 'success') {
			let tempMilestonetemplateitems = response.data.main.sort((a, b) => a.id - b.id);

			deleteOldMilestoneitems(() => {
				tempMilestonetemplateitems.forEach((item) => {
					let itemfound = false;
					milestoneitems.some((milestoneitem) => {
						if(item.parentid == milestoneitem.milestonetemplateid && item.milestonestageid == milestoneitem.milestonestageid) {
							itemfound = true;
							return true;
						}
					});

					if(!itemfound) {
						milestoneitems.push({
							name: item.milestonestageid_name,
							milestonetemplateid: item.parentid,
							milestonetemplateid_name: item.parentid_name,
							milestonestageid: item.milestonestageid,
							milestonestageid_name: item.milestonestageid_name,
							percentage: item.percentage
						});
					}
				});

				this.props.updateFormState(this.props.form, {
					milestoneitems
				});
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});

	function deleteOldMilestoneitems(milestonedeleteCB) {
		let deleteitemcount = 0;
		projectitems.forEach((item) => {
			milestonetemplateidArr.push(item.milestonetemplateid);
		});

		milestoneitems.forEach((milestoneitem, milestoneindex) => {
			if(!milestonetemplateidArr.includes(milestoneitem.milestonetemplateid)) {
				deleteitemcount++;
			}
		});

		for (var i = 0; i < deleteitemcount; i++) {
			milestoneitems.some((milestoneitem, milestoneindex) => {
				if(!milestonetemplateidArr.includes(milestoneitem.milestonetemplateid)) {
					milestoneitems.splice(milestoneindex, 1);
					return true;
				}
			});
		}
		milestonedeleteCB();
	}
}

export function callbackMilestoneTemplate(value, valueobj, item, itemstr) {
	let milestoneitems = [...this.props.resource.milestoneitems];
	let projectitems = [...this.props.resource.projectitems];
	let milestonetemplateidArr = [], deleteitemcount = 0;

	if(value > 0) {
		axios.get(`/api/milestonetemplateitems?field=id,parentid,milestonestageid,percentage,milestonestages/name/milestonestageid,displayorder,milestonetemplates/name/parentid&filtercondition=milestonetemplateitems.parentid=${value}`).then((response) => {
			if (response.data.message == 'success') {
				let tempMilestonetemplateitems = response.data.main.sort((a, b) => a.displayorder - b.displayorder);

				deleteOldMilestoneitems(() => {
					let itemfound = false;
					milestoneitems.some((milestoneitem) => {
						if(milestoneitem.milestonetemplateid == value) {
							itemfound = true;
							return true;
						}
					});

					if(!itemfound) {
						tempMilestonetemplateitems.forEach((item) => {
							milestoneitems.push({
								name: item.milestonestageid_name,
								milestonetemplateid: item.parentid,
								milestonetemplateid_name: item.parentid_name,
								milestonestageid: item.milestonestageid,
								milestonestageid_name: item.milestonestageid_name,
								percentage: item.percentage
							});
						});
					}
				});

				this.props.updateFormState(this.props.form, {
					milestoneitems,
					[`${itemstr}.milestonetemplateid_name`] : valueobj.name
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	} else {
		deleteOldMilestoneitems(() => {
			this.props.updateFormState(this.props.form, {
				milestoneitems
			});
		});
	}

	function deleteOldMilestoneitems(milestonedeleteCB) {
		projectitems.forEach((item) => {
			milestonetemplateidArr.push(item.milestonetemplateid);
		});

		milestoneitems.forEach((milestoneitem, milestoneindex) => {
			if(!milestonetemplateidArr.includes(milestoneitem.milestonetemplateid)) {
				deleteitemcount++;
			}
		});

		for (var i = 0; i < deleteitemcount; i++) {
			milestoneitems.some((milestoneitem, milestoneindex) => {
				if(!milestonetemplateidArr.includes(milestoneitem.milestonetemplateid)) {
					milestoneitems.splice(milestoneindex, 1);
					return true;
				}
			});
		}
		milestonedeleteCB();
	}
}

export function callBackConsigneeName (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		consigneeid: valueobj.id,
		consigneeid_name: valueobj.name,
		deliveryaddress: null,
		deliveryaddressid: null
	});
}

export function addressonChange(address, addressobj) {
	this.props.updateFormState(this.props.form, {
		[`${address}`] : addressobj.displayaddress,
		[`${address}id`] : addressobj.id
	});
}

export function getItemById () {
	axios.get(`/api/projects/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;

			tempObj.projectitems.forEach((item) => {
				if (item.uomconversiontype)
					item.alternateuom = true;
			});

			this.props.initialize(tempObj);

			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});

			this.controller.checkVisibilityForButton();
			this.controller.callGetRelatedResource();
			this.controller.getboqmilstonestatus();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getboqmilstonestatus() {
	axios.get(`/api/boqmilestonestatus?&field=id,boqid,milestoneitemsid,completedqty,invoicedqty&filtercondition=boqmilestonestatus.projectid=${this.props.resource.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.setState({
				boqmilestonestatusarr: response.data.main
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function callGetRelatedResource() {
	setTimeout(() => {
		let id = this.props.resource.id;
		let array = [{
			resource: 'itemrequests',
			access: 'showItemReqTab',
			url: `/listapi/itemrequests?&field=id,itemrequestno,itemrequestdate,status,issuedpercent&filtercondition=itemrequests.projectid=${id}`,
			array: 'itemrequestsArray'
		}, {
			resource: 'salesinvoices',
			access: 'showInvoiceTab',
			url: `/listapi/salesinvoices?&field=id,invoiceno,invoicedate,status,finaltotal,outstandingamount,currencyid&filtercondition=salesinvoices.projectid=${id}`,
			array: 'invoiceArray'
		}, {
			resource: 'receiptvouchers',
			access: 'showReceiptTab',
			url: `/listapi/receiptvouchers?&field=id,voucherno,voucherdate,amount,currencyid,status&filtercondition=journalvouchers.projectid=${id}`,
			array: 'receiptArray'
		}, {
			resource: 'workorders',
			access: 'showWOTab',
			url: `/listapi/workorders?field=id,wonumber,wodate,status,partners/name/partnerid&filtercondition=workorders.projectid=${id}`,
			array: 'woArray'
		}, {
			resource: 'productionorders',
			access: 'showProductionTab',
			url: `/listapi/productionorders?field=id,orderno,orderdate,ordertype,status&filtercondition=productionorders.projectid=${id}`,
			array: 'productionArray'
		}, {
			resource: 'expenserequests',
			access: 'showExpenseTab',
			url: `/listapi/expenserequests?field=id,expenserequestno,expensedate,currencyid,amount,amountrequested,amountapproved,status,remarks&filtercondition=expenserequests.projectid=${id}`,
			array: 'expenseArray'
		}, {
			resource: 'proformainvoices',
			access: 'showProformaTab',
			url: `/api/proformainvoices?field=id,invoiceno,invoicedate,currencyid,amount,status,paymentstatus&filtercondition=proformainvoices.projectid=${id}`,
			array: 'proformaArray'
		}];
		let tempObj = {};
		async.each(array, (prime, eachCB) => {
			if(this.state[prime.access]) {
				axios.get(prime.url).then((response) => {
					if (response.data.message == 'success') {
						tempObj[prime.array] = response.data.main;
					} else {
						let apiResponse = commonMethods.apiResult(response);
						this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
					}
					eachCB(null);
				});
			} else {
				eachCB(null);
			}
		}, () => {
			this.props.updateFormState(this.props.form, tempObj);
		});
	}, 0);
}

export function checkVisibilityForButton () {
	setTimeout(() => {
		let showDeliveryBtn = false

		this.props.resource.projectitems.forEach((item) => {
			if(item.itemid_itemtype == 'Product') {
				if(item.quantity > (item.deliveredqty || 0))
					showDeliveryBtn = true
			}
		});
		this.setState({ showDeliveryBtn });
	}, 0);
}

export function itemLevelInitfn(itemstr, updateValue) {
	let tempItem = this.selector(this.props.fullstate, itemstr);

	if(!tempItem[`${updateValue}`])
		this.props.updateFormState(this.props.form, {
			[`${itemstr}[${updateValue}]`] : this.props.resource[`${updateValue}`]
		});
}

export function renderTableQtyfield(item) {
	return (
		<RenderItemQty status={this.props.resource.status} resource={this.props.resource} item={item} uomObj={this.props.app.uomObj} statusArray={['Approved','Completed']}/>
	);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function renderTableReffield(item) {
	return (
		<div>
			<span>{item.internalrefno}</span>
			{item.internalrefno ?<br></br> : null}
			<span className="text-muted">{item.clientrefno ? `(${item.clientrefno})` : null}</span>
		</div>
	)
}

export function renderItemDetails(item) {
	return (
		<div>
			<div className={item.itemtype == 'Section' ? 'text-ellipsis-line-2' : ''}>{item.itemtype == 'Section' ? item.name : item.itemid_name}</div>
			<div className="text-ellipsis-line-2 font-13 text-muted">{item.description}</div>
		</div>
	);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && param !='Amend' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/projects'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/projects/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace("/list/projects");
			else {
				this.props.initialize({
					...response.data.main,
					tempfinaltotal : response.data.main.finaltotal
				});

				this.controller.checkVisibilityForButton();
			}
		}

		this.updateLoaderFlag(false);
	});
}

export function createRelatedTransaction (param) {
	if (param == 'itemrequest')
		this.props.history.push({
			pathname: '/createItemRequest',
			params: {
				...this.props.resource,
				param: 'Projects'
			}
		});
	else if (param == 'invoice') {
		checkTransactionExist('salesinvoices', 'projects', this.props.resource.id, this.openModal, (param)=> {
			if(param) {
				this.props.history.push({
					pathname: '/createSalesInvoice',
					params: {
						...this.props.resource,
						param: 'Projects'
					}
				});
			}
		});
	} else if (param == 'receipt')
		this.props.history.push({
			pathname: '/createReceiptVoucher',
			params: {
				...this.props.resource,
				param: 'Projects'
			}
		});
	else if (param == 'workorder')
		this.props.history.push({
			pathname: '/createWorkOrder',
			params: {
				param : 'Projects',
				projectid : this.props.resource.id,
				estimationavailable : this.props.resource.estimationavailable,
				currencyid : this.props.resource.currencyid,
				currencyexchangerate : this.props.resource.currencyexchangerate,
				defaultcostcenter: this.props.resource.defaultcostcenter,
				allProjectObj: {...this.props.resource}
			}
		});
	else if (param == 'productionorder')
		this.props.history.push({
			pathname: '/createProductionOrder',
			params: {
				...this.props.resource,
				param: 'Projects'
			}
		});
	else if (param == 'expenserequest')
		this.props.history.push({
			pathname: '/createExpenseRequest',
			params: {
				...this.props.resource,
				param: 'Projects'
			}
		});
	else if (param == 'proformainvoices'){
		this.props.history.push({
			pathname: '/createProformaInvoice', 
			params: {
				...this.props.resource, 
				param: 'Projects'
			}
		});
	}
}

export function createEstimations() {
	this.updateLoaderFlag(true);
	checkTransactionExist('projectestimations', 'projects', this.props.resource.id, this.openModal, (param) => {
		if(param)
			this.props.history.push({pathname: '/createProjectEstimation', params: {...this.props.resource, param: 'Projects'}});
		this.updateLoaderFlag(false);
	});
}

export function getQuoteItemDetails() {
	this.props.openModal({
		render: (closeModal) => {
			return <ProjectQuoteitemdetailsModal resource={this.props.resource} callback={this.controller.projectquoteitemmodalcallback} customFieldsOperation={this.customFieldsOperation} updateFormState={this.props.updateFormState} form={this.props.form} openModal={this.props.openModal} closeModal={closeModal} />
		}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
	});
}

export function projectquoteitemmodalcallback() {
	this.props.resource.kititemprojectdetails.forEach((kititem, index) => {
		this.controller.quantityOnChange(kititem.quantity, kititem, `kititemprojectdetails[${index}]`);
	});
	this.controller.computeFinalRate();
}

export function getProjectProfitDetail () {
	return this.openModal({
		render: (closeModal) => {
			return <ProjectprofitdetailsModal
				resource = {this.props.resource}
				openModal = {this.props.openModal}
				closeModal = {closeModal}
				history = {this.props.history}
				updateLoaderFlag = {this.updateLoaderFlag} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function cancel () {
	this.props.history.goBack();
}


export function profitabilityAnalysis() {
	return this.openModal({
		render: (closeModal) => {
			return <ProjectProfitAnalysisModal
					resourcename = {'projects'}
					resourceid = {this.props.resource.id}
					resourceitems = {this.props.resource.projectitems}
					app = {this.props.app}
					closeModal = {closeModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function boqGridEditView() {
	return this.props.openModal({
		render: (closeModal) => {
			return <BOQGridEditModal
				childjson = {this.state.pagejson.projectitems}
				resourcename={'projectitems'}
				resource = {this.props.resource}
				updateFormState = {this.props.updateFormState}
				computeFinalRate = {this.controller.computeFinalRate}
				onLoadKit = {this.controller.onLoadKit}
				getimportMilestoneItems = {this.controller.getimportMilestoneItems}
				form = {this.props.form}
				openModal={this.props.openModal}
				app = {this.props.app}
				closeModal = {closeModal} />
		}, className: {
			content: 'react-modal-custom-class-100',
			overlay: 'react-modal-overlay-custom-class'
		},
		confirmModal: true
	});
}

export function markupbtncallback() {
	return this.props.openModal({
		render: (closeModal) => {
			return <ProjectMarkupModal
				resourcename = {'projects'}
				resource = {this.props.resource}
				updateFormState = {this.props.updateFormState}
				computeFinalRate = {this.controller.computeFinalRate}
				form = {this.props.form}
				openModal={this.props.openModal}
				app = {this.props.app}
				closeModal = {closeModal} />
		}, className: {
			content: 'react-modal-custom-class-100',
			overlay: 'react-modal-overlay-custom-class'
		},
		confirmModal: true
	});
}