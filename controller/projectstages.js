import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';
import NewProjectStageModal from '../components/details/newprojectstagemodal';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function isactiveInitfn() {
	if(this.props.resource.isactiveInitfn == null) {
		this.props.updateFormState(this.props.form, {
			isactive : true
		});
	}
}

export function getItemById () {
	axios.get(`/api/projectstages/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param) {
	if(param == 'Delete') {
		this.openModal({
			render: (closeModal) => {
				return <NewProjectStageModal parentresource={this.props.resource} callback={(id) => {
					this.props.updateFormState(this.props.form, {
						stageid : id
					});
					setTimeout(()=>{this.controller.saveProjectStage(param);}, 0);
				}} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	} else {
		this.controller.saveProjectStage(param);
	}
}

export function saveProjectStage (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	let tempData = {
		actionverb : param,
		data : this.props.resource,
		ignoreExceptions : confirm ? true : false
	};

	axios({
		method : 'post',
		data : tempData,
		url : '/api/projectstages'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.saveProjectStage(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/projectstages/${response.data.main.id}`);
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/projectstages");
				} else {
					this.props.initialize(response.data.main);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
