import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {};
	let queryString = `/api/employees?&field=id,displayname&filtercondition=employees.userid=${this.props.app.user.id}`;

	axios.get(queryString).then((response)=> {
		if (response.data.message == 'success') {
			if(response.data.main.length > 0) {
				tempObj.employeeid = response.data.main[0].id;
			}
		}
		this.props.initialize(tempObj);
		this.updateLoaderFlag(false);
	});
}

export function getItemById () {
	axios.get(`/api/leaverequests/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function changeHalfDay () {
	if(this.props.resource.ishalfday)
		this.props.updateFormState(this.props.form, {
			todate: this.props.resource.fromdate
		});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	let resourceObj = this.props.resource;

	resourceObj.fromdate = new Date(resourceObj.fromdate).toDateString();
	resourceObj.todate = new Date(resourceObj.todate).toDateString();

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : resourceObj,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/leaverequests'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		});

		if (response.data.message == 'success') {
			if (this.state.createParam)  {
				this.props.history.replace(`/details/leaverequests/${response.data.main.id}`);
			} else {
				if (param != 'Delete')
					this.props.initialize(response.data.main);
				else
					this.props.history.replace('/list/leaverequests');
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
