import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';
import * as utils from '../utils/utils';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		projectestimationtemplateitems: [],
		productitems: [],
		subcontractitems: [],
		labouritems: []
	};

	if(this.props.location.params) {
		let params = this.props.location.params;
		tempObj.boqitemid = params.boqitemid;
		tempObj.boqitemid_itemtype = params.boqitemid_itemtype;
		for (var i = 0; i < params.productitems.length; i++) {
			let tempProdChiObj = {};
			utils.assign(tempProdChiObj, params.productitems[i], ['itemid', 'itemid_name', 'itemid_itemtype', 'quantity', 'description', 'quantity', 'uomid', 'uomconversionfactor', 'uomconversiontype', 'activitytypeid', 'rate', 'amount']);

			tempObj.productitems.push(tempProdChiObj);
		}
		for (var i = 0; i < params.subcontractitems.length; i++) {
			let tempSubChiObj = {};
			utils.assign(tempSubChiObj, params.subcontractitems[i], ['itemid', 'itemid_name', 'itemid_itemtype', 'quantity', 'description', 'quantity', 'uomid', 'uomconversionfactor', 'uomconversiontype', 'activitytypeid', 'rate', 'amount']);

			tempObj.subcontractitems.push(tempSubChiObj);
		}
		for (var i = 0; i < params.labouritems.length; i++) {
			let tempLabChiObj = {};
			utils.assign(tempLabChiObj, params.labouritems[i], ['itemid', 'itemid_name', 'itemid_itemtype', 'quantity', 'description', 'quantity', 'uomid', 'uomconversionfactor', 'uomconversiontype', 'activitytypeid', 'rate', 'amount']);

			tempObj.labouritems.push(tempLabChiObj);
		}
	}
	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}


export function getItemById () {
	axios.get(`/api/projectestimationtemplate/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.productitems = [];
			tempObj.subcontractitems = [];
			tempObj.labouritems = [];

			tempObj.projectestimationtemplateitems.forEach((item) => {
				if(item.itemid_itemtype == 'Product')
					tempObj.productitems.push(item);
				else if(item.itemid_itemtype == 'Project' || item.itemid_itemtype == 'Service')
					tempObj.subcontractitems.push(item);
				else if(item.activitytypeid)
					tempObj.labouritems.push(item);
			});

			this.props.initialize(tempObj);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackitem(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		boqitemid_itemtype : valueobj.itemtype,
		productitems : [],
		subcontractitems : [],
		labouritems : []
	});
}
 

export function callbackProduct(id, valueobj, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, null, null, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app),

		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	});
}

export function computeFinalRate() {
	setTimeout(() => {
		let projectestimationtemplateitems = [];

		this.props.resource.productitems.forEach((item) => {
			projectestimationtemplateitems.push(item);
		});

		this.props.resource.subcontractitems.forEach((item) => {
			projectestimationtemplateitems.push(item);
		});

		this.props.resource.labouritems.forEach((item) => {
			projectestimationtemplateitems.push(item);
		});

		this.props.updateFormState(this.props.form, {
			projectestimationtemplateitems
		});

		setTimeout(() => {
			taxEngine(this.props, 'resource', 'projectestimationtemplateitems');
		}, 0);
	}, 0);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	let templateObj = this.props.resource;
	templateObj.projectestimationtemplateitems = [];

	templateObj.productitems.forEach((item) => {
		templateObj.projectestimationtemplateitems.push(item);
	});
	templateObj.subcontractitems.forEach((item) => {
		templateObj.projectestimationtemplateitems.push(item);
	});
	templateObj.labouritems.forEach((item) => {
		templateObj.projectestimationtemplateitems.push(item);
	});

	axios({
		method : 'POST',
		data : {
			actionverb : param,
			data : templateObj
		},
		url : '/api/projectestimationtemplate'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		if (response.data.message == 'success') {
			if (this.props.isModal) {
				this.props.closeModal();
				this.props.callback(response.data.main);
			}else {
				if (this.state.createParam)
					this.props.history.replace(`/details/projectestimationtemplate/${response.data.main.id}`);
				else if(param == 'Delete')
					this.props.history.replace('/list/projectestimationtemplate');
				else {
					let tempObj = response.data.main;
					tempObj.productitems = [];
					tempObj.subcontractitems = [];
					tempObj.labouritems = [];

					tempObj.projectestimationtemplateitems.forEach((item) => {
						if(item.itemid_itemtype == 'Product')
							tempObj.productitems.push(item);
						else if(item.itemid_itemtype == 'Project' || item.itemid_itemtype == 'Service')
							tempObj.subcontractitems.push(item);
						else if(item.activitytypeid)
							tempObj.labouritems.push(item);
					});
					this.props.initialize(tempObj);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	if (this.props.isModal) {
		this.props.closeModal();
	} else {
		this.props.history.goBack();
	}
}
