import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { modalService, commonMethods, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		let tempObj = {};

		if(this.props.location.params)
			tempObj = { ...this.props.location.params };

		this.props.initialize(tempObj);

		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById() {
	axios.get(`/api/attendancecapture/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == "success")
			this.props.initialize(response.data.main);
		else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function changeOnDuty () {
	if(this.props.resource.onduty) {

		let intimeValue = this.props.resource.intime ? new Date(this.props.resource.intime) : new Date(this.props.resource.date);
		let outtimeValue = this.props.resource.outtime ? new Date(this.props.resource.outtime) : new Date(this.props.resource.date);

		axios.get(`/api/employees?&field=id,displayname,locationid,workstarttime,workendtime&filtercondition=employees.id=${this.props.resource.employeeid}`).then((response)=> {
			if (response.data.message == 'success') {
				if(response.data.main.length > 0) {
					let in_hours = response.data.main[0].workstarttime ? new Date(response.data.main[0].workstarttime).getHours() : 9;
					let in_minutes = response.data.main[0].workstarttime ? new Date(response.data.main[0].workstarttime).getMinutes() : 0;

					intimeValue = new Date(intimeValue.setHours(in_hours));
					intimeValue = new Date(intimeValue.setMinutes(in_minutes));


					let out_hours = response.data.main[0].workendtime ? new Date(response.data.main[0].workendtime).getHours() : 18;
					let out_minutes = response.data.main[0].workendtime ? new Date(response.data.main[0].workendtime).getMinutes() : 0;

					outtimeValue = new Date(outtimeValue.setHours(out_hours));
					outtimeValue = new Date(outtimeValue.setMinutes(out_minutes));
				}

				this.props.updateFormState(this.props.form, {
					intime : intimeValue,
					outtime : outtimeValue
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	} else
		this.props.updateFormState(this.props.form, {
			intime : null,
			outtime : null
		});
};

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	let tempObj = this.props.resource;

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	if (tempObj.leave && tempObj.leavetype == 'Full Day') {
		tempObj.intime = '';
		tempObj.outtime = '';
	} else {
		if (tempObj.onduty && !tempObj.intime) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Warning",
				body : "In Time is required !!!",
				btnArray : ["Ok"]
			}));
		}

		if (tempObj.onduty && !tempObj.outtime) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Warning",
				body : "Out Time is required !!!",
				btnArray : ["Ok"]
			}));
		}
	}

	if ((tempObj.intime && new Date(tempObj.date).toDateString() != new Date(tempObj.intime).toDateString()) || (tempObj.outtime &&  new Date(tempObj.date).toDateString() != new Date(tempObj.outtime).toDateString())) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Warning",
			body : "In Time / Out Time date must be equal to Attendance Date !!!",
			btnArray : ["Ok"]
		}));
	}

	let historydate = new Date(tempObj.date);
	let historyArray = tempObj.history ? tempObj.history.details : [];

	historyArray.push({
		date : new Date().setFullYear(historydate.getFullYear(),historydate.getMonth(),historydate.getDate()),
		intime :  tempObj.intime,
		outtime : tempObj.outtime,
		leave : (tempObj.leave) ? 'leave' : '',
		permission : tempObj.permission,
		leavetype : tempObj.leavetype,
		onduty : tempObj.onduty,
		remarks : tempObj.remarks,
		modified : new Date(),
		modifiedby : this.props.app.user.id,
		modifiedby_displayname : this.props.app.user.displayname
	});

	tempObj.history = {
		details : historyArray
	};

	if((tempObj.loginlatitude && tempObj.loginlongitude && !tempObj.intime) || (tempObj.logoutlatitude && tempObj.logoutlongitude && !tempObj.outtime)) {
		return this.props.openModal(modalService['infoMethod']({
			header : 'Warning',
			bodyArray : ['This has been check-in/check-out from mobile app, so you cannot remove in/out time'],
			btnArray : ['Ok']
		}));
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : tempObj,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/attendancecapture'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == "success") {
			if(this.props.isModal) {
				if(param != 'Delete')
					this.props.callback(response.data.main);
				this.props.closeModal();
			} else {
				if(param == 'Delete')
					this.props.history.replace('/attendancecapture');
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	if(this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}
