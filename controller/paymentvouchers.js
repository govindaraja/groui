import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation, checkAccountBalance } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';
import EmailModal from '../components/details/emailmodal';
import { AccountBalance,ChildEditModal } from '../components/utilcomponents';
import EstimationItemPickingdetailsModal from '../components/details/estimationitempickingdetailsmodal';
import CostCenterPickingdetailsModal from '../components/details/costcenterpickingdetailsmodal';

let onLoadFlag = false;

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();

	this.setState({
		paymentforarray: [{
			name: 'PO',
			displayname: 'Purchase Order'
		}, {
			name: 'WO',
			displayname: 'Work Order'
		}]
	});
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		voucherdate: new Date(new Date().setHours(0, 0, 0, 0)),
		vouchertype: 'Payment',
		journalvoucheritems: [],
		employeeoutstandings: [],
		invoiceitems: [],
		deductions: [],
		otheraccounts: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;
		let purchaseorderitemArray = params.supplierquotationitems;

		if(params.param == 'copy') {
			tempObj = params;
		}

		if(params.param == 'Purchase Orders') {
			tempObj.paymentfor = 'PO';
			tempObj.isadvance = true;

			utils.assign(tempObj, params, ['partnerid', 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'poid' : 'id'}, {'amount' : 'finaltotal'}, 'contactid', 'contactperson', 'email', 'phone', 'mobile']);
			this.customFieldsOperation('purchaseorders', tempObj, params, 'journalvouchers');
		}

		if(params.param == 'Purchase Invoices') {

			utils.assign(tempObj, params, ['partnerid', 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'amount' : 'outstandingamount'}, 'contactid', 'contactperson', 'email', 'phone', 'mobile']);
			this.customFieldsOperation('purchaseinvoices', tempObj, params, 'journalvouchers');

			tempObj.invoiceitems.push({
				accountid : params.payableaccountid,
				xpurchaseinvoiceid_partnerreference : params.partnerreference,
				xpurchaseinvoiceid : params.id,
				xpurchaseinvoiceid_paymentduedate : params.paymentduedate,
				xpurchaseinvoiceid_invoicedate : params.supplierinvoicedate,
				xpurchaseinvoiceid_finaltotal : params.finaltotal,
				xpurchaseinvoiceid_outstandingamount : params.outstandingamount,
				debit : params.outstandingamount
			});
			onLoadFlag = true;
		}

		if(params.param == 'Accounts Payable Report') {
			tempObj.currencyid = params.paymentArray[0].currencyid ? params.paymentArray[0].currencyid : this.props.app.defaultCurrency;
			utils.assign(tempObj, params.paymentArray[0], ['partnerid', 'currencyexchangerate', 'contactid', 'contactperson', 'email', 'phone', 'mobile']);

			let totalBalance=0;
			for(var i = 0; i < params.paymentArray.length; i++) {
				totalBalance += params.paymentArray[i].balance;
				tempObj.invoiceitems.push({
					xpurchaseinvoiceid_partnerreference : params.paymentArray[i].voucherno,
					xpurchaseinvoiceid : params.paymentArray[i].xpurchaseinvoiceid,
					xpurchaseinvoiceid_paymentduedate : params.paymentArray[i].duedate,
					xpurchaseinvoiceid_invoicedate : params.paymentArray[i].voucherdate,
					xpurchaseinvoiceid_finaltotal : params.paymentArray[i].balance,
					xpurchaseinvoiceid_outstandingamount : params.paymentArray[i].balance,
					xjournalvoucherid_voucherno : params.paymentArray[i].voucherno,
					xjournalvoucherid : params.paymentArray[i].xjournalvoucherid,
					xjournalvoucherid_paymentduedate : params.paymentArray[i].voucherdate,
					xjournalvoucherid_voucherdate : params.paymentArray[i].voucherdate,
					xjournalvoucherid_finaltotal : params.paymentArray[i].balance,
					accountid : params.paymentArray[i].accountid,
					debit : params.paymentArray[i].balance
				});
			}
			tempObj.amount = totalBalance;
			onLoadFlag = true;
		}

		if(params.param == 'Work Orders') {
			tempObj.paymentfor = 'WO';
			tempObj.isadvance = true;

			utils.assign(tempObj, params, ['partnerid', 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'woid' : 'id'}, {'amount' : 'finaltotal'}, {'contactid' : 'suppliercontactid'}, {'contactperson' : 'suppliercontactperson'}, {'email' : 'supplieremail'}, {'phone' : 'supplierphone'}, {'mobile' : 'suppliermobile'}]);

			this.customFieldsOperation('workorders', tempObj, params, 'journalvouchers');
		}

		if(params.param == 'Expense Requests') {
			utils.assign(tempObj, params, ['employeeid', 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'amount' : 'outstandingamount'}]);

			tempObj.employeeoutstandings.push({
				accountid : params.emppayableaccountid,
				xexpenserequestid_expenserequestno : params.expenserequestno,
				xexpenserequestid : params.id,
				xexpenserequestid_expensedate : params.expensedate,
				xexpenserequestid_amount : params.amount,
				xexpenserequestid_outstandingamount : params.outstandingamount,
				debit : params.outstandingamount
			});
			onLoadFlag = true;
		}

		if(params.param == 'Pay Roll') {
			utils.assign(tempObj, params, ['employeeid', 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'amount' : 'outstandingamount'}]);

			tempObj.employeeoutstandings.push({
				accountid : params.emppayableaccountid,
				xpayrollid_payrollno : params.payrollno,
				xpayrollid : params.id,
				xpayrollid_payrolldate : params.payrolldate,
				xpayrollid_amount : params.amount,
				xpayrollid_outstandingamount : params.outstandingamount,
				debit : params.outstandingamount
			});
			onLoadFlag = true;
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.currencyOnChange();
	}, 0);
	
	this.updateLoaderFlag(false);
}

export function renderEmployeeAccountBalance(item, itemstr) {
	/*checkAccountBalance({
		companyid: this.props.resource.companyid,
		accountid: item.accountid,
		show: 'debit',
		againstid: item.xexpenserequestid > 0 ? item.xexpenserequestid : (item.xpayrollid ? item.xpayrollid : item.xjournalvoucherid),
		type: item.xexpenserequestid > 0 ? 'expenserequests' : (item.xpayrollid ? 'payrolls' :  'journalvouchers'),
		employeeid: this.props.resource.employeeid,
	}, item, itemstr, this.selector, this.props);
	return item.accountbalance;*/
	return (
		<AccountBalance app={this.props.app} updateFn={(value) => this.updateLocalCompState(`${itemstr}.accountbalance`, value)} currencyid={this.props.resource.currencyid} showAs="text" balance={item.xexpenserequestid ? item.xexpenserequestid_outstandingamount : (item.xpayrollid ? item.xpayrollid_outstandingamount : null)} item={{
			companyid: this.props.resource.companyid,
			accountid: item.accountid,
			show: 'debit',
			againstid: item.xexpenserequestid > 0 ? item.xexpenserequestid : (item.xpayrollid ? item.xpayrollid : item.xjournalvoucherid),
			type: item.xexpenserequestid > 0 ? 'expenserequests' : (item.xpayrollid ? 'payrolls' :  'journalvouchers'),
			employeeid: this.props.resource.employeeid,
		}} />
	);
}

export function renderAccountBalance(item, itemstr) {
	/*checkAccountBalance({
		companyid: this.props.resource.companyid,
		accountid: item.accountid,
		show: 'debit',
		againstid: item.xpurchaseinvoiceid > 0 ? item.xpurchaseinvoiceid : item.xjournalvoucherid,
		type: item.xpurchaseinvoiceid > 0 ? 'purchaseinvoices' : 'journalvouchers',
		partnerid: this.props.resource.partnerid,
	}, item, itemstr, this.selector, this.props);
	return item.accountbalance;*/
	return (
		<AccountBalance app={this.props.app} updateFn={(value) => this.updateLocalCompState(`${itemstr}.accountbalance`, value)} currencyid={this.props.resource.currencyid} showAs="text" balance={item.xpurchaseinvoiceid ? item.xpurchaseinvoiceid_outstandingamount : null} item={{
			companyid: this.props.resource.companyid,
			accountid: item.accountid,
			show: 'debit',
			againstid: item.xpurchaseinvoiceid > 0 ? item.xpurchaseinvoiceid : item.xjournalvoucherid,
			type: item.xpurchaseinvoiceid > 0 ? 'purchaseinvoices' : 'journalvouchers',
			partnerid: this.props.resource.partnerid,
		}} />
	);
}

export function getItemById () {
	axios.get(`/api/paymentvouchers/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;

			if(tempObj.vouchertype == 'Receipt' && this.props.match.path != '/details/receiptvouchers/:id')
				return this.props.history.replace(`/details/receiptvouchers/${tempObj.id}`);
			if(tempObj.vouchertype == 'Payment' && this.props.match.path != '/details/paymentvouchers/:id')
				return this.props.history.replace(`/details/paymentvouchers/${tempObj.id}`);
			if(tempObj.vouchertype == 'Credit Note' && this.props.match.path != '/details/creditnotes/:id')
				return this.props.history.replace(`/details/creditnotes/${tempObj.id}`);
			if(tempObj.vouchertype == 'Debit Note' && this.props.match.path != '/details/debitnotes/:id')
				return this.props.history.replace(`/details/debitnotes/${tempObj.id}`);
			if(tempObj.vouchertype == 'Journal Voucher' && this.props.match.path != '/details/journalvouchers/:id')
				return this.props.history.replace(`/details/journalvouchers/${tempObj.id}`);

			tempObj.invoiceitems = [];
			tempObj.deductions = [];
			tempObj.otheraccounts = [];
			tempObj.employeeoutstandings = [];

			for (var i = 0; i < tempObj.journalvoucheritems.length; i++) {
				if ((tempObj.journalvoucheritems[i].xpurchaseinvoiceid || tempObj.journalvoucheritems[i].xjournalvoucherid) && tempObj.partnerid > 0)
					tempObj.invoiceitems.push(tempObj.journalvoucheritems[i]);
				else if ((tempObj.journalvoucheritems[i].xexpenserequestid || tempObj.journalvoucheritems[i].xpayrollid || tempObj.journalvoucheritems[i].xjournalvoucherid) && tempObj.employeeid > 0)
					tempObj.employeeoutstandings.push(tempObj.journalvoucheritems[i]);
				else if (tempObj.journalvoucheritems[i].debit > 0)
					tempObj.otheraccounts.push(tempObj.journalvoucheritems[i]);
				else
					tempObj.deductions.push(tempObj.journalvoucheritems[i]);
			}
		
			if(tempObj.poid)
				tempObj.paymentfor = 'PO';

			if(tempObj.woid)
				tempObj.paymentfor = 'WO';

			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackEngineer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson: valueobj.userid
	});
}

export function callbackProject(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		projectid_estimationavailable: valueobj.estimationavailable
	});
}

export function callBackPartner (id, valueobj) {
	let tempObj = {
		contactid: valueobj.contactid,
		contactperson: valueobj.contactid_name,
		email: valueobj.contactid_email,
		phone: valueobj.contactid_phone,
		mobile: valueobj.contactid_mobile,
	};

	this.customFieldsOperation('partners', tempObj, valueobj, 'journalvouchers');
	this.props.updateFormState(this.props.form, tempObj);

	this.controller.getPurchaseInvoices();
}

export function callBackEmployee (id, valueobj) {
	this.controller.getPurchaseInvoices();
 }

export function callBackWO (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		defaultcostcenter: valueobj.defaultcostcenter
	});
}

export function callBackPO (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		defaultcostcenter: valueobj.defaultcostcenter
	});
}

export function isAdvanceChange () {
	this.props.updateFormState(this.props.form, {
		paymentfor: null,
		invoiceitems: [],
		employeeoutstandings: [],
		otheraccounts: [],
		onaccountamount: 0,
		poid: null,
		woid: null
	});
	this.controller.getPurchaseInvoices();
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			if(onLoadFlag)
				onLoadFlag = false;
			else
				this.controller.getPurchaseInvoices();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		if(onLoadFlag)
			onLoadFlag = false;
		else
			this.controller.getPurchaseInvoices();
	}
}

export function callBackAccount (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.accountid_name`]: valueobj.name,
		[`${itemstr}.accountid_createfollowup`]: valueobj.createfollowup,
		[`${itemstr}.followup`]: valueobj.createfollowup,
	});
}

export function paymentforOnChange () {
	this.props.updateFormState(this.props.form, {
		poid: null,
		woid: null
	});
}

export function getPurchaseInvoices () {
	if(this.props.resource.partnerid && this.props.resource.employeeid)
		return this.props.openModal(modalService.infoMethod({
			header : "Failure",
			body : "Please Choose either Partner or Employee",
			btnArray : ["Ok"]
		}));

	if(this.props.resource.isadvance || !this.props.resource.companyid || !this.props.resource.currencyid)
		return null;

	if(!this.props.resource.partnerid && !this.props.resource.employeeid)
		return null;

	this.updateLoaderFlag(true);
	let tempObj = {
		invoiceitems: [],
		amount: 0,
		employeeoutstandings: []
	};
	if (this.props.resource.partnerid) {
		axios.get(`/api/query/getpartneroutstandingsquery?type=Payment&companyid=${this.props.resource.companyid}&currencyid=${this.props.resource.currencyid}&partnerid=${this.props.resource.partnerid}`).then((response) => {
			if (response.data.message == 'success') {

				response.data.main.forEach((invitem) => {
					tempObj.amount += invitem.balance;
					invitem.debit = invitem.balance;
					if(invitem.xpurchaseinvoiceid)
						invitem.xpurchaseinvoiceid_outstandingamount = invitem.balance;
					tempObj.invoiceitems.push(invitem);
				});

				tempObj.amount = Number(tempObj.amount.toFixed(this.props.app.roundOffPrecision));

				if (tempObj.invoiceitems.length == 0) {
					this.props.openModal(modalService.infoMethod({
						header : "Warning",
						body : "No outstanding found for this Customer. You can create an advance entry",
						btnArray : ["Ok"]
					}));
				}
					
				for (var i = 0; i < tempObj.invoiceitems.length; i++) {
					tempObj.invoiceitems[i].tempduedate = (tempObj.invoiceitems[i].xpurchaseinvoiceid ? tempObj.invoiceitems[i].xpurchaseinvoiceid_paymentduedate : tempObj.invoiceitems[i].xjournalvoucherid_voucherdate);
				}

				tempObj.invoiceitems.sort((a, b) => {
					let aduedate = (a.xpurchaseinvoiceid_paymentduedate || a.xjournalvoucherid_voucherdate);
					let bduedate = (b.xpurchaseinvoiceid_paymentduedate || b.xjournalvoucherid_voucherdate);

					return new Date(aduedate) - new Date(bduedate);
				});

				this.props.updateFormState(this.props.form, tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	if (this.props.resource.employeeid) {
		axios.get(`/api/query/getemployeeoutstandingsquery?type=Payment&companyid=${this.props.resource.companyid}&currencyid= ${this.props.resource.currencyid}&employeeid=${this.props.resource.employeeid}`).then((response) => {
			if (response.data.message == 'success') {

				response.data.main.forEach((invitem) => {
					tempObj.amount += invitem.balance;
					invitem.debit = invitem.balance;
					if(invitem.xexpenserequestid)
						invitem.xexpenserequestid_outstandingamount = invitem.balance;
					if(invitem.xpayrollid)
						invitem.xpayrollid_outstandingamount = invitem.balance;
					tempObj.employeeoutstandings.push(invitem);
				});
				tempObj.amount = Number(tempObj.amount.toFixed(this.props.app.roundOffPrecision));

				if (tempObj.employeeoutstandings.length == 0) {
					this.props.openModal(modalService.infoMethod({
						header : "Warning",
						body : "No outstanding found for this Employee",
						btnArray : ["Ok"]
					}));
				}

				for (var i = 0; i < tempObj.employeeoutstandings.length; i++) {
					tempObj.employeeoutstandings[i].tempduedate = (tempObj.employeeoutstandings[i].xexpenserequestid ? tempObj.employeeoutstandings[i].xexpenserequestid_expensedate : tempObj.employeeoutstandings[i].xjournalvoucherid_voucherdate);
				}

				tempObj.employeeoutstandings.sort((a, b) => {
					let aduedate = a.xexpenserequestid_expensedate || a.xjournalvoucherid_voucherdate;
					let bduedate = b.xexpenserequestid_expensedate || b.xjournalvoucherid_voucherdate;
					return new Date(aduedate) - new Date(bduedate);
				});

				this.props.updateFormState(this.props.form, tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson: valueobj.name,
		phone: valueobj.phone,
		email: valueobj.email,
		mobile: valueobj.mobile,
		contactid_name: valueobj.name,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile,
	});
}

export function getAllocatedAmount (param) {
	let childNameObj = {
		totalinvoiceamount: 'invoiceitems',
		totalaccountamount: 'otheraccounts',
		totaldeductionamount: 'deductions',
		totalexpenseamount: 'employeeoutstandings',
	}
	let childName = childNameObj[param];
	let childFieldName = param == 'totaldeductionamount' ? 'credit' : 'debit';
	let totalamount = 0;
	
	for(var i = 0; i < this.props.resource[childName].length;i++)
		totalamount += (this.props.resource[childName][i][childFieldName] ? this.props.resource[childName][i][childFieldName] : 0);

	return Number(totalamount.toFixed(this.props.app.roundOffPrecision));
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Supplier',
			data : emailObj
		},
		url : '/api/paymentvouchers'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					var apiResponse = commonMethods.apiResult(response);
					this.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				var apiResponse = commonMethods.apiResult(response);
				this.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			var apiResponse = commonMethods.apiResult(response);
			this.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function openCostCenterPickDetails (item, itemstr) {
	this.openModal({render: (closeModal) => {
		return <CostCenterPickingdetailsModal item={item} resource={this.props.resource} itemstr={itemstr} amountstr={`${item.debit ? 'debit' : 'credit'}`} app={this.props.app} form={this.props.form} updateFormState={this.props.updateFormState} openModal={this.openModal} closeModal={closeModal} />
	}, className: {
		content: 'react-modal-custom-class-60',
		overlay: 'react-modal-overlay-custom-class'
	}, confirmModal: true });
}

export function save (param) {
	this.updateLoaderFlag(true);
	let tempresourceObj = {
		journalvoucheritems: []
	};

	tempresourceObj.amountinwords = commonMethods.numberToWord(this.props.app, this.props.resource.amount, this.props.resource.currencyid);
	if (this.props.resource.currencyid == this.props.app.defaultCurrency) {
		tempresourceObj.amountinwordslc = commonMethods.numberToWord(this.props.app, this.props.resource.amount);
		tempresourceObj.amountlc = this.props.resource.amount;
	} else {
		if (this.props.resource.currencyexchangerate) {
			tempresourceObj.amountlc = Number((this.props.resource.amount * this.props.resource.currencyexchangerate).toFixed(this.props.app.roundOffPrecision));				
			tempresourceObj.amountinwordslc = commonMethods.numberToWord(this.props.app, tempresourceObj.amountlc)
		} else {
			tempresourceObj.amountlc = 0;
		}
	}
	tempresourceObj.isadvance = this.props.resource.isadvance;
	if(tempresourceObj.isadvance && !this.props.resource.partnerid && !this.props.resource.employeeid)
		tempresourceObj.isadvance = false;

	if (!tempresourceObj.isadvance) {

		for (var i = 0; i < this.props.resource.invoiceitems.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.invoiceitems[i]);

		for (var i = 0; i < this.props.resource.deductions.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.deductions[i]);

		for (var i = 0; i < this.props.resource.otheraccounts.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.otheraccounts[i]);

		for (var i = 0; i < this.props.resource.employeeoutstandings.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.employeeoutstandings[i]);

		let creditAmount = 0, debitAmount = 0;
		creditAmount += this.props.resource.amount;
		debitAmount += (this.props.resource.onaccountamount ? this.props.resource.onaccountamount : 0);

		for (var i = 0; i < tempresourceObj.journalvoucheritems.length; i++) {
			creditAmount += tempresourceObj.journalvoucheritems[i].credit ? tempresourceObj.journalvoucheritems[i].credit : 0;
			debitAmount += tempresourceObj.journalvoucheritems[i].debit ? tempresourceObj.journalvoucheritems[i].debit : 0;
		}

		this.props.updateFormState(this.props.form, tempresourceObj);
		if (Number(creditAmount.toFixed(this.props.app.roundOffPrecision)) == Number(debitAmount.toFixed(this.props.app.roundOffPrecision))) {
			setTimeout(() => {
				this.controller.savePaymentVoucher(param);
			}, 0);
		} else {
			this.updateLoaderFlag(false);
			let message = {
				header : 'Error',
				body : 'Sum of Allocated Amount should equal to sum of Voucher amount and Deductions',
				btnArray : ['Ok']
			};
			this.props.openModal(modalService.infoMethod(message));
		}
	} else {
		for (var i = 0; i < this.props.resource.deductions.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.deductions[i]);

		for (var i = 0; i < this.props.resource.otheraccounts.length; i++)
			tempresourceObj.journalvoucheritems.push(this.props.resource.otheraccounts[i]);

		this.props.updateFormState(this.props.form, tempresourceObj);
		setTimeout(() => {
			this.controller.savePaymentVoucher(param);
		}, 0);
	}
}

export function savePaymentVoucher (param, confirm,confirm2) {
	if (param == 'Send To Supplier') {
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'paymentvouchers'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
		this.updateLoaderFlag(false);
	} else {
		this.updateLoaderFlag(true);

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm2 && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
			let message = {
				header : 'Cost Center Alert',
				body : `Cost Center not selected. Do you want to ${param} ?`,
				btnArray : ['Yes','No']
			};

			return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
				if(resparam)
					this.controller.savePaymentVoucher(param,confirm,true);
				else
					this.updateLoaderFlag(false);
			}));	
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/paymentvouchers'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.savePaymentVoucher(param, true,confirm2);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					this.props.history.replace(`/details/paymentvouchers/${response.data.main.id}`);
				} else {
					if (param == 'Delete') {
						this.props.history.replace("/list/paymentvouchers");
					} else {
						let tempObj = response.data.main;
						tempObj.invoiceitems = [];
						tempObj.deductions = [];
						tempObj.otheraccounts = [];
						tempObj.employeeoutstandings = [];

						for (var i = 0; i < tempObj.journalvoucheritems.length; i++) {
							if ((tempObj.journalvoucheritems[i].xpurchaseinvoiceid || tempObj.journalvoucheritems[i].xjournalvoucherid) && tempObj.partnerid)
								tempObj.invoiceitems.push(tempObj.journalvoucheritems[i]);
							else if ((tempObj.journalvoucheritems[i].xexpenserequestid || tempObj.journalvoucheritems[i].xpayrollid || tempObj.journalvoucheritems[i].xjournalvoucherid) && tempObj.employeeid)
								tempObj.employeeoutstandings.push(tempObj.journalvoucheritems[i]);
							else if(tempObj.journalvoucheritems[i].debit > 0)
								tempObj.otheraccounts.push(tempObj.journalvoucheritems[i]);
							else
								tempObj.deductions.push(tempObj.journalvoucheritems[i]);
						}
						this.props.initialize(tempObj);
					}
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function cancel () {
	this.props.history.goBack();
}

export function openEstimationItemPickDetails (index,itemstr){
	let getBody = (closeModal) => {
		return (
			<EstimationItemPickingdetailsModal />
		)
	}
	this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} index={index} child={'otheraccounts'} itemstr={itemstr} app={this.props.app} updateFormState={this.props.updateFormState} createOrEdit={this.props.createOrEdit} openModal={this.openModal} closeModal={closeModal} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}