import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initializeState();
	else
		this.controller.getItemById();
}

export function initializeState () {
	let tempObj = {
		roundoffmethod : this.props.app.defaultRoundOffMethod,
		paystructureitems : [],
		totalearnings : 0,
		totaldeductions : 0,
		totalemployercontributions : 0
	};

	if(this.props.location.params && this.props.location.params.param == 'copy')
		tempObj = this.props.location.params;

	this.props.initialize(tempObj);
	this.controller.computeFinalRate();
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/paystructures/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callBackComponent (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.componentid_name`]: valueobj.name,
		[`${itemstr}.type`]: valueobj.type,
		[`${itemstr}.componentid_calculationtype`]: valueobj.calculationtype,
		[`${itemstr}.tdsapplicable`]: valueobj.tdsapplicable,
		[`${itemstr}.taxexempt`]: valueobj.taxexempt,
		[`${itemstr}.pfapplicable`]: valueobj.pfapplicable,
		[`${itemstr}.esiapplicable`]: valueobj.esiapplicable,
		[`${itemstr}.utilitymoduleid`]: valueobj.utilitymoduleid,
		[`${itemstr}.componentid_accountid`]: valueobj.accountid,
		[`${itemstr}.componentid_accrualaccountid`]: valueobj.accrualaccountid,
		[`${itemstr}.earningvalue`]: null,
		[`${itemstr}.deductionvalue`]: null,
		[`${itemstr}.componentid_roundoffmethod`]: valueobj.roundoffmethod,
	});

	this.controller.computeFinalRate();
}

export function computeFinalRate() {
	setTimeout(() => {

		let netpay = 0,
			totalearnings = 0,
			totaldeductions = 0,
			totalemployercontributions = 0;

		this.props.resource.paystructureitems.forEach((item) => {
			if(item.earningvalue > 0 && item.type == 'Earning')
				totalearnings += item.earningvalue;

			if(item.deductionvalue > 0 && item.type == 'Deduction')
				totaldeductions += item.deductionvalue;

			if(item.employercontributionvalue > 0 && item.type == 'Employer Contribution')
				totalemployercontributions += item.employercontributionvalue;
		});

		netpay = totalearnings - totaldeductions;

		this.props.updateFormState(this.props.form, {
			totalearnings,
			totaldeductions,
			totalemployercontributions,
			netpay
		});

	}, 0);
}

export function callbackEmployee (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		employeeid_displayname : valueobj.displayname,
		name : null
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/paystructures'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace("/details/paystructures/" + response.data.main.id);
			else if (param == 'Delete')
				this.props.history.replace("/list/paystructures");
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
