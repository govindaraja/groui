import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { modalService, commonMethods, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		let tempObj = {};
		if(this.props.location.params) {
			tempObj.parentid = this.props.location.params.parentid ? this.props.location.params.parentid : null
		};
		this.props.initialize(tempObj);
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById() {
	axios.get(`/api/teamstructure/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == "success") {
			this.props.initialize(response.data.main);
			this.controller.getUsers(response.data.main.id);
		} else {
			this.updateLoaderFlag(false);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getUsers(id) {
	axios.get(`/api/users?pagelength=10000&includeinactive=true&field=id,displayname,salesteamlevels,isactive,modified&filtercondition=${id}=ANY(users.salesteamlevels)`).then((response)=> {
			if (response.data.message == 'success') {
				this.props.updateFormState(this.props.form, {
					users: response.data.main
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
}

export function userCB(value, valueObj) {
	this.setState({
		user: value ? valueObj : null
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/teamstructure'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == "success") {
			if(this.props.isModal) {
				this.props.callback(response.data.main);
				this.props.closeModal();
			} else {
				if(this.state.createParam)
					this.props.history.replace(`/details/teamstructure/${response.data.main.id}`);
				else if(param == 'Delete')
					this.props.history.replace('/teamstructure');
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function userOperation (param, user) {
	let tempObj = user;
	if (param == 'Add') {
		if(!tempObj.salesteamlevels)
			tempObj.salesteamlevels = [];

		if(tempObj.salesteamlevels.indexOf(this.props.resource.id) >= 0) {
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : `The user ${tempObj.displayname} already part of ${this.props.resource.name} team!!!`,
				btnName : ["Ok"]
			}));
		} else {
			tempObj.salesteamlevels.push(this.props.resource.id);
		}
	} else {
		tempObj.salesteamlevels.splice(tempObj.salesteamlevels.indexOf(this.props.resource.id), 1);
	}

	this.updateLoaderFlag(true);

	axios({
		method : 'post',
		data : {
			actionverb : 'Update SalesTeamLevel',
			data : tempObj
		},
		url : '/api/users'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		if(response.data.message == "success") {
			this.setState({
				user: null
			});
			if (response.data.message == 'success')
				this.controller.getItemById();
			else
				this.updateLoaderFlag(false);
		}
	});
}

export function cancel () {
	if(this.props.isModal) {
		this.props.callback();
		this.props.closeModal();
	} else {
		this.props.history.goBack();
	}
}
