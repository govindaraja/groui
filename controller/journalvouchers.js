import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation, checkAccountBalance } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';
import AdvancesinvoicesModal from '../components/details/advancesandinvoices';
import CostCenterPickingdetailsModal from '../components/details/costcenterpickingdetailsmodal';

export function onLoad () {
	if(this.state.createParam) {
		axios.get(`/api/accounts?field=id,name,type,accountgroup,createfollowup&includeinactive=true`).then((response) => {
			if(response.data.message == 'success') {
				let accountObj = {};
				for(var i = 0; i < response.data.main.length; i++)
					accountObj[response.data.main[i].id] = response.data.main[i];

				this.controller.initialiseState(accountObj);
			}
		});
	} else
		this.controller.getItemById();

	this.setState({
		journalVoucherTypes:  {
			'salesinvoices' : 'Sales Invoice',
			'purchaseinvoices' : 'Purchase Invoice',
			'journalvouchers' : 'Journal Voucher',
			'receiptvouchers' : 'Receipt',
			'paymentvouchers' : 'Payment',
			'creditnote' : 'Credit Note',
			'debitnote' : 'Debit Note',
			'expenserequests' : 'Expense Request',
			'payrolls' : 'Pay Roll'
		},
		reversejournalVoucherTypes: {
			'Sales Invoice' : 'salesinvoices',
			'Purchase Invoice' : 'purchaseinvoices',
			'Journal Voucher' : 'journalvouchers',
			'Receipt' : 'receiptvouchers',
			'Payment' : 'paymentvouchers',
			'Credit Note' : 'creditnote',
			'Debit Note' : 'debitnote',
			'Expense Request' : 'expenserequests',
			'Pay Roll' : 'payrolls'
		},
		againstreferenceArray: [{name: 'salesinvoices', displayname: 'Sales Invoice'}, {name: 'purchaseinvoices', displayname: 'Purchase Invoice'}, {name: 'journalvouchers', displayname: 'Journal Voucher'}, {name: 'receiptvouchers', displayname: 'Receipt'}, {name: 'paymentvouchers', displayname: 'Payment'}, {name: 'creditnote', displayname: 'Credit Note'}, {name: 'debitnote', displayname: 'Debit Note'}, {name: 'expenserequests', displayname: 'Expense Request'}, {name: 'payrolls', displayname: 'Pay Roll'}]
	});
}

export function initialiseState(accountObj) {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		voucherdate: new Date(new Date().setHours(0, 0, 0, 0)),
		vouchertype: 'Journal Voucher',
		journalvoucheritems: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'copy') {
			tempObj = params;
			tempObj.voucherdate = new Date(new Date().setHours(0, 0, 0, 0));
		}

		if (params.param == "Accounts Receivable Report") {
			tempObj.currencyid = params.voucherArray[0].currencyid > 0 ? params.voucherArray[0].currencyid : tempObj.currencyid;
			for(var i = 0; i < params.voucherArray.length; i++) {
				tempObj.journalvoucheritems.push({
					accountid : params.voucherArray[i].accountid,
					accountid_name : accountObj[params.voucherArray[i].accountid].name,
					accountid_type : accountObj[params.voucherArray[i].accountid].type,
					accountid_accountgroup : accountObj[params.voucherArray[i].accountid].accountgroup,
					accountid_createfollowup : accountObj[params.voucherArray[i].accountid].createfollowup,
					followup : accountObj[params.voucherArray[i].accountid].createfollowup,
					partnerid : params.voucherArray[i].partnerid,
					partnerid_name : params.voucherArray[i].partnerid_name,
					credit : params.voucherArray[i].balance,
					againstreference : this.state.reversejournalVoucherTypes[params.voucherArray[i].vouchertype],
					xsalesinvoiceid : params.voucherArray[i].xsalesinvoiceid,
					xsalesinvoiceid_invoiceno : params.voucherArray[i].xsalesinvoiceid > 0 ? params.voucherArray[i].voucherno : null,
					xsalesinvoiceid_invoicedate : params.voucherArray[i].xsalesinvoiceid > 0 ? params.voucherArray[i].voucherdate : null,
					xpurchaseinvoiceid : params.voucherArray[i].xpurchaseinvoiceid,
					xpurchaseinvoiceid_partnerreference : params.voucherArray[i].xpurchaseinvoiceid > 0 ? params.voucherArray[i].voucherno : null,
					xpurchaseinvoiceid_supplierinvoicedate : params.voucherArray[i].xpurchaseinvoiceid > 0 ? params.voucherArray[i].supplierinvoicedate : null,
					xjournalvoucherid : params.voucherArray[i].xjournalvoucherid,
					xjournalvoucherid_voucherno : params.voucherArray[i].xjournalvoucherid > 0 ? params.voucherArray[i].voucherno : null,
					xjournalvoucherid_voucherdate : params.voucherArray[i].xjournalvoucherid > 0 ? params.voucherArray[i].voucherdate : null
				});
			}
		}
		if (params.param == "Accounts Payable Report") {
			tempObj.currencyid = params.voucherArray[0].currencyid > 0 ? params.voucherArray[0].currencyid : tempObj.currencyid;
			for(var i = 0; i < params.voucherArray.length; i++) {
				tempObj.journalvoucheritems.push({
					accountid : params.voucherArray[i].accountid,
					accountid_name : accountObj[params.voucherArray[i].accountid].name,
					accountid_type : accountObj[params.voucherArray[i].accountid].type,
					accountid_accountgroup : accountObj[params.voucherArray[i].accountid].accountgroup,
					accountid_createfollowup : accountObj[params.voucherArray[i].accountid].createfollowup,
					followup : accountObj[params.voucherArray[i].accountid].createfollowup,
					partnerid : params.voucherArray[i].partnerid,
					partnerid_name : params.voucherArray[i].partnerid_name,
					debit : params.voucherArray[i].balance,
					againstreference : this.state.reversejournalVoucherTypes[params.voucherArray[i].vouchertype],
					xsalesinvoiceid : params.voucherArray[i].xsalesinvoiceid,
					xsalesinvoiceid_invoiceno : params.voucherArray[i].xsalesinvoiceid > 0 ? params.voucherArray[i].voucherno : null,
					xsalesinvoiceid_invoicedate : params.voucherArray[i].xsalesinvoiceid > 0 ? params.voucherArray[i].voucherdate : null,
					xpurchaseinvoiceid : params.voucherArray[i].xpurchaseinvoiceid,
					xpurchaseinvoiceid_partnerreference : params.voucherArray[i].xpurchaseinvoiceid > 0 ? params.voucherArray[i].voucherno : null,
					xpurchaseinvoiceid_supplierinvoicedate : params.voucherArray[i].xpurchaseinvoiceid > 0 ? params.voucherArray[i].supplierinvoicedate : null,
					xjournalvoucherid : params.voucherArray[i].xjournalvoucherid,
					xjournalvoucherid_voucherno : params.voucherArray[i].xjournalvoucherid > 0 ? params.voucherArray[i].voucherno : null,
					xjournalvoucherid_voucherdate : params.voucherArray[i].xjournalvoucherid > 0 ? params.voucherArray[i].voucherdate : null
				});
			}
		}
		if (params.param == "Unsettled Receipt Payment") {
			
			tempObj.currencyid = params.voucherArray[0].currencyid > 0 ? params.voucherArray[0].currencyid : tempObj.currencyid;

			for(var i = 0; i < params.voucherArray.length; i++) {
				tempObj.journalvoucheritems.push({
					accountid : params.voucherArray[i].accountid,
					accountid_name : accountObj[params.voucherArray[i].accountid].name,
					accountid_type : accountObj[params.voucherArray[i].accountid].type,
					accountid_accountgroup : accountObj[params.voucherArray[i].accountid].accountgroup,
					accountid_createfollowup : accountObj[params.voucherArray[i].accountid].createfollowup,
					followup : accountObj[params.voucherArray[i].accountid].createfollowup,
					partnerid : params.voucherArray[i].partnerid,
					partnerid_name : params.voucherArray[i].partnerid_name,
					employeeid : params.voucherArray[i].employeeid,
					employeeid_displayname : params.voucherArray[i].employeeid_displayname,
					debit : params.voucherArray[i].credit > 0 ? params.voucherArray[i].outstandingamount : null,
					credit : params.voucherArray[i].debit > 0 ? params.voucherArray[i].outstandingamount : null,
					againstreference : this.state.reversejournalVoucherTypes[params.voucherArray[i].vouchertype],
					xjournalvoucherid : ['Expense Request', 'Pay Roll'].indexOf(params.voucherArray[i].vouchertype) == -1 ? params.voucherArray[i].relatedid : null,
					xjournalvoucherid_voucherno : ['Expense Request', 'Pay Roll'].indexOf(params.voucherArray[i].vouchertype) == -1 ? params.voucherArray[i].voucherno : null,
					xjournalvoucherid_voucherdate : ['Expense Request', 'Pay Roll'].indexOf(params.voucherArray[i].vouchertype) == -1 ? params.voucherArray[i].voucherdate : null,
					xexpenserequestid: params.voucherArray[i].vouchertype == 'Expense Request' ? params.voucherArray[i].relatedid : null,
					xexpenserequestid_expenserequestno: params.voucherArray[i].vouchertype == 'Expense Request' ? params.voucherArray[i].voucherno : null,
					xexpenserequestid_expensedate : params.voucherArray[i].vouchertype == 'Expense Request' ? params.voucherArray[i].voucherdate : null,
					xpayrollid: params.voucherArray[i].vouchertype == 'Pay Roll' ? params.voucherArray[i].relatedid : null,
					xpayrollid_payrollno: params.voucherArray[i].vouchertype == 'Pay Roll' ? params.voucherArray[i].voucherno : null,
					xpayrollid_payrolldate : params.voucherArray[i].vouchertype == 'Pay Roll' ? params.voucherArray[i].voucherdate : null,
				});
			}
		}

		if (params.param == 'Payroll Batch') {
			tempObj.currencyid = params.voucherArray[0].currencyid > 0 ? params.voucherArray[0].currencyid : tempObj.currencyid;
			tempObj.description = params.voucherArray[0].remarks;
			tempObj.defaultcostcenter = params.voucherArray[0].defaultcostcenter;

			for(var i = 0; i < params.voucherArray.length; i++) {
				tempObj.journalvoucheritems.push({
					accountid : params.voucherArray[i].accountid,
					accountid_name : accountObj[params.voucherArray[i].accountid].name,
					accountid_type : accountObj[params.voucherArray[i].accountid].type,
					accountid_accountgroup : accountObj[params.voucherArray[i].accountid].accountgroup,
					accountid_createfollowup : accountObj[params.voucherArray[i].accountid].createfollowup,
					followup : accountObj[params.voucherArray[i].accountid].createfollowup,
					employeeid : params.voucherArray[i].employeeid ? params.voucherArray[i].employeeid : null,
					employeeid_displayname : params.voucherArray[i].employeeid ? params.voucherArray[i].employeeid_displayname : null,
					debit : params.voucherArray[i].debit > 0 ? params.voucherArray[i].debit : null,
					credit : params.voucherArray[i].credit > 0 ? params.voucherArray[i].credit : null,
					againstreference : this.state.reversejournalVoucherTypes[params.voucherArray[i].vouchertype],
					//remarks: params.voucherArray[i].remarks,
					instrumentno: params.voucherArray[i].instrumentno ? params.voucherArray[i].instrumentno : null,
					instrumentdate: params.voucherArray[i].instrumentdate ? params.voucherArray[i].instrumentdate : null,
					xpayrollid : params.voucherArray[i].payrollid ? params.voucherArray[i].payrollid : null,
					xpayrollid_payrollno : params.voucherArray[i].payrollid ? params.voucherArray[i].payrollno : null,
					xpayrollid_payrolldate : params.voucherArray[i].payrollid ? params.voucherArray[i].payrolldate : null,
					payrollbankid : params.voucherArray[i].payrollid ? params.voucherArray[i].payrollbankid : null
				});
			}
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.currencyOnChange();
	}, 0);
	
	this.updateLoaderFlag(false);
}

export function renderAgainstvoucher(item) {
	return (
		<div>
			<span>{item.xsalesinvoiceid > 0 ? (this.state.journalVoucherTypes[item.againstreference] + ' / ') : (item.xpurchaseinvoiceid > 0 ? (this.state.journalVoucherTypes[item.againstreference] + ' / ') : (item.xexpenserequestid > 0 ? (this.state.journalVoucherTypes[item.againstreference] + ' / ') : (item.xjournalvoucherid > 0 ? (this.state.journalVoucherTypes[item.againstreference] + ' / ') : (item.xpayrollid > 0 ? (this.state.journalVoucherTypes[item.againstreference] + ' / ') : ''))))}</span>
			<span>{item.xsalesinvoiceid > 0 ? (item.xsalesinvoiceid_invoiceno+' / ') : (item.xpurchaseinvoiceid > 0 ? (item.xpurchaseinvoiceid_partnerreference+' / ') : (item.xexpenserequestid > 0 ? (item.xexpenserequestid_expenserequestno+' / ') : (item.xjournalvoucherid > 0 ? (item.xjournalvoucherid_voucherno+' / ') : (item.xpayrollid > 0 ? (item.xpayrollid_payrollno+' / ') : ''))))}</span>
			<span>{item.xsalesinvoiceid > 0 ? (dateFilter(item.xsalesinvoiceid_invoicedate)) : (item.xpurchaseinvoiceid > 0 ? (dateFilter(item.xpurchaseinvoiceid_supplierinvoicedate)) : (item.xexpenserequestid > 0 ? (dateFilter(item.xexpenserequestid_expensedate)) : (item.xjournalvoucherid > 0 ? (dateFilter(item.xjournalvoucherid_voucherdate)) : (item.xpayrollid > 0 ? (dateFilter(item.xpayrollid_payrolldate)) : ''))))}</span>
		</div>
	);
}

export function renderItemDebit(item, itemstr, param) {
	/*checkAccountBalance({
		companyid: this.props.resource.companyid,
		accountid: item.accountid,
		show: 'both',
		againstid: item.xsalesinvoiceid > 0 ? item.xsalesinvoiceid : (item.xpurchaseinvoiceid>0 ? item.xpurchaseinvoiceid : (item.xexpenserequestid>0 ? item.xexpenserequestid : (item.xpayrollid>0 ? item.xpayrollid : item.xjournalvoucherid))),
		type: item.xsalesinvoiceid > 0 ? 'salesinvoices' : (item.xpurchaseinvoiceid>0 ? 'purchaseinvoices' : (item.xexpenserequestid>0 ? 'expenserequests' : (item.xpayrollid>0 ? 'payrolls' : 'journalvouchers'))),
		employeeid: item.employeeid,
		partnerid: item.partnerid,
	}, item, itemstr, this.selector, this.props);*/

	return {
		companyid: this.props.resource.companyid,
		accountid: item.accountid,
		show: 'both',
		againstid: item.xsalesinvoiceid > 0 ? item.xsalesinvoiceid : (item.xpurchaseinvoiceid>0 ? item.xpurchaseinvoiceid : (item.xexpenserequestid>0 ? item.xexpenserequestid : (item.xpayrollid>0 ? item.xpayrollid : item.xjournalvoucherid))),
		type: item.xsalesinvoiceid > 0 ? 'salesinvoices' : (item.xpurchaseinvoiceid>0 ? 'purchaseinvoices' : (item.xexpenserequestid>0 ? 'expenserequests' : (item.xpayrollid>0 ? 'payrolls' : 'journalvouchers'))),
		employeeid: item.employeeid,
		partnerid: item.partnerid,
	};
}

export function getItemById () {
	axios.get(`/api/journals/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;

			if(tempObj.vouchertype == 'Receipt' && this.props.match.path != '/details/receiptvouchers/:id')
				return this.props.history.replace(`/details/receiptvouchers/${tempObj.id}`);
			if(tempObj.vouchertype == 'Payment' && this.props.match.path != '/details/paymentvouchers/:id')
				return this.props.history.replace(`/details/paymentvouchers/${tempObj.id}`);
			if(tempObj.vouchertype == 'Credit Note' && this.props.match.path != '/details/creditnotes/:id')
				return this.props.history.replace(`/details/creditnotes/${tempObj.id}`);
			if(tempObj.vouchertype == 'Debit Note' && this.props.match.path != '/details/debitnotes/:id')
				return this.props.history.replace(`/details/debitnotes/${tempObj.id}`);
			if(tempObj.vouchertype == 'Journal Voucher' && this.props.match.path != '/details/journalvouchers/:id')
				return this.props.history.replace(`/details/journalvouchers/${tempObj.id}`);

			this.props.initialize(tempObj);

			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
	}
}

export function callBackAccount (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.accountid_name`] : valueobj.name,
		[`${itemstr}.accountid_type`] : valueobj.type,
		[`${itemstr}.accountid_accountgroup`] : valueobj.accountgroup,
		[`${itemstr}.accountid_createfollowup`] : valueobj.createfollowup,
		[`${itemstr}.followup`] : valueobj.createfollowup,
		[`${itemstr}.partnerid`] : null,
		[`${itemstr}.employeeid`] : null,
		[`${itemstr}.xsalesinvoiceid`] : null,
		[`${itemstr}.xpurchaseinvoiceid`] : null,
		[`${itemstr}.xjournalvoucherid`] : null,
		[`${itemstr}.xexpenserequestid`] : null,
		[`${itemstr}.xpayrollid`] : null,
		[`${itemstr}.againstreference`] : null,
	});
}

export function callBackSalesInvoice (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.xsalesinvoiceid_invoiceno`] : valueobj.invoiceno,
		[`${itemstr}.xsalesinvoiceid_invoicedate`] : valueobj.invoicedate,
	});
}

export function callBackPurchaseInvoice (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.xpurchaseinvoiceid_partnerreference`] : valueobj.partnerreference,
		[`${itemstr}.xpurchaseinvoiceid_supplierinvoicedate`] : valueobj.supplierinvoicedate,
	});
}

export function callBackjournal (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.xjournalvoucherid_voucherno`] : valueobj.voucherno,
		[`${itemstr}.xjournalvoucherid_voucherdate`] : valueobj.voucherdate,
	});
}

export function callBackExpenseRequest (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.xexpenserequestid_expenserequestno`] : valueobj.expenserequestno,
		[`${itemstr}.xexpenserequestid_expensedate`] : valueobj.expensedate,
	});
}

export function callBackPayRoll (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.xpayrollid_payrollno`] : valueobj.payrollno,
		[`${itemstr}.xpayrollid_payrolldate`] : valueobj.payrolldate,
	});
}

export function callBackItemEmployee (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.employeeid_displayname`] : valueobj.displayname,
		[`${itemstr}.employeeid_name`] : valueobj.name,
	});
}

export function callBackItemPartner (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.partnerid_name`] : valueobj.name
	});
}

export function againstreferenceOnChange (itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.xsalesinvoiceid`] : null,
		[`${itemstr}.xpurchaseinvoiceid`] : null,
		[`${itemstr}.xjournalvoucherid`] : null,
		[`${itemstr}.xexpenserequestid`] : null,
		[`${itemstr}.xpayrollid`] : null,
	});
}

export function getAdvanceandInvoices () {
	this.openModal({render: (closeModal) => {
		return <AdvancesinvoicesModal companyid={this.props.resource.companyid} partnerid={this.props.resource.partnerid} currencyid={this.props.resource.currencyid} partnerid_name={this.props.resource.partnerid_name} callback={(companyid, partnerid, array) => {
		let tempObj = {};
		tempObj.companyid = companyid;
		tempObj.partnerid = partnerid;
		tempObj.journalvoucheritems = array;
		this.props.updateFormState(this.props.form, tempObj);
}} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function getVoucherDetails (param) {
	let tempObj = {
		creditSum: 0,
		debitSum: 0,
		balanceSum: 0,
		balanceType: 'Cr'
	};

	for (var i = 0; i < this.props.resource.journalvoucheritems.length; i++) {
		tempObj.creditSum += (this.props.resource.journalvoucheritems[i].credit || 0);
		tempObj.debitSum += (this.props.resource.journalvoucheritems[i].debit || 0);
	}
	
	tempObj.creditSum = Number(tempObj.creditSum.toFixed(this.props.app.roundOffPrecision));
	tempObj.debitSum = Number(tempObj.debitSum.toFixed(this.props.app.roundOffPrecision));

	if (tempObj.creditSum > tempObj.debitSum) {
		tempObj.balanceSum = Number((tempObj.creditSum - tempObj.debitSum).toFixed(this.props.app.roundOffPrecision));
		tempObj.balanceType = 'Dr';
	} else {
		tempObj.balanceSum = Number((tempObj.debitSum - tempObj.creditSum).toFixed(this.props.app.roundOffPrecision));
		tempObj.balanceType = 'Cr';
	}
	return tempObj[param];
}

export function openCostCenterPickDetails (item, itemstr) {
	this.openModal({render: (closeModal) => {
		return <CostCenterPickingdetailsModal item={item} resource={this.props.resource} itemstr={itemstr} amountstr={`${item.debit ? 'debit' : 'credit'}`} app={this.props.app} form={this.props.form} updateFormState={this.props.updateFormState} openModal={this.openModal} closeModal={closeModal} />
	}, className: {
		content: 'react-modal-custom-class-60',
		overlay: 'react-modal-overlay-custom-class'
	}, confirmModal: true });
}

export function save (param) {
	this.updateLoaderFlag(true);
	let errorMessage = [];

	if (this.props.resource.journalvoucheritems.length <= 0) {
		errorMessage.push("Please enter at least one item");
	} else {
		let tempTotalDebit = 0, tempTotalCredit = 0;

		for (var i = 0; i < this.props.resource.journalvoucheritems.length; i++) {
			if ((this.props.resource.journalvoucheritems[i].debit && this.props.resource.journalvoucheritems[i].credit) || (!this.props.resource.journalvoucheritems[i].debit && !this.props.resource.journalvoucheritems[i].credit)) {
				errorMessage.push("Please enter debit or credit amount");
			} else {
				let amount = (this.props.resource.journalvoucheritems[i].debit) ? this.props.resource.journalvoucheritems[i].debit : this.props.resource.journalvoucheritems[i].credit;
				if (amount < 0) {
					errorMessage.push("Please enter debit or credit amount greater than zero");
				}
			}
			tempTotalDebit += (this.props.resource.journalvoucheritems[i].debit) ? this.props.resource.journalvoucheritems[i].debit : 0;
			tempTotalCredit += (this.props.resource.journalvoucheritems[i].credit) ? this.props.resource.journalvoucheritems[i].credit : 0;
		}

		if (Number(tempTotalDebit.toFixed(this.props.app.roundOffPrecision)) != Number(tempTotalDebit.toFixed(this.props.app.roundOffPrecision))) {
			errorMessage.push("For Journal voucher items total debit amount must be equal to total credit amount");
		}
	}
	if (errorMessage.length > 0) {
		let response = {
			data : {
				message : "Failure",
				error : errorMessage
			}
		}
		let apiResponse = commonMethods.apiResult(response)
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	} else {
		this.controller.saveJournalVoucher(param);
	}
}

export function saveJournalVoucher (param, confirm,confirm2) {
	this.updateLoaderFlag(true);

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm2 && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
		let message = {
			header : 'Cost Center Alert',
			body : `Cost Center not selected. Do you want to ${param} ?`,
			btnArray : ['Yes','No']
		};

		return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
			if(resparam)
				this.controller.saveJournalVoucher(param, confirm,true);
			else
				this.updateLoaderFlag(false);
		}));	
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/journals'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.saveJournalVoucher(param, true,confirm2);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/journalvouchers/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/journalvouchers");
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
