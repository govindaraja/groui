import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		let tempObj = {};
		if(this.props.location.params) {
			const params = this.props.location.params;
			utils.assign(tempObj, params, this.state.pagejson.controller.initializeArray);
		}
		this.props.initialize(tempObj);
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById () {
	axios.get(`/api/${this.state.pagejson.resourcename}/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == 'success') {
			this.props.initialize(response.data.main);
			if(this.state.pagejson.showndt) {
				this.setState({
					ndt: {
						notes: response.data.notes,
						documents: response.data.documents,
						tasks: response.data.tasks
					}
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions: confirm ? true : false
		},
		url : `/api/${this.state.pagejson.resourcename}`
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam) {
				if (this.props.isModal) {
					this.props.closeModal();
					this.props.callback(response.data.main);
				} else {
					this.props.history.replace(`/details/${this.state.pagejson.resourcename}/${response.data.main.id}`);
				}
			} else {
				if (param == 'Delete')
					this.props.history.replace(`${this.state.pagejson.backpage}`);
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	if (this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}
