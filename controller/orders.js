import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';

import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';
import { OrderPoQtyField, ItemRateField, ChildEditModal } from '../components/utilcomponents';
import AddonModal from '../components/details/addonitem';
import StockdetailsModal from '../containers/stockdetails';
import AnalyseRateModal from '../containers/analyseitemrate';
import ProfitanalyseModal from '../containers/profitabilityanalyseforquote';
import KititemaddModal from '../components/details/kititemaddmodal';
import QuoteitemdetailsModal from '../components/details/quoteitemdetailsmodal';
import ComparequoteorderdetailsModal from '../components/details/comparequoteorderdetailsmodal';
import EmailModal from '../components/details/emailmodal';
import { RenderItemQty } from '../utils/customfieldtypes';
import ProductionItemPickModal from '../components/details/productionitempickmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.checkWarehouse();
	else
		this.controller.getItemById();
}

export function checkWarehouse () {
	return axios.get(`/api/stocklocations?&field=id,name&filtercondition=stocklocations.parentid is null`).then((response) => {
		if (response.data.message == 'success') {
			if(response.data.main.length == 1)
				this.controller.initializeState(response.data.main[0].id);
			else
				this.controller.initializeState();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function initializeState(warehouseid) {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		orderdate: new Date(new Date().setHours(0, 0, 0, 0)),
		orderitems: [],
		kititemorderdetails: [],
		additionalcharges: []
	};

	for (var i = 0; i < this.props.app.appSettings.length; i++) {
		if (this.props.app.appSettings[i].module == 'Sales' && this.props.app.appSettings[i].name == 'Default Delivery Policy') {
			tempObj.deliverypolicy = this.props.app.appSettings[i].value.value;
		}
	}

	if(this.props.location.params) {
		let params = this.props.location.params;
		let orderitemArray = [], additionalchargesArray = [], kititemquoteArray = [];

		if (params.param == 'copy') {
			tempObj = params;
		}
		if (params.param == 'Leads') {
			tempObj.leadid = params.id;
			tempObj.contactperson = params.contactid_name;
			orderitemArray = params.leaditems;
			this.customFieldsOperation('leads', tempObj, params, 'orders');
		}
		if (params.param == 'Quotes') {
			tempObj.quoteno = params.id;
			tempObj.leadid = params.leadid;
			tempObj.tandc = params.tandc;
			tempObj.contactperson = params.contactperson;
			orderitemArray = params.quoteitems;
			orderitemArray.forEach((a) => {
				a.quoteid = a.parentid;
			});
			kititemquoteArray = params.kititemquotedetails;
			additionalchargesArray = params.additionalcharges;
			this.customFieldsOperation('quotes', tempObj, params, 'orders');
		}

		if (params.param == 'Leads' || params.param == 'Quotes') {
			utils.assign(tempObj, params, ['contactid', 'companyid', 'currencyid', 'currencyexchangerate', 'email', 'mobile', 'phone', 'deliverydate', 'customerreference', 'deliveryaddress', 'deliveryaddressid', 'paymentterms', 'modeofdespatch', 'freight', 'billingaddress', 'billingaddressid', 'salesperson', 'territoryid', 'taxid', 'roundoffmethod', 'remarks']);

			if(params.customerid > 0) {
				utils.assign(tempObj, params, ['customerid', 'pricelistid', 'creditperiod']);
				tempObj.paymentterms = tempObj.paymentterms ? tempObj.paymentterms : params.customerid_paymentterms;
				tempObj.modeofdespatch = tempObj.modeofdespatch ? tempObj.modeofdespatch : params.customerid_modeofdespatch;
				tempObj.freight = tempObj.freight ? tempObj.freight : params.customerid_freight;
			}

			if(warehouseid > 0)
				tempObj.warehouseid = warehouseid;

			for (var i = 0; i < orderitemArray.length; i++) {
				let tempChiObj = {
					index: i+1,
					alternateuom: orderitemArray[i].uomconversiontype ? true : false,
					deliverydate: orderitemArray[i].deliverydate ? orderitemArray[i].deliverydate : tempObj.deliverydate,
					quoteitemid: tempObj.quoteno > 0 ? orderitemArray[i].id : null
				};
				utils.assign(tempChiObj, orderitemArray[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'itemid_addonhelptext', 'description', 'baseitemrate', 'addonrate', 'rate', 'quantity', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'itemid_issaleskit', 'discountmode',  'drawingno', 'discountquantity', 'itemid_keepstock', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'quoteid', {'quote_index' : 'index'}, 'remarks', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingquantity', 'billingconversiontype', 'billingconversionfactor', 'itemid_itemcategorymasterid']);

				if(warehouseid > 0)
					tempChiObj.warehouseid = warehouseid;

				this.customFieldsOperation('quoteitems', tempChiObj, orderitemArray[i], 'orderitems');
				tempObj.orderitems.push(tempChiObj);
			}

			for (var i = 0; i < tempObj.orderitems.length; i++) {
				for (var j = 0; j < kititemquoteArray.length; j++) {
					if(tempObj.orderitems[i].quoteid == kititemquoteArray[j].parentid && tempObj.orderitems[i].quote_index == kititemquoteArray[j].rootindex) {
						let tempChiObj = {
							rootindex: tempObj.orderitems[i].index,
							warehouseid: tempObj.orderitems[i].warehouseid
						};
						utils.assign(tempChiObj, kititemquoteArray[j], ['itemid', 'itemid_name', 'parent', 'uomid', 'uomid_name', 'quantity', 'conversion', 'rate', 'ratelc', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'index', 'parentindex', 'addondisplaygroup', 'addondisplayorder']);
						tempObj.kititemorderdetails.push(tempChiObj);
					}
				}
			}

			for (var j = 0; j < additionalchargesArray.length; j++) {
				let tempChiObj = {};
				utils.assign(tempChiObj, additionalchargesArray[j], ['additionalchargesid', 'additionalchargesid_name', 'description', 'amount', 'taxcodeid', 'defaultpercentage', 'displayorder']);
				tempObj.additionalcharges.push(tempChiObj);
			}
		}
	}
	this.props.initialize(tempObj);
	this.controller.computeFinalRate();

	setTimeout(() => {
		if(this.props.location.params && this.props.location.params.param == 'Leads')
			this.controller.onLoadKit(0, []);

		if(!this.props.location.params || this.props.location.params.param != 'Quotes')
			this.controller.getTandC();

		this.controller.currencyOnChange();
	}, 0);
	this.updateLoaderFlag(false);
}

export function renderTableQtyfield(item) {
	return (
		<RenderItemQty status={this.props.resource.status} resource={this.props.resource} item={item} uomObj={this.props.app.uomObj} statusArray={['Approved','Hold']}/>
	);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function getItemById () {
	axios.get(`/api/orders/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.orderitems.forEach((item)=> {
				if (item.uomconversiontype)
					item.alternateuom = true;
			});
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			this.controller.checkVisibilityForButton();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function onLoadKit (count, kititemorderdetails) {
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;
	if(count < this.props.resource.orderitems.length) {
		if(this.props.resource.orderitems[count].itemid_issaleskit) {
			let promise1 = commonMethods.getItemDetails(this.props.resource.orderitems[count].itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales');
			promise1.then((returnObject) => {
				returnObject.kititems.forEach((kititem) => {
					kititem.rootindex = this.props.resource.orderitems[count].index;
					if(!kititem.parentindex) {
						let tempQuantity = Number((kititem.conversion * this.props.resource.orderitems[count].quantity).toFixed(roundOffPrecisionStock));
						calculateQuantity(kititem.index, tempQuantity);
						kititem.quantity = tempQuantity;
					}
				});
				function calculateQuantity(tempIndex, tempQuantity) {
					returnObject.kititems.forEach((kititem) => {
						if (kititem.parentindex == tempIndex) {
							let sectempQty = Number((kititem.conversion * tempQuantity).toFixed(roundOffPrecisionStock));
							kititem.quantity = sectempQty;
							calculateQuantity(kititem.index, sectempQty);
						}
					});
				}
				returnObject.kititems.forEach((kititem) => {
					kititemorderdetails.push(kititem);
				});
				this.controller.onLoadKit(count+1, kititemorderdetails);
			}, (reason) => {});
		} else {
			this.controller.onLoadKit(count+1, kititemorderdetails);
		}
	} else {
		this.props.updateFormState(this.props.form, {
			kititemorderdetails
		});
		this.controller.computeFinalRate();
	}
}

export function checkVisibilityForButton() {
	setTimeout(() => {
		let showDeliveryBtn = false;
		let showInvoiceBtn = false;
		for(var i=0; i<this.props.resource.orderitems.length; i++) {
			let qty = Number(this.props.resource.orderitems[i].quantity.toFixed(this.props.app.roundOffPrecisionStock));
			let deliveredqty = Number((this.props.resource.orderitems[i].deliveredqty || 0).toFixed(this.props.app.roundOffPrecisionStock));
			let invoicedqty = Number((this.props.resource.orderitems[i].invoicedqty || 0).toFixed(this.props.app.roundOffPrecisionStock));
			if(qty > deliveredqty)
				showDeliveryBtn = true;
			if(qty > invoicedqty)
				showInvoiceBtn = true;
		}
		this.setState({
			showDeliveryBtn,
			showInvoiceBtn
		});
	}, 0);
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Order' and termsandconditions.isdefault`).then((response) => {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function callBackConsigneeName(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		consigneeid_name : valueobj.name,
		deliveryaddress : null,
		deliveryaddressid : null,
		deliverycontactid: null,
		deliverycontactperson: null,
		deliverycontactmobile: null,
		deliverycontactphone: null,
		deliverycontactemail: null
	});
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function deliverydateonchange() {
	let tempObj = {};
	for (var i = 0; i < this.props.resource.orderitems.length; i++) {
		if (!this.props.resource.orderitems[i].deliverydate || this.props.resource.orderitems[i].deliverydate == null)
			tempObj[`orderitems[${i}].deliverydate`] = this.props.resource.deliverydate;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function warehouseonchange() {
	let tempObj = {};
	for (var i = 0; i < this.props.resource.orderitems.length; i++) {
		if (!this.props.resource.orderitems[i].warehouseid || this.props.resource.orderitems[i].warehouseid == null)
			tempObj[`orderitems[${i}].warehouseid`] = this.props.resource.warehouseid;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function currencyOnChange () {
	if (this.props.resource.pricelistid) {
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid) {
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});
		}
	}
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function quantityOnChange(itemqty, item, itemstr) {
	let kititemorderdetails = this.props.resource.kititemorderdetails;
	let tempObj = {};
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;

	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
	}

	for (var i = 0; i < kititemorderdetails.length; i++) {
		if (item.index == kititemorderdetails[i].rootindex) {
			if (!kititemorderdetails[i].parentindex) {
				let tempIndex = kititemorderdetails[i].index;
				let tempQuantity = Number((kititemorderdetails[i].conversion * itemqty).toFixed(roundOffPrecisionStock));
				tempObj[`kititemorderdetails[${i}].quantity`] = tempQuantity;
				calculateQuantity(tempIndex, tempQuantity, item.index);
			}
		}
	}

	function calculateQuantity(tempIndex, tempQuantity, index) {
		for (var j = 0; j < kititemorderdetails.length; j++) {
			if (tempIndex == kititemorderdetails[j].parentindex && index == kititemorderdetails[j].rootindex) {
				let sectempQty = Number((kititemorderdetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
				tempObj[`kititemorderdetails[${j}].quantity`] = sectempQty;
				calculateQuantity(kititemorderdetails[j].index, sectempQty, index);
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function deleteOrderItem (index) {
	let itemCount = 0;
	let kititemorderdetails = [...this.props.resource.kititemorderdetails];
	let orderitems = [...this.props.resource.orderitems];

	for (var i = 0; i < kititemorderdetails.length; i++) {
		if (kititemorderdetails[i].rootindex == this.props.resource.orderitems[index].index)
			itemCount++;
	}
	for (var i = 0; i < itemCount; i++) {
		for (var j = 0; j < kititemorderdetails.length; j++) {
			if (kititemorderdetails[j].rootindex == this.props.resource.orderitems[index].index) {
				kititemorderdetails.splice(j, 1);
				break;
			}
		}
	}
	orderitems.splice(index, 1);

	this.props.updateFormState(this.props.form, {
		kititemorderdetails,
		orderitems
	});
	this.controller.computeFinalRate();
}

export function deleteKitItem(index) {
	let errorArray = [];
	let itemfound = false;
	let kititemorderdetails = [...this.props.resource.kititemorderdetails];

	for (var i = 0; i < kititemorderdetails.length; i++) {
		if (index != i && kititemorderdetails[index].rootindex == kititemorderdetails[i].rootindex && kititemorderdetails[index].parentindex == kititemorderdetails[i].parentindex) {
			itemfound = true;
		}
	}
	if (!itemfound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	let itemCouFound = false;
	for (var i = 0; i < kititemorderdetails.length; i++) {
		if (index != i && kititemorderdetails[index].rootindex == kititemorderdetails[i].rootindex && !kititemorderdetails[i].parentindex)
			itemCouFound = true;
	}
	if (!itemCouFound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	if (errorArray.length == 0) {
		let parentIndexArray = [kititemorderdetails[index].index];
		let rootindex = kititemorderdetails[index].rootindex;
		deleteArray(kititemorderdetails[index].rootindex, kititemorderdetails[index].index);

		function deleteArray(rootindex, index) {
			for (var i = 0; i < kititemorderdetails.length; i++) {
				if (kititemorderdetails[i].rootindex == rootindex && kititemorderdetails[i].parentindex == index) {
					parentIndexArray.push(kititemorderdetails[i].index)
					deleteArray(rootindex, kititemorderdetails[i].index);
				}
			}
		}

		for (var i = 0; i < parentIndexArray.length; i++) {
			for (var j = 0; j < kititemorderdetails.length; j++) {
				if (kititemorderdetails[j].rootindex == rootindex && kititemorderdetails[j].index == parentIndexArray[i]) {
					kititemorderdetails.splice(j, 1);
					break;
				}
			}
		}

		this.props.updateFormState(this.props.form, {kititemorderdetails});
	} else {
		let response = {
			data : {
				message : 'failure',
				error : utils.removeDuplicate(errorArray)
			}
		};
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
}

export function tandcOnchange(id, valueobj) {
	if(id) {
		this.props.updateFormState(this.props.form, {
			tandc : valueobj.terms
		});
	}
}

export function callBackCustomerName (id, valueobj) {
	let tempObj = {
		customerid_name: valueobj.name,
		paymentterms: this.props.resource.paymentterms ? this.props.resource.paymentterms : valueobj.paymentterms,
		modeofdespatch: this.props.resource.modeofdespatch ? this.props.resource.modeofdespatch : valueobj.modeofdispatch,
		freight: this.props.resource.freight ? this.props.resource.freight : valueobj.freight,
		territoryid: valueobj.territory,
		salesperson: this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		contactid: valueobj.contactid,
		email: valueobj.contactid_email,
		phone: valueobj.contactid_phone,
		mobile: valueobj.contactid_mobile,
		contactperson: valueobj.contactid_name,
		contactid_name: valueobj.contactid_name,
		contactid_email: valueobj.contactid_email,
		contactid_phone: valueobj.contactid_phone,
		contactid_mobile: valueobj.contactid_mobile,
		deliveryaddress: valueobj.addressid_displayaddress,
		billingaddress: valueobj.addressid_displayaddress,
		billingaddressid: valueobj.addressid,
		deliveryaddressid: valueobj.addressid,
		deliverycontactid: valueobj.contactid,
		deliverycontactperson: valueobj.contactid_name,
		deliverycontactphone: valueobj.contactid_phone,
		deliverycontactemail: valueobj.contactid_email,
		deliverycontactmobile: valueobj.contactid_mobile,
		deliverycontactid_name: valueobj.contactid_name,
		deliverycontactid_phone: valueobj.contactid_phone,
		deliverycontactid_email: valueobj.contactid_email,
		deliverycontactid_mobile: valueobj.contactid_mobile
	};

	if (valueobj.salespricelistid) {
		tempObj.pricelistid = valueobj.salespricelistid;
	} else if (valueobj.partnergroupid_pricelistid) {
		tempObj.pricelistid = valueobj.partnergroupid_pricelistid;
	} else {
		for (var i = 0; i < this.props.app.appSettings.length; i++) {
			if (this.props.app.appSettings[i].module == 'Sales' && this.props.app.appSettings[i].name == 'Pricelistid') {
				tempObj.pricelistid = this.props.app.appSettings[i].value['value'];
				break;
			}
		}
	}
	if (valueobj.salescreditperiod)
		tempObj.creditperiod = valueobj.salescreditperiod;
	else
		tempObj.creditperiod = valueobj.partnergroupid_creditperiod;

	this.customFieldsOperation('partners', tempObj, valueobj, 'orders');

	this.props.updateFormState(this.props.form, tempObj);
}

export function contactpersonChange (id, valueobj, type) {
	let tempObj = {};
	if(type == 'billing') {
		tempObj = {
			contactperson: valueobj.name,
			phone: valueobj.phone,
			email: valueobj.email,
			mobile: valueobj.mobile,
			contactid_name: valueobj.name,
			contactid_phone: valueobj.phone,
			contactid_email: valueobj.email,
			contactid_mobile: valueobj.mobile
		};
	} else {
		tempObj = {
			deliverycontactperson: valueobj.name,
			deliverycontactphone: valueobj.phone,
			deliverycontactemail: valueobj.email,
			deliverycontactmobile: valueobj.mobile,
			deliverycontactid_name: valueobj.name,
			deliverycontactid_phone: valueobj.phone,
			deliverycontactid_email: valueobj.email,
			deliverycontactid_mobile: valueobj.mobile
		};
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = valueobj.parentid;
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.drawingno`] = returnObject.drawingno;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;
		tempObj[`${itemstr}.itemid_addonhelptext`] = returnObject.addonhelptext;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.itemid_recommendedsellingprice`] = returnObject.itemid_recommendedsellingprice;
		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'orderitems', itemstr);

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		if (item.index) {
			let itemCount = 0;
			tempObj.kititemorderdetails = [...this.props.resource.kititemorderdetails];
			for (var i = 0; i < tempObj.kititemorderdetails.length; i++) {
				if (tempObj.kititemorderdetails[i].rootindex == item.index)
					itemCount++;
			}
			for (var i = 0; i < itemCount; i++) {
				for (var j = 0; j < tempObj.kititemorderdetails.length; j++) {
					if (tempObj.kititemorderdetails[j].rootindex == item.index) {
						tempObj.kititemorderdetails.splice(j, 1);
						break;
					}
				}
			}

			if (returnObject.itemid_issaleskit) {
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = item.index;
					tempObj.kititemorderdetails.push(returnObject.kititems[i]);
				}
			}
		} else {
			let index = 0;
			for (var i = 0; i < this.props.resource.orderitems.length; i++) {
				if (this.props.resource.orderitems[i].index > index)
					index = this.props.resource.orderitems[i].index;
			}
			tempObj[`${itemstr}.index`] = index + 1;

			if (returnObject.itemid_issaleskit) {
				tempObj.kititemorderdetails = [...this.props.resource.kititemorderdetails];
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = tempObj[`${itemstr}.index`];
					tempObj.kititemorderdetails.push(returnObject.kititems[i]);
				}
			}
		}

		if(returnObject.hasaddons) {
			tempObj[`${itemstr}.baseitemrate`] = tempObj[`${itemstr}.rate`];
			tempObj[`${itemstr}.addonrate`] = 0;
		}

		this.props.updateFormState(this.props.form, tempObj);
		var tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
		if(returnObject.hasaddons) {
			this.controller.openItemAddOn(this.selector(this.props.fullstate, itemstr).index, itemstr);
		}
	}, (reason) => {});
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function computeFinalRate() {
	setTimeout(() =>{
		let tempObj = {};
		for (var i = 0; i < this.props.resource.orderitems.length; i++) {
			if (this.props.resource.orderitems[i].itemid_hasaddons) {
				tempObj[`orderitems[${i}].rate`] = (this.props.resource.orderitems[i].baseitemrate ? this.props.resource.orderitems[i].baseitemrate : 0) + ((this.props.resource.orderitems[i].addonrate ? this.props.resource.orderitems[i].addonrate : 0));
			}
		}
		this.props.updateFormState(this.props.form, tempObj);
		taxEngine(this.props, 'resource', 'orderitems');
	}, 0);
}

export function quoteOnChange(id, valueobj) {
	axios.get(`/api/quotes/${id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = {
				orderitems: [],
				kititemorderdetails: []
			};
			utils.assign(tempObj, response.data.main, ['customerreference', 'paymentterms', 'modeofdespatch', 'freight']);

			this.customFieldsOperation('quotes', tempObj, response.data.main, 'orders');

			for (var i = 0; i < response.data.main.quoteitems.length; i++) {
				let tempChiObj = {
					index: i+1,
					alternateuom: response.data.main.quoteitems[i].uomconversiontype ? true : false,
					deliverydate: response.data.main.quoteitems[i].deliverydate ? response.data.main.quoteitems[i].deliverydate : this.props.resource.deliverydate,
					quoteid: response.data.main.id,
					quoteitemid: response.data.main.quoteitems[i].id
				};
				utils.assign(tempChiObj, response.data.main.quoteitems[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'itemid_addonhelptext', 'description', 'baseitemrate', 'addonrate', 'rate', 'quantity', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'itemid_issaleskit', 'discountmode', 'itemname', 'drawingno', 'discountquantity', 'itemid_keepstock', 'itemid_quantitytype', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', {'quote_index' : 'index'}, 'remarks', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingquantity', 'billingconversiontype', 'billingconversionfactor', 'itemid_itemcategorymasterid']);

				tempChiObj['warehouseid'] = this.props.resource.warehouseid;

				this.customFieldsOperation('quoteitems', tempChiObj, response.data.main.quoteitems[i], 'orderitems');
				for (var j = 0; j < response.data.main.kititemquotedetails.length; j++) {
					if(response.data.main.quoteitems[i].index == response.data.main.kititemquotedetails[j].rootindex) {
						let tempKitChiObj = {
							rootindex: tempChiObj.index,
							warehouseid: tempChiObj.warehouseid
						};
						utils.assign(tempKitChiObj, response.data.main.kititemquotedetails[j], ['itemid', 'itemid_name', 'parent', 'uomid', 'uomid_name', 'quantity', 'conversion', 'rate', 'ratelc', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'index', 'parentindex', 'addondisplaygroup', 'addondisplayorder']);
						this.customFieldsOperation('kititemquotedetails', tempKitChiObj, response.data.main.kititemquotedetails[j], 'kititemorderdetails');
						tempObj.kititemorderdetails.push(tempKitChiObj);
					}
				}
				tempObj.orderitems.push(tempChiObj);
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}
	});
}

export function conversionOnChange (item) {
	for (var i = 0; i < this.props.resource.orderitems.length; i++) {
		if (item.rootindex == this.props.resource.orderitems[i].index) {
			this.controller.quantityOnChange(this.props.resource.orderitems[i].quantity, this.props.resource.orderitems[i], `orderitems[${i}]`);
			break;
		}
	}
}

export function openItemAddOn(index, itemstr) {
	let getBody = (closeModal) => {
		return (
			<AddonModal />
		)
	}
	this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} child={'orderitems'} kit={'kititemorderdetails'} index={index} itemstr={itemstr} array={this.props.array} updateFormState={this.props.updateFormState} resource={this.props.resource} app={this.props.app} callback={this.controller.itemaddoncallback} openStockDetails={this.controller.openStockDetails} computeFinalRate={this.controller.computeFinalRate} checkCondition={this.checkCondition} closeModal={closeModal} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
}

export function itemaddoncallback () {
	for (var i = 0; i < this.props.resource.orderitems.length; i++)
		this.controller.quantityOnChange(this.props.resource.orderitems[i].quantity, this.props.resource.orderitems[i], `orderitems[${i}]`);
}

export function openStockDetails (item) {
	this.props.openModal({render: (closeModal) => {return <StockdetailsModal resource = {this.props.resource} item={item} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function analyseitemRate (item) {
	this.props.openModal({render: (closeModal) => {return <AnalyseRateModal item={item} param="orders" customerid={this.props.resource.customerid} customerid_name={this.props.resource.customerid_name} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function getQuoteItemDetails () {
	this.props.openModal({render: (closeModal) => {return <QuoteitemdetailsModal resource={this.props.resource} array={this.props.array} callback={this.controller.quoteitemmodalcallback} customFieldsOperation={this.customFieldsOperation} updateFormState={this.props.updateFormState} form={this.props.form} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function quoteitemmodalcallback() {
	for (var k = 0; k < this.props.resource.kititemorderdetails.length; k++) {
		this.controller.quantityOnChange(this.props.resource.kititemorderdetails[k].quantity, this.props.resource.kititemorderdetails[k]);
	}
	this.controller.computeFinalRate();
}

export function addKitItem() {
	this.openModal({
		render: (closeModal) => {
			return <KititemaddModal resource = {this.props.resource} child={'orderitems'} kit={'kititemorderdetails'} array={this.props.array} callback={()=>{
				for (var i = 0; i < this.props.resource.orderitems.length; i++) {
					this.controller.quantityOnChange(this.props.resource.orderitems[i].quantity, this.props.resource.orderitems[i], `orderitems[${i}]`);
				}
			}} closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}


export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send Acknowledgement',
			data : emailObj
		},
		url : '/api/orders'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						acknowledgementsent: response.data.main.acknowledgementsent,
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					acknowledgementsent: response.data.main.acknowledgementsent,
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function openemail() {
	let emailobj = {
		firsttime: true,
		sendemail: true
	};
	this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'orders'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
}


export function save (param, confirm) {
	if (param == 'Send Acknowledgement') {
		return this.props.resource.acknowledgementsent ?  this.controller.openAcknowledgeAlert() : this.controller.openemail();
	}

	this.updateLoaderFlag(true);

	if(param != 'Update' && param != 'Hold' && param != 'Unhold' && param != 'Cancel' && param != 'Delete' && param !='Amend' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/orders'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace("/details/orders/" + response.data.main.id);
			} else if (param == 'Delete') {
				this.props.history.replace("/list/orders");
			} else {
				this.props.initialize(response.data.main);
				this.controller.checkVisibilityForButton();
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function openAcknowledgeAlert() {
	let message = {
		header : 'Acknowledgement Alert',
		body : 'Acknowledgement already sent. Do you want to send again?',
		btnArray : ['Yes','No']
	};
	this.props.openModal(modalService['confirmMethod'](message, (param) => {
		if(param) {
			this.controller.openemail();
		}
	}));
}

export function createDeliveryNote() {
	this.updateLoaderFlag(true);
	checkTransactionExist('deliverynotes', 'orders', this.props.resource.id, this.openModal, (param)=> {
		if(param) {
			let tempObj = this.props.resource;
			for (var i = 0; i < tempObj.orderitems.length; i++)
				tempObj.orderitems[i].sourceresource = 'orderitems';

			if (tempObj.deliverypolicy == 'After Order')
				tempObj.param = "Sales Orders for After Order";
			if (tempObj.deliverypolicy == 'On Demand')
				tempObj.param = "Sales Orders for On Demand";

			this.props.history.push({pathname: '/createDeliveryNote', params: tempObj});
		}
		this.updateLoaderFlag(false);
	});
}

export function createSalesInvoice() {
	this.updateLoaderFlag(true);
	checkTransactionExist('salesinvoices', 'orders', this.props.resource.id, this.openModal, (param) => {
		if(param) {
			var tempObj = this.props.resource;
			for (var i = 0; i < tempObj.orderitems.length; i++)
				tempObj.orderitems[i].sourceresource = 'orderitems';

			tempObj.param = "Sales Orders";
			tempObj.orderid_ponumber = tempObj.ponumber;
			this.props.history.push({pathname: '/createSalesInvoice', params: tempObj});
		}
		this.updateLoaderFlag(false);
	});
}

export function createProformaInvoice() {
	this.updateLoaderFlag(true);
	checkTransactionExist('proformainvoices', 'orders', this.props.resource.id, this.openModal, (param) => {
		if(param) {
			var tempObj = this.props.resource;
			for (var i = 0; i < tempObj.orderitems.length; i++)
				tempObj.orderitems[i].sourceresource = 'orderitems';

			tempObj.param = "Sales Orders";
			tempObj.orderid_ponumber = tempObj.ponumber;
			this.props.history.push({pathname: '/createProformaInvoice', params: tempObj});
		}
		this.updateLoaderFlag(false);
	});
}

export function createReceipt() {
	this.props.history.push({pathname: '/createReceiptVoucher', params: {...this.props.resource, param: 'Sales Orders'}});
}

export function createWorkOrder() {
	this.props.history.push({pathname: '/createWorkOrder', params: {
		param : 'Sales Orders',
		orderid : this.props.resource.id,
		currencyid : this.props.resource.currencyid,
		currencyexchangerate : this.props.resource.currencyexchangerate
	}});
}

export function createProductionOrder() {
	this.openModal({
		render: (closeModal) => {
			return <ProductionItemPickModal updateLoaderFlag={this.updateLoaderFlag} resource = {this.props.resource} app={this.props.app} openModal={this.openModal} history={this.props.history} closeModal={closeModal} productionCallback={this.controller.productionCallback}/>
		}, className: {
			content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function productionCallback(orderitems) {
	this.updateLoaderFlag(true);
	checkTransactionExist('productionorders', 'orders', this.props.resource.id, this.openModal, (param) => {
		if(param) {
			this.props.history.push({pathname: '/createProductionOrder', params: {...this.props.resource,orderitems:orderitems, param: 'Sales Orders'}});
		}
		this.updateLoaderFlag(false);
	});
}

export function createPurchaseOrder () {
	let purchaseorderneeded = false;
	this.props.resource.orderitems.forEach((orderitem) => {
		if(orderitem.itemid_allowpurchase && orderitem.itemid_itemtype == 'Product')
			purchaseorderneeded = true;
		if(orderitem.itemid_hasaddons)
			purchaseorderneeded = true;
	});
	/*let tempArray = [], tempKititemArray = [];
	for(var k = 0; k < this.props.resource.orderitems.length; k++) {
		if(this.props.resource.orderitems[k].itemid_itemtype == 'Product' && this.props.resource.orderitems[k].itemid_allowpurchase) {
			let attributeArray = ['itemid', 'uomid', 'itemid_name', 'itemid_keepstock', 'uomid_name', 'description', 'quantity', 'itemid_uomgroupid', 'warehouseid', 'uomconversiontype', 'uomconversionfactor', 'alternateuom', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingquantity', 'billingconversiontype', 'billingconversionfactor', 'itemid_purchaseuomid'];
			let tempObj = {
				companyid : this.props.resource.companyid,
				orderid : this.props.resource.id,
				currencyid : this.props.resource.currencyid,
				currencyexchangerate : this.props.resource.currencyexchangerate,
				order_index : this.props.resource.orderitems[k].index
			};
			this.customFieldsOperation('orders', tempObj, this.props.resource, 'purchaseorders');
			for (var m = 0; m < attributeArray.length; m++)
				tempObj[attributeArray[m]] = this.props.resource.orderitems[k][attributeArray[m]];

			this.customFieldsOperation('orderitems', tempObj, this.props.resource.orderitems[k], 'purchaseorderitems');

			tempArray.push(tempObj);
		}
	}
	for(var k = 0; k < this.props.resource.kititemorderdetails.length; k++) {
		if(this.props.resource.kititemorderdetails[k].itemid_itemtype == 'Product' && this.props.resource.kititemorderdetails[k].itemid_allowpurchase) {
			let tempObj = {
				companyid : this.props.resource.companyid,
				orderid : this.props.resource.id,
				currencyid : this.props.resource.currencyid,
				currencyexchangerate : this.props.resource.currencyexchangerate,
				...this.props.resource.kititemorderdetails[k]
			};
			this.customFieldsOperation('orders', tempObj, this.props.resource, 'purchaseorders');

			this.customFieldsOperation('orderitems', tempObj, this.props.resource.kititemorderdetails[k], 'purchaseorderitems');

			tempKititemArray.push(tempObj);
		}
	}*/
	if(purchaseorderneeded) {
		this.updateLoaderFlag(true);
		checkTransactionExist('purchaseorders', 'orders', this.props.resource.id, this.openModal, (param) => {
			if(param) {
				let tempObj = {
					...this.props.resource
					/*deliveryaddress: this.props.resource.deliveryaddress,
					deliveryaddressid: this.props.resource.deliveryaddressid,
					...tempArray[0],
					purchaseItemArray : tempArray,
					purchaseKitItemArray : tempKititemArray*/
				};

				this.props.history.push({pathname: '/createPurchaseOrder', params: {...tempObj, param: 'Sales Order'}});
			}
			this.updateLoaderFlag(false);
		});
	} else {
		this.props.openModal(modalService.infoMethod({
			header : 'Warning',
			body : "Purchase Order is not needed for this Sales Order",
			btnArray : ['Ok']
		}));
	}
}

export function profitanalysedetails() {
	this.openModal({render: (closeModal) => {
		return <ProfitanalyseModal resource = {this.props.resource} param="orders" app={this.props.app} closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class-60',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function compareQuoteOrderDetails () {
	this.openModal({render: (closeModal) => {
		return <ComparequoteorderdetailsModal resource = {this.props.resource} openModal={this.props.openModal} closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function cancel () {
	this.props.history.goBack();
}
