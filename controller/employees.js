import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	this.props.initialize({});
	this.updateLoaderFlag(false);
}

export function displaynameFoucsfn() {
	if(!this.props.resource.displayname) {
		let tempObj = {};
		tempObj.displayname = this.props.resource.firstname + ' ';

		if(this.props.resource.middlename)
			tempObj.displayname += '' + this.props.resource.middlename + ' ';

		tempObj.displayname += this.props.resource.lastname;

		this.props.updateFormState(this.props.form, tempObj);
	}
}

export function getItemById () {
	axios.get(`/api/employees/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			this.setState({
				ndt : {
					notes : response.data.notes,
					documents : response.data.documents,
					tasks : response.data.tasks
				}
			});

			if(tempObj.workstarttime && tempObj.workendtime) {
				tempObj.workstarttime = new Date(tempObj.workstarttime);
				tempObj.workendtime = new Date(tempObj.workendtime);
			}

			this.props.initialize(tempObj);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function trackOnChange () {
	let tempObj = {};

	if (this.props.resource.trackemployee)
		tempObj.trackingpasscode = Math.floor((Math.random() * 1000000) + 1).toString();
	else
		tempObj.trackingpasscode = null;

	this.props.updateFormState(this.props.form, tempObj);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/employees'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/employees/${response.data.main.id}`);
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/employees");
				} else {
					let tempObj = response.data.main;
					if(tempObj.workstarttime && tempObj.workendtime) {
						tempObj.workstarttime = new Date(tempObj.workstarttime);
						tempObj.workendtime = new Date(tempObj.workendtime);
					}
					this.props.initialize(tempObj);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function uploadImageFile (file) {
	this.updateLoaderFlag(true);
	let tempResourceobj = this.props.resource;
	tempResourceobj.parentresource = "employees";
	tempResourceobj.resourceName = "employees";
	tempResourceobj.parentid = tempResourceobj.id;
	tempResourceobj.imagename = "";

	let tempData = {
		actionverb : 'Save',
		data : tempResourceobj
	};

	const formData = new FormData();
	formData.append('file', file);
	formData.append('data', JSON.stringify(tempResourceobj));
	formData.append('actionverb', 'Save');

	const config = {
		headers: {
			'content-type': 'multipart/form-data'
		}
	};
	let fileName = file.name.substr(0, file.name.lastIndexOf('.'));

	if(!(/^[a-zA-Z0-9\.\,\_\-\'\!\*\(\)\ ]*$/.test(fileName))) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Invalid file name. Please change file name. file name contain invalid specical characters. Allowed special characters are '_ - . , ! ( ) *'",
			btnArray : ["Ok"]
		}));
	}

	if (file.size > 10485760) {
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "File Size should not be exceed than <b class='text-danger'>10 MB</b>",
			btnArray : ["Ok"]
		}));
	}

	if (!file.type.match('image.*')) {
		this.props.updateFormState(this.props.form, {
			image: null
		});
		let message = {
			header : "Error",
			body : "Invalid Image Format.Please Choose Image Type File",
			btnArray : ["Ok"]
		};
		this.props.openModal(modalService.infoMethod(message));
		this.updateLoaderFlag(false);
	} else {
		axios.post('/upload', formData, config).then((response) => {
			if (response.data.message == 'success') {
				let tempobj = response.data.main;
				tempobj.imagename = response.data.main.filename;
				tempobj.image = '';
				this.props.updateFormState(this.props.form, tempobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function removeImage() {
	this.props.updateFormState(this.props.form, {
		imageurl: null
	});
}

export function cancel () {
	this.props.history.goBack();
}
