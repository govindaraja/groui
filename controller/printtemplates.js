import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService, pageValidation } from '../utils/services';
import PrintTemplatePreview from '../components/details/printtemplatepreview';
import PrintTemplateGalleryDetails from '../components/details/printtemplategallerydetails';

export function onLoad () {
	axios.get(`/api/companymaster?field=id,name,legalname&filtercondition=`).then((response) => {
		if(response.data.message == 'success') {
			this.setState({
				companiesArray: response.data.main,
				resourceArray: this.controller.getResourceName(),
				qz : (this.props.app.feature.useRawPrinting) ? require('../utils/rawprinting') : null
			});
			this.controller.initialize();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function initialize() {
	if(this.state.createParam) {
		this.props.initialize({
			templatetype : 'MS Word'
		});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getResourceName () {
	let resourceArray = [];

	let myResources = this.props.app.myResources;

	for(let prop in myResources) {
		if(!myResources[prop]['hideInPermission'] && myResources[prop] && myResources[prop].authorization && myResources[prop].authorization['Print']) {
			resourceArray.push({
				name : prop,
				displayName : myResources[prop].displayName
			});
		}
	}

	resourceArray.push({
		name : 'balancesheet',
		displayName : 'Balance Sheet'
	}, {
		name : 'profitloss',
		displayName : 'Profit & Loss'
	});

	resourceArray.sort((a, b) => {
		return (a.displayName.toLowerCase() < b.displayName.toLowerCase()) ? -1 : (a.displayName.toLowerCase() > b.displayName.toLowerCase()) ? 1 : 0;
	});
	return resourceArray;
}

export function resourceOnChange () {
	this.props.updateFormState(this.props.form, {
		printmodules: []
	});

	this.controller.getResourceRelatedDetails();
}

export function getResourceRelatedDetails () {

	this.updateLoaderFlag(true);

	let parentJson = (this.props.app.myResources[this.props.resource.resource] && this.props.app.myResources[this.props.resource.resource].type == 'lookup') ? this.props.app.myResources[this.props.resource.resource].lookupResource : this.props.resource.resource;

	axios.get(`/api/common/methods/getAvailableFieldsForPrint?resource=${parentJson}`).then((response) => {
		if(response.data.message == 'success') {
			let resourceFields = response.data.main;
			if(this.props.resource.resource == 'contacts')
				delete resourceFields.Main;

			axios.get(`/api/printmodules?field=id,name,description&filtercondition='${this.props.resource.resource}'=ANY(printmodules.resources)`).then((secresponse) => {
				if(secresponse.data.message == 'success') {
					let moduleObj = {},
						moduleArray = [];
					moduleArray = secresponse.data.main;
					moduleArray.map((module) => {
						moduleObj[module.id] = module;
					});
					this.setState({ resourceFields, moduleArray, moduleObj });
					this.updateLoaderFlag(false);
				} else {
					let apiResponse = commonMethods.apiResult(secresponse);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function getItemById () {
	axios.get(`/api/templates/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			setTimeout(this.controller.getResourceRelatedDetails, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/templates'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/printtemplates/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace('/list/printtemplates');
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function imageUpload (file) {
	this.updateLoaderFlag(true);
	let tempResourceobj = this.props.resource;
	tempResourceobj.parentresource = "templates";
	tempResourceobj.resourceName = "templates";
	tempResourceobj.parentid = tempResourceobj.id;
	tempResourceobj.imagename = "";

	let tempData = {
		actionverb : 'Save',
		data : tempResourceobj
	};

	const formData = new FormData();
	formData.append('file', file);
	formData.append('data', JSON.stringify(tempResourceobj));
	formData.append('actionverb', 'Save');

	const config = {
		headers: {
			'content-type': 'multipart/form-data'
		}
	};
	let fileName = file.name.substr(0, file.name.lastIndexOf('.'));

	if(!(/^[a-zA-Z0-9\.\,\_\-\'\!\*\(\)\ ]*$/.test(fileName))) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Invalid file name. Please change file name. file name contain invalid specical characters. Allowed special characters are '_ - . , ! ( ) *'",
			btnArray : ["Ok"]
		}));
	}

	if (file.size > 10485760) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "File Size should not be exceed than <b class='text-danger'>10 MB</b>",
			btnArray : ["Ok"]
		}));
	}

	if (file.name.split('.').pop() != 'docx') {
		this.props.updateFormState(this.props.form, {
			image: null
		});
		let message = {
			header : "Error",
			body : "Invalid Document Format.Please Choose Document Type File",
			btnArray : ["Ok"]
		};
		this.props.openModal(modalService.infoMethod(message));
		this.updateLoaderFlag(false);
	} else {
		axios.post('/upload', formData, config).then((response) => {
			if (response.data.message == 'success') {
				let tempobj = response.data.main;
				tempobj.imagename = response.data.main.filename;
				tempobj.path = response.data.main.imageurl;
				tempobj.wordfilename = response.data.main.filename;
				tempobj.image = '';
				this.props.updateFormState(this.props.form, tempobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function removeImage() {
	this.props.updateFormState(this.props.form, {
		path: null
	});
}

export function preview () {
	if(this.props.resource.resource == 'contacts' || this.props.resource.resource == 'customerstatements' || this.props.resource.resource == 'balancesheet' || this.props.resource.resource == 'profitloss') {
		return this.props.openModal(modalService.infoMethod({
			header : "Warning",
			body : "You can't preview this document from the templates page. Please preview from the corresponding transaction / report page.",
			btnArray : ["Ok"]
		}));
	} else {
		this.openModal({
			render: (closeModal) => {
				return <PrintTemplatePreview
					resource={this.props.resource}
					callback = {(item) => this.controller.previewcallback(item)}
					openModal={this.openModal}
					closeModal={closeModal} />
			}, className: {
				content: 'react-modal-custom-class-30',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}
}

export function previewcallback (transactionobj) {
	this.updateLoaderFlag(true);

	let printData = {
		actionverb : 'Print',
		data : transactionobj
	};

	axios({
		method : 'post',
		data : printData,
		url : `/api/${transactionobj.resourceName}`
	}).then((response) => {
		if (response.data.message) {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		} else {
			if(response.data.rawtemplate){
				let qz  = this.state.qz;
				qz.printers.getDefault().then(function(printer) {
					console.log(printer);
				     var data = [];
				    let config = qz.configs.create(printer);
				    if(response.data.language)
				    	data.push({type : 'raw',data : '',options: { language: response.data.language}});
				    data.push(response.data.rawtemplate);
				    qz.print(config, data);
				});
			}else{
				let win = window.open("/print/"+response.data.filename+"?url=" + response.data.url, '_blank');
				if (!win) {
					modalService['infoMethod']({
						header : 'Warning',
						body : "Popup Blocker is enabled! Please add this site to your exception list.",
						btnArray : ['Ok']
					});
				}
			}
		}

		this.updateLoaderFlag(false);
	});

}

export function getPrintTemplate() {
	this.openModal({render: (closeModal) => {
		return <PrintTemplateGalleryDetails openModal={this.openModal} closeModal={closeModal} callback={(templateid)=> {
			this.props.history.replace(`/details/printtemplates/${templateid}`);
		}} />
	}, className: {
		content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
	});
}

export function cancel () {
	this.props.history.goBack();
}
