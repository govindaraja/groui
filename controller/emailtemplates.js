import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({
			resourceArray: this.controller.getResourceName()
		});
		this.controller.getCompanies();
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getResourceName () {
	let resourceArray = [];

	let actionVerb = ['Send To Customer', 'Send To Supplier', 'Send Acknowledgement', 'Send Email', 'Send Mail'];

	let myResources = this.props.app.myResources;

	for(let prop in myResources) {
		if (!myResources[prop].hideInPermission)
			for(let acVerb in myResources[prop].authorization) {
				if(actionVerb.indexOf(acVerb) >= 0) {
					resourceArray.push({
						resourceName : myResources[prop].resourceName,
						displayName : myResources[prop].displayName
					});
					break;
				}
			}
	}

	resourceArray.sort((a, b) => {
		return (a.displayName < b.displayName) ? -1 : (a.displayName > b.displayName) ? 1 : 0;
	});

	return resourceArray;
}

export function getCompanies () {
	this.updateLoaderFlag(true);

	let companiesArray = [];

	axios.get(`/api/companymaster?field=id,name,legalname&filtercondition=`).then((response) => {
		if(response.data.message == 'success') {
			companiesArray = response.data.main;

			this.props.updateFormState(this.props.form, { companiesArray });
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function getTemplateFields () {
	this.updateLoaderFlag(true);
	let fieldsArray = [];
	let tempobj = {...this.props.resource};
	delete tempobj.resourceFields;
	tempobj.template = tempobj.template || '';
	tempobj.subject = tempobj.subject || '';
	tempobj.cc = tempobj.cc || '';
	tempobj.bcc = tempobj.bcc || '';

	let resourceJSON = JSON.parse(JSON.stringify(this.props.app.myResources));

	let parentJson = (resourceJSON[this.props.resource.resourcename] && resourceJSON[this.props.resource.resourcename].type == 'lookup') ? resourceJSON[this.props.resource.resourcename].lookupResource : this.props.resource.resourcename;

	if((this.props.resource.id && this.props.resource.isoldemailtemplate) || this.props.resource.resourcename == 'customerstatements') {
		//let resourceJSON = JSON.parse(JSON.stringify(this.props.app.myResources));

		resourceJSON.customerstatements.fields = {
		    'legalname': {
		        "displayName": "Customer",
		        "emailtype": "simple"
		    },
		    'requestparam.fromdate': {
		        "displayName": "From Date",
		        "emailtype": "date"
		    },
		    'requestparam.todate': {
		        "displayName": "To Date",
		        "emailtype": "date"
		    },
		    "salesperson": {
		        "displayName": "Sales Person",
		        "isForeignKey": true,
		        "foreignKeyOptions": {
		            "resource": "users",
		            "mainField": "users_salesperson.displayname",
		            "additionalField": ["users_salesperson.email"]
		        },
		        "emailtype": "simple"
		    },
		    "collectionrep": {
		        "displayName": "Collection Rep",
		        "isForeignKey": true,
		        "foreignKeyOptions": {
		            "resource": "users",
		            "mainField": "users_collectionrep.displayname",
		            "additionalField": ["users_collectionrep.email"]
		        },
		        "emailtype": "simple"
		    }
		};
		for (let prop in resourceJSON[this.props.resource.resourcename].fields) {
			if (resourceJSON[this.props.resource.resourcename].fields[prop].emailtype) {

				if (resourceJSON[this.props.resource.resourcename].fields[prop].isForeignKey) {
					let foreignResource = resourceJSON[this.props.resource.resourcename].fields[prop].foreignKeyOptions.resource;
					let mainField = resourceJSON[this.props.resource.resourcename].fields[prop].foreignKeyOptions.mainField.split('.')[1];

					let foreignkeyDisplayname = resourceJSON[foreignResource].fields[mainField].displayName;
					let oldFieldName = resourceJSON[this.props.resource.resourcename].fields[prop].displayName;
					let newFieldName = resourceJSON[this.props.resource.resourcename].fields[prop].displayName +"'s "+foreignkeyDisplayname;

					tempobj.template = tempobj.template.replace(new RegExp('{{' + oldFieldName + '}}', 'g'), '{{' + newFieldName + '}}');
					tempobj.subject = tempobj.subject.replace(new RegExp('{{' + oldFieldName + '}}', 'g'), '{{' + newFieldName + '}}');
					tempobj.cc = tempobj.cc.replace(new RegExp('{{' + oldFieldName + '}}', 'g'), '{{' + newFieldName + '}}');
					tempobj.bcc = tempobj.bcc.replace(new RegExp('{{' + oldFieldName + '}}', 'g'), '{{' + newFieldName + '}}');
					fieldsArray.push({field: newFieldName});

					let additionalField = resourceJSON[this.props.resource.resourcename].fields[prop].foreignKeyOptions.additionalField;

					for (let i = 0; i < additionalField.length; i++) {
						let oldAdditionalFieldName = resourceJSON[this.props.resource.resourcename].fields[prop].displayName + " - " + resourceJSON[foreignResource].fields[additionalField[i].split('.')[1]].displayName;
						let newAdditionalFieldName = resourceJSON[this.props.resource.resourcename].fields[prop].displayName + "'s " + resourceJSON[foreignResource].fields[additionalField[i].split('.')[1]].displayName;

						tempobj.template = tempobj.template.replace(new RegExp('{{' + oldAdditionalFieldName + '}}', 'g'), '{{' + newAdditionalFieldName + '}}');
						tempobj.subject = tempobj.subject.replace(new RegExp('{{' + oldAdditionalFieldName + '}}', 'g'), '{{' + newAdditionalFieldName + '}}');
						tempobj.cc = tempobj.cc.replace(new RegExp('{{' + oldAdditionalFieldName + '}}', 'g'), '{{' + newAdditionalFieldName + '}}');
						tempobj.bcc = tempobj.bcc.replace(new RegExp('{{' + oldAdditionalFieldName + '}}', 'g'), '{{' + newAdditionalFieldName + '}}');
						fieldsArray.push({field: newAdditionalFieldName});
					}
				} else {
					fieldsArray.push({field: resourceJSON[this.props.resource.resourcename].fields[prop].displayName});
				}
			}
		}
	}

	axios.get(`/api/common/methods/getAvailableFieldsForPrint?resource=${parentJson}`).then((response) => {
		if(response.data.message == 'success') {
			this.props.updateFormState(this.props.form, {
				resourceFields: this.props.resource.resourcename == 'customerstatements' ? fieldsArray : response.data.main.Main,
				...tempobj,
				example : 'You can include dynamic fields in Subject and Body of the template. Please copy required field from the Template Fields sections below and paste.'
			});

			if(this.props.resource.isoldemailtemplate && this.props.resource.resourcename != 'customerstatements') {
				this.props.openModal(modalService.infoMethod({
					header : "Warning",
					body : "This email template had some old tags, we just replaced them automatically with new tags! Please save the template.",
					btnArray : ["Ok"]
				}));
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getItemById () {
	axios.get(`/api/emailtemplates/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == 'success') {
			this.props.initialize(response.data.main);

			setTimeout(() => {
				this.props.updateFormState(this.props.form, {
					resourceArray: this.controller.getResourceName()
				});
				this.controller.getTemplateFields();
			}, 0);
			this.controller.getCompanies();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}
	let resourceObj = {...this.props.resource};
	resourceObj.isoldemailtemplate = this.props.resource.resourcename=='customerstatements' ? true : false;

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : resourceObj,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/emailtemplates'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/emailtemplates/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace('/list/emailtemplates');
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}