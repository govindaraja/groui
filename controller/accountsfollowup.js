import React, { Component } from 'react';
import axios from 'axios';
import { RenderHistory } from '../utils/customfieldtypes';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	this.controller.getItemById();
}

export function getItemById () {
	axios.get(`/api/accountsfollowup/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == 'success') {
			this.props.initialize({
				...response.data.main,
				histroylength : response.data.main.history ? Object.keys(response.data.main.history).length : 0
			});

			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function historyModal(item) {
	let historyArray = [];
	for(let prop in this.props.resource.history)
		historyArray.push(this.props.resource.history[prop]);

	this.openModal({
		render: (closeModal) => {
			return <RenderHistory
				resource = {this.props.resource}
				historyArray = {historyArray}
				closeModal = {closeModal}
				openModal = {this.openModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : `/api/accountsfollowup`
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			if (param == 'Delete')
				this.props.history.replace(`${this.state.pagejson.backpage}`);
			else
				this.controller.getItemById();
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
