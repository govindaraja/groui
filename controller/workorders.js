import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';
import { ItemRateField } from '../components/utilcomponents';
import WOProjectdetailsModal from '../components/details/woprojectitemdetailsmodal';
import WOProjectEstimationDetailsModal from '../components/details/woprojectestimationitemdetailsmodal';
import EmailModal from '../components/details/emailmodal';
import { RenderItemQty } from '../utils/customfieldtypes';
import ConfirmValuationModal from '../components/details/confirmvaluationmodal';
import {ChildEditModal} from '../components/utilcomponents';
import StockdetailsModal from '../containers/stockdetails';
import ProductionorderitemrequestplannedqtydetailsModal from '../components/details/productionorderitemrequestplannedqtydetailsmodal';

export function onLoad () {
	axios.get(`/api/roles?&field=id,name&filtercondition=roles.name='Production Exception Approvers'`).then((response) => {
		if (response.data.message == 'success') {
			this.setState({
				producitonExceptionApproverRoleId: response.data.main.length > 0 ? response.data.main[0].id : null
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();

	let workorderArray = ["General", "Sales Orders"];
	if(this.props.app.feature.useProjects)
		workorderArray.push("Projects");
	if(this.props.app.feature.useServiceFlow)
		workorderArray.push("Service Calls");
	this.setState({
		workorderArray: workorderArray
	});
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		companyid: this.props.app.selectedcompanyid,
		wodate: new Date(new Date().setHours(0, 0, 0, 0)),
		workorderfor: 'General',
		workorderitems: [],
		workorderoutitems: [],
		workorderinitems: [],
		additionalcharges: [],
		confirmvaluation: {}
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'copy') {
			tempObj = params;
			if(params.worktype == 'Job Work') {
				tempObj.confirmvaluation = {};
			}
		}

		if(params.param == 'Projects' || params.param == 'Sales Orders') {
			if(params.param == 'Sales Orders') {
				tempObj.orderid = params.orderid;
				tempObj.workorderfor = 'Sales Orders';
			}

			if(params.param == 'Projects') {
				tempObj.projectid = params.projectid;
				tempObj.projectid_estimationavailable = params.estimationavailable;
				tempObj.workorderfor = 'Projects';

				this.customFieldsOperation('projects', tempObj, params.allProjectObj, 'workorders');
			}

			tempObj.defaultcostcenter = params.defaultcostcenter;
			tempObj.currencyid = params.currencyid;
			tempObj.currencyexchangerate = params.currencyexchangerate;
		}

		if(params.param == 'Service Calls') {
			tempObj.workorderfor = 'Service Calls';
			utils.assign(tempObj, params, [{'servicecallid' : 'id'}, 'currencyid', 'currencyexchangerate', 'defaultcostcenter', 'territoryid']);
		}
	}

	this.props.initialize(tempObj);
	this.controller.getTandC();

	this.updateLoaderFlag(false);
}

export function renderTableQtyfield(item) {
	return (
		<RenderItemQty status={this.props.resource.status} resource={this.props.resource} item={item} uomObj={this.props.app.uomObj} statusArray={['Approved','Sent To Supplier','Hold']} />
	);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function getItemById () {
	axios.get(`/api/workorders/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.workorderitems.map((item) => {
				if(item.uomconversiontype)
					item.alternateuom = true;
			});
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			this.controller.checkVisibilityForButton();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function checkVisibilityForButton () {
	setTimeout(() => {
		let showPurchaseInvoiceBtn = false , showConfirmValuationBtn = true;
		this.props.resource.workorderitems.forEach((item) => {
			if((item.completedqty || 0) > (item.invoicedqty || 0))
				showPurchaseInvoiceBtn = true;
		});

		this.props.resource.workorderinitems.forEach((item) => {
			if((item.quantity-(item.deliveredqty || 0)-(item.closedqty || 0))>0)
				showConfirmValuationBtn = false;
		});
		this.setState({
			showPurchaseInvoiceBtn,showConfirmValuationBtn
		});
	}, 0);
}

export function currencyOnChange () {
	if (this.props.resource.pricelistid) {
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid) {
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});
		}
	}
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Work Order' and termsandconditions.isdefault`).then((response) => {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function tandcOnchange(id, valueobj) {
	if(id) {
		this.props.updateFormState(this.props.form, {
			tandc: valueobj.terms
		});
	}
}

export function workorderforOnChange () {
	this.props.updateFormState(this.props.form, {
		orderid: null,
		projectid: null,
		servicecallid: null,
	});
}

export function callBackSupplier(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		partnerid_name : valueobj.name,
		suppliercontactperson: null,
		suppliermobile: null,
		supplieremail: null,
		pricelistid: valueobj.purchasepricelistid,
		creditperiod: valueobj.purchasecreditperiod,
		suppliercontactid: valueobj.contactid,
		supplieremail: valueobj.contactid_email,
		suppliermobile: valueobj.contactid_mobile,
		supplierphone: valueobj.contactid_phone,
		suppliercontactperson: valueobj.contactid_name,
		paymentterms : valueobj.purchasepaymentterms
	});
}

export function callbackProject (id, valueobj) {
	let tempObj = {
		defaultcostcenter: valueobj.defaultcostcenter,
		projectid_customerid: valueobj.customerid,
		projectid_warehouseid: valueobj.warehouseid,
		projectid_estimationavailable : valueobj.estimationavailable,
		workorderitems: []
	};

	this.customFieldsOperation('projects', tempObj, valueobj, 'workorders');
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackOrder(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		defaultcostcenter: valueobj.defaultcostcenter
	});
}

export function callbackServiceCall(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		defaultcostcenter: valueobj.defaultcostcenter
	});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson: valueobj.name,
		phone: valueobj.phone,
		email: valueobj.email,
		mobile: valueobj.mobile,
		contactid_name: valueobj.name,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile,
	});
}

export function suppliercontactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		suppliercontactperson: valueobj.name,
		supplieremail: valueobj.email,
		suppliermobile: valueobj.mobile
	});
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
	}
	if(address == 'supplieraddress') {
		tempObj.supplieraddress = addressobj.displayaddress;
		tempObj.supplieraddressid = addressobj.id;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = valueobj.parentid;
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid: this.props.resource.partnerid, partneraddressid: this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.rate`] = 0;
		tempObj[`${itemstr}.labourrate`] = 0;
		tempObj[`${itemstr}.materialrate`] = 0;
		tempObj[`${itemstr}.splitrate`] = returnObject.splitrate;
		tempObj[`${itemstr}.itemid_itemtype`] = returnObject.itemid_itemtype;
		if (item.itemid_itemtype != 'Project') {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		} else {
			tempObj[`${itemstr}.labourtaxid`] = (this.props.resource.labourtaxid && this.props.resource.labourtaxid.length > 0)  ? this.props.resource.labourtaxid : [];
			tempObj[`${itemstr}.materialtaxid`] = (this.props.resource.materialtaxid && this.props.resource.materialtaxid.length > 0)  ? this.props.resource.materialtaxid : [];
		}
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.uomconversionfactor`] = null;
		tempObj[`${itemstr}.uomconversiontype`] = null;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'workorderitems', itemstr);
		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}


		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function uomOnchange (id, valueobj, item, itemstr) {
	let tempObj = {};

	if (valueobj.id == id && valueobj.alternateuom) {
		tempObj[`${itemstr}.uomconversiontype`] = valueobj.conversiontype;
		tempObj[`${itemstr}.uomconversionfactor`] = valueobj.conversionrate;
		tempObj[`${itemstr}.alternateuom`] = valueobj.alternateuom;
	} else {
		tempObj[`${itemstr}.uomconversiontype`] = null;
		tempObj[`${itemstr}.uomconversionfactor`] = null;
		tempObj[`${itemstr}.alternateuom`] = false;
	}
	this.props.updateFormState(this.props.form, tempObj);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.partnerid, partneraddressid: this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase', id, tempObj[`${itemstr}.alternateuom`], tempObj[`${itemstr}.uomconversionfactor`]);
	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, item, itemstr) {
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.partnerid, partneraddressid: this.props.resource.supplieraddressid}, this.props.resource.pricelistid, 'purchase', item.uomid, item.alternateuom, value);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function computeFinalRate() {
	setTimeout(() =>{
		let tempObj = {};
		for (var i = 0; i < this.props.resource.workorderitems.length; i++) {
			if (this.props.resource.workorderitems[i].itemid_itemtype == 'Project' && this.props.resource.workorderitems[i].splitrate) {
				tempObj[`workorderitems[${i}].rate`] = (this.props.resource.workorderitems[i].labourrate ? this.props.resource.workorderitems[i].labourrate : 0) + ((this.props.resource.workorderitems[i].materialrate ? this.props.resource.workorderitems[i].materialrate : 0));
			}
		}		

		this.props.updateFormState(this.props.form, tempObj);

		taxEngine(this.props, 'resource', 'workorderitems', null, null, true);
	}, 0);
}


export function workorderoutitemOnChange (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid_name`] = valueobj.name;
	tempObj[`${itemstr}.itemid_keepstock`] = valueobj.keepstock;
	tempObj[`${itemstr}.itemid_issaleskit`] = valueobj.issaleskit;
	tempObj[`${itemstr}.itemid_hasaddons`] = valueobj.hasaddons;
	tempObj[`${itemstr}.itemid_itemtype`] = valueobj.itemtype;
	tempObj[`${itemstr}.description`] = valueobj.description;
	tempObj[`${itemstr}.uomid`] = valueobj.stockuomid;
	tempObj[`${itemstr}.uomid_name`] = valueobj.stockuomid_name;
	tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
	this.props.updateFormState(this.props.form, tempObj);
}

export function workorderinitemOnChange (id, valueobj, item, itemstr) {
	let itemcount = 0;
	this.props.resource.workorderinitems.forEach((woinitem) => {
		if(woinitem.itemid == id)
			itemcount ++;
	});
	if(itemcount > 1) {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.itemid`]: null
		});
		return this.props.openModal(modalService['infoMethod']({
			header : 'Error',
			body : `Item Cannot be Duplicated`,
			btnArray : ['Ok']
		}));
	}
	let tempObj = {};
	tempObj[`${itemstr}.bomid`] = null;
	tempObj[`${itemstr}.itemid_name`] = valueobj.name;
	tempObj[`${itemstr}.itemid_keepstock`] = valueobj.keepstock;
	tempObj[`${itemstr}.itemid_issaleskit`] = valueobj.issaleskit;
	tempObj[`${itemstr}.itemid_hasaddons`] = valueobj.hasaddons;
	tempObj[`${itemstr}.description`] = valueobj.description;
	tempObj[`${itemstr}.uomid`] = valueobj.stockuomid;
	tempObj[`${itemstr}.uomid_name`] = valueobj.stockuomid_name;
	tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
	tempObj[`${itemstr}.itemid_bomtype`] = valueobj.bomtype;
	tempObj[`${itemstr}.itemid_itemtype`] = valueobj.itemtype;

	this.props.updateFormState(this.props.form, tempObj);
	setTimeout(() => {
		this.controller.getBom(id, itemstr);
	}, 0);
}

export function getBom (itemid, itemstr) {
	this.updateLoaderFlag(true);
	axios.get(`/api/bom?&field=id,name,isdefault&filtercondition=bom.status='Approved' and bom.itemid=${itemid}&sortstring=(case when isdefault is true then 1 when isdefault is false then 2 else 3 end)`).then((response) => {
		if (response.data.message == 'success') {
			let bomitemsarray = response.data.main;
			if(bomitemsarray.length > 0 && (bomitemsarray.filter(bom=>bom.isdefault).length > 0 || bomitemsarray.length == 1)) {
				let bom = bomitemsarray.filter(bom=>bom.isdefault).length > 0 ? bomitemsarray.filter(bom=>bom.isdefault)[0] : bomitemsarray[0];
				let tempObj = {};
				tempObj[`${itemstr}.bomid`] = bom.id;
				tempObj[`${itemstr}.bomid_name`] = bom.name;
				this.props.updateFormState(this.props.form, tempObj);
			}

			setTimeout(() => {
				let tempchilditem = this.selector(this.props.fullstate, itemstr);
				this.controller.getBOMItems(tempchilditem, itemstr);
			}, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackBom(id, item, itemstr) {
	item.bomid = id;
	if(Number(id) > 0) {
		this.controller.getBOMItems(item, itemstr)
	} else {
		let woinitem = this.selector(this.props.fullstate, itemstr);
		let workorderoutitems = this.props.resource.workorderoutitems.filter(wooutitem=>!(wooutitem.isbomitem && wooutitem.specificitemid==woinitem.itemid));
		this.props.updateFormState(this.props.form, { workorderoutitems });
	}
}

export function applicableforOnChange(itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.specificitemid`]: null
	})
}

export function getSpecificItems() {
	let array = [];
	
	this.props.resource.workorderinitems.forEach((fpitem) => {
		array.push({
			id : fpitem.itemid,
			name : fpitem.itemid_name
		});
	});
	
	return array;
}

export function specificItemOnChange(value, item, itemstr) {
	let tempObj = {};
	for(let i=0;i<this.props.resource.workorderinitems.length;i++){
		if(this.props.resource.workorderinitems[i].itemid == value){
			tempObj[`${itemstr}.specificitemid_bomtype`] = this.props.resource.workorderinitems[i].itemid_bomtype;
			break;
		}
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function getBOMItems (itemobj, itemstr) {
	if(!itemobj.itemid || !itemobj.bomid)
		return null;

	let workorderoutitems = this.props.resource.workorderoutitems ?  this.props.resource.workorderoutitems.filter(bomitem=>!(bomitem.specificitemid == itemobj.itemid && bomitem.isbomitem)) : [];

	this.updateLoaderFlag(true);

	axios.get(`/api/query/getmultibomitemsquery?itemid=${itemobj.itemid}&bomid=${itemobj.bomid}`).then((response) => {
		if (response.data.message == 'success') {
			let responseArr = response.data.main.sort((a,b) => (a.id - b.id));

			responseArr.forEach((item) => {
				let tempObj = {
					itemid: item.itemid,
					itemid_name: item.itemid_name,
					itemid_keepstock: item.itemid_keepstock,
					itemid_issaleskit: item.itemid_issaleskit,
					itemid_hasaddons: item.itemid_hasaddons,
					description: item.description,
					uomid: item.uomid,
					uomid_name: item.uomid_name,
					conversion: item.quantity,
					quantity: Number((item.quantity * itemobj.quantity).toFixed(this.props.app.roundOffPrecisionStock)),
					applicablefor: 'Specific Item',
					specificitemid: itemobj.itemid,
					specificitemid_bomtype : itemobj.itemid_bomtype,
					isbomitem : true,
					warehouseid: itemobj.warehouseid
				};
				this.customFieldsOperation('itemmaster', tempObj, item, 'workorderoutitems');
				workorderoutitems.push(tempObj);
			});
			this.props.updateFormState(this.props.form, {
				workorderoutitems
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function workorderinitemsQtyOnChange (value, item, itemstr) {
	let tempObj = {};
	this.props.resource.workorderoutitems.forEach((itemobj, index) => {
		if(itemobj.specificitemid == item.itemid)
			tempObj[`workorderoutitems[${index}].quantity`] = Number((itemobj.conversion * (value ? value : 0)).toFixed(this.props.app.roundOffPrecisionStock));
	});
	this.props.updateFormState(this.props.form, tempObj);
}

export function conversionOnChange (value, item, itemstr) {
	let tempObj = {};
	let fpQty = 0;
	this.props.resource.workorderinitems.some((itemobj) => {
		if(itemobj.itemid == item.specificitemid) {
			fpQty = itemobj.quantity;
			return true;
		}
	});

	if(fpQty > 0) {
		tempObj[`${itemstr}.quantity`] = Number((value * fpQty).toFixed(this.props.app.roundOffPrecisionStock));
		this.props.updateFormState(this.props.form, tempObj);
	}
}

export function deleteWOOutItem (index) {
	let workorderoutitems = [...this.props.resource.workorderoutitems];
	let initem = this.props.resource.workorderinitems.filter(initem=>initem.itemid == workorderoutitems[index].specificitemid)[0];

	if(workorderoutitems[index].isbomitem && initem.itemid_bomtype == 'Standard'){
		if(this.state.producitonExceptionApproverRoleId && this.props.app.user.roleid.includes(this.state.producitonExceptionApproverRoleId)) {
			let message = {
				header : 'Error',
				body : 'Do you want to delete standard bom item ?',
				btnArray : ['Yes','No']
			};
			this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if(param) {
					workorderoutitems.splice(index, 1);
					this.props.updateFormState(this.props.form, {
						workorderoutitems
					});
				}
			}));
		} else {
			this.props.openModal(modalService['infoMethod']({
				header : 'Error',
				body : `Only Production Exception Approver have access to delete standard bom items.`,
				btnArray : ['Ok']
			}));
		}
	} else {
		workorderoutitems.splice(index, 1);
		this.props.updateFormState(this.props.form, {
			workorderoutitems
		});
	}
}

export function deleteWOInItem(index) {
	let workorderinitems = [...this.props.resource.workorderinitems];
	let workorderoutitems = this.props.resource.workorderoutitems.filter(item=>item.specificitemid != workorderinitems[index].itemid);
	workorderinitems.splice(index,1);
	this.props.updateFormState(this.props.form, {
		workorderoutitems,
		workorderinitems
	});
}

export function getProjectItems (param) {
	this.openModal({render: (closeModal) => {return <WOProjectdetailsModal resource={this.props.resource} sourcename={param} closeModal={closeModal} callback={(itemArray) => {
		this.controller.projectItemsCallback(itemArray);
	}} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function projectItemsCallback(itemArray) {
	let workorderitems = [...this.props.resource.workorderitems];
	for (var i = 0; i < itemArray.length; i++) {
		let itemFound = false;
		for (var j = 0; j < workorderitems.length; j++) {
			if (workorderitems[j].boqitemsid == itemArray[i].boqitemsid) {
				workorderitems[j].quantity = itemArray[i].quantity;
				itemFound = true;

				if (!itemArray[i].checked) {
					workorderitems.splice(j, 1);
				}
				break;
			}
		}

		if (!itemFound && itemArray[i].checked) {
			itemArray[i].discountmode = 'Percentage';
			itemArray[i].discountquantity = null;
			itemArray[i].taxid = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : null;
			workorderitems.push(itemArray[i]);
		}
	}
	
	this.props.updateFormState(this.props.form, {
		workorderitems
	});
	if(!this.props.resource.taxid || (this.props.resource.taxid && this.props.resource.taxid.length == 0))
		setTimeout(() => this.controller.getRateFn(0), 0);
}

export function getRateFn (count) {
	if (count < this.props.resource.workorderitems.length) {
		let promise = commonMethods.getItemDetails(this.props.resource.workorderitems[count].itemid, {customerid: this.props.resource.partnerid, partneraddressid: this.props.resource.supplieraddressid}, null, 'purchase', this.props.resource.workorderitems[count].uomid, this.props.resource.workorderitems[count].alternateuom, this.props.resource.workorderitems[count].uomconversionfactor);
		promise.then((returnObject) => {
			let tempObj = {};
			tempObj[`workorderitems[${count}].taxid`] = returnObject.taxid;

			this.props.updateFormState(this.props.form, tempObj);

			this.controller.getRateFn(count+1);
		}, (reason) => {});
	} else {
		this.controller.computeFinalRate();
	}
}

export function getProjectEstimationItems () {
	this.openModal({render: (closeModal) => {
		return <WOProjectEstimationDetailsModal
			resource={this.props.resource}
			itemarray = {'workorderitems'}
			param={'workorders'} 
			updateFormState = {this.props.updateFormState}
			form = {this.props.form}
			app={this.props.app}
			openModal={this.props.openModal}
			closeModal={closeModal}
			callback={(itemArray) => {this.controller.projectEstimationItemsCallback(itemArray);}}/>
		},
		className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function projectEstimationItemsCallback(itemArray) {
	let workorderitems = [...this.props.resource.workorderitems];
	for (var i = 0; i < itemArray.length; i++) {
		let itemFound = false;
		for (var j = 0; j < workorderitems.length; j++) {
			let tempObj = {};
			if (workorderitems[j].projectestimationitemsid == itemArray[i].projectestimationitemsid) {
				workorderitems[j].quantity = itemArray[i].quantity;
				itemFound = true;
				break;
			}
		}

		if (!itemFound) {
			itemArray[i].discountmode = 'Percentage';
			itemArray[i].discountquantity = null;
			itemArray[i].taxid = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : null;
			workorderitems.push(itemArray[i]);
		}
	}

	this.props.updateFormState(this.props.form, {
		workorderitems
	});
	this.controller.computeFinalRate();
	if(!this.props.resource.taxid || (this.props.resource.taxid && this.props.resource.taxid.length == 0))
		setTimeout(() => this.controller.getRateFn(0), 0);
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Supplier',
			data : emailObj
		},
		url : '/api/workorders'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.suppliercontactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					var apiResponse = commonMethods.apiResult(response);
					this.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				var apiResponse = commonMethods.apiResult(response);
				this.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			var apiResponse = commonMethods.apiResult(response);
			this.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function save (param, confirm) {
	if (param == 'Send To Supplier') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'workorders'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});

	} else {
		this.updateLoaderFlag(true);
		let completedqty = 0, totalqty = 0;

		if(param !='Update' && param !='Hold' && param !='Unhold' && param !='Cancel' && param !='Delete' && param !='Amend' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}
		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/workorders'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					this.props.history.replace(`/details/workorders/${response.data.main.id}`);
				} else {
					if (param == 'Delete') {
						this.props.history.replace("/list/workorders");
					} else {
						this.props.initialize(response.data.main);
						this.controller.checkVisibilityForButton();
					}
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function createPurchaseInvoice () {
	this.props.history.push({pathname: '/createPurchaseInvoice', params: {...this.props.resource, param: 'Work Orders'}});
}

export function createPayment() {
	this.props.history.push({pathname: '/createPaymentVoucher', params: {...this.props.resource, param: 'Work Orders'}});	
}

export function createItemRequest() {
	this.props.openModal({
		render: (closeModal) => {
			return <ProductionorderitemrequestplannedqtydetailsModal resource={this.props.resource} resourcename="workorders" openModal={this.props.openModal} closeModal={closeModal} history={this.props.history} />
		}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
	});
}

export function createWorkProgress() {
	this.props.history.push({pathname: '/createWorkProgress', params: {...this.props.resource, param: 'Work Orders'}});
}

export function createReceiptNote() {
	this.props.history.push({pathname: '/createReceiptNote', params: {...this.props.resource, param: 'Job Work'}});	
}


export function confirmValuation() {
	let getBody = (closeModal) => {
		return (
			<ConfirmValuationModal />
		);
	}
	this.props.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} app={this.props.app} updateFormState={this.props.updateFormState} openModal={this.openModal} closeModal={closeModal} resourceName={'workorders'} getBody={getBody} save={this.controller.save} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'},confirmModal: true});
}

export function cancel () {
	this.props.history.goBack();
}

export function openStockDetails (item) {
	this.props.openModal({render: (closeModal) => {return <StockdetailsModal resource = {this.props.resource} item={item} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}