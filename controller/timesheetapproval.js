import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initializeState();
	else
		this.controller.getItemById();
}

export function initializeState () {
	let tempObj = {
		resultArray : [],
		summaryArray : [],
		detailsArray : []
	}

	axios.get(`/api/employees?&field=id,displayname&filtercondition=employees.userid=${this.props.app.user.id}`).then((response)=> {
		if (response.data.message == 'success') {
			if(response.data.main.length > 0)
				tempObj.employeeid = response.data.main[0].id;
		}

		this.props.initialize(tempObj);
		this.updateLoaderFlag(false);
	});
}

export function callbackEmployee (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		employeeid_displayname : valueobj.displayname
	});

	this.controller.getTimesheetDetails();
}

export function getTimesheetDetails () {
	if(this.props.resource.employeeid && this.props.resource.fromdate && this.props.resource.todate) {
		this.updateLoaderFlag(true);

		let filterString = [];

		['employeeid', 'fromdate', 'todate'].forEach((item) => {
			if(this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});

		axios.get(`/api/query/gettimesheetdetailsquery?${filterString.join('&')}`).then((response) => {
			if(response.data.message == 'success') {

				response.data.summaryArray.sort(function (a, b) {
					return (a.startdatetime < b.startdatetime) ? -1 : (a.startdatetime > b.startdatetime) ? 1 : 0;
				});

				let detailsArray = [], resultArray = [], summaryArray = [];

				resultArray = [...response.data.main];
				summaryArray = [...response.data.summaryArray];

				this.props.updateFormState(this.props.form, {
					detailsArray,
					resultArray,
					summaryArray
				});
				setTimeout(this.controller.filterDateFn, 0);

			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}
}

export function filterDateFn () {
	setTimeout(() => {

		let detailsArray = [];
		let summaryArray = [...this.props.resource.summaryArray];

		this.props.resource.detailsfor = this.props.resource.detailsfor ? ((new Date(this.props.resource.fromdate) <= new Date(this.props.resource.detailsfor) && new Date(this.props.resource.todate) >= new Date(this.props.resource.detailsfor)) ? this.props.resource.detailsfor : this.props.resource.fromdate) : this.props.resource.fromdate;

		if(this.props.resource.resultArray.length > 0) {
			this.props.resource.resultArray.forEach((item) => {
				if(new Date(item.startdatetime).toDateString() == new Date(this.props.resource.detailsfor).toDateString())
					detailsArray.push(item);
			});

			if(detailsArray.length == 0)
				detailsArray.push({
					activitytype : 'Not Available !!!'
				});
		} else {
			detailsArray.push({
				activitytype : 'Not Available !!!'
			});
		}

		if(detailsArray.length > 0) {
			detailsArray.sort((a, b) => (a.startdatetime < b.startdatetime) ? -1 : (a.startdatetime > b.startdatetime) ? 1 : 0);
		}

		if(summaryArray.length == 0)
			summaryArray.push({
				startdatetime : 'Not Available !!!'
			});

		this.props.updateFormState(this.props.form, {
			detailsArray,
			summaryArray 
		});

		this.updateLoaderFlag(false);
	}, 0);
}

export function getItemById () {
	axios.get(`/api/timesheetapproval/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});

			setTimeout(this.controller.getTimesheetDetails, 0);

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/timesheetapproval'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace("/details/timesheetapproval/" + response.data.main.id);
			else if (param == 'Delete')
				this.props.history.replace("/list/timesheetapproval");
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
