import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import ComponentPickingModal from '../components/details/servicereportcomponentpickingmodal';
import { ItemRateField, ChildEditModal } from '../components/utilcomponents';
import EmailModal from '../components/details/emailmodal';
import ServicecallequipmentaddModal from '../components/details/servicecallequipmentaddmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		servicereportdate: new Date(new Date().setHours(0, 0, 0, 0)),
		servicetype: 'Field Service',
		engineerid: this.props.app.user.isengineer ? this.props.app.user.id : null,
		servicereportitems: [],
		equipmentitems: [],
		componentitems: [],
		serviceitems: [],
		expenseitems: [],
		additionalcharges: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'copy')
			tempObj = {...tempObj, ...params};
		else {
			let equipmentitemArr = params.equipmentitems;
			tempObj.equipmentitems = [];

			utils.assign(tempObj, params, [{'servicecallid' : 'id'}, {'servicecallid_calltype' : 'calltype'}, 'companyid', 'customerid', 'customerid_name', 'contactid', 'defaultcostcenter',  'contractid', 'contractid_contractno', 'contracttypeid', 'contracttypeid_name', 'equipmentid', 'equipmentid_displayname', 'salesperson', 'pricelistid', 'territoryid', 'currencyid', 'currencyexchangerate', {'paymentterms' : 'customerid_paymentterms'}, {'calltype' : 'calltype'}]);

			this.customFieldsOperation('servicecalls', tempObj, params, 'servicereports');

			equipmentitemArr.forEach((item) => {
				let tempChildObj = {
				};
				utils.assign(tempChildObj, item, ['equipmentid', 'equipmentid_displayname', 'equipmentid_serialno', 'equipmentid_description', 'contracttypeid', 'contracttypeid_name', 'contractid', 'contractid_contractno', 'scheduleid','equipmentid_invoicedate', 'equipmentid_installeddate']);

				this.customFieldsOperation('equipmentitems', tempChildObj, item, 'equipmentitems');
				tempObj.equipmentitems.push(tempChildObj);
			});

			if(params.status == 'Assigned' && (params.inhouseservicestatus == 'Waiting for Equipment' || params.inhouseservicestatus == 'Equipment Received') && params.inhouseservicerequired) {
				tempObj.servicetype = 'Field Service';
				let message = {
					header : 'Error',
					body : 'Please note that a Service Report of Service Type "In-house Service" can be created only if the In-house Service Status is Assigned',
					btnArray : ['Ok']
				};
				this.props.openModal(modalService['infoMethod'](message));
			} else if(params.status == 'Assigned' && params.inhouseservicestatus == 'Assigned' && params.inhouseservicerequired) {
				tempObj.servicetype =  'In-house Service';
			} else {
				tempObj.servicetype =  'Field Service';
			}
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		if(this.props.resource.servicecallid > 0) {
			this.controller.currencyOnChange();
			this.controller.getServicesFromEstimations();	
		}
	}, 0);

	this.updateLoaderFlag(false);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function getItemById () {
	axios.get(`/api/servicereports/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempservicereportObj = {
				...response.data.main,
				componentitems : [],
				serviceitems : [],
				expenseitems : []
			};

			tempservicereportObj.servicereportitems.forEach((item) => {
				if (item.uomconversiontype)
					item.alternateuom = true;

				if ((item.itemrequestitemsid || item.bufferstockitemsid) && item.itemid_itemtype == 'Product')
					tempservicereportObj.componentitems.push(item);

				if(item.itemid_itemtype == 'Service')
					tempservicereportObj.serviceitems.push(item);

				if(item.itemid_itemtype == 'Expense')
					tempservicereportObj.expenseitems.push(item);
			});

			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});

			this.props.initialize(tempservicereportObj);

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function currencyOnChange () {
	if (this.props.resource.pricelistid)
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid)
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});

	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function callBackServiceCall (id, valueobj) {
	let tempObj = {
		servicecallid_calltype : valueobj.calltype,
		defaultcostcenter: valueobj.defaultcostcenter,
		contractid : valueobj.contractid,
		contractid_contractno : valueobj.contractid_contractno,
		contracttypeid : valueobj.contracttypeid,
		contracttypeid_name : valueobj.contracttypeid_name,
		equipmentid : valueobj.equipmentid,
		equipmentid_displayname : valueobj.equipmentid_displayname,
		currencyid: valueobj.currencyid,
		currencyexchangerate: valueobj.currencyexchangerate,
		salesperson : valueobj.salesperson,
		pricelistid : valueobj.pricelistid,
		componentitems : [],
		serviceitems : [],
		expenseitems : [],
		equipmentitems : [],
		calltype : valueobj.calltype,
		servicetype: 'Field Service'
	}

	if(valueobj.status == 'Assigned' && valueobj.inhouseservicestatus != 'Assigned' && valueobj.inhouseservicerequired) {
		let message = {
			header : 'Error',
			body : 'Please note that a Service Report of Service Type "In-house Service" can be created only if the In-house Service Status is Assigned',
			btnArray : ['Ok']
		};
		this.props.openModal(modalService['infoMethod'](message));
	} else if(valueobj.status == 'Assigned' && valueobj.inhouseservicestatus == 'Assigned' && valueobj.inhouseservicerequired) {
		tempObj.servicetype =  'In-house Service';
	}

	this.props.updateFormState(this.props.form, tempObj);

	this.updateLoaderFlag(true);

	this.controller.getEquipmentItems();
}

export function getEquipmentItems () {
	let equipmentCustomField = this.getCustomFields('equipmentitems', 'string', false, this.props.app);
	let equipmentitems = [];

	return axios.get(`/api/equipmentitems?field=id,equipmentid,equipments/displayname/equipmentid,equipments/description/equipmentid,equipments/serialno/equipmentid,contractid,contracts/contractno/contractid,contracttypeid,contracttypes/contracttypes/contracttypeid,scheduleid${(equipmentCustomField ? ','+equipmentCustomField : '' )}&filtercondition=equipmentitems.parentresource = 'servicecalls' and equipmentitems.parentid=${this.props.resource.servicecallid}`).then((response)=> {
		if (response.data.message == 'success') {
			response.data.main.forEach((item) => {
				let tempObj = {
					equipmentid : item.equipmentid,
					equipmentid_displayname : item.equipmentid_displayname,
					equipmentid_serialno : item.equipmentid_serialno,
					equipmentid_description : item.equipmentid_description,
					contracttypeid : item.contracttypeid,
					contracttypeid_name : item.contracttypeid_name,
					contractid : item.contractid,
					contractid_contractno : item.contractid_contractno,
					scheduleid : item.scheduleid
				};
				this.customFieldsOperation('equipmentitems', tempObj, item, 'equipmentitems');

				equipmentitems.push(tempObj);
			});

			this.props.updateFormState(this.props.form, { equipmentitems });

			this.controller.getServicesFromEstimations();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function callbackEngineer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson: valueobj.id
	});
}

export function callBackCustomerName (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		territoryid: valueobj.territory
	});
}

export function calculateTimespent (value) {
	if (this.props.resource.starttime != null && this.props.resource.endtime != null) {
		if(new Date(this.props.resource.starttime).getTime() <= new Date(this.props.resource.endtime).getTime()) {
			let timeStart = new Date(this.props.resource.starttime).getTime();
			let timeEnd = new Date(this.props.resource.endtime).getTime();
			let timeDiff = Math.abs(timeEnd - timeStart);

			let hours = Math.floor(timeDiff / 1000 / 60 / 60);
			if (hours < 10)
				hours = '0' + hours;

			timeDiff -= hours * 1000 * 60 * 60;

			let minutes = Math.floor(timeDiff / 1000 / 60);
			if (minutes < 10)
				minutes = '0' + minutes;

			timeDiff -= minutes * 1000 * 60;

			this.props.updateFormState(this.props.form, {
				timespent: `${hours}:${minutes}`
			});
		}
	}
}

export function equipmentonChange(equipmentid, item, itemstr) {
	let tempObj = {};
	if(equipmentid > 0) {
		this.props.resource.equipmentitems.forEach((a) => {
			if(a.equipmentid == equipmentid) {
				tempObj[`${itemstr}.contractid`] = a.contractid;
				tempObj[`${itemstr}.contractid_contractno`] = a.contractid_contractno;
				tempObj[`${itemstr}.contracttypeid`] = a.contracttypeid;
				tempObj[`${itemstr}.contracttypeid_name`] = a.contracttypeid_name;

				this.customFieldsOperation('equipmentitems', tempObj, a, 'equipmentitems', itemstr);
			}
		});

		this.props.updateFormState(this.props.form, tempObj);
	} else {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.contractid`] : null,
			[`${itemstr}.contractid_contractno`] : null,
			[`${itemstr}.contracttypeid`] : null,
			[`${itemstr}.contracttypeid_name`] : null
		});
	}
	let promise1 = commonMethods.getItemDetails(item.itemid, this.props.resource.customerid, null, 'sales', item.uomid, item.alternateuom, item.uomconversionfactor, equipmentid > 0 ? tempObj[`${itemstr}.contractid`] : null, this.props.resource.servicecallid, equipmentid > 0 ? equipmentid : null);

	promise1.then((returnObject) => {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.freeorchargeable`] : returnObject.freeorchargeable,
			[`${itemstr}.rate`] : returnObject.freeorchargeable == 'Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app)
		});

		this.controller.computeFinalRate();

	}, (reason) => {});
}

export function getServicesFromEstimations() {
	this.updateLoaderFlag(true);

	let itemidarray = [],
		serviceitems = this.props.resource.serviceitems,
		expenseitems = this.props.resource.expenseitems;

	for (let i = 0; i < serviceitems.length; i++)
		itemidarray.push(serviceitems[i].itemid);

	for (let j = 0; j < expenseitems.length; j++)
		itemidarray.push(expenseitems[j].itemid);

	return axios.get(`/api/estimationitems?field=id,itemid,itemmaster/itemtype/itemid,itemmaster/name/itemid,itemmaster/issaleskit/itemid,itemmaster/keepstock/itemid,itemmaster/uomgroupid/itemid,rate,description,taxid,uomid,itemnamevariationid,size,freeorchargeable,quantity,discountquantity,discountmode,uomconversionfactor,uomconversiontype,equipmentid,contractid,contracttypeid&filtercondition=estimations_parentid.status='Accepted' and itemmaster_itemid.itemtype in ('Service', 'Expense') and estimations_parentid.servicecallid=${this.props.resource.servicecallid}`).then((response) => {
		if (response.data.message == 'success') {
			response.data.main.forEach((item) => {
				if(itemidarray.indexOf(item.itemid) == -1) {
					if(item.itemid_itemtype == 'Service')
						serviceitems.push({
							itemid : item.itemid,
							itemid_name : item.itemid_name,
							itemid_issaleskit : item.itemid_issaleskit,
							itemid_itemtype : item.itemid_itemtype,
							rate : item.rate,
							discountmode : item.discountmode,
							discountquantity : item.discountquantity,
							description : item.description,
							quantity : item.quantity,
							taxid : item.taxid,
							uomid : item.uomid,
							itemid_uomgroupid : item.itemid_uomgroupid,
							itemnamevariationid : item.itemnamevariationid,
							itemnamevariationid_name : item.itemnamevariationid_name,
							alternateuom : item.alternateuom,
							size : item.size,
							itemid_keepstock : item.itemid_keepstock,
							freeorchargeable : item.freeorchargeable,
							uomconversiontype : item.uomconversiontype,
							uomconversionfactor : item.uomconversionfactor,
							alternateuom : (item.uomconversiontype) ? true :false,
							equipmentid: item.equipmentid,
							contractid: item.contractid,
							contracttypeid: item.contracttypeid
						});

					if(item.itemid_itemtype == 'Expense')
						expenseitems.push({
							itemid : item.itemid,
							itemid_name : item.itemid_name,
							itemid_issaleskit : item.itemid_issaleskit,
							itemid_itemtype : item.itemid_itemtype,
							rate : item.rate,
							discountmode : item.discountmode,
							discountquantity : item.discountquantity,
							description : item.description,
							quantity : item.quantity,
							taxid : item.taxid,
							uomid : item.uomid,
							itemid_uomgroupid : item.itemid_uomgroupid,
							itemnamevariationid : item.itemnamevariationid,
							itemnamevariationid_name : item.itemnamevariationid_name,
							alternateuom : item.alternateuom,
							freeorchargeable : item.freeorchargeable,
							uomconversiontype : item.uomconversiontype,
							uomconversionfactor : item.uomconversionfactor
						});
				}
			});

			this.props.updateFormState(this.props.form, {
				serviceitems,
				expenseitems
			});

			this.controller.computeFinalRate();

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, this.props.resource.customerid, null, 'sales', item.uomid, item.alternateuom, item.uomconversionfactor,item.contractid, this.props.resource.servicecallid, item.equipmentid);

	promise.then((returnObject)=> {
		let tempObj = {
			[`${itemstr}.itemid`] : returnObject.itemid,
			[`${itemstr}.itemid_name`] : returnObject.itemid_name,
			[`${itemstr}.itemid_issaleskit`] : returnObject.itemid_issaleskit,
			[`${itemstr}.itemid_itemtype`] : returnObject.itemid_itemtype,
			[`${itemstr}.description`] : returnObject.description,
			[`${itemstr}.rate`] : returnObject.freeorchargeable=='Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app),
			[`${itemstr}.quantity`] : item.quantity ? item.quantity : 1,
			[`${itemstr}.freeorchargeable`] : returnObject.freeorchargeable,
			[`${itemstr}.uomid`] : returnObject.uomid,
			[`${itemstr}.uomid_name`] : returnObject.uomid_name,
			[`${itemstr}.alternateuom`] : returnObject.alternateuom,
			[`${itemstr}.itemid_uomgroupid`] : returnObject.uomgroupid,
			[`${itemstr}.taxid`] : (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid,
			[`${itemstr}.itemnamevariationid`] : returnObject.itemnamevariationid,
			[`${itemstr}.itemnamevariationid_name`] : returnObject.itemnamevariationid_name,
			[`${itemstr}.itemid_keepstock`] : returnObject.keepstock,
			[`${itemstr}.accountid`] : returnObject.itemid_incomeaccountid
		};

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'servicereportitems', itemstr);

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		this.props.updateFormState(this.props.form, tempObj);

		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
	}, (reason)=> {});
}


export function quantityOnChange(itemqty, item, itemstr) {
	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.billingquantity`]: Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))
		});
	}

	this.controller.computeFinalRate();
}

export function computeFinalRate() {
	setTimeout(() => {
		let servicereportitems = [];

		for (let i = 0; i < this.props.resource.componentitems.length; i++)
			servicereportitems.push(this.props.resource.componentitems[i]);

		for (let i = 0; i < this.props.resource.serviceitems.length; i++)
			servicereportitems.push(this.props.resource.serviceitems[i]);

		for (let i = 0; i < this.props.resource.expenseitems.length; i++)
			servicereportitems.push(this.props.resource.expenseitems[i]);

		this.props.updateFormState(this.props.form, {
			servicereportitems
		});

		setTimeout(() => {
			taxEngine(this.props, 'resource', 'servicereportitems');
		}, 0)
	});
}

export function freeorchargeableOnChange(value, item, itemstr) {
	if(value == 'Free') {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.rate`] : 0,
			[`${itemstr}.discountquantity`] : null,
			[`${itemstr}.taxid`] : null
		});
		this.controller.computeFinalRate();
	}
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, this.props.resource.customerid, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor, item.contractid, this.props.resource.servicecallid, item.equipmentid);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = returnObject.freeorchargeable=='Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = returnObject.freeorchargeable=='Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = returnObject.freeorchargeable=='Free' ? 0 : Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = returnObject.freeorchargeable=='Free' ? 0 : Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, this.props.resource.customerid, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor, item.contractid, this.props.resource.servicecallid, item.equipmentid);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = returnObject.freeorchargeable=='Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = returnObject.freeorchargeable=='Free' ? 0 : commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = returnObject.freeorchargeable=='Free' ? 0 : Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = returnObject.freeorchargeable=='Free' ? 0 : Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function addComponentItem() {
	if(this.props.resource.servicecallid) {
		let getBody = (closeModal) => {
			return (
				<ComponentPickingModal />
			)
		}
		this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} array={this.props.array} updateFormState={this.props.updateFormState} callback={this.controller.computeFinalRate} closeModal={closeModal} getBody={getBody} customFieldsOperation = {this.customFieldsOperation} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
	} else {
		let message = {
			header : 'Error',
			body : 'Please choose service call no and then add component',
			btnArray : ['Ok']
		};
		this.props.openModal(modalService['infoMethod'](message));
	}
}

export function openAcknowledgeAlert() {
	let message = {
		header : 'Acknowledgement Alert',
		body : 'Acknowledgement already sent. Do you want to send again?',
		btnArray : ['Yes','No']
	};

	this.props.openModal(modalService['confirmMethod'](message, (param) => {
		if(param)
			this.controller.openemail();
	}));
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;

	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Customer',
			data : emailObj
		},
		url : '/api/servicereports'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.servicecallid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);

					this.props.updateFormState(this.props.form, {
						senttocutomer: response.data.main.senttocutomer,
						modified: response.data.main.modified
					});

					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);

				this.props.updateFormState(this.props.form, {
					senttocutomer: response.data.main.senttocutomer,
					modified: response.data.main.modified
				});

				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function openemail() {
	let emailobj = {
		firsttime: true,
		sendemail: true
	};

	this.openModal({
		render: (closeModal) => {
			return <EmailModal
					parentresource = {this.props.resource}
					app = {this.props.app}
					resourcename = {'servicereports'}
					activityemail = {false}
					callback = {this.controller.emailFn}
					openModal = {this.openModal}
					closeModal = {closeModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}, confirmModal: true
	});
}

export function addEquipment () {
	this.openModal({
		render: (closeModal) => {
			return <ServicecallequipmentaddModal
					   resource = {this.props.resource}
					   app = {this.props.app}
					   array = {this.props.array}
					   closeModal = {closeModal}
					   openModal = {this.openModal}
					   updateFormState = {this.props.updateFormState}
					   form = {this.props.form}
					   resourcename = {'servicereports'}
					   childresourcename = {'equipmentitems'}
					   customFieldsOperation = {this.customFieldsOperation}
					   getCustomFields = {this.getCustomFields} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function save (param, confirm) {
	if (param == 'Send To Customer')
		return this.props.resource.senttocutomer ?  this.controller.openAcknowledgeAlert() : this.controller.openemail();
	else {
		this.updateLoaderFlag(true);

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		let tempServiceReportObj = this.props.resource;
		tempServiceReportObj.servicereportitems = [];

		tempServiceReportObj.componentitems.forEach((item) => {
			tempServiceReportObj.servicereportitems.push(item);
		});

		tempServiceReportObj.serviceitems.forEach((item) => {
			tempServiceReportObj.servicereportitems.push(item);
		});

		tempServiceReportObj.expenseitems.forEach((item) => {
			tempServiceReportObj.servicereportitems.push(item);
		});
		setTimeout(() => {
			axios({
				method : 'post',
				data : {
					actionverb : param,
					data : tempServiceReportObj,
					ignoreExceptions : confirm ? true : false
				},
				url : '/api/servicereports'
			}).then((response)=> {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
					if (resparam)
						this.controller.save(param, true);
				}));

				if (response.data.message == 'success') {
					if(this.state.createParam)
						this.props.history.replace(`/details/servicereports/${response.data.main.id}`);
					else if (param == 'Delete')
						this.props.history.replace("/list/servicereports");
					else {
						let tempObj = {
							...response.data.main,
							componentitems : [],
							serviceitems : [],
							expenseitems : []
						};

						tempObj.servicereportitems.forEach((item) => {
							if((item.itemrequestitemsid || item.bufferstockitemsid) && item.itemid_itemtype == 'Product')
								tempObj.componentitems.push(item);

							if(item.itemid_itemtype == 'Service')
								tempObj.serviceitems.push(item);

							if(item.itemid_itemtype == 'Expense')
								tempObj.expenseitems.push(item);
						});

						this.props.initialize(tempObj);
					}
				}

				this.updateLoaderFlag(false);
			});
		},0);
	}
}

export function imageUpload (file) {
	this.updateLoaderFlag(true);
	let tempResourceobj = this.props.resource;
	tempResourceobj.parentresource = "servicereports";
	tempResourceobj.resourceName = "servicereports";
	tempResourceobj.parentid = tempResourceobj.id;
	tempResourceobj.imagename = "";

	let tempData = {
		actionverb : 'Save',
		data : tempResourceobj
	};

	const formData = new FormData();
	formData.append('file', file);
	formData.append('data', JSON.stringify(tempResourceobj));
	formData.append('actionverb', 'Save');

	const config = {
		headers: {
			'content-type': 'multipart/form-data'
		}
	};
	let fileName = file.name.substr(0, file.name.lastIndexOf('.'));

	if(!(/^[a-zA-Z0-9\.\,\_\-\'\!\*\(\)\ ]*$/.test(fileName))) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "File name contains unsupported special characters. Only following special characters are supported: _ - . , ( )",
			btnArray : ["Ok"]
		}));
	}

	if (file.size > 10485760) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "File size should not be more than 10 MB.",
			btnArray : ["Ok"]
		}));
	}

	if (!file.type.match('image.*')) {
		this.props.updateFormState(this.props.form, {
			image: null
		});
		let message = {
			header : "Error",
			body : "Invalid image format. Supported formats are JPG and PNG.",
			btnArray : ["Ok"]
		};
		this.props.openModal(modalService.infoMethod(message));
		this.updateLoaderFlag(false);
	} else {
		axios.post('/upload', formData, config).then((response) => {
			if (response.data.message == 'success') {
				let tempobj = response.data.main;
				tempobj.imagename = response.data.main.filename;
				tempobj.image = '';
				this.props.updateFormState(this.props.form, tempobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function removeImage() {
	this.props.updateFormState(this.props.form, {
		imageurl: null
	});
}

export function createServiceInvoice () {
	this.props.history.push({
		pathname: '/createServiceInvoice',
		params: {
			...this.props.resource,
			param: 'Service Reports'
		}
	});
}

export function cancel () {
	this.props.history.goBack();
}
