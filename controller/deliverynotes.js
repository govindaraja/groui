import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation, checkStatus } from '../utils/services';
import {ChildEditModal} from '../components/utilcomponents';
import StockdetailsModal from '../containers/stockdetails';
import EmailModal from '../components/details/emailmodal';
import DeliverynoteanalysedetailsModal from '../components/details/deliverynoteanalysedetailsmodal';
import DeliverynotereceiptpickingdetailsModal from '../components/details/deliverynotereceiptpickingdetailsmodal';
import DeliverynotepickingdetailsModal from '../components/details/deliverynotepickingdetailsmodal';
import DeliverynoteinstallationcontractdetailsModal from '../components/details/deliverynoteinstalltioncontractdetailsmodal';

let stockRoundOffPre = 5;

export function onLoad () {
	stockRoundOffPre = this.props.app.roundOffPrecisionStock;

	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let orderArray = [], deliveryidArray = [], deliverynoteArray = [], salesinvoiceArray = [], projectidArray = [], itemreqArray = [];
	let atLeastOneItemFound = false;

	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		deliverynotedate: new Date(new Date().setHours(0, 0, 0, 0)),
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		deliverynoteitems: [],
		kititemdeliverydetails: [],
		additionalcharges: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'Sales Orders for After Order') {
			tempObj.deliverynotefor = 'Sales Orders for After Order';

			utils.assign(tempObj, params, ['companyid', 'customerid', 'consigneeid', 'contactid', {'contactname': 'contactperson'}, {'contactphone': 'phone'}, {'contactmobile': 'mobile'}, {'contactemail': 'email'}, 'deliverycontactid', 'deliverycontactperson', 'deliverycontactphone', 'deliverycontactemail', 'deliverycontactmobile', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', 'deliveryaddress', 'deliveryaddressid', 'billingaddress', 'billingaddressid', {'customerreference' : 'ponumber'}, {'dispatchthrough' : 'modeofdespatch'}, 'salesperson', 'territoryid', {'dcorderno' : 'ponumber'}, {'dcorderdate' : 'orderdate'}, {'dcpodate' : 'podate'}, 'paymentterms', 'taxid', 'roundoffmethod']);

			this.customFieldsOperation('orders', tempObj, params, 'deliverynotes');

			params.orderitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;

				if(item.itemid_itemtype != 'Service' && item.quantity > item.deliveredqty) {
					if(orderArray.indexOf(item.parentid) == -1)
						orderArray.push(item.parentid);

					let tempChildObj = {
						index: index + 1,
						quantity: Number((item.quantity - item.deliveredqty).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'orderitems'
					};
					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_uomgroupid', 'itemid_itemgroupid', 'itemid_hasaddons', 'itemnamevariationid', 'itemnamevariationid_name', 'description', 'uomid', 'uomid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_isequipment', 'itemid_warrantycontracttypeid', 'warehouseid', {'sourceid' : 'id'}, {'orderitemsid' : 'id'}, 'uomconversionfactor', 'uomconversiontype', {'order_index' : 'index'}, 'baseitemrate', 'addonrate', 'rate', 'discountmode', 'discountquantity', 'taxid', {'orderid' : 'parentid'}, 'displaygroup', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('orderitems', tempChildObj, item, 'deliverynoteitems');
					tempObj.deliverynoteitems.push(tempChildObj);
				}
			});

			if(orderArray.length == 1)
				tempObj.orderid = orderArray[0];
		}

		if(params.param == "Sales Orders for On Demand") {
			tempObj.deliverynotefor = 'Sales Orders for On Demand';

			utils.assign(tempObj, params, ['companyid', 'customerid', 'consigneeid', 'contactid', {'contactname': 'contactperson'}, {'contactphone': 'phone'}, {'contactmobile': 'mobile'}, {'contactemail': 'email'}, 'deliverycontactid', 'deliverycontactperson', 'deliverycontactphone', 'deliverycontactemail', 'deliverycontactmobile', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', 'deliveryaddress', 'deliveryaddressid', 'billingaddress', 'billingaddressid', {'customerreference' : 'ponumber'}, {'dispatchthrough' : 'modeofdespatch'}, 'salesperson', 'territoryid', {'dcorderno' : 'ponumber'}, {'dcorderdate' : 'orderdate'}, {'dcpodate' : 'podate'}, 'paymentterms', 'taxid', 'roundoffmethod']);

			this.customFieldsOperation('orders', tempObj, params, 'deliverynotes');

			params.orderitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;

				if(item.itemid_itemtype != 'Service' && item.quantity > item.deliveredqty) {
					if(orderArray.indexOf(item.parentid) == -1)
						orderArray.push(item.parentid);

					let tempChildObj = {
						index: index + 1,
						quantity: Number((item.quantity - item.deliveredqty).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'orderitems'
					};
					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_uomgroupid', 'itemid_itemgroupid', 'itemid_hasaddons', 'itemnamevariationid', 'itemnamevariationid_name', 'description', 'uomid', 'uomid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'warehouseid', {'sourceid' : 'id'}, {'orderitemsid' : 'id'}, 'uomconversionfactor', 'uomconversiontype', {'order_index' : 'index'}, 'baseitemrate', 'addonrate', 'rate', 'discountmode', 'discountquantity', 'taxid', {'orderid' : 'parentid'}, 'displaygroup', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('orderitems', tempChildObj, item, 'deliverynoteitems');

					tempObj.deliverynoteitems.push(tempChildObj);
				}
			});

			if(orderArray.length == 1)
				tempObj.orderid = orderArray[0];
		}

		if(params.param == 'Sales Invoices') {
			tempObj.deliverynotefor = 'Sales Invoices';

			utils.assign(tempObj, params, ['companyid', 'customerid', 'consigneeid', 'contactid', {'contactname': 'contactperson'}, {'contactphone': 'phone'}, {'contactmobile': 'mobile'}, {'contactemail': 'email'}, 'deliverycontactid', 'deliverycontactperson', 'deliverycontactphone', 'deliverycontactemail', 'deliverycontactmobile', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', 'deliveryaddress', 'deliveryaddressid', 'billingaddress', 'billingaddressid', 'customerreference', {'dcorderno' : 'invorderno'}, {'dcorderdate' : 'invorderdate'}, {'dcpodate' : 'invpodate'}, 'invoiceno', 'invoicedate', 'salesperson', 'territoryid', 'taxid', 'dispatchthrough', 'vehicleno', 'roundoffmethod', 'paymentterms', 'ewaybillno']);

			this.customFieldsOperation('salesinvoices', tempObj, params, 'deliverynotes');

			params.salesinvoiceitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;
				if(item.itemid_itemtype != 'Service' && item.quantity > item.deliveredqty) {
					if(orderArray.indexOf(item.orderitemsid_parentid) == -1)
						orderArray.push(item.orderitemsid_parentid);

					if(salesinvoiceArray.indexOf(item.parentid) == -1)
						salesinvoiceArray.push(item.parentid);

					let tempChildObj = {
						index: index + 1,
						quantity: Number((item.quantity - item.deliveredqty).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'salesinvoiceitems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_hasaddons', 'itemid_uomgroupid', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'description', 'uomid', 'uomid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', {'warehouseid' : 'orderitemsid_warehouseid'}, {'sourceid' : 'id'}, {'orderitemsid' : 'orderitemsid'}, 'uomconversionfactor', 'uomconversiontype', {'order_index' : 'orderitemsid_index'}, 'baseitemrate', 'addonrate', 'rate', 'discountmode', 'discountquantity', 'taxid', {'orderid' : 'orderitemsid_parentid'}, 'displaygroup', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('salesinvoiceitems', tempChildObj, item, 'deliverynoteitems');

					tempObj.deliverynoteitems.push(tempChildObj);
				}
			});

			if(orderArray.length == 1)
				tempObj.orderid = orderArray[0];
			if(salesinvoiceArray.length == 1)
				tempObj.salesinvoiceid = salesinvoiceArray[0];
		}

		if(params.param == 'Purchase Returns') {
			tempObj.deliverynotefor = 'Purchase Returns';

			if (params.againstorder)
				tempObj['taxid'] = params.taxid;

			utils.assign(tempObj, params, [{'customerid' : 'partnerid'}, {'consigneeid' : 'partnerid'}, 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'deliverynotedate' : 'returndate'}, {'purchasereturnid' : 'id'}, 'warehouseid', {'purchasereturnid_againstorder': 'againstorder'}]);

			this.customFieldsOperation('purchasereturns', tempObj, params, 'deliverynotes');

			params.purchasereturnitems.forEach((item, index) => {
				item.qtyreturned = item.qtyreturned ? item.qtyreturned : 0;

				if (params.againstorder) {
				if(item.qtyauthorized > item.qtyreturned) {
					if(orderArray.indexOf(item.receiptnoteitemsid_parentid) == -1)
						orderArray.push(item.receiptnoteitemsid_parentid);

					let tempChildObj = {
						index: index + 1,
						quantity: Number((item.qtyauthorized - item.qtyreturned).toFixed(stockRoundOffPre)),
						alternateuom: item.receiptnoteitemsid_uomconversiontype ? true : false,
						sourceresource: 'purchasereturnitems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemid_uomgroupid', {'description' : 'receiptnoteitemsid_description'}, {'uomid' : 'receiptnoteitemsid_uomid'}, 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', {'warehouseid' : 'receiptnoteitemsid_warehouseid'}, {'sourceid' : 'id'}, {'uomconversionfactor' : 'receiptnoteitemsid_uomconversionfactor'}, {'uomconversiontype' : 'receiptnoteitemsid_uomconversiontype'}, {'receiptnote_index' : 'receiptnoteitemsid_index'}, {'receiptnoteid' : 'receiptnoteitemsid_parentid'}]);

					this.customFieldsOperation('purchasereturnitems', tempChildObj, item, 'deliverynoteitems');

					tempObj.deliverynoteitems.push(tempChildObj);
				}
				} else {
					if (item.qtyauthorized > item.qtyreturned) {
						let tempChildObj = {
							index: index + 1,
							quantity: Number((item.qtyauthorized - item.qtyreturned).toFixed(stockRoundOffPre)),
							alternateuom: item.uomconversiontype ? true : false,
							sourceresource: 'purchasereturnitems'
						};

						utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemid_uomgroupid', 'description', 'uomid', 'uomid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'warehouseid', 'remarks', {'sourceid' : 'id'}, 'uomconversionfactor', 'uomconversiontype', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

						tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

						this.customFieldsOperation('purchasereturnitems', tempChildObj, item, 'deliverynoteitems');

						tempObj.deliverynoteitems.push(tempChildObj);
					}
				}
			});
		}

		if(params.param == 'Item Requests for Service Call') {
			tempObj.deliverynotefor = 'Item Requests for Service Call';

			utils.assign(tempObj, params, ['customerid', {'consigneeid' : 'customerid'}, 'companyid', 'defaultcostcenter', 'currencyid', 'currencyexchangerate', {'itemrequestid' : 'id'}, 'servicecallid', 'engineerid', 'salesperson', 'warehouseid', 'deliveryaddress','territoryid']);

			this.customFieldsOperation('itemrequests', tempObj, params, 'deliverynotes');

			params.itemrequestitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;

				if(item.quantity > item.deliveredqty) {
					if(itemreqArray.indexOf(item.parentid) == -1)
						itemreqArray.push(item.parentid);

					let tempChildObj = {
						index: index + 1,
						quantity: Number((item.quantity - item.deliveredqty).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'itemrequestitems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemid_uomgroupid', 'description', 'uomid', 'uomid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch','itemid_itemgroupid', {'sourceid' : 'id'}, 'uomconversionfactor', 'uomconversiontype', 'warehouseid', {'itemrequest_index' : 'index'}, {'itemrequestid' : 'parentid'},	'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('itemrequestitems', tempChildObj, item, 'deliverynoteitems');

					tempObj.deliverynoteitems.push(tempChildObj);
				}
			});
		}

		if(params.param == 'Item Requests for Engineer' || params.param == "Item Requests for General" || params.param == "Item Requests for Job Work") {
			tempObj.deliverynotefor = params.param;

			utils.assign(tempObj, params, ['companyid', 'defaultcostcenter', 'workorderid', 'customerid', 'currencyid', 'currencyexchangerate', {'itemrequestid' : 'id'}, 'engineerid', 'salesperson', 'warehouseid',{'consigneeid':'customerid'},'deliveryaddressid','deliveryaddress','territoryid']);

			this.customFieldsOperation('itemrequests', tempObj, params, 'deliverynotes');

			params.itemrequestitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;

				if(item.quantity > item.deliveredqty) {
					if(itemreqArray.indexOf(item.parentid) == -1)
						itemreqArray.push(item.parentid);

					let tempChildObj = {
						index: index + 1,
						quantity: Number((item.quantity - item.deliveredqty).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'itemrequestitems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemid_uomgroupid', 'description', 'uomid', 'uomid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch','itemid_itemgroupid', {'sourceid' : 'id'}, 'uomconversionfactor', 'uomconversiontype', 'warehouseid', {'itemrequest_index' : 'index'}, {'itemrequestid' : 'parentid'}, 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('itemrequestitems', tempChildObj, item, 'deliverynoteitems');
					tempObj.deliverynoteitems.push(tempChildObj);
				}
			});
		}

		if(params.param == 'Item Requests for Project') {
			tempObj.deliverynotefor = 'Item Requests for Project';

			utils.assign(tempObj, params, ['customerid', 'defaultcostcenter', {'consigneeid' : 'projectid_consigneeid'}, 'companyid', 'currencyid', 'currencyexchangerate', {'itemrequestid' : 'id'}, 'projectid', 'warehouseid', {'deliveryaddress' : 'projectid_deliveryaddress'}, {'billingaddress' : 'projectid_billingaddress'}, {'deliveryaddressid' : 'projectid_deliveryaddressid'}, {'billingaddressid' : 'projectid_billingaddressid'},'territoryid']);

			this.customFieldsOperation('itemrequests', tempObj, params, 'deliverynotes');

			params.itemrequestitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;

				if(item.quantity > item.deliveredqty) {
					if(itemreqArray.indexOf(item.parentid) == -1)
						itemreqArray.push(item.parentid);

					let tempChildObj = {
						index: index + 1,
						quantity: Number((item.quantity - item.deliveredqty).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'itemrequestitems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemid_uomgroupid', 'description', 'uomid', 'uomid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch','itemid_itemgroupid', {'sourceid' : 'id'}, 'uomconversionfactor', 'uomconversiontype', 'warehouseid', {'itemrequest_index' : 'index'}, {'itemrequestid' : 'parentid'},	'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('itemrequestitems', tempChildObj, item, 'deliverynoteitems');
					tempObj.deliverynoteitems.push(tempChildObj);
				}
			});
		}

		if(params.param == 'Project Delivery Planner') {
			tempObj.deliverynotefor = 'Item Requests for Project';

			utils.assign(tempObj, params.itemrequestitems[0], ['customerid', 'defaultcostcenter', {'consigneeid' : 'projectid_consigneeid'}, 'companyid', 'currencyid', 'currencyexchangerate', 'itemrequestid' , 'projectid', 'warehouseid', {'deliveryaddress' : 'projectid_deliveryaddress'}, {'billingaddress' : 'projectid_billingaddress'}, {'deliveryaddressid' : 'projectid_deliveryaddressid'}, {'billingaddressid' : 'projectid_billingaddressid'}, 'defaultcostcenter']);

			this.customFieldsOperation('itemrequests', tempObj, params, 'deliverynotes');

			params.itemrequestitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;

				if(item.qtyrequest > item.deliveredqty) {
					if(itemreqArray.indexOf(item.itemrequestid) == -1)
						itemreqArray.push(item.itemrequestid);

					let toBeDeliverdQty = Number((item.qtyrequest - item.deliveredqty).toFixed(stockRoundOffPre));

					if(item._pickedQty > 0 && item._pickedQty < toBeDeliverdQty)
						toBeDeliverdQty = item._pickedQty;

					let tempChildObj = {
						index: index + 1,
						quantity: toBeDeliverdQty,
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'itemrequestitems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemid_uomgroupid', 'description', 'uomid', 'uomid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'sourceid', 'uomconversionfactor', 'uomconversiontype', {'warehouseid' : 'itemwarehouseid'}, {'itemrequest_index' : 'index'}, 'itemrequestid', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('itemrequestitems', tempChildObj, item, 'deliverynoteitems');
					tempObj.deliverynoteitems.push(tempChildObj);
				}
			});
		}

		if(params.param == 'Item Requests for Production Order') {
			tempObj.deliverynotefor = 'Item Requests for Production Order';

			utils.assign(tempObj, params, ['companyid', {'itemrequestid' : 'id'}, 'productionorderid', 'warehouseid', 'defaultcostcenter']);

			this.customFieldsOperation('itemrequests', tempObj, params, 'deliverynotes');

			params.itemrequestitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty? item.deliveredqty : 0;

				if(item.quantity > item.deliveredqty) {
					let tempChildObj = {
						index: index + 1,
						quantity: Number((item.quantity - item.deliveredqty).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'itemrequestitems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemid_uomgroupid', 'description', 'uomid', 'uomid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch','itemid_itemgroupid', {'sourceid' : 'id'}, 'uomconversionfactor', 'uomconversiontype', 'warehouseid']);

					this.customFieldsOperation('itemrequestitems', tempChildObj, item, 'deliverynoteitems');
					tempObj.deliverynoteitems.push(tempChildObj);
				}
			});
		}

		if(params.param == 'Stock Transfer') {
			tempObj.deliverynotefor = 'Stock Transfer';

			utils.assign(tempObj, params, ['companyid', {'stocktransferid' : 'id'}, {'warehouseid' : 'sourcewarehouseid'}, {'deliveryaddress': 'destinationwarehouseid_address'}]);

			this.customFieldsOperation('stocktransfer', tempObj, params, 'deliverynotes');

			params.stocktransferitems.forEach((item, index) => {
				item.deliveredqty = item.deliveredqty ? item.deliveredqty : 0;

				if(item.quantity > item.deliveredqty) {
					let tempChildObj = {
						index: index + 1,
						quantity: Number((item.quantity - item.deliveredqty).toFixed(stockRoundOffPre)),
						warehouseid: params.sourcewarehouseid,
						sourceresource: 'stocktransferitems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name',  'itemid_uomgroupid', 'description', 'uomid', 'uomid_name', 'itemid_keepstock', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', {'sourceid' : 'id'}]);

					this.customFieldsOperation('stocktransferitems', tempChildObj, item, 'deliverynoteitems');
					tempObj.deliverynoteitems.push(tempChildObj);
				}
			});
		}
	} else {
		tempObj.deliverynotefor = 'Other';
	}

	if(this.props.location.params && tempObj.deliverynoteitems.length == 0) {
		this.props.initialize({
			currencyid: this.props.app.defaultCurrency,
			companyid: this.props.app.selectedcompanyid,
			roundoffmethod: this.props.app.defaultRoundOffMethod,
			deliverynotedate: new Date(new Date().setHours(0, 0, 0, 0)),
			salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
			deliverynoteitems: [],
			kititemdeliverydetails: [],
			additionalcharges: []
		});
		let apiResponse = commonMethods.apiResult({
			data: {
				message: 'Items are Already Delivered'
			}
		});
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	} else {
		this.props.initialize(tempObj);
	}

	setTimeout(() => {
		if(this.props.location.params) {
			if(this.props.location.params.param == 'Sales Orders for After Order' || this.props.location.params.param == 'Sales Orders for On Demand') {
				this.controller.getKititems(orderArray , 'kititemorderdetails');
				this.controller.getAdditionalCharges(orderArray, 'orders');
			}
			if(this.props.location.params.param == 'Sales Invoices') {
				this.controller.getKititems(orderArray, 'kititemorderdetails');
				this.controller.getAdditionalCharges(salesinvoiceArray, 'salesinvoices');
			}

			if(this.props.location.params.param == 'Purchase Returns')
				this.controller.getKititems(orderArray, 'kititemreceiptdetails');

			if(['Item Requests for Project', 'Item Requests for Engineer', 'Item Requests for Service Call', 'Item Requests for Production Order', 'Item Requests for General', 'Project Delivery Planner'].indexOf(this.props.location.params.param) >= 0) {
				this.controller.getKititems(itemreqArray, 'kititemitemrequestdetails');

				if(['Project Delivery Planner', 'Item Requests for Project'].includes(this.props.location.params.param) && this.props.app.feature.showAmountinDelivery) {
					this.controller.getItemRateFromProject();
				}
			}

			this.controller.computeFinalRate();
		}
	}, 0);
	
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/deliverynotes/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.deliverynoteitems.forEach((item) => {
				if(item.uomconversiontype)
					item.alternateuom = true;
			});
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			this.controller.checkVisibilityForButton();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function checkVisibilityForButton () {
	setTimeout(() => {
		let showSalesInvoiceBtn = false;
		this.props.resource.deliverynoteitems.forEach((item) => {
			let qty = Number(item.quantity.toFixed(this.props.app.roundOffPrecisionStock));
			let invoicedqty = Number((item.invoicedqty || 0).toFixed(this.props.app.roundOffPrecisionStock));
			if(item.sourceresource == 'orderitems' && (qty > invoicedqty))
				showSalesInvoiceBtn = true
		});
		this.setState({
			showSalesInvoiceBtn: showSalesInvoiceBtn
		});
	}, 0);
}

export function getKititems (array, param) {
	if(array.length == 0)
		return null;

	let url, parentidfield, parentindexfield;
	if(param == 'kititemorderdetails') {
		url=`/api/kititemorderdetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid,rate,addondisplaygroup,addondisplayorder&filtercondition=kititemorderdetails.parentid in (${array.join()})`;
		parentidfield = 'orderid';
		parentindexfield = 'order_index';
	} else if(param == 'kititemreceiptdetails') {
		url = `/api/kititemreceiptdetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid,addondisplaygroup,addondisplayorder&filtercondition=kititemreceiptdetails.parentid in (${array.join()})`;
		parentidfield = 'receiptnoteid';
		parentindexfield = 'receiptnote_index';
	} else if(param == 'kititemitemrequestdetails') {
		url = `/api/kititemitemrequestdetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid,addondisplaygroup,addondisplayorder&filtercondition=kititemitemrequestdetails.parentid in (${array.join()})`;
		parentidfield = 'itemrequestid';
		parentindexfield = 'itemrequest_index';
	} else {
		return null;
	}

	axios.get(url).then((response) => {
		if(response.data.message == 'success') {
			let kititemdeliverydetails = [];
			let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;
			this.props.resource.deliverynoteitems.forEach((item) => {
				for (var j = 0; j < response.data.main.length; j++) {
					if(response.data.main[j].parentid == item[parentidfield] && item[parentindexfield] == response.data.main[j].rootindex) {
						kititemdeliverydetails.push({
							itemid: response.data.main[j].itemid,
							itemid_name: response.data.main[j].itemid_name,
							parent: response.data.main[j].parent,
							uomid: response.data.main[j].uomid,
							uomid_name: response.data.main[j].uomid_name,
							quantity: response.data.main[j].quantity,
							rate: response.data.main[j].rate,
							conversion: response.data.main[j].conversion,
							itemid_issaleskit: response.data.main[j].itemid_issaleskit,
							itemid_hasserial: response.data.main[j].itemid_hasserial,
							itemid_hasbatch: response.data.main[j].itemid_hasbatch,
							itemid_keepstock: response.data.main[j].itemid_keepstock,
							rootindex: item.index,
							warehouseid: item.warehouseid,
							index: response.data.main[j].index,
							parentindex: response.data.main[j].parentindex,
							addondisplaygroup: response.data.main[j].addondisplaygroup,
							addondisplayorder: response.data.main[j].addondisplayorder
						});
					}
				}
			});
			this.props.resource.deliverynoteitems.forEach((invitem) => {
				for (var i = 0; i < kititemdeliverydetails.length; i++) {
					if (invitem.index == kititemdeliverydetails[i].rootindex) {
						if (!kititemdeliverydetails[i].parentindex) {
							let tempIndex = kititemdeliverydetails[i].index;
							let tempQuantity = Number((kititemdeliverydetails[i].conversion * invitem.quantity).toFixed(roundOffPrecisionStock));
							kititemdeliverydetails[i].quantity = tempQuantity;
							calculateQuantity(tempIndex, tempQuantity, invitem.index);
						}
					}
				}
			});
			function calculateQuantity(tempIndex, tempQuantity, index) {
				for (var j = 0; j < kititemdeliverydetails.length; j++) {
					if (tempIndex == kititemdeliverydetails[j].parentindex && index == kititemdeliverydetails[j].rootindex) {
						let sectempQty = Number((kititemdeliverydetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
						kititemdeliverydetails[j].quantity = sectempQty;
						calculateQuantity(kititemdeliverydetails[j].index, sectempQty, index);
					}
				}
			}

			this.props.updateFormState(this.props.form, {
				kititemdeliverydetails
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getItemRateFromProject() {
	let itemrequestitemsidArray = [];

	this.props.resource.deliverynoteitems.forEach((item) => {
		if(item.sourceresource == 'itemrequestitems') {
			itemrequestitemsidArray.push(item.sourceid);
		}
	});

	if(itemrequestitemsidArray.length > 0) {
		axios.get(`/api/itemrequestitems?field=id,boqitemsid,projectestimationitemsid,projectitems/itemid/boqitemsid,projectitems/rate/boqitemsid,projectitems/discountmode/boqitemsid,projectitems/discountquantity/boqitemsid,projectestimationitems/rate/projectestimationitemsid&filtercondition=itemrequestitems.id IN (${itemrequestitemsidArray})`).then((response) => {
			if (response.data.message == 'success') {

				let itemrequestitemObj = {}, tempObj = {};
				response.data.main.forEach((reqitem) => {
					if(!itemrequestitemObj[reqitem.id]) {
						itemrequestitemObj[reqitem.id] = reqitem;
					}
				});

				this.props.resource.deliverynoteitems.forEach((item, index) => {
					if(itemrequestitemObj[item.sourceid] && item.sourceresource == 'itemrequestitems') {

						tempObj[`deliverynoteitems[${index}].rate`] = itemrequestitemObj[item.sourceid].projectestimationitemsid_rate;

						if(itemrequestitemObj[item.sourceid].boqitemsid > 0 && itemrequestitemObj[item.sourceid].boqitemsid_itemid == item.itemid) {
							tempObj[`deliverynoteitems[${index}].rate`] = itemrequestitemObj[item.sourceid].boqitemsid_rate;
						}

						tempObj[`deliverynoteitems[${index}].discountquantity`] = itemrequestitemObj[item.sourceid].boqitemsid > 0 ? itemrequestitemObj[item.sourceid].boqitemsid_discountquantity : itemrequestitemObj[item.sourceid].projectestimationitemsid_discountquantity;

						tempObj[`deliverynoteitems[${index}].discountmode`] = itemrequestitemObj[item.sourceid].boqitemsid > 0 ? itemrequestitemObj[item.sourceid].boqitemsid_discountmode : (itemrequestitemObj[item.sourceid].projectestimationitemsid > 0 ?  itemrequestitemObj[item.sourceid].projectestimationitemsid_discountmode : 'Percentage');
					}
				});

				this.props.updateFormState(this.props.form, tempObj);
				this.controller.computeFinalRate();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}
}

export function currencyOnChange (id, valueobj) {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(id);

		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function callBackConsigneeName (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		consigneeid_name: valueobj.name,
		deliveryaddress: null,
		deliveryaddressid: null,
		deliverycontactid: null,
		deliverycontactperson: null,
		deliverycontactmobile: null,
		deliverycontactphone: null,
		deliverycontactemail: null
	});
}

export function callBackCustomer (id, valueobj) {
	let tempObj = {
		consigneeid: valueobj.id,
		territoryid: valueobj.territory,
		salesperson: this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		billingaddress: null,
		billingaddressid: null,
		contactid: valueobj.contactid,
		contactemail: valueobj.contactid_email,
		contactmobile: valueobj.contactid_mobile,
		contactphone: valueobj.contactid_phone,
		contactname: valueobj.contactid_name,
		contactid_name: valueobj.contactid_name,
		contactid_email: valueobj.contactid_email,
		contactid_mobile: valueobj.contactid_mobile,
		contactid_phone: valueobj.contactid_phone,
		deliverycontactid: valueobj.contactid,
		deliverycontactperson: valueobj.contactid_name,
		deliverycontactphone: valueobj.contactid_phone,
		deliverycontactemail: valueobj.contactid_email,
		deliverycontactmobile: valueobj.contactid_mobile,
		deliverycontactid_name: valueobj.contactid_name,
		deliverycontactid_phone: valueobj.contactid_phone,
		deliverycontactid_email: valueobj.contactid_email,
		deliverycontactid_mobile: valueobj.contactid_mobile
	};
	this.customFieldsOperation('partners', tempObj, valueobj, 'deliverynotes');
	this.props.updateFormState(this.props.form, tempObj);
}

export function contactpersonChange (id, valueobj, type) {
	let tempObj = {};
	if(type == 'billing') {
		tempObj = {
			contactname: valueobj.name,
			contactphone: valueobj.phone,
			contactemail: valueobj.email,
			contactmobile: valueobj.mobile,
			contactid_name: valueobj.name,
			contactid_phone: valueobj.phone,
			contactid_email: valueobj.email,
			contactid_mobile: valueobj.mobile
		};
	} else {
		tempObj = {
			deliverycontactperson: valueobj.name,
			deliverycontactphone: valueobj.phone,
			deliverycontactemail: valueobj.email,
			deliverycontactmobile: valueobj.mobile,
			deliverycontactid_name: valueobj.name,
			deliverycontactid_phone: valueobj.phone,
			deliverycontactid_email: valueobj.email,
			deliverycontactid_mobile: valueobj.mobile
		};
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
	}
	if(address == 'equipmentaddress') {
		tempObj.equipmentaddress = addressobj.displayaddress;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackEngineer (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson: valueobj.id
	});
}

export function getAdditionalCharges (array, parentResource) {
	if(array.length == 0)
		return null;

	axios.get(`/api/additionalcharges?field=name,amount,applyforvaluation,parentid,parentresource,additionalchargesid,additionalchargesmaster/name/additionalchargesid,description,taxcodeid,defaultpercentage,displayorder&filtercondition=additionalcharges.parentresource='${parentResource}' and additionalcharges.parentid in (${array.join()})`).then((response) => {
		if (response.data.message == 'success') {
			let additionalcharges = response.data.main.sort((a, b) => a.displayorder - b.displayorder);
			this.props.updateFormState(this.props.form, {
				additionalcharges
			});
			this.controller.computeFinalRate();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function callBackWarehouseAddress (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		goodsissueaddress: valueobj.address
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = valueobj.parentid;
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.rate`] = returnObject.rate;
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = false;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.drawingno`] = returnObject.drawingno;
		tempObj[`${itemstr}.warehouseid`] = this.props.resource.warehouseid;

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'deliverynoteitems', itemstr);

		if (item.index) {
			let itemCount = 0;
			tempObj.kititemdeliverydetails = [...this.props.resource.kititemdeliverydetails];
			for (var i = 0; i < tempObj.kititemdeliverydetails.length; i++) {
				if (tempObj.kititemdeliverydetails[i].rootindex == item.index)
					itemCount++;
			}
			for (var i = 0; i < itemCount; i++) {
				for (var j = 0; j < tempObj.kititemdeliverydetails.length; j++) {
					if (tempObj.kititemdeliverydetails[j].rootindex == item.index) {
						tempObj.kititemdeliverydetails.splice(j, 1);
						break;
					}
				}
			}

			if (returnObject.itemid_issaleskit) {
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = item.index;
					tempObj.kititemdeliverydetails.push(returnObject.kititems[i]);
				}
			}
		} else {
			let index = 0;
			for (var i = 0; i < this.props.resource.deliverynoteitems.length; i++) {
				if (this.props.resource.deliverynoteitems[i].index > index)
					index = this.props.resource.deliverynoteitems[i].index;
			}
			tempObj[`${itemstr}.index`] = index + 1;

			if (returnObject.itemid_issaleskit) {
				tempObj.kititemdeliverydetails = [...this.props.resource.kititemdeliverydetails];
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = tempObj[`${itemstr}.index`];
					tempObj.kititemdeliverydetails.push(returnObject.kititems[i]);
				}
			}
		}

		if(tempObj[`${itemstr}.itemid_hasaddons`]) {
			tempObj[`${itemstr}.baseitemrate`] = tempObj[`${itemstr}.rate`];
			tempObj[`${itemstr}.addonrate`] = 0;
		}

		this.props.updateFormState(this.props.form, tempObj);
		setTimeout(() => {
			let tempchilditem = this.selector(this.props.fullstate, itemstr);
			this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
			this.controller.warehouseonchange(tempchilditem.warehouseid, tempchilditem, itemstr);
		}, 0);

	}, (reason) => {});
}

export function quantityOnChange(itemqty, item, itemstr) {
	let kititemdeliverydetails = [...this.props.resource.kititemdeliverydetails];
	let tempObj = {};
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;

	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
	}

	for (var i = 0; i < kititemdeliverydetails.length; i++) {
		if (item.index == kititemdeliverydetails[i].rootindex) {
			if (!kititemdeliverydetails[i].parentindex) {
				let tempIndex = kititemdeliverydetails[i].index;
				let tempQuantity = Number((kititemdeliverydetails[i].conversion * itemqty).toFixed(roundOffPrecisionStock));
				tempObj[`kititemdeliverydetails[${i}].quantity`] = tempQuantity;
				calculateQuantity(tempIndex, tempQuantity, item.index);
			}
		}
	}

	function calculateQuantity(tempIndex, tempQuantity, index) {
		for (var j = 0; j < kititemdeliverydetails.length; j++) {
			if (tempIndex == kititemdeliverydetails[j].parentindex && index == kititemdeliverydetails[j].rootindex) {
				let sectempQty = Number((kititemdeliverydetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
				tempObj[`kititemdeliverydetails[${j}].quantity`] = sectempQty;
				calculateQuantity(kititemdeliverydetails[j].index, sectempQty, index);
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};
	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function conversionOnChange (item) {
	for (var i = 0; i < this.props.resource.deliverynoteitems.length; i++) {
		if (item.rootindex == this.props.resource.deliverynoteitems[i].index) {
			this.controller.quantityOnChange(this.props.resource.deliverynoteitems[i].quantity, this.props.resource.deliverynoteitems[i], `deliverynoteitems[${i}]`);
			break;
		}
	}
}

export function computeFinalRate() {
	setTimeout(() =>{
		let tempObj = {};
		for (var i = 0; i < this.props.resource.deliverynoteitems.length; i++) {
			if (this.props.resource.deliverynoteitems[i].itemid_hasaddons) {
				tempObj[`deliverynoteitems[${i}].rate`] = (this.props.resource.deliverynoteitems[i].baseitemrate ? this.props.resource.deliverynoteitems[i].baseitemrate : 0) + ((this.props.resource.deliverynoteitems[i].addonrate ? this.props.resource.deliverynoteitems[i].addonrate : 0));
			}
		}
		this.props.updateFormState(this.props.form, tempObj);

		taxEngine(this.props, 'resource', 'deliverynoteitems');
	}, 0);
}

export function warehouseonchange(value, item, itemstr) {
	let tempObj = {
		[`${itemstr}.pickingdetails`]: null,
		[`${itemstr}.receiptpickingdetails`]: null,
	};
	this.props.resource.kititemdeliverydetails.forEach((kititem, kitindex) => {
		if(kititem.rootindex == item.index) {
			tempObj[`kititemdeliverydetails[${kitindex}].warehouseid`] = value;
			tempObj[`kititemdeliverydetails[${kitindex}].pickingdetails`] = null;
			tempObj[`kititemdeliverydetails[${kitindex}].receiptpickingdetails`] = null;
		}
	});
	console.log(tempObj, item.index, this.props.resource.kititemdeliverydetails)
	this.props.updateFormState(this.props.form, tempObj);
}

export function manualReceiptPickingChange(itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.receiptpickingdetails`]: null
	});
}

export function deleteDeliveryItem (index) {
	if(this.props.resource.deliverynotefor == 'Sales Orders for After Order' || this.props.resource.deliverynotefor == 'Sales Orders for On Demand' || this.props.resource.deliverynotefor == 'Sales Invoices' || this.props.resource.deliverynotefor == 'Other' || this.props.resource.deliverynotefor == 'Item Requests for Project' || this.props.resource.deliverynotefor == 'Item Requests for Engineer' || this.props.resource.deliverynotefor == 'Item Requests for Service Call' || this.props.resource.deliverynotefor == 'Item Requests for Production Order' || this.props.resource.deliverynotefor == 'Projects' || this.props.resource.deliverynotefor == 'Item Requests for General' || this.props.resource.deliverynotefor == 'Item Requests for Job Work') {
		let itemCount = 0;
		let kititemdeliverydetails = [...this.props.resource.kititemdeliverydetails];
		let deliverynoteitems = [...this.props.resource.deliverynoteitems];

		for (var i = 0; i < kititemdeliverydetails.length; i++) {
			if (kititemdeliverydetails[i].rootindex == this.props.resource.deliverynoteitems[index].index)
				itemCount++;
		}
		for (var i = 0; i < itemCount; i++) {
			for (var j = 0; j < kititemdeliverydetails.length; j++) {
				if (kititemdeliverydetails[j].rootindex == this.props.resource.deliverynoteitems[index].index) {
					kititemdeliverydetails.splice(j, 1);
					break;
				}
			}
		}
		deliverynoteitems.splice(index, 1);

		this.props.updateFormState(this.props.form, {
			kititemdeliverydetails,
			deliverynoteitems
		});
	} else {
		let apiResponse = commonMethods.apiResult({
			data:{
				message: 'No Access to delete Sales Return Items or Stock Transfer Items'
			}
		});
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
	this.controller.computeFinalRate();
}

export function deleteKitItem(index) {
	if(this.props.resource.deliverynotefor == 'Other') {
		let errorArray = [];
		let itemfound = false;
		let kititemdeliverydetails = [...this.props.resource.kititemdeliverydetails];

		for (var i = 0; i < kititemdeliverydetails.length; i++) {
			if (index != i && kititemdeliverydetails[index].rootindex == kititemdeliverydetails[i].rootindex && kititemdeliverydetails[index].parentindex == kititemdeliverydetails[i].parentindex) {
				itemfound = true;
			}
		}
		if (!itemfound)
			errorArray.push("Item is a only child for parent. Delete the Parent item");

		let itemCouFound = false;
		for (var i = 0; i < kititemdeliverydetails.length; i++) {
			if (index != i && kititemdeliverydetails[index].rootindex == kititemdeliverydetails[i].rootindex && !kititemdeliverydetails[i].parentindex)
				itemCouFound = true;
		}
		if (!itemCouFound)
			errorArray.push("Item is a only child for parent. Delete the Parent item");

		if (errorArray.length == 0) {
			let parentIndexArray = [kititemdeliverydetails[index].index];
			let rootindex = kititemdeliverydetails[index].rootindex;
			deleteArray(kititemdeliverydetails[index].rootindex, kititemdeliverydetails[index].index);

			function deleteArray(rootindex, index) {
				for (var i = 0; i < kititemdeliverydetails.length; i++) {
					if (kititemdeliverydetails[i].rootindex == rootindex && kititemdeliverydetails[i].parentindex == index) {
						parentIndexArray.push(kititemdeliverydetails[i].index)
						deleteArray(rootindex, kititemdeliverydetails[i].index);
					}
				}
			}

			for (var i = 0; i < parentIndexArray.length; i++) {
				for (var j = 0; j < kititemdeliverydetails.length; j++) {
					if (kititemdeliverydetails[j].rootindex == rootindex && kititemdeliverydetails[j].index == parentIndexArray[i]) {
						kititemdeliverydetails.splice(j, 1);
						break;
					}
				}
			}

			this.props.updateFormState(this.props.form, {kititemdeliverydetails});
		} else {
			let response = {
				data : {
					message : 'failure',
					error : utils.removeDuplicate(errorArray)
				}
			};
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	} else {
		let apiResponse = commonMethods.apiResult({
			data:{
				message: "You can't delete kit items in this transaction"
			}
		});
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	var extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Customer',
			data : emailObj
		},
		url : '/api/deliverynotes'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function setitempickingdetails(param, confirm) {
	let tempObj = this.props.resource;
	let updateObj = {};
	['deliverynoteitems', 'kititemdeliverydetails'].forEach((prop) => {
		tempObj[prop].forEach((item, index) => {
			if(!item.pickingdetails || !item.pickingdetails.details) {
				updateObj[`${prop}[${index}].pickingdetails`] = {
					details: []
				};
			}
		});
	});

	this.props.updateFormState(this.props.form, updateObj);

	setTimeout(() => {
		this.controller.save(param, confirm, true);
	}, 0);
}

export function save (param, confirm, pickingflag,confirm2) {

	if(param != 'Dispatch' && param != 'Cancel' && param != 'Delete' && param != 'Send To Customer' && !pickingflag) {
		return this.controller.setitempickingdetails(param, confirm);
	}

	if(param != 'Send To Customer' && param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm2 && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
		let message = {
			header : 'Cost Center Alert',
			body : `Cost Center not selected. Do you want to ${param} ?`,
			btnArray : ['Yes','No']
		};

		return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
			if(resparam)
				this.controller.save(param, confirm, pickingflag,true);
			else
				this.updateLoaderFlag(false);
		}));	
	}

	if (param == 'Send To Customer') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		return this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'deliverynotes'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});

	}

	this.updateLoaderFlag(true);

	if(param !='Update' && param !='Cancel' && param !='Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/deliverynotes'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (secparam) => {
			if(secparam)
				this.controller.save(param, true, pickingflag,confirm2);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/deliverynotes/${response.data.main.id}`);
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/deliverynotes");
				} else {
					this.props.initialize(response.data.main);
					this.controller.checkVisibilityForButton();
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function createInvoice () {
	let promise = checkStatus.salesinvoice(this.props.resource);
	promise.then((returnObject) => {
		let temparray = [];
		for (var i = 0; i < returnObject.length; i++) {
			if (returnObject[i].status == 'Hold' || returnObject[i].status == 'Closed') {
				temparray.push("Order "+returnObject[i].orderno+" for item "+returnObject[i].itemid_name+" is not Approved");
			}
		}
		if(temparray.length > 0) {
			let message = {
				header : 'Warning',
				body : temparray,
				btnArray : ['Ok']
			};
			this.props.openModal(modalService.infoMethod(message));
		} else {
			this.updateLoaderFlag(true);
			checkTransactionExist('salesinvoices', 'deliverynotes', this.props.resource.id, this.openModal, (param) => {
				if(param) {
					this.props.history.push({pathname: '/createSalesInvoice', params: {...this.props.resource, param: 'Delivery Notes'}});
				}
				this.updateLoaderFlag(false);
			});
		}
	}, (reason)=> {});
}

export function createServiceInvoice() {
	this.updateLoaderFlag(true);
	axios.get(`/api/itemrequests/${this.props.resource.itemrequestid}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			this.props.resource.deliverynoteitems.forEach((item) => {
				for(var i=0; i< tempObj.itemrequestitems.length; i++) {
					if(item.sourceresource == 'itemrequestitems' && item.sourceid > 0 && item.sourceid == tempObj.itemrequestitems[i].id) {
						tempObj.itemrequestitems[i].dcitemfound = true;
						tempObj.itemrequestitems[i].dcquantity = item.quantity;
						break;
					}
				}
			});
			for (var i = 0; i < tempObj.itemrequestitems.length; i++) {
				if (!tempObj.itemrequestitems[i].dcitemfound) {
					tempObj.itemrequestitems.splice(i, 1);
					i = -1;
				}
			}

			this.props.history.push({pathname: '/createServiceInvoice', params: {...response.data.main, param: 'Item Requests'}});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});	
}

export function createReceipt () {
	this.updateLoaderFlag(true);
	axios.get(`/api/stocktransfer/${this.props.resource.stocktransferid}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.history.push({pathname: '/createReceiptNote', params: {...response.data.main, param: 'Stock Transfer'}});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function analyseDeliveryNote () {
	this.updateLoaderFlag(true);
	let itemidarr = [];

	for(var i = 0; i < this.props.resource.deliverynoteitems.length; i++) {
		itemidarr.push(this.props.resource.deliverynoteitems[i].itemid);
	}

	if(itemidarr.length > 0) {
		axios.get(`/api/query/analysedeliverynoteitemquery?itemid=${itemidarr.toString()}`).then((response) => {
			let analyseitemArray = response.data.itemarray;
			for (var i = 0; i < this.props.resource.deliverynoteitems.length; i++) {
				for (var j = 0; j < analyseitemArray.length; j++) {
					if (this.props.resource.deliverynoteitems[i].itemid == analyseitemArray[j].id) {
						analyseitemArray[j].itemid_totalweight = this.props.resource.deliverynoteitems[i].quantity * analyseitemArray[j].itemid_weight;
						analyseitemArray[j].itemid_totalvolume = this.props.resource.deliverynoteitems[i].quantity * analyseitemArray[j].itemid_volume;
					}
				}
			}

			this.updateLoaderFlag(false);

			this.openModal({render: (closeModal) => {return <DeliverynoteanalysedetailsModal analyseitemArray={analyseitemArray} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}});
		});
	}
}

export function openPickDetails(item, kititemparam, itemstr) {
	let getBody = (closeModal) => {
		return (
			<DeliverynotepickingdetailsModal />
		)
	}
	this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} itemstr={itemstr} app={this.props.app} updateFormState={this.props.updateFormState} openModal={this.openModal} closeModal={closeModal} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function openReceiptPickDetails(item, kititemparam, itemstr) {
	let getBody = (closeModal) => {
		return (
			<DeliverynotereceiptpickingdetailsModal />
		)
	}
	this.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} itemstr={itemstr} app={this.props.app} updateFormState={this.props.updateFormState} kititemparam={kititemparam} openModal={this.openModal} closeModal={closeModal} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function openStockDetails (item) {
	this.openModal({render: (closeModal) => {return <StockdetailsModal resource = {this.props.resource} item={item} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function createContract () {
	this.openModal({render: (closeModal) => {return <DeliverynoteinstallationcontractdetailsModal resource={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} array={this.props.array} openModal={this.openModal} updateLoaderFlag={this.updateLoaderFlag} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function cancel () {
	this.props.history.goBack();
}
