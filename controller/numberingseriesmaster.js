import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initializeState();
	else
		this.controller.getItemById();
}

export function initializeState () {
	let tempObj = {
		companyid: this.props.app.selectedcompanyid,
		currentvalue : 0,
		isdefault : false
	};

	this.props.initialize(tempObj);
	setTimeout(this.controller.getResourceName, 0);

	this.updateLoaderFlag(false);
}

export function getResourceName () {
	let resourceArray = [];

	let myResources = this.props.app.myResources;

	for(let prop in myResources) {
		if(!myResources[prop]['hideInPermission'] && myResources[prop]['fields'] && myResources[prop]['fields']['numberingseriesmasterid'])
			resourceArray.push({
				resourceName : myResources[prop].resourceName,
				displayName : myResources[prop].displayName
			});
	}

	resourceArray.sort((a, b) => {
		return (a.displayName < b.displayName) ? -1 : (a.displayName > b.displayName) ? 1 : 0;
	});

	this.props.updateFormState(this.props.form, { resourceArray });

	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/numberingseriesmaster/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.controller.getResourceName();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	let tempObj = this.props.resource,
		errorArray = [];

	if(tempObj.format.indexOf('#') == -1) {
		errorArray.push("Format is invalid. please refer the instruction");
	} else {
		let formatsplit = tempObj.format.split('#');
		let isnumber = Number(formatsplit[1][0]);

		if(formatsplit.length > 2)
			errorArray.push("Format you have entered ##. it should be one #");
		if(isNaN(isnumber))
			errorArray.push("Format you have entered after # should be a number");
	}

	if(errorArray.length > 0) {
		this.updateLoaderFlag(false);
		let response = {
			data : {
				message : 'failure',
				error : errorArray
			}
		};

		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	} else
		this.controller.saveNumberingSeries(param, tempObj)
}

export function saveNumberingSeries (param, tempObj, confirm) {
	this.updateLoaderFlag(true);

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : tempObj,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/numberingseriesmaster'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.saveNumberingSeries(param, tempObj, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/numberingseriesmaster/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace('/list/numberingseriesmaster');
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
