import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { commonMethods, modalService, pageValidation } from '../utils/services';
import EmailModal from '../components/details/emailmodal';
import PayrollRecoveryComponentModal from '../components/details/payrollrecoverycomponentmodal';
import CostCenterPickingdetailsModal from '../components/details/costcenterpickingdetailsmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initializeState();
	else
		this.controller.getItemById();
}

export function initializeState () {
	let tempObj = {
		currencyid : this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod : this.props.app.defaultRoundOffMethod,
		payrolldate : new Date(new Date().setHours(0, 0, 0, 0)),
		payrollitems : [],
		totalearnings : 0,
		totaldeductions : 0,
		totalemployercontributions : 0,
		totalLoanAdvances : 0
	};

	this.props.initialize(tempObj);
	this.controller.getEmployeeLoanAdvances();
	this.controller.computeFinalRate();

	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/payrolls/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			}, ()=> {
				this.controller.getEmployeeLoanAdvances();
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getEmployeeLoanAdvances () {
	let totalLoanAdvances = 0;

	if (this.props.resource && this.props.resource.employeeid && this.props.resource.currencyid) {
		axios.get(`/api/query/getemployeeoutstandingsquery?type=Receipt&companyid=${this.props.resource.companyid}&currencyid=${this.props.resource.currencyid}&employeeid=${this.props.resource.employeeid}`).then((response) => {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.main.length; i++)
					totalLoanAdvances += response.data.main[i].balance;

				this.props.updateFormState(this.props.form, {
					totalLoanAdvances
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}
}

export function callBackEmployee (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		employeeid_displayname : valueobj.displayname,
		emppayableaccountid : valueobj.emppayableaccountid,
		paystructureid : null
	});

	this.controller.getPaystructures();
	this.controller.getEmployeeLoanAdvances();
}

export function callBackPayStructure (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		paystructureid_name : valueobj.name,
		paystructureid_effectivefrom : valueobj.effectivefrom,
		roundoffmethod : valueobj.roundoffmethod
	});

	this.controller.getPaystructureitems();
}

export function getPaystructures () {
	if(!this.props.resource.employeeid || !this.props.resource.periodstart)
		return null;

	this.updateLoaderFlag(true);

	let filterString = `effectivefrom::DATE <= '${new Date(this.props.resource.periodstart).toDateString()}'::DATE AND status = 'Approved' AND employeeid = ${this.props.resource.employeeid}`

	axios.get(`/api/paystructures?field=id,name,effectivefrom,status,employeeid,roundoffmethod&filtercondition=${filterString}`).then((response) => {
		if(response.data.message == 'success') {
			if(response.data.main.length > 0) {
				let payArray = response.data.main.sort((a, b) =>  (a.effectivefrom > b.effectivefrom) ? -1 : (a.effectivefrom < b.effectivefrom) ? 1 : 0);

				if(payArray[0].id != this.props.resource.paystructureid) {
					this.props.updateFormState(this.props.form, {
						paystructureid : payArray[0].id,
						roundoffmethod : payArray[0].roundoffmethod
					});
				} else {
					this.updateLoaderFlag(false);
				}

				setTimeout(this.controller.getPaystructureitems, 0);
			} else {
				this.updateLoaderFlag(false);
			}

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getPaystructureitems (id) {
	if(this.props.resource.useforopeningbalance) {
		this.props.array.removeAll('payrollitems');
		this.updateLoaderFlag(false);
	} else {
		let payrollitems = [];

		axios.get(`/api/paystructureitems?field=id,componentid,type,parentid,parentresource,earningvalue,deductionvalue,employercontributionvalue,utilitymoduleid,displayorder,tdsapplicable,taxexempt,pfapplicable,esiapplicable,payrollcomponents/name/componentid,payrollcomponents/accountid/componentid,payrollcomponents/accrualaccountid/componentid,payrollcomponents/calculationtype/componentid,payrollcomponents/roundoffmethod/componentid&filtercondition=paystructureitems.isdeleted=FALSE AND paystructureitems.parentid=${this.props.resource.paystructureid}`).then((response) => {
			if(response.data.message == 'success') {
				if (response.data.main.length > 0) {

					response.data.main.sort((a, b) => (a.displayorder < b.displayorder) ? -1 : (a.displayorder > b.displayorder) ? 1 : 0);

					response.data.main.forEach((item) => {
						let tempObj = {
							componentid : item.componentid,
							componentid_name : item.componentid_name,
							componentid_calculationtype : item.componentid_calculationtype,
							componentid_accountid : item.componentid_accountid,
							componentid_accrualaccountid : item.componentid_accrualaccountid,
							componentid_roundoffmethod : item.componentid_roundoffmethod,
							type : item.type,
							utilitymoduleid : item.utilitymoduleid,
							tdsapplicable : item.tdsapplicable,
							taxexempt : item.taxexempt,
							pfapplicable : item.pfapplicable,
							esiapplicable : item.esiapplicable,
							paystructureitemid : item.id
						}

						if(['Attendance Based', 'Computed'].indexOf(item.componentid_calculationtype) >= 0) {
							if(item.type == 'Earning')
								tempObj.defaultvalue = item.earningvalue;

							if(item.type == 'Deduction')
								tempObj.defaultvalue = item.deductionvalue;

							if(item.type == 'Employer Contribution')
								tempObj.defaultvalue = item.employercontributionvalue;
						} else {
							if(item.type == 'Earning') {
								tempObj.value = item.earningvalue;
								tempObj.defaultvalue = item.earningvalue;
							}

							if(item.type == 'Deduction') {
								tempObj.value = item.deductionvalue;
								tempObj.defaultvalue = item.deductionvalue;
							}

							if(item.type == 'Employer Contribution') {
								tempObj.value = item.employercontributionvalue;
								tempObj.defaultvalue = item.employercontributionvalue;
							}
						}

						payrollitems.push(tempObj);
					});
					this.props.updateFormState(this.props.form, {
						payrollitems
					});
				}
				this.controller.computeFinalRate();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}
}

export function generatePayroll () {
	this.updateLoaderFlag(true);

	let filterString = [];

	['employeeid', 'periodstart', 'periodend', 'paydaystotal'].forEach((item) => {
		if(this.props.resource[item]) {
			filterString.push(`${item}=${this.props.resource[item]}`)
		}
	});

	axios.get(`/api/query/getpayablepaydaysquery?${filterString.join('&')}`).then((response) => {
		if(response.data.message == 'success') {
			this.props.updateFormState(this.props.form, {
				defaultpaydaypaid : response.data.payablepaydays,
				paydaypaid : response.data.payablepaydays
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function setPeriod () {
	let temmObj = {
		periodstart: this.props.resource.periodstart,
		periodend: this.props.resource.periodend ? this.props.resource.periodend : (this.props.resource.periodstart ? new Date(new Date(this.props.resource.periodstart).getFullYear(), new Date(this.props.resource.periodstart).getMonth()+1,0) : null)
	};

	let timeDiff = Math.abs(new Date(this.props.resource.periodstart).getTime() - new Date(temmObj.periodend).getTime());
	let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))+1;

	temmObj.paydaystotal = diffDays;
	temmObj.defaultpaydaypaid = null;
	temmObj.paydaypaid = null;

	if(!temmObj.periodstart || !temmObj.periodend)
		temmObj = {};

	this.props.updateFormState(this.props.form, temmObj);

	this.controller.getPaystructures(this.props.resource.employeeid);
}

export function paydaypaidOnChange () {
	if(this.props.resource.defaultpaydaypaid == this.props.resource.paydaypaid) {
		this.props.updateFormState(this.props.form, {
			reason : null
		});
	}
}

export function callBackComponent (id, valueobj, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.componentid`]: valueobj.id,
		[`${itemstr}.componentid_name`]: valueobj.name,
		[`${itemstr}.type`]: valueobj.type,
		[`${itemstr}.componentid_calculationtype`]: valueobj.calculationtype,
		[`${itemstr}.tdsapplicable`]: valueobj.tdsapplicable,
		[`${itemstr}.taxexempt`]: valueobj.taxexempt,
		[`${itemstr}.pfapplicable`]: valueobj.pfapplicable,
		[`${itemstr}.esiapplicable`]: valueobj.esiapplicable,
		[`${itemstr}.utilitymoduleid`]: valueobj.utilitymoduleid,
		[`${itemstr}.componentid_accountid`]: valueobj.accountid,
		[`${itemstr}.componentid_accrualaccountid`]: valueobj.accrualaccountid,
		[`${itemstr}.componentid_roundoffmethod`]: valueobj.roundoffmethod,
		[`${itemstr}.value`]: null,
		[`${itemstr}.defaultvalue`]: null,
		[`${itemstr}.ismanuallyoverride`]: false,
	});
	this.controller.computeFinalRate();
}

export function valueOnChange (item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.value`]: null		
	});
}


export function computeFinalRate() {
	setTimeout(() => {

		let netpay = 0,
			totalearnings = 0,
			totaldeductions = 0,
			totalemployercontributions = 0,
			totalrecovery = 0;

		this.props.resource.payrollitems.forEach((item) => {
			if(item.value > 0 && item.type == 'Earning')
				totalearnings += item.value;

			if(item.value > 0 && item.type == 'Deduction')
				totaldeductions += item.value;

			if(item.value > 0 && item.type == 'Employer Contribution')
				totalemployercontributions += item.value;

			if(item.value > 0 && item.type == 'Recovery')
				totalrecovery += item.value;
		});

		totaldeductions = totaldeductions + totalrecovery;
		netpay = totalearnings - totaldeductions;

		this.props.updateFormState(this.props.form, {
			totalearnings,
			totaldeductions,
			totalemployercontributions,
			netpay
		});

	}, 0);
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function getRecoveryComponentDetails () {
	this.props.openModal({
		render: (closeModal) => {
			return <PayrollRecoveryComponentModal
				resource={this.props.resource}
				callback={this.controller.recoveryComponentCB}
				customFieldsOperation={this.customFieldsOperation}
				updateFormState={this.props.updateFormState}
				form={this.props.form}
				app={this.props.app}
				closeModal={closeModal} />
		},
		className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function recoveryComponentCB (payrollitems) {
	this.props.updateFormState(this.props.form, { payrollitems });
	this.controller.computeFinalRate();
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send Mail',
			data : emailObj
		},
		url : '/api/payrolls'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.employeeid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function openCostCenterPickDetails (item, itemstr) {
	this.openModal({render: (closeModal) => {
		return <CostCenterPickingdetailsModal item={item} resource={this.props.resource} itemstr={itemstr} amountstr={'value'} app={this.props.app} form={this.props.form} updateFormState={this.props.updateFormState} openModal={this.openModal} closeModal={closeModal} />
	}, className: {
		content: 'react-modal-custom-class-60',
		overlay: 'react-modal-overlay-custom-class'
	}, confirmModal: true });
}

export function save (param, confirm, confirm2) {
	if (param == 'Send Mail') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'payrolls'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
	} else {
		this.updateLoaderFlag(true);

		if(param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm2 && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
			let message = {
				header : 'Cost Center Alert',
				body : `Cost Center not selected. Do you want to ${param} ?`,
				btnArray : ['Yes','No']
			};

			return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
				if(resparam)
					this.controller.save(param,confirm,true);
				else
					this.updateLoaderFlag(false);
			}));	
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/payrolls'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true,confirm2);
			}));

			if (response.data.message == 'success') {
				if (this.props.isModal) {
					if (param != 'Delete')
						this.props.callback(response.data.main);

					this.props.closeModal();
				} else {
					if (this.state.createParam)
						this.props.history.replace("/details/payrolls/" + response.data.main.id);
					else if (param == 'Delete')
						this.props.history.replace("/list/payrolls");
					else
						this.props.initialize(response.data.main);
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function createPayment () {
	let paymentObject = this.props.resource;
	paymentObject.param = 'Pay Roll';
	this.props.history.push({pathname: '/createPaymentVoucher', params: paymentObject});
}

export function cancel () {
	if(this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}