import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, detailsSVC, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	let uomArray = [], parentuomArray = [], uomObj = {}, capacityfieldArray = [];

	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();

	for (var i = 0; i < this.props.app.appSettings.length; i++) {
		if (this.props.app.appSettings[i].name == 'Itemmaster' && this.props.app.appSettings[i].module == 'Itemmaster') {
			this.setState({
				settings: this.props.app.appSettings[i].value
			});
			break;
		}
	}

	for(var prop in this.props.app.uomObj) {
		if(this.props.app.uomObj[prop].isparent) {
			parentuomArray.push(this.props.app.uomObj[prop]);

			if(!uomObj[this.props.app.uomObj[prop].id])
				uomObj[this.props.app.uomObj[prop].id] = [];

			uomObj[this.props.app.uomObj[prop].id].push(this.props.app.uomObj[prop]);
		} else {
			if(!uomObj[this.props.app.uomObj[prop].parentid])
				uomObj[this.props.app.uomObj[prop].parentid] = [];

			uomObj[this.props.app.uomObj[prop].parentid].push(this.props.app.uomObj[prop]);
		}

		uomArray.push(this.props.app.uomObj[prop]);
	}

	uomArray.sort((a, b) => {
		return a.name.toLowerCase() < b.name.toLowerCase() ? -1 : (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : 0);
	});
	parentuomArray.sort((a, b) => {
		return a.name.toLowerCase() < b.name.toLowerCase() ? -1 : (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : 0);
	});

	let itemmasterFields = this.props.app.myResources['itemmaster'].fields;
	let equipmentFields = this.props.app.myResources['equipments'].fields;

	Object.keys(itemmasterFields).forEach((item) => {
		if(equipmentFields[`${item}`] && itemmasterFields[`${item}`].type == 'integer' && !['id', 'createdby', 'modifiedby'].includes(item)) {
			capacityfieldArray.push({
				name: item,
				displayname: itemmasterFields[`${item}`].displayName
			});
		}
	});

	this.setState({
		uomArray,
		parentuomArray,
		uomObj,
		capacityfieldArray
	});
}

export function initialiseState() {
	let tempObj = {
		itemnamevariations: [],
		saleskititems: [],
		alternativeuoms: [],
		itemaddons : []
	};

	if(this.props.location.params && this.props.location.params.param == 'copy')
		tempObj = this.props.location.params;

	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/itemmaster/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;

			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function itemTypeOnchange () {
	if(this.props.resource.itemtype != 'Product') {
		this.props.updateFormState(this.props.form, {
			isequipment : false,
			keepstock : false,
			hasserial : false,
			isinstallationapplicable : false,
			iswarrantyapplicable : false,
			warrantycontracttypeid : null,
			warrantycontractbasedon : null,
			issaleskit : false,
			barcode : null,
			stockuomid : null,
			allowbackorder : false,
			hasbatch : false,
			hasaddon : false,
			addonhelptext : null,
			leadtime : null,
			safetystock : null,
			reorderqty : null,
			isuseforcasuallabour: false,
			isuseforinternallabour: false,
			activitytypeid : null
		});
	}
}

export function callbackCategory (value, valueobj) {
	this.props.updateFormState(this.props.form, {
		itemcategorymasterid_name: valueobj.name
	});
}

export function callbackItemGroup (value, valueobj) {
	this.props.updateFormState(this.props.form, {
		itemgroupid_fullname: valueobj.fullname,
		itemgroupid_groupname: valueobj.groupname
	});
}

export function stockUOMOnChange (value, valueobj) {

	this.props.updateFormState(this.props.form, {
		salesuomid: value,
		purchaseuomid: value,
		stockuomid: value,
		stockuomid_name: valueobj.name
	});
}

export function equipmentChange () {
	if(this.props.resource.isequipment == false) {
		this.props.updateFormState(this.props.form, {
			iswarrantyapplicable: false,
			isinstallationapplicable: false,
			warrantycontracttypeid: null,
			warrantycontractbasedon: null
		});
	}
}

export function sparewarrantyonChange() {
	if(!this.props.resource.enablesparewarranty) {
		this.props.updateFormState(this.props.form, {
			sparewarrantyduration: null
		});
	}
}

export function hasBatchOnChange () {
	if(this.props.resource.hasbatch) {
		this.props.updateFormState(this.props.form, {
			isautogenerateserialno: null,
			serialnoformat: null,
			serialnocurrentvalue: null
		});
	}
}

export function addonsOnChange () {
	this.props.array.removeAll('itemaddons');
	this.props.updateFormState(this.props.form, {
		addonhelptext : null
	});
}

export function callbackCustomer (value, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.partnerid_name`] = valueobj.name;
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackProduct (value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.itemid_name`]: valueobj.name,
		[`${itemstr}.uomid`]: valueobj.stockuomid,
		[`${itemstr}.uomid_name`]: valueobj.stockuomid_name,
	});
}


export function callbackAddOnProduct (value, valueobj, item, itemstr) {
	let tempObj = {};
	if (this.props.resource.id == value) {
		tempObj[`${itemstr}.itemid`] = null;
		this.props.updateFormState(this.props.form, tempObj);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Parent Item Name can't be added!!!",
			btnArray : ["Ok"]
		}));
	}

	let itemCountFound = 0;
	for (var i = 0; i < this.props.resource.itemaddons.length; i++) {
		if (this.props.resource.itemaddons[i].itemid == value)
			itemCountFound++;
	}

	if (itemCountFound > 1) {
		tempObj[`${itemstr}.itemid`] = null;
		this.props.updateFormState(this.props.form, tempObj);
		this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Item Name already exists!!!",
			btnArray : ["Ok"]
		}));
	} else {
		tempObj[`${itemstr}.itemid`] = valueobj.id;
		tempObj[`${itemstr}.itemid_name`] = valueobj.name;
		tempObj[`${itemstr}.uomid`] = valueobj.stockuomid;
		tempObj[`${itemstr}.uomid_name`] = valueobj.stockuomid_name;
		this.props.updateFormState(this.props.form, tempObj);
	}
}

export function getUOMName(value, item, itemstr) {
	let tempObj = {};
	for (var i = 0; i < this.state.uomArray.length; i++) {
		if (this.state.uomArray[i].id == value) {
			tempObj[`${itemstr}.uomid_name`] = this.state.uomArray[i].name;
			this.props.updateFormState(this.props.form, tempObj);
			break;
		}
	}
}

export function save (param) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	if (this.props.resource.issaleskit && this.props.resource.hasserial) {
		this.updateLoaderFlag(false);
		let response = {
			data : {
				message : 'failure',
				error : ["Sales kit cannot have serial numbers. Please uncheck Has Serial option"]
			}
		};
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	} else {
		this.controller.checkAccount(param);
	}
}

export function checkAccount (param) {
	if(!this.props.resource.expenseaccountid && !this.props.resource.keepstock && !this.props.resource.issaleskit) {
		let message = {
			header : 'Warning',
			body : 'For non-stock items, we recommend selecting Sales and Purchase accounts (Under Accounts section) at the item level, or at the item group level for accurate tracking of p & l under appropriate accounts. You have not selected any accounts at the item level. Do you want continue anyway?',
			btnArray : ['Yes','No']
		};
		this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
			if(resparam) {
				this.controller.saveItem(param);
			}
		}));
		this.updateLoaderFlag(false);
	} else {
		this.controller.saveItem(param);
	}
}

export function saveItem (param, confirm) {
	this.updateLoaderFlag(true);
	let tempResourceObj = this.props.resource;

	if (!tempResourceObj.allowpurchase) {
		let tempPurchaseArray = ['purchaseuomid', 'defaultpurchasecost', 'lastpurchasecost'];
		for (var i = 0; i < tempPurchaseArray.length; i++) {
			delete tempResourceObj[tempPurchaseArray[i]];
		}
	}
	if (!tempResourceObj.allowpurchase && !tempResourceObj.allowproduction) {
		let tempPurchaseArray = ['safetystock', 'allowbackorder', 'minimumorderqty', 'leadtime', 'autocreatematerialrequest'];
		for (var i = 0; i < tempPurchaseArray.length; i++) {
			delete tempResourceObj[tempPurchaseArray[i]];
		}
	}
	
	if(!tempResourceObj.keepstock){
		tempResourceObj.hasserial=false;
		tempResourceObj.hasbatch=false;
	}

	if (!tempResourceObj.allowsales) {
		let tempSalesArray = ['recommendedsellingprice', 'minimumsellingprice', 'salesuomid'];
		for (var i = 0; i < tempSalesArray.length; i++) {
			delete tempResourceObj[tempSalesArray[i]];
		}
	}

	if (tempResourceObj.itemtype != 'Product') {
		tempResourceObj.keepstock = false;
		tempResourceObj.isequipment = false;
		tempResourceObj.hasserial = false;
		tempResourceObj.hasbatch = false;
		tempResourceObj.issaleskit = false;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : tempResourceObj,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/itemmaster'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
			if (resparam)
				this.controller.saveItem(param, true);
		});

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				if (this.props.isModal) {
					this.props.closeModal();
					this.props.callback(response.data.main);
				} else {
					this.props.history.replace(`/details/itemmaster/${response.data.main.id}`);
				}
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/itemmaster");
				} else {
					this.props.initialize(response.data.main);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function imageUpload (file) {
	this.updateLoaderFlag(true);
	let tempResourceobj = this.props.resource;
	tempResourceobj.parentresource = "itemmaster";
	tempResourceobj.resourceName = "itemmaster";
	tempResourceobj.parentid = tempResourceobj.id;
	tempResourceobj.imagename = "";

	let tempData = {
		actionverb : 'Save',
		data : tempResourceobj
	};

	const formData = new FormData();
	formData.append('file', file);
	formData.append('data', JSON.stringify(tempResourceobj));
	formData.append('actionverb', 'Save');

	const config = {
		headers: {
			'content-type': 'multipart/form-data'
		}
	};
	let fileName = file.name.substr(0, file.name.lastIndexOf('.'));

	if(!(/^[a-zA-Z0-9\.\,\_\-\'\!\*\(\)\ ]*$/.test(fileName))) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Invalid file name. Please change file name. file name contain invalid specical characters. Allowed special characters are '_ - . , ! ( ) *'",
			btnArray : ["Ok"]
		}));
	}

	if (file.size > 10485760) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "File Size should not be exceed than <b class='text-danger'>10 MB</b>",
			btnArray : ["Ok"]
		}));
	}

	if (!file.type.match('image.*')) {
		this.props.updateFormState(this.props.form, {
			image: null
		});
		let message = {
			header : "Error",
			body : "Invalid Image Format.Please Choose Image Type File",
			btnArray : ["Ok"]
		};
		this.props.openModal(modalService.infoMethod(message));
		this.updateLoaderFlag(false);
	} else {
		axios.post('/upload', formData, config).then((response) => {
			if (response.data.message == 'success') {
				let tempobj = response.data.main;
				tempobj.imagename = response.data.main.filename;
				tempobj.image = '';
				this.props.updateFormState(this.props.form, tempobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function removeImage() {
	this.props.updateFormState(this.props.form, {
		imageurl: null
	});
}

export function cancel () {
	if(this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}
