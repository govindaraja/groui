import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById () {
	axios.get(`/api/milestonestages/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/milestonestages'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/milestonestages/${response.data.main.id}`);
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/milestonestages");
				} else {
					this.props.initialize(response.data.main);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
