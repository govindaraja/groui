import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function setYearEnd() {
	let tempObj = {};

	let tempDate = new Date(this.props.resource.startdate);

	if (tempDate.getMonth() >= 3)
		tempObj.enddate = new Date(tempDate.getFullYear() + 1, 3, 0);
	else
		tempObj.enddate = new Date(tempDate.getFullYear(), 3, 0);

	this.props.updateFormState(this.props.form, tempObj);
}

export function getItemById () {
	axios.get(`/api/taxexemptions/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt : {
					notes : response.data.notes,
					documents : response.data.documents,
					tasks : response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if (param != 'Update' && param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);

		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/taxexemptions'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if (this.state.createParam)
				this.props.history.replace(`/details/taxexemptions/${response.data.main.id}`);
			else {
				if (param == 'Delete')
					this.props.history.replace(`/list/taxexemptions`);
				else
					this.props.initialize(response.data.main);
			}
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
