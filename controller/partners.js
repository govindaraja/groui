import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	let partnerParamName = 'partners';

	if(this.props.match.path.indexOf('details/customers') >= 0 || this.props.match.path.indexOf('createCustomer') >= 0)
		partnerParamName = 'customers';
	if(this.props.match.path.indexOf('details/suppliers') >= 0 || this.props.match.path.indexOf('createSupplier') >= 0)
		partnerParamName = 'suppliers';
	if(this.props.match.path.indexOf('details/competitors') >= 0 || this.props.match.path.indexOf('createCompetitor') >= 0)
		partnerParamName = 'competitors';

	this.setState({
		partnerParamName: partnerParamName,
		customersReadAccess: utils.checkActionVerbAccess(this.props.app, 'customers', 'Read') ? true : false,
		customersSaveAccess: utils.checkActionVerbAccess(this.props.app, 'customers', 'Save') ? true : false,
		suppliersReadAccess: utils.checkActionVerbAccess(this.props.app, 'suppliers', 'Read') ? true : false,
		suppliersSaveAccess: utils.checkActionVerbAccess(this.props.app, 'suppliers', 'Save') ? true : false,
		competitorsReadAccess: utils.checkActionVerbAccess(this.props.app, 'competitors', 'Read') ? true : false,
		competitorsSaveAccess: utils.checkActionVerbAccess(this.props.app, 'competitors', 'Save') ? true : false
	}, () => {
		if (this.state.createParam)
			this.controller.initialiseState();
		else
			this.controller.getItemById();
	});
}

export function initialiseState() {
	let tempObj = {
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		collectionrep: this.props.app.user.issalesperson ? this.props.app.user.id : null,
	};

	if (this.props.isModal) {
		tempObj.iscustomer = this.props.location.params.iscustomer ? true : false;
		tempObj.issupplier = this.props.location.params.issupplier ? true : false;
		tempObj.iscompetitor = this.props.location.params.iscompetitor ? true : false;
	}

	if(this.props.match.path == '/createCustomer') {
		tempObj.iscustomer = true;
		if(this.props.location.params) {
			let params = this.props.location.params;

			tempObj.salesperson = params.salesperson ? params.salesperson : tempObj.salesperson;
			tempObj.territory = params.territoryid ? params.territoryid : null;

			if(params.param == 'leads') {
				utils.assign(tempObj, params, [{'salespricelistid' : 'pricelistid'}, 'industryid']);
				this.customFieldsOperation('leads', tempObj, params, 'partners');
			}

			if(params.param == 'contacts') {
				utils.assign(tempObj, params, [{'tempcontactid' : 'id'}, {'name' : 'organization'}, 'industryid', {'tempaddressid' : 'addressid'}]);
				this.customFieldsOperation('contacts', tempObj, params, 'partners');
			}
		}
	}
	if(this.props.match.path == '/createSupplier')
		tempObj.issupplier = true;
	if(this.props.match.path == '/createCompetitor')
		tempObj.iscompetitor = true;

	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/${this.state.partnerParamName}/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			setTimeout(() => {
				this.controller.getContacts();
				this.controller.getAddresses();
			}, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getContacts () {
	let activecontacts = [];
	for(var i=0; i<this.props.resource.contacts.length; i++) {
		if(this.props.resource.contacts[i].isactive)
			activecontacts.push(this.props.resource.contacts[i]);
	}
	for (var i = 0; i < activecontacts.length; i++) {
		if (activecontacts[i].id == this.props.resource.contactid) {
			this.props.updateFormState(this.props.form, {
				contactid: activecontacts[i].id,
				phone: activecontacts[i].phone,
				email: activecontacts[i].email,
				mobile: activecontacts[i].mobile
			});
		}
	}
	activecontacts.sort((a, b) => {
		if(a.name > b.name) return 1;
		if(a.name < b.name) return -1;
		return 0;
	});
	this.props.updateFormState(this.props.form, {
		activecontacts: activecontacts
	});
}

export function getAddresses () {
	let activeaddresses = [];
	for(var i=0; i<this.props.resource.addresses.length; i++) {
		if(this.props.resource.addresses[i].isactive) {
			let tempaddress = this.props.resource.addresses[i]
			if(this.props.app.feature.useMasterForAddresses) {
				var addressData = '';
				addressData = (tempaddress.firstline) ? addressData + (tempaddress.firstline) : addressData + '';
				addressData = (tempaddress.streetid_name) ? addressData + ',\n' + (tempaddress.streetid_name) : addressData + '';
				addressData = (tempaddress.areaid_name) ? addressData + ',\n' + (tempaddress.areaid_name) : addressData + '';
				addressData = (tempaddress.cityid_name) ? addressData + ',\n' + (tempaddress.cityid_name) : addressData + '';
				addressData = (tempaddress.stateid_name) ? addressData + ',\n' + (tempaddress.stateid_name) : addressData + '';
				addressData = (tempaddress.countryid_name) ? addressData + ',\n' + (tempaddress.countryid_name) : addressData + '';
				addressData = (tempaddress.areaid_pincode) ? addressData + ',\n' + 'PIN-' + (tempaddress.areaid_pincode) + '.\n' : addressData + '';
				tempaddress.displayaddress = (addressData!='') ? addressData : tempaddress.displayaddress;
			}
			activeaddresses.push(tempaddress);
		}
	}
	activeaddresses.sort((a, b) => {
		if(a.title > b.title) return 1;
		if(a.title < b.title) return -1;
		return 0;
	});
	this.props.updateFormState(this.props.form, {
		activeaddresses: activeaddresses
	});
}

export function contactpersonChange (id, valueobj) {
	let tempObj = {};
	tempObj[`contactid`] = id;
	tempObj[`contactperson`] = valueobj.name;
	tempObj[`phone`] = valueobj.phone;
	tempObj[`email`] = valueobj.email;
	tempObj[`mobile`] = valueobj.mobile;
	this.props.updateFormState(this.props.form, tempObj);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : `/api/${this.state.partnerParamName}`
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if (this.state.createParam) {
				if (this.props.isModal) {
					this.props.callback(response.data.main);
					this.props.closeModal();
				} else {
					if(this.props.match.path.indexOf('createCustomer') >= 0)
						this.props.history.replace(`/details/customers/${response.data.main.id}`);
					else if(this.props.match.path.indexOf('createSupplier') >= 0)
						this.props.history.replace(`/details/suppliers/${response.data.main.id}`);
					else if(this.props.match.path.indexOf('createCompetitor') >= 0)
						this.props.history.replace(`/details/competitors/${response.data.main.id}`);
				}
			} else {
				if(this.props.isModal) {
					this.props.callback(response.data.main);
					this.props.closeModal();
				} else if (param == 'Delete') {
					if(this.props.match.path.indexOf('details/customers') >= 0)
						this.props.history.replace(`/list/customers`);
					else if(this.props.match.path.indexOf('details/suppliers') >= 0)
						this.props.history.replace(`/list/suppliers`);
					else if(this.props.match.path.indexOf('details/competitors') >= 0)
						this.props.history.replace(`/list/competitors`);
					else
						this.props.history.goBack();
				} else {
					this.props.initialize(response.data.main);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function addNewContact () {
	this.props.history.push({pathname: '/createContact', params: {...this.props.resource, parentresource: 'partners', parentid: this.props.resource.id}});
}

export function addNewAddress () {
	let tempObj = {
		parentresource: 'partners',
		parentid: this.props.resource.id,
		gstregtype: this.props.resource.gstregtype
	};
	this.props.history.push({pathname: '/createAddress', params: tempObj});
}

export function getAddressDetails (id) {
	this.props.history.push(`/details/address/${id}`);
}

export function getContactDetails (id) {
	let tempObj = {
		parentresource : 'partners',
		parentid : this.props.resource.id
	};
	this.props.history.push({pathname: `/details/contacts/${id}`, params: tempObj});
}

export function cancel () {
	if(this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}
