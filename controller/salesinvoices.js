import React, { Component } from 'react';
import axios from 'axios';
import * as ExcelJS from 'exceljs';
import { saveAs } from 'file-saver';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation, checkStatus } from '../utils/services';
import { dateFilter, itemmasterDisplaynamefilter, uomFilter } from '../utils/filter';

import { ItemRateField } from '../components/utilcomponents';
import OtherservicecalladdModal from '../components/details/otherservicecalladdmodal';
import OthercontractaddModal from '../components/details/othercontractaddmodal';
import EmailModal from '../components/details/emailmodal';
import InvoiceProjectItemAddModal from '../components/details/invoiceprojectitemaddmodal';
import InvoiceMilestoneProjectItemAddModal from '../components/details/invoicemilestoneprojectitemaddmodal';

let stockRoundOffPre = 5;

export function onLoad () {
	for(var i = 0; i < this.props.app.appSettings.length; i++) {
		if(this.props.app.appSettings[i].module == 'Stock' && this.props.app.appSettings[i].name == 'Rounding Off Precision') {
			stockRoundOffPre = this.props.app.appSettings[i].value.value;
			break;
		}
	}
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		invoicetype: this.state.pagejson.pagename == 'Sales Invoices' ? 'Commercial Invoice' : (this.state.pagejson.pagename == 'Service Invoices' ? 'Service Invoice' : (this.state.pagejson.pagename == 'Contract Invoices' ? 'Contract Invoice' : null)),
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		invoicedate: new Date(new Date().setHours(0, 0, 0, 0)),
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		autogenerate: true,
		salesinvoiceitems: [],
		additionalcharges: [],
		contractfor: 'Equipment'
	};

	let orderArray = [], deliverynoteArray = [], servicereportArray = [], servicecallArr = [];
	if(this.props.location.params) {
		const params = this.props.location.params;

		if (params.param == 'Sales Orders') {

			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', 'contactperson', 'phone', 'mobile', 'email', 'deliverycontactid', 'deliverycontactperson', 'deliverycontactphone', 'deliverycontactemail', 'deliverycontactmobile', 'deliveryaddress', 'billingaddress', 'deliveryaddressid', 'billingaddressid', {'customerreference' : 'orderid_ponumber'}, 'currencyid', 'currencyexchangerate', {'dispatchthrough' : 'modeofdespatch'}, 'paymentterms', 'salesperson', 'territoryid', 'taxid', 'roundoffmethod', 'creditperiod']);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.invorderno = params.ponumber || params.orderid_ponumber;
			tempObj.invorderdate = params.orderdate;
			tempObj.invpodate = params.podate || params.orderid_podate;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			
			this.customFieldsOperation('orders', tempObj, params, 'salesinvoices');

			params.orderitems.forEach((item, index) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				if(item.quantity > item.invoicedqty) {
					if(orderArray.indexOf(item.parentid) == -1)
						orderArray.push(item.parentid);

					let tempChildObj = {
						index: index+1,
						quantity : Number((item.quantity - item.invoicedqty).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'orderitems'
					};
					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'description', 'rate', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'itemid_issaleskit', 'discountmode', 'discountquantity', 'itemid_keepstock', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'poitemnumber', 'addonrate', 'baseitemrate', {'sourceid' : 'id'},  {'orderitemsid' : 'id'},'itemid_hasserial', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('orderitems', tempChildObj, item, 'salesinvoiceitems');
					tempObj.salesinvoiceitems.push(tempChildObj);
				}
			});
			if(orderArray.length == 1) {
				tempObj.orderid = orderArray[0];
				tempObj.orderid_creditperiod =  params.creditperiod;
			}
		}
		if (params.param == 'Delivery Notes') {

			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', {'contactperson': 'contactname'}, {'phone': 'contactphone'}, {'mobile': 'contactmobile'}, {'email': 'contactemail'}, 'deliverycontactid', 'deliverycontactperson', 'deliverycontactphone', 'deliverycontactemail', 'deliverycontactmobile', 'deliveryaddress', 'billingaddress', 'deliveryaddressid', 'billingaddressid', 'customerreference', 'currencyid', 'currencyexchangerate', 'dispatchthrough', {'paymentterms' : 'orderid_paymentterms'}, 'salesperson', 'territoryid', 'taxid', 'roundoffmethod', {'creditperiod' : 'orderid_creditperiod'}, {'invorderno' : 'dcorderno'}, {'invorderdate' : 'dcorderdate'}, {'invpodate' : 'dcpodate'}, {'invdeliverynoteno' : 'deliverynotenumber'}, {'invdeliverynotedate' : 'deliverynotedate'}, 'vehicleno', 'ewaybillno']);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;

			this.customFieldsOperation('deliverynotes', tempObj, params, 'salesinvoices');

			params.deliverynoteitems.forEach((item, index) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				if(item.quantity > item.invoicedqty) {
					if(orderArray.indexOf(item.orderitemsid_parentid) == -1)
						orderArray.push(item.orderitemsid_parentid);
					if(deliverynoteArray.indexOf(item.parentid) == -1)
						deliverynoteArray.push(item.parentid);

					let tempChildObj = {
						index: index+1,
						quantity : Number((item.quantity - item.invoicedqty).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'deliverynoteitems'
					};
					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'description', 'rate', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'discountmode', 'discountquantity', 'itemid_keepstock', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'poitemnumber', 'addonrate', 'baseitemrate', {'sourceid' : 'id'},  'orderitemsid','itemid_hasserial', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor','itemmakeid']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('deliverynoteitems', tempChildObj, item, 'salesinvoiceitems');
					tempObj.salesinvoiceitems.push(tempChildObj);
				}
			});
			if(orderArray.length == 1) {
				tempObj.orderid = orderArray[0];
				tempObj.orderid_creditperiod = tempObj.creditperiod;
			}
			if(deliverynoteArray.length == 1)
				tempObj.deliverynoteid = deliverynoteArray[0];
		}

		if (params.param == 'Contracts') {
			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', 'contactperson', 'mobile', 'phone', 'email',  'billingaddress', 'billingaddressid', 'currencyid', 'currencyexchangerate', 'salesperson', 'taxid', 'roundoffmethod', {'deliveryaddress' : 'installationaddress'}, {'deliveryaddressid' : 'installationaddressid'}, 'territoryid', {'customerreference' : 'ponumber'}, 'contractfor']);

			tempObj.invoicetype = 'Contract Invoice';
			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;

			this.customFieldsOperation('contracts', tempObj, params, 'salesinvoices');

			if(params.billingschedule != 'Not Applicable') {
				tempObj.contractid = params.id;
				tempObj.contractid_billingschedule = params.billingschedule;
			}

			if(['Facility', 'Equipment'].includes(params.contractfor)) {
				let times = 0;
				params.contractbillingschedules.forEach((schedule)=>{
					if(!schedule.invoiceid)
						times +=1;
				});

				params.contractitems.forEach((item, index) => {
					if(item.contractstatus != 'Expired' ||  (item.contractstatus == 'Expired' && !item.manualexpiredate)) {

						let tempChildObj = {
							index: index+1,
							contractid : params.id,
							contractid_contractno : params.contractno,
							contractid_startdate : params.startdate,
							contractid_expiredate : params.expiredate,
							sourceresource: 'contractitems'
						};

						if(params.billingschedule != 'Not Applicable') {
							let temprate = times > 0 ? Number(((item.invoiceamount > 0 ? (item.amount-item.invoiceamount) : item.amount) / (times * item.quantity)).toFixed(this.props.app.roundOffPrecision)) : 0;
							let discountqty = item.discountmode == 'Rupees' ? null : item.discountquantity;
							temprate = Number((temprate / (1 - (0.01 * discountqty))).toFixed(2));
							tempChildObj.rate = temprate;
							tempChildObj.discountquantity = discountqty;
						} else {
							tempChildObj.rate = item.rate;
							tempChildObj.discountquantity = item.discountquantity;
						}

						utils.assign(tempChildObj, item, ['equipmentid', 'equipmentid_displayname', {'sourceid' : 'id'}, {'contractitemsid' : 'id'}, 'quantity', 'contracttypeid', 'contracttypeid_name', 'description', 'discountmode', 'taxid', 'taxdetails', 'amount', 'finalratelc', 'ratelc', 'amountwithtax', 'remarks', {'accountid' : 'contracttypeid_incomeaccountid'}, 'capacity', 'capacityfield']);

						this.customFieldsOperation('contractitems', tempChildObj, item, 'salesinvoiceitems');

						tempObj.salesinvoiceitems.push(tempChildObj);
					}
				});
			} else {
				params.contractcovereditems.forEach((item, index) => {
					let tempChildObj = {
						index: index+1,
						contractid : params.id,
						contractid_contractno : params.contractno,
						contractid_startdate : params.startdate,
						contractid_expiredate : params.expiredate,
						sourceresource: 'contractcovereditems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', {'sourceid' : 'id'}, 'quantity', 'uomid', 'uomid_name', 'rate', 'contracttypeid', 'contracttypeid_name', 'description', 'discountmode', 'discountquantity', 'remarks', 'capacity', 'capacityfield']);

					this.customFieldsOperation('contractcovereditems', tempChildObj, item, 'salesinvoiceitems');

					tempObj.salesinvoiceitems.push(tempChildObj);
				});
			}
		}
		if (params.param == 'Service Reports') {

			utils.assign(tempObj, params, ['companyid', 'customerid', {'servicereportid' : 'id'}, 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'engineerid', 'billingaddress', 'billingaddressid', 'currencyid', 'currencyexchangerate', 'salesperson', 'taxid', 'roundoffmethod','territoryid']);

			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;

			this.customFieldsOperation('servicereports', tempObj, params, 'salesinvoices');

			params.servicereportitems.forEach((item, index) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				if(item.itemrequestitemsid > 0) {
					item.itemrequestitemsid_invoicedqty = item.itemrequestitemsid_invoicedqty ? item.itemrequestitemsid_invoicedqty : 0;
					item.itemrequestitemsid_returnedqty = item.itemrequestitemsid_returnedqty ? item.itemrequestitemsid_returnedqty : 0;
					item.itemrequestitemsid_closedqty = item.itemrequestitemsid_closedqty ? item.itemrequestitemsid_closedqty : 0;

					if((item.quantity - item.itemrequestitemsid_returnedqty - item.itemrequestitemsid_closedqty) > item.itemrequestitemsid_invoicedqty)
						item.itemrequestitemsid_quantity = item.quantity;
				}
				if(((item.itemrequestitemsid > 0 && (item.itemrequestitemsid_quantity - item.itemrequestitemsid_returnedqty - item.itemrequestitemsid_closedqty) > item.itemrequestitemsid_invoicedqty) || (!item.itemrequestitemsid && item.quantity > item.invoicedqty)) && item.freeorchargeable == 'Chargeable') {
					if(servicereportArray.indexOf(item.parentid) == -1)
						servicereportArray.push(item.parentid);

					let tempChildObj = {
						index: index+1,
						servicecallid : params.servicecallid,
						sourceresource : item.itemrequestitemsid > 0 ? 'itemrequestitems' : 'servicereportitems',
						sourceid : item.itemrequestitemsid > 0 ? item.itemrequestitemsid : item.id,
						servicereportitemsid : item.itemrequestitemsid > 0 ? null : item.id,
						quantity : item.itemrequestitemsid > 0 ? Number((item.itemrequestitemsid_quantity - item.itemrequestitemsid_returnedqty - item.itemrequestitemsid_invoicedqty).toFixed(stockRoundOffPre)) : Number((item.quantity - item.invoicedqty).toFixed(stockRoundOffPre)),
						alternateuom : item.uomconversiontype ? true : false
					};
					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_itemtype', 'description', 'rate', 'uomid', 'uomconversionfactor', 'uomconversiontype', 'discountmode', 'discountquantity', 'taxid', 'taxdetails', 'amount', 'finalratelc', 'ratelc', 'amountwithtax', 'remarks', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('servicereportitems', tempChildObj, item, 'salesinvoiceitems');

					tempObj.salesinvoiceitems.push(tempChildObj);

				}
			});
			if(servicereportArray.length == 1)
				tempObj.servicereportid = servicereportArray[0];
		}
		if (params.param == 'Item Requests') {

			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'engineerid', 'billingaddress', 'deliveryaddress', 'deliveryaddressid', 'billingaddressid', 'currencyid', 'currencyexchangerate', 'salesperson', 'taxid','territoryid']);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;

			this.customFieldsOperation('itemrequests', tempObj, params, 'salesinvoices');

			params.itemrequestitems.forEach((item, index) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				item.returnedqty = item.returnedqty ? item.returnedqty : 0;
				item.quantity = item.quantity ? item.quantity : 0;

				let qty = 0;
				if(item.dcitemfound)
					qty = (item.dcquantity < (item.quantity - item.returnedqty - item.invoicedqty)) ? item.dcquantity : (item.quantity - item.returnedqty - item.invoicedqty);
				else
					qty = (item.quantity - item.returnedqty - item.invoicedqty);

				if(qty > 0 && item.freeorchargeable == 'Chargeable') {
					let tempChildObj = {
						index: index+1,
						servicecallid : params.servicecallid,
						sourceresource : 'itemrequestitems',
						sourceid : item.id,
						alternateuom : item.uomconversiontype ? true : false,
						quantity : Number(qty.toFixed(stockRoundOffPre))
					};
					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_itemtype', 'description', 'uomid', 'uomconversionfactor', 'uomconversiontype', 'remarks', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('itemrequestitems', tempChildObj, item, 'salesinvoiceitems');

					tempObj.salesinvoiceitems.push(tempChildObj);
				}

			});
		}
		if (params.param == 'Projects') {

			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', 'contactperson', 'mobile', 'phone', 'email', 'billingaddress', 'deliveryaddress', 'deliveryaddressid', 'billingaddressid', 'currencyid', 'currencyexchangerate', 'salesperson', 'territoryid', 'taxid', 'labourtaxid', 'materialtaxid', {'projectid' : 'id'}, 'paymentterms', 'roundoffmethod', {'invpodate' : 'podate'}, {'customerreference' : 'ponumber'}, {'invorderno' : 'ponumber'}, {'invorderdate' : 'podate'}, {'projectid_ismilestonerequired' : 'ismilestonerequired'}]);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;

			this.customFieldsOperation('projects', tempObj, params, 'salesinvoices');
		}
		if(params.param == 'Service Calls') {
			utils.assign(tempObj, params, ['companyid', 'customerid', {'consigneeid' : 'customerid'}, 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'currencyid', 'currencyexchangerate', 'salesperson', 'territoryid', 'contactid', {'deliveryaddress' : 'installationaddress'}, {'deliveryaddressid' : 'installationaddressid'}, {'billingaddress' : 'address'}, {'billingaddressid' : 'addressid'}]);

			tempObj.engineerid = this.props.app.user.isengineer ? this.props.app.user.id : null;
			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;

			servicecallArr.push(params.id);

			this.customFieldsOperation('servicecalls', tempObj, params, 'salesinvoices');
		}

		if (params.param == 'Proforma Invoices') {
			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', 'suppliercode', 'contactid', 'contactperson', 'mobile', 'phone', 'email', 'billingaddress', 'deliveryaddress', 'deliveryaddressid', 'billingaddressid', 'currencyid', 'currencyexchangerate', 'salesperson', 'territoryid', 'taxid', 'labourtaxid', 'materialtaxid', 'projectid', 'paymentterms', 'roundoffmethod', 'invpodate', 'customerreference', 'invorderno', 'invorderdate', 'projectid_ismilestonerequired','contractid','contractfor','contractbillingscheduleid']);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;

			this.customFieldsOperation('proformainvoices', tempObj, params, 'salesinvoices');

			params.proformainvoiceitems.forEach((item, index) => {

				let tempChildObj = {
					index: index+1,
					quantity : Number((item.quantity).toFixed(stockRoundOffPre)),
					alternateuom: item.uomconversiontype ? true : false,
					accountid : item.contracttypeid > 0 ? item.contracttypeid_incomeaccountid : null
				};

				utils.assign(tempChildObj, item, ['itemid','itemid_name','itemid_itemtype','uomid','uomid_name' ,'uomconversiontype','uomconversionfactor','description'  ,'splitrate' ,'labourrate' ,'materialrate' ,'rate' ,'sourceresource' ,'sourceid','discountquantity','discountmode','labourtaxid','materialtaxid','taxid','itemid_usebillinguom','usebillinguom','billinguomid','billingrate','billingquantity','billingconversiontype','billingconversionfactor','boqmilestonestatusid','boqmilestonestatusid_name','boqid','boqid_rate','milestonestageid','equipmentid', 'equipmentid_displayname', 'contractitemsid', 'contracttypeid', 'contracttypeid_name', 'taxdetails', 'amount', 'finalratelc', 'ratelc', 'amountwithtax', 'remarks','contractid','contractid_contractno','contractid_startdate','contractid_expiredate','capacityfield','capacity']);

				tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

				this.customFieldsOperation('proformainvoiceitems', tempChildObj, item, 'salesinvoiceitems');
				tempObj.salesinvoiceitems.push(tempChildObj);
			});
			if(orderArray.length == 1) {
				tempObj.orderid = orderArray[0];
				tempObj.orderid_creditperiod =  params.creditperiod;
			}
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.getTandC();

		if(this.props.resource.customerid)
			this.controller.getPartnerDetails();

		let itemidArray = [];
		this.props.resource.salesinvoiceitems.forEach((item) => {
			if(item.itemid > 0)
				itemidArray.push(item.itemid);
		});

		if(itemidArray.length > 0)
			this.controller.getAccountDetails(itemidArray);

		if(!this.props.resource.currencyexchangerate)
			this.controller.currencyOnChange();

		if(this.props.location.params) {

			if(this.props.resource.salesinvoiceitems.length == 0 && this.props.location.params.param != 'Projects' && this.props.location.params.param != 'Service Calls')
				this.controller.showError();

			if(['Sales Orders', 'Delivery Notes', 'Service Reports'].indexOf(this.props.location.params.param) >= 0) {
				let idarray = this.props.location.params.param == 'Sales Orders' ? orderArray : (this.props.location.params.param == 'Delivery Notes' ? deliverynoteArray : servicereportArray);
				let resource = this.props.location.params.param == 'Sales Orders' ? 'orders' : (this.props.location.params.param == 'Delivery Notes' ? 'deliverynotes' : 'servicereports');

				if(idarray && idarray.length > 0)
					this.controller.getAdditionalCharges(idarray, resource);
			}

			if(this.props.location.params.param == 'Contracts' && this.props.location.params.billingschedule == 'Not Applicable')
					this.controller.checkforContracts(this.props.location.params.id);

			if(this.props.location.params.param == 'Item Requests')
				this.controller.getRateFromEstimations(itemidArray, [this.props.location.params.servicecallid]);

			if(this.props.location.params.param == 'Projects')
				this.controller.addProjectItems();

			if(this.props.location.params.param == 'Service Calls')
				this.controller.getServiceReportDetails(servicecallArr, true, this.props.resource.customerid, this.props.location.params.id);

			this.controller.computeFinalRate();
		}
	}, 0);
	this.updateLoaderFlag(false);
}

export function renderTableItemfield(item) {
	return(
		<div>
			<span>{item.itemid_name}</span>
			{item.boqmilestonestatusid ? <br></br> : null}
			{item.boqmilestonestatusid ? <span className="text-muted">{`(${item.boqmilestonestatusid_name})`}</span> : null}
		</div>
	);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function renderTableQtyfield(item) {
	if(item.capacityfield)
		return <span>{item.quantity} {`${itemmasterDisplaynamefilter(item.capacityfield, this.props.app)}`}</span>;
	else
		return <span>{item.quantity} {`${item.uomid ? `${uomFilter(item.uomid, this.props.app.uomObj)}` : 'Nos'}`}</span>;
}

export function getItemById () {
	axios.get(`/api/salesinvoices/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			for (var i = 0; i < tempObj.salesinvoiceitems.length; i++) {
				if (tempObj.salesinvoiceitems[i].uomconversiontype)
					tempObj.salesinvoiceitems[i].alternateuom = true;
			}
			if(tempObj.invoicetype == 'Service Invoice' && this.props.match.path != '/details/serviceinvoices/:id')
				return this.props.history.replace(`/details/serviceinvoices/${tempObj.id}`);
			if(tempObj.invoicetype == 'Contract Invoice' && this.props.match.path != '/details/contractinvoices/:id')
				return this.props.history.replace(`/details/contractinvoices/${tempObj.id}`);

			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			this.controller.checkVisibilityForButton();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function checkVisibilityForButton () {
	setTimeout(() => {
		let showDeliveryBtn = false;

		for(var i=0;i<this.props.resource.salesinvoiceitems.length;i++) {
			if(this.props.resource.salesinvoiceitems[i].sourceresource == 'orderitems' && (this.props.resource.salesinvoiceitems[i].quantity > (this.props.resource.salesinvoiceitems[i].deliveredqty || 0))) {
				this.setState({
					showDeliveryBtn: true
				});
				break;
			}
		}
	}, 0);
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Sales Invoice' and termsandconditions.isdefault`).then((response)=> {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function tandcOnchange(id, valueobj) {
	if(id) {
		this.props.updateFormState(this.props.form, {
			tandc: valueobj.terms
		});
	}
}
export function currencyOnChange () {
	if (this.props.resource.pricelistid) {
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid) {
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});
		}
	}
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject)=> {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason)=> {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function callBackCustomerName (id, valueobj) {
	let tempObj = {
		consigneeid: valueobj.id,
		salesperson: this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		receivableaccountid: (valueobj.receivableaccountid || valueobj.partnergroupid_receivableaccountid || this.props.app.defaultreceivableaccountid),
		suppliercode: valueobj.suppliercode,
		territoryid: valueobj.territory,
		customergstin: valueobj.gstin,
		customergstregtype: valueobj.gstregtype,
		consigneegstin: valueobj.gstin,
		consigneegstregtype: valueobj.gstregtype,
		contactid: valueobj.contactid,
		contactperson: valueobj.contactid_name,
		mobile: valueobj.contactid_mobile,
		phone: valueobj.contactid_phone,
		email: valueobj.contactid_email,
		billingaddress: valueobj.addressid_displayaddress,
		deliveryaddress: valueobj.addressid_displayaddress,
		billingaddressid: valueobj.addressid,
		deliveryaddressid: valueobj.addressid,
		customerstate : valueobj.addressid_state,
		consigneestate : valueobj.addressid_state,
		deliverycontactid: valueobj.contactid,
		deliverycontactperson: valueobj.contactid_name,
		deliverycontactphone: valueobj.contactid_phone,
		deliverycontactemail: valueobj.contactid_email,
		deliverycontactmobile: valueobj.contactid_mobile,
		deliverycontactid_name: valueobj.contactid_name,
		deliverycontactid_phone: valueobj.contactid_phone,
		deliverycontactid_email: valueobj.contactid_email,
		deliverycontactid_mobile: valueobj.contactid_mobile
	};

	let creditPeriod = (valueobj.salescreditperiod || valueobj.partnergroupid_creditperiod);
	if(creditPeriod >= 0) {
	    tempObj.paymentduedate = new Date(new Date(this.props.resource.invoicedate).setDate(new Date(this.props.resource.invoicedate).getDate() + creditPeriod));
	}

	tempObj.paymentterms = this.props.resource.paymentterms ? this.props.resource.paymentterms : valueobj.paymentterms;
	tempObj.dispatchthrough = this.props.resource.dispatchthrough ? this.props.resource.dispatchthrough : valueobj.modeofdispatch;

	this.customFieldsOperation('partners', tempObj, valueobj, 'salesinvoices');
	this.props.updateFormState(this.props.form, tempObj);
}

export function callBackConsigneeName (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		consigneeid_name: valueobj.name,
		consigneegstin: valueobj.gstin,
		consigneegstregtype: valueobj.gstregtype,
		deliveryaddress: null,
		deliveryaddressid: null,
		deliverycontactid: null,
		deliverycontactperson: null,
		deliverycontactmobile: null,
		deliverycontactphone: null,
		deliverycontactemail: null
	});
}

export function contactpersonChange (id, valueobj, type) {
	let tempObj = {};
	if(type == 'billing') {
		tempObj = {
			contactperson: valueobj.name,
			phone: valueobj.phone,
			email: valueobj.email,
			mobile: valueobj.mobile,
			contactid_name: valueobj.name,
			contactid_phone: valueobj.phone,
			contactid_email: valueobj.email,
			contactid_mobile: valueobj.mobile
		};
	} else {
		tempObj = {
			deliverycontactperson: valueobj.name,
			deliverycontactphone: valueobj.phone,
			deliverycontactemail: valueobj.email,
			deliverycontactmobile: valueobj.mobile,
			deliverycontactid_name: valueobj.name,
			deliverycontactid_phone: valueobj.phone,
			deliverycontactid_email: valueobj.email,
			deliverycontactid_mobile: valueobj.mobile
		};
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackEngineer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson: valueobj.id
	});
}

export function projectCallback(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		[`projectid_ismilestonerequired`] : valueobj.ismilestonerequired,
		defaultcostcenter: valueobj.defaultcostcenter
	});
	this.controller.addProjectItems();
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
		tempObj.consigneegstin = addressobj.gstin ? addressobj.gstin : this.props.resource.consigneegstin;

		if(this.props.app.feature.useMasterForAddresses)
			tempObj.consigneestate = addressobj.stateid_name ? addressobj.stateid_name : this.props.resource.consigneestate;
		else
			tempObj.consigneestate = addressobj.state ? addressobj.state : this.props.resource.consigneestate;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
		tempObj.customergstin = addressobj.gstin ? addressobj.gstin : this.props.resource.customergstin;

		if(this.props.app.feature.useMasterForAddresses)
			tempObj.customerstate = addressobj.stateid_name ? addressobj.stateid_name : this.props.resource.customerstate;
		else
			tempObj.customerstate = addressobj.state ? addressobj.state : this.props.resource.customerstate;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function getPartnerDetails () {
	let tempObj = {};
	return axios.get(`/api/partners?field=id,name,receivableaccountid,partnergroupid,salescreditperiod,customergroups/pricelistid/partnergroupid,customergroups/receivableaccountid/partnergroupid,territory,customergroups/creditperiod/partnergroupid&filtercondition=partners.id=${this.props.resource.customerid}`).then((response)=> {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				tempObj.receivableaccountid = (response.data.main[0].receivableaccountid || response.data.main[0].partnergroupid_receivableaccountid || this.props.app.defaultreceivableaccountid);
				tempObj.creditperiod = (this.props.resource.orderid_creditperiod || response.data.main[0].salescreditperiod || response.data.main[0].partnergroupid_creditperiod);
				if(tempObj.creditperiod >= 0)
					tempObj.paymentduedate = new Date(new Date(this.props.resource.invoicedate).setDate(new Date(this.props.resource.invoicedate).getDate() + tempObj.creditperiod));
	
				this.props.updateFormState(this.props.form, tempObj);
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = valueobj.parentid;
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, null, 'sales');
	promise.then((returnObject)=> {
		let tempObj = {};
		tempObj[`${itemstr}.itemid`] = returnObject.itemid;
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.itemid_itemtype`] = returnObject.itemid_itemtype;
		tempObj[`${itemstr}.rate`] = 0;
		tempObj[`${itemstr}.labourrate`] = 0;
		tempObj[`${itemstr}.materialrate`] = 0;
		tempObj[`${itemstr}.splitrate`] = returnObject.splitrate;
		if (returnObject.itemid_itemtype != 'Project') {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		} else {
			tempObj[`${itemstr}.labourtaxid`] = (this.props.resource.labourtaxid && this.props.resource.labourtaxid.length > 0) ? this.props.resource.labourtaxid : [];
			tempObj[`${itemstr}.materialtaxid`] = (this.props.resource.materialtaxid && this.props.resource.materialtaxid.length > 0) ? this.props.resource.materialtaxid : [];
		}
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.accountid`] = returnObject.itemid_incomeaccountid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.itemid_recommendedsellingprice`] = returnObject.itemid_recommendedsellingprice;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'salesinvoiceitems', itemstr);

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		this.props.updateFormState(this.props.form, tempObj);

		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
	}, (reason)=> {});
}

export function quantityOnChange(itemqty, item, itemstr) {
	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);

		this.props.updateFormState(this.props.form, {
			[`${itemstr}.billingquantity`]: Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))
		});
	}
	this.controller.computeFinalRate();
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, null, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function computeFinalRate() {
	setTimeout(()=>{
		if(this.props.resource.invoicetype != 'Contract Invoice') {
			let tempObj = {};
			for (var i = 0; i < this.props.resource.salesinvoiceitems.length; i++) {
				if (this.props.resource.salesinvoiceitems[i].itemid_itemtype == 'Project' && this.props.resource.salesinvoiceitems[i].splitrate) {
					tempObj[`salesinvoiceitems[${i}].rate`] = (this.props.resource.salesinvoiceitems[i].labourrate ? this.props.resource.salesinvoiceitems[i].labourrate : 0) + ((this.props.resource.salesinvoiceitems[i].materialrate ? this.props.resource.salesinvoiceitems[i].materialrate : 0));
				}
			}
			this.props.updateFormState(this.props.form, tempObj);
			taxEngine(this.props, 'resource', 'salesinvoiceitems', null, null, true);
		} else {
			taxEngine(this.props, 'resource', 'salesinvoiceitems');
		}
	}, 0);
}

export function callBackContract (array, contractObj) {
	if(array.length == 0)
		return null;

	this.updateLoaderFlag(true);

	let contractURL;

	if(this.props.resource.contractfor == 'O & M')
		contractURL = `/api/contractcovereditems?field=id,itemid,itemmaster/name/itemid,quantity,uomid,uom/name/uomid,rate,description,remarks,parentresource,parentid&filtercondition=contractcovereditems.parentid in (${array.join()})`
	else
		contractURL = `/api/contractitems?field=id,equipmentid,equipments/displayname/equipmentid,description,rate,discountmode,discountquantity,taxid,taxdetails,quantity,amount,finalratelc,ratelc,amountwithtax,remarks,parentid,contracts/contractno/parentid,contracts/startdate/parentid,contracts/expiredate/parentid,contracttypes/name/contracttypeid,contracttypes/incomeaccountid/contracttypeid,contracttypeid,contractstatus,manualexpiredate,capacity,capacityfield&filtercondition=contractitems.parentid in (${array.join()})`

	axios.get(contractURL).then((response) => {
		if(response.data.message == 'success') {
			let salesinvoiceitems = [...this.props.resource.salesinvoiceitems];
			let itemidArray = [];
			if(this.props.resource.contractfor == 'O & M') {
				response.data.main.forEach((item, index) => {
					let tempChildObj = {
						index: index+1,
						contractid : item.parentid,
						contractid_contractno : contractObj[item.parentid].contractno,
						contractid_startdate : contractObj[item.parentid].startdate,
						contractid_expiredate : contractObj[item.parentid].expiredate,
						sourceresource: 'contractcovereditems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', {'sourceid' : 'id'}, 'quantity', 'uomid', 'uomid_name', 'rate', 'description', 'remarks']);
``
					this.customFieldsOperation('contractcovereditems', tempChildObj, item, 'salesinvoiceitems');

					itemidArray.push(item.itemid);
					salesinvoiceitems.push(tempChildObj);
				});
			} else {
				response.data.main.forEach((item, index) => {
				if(item.contractstatus != 'Expired' ||  (item.contractstatus == 'Expired' && !item.manualexpiredate)) {
						let tempChildObj = {
							index: index+1,
							contractid : item.parentid,
							contractid_contractno : item.parentid_contractno,
							contractid_startdate : item.parentid_startdate,
							contractid_expiredate : item.parentid_expiredate,
							sourceresource: 'contractitems'
						};
						utils.assign(tempChildObj, item, ['equipmentid', 'equipmentid_displayname', {'sourceid' : 'id'}, {'contractitemsid' : 'id'}, 'quantity', 'contracttypeid', 'contracttypeid_name', 'description', 'rate', 'discountmode', 'discountquantity', 'taxid', 'taxdetails', 'amount', 'finalratelc', 'ratelc', 'amountwithtax', 'remarks', {'accountid' : 'contracttypeid_incomeaccountid'}, 'capacity', 'capacityfield']);

						this.customFieldsOperation('contractitems', tempChildObj, item, 'salesinvoiceitems');

						salesinvoiceitems.push(tempChildObj);
					}
				});
			}
			this.props.updateFormState(this.props.form, {
				salesinvoiceitems
			});
			this.controller.computeFinalRate();
			if(itemidArray.length > 0)
				this.controller.getAccountDetails(itemidArray);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
};

export function checkforContracts (contractid) {
	let filterString = `contracts.customerid=${this.props.resource.customerid} and (not contracts.invoicenotapplicable is true) and (not contracts.hasinvoiced is true) and contracts.billingschedule='Not Applicable' and contracts.status in ('Approved','Sent To Customer') and contracts.id<>${contractid} and contracts.contractfor='${this.props.resource.contractfor}'`;
	axios.get(`/api/contracts?field=id,contractno,startdate,expiredate&filtercondition=${encodeURIComponent(filterString)}`).then((response) => {
		if(response.data.message == 'success') {
			if(response.data.main.length > 0) {
				this.openModal({
					render: (closeModal) => {
						return <OthercontractaddModal array={response.data.main} callback={(array, contractObj)=>{
								this.controller.callBackContract(array, contractObj);
							}
						} closeModal={closeModal} />
					}, className: {
						content: 'react-modal-custom-class',
						overlay: 'react-modal-overlay-custom-class'
					}
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function checkforServicecalls (customerid, servicecallid) {
	axios.get(`/api/query/servicecallpendinginvoicequery?customerid=${customerid}&servicecallid=${servicecallid}`).then((response) => {
		if(response.data.message == 'success') {
			if(response.data.main.length > 0) {
				this.props.openModal({
					render: (closeModal) => {
						return <OtherservicecalladdModal array={response.data.main} callback={(array, paramflag) => {
								this.controller.getServiceReportDetails(array, paramflag);
							}
						} closeModal={closeModal} />
					}, className: {
						content: 'react-modal-custom-class',
						overlay: 'react-modal-overlay-custom-class'
					}
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getServiceReportDetails (array, checkparam, customerid, servicecallid) {
	if(array.length == 0)
		return null;

	this.updateLoaderFlag(true);

	let salesinvoiceitems = [...this.props.resource.salesinvoiceitems];
	let itemidArray = [];
	var sricustomFields = this.getCustomFields('servicereportitems','string');

	axios.get(`/api/servicereportitems?field=id,itemid,itemmaster/name/itemid,itemmaster/itemtype/itemid,description,rate,uomid,uomconversionfactor,uomconversiontype,quantity,itemmaster/usebillinguom/itemid,usebillinguom,billinguomid,billingrate,billingquantity,billingconversiontype,billingconversionfactor,discountmode,discountquantity,taxid,taxdetails,amount,finalratelc,ratelc,amountwithtax,remarks,invoicedqty,itemrequestitemsid,itemrequestitems/invoicedqty/itemrequestitemsid,itemrequestitems/quantity/itemrequestitemsid,itemrequestitems/returnedqty/itemrequestitemsid,itemrequestitems/closedqty/itemrequestitemsid,freeorchargeable,servicereports/servicecallid/parentid${sricustomFields ? (','+sricustomFields) : ''}&filtercondition=servicereports_parentid.status='Approved' and servicereports_parentid.servicecallid in (${array.join()})`).then((response) => {
		if(response.data.message == 'success') {
			response.data.main.forEach((item, index) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				let itemFound = false;
				salesinvoiceitems.forEach((secitem) => {
					if(secitem.sourceresource == 'itemrequestitems' && secitem.sourceid == item.itemrequestitemsid) {
						itemFound = true;
					}
				});

				if(!itemFound) {
					if(item.itemrequestitemsid > 0) {
						item.itemrequestitemsid_invoicedqty = item.itemrequestitemsid_invoicedqty ? item.itemrequestitemsid_invoicedqty : 0;

						item.itemrequestitemsid_returnedqty = item.itemrequestitemsid_returnedqty ? item.itemrequestitemsid_returnedqty : 0;

						item.itemrequestitemsid_closedqty = item.itemrequestitemsid_closedqty ? item.itemrequestitemsid_closedqty : 0;
					}

					if (((item.itemrequestitemsid && (item.itemrequestitemsid_quantity - item.itemrequestitemsid_returnedqty - item.itemrequestitemsid_closedqty) > item.itemrequestitemsid_invoicedqty) || (!item.itemrequestitemsid && item.quantity > item.invoicedqty)) && item.freeorchargeable == 'Chargeable') {
						itemidArray.push(item.itemid);
						let tempChiObj = {
							sourceresource : item.itemrequestitemsid > 0 ? 'itemrequestitems' : 'servicereportitems',
							sourceid : item.itemrequestitemsid > 0 ? item.itemrequestitemsid : item.id,
							servicereportitemsid : item.itemrequestitemsid > 0 ? null : item.id,
							alternateuom : item.uomconversionfactor ? true : null,
							quantity : item.itemrequestitemsid > 0 ? Number((item.itemrequestitemsid_quantity - item.itemrequestitemsid_returnedqty -  item.itemrequestitemsid_invoicedqty).toFixed(stockRoundOffPre)) : Number((item.quantity - item.invoicedqty).toFixed(stockRoundOffPre)),
							servicecallid: item.parentid_servicecallid
						};
						utils.assign(tempChiObj, item, ['itemid', 'itemid_name', 'itemid_itemtype', 'uomid', 'uomconversionfactor', 'uomconversiontype', 'description', 'rate', 'discountmode', 'discountquantity', 'taxid', 'remarks', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

						tempChiObj.billingquantity = item.usebillinguom ? (Number(((tempChiObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

						this.customFieldsOperation('servicereportitems', tempChiObj, item, 'salesinvoiceitems');

						salesinvoiceitems.push(tempChiObj);
					}
				}
			});
			this.controller.getItemrequestitems(array, checkparam, customerid, servicecallid, itemidArray, salesinvoiceitems);
		} else {
			var apiResponse = commonMethods.apiResult(response);
			this.openModal(modalService[apiResponse.methodName](apiResponse.message));
			this.updateLoaderFlag(false);
		}
	});
}

export function getItemrequestitems(array, checkparam, customerid, servicecallid, itemidArray, salesinvoiceitems) {
	let etimationitemArray = [];
	var iricustomFields = this.getCustomFields('itemrequestitems','string');
	axios.get(`/api/itemrequestitems?field=id,itemid,itemmaster/name/itemid,itemmaster/itemtype/itemid,description,uomid,quantity,remarks,invoicedqty,returnedqty,closedqty,freeorchargeable,itemrequests/servicecallid/parentid,uomconversionfactor,uomconversiontype,itemmaster/usebillinguom/itemid,usebillinguom,billinguomid,billingquantity,billingconversiontype,billingconversionfactor${iricustomFields ? (','+iricustomFields) : ''}&filtercondition=itemrequests_parentid.status='Approved' and itemrequests_parentid.servicecallid in (${array.join()})`).then((response) => {
		if(response.data.message == 'success') {
			response.data.main.forEach((item) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				let itemFound = false;
				salesinvoiceitems.forEach((secitem) => {
					if(secitem.sourceresource == 'itemrequestitems' && secitem.sourceid == item.id)
						itemFound = true;
				});
				if(!itemFound) {
					item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
					item.returnedqty = item.returnedqty ? item.returnedqty : 0;
					item.quantity = item.quantity ? item.quantity : 0;
					item.closedqty = item.closedqty ? item.closedqty : 0;

					if((item.quantity - item.returnedqty - item.closedqty) > item.invoicedqty && item.freeorchargeable == 'Chargeable') {
						etimationitemArray.push(item.itemid);
						itemidArray.push(item.itemid);

						let tempChiObj = {
							sourceresource : 'itemrequestitems',
							sourceid : item.id,
							alternateuom : item.uomconversionfactor ? true : null,
							quantity : Number((item.quantity - item.returnedqty - item.invoicedqty).toFixed(stockRoundOffPre)),
							servicecallid: item.parentid_servicecallid
						};
						utils.assign(tempChiObj, item, ['itemid', 'itemid_name', 'itemid_itemtype', 'uomid', 'uomconversionfactor', 'uomconversiontype', 'description', 'taxid', 'remarks', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingquantity', 'billingconversiontype', 'billingconversionfactor']);

						this.customFieldsOperation('itemrequestitems', tempChiObj, item, 'salesinvoiceitems');

						salesinvoiceitems.push(tempChiObj);
					}
				}
			});
			this.props.updateFormState(this.props.form, {
				salesinvoiceitems
			});

			this.controller.computeFinalRate();
			this.controller.getAccountDetails(itemidArray);
			this.controller.getRateFromEstimations(etimationitemArray, array);

			if(checkparam)
				this.controller.checkforServicecalls(customerid, servicecallid);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function showError () {
	let tempObj = {
		invoicedate: new Date(new Date().setHours(0, 0, 0, 0)),
		salesinvoiceitems: [],
		additionalcharges: [],
		currencyid: this.props.app.defaultCurrency
	};
	this.props.initialize(tempObj);
	let apiResponse = commonMethods.apiResult({
		data: {
			message: 'Items are Already Invoiced'
		}
	});
	this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
}

export function getAccountDetails (itemidArray) {
	if(itemidArray.length == 0)
		return null;

	axios.get(`/api/common/methods/gettaxaccountdetails?itemarray=${itemidArray.join()}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = {};
			this.props.resource.salesinvoiceitems.forEach((item, index) => {
				for (var i = 0; i < response.data.main.length; i++) {
					if(item.itemid == response.data.main[i].itemid) {
						tempObj[`salesinvoiceitems[${index}].accountid`] = response.data.main[i].incomeaccountid;

						if(this.props.resource.invoicetype == 'Contract Invoice' && this.props.resource.contractfor == 'O & M')
							tempObj[`salesinvoiceitems[${index}].taxid`] = response.data.main[i].salestaxid;

						break;
					}
				}
			});
			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getRateFromEstimations (itemidArray, servicecallarray) {
	if(itemidArray.length == 0 || servicecallarray.length == 0)
		return null;

	axios.get(`/api/estimationitems?field=id,itemid,itemmaster/name/itemid,description,rate,uomid,quantity,discountmode,discountquantity,taxid,taxdetails,billingrate,amount,finalratelc,ratelc,amountwithtax,remarks,invoicedqty,estimations/servicecallid/parentid&filtercondition=estimations_parentid.status='Accepted' and estimationitems.itemid in (${itemidArray.join()}) and estimations_parentid.servicecallid in (${servicecallarray.join()})`).then((response) => {
		if(response.data.message == 'success') {
			let itemObj = {}, tempObj = {};

			response.data.main.forEach((item) => {
				itemObj[`${item.parentid_servicecallid}_${item.itemid}`] = item;
			});

			this.props.resource.salesinvoiceitems.forEach((item, index) => {
				if(itemObj[`${item.servicecallid}_${item.itemid}`]) {
					tempObj[`salesinvoiceitems[${index}.rate]`] = itemObj[`${item.servicecallid}_${item.itemid}`].rate;
					tempObj[`salesinvoiceitems[${index}.discountmode]`] = itemObj[`${item.servicecallid}_${item.itemid}`].discountmode;
					tempObj[`salesinvoiceitems[${index}.discountquantity]`] = itemObj[`${item.servicecallid}_${item.itemid}`].discountquantity;
					tempObj[`salesinvoiceitems[${index}.taxid]`] = itemObj[`${item.servicecallid}_${item.itemid}`].taxid;
					tempObj[`salesinvoiceitems[${index}.billingrate]`] = itemObj[`${item.servicecallid}_${item.itemid}`].billingrate;
				}
			});

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getAdditionalCharges (array, parentResource) {
	axios.get(`/api/additionalcharges?field=name,amount,applyforvaluation,parentid,parentresource,additionalchargesid,additionalchargesmaster/name/additionalchargesid,description,taxcodeid,defaultpercentage,displayorder&filtercondition=additionalcharges.parentid in (${array.join()}) and additionalcharges.parentresource ='${parentResource}'`).then((response) => {
		if (response.data.message == 'success') {
			let additionalcharges = response.data.main.sort((a, b) => {
				return (a.displayorder) - (b.displayorder);
			});
			this.props.updateFormState(this.props.form, {
				additionalcharges
			});
			this.controller.computeFinalRate();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getProjectItems () {
	if(!this.props.resource.projectid) {
		return this.props.updateFormState(this.props.form, {
			salesinvoiceitems: []
		});
	}

	this.updateLoaderFlag(true);
	let salesinvoiceitems = [];

	axios.get(`/api/projectitems?field=id,itemid,itemmaster/name/itemid,itemmaster/itemtype/itemid,description,splitrate,labourrate,materialrate,rate,uomid,quantity,invoicedqty,discountmode,discountquantity,labourtaxid,materialtaxid,taxid,taxdetails,amount,finalratelc,ratelc,amountwithtax,displayorder,uomconversiontype,uomconversionfactor,itemmaster/usebillinguom/itemid,usebillinguom,billinguomid,billingrate,billingquantity,billingconversiontype,billingconversionfactor&filtercondition=projectitems.itemtype='Item' and projectitems.parentid=${this.props.resource.projectid}`).then((response) => {
		if(response.data.message == 'success') {
			let itemstring = '';
			response.data.main = response.data.main.sort((a,b) => {
				return a.displayorder - b.displayorder;
			});
			let itemidArray = [];
			response.data.main.forEach((item) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				if(item.quantity > item.invoicedqty) {
					itemidArray.push(item.itemid);
					salesinvoiceitems.push({
						itemid: item.itemid,
						itemid_name : item.itemid_name,
						itemid_itemtype : item.itemid_itemtype,
						uomid : item.uomid,
						uomconversiontype : item.uomconversiontype,
						uomconversionfactor : item.uomconversionfactor,
						description : item.description,
						quantity : item.quantity,
						splitrate : item.splitrate,
						labourrate : item.labourrate,
						materialrate : item.materialrate,
						rate : item.rate,
						sourceresource : 'projectitems',
						sourceid : item.id,
						discountmode : item.discountmode,
						discountquantity : item.discountquantity,
						labourtaxid : item.labourtaxid,
						materialtaxid : item.materialtaxid,
						taxid : item.taxid,
						alternateuom : item.uomconversiontype ? true : false,
						itemid_usebillinguom : item.itemid_usebillinguom,
						usebillinguom : item.usebillinguom,
						billinguomid : item.billinguomid,
						billingrate : item.billingrate,
						billingquantity : item.billingquantity,
						billingconversiontype : item.billingconversiontype,
						billingconversionfactor : item.billingconversionfactor
					});
				}
			});
			this.props.updateFormState(this.props.form, {
				salesinvoiceitems
			});
			this.controller.computeFinalRate();
			this.controller.getAccountDetails(itemidArray);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function addProjectItems() {
	if(this.props.resource.projectid_ismilestonerequired) {
		this.openModal({
			render: (closeModal) => {
				return <InvoiceMilestoneProjectItemAddModal salesinvoiceitems={this.props.resource.salesinvoiceitems} projectid={this.props.resource.projectid} invoicedate={this.props.resource.invoicedate} closeModal={closeModal} openModal={this.props.openModal} getCustomFields={this.getCustomFields} customFieldsOperation={this.customFieldsOperation} callback = {(itemarr)=> {
					this.controller.callbackInvoiceMilestoneProjectItem(itemarr);
				}} />
			}, className: {content: 'react-modal-custom-class-90', overlay: 'react-modal-overlay-custom-class'}
		});
	} else {
		this.openModal({
			render: (closeModal) => {
				return <InvoiceProjectItemAddModal salesinvoiceitems={this.props.resource.salesinvoiceitems} projectid={this.props.resource.projectid} closeModal={closeModal} openModal={this.props.openModal} getCustomFields={this.getCustomFields} customFieldsOperation={this.customFieldsOperation} callback = {(itemarr)=> {
					this.controller.callbackInvoiceProjectItem(itemarr);
				}} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}
}

export function  callbackInvoiceProjectItem(itemarr) {
	this.updateLoaderFlag(true);
	let salesinvoiceitems = [...this.props.resource.salesinvoiceitems];
	let itemidArray = [];

	for(var i = 0; i < itemarr.length; i++) {
		let itemFound=false;
		for(var j = 0; j < salesinvoiceitems.length; j++) {
			if(salesinvoiceitems[j].sourceid == itemarr[i].id) {
				if(salesinvoiceitems[j].quantity < itemarr[i].qty) {
					salesinvoiceitems[j].quantity = itemarr[i].qty;
				}
				itemFound=true;
				break;
			}
		}
		if(!itemFound) {
			itemidArray.push(itemarr[i].itemid);
			let tempSalesInvItemObj = {
				itemid: itemarr[i].itemid,
				itemid_name : itemarr[i].itemid_name,
				itemid_itemtype : itemarr[i].itemid_itemtype,
				uomid : itemarr[i].uomid,
				uomconversiontype : itemarr[i].uomconversiontype,
				uomconversionfactor : itemarr[i].uomconversionfactor,
				description : itemarr[i].description,
				quantity : itemarr[i].qty,
				splitrate : itemarr[i].splitrate,
				labourrate : itemarr[i].labourrate,
				materialrate : itemarr[i].materialrate,
				rate : itemarr[i].rate,
				sourceresource : 'projectitems',
				sourceid : itemarr[i].id,
				discountmode : itemarr[i].discountmode,
				discountquantity : itemarr[i].discountquantity,
				labourtaxid : itemarr[i].labourtaxid,
				materialtaxid : itemarr[i].materialtaxid,
				taxid : itemarr[i].taxid,
				alternateuom : itemarr[i].uomconversiontype ? true : false,
				itemid_usebillinguom : itemarr[i].itemid_usebillinguom,
				usebillinguom : itemarr[i].usebillinguom,
				billinguomid : itemarr[i].billinguomid,
				billingrate : itemarr[i].billingrate,
				billingquantity : itemarr[i].billingquantity,
				billingconversiontype : itemarr[i].billingconversiontype,
				billingconversionfactor : itemarr[i].billingconversionfactor,
				boqid : itemarr[i].id
			};
			this.customFieldsOperation('projectitems', tempSalesInvItemObj, itemarr[i], 'salesinvoiceitems');
			salesinvoiceitems.push(tempSalesInvItemObj);
		}
	}
	this.props.updateFormState(this.props.form, {
		salesinvoiceitems
	});
	this.controller.computeFinalRate();
	this.controller.getAccountDetails(itemidArray);
	this.updateLoaderFlag(false);
}

export function callbackInvoiceMilestoneProjectItem(invoiceitemsArr) {
	this.updateLoaderFlag(true);
	let salesinvoiceitems = [...this.props.resource.salesinvoiceitems];
	let itemidArray = [], boqstatusidArr = [];

	salesinvoiceitems.forEach((invitem) => {
		boqstatusidArr.push(invitem.boqmilestonestatusid);
	});

	invoiceitemsArr.forEach((item) => {
		let itemFound = false;
		salesinvoiceitems.forEach((invoiceitem, invoiceindex) => {
			if(item.boqid == invoiceitem.boqid && item.boqmilestonestatusid == invoiceitem.boqmilestonestatusid) {
				invoiceitem.quantity = item.qty;

				if (!item.checked)
					salesinvoiceitems.splice(invoiceindex, 1);

				itemFound=true;
			}
		});

		if(!itemFound && item.checked) {
			if(!boqstatusidArr.includes(item.boqmilestonestatusid) && item.qty > 0) {
				itemidArray.push(item.boqid_itemid);
				let temprate = Number(item.rate.toFixed(this.props.app.roundOffPrecision));
				let tempSalesInvItemObj = {
					itemid: item.boqid_itemid,
					itemid_name : item.itemid_name,
					itemid_itemtype : item.itemid_itemtype,
					uomid : item.boqid_uomid,
					uomconversiontype : item.boqid_uomconversiontype,
					uomconversionfactor : item.boqid_uomconversionfactor,
					description : item.boqid_description,
					quantity : item.qty,
					splitrate : item.boqid_splitrate,
					labourrate : item.boqid_labourrate,
					materialrate : item.boqid_materialrate,
					rate : temprate,
					sourceresource : 'boqmilestonestatus',
					sourceid : item.boqmilestonestatusid,
					discountquantity : item.boqid_discountquantity,
					discountmode : item.boqid_discountmode,
					labourtaxid : item.boqid_labourtaxid,
					materialtaxid : item.boqid_materialtaxid,
					taxid : item.boqid_taxid,
					alternateuom : item.boqid_uomconversiontype ? true : false,
					itemid_usebillinguom : item.itemid_usebillinguom,
					usebillinguom : item.boqid_usebillinguom,
					billinguomid : item.boqid_billinguomid,
					billingrate : item.boqid_billingrate,
					billingquantity : item.boqid_billingquantity,
					billingconversiontype : item.boqid_billingconversiontype,
					billingconversionfactor : item.boqid_billingconversionfactor,
					boqmilestonestatusid : item.boqmilestonestatusid,
					boqmilestonestatusid_name : item.boqmilestonestatusid_name,
					boqid : item.boqid,
					boqid_rate : item.boqid_rate,
					milestonestageid : item.milestonestageid
				};
				this.customFieldsOperation('projectitems', tempSalesInvItemObj, item, 'salesinvoiceitems');
				salesinvoiceitems.push(tempSalesInvItemObj);
			}
		}
	});

	this.props.updateFormState(this.props.form, {
		salesinvoiceitems
	});
	this.controller.computeFinalRate();
	this.controller.getAccountDetails(itemidArray);
	this.updateLoaderFlag(false);
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Customer',
			data : emailObj
		},
		url : '/api/salesinvoices'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function save (param, confirm,confirm2) {
	if (param == 'Send To Customer') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'salesinvoices'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
	} else {
		this.updateLoaderFlag(true);

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm2 && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
			let message = {
				header : 'Cost Center Alert',
				body : `Cost Center not selected. Do you want to ${param} ?`,
				btnArray : ['Yes','No']
			};

			return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
				if(resparam)
					this.controller.save(param,confirm,true);
				else
					this.updateLoaderFlag(false);
			}));	
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/salesinvoices'
		}).then((response)=> {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if(resparam)
					this.controller.save(param, true,confirm2);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					if (response.data.main.invoicetype == 'Contract Invoice')
						this.props.history.replace(`/details/contractinvoices/${response.data.main.id}`);
					else if(response.data.main.invoicetype == 'Service Invoice')
						this.props.history.replace(`/details/serviceinvoices/${response.data.main.id}`);
					else
						this.props.history.replace(`/details/salesinvoices/${response.data.main.id}`);
				} else {
					if (param == 'Delete') {
						if(this.props.resource.invoicetype == 'Service Invoice')
							this.props.history.replace("/list/serviceinvoices");
						else if(this.props.resource.invoicetype == 'Contract Invoice')
							this.props.history.replace("/list/contractinvoices");
						else
							this.props.history.replace("/list/salesinvoices");
					} else {
						this.props.initialize(response.data.main);
						this.controller.checkVisibilityForButton();
					}
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function createDC () {
	let promise = checkStatus.deliverynote(this.props.resource);
	promise.then((returnObject) => {
		let temparray = [];
		for (var i = 0; i < returnObject.length; i++) {
			if (returnObject[i].status == 'Hold' || returnObject[i].status == 'Closed') {
				temparray.push("Order <b class='text-danger'>"+returnObject[i].orderno+"</b> for item <b class='text-danger'>"+returnObject[i].itemid_name+"</b> is not Approved");
			}
		}
		if(temparray.length > 0) {
			let message = {
				header : 'Warning',
				body : temparray,
				btnArray : ['Ok']
			};
			this.props.openModal(modalService.infoMethod(message));
		} else {
			let tempObj = this.props.resource;
			tempObj.param = "Sales Invoices";
			for (var i = 0; i < tempObj.salesinvoiceitems.length; i++) {
				tempObj.salesinvoiceitems[i].sourceresource = 'salesinvoiceitems';
			}

			this.props.history.push({pathname: '/createDeliveryNote', params: tempObj});
		}
	}, (reason)=> {});
}

export function createReceipt () {
	this.props.history.push({pathname: '/createReceiptVoucher', params: {...this.props.resource, param: 'Sales Invoices'}});
}

export function getMilestoneExcelData() {
	this.updateLoaderFlag(true);

	axios.get(`/api/query/getmilestoneprojectiteminvoicedetailsquery?salesinvoiceid=${this.props.resource.id}`).then((response) => {
		if (response.data.message != 'success') {
			let apiResponse = commonMethods.apiResult(response);
			this.updateLoaderFlag(false);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		let uniqueMilestoneitemsObj = {}, uniqueMilestoneitemsArr = [], boqItemsObj = {}, boqItemsArr = [], uniqueMilestoneTotalObj = {}, boqMileObj = {}, uniqueTaxObj = {}, uniqueTaxArray = [], boqAmountObj = {};

		boqAmountObj = {
			boqAmount : 0
		};

		response.data.main.forEach(item => {
			item.previous_taxdetails = item.previous_taxdetails ? item.previous_taxdetails : [];
			item.current_taxdetails = item.current_taxdetails ? [item.current_taxdetails] : [];
			item.boqid_taxdetails = item.boqid_taxdetails ? [item.boqid_taxdetails] : [];

			item.taxPreviousArray = [];
			item.taxCurrentArray = [];
			item.taxBoqArray = [];

			item.taxPreviousObj = {};
			item.taxCurrentObj = {};
			item.taxBoqObj = {};

			let taxtempObj = {
				previous_taxdetails: 'taxPreviousArray',
				current_taxdetails: 'taxCurrentArray',
				boqid_taxdetails: 'taxBoqArray',
			}

			Object.keys(taxtempObj).forEach((taxprop) => {
				item[taxprop].forEach((tax) => {
					for(var prop in tax) {
						for (var secprop in tax[prop]) {
							item[taxtempObj[taxprop]].push(tax[prop][secprop]);
						}
					}
				});
			});
		});

		let tempTaxObj = {};

		response.data.main.forEach(item => {
			if(item.milestoneitemsid > 0 && !uniqueMilestoneitemsObj[item.milestoneitemsid]) {
				uniqueMilestoneitemsObj[item.milestoneitemsid] = {};

				uniqueMilestoneitemsArr.push({
					milestoneitemsid: item.milestoneitemsid,
					milestonestageid: item.milestonestageid,
					milestoneitemsid_displayorder: item.milestoneitemsid_displayorder,
					milestoneitemsid_name: item.boqmilestonestatusid_name
				});
			}

			['taxPreviousArray', 'taxCurrentArray', 'taxBoqArray'].forEach(taxprop => {
				item[taxprop].forEach(tax => {
					if(!tempTaxObj[tax.id]) {
						tempTaxObj[tax.id] = {};
						uniqueTaxArray.push({
							id: tax.id,
							description: tax.description
						});
					}

					if(['taxPreviousArray', 'taxCurrentArray'].includes(taxprop)) {
						if(uniqueTaxObj[`${taxprop}_${tax.id}_${item.boqid}_${item.milestoneitemsid}`])
							uniqueTaxObj[`${taxprop}_${tax.id}_${item.boqid}_${item.milestoneitemsid}`] += tax.taxamount;
						else
							uniqueTaxObj[`${taxprop}_${tax.id}_${item.boqid}_${item.milestoneitemsid}`] = tax.taxamount;
					}
				});
			});

			if(!boqItemsObj[item.boqid]) {
				boqItemsObj[item.boqid] = {};

				item.taxBoqObj = {};
				item.taxBoqArray.forEach(tax => {
					item.taxBoqObj[tax.id] = tax.taxamount;
				});

				boqItemsArr.push({
					boqid: item.boqid,
					boqid_internalrefno: item.boqid_internalrefno,
					boqid_clientrefno: item.boqid_clientrefno,
					boqid_description: item.boqid_description,
					boqid_itemtype: item.boqid_itemtype,
					itemid: item.boqid_itemid,
					itemid_name: item.itemid_name,
					boqid_quantity: item.boqid_quantity,
					uomid_name: item.uomid_name,
					boqid_rate: item.boqid_rate,
					boqid_amount: item.boqid_amount,
					boqid_amountwithtax: item.boqid_amountwithtax,
					displayorder: item.boqid_displayorder,
					taxdetails: {},
					childids: [],
					taxBoqObj: item.taxBoqObj
				});
				boqAmountObj.boqAmount += item.boqid_amount;
			}

			if(!boqAmountObj.hasOwnProperty(item.milestoneitemsid)) {
				boqAmountObj[item.milestoneitemsid] = {
					previous: 0,
					current: 0,
					cumulative: 0
				};
			}

			boqAmountObj[item.milestoneitemsid].previous += item.previous_amount;
			boqAmountObj[item.milestoneitemsid].current += item.current_amount;
			boqAmountObj[item.milestoneitemsid].cumulative += (item.current_amount + item.previous_amount);

			boqMileObj[`${item.boqid}_${item.milestoneitemsid}`] = item;
		});

		for(var prop in boqAmountObj) {
			if(typeof(boqAmountObj[prop]) == 'object') {
				boqAmountObj[prop].previous = Number((boqAmountObj[prop].previous).toFixed(this.props.app.roundOffPrecision));
				boqAmountObj[prop].current = Number((boqAmountObj[prop].current).toFixed(this.props.app.roundOffPrecision));
				boqAmountObj[prop].cumulative = Number((boqAmountObj[prop].cumulative).toFixed(this.props.app.roundOffPrecision));
			} else {
				boqAmountObj.boqAmount = Number((boqAmountObj.boqAmount).toFixed(this.props.app.roundOffPrecision));
			}
		}

		boqItemsArr.forEach(item => {
			item.previous = {};
			item.current = {};
			item.cumulative = {};

			if(item.boqid_itemtype == 'Section')
				return null;

			uniqueMilestoneitemsArr.forEach(milestone => {
				let mileObj = boqMileObj[`${item.boqid}_${milestone.milestoneitemsid}`];

				item.previous[milestone.milestoneitemsid] = {
					quantity: mileObj ? mileObj.previous_qty : 0,
					rate: mileObj ? mileObj.previous_rate : 0,
					amount: mileObj ? mileObj.previous_amount : 0,
					amountwithtax: mileObj ? mileObj.previous_amountwithtax : 0,
					taxdetails: {}
				};

				item.current[milestone.milestoneitemsid] = {
					quantity: mileObj ? mileObj.current_qty : 0,
					rate: mileObj ? mileObj.current_rate : 0,
					amount: mileObj ? mileObj.current_amount : 0,
					amountwithtax: mileObj ? mileObj.current_amountwithtax : 0,
					taxdetails: {}
				};

				item.cumulative[milestone.milestoneitemsid] = {
					quantity: mileObj ? mileObj.previous_qty + mileObj.current_qty : 0,
					amount: mileObj ? mileObj.previous_amount + mileObj.current_amount : 0,
					taxdetails: {},
					amountwithtax: mileObj ? mileObj.previous_amountwithtax + mileObj.current_amountwithtax : 0
				};

				uniqueTaxArray.forEach(tax => {
					item.previous[milestone.milestoneitemsid].taxdetails[tax.id] = uniqueTaxObj[`taxPreviousArray_${tax.id}_${item.boqid}_${milestone.milestoneitemsid}`] ? uniqueTaxObj[`taxPreviousArray_${tax.id}_${item.boqid}_${milestone.milestoneitemsid}`] : 0;
					item.current[milestone.milestoneitemsid].taxdetails[tax.id] = uniqueTaxObj[`taxCurrentArray_${tax.id}_${item.boqid}_${milestone.milestoneitemsid}`] ? uniqueTaxObj[`taxCurrentArray_${tax.id}_${item.boqid}_${milestone.milestoneitemsid}`] : 0;
					item.cumulative[milestone.milestoneitemsid].taxdetails[tax.id] = item.previous[milestone.milestoneitemsid].taxdetails[tax.id] + item.current[milestone.milestoneitemsid].taxdetails[tax.id];
				});

				item.cumulative[milestone.milestoneitemsid].rate = Number((item.cumulative[milestone.milestoneitemsid].amount / item.cumulative[milestone.milestoneitemsid].quantity).toFixed(this.props.app.roundOffPrecision));
			});
		});

		boqItemsArr.sort((a,b) => a.displayorder - b.displayorder);

		uniqueMilestoneitemsArr.sort((a,b) => a.milestonestageid - b.milestonestageid || a.milestoneitemsid_displayorder - b.milestoneitemsid_displayorder);

		let tempTopLevelParentFound = false;
		boqItemsArr.forEach((item, itemindex) => {
			let parentintrefno = item.boqid_internalrefno.split('.').splice(0, item.boqid_internalrefno.split('.').length - 1).join('.');
			let parentFound = false;
			boqItemsArr.forEach((secitem, secindex) => {
				if(secindex != itemindex && secitem.boqid_internalrefno.indexOf(`${item.boqid_internalrefno}.`) === 0)
					item.childids.push(secitem.boqid);
				if(secitem.boqid_internalrefno == parentintrefno)
					parentFound = true;
			});

			if(!parentFound)
				item.istoplevelparent = true;
			if(!parentFound && item.boqid_itemtype == 'Section')
				tempTopLevelParentFound = true;
		});

		boqItemsArr.sort((a, b) => a.childids.length - b.childids.length);

		let boqtempObj = {};
		boqItemsArr.forEach(item => boqtempObj[item.boqid] = item);

		let tempTaxTemplateObj = {};
		uniqueTaxArray.forEach(tax => {
			tempTaxTemplateObj[tax.id] = 0;
		});

		boqItemsArr.forEach(item => {
			item.summary = {
				amount: 0,
				amountwithtax: 0,
				taxdetails: JSON.parse(JSON.stringify(tempTaxTemplateObj)),
				previous: {},
				current: {},
				cumulative: {}
			};

			uniqueMilestoneitemsArr.forEach(milestone => {
				item.summary.previous[milestone.milestoneitemsid] = {
					amount: 0,
					amountwithtax: 0,
					taxdetails: JSON.parse(JSON.stringify(tempTaxTemplateObj))
				};
				item.summary.current[milestone.milestoneitemsid] = {
					amount: 0,
					amountwithtax: 0,
					taxdetails: JSON.parse(JSON.stringify(tempTaxTemplateObj))
				};
				item.summary.cumulative[milestone.milestoneitemsid] = {
					amount: 0,
					amountwithtax: 0,
					taxdetails: JSON.parse(JSON.stringify(tempTaxTemplateObj))
				};
			});

			[item.boqid, ...item.childids].forEach(id => {
				if(boqtempObj[id].boqid_itemtype == 'Section')
					return null;

				item.summary.amount += (boqtempObj[id].boqid_amount || 0);
				item.summary.amountwithtax += (boqtempObj[id].boqid_amountwithtax || 0);

				uniqueTaxArray.forEach(tax => {
					item.summary.taxdetails[tax.id] += (boqtempObj[id].taxBoqObj[tax.id] || 0);
				});

				uniqueMilestoneitemsArr.forEach(milestone => {
					item.summary.previous[milestone.milestoneitemsid].amount += (boqtempObj[id].previous[milestone.milestoneitemsid].amount);
					item.summary.previous[milestone.milestoneitemsid].amountwithtax += (boqtempObj[id].previous[milestone.milestoneitemsid].amountwithtax);

					item.summary.current[milestone.milestoneitemsid].amount += (boqtempObj[id].current[milestone.milestoneitemsid].amount);
					item.summary.current[milestone.milestoneitemsid].amountwithtax += (boqtempObj[id].current[milestone.milestoneitemsid].amountwithtax);

					item.summary.cumulative[milestone.milestoneitemsid].amount += (boqtempObj[id].previous[milestone.milestoneitemsid].amount + boqtempObj[id].current[milestone.milestoneitemsid].amount);
					item.summary.cumulative[milestone.milestoneitemsid].amountwithtax += (boqtempObj[id].previous[milestone.milestoneitemsid].amountwithtax + boqtempObj[id].current[milestone.milestoneitemsid].amountwithtax);

					uniqueTaxArray.forEach(tax => {
						item.summary.previous[milestone.milestoneitemsid].taxdetails[tax.id] += (boqtempObj[id].previous[milestone.milestoneitemsid].taxdetails[tax.id]);
						item.summary.current[milestone.milestoneitemsid].taxdetails[tax.id] += (boqtempObj[id].current[milestone.milestoneitemsid].taxdetails[tax.id]);

						item.summary.cumulative[milestone.milestoneitemsid].taxdetails[tax.id] += (boqtempObj[id].previous[milestone.milestoneitemsid].taxdetails[tax.id] + boqtempObj[id].current[milestone.milestoneitemsid].taxdetails[tax.id]);
					});
				});
			});
		});

		boqItemsArr.sort((a,b) => a.displayorder - b.displayorder);

		let uniqueObj = {
			uniqueMilestoneitemsObj,
			uniqueMilestoneitemsArr,
			boqItemsObj,
			boqItemsArr,
			uniqueMilestoneTotalObj,
			boqMileObj,
			boqAmountObj,
			uniqueTaxArray
		};

		let colsArray = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

		['A', 'B', 'C'].forEach(tempcol => {
			colsArray.forEach(colitem => {
				colsArray.push(`${tempcol}${colitem}`);
			});
		});

		const worksheetStyleObj = {
			colsArray: colsArray,
			defaultfontStyle: {
				name: 'Lato',
				family: 2,
				size: 10,
				color: '#FF000000'
			},
			defaultfontBoldStyle: {
				name: 'Lato',
				family: 2,
				size: 10,
				color: '#FF000000',
				bold: true
			},
			defaultfillStyle: {
				type: 'gradient',
				gradient: 'path',
				center: {
					left: 0.5,
					top: 0.5
				},
				stops: [{
					position: 0,
					color: {
						argb: 'FFDCDCDC'
					}
				}, {
					position: 1,
					color: {
						argb: 'FFDCDCDC'
					}
				}]
			},
			defaultalignment: {
				vertical: 'middle',
				horizontal: 'left'
			},
			defaultborderStyle: {
				top: {
					style: 'thin'
				},
				left: {
					style: 'thin'
				},
				bottom: {
					style: 'thin'
				},
				right: {
					style: 'thin'
				}
			}
		};

		let workbook = new ExcelJS.Workbook();

		if(tempTopLevelParentFound)
			this.controller.generateSummaryExcel(uniqueObj, workbook, worksheetStyleObj);
		else
			this.controller.generateExcel(uniqueObj, workbook, worksheetStyleObj);

		this.updateLoaderFlag(false);
	});
}

export function generateSummaryExcel(uniqueObj, workbook, worksheetStyleObj) {
	let invoiceDate = dateFilter(this.props.resource.invoicedate);
	let columnArr = [{name: 'Ref No', key: 'boqid_clientrefno'}, {name: 'Description', key: 'boqid_description'}, {name: 'BOQ Amount', key: 'boqid_amount'}]
	let tempColArr = [{name:'Previous', key: 'previous'}, {name: 'Current', key: 'current'}, {name: 'Cumulative', key: 'cumulative'}];

	let summaryReportData = [['Invoice Summary'],[],['Customer', this.props.resource.customerid_displayname, '', 'Invoice No', this.props.resource.invoiceno], ['Project Name', this.props.resource.projectid_displayname, '', 'Invoice Date', invoiceDate],[],[]];

	let worksheet = workbook.addWorksheet('Summary', {showGridLines: true});

	let mergesArray = [];
	let totalsArray = [];

	let temptrRow1Arr = [], temptrRow2Arr = [], temptrBTArr = [], temptrATArr = [] = [], boqTrArr = [], taxTrArr = [], summaryTrArr = [];

	let parentBoqArr = uniqueObj.boqItemsArr.filter(a => a.istoplevelparent);

	let tempTaxTemplateObj = {};
	uniqueObj.uniqueTaxArray.forEach(tax => {
		tempTaxTemplateObj[tax.id] = 0;
	});

	let totalBoqAmountObj = {
		amount: 0,
		amountwithtax: 0,
		previous: 0,
		current: 0,
		cumulative: 0,
		taxdetails: JSON.parse(JSON.stringify(tempTaxTemplateObj))
	};

	uniqueObj.uniqueMilestoneitemsArr.forEach(milestone => {
		totalBoqAmountObj[milestone.milestoneitemsid] = {
			previous: {
				amount: 0,
				amountwithtax: 0,
				taxdetails: JSON.parse(JSON.stringify(tempTaxTemplateObj))
			},
			current: {
				amount: 0,
				amountwithtax: 0,
				taxdetails: JSON.parse(JSON.stringify(tempTaxTemplateObj))
			},
			cumulative: {
				amount: 0,
				amountwithtax: 0,
				taxdetails: JSON.parse(JSON.stringify(tempTaxTemplateObj))
			}
		};
	});

	parentBoqArr.forEach((item, itemindex) => {
		totalBoqAmountObj.amount += item.summary.amount;
		totalBoqAmountObj.amountwithtax += item.summary.amountwithtax;

		uniqueObj.uniqueTaxArray.forEach(tax => {
			totalBoqAmountObj.taxdetails[tax.id] += item.summary.taxdetails[tax.id];
		});

		tempColArr.forEach((colObj, colindex) => {
			let tempMileAmount = 0;
			uniqueObj.uniqueMilestoneitemsArr.forEach(milestone => {
				totalBoqAmountObj[milestone.milestoneitemsid][colObj.key].amount += item.summary[colObj.key][milestone.milestoneitemsid].amount;
				totalBoqAmountObj[milestone.milestoneitemsid][colObj.key].amountwithtax += item.summary[colObj.key][milestone.milestoneitemsid].amountwithtax;

				tempMileAmount += item.summary[colObj.key][milestone.milestoneitemsid].amountwithtax;


				uniqueObj.uniqueTaxArray.forEach(tax => {
					totalBoqAmountObj[milestone.milestoneitemsid][colObj.key].taxdetails[tax.id] += item.summary[colObj.key][milestone.milestoneitemsid].taxdetails[tax.id];
				});
			});
			totalBoqAmountObj[colObj.key] += tempMileAmount;
		});
	});

	for(var prop in totalBoqAmountObj) {
		if(typeof(totalBoqAmountObj[prop]) == 'object' && prop != 'taxdetails') {
			totalBoqAmountObj[prop].previous.amount = Number((totalBoqAmountObj[prop].previous.amount).toFixed(this.props.app.roundOffPrecision));
			totalBoqAmountObj[prop].previous.amountwithtax = Number((totalBoqAmountObj[prop].previous.amountwithtax).toFixed(this.props.app.roundOffPrecision));

			totalBoqAmountObj[prop].current.amount = Number((totalBoqAmountObj[prop].current.amount).toFixed(this.props.app.roundOffPrecision));
			totalBoqAmountObj[prop].current.amountwithtax = Number((totalBoqAmountObj[prop].current.amountwithtax).toFixed(this.props.app.roundOffPrecision));

			totalBoqAmountObj[prop].cumulative.amount = Number((totalBoqAmountObj[prop].cumulative.amount).toFixed(this.props.app.roundOffPrecision));
			totalBoqAmountObj[prop].cumulative.amountwithtax = Number((totalBoqAmountObj[prop].cumulative.amountwithtax).toFixed(this.props.app.roundOffPrecision));
		} else {
			totalBoqAmountObj.amount = Number((totalBoqAmountObj.amount).toFixed(this.props.app.roundOffPrecision));
			totalBoqAmountObj.amountwithtax = Number((totalBoqAmountObj.amountwithtax).toFixed(this.props.app.roundOffPrecision));
		}
	}

	parentBoqArr.forEach(item => boqTrArr.push([]));
	uniqueObj.uniqueTaxArray.forEach(item => taxTrArr.push([]));

	parentBoqArr.forEach((item, itemindex) => {
		columnArr.forEach((colObj) => {
			if(itemindex == 0) {
				temptrRow1Arr.push('');
				temptrRow2Arr.push(colObj.name);
				if(colObj.key == 'boqid_description') {
					temptrBTArr.push('Total Before Tax');
					temptrATArr.push('Total After Tax');
					summaryTrArr.push('Total-Summary');
				} else if(colObj.key == 'boqid_amount') {
					temptrBTArr.push(totalBoqAmountObj.amount);
					temptrATArr.push(totalBoqAmountObj.amountwithtax);
					summaryTrArr.push('');
				} else {
					temptrBTArr.push('');
					temptrATArr.push('');
					summaryTrArr.push('');
				}
			}

			if(colObj.key == 'boqid_amount')
				boqTrArr[itemindex].push(item.summary.amount);
			else
				boqTrArr[itemindex].push(item[colObj.key]);
		});
	});

	let summaryMergesArray = [];

	parentBoqArr.forEach((item, itemindex) => {
		tempColArr.forEach((colObj, colindex) => {
			if(itemindex === 0) {
				temptrRow1Arr.push(colObj.name);
				summaryTrArr.push(totalBoqAmountObj[colObj.key]);
				mergesArray.push({
					s: {
						r: summaryReportData.length + 1,
						c: temptrRow1Arr.length - 1
					},
					e: {
						r: summaryReportData.length + 1,
						c: temptrRow1Arr.length - 1 + uniqueObj.uniqueMilestoneitemsArr.length - 1
					}
				});
				summaryMergesArray.push({
					s: {
						r: 0,
						c: summaryTrArr.length - 1
					},
					e: {
						r: 0,
						c: summaryTrArr.length - 1 + uniqueObj.uniqueMilestoneitemsArr.length - 1
					}
				});

				for (var i = 0; i < uniqueObj.uniqueMilestoneitemsArr.length - 1; i++) {
					temptrRow1Arr.push('');
					summaryTrArr.push('');
				}
			}

			uniqueObj.uniqueMilestoneitemsArr.forEach(milestone => {
				if(itemindex === 0) {
					temptrRow2Arr.push(milestone.milestoneitemsid_name);
					temptrBTArr.push(totalBoqAmountObj[milestone.milestoneitemsid][colObj.key].amount == 0 ? '-' : totalBoqAmountObj[milestone.milestoneitemsid][colObj.key].amount);
					temptrATArr.push(totalBoqAmountObj[milestone.milestoneitemsid][colObj.key].amountwithtax == 0 ? '-' : totalBoqAmountObj[milestone.milestoneitemsid][colObj.key].amountwithtax);
				}

				boqTrArr[itemindex].push(item.summary[colObj.key][milestone.milestoneitemsid].amount == 0 ? '-' : item.summary[colObj.key][milestone.milestoneitemsid].amount);
			});
		});
	});

	uniqueObj.uniqueTaxArray.forEach((tax, taxindex) => {
		columnArr.forEach((colObj) => {
			if(colObj.key == 'boqid_description') {
				taxTrArr[taxindex].push(tax.description);
			} else if(colObj.key == 'boqid_amount') {
				taxTrArr[taxindex].push(totalBoqAmountObj.taxdetails[tax.id]);
			} else {
				taxTrArr[taxindex].push('');
			}
		});

		tempColArr.forEach((colObj, colindex) => {
			uniqueObj.uniqueMilestoneitemsArr.forEach(milestone => {
				taxTrArr[taxindex].push(totalBoqAmountObj[milestone.milestoneitemsid][colObj.key].taxdetails[tax.id] == 0 ? '-' : totalBoqAmountObj[milestone.milestoneitemsid][colObj.key].taxdetails[tax.id]);
			});
		});
	});
	
	summaryReportData.push(temptrRow1Arr, temptrRow2Arr, ...boqTrArr, temptrBTArr, ...taxTrArr, temptrATArr, summaryTrArr);

	summaryMergesArray.forEach(tempSumm => {
		tempSumm.s.r = summaryReportData.length;
		tempSumm.e.r = summaryReportData.length;
	});

	mergesArray.push(...summaryMergesArray);

	worksheet.addRows(summaryReportData);

	mergesArray.forEach(mergercell => {
		worksheet.mergeCells(`${worksheetStyleObj.colsArray[mergercell.s.c]}${mergercell.s.r}:${worksheetStyleObj.colsArray[mergercell.e.c]}${mergercell.s.r}`);
	});

	worksheet.mergeCells('A1:F1');

	const a1 = worksheet.getCell('A1');
	a1.font = {
		name: 'Lato',
		family: 2,
		size: 16,
		bold: true
	};
	a1.fill = worksheetStyleObj.defaultfillStyle;
	a1.alignment = worksheetStyleObj.defaultalignment;

	['A3', 'B3', 'D3', 'E3', 'A4', 'B4', 'D4', 'E4'].forEach((cell) => {
		const cellEle = worksheet.getCell(cell);
		cellEle.alignment = worksheetStyleObj.defaultalignment;
		if(['B3','E3','B4', 'E4'].includes(cell)) {
			cellEle.fill = worksheetStyleObj.defaultfillStyle;
			cellEle.font = worksheetStyleObj.defaultfontBoldStyle;
		} else {
			cellEle.font = worksheetStyleObj.defaultfontStyle;
		}
	});

	/** apply border color for milestone table **/
	for (var i = 5; i < summaryReportData.length; i++) {
		for (var j = 0; j < summaryReportData[i].length; j++) {
			const cellEle = worksheet.getCell(`${worksheetStyleObj.colsArray[j]}${i+1}`);
			cellEle.border = worksheetStyleObj.defaultborderStyle;
			cellEle.font = worksheetStyleObj.defaultfontStyle;

			if(cellEle.value == '-') {
				cellEle.alignment = {
					vertical: 'middle',
					horizontal: 'center'
				};
			}
		}
	}

	/** fill color for last Total column **/
	summaryReportData[summaryReportData.length-1].forEach((item, index) => {
		const cellEle = worksheet.getCell(`${worksheetStyleObj.colsArray[index]}${summaryReportData.length}`);
		cellEle.fill = worksheetStyleObj.defaultfillStyle;
	});

	/**  fill color for milestone table header **/
	let milestoneheaderIndexArr = [6, 7];
	milestoneheaderIndexArr.forEach(row => {
		summaryReportData[row].forEach((item, index) => {
			if(summaryReportData[row][index]) {
				const cellEle = worksheet.getCell(`${worksheetStyleObj.colsArray[index]}${row+1}`);
				cellEle.fill = worksheetStyleObj.defaultfillStyle;
				cellEle.font = worksheetStyleObj.defaultfontBoldStyle;
				if(row == 7) {
					let tempfontObj = JSON.parse(JSON.stringify(worksheetStyleObj.defaultfontBoldStyle));
					cellEle.font = tempfontObj;
					tempfontObj.size = 9;
				}
				cellEle.alignment = {
					vertical: 'middle',
					horizontal: 'center'
				};
			}
		});
	});

	/** set column width **/
	for (let i = 0; i < worksheet.columns.length; i++) {
		let dataMax = 0;
		const column = worksheet.columns[i];
		for (let j = 1; j < column.values.length; j++) {
			const columnLength = column.values[j] ? column.values[j].length : 1;
			if (columnLength > dataMax) {
				dataMax = columnLength;
			}
		}
		column.width = dataMax < 10 ? 10 : dataMax + 10;
	}

	this.controller.generateExcel(uniqueObj, workbook, worksheetStyleObj);
}

export function generateExcel(uniqueObj, workbook, worksheetStyleObj) {
	let invoiceDate = dateFilter(this.props.resource.invoicedate);
	let tempColArr = [{name:'Previous', key: 'previous'}, {name: 'Current', key: 'current'}, {name: 'Cumulative', key: 'cumulative'}];

	let reportData = [['Annexure - Invoice Details'],[],['Customer', this.props.resource.customerid_displayname, '', 'Invoice No', this.props.resource.invoiceno], ['Project Name', this.props.resource.projectid_displayname, '', 'Invoice Date', invoiceDate],[],[]];

	let columnArr = [{name: 'Ref No', key: 'boqid_clientrefno'}, {name: 'Description', key: 'boqid_description'}, {name: 'Qty', key: 'boqid_quantity'}, {name: 'UOM', key: 'uomid_name'}, {name: 'Rate', key: 'boqid_rate'}, {name: 'Amount', key: 'boqid_amount'}];

	let mergesArray = [];
	let totalsArray = [];

	let worksheet = workbook.addWorksheet('Annexure', {showGridLines: true});

	let temptrRow1Arr = [], temptrRow2Arr = [], temptrRow3Arr = [], temptotalArr = [], boqTrArr = [];

	uniqueObj.boqItemsArr.forEach(item => {
		boqTrArr.push([]);
	});

	uniqueObj.boqItemsArr.forEach((item, itemindex) => {
		columnArr.forEach((colObj, colindex) => {
			if(itemindex === 0) {
				temptrRow1Arr.push('');
				temptrRow2Arr.push('');
				temptrRow3Arr.push(colObj.name);
				temptotalArr.push(colObj.key == 'boqid_description' ? 'Total' : (colObj.key == 'boqid_amount' ? uniqueObj.boqAmountObj.boqAmount : ''));
			}

			if(item.boqid_itemtype == 'Section') {
				if(['boqid_clientrefno', 'boqid_description'].includes(colObj.key))
					boqTrArr[itemindex].push(item[colObj.key]);
				else
					boqTrArr[itemindex].push('');
			} else {
				boqTrArr[itemindex].push(item[colObj.key]);
			}
		});
	});

	uniqueObj.boqItemsArr.forEach((item, itemindex) => {
		tempColArr.forEach((colObj, colindex) => {
			if(itemindex === 0) {
				temptrRow1Arr.push(colObj.name);
				mergesArray.push({
					s: {
						r: reportData.length+1,
						c: temptrRow1Arr.length -1
					},
					e: {
						r: reportData.length+1,
						c: temptrRow1Arr.length -1 + (uniqueObj.uniqueMilestoneitemsArr.length * 2) - 1
					}
				});

				for (var i = 0; i < (uniqueObj.uniqueMilestoneitemsArr.length * 2) - 1; i++)
					temptrRow1Arr.push('');
			}

			uniqueObj.uniqueMilestoneitemsArr.forEach(milestone => {
				if(itemindex === 0) {
					temptrRow2Arr.push(milestone.milestoneitemsid_name, '');
					mergesArray.push({
						s: {
							r: reportData.length + 2,
							c: temptrRow2Arr.length - 2
						},
						e: {
							r: reportData.length + 2,
							c: temptrRow2Arr.length - 1
						}
					});
					temptrRow3Arr.push('Qty', 'Amount');
					temptotalArr.push('', uniqueObj.boqAmountObj[milestone.milestoneitemsid][colObj.key] == 0 ? '-' : uniqueObj.boqAmountObj[milestone.milestoneitemsid][colObj.key]);
				}

				if(item.boqid_itemtype != 'Section')
					boqTrArr[itemindex].push(item[colObj.key][milestone.milestoneitemsid].quantity == 0 ? '-' : item[colObj.key][milestone.milestoneitemsid].quantity, item[colObj.key][milestone.milestoneitemsid].amount == 0 ? '-' : item[colObj.key][milestone.milestoneitemsid].amount);
			});
		});
	});

	reportData.push(temptrRow1Arr, temptrRow2Arr, temptrRow3Arr, ...boqTrArr, temptotalArr);

	worksheet.addRows(reportData);

	mergesArray.forEach(mergercell => {
		worksheet.mergeCells(`${worksheetStyleObj.colsArray[mergercell.s.c]}${mergercell.s.r}:${worksheetStyleObj.colsArray[mergercell.e.c]}${mergercell.s.r}`);
	});

	worksheet.mergeCells('A1:F1');

	const a1 = worksheet.getCell('A1');
	a1.font = {
		name: 'Lato',
		family: 2,
		size: 16,
		bold: true
	};
	a1.fill = worksheetStyleObj.defaultfillStyle;
	a1.alignment = worksheetStyleObj.defaultalignment;

	['A3', 'B3', 'D3', 'E3', 'A4', 'B4', 'D4', 'E4'].forEach((cell) => {
		const cellEle = worksheet.getCell(cell);
		//cellEle.font = worksheetStyleObj.defaultfontStyle;
		cellEle.alignment = worksheetStyleObj.defaultalignment;
		if(['B3','E3','B4','E4'].includes(cell)) {
			cellEle.fill = worksheetStyleObj.defaultfillStyle;
			cellEle.font = worksheetStyleObj.defaultfontBoldStyle;
		} else {
			cellEle.font = worksheetStyleObj.defaultfontStyle;
		}
	});

	/** apply border color for milestone table **/
	for (var i = 6; i < reportData.length; i++) {
		for (var j = 0; j < reportData[i].length; j++) {
			const cellEle = worksheet.getCell(`${worksheetStyleObj.colsArray[j]}${i+1}`);
			cellEle.border = worksheetStyleObj.defaultborderStyle;
			cellEle.font = worksheetStyleObj.defaultfontStyle;

			if(cellEle.value == '-') {
				cellEle.alignment = {
					vertical: 'middle',
					horizontal: 'center'
				};
			}
		}
	}

	/** fill color for last Total column **/
	reportData[reportData.length-1].forEach((item, index) => {
		const cellEle = worksheet.getCell(`${worksheetStyleObj.colsArray[index]}${reportData.length}`);
		cellEle.fill = worksheetStyleObj.defaultfillStyle;
	});

	/**  fill color for milestone table header **/
	for (var i = 6; i <= 8; i++) {
		reportData[i].forEach((item, index) => {
			if(reportData[i][index]) {
				const cellEle = worksheet.getCell(`${worksheetStyleObj.colsArray[index]}${i+1}`);
				cellEle.fill = worksheetStyleObj.defaultfillStyle;
				if([6, 8].includes(i)) {
					cellEle.font = worksheetStyleObj.defaultfontBoldStyle;
				}
				if(i == 7) {
					let tempfontObj = JSON.parse(JSON.stringify(worksheetStyleObj.defaultfontBoldStyle));
					cellEle.font = tempfontObj;
					tempfontObj.size = 9;
				}
				cellEle.alignment = {
					vertical: 'middle',
					horizontal: 'center'
				};
			}
		});
	}

	/** set column width **/
	for (let i = 0; i < worksheet.columns.length; i++) {
		let dataMax = 0;
		const column = worksheet.columns[i];
		for (let j = 1; j < column.values.length; j++) {
			const columnLength = column.values[j] ? column.values[j].length : 1;
			if (columnLength > dataMax) {
				dataMax = columnLength;
			}
		}
		column.width = dataMax < 10 ? 10 : dataMax;
	}

	workbook.xlsx.writeBuffer().then((buffer) => {
		saveAs(new Blob([buffer],{type:"application/octet-stream"}), "Running Bill_"+ this.props.resource.invoiceno +"_"+ (dateFilter(new Date())) + ".xlsx");	
	});
}

export function cancel () {
	this.props.history.goBack();
}
