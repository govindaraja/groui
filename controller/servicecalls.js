import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { commonMethods, modalService, pageValidation } from '../utils/services';
import ServicecallequipmentaddModal from '../components/details/servicecallequipmentaddmodal';
import { ServicecallprofitanalyseModal } from '../components/details/servicecallprofitabilityanalyse';
import async from 'async';
import EquipmentCoveredItemDetailsModal from '../components/details/equipmentcovereditemdetailsmodal';

export function onLoad () {
	this.setState({
		showEstimationTab: utils.checkActionVerbAccess(this.props.app, 'estimations', 'Menu'),
		showItemReqTab: utils.checkActionVerbAccess(this.props.app, 'itemrequests', 'Menu'),
		showServiceReportTab: utils.checkActionVerbAccess(this.props.app, 'servicereports', 'Menu'),
		showInvoiceTab: utils.checkActionVerbAccess(this.props.app, 'salesinvoices', 'Menu'),
		showReceiptTab: utils.checkActionVerbAccess(this.props.app, 'receiptvouchers', 'Menu'),
		showWOTab: utils.checkActionVerbAccess(this.props.app, 'workorders', 'Menu'),
		showExpenseTab: utils.checkActionVerbAccess(this.props.app, 'expenserequests', 'Menu'),
		feedbackContactArray: []
	});

	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid : this.props.app.defaultCurrency,
		companyid : this.props.app.selectedcompanyid,
		servicecalldate : new Date(),
		equipmentitems : [],
		servicevisits : [],
		calltype : 'Service',
		activatewarranty : false,
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null
	};

	if(this.props.location.params) {

		const params = this.props.location.params;

		if(params.param == 'copy')
			tempObj = params;
		else if(params.param == 'Dialer') {
			tempObj.customerid = params.customerid;
			tempObj.contactid = params.contactid;
			tempObj.contactperson = params.contactperson;
			tempObj.email = params.email;
			tempObj.phone = params.phone;
			tempObj.mobile = params.mobile;
			tempObj.installationcontactid = params.contactid;
			tempObj.installationcontactperson = params.contactperson;
			tempObj.installationemail = params.email;
			tempObj.installationphone = params.phone;
			tempObj.installationmobile = params.mobile;
		} else {
			let equipmentArray = params.equipmentArray;

			utils.assign(tempObj, params, ['customerid', 'customerid_name', 'territoryid','contactid','contactperson','email','phone','mobile','addressid','addressid','installationcontactid','installationcontactperson','installationemail','installationphone','installationmobile','installationaddressid','installationaddress']);
			
			equipmentArray.map((item) => {
				if(params.equipmentid == item.id) {

					if(item.installationaddressid > 0) {
						tempObj.installationaddressid = item.installationaddressid;
						tempObj.installationaddress = item.installationaddress;
					} else if (item.equipmentaddressid > 0) {
						tempObj.installationaddressid = item.equipmentaddressid;
						tempObj.installationaddress = item.equipmentaddress;
					} else if (item.primaryaddressid > 0) {
						tempObj.installationaddressid = item.primaryaddressid;
						tempObj.installationaddress = item.primaryaddress;
						tempObj.addressid = item.primaryaddressid;
						tempObj.address = item.primaryaddress;
					} else if (item.billingaddressid > 0) {
						tempObj.addressid = item.billingaddressid;
						tempObj.address = item.billingaddress;
					}

					if(item.contactid > 0) {
						tempObj.contactid = item.contactid;
						tempObj.contactperson = item.contactperson;
						tempObj.email = item.email;
						tempObj.phone = item.phone;
						tempObj.mobile = item.mobile;
					} else if (item.primarycontactid > 0) {
						tempObj.contactid = item.primarycontactid;
						tempObj.contactperson = item.primarycontactperson;
						tempObj.email = item.primaryemail;
						tempObj.phone = item.primaryphone;
						tempObj.mobile = item.primarymobile;
						tempObj.installationcontactid = item.primarycontactid;
						tempObj.installationcontactperson = item.primarycontactperson;
						tempObj.installationemail = item.primaryemail;
						tempObj.installationphone = item.primaryphone;
						tempObj.installationmobile = item.primarymobile;
					} else if (item.installationcontactid > 0) {
						tempObj.installationcontactid = item.installationcontactid;
						tempObj.installationcontactperson = item.installationcontactperson;
						tempObj.installationemail = item.installationemail;
						tempObj.installationphone = item.installationphone;
						tempObj.installationmobile = item.installationmobile;
					}

					tempObj.addressid = tempObj.addressid ? tempObj.addressid : item.equipmentaddressid;
					tempObj.address = tempObj.address ? tempObj.address : item.equipmentaddress;
					
					let tempChildObj = {};

					utils.assign(tempChildObj, item, [{'equipmentid' : 'id'}, {'equipmentid_displayname' : 'displayname'}, {'equipmentid_description' : 'description'}, {'equipmentid_serialno' : 'serialno'}, 'contracttypeid', 'contracttypeid_name', 'contractid', {'contractid_contractno' : 'contractno'}, {'contractid_startdate' : 'startdate'}, {'contractid_expiredate' : 'expiredate'}]);

					this.customFieldsOperation('equipments', tempChildObj, item, 'equipmentitems');

					tempObj.equipmentitems.push(tempChildObj);
				}
			});
		}
	}

	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function addressonChange(address, addressobj) {
	if(address == 'installationaddress')
		this.props.updateFormState(this.props.form, {
			installationaddress : addressobj.displayaddress,
			installationaddressid : addressobj.id
		});

	if(address == 'address')
		this.props.updateFormState(this.props.form, {
			address : addressobj.displayaddress,
			addressid : addressobj.id
		});
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});
		}, (reason) => {});
	} else
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
}

/*export function callbackEngineer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson : valueobj.userid,
		engineerid_userid : valueobj.userid
	});
}*/

export function callbackInHouseServiceEngineer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		engineerid : (this.props.resource.engineerid && this.props.resource.engineerid.length > 0) ? this.props.resource.engineerid : [id],
		salesperson : this.props.resource.salesperson ? this.props.resource.salesperson : valueobj.userid
	});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson : valueobj.name,
		phone : valueobj.phone,
		email : valueobj.email,
		mobile : valueobj.mobile
	});
}

export function installationcontactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		installationcontactperson : valueobj.name,
		installationphone : valueobj.phone,
		installationemail : valueobj.email,
		installationmobile : valueobj.mobile
	});
}

export function feedbackcontactpersonChange (name) {
	for (var i = 0; i < this.state.feedbackContactArray.length; i++) {
		if (this.state.feedbackContactArray[i].name == name) {
			this.props.updateFormState(this.props.form, {
				feedbackcontactmobile: this.state.feedbackContactArray[i].mobile
			});
			break;
		}
	}
}

export function getItemById () {
	axios.get(`/api/servicecalls/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			this.controller.callGetRelatedResource();
			this.controller.getFeebacKContacts();
			this.controller.getServiceVisits();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getFeebacKContacts() {
	setTimeout(() => {
		axios.get(`/api/contacts?&field=id,name,mobile,phone,email&filtercondition=contacts.parentresource='partners' and contacts.parentid=${this.props.resource.customerid}`).then((response) => {
			if (response.data.message == 'success') {
				this.setState({
					feedbackContactArray: response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}, 0);
}

export function callGetRelatedResource() {
	setTimeout(() => {
		let id = this.props.resource.id;
		let array = [{
			resource: 'estimations',
			access: 'showEstimationTab',
			url: `/listapi/estimations?&field=id,estimationno,estimationdate,status&filtercondition=estimations.servicecallid=${id}`,
			array: 'estimationArray'
		}, {
			resource: 'itemrequests',
			access: 'showItemReqTab',
			url: `/listapi/itemrequests?&field=id,itemrequestno,itemrequestdate,status,issuedpercent&filtercondition=itemrequests.servicecallid=${id}`,
			array: 'itemrequestsArray'
		}, {
			resource: 'servicereports',
			access: 'showServiceReportTab',
			url: `/listapi/servicereports?field=id,servicereportno,servicereportdate,reportno,status,users/displayname/engineerid,servicerendered&filtercondition=servicereports.servicecallid=${id}`,
			array: 'servicereportsArray'
		}, {
			resource: 'salesinvoices',
			access: 'showInvoiceTab',
			url: `/api/query/servicecallinvoicenoquery?servicecallid=${id}`,
			array: 'serviceinvoiceArray'
		}, {
			resource: 'receiptvouchers',
			access: 'showReceiptTab',
			url: `/listapi/receiptvouchers?&field=id,voucherno,voucherdate,amount,currencyid,status&filtercondition=journalvouchers.servicecallid=${id}`,
			array: 'receiptArray'
		}, {
			resource: 'workorders',
			access: 'showWOTab',
			url: `/listapi/workorders?field=id,wonumber,wodate,status,partners/name/partnerid&filtercondition=workorders.servicecallid=${id}`,
			array: 'woArray'
		}, {
			resource: 'expenserequests',
			access: 'showExpenseTab',
			url: `/listapi/expenserequests?field=id,expenserequestno,expensedate,currencyid,amount,amountrequested,amountapproved,status,remarks&filtercondition=expenserequests.servicecallid=${id}`,
			array: 'expenseArray'
		}];
		let tempObj = {};
		async.each(array, (prime, eachCB) => {
			if(this.state[prime.access]) {
				axios.get(prime.url).then((response) => {
					if (response.data.message == 'success') {
						tempObj[prime.array] = response.data.main;
					} else {
						let apiResponse = commonMethods.apiResult(response);
						this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
					}
					eachCB(null);
				});
			} else {
				eachCB(null);
			}
		}, () => {
			this.props.updateFormState(this.props.form, tempObj);
		});
	}, 0);
}

export function callTypeChange() {
	this.props.array.removeAll('equipmentitems');
}

export function callBackCustomerName(id, valueobj) {
	let tempObj = {
		equipmentid: null,
		equipmentid_displayname: null,
		contractid: null,
		equipmentitems : [],
		territoryid : valueobj.territory,
		address : null,
		addressid : null,
		installationaddressid : null,
		installationaddress : null,
		installationcontactid: valueobj.contactid,
		installationcontactperson: valueobj.contactid_name,
		installationemail: valueobj.contactid_email,
		installationmobile: valueobj.contactid_mobile,
		installationphone: valueobj.contactid_phone
	};

	if (!this.props.resource.tempcontactid) {
		tempObj.contactid = valueobj.contactid;
		tempObj.email = valueobj.contactid_email;
		tempObj.mobile = valueobj.contactid_mobile;
		tempObj.phone = valueobj.contactid_phone;
		tempObj.contactperson = valueobj.contactid_name;
	}

	if (valueobj.salespricelistid)
		tempObj.pricelistid = valueobj.salespricelistid;
	else if (valueobj.partnergroupid_pricelistid)
		tempObj.pricelistid = valueobj.partnergroupid_pricelistid;
	else {
		for (let i = 0; i < this.props.app.appSettings.length; i++) {
			if (this.props.app.appSettings[i].module == 'Sales' && this.props.app.appSettings[i].name == 'Pricelistid') {
				tempObj.pricelistid = this.props.app.appSettings[i].value['value'];
				break;
			}
		}
	}

	this.props.updateFormState(this.props.form, tempObj);
}

export function addEquipment () {
	this.openModal({
		render: (closeModal) => {
			return <ServicecallequipmentaddModal
					   resource = {this.props.resource}
					   app = {this.props.app}
					   array = {this.props.array}
					   closeModal = {closeModal}
					   openModal = {this.openModal}
					   updateFormState = {this.props.updateFormState}
					   form = {this.props.form}
					   resourcename = {'servicecalls'}
					   childresourcename = {'equipmentitems'}
					   customFieldsOperation = {this.customFieldsOperation} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function openEquipmentCoveredItemDetails(item) {
	this.openModal({
		render: (closeModal) => {
			return <EquipmentCoveredItemDetailsModal item = {item} app = {this.props.app} closeModal = {closeModal} openModal = {this.openModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/servicecalls'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam)=> {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/servicecalls/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace("/list/servicecalls");
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function createContract () {
	this.props.history.push({
		pathname: '/createContract',
		params: {...this.props.resource, param: 'Service Calls'}
	});
}

export function createServices (param) {
	if(['servicereport', 'estimation', 'itemrequest', 'workorder', 'expenserequest'].indexOf(param) >=0 && this.props.resource.status != 'Assigned') {
		let message = {
			header : 'Error',
			body : 'Please assign Engineer and Change the Status to Assigned',
			btnArray : ['Ok']
		};
		return this.props.openModal(modalService.infoMethod(message));
	}

	if (param == 'servicereport')
		this.props.history.push({
			pathname: '/createServiceReport',
			params: {...this.props.resource, param: 'Service Calls'}
		});
	else if (param == 'estimation')
		this.props.history.push({
			pathname: '/createEstimation',
			params: {...this.props.resource, param: 'Service Calls'}
		});
	else if (param == 'itemrequest')
		this.props.history.push({
			pathname: '/createItemRequest',
			params: {...this.props.resource, param: 'Service Calls'}
		});
	else if (param == 'invoice')
		this.props.history.push({
			pathname: '/createServiceInvoice',
			params: {...this.props.resource, param: 'Service Calls'}
		});
	else if (param == 'receipt')
		this.props.history.push({
			pathname: '/createReceiptVoucher',
			params: {...this.props.resource, param: 'Service Calls'}
		});
	else if (param == 'workorder')
		this.props.history.push({
			pathname: '/createWorkOrder',
			params: {...this.props.resource, param: 'Service Calls'}
		});
	else if (param == 'expenserequest')
		this.props.history.push({
			pathname: '/createExpenseRequest',
			params: {...this.props.resource, param: 'Service Calls'}
		});
}
/*
export function servicereportOnLoad() {
	let queryString = "/api/servicereports?&field=id,servicereportno,servicereportdate,status&filtercondition=servicereports.servicecallid=" + $scope.resource.id;
	axios.get(``).then((response) => {
		if (response.data.message == 'success') {
			for(let i = 0; i < response.data.main.length; i++) {
				this.props.array.push('servicereportsArray', response.data.main[i]);
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}*/

export function profitanalysedetails() {
	this.openModal({render: (closeModal) => {
		return <ServicecallprofitanalyseModal
				   resource = {this.props.resource}
				   param = "servicecalls"
				   app = {this.props.app}
				   closeModal = {closeModal} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function getServiceVisits() {
	setTimeout(() => {
		axios.get(`/api/salesactivities?&field=id,activitytypeid,status,assignedto,plannedstarttime,plannedendtime,users/displayname/assignedto,activitytypes/name/activitytypeid,activitytypes/category/activitytypeid&sortstring=salesactivities.id&filtercondition=salesactivities.parentresource='servicecalls' and salesactivities.parentid=${this.props.resource.id} and activitytypes_activitytypeid.category='Service Allocation'`).then((response) => {
			if (response.data.message == 'success') {
				this.props.updateFormState(this.props.form, {
					[`servicevisits`] : response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}, 0);
}

export function addServiceVisit() {
	this.props.createOrEdit('/createActivity', null, {
		parentid: this.props.resource.id,
		parentresource: 'servicecalls',
		servicecallid : this.props.resource.id,
		contactid : this.props.resource.installationcontactid,
		partnerid : this.props.resource.customerid,
		tempactivitycategory: 'Service Allocation',
		param: 'servicecalls'
	}, (valueObj) => {
		this.controller.getServiceVisits();
	});
}

export function openServiceVisit(item) {
	let tempObj = {...item};
	tempObj.tempactivitycategory = 'Service Allocation';
	this.props.createOrEdit('/details/activities/:id', item.id, tempObj, (valueObj) => {
		this.controller.getServiceVisits();
	});
}

export function cancel () {
	this.props.history.goBack();
}