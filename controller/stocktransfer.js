import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import { uomFilter, currencyFilter } from '../utils/filter';
import StockdetailsModal from '../containers/stockdetails';
import StockTransferReceiptNotePickingModal from '../components/details/stocktransferreceiptnotepickingmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		companyid: this.props.app.selectedcompanyid,
		transferdate: new Date(new Date().setHours(0, 0, 0, 0)),
		deliverydate: new Date(new Date().setHours(0, 0, 0, 0)),
		receiveddate: new Date(new Date().setHours(0, 0, 0, 0)),
		stocktransferitems: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'copy') {
			tempObj = this.props.location.params
		} else if(params.param == "Project Purchase Planner") {
			tempObj.projectid = params.projectid;
			tempObj.projectid_projectno = params.projectid_projectno;
			tempObj.projectid_projectname = params.projectid_projectname;

			tempObj.destinationwarehouseid = params.projectid_warehouseid;
			tempObj.sourcewarehouseid = null;

			params.purchaseItemArray.forEach(item => {
				if(item.itemid_issaleskit || item.itemid_itemtype != 'Product' || !item.itemid_keepstock)
					return null;

				let quantity = item.quantity;

				if(item.uomid != item.stockuomid)
					quantity = Number((quantity / (item.requestconversionfactor ? item.requestconversionfactor : 1)).toFixed(this.props.app.roundOffPrecisionStock));

				tempObj.stocktransferitems.push({
					itemid: item.itemid,
					itemid_name: item.itemid_name,
					itemid_itemtype: item.itemid_itemtype,
					itemid_keepstock: item.itemid_keepstock,
					itemid_issaleskit: item.itemid_issaleskit,
					description: item.description,
					uomid: item.stockuomid,
					uomid_name: uomFilter(item.stockuomid, this.props.app.uomObj),
					quantity: quantity
				});
			});
		}
	}

	this.props.initialize(tempObj);	
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/stocktransfer/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});

			this.controller.setVisibilityForButton();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackProject (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		projectid_projectname: id ? valueobj.projectname : null,
		projectid_projectno: id ? valueobj.projectno : null,
		destinationwarehouseid: id ? valueobj.warehouseid : null
	});
}

export function callbackItem (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid_name`] = valueobj.name;
	tempObj[`${itemstr}.description`] = valueobj.description;
	tempObj[`${itemstr}.itemid_keepstock`] = valueobj.keepstock;
	tempObj[`${itemstr}.itemid_issaleskit`] = valueobj.issaleskit;
	tempObj[`${itemstr}.uomid`] = valueobj.stockuomid;
	tempObj[`${itemstr}.uomid_name`] = valueobj.stockuomid_name;
	tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;

	this.customFieldsOperation('itemmaster', tempObj, valueobj, 'stocktransferitems', itemstr);

	this.props.updateFormState(this.props.form, tempObj);
}	

export function openStockDetails (item) {
	this.openModal({render: (closeModal) => {return <StockdetailsModal resource = {this.props.resource} item={item} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param !='Cancel' && param !='Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/stocktransfer'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/stocktransfer/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/stocktransfer");
				else
					this.props.initialize(response.data.main);

				this.controller.setVisibilityForButton();
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function setVisibilityForButton () {
	setTimeout(() => {
		let showDelivery = false;
		let showReceipt = false;
		this.props.resource.stocktransferitems.forEach((item) => {
			let quantity = item.quantity ? item.quantity : 0;
			let deliveredqty = item.deliveredqty ? item.deliveredqty : 0;
			let receivedqty = item.receivedqty ? item.receivedqty : 0;
			if(quantity - deliveredqty > 0)
				showDelivery = true;
			if(deliveredqty - receivedqty > 0)
				showReceipt = true;
		});
		this.setState({
			showDelivery,
			showReceipt
		});
	}, 0);
}

export function getReceiptNoteDetails () {
	this.openModal({
		render: (closeModal) => {
			return <StockTransferReceiptNotePickingModal resource={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} closeModal={closeModal} openModal={this.openModal} customFieldsOperation={this.customFieldsOperation} getCustomFields={this.getCustomFields} />
	}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function createDC () {
	this.props.history.push({pathname: '/createDeliveryNote', params: {...this.props.resource, param: 'Stock Transfer'}});
}

export function createReceipt () {
	this.props.history.push({pathname: '/createReceiptNote', params: {...this.props.resource, param: 'Stock Transfer'}});
}

export function cancel () {
	this.props.history.goBack();
}
