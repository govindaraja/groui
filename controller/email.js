import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById () {
	axios.get(`/api/email/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);

			this.props.updateFormState(this.props.form, {
				relatedResourceName : this.props.app.myResources[this.props.resource.relatedresource].displayName
			});
			setTimeout(this.controller.getEmailAttachment, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getEmailAttachment() {
	let emailattachmentdetails = [];
	if(this.props.resource.attachment) {
		let tempfilename = this.props.resource.attachment.substr(0, this.props.resource.attachment.indexOf('?'));

		let fileName = tempfilename.substr(tempfilename.lastIndexOf('/')+1);

		emailattachmentdetails.push({
			name: fileName,
			url: this.props.resource.attachment
		});
	}

	if(this.props.resource.extradocuments.array.length > 0) {
		this.props.resource.extradocuments.array.forEach((attachment) => {
			emailattachmentdetails.push({
				name: attachment.name,
				url: attachment.fileurl
			});
		});
	}

	this.props.updateFormState(this.props.form, {
		emailattachmentdetails : emailattachmentdetails
	});
}

export function openRelatedId () {
	if(!['notifications', 'customerstatements'].includes(this.props.resource.relatedresource))
		this.props.history.replace(`/details/${this.props.resource.relatedresource}/${this.props.resource.relatedid}`);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	let tempData = {
		actionverb : param,
		data : this.props.resource,
		ignoreExceptions : confirm ? true : false
	}

	axios({
		method : 'post',
		data : tempData,
		url : '/api/email'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == 'success') {
			this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}