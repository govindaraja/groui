import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, detailsSVC, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import PricelistversionCopyModal from '../components/details/pricelistversioncopymodal';

export function onLoad () {
	if(this.state.createParam)
		this.props.initialize({pricelistitems:[]});
	else
		this.controller.getItemById();
}

export function getItemById () {
	axios.get(`/api/pricelistversions/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function copyPriceListVersion () {
	this.updateLoaderFlag(true);
	axios.get(`/api/pricelistversions?field=id,status&filtercondition=parentid=${this.props.resource.parentid}`).then((response) => {
		if (response.data.message == 'success') {
			let canCopyVersion = true;
			for (var i = 0; i < response.data.main.length; i++) {
				if (response.data.main[i].status != 'Approved') {
					canCopyVersion = false;
					break;
				}
			}

			if (canCopyVersion) {
				this.openModal({render: (closeModal) => {
					return  <PricelistversionCopyModal resource={this.props.resource} closeModal={closeModal} callback={(description) => {
						this.controller.saveCopiedPricelistversion(description);
					}} />
					}, className: {
						content: 'react-modal-custom-class',
						overlay: 'react-modal-overlay-custom-class'
					}
				});

			} else {
				this.props.openModal(modalService.infoMethod({
					header : "Error",
					body : "Please approve the exisiting versions before create new version!!!!!",
					btnArray : ["Ok"]
				}));
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function exportPriceList () {
	this.updateLoaderFlag(true);
	axios.get(`/api/query/pricelistitemsquery?versionid=${this.props.resource.id}`).then((response) => {
		if(response.data.message=='success') {
			let win = window.open('/print/' + response.data.filename + '?url=' + response.data.url);
			if (!win) {
				this.props.openModal(modalService['infoMethod']({
					header : 'Warning',
					body : "Popup Blocker is enabled! Please add this site to your exception list.",
					btnArray : ['Ok']
				}));
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function uploadExcel (file) {
	this.updateLoaderFlag(true);

	if (['xls', 'xlsx'].indexOf(file.name.split('.')[file.name.split('.').length - 1]) >= 0) {
		let tempObj = {
			parentresource: "pricelistversions",
			resourceName: "pricelistversions",
			parentid: this.props.resource.parentid,
			id : this.props.resource.id
		};

		let tempData = {
			actionverb : 'Import',
			data : tempObj
		};

		const formData = new FormData();
		formData.append('file', file);
		formData.append('data', JSON.stringify(tempObj));
		formData.append('actionverb', 'Import');

		const config = {
			headers: {
				'content-type': 'multipart/form-data'
			}
		};

		if (file.size > 10485760) {
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "File Size should not be exceed than <b class='text-danger'>10 MB</b>",
				btnArray : ["Ok"]
			}));
		}

		axios.post('/upload', formData, config).then((response) => {
			if (response.data.message == 'success') {
				this.controller.getItemById();
				this.setState({
					pricelistVersionItemsRefreshCount: (this.state.pricelistVersionItemsRefreshCount ? this.state.pricelistVersionItemsRefreshCount : 0) + 1
				});
			}
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			this.updateLoaderFlag(false);
		});
	} else {
		this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Please upload only xls, xlsx typed files",
			btnArray : ["Ok"]
		}));
		this.updateLoaderFlag(false);
	}
}

export function saveCopiedPricelistversion (description) {
	this.updateLoaderFlag(true);
	let tempData = {
		actionverb : 'Save',
		data : {
			priceversionid : this.props.resource.id,
			parentid : this.props.resource.parentid,
			remarks : description,
			currencyid : this.props.resource.currencyid,
			parentresource : 'pricelists'
		}
	};

	axios({
		method : 'POST',
		data : tempData,
		url : '/api/pricelistversions'
	}).then((response) => {
		if (response.data.message == 'success') {
			this.props.history.push(`/details/pricelistversion/${response.data.main.id}`);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	if (param == 'Approve' && !(this.props.resource.effectivedate))
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Effective Date is mandatory",
			btnArray : ["Ok"]
		}));
	this.updateLoaderFlag(true);

	let tempData = {
		actionverb : param,
		data : this.props.resource,
		ignoreExceptions : confirm ? true : false
	};

	axios({
		method : 'post',
		data : tempData,
		url : '/api/pricelistversions'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		});

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/pricelistversions/${response.data.main.id}`);
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/pricelists");
				} else {
					this.props.initialize(response.data.main);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
