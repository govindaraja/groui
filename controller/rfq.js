import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';
import { commonMethods, detailsSVC, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import SuggestsupplierModal from '../components/details/rfqsuggestsuppliermodal';
import AnalyseSupplierquotationModal from '../components/details/analysesupplierquotationmodal';
import EmailModal from '../components/details/emailmodal';
import PurchaseOrderItemRequestAddModal from '../components/details/purchaseorderitemrequestaddmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		rfqdate: new Date(new Date().setHours(0, 0, 0, 0)),
		rfqitems: [],
		rfqparticipants: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'copy') {
			tempObj = params;
		}

		if(params.param == 'Purchase Planner' || params.param == 'Project Purchase Planner') {
			for (var i = 0; i < params.purchaseArray.length; i++) {
				let tempChildObj = {
					index: i+1
				};

				utils.assign(tempChildObj, params.purchaseArray[i], ['itemid', 'itemid_name', 'description', 'quantity', 'uomid', 'uomid_name', 'uomconversiontype', 'uomconversionfactor', 'usebillinguom', 'billinguomid', 'billingconversiontype', 'billingconversionfactor', 'itemrequestitemsid', 'itemrequestid']);

				if(params.param == 'Project Purchase Planner') {
					tempChildObj.deliverydate = params.purchaseArray[i].requireddate ? params.purchaseArray[i].requireddate : null;
					tempChildObj.quantity = Number(((params.purchaseArray[i].quantity * (params.purchaseArray[i].uomconversionfactor ? params.purchaseArray[i].uomconversionfactor : 1)) / (params.purchaseArray[i].requestconversionfactor ? params.purchaseArray[i].requestconversionfactor : 1)).toFixed(this.props.app.roundOffPrecisionStock));
				}

				if(params.purchaseArray[i].usebillinguom)
					tempChildObj[`billingquantity`] = Number(((tempChildObj.quantity / (params.purchaseArray[i].uomconversionfactor ? params.purchaseArray[i].uomconversionfactor : 1)) * params.purchaseArray[i].billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));

				if(params.projectid > 0)
					this.customFieldsOperation('itemrequestitems', tempChildObj, params.purchaseArray[i], 'rfqitems');

				tempObj.rfqitems.push(tempChildObj);
			}

			if(params.projectid > 0) {
				utils.assign(tempObj, params, ['companyid', 'projectid', 'projectid_projectname',  'projectid_projectno', 'projectid_displayname', {'deliveryaddress': 'projectid_deliveryaddress'}, {'deliveryaddressid': 'projectid_deliveryaddressid'}, {'deliverydate': 'requireddate'}]);
				this.customFieldsOperation('itemrequests', tempObj, params, 'rfq');
			}
		}
	}

	this.props.initialize(tempObj);

	setTimeout(this.controller.getTandC, 0);

	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/rfq/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			for (var i = 0; i < tempObj.rfqitems.length; i++) {
				if (tempObj.rfqitems[i].uomconversiontype)
					tempObj.rfqitems[i].alternateuom = true;
			}
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			setTimeout(() => {
				this.controller.getparticipantsemail();
				this.controller.getSuppCount();
			}, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Rfq' and termsandconditions.isdefault`).then((response) => {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function tandcOnchange(id, valueobj) {
	if(id)
		this.props.updateFormState(this.props.form, {
			tandc: valueobj.terms
		});
}

export function addressonChange(address, addressobj) {
	this.props.updateFormState(this.props.form, {
		deliveryaddress: addressobj.displayaddress,
		deliveryaddressid: addressobj.id
	});
}

export function deliverydateonchange() {
	let tempObj = {};
	for (var i = 0; i < this.props.resource.rfqitems.length; i++) {
		if (!this.props.resource.rfqitems[i].deliverydate)
			tempObj[`rfqitems[${i}].deliverydate`] = this.props.resource.deliverydate;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson: valueobj.name,
		phone: valueobj.phone,
		email: valueobj.email,
		mobile: valueobj.mobile,
	});
}

export function callbackproject (id, valueobj) {
	let tempObj = {
		deliveryaddress: valueobj.deliveryaddress,
		deliveryaddressid: valueobj.deliveryaddressid
	};
	this.customFieldsOperation('projects', tempObj, valueobj, 'rfq');
	this.props.updateFormState(this.props.form, tempObj);
}

export function callBackPartner (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.partnerid_name`]: valueobj.name
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, null, null, 'purchase');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'rfqitems', itemstr);
		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		this.props.updateFormState(this.props.form, tempObj);

		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);

	}, (reason) => {});
}

export function quantityOnChange(itemqty, item, itemstr) {
	let tempObj = {};

	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));

		this.props.updateFormState(this.props.form, tempObj);
	}
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, this.props.resource.partnerid, this.props.resource.pricelistid, 'purchase', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let updateObj = {};

		if(item.usebillinguom)
			updateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));

		this.props.updateFormState(this.props.form, updateObj);

	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, this.props.resource.partnerid, this.props.resource.pricelistid, 'purchase', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			if(item.usebillinguom)
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));

			this.props.updateFormState(this.props.form, tempObj);
		}, (reason) => {});
	}
}

export function getItemRequest () {
	this.openModal({render: (closeModal) => {
		return <PurchaseOrderItemRequestAddModal resource={this.props.resource} jsonname={"rfqitems"} openModal={this.props.openModal} closeModal={closeModal} callback={this.controller.itemreqCallBack} />
	}, className: {
		content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
	});
}


export function itemreqCallBack(itemRequestItemsArray) {	
	this.updateLoaderFlag(true);

	let rfqitems = [...this.props.resource.rfqitems];

	let itemrequestidArr = [];

	let index = 0, itempushedflag = false;

	for (var k = 0; k < rfqitems.length; k++) {
		if (rfqitems[k].index > index)
			index = rfqitems[k].index;
	}

	for(var i=0; i<itemRequestItemsArray.length; i++) {
		let itemFound=false;
		for(var j = 0; j < rfqitems.length; j++) {
			if(rfqitems[j].itemrequestitemsid == itemRequestItemsArray[i].itemrequestitemsid) {
				itemFound=true;
				if (!itemRequestItemsArray[i].checked) {
					rfqitems.splice(j, 1);
				}
				break;
			}
		}
		if(!itemFound && itemRequestItemsArray[i].checked) {
			index++;
			itempushedflag = true;

			itemRequestItemsArray[i].quantity = itemRequestItemsArray[i].suggestedqty;
			itemRequestItemsArray[i].uomid = itemRequestItemsArray[i].purchaseuomid;
			itemRequestItemsArray[i].uomid_name = itemRequestItemsArray[i].purchaseuomid_name;

			let tempChildObj = {
				index: index,
				alternateuom: itemRequestItemsArray[i].uomconversiontype ? true : false,
				newitem: true,
				deliverydate: itemRequestItemsArray[i].deliverydate ? itemRequestItemsArray[i].deliverydate : this.props.resource.deliverydate
			};

			utils.assign(tempChildObj, itemRequestItemsArray[i], ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemid_keepstock', 'itemid_uomgroupid', 'description', 'quantity', 'uomid', 'uomid_name', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'remarks', 'usebillinguom', 'billinguomid', 'billinguomid_name', 'billingconversiontype', 'billingconversionfactor', 'itemrequestitemsid']);

			tempChildObj.quantity = Number(((itemRequestItemsArray[i].quantity * (itemRequestItemsArray[i].uomconversionfactor ? itemRequestItemsArray[i].uomconversionfactor : 1)) / (itemRequestItemsArray[i].requestconversionfactor ? itemRequestItemsArray[i].requestconversionfactor : 1)).toFixed(this.props.app.roundOffPrecisionStock));

			if(itemRequestItemsArray[i].usebillinguom) {
				tempChildObj.billingquantity = Number(((tempChildObj.quantity / (itemRequestItemsArray[i].uomconversionfactor ? itemRequestItemsArray[i].uomconversionfactor : 1)) * itemRequestItemsArray[i].billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.customFieldsOperation('itemrequestitems', tempChildObj, itemRequestItemsArray[i], 'rfqitems');

			rfqitems.push(tempChildObj);
		}
	}

	this.props.updateFormState(this.props.form, { rfqitems });
	this.updateLoaderFlag(false);
}

export function save (param, confirm) {
	if(param == 'Send To Supplier') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'rfq'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
	} else {
		this.updateLoaderFlag(true);

		if(param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/rfq'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					this.props.history.replace(`/details/rfq/${response.data.main.id}`);
				} else {
					if (param == 'Delete')
						this.props.history.replace("/list/rfq");
					else
						this.props.initialize(response.data.main);
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function getparticipantsemail () {
	if(!(this.props.resource.rfqparticipants.length > 0))
		return null;

	let tempObj = {};
	axios.get(`/api/contacts?field=id,email,parentid&filtercondition=contacts.isprimary=true and contacts.parentresource='partners' and contacts.parentid in (${this.props.resource.rfqparticipants.map((a) => a.partnerid).join()})`).then((response) => {
		if (response.data.message == 'success') {
			tempObj.contactid_email = response.data.main.map((a) => a.email).join();
			this.props.resource.rfqparticipants.forEach((rfqparticipant, i) => {
				response.data.main.forEach((partner) => {
					if(rfqparticipant.partnerid == partner.parentid)
						tempObj[`rfqparticipants[${i}].partnerid_email`] = partner.email;
				});
			});
			this.props.updateFormState(this.props.form, tempObj);
		}
	});
}

export function getSuppCount () {
	if (!this.props.resource.status == 'Sent To Supplier')
		return null;

	axios.get(`/api/supplierquotation?field=id,supplierquoteno,rfqid,supplierid,partners/name/partnerid,finaltotal&filtercondition=supplierquotation.rfqid=${this.props.resource.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.setState({
				supplierQuotes: response.data.main,
				supplierCount: response.data.main.length
			}, () => {
				this.controller.analyseButton();
			});
		}
	});
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Supplier',
			data : emailObj
		},
		url : '/api/rfq'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					let bccArray= [];
					this.props.resource.rfqparticipants.forEach((item) => {
						if (item.partnerid_email)
							bccArray.push(item.partnerid_email);
					});

					emailObj = response.data.main;
					emailObj.toaddress = this.props.app.user.email;
					emailObj.bcc = bccArray.length > 0 ? bccArray.join() : response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function createSupplierQuote(partnerobj) {
	this.props.history.push({
		pathname: '/createSupplierQuotation',
		params: { 
			...this.props.resource,
			partnerid: partnerobj.partnerid,
			param: 'RFQ'
		}
	});
}

export function getVendor (vendorDetails) {
	let rfqparticipants = [...this.props.resource.rfqparticipants];
	vendorDetails.forEach((vendor) => {
		if(vendor.check) {
			let itemFound = false;
			rfqparticipants.forEach((participant) => {
				if(vendor.partnerid == participant.partnerid)
					itemFound = true;
			});
			rfqparticipants.push(vendor);
		}
	});
	this.props.updateFormState(this.props.form, {
		rfqparticipants: rfqparticipants
	});
}

export function suggestVendor () {
	if(!(this.props.resource.rfqitems.length > 0) || this.props.resource.rfqitems.filter(item => item.itemid > 0).length == 0)
		return null;

	let vendorDetails = [];

	axios.get(`/api/query/suggestvendorquery?itemarray=${this.props.resource.rfqitems.filter(item => item.itemid > 0).map((a) => a.itemid).join()}`).then((response) => {
		if (response.data.message == 'success') {
			response.data.main.forEach((partner) => {
				this.props.resource.rfqparticipants.forEach((participant) => {
					if(partner.partnerid == participant.partnerid)
						partner.check = true;
				});
			});
			vendorDetails = response.data.main;


			this.openModal({
				render: (closeModal) => {
					return <SuggestsupplierModal vendorDetails={vendorDetails} callback={this.controller.getVendor} closeModal={closeModal} />
				}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
			});
		}
	});
}

export function analyseButton () {
	if(!(this.state.supplierQuotes.length > 0))
		return null;

	axios.get(`/api/supplierquotationitems?field=id,parentid,itemid,itemmaster/name/itemid,uomid,uom/name/uomid,quantity,amount&filtercondition=supplierquotationitems.parentid in (${this.state.supplierQuotes.map((a) => a.id).join()})`).then((response) => {
		if(response.data.message == 'success') {
			let {supplierQuotes} = this.state;
			supplierQuotes.forEach((quote) => {
				quote.supplierquotationitems = {};
				response.data.main.forEach((quoteitem) => {
					if(quoteitem.parentid == quote.id)
						quote.supplierquotationitems[quoteitem.itemid] = quoteitem;
				});
			});
			this.setState({supplierQuotes});
		}
	});
}

export function getAnalyseAlert() {
	return (
		<div>{this.state.supplierCount} Supplier Quotations found . <span onClick={this.controller.openSupplierQuotations} className='alert-link'>Analyse</span> </div>
	);
}

export function openSupplierQuotations() {
	setTimeout(() => {
		this.openModal({
			render: (closeModal) => {
				return <AnalyseSupplierquotationModal rfqitems={this.props.resource.rfqitems} supplierquotes={this.state.supplierQuotes}  history={this.props.history} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}, 0);
}

export function renderQty(item) {
	return `${item.quantity} ${uomFilter(item.uomid, this.props.app.uomObj)}`;
}

export function cancel () {
	this.props.history.goBack();
}
