import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		let tempObj = {
			disableemail: false,
			changepassword: false
		};
		if(this.props.app.user.roleid.indexOf(1) == -1) {
			tempObj.userids = [];
			tempObj.email = this.props.app.user.email;
			tempObj.name = this.props.app.user.displayname;
			tempObj.userids.push(this.props.app.user.id);
			tempObj.disableemail = true;
		}
		this.props.initialize(tempObj);
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
	this.controller.getAllUsers();
}

export function getAllUsers() {
	axios.get(`/api/users?field=id,displayname`).then((response) => {
		if(response.data.message == 'success') {
			this.setState({
				usersArray: response.data.main
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getItemById () {
	axios.get(`/api/emailaccounts/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempobj = response.data.main;
			tempobj.disableemail = this.props.app.user.roleid.indexOf(1) == -1 ? true : false;
			this.props.initialize(tempobj);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function mailserverChange () {
	let tempObj = {
		password: null,
		smtphost: '',
		smtpport: '',
		smtpsecureconnection: null,
		imaphost: '',
		imaphost: '',
		imapusetls: null,
		imapusetls: null,
		preferedmailbox: ['INBOX', '[Gmail]/Sent Mail']
	};
	
	if(this.props.resource.mailserver == 'Gmail' || this.props.resource.mailserver == 'Gsuite') {
		tempObj.smtphost = 'smtp.gmail.com';
		tempObj.smtpport = '587';
		tempObj.smtpsecureconnection = 'TLS';
		tempObj.imaphost = 'imap.gmail.com';
		tempObj.imapport = '993';
		tempObj.imapusetls = true;
		tempObj.preferedmailbox = ['INBOX','[Gmail]/Sent Mail'] ;
		tempObj.isincoming = this.props.resource.mailserver == 'Gmail' ? false : this.props.resource.isincoming;
	}
	
	if(this.props.resource.mailserver == 'Yahoo') {
		tempObj.smtphost = 'smtp.mail.yahoo.com';
		tempObj.smtpport = '587';
		tempObj.smtpsecureconnection = 'TLS';
		tempObj.imaphost = 'imap.mail.yahoo.com';
		tempObj.imapport = '993';
		tempObj.imapusetls = true;
		tempObj.preferedmailbox = ['Inbox','Sent'] ;
	}
	
	if(this.props.resource.mailserver == 'Office365') {
		tempObj.smtphost = 'smtp.office365.com';
		tempObj.smtpport = '587';
		tempObj.smtpsecureconnection = 'TLS';
		tempObj.imaphost = 'outlook.office365.com';
		tempObj.imapport = '993';
		tempObj.imapusetls = true;
		tempObj.preferedmailbox = ['Inbox','Sent'] ;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function forgotPass () {
	this.props.updateFormState(this.props.form, {
		password: ''
	});
}

export function save (param) {
	this.updateLoaderFlag(true);
	if((this.props.resource.email && this.props.app.user.email != this.props.resource.email) && this.props.app.user.roleid.indexOf(1) == -1) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService['infoMethod']({
			header : 'Access denied',
			body : 'You are not allowed to create or save others emailaccount',
			btnArray : ['Ok']
		}));
	}

	if(this.props.resource.password && (!this.props.resource.id || this.props.resource.changepassword) && param == 'Save' && (this.props.resource.mailserver != 'Gsuite')) {
		axios.get(`/api/common/methods/encryptpassword?usecrypto=true&newpassword=${encodeURIComponent(this.props.resource.password)}`).then((response) => {
			this.props.updateFormState(this.props.form, {
				password: response.data.password
			});
			setTimeout(() => {
				this.controller.saveEmailaccount(param);
			}, 0);
		});
	} else {
		this.controller.saveEmailaccount(param);
	}
}

export function saveEmailaccount (param, confirm) {

	let tempData = {
		actionverb : param,
		data : this.props.resource,
		ignoreExceptions : confirm ? true : false
	};

	axios({
		method : 'post',
		data : tempData,
		url : '/api/emailaccounts'
	}).then((response) => {
		if (!this.state.createParam || response.data.message != 'success') {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.saveEmailaccount(param, true);
			}));
		}

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/emailaccounts/${response.data.main.id}`);
				let message = {
					header : 'Success',
					body : 'Email account is added. Please click on Verify to start using this account.',
					btnArray : ['Ok']
				};
				this.props.openModal(modalService['infoMethod'](message));
			} else {
				if(param == 'Verify' && response.data.main.oauthurl) {
					window.location = response.data.main.oauthurl;
				} else if(param != 'Delete') {
					let tempObj = response.data.main;
					tempObj.changepassword = false;
					this.props.initialize(tempObj);
				} else {
					this.props.history.replace("/list/emailaccounts");
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.replace("/list/emailaccounts");
}
