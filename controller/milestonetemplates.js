import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';
import * as utils from '../utils/utils';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({
			milestonetemplateitems: []
		});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById () {
	axios.get(`/api/milestonetemplates/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackMilestoneStage(value, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.milestonestageid_name`]: valueobj.name
	});
}

export function save (param) {
	this.updateLoaderFlag(true);

	axios({
		method : 'POST',
		data : {
			actionverb : param,
			data : this.props.resource
		},
		url : '/api/milestonetemplates'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

		if (response.data.message == 'success') {
			if (this.state.createParam)
				this.props.history.replace(`/details/milestonetemplates/${response.data.main.id}`);
			else if(param == 'Delete')
				this.props.history.replace('/list/milestonetemplates');
			else {
				this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
