import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		contractentitlements: [],
		servicearray: [],
		componentsarray: [],
		itemgrouparray: [],
		itemcategoryarray: []
	};

	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/contracttypes/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = {};
			tempObj = response.data.main;
			tempObj.servicearray = [];
			tempObj.componentsarray = [];
			tempObj.itemgrouparray = [];
			tempObj.itemcategoryarray = [];
			for(var i=0;i<tempObj.contractentitlements.length;i++) {
				if(tempObj.contractentitlements[i].itemid_itemtype=='Product')
					tempObj.componentsarray.push(tempObj.contractentitlements[i]);
				if(tempObj.contractentitlements[i].itemid_itemtype=='Service')
					tempObj.servicearray.push(tempObj.contractentitlements[i]);
				if(tempObj.contractentitlements[i].itemgroupid)
					tempObj.itemgrouparray.push(tempObj.contractentitlements[i]);
				if(tempObj.contractentitlements[i].itemcategoryid)
					tempObj.itemcategoryarray.push(tempObj.contractentitlements[i]);
			}
			this.props.initialize(tempObj);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.itemid_name`] : valueobj.name
	});
}

export function callbackItemcategory (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.itemcategoryid_name`] : valueobj.name
	});
}

export function callbackItemgroup (id, valueobj, item, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.itemgroupid_fullname`] : valueobj.fullname
	});
}

export function save (param) {
	let errArray = [];
	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}
	let contractentitlementsArray = [];

	for (var i = 0; i < this.props.resource.componentsarray.length; i++) {
		let componentFound = false;
		for (var j = 0; j < contractentitlementsArray.length; j++) {
			if (this.props.resource.componentsarray[i].itemid == contractentitlementsArray[j].itemid) {
				errArray.push("Item " + this.props.resource.componentsarray[i].itemid_name + " already exists");
				componentFound = true;
				break;
			}
		}
		if (!componentFound)
			contractentitlementsArray.push(this.props.resource.componentsarray[i]);
	}
	for (var i = 0; i < this.props.resource.servicearray.length; i++) {
		let serviceFound = false;
		for (var j = 0; j < contractentitlementsArray.length; j++) {
			if (this.props.resource.servicearray[i].itemid == contractentitlementsArray[j].itemid) {
				errArray.push("Item " + this.props.resource.servicearray[i].itemid_name + " already exists");
				serviceFound = true;
				break;
			}
		}
		if (!serviceFound)
			contractentitlementsArray.push(this.props.resource.servicearray[i]);
	}
	for(var i = 0; i < this.props.resource.itemgrouparray.length; i++) {
		let itemgroupFound = false;
		for (var j = 0; j < contractentitlementsArray.length; j++) {
			if (this.props.resource.itemgrouparray[i].itemgroupid == contractentitlementsArray[j].itemgroupid) {
				errArray.push("Item Group " + this.props.resource.itemgrouparray[i].itemgroupid_fullname + " already exists");
				itemgroupFound = true;
				break;
			}
		}
		if (!itemgroupFound)
			contractentitlementsArray.push(this.props.resource.itemgrouparray[i]);
	}
	for (var i = 0; i < this.props.resource.itemcategoryarray.length; i++) {
		let itemcategoryFound = false;
		for (var j = 0; j < contractentitlementsArray.length; j++) {
			if (this.props.resource.itemcategoryarray[i].itemcategoryid == contractentitlementsArray[j].itemcategoryid) {
				errArray.push("Item Category " + this.props.resource.itemcategoryarray[i].itemcategoryid_name + " already exists");
				itemcategoryFound = true;
				break;
			}
		}
		if (!itemcategoryFound)
			contractentitlementsArray.push(this.props.resource.itemcategoryarray[i]);
	}

	if(errArray.length > 0) {
		let errResponse = {
			data: {
				message: 'failure',
				error: errArray
			}
		};
		let apiResponse = commonMethods.apiResult(errResponse);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	} else {
		this.updateLoaderFlag(true);
		this.props.updateFormState(this.props.form, {contractentitlements : contractentitlementsArray});
		setTimeout(() => {
			this.controller.saveContractType(param)
		}, 0);
	}
}

export function saveContractType (param, confirm) {
	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/contracttypes'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.saveContractType(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/contracttypes/${response.data.main.id}`);
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/contracttypes");
				} else {
					this.props.initialize(response.data.main);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
