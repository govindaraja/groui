import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';
import ActivityTypeModal from '../components/details/activitytypemodal';
import NextActivityConfirmModal from '../components/details/nextactivityalertmodal';

export function onLoad () {
	let allowActivityOverwrite = false,
		showservicesection = this.props.isModal ? true : false,
		isModal = this.props.isModal ? true : false;

	this.props.app.feature.allowActivityOverwrite.some(id => {
		if(this.props.app.user.roleid.indexOf(id) >= 0) {
			allowActivityOverwrite = true;
			return true;
		}
	});

	this.setState({ allowActivityOverwrite, showservicesection, isModal }, () => {
		if(this.state.createParam)
			this.controller.initialiseState();
		else
			this.controller.getItemById();
	});
}

export function initialiseState() {
	let tempObj = {
		activityhistorydetails : []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'Activity')
			tempObj.relatedactivityid = params.id;

		if(params.tempactivitycategory) {
			tempObj.tempactivitycategory = params.tempactivitycategory;
			//tempObj.assignedto = params.tempactivitycategory == 'Sales Followup' ? this.props.app.user.id : null;
		}

		tempObj.parentid = params.parentid;
		tempObj.parentresource = params.parentresource;

		tempObj.partnerid = params.partnerid;
		tempObj.contactid = params.contactid;
		tempObj.leadid = params.leadid;
		tempObj.quoteid = params.quoteid;
		tempObj.projectquoteid = params.projectquoteid;
		tempObj.servicecallid = params.servicecallid;
		tempObj.plannedstarttime = params.plannedstarttime;
		tempObj.plannedendtime = params.plannedendtime;


		if(params.assignedto)
			tempObj.assignedto = params.assignedto;

		if(params.param == 'Dialer') {
			tempObj.contactid = params.contactid;
			tempObj.contactperson = params.contactperson;
			tempObj.partnerid = params.customerid;
			tempObj.startdatetime = params.startdatetime;
			tempObj.enddatetime = params.enddatetime;
		}
	}

	this.props.initialize(tempObj);
	if(!this.props.isModal)
		this.controller.showActivityTypeModal();

	this.setState({
		hidecontactdetailsection: this.props.location.params ? this.props.location.params.hidehistorypartnersection : false
	});
	this.updateLoaderFlag(false);
}

export function showActivityTypeModal() {
	this.updateLoaderFlag(true);
	this.openModal({render: (closeModal) => {
			return <ActivityTypeModal closeModal={closeModal} openModal={this.openModal} resource = {this.props.resource} callback={(val) => {
				if(val) {
					this.props.updateFormState(this.props.form, {
						activitytypeid: val.id,
						tempactivitycategory: val.category
					});
					this.controller.activitytypeOnChange(val.id, val);
				} else{
					if(this.props.isModal){
						this.props.closeModal();
					}else{
						this.props.history.goBack();
					}
				}
				this.updateLoaderFlag(false);
			}}/>
		}, className: {
			content: 'react-modal-custom-class-30', overlay: 'react-modal-overlay-custom-class'
		}, confirmModal: true
	});
}

export function generateReleatedPageURL() {
	return `/details/${this.props.resource.parentresource}/${this.props.resource.parentid}`;
}

export function renderSubActivityStatusfield(item) {
	let statusObj = {
		"Open" : "gs-bg-info text-primary semi-bold",
		"Planned" : "gs-bg-info text-primary semi-bold",
		"Inprogress" : "gs-bg-warning gs-text-warning semi-bold",
		"Completed" : "gs-bg-success text-success semi-bold",
		"Cancelled" : "gs-bg-danger text-danger semi-bold"
	}
	return <div className="text-center"><span className={`${statusObj[item.status]}`} style={{padding: '3px 6px', borderRadius: '2px'}}>{item.status}</span></div>;
}

export function renderSubActivityAnchorfield(item) {
	return <a className="gs-anchor" href="javascript:void(0);" onClick={()=> this.controller.openSubActivity(item)}>Open</a>
}

export function openSubActivity(item) {
	if(this.props.isModal)
		return this.props.createOrEdit('/details/activities/:id', item.id, null, (valueObj) => {
			this.controller.getSubActivities();
		});
	else
		this.props.history.push(`/details/activities/${item.id}`);
}

export function gotoRelatedPage() {
	if (this.props.isModal)
		this.props.closeModal();
	else {
		if(this.props.resource.parentresource == 'partners')
			this.props.history.push({pathname: '/customerstatementreport', params: {partnerid: this.props.resource.parentid}});
		else
			this.props.history.push(`/details/${this.props.resource.parentresource}/${this.props.resource.parentid}`);
	}
}

export function getItemById () {
	axios.get(`/api/salesactivities/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.tempactivitycategory = response.data.main.activitytypeid_category;
			tempObj.activityhistorydetails = [];
			tempObj.linkdisplayname = tempObj.parentresource ? `${this.props.app.myResources[tempObj.parentresource].displayName} - ${tempObj.parentid}` : '';
			this.props.initialize(tempObj);
			this.setState({
				hidecontactdetailsection: this.props.location.params ? this.props.location.params.hidehistorypartnersection : false,
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			},()=>{
				this.controller.getActivities();
				this.controller.getSubActivities();
				if(this.props.resource.parentid > 0){
					this.controller.getRelatedResource();
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getSubActivities() {
	if(!this.props.resource.activitytypeid_allowsubactivity)
		return null;

	let queryString = `/api/salesactivities?&field=id,activitytypeid,activitytypes/name/activitytypeid,description,status,&filtercondition=salesactivities.relatedactivityid=${this.props.resource.id}`;

	axios.get(`${queryString}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.updateFormState(this.props.form, {
				subactivities: response.data.main
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callBackPartner(id, valueobj) {
	this.props.updateFormState(this.props.form,  {
		contactid: valueobj.contactid,
		contactid_email: valueobj.contactid_email,
		contactid_mobile: valueobj.contactid_mobile,
		contactid_phone: valueobj.contactid_phone
	});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form,  {
		contactperson: valueobj.name,
		contactid_organization: valueobj.organization,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile
	});
}

export function showContactDetails() {
	this.props.createOrEdit('/createContact', null, {}, (valueobj) => {
		this.props.updateFormState(this.props.form,  {
			contactid: valueobj.id,
			contactperson: valueobj.name,
			contactid_organization: valueobj.organization,
			contactid_phone: valueobj.phone,
			contactid_email: valueobj.email,
			contactid_mobile: valueobj.mobile
		});
	});
}

export function getRelatedResource(){
	let titleField = '';
	for(var prop in this.props.app.myResources[this.props.resource.parentresource].fields) {
		if(this.props.app.myResources[this.props.resource.parentresource].fields[prop].title)
			titleField = prop;
	}
	axios.get(`/api/${this.props.resource.parentresource}?field=${titleField}&filtercondition=${this.props.resource.parentresource}.id=${this.props.resource.parentid}`).then((response)=> {
		if (response.data.message == 'success') {
			this.props.updateFormState(this.props.form,  {
				linkdisplayname : `${this.props.resource.parentresource == 'partners' && this.props.resource.activitytypeid_category == 'Payment Followup' ? 'Partner Account Details' : this.props.app.myResources[this.props.resource.parentresource].displayName} - ${response.data.main[0][`${titleField}`]}`
			});
		} else {
			this.props.updateFormState(this.props.form,  {
				linkdisplayname : `${this.props.app.myResources[this.props.resource.parentresource].displayName} - ${this.props.resource.parentid}`
			});
		}
		this.updateLoaderFlag(false);
	});
}

export function getActivities(param) {
	let { activityhistorydetails } = this.props.resource;

	let pagelengthStr = '';
	if(!param) {
		pagelengthStr += `&pagelength=3`;
	}
	let queryString = `/api/salesactivities?field=id,created,modified,description,activitytypeid,activitytypes/name/activitytypeid,duedate,plannedstarttime,plannedendtime,startdatetime,enddatetime,status,createdby,users/displayname/createdby,assignedto,users/displayname/assignedto,checkindetails,checkoutdetails,completionremarks&filtercondition=salesactivities.id!=${this.props.resource.id} and (salesactivities.parentresource='${this.props.resource.parentresource}' and salesactivities.parentid=${this.props.resource.parentid}) and activitytypes_activitytypeid.category != 'General'${pagelengthStr}`;

	axios.get(queryString).then((response)=> {
		if(response.data.message == 'success') {
			response.data.main.forEach((item) => {
				activityhistorydetails.push(item);
			});

			this.props.updateFormState(this.props.form, {
				activityhistorydetails:[...activityhistorydetails],
				showactivity : activityhistorydetails.length >= Number(response.data.count) ? false : true,
				tempshowactivity : this.props.resource.tempshowactivity ? !this.props.resource.tempshowactivity : true
			});
		}
	});
}

export function activitytypeOnChange(id, valueobj) {
	let tempassignedto = null;
	if((valueobj.category == 'Sales Followup' && this.props.app.user.issalesperson) || (valueobj.category == 'Service Allocation' && this.props.app.user.isengineer) || (!['Sales Followup', 'Service Allocation'].includes(valueobj.category))) {
		tempassignedto = this.props.app.user.id;
	}

	this.props.updateFormState(this.props.form,  {
		activitytypeid_name: valueobj.name,
		activitytypeid_category: valueobj.category,
		activitytypeid_entrymode: valueobj.entrymode,
		activitytypeid_planningtype: valueobj.planningtype,
		activitytypeid_trackduration: valueobj.trackduration,
		activitytypeid_showduedate: valueobj.showduedate,
		activitytypeid_triptracking: valueobj.triptracking,
		activitytypeid_allowsubactivity: valueobj.allowsubactivity,
		activitytypeid_isgeotagged: valueobj.isgeotagged,
		activitytypeid_planningduration: valueobj.planningduration,
		assignedto: tempassignedto
	});

	setTimeout(() => {
		if(!this.props.resource.plannedendtime)
			this.controller.plannedstarttimeOnChange();
	}, 0);
}

export function plannedstarttimeOnChange() {
	if(this.props.resource.activitytypeid_planningduration && this.props.resource.plannedstarttime) {
		this.props.updateFormState(this.props.form,  {
			plannedendtime: new Date(new Date(this.props.resource.plannedstarttime).getTime() + this.props.resource.activitytypeid_planningduration*60000)
		});
	}
}

export function calculateTimeDuration (param) {
	let fieldObj = {
		actual: {
			start: 'startdatetime',
			end: 'enddatetime',
			duration: 'duration'
		},
		travel: {
			start: 'travelstarttime',
			end: 'travelendtime',
			duration: 'travelduration'
		}
	};

	if(!this.props.resource[fieldObj[param].start] || !this.props.resource[fieldObj[param].end])
		this.props.updateFormState(this.props.form, {
			[`${fieldObj[param].duration}`]: null
		});

	if (this.props.resource[fieldObj[param].start] != null && this.props.resource[fieldObj[param].end] != null) {
		if(new Date(new Date(this.props.resource[fieldObj[param].start]).setSeconds(0, 0)).getTime() <= new Date(new Date(this.props.resource[fieldObj[param].end]).setSeconds(0, 0)).getTime()) {
			let timeStart = new Date(new Date(this.props.resource[fieldObj[param].start]).setSeconds(0, 0)).getTime();
			let timeEnd = new Date(new Date(this.props.resource[fieldObj[param].end]).setSeconds(0, 0)).getTime();
			let timeDiff = Math.abs(timeEnd - timeStart);

			let minutes = Math.floor(timeDiff / 1000 / 60);

			this.props.updateFormState(this.props.form, {
				[`${fieldObj[param].duration}`]: minutes
			});
		}
	}
}

export function createSubActivity() {
	this.updateLoaderFlag(true);
	let tempObj = JSON.parse(JSON.stringify(this.props.resource));
	tempObj.relatedactivityid  = tempObj.id;
	delete tempObj.tempactivitycategory;

	if(this.props.isModal) {
		this.props.createOrEdit('/createActivity', null, {...tempObj, param: 'Activity'}, (valueobj) => {
			this.updateLoaderFlag(false);
		});
	} else {
		this.props.history.push({pathname: '/createActivity', params: {...tempObj, param: 'Activity'}});
		this.updateLoaderFlag(false);
	}
}

export function activityActionBtn(additionalparam) {
	this.updateLoaderFlag(true);

	let tempObj = JSON.parse(JSON.stringify(this.props.resource));
	let tempActionVerb = additionalparam == 'workend' ? 'Complete' : 'Save';
	tempObj.internalaction = additionalparam;
	/*if(additionalparam == 'travel') {
		if(!this.props.resource.travelstarttime)
			tempObj.travelstarttime = new Date();

		if(this.props.resource.travelstarttime && !this.props.resource.travelendtime)
			tempObj.travelendtime = new Date();
	}
	if(additionalparam == 'work') {
		if(!this.props.resource.startdatetime) {
			tempObj.startdatetime = new Date();
		}

		if(this.props.resource.startdatetime && !this.props.resource.enddatetime) {
			tempObj.enddatetime = new Date();
			tempActionVerb = 'Complete';
		}
	}*/

	this.controller.saveFN(tempActionVerb, null, tempObj);
}

export function save (param) {
	let tempObj = JSON.parse(JSON.stringify(this.props.resource));
	this.controller.saveFN(param, null, tempObj);
}

export function saveFN (param, confirm, tempObj,dialerconfirm) {
	this.updateLoaderFlag(true);

	let { calldetails } = this.props.app;
	let calldetailObj = {};
	if(Object.keys(calldetails).length > 0) {
		Object.keys(calldetails).some((prop) => {
			if(calldetails[prop].isactive) {
				calldetailObj = calldetails[prop];
				return true;
			}
		});
	}

	if(this.props.app.feature.enableDialerIntegration && Object.keys(calldetailObj).length > 0 && !this.props.resource.callid && !dialerconfirm){
		return this.props.openModal(modalService['confirmMethod']({
			header : 'Alert',
			body : 'Would you like to link the activity with this call?',
			btnArray : ['Ok', 'Cancel']
		}, (param1)=> {
			if(param1) {
				this.props.updateFormState(this.props.form,{callid : calldetailObj.callId});
				setTimeout(()=>{this.controller.save(param, confirm, tempObj,true);},0);
			} else
				this.controller.save(param,null,true);
		}));
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : tempObj,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/salesactivities'
	}).then((response)=> {
		if(this.props.isModal || response.data.message != 'success') {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true, tempObj);
			}));
		}

		if (response.data.message == 'success') {
			if (this.props.isModal) {
				if(param == 'Delete') {
					this.props.closeModal();
					this.props.callback(response.data.main);
					return;
				}

				if(!this.state.createParam) {
					delete response.data.main.internalaction;
					this.props.initialize(response.data.main);
				}

				if(response.data.main.status == 'Completed' && ['Sales Followup', 'Payment Followup'].includes(response.data.main.activitytypeid_category)) {
					this.openModal({
						render: (closeModal) => {
							return <NextActivityConfirmModal closeModal={closeModal} callback={(val) => {
								if(val) {
									this.props.closeModal();
									let { plannedstarttime, plannedendtime, ...otherResourceObj } = response.data.main;
									otherResourceObj.tempactivitycategory = response.data.main.activitytypeid_category;

									otherResourceObj.hidehistorypartnersection = this.props.location.params ? this.props.location.params.hidehistorypartnersection : false;

									this.props.createOrEdit('/createActivity', null, otherResourceObj, (responseobj) => {
										this.props.callback(responseobj);
									});
								} else {
									if(this.state.createParam) {
										this.props.closeModal();
									}
									this.props.callback(response.data.main);
								}
							}}/>
						}, className: {
							content: 'react-modal-custom-class-30', overlay: 'react-modal-overlay-custom-class'
						}, confirmModal: true
					});
				} else {
					if(this.state.createParam) {
						this.props.closeModal();
					}
					this.props.callback(response.data.main);
				}
			} else {
				if(this.state.createParam)
					this.props.history.replace(`/details/activities/${response.data.main.id}`);
				else {
					if (param == 'Delete') {
						if(this.props.isModal)
							this.props.closeModal();
						else
							this.props.history.goBack();
					} else {
						delete response.data.main.internalaction;
						this.props.initialize(response.data.main);
						if(response.data.main.status == 'Completed' && ['Sales Followup', 'Payment Followup'].includes(response.data.main.activitytypeid_category)) {
							this.openModal({render: (closeModal) => {
									return <NextActivityConfirmModal closeModal={closeModal} callback={(val) => {
										if(val) {
											let { plannedstarttime, plannedendtime, ...otherResourceObj } = this.props.resource;
											otherResourceObj.tempactivitycategory = this.props.resource.activitytypeid_category;

											this.props.history.replace({pathname: '/createActivity', params: {...otherResourceObj}});
										}
									}}/>
								}, className: {
									content: 'react-modal-custom-class-30', overlay: 'react-modal-overlay-custom-class'
								}, confirmModal: true
							});
						}
					}
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	if(this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}