import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, modalService, pageValidation, checkAccountBalance } from '../utils/services';
import { currencyFilter } from '../utils/filter';
import { AccountBalance } from '../components/utilcomponents';
import ExpenseRequestTypeModal from '../components/details/expenserequesttypemodal';
import ExpenseRequestConfirmExpenseModal from '../components/details/expenserequestconfirmexpensemodal';
import ExpenseRequestProjectEstimationPickingDetailsModal from '../components/details/expenserequestprojectestimationpickingdetailsmodal';
import ExpenseRequestPurchaseInvoiceModal from '../components/details/expenserequestpurchaseinvoicedetailsmodal';
import ExpenseRequestSettlementPickingDetailsModal from '../components/details/expenserequestsettlementpickingdetailsmodal'; 
import CostCenterPickingdetailsModal from '../components/details/costcenterpickingdetailsmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		expensedate: new Date(),
		expenserequestitems: [],
		expenserequestsettlementitems: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;
		if(params.param == 'Service Calls') {
			utils.assign(tempObj, params, [{'servicecallid' : 'id'}, 'companyid', 'customerid', 'currencyid','defaultcostcenter', {'employeeid_displayname' : 'engineerid_displayname'}, {'emppayableaccountid' : 'engineerid_emppayableaccountid'}]);
			this.customFieldsOperation('servicecalls', tempObj, params, 'expenserequests');
		}

		if(params.param == 'Projects') {
			utils.assign(tempObj, params, [{'projectid' : 'id'}, 'companyid', 'customerid', 'currencyid', 'defaultcostcenter', {'projectid_estimationavailable' : 'estimationavailable'}]);
			this.customFieldsOperation('projects', tempObj, params, 'expenserequests');
		}
	}

	this.props.initialize(tempObj);
	this.controller.showTypeModal();
	this.updateLoaderFlag(false);
}

export function showTypeModal() {
	this.updateLoaderFlag(true);
	this.openModal({render: (closeModal) => {
		return <ExpenseRequestTypeModal closeModal={closeModal} callback={(val) => {
			if(val) {
				this.props.updateFormState(this.props.form, {
					type: val
				});

				setTimeout(() => {
					if(!this.props.resource.employeeid)
						this.controller.getEmployeeDetails();
					else {
						this.controller.getEmployeeAdvance();
						//this.controller.getadvanceitems();
					}
				}, 0);
			} else
				this.props.history.goBack();
			this.updateLoaderFlag(false);
		}}/>
	}, className: {
		content: 'react-modal-custom-class-30', overlay: 'react-modal-overlay-custom-class'
	},
	confirmModal: true
	});
}

export function getEmployeeDetails() {
	axios.get(`/api/employees?&field=id,displayname,emppayableaccountid&filtercondition=employees.userid=${this.props.app.user.id}`).then((response) => {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					employeeid: response.data.main[0].id,
					employeeid_displayname: response.data.main[0].displayname,
					emppayableaccountid: response.data.main[0].emppayableaccountid
				});
				setTimeout(() => {
					//this.controller.getadvanceitems();
					this.controller.getEmployeeAdvance();
				}, 0);
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getEmployeeAdvance() {
	this.updateLoaderFlag(true);
	let currentemployeeadvance = 0;
	axios.get(`/api/query/expenserequestuserdetailsquery?employeeid=${this.props.resource.employeeid}`).then((response) => {
		if (response.data.message == 'success') {
			currentemployeeadvance = response.data.main[0].amount;
			this.setState({ currentemployeeadvance });
		} else {
			let apiResponse = commonMethods.apiResult(res);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function renderEmployeeAdvance() {
	return (
		<div>
			{this.state.currentemployeeadvance > 0 ? <div><span className="fa fa-info-circle" style={{marginRight: '5px'}}></span>{`${this.props.resource.employeeid_displayname} has an advance amount of ${currencyFilter(this.state.currentemployeeadvance, this.props.resource.currencyid, this.props.app)}`}</div> : null }
			{this.state.currentemployeeadvance == 0 ? <div><span className="fa fa-info-circle" style={{marginRight: '5px'}}></span>{`${this.props.resource.employeeid_displayname} has no advances`}</div> : null }
		</div>
	);
}

export function getItemById () {
	axios.get(`/api/expenserequests/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			this.controller.getEmployeeAdvance();
			this.controller.checkPurchaseinvoiceBtnfn();
		} else {
			var apiResponse = commonMethods.apiResult(response);
			this.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject) => {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});
			//this.controller.getadvanceitems();
		}, (reason)=> {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		//this.controller.getadvanceitems();
	}
}

export function getadvanceitems() {
	if(this.props.resource.type == 'Advance')
		return;

	if (this.props.resource.employeeid && this.props.resource.currencyid) {
		this.props.array.removeAll('expenserequestsettlementitems');

		axios.get(`/api/query/getemployeeoutstandingsquery?type=Receipt&companyid=${this.props.resource.companyid}&currencyid=${this.props.resource.currencyid}&employeeid=${this.props.resource.employeeid}`).then((response) => {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.main.length; i++) {
					response.data.main[i].credit = response.data.main[i].balance;
					if(response.data.main[i].xexpenserequestid)
						response.data.main[i].xexpenserequestid_outstandingamount = response.data.main[i].balance;
					if(response.data.main[i].xpayrollid)
						response.data.main[i].xpayrollid_outstandingamount = response.data.main[i].balance;
				}

				this.props.updateFormState(this.props.form, {
					expenserequestsettlementitems: response.data.main
				});
				this.controller.computeFinalRate();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}
}

export function callBackAccount (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.accountid_name`] = valueobj.name;
	this.props.updateFormState(this.props.form, tempObj);
}

export function callBackServiceCall (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		servicecallid_servicecallno: valueobj.servicecallno,
		defaultcostcenter: valueobj.defaultcostcenter,
		customerid: valueobj.customerid,
		customerid_name: valueobj.customerid_name
	});
}

export function callBackProject(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		projectid_name: valueobj.projectname,
		defaultcostcenter: valueobj.defaultcostcenter,
		customerid: valueobj.customerid,
		customerid_name: valueobj.customerid_name,
		projectid_estimationavailable: valueobj.estimationavailable
	});
}

export function callBackSalesOrder (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		orderid_orderno: valueobj.orderno,
		defaultcostcenter: valueobj.defaultcostcenter,
		customerid: valueobj.customerid,
		customerid_name: valueobj.customerid_name
	});
}

export function callBackEmployee(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		employeeid: valueobj.id,
		employeeid_displayname: valueobj.displayname,
		emppayableaccountid: valueobj.emppayableaccountid
	});
	this.props.array.removeAll('expenserequestsettlementitems');
	this.controller.getEmployeeAdvance();
	//this.controller.getadvanceitems();
}

export function callBackCategory(id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.expensecategoryid_name`] = valueobj.name;
	tempObj[`${itemstr}.accountid`] = valueobj.accountid;
	tempObj[`${itemstr}.accountid_name`] = valueobj.accountid_name;
	this.props.updateFormState(this.props.form, tempObj);
}

export function withinvoiceOnChange(value, item, itemstr) {
	if(!value) {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.xpurchaseinvoiceid`] : null,
			[`${itemstr}.xpurchaseinvoiceid_partnerid`] : null,
			[`${itemstr}.xpurchaseinvoiceid_invoicedate`] : null,
			[`${itemstr}.xpurchaseinvoiceid_partnerreference`] : null
		});
	}
	this.controller.checkPurchaseinvoiceBtnfn();
}

export function computeFinalRate() {
	setTimeout(() => {
		let tempObj = {};
		tempObj.amountrequested = 0;
		tempObj.amountapproved = 0;
		tempObj.settledamount = 0;
		for(var i = 0; i < this.props.resource.expenserequestitems.length; i++) {
			if(this.props.resource.expenserequestitems[i].amountrequested)
				tempObj.amountrequested += this.props.resource.expenserequestitems[i].amountrequested;
			if(this.props.resource.expenserequestitems[i].amountapproved)
				tempObj.amountapproved += this.props.resource.expenserequestitems[i].amountapproved;
		}
		for(var i = 0; i < this.props.resource.expenserequestsettlementitems.length; i++) {
			tempObj.settledamount += this.props.resource.expenserequestsettlementitems[i].credit;
		}
		tempObj.amount = (tempObj.advanceamount || 0) + (tempObj.amountapproved) - tempObj.settledamount;
		this.props.updateFormState(this.props.form, tempObj);
	}, 0);
}

export function getAccountBalance(item, itemstr) {
	/*checkAccountBalance({
		companyid: this.props.resource.companyid,
		accountid: item.accountid,
		show: 'credit',
		againstid: item.xexpenserequestid > 0 ? item.xexpenserequestid : item.xjournalvoucherid,
		type: item.xexpenserequestid > 0 ? 'expenserequests' : 'journalvouchers',
		employeeid: this.props.resource.employeeid,
	}, item, itemstr, this.selector, this.props);
	return item.accountbalance;*/
	return (
		<AccountBalance app={this.props.app} updateFn={(value) => this.updateLocalCompState(`${itemstr}.accountbalance`, value)} currencyid={this.props.resource.currencyid} showAs="text" balance={item.xexpenserequestid ? item.xexpenserequestid_outstandingamount : null} item={{
			companyid: this.props.resource.companyid,
			accountid: item.accountid,
			show: 'credit',
			againstid: item.xexpenserequestid > 0 ? item.xexpenserequestid : item.xjournalvoucherid,
			type: item.xexpenserequestid > 0 ? 'expenserequests' : 'journalvouchers',
			employeeid: this.props.resource.employeeid,
		}} />
	);
}

export function openCostCenterPickDetails (item, itemstr) {
	this.openModal({render: (closeModal) => {
		return <CostCenterPickingdetailsModal item={item} resource={this.props.resource} itemstr={itemstr} amountstr={'amountapproved'} app={this.props.app} form={this.props.form} updateFormState={this.props.updateFormState} openModal={this.openModal} closeModal={closeModal} />
	}, className: {
		content: 'react-modal-custom-class-60',
		overlay: 'react-modal-overlay-custom-class'
	}, confirmModal: true });
}

export function save (param, confirm,confirm2) {
	this.updateLoaderFlag(true);

	if(param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && !confirm2 && this.props.app.feature.enableCostCenter && (!this.props.resource.defaultcostcenter || this.props.resource.defaultcostcenter.length == 0)){
		let message = {
			header : 'Cost Center Alert',
			body : `Cost Center not selected. Do you want to ${param} ?`,
			btnArray : ['Yes','No']
		};

		return this.props.openModal(modalService['confirmMethod'](message, (resparam) => {
			if(resparam)
				this.controller.save(param, confirm,true);
			else
				this.updateLoaderFlag(false);
		}));	
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/expenserequests'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true,confirm2);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/expenserequests/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/expenserequests");
				else {
					this.props.initialize(response.data.main);
					this.controller.checkPurchaseinvoiceBtnfn();
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function checkPurchaseinvoiceBtnfn() {
	let showPurchaseinvoicebtn = false;
	this.props.resource.expenserequestitems.forEach((item) => {
		if(item.haspurchaseinvoice && !item.xpurchaseinvoiceid) {
			showPurchaseinvoicebtn = true;
		}
	});

	this.setState({showPurchaseinvoicebtn});
}

export function createPayment () {
	this.props.history.push({pathname: '/createPaymentVoucher', params: {...this.props.resource, param: 'Expense Requests'}});
}

export function getExpenseAdvancePickModal() {
	this.openModal({render: (closeModal) => {
		return <ExpenseRequestSettlementPickingDetailsModal
			resource={this.props.resource}
			updateFormState = {this.props.updateFormState}
			form = {this.props.form}
			app={this.props.app}
			openModal={this.props.openModal}
			closeModal={closeModal}
			callback = {(settlementarr)=> {
				this.controller.callbackSettlementPick(settlementarr);
			}}/>
		},
		className: {
			content: 'react-modal-custom-class-60',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function callbackSettlementPick(settlementarr) {
	this.updateLoaderFlag(true);
	let expenserequestsettlementitems = [...this.props.resource.expenserequestsettlementitems];
	let projectidArr = [];

	for(var i = 0; i < settlementarr.length; i++) {
		let itemFound=false;
		for(var j = 0; j < expenserequestsettlementitems.length; j++) {
			if((settlementarr[i].xexpenserequestid > 0 && (settlementarr[i].xexpenserequestid == expenserequestsettlementitems[j].xexpenserequestid)) || (settlementarr[i].xjournalvoucherid > 0 && (settlementarr[i].xjournalvoucherid == expenserequestsettlementitems[j].xjournalvoucherid))) {

				expenserequestsettlementitems[j].balance = settlementarr[i].balance;
				expenserequestsettlementitems[j].credit = settlementarr[i].credit;
				itemFound=true;
				if (!settlementarr[i].checked)
					expenserequestsettlementitems.splice(j, 1);

				break;
			}
		}
		if(!itemFound && settlementarr[i].checked) {
			expenserequestsettlementitems.push(settlementarr[i]);
		}
	}

	this.props.updateFormState(this.props.form, {
		expenserequestsettlementitems
	});
	setTimeout(() => {
		this.controller.computeFinalRate();
		this.updateLoaderFlag(false);
	}, 0);
}

export function projectestimationPickDetails(item, itemstr) {
	this.openModal({render: (closeModal) => {
		return <ExpenseRequestProjectEstimationPickingDetailsModal
			resource={this.props.resource}
			item={item}
			itemstr={itemstr}
			updateFormState = {this.props.updateFormState}
			form = {this.props.form}
			app={this.props.app}
			openModal={this.props.openModal}
			closeModal={closeModal}/>
		},
		className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function confirmExpense() {
	this.openModal({
		render: (closeModal) => {
			return <ExpenseRequestConfirmExpenseModal resource = {this.props.resource} openModal={this.openModal} closeModal={closeModal} callback={() => {
				this.controller.save('Confirm Expense',null,true);
			}} />
		}, className: {
			content: 'react-modal-custom-class-60', overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function createPurchaseInvoice() {
	let expenserequestitemArr = [];
	this.props.resource.expenserequestitems.forEach((item) => {
		if(item.haspurchaseinvoice && !item.xpurchaseinvoiceid)
			expenserequestitemArr.push({
				expenserequestid: this.props.resource.id,
				expenserequestitemsid: item.id,
				expensecategoryid_name: item.expensecategoryid_name,
				partnerid: item.partnerid,
				partnerid_name: item.partnerid_name,
				invoicedate: item.invoicedate,
				partnerreference: item.partnerreference
			});
	});

	if(expenserequestitemArr.length == 0)
		return this.props.openModal(modalService['infoMethod']({
			header : 'Warning',
			bodyArray : ["There is no pending item to purchase invoice"],
			btnArray : ['Ok']
		}));

	if(expenserequestitemArr.length == 1)
		this.props.history.push({pathname: '/createPurchaseInvoice', params: {...expenserequestitemArr[0], param: 'Expense Requests'}});

	if(expenserequestitemArr.length > 1) {
		this.openModal({
			render: (closeModal) => {
				return <ExpenseRequestPurchaseInvoiceModal expenserequestitemArr = {expenserequestitemArr} openModal={this.openModal} closeModal={closeModal} callback={(purchaseinvoicearr) => {
						this.props.history.push({pathname: '/createPurchaseInvoice', params: {...purchaseinvoicearr[0], param: 'Expense Requests'}});
					}} />
			}, className: {
				content: 'react-modal-custom-class-40', overlay: 'react-modal-overlay-custom-class'
			}
		});
	}
}

export function cancel () {
	this.props.history.goBack();
}
