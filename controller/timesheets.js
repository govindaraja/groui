import React, { Component } from 'react';
import axios from 'axios';
import { RenderReferenceButton } from '../utils/customfieldtypes';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		timesheetreferences : []
	};

	axios.get(`/api/employees?&field=id,displayname&filtercondition=employees.userid=${this.props.app.user.id}`).then((response) => {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0)
				tempObj['employeeid'] = response.data.main[0].id;

			this.props.initialize(tempObj);

			setTimeout(this.controller.getNextTimesheet, 0);

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getNextTimesheet () {
	this.updateLoaderFlag(true);

	let startdatetime = new Date();

	let filterString = `employeeid=${this.props.resource.employeeid} AND startdatetime::DATE='${new Date().toDateString()}'::DATE`;

	let queryString = `/api/timesheets?&field=id,employeeid,startdatetime,enddatetime,remarks&sortstring=timesheets.enddatetime&filtercondition=${filterString}`;

	axios.get(`${queryString}`).then((response) => {
		if(response.data.message == 'success') {
			if(response.data.main.length > 0) {
				let templ = response.data.main.length-1;

				startdatetime = new Date(new Date(response.data.main[templ].enddatetime).setMinutes(new Date(response.data.main[templ].enddatetime).getMinutes()));

			}

			this.props.updateFormState(this.props.form, { startdatetime });
		}

		this.updateLoaderFlag(false);
	});
}

export function callbackEmployee(id, valueobj) {
	this.controller.getNextTimesheet();
}

export function calculateTimespent () {
	if (this.props.resource.startdatetime != null && this.props.resource.enddatetime != null) {
		if(new Date(this.props.resource.startdatetime).getTime() <= new Date(this.props.resource.enddatetime).getTime()) {
			let timeStart = new Date(this.props.resource.startdatetime).getTime();
			let timeEnd = new Date(this.props.resource.enddatetime).getTime();
			let timeDiff = Math.abs(timeEnd - timeStart);

			let hours = Math.floor(timeDiff / 1000 / 60 / 60);
			if (hours < 10) {
				hours = '0' + hours;
			}
			timeDiff -= hours * 1000 * 60 * 60;


			let minutes = Math.floor(timeDiff / 1000 / 60);
			if (minutes < 10) {
				minutes = '0' + minutes;
			}
			timeDiff -= minutes * 1000 * 60;

			let duration = hours + ":" + minutes;

			this.props.updateFormState(this.props.form, { duration });
		}
	}
}

export function getItemById () {
	axios.get(`/api/timesheets/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		this.updateLoaderFlag(false);
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/timesheets'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				//this.props.history.replace(`/details/timesheets/${response.data.main.id}`);
				this.props.initialize({
					employeeid : response.data.main.employeeid,
					startdatetime : new Date(new Date(response.data.main.enddatetime).setMinutes(new Date(response.data.main.enddatetime).getMinutes())),
					timesheetreferences : []
				});
			} else if (param == 'Delete')
				this.props.history.replace("/list/timesheets");
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function renderReferenceButton(item) {console.log(item)
	return <RenderReferenceButton item = {item} callback = {(val) => this.controller.openActivity(val)} />
}

export function openActivity (item) {
	if(item.type == 'Service Reports' && item.servicereportid)
		this.props.history.push({pathname: `/details/servicereports/${item.servicereportid}`, params: item});

	if(item.type == 'Sales Activity' && item.salesactivityid)
		this.props.history.push({pathname: `/details/salesactivities/${item.salesactivityid}`, params: {...item, showrelatedtosection
			: true}});

	if(item.type == 'Internal Labour' && item.workprogressid)
		this.props.history.push(`/details/workprogress/${item.workprogressid}`);
}

export function cancel () {
	this.props.history.goBack();
}
