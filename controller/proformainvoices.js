import React, { Component } from 'react';
import axios from 'axios';
import * as ExcelJS from 'exceljs';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation, checkStatus } from '../utils/services';
import { dateFilter,itemmasterDisplaynamefilter } from '../utils/filter';

import { ItemRateField } from '../components/utilcomponents';
import OtherservicecalladdModal from '../components/details/otherservicecalladdmodal';
import OthercontractaddModal from '../components/details/othercontractaddmodal';
import EmailModal from '../components/details/emailmodal';
import ProformaTypeModal from '../components/details/proformatypemodal';
import InvoiceProjectItemAddModal from '../components/details/invoiceprojectitemaddmodal';
import InvoiceMilestoneProjectItemAddModal from '../components/details/invoicemilestoneprojectitemaddmodal';

let stockRoundOffPre = 5;

export function onLoad () {
	for(var i = 0; i < this.props.app.appSettings.length; i++) {
		if(this.props.app.appSettings[i].module == 'Stock' && this.props.app.appSettings[i].name == 'Rounding Off Precision') {
			stockRoundOffPre = this.props.app.appSettings[i].value.value;
			break;
		}
	}
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		proformatype: 'Advance',
		companyid: this.props.app.selectedcompanyid,
		roundoffmethod: this.props.app.defaultRoundOffMethod,
		invoicedate: new Date(new Date().setHours(0, 0, 0, 0)),
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		proformainvoiceitems: [],
		equipmentitems : [],
		additionalcharges: []
	};

	let orderArray = [];
	if(this.props.location.params) {
		const params = this.props.location.params;

		if (params.param == 'Sales Orders') {
			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', 'contactperson', 'phone', 'mobile', 'email', 'deliveryaddress', 'billingaddress', 'deliveryaddressid', 'billingaddressid', {'customerreference' : 'orderid_ponumber'},{'orderid' : 'id'}, 'currencyid', 'currencyexchangerate', {'dispatchthrough' : 'modeofdespatch'}, 'paymentterms', 'salesperson', 'territoryid', 'taxid', 'roundoffmethod', 'creditperiod','tandc']);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.invorderno = params.ponumber || params.orderid_ponumber;
			tempObj.invorderdate = params.orderdate;
			tempObj.invpodate = params.podate || params.orderid_podate;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.proformafor = 'Sales Order';
			
			this.customFieldsOperation('orders', tempObj, params, 'proformainvoices');

			params.orderitems.forEach((item, index) => {
				item.invoicedqty = item.invoicedqty ? item.invoicedqty : 0;
				if(item.quantity > item.invoicedqty) {
					if(orderArray.indexOf(item.parentid) == -1)
						orderArray.push(item.parentid);

					let tempChildObj = {
						index: index+1,
						quantity : Number((item.quantity - item.invoicedqty).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false,
						sourceresource: 'orderitems'
					};
					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'description', 'rate', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'itemid_issaleskit', 'discountmode', 'discountquantity', 'itemid_keepstock', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'poitemnumber', 'addonrate', 'baseitemrate', {'sourceid' : 'id'},  {'orderitemsid' : 'id'},'itemid_hasserial', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('orderitems', tempChildObj, item, 'proformainvoiceitems');
					tempObj.proformainvoiceitems.push(tempChildObj);
				}
			});
			if(orderArray.length == 1) {
				tempObj.orderid = orderArray[0];
				tempObj.orderid_creditperiod =  params.creditperiod;
			}
		}

		if (params.param == 'Quotes') {

			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', 'contactperson', 'phone', 'mobile', 'email', 'deliveryaddress', 'billingaddress', 'deliveryaddressid', 'billingaddressid', 'customerreference', 'currencyid', 'currencyexchangerate', {'dispatchthrough' : 'modeofdespatch'}, 'paymentterms', 'salesperson', 'territoryid', 'taxid', 'roundoffmethod', 'creditperiod',{'quoteid' : 'id'},{'quoteid_quoteno' : 'quoteno'},'tandc']);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.proformafor = 'Quotation';
			
			this.customFieldsOperation('quotes', tempObj, params, 'proformainvoices');

			params.quoteitems.forEach((item, index) => {
				let tempChildObj = {
					index: index+1,
					quantity : Number((item.quantity).toFixed(stockRoundOffPre)),
					alternateuom: item.uomconversiontype ? true : false
				};
				utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'description', 'rate', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'itemid_issaleskit', 'discountmode', 'discountquantity', 'itemid_keepstock', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'addonrate', 'baseitemrate','itemid_hasserial', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

				tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

				this.customFieldsOperation('quoteitems', tempChildObj, item, 'proformainvoiceitems');
				tempObj.proformainvoiceitems.push(tempChildObj);
			});
		}

		if (params.param == 'Estimation') {
			utils.assign(tempObj, params, ['companyid', 'customerid', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', 'contactperson', 'phone', 'mobile', 'email', 'currencyid', 'currencyexchangerate', 'paymentterms', 'salesperson', 'territoryid', 'taxid', 'roundoffmethod', 'creditperiod',{'estimationid' : 'id'},'tandc']);

			tempObj.proformafor = 'Estimation';

			this.customFieldsOperation('estimations', tempObj, params, 'proformainvoices');

			params.estimationitems.forEach((item, index) => {
				let tempChildObj = {
					index: index+1,
					quantity : Number((item.quantity).toFixed(stockRoundOffPre)),
					alternateuom: item.uomconversiontype ? true : false
				};
				utils.assign(tempChildObj, item, ['itemid', 'itemid_name', 'itemid_itemgroupid', 'itemid_hasaddons', 'description', 'rate', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'itemid_issaleskit', 'discountmode', 'discountquantity', 'itemid_keepstock', 'taxid', 'uomconversionfactor', 'uomconversiontype','itemid_hasserial', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

				tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

				this.customFieldsOperation('estimationitems', tempChildObj, item, 'proformainvoiceitems');
				tempObj.proformainvoiceitems.push(tempChildObj);
			});
		}

		if (params.param == 'Contracts') {
			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', 'contactperson', 'mobile', 'phone', 'email',  'billingaddress', 'billingaddressid', 'currencyid', 'currencyexchangerate', 'salesperson', 'taxid', 'roundoffmethod', {'deliveryaddress' : 'installationaddress'}, {'deliveryaddressid' : 'installationaddressid'}, 'territoryid', {'customerreference' : 'ponumber'},'contractfor','tandc']);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.proformafor = 'Contract';

			this.customFieldsOperation('contracts', tempObj, params, 'proformainvoices');

			if(params.billingschedule != 'Not Applicable') {
				tempObj.contractid = params.id;
				tempObj.contractid_billingschedule = params.billingschedule;
			}

			if(['Facility', 'Equipment'].includes(params.contractfor)) {
				let times = 0;
				params.contractbillingschedules.forEach((schedule)=>{
					if(!schedule.invoiceid)
						times +=1;
				});

				params.contractitems.forEach((item, index) => {
					if(item.contractstatus != 'Expired' ||  (item.contractstatus == 'Expired' && !item.manualexpiredate)) {

						let tempChildObj = {
							index: index+1,
							contractid : params.id,
							contractid_contractno : params.contractno,
							contractid_startdate : params.startdate,
							contractid_expiredate : params.expiredate,
							sourceresource: 'contractitems'
						};

						if(params.billingschedule != 'Not Applicable') {
							let temprate = times > 0 ? Number(((item.invoiceamount > 0 ? (item.amount-item.invoiceamount) : item.amount) / (times * item.quantity)).toFixed(2)) : 0;
							let discountqty = item.discountmode == 'Rupees' ? null : item.discountquantity;
							temprate = Number((temprate / (1 - (0.01 * discountqty))).toFixed(2));
							tempChildObj.rate = temprate;
							tempChildObj.discountquantity = discountqty;
						} else {
							tempChildObj.rate = item.rate;
							tempChildObj.discountquantity = item.discountquantity;
						}

						utils.assign(tempChildObj, item, ['equipmentid', 'equipmentid_displayname', {'sourceid' : 'id'}, {'contractitemsid' : 'id'}, 'quantity', 'contracttypeid', 'contracttypeid_name', 'description', 'discountmode', 'taxid', 'taxdetails', 'amount', 'finalratelc', 'ratelc', 'amountwithtax', 'remarks', {'accountid' : 'contracttypeid_incomeaccountid'}, 'capacity', 'capacityfield']);

						this.customFieldsOperation('contractitems', tempChildObj, item, 'salesinvoiceitems');

						tempObj.equipmentitems.push(tempChildObj);
					}
				});
			} else {
				params.contractcovereditems.forEach((item, index) => {
					let tempChildObj = {
						index: index+1,
						contractid : params.id,
						contractid_contractno : params.contractno,
						contractid_startdate : params.startdate,
						contractid_expiredate : params.expiredate,
						sourceresource: 'contractcovereditems'
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', {'sourceid' : 'id'}, 'quantity', 'uomid', 'uomid_name', 'rate', 'contracttypeid', 'contracttypeid_name', 'description', 'discountmode', 'discountquantity', 'remarks', 'capacity', 'capacityfield']);

					this.customFieldsOperation('contractcovereditems', tempChildObj, item, 'salesinvoiceitems');

					tempObj.equipmentitems.push(tempChildObj);
				});
			}
		}

		if (params.param == 'Contract Enquiries') {
			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', 'contactperson', 'mobile', 'phone', 'email',  'billingaddress', 'billingaddressid', 'currencyid', 'currencyexchangerate', 'salesperson', 'taxid', 'roundoffmethod', {'deliveryaddress' : 'installationaddress'}, {'deliveryaddressid' : 'installationaddressid'}, 'territoryid', {'customerreference' : 'ponumber'},{'contractenquiryid' : 'id'},{'contractenquiryid_contractenquiryno' : 'contractenquiryno'},'paymentterms','contractfor','tandc']);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.proformafor = 'Contract Enquiry';

			this.customFieldsOperation('contractenquiries', tempObj, params, 'proformainvoices');

			if(['Facility', 'Equipment'].includes(params.contractfor)) {
				params.contractenquiryitems.forEach((item, index) => {
					if(!item.islost) {
						let tempChildObj = {
							index: index+1
						};
						utils.assign(tempChildObj, item, ['equipmentid', 'equipmentid_displayname', {'sourceid' : 'id'}, 'quantity','rate','discountquantity', 'contracttypeid', 'contracttypeid_name', 'description', 'discountmode', 'taxid', 'taxdetails', 'amount', 'finalratelc', 'ratelc', 'amountwithtax', 'remarks', {'accountid' : 'contracttypeid_incomeaccountid'}, 'capacity', 'capacityfield','itemid','itemid_name','uomid', 'uomid_name']);

						this.customFieldsOperation('contractenquiryitems', tempChildObj, item, 'proformainvoiceitems');

						tempObj.equipmentitems.push(tempChildObj);
					}
				});
			} else {
				params.contractenquirycovereditems.forEach((item, index) => {
					let tempChildObj = {
						index: index+1
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name', {'sourceid' : 'id'}, 'quantity', 'uomid', 'uomid_name','taxid','taxdetails', 'rate','amount', 'finalratelc', 'ratelc', 'amountwithtax', 'contracttypeid', 'contracttypeid_name', 'description', 'discountmode', 'discountquantity', 'remarks', 'capacity', 'capacityfield']);

					this.customFieldsOperation('contractenquirycovereditems', tempChildObj, item, 'proformainvoiceitems');

					tempObj.equipmentitems.push(tempChildObj);
				});
			}
		}

		if (params.param == 'Project Quotes') {

			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', 'contactperson', 'mobile', 'phone', 'email', 'billingaddress', 'deliveryaddress', 'deliveryaddressid', 'billingaddressid', 'currencyid', 'currencyexchangerate',{'dispatchthrough' : 'modeofdespatch'}, 'salesperson', 'territoryid', 'taxid', 'labourtaxid', 'materialtaxid', {'projectquoteid' : 'id'},{'projectquoteid_quoteno' : 'quoteno'}, 'paymentterms', 'roundoffmethod','creditperiod', 'customerreference','tandc']);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.proformafor = 'Project Quotation';
			
			this.customFieldsOperation('projectquotes', tempObj, params, 'proformainvoices');

			params.projectquoteitems.forEach((item, index) => {
				if(item.itemtype == 'Item'){
					let tempChildObj = {
						index: index+1,
						quantity : Number((item.quantity).toFixed(stockRoundOffPre)),
						alternateuom: item.uomconversiontype ? true : false
					};

					utils.assign(tempChildObj, item, ['itemid', 'itemid_name','itemid_itemtype', 'itemid_itemgroupid', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'description', 'rate','splitrate','labourrate','materialrate', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'itemid_issaleskit', 'discountmode', 'discountquantity', 'itemid_keepstock', 'taxid','labourtaxid','materialtaxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'addonrate', 'baseitemrate','itemid_hasserial', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingconversiontype', 'billingconversionfactor']);

					tempChildObj.billingquantity = item.usebillinguom ? (Number(((tempChildObj.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.customFieldsOperation('projectquoteitems', tempChildObj, item, 'proformainvoiceitems');
					tempObj.proformainvoiceitems.push(tempChildObj);
				}
			});
		}
		
		if (params.param == 'Projects') {

			utils.assign(tempObj, params, ['companyid', 'customerid', 'defaultcostcenter', {'suppliercode' : 'customerid_suppliercode'}, 'contactid', 'contactperson', 'mobile', 'phone', 'email', 'billingaddress', 'deliveryaddress', 'deliveryaddressid', 'billingaddressid', 'currencyid', 'currencyexchangerate', 'salesperson', 'territoryid', 'taxid', 'labourtaxid', 'materialtaxid', {'projectid' : 'id'}, 'paymentterms', 'roundoffmethod', {'invpodate' : 'podate'}, {'customerreference' : 'ponumber'}, {'invorderno' : 'ponumber'}, {'invorderdate' : 'podate'}, {'projectid_ismilestonerequired' : 'ismilestonerequired'},{'paymentpercent' : 'advancepercent'},'advancepercent',{'retentionpercent' : 'retentionpercent'}]);

			tempObj.customerstate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.customergstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.customergstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.consigneeid = params.consigneeid ? params.consigneeid : params.customerid;
			tempObj.consigneestate = params.billingaddressid_state ? params.billingaddressid_state : params.customerstate;
			tempObj.consigneegstin = params.billingaddressid_gstin ? params.billingaddressid_gstin : params.customerid_gstin;
			tempObj.consigneegstregtype = params.customerid_gstregtype ? params.customerid_gstregtype : params.customergstregtype;
			tempObj.proformafor = 'Project';
			tempObj.proformatype = params.projectid_ismilestonerequired ? 'Completion' : 'Advance';

			this.customFieldsOperation('projects', tempObj, params, 'proformainvoices');
		}

		if (params.param == 'Milestone Progress') {

			utils.assign(tempObj, params, ['companyid', 'customerid', {'defaultcostcenter' : 'projectid_defaultcostcenter'}, {'suppliercode' : 'customerid_suppliercode'}, 'projectid', {'contactid' : 'projectid_contactid'},{'contactperson' : 'projectid_contactperson'},{'mobile' : 'projectid_mobile'},{'phone' : 'projectid_phone'},{'email' : 'projectid_email'},{'billingaddress' : 'projectid_billingaddress'},{'deliveryaddress' : 'projectid_deliveryaddress'},{'billingaddressid' : 'projectid_billingaddressid'},{'deliveryaddressid' : 'projectid_deliveryaddressid'},{'currencyid' : 'projectid_currencyid'},{'currencyexchangerate' : 'projectid_currencyexchangerate'},{'territoryid' : 'projectid_territoryid'},{'taxid' : 'projectid_taxid'},{'labourtaxid' : 'projectid_labourtaxid'},{'materialtaxid' : 'projectid_materialtaxid'},{'paymentterms' : 'projectid_paymentterms'},{'roundoffmethod' : 'projectid_roundoffmethod'},{'invpodate' : 'projectid_podate'},{'customerreference' : 'projectid_ponumber'},{'invorderno' : 'projectid_ponumber'},{'invorderdate' : 'projectid_podate'},{'projectid_ismilestonerequired' : 'projectid_ismilestonerequired'},{'advancepercent' : 'projectid_advancepercent'},{'retentionpercent' : 'projectid_retentionpercent'}]);

			tempObj.proformatype = 'Completion';
			tempObj.proformafor = 'Project';

			this.customFieldsOperation('milestoneprogress', tempObj, params, 'proformainvoices');
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		this.controller.getTandC();

		if(this.props.resource.customerid)
			this.controller.getPartnerDetails();

		if(!this.props.resource.currencyexchangerate)
			this.controller.currencyOnChange();

		if(this.props.location.params) {

			if(this.props.location.params.param == 'Sales Orders')
				this.controller.getAdditionalCharges(this.props.location.params.id, 'orders');
			if(this.props.location.params.param == 'Quotes')
				this.controller.getAdditionalCharges(this.props.location.params.id, 'quotes');
			if(this.props.location.params.param == 'Estimation')
				this.controller.getAdditionalCharges(this.props.location.params.id, 'estimations');
			if(this.props.location.params.param == 'Contract Enquiries')
				this.controller.getAdditionalCharges(this.props.location.params.id, 'contractenquiries');
			if(this.props.location.params.param == 'Project Quotes')
				this.controller.getAdditionalCharges(this.props.location.params.id, 'projectquotes');

			if(this.props.location.params.param == 'Contracts' && this.props.location.params.billingschedule == 'Not Applicable')
				this.controller.checkforContracts(this.props.location.params.id);

			if(this.props.location.params.param == 'Projects')
				this.controller.showTypeModal();

			if(this.props.location.params.param == 'Milestone Progress')
				this.controller.addProjectItems();

			this.controller.computeFinalRate();
		}
	}, 0);
	this.updateLoaderFlag(false);
}

export function showTypeModal() {
	this.updateLoaderFlag(true);
	this.openModal({render: (closeModal) => {
		return <ProformaTypeModal closeModal={closeModal} callback={(val) => {
			if(val) {
				let tempObj = {
					proformatype : val
				};
				if(val == 'Advance'){
					tempObj.advancepercent = null;
					tempObj.advanceamount = null;
					tempObj.retentionpercent = null;
					tempObj.retentionamount = null;
				}
				if(val == 'Completion'){
					tempObj.paymentpercent = null;
				}
				this.props.updateFormState(this.props.form, tempObj);

				setTimeout(() => {
					this.controller.addProjectItems();
				}, 0);
			} else
				this.props.history.goBack();
			this.updateLoaderFlag(false);
		}}/>
	}, className: {
		content: 'react-modal-custom-class-30', overlay: 'react-modal-overlay-custom-class'
	},
	confirmModal: true
	});
}

export function renderTableItemfield(item) {
	return(
		<div>
			<span>{item.itemid_name}</span>
			{item.boqmilestonestatusid ? <br></br> : null}
			{item.boqmilestonestatusid ? <span className="text-muted">{`(${item.boqmilestonestatusid_name})`}</span> : null}
		</div>
	);
}

export function renderTableRatefield(item) {
	return <ItemRateField item={item} currencyid={this.props.resource.currencyid} app={this.props.app} />;
}

export function renderTableQtyfield(item) {
	if(item.capacityfield)
		return <span>{item.quantity} {`${itemmasterDisplaynamefilter(item.capacityfield, this.props.app)}`}</span>;
	else
		return <span>{item.quantity}</span>;
}

export function getItemById () {
	axios.get(`/api/proformainvoices/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			
			for (var i = 0; i < tempObj.proformainvoiceitems.length; i++) {
				if (tempObj.proformainvoiceitems[i].uomconversiontype)
					tempObj.proformainvoiceitems[i].alternateuom = true;
			}


			if(tempObj.proformafor == 'Contract' || tempObj.proformafor == 'Contract Enquiry'){
				tempObj.equipmentitems = [];
				tempObj.proformainvoiceitems.forEach((item) => {
					tempObj.equipmentitems.push(item);
				});
				tempObj.proformainvoiceitems = [];

			}

			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			this.controller.checkVisibilityForButton();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function checkVisibilityForButton () {
	setTimeout(() => {
		let showDeliveryBtn = false;

		for(var i=0;i<this.props.resource.proformainvoiceitems.length;i++) {
			if(this.props.resource.proformainvoiceitems[i].sourceresource == 'orderitems' && (this.props.resource.proformainvoiceitems[i].quantity > (this.props.resource.proformainvoiceitems[i].deliveredqty || 0))) {
				this.setState({
					showDeliveryBtn: true
				});
				break;
			}
		}
	}, 0);
}

export function getTandC() {
	axios.get(`/api/termsandconditions?field=id,terms&filtercondition=termsandconditions.resource='Sales Invoice' and termsandconditions.isdefault`).then((response)=> {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				this.props.updateFormState(this.props.form, {
					tandcid : response.data.main[0].id,
					tandc : response.data.main[0].terms
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function tandcOnchange(id, valueobj) {
	if(id) {
		this.props.updateFormState(this.props.form, {
			tandc: valueobj.terms
		});
	}
}
export function currencyOnChange () {
	if (this.props.resource.pricelistid) {
		if (this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.app.defaultCurrency && this.props.app.pricelistObj[this.props.resource.pricelistid].currencyid != this.props.resource.currencyid) {
			this.props.updateFormState(this.props.form, {
				pricelistid : null
			});
		}
	}
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject)=> {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

			this.controller.computeFinalRate();
		}, (reason)=> {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
		this.controller.computeFinalRate();
	}
}

export function callBackCustomerName (id, valueobj) {
	let tempObj = {
		consigneeid: valueobj.id,
		salesperson: this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
		receivableaccountid: (valueobj.receivableaccountid || valueobj.partnergroupid_receivableaccountid || this.props.app.defaultreceivableaccountid),
		suppliercode: valueobj.suppliercode,
		territoryid: valueobj.territory,
		customergstin: valueobj.gstin,
		customergstregtype: valueobj.gstregtype,
		consigneegstin: valueobj.gstin,
		consigneegstregtype: valueobj.gstregtype,
		contactid: valueobj.contactid,
		contactperson: valueobj.contactid_name,
		mobile: valueobj.contactid_mobile,
		phone: valueobj.contactid_phone,
		email: valueobj.contactid_email,
		billingaddress: valueobj.addressid_displayaddress,
		deliveryaddress: valueobj.addressid_displayaddress,
		billingaddressid: valueobj.addressid,
		deliveryaddressid: valueobj.addressid,
		customerstate : valueobj.addressid_state,
		consigneestate : valueobj.addressid_state
	};

	let creditPeriod = (valueobj.salescreditperiod || valueobj.partnergroupid_creditperiod);
	if(creditPeriod >= 0) {
	    tempObj.paymentduedate = new Date(new Date(this.props.resource.invoicedate).setDate(new Date(this.props.resource.invoicedate).getDate() + creditPeriod));
	}

	tempObj.paymentterms = this.props.resource.paymentterms ? this.props.resource.paymentterms : valueobj.paymentterms;
	tempObj.dispatchthrough = this.props.resource.dispatchthrough ? this.props.resource.dispatchthrough : valueobj.modeofdispatch;

	this.customFieldsOperation('partners', tempObj, valueobj, 'proformainvoices');
	this.props.updateFormState(this.props.form, tempObj);
}

export function callBackConsigneeName (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		consigneeid_name: valueobj.name,
		consigneegstin: valueobj.gstin,
		consigneegstregtype: valueobj.gstregtype,
		deliveryaddress: null,
		deliveryaddressid: null
	});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactperson: valueobj.name,
		phone: valueobj.phone,
		email: valueobj.email,
		mobile: valueobj.mobile,
		contactid_name: valueobj.name,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile,
	});
}

export function callbackEngineer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson: valueobj.userid
	});
}

export function projectCallback(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		[`projectid_ismilestonerequired`] : valueobj.ismilestonerequired
	});
	this.controller.addProjectItems();
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
		tempObj.consigneegstin = addressobj.gstin ? addressobj.gstin : this.props.resource.consigneegstin;

		if(this.props.app.feature.useMasterForAddresses)
			tempObj.consigneestate = addressobj.stateid_name ? addressobj.stateid_name : this.props.resource.consigneestate;
		else
			tempObj.consigneestate = addressobj.state ? addressobj.state : this.props.resource.consigneestate;
	}
	if(address == 'billingaddress') {
		tempObj.billingaddress = addressobj.displayaddress;
		tempObj.billingaddressid = addressobj.id;
		tempObj.customergstin = addressobj.gstin ? addressobj.gstin : this.props.resource.customergstin;

		if(this.props.app.feature.useMasterForAddresses)
			tempObj.customerstate = addressobj.stateid_name ? addressobj.stateid_name : this.props.resource.customerstate;
		else
			tempObj.customerstate = addressobj.state ? addressobj.state : this.props.resource.customerstate;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function getPartnerDetails () {
	let tempObj = {};
	return axios.get(`/api/partners?field=id,name,receivableaccountid,partnergroupid,salescreditperiod,customergroups/pricelistid/partnergroupid,customergroups/receivableaccountid/partnergroupid,territory,customergroups/creditperiod/partnergroupid&filtercondition=partners.id=${this.props.resource.customerid}`).then((response)=> {
		if (response.data.message == 'success') {
			if (response.data.main.length > 0) {
				tempObj.receivableaccountid = (response.data.main[0].receivableaccountid || response.data.main[0].partnergroupid_receivableaccountid || this.props.app.defaultreceivableaccountid);
				tempObj.creditperiod = (this.props.resource.orderid_creditperiod || response.data.main[0].salescreditperiod || response.data.main[0].partnergroupid_creditperiod);
				if(tempObj.creditperiod >= 0)
					tempObj.paymentduedate = new Date(new Date(this.props.resource.invoicedate).setDate(new Date(this.props.resource.invoicedate).getDate() + tempObj.creditperiod));
	
				this.props.updateFormState(this.props.form, tempObj);
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj.id, item, itemstr);
}

export function callBackVariation (id, valueobj, item, itemstr) {
	let tempObj = {};
	tempObj[`${itemstr}.itemid`] = valueobj.parentid;
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.getItemDetails(valueobj.parentid, item, itemstr);
}

export function getItemDetails(id, item, itemstr) {
	let promise = commonMethods.getItemDetails(id, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, null, 'sales');
	promise.then((returnObject)=> {
		let tempObj = {};
		tempObj[`${itemstr}.itemid`] = returnObject.itemid;
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.itemid_itemtype`] = returnObject.itemid_itemtype;
		tempObj[`${itemstr}.rate`] = 0;
		tempObj[`${itemstr}.labourrate`] = 0;
		tempObj[`${itemstr}.materialrate`] = 0;
		tempObj[`${itemstr}.splitrate`] = returnObject.splitrate;
		if (returnObject.itemid_itemtype != 'Project') {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
		} else {
			tempObj[`${itemstr}.labourtaxid`] = (this.props.resource.labourtaxid && this.props.resource.labourtaxid.length > 0) ? this.props.resource.labourtaxid : [];
			tempObj[`${itemstr}.materialtaxid`] = (this.props.resource.materialtaxid && this.props.resource.materialtaxid.length > 0) ? this.props.resource.materialtaxid : [];
		}
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = (this.props.resource.taxid && this.props.resource.taxid.length > 0) ? this.props.resource.taxid : returnObject.taxid;
		tempObj[`${itemstr}.accountid`] = returnObject.itemid_incomeaccountid;
		tempObj[`${itemstr}.itemnamevariationid`] = returnObject.itemnamevariationid;
		tempObj[`${itemstr}.itemnamevariationid_name`] = returnObject.itemnamevariationid_name;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasaddons`] = returnObject.hasaddons;
		tempObj[`${itemstr}.itemid_itemcategorymasterid`] = returnObject.itemcategorymasterid;
		tempObj[`${itemstr}.itemid_itemgroupid`] = returnObject.itemgroupid;
		tempObj[`${itemstr}.itemid_recommendedsellingprice`] = returnObject.itemid_recommendedsellingprice;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'purchaseinvoiceitems', itemstr);

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if(tempObj[`${itemstr}.usebillinguom`]) {
			tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (tempObj[`${itemstr}.uomconversionfactor`] ? tempObj[`${itemstr}.uomconversionfactor`] : 1)) * tempObj[`${itemstr}.billingconversionfactor`]).toFixed(this.props.app.roundOffPrecision));
		}

		this.props.updateFormState(this.props.form, tempObj);

		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
	}, (reason)=> {});
}

export function quantityOnChange(itemqty, item, itemstr) {
	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);

		this.props.updateFormState(this.props.form, {
			[`${itemstr}.billingquantity`]: Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))
		});
	}
	this.controller.computeFinalRate();
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let item = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, this.props.resource.pricelistid, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : valueobj.alternateuom, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

		if(item.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(item.itemid, {customerid: this.props.resource.customerid, partneraddressid: this.props.resource.billingaddressid}, null, 'sales', item.usebillinguom ? item.billinguomid : item.uomid, item.usebillinguom ? true : item.uomconversiontype ? true : false, item.usebillinguom ? item.billingconversionfactor : item.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);

			if(item.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(item.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function billingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function proformaTypeOnChange() {
	let tempObj = {
		proformainvoiceitems : [],
		paymentpercent : null,
		advancepercent : null,
		advanceamount : null,
		retentionpercent : null,
		retentionamount : null
	};
	this.props.updateFormState(this.props.form, tempObj);
}

export function computeFinalRate() {
	setTimeout(()=>{
		if(this.props.resource.proformafor != 'Contract' && this.props.resource.proformafor != 'Contract Enquiry') {
			let tempObj = {};
			for (var i = 0; i < this.props.resource.proformainvoiceitems.length; i++) {
				if (this.props.resource.proformainvoiceitems[i].itemid_itemtype == 'Project' && this.props.resource.proformainvoiceitems[i].splitrate) {
					tempObj[`proformainvoiceitems[${i}].rate`] = (this.props.resource.proformainvoiceitems[i].labourrate ? this.props.resource.proformainvoiceitems[i].labourrate : 0) + ((this.props.resource.proformainvoiceitems[i].materialrate ? this.props.resource.proformainvoiceitems[i].materialrate : 0));
				}
			}
			this.props.updateFormState(this.props.form, tempObj);
			taxEngine(this.props, 'resource', 'proformainvoiceitems', null, null, true);
		} else {
			taxEngine(this.props, 'resource', 'equipmentitems');
		}
		this.controller.proformaCalculation();
	}, 0);
}

export function proformaCalculation(){
	let tempObj = {
		amount : 0,
		advanceamount  : null,
		retentionamount : null
	};
	if(this.props.resource.finaltotal > 0){
		tempObj.amount = this.props.resource.finaltotal;
		if(this.props.resource.proformatype == 'Advance' && this.props.resource.paymentpercent > 0)
			tempObj.amount = Number(((this.props.resource.finaltotal*this.props.resource.paymentpercent)/100).toFixed(this.props.app.roundOffPrecision));

		if(this.props.resource.proformatype == 'Completion' && (this.props.resource.advancepercent > 0 || this.props.resource.retentionpercent > 0)){
			tempObj.advanceamount  = this.props.resource.advancepercent > 0 ? (this.props.resource.finaltotal*this.props.resource.advancepercent)/100 : 0;
			tempObj.retentionamount  = this.props.resource.retentionpercent > 0 ? (this.props.resource.finaltotal*this.props.resource.retentionpercent)/100 : 0;
			tempObj.amount = Number((this.props.resource.finaltotal-tempObj.advanceamount-tempObj.retentionamount).toFixed(this.props.app.roundOffPrecision));
		}
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function callBackContract (array) {
	if(array.length == 0)
		return null;

	this.updateLoaderFlag(true);

	let contractURL;

	if(this.props.resource.contractfor == 'O & M')
		contractURL = `/api/contractcovereditems?field=id,itemid,itemmaster/name/itemid,quantity,uomid,uom/name/uomid,rate,description,remarks,parentresource,parentid&filtercondition=contractcovereditems.parentid in (${array.join()})`
	else
		contractURL = `/api/contractitems?field=id,equipmentid,equipments/displayname/equipmentid,description,rate,discountmode,discountquantity,taxid,taxdetails,quantity,amount,finalratelc,ratelc,amountwithtax,remarks,parentid,contracts/contractno/parentid,contracts/startdate/parentid,contracts/expiredate/parentid,contracttypes/name/contracttypeid,contracttypes/incomeaccountid/contracttypeid,contracttypeid,contractstatus,manualexpiredate,capacity,capacityfield&filtercondition=contractitems.parentid in (${array.join()})`

	axios.get(contractURL).then((response) => {
		if(response.data.message == 'success') {
			let equipmentitems = [...this.props.resource.equipmentitems];
			response.data.main.forEach((item, index) => {
				if(item.contractstatus != 'Expired' ||  (item.contractstatus == 'Expired' && !item.manualexpiredate)) {
					let tempChildObj = {
						index: index+1,
						contractid : item.parentid,
						contractid_contractno : item.parentid_contractno,
						contractid_startdate : item.parentid_startdate,
						contractid_expiredate : item.parentid_expiredate,
						sourceresource: 'contractitems'
					};
					utils.assign(tempChildObj, item, ['equipmentid', 'equipmentid_displayname', {'sourceid' : 'id'}, {'contractitemsid' : 'id'}, 'quantity', 'contracttypeid', 'contracttypeid_name', 'description', 'rate', 'discountmode', 'discountquantity', 'taxid', 'taxdetails', 'amount', 'finalratelc', 'ratelc', 'amountwithtax', 'remarks']);

					this.customFieldsOperation('contractitems', tempChildObj, item, 'proformainvoiceitems');

					equipmentitems.push(tempChildObj);
				}
			});
			this.props.updateFormState(this.props.form, {
				equipmentitems
			});
			this.controller.computeFinalRate();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
};

export function checkforContracts (contractid) {
	let filterString = `contracts.customerid=${this.props.resource.customerid} and (not contracts.invoicenotapplicable is true) and (not contracts.hasinvoiced is true) and contracts.billingschedule='Not Applicable' and contracts.status in ('Approved','Sent To Customer') and contracts.id<>${contractid} and contracts.contractfor='${this.props.resource.contractfor}'`;
	axios.get(`/api/contracts?field=id,contractno,startdate,expiredate&filtercondition=${encodeURIComponent(filterString)}`).then((response) => {
		if(response.data.message == 'success') {
			if(response.data.main.length > 0) {
				this.openModal({
					render: (closeModal) => {
						return <OthercontractaddModal array={response.data.main} callback={(array)=>{
								this.controller.callBackContract(array);
							}
						} closeModal={closeModal} />
					}, className: {
						content: 'react-modal-custom-class',
						overlay: 'react-modal-overlay-custom-class'
					}
				});
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function showError () {
	let tempObj = {
		invoicedate: new Date(new Date().setHours(0, 0, 0, 0)),
		proformainvoiceitems: [],
		additionalcharges: [],
		currencyid: this.props.app.defaultCurrency
	};
	this.props.initialize(tempObj);
	let apiResponse = commonMethods.apiResult({
		data: {
			message: 'Items are Already Invoiced'
		}
	});
	this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
}

export function getAdditionalCharges (id, parentResource) {
	
	axios.get(`/api/additionalcharges?field=name,amount,applyforvaluation,parentid,parentresource,additionalchargesid,additionalchargesmaster/name/additionalchargesid,description,taxcodeid,defaultpercentage,displayorder&filtercondition=additionalcharges.parentid in (${id}) and additionalcharges.parentresource ='${parentResource}'`).then((response) => {
		if (response.data.message == 'success') {
			let additionalcharges = response.data.main.sort((a, b) => {
				return (a.displayorder) - (b.displayorder);
			});
			this.props.updateFormState(this.props.form, {
				additionalcharges
			});
			this.controller.computeFinalRate();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function addProjectItems() {
	if(this.props.resource.projectid_ismilestonerequired && this.props.resource.proformatype == 'Completion') {
		this.openModal({
			render: (closeModal) => {
				return <InvoiceMilestoneProjectItemAddModal salesinvoiceitems={this.props.resource.proformainvoiceitems} projectid={this.props.resource.projectid} invoicedate={this.props.resource.invoicedate} closeModal={closeModal} openModal={this.props.openModal} getCustomFields={this.getCustomFields} customFieldsOperation={this.customFieldsOperation} callback = {(itemarr)=> {
					this.controller.callbackInvoiceMilestoneProjectItem(itemarr);
				}} />
			}, className: {content: 'react-modal-custom-class-90', overlay: 'react-modal-overlay-custom-class'}
		});
	} else {
		this.openModal({
			render: (closeModal) => {
				return <InvoiceProjectItemAddModal salesinvoiceitems={this.props.resource.proformainvoiceitems} projectid={this.props.resource.projectid} closeModal={closeModal} openModal={this.props.openModal} getCustomFields={this.getCustomFields} customFieldsOperation={this.customFieldsOperation} callback = {(itemarr)=> {
					this.controller.callbackInvoiceProjectItem(itemarr);
				}} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}
}

export function  callbackInvoiceProjectItem(itemarr) {
	this.updateLoaderFlag(true);
	let proformainvoiceitems = [...this.props.resource.proformainvoiceitems];
	let itemidArray = [];

	for(var i = 0; i < itemarr.length; i++) {
		let itemFound=false;
		for(var j = 0; j < proformainvoiceitems.length; j++) {
			if(proformainvoiceitems[j].sourceid == itemarr[i].id) {
				if(proformainvoiceitems[j].quantity < itemarr[i].qty) {
					proformainvoiceitems[j].quantity = itemarr[i].qty;
				}
				itemFound=true;
				break;
			}
		}
		if(!itemFound) {
			itemidArray.push(itemarr[i].itemid);
			let tempSalesInvItemObj = {
				itemid: itemarr[i].itemid,
				itemid_name : itemarr[i].itemid_name,
				itemid_itemtype : itemarr[i].itemid_itemtype,
				uomid : itemarr[i].uomid,
				uomconversiontype : itemarr[i].uomconversiontype,
				uomconversionfactor : itemarr[i].uomconversionfactor,
				description : itemarr[i].description,
				quantity : itemarr[i].qty,
				splitrate : itemarr[i].splitrate,
				labourrate : itemarr[i].labourrate,
				materialrate : itemarr[i].materialrate,
				rate : itemarr[i].rate,
				sourceresource : 'projectitems',
				sourceid : itemarr[i].id,
				discountmode : itemarr[i].discountmode,
				discountquantity : itemarr[i].discountquantity,
				labourtaxid : itemarr[i].labourtaxid,
				materialtaxid : itemarr[i].materialtaxid,
				taxid : itemarr[i].taxid,
				alternateuom : itemarr[i].uomconversiontype ? true : false,
				itemid_usebillinguom : itemarr[i].itemid_usebillinguom,
				usebillinguom : itemarr[i].usebillinguom,
				billinguomid : itemarr[i].billinguomid,
				billingrate : itemarr[i].billingrate,
				billingquantity : itemarr[i].billingquantity,
				billingconversiontype : itemarr[i].billingconversiontype,
				billingconversionfactor : itemarr[i].billingconversionfactor,
				boqid : itemarr[i].id
			};
			this.customFieldsOperation('projectitems', tempSalesInvItemObj, itemarr[i], 'proformainvoiceitems');
			proformainvoiceitems.push(tempSalesInvItemObj);
		}
	}
	this.props.updateFormState(this.props.form, {
		proformainvoiceitems
	});
	this.controller.computeFinalRate();
	this.updateLoaderFlag(false);
}

export function callbackInvoiceMilestoneProjectItem(invoiceitemsArr) {
	this.updateLoaderFlag(true);
	let proformainvoiceitems = [...this.props.resource.proformainvoiceitems];
	let itemidArray = [], boqstatusidArr = [];

	proformainvoiceitems.forEach((invitem) => {
		boqstatusidArr.push(invitem.boqmilestonestatusid);
	});

	invoiceitemsArr.forEach((item) => {
		let itemFound = false;
		proformainvoiceitems.forEach((invoiceitem, invoiceindex) => {
			if(item.boqid == invoiceitem.boqid && item.boqmilestonestatusid == invoiceitem.boqmilestonestatusid) {
				invoiceitem.quantity = item.qty;

				if (!item.checked)
					proformainvoiceitems.splice(invoiceindex, 1);

				itemFound=true;
			}
		});

		if(!itemFound && item.checked) {
			if(!boqstatusidArr.includes(item.boqmilestonestatusid) && item.qty > 0) {
				itemidArray.push(item.boqid_itemid);
				let temprate = Number(item.rate.toFixed(this.props.app.roundOffPrecision));
				let tempSalesInvItemObj = {
					itemid: item.boqid_itemid,
					itemid_name : item.itemid_name,
					itemid_itemtype : item.itemid_itemtype,
					uomid : item.boqid_uomid,
					uomconversiontype : item.boqid_uomconversiontype,
					uomconversionfactor : item.boqid_uomconversionfactor,
					description : item.boqid_description,
					quantity : item.qty,
					splitrate : item.boqid_splitrate,
					labourrate : item.boqid_labourrate,
					materialrate : item.boqid_materialrate,
					rate : temprate,
					sourceresource : 'boqmilestonestatus',
					sourceid : item.boqmilestonestatusid,
					discountquantity : item.boqid_discountquantity,
					discountmode : item.boqid_discountmode,
					labourtaxid : item.boqid_labourtaxid,
					materialtaxid : item.boqid_materialtaxid,
					taxid : item.boqid_taxid,
					alternateuom : item.boqid_uomconversiontype ? true : false,
					itemid_usebillinguom : item.itemid_usebillinguom,
					usebillinguom : item.boqid_usebillinguom,
					billinguomid : item.boqid_billinguomid,
					billingrate : item.boqid_billingrate,
					billingquantity : item.boqid_billingquantity,
					billingconversiontype : item.boqid_billingconversiontype,
					billingconversionfactor : item.boqid_billingconversionfactor,
					boqmilestonestatusid : item.boqmilestonestatusid,
					boqmilestonestatusid_name : item.boqmilestonestatusid_name,
					boqid : item.boqid,
					boqid_rate : item.boqid_rate,
					milestonestageid : item.milestonestageid
				};
				this.customFieldsOperation('projectitems', tempSalesInvItemObj, item, 'proformainvoiceitems');
				proformainvoiceitems.push(tempSalesInvItemObj);
			}
		}
	});

	this.props.updateFormState(this.props.form, {
		proformainvoiceitems
	});
	this.controller.computeFinalRate();
	this.updateLoaderFlag(false);
}

export function emailFn (emailObj, callback) {
	emailObj.id = this.props.resource.id;
	emailObj.modified = this.props.resource.modified;
	let extradocuments = emailObj.extradocuments;

	axios({
		method : 'post',
		data : {
			actionverb : 'Send To Customer',
			data : emailObj
		},
		url : '/api/proformainvoices'
	}).then((response) => {
		if (response.data.message == 'success') {
			if (emailObj.firsttime) {
				if (emailObj.sendemail) {
					emailObj = response.data.main;
					emailObj.toaddress = this.props.resource.contactid_email;
					emailObj.bcc = response.data.main.bcc;
					emailObj.cc = response.data.main.cc;
					emailObj.firsttime = false;
					emailObj.extradocuments = extradocuments;
					callback(emailObj, false);
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						status: response.data.main.status,
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
				}
			} else {
				callback(null, true);
				this.props.updateFormState(this.props.form, {
					status: response.data.main.status,
					modified: response.data.main.modified
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
			}
		} else {
			callback(null, true);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, this.controller.callbackModal));
		}
	});
}

export function save (param, confirm) {
	if (param == 'Send To Customer') {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'proformainvoices'} activityemail={false} callback={this.controller.emailFn} openModal={this.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
	} else {
		this.updateLoaderFlag(true);

		if(param != 'Update' && param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
			this.updateLoaderFlag(false);
			return true;
		}

		let tempObj = JSON.parse(JSON.stringify(this.props.resource));

		tempObj.amountinwords = commonMethods.numberToWord(this.props.app, this.props.resource.amount, this.props.resource.currencyid);
		if (this.props.resource.currencyid == this.props.app.defaultCurrency) {
			tempObj.amountinwordslc = commonMethods.numberToWord(this.props.app, this.props.resource.amount);
			tempObj.amountlc = this.props.resource.amount;
		} else {
			if (this.props.resource.currencyexchangerate) {
				tempObj.amountlc = Number((this.props.resource.amount * this.props.resource.currencyexchangerate).toFixed(this.props.app.roundOffPrecision));				
				tempObj.amountinwordslc = commonMethods.numberToWord(this.props.app, tempObj.amountlc);
			} else {
				tempObj.amountlc = 0;
			}
		}

		if(this.props.resource.proformafor == 'Contract' || this.props.resource.proformafor == 'Contract Enquiry'){
			tempObj.proformainvoiceitems = [];
			tempObj.equipmentitems.forEach((item) => {
				tempObj.proformainvoiceitems.push(item);
			});
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : tempObj,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/proformainvoices'
		}).then((response)=> {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if(resparam)
					this.controller.save(param, true);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam) {
					this.props.history.replace(`/details/proformainvoices/${response.data.main.id}`);	
				} else {
					if (param == 'Delete') {
						this.props.history.push("/list/proformainvoices");
					} else {
						let tempObj = response.data.main;
		
						if(tempObj.proformafor == 'Contract' || tempObj.proformafor == 'Contract Enquiry'){
							tempObj.equipmentitems = [];
							tempObj.proformainvoiceitems.forEach((item) => {
								tempObj.equipmentitems.push(item);
							});
							tempObj.proformainvoiceitems = [];
						}
						this.props.initialize(tempObj);
					}
				}
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function createDC () {
	this.updateLoaderFlag(true);
	this.props.history.push({pathname: '/createDeliveryNote', params: {...this.props.resource, param: 'Proforma Invoices'}});
}

export function createSalesInvoice () {
	this.updateLoaderFlag(true);
	if(this.props.resource.proformafor == 'Contract'){
		let tempObj = JSON.parse(JSON.stringify(this.props.resource));
		tempObj.proformainvoiceitems = tempObj.equipmentitems;
		tempObj.param = 'Proforma Invoices';
		this.props.history.push({pathname: '/createContractInvoice', params: tempObj});
	}
	if(this.props.resource.proformafor == 'Project')
		this.props.history.push({pathname: '/createSalesInvoice', params: {...this.props.resource, param: 'Proforma Invoices'}});
}

export function createReceipt () {
	this.updateLoaderFlag(true);
	if (this.props.resource.paymentstatus == 'Received') {
		let message = {
			header : 'Create Receipt Voucher ?',
			body : 'Advance Receipt Voucher already created. Would you like to create the receipt voucher now?',
			btnArray : ['Yes','No']
		};
		this.props.openModal(modalService['confirmMethod'](message, (param) => {
			if(param) {
				checkTransactionExist('receiptvouchers', 'proformainvoices', this.props.resource.id, this.openModal, (confirm) => {
					if(confirm)
						this.props.history.push({pathname: '/createReceiptVoucher', params: {...this.props.resource, param: 'Proforma Invoices'}});
					this.updateLoaderFlag(false);
				});	
			}
			this.updateLoaderFlag(false);
		}));
	}else{
		checkTransactionExist('receiptvouchers', 'proformainvoices', this.props.resource.id, this.openModal, (confirm) => {
			if(confirm)
				this.props.history.push({pathname: '/createReceiptVoucher', params: {...this.props.resource, param: 'Proforma Invoices'}});
			this.updateLoaderFlag(false);
		});
	}
}

export function cancel () {
	this.props.history.goBack();
}
