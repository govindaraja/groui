import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import PreviousissueditemModal from '../components/details/previousissueditemmodal';
import StockdetailsModal from '../containers/stockdetails';
import { ItemrequestprofitanalyseModal } from '../components/details/itemrequestprofitabilityanalyse';
import { RenderItemQty } from '../utils/customfieldtypes';
import ProjectEstimationitemModal from '../components/details/projectestimationitemmodal';
import KititemaddModal from '../components/details/kititemaddmodal';
import ItemrequestProjectdetailsModal from '../components/details/itemrequestprojectitemdetailsmodal';
import ProductionRawMaterialItemModal from '../components/details/productionrawmaterialitemmodal';
import WorkOrderIssueItemModal from '../components/details/workorderissueitemmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		itemrequestdate: new Date(new Date().setHours(0, 0, 0, 0)),
		itemrequestitems: [],
		kititemitemrequestdetails: [],
		equipmentitems: []
	};

	if(this.props.location.params) {
		const params = this.props.location.params;
		tempObj.equipmentitems = [];

		if (params.param == 'copy') {
			tempObj = params;
		}

		if (params.param == 'Service Calls') {
			tempObj.itemrequestfor = 'Service Call';
			tempObj.servicecallid = params.id;
			tempObj.servicecallid_servicecallno = params.servicecallno;
			tempObj.defaultcostcenter = params.defaultcostcenter;
			tempObj.deliveryaddress = params.address;
			tempObj.equipmentitems = params.equipmentitems;
			tempObj.territoryid = params.territoryid;
			tempObj.engineerid = this.props.app.user.isengineer ? this.props.app.user.id : null;
			this.customFieldsOperation('servicecalls', tempObj, params, 'itemrequests');
		}
		if (params.param == 'Estimations') {
			let itemrequestitemArray = params.estimationitems;

			tempObj.itemrequestfor = 'Service Call';
			tempObj.servicecallid = params.servicecallid;
			tempObj.servicecallid_servicecallno = params.servicecallid_servicecallno;
			tempObj.defaultcostcenter = params.servicecallid_defaultcostcenter;
			tempObj.equipmentitems = params.equipmentitems;
			tempObj.engineerid = params.engineerid;
			tempObj.territoryid = params.territoryid;
			tempObj.itemrequestitems = [];
			
			this.customFieldsOperation('estimations', tempObj, params, 'itemrequests');

			for (var i = 0; i < itemrequestitemArray.length; i++) {
				if(itemrequestitemArray[i].itemid_itemtype == 'Product') {
					let uomName;
					if (this.props.app.uomObj[itemrequestitemArray[i].uomid])
						uomName = this.props.app.uomObj[itemrequestitemArray[i].uomid].name;

					let tempchildObj = {
						itemid : itemrequestitemArray[i].itemid,
						itemid_name : itemrequestitemArray[i].itemid_name,
						itemid_issaleskit : itemrequestitemArray[i].itemid_issaleskit,
						itemid_keepstock : itemrequestitemArray[i].itemid_keepstock,
						itemid_uomgroupid : itemrequestitemArray[i].itemid_uomgroupid,
						freeorchargeable : itemrequestitemArray[i].freeorchargeable,
						description : itemrequestitemArray[i].description,
						quantity : itemrequestitemArray[i].quantity,						
						uomid : itemrequestitemArray[i].uomid,
						uomid_name : uomName,
						uomconversionfactor : itemrequestitemArray[i].uomconversionfactor,
						uomconversiontype : itemrequestitemArray[i].uomconversiontype,
						equipmentid : itemrequestitemArray[i].equipmentid,
						contracttypeid : itemrequestitemArray[i].contracttypeid,
						contractid : itemrequestitemArray[i].contractid,
						alternateuom: itemrequestitemArray[i].uomconversiontype ? true : false,
						itemid_usebillinguom: itemrequestitemArray[i].itemid_usebillinguom,
						usebillinguom: itemrequestitemArray[i].usebillinguom,
						billinguomid: itemrequestitemArray[i].billinguomid,
						billingquantity: itemrequestitemArray[i].billingquantity,
						billingconversiontype: itemrequestitemArray[i].billingconversiontype,
						billingconversionfactor: itemrequestitemArray[i].billingconversionfactor
					};

					this.customFieldsOperation('estimationitems', tempchildObj, itemrequestitemArray[i], 'itemrequestitems');
					tempObj.itemrequestitems.push(tempchildObj);
				}
			}
		}
		if (params.param == 'Projects') {
			tempObj.itemrequestfor = 'Project';
			tempObj.projectid = params.id;
			tempObj.projectid_projectname = params.projectname;
			tempObj.projectid_estimationavailable = params.estimationavailable;
			tempObj.defaultcostcenter = params.defaultcostcenter;
			tempObj.territoryid = params.territoryid;
			tempObj.warehouseid = params.warehouseid;

			tempObj.itemrequestitems = [];

			this.customFieldsOperation('projects', tempObj, params, 'itemrequests');
		}
		if (params.param == 'Work Orders') {
			tempObj.itemrequestfor = 'Project';
			tempObj.workorderid = params.id;
			tempObj.contractorid = params.partnerid;
			tempObj.projectid = params.projectid;
			tempObj.projectid_estimationavailable = params.estimationavailable;
			tempObj.customerid = params.projectid_customerid;
			tempObj.projectid_projectname = params.projectid_projectname;
			tempObj.warehouseid = params.projectid_warehouseid;

			tempObj.itemrequestitems = [];

			this.customFieldsOperation('workorders', tempObj, params, 'itemrequests');
		}
		if (params.param == 'Production Orders') {
			let itemrequestitemArray = [];
			itemrequestitemArray = params.productionbomitems;

			tempObj.itemrequestfor = 'Production Order';
			tempObj.companyid = params.companyid;
			tempObj.productionorderid = params.id;
			tempObj.defaultcostcenter = params.defaultcostcenter;
			tempObj.warehouseid = params.rawmaterialwarehouseid;
			tempObj.itemrequestitems = [];

			this.customFieldsOperation('productionorders', tempObj, params, 'itemrequests');

			for (var i = 0; i < itemrequestitemArray.length; i++) {
				if(itemrequestitemArray[i].itemid_itemtype == 'Product') {
					let uomName;
					if (this.props.app.uomObj[itemrequestitemArray[i].uomid]) {
						uomName = this.props.app.uomObj[itemrequestitemArray[i].uomid].name;
					}

					let tempChildObj = {
						itemid : itemrequestitemArray[i].itemid,
						itemid_name : itemrequestitemArray[i].itemid_name,
						itemid_issaleskit : itemrequestitemArray[i].itemid_issaleskit,
						itemid_keepstock : itemrequestitemArray[i].itemid_keepstock,
						itemid_uomgroupid : itemrequestitemArray[i].itemid_uomgroupid,
						description : itemrequestitemArray[i].description,
						quantity : itemrequestitemArray[i].quantity,
						uomid : itemrequestitemArray[i].uomid,
						uomid_name : uomName,
						warehouseid : params.rawmaterialwarehouseid,
						productionbomitemsid : itemrequestitemArray[i].id
					};

					this.customFieldsOperation('productionbomitems', tempChildObj, itemrequestitemArray[i], 'itemrequestitems');
					tempObj.itemrequestitems.push(tempChildObj);
				}
			}
		}
		if (params.param == 'Job Work') {
			let itemrequestitemArray = params.workorderoutitems;

			tempObj.itemrequestfor = 'Job Work';
			tempObj.workorderid = params.id;
			tempObj.customerid = params.partnerid;
			tempObj.customerid_name = params.partnerid_name;
			tempObj.deliveryaddress = params.supplieraddress;
			tempObj.defaultcostcenter = params.defaultcostcenter;
			tempObj.workorderid_wonumber = params.wonumber;
			tempObj.itemrequestitems = [];
			
			this.customFieldsOperation('workorders', tempObj, params, 'itemrequests');

			for (var i = 0; i < itemrequestitemArray.length; i++) {
				if(itemrequestitemArray[i].itemid_itemtype == 'Product') {
					let uomName;
					if (this.props.app.uomObj[itemrequestitemArray[i].uomid])
						uomName = this.props.app.uomObj[itemrequestitemArray[i].uomid].name;

					let tempchildObj = {
						itemid : itemrequestitemArray[i].itemid,
						itemid_name : itemrequestitemArray[i].itemid_name,
						itemid_issaleskit : itemrequestitemArray[i].itemid_issaleskit,
						itemid_keepstock : itemrequestitemArray[i].itemid_keepstock,
						itemid_uomgroupid : itemrequestitemArray[i].itemid_uomgroupid,
						description : itemrequestitemArray[i].description,
						quantity : itemrequestitemArray[i].quantity,						
						uomid : itemrequestitemArray[i].uomid,
						uomid_name : uomName,
						uomconversionfactor : itemrequestitemArray[i].uomconversionfactor,
						uomconversiontype : itemrequestitemArray[i].uomconversiontype,
						alternateuom: itemrequestitemArray[i].uomconversiontype ? true : false,
						warehouseid: itemrequestitemArray[i].warehouseid,
						workorderoutitemsid: itemrequestitemArray[i].id,
						itemid_usebillinguom: itemrequestitemArray[i].itemid_usebillinguom,
						usebillinguom: itemrequestitemArray[i].usebillinguom,
						billinguomid: itemrequestitemArray[i].billinguomid,
						billingquantity: itemrequestitemArray[i].billingquantity,
						billingconversiontype: itemrequestitemArray[i].billingconversiontype,
						billingconversionfactor: itemrequestitemArray[i].billingconversionfactor
					};

					this.customFieldsOperation('workorderoutitems', tempchildObj, itemrequestitemArray[i], 'itemrequestitems');
					tempObj.itemrequestitems.push(tempchildObj);
				}
			}
		}

		if (params.param == 'Service Calls' || params.param == 'Estimations' || params.param == 'Projects')
			utils.assign(tempObj, params, ['companyid', 'customerid', 'customerid_name', 'contactid',  'contactperson', 'email', 'phone', 'mobile', 'salesperson', 'pricelistid', 'territoryid', 'currencyid', 'currencyexchangerate']);
	}

	this.props.initialize(tempObj);
	setTimeout(this.controller.currencyOnChange, 0);
	this.updateLoaderFlag(false);
}

export function renderTableQtyfield(item) {
	return (
		<RenderItemQty status={this.props.resource.status} resource={this.props.resource} item={item} uomObj={this.props.app.uomObj} statusArray={['Approved']}/>
	);
}

export function getItemById () {
	axios.get(`/api/itemrequests/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			for (var i = 0; i < tempObj.itemrequestitems.length; i++) {
				if (tempObj.itemrequestitems[i].uomconversiontype)
					tempObj.itemrequestitems[i].alternateuom = true;
			}
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});

			this.controller.showVisibilityForButton();
			setTimeout(this.controller.getEquipmentItems, 0);

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getEquipmentItems () {
	if(!this.props.resource.servicecallid)
		return null;

	let queryString = `/api/equipmentitems?field=id,equipmentid,equipments/displayname/equipmentid,equipments/description/equipmentid,equipments/serialno/equipmentid,contractid,contracts/contractno/contractid,contracttypeid,contracttypes/contracttypes/contracttypeid,scheduleid&filtercondition=equipmentitems.parentresource='servicecalls' and equipmentitems.parentid=${this.props.resource.servicecallid}`;

	axios.get(`${queryString}`).then((response)=> {
		if (response.data.message == 'success') {
			let equipmentitems = response.data.main.map((item) => {
				return {
					equipmentid : item.equipmentid,
					equipmentid_displayname : item.equipmentid_displayname,
					equipmentid_serialno : item.equipmentid_serialno,
					equipmentid_description : item.equipmentid_description,
					contracttypeid : item.contracttypeid,
					contracttypeid_name : item.contracttypeid_name,
					contractid : item.contractid,
					contractid_contractno : item.contractid_contractno,
					scheduleid : item.scheduleid
				}
			});
			this.props.updateFormState(this.props.form, {
				equipmentitems
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject)=> {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});
		}, (reason)=> {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
	}
}

export function requestForOnChange() {
	this.props.updateFormState(this.props.form, {
		servicecallid: null,
		customerid: null,
		projectid: null,
		departmentid: null,
		itemrequestitems: []
	});
}

export function callbackEngineer(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		salesperson: valueobj.id
	});
}

export function workorderOnChange(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contractorid: valueobj.partnerid
	});
}

export function projectOnChange(id, valueobj) {
	let tempObj = {
		warehouseid: valueobj.warehouseid,
		defaultcostcenter: valueobj.defaultcostcenter,
		projectid_estimationavailable : valueobj.estimationavailable
	};
	this.customFieldsOperation('projects', tempObj, valueobj, 'itemrequests');
	this.props.updateFormState(this.props.form, tempObj);
}

export function addressonChange(address, addressobj) {
	let tempObj = {};
	if(address == 'deliveryaddress') {
		tempObj.deliveryaddress = addressobj.displayaddress;
		tempObj.deliveryaddressid = addressobj.id;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function callbackProduct (id, valueobj, item, itemstr) {
	this.controller.getItemDetails(valueobj, item, itemstr);
}

export function getItemDetails(itemobj, item, itemstr) {
	let promise = commonMethods.getItemDetails(itemobj.id, this.props.resource.customerid, null, 'sales', item.uomid, item.alternateuom, item.uomconversionfactor, item.contractid, this.props.resource.servicecallid, item.equipmentid);
	promise.then((returnObject)=> {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app);
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.freeorchargeable`] = returnObject.freeorchargeable;
		tempObj[`${itemstr}.uomid`] = (this.props.resource.itemrequestfor == 'Engineer' || this.props.resource.itemrequestfor == 'General') ? itemobj.stockuomid : returnObject.uomid;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.taxid`] = returnObject.taxid;
		tempObj[`${itemstr}.itemid_keepstock`] = returnObject.keepstock;
		tempObj[`${itemstr}.itemid_hasbatch`] = returnObject.hasbatch;
		tempObj[`${itemstr}.itemid_hasserial`] = returnObject.hasserial;
		tempObj[`${itemstr}.accountid`] = returnObject.itemid_incomeaccountid;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'itemrequestitems', itemstr);

		if (returnObject.alternateuom && (this.props.resource.itemrequestfor != 'Engineer' && this.props.resource.itemrequestfor != 'General')) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		if (item.index) {
			let itemCount = 0;
			tempObj.kititemitemrequestdetails = [...this.props.resource.kititemitemrequestdetails];
			for (var i = 0; i < tempObj.kititemitemrequestdetails.length; i++) {
				if (tempObj.kititemitemrequestdetails[i].rootindex == item.index)
					itemCount++;
			}
			for (var i = 0; i < itemCount; i++) {
				for (var j = 0; j < tempObj.kititemitemrequestdetails.length; j++) {
					if (tempObj.kititemitemrequestdetails[j].rootindex == item.index) {
						tempObj.kititemitemrequestdetails.splice(j, 1);
						break;
					}
				}
			}

			if (returnObject.itemid_issaleskit) {
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = item.index;
					tempObj.kititemitemrequestdetails.push(returnObject.kititems[i]);
				}
			}
		} else {
			let index = 0;
			for (var i = 0; i < this.props.resource.itemrequestitems.length; i++) {
				if (this.props.resource.itemrequestitems[i].index > index)
					index = this.props.resource.itemrequestitems[i].index;
			}
			tempObj[`${itemstr}.index`] = index + 1;

			if (returnObject.itemid_issaleskit) {
				tempObj.kititemitemrequestdetails = [...this.props.resource.kititemitemrequestdetails];
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = tempObj[`${itemstr}.index`];
					tempObj.kititemitemrequestdetails.push(returnObject.kititems[i]);
				}
			}
		}

		this.props.updateFormState(this.props.form, tempObj);
		var tempchilditem = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem.quantity, tempchilditem, itemstr);
	}, (reason)=> {});
}

export function uomOnchange (id, valueobj, itemstr, type) {
	let tempObj = {};
	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);
	let item = this.selector(this.props.fullstate, itemstr);

	if(item.usebillinguom) {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.billingquantity`]: Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))
		});
	}
}

export function quantityOnChange(itemqty, item, itemstr) {
	let kititemitemrequestdetails = this.props.resource.kititemitemrequestdetails;
	let tempObj = {};
	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;

	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
	}

	for (var i = 0; i < kititemitemrequestdetails.length; i++) {
		if (item.index == kititemitemrequestdetails[i].rootindex) {
			if (!kititemitemrequestdetails[i].parentindex) {
				let tempIndex = kititemitemrequestdetails[i].index;
				let tempQuantity = Number((kititemitemrequestdetails[i].conversion * itemqty).toFixed(roundOffPrecisionStock));
				tempObj[`kititemitemrequestdetails[${i}].quantity`] = tempQuantity;
				calculateQuantity(tempIndex, tempQuantity, item.index);
			}
		}
	}
	function calculateQuantity(tempIndex, tempQuantity, index) {
		for (var j = 0; j < kititemitemrequestdetails.length; j++) {
			if (tempIndex == kititemitemrequestdetails[j].parentindex && index == kititemitemrequestdetails[j].rootindex) {
				let sectempQty = Number((kititemitemrequestdetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
				tempObj[`kititemitemrequestdetails[${j}].quantity`] = sectempQty;
				calculateQuantity(kititemitemrequestdetails[j].index, sectempQty, index);
			}
		}
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function conversionfactoronchange (value, itemstr, type) {
	let item = this.selector(this.props.fullstate, itemstr);
	if (value > 0 && item.usebillinguom) {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.billingquantity`]: Number(((item.quantity / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))
		});
	}
}

export function warehouseonchange() {
	let tempObj = {};
	for (var i = 0; i < this.props.resource.itemrequestitems.length; i++) {
		if (!this.props.resource.itemrequestitems[i].warehouseid)
			tempObj[`itemrequestitems[${i}].warehouseid`] = this.props.resource.warehouseid;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function requiredateChange() {
	let tempObj = {};
	for (var i = 0; i < this.props.resource.itemrequestitems.length; i++) {
		if (!this.props.resource.itemrequestitems[i].requireddate)
			tempObj[`itemrequestitems[${i}].requireddate`] = this.props.resource.requireddate;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function equipmentonChange(equipmentid, item, itemstr) {
	let tempObj = {};
	if(equipmentid > 0) {
		for(var i = 0; i < this.props.resource.equipmentitems.length; i++) {
			if(this.props.resource.equipmentitems[i].equipmentid == equipmentid) {
				tempObj[`${itemstr}.contractid`] = this.props.resource.equipmentitems[i].contractid;
				tempObj[`${itemstr}.contractid_contractno`] = this.props.resource.equipmentitems[i].contractid_contractno;
				tempObj[`${itemstr}.contracttypeid`] = this.props.resource.equipmentitems[i].contracttypeid;
				tempObj[`${itemstr}.contracttypeid_name`] = this.props.resource.equipmentitems[i].contracttypeid_name;
			}
		}
		this.props.updateFormState(this.props.form, tempObj);
	} else {
		tempObj[`${itemstr}.contractid`] = null;
		tempObj[`${itemstr}.contractid_contractno`] = null;
		tempObj[`${itemstr}.contracttypeid`] = null;
		tempObj[`${itemstr}.contracttypeid_name`] = null;
		this.props.updateFormState(this.props.form, tempObj);
	}

	let promise1 = commonMethods.getItemDetails(item.itemid, this.props.resource.customerid, null, 'sales', item.uomid, item.alternateuom, item.uomconversionfactor, equipmentid > 0 ? tempObj[`${itemstr}.contractid`] : null, this.props.resource.servicecallid, equipmentid > 0 ? equipmentid : null);
	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.freeorchargeable`] = returnObject.freeorchargeable;

		this.props.updateFormState(this.props.form, rateUpdateObj);
	}, (reason) => {});
}

export function addKitItem() {
	this.openModal({
		render: (closeModal) => {
			return <KititemaddModal resource = {this.props.resource} child={'itemrequestitems'} kit={'kititemitemrequestdetails'} array={this.props.array} callback={()=>{
				for (var i = 0; i < this.props.resource.itemrequestitems.length; i++) {
					this.controller.quantityOnChange(this.props.resource.itemrequestitems[i].quantity, this.props.resource.itemrequestitems[i], `itemrequestitems[${i}]`);
				}
			}} closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function conversionOnChange (item) {
	for (var i = 0; i < this.props.resource.itemrequestitems.length; i++) {
		if (item.rootindex == this.props.resource.itemrequestitems[i].index) {
			this.controller.quantityOnChange(this.props.resource.itemrequestitems[i].quantity, this.props.resource.itemrequestitems[i], `itemrequestitems[${i}]`);
			break;
		}
	}
}

export function deleteItemRequestItem (index) {
	let itemCount = 0;
	let kititemitemrequestdetails = [...this.props.resource.kititemitemrequestdetails];
	let itemrequestitems = [...this.props.resource.itemrequestitems];

	for (var i = 0; i < kititemitemrequestdetails.length; i++) {
		if (kititemitemrequestdetails[i].rootindex == this.props.resource.itemrequestitems[index].index)
			itemCount++;
	}
	for (var i = 0; i < itemCount; i++) {
		for (var j = 0; j < kititemitemrequestdetails.length; j++) {
			if (kititemitemrequestdetails[j].rootindex == this.props.resource.itemrequestitems[index].index) {
				kititemitemrequestdetails.splice(j, 1);
				break;
			}
		}
	}
	itemrequestitems.splice(index, 1);

	this.props.updateFormState(this.props.form, {
		kititemitemrequestdetails,
		itemrequestitems
	});
}

export function deleteKitItem(index) {
	let errorArray = [];
	let itemfound = false;
	let kititemitemrequestdetails = [...this.props.resource.kititemitemrequestdetails];

	for (var i = 0; i < kititemitemrequestdetails.length; i++) {
		if (index != i && kititemitemrequestdetails[index].rootindex == kititemitemrequestdetails[i].rootindex && kititemitemrequestdetails[index].parentindex == kititemitemrequestdetails[i].parentindex) {
			itemfound = true;
		}
	}
	if (!itemfound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	let itemCouFound = false;
	for (var i = 0; i < kititemitemrequestdetails.length; i++) {
		if (index != i && kititemitemrequestdetails[index].rootindex == kititemitemrequestdetails[i].rootindex && !kititemitemrequestdetails[i].parentindex)
			itemCouFound = true;
	}
	if (!itemCouFound)
		errorArray.push("Item is a only child for parent. Delete the Parent item");

	if (errorArray.length == 0) {
		let parentIndexArray = [kititemitemrequestdetails[index].index];
		let rootindex = kititemitemrequestdetails[index].rootindex;
		deleteArray(kititemitemrequestdetails[index].rootindex, kititemitemrequestdetails[index].index);

		function deleteArray(rootindex, index) {
			for (var i = 0; i < kititemitemrequestdetails.length; i++) {
				if (kititemitemrequestdetails[i].rootindex == rootindex && kititemitemrequestdetails[i].parentindex == index) {
					parentIndexArray.push(kititemitemrequestdetails[i].index)
					deleteArray(rootindex, kititemitemrequestdetails[i].index);
				}
			}
		}

		for (var i = 0; i < parentIndexArray.length; i++) {
			for (var j = 0; j < kititemitemrequestdetails.length; j++) {
				if (kititemitemrequestdetails[j].rootindex == rootindex && kititemitemrequestdetails[j].index == parentIndexArray[i]) {
					kititemitemrequestdetails.splice(j, 1);
					break;
				}
			}
		}

		this.props.updateFormState(this.props.form, {kititemitemrequestdetails});
	} else {
		let response = {
			data : {
				message : 'failure',
				error : utils.removeDuplicate(errorArray)
			}
		};
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}
}

export function openPrevIssueDetails (item) {
	this.openModal({render: (closeModal) => {return <PreviousissueditemModal servicecallid={this.props.resource.servicecallid} customerid={this.props.resource.customerid} engineerid={this.props.resource.engineerid} item={item} openModal={this.props.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function openStockDetails (item) {
	this.openModal({render: (closeModal) => {return <StockdetailsModal resource = {this.props.resource} item={item} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function getProjectEstimationDetails () {	
	this.openModal({
		render: (closeModal) => {
			return <ProjectEstimationitemModal itemrequestitems={this.props.resource.itemrequestitems} projectid={this.props.resource.projectid} closeModal={closeModal} openModal={this.props.openModal} customFieldsOperation={this.customFieldsOperation} callback = {(itemreqarr)=> {
				this.controller.callbackProjectEstimation(itemreqarr);
			}} />
		}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
	});
}

export function callbackProjectEstimation (itemreqarr) {
	this.updateLoaderFlag(true);
	let itemrequestitems = [...this.props.resource.itemrequestitems];
	let kititemitemrequestdetails = [...this.props.resource.kititemitemrequestdetails];
	let projectidArr = [], kititemindexArr = [];
	for(var i = 0; i < itemreqarr.length; i++) {
		let index = 0;
		for (var k = 0; k < itemrequestitems.length; k++) {
			if (itemrequestitems[k].index > index)
				index = itemrequestitems[k].index;
		}
		let itemFound=false;
		for(var j = 0; j < itemrequestitems.length; j++) {
			if(itemrequestitems[j].projectestimationitemsid == itemreqarr[i].projectestimationitemsid) {
				itemrequestitems[j].quantity = itemreqarr[i].quantity;
				itemFound=true;
				if (!itemreqarr[i].checked) {

					let itemCount = 0;
					for (var l = 0; l < kititemitemrequestdetails.length; l++) {
						if (kititemitemrequestdetails[l].rootindex == this.props.resource.itemrequestitems[j].index)
							itemCount++;
					}
					for (var m = 0; m < itemCount; m++) {
						for (var n = 0; n < kititemitemrequestdetails.length; n++) {
							if (kititemitemrequestdetails[n].rootindex == this.props.resource.itemrequestitems[j].index) {
								kititemitemrequestdetails.splice(n, 1);
								break;
							}
						}
					}
					itemrequestitems.splice(j, 1);
				}
				break;
			}
		}
		if(!itemFound && itemreqarr[i].checked) {
			let tempItemRequestItemObj = {
				projectestimationitemsid : itemreqarr[i].projectestimationitemsid,
				projectid : itemreqarr[i].projectid,
				boqitemsid : itemreqarr[i].boqitemsid,
				boqitemid : itemreqarr[i].boqitemid,
				boqitemsid_internalrefno : itemreqarr[i].boqitemsid_internalrefno,
				boqitemsid_index : itemreqarr[i].boqitemid == itemreqarr[i].itemid ? itemreqarr[i].boqitemsid_index : null,
				itemid : itemreqarr[i].itemid,
				itemid_name : itemreqarr[i].itemid_name,
				itemid_keepstock : itemreqarr[i].itemid_keepstock,
				itemid_issaleskit : itemreqarr[i].itemid_issaleskit,
				description : itemreqarr[i].description,
				specification : itemreqarr[i].specification,
				warehouseid : this.props.resource.warehouseid,
				quantity : itemreqarr[i].quantity,
				uomid : itemreqarr[i].uomid,
				uomid_name : itemreqarr[i].uomid_name,
				uomconversionfactor : itemreqarr[i].uomconversionfactor,
				uomconversiontype : itemreqarr[i].uomconversiontype,
				index :  index + 1,
				requireddate : this.props.resource.requireddate,
				usebillinguom : itemreqarr[i].usebillinguom,
				billingconversionfactor : itemreqarr[i].billingconversionfactor,
				billingconversiontype : itemreqarr[i].billingconversiontype,
				billinguomid : itemreqarr[i].billinguomid,
				billingquantity : itemreqarr[i].usebillinguom ? Number(((itemreqarr[i].quantity / (itemreqarr[i].uomconversionfactor ? itemreqarr[i].uomconversionfactor : 1)) * itemreqarr[i].billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock)) : null,
				approvedmakes : itemreqarr[i].approvedmakes
			};

			if(itemreqarr[i].projectid) {
				projectidArr.push(itemreqarr[i].projectid);
			}
			if(itemreqarr[i].itemid_issaleskit && itemreqarr[i].boqitemid != itemreqarr[i].itemid) {
				kititemindexArr.push(tempItemRequestItemObj.index);
			}
			this.customFieldsOperation('projectestimationitems', tempItemRequestItemObj, itemreqarr[i], 'itemrequestitems');
			itemrequestitems.push(tempItemRequestItemObj);
		}
	}
	for (var i = 0; i < itemrequestitems.length; i++) {
		if (itemrequestitems[i].uomconversiontype)
			itemrequestitems[i].alternateuom = true;
	}
	this.props.updateFormState(this.props.form, {
		itemrequestitems,
		kititemitemrequestdetails
	});
	setTimeout(() => {
		this.controller.getEstimationkititem(projectidArr, kititemindexArr);
	}, 0);
}

export function getEstimationkititem(projectidArr, kititemindexArr) {
	if(projectidArr.length == 0 && kititemindexArr.length == 0) {
		this.updateLoaderFlag(false);
		return null;
	}

	if(projectidArr.length == 0 && kititemindexArr.length > 0) {
		return this.controller.getKitItemAddedInProjectEstimation(0, kititemindexArr, this.props.resource.kititemitemrequestdetails);
	}

	let queryString = `/api/kititemprojectdetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,costratio,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid&filtercondition=kititemprojectdetails.parentid in (${projectidArr.join()})`;

	axios.get(`${queryString}`).then((response)=> {
		if (response.data.message == 'success') {
			let kititemitemrequestdetails = [...this.props.resource.kititemitemrequestdetails];
			let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;
			this.props.resource.itemrequestitems.forEach((item) => {
				for (var j = 0; j < response.data.main.length; j++) {
					if(!item.id && response.data.main[j].parentid == item.projectid && item.boqitemsid_index == response.data.main[j].rootindex) {
						kititemitemrequestdetails.push({
							itemid: response.data.main[j].itemid,
							itemid_name: response.data.main[j].itemid_name,
							parent: response.data.main[j].parent,
							uomid: response.data.main[j].uomid,
							uomid_name: response.data.main[j].uomid_name,
							quantity: response.data.main[j].quantity,
							rate: response.data.main[j].rate,
							conversion: response.data.main[j].conversion,
							itemid_issaleskit: response.data.main[j].itemid_issaleskit,
							itemid_hasserial: response.data.main[j].itemid_hasserial,
							itemid_hasbatch: response.data.main[j].itemid_hasbatch,
							itemid_keepstock: response.data.main[j].itemid_keepstock,
							rootindex: item.index,
							warehouseid: item.warehouseid,
							costratio: response.data.main[j].costratio,
							index: response.data.main[j].index,
							parentindex: response.data.main[j].parentindex
						});
					}
				}
			});
			this.props.resource.itemrequestitems.forEach((invitem) => {
				for (var i = 0; i < kititemitemrequestdetails.length; i++) {
					if (!invitem.id && invitem.index == kititemitemrequestdetails[i].rootindex) {
						if (!kititemitemrequestdetails[i].parentindex) {
							let tempIndex = kititemitemrequestdetails[i].index;
							let tempQuantity = Number((kititemitemrequestdetails[i].conversion * invitem.quantity).toFixed(roundOffPrecisionStock));
							kititemitemrequestdetails[i].quantity = tempQuantity;
							calculateQuantity(tempIndex, tempQuantity, invitem.index);
						}
					}
				}
			});
			function calculateQuantity(tempIndex, tempQuantity, index) {
				for (var j = 0; j < kititemitemrequestdetails.length; j++) {
					if (tempIndex == kititemitemrequestdetails[j].parentindex && index == kititemitemrequestdetails[j].rootindex) {
						let sectempQty = Number((kititemitemrequestdetails[j].conversion * tempQuantity).toFixed(roundOffPrecisionStock));
						kititemitemrequestdetails[j].quantity = sectempQty;
						calculateQuantity(kititemitemrequestdetails[j].index, sectempQty, index);
					}
				}
			}

			this.props.updateFormState(this.props.form, {
				kititemitemrequestdetails
			});

			setTimeout(()=>this.controller.getKitItemAddedInProjectEstimation(0, kititemindexArr, this.props.resource.kititemitemrequestdetails), 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getKitItemAddedInProjectEstimation(count, kititemindexArr, kititemitemrequestdetails) {
	if(kititemindexArr.length == 0) {
		this.updateLoaderFlag(false);
		return null;
	}

	let roundOffPrecisionStock = this.props.app.roundOffPrecisionStock;
	if(count < kititemindexArr.length) {
		this.props.resource.itemrequestitems.forEach((item, itemindex) => {
			if(!item.id && item.index == kititemindexArr[count]) {
				if(this.props.resource.itemrequestitems[itemindex].itemid_issaleskit) {
					let promise1 = commonMethods.getItemDetails(this.props.resource.itemrequestitems[itemindex].itemid, this.props.resource.customerid, null, 'sales');
					promise1.then((returnObject)=> {
						returnObject.kititems.forEach((kititem) => {
							kititem.rootindex = this.props.resource.itemrequestitems[itemindex].index;
							if(!kititem.parentindex) {
								let tempQuantity = Number((kititem.conversion * this.props.resource.itemrequestitems[itemindex].quantity).toFixed(roundOffPrecisionStock));
								calculateQuantity(kititem.index, tempQuantity);
								kititem.quantity = tempQuantity;
							}
						});
						function calculateQuantity(tempIndex, tempQuantity) {
							returnObject.kititems.forEach((kititem) => {
								if (kititem.parentindex == tempIndex) {
									let sectempQty = Number((kititem.conversion * tempQuantity).toFixed(roundOffPrecisionStock));
									kititem.quantity = sectempQty;
									calculateQuantity(kititem.index, sectempQty);
								}
							});
						}
						returnObject.kititems.forEach((kititem) => {
							kititemitemrequestdetails.push(kititem);
						});
						this.controller.getKitItemAddedInProjectEstimation(count+1, kititemindexArr, kititemitemrequestdetails);
					}, (reason)=> {});
				} else {
					this.controller.getKitItemAddedInProjectEstimation(count+1, kititemindexArr, kititemitemrequestdetails);
				}
			}
		});
	} else {
		this.props.updateFormState(this.props.form, {
			kititemitemrequestdetails
		});
		this.updateLoaderFlag(false);
	}
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param != 'Cancel' && param != 'Delete' && param !='Amend' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions: confirm ? true : false
		},
		url : '/api/itemrequests'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (paramflag)=>{
			if(paramflag)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/itemrequests/${response.data.main.id}`);
			} else {
				if (param == 'Delete')
					this.props.history.replace("/list/itemrequests");
				else {
					this.props.initialize(response.data.main);
					this.controller.showVisibilityForButton();
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function showVisibilityForButton() {
	setTimeout(() => {
		if(this.props.resource.status == 'Approved') {
			let showDelivery = false;
			let showReceipt = false;
			let showInvoice = false;
			this.props.resource.itemrequestitems.forEach((item) => {
				if(item.quantity > (item.deliveredqty ? item.deliveredqty : 0))
					showDelivery = true;

				if(this.props.resource.itemrequestfor == 'project') {
					if(((item.deliveredqty ? item.deliveredqty : 0) - (item.invoicedqty ? item.invoicedqty : 0)) > (item.returnedqty ? item.returnedqty : 0))
						showReceipt = true;
				} else {
					if(((item.deliveredqty ? item.deliveredqty : 0) - (item.invoicedqty ? item.invoicedqty : 0) - (item.usedqty ? item.usedqty : 0)) > (item.returnedqty ? item.returnedqty : 0))
						showReceipt = true;
				}
				if(this.props.resource.servicecallid > 0) {
					if(item.freeorchargeable == 'Chargeable' && ((item.quantity ? item.quantity : 0) - (item.returnedqty ? item.returnedqty : 0) - (item.invoicedqty ? item.invoicedqty : 0)) > 0)
						showInvoice = true;
				}
			});
			this.setState({
				showDelivery,
				showReceipt,
				showInvoice
			});
		}
	}, 0);
}

export function profitanalysedetails() {
	this.openModal({render: (closeModal) => {
		return <ItemrequestprofitanalyseModal resource = {this.props.resource} param="itemrequests" app={this.props.app} closeModal={closeModal} />
		}, className: {
			content: 'react-modal-custom-class-60',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function getProjectItemDetails () {
	this.openModal({render: (closeModal) => {
		return <ItemrequestProjectdetailsModal resource={this.props.resource} customFieldsOperation={this.customFieldsOperation} updateFormState={this.props.updateFormState} form={this.props.form} openModal={this.props.openModal} closeModal={closeModal} getCustomFields={this.getCustomFields}/>
	}, className: {
		content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
	});
}

export function getProductionRawMaterialItemDetails () {
	this.openModal({render: (closeModal) => {
		return <ProductionRawMaterialItemModal resource={this.props.resource} customFieldsOperation={this.customFieldsOperation} updateFormState={this.props.updateFormState} form={this.props.form} openModal={this.props.openModal} closeModal={closeModal} getCustomFields={this.getCustomFields}/>
	}, className: {
		content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
	});
}

export function getWorkOrderIssueItemDetails () {
	this.openModal({render: (closeModal) => {
		return <WorkOrderIssueItemModal resource={this.props.resource} customFieldsOperation={this.customFieldsOperation} updateFormState={this.props.updateFormState} form={this.props.form} openModal={this.props.openModal} resourceName={'workorders'} closeModal={closeModal} getCustomFields={this.getCustomFields}/>
	}, className: {
		content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
	});
}



export function createPurchaseOrder() {
	let tempObj = this.props.resource;

	tempObj.param = "Item Requests for Project";
	for (var i = 0; i < tempObj.itemrequestitems.length; i++)
		tempObj.itemrequestitems[i].sourceresource = 'itemrequestitems';

	this.props.history.push({
		pathname: '/createPurchaseOrder',
		params: tempObj
	});
}

export function createDeliveryNote () {
	let tempObj = this.props.resource;

	if(tempObj.itemrequestfor == 'Service Call')
		tempObj.param = "Item Requests for Service Call";
	else if(tempObj.itemrequestfor == 'Engineer')
		tempObj.param = "Item Requests for Engineer";
	else if(tempObj.itemrequestfor == 'Production Order')
		tempObj.param = "Item Requests for Production Order";
	else if(tempObj.itemrequestfor == 'General')
		tempObj.param = "Item Requests for General";
	else if(tempObj.itemrequestfor == 'Job Work')
		tempObj.param = "Item Requests for Job Work";
	else
		tempObj.param = "Item Requests for Project";
	for (var i = 0; i < tempObj.itemrequestitems.length; i++)
		tempObj.itemrequestitems[i].sourceresource = 'itemrequestitems';

	this.props.history.push({
		pathname: '/createDeliveryNote',
		params: tempObj
	});
}

export function createReceiptNote () {
	let tempObj = this.props.resource;

	if(tempObj.itemrequestfor == 'Service Call')
		tempObj.param = "Item Requests for Service Call";
	else if(tempObj.itemrequestfor == 'Engineer')
		tempObj.param = "Item Requests for Engineer";
	else if(tempObj.itemrequestfor == 'Production Order')
		tempObj.param = "Item Requests for Production Order";
	else if(tempObj.itemrequestfor == 'General')
		tempObj.param = "Item Requests for General";
	else if(tempObj.itemrequestfor == 'Job Work')
		tempObj.param = "Item Requests for Job Work";
	else
		tempObj.param = "Item Requests for Project";

	for (var i = 0; i < tempObj.itemrequestitems.length; i++)
		tempObj.itemrequestitems[i].sourceresource = 'itemrequestitems';

	this.props.history.push({
		pathname: '/createReceiptNote',
		params: tempObj
	});
}

export function createServiceInvoice () {
	this.props.history.push({
		pathname: '/createServiceInvoice',
		params: { ...this.props.resource,
			param: 'Item Requests'
		}
	});
}

export function cancel () {
	this.props.history.goBack();
}
