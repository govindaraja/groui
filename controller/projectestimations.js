import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import { ChildEditModal } from '../components/utilcomponents';
import EstimationCostBreakupDetails from '../components/details/projectestimationcostbreakupdetails';
import EstimationOverheadCostDetails from '../components/details/projectestimationoverheadcostdetails';
import ProjectQuoteitemAddmodal from '../components/details/projectquoteitemaddmodal';
import BOQGridEditView from '../components/details/projectestimationboqgridview';
import { uomFilter, currencyFilter } from '../utils/filter';

export function onLoad () {
	let restrictestimationcommercials = false;
	this.props.app.feature.restrictprojectestimationrole.some((restrictrole) => {
		if(this.props.app.user.roleid.indexOf(restrictrole) > -1) {
			restrictestimationcommercials = true;
			return true;
		}
	});
	this.setState({
		restrictestimationcommercials
	}, () => {
		if(this.state.createParam)
			this.controller.initialiseState();
		else
			this.controller.getItemById();
	});
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		estimationdate: new Date(new Date().setHours(0, 0, 0, 0)),
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null,
		projectestimationitems: [],
		estimationitems: [{
			isoverhead: true,
			internalrefno: 'Overhead',
			rate: 0
		}],
		additionalcharges: []
	};
	let projectitemidArr = [];

	if(this.props.location.params) {
		const params = this.props.location.params;

		if(params.param == 'Project Quotes') {
			tempObj.estimationtype = 'Quotation';
			utils.assign(tempObj, params, [{'projectquoteid' : 'id'}, {'projectquoteid_quoteno' : 'quoteno'}, 'companyid', 'customerid', 'customerid_name', 'salesperson', 'currencyid', 'currencyexchangerate', 'projectname']);

			this.customFieldsOperation('projectquotes', tempObj, params, 'projectestimations');

			for (var i = 0; i < params.projectquoteitems.length; i++) {
				projectitemidArr.push(params.projectquoteitems[i].id);
			}
		}
		if(params.param == 'Projects') {
			tempObj.estimationtype = 'Project';
			utils.assign(tempObj, params, [{'projectid' : 'id'}, 'projectname', 'companyid', 'customerid', 'customerid_name', 'salesperson', 'currencyid', 'currencyexchangerate']);

			this.customFieldsOperation('projects', tempObj, params, 'projectestimations');

			for (var i = 0; i < params.projectitems.length; i++) {
				projectitemidArr.push(params.projectitems[i].id);
			}
		}
	}

	this.props.initialize(tempObj);

	setTimeout(() => {
		if(!this.props.resource.currencyexchangerate)
			this.controller.currencyOnChange();

		if(this.props.location.params && this.props.location.params.param == 'Project Quotes') {
			this.controller.getProjectEstimationitemDetails(projectitemidArr, 'Project Quotes', this.props.location.params.projectquoteitems);
		}
		if(this.props.location.params && this.props.location.params.param == 'Projects') {
			this.controller.getProjectEstimationitemDetails(projectitemidArr, 'Projects', this.props.location.params.projectitems);
		}
	}, 0);
	this.updateLoaderFlag(false);
}

export function renderTableReffield(item) {
	return (
		<div>
			<span className={`${((item.internalrefno && item.isoverhead) || item.estimationtype == 'Detailed') ? 'font-weight-bold' : (!item.isparent && item.internalrefno != 'Overhead' ? 'font-13' : '')}`}>{`${item.boqquoteitemsid ? (item.boqquoteitemsid_internalrefno || "") : (item.boqitemsid ? item.boqitemsid_internalrefno || "" : (item.internalrefno || ""))}`}</span>
			{item.boqquoteitemsid_internalrefno || item.boqitemsid_clientrefno ? <br></br> : null}
			<span className={`${!item.isparent && item.internalrefno != 'Overhead' ? 'font-13 text-muted' : 'text-muted'}`}>{item.boqquoteitemsid ? `${item.boqquoteitemsid_clientrefno ? `(${item.boqquoteitemsid_clientrefno || ''})` : ''}` : `${item.boqitemsid_clientrefno ? `(${item.boqitemsid_clientrefno || ''})` : ''}`}</span>
		</div>
	);
}

export function renderBOQEstimatedQty(item) {
	let tempqty = item.boqquoteitemsid ? (item.boqquoteitemsid_israteonly ? 'RO' : item.boqquoteitemsid_quantity) : item.boqitemsid_quantity;
	return (
		<div>
			{item.isparent ? <div className={`${item.estimationtype == 'Detailed' ? 'font-weight-bold' : ''}`}>{`${tempqty} ${uomFilter(item.boqquoteitemsid ? item.boqquoteitemsid_uomid : item.boqitemsid_uomid, this.props.app.uomObj)}`}</div> : null}
			{!item.isparent ? <div className={`${item.internalrefno!='Overhead' ? 'font-13' : ''}`}>{item.quantity} {uomFilter(item.uomid, this.props.app.uomObj)}</div> : null}
		</div>
	);
}

export function renderEstimationBOQItem(item) {
	return (
		<div>
			{item.isparent ? <div className={`${item.estimationtype == 'Detailed' ? 'font-weight-bold' : ''}`}>{item.boqitemid_name}</div> : <div className={`${item.internalrefno!='Overhead' ? 'pl-3 font-13' : ''}`}>{item.itemid_name}</div>}

			<div className={`text-ellipsis-line-2 font-13 text-muted ${!item.isparent && item.internalrefno != 'Overhead' ? 'pl-3' : ''}`}>{!item.isparent ? item.description : (item.estimationtype == 'Simple' ? item.description : (item.boqitemsid ? item.boqitemsid_description : item.boqquoteitemsid_description))}</div>
		</div>
	);
}

export function renderBOQEstimatedRate(item) {
	return (
		<div className={`${item.estimationtype == 'Detailed' || item.isoverhead ? 'font-weight-bold' : (!item.isparent && item.internalrefno!='Overhead' ? 'font-13' : '')}`}>
			{currencyFilter(item.rate, this.props.resource.currencyid, this.props.app)}
		</div>
	);
}

export function getItemById () {
	axios.get(`/api/projectestimations/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			this.controller.parseProjectEstimationItem(response.data.main);

			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function parseProjectEstimationItem(res) {
	let tempEstimationObj = res;
	tempEstimationObj.estimationitems = [];

	let itemprop = tempEstimationObj.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';
	let uniqueItemsArr = [];
	let uniqueItemObj = {};
	let overheaditems = [];

	tempEstimationObj.projectestimationitems.forEach((item) => {
		if(item[itemprop] > 0) {
			if(!uniqueItemObj[item[itemprop]]) {
				uniqueItemObj[item[itemprop]] = {
					[`${itemprop}`]: item[itemprop],
					[`${itemprop}_displayorder`]: item[`${itemprop}_displayorder`],
					items: []
				};
			}
			uniqueItemObj[item[itemprop]].items.push(item);
		} else {
			item.internalrefno = 'Overhead';
			overheaditems.push(item);
		}
	});

	uniqueItemsArr = Object.keys(uniqueItemObj).map(prop => uniqueItemObj[prop]);

	//uniqueItemsArr.sort((a, b) => a[`${itemprop}_displayorder`] - b[`${itemprop}_displayorder`] );

	uniqueItemsArr.forEach((item) => {
		let tempParentobj = {
			...item.items[0],
			estimationtype : item.items.length > 1 || (item.items[0].itemid != item.items[0].boqitemid) ? 'Detailed' : 'Simple',
			isparent: true
		};

		tempEstimationObj.estimationitems.push(tempParentobj);

		if(tempParentobj.estimationtype == 'Detailed') {
			item.items.forEach((propitem) => {
				propitem.isparent = false;
				tempEstimationObj.estimationitems.push(propitem);
			});
		}
	});

	tempEstimationObj.estimationitems.push({
		isoverhead: true,
		internalrefno: 'Overhead',
		rate: 0
	});
	tempEstimationObj.estimationitems.push(...overheaditems);

	this.props.initialize(tempEstimationObj);
	this.controller.computeFinalRate();
}

export function currencyOnChange () {
	if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
		let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
		promise.then((returnRateObject)=> {
			this.props.updateFormState(this.props.form, {
				currencyexchangerate : returnRateObject.rate
			});

		}, (reason)=> {});
	} else {
		this.props.updateFormState(this.props.form, {
			currencyexchangerate : null
		});
	}
}

export function getProjectEstimationitemDetails(array, resourcename, projectitems) {
	if(array.length == 0)
		return null;

	let queryString = '';
	if(resourcename == 'Projects')
		queryString = `/api/projectestimationitems?field=boqitemsid,projectestimations/projectid/parentid&filtercondition=projectestimations_parentid.projectid=${this.props.resource.projectid} and projectestimationitems.boqitemsid is not null and projectestimationitems.activitytypeid is null and projectestimations_parentid.status != 'Cancelled'`;
	if(resourcename == 'Project Quotes')
		queryString = `/api/projectestimationitems?field=boqquoteitemsid,projectestimations/projectquoteid/parentid&filtercondition=projectestimations_parentid.projectquoteid=${this.props.resource.projectquoteid} and projectestimationitems.boqquoteitemsid is not null and projectestimationitems.activitytypeid is null and projectestimations_parentid.status != 'Cancelled'`;

	return axios.get(`${queryString}`).then((response)=> {
		if (response.data.message == 'success') {
			let estimationitems = [];

			if(resourcename == 'Projects') {
				for (var i = 0; i < projectitems.length; i++) {
					if(projectitems[i].itemtype == 'Item') {
						let itemFound = false;
						for(var j=0;j<response.data.main.length;j++) {
							if(response.data.main[j].boqitemsid == projectitems[i].id) {
								itemFound = true;
								break;
							}
						}
						if(!itemFound) {
							let tempChiObj = {
								index: i+1,
								estimationtype : 'Simple',
								isparent: true
							};

							utils.assign(tempChiObj, projectitems[i], [{'boqitemsid' : 'id'}, {'boqitemid' : 'itemid'}, 'itemid','itemid_name', 'itemid_itemtype', {'boqitemid_name' : 'itemid_name'}, {'boqitemid_itemtype' : 'itemid_itemtype'}, {'boqitemid_usebillinguom' : 'itemid_usebillinguom'}, {'boqitemid_issaleskit' : 'itemid_issaleskit'}, {'boqitemsid_specification' : 'specification'}, 'specification', {'boqitemsid_description' : 'description'}, 'description', {'boqitemsid_quantity' : 'quantity'}, {'boqitemsid_estimatedqty' : 'quantity'}, 'quantity', {'boqitemsid_uomid' : 'uomid'}, 'uomid', 'uomid_name', {'boqitemsid_internalrefno' : 'internalrefno'}, {'boqitemsid_clientrefno' : 'clientrefno'}, 'uomconversionfactor', 'uomconversiontype', {'boqitemsid_uomconversionfactor' : 'uomconversionfactor'}, {'boqitemsid_uomconversiontype' : 'uomconversiontype'}, {'boqitemid_defaultpurchasecost' : 'itemid_defaultpurchasecost'}, {'boqitemid_lastpurchasecost' : 'itemid_lastpurchasecost'}, 'billinguomid', 'usebillinguom','billingconversionfactor','billingconversiontype','billingquantity',{'billingrate' : 0},'approvedmakes']);

							this.customFieldsOperation('projectitems', tempChiObj, projectitems[i], 'estimationitems');
							estimationitems.push(tempChiObj);
						}
					}
				}
			}

			if(resourcename == 'Project Quotes') {
				for (var i = 0; i < projectitems.length; i++) {
					if(projectitems[i].itemtype == 'Item') {
						let itemFound = false;
						for(var j = 0; j < response.data.main.length; j++) {
							if(response.data.main[j].boqquoteitemsid == projectitems[i].id) {
								itemFound = true;
								break;
							}
						}
						if(!itemFound) {
							let tempChiObj = {
								index: i+1,
								estimationtype : 'Simple',
								isparent: true
							};

							utils.assign(tempChiObj, projectitems[i], [{'boqquoteitemsid' : 'id'}, {'boqitemid' : 'itemid'}, 'itemid','itemid_name', 'itemid_itemtype', {'boqitemid_name' : 'itemid_name'}, {'boqitemid_itemtype' : 'itemid_itemtype'}, {'boqitemid_usebillinguom' : 'itemid_usebillinguom'}, {'boqitemid_issaleskit' : 'itemid_issaleskit'}, {'boqquoteitemsid_specification' : 'specification'}, 'specification', {'boqquoteitemsid_description' : 'description'}, 'description',  {'boqquoteitemsid_israteonly' : 'israteonly'}, {'boqquoteitemsid_quantity' : 'quantity'}, {'boqquoteitemsid_estimatedqty' : 'quantity'}, 'quantity', {'boqquoteitemsid_uomid' : 'uomid'}, 'uomid', 'uomid_name', {'boqquoteitemsid_internalrefno' : 'internalrefno'}, {'boqquoteitemsid_clientrefno' : 'clientrefno'}, 'uomconversionfactor', 'uomconversiontype', {'boqquoteitemsid_uomconversionfactor' : 'uomconversionfactor'}, {'boqquoteitemsid_uomconversiontype' : 'uomconversiontype'}, {'boqitemid_defaultpurchasecost' : 'itemid_defaultpurchasecost'}, {'boqitemid_lastpurchasecost' : 'itemid_lastpurchasecost'}, 'billinguomid', 'usebillinguom','billingconversionfactor','billingconversiontype','billingquantity',{'billingrate' : 0},'approvedmakes']);

							this.customFieldsOperation('projectquoteitems', tempChiObj, projectitems[i], 'estimationitems');
							estimationitems.push(tempChiObj);
						}
					}
				}
			}

			estimationitems.push({
				isoverhead: true,
				internalrefno: 'Overhead',
				rate: 0
			});

			this.props.updateFormState(this.props.form, {
				estimationitems
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function callBackCustomerName (id, valueobj) {
	let tempObj = {
		projectquoteid: null,
		projectquoteid_quoteno: null,
		estimationitems: [{
			isoverhead: true,
			internalrefno: 'Overhead',
			rate: 0
		}],
		salesperson : this.props.app.user.issalesperson ? (this.props.resource.salesperson ? this.props.resource.salesperson : this.props.app.user.id) : valueobj.salesperson,
	};

	this.props.updateFormState(this.props.form, tempObj);
}

export function callBackProjectQuote (id, valueobj) {
	this.props.updateFormState(this.props.form, {
		projectquoteid_quoteno : valueobj.quoteno,
		salesperson : valueobj.salesperson,
		estimationitems: [{
			isoverhead: true,
			internalrefno: 'Overhead',
			rate: 0
		}],
		projectestimationitems : []
	});

	this.updateLoaderFlag(true);
	this.controller.getBOQItemDetails();
}

export function callBackProject(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		projectname : valueobj.projectname,
		salesperson : valueobj.salesperson,
		estimationitems: [{
			isoverhead: true,
			internalrefno: 'Overhead',
			rate: 0
		}],
		projectestimationitems : []
	});

	this.updateLoaderFlag(true);
	this.controller.getBOQItemDetails();
}

export function estimationforOnchange() {
	this.props.updateFormState(this.props.form, {
		projectquoteid : null,
		projectid : null,
		projectname: null,
		projectestimationitems : [],
		estimationitems : [{
			isoverhead: true,
			internalrefno: 'Overhead',
			rate: 0
		}]
	});
}

export function getBOQItemDetails() {
	let queryString = this.props.resource.projectquoteid ? `/api/query/getprojectitemdetailsquery?projectquoteid=${this.props.resource.projectquoteid}` : `/api/query/getprojectitemdetailsquery?projectid=${this.props.resource.projectid}`

	return axios.get(`${queryString}`).then((response)=> {
		if (response.data.message == 'success') {
			let estimationitemArr = [], index = 0;
			response.data.main.forEach((item) => {
				let tempObj = {
					index: (index+1),
					estimationtype : 'Simple',
					isparent: true
				};
				if(this.props.resource.projectquoteid)
					utils.assign(tempObj, item, [{'boqquoteitemsid' : 'id'}, {'boqitemid' : 'itemid'}, 'itemid', {'boqitemid_name' : 'itemid_name'}, {'boqitemid_itemtype' : 'itemid_itemtype'}, {'boqitemid_usebillinguom' : 'itemid_usebillinguom'}, {'boqitemid_issaleskit' : 'itemid_issaleskit'}, {'boqquoteitemsid_specification' : 'specification'}, 'specification', {'boqquoteitemsid_description' : 'description'}, 'description', {'boqquoteitemsid_israteonly' : 'israteonly'}, {'boqquoteitemsid_quantity' : 'quantity'}, {'boqquoteitemsid_estimatedqty' : 'quantity'}, 'quantity', {'boqquoteitemsid_uomid' : 'uomid'}, 'uomid', 'uomid_name', {'boqquoteitemsid_internalrefno' : 'internalrefno'}, {'boqquoteitemsid_clientrefno' : 'clientrefno'}, 'uomconversionfactor', 'uomconversiontype', {'boqquoteitemsid_uomconversionfactor' : 'uomconversionfactor'}, {'boqquoteitemsid_uomconversiontype' : 'uomconversiontype'}, {'boqitemid_defaultpurchasecost' : 'itemid_defaultpurchasecost'}, {'boqitemid_lastpurchasecost' : 'itemid_lastpurchasecost'}, 'billinguomid', 'usebillinguom','billingconversionfactor','billingconversiontype','billingquantity',{'billingrate' : 0}]);

				if(this.props.resource.projectid)
					utils.assign(tempObj, item, [{'boqitemsid' : 'id'}, {'boqitemid' : 'itemid'}, 'itemid', {'boqitemid_name' : 'itemid_name'}, {'boqitemid_itemtype' : 'itemid_itemtype'}, {'boqitemid_usebillinguom' : 'itemid_usebillinguom'}, {'boqitemid_issaleskit' : 'itemid_issaleskit'}, {'boqitemsid_specification' : 'specification'}, 'specification', {'boqitemsid_description' : 'description'}, 'description', {'boqitemsid_quantity' : 'quantity'}, {'boqitemsid_estimatedqty' : 'quantity'}, 'quantity', {'boqitemsid_uomid' : 'uomid'}, 'uomid', 'uomid_name', {'boqitemsid_internalrefno' : 'internalrefno'}, {'boqitemsid_clientrefno' : 'clientrefno'}, 'uomconversionfactor', 'uomconversiontype', {'boqitemsid_uomconversionfactor' : 'uomconversionfactor'}, {'boqitemsid_uomconversiontype' : 'uomconversiontype'}, {'boqitemid_defaultpurchasecost' : 'itemid_defaultpurchasecost'}, {'boqitemid_lastpurchasecost' : 'itemid_lastpurchasecost'}, 'billinguomid', 'usebillinguom','billingconversionfactor','billingconversiontype','billingquantity',{'billingrate' : 0}]);

				this.customFieldsOperation('projectquoteitems', tempObj, item, 'projectestimationitems');

				estimationitemArr.push(tempObj);
			});

			estimationitemArr.push({
				isoverhead: true,
				internalrefno: 'Overhead',
				rate: 0
			});

			this.props.updateFormState(this.props.form, {estimationitems: estimationitemArr});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function activitytypeChange(value, valueobj, item, itemstr, parentitem, parentitemstr) {
	let tempchilditem = this.selector(this.props.fullstate, parentitemstr);
	let tempObj = {};

	tempObj[`${itemstr}.activitytypeid_name`] = valueobj.name;

	if(tempchilditem && Object.keys(tempchilditem).length > 0) {
		tempObj[`${itemstr}.boqitemid`] = tempchilditem.boqitemid;
		tempObj[`${itemstr}.boqitemid_name`] = tempchilditem.boqitemid_name;
		if(tempchilditem.boqquoteitemsid) {
			tempObj[`${itemstr}.boqquoteitemsid`] = tempchilditem.boqquoteitemsid;
		} else {
			tempObj[`${itemstr}.boqitemsid`] = tempchilditem.boqitemsid;
		}
	}
	this.props.updateFormState(this.props.form, tempObj);
	this.controller.computeFinalRate();
}

export function itemcallback(value, valueobj, item, itemstr, parentitem, parentitemstr) {
	this.controller.getItemDetails(valueobj, item, itemstr, parentitem, parentitemstr);
}

export function getItemDetails(valueobj, item, itemstr, parentitem, parentitemstr) {
	let tempchilditem = this.selector(this.props.fullstate, parentitemstr);
	
	let promise = commonMethods.getItemDetails(valueobj.id, this.props.resource.customerid, null, 'sales');
	promise.then((returnObject) => {
		let tempObj = {};
		tempObj[`${itemstr}.itemid_name`] = returnObject.itemid_name;
		tempObj[`${itemstr}.itemid_itemtype`] = returnObject.itemid_itemtype;
		tempObj[`${itemstr}.description`] = returnObject.description;
		tempObj[`${itemstr}.itemid_issaleskit`] = returnObject.itemid_issaleskit;
		tempObj[`${itemstr}.quantity`] = item.quantity ? item.quantity : 1;
		tempObj[`${itemstr}.uomid`] = returnObject.uomid;
		tempObj[`${itemstr}.uomid_name`] = returnObject.uomid_name;
		tempObj[`${itemstr}.alternateuom`] = returnObject.alternateuom;
		tempObj[`${itemstr}.itemid_uomgroupid`] = returnObject.uomgroupid;
		tempObj[`${itemstr}.itemid_lastpurchasecost`] = returnObject.itemid_lastpurchasecost;
		tempObj[`${itemstr}.itemid_defaultpurchasecost`] = valueobj.defaultpurchasecost;
		tempObj[`${itemstr}.itemid_isuseforinternallabour`] = returnObject.itemid_isuseforinternallabour;

		this.customFieldsOperation('itemmaster', tempObj, returnObject, 'projectestimationitems', itemstr);

		if(tempchilditem) {
			tempObj[`${itemstr}.boqitemid`] = tempchilditem.boqitemid;
			tempObj[`${itemstr}.boqitemid_name`] = tempchilditem.boqitemid_name;
			tempObj[`${itemstr}.boqitemid_itemtype`] = tempchilditem.boqitemid_itemtype;
			tempObj[`${itemstr}.boqitemid_defaultpurchasecost`] = tempchilditem.boqitemid_defaultpurchasecost;
			tempObj[`${itemstr}.boqitemid_lastpurchasecost`] = tempchilditem.boqitemid_lastpurchasecost;
			tempObj[`${itemstr}.boqitemid_usebillinguom`] = tempchilditem.boqitemid_usebillinguom;
			tempObj[`${itemstr}.boqitemid_issaleskit`] = tempchilditem.boqitemid_issaleskit;

			if(tempchilditem.boqquoteitemsid) {
				tempObj[`${itemstr}.boqquoteitemsid`] = tempchilditem.boqquoteitemsid;
				tempObj[`${itemstr}.boqquoteitemsid_quantity`] = tempchilditem.boqquoteitemsid_quantity;
				tempObj[`${itemstr}.boqquoteitemsid_estimatedqty`] = tempchilditem.boqquoteitemsid_estimatedqty;
				tempObj[`${itemstr}.boqquoteitemsid_description`] = tempchilditem.boqquoteitemsid_description;
				tempObj[`${itemstr}.boqquoteitemsid_uomid`] = tempchilditem.boqquoteitemsid_uomid;
				tempObj[`${itemstr}.boqquoteitemsid_clientrefno`] = tempchilditem.boqquoteitemsid_clientrefno;
				tempObj[`${itemstr}.boqquoteitemsid_internalrefno`] = tempchilditem.boqquoteitemsid_internalrefno;
				tempObj[`${itemstr}.boqquoteitemsid_israteonly`] = tempchilditem.boqquoteitemsid_israteonly;
			} else {
				tempObj[`${itemstr}.boqitemsid`] = tempchilditem.boqitemsid;
				tempObj[`${itemstr}.boqitemsid_quantity`] = tempchilditem.boqitemsid_quantity;
				tempObj[`${itemstr}.boqitemsid_estimatedqty`] = tempchilditem.boqitemsid_estimatedqty;
				tempObj[`${itemstr}.boqitemsid_description`] = tempchilditem.boqitemsid_description;
				tempObj[`${itemstr}.boqitemsid_uomid`] = tempchilditem.boqitemsid_uomid;
				tempObj[`${itemstr}.boqitemsid_clientrefno`] = tempchilditem.boqitemsid_clientrefno;
				tempObj[`${itemstr}.boqitemsid_internalrefno`] = tempchilditem.boqitemsid_internalrefno;
			}
		}

		if (returnObject.alternateuom) {
			tempObj[`${itemstr}.uomconversionfactor`] = returnObject.uomconversionfactor;
			tempObj[`${itemstr}.uomconversiontype`] = returnObject.uomconversiontype;
		} else {
			tempObj[`${itemstr}.uomconversionfactor`] = null;
			tempObj[`${itemstr}.uomconversiontype`] = null;
		}

		tempObj[`${itemstr}.itemid_usebillinguom`] = returnObject.itemid_usebillinguom;
		tempObj[`${itemstr}.usebillinguom`] = returnObject.usebillinguom;
		tempObj[`${itemstr}.billinguomid`] = returnObject.billinguomid;
		tempObj[`${itemstr}.billingconversiontype`] = returnObject.billingconversiontype;
		tempObj[`${itemstr}.billingconversionfactor`] = returnObject.billingconversionfactor;

		this.props.updateFormState(this.props.form, tempObj);
		let tempchilditem1 = this.selector(this.props.fullstate, itemstr);
		this.controller.quantityOnChange(tempchilditem1.quantity, tempchilditem1, itemstr, parentitem, parentitemstr);
	}, (reason) => {});
}

export function quantityOnChange(itemqty, item, itemstr, parentitem, parentitemstr) {
	let tempObj = {};
	if(item.usebillinguom) {
		let tempchilditem = this.selector(this.props.fullstate, itemstr);
		tempObj[`${itemstr}.billingquantity`] = Number(((tempchilditem.quantity / (tempchilditem.uomconversionfactor ? tempchilditem.uomconversionfactor : 1)) * tempchilditem.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		this.props.updateFormState(this.props.form, tempObj);
	}
	this.controller.computeFinalRate();
}

export function uomOnchange (id, valueobj, itemstr, parentitem, parentitemstr, type) {
	let tempObj = {};

	let name_conversiontype = type == 'billing' ? 'billingconversiontype' : 'uomconversiontype';
	let name_conversionfactor = type == 'billing' ? 'billingconversionfactor' : 'uomconversionfactor';

	tempObj[`${itemstr}.${name_conversiontype}`] = valueobj.alternateuom ? valueobj.conversiontype : (type == 'billing' ? 'Fixed' : null);
	tempObj[`${itemstr}.${name_conversionfactor}`] = valueobj.alternateuom ? valueobj.conversionrate : (type == 'billing' ? 1 : null);

	this.props.updateFormState(this.props.form, tempObj);

	let itemobj = this.selector(this.props.fullstate, itemstr);

	let promise1 = commonMethods.getItemDetails(itemobj.itemid, this.props.resource.customerid, null, 'sales', itemobj.usebillinguom ? itemobj.billinguomid : itemobj.uomid, itemobj.usebillinguom ? true : valueobj.alternateuom, itemobj.usebillinguom ? itemobj.billingconversionfactor : itemobj.uomconversionfactor);

	promise1.then((returnObject) => {
		let rateUpdateObj = {};

		rateUpdateObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app);

		if(itemobj.usebillinguom) {
			rateUpdateObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
			rateUpdateObj[`${itemstr}.rate`] = Number(((rateUpdateObj[`${itemstr}.billingrate`] / (itemobj.uomconversionfactor ? itemobj.uomconversionfactor : 1)) * itemobj.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			rateUpdateObj[`${itemstr}.billingquantity`] = Number(((itemobj.quantity / (itemobj.uomconversionfactor ? itemobj.uomconversionfactor : 1)) * itemobj.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
		}

		this.props.updateFormState(this.props.form, rateUpdateObj);

		this.controller.computeFinalRate();
	}, (reason) => {});
}

export function conversionfactoronchange (value, itemstr, parentitem, parentitemstr, type) {
	let itemobj = this.selector(this.props.fullstate, itemstr);
	let tempObj = {};
	if (value > 0) {
		let promise1 = commonMethods.getItemDetails(itemobj.itemid, this.props.resource.customerid, null, 'sales', itemobj.usebillinguom ? itemobj.billinguomid : itemobj.uomid, itemobj.usebillinguom ? true : itemobj.uomconversiontype ? true : false, itemobj.usebillinguom ? itemobj.billingconversionfactor : itemobj.uomconversionfactor);
		promise1.then((returnObject) => {
			tempObj[`${itemstr}.rate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app);

			if(itemobj.usebillinguom) {
				tempObj[`${itemstr}.billingrate`] = commonMethods.getRate(returnObject.rate, this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app);
				tempObj[`${itemstr}.rate`] = Number(((tempObj[`${itemstr}.billingrate`] / (itemobj.uomconversionfactor ? itemobj.uomconversionfactor : 1)) * itemobj.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				tempObj[`${itemstr}.billingquantity`] = Number(((itemobj.quantity / (itemobj.uomconversionfactor ? itemobj.uomconversionfactor : 1)) * itemobj.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
			}

			this.props.updateFormState(this.props.form, tempObj);
			this.controller.computeFinalRate();
		}, (reason) => {});
	} else {
		tempObj[`${itemstr}.rate`] = 0;
		if(itemobj.usebillinguom)
			tempObj[`${itemstr}.billingrate`] = 0;
		this.props.updateFormState(this.props.form, tempObj);
		this.controller.computeFinalRate();
	}
}

export function estimationBillingRateOnChange(itemstr) {
	let item = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function billingRateOnChange(itemstr, parentitem, parentitemstr) {
	let itemobj = this.selector(this.props.fullstate, itemstr);
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.rate`]: Number(((itemobj.billingrate / (itemobj.uomconversionfactor ? itemobj.uomconversionfactor : 1)) * itemobj.billingconversionfactor).toFixed(this.props.app.roundOffPrecision))
	});
	this.controller.computeFinalRate();
}

export function computeFinalRate() {
	setTimeout(() => {
		let tempObj = {};

		let itemprop = this.props.resource.projectquoteid ? 'boqquoteitemsid' : 'boqitemsid';

		this.props.resource.estimationitems.forEach((itemobj, itemindex) => {
			let estimatedcost = 0, overheadestimatedrate = 0;
			let itemqtyprop = itemobj.boqitemsid ? 'boqitemsid_estimatedqty' : 'boqquoteitemsid_estimatedqty';
			let itemqty = itemobj.boqitemsid ? itemobj.boqitemsid_estimatedqty : itemobj.boqquoteitemsid_estimatedqty;

			let estItemStr = `estimationitems[${itemindex}]`;

			if(itemobj.estimationtype == 'Detailed') {
				this.props.resource.estimationitems.forEach((childitemobj, childitemindex) => {
					if(itemindex != childitemindex && itemobj[itemprop] == childitemobj[itemprop]) {

						tempObj[`estimationitems[${childitemindex}].${itemqtyprop}`] = itemqty;

						tempObj[`estimationitems[${childitemindex}].amount`] = Number((childitemobj.quantity * (childitemobj.rate || 0)).toFixed(this.props.app.roundOffPrecision));
						estimatedcost += tempObj[`estimationitems[${childitemindex}].amount`];
					}
				});

				if(itemobj.usebillinguom) {
					tempObj[`${estItemStr}.billingrate`] = Number((((estimatedcost/itemqty) / (itemobj.billingconversionfactor)) * (itemobj.uomconversionfactor ? itemobj.uomconversionfactor : 1)).toFixed(this.props.app.roundOffPrecision));

					tempObj[`${estItemStr}.rate`] = Number(((tempObj[`${estItemStr}.billingrate`] / (itemobj.uomconversionfactor ? itemobj.uomconversionfactor : 1)) * itemobj.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				} else {
					tempObj[`${estItemStr}.rate`] = Number((estimatedcost/itemqty).toFixed(this.props.app.roundOffPrecision));
				}

				tempObj[`${estItemStr}.amount`] = Number((tempObj[`${estItemStr}.rate`] * itemqty).toFixed(this.props.app.roundOffPrecision));
			}

			if(itemobj.estimationtype == 'Simple') {
				tempObj[`${estItemStr}.amount`] = Number((itemobj.rate * itemobj.quantity).toFixed(this.props.app.roundOffPrecision));
			}

			if(itemobj.isoverhead) {
				this.props.resource.estimationitems.forEach((overheaditemobj, overheaditemindex) => {
					if(itemindex != overheaditemindex && !overheaditemobj[itemprop]) {

						tempObj[`estimationitems[${overheaditemindex}].internalrefno`] = 'Overhead';
						tempObj[`estimationitems[${overheaditemindex}].amount`] = Number((overheaditemobj.quantity * (overheaditemobj.rate || 0)).toFixed(this.props.app.roundOffPrecision));
						overheadestimatedrate += overheaditemobj.rate || 0;
					}
				});

				tempObj[`${estItemStr}.rate`] = Number((overheadestimatedrate).toFixed(this.props.app.roundOffPrecision));
			}
		});

		this.props.updateFormState(this.props.form, tempObj);
	}, 0);
}

export function estimationtypeonChange(item, itemstr, index, importparam) {
	this.updateLoaderFlag(true);
	let updateItemObj = [];
	let estimationitems = [...this.props.resource.estimationitems];
	let itemprop = this.props.resource.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';
	let tempchilditem = this.selector(this.props.fullstate, itemstr);

	if(tempchilditem.estimationtype == 'Simple') {
		let tempObj = {
			isparent: false
		};
		tempObj.itemid = tempchilditem.boqitemid;
		tempObj.itemid_name = tempchilditem.boqitemid_name;
		tempObj.description = tempchilditem.description;
		tempObj.itemid_issaleskit = tempchilditem.boqitemid_issaleskit;
		tempObj.itemid_itemtype = tempchilditem.boqitemid_itemtype;

		tempObj.rate = tempchilditem.rate ? (tempchilditem.rate) : null;

		tempObj.quantity = tempchilditem.quantity ? tempchilditem.quantity : 1;
		tempObj.uomid = tempchilditem.uomid;
		tempObj.uomid_name = tempchilditem.uomid_name;
		tempObj.uomconversionfactor = tempchilditem.uomconversionfactor;
		tempObj.uomconversiontype = tempchilditem.uomconversiontype;
		tempObj.itemid_lastpurchasecost = tempchilditem.boqitemid_lastpurchasecost;
		tempObj.itemid_defaultpurchasecost = tempchilditem.boqitemid_defaultpurchasecost;
		tempObj.amount = tempObj.rate * tempObj.quantity;

		tempObj.boqitemid = tempchilditem.boqitemid;
		tempObj.boqitemid_name = tempchilditem.boqitemid_name;
		tempObj.boqitemid_itemtype = tempchilditem.boqitemid_itemtype;
		tempObj.boqitemid_defaultpurchasecost = tempchilditem.boqitemid_defaultpurchasecost;
		tempObj.boqitemid_lastpurchasecost = tempchilditem.boqitemid_lastpurchasecost;
		tempObj.specification = tempchilditem.specification;
		tempObj.approvedmakes = tempchilditem.approvedmakes;

		if(tempchilditem.boqquoteitemsid) {
			tempObj.boqquoteitemsid = tempchilditem.boqquoteitemsid;
			tempObj.boqquoteitemsid_quantity = tempchilditem.boqquoteitemsid_quantity;
			tempObj.boqquoteitemsid_estimatedqty = tempchilditem.boqquoteitemsid_estimatedqty;
			tempObj.boqquoteitemsid_description = tempchilditem.boqquoteitemsid_description;
			tempObj.boqquoteitemsid_uomid = tempchilditem.boqquoteitemsid_uomid;
			tempObj.boqquoteitemsid_clientrefno = tempchilditem.boqquoteitemsid_clientrefno;
			tempObj.boqquoteitemsid_internalrefno = tempchilditem.boqquoteitemsid_internalrefno;
			tempObj.boqquoteitemsid_israteonly = tempchilditem.boqquoteitemsid_israteonly;
			this.customFieldsOperation('projectquoteitems', tempObj, tempchilditem, 'projectestimationitems');
		} else {
			tempObj.boqitemsid = tempchilditem.boqitemsid;
			tempObj.boqitemsid_quantity = tempchilditem.boqitemsid_quantity;
			tempObj.boqitemsid_estimatedqty = tempchilditem.boqitemsid_estimatedqty;
			tempObj.boqitemsid_description = tempchilditem.boqitemsid_description;
			tempObj.boqitemsid_uomid = tempchilditem.boqitemsid_uomid;
			tempObj.boqitemsid_clientrefno = tempchilditem.boqitemsid_clientrefno;
			tempObj.boqitemsid_internalrefno = tempchilditem.boqitemsid_internalrefno;
			this.customFieldsOperation('projectitems', tempObj, tempchilditem, 'projectestimationitems');
		}

		if(tempchilditem.boqitemid_itemtype == 'Product') {
			tempObj.itemid_usebillinguom = tempchilditem.boqitemid_usebillinguom;
			tempObj.usebillinguom = tempchilditem.usebillinguom;
			tempObj.billinguomid = tempchilditem.billinguomid;
			tempObj.billingconversiontype = tempchilditem.billingconversiontype;
			tempObj.billingconversionfactor = tempchilditem.billingconversionfactor;

			if(tempObj.usebillinguom) {
				tempObj.billingquantity = Number(((tempObj.quantity / (tempObj.uomconversionfactor ? tempObj.uomconversionfactor : 1)) * tempObj.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
				tempObj.billingrate = commonMethods.getRate(tempchilditem.usebillinguom ? (tempchilditem.billingrate || 0) : (tempchilditem.rate || 0), this.props.resource.currencyid, this.props.resource.currencyexchangerate, null, this.props.app);
				tempObj.rate = Number(((tempObj.billingrate / (tempObj.uomconversionfactor ? tempObj.uomconversionfactor : 1)) * tempObj.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
			}
		}

		estimationitems[index].rate = tempObj.rate;
		estimationitems[index].amount = 0;
		estimationitems[index].estimationtype = 'Detailed';
		estimationitems[index].isparent = true;
		estimationitems.splice(index+1, 0, tempObj);

		this.props.updateFormState(this.props.form, {
			estimationitems
		});
		setTimeout(() => {
			this.controller.computeFinalRate();
		}, 0);
	} else {
		let itemcount = 0;
		estimationitems.forEach((estitem, estindex) => {
			if(estitem[itemprop] == tempchilditem[itemprop] && estindex != index) {
				itemcount++;
			}
		});
		let confirmCallback = (param) => {
			if(param) {
				estimationitems[index].estimationtype = 'Simple';

				for (var i = 0; i < itemcount; i++) {
					for (var j = 0; j < estimationitems.length; j++) {
						if (estimationitems[j][itemprop] == tempchilditem[itemprop] && j != index) {
							estimationitems.splice(j, 1);
						}
					}
				}

				this.props.updateFormState(this.props.form, {
					estimationitems
				});
			}
		};
		if(!importparam && itemcount > 0) {
			this.props.openModal(modalService['confirmMethod']({
				header : 'Confirm',
				body : 'If you disabled the detailed estimation, estimation items added for this BOQ item will be no longer available. Would you like to continue?',
				btnArray : ['Yes','No']
			}, confirmCallback));
		} else {
			confirmCallback(true);
		}
	}
	this.updateLoaderFlag(false);
}

export function addEstimationDetailedItem(index) {
	let tempchilditem = this.selector(this.props.fullstate, `estimationitems[${index}]`);
	let itemprop = this.props.resource.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';
	let estimationitems = [...this.props.resource.estimationitems];
	let lastItemindex = tempchilditem.isoverhead ? estimationitems.length - 1 : 0;
	let tempObj = {};

	for(var i=0; i<estimationitems.length; i++) {
		if(estimationitems[i][itemprop] == tempchilditem[itemprop]) {
			lastItemindex = i;
		}
	}

	if(tempchilditem[itemprop]) {
		tempObj.boqitemid = tempchilditem.boqitemid;
		tempObj.boqitemid_name = tempchilditem.boqitemid_name;
		tempObj.boqitemid_itemtype = tempchilditem.boqitemid_itemtype;
		tempObj.boqitemid_defaultpurchasecost = tempchilditem.boqitemid_defaultpurchasecost;
		tempObj.boqitemid_lastpurchasecost = tempchilditem.boqitemid_lastpurchasecost;
		tempObj.boqitemid_usebillinguom = tempchilditem.boqitemid_usebillinguom;
		tempObj.boqitemid_issaleskit = tempchilditem.boqitemid_issaleskit;

		tempObj[itemprop] = tempchilditem[itemprop];
		tempObj[`${itemprop}_quantity`] = tempchilditem[`${itemprop}_quantity`];
		tempObj[`${itemprop}_estimatedqty`] = tempchilditem[`${itemprop}_estimatedqty`];
		tempObj[`${itemprop}_description`] = tempchilditem[`${itemprop}_description`];
		tempObj[`${itemprop}_uomid`] = tempchilditem[`${itemprop}_uomid`];
		tempObj[`${itemprop}_clientrefno`] = tempchilditem[`${itemprop}_clientrefno`];
		tempObj[`${itemprop}_internalrefno`] = tempchilditem[`${itemprop}_internalrefno`];
	} else {
		tempObj.internalrefno = 'Overhead';
	}

	estimationitems.splice(lastItemindex+1, 0, tempObj);

	this.props.updateFormState(this.props.form, {
		estimationitems
	});
}

export function deleteEstimationItem(index) {
	this.updateLoaderFlag(true);
	let estimationitems = [...this.props.resource.estimationitems];
	let estimationitemid = estimationitems[index].id;
	let itemprop = this.props.resource.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';
	let deleteitemcount = 0;
	let estimationarr = [];

	if(estimationitems[index].isoverhead) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService['infoMethod']({
			header : "Warning",
			body : "This row is can't be deleted",
			btnArray : ["Ok"]
		}));
	}

	if(estimationitems[index].estimationtype == 'Detailed') {
		estimationitems.forEach((item, itemindex) => {
			if(item[itemprop] == estimationitems[index][itemprop]) {
				deleteitemcount++;
				if(item.id > 0)
					estimationarr.push(item.id);
			}
		});
	} else {
		estimationarr.push(estimationitemid);
	}

	if(!Number(estimationitemid) > 0) {
		if(estimationitems[index].estimationtype == 'Detailed') {
			for (var i = 0; i < deleteitemcount; i++) {
				for (var j = 0; j < estimationitems.length; j++) {
					if (estimationitems[j][itemprop] == estimationitems[index][itemprop]) {
						estimationitems.splice(j, 1);
						break;
					}
				}
			}
		} else {
			estimationitems.splice(index, 1);
		}

		this.props.updateFormState(this.props.form, {
			estimationitems
		});
		setTimeout(this.controller.computeFinalRate(), 0);
		this.updateLoaderFlag(false);
	} else {

		axios.get(`/api/query/projectestimationitemdeletecheckquery?projectestimationitemsid=${estimationarr}`).then((response)=> {
			if(response.data.main && Object.keys(response.data.main).length > 0) {
				let temperrArr = [];
				for(var prop in response.data.main) {
					temperrArr.push(...response.data.main[prop]);
				}
				response.data.error = temperrArr;
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			} else {
				if(estimationitems[index].estimationtype == 'Detailed') {
					for (var i = 0; i < deleteitemcount; i++) {
						for (var j = 0; j < estimationitems.length; j++) {
							if (estimationitems[j][itemprop] == estimationitems[index][itemprop]) {
								estimationitems.splice(j, 1);
								break;
							}
						}
					}
				} else {
					estimationitems.splice(index, 1);
				}

				this.props.updateFormState(this.props.form, {
					estimationitems
				});
				setTimeout(this.controller.computeFinalRate(), 0);
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function checkBOQQuantity(tempEstObj, confirm) {
	let uniqueArray = [], errorArr = [];
	let itemprop = this.props.resource.projectid ? 'boqitemsid' : 'boqquoteitemsid';
	let resourcename = this.props.resource.projectid ? 'projectitems' : 'projectquoteitems';
	let idprop = this.props.resource.projectid ? 'projectid' : 'projectquoteid'

	tempEstObj.projectestimationitems.forEach(item => {
		if(item[itemprop] > 0 && !uniqueArray.includes(item[itemprop]))
			uniqueArray.push(item[itemprop]);
	});

	if(uniqueArray.length == 0) {
		return this.controller.save('Approve', confirm, true);
	}

	axios.get(`/api/${resourcename}?field=id,internalrefno,itemmaster/name/itemid,description,quantity,estimatedqty,uom/name/uomid&filtercondition=${resourcename}.parentid=${this.props.resource[idprop]} AND ${resourcename}.id IN  (${uniqueArray})`).then((response) => {
		if(response.data.message != 'success') {
			this.updateLoaderFlag(false);
			let apiResponse = commonMethods.apiResult(response);
			return this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		if(response.data.main.length == 0) {
			return this.controller.save('Approve', confirm, true);
		}

		response.data.main.forEach(item => {
			if(item.quantity != item.estimatedqty) {
				errorArr.push(item);
			}
		});

		if(errorArr.length == 0) {
			return this.controller.save('Approve', confirm, true);
		}

		return this.props.openModal({
			render: (closeModal) => {
				return <ErrorAlertModal errors = {errorArr} closeModal = {closeModal} 
					callback = {(param) => {
						this.updateLoaderFlag(false);
						if(param)
							return this.controller.save('Approve', confirm, true);
					}}/>
			}, className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	});
}

export function save (param, confirm, approveparam) {
	this.updateLoaderFlag(true);

	if(param != 'Cancel' && param != 'Delete' && param !='Amend' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	let tempEstObj = this.props.resource;
	tempEstObj.projectestimationitems = [];
	let itemprop = this.props.resource.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';

	tempEstObj.estimationitems.forEach((item) => {
		if((item[itemprop] && (!item.estimationtype || item.estimationtype == 'Simple')) || (!item[itemprop] && !item.isoverhead)) {
			tempEstObj.projectestimationitems.push(item);
		}
	});

	if(param == 'Approve' && this.props.resource.id && !approveparam)
		return this.controller.checkBOQQuantity(tempEstObj, confirm);

	setTimeout(() => {
		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : tempEstObj,
				ignoreExceptions: confirm ? true : false
			},
			url : '/api/projectestimations'
		}).then((response)=> {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam)=> {
				if (resparam)
					this.controller.save(param, true);
			}));

			if (response.data.message == 'success') {
				if(this.state.createParam)
					this.props.history.replace(`/details/projectestimations/${response.data.main.id}`);
				else if (param == 'Delete')
					this.props.history.replace("/list/projectestimations");
				else {
					this.controller.parseProjectEstimationItem(response.data.main);
				}
			}
			this.updateLoaderFlag(false);
		});
	}, 0);
}

export function getQuoteItemDetails () {
	this.props.openModal({render: (closeModal) => {
		return <ProjectQuoteitemAddmodal resource={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} computeFinalRate={this.controller.computeFinalRate} closeModal={closeModal} customFieldsOperation = {this.customFieldsOperation} />
	}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
}

export function boqGridEditView() {
	return this.props.openModal({
		render: (closeModal) => {
			return <BOQGridEditView 
				childjson={this.state.pagejson.estimationitems}
				resourcename={'estimationitems'}
				resource={this.props.resource}
				form={this.props.form}
				updateFormState={this.props.updateFormState}
				app={this.props.app}
				estimationtypeonChange={this.controller.estimationtypeonChange}
				computeFinalRate={this.controller.computeFinalRate}
				closeModal={closeModal}
				openModal={this.props.openModal}
				boqeditCB = {this.controller.editDetailedEstimation}
				/>
		}, className: {
			content: 'react-modal-custom-class-100',
			overlay: 'react-modal-overlay-custom-class'
		}, confirmModal: true
	});
}

export function editDetailedEstimation () {
	let internalrefFound = true;
	this.props.resource.estimationitems.every((item) => {
		if(!item.boqitemsid_internalrefno && !item.boqquoteitemsid_internalrefno && !item.internalrefno) {
			internalrefFound = false;
			return false;
		}
		return true;
	});
	if(!internalrefFound) {
		return this.props.openModal(modalService['infoMethod']({
			header : 'Warning',
			body : 'Internal Ref No is mandatory to edit Estimation Item in Grid View',
			btnArray : ['Ok']
		}));
	} else {
		this.controller.computeFinalRate();
	}
}

export function cancel () {
	this.props.history.goBack();
}


export class ErrorAlertModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.renderErrorTable = this.renderErrorTable.bind(this);
		this.cancel = this.cancel.bind(this);
	}

	renderErrorTable() {
		return this.props.errors.map((err, index) => {
			return(
				<tr key={index}>
					<td>{err.internalrefno}</td>
					<td>{err.itemid_name}</td>
					<td>{err.description}</td>
					<td className="text-center">{err.quantity} {err.uomid_name}</td>
					<td className="text-center">{err.estimatedqty} {err.uomid_name}</td>
				</tr>
			);
		})
	}

	cancel(param) {
		this.props.callback(param);
		this.props.closeModal();
	}

	render() {
		if(this.props.errors.length == 0)
			return null;

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title gs-text-color">Alert</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12 form-group text-danger">
							Quantity in BOQ has changes for the following items after they were added to the estimation. Have you made the necessary changes for the new quantities?
						</div>
						<div className="col-md-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th style={{width: '12%'}}>Internal Ref No</th>
										<th style={{width: '20%'}}>BOQ Item</th>
										<th>Description</th>
										<th style={{width: '12%'}}>BOQ Qty</th>
										<th style={{width: '20%'}}>Considered Qty for Estimation</th>
									</tr>
								</thead>
								<tbody>
									{this.renderErrorTable()}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={()=>this.cancel(false)}><i className="fa fa-times"></i>Cancel</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={()=>this.cancel(true)}><i className="fa fa-check"></i>Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}