import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';

export function onLoad () {
	this.setState({
		showrelatedtosection: this.props.location.params ? this.props.location.params.showrelatedtosection : true,
		disableinput: this.props.location.params ? this.props.location.params.disableinput : null
	});
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}


export function initialiseState() {
	let tempObj = {
		activityhistorydetails: [],
		nextactivity: {},
		reminder: null,
		parentresource: null,
		activitystatus: 'Completed'
	};

	if(this.props.location.params) {
		let params = this.props.location.params;
		tempObj.parentresource = params.parentresource;
		tempObj.parentid = params.parentid;
		tempObj.partnerid = params.partnerid;;

		tempObj.contactid = params.contactid;
		tempObj.leadid = params.leadid;
		tempObj.quoteid = params.quoteid;
		tempObj.projectquoteid = params.projectquoteid;

		if(params.param == 'Dialer') {
			tempObj.contactid = params.contactid;
			tempObj.contactperson = params.contactperson;
			tempObj.partnerid = params.customerid;
			tempObj.startdatetime = params.startdatetime;
			tempObj.enddatetime = params.enddatetime;
		}
	}

	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	let transactionObj = {
		'leads': 'Lead',
		'contacts': 'Contacts',
		'quotes': 'Quotation',
		'projectquotes': 'Project Quotation',
		'contractenquiries': 'Contract Enquiry'
	};

	axios.get(`/api/salesactivities/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.activityhistorydetails = [];
			tempObj.linkdisplayname = tempObj.parentresource ? transactionObj[tempObj.parentresource] : 'None';

			this.props.initialize(tempObj);
			this.controller.getActivities();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callBackPartner(id, valueobj) {
	this.props.updateFormState(this.props.form,  {
		contactid: valueobj.contactid,
		contactid_email: valueobj.contactid_email,
		contactid_mobile: valueobj.contactid_mobile,
		contactid_phone: valueobj.contactid_phone
	});
}

export function contactpersonChange (id, valueobj) {
	this.props.updateFormState(this.props.form,  {
		contactperson: valueobj.name,
		contactid_organization: valueobj.organization,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile
	});
}

export function parentresourceOnchange() {
	this.props.updateFormState(this.props.form, {
		parentid: null,
		partnerid: null,
		leadid: null,
		quoteid: null,
		projectquoteid: null,
		contactid: null
	});
}

export function callbackLead(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		leadid: id,
		partnerid: valueobj.customerid,
		contactid: valueobj.contactid,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile
	});
}

export function callbackContact(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		contactid: id,
		contactperson: valueobj.name,
		contactid_organization: valueobj.organization,
		partnerid: valueobj.parentresource == 'partners' ? valueobj.parentid : null,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile
	});
}

export function callbackQuoteno(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		quoteid: id,
		leadid: valueobj.leadid,
		partnerid: valueobj.customerid,
		contactid: valueobj.contactid,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile
	});
}

export function callbackProjectQuoteno(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		projectquoteid: id,
		leadid: valueobj.leadid,
		partnerid: valueobj.customerid,
		contactid: valueobj.contactid,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile
	});
}

export function callbackContractEnquiry(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		partnerid: valueobj.customerid,
		contactid: valueobj.contactid,
		contactid_phone: valueobj.phone,
		contactid_email: valueobj.email,
		contactid_mobile: valueobj.mobile
	});
}

export function activitytypeOnchange(id, valueobj) {
	this.props.updateFormState(this.props.form, {
		activitytypeid: id,
		activitytypeid_name: valueobj.name
	});
}

export function nextactivitytypeOnchange(id, valueobj) {
	let tempObj = {};
	tempObj[`nextactivity.activitytypeid`] = id;
	tempObj[`nextactivity.activitytypeid_name`] = valueobj.name;
	this.props.updateFormState(this.props.form, tempObj);
}

export function getActivities() {
	let { activityhistorydetails } = this.props.resource;
	let queryString = `/api/salesactivities?field=id,created,users/displayname/createdby,users/displayname/modifiedby,description,activitytypeid,startdatetime,enddatetime,activitystatus,reminder,notes,activitytypes/name/activitytypeid,activitytypes/icontext/activitytypeid&pagelength=3&skip=${this.props.resource.activityhistorydetails.length}&filtercondition=salesactivities.id!=${this.props.resource.id} and (salesactivities.parentresource='${this.props.resource.parentresource}' and salesactivities.parentid=${this.props.resource.parentid})`;

	axios.get(queryString).then((response)=> {
		if(response.data.message == 'success') {
			response.data.main.forEach((item) => {
				activityhistorydetails.push(item);
			});
			this.props.updateFormState(this.props.form, {
				activityhistorydetails
			});

			this.setState({
				showactivity: activityhistorydetails.length >= response.data.count ? false : true
			});
		}
	});
}

export function getOutComes(viewValue) {
	let filterStr = `leadoutcomes.name ilike '%${viewValue.replace(/'/g,"''")}%' and leadoutcomes.isdeleted=false`;
	filterStr = encodeURIComponent(filterStr);
	return axios.get(`/api/leadoutcomes?&field=id,name&filtercondition=${filterStr}`).then((response)=> {
		if (response.data.message == "success") {
			return response.data.main;
		} else {
			return [];
		}
	});
}

export function generateReleatedPageURL() {
	return `/details/${this.props.resource.parentresource}/${this.props.resource.parentid}`;
}

export function gotoRelatedPage() {
	if (this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.push(`/details/${this.props.resource.parentresource}/${this.props.resource.parentid}`);
}

export function showContactDetails() {
	this.props.createOrEdit('/createContact', null, {}, (valueobj) => {
		this.props.updateFormState(this.props.form,  {
			contactid: valueobj.id,
			contactperson: valueobj.name,
			contactid_organization: valueobj.organization,
			contactid_phone: valueobj.phone,
			contactid_email: valueobj.email,
			contactid_mobile: valueobj.mobile
		});
	});
}

export function save (param, confirm,dialerconfirm) {
	this.updateLoaderFlag(true);
	let { calldetails } = this.props.app;
	let calldetailObj = {};
	if(Object.keys(calldetails).length > 0) {
		Object.keys(calldetails).some((prop) => {
			if(calldetails[prop].isactive) {
				calldetailObj = calldetails[prop];
				return true;
			}
		});
	}

	if(this.props.app.feature.enableDialerIntegration && Object.keys(calldetailObj).length > 0 && !this.props.resource.callid && !dialerconfirm){
		return this.props.openModal(modalService['confirmMethod']({
			header : 'Alert',
			body : 'Would you like to link the activity with this call?',
			btnArray : ['Ok', 'Cancel']
		}, (param1)=> {
			if(param1) {
				this.props.updateFormState(this.props.form,{callid : calldetailObj.callId});
				setTimeout(()=>{this.controller.save(param,null,true);},0);
			} else
				this.controller.save(param,null,true);
		}));
	}
	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/salesactivities'
	}).then((response)=> {
		if(this.props.isModal || response.data.message != 'success') {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.controller.save(param, true);
			}));
		}

		if (response.data.message == 'success') {
			if (this.props.isModal) {
				this.props.closeModal();
				this.props.callback(response.data.main);
			} else {
				this.props.initialize(response.data.main);
				this.props.history.replace(`/details/salesactivities/${response.data.main.id}`);
				let message = {
					header : 'Success !',
					body : 'Your operation is successful',
					btnArray : ['Create Another Activity', 'Close']
				};
				this.props.openModal(modalService['confirmMethod'](message, (param)=> {
					if(param) {
						this.props.initialize({});
						this.props.history.push('/createSalesactivity');
					}
				}));
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	if(this.props.isModal)
		this.props.closeModal();
	else
		this.props.history.goBack();
}
