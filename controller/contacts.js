import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		salesperson: this.props.app.user.issalesperson ? this.props.app.user.id : null
	};

	if(this.props.location.params) {
		let params = this.props.location.params;
		if(params.parentresource == 'companymaster' || params.parentresource == 'partners') {
			this.setState({connectservice : true});
		}
		if(params.parentresource == 'leads' || params.parentresource == 'partners') {
			tempObj.salesperson = params.salesperson;
			tempObj.industryid = params.industryid;
			tempObj.territoryid = params.parentresource == 'partners' ? params.territory : params.territoryid;
			tempObj.sourceid = params.source;
		}
		if(params.param == 'Dialer') {
			tempObj.mobile = params.mobile;
			tempObj.phone = params.mobile;
		}
		tempObj.parentresource = params.parentresource;
		tempObj.parentid = params.parentid;
	}
	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function getItemById () {
	axios.get(`/api/contacts/${this.props.match.params.id}`).then((response)=> {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				connectservice: (this.props.location.params && (this.props.location.params.parentresource == 'companymaster' || this.props.location.params.parentresource == 'partners')) ? true : false,
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function addressonChange(address, addressobj) {
	this.props.updateFormState(this.props.form, {
		address: addressobj.displayaddress,
		addressid: addressobj.id
	});
}

export function modifiedcallback (modifiedDate) {
	this.props.updateFormState(this.props.form, {
		modified: modifiedDate
	});
}

export function customerOnChange(value, valueObj) {
	let tempObj = {};
	if(!value) {
		tempObj.parentresource = null;
		tempObj.parentid = null;
	} else {
		tempObj.parentresource = 'partners';
		tempObj.parentid = value;
		tempObj.salesperson = valueObj.salesperson;
		tempObj.industryid = valueObj.industryid;
		tempObj.territoryid = valueObj.territory;
	}
	this.props.updateFormState(this.props.form, tempObj);
}

export function checkStage(stageParam) {
	this.updateLoaderFlag(true);
	return this.props.openModal(modalService.confirmMethod({
		header : "Warning",
		body : `Do you want to set this contact as ${stageParam}?`,
		btnArray : ["Ok", 'Cancel']
	}, (param) => {
		if(param) {
			this.props.updateFormState(this.props.form, {
				stage: stageParam
			});
			setTimeout(()=>{this.controller.save('Save')}, 0);
		}
		this.updateLoaderFlag(false);
	}));
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	axios({
		method : 'POST',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/contacts'
	}).then((response)=> {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if (this.props.isModal) {
				this.props.closeModal();
				this.props.callback(response.data.main);
			} else if (this.state.connectservice) {
				this.props.initialize(response.data.main);
				if(this.state.createParam)
					setTimeout(this.props.history.goBack, 0);
			} else {
				if (this.state.createParam)
					this.props.history.replace(`/details/contacts/${response.data.main.id}`);
				else if(param == 'Delete')
					this.props.history.replace('/list/contacts');
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function createCustomer(value) {
	this.props.history.push({pathname: (value == 'suppliers' ? '/createSupplier' : '/createCustomer'), params: {...this.props.resource, param: 'contacts'}});
}

export function createLead() {
	this.props.history.push({pathname: '/createLead', params: this.props.resource});
}

export function cancel () {
	if (this.props.isModal) {
		this.props.closeModal();
	} else{
		this.props.history.goBack();
	}
}
