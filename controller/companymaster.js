import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { modalService, commonMethods, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function getItemById() {
	axios.get(`/api/companymaster/${this.props.match.params.id}`).then((response) => {
		if(response.data.message == "success") {
			this.props.initialize(response.data.main);
			this.setState({
				ndt : {
					notes : response.data.notes,
					documents : response.data.documents,
					tasks : response.data.tasks,
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function indiamartonChange() {
	this.props.updateFormState(this.props.form, {
		indiamartregmobile: null,
		indiamartapikey: null,
		indiamartsalesperson: null,
		indiamartsource: null,
		indiamartstage: null,
		indiamartpriority: null,
		indiamartnumberingseriesmasterid: null
	});
}

export function contactpersonChange (id) {
	for(var i = 0; i < this.props.resource.contacts.length; i++) {
		if(this.props.resource.contacts[i].id == id) {
			this.props.updateFormState(this.props.form, {
				contactperson: this.props.resource.contacts[i].name,
				contactid_phone: this.props.resource.contacts[i].phone,
				contactid_mobile: this.props.resource.contacts[i].mobile,
				contactid_email: this.props.resource.contacts[i].email
			});
			break;
		}
	}
}

export function addNewContact () {
	this.props.history.push({
		pathname: '/createContact',
		params: {
			parentresource : 'companymaster',
			parentid : this.props.resource.id
		}
	});
}

export function addNewAddress () {
	this.props.history.push({
		pathname: '/createAddress',
		params: {
			parentresource: 'companymaster',
			parentid: this.props.resource.id
		}
	});
}

export function getAddressDetails (id) {
	this.props.history.push(`/details/address/${id}`);
}

export function getContactDetails (id) {
	this.props.history.push({
		pathname: `/details/contacts/${id}`,
		params: {
			parentresource : 'companymaster',
			parentid : this.props.resource.id
		}
	});
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/companymaster'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if(response.data.message == "success") {
			if(this.state.createParam) {
				this.props.history.replace(`/details/companymaster/${response.data.main.id}`);
			} else {
				if(param == 'Delete')
					this.props.history.replace('/list/companymaster');
				else
					this.props.initialize(response.data.main);
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function imageUpload (file) {
	this.updateLoaderFlag(true);
	let tempResourceobj = this.props.resource;
	tempResourceobj.parentresource = "companymaster";
	tempResourceobj.resourceName = "companymaster";
	tempResourceobj.parentid = tempResourceobj.id;
	tempResourceobj.imagename = "";

	let tempData = {
		actionverb : 'Save',
		data : tempResourceobj
	};

	const formData = new FormData();
	formData.append('file', file);
	formData.append('data', JSON.stringify(tempResourceobj));
	formData.append('actionverb', 'Save');

	const config = {
		headers: {
			'content-type': 'multipart/form-data'
		}
	};
	let fileName = file.name.substr(0, file.name.lastIndexOf('.'));

	if(!(/^[a-zA-Z0-9\.\,\_\-\'\!\*\(\)\ ]*$/.test(fileName))) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "Invalid file name. Please change file name. file name contain invalid specical characters. Allowed special characters are '_ - . , ! ( ) *'",
			btnArray : ["Ok"]
		}));
	}

	if (file.size > 10485760) {
		this.updateLoaderFlag(false);
		return this.props.openModal(modalService.infoMethod({
			header : "Error",
			body : "File Size should not be exceed than <b class='text-danger'>10 MB</b>",
			btnArray : ["Ok"]
		}));
	}

	if (!file.type.match('image.*')) {
		this.props.updateFormState(this.props.form, {
			image: null
		});
		let message = {
			header : "Error",
			body : "Invalid Image Format.Please Choose Image Type File",
			btnArray : ["Ok"]
		};
		this.props.openModal(modalService.infoMethod(message));
		this.updateLoaderFlag(false);
	} else {
		axios.post('/upload', formData, config).then((response) => {
			if (response.data.message == 'success') {
				let tempobj = response.data.main;
				tempobj.imagename = response.data.main.filename;
				tempobj.image = '';
				this.props.updateFormState(this.props.form, tempobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
}

export function removeImage() {
	this.props.updateFormState(this.props.form, {
		imageurl: null
	});
}

export function cancel () {
	this.props.history.goBack();
}
