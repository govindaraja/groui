import React, { Component } from 'react';
import axios from 'axios';
import * as utils from '../utils/utils';
import { commonMethods, modalService, pageValidation, checkTransactionExist } from '../utils/services';
import MilestoneProgressProjectDetails from '../components/details/milestoneprogressprojectdetailsmodal';

export function onLoad () {
	if(this.state.createParam)
		this.controller.initialiseState();
	else
		this.controller.getItemById();
}

export function initialiseState() {
	let tempObj = {
		currencyid: this.props.app.defaultCurrency,
		companyid: this.props.app.selectedcompanyid,
		progressdate: new Date(new Date().setHours(0, 0, 0, 0)),
		milestoneprogressitems: []
	};

	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function renderTableReffield(item) {
	return (
		<div>
			<span>{`${item.boqid_internalrefno || ''}`}</span>
			{item.boqid_clientrefno ? <br></br> : null}
			{item.boqid_clientrefno ? <span className="text-muted">{`(${item.boqid_clientrefno || ''})`}</span> : null}
		</div>
	);
}

export function getItemById () {
	axios.get(`/api/milestoneprogress/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);

			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function callBackProject(value, valueobj) {
	this.props.updateFormState(this.props.form, {
		[`projectid_projectname`] : valueobj.projectname,
		[`projectid_projectno`] : valueobj.projectno,
		[`projectid_displayname`] : valueobj.displayname,
		[`customerid`] : valueobj.customerid
	});
}

export function getProjectItemDetails() {
	return this.openModal({
		render : (closeModal) => {
			return <MilestoneProgressProjectDetails param={"milestoneprogress"} resource={this.props.resource} app={this.props.app} updateFormState={this.props.updateFormState} form={this.props.form} customFieldsOperation={this.customFieldsOperation} openModal={this.openModal} closeModal={closeModal} callback={(itemarr)=> {
					this.controller.callbackMilestoneProjectItem(itemarr);
				}} />
		}, className: {
			content : 'react-modal-custom-class-90',
			overlay : 'react-modal-overlay-custom-class'
		}
	});
}

export function callbackMilestoneProjectItem(progressitemsArr) {
	this.updateLoaderFlag(true);
	let milestoneprogressitems = [...this.props.resource.milestoneprogressitems];
	let boqstatusidArr = [];

	milestoneprogressitems.forEach((reqitem) => {
		boqstatusidArr.push(reqitem.boqmilestonestatusid);
	});

	progressitemsArr.forEach((item) => {
		let itemFound = false;
		milestoneprogressitems.forEach((progressitem, progressindex) => {
			if (progressitem.boqid == item.boqid && item.boqmilestonestatusid == progressitem.boqmilestonestatusid) {

				itemFound = true;
				progressitem.quantity = item.quantity;
				if (!item.checked)
					milestoneprogressitems.splice(progressindex, 1);
			}
		});

		if(!itemFound && item.checked) {
			if(!boqstatusidArr.includes(item.boqmilestonestatusid) && item.quantity > 0) {

				let tempObj = {};
				utils.assign(tempObj, item, ['boqid', 'milestonestageid', 'boqmilestonestatusid', 'boqmilestonestatusid_name', 'boqid_description', 'boqid_specification', 'boqid_quantity', 'boqid_internalrefno', 'boqid_clientrefno', 'boqid_rate', 'milestonestageid_name', 'quantity', {'itemid' : 'boqid_itemid'}, 'itemid_name', {'uomid' : 'boqid_uomid'}, 'uomid_name']);

				this.customFieldsOperation('boqmilestonestatus', tempObj, item, 'milestoneprogressitems');

				milestoneprogressitems.push(tempObj);
			}
		}
	});

	this.props.updateFormState(this.props.form, { milestoneprogressitems });
	this.updateLoaderFlag(false);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	if(param != 'Update' && param != 'Cancel' && param != 'Delete' && pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/milestoneprogress'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		}));

		if (response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/milestoneprogress/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace("/list/milestoneprogress");
			else {
				this.props.initialize(response.data.main);
			}
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}

export function createProformaInvoice() {
	this.updateLoaderFlag(true);
	checkTransactionExist('proformainvoices', 'projects', this.props.resource.projectid, this.openModal, (confirm) => {
		if(confirm)
			this.props.history.push({pathname: '/createProformaInvoice', params: {...this.props.resource, param: 'Milestone Progress'}});
		this.updateLoaderFlag(false);
	});
}