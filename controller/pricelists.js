import React, { Component } from 'react';
import axios from 'axios';

import * as utils from '../utils/utils';
import { commonMethods, detailsSVC, taxEngine, modalService, checkTransactionExist, pageValidation } from '../utils/services';
import { ChildEditModal } from '../components/utilcomponents';
import PricelistversionCreateModal from '../components/details/pricelistversioncreatemodal';
import PricelistversionCopyModal from '../components/details/pricelistversioncopymodal';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function isactiveInitfn() {
	if(this.props.resource.isactive == null) {
		this.props.updateFormState(this.props.form, {
			isactive: true
		});
	}
}

export function getItemById () {
	axios.get(`/api/pricelists/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			tempObj.disableversion = false;
			tempObj.pricelistversions.sort((a, b) => {
				return (a.versionno < b.versionno) ? -1 : (a.versionno > b.versionno ? 1 : 0);
			});
			for (var i = 0; i < tempObj.pricelistversions.length; i++) {
				if (tempObj.pricelistversions[i].status != 'Approved') {
					tempObj.disableversion = true;
					break;
				}
			}
			this.props.initialize(tempObj);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
			setTimeout(() => {
				this.controller.getCustomerGroups();
			}, 0);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function getCustomerGroups() {
	axios.get(`/api/customergroups?field=id,name&filtercondition=customergroups.pricelistid=${this.props.resource.id}`).then((response) => {
		if (response.data.message == 'success') {
			let groupArray = [];
			for (var i = 0; i < response.data.main.length; i++)
				groupArray.push(response.data.main[i].name);

			this.props.updateFormState(this.props.form, {
				customergroup: groupArray.join()
			});
			this.controller.getExistingPriceList();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getExistingPriceList() {
	axios.get(`/api/pricelists?field=id,name`).then((response) => {
		if (response.data.message == 'success') {
			this.props.updateFormState(this.props.form, {
				pricelistids: response.data.main
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function getPriceVersionDetails (item) {
	this.props.history.push(`/details/pricelistversion/${item.id}`);
}

export function save (param, confirm) {
	this.updateLoaderFlag(true);

	let tempData = {
		actionverb : param,
		data : this.props.resource,
		ignoreExceptions : confirm ? true : false
	};

	axios({
		method : 'post',
		data : tempData,
		url : '/api/pricelists'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
			if (resparam)
				this.controller.save(param, true);
		});

		if (response.data.message == 'success') {
			if(this.state.createParam) {
				this.props.history.replace(`/details/pricelists/${response.data.main.id}`);
			} else {
				if (param == 'Delete') {
					this.props.history.replace("/list/pricelists");
				} else {
					let tempObj = response.data.main;
					for (var i = 0; i < tempObj.pricelistversions.length; i++) {
						if (tempObj.pricelistversions[i].status != 'Approved') {
							tempObj.disableversion = true;
							break;
						}
					}
					this.props.initialize(tempObj);
				}
			}
		}
		this.updateLoaderFlag(false);
	});
}

export function createPriceListVersion () {
	let getBody = (closeModal) => {
		return (
			<PricelistversionCreateModal resource = {this.props.resource} openModal={this.props.openModal} closeModal={closeModal} callback={(id) => {
			this.props.history.push(`/details/pricelistversion/${id}`);
		}} />
		)
	}
	this.openModal({render: (closeModal) => {
		return  <ChildEditModal form={this.props.form} closeModal={closeModal} getBody={getBody} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}
	});
}

export function copyPriceListVersion (versionobj) {
	this.updateLoaderFlag(true);
	axios.get(`/api/pricelistversions?field=id,status&filtercondition=parentid=${this.props.resource.id}`).then((response) => {
		if (response.data.message == 'success') {
			let canCopyVersion = true;
			for (var i = 0; i < response.data.main.length; i++) {
				if (response.data.main[i].status != 'Approved') {
					canCopyVersion = false;
					break;
				}
			}

			if (canCopyVersion) {
				this.openModal({render: (closeModal) => {
					return  <PricelistversionCopyModal resource={this.props.resource} closeModal={closeModal} callback={(description) => {
						this.controller.saveCopiedPricelistversion(description, versionobj);
					}} />
					}, className: {
						content: 'react-modal-custom-class',
						overlay: 'react-modal-overlay-custom-class'
					}
				});

			} else {
				this.props.openModal(modalService.infoMethod({
					header : "Error",
					body : "Please approve the exisiting versions before create new version!!!!!",
					btnArray : ["Ok"]
				}));
			}
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function saveCopiedPricelistversion (description, versionobj) {
	this.updateLoaderFlag(true);
	let tempData = {
		actionverb : 'Save',
		data : {
			priceversionid : versionobj.id,
			parentid : this.props.resource.id,
			remarks : description,
			currencyid : this.props.resource.currencyid,
			parentresource : 'pricelists'
		}
	};

	axios({
		method : 'POST',
		data : tempData,
		url : '/api/pricelistversions'
	}).then((response) => {
		if (response.data.message == 'success') {
			this.props.history.push(`/details/pricelistversion/${response.data.main.id}`);
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
