import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	axios.get(`/api/taxratemaster?field=id,name,description,type,basefactor,value`).then((response) => {
		if (response.data.message == 'success') {
			this.setState({
				taxratemasterArray: response.data.main
			});

			if (this.state.createParam)
				this.controller.initialiseState();
			else
				this.controller.getItemById();
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
	});
}

export function initialiseState() {
	let tempObj = {
		taxtemplate : []
	};

	if (this.props.location.params && this.props.location.params.param == 'copy') {
		tempObj = this.props.location.params;
		tempObj.taxtemplate = this.controller.setBasearrTotaxtemplate(tempObj.taxtemplate);
	}

	this.props.initialize(tempObj);
	this.updateLoaderFlag(false);
}

export function taxRateOnChange (value, valueObj, itemstr) {
	this.props.updateFormState(this.props.form, {
		[`${itemstr}.taxratemasterid_name`]: valueObj.name,
		[`${itemstr}.taxratemasterid_type`]: valueObj.type,
		[`${itemstr}.taxratemasterid_description`]: valueObj.description,
		[`${itemstr}.taxratemasterid_basefactor`]: valueObj.basefactor,
		[`${itemstr}.taxratemasterid_value`]: valueObj.value,
	});
}

export function getItemById () {
	axios.get(`/api/taxcodemaster/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			let tempObj = response.data.main;
			
			tempObj.taxtemplate = this.controller.setBasearrTotaxtemplate(tempObj.taxtemplate);

			this.props.initialize(tempObj);

		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function setBasearrTotaxtemplate (taxtemplate) {
	taxtemplate.map((item) => {
		item.basearr = item.basearray.join();
	});

	taxtemplate.sort((a, b) => a.orders-b.orders);

	return taxtemplate;
}

export function save (param) {
	this.updateLoaderFlag(true);

	if (pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	let tempObj = this.props.resource,
		errorArray = [],
		errorFound = false;

	tempObj.taxtemplate.map((item, index) => {
		item.basearray = item.basearr.split(',');
		item.orders = index + 1;
	});

	for (let i = 0; i < tempObj.taxtemplate.length; i++) {
		if (tempObj.taxtemplate[i].basearray.length > 0 && tempObj.taxtemplate[i].basearray.length <= tempObj.taxtemplate[i].orders) {
			for (let j = 0; j < tempObj.taxtemplate[i].basearray.length; j++) {
				if (tempObj.taxtemplate[i].basearray[j] == '' || isNaN(Number(tempObj.taxtemplate[i].basearray[j]))) {
					errorFound = true;
					errorArray.push(`Please Enter Valid Base Array for Row :- ${i+1}`);
					break;
				}

				if (Number(tempObj.taxtemplate[i].basearray[j]) < 0 || Number(tempObj.taxtemplate[i].basearray[j]) >= tempObj.taxtemplate[i].orders) {
					errorFound = true;
					errorArray.push(`Please Enter Valid Base Array for Row :- ${i+1}`);
					break;
				}
			}
		} else {
			errorFound = true;
			errorArray.push(`Please Enter Valid Base Array for Row :- ${i+1}`);
			break;
		}

		if (errorFound)
			break;
	}

	if (errorArray.length > 0) {
		let response = {
			data : {
				message : 'failure',
				error : errorArray
			}
		};

		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		this.updateLoaderFlag(false);
	} else {
		this.controller.saveTaxCode(param, tempObj);
	}
}

export function saveTaxCode (param, tempObj, confirm) {
	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : tempObj,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/taxcodemaster'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.saveTaxCode(param, tempObj, true);
		}));

		if (response.data.message == 'success') {
			if (this.state.createParam)
				this.props.history.replace(`/details/taxcodemaster/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace('/list/taxcodemaster');
			else 
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
