import React, { Component } from 'react';
import axios from 'axios';
import moment from 'moment';

import { commonMethods, modalService, pageValidation } from '../utils/services';

export function onLoad () {
	if(this.state.createParam) {
		this.props.initialize({
			companyid: this.props.app.selectedcompanyid
		});
		this.updateLoaderFlag(false);
	} else
		this.controller.getItemById();
}

export function startdateOnChange() {
	if(this.props.resource.startdate && !this.props.resource.enddate) {
		let tempEnddate = new Date(this.props.resource.startdate).setFullYear(new Date(this.props.resource.startdate).getFullYear() + 1);

		let enddate = moment(new Date(tempEnddate).setDate(new Date(tempEnddate).getDate() - 1));

		this.props.updateFormState(this.props.form, {
			enddate
		});
	}
}

export function getItemById () {
	axios.get(`/api/financialyears/${this.props.match.params.id}`).then((response) => {
		if (response.data.message == 'success') {
			this.props.initialize(response.data.main);
			this.setState({
				ndt: {
					notes: response.data.notes,
					documents: response.data.documents,
					tasks: response.data.tasks
				}
			});
		} else {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}
		this.updateLoaderFlag(false);
	});
}

export function save (param) {

	if(pageValidation(this.props)) {
		this.updateLoaderFlag(false);
		return true;
	}

	if (param == 'Close') {
		let message = {
			header : 'Confirm Dialog',
			body : 'If you close the financial period, you will not be able to make any entries for this period. Continue?',
			btnArray : ['Ok','Cancel']
		};
		this.props.openModal(modalService['confirmMethod'](message, (flag) => {
			if(flag)
				this.controller.saveFinancialYear(param);
		}));
	} else
		this.controller.saveFinancialYear(param);

}

export function saveFinancialYear (param, confirm) {
	this.updateLoaderFlag(true);

	axios({
		method : 'post',
		data : {
			actionverb : param,
			data : this.props.resource,
			ignoreExceptions : confirm ? true : false
		},
		url : '/api/financialyears'
	}).then((response) => {
		let apiResponse = commonMethods.apiResult(response);
		this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
			if (resparam)
				this.controller.saveFinancialYear(param, true);
		}));

		if(response.data.message == 'success') {
			if(this.state.createParam)
				this.props.history.replace(`/details/financialyears/${response.data.main.id}`);
			else if (param == 'Delete')
				this.props.history.replace('/list/financialyears');
			else
				this.props.initialize(response.data.main);
		}

		this.updateLoaderFlag(false);
	});
}

export function cancel () {
	this.props.history.goBack();
}
