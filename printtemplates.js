exports.printtemplateArray = [{
		"name" : "Contacts",
		"resource" : "contacts",
		"displayname" : "Contacts",
		"templatename" : "contacts.docx",
		"printmodules" : [],
		"isincludeinsetup" : true
	}, {
		"name" : "Quote",
		"resource" : "quotes",
		"displayname" : "{{Quote No}}",
		"templatename" : "quote.docx",
		"printmodules" : ["Item Discount", "Watermark", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "Proforma Invoice(Quote)",
		"resource" : "quotes",
		"displayname" : "{{Quote No}}",
		"templatename" : "proforma invoice(quote).docx",
		"printmodules" : ["Item Discount", "Watermark", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "Sales Order",
		"resource" : "orders",
		"displayname" : "{{Order No}}",
		"templatename" : "sales order.docx",
		"printmodules" : ["Item Discount", "Watermark", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "Proforma Invoice(SO)",
		"resource" : "orders",
		"displayname" : "{{Order No}}",
		"templatename" : "proforma invoice(so).docx",
		"printmodules" : ["Item Discount", "Watermark", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "Sales Order With Bank Details",
		"resource" : "orders",
		"displayname" : "{{Order No}}",
		"templatename" : "sales order with bank details.docx",
		"printmodules" : ["Item Discount", "Watermark", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "Delivery Note",
		"resource" : "deliverynotes",
		"displayname" : "{{Delivery Note No}}",
		"templatename" : "delivery note.docx",
		"printmodules" : ["Watermark", "Serial No", "Batch No", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "Stock Reservation",
		"resource" : "stockreservations",
		"displayname" : "{{Stock Reservation No}}",
		"templatename" : "stock reservation.docx",
		"printmodules" : [],
		"isincludeinsetup" : true
	}, {
		"name" : "Sales Invoice",
		"resource" : "salesinvoices",
		"displayname" : "{{Invoice No}}",
		"templatename" : "sales invoice.docx",
		"printmodules" : ["Item Discount", "Watermark", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "Sales Invoice 3-Copies",
		"resource" : "salesinvoices",
		"displayname" : "{{Invoice No}}",
		"templatename" : "sales invoice 3copies.docx",
		"printmodules" : ["Item Discount", "Watermark", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "RFQ",
		"resource" : "rfq",
		"displayname" : "{{RFQ No}}",
		"templatename" : "rfq.docx",
		"printmodules" : ["Watermark"],
		"isincludeinsetup" : false
	}, {
		"name" : "Purchase Order",
		"resource" : "purchaseorders",
		"displayname" : "{{PO Number}}",
		"templatename" : "purchase order.docx",
		"printmodules" : ["Item Discount", "Watermark", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "Receipt Note",
		"resource" : "receiptnotes",
		"displayname" : "{{Receipt Note No}}",
		"templatename" : "receipt note.docx",
		"printmodules" : ["Watermark", "Serial No", "Batch No", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "Purchase Invoice",
		"resource" : "purchaseinvoices",
		"displayname" : "{{Invoice No}}",
		"templatename" : "purchase invoice.docx",
		"printmodules" : ["Item Discount", "Watermark", "Kit Item Aggregation"],
		"isincludeinsetup" : true
	}, {
		"name" : "Receipt Voucher",
		"resource" : "receiptvouchers",
		"displayname" : "{{Voucher No}}",
		"templatename" : "receipt voucher.docx",
		"printmodules" : ["Watermark", "Journal Voucher Sections"],
		"isincludeinsetup" : true
	}, {
		"name" : "Payment Voucher",
		"resource" : "paymentvouchers",
		"displayname" : "{{Voucher No}}",
		"templatename" : "payment voucher.docx",
		"printmodules" : ["Watermark", "Journal Voucher Sections"],
		"isincludeinsetup" : true
	}, {
		"name" : "Journal Voucher",
		"resource" : "journals",
		"displayname" : "{{Voucher No}}",
		"templatename" : "journal voucher.docx",
		"printmodules" : ["Watermark", "Journal Voucher Sections"],
		"isincludeinsetup" : true
	}, {
		"name" : "Credit Note",
		"resource" : "creditnotes",
		"displayname" : "{{Voucher No}}",
		"templatename" : "credit note.docx",
		"printmodules" : ["Watermark", "Item Discount"],
		"isincludeinsetup" : true
	}, {
		"name" : "Debit Note",
		"resource" : "debitnotes",
		"displayname" : "{{Voucher No}}",
		"templatename" : "debit note.docx",
		"printmodules" : ["Watermark", "Item Discount"],
		"isincludeinsetup" : true
	}, {
		"name" : "Payslip",
		"resource" : "payrolls",
		"displayname" : "{{Payroll No}}",
		"templatename" : "payslip.docx",
		"printmodules" : ["Pay Slip (Earning / Deduction Spliting)"],
		"isincludeinsetup" : true
	}, {
		"name" : "Salary Payment Advice",
		"resource" : "paymentvouchers",
		"displayname" : "Salary Payment Advice",
		"templatename" : "salary payment advice.docx",
		"printmodules" : ["Watermark", "Payroll Details - JV"],
		"isincludeinsetup" : true
	}, {
		"name" : "Expense Request",
		"resource" : "expenserequests",
		"displayname" : "{{Request No}}",
		"templatename" : "expense request.docx",
		"printmodules" : [],
		"isincludeinsetup" : true
	}, {
		"name" : "Service Call",
		"resource" : "servicecalls",
		"displayname" : "{{Service Call No}}",
		"templatename" : "service call.docx",
		"printmodules" : ["Watermark"],
		"isincludeinsetup" : false
	}, {
		"name" : "Estimation",
		"resource" : "estimations",
		"displayname" : "{{Estimation No}}",
		"templatename" : "estimation.docx",
		"printmodules" : ["Watermark", "Item Discount"],
		"isincludeinsetup" : false
	}, {
		"name" : "Estimation Sections",
		"resource" : "estimations",
		"displayname" : "{{Estimation No}}",
		"templatename" : "estimation sections.docx",
		"printmodules" : ["Watermark", "Item Discount", "Estimation Item Reordering"],
		"isincludeinsetup" : false
	}, {
		"name" : "Item Request",
		"resource" : "itemrequests",
		"displayname" : "{{Item Request No}}",
		"templatename" : "item request.docx",
		"printmodules" : ["Watermark"],
		"isincludeinsetup" : false
	}, {
		"name" : "Service Report",
		"resource" : "servicereports",
		"displayname" : "{{Service Report No}}",
		"templatename" : "service report.docx",
		"printmodules" : ["Watermark", "Item Discount", "Service Report Item Reordering"],
		"isincludeinsetup" : false
	}, {
		"name" : "Service Report Sections",
		"resource" : "servicereports",
		"displayname" : "{{Service Report No}}",
		"templatename" : "service report sections.docx",
		"printmodules" : ["Watermark", "Item Discount", "Service Report Item Reordering"],
		"isincludeinsetup" : false
	}, {
		"name" : "Service Report With Customer Signature",
		"resource" : "servicereports",
		"displayname" : "{{Service Report No}}",
		"templatename" : "service report with customer signature.docx",
		"printmodules" : ["Watermark", "Item Discount", "Service Report Item Reordering"],
		"isincludeinsetup" : false
	}, {
		"name" : "Service Report Sections With Customer Signature",
		"resource" : "servicereports",
		"displayname" : "{{Service Report No}}",
		"templatename" : "service report sections with customer signature.docx",
		"printmodules" : ["Watermark", "Item Discount", "Service Report Item Reordering"],
		"isincludeinsetup" : false
	}, {
		"name" : "Work Order",
		"resource" : "workorders",
		"displayname" : "{{Work Order No}}",
		"templatename" : "work order.docx",
		"printmodules" : ["Watermark", "Item Discount"],
		"isincludeinsetup" : false
	}, {
		"name" : "Work Order With Split Rate",
		"resource" : "workorders",
		"displayname" : "{{Work Order No}}",
		"templatename" : "work order with split rate.docx",
		"printmodules" : ["Watermark", "Item Discount", "Split Rate"],
		"isincludeinsetup" : false
	}, {
		"name" : "Service Invoice",
		"resource" : "salesinvoices",
		"displayname" : "{{Invoice No}}",
		"templatename" : "service invoice.docx",
		"printmodules" : ["Watermark", "Item Discount"],
		"isincludeinsetup" : false
	}, {
		"name" : "Contract Enquiry-Equipment Wise",
		"resource" : "contractenquiries",
		"displayname" : "{{Contract Enquiry No}}",
		"templatename" : "contract enquiry-equipment wise.docx",
		"printmodules" : ["Watermark", "Item Discount", "Contract Type, Schedule, HSN Code"],
		"isincludeinsetup" : false
	}, {
		"name" : "Contract Enquiry-Item Wise",
		"resource" : "contractenquiries",
		"displayname" : "{{Contract Enquiry No}}",
		"templatename" : "contract enquiry-item wise.docx",
		"printmodules" : ["Watermark", "Item Discount", "Contract Type, Schedule, HSN Code"],
		"isincludeinsetup" : false
	}, {
		"name" : "Contract-Equipment Wise",
		"resource" : "contracts",
		"displayname" : "{{Contract No}}",
		"templatename" : "contract-equipment wise.docx",
		"printmodules" : ["Watermark", "Item Discount", "Contract Type, Schedule, HSN Code"],
		"isincludeinsetup" : false
	}, {
		"name" : "Contract Invoice",
		"resource" : "salesinvoices",
		"displayname" : "{{Invoice No}}",
		"templatename" : "contract invoice.docx",
		"printmodules" : ["Watermark", "Item Discount"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Quote",
		"resource" : "projectquotes",
		"displayname" : "{{Quote No}}",
		"templatename" : "project quote.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Quote-Split Rate",
		"resource" : "projectquotes",
		"displayname" : "{{Quote No}}",
		"templatename" : "project quote-split rate.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Split Rate"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project-Split Rate",
		"resource" : "projects",
		"displayname" : "{{Project No}}",
		"templatename" : "project-split rate.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Split Rate"],
		"isincludeinsetup" : false
	}, {
		"name" : "Milestone Progress-Landscape",
		"resource" : "milestoneprogress",
		"displayname" : "{{Progress No}}",
		"templatename" : "milestone progress-landscape.docx",
		"printmodules" : [],
		"isincludeinsetup" : false
	}, {
		"name" : "Milestone Progress-Portrait",
		"resource" : "milestoneprogress",
		"displayname" : "{{Progress No}}",
		"templatename" : "milestone progress-portrait.docx",
		"printmodules" : [],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Invoice",
		"resource" : "salesinvoices",
		"displayname" : "{{Invoice No}}",
		"templatename" : "project invoice.docx",
		"printmodules" : ["Watermark", "Item Discount"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Invoice-Split Rate",
		"resource" : "salesinvoices",
		"displayname" : "{{Invoice No}}",
		"templatename" : "project invoice-split rate.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Split Rate"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Invoice-Progress",
		"resource" : "salesinvoices",
		"displayname" : "{{Invoice No}}",
		"templatename" : "project invoice-progress.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation"],
		"isincludeinsetup" : false
	}, {
		"name" : "BOM",
		"resource" : "bom",
		"displayname" : "BOM",
		"templatename" : "bom.docx",
		"printmodules" : [],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Quote Long Description",
		"resource" : "projectquotes",
		"displayname" : "{{Quote No}}",
		"templatename" : "project quote long description.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Quote-Split Rate Long Description",
		"resource" : "projectquotes",
		"displayname" : "{{Quote No}}",
		"templatename" : "project quote-split rate long description.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Split Rate"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project",
		"resource" : "projects",
		"displayname" : "{{Project No}}",
		"templatename" : "project.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Long Description",
		"resource" : "projects",
		"displayname" : "{{Project No}}",
		"templatename" : "project long description.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project-Split Rate Long Description",
		"resource" : "projects",
		"displayname" : "{{Project No}}",
		"templatename" : "project-split rate long description.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Split Rate"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Quote (Section wise Total)",
		"resource" : "projectquotes",
		"displayname" : "{{Quote No}}",
		"templatename" : "project quote-sectionwise total.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Group Wise Total"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Quote-Split Rate (Section wise Total)",
		"resource" : "projectquotes",
		"displayname" : "{{Quote No}}",
		"templatename" : "project quote-split rate-sectionwise total.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Split Rate", "Group Wise Total"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Quote Long Description (Section wise Total)",
		"resource" : "projectquotes",
		"displayname" : "{{Quote No}}",
		"templatename" : "project quote long description-sectionwise total.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Group Wise Total"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Quote-Split Rate Long Description (Section wise Total)",
		"resource" : "projectquotes",
		"displayname" : "{{Quote No}}",
		"templatename" : "project quote-split rate long description-sectionwise total.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Split Rate", "Group Wise Total"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project-Split Rate (Section wise Total)",
		"resource" : "projects",
		"displayname" : "{{Project No}}",
		"templatename" : "project-split rate-sectionwise total.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Split Rate", "Group Wise Total"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project (Section wise Total)",
		"resource" : "projects",
		"displayname" : "{{Project No}}",
		"templatename" : "project-sectionwise total.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Group Wise Total"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project-Split Rate Long Description (Section wise Total)",
		"resource" : "projects",
		"displayname" : "{{Project No}}",
		"templatename" : "project-split rate long description-sectionwise total.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Split Rate", "Group Wise Total"],
		"isincludeinsetup" : false
	}, {
		"name" : "Project Long Description (Section wise Total)",
		"resource" : "projects",
		"displayname" : "{{Project No}}",
		"templatename" : "project long description-sectionwise total.docx",
		"printmodules" : ["Watermark", "Item Discount", "Kit Item Aggregation", "Group Wise Total"],
		"isincludeinsetup" : false
	}, {
		"name" : "Proforma Invoice",
		"resource" : "proformainvoices",
		"displayname" : "{{Proforma Invoice No}}",
		"templatename" : "proforma invoice.docx",
		"printmodules" : ["Watermark", "Item Discount"],
		"isincludeinsetup" : false
	}
]