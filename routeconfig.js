import List from './containers/list';
import DetailForm from './containers/details';
import ReportForm from './reports/reports';
import DocumentLibraryForm from './containers/documentlibrary';
import SalesActivityReportForm from './reports/salesactivityreport';
import SalesRegisterReportForm from './reports/salesregisterreport';
import QuotationItemsReportForm from './reports/quotationitemsreport';
import OrderItemsReportForm from './reports/orderitemsreport';
import LeadItemsReportForm from './reports/leaditemsreport';
import ProjectQuoteItemsReportForm from './reports/projectquoteitemsreport';
import ProjectsReportForm from './reports/projectsreport';
import ProjectsProfitabilityReportForm from './reports/projectprofitabilityreport';
import PurchaseRegisterReportForm from './reports/purchaseregisterreport';
import StockTransactionHistoryReportForm from './reports/stocktransactionhistoryreport';
import SalesInvoiceItemsReportForm from './reports/salesinvoiceitemsreport';
import SalesProfitabilityReportForm from './reports/salesprofitabilityreport';
import TimesheetReportForm from './reports/timesheetreport';
import AttendanceReportForm from './reports/attendancereport';
import ContactsReportForm from './reports/contactsreport';
import ProjectPurchasePlannerReportForm from './reports/projectpurchaseplannerreport';
import ContractItemsReportForm from './reports/contractitemsreport';
import ContractEnquiryItemsReportForm from './reports/contractenquiryitemsreport';
import EquipmentScheduleReportForm from './reports/equipmentschedulereport';
import ContractDueforRenewalReportForm from './reports/contractsdueforrenewalreport';
import UnsettledReceiptPaymentReportForm from './reports/unsettledreceiptspaymentsreport';
import ServicecallAllotmentReportForm from './reports/servicecallallotmentreport';
import EngineerPerformanceReportForm from './reports/engineerperformancereport';
import AccountReceivableReportForm from './reports/accountreceivablereport';
import AccountPayableReportForm from './reports/accountpayablereport';
import GeneralLedgerReportForm from './reports/generalledgerreport';
import GeneralLedgerCostSplitUpReportForm from './reports/generalledgercostsplitups';
import AccountBalanceReportForm from './reports/accountbalancereport';
import CashBookReportForm from './reports/cashbookreport';
import SalesPerformanceReportForm from './reports/salesperformancereport';
import StockStatementReportForm from './reports/stockstatementreport';
import SerialnoStatusReportForm from './reports/serialnostatusreport';
import SerialnoHistoryReportForm from './reports/serialnohistoryreport';
import PurchasePlannerReportForm from './reports/purchaseplannerreport';
import StockAgeingReportForm from './reports/stockageingreport';
import PurchaseInvoiceItemsReportForm from './reports/purchaseinvoiceitemsreport';
import MaterialAvailabilityReportForm from './reports/materialavailabilityreport';
import StockAsonDateReportForm from './reports/stockasondatereport';
import CustomerSearchReportForm from './reports/customersearch';
import ServiceDashboardForm from './reports/servicedashboard';
import StockBatchReportForm from './reports/stockbybatchreport';
import StockBatchHistoryReportForm from './reports/stockbatchhistoryreport';
import ActivityCalendarForm from './reports/activitycalendar';
import CustomerStatementForm from './reports/customerstatementreport';
import CollectionStatusReportForm from './reports/collectionstatusreport';
import TargetActualsReportForm from './reports/targetactualsreport';
import EmployeeLocationsReportForm from './reports/employeelocationsreport';
import BankReconciliationReportForm from './reports/bankreconciliationreport';
import TeamStructure from './containers/teamstructure';
import AttendanceCapture from './containers/attendancecapture';
import Settings from './containers/settings';
import Permissions from './containers/permissions';
import DeliveryNotePlannerReportForm from './reports/deliveryplannerreport';
import BalanceSheetReportForm from './reports/balancesheetreport';
import ProfitLossReportForm from './reports/profitlossreport';
import ReceiptNotePlannerReportForm from './reports/receiptnoteplannerreport';
import SalesInvoicePlannerReportForm from './reports/salesinvoicetoolreport';
import PurchaseInvoicePlannerReportForm from './reports/purchaseinvoicetoolreport';
//import ImportCustomer from './containers/importcustomer';
//import ImportItemMaster from './containers/importitemmaster';
import ImportSalesInvoice from './containers/importsalesinvoice';
import ImportPurchaseInvoice from './containers/importpurchaseinvoice';
import AccountingFormsReportForm from './reports/accountingformsreport';
import GSTR1ReportForm from './reports/gstr1';
import CustomReportForm from './reports/customreport';
import SalesPipelineReportForm from './reports/salespipelinereport';
import ChangePasswordForm from './containers/changepassword';
import PreferencesForm from './containers/preferences';
import ProductionScheduleReportForm from './reports/productionschedulereport';
import OrderCalendarForm from './containers/ordercalendar';
import OrderAcknowledgement from './containers/orderacknowledgement';
import OrderCheckList from './containers/orderchecklists';
import PayrollBatchForm from './containers/payrollbatch';
import PayrollReportForm from './reports/payrollreport';
import ItemRequestItemsReportForm from './reports/itemrequestitemsreport';
import DeliveryRoutePlannerReportForm from './reports/deliveryrouteplannerreport';
import ProjectDeliveryPlannerReportForm from './reports/projectdeliveryplannerreport';
import ProjectContractorPaymentReportForm from './reports/projectcontractorpaymentreport';
import ProjectCostVarianceReportForm from './reports/projectcostvariancereport';
import ProjectProfitVarianceReportForm from './reports/projectprofitvariancereport';
import ProjectMilestoneProgressSummaryReportForm from './reports/projectsmilestoneprogresssummaryreport';
import ListviewComponent from './containers/listview';
import DataImport from './import/import';
import CallStatusReportForm from './reports/callstatusreport';
import DataSet from './containers/dataset';
import ReportBuilderConfigForm from './reportbuilder/report';
import ReportBuilderForm from './reportbuilder/reportbuilder';
//import RevenueRecongnitionReportForm from './reports/revenuerecongnitionreport';
import LeaveCreditBatchForm from './containers/leavecreditbatch';
import ReconcileWIP from './reports/reconcilewip';
import ForceChangePasswordForm from './containers/forcechangepassword';
import PerformanceTarget from './containers/performancetargets';
import PerformanceTargetDashBoard from './containers/performancetargetdashboard';
import PerformanceTargetReport from './containers/performancetargetreport';
import DashboardBuilderForm from './containers/dashboardbuilder';
import DashboardWidgetForm from './containers/dashboardwidget';
import DataAccessForm from './containers/dataaccesscontainer';
import ServiceEngineerAssignment from './containers/serviceengineerassignment';
import UserLogReportForm from './reports/userlogreport';

export default {
	'/list/leads' : {
		pageoptions: {
			title: 'leads',
			type: 'List'
		},
		component: List
	},
	'/createLead' : {
		pageoptions: {
			title: 'leads',
			type: 'Details',
			controller: 'leads'
		},
		component: DetailForm
	},
	'/details/leads/:id' : {
		pageoptions: {
			title: 'leads',
			type: 'Details',
			controller: 'leads'
		},
		component: DetailForm
	},
	'/list/quotes' : {
		pageoptions: {
			title: 'quotes',
			type: 'List'
		},
		component: List
	},
	'/createQuote' : {
		pageoptions: {
			title: 'quotes',
			type: 'Details',
			controller: 'quotes'
		},
		component: DetailForm
	},
	'/details/quotes/:id' : {
		pageoptions: {
			title: 'quotes',
			type: 'Details',
			controller: 'quotes'
		},
		component: DetailForm
	},
	'/list/orders' : {
		pageoptions: {
			title: 'orders',
			type: 'List'
		},
		component: List
	},
	'/createOrder' : {
		pageoptions: {
			title: 'orders',
			type: 'Details',
			controller: 'orders'
		},
		component: DetailForm
	},
	'/details/orders/:id' : {
		pageoptions: {
			title: 'orders',
			type: 'Details',
			controller: 'orders'
		},
		component: DetailForm
	},
	'/list/deliverynotes' : {
		pageoptions: {
			title: 'deliverynotes',
			type: 'List'
		},
		component: List
	},
	'/createDeliveryNote' : {
		pageoptions: {
			title: 'deliverynotes',
			type: 'Details',
			controller: 'deliverynotes'
		},
		component: DetailForm
	},
	'/details/deliverynotes/:id' : {
		pageoptions: {
			title: 'deliverynotes',
			type: 'Details',
			controller: 'deliverynotes'
		},
		component: DetailForm
	},
	'/list/salesinvoices' : {
		pageoptions: {
			title: 'salesinvoices',
			type: 'List'
		},
		component: List
	},
	'/details/salesinvoices/:id' : {
		pageoptions: {
			title: 'salesinvoices',
			type: 'Details',
			controller: 'salesinvoices'
		},
		component: DetailForm
	},
	'/createSalesActivity' : {
		pageoptions: {
			title: 'salesactivities',
			type: 'Details',
			controller: 'salesactivities'
		},
		component: DetailForm
	},
	'/details/salesactivities/:id' : {
		pageoptions: {
			title: 'salesactivities',
			type: 'Details',
			controller: 'salesactivities'
		},
		component: DetailForm
	},
	'/list/contacts' : {
		pageoptions: {
			title: 'contacts',
			type: 'List'
		},
		component: List
	},
	'/createContact' : {
		pageoptions: {
			title: 'contacts',
			type: 'Details',
			controller: 'contacts'
		},
		component: DetailForm
	},
	'/details/contacts/:id' : {
		pageoptions: {
			title: 'contacts',
			type: 'Details',
			controller: 'contacts'
		},
		component: DetailForm
	},
	'/list/servicecalls' : {
		pageoptions: {
			title: 'servicecalls',
			type: 'List'
		},
		component: List
	},
	'/createServiceCall' : {
		pageoptions: {
			title: 'servicecalls',
			type: 'Details',
			controller: 'servicecalls'
		},
		component: DetailForm
	},
	'/details/servicecalls/:id' : {
		pageoptions: {
			title: 'servicecalls',
			type: 'Details',
			controller: 'servicecalls'
		},
		component: DetailForm
	},
	'/list/estimations' : {
		pageoptions: {
			title: 'estimations',
			type: 'List'
		},
		component: List
	},
	'/createEstimation' : {
		pageoptions: {
			title: 'estimations',
			type: 'Details',
			controller: 'estimations'
		},
		component: DetailForm
	},
	'/details/estimations/:id' : {
		pageoptions: {
			title: 'estimations',
			type: 'Details',
			controller: 'estimations'
		},
		component: DetailForm
	},
	'/list/itemrequests' : {
		pageoptions: {
			title: 'itemrequests',
			type: 'List'
		},
		component: List
	},
	'/createItemRequest' : {
		pageoptions: {
			title: 'itemrequests',
			type: 'Details',
			controller: 'itemrequests'
		},
		component: DetailForm
	},
	'/details/itemrequests/:id' : {
		pageoptions: {
			title: 'itemrequests',
			type: 'Details',
			controller: 'itemrequests'
		},
		component: DetailForm
	},
	'/list/servicereports' : {
		pageoptions: {
			title: 'servicereports',
			type: 'List'
		},
		component: List
	},
	'/createServiceReport' : {
		pageoptions: {
			title: 'servicereports',
			type: 'Details',
			controller: 'servicereports'
		},
		component: DetailForm
	},
	'/details/servicereports/:id' : {
		pageoptions: {
			title: 'servicereports',
			type: 'Details',
			controller: 'servicereports'
		},
		component: DetailForm
	},
	'/list/serviceinvoices' : {
		pageoptions: {
			title: 'serviceinvoices',
			type: 'List'
		},
		component: List
	},
	'/createServiceInvoice' : {
		pageoptions: {
			title: 'serviceinvoices',
			type: 'Details',
			controller: 'salesinvoices'
		},
		component: DetailForm
	},
	'/details/serviceinvoices/:id' : {
		pageoptions: {
			title: 'serviceinvoices',
			type: 'Details',
			controller: 'salesinvoices'
		},
		component: DetailForm
	},
	'/list/salesinvoices' : {
		pageoptions: {
			title: 'salesinvoices',
			type: 'List'
		},
		component: List
	},
	'/createSalesInvoice' : {
		pageoptions: {
			title: 'salesinvoices',
			type: 'Details',
			controller: 'salesinvoices'
		},
		component: DetailForm
	},
	'/details/salesinvoices/:id' : {
		pageoptions: {
			title: 'salesinvoices',
			type: 'Details',
			controller: 'salesinvoices'
		},
		component: DetailForm
	},
	'/list/contractinvoices' : {
		pageoptions: {
			title: 'contractinvoices',
			type: 'List'
		},
		component: List
	},
	'/createContractInvoice' : {
		pageoptions: {
			title: 'contractinvoices',
			type: 'Details',
			controller: 'salesinvoices'
		},
		component: DetailForm
	},
	'/details/contractinvoices/:id' : {
		pageoptions: {
			title: 'contractinvoices',
			type: 'Details',
			controller: 'salesinvoices'
		},
		component: DetailForm
	},
	'/list/leaverequests' : {
		pageoptions: {
			title: 'leaverequests',
			type: 'List'
		},
		component: List
	},
	'/createLeaveRequest' : {
		pageoptions: {
			title: 'leaverequests',
			type: 'Details',
			controller: 'leaverequests'
		},
		component: DetailForm
	},
	'/details/leaverequests/:id' : {
		pageoptions: {
			title: 'leaverequests',
			type: 'Details',
			controller: 'leaverequests'
		},
		component: DetailForm
	},
	'/list/expenserequests' : {
		pageoptions: {
			title: 'expenserequests',
			type: 'List'
		},
		component: List
	},
	'/createExpenseRequest' : {
		pageoptions: {
			title: 'expenserequests',
			type: 'Details',
			controller: 'expenserequests'
		},
		component: DetailForm
	},
	'/details/expenserequests/:id' : {
		pageoptions: {
			title: 'expenserequests',
			type: 'Details',
			controller: 'expenserequests'
		},
		component: DetailForm
	},
	'/list/customers' : {
		pageoptions: {
			title: 'customers',
			type: 'List'
		},
		component: List
	},
	'/createCustomer' : {
		pageoptions: {
			title: 'partners',
			type: 'Details',
			controller: 'partners'
		},
		component: DetailForm
	},
	'/details/customers/:id' : {
		pageoptions: {
			title: 'partners',
			type: 'Details',
			controller: 'partners'
		},
		component: DetailForm
	},
	'/list/suppliers' : {
		pageoptions: {
			title: 'suppliers',
			type: 'List'
		},
		component: List
	},
	'/createSupplier' : {
		pageoptions: {
			title: 'partners',
			type: 'Details',
			controller: 'partners'
		},
		component: DetailForm
	},
	'/details/suppliers/:id' : {
		pageoptions: {
			title: 'partners',
			type: 'Details',
			controller: 'partners'
		},
		component: DetailForm
	},
	'/list/competitors' : {
		pageoptions: {
			title: 'competitors',
			type: 'List'
		},
		component: List
	},
	'/createCompetitor' : {
		pageoptions: {
			title: 'partners',
			type: 'Details',
			controller: 'partners'
		},
		component: DetailForm
	},
	'/details/competitors/:id' : {
		pageoptions: {
			title: 'partners',
			type: 'Details',
			controller: 'partners'
		},
		component: DetailForm
	},
	'/details/partners/:id' : {
		pageoptions: {
			title: 'partners',
			type: 'Details',
			controller: 'partners'
		},
		component: DetailForm
	},
	'/list/tasks' : {
		pageoptions: {
			title: 'tasks',
			type: 'List'
		},
		component: List
	},
	'/createTask' : {
		pageoptions: {
			title: 'tasks',
			type: 'Details',
			controller: 'tasks'
		},
		component: DetailForm
	},
	'/details/tasks/:id' : {
		pageoptions: {
			title: 'tasks',
			type: 'Details',
			controller: 'tasks'
		},
		component: DetailForm
	},
	'/list/customergroups' : {
		pageoptions: {
			title: 'customergroups',
			type: 'List'
		},
		component: List
	},
	'/createCustomerGroup' : {
		pageoptions: {
			title: 'customergroups',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/customergroups/:id' : {
		pageoptions: {
			title: 'customergroups',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/leadsourcemaster' : {
		pageoptions: {
			title: 'leadsourcemaster',
			type: 'List'
		},
		component: List
	},
	'/createLeadSourceMaster' : {
		pageoptions: {
			title: 'leadsourcemaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/leadsourcemaster/:id' : {
		pageoptions: {
			title: 'leadsourcemaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/exclusivepartners' : {
		pageoptions: {
			title: 'exclusivepartners',
			type: 'List'
		},
		component: List
	},
	'/createExclusivePartner' : {
		pageoptions: {
			title: 'exclusivepartners',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/exclusivepartners/:id' : {
		pageoptions: {
			title: 'exclusivepartners',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/activitytypes' : {
		pageoptions: {
			title: 'activitytypes',
			type: 'List'
		},
		component: List
	},
	'/createActivitytype' : {
		pageoptions: {
			title: 'activitytypes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/activitytypes/:id' : {
		pageoptions: {
			title: 'activitytypes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/saleslostreasons' : {
		pageoptions: {
			title: 'saleslostreasons',
			type: 'List'
		},
		component: List
	},
	'/createSaleslostreason' : {
		pageoptions: {
			title: 'saleslostreasons',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/saleslostreasons/:id' : {
		pageoptions: {
			title: 'saleslostreasons',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/industries' : {
		pageoptions: {
			title: 'industries',
			type: 'List'
		},
		component: List
	},
	'/createIndustry' : {
		pageoptions: {
			title: 'industries',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/industries/:id' : {
		pageoptions: {
			title: 'industries',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/contracttypes' : {
		pageoptions: {
			title: 'contracttypes',
			type: 'List'
		},
		component: List
	},
	'/createContractType' : {
		pageoptions: {
			title: 'contracttypes',
			type: 'Details',
			controller: 'contracttypes'
		},
		component: DetailForm
	},
	'/details/contracttypes/:id' : {
		pageoptions: {
			title: 'contracttypes',
			type: 'Details',
			controller: 'contracttypes'
		},
		component: DetailForm
	},
	'/list/complaintcategory' : {
		pageoptions: {
			title: 'complaintcategory',
			type: 'List'
		},
		component: List
	},
	'/createComplaintCategory' : {
		pageoptions: {
			title: 'complaintcategory',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/complaintcategory/:id' : {
		pageoptions: {
			title: 'complaintcategory',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/equipments' : {
		pageoptions: {
			title: 'equipments',
			type: 'List'
		},
		component: List
	},
	'/createEquipment' : {
		pageoptions: {
			title: 'equipments',
			type: 'Details',
			controller: 'equipments'
		},
		component: DetailForm
	},
	'/details/equipments/:id' : {
		pageoptions: {
			title: 'equipments',
			type: 'Details',
			controller: 'equipments'
		},
		component: DetailForm
	},
	'/list/territories' : {
		pageoptions: {
			title: 'territories',
			type: 'List'
		},
		component: List
	},
	'/createTerritory' : {
		pageoptions: {
			title: 'territories',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/territories/:id' : {
		pageoptions: {
			title: 'territories',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/uom' : {
		pageoptions: {
			title: 'uom',
			type: 'List'
		},
		component: List
	},
	'/createUOM' : {
		pageoptions: {
			title: 'uom',
			type: 'Details',
			controller: 'uom'
		},
		component: DetailForm
	},
	'/details/uom/:id' : {
		pageoptions: {
			title: 'uom',
			type: 'Details',
			controller: 'uom'
		},
		component: DetailForm
	},
	'/list/itemgroups' : {
		pageoptions: {
			title: 'itemgroups',
			type: 'List'
		},
		component: List
	},
	'/createItemGroup' : {
		pageoptions: {
			title: 'itemgroups',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/itemgroups/:id' : {
		pageoptions: {
			title: 'itemgroups',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/itemcategorymaster' : {
		pageoptions: {
			title: 'itemcategorymaster',
			type: 'List'
		},
		component: List
	},
	'/createItemCategoryMaster' : {
		pageoptions: {
			title: 'itemcategorymaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/itemcategorymaster/:id' : {
		pageoptions: {
			title: 'itemcategorymaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/stocklocations' : {
		pageoptions: {
			title: 'stocklocations',
			type: 'List'
		},
		component: List
	},
	'/createStockLocation' : {
		pageoptions: {
			title: 'stocklocations',
			type: 'Details',
			controller: 'stocklocations'
		},
		component: DetailForm
	},
	'/details/stocklocations/:id' : {
		pageoptions: {
			title: 'stocklocations',
			type: 'Details',
			controller: 'stocklocations'
		},
		component: DetailForm
	},
	'/list/stockinoutreasoncodes' : {
		pageoptions: {
			title: 'stockinoutreasoncodes',
			type: 'List'
		},
		component: List
	},
	'/createStockinoutcode' : {
		pageoptions: {
			title: 'stockinoutreasoncodes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/stockinoutreasoncodes/:id' : {
		pageoptions: {
			title: 'stockinoutreasoncodes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/inspectionagents' : {
		pageoptions: {
			title: 'inspectionagents',
			type: 'List'
		},
		component: List
	},
	'/createInspectionagent' : {
		pageoptions: {
			title: 'inspectionagents',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/inspectionagents/:id' : {
		pageoptions: {
			title: 'inspectionagents',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/itemserialnos' : {
		pageoptions: {
			title: 'itemserialnos',
			type: 'List'
		},
		component: List
	},
	'/details/itemserialnos/:id' : {
		pageoptions: {
			title: 'itemserialnos',
			type: 'Details',
			controller: 'itemserialnos'
		},
		component: DetailForm
	},
	'/list/hsncodes' : {
		pageoptions: {
			title: 'hsncodes',
			type: 'List'
		},
		component: List
	},
	'/createHSNCodes' : {
		pageoptions: {
			title: 'hsncodes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/hsncodes/:id' : {
		pageoptions: {
			title: 'hsncodes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/workcenters' : {
		pageoptions: {
			title: 'workcenters',
			type: 'List'
		},
		component: List
	},
	'/createWorkcenter' : {
		pageoptions: {
			title: 'workcenters',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/workcenters/:id' : {
		pageoptions: {
			title: 'workcenters',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/processes' : {
		pageoptions: {
			title: 'processes',
			type: 'List'
		},
		component: List
	},
	'/createProcess' : {
		pageoptions: {
			title: 'processes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/processes/:id' : {
		pageoptions: {
			title: 'processes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/routings' : {
		pageoptions: {
			title: 'routings',
			type: 'List'
		},
		component: List
	},
	'/createRouting' : {
		pageoptions: {
			title: 'routings',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/routings/:id' : {
		pageoptions: {
			title: 'routings',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/bom' : {
		pageoptions: {
			title: 'bom',
			type: 'List'
		},
		component: List
	},
	'/createBOM' : {
		pageoptions: {
			title: 'bom',
			type: 'Details',
			controller: 'bom'
		},
		component: DetailForm
	},
	'/details/bom/:id' : {
		pageoptions: {
			title: 'bom',
			type: 'Details',
			controller: 'bom',
		},
		component: DetailForm
	},
	'/list/accountingformsmaster' : {
		pageoptions: {
			title: 'accountingformsmaster',
			type: 'List'
		},
		component: List
	},
	'/createAccountingformsmaster' : {
		pageoptions: {
			title: 'accountingformsmaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/accountingformsmaster/:id' : {
		pageoptions: {
			title: 'accountingformsmaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/banks' : {
		pageoptions: {
			title: 'banks',
			type: 'List'
		},
		component: List
	},
	'/createBank' : {
		pageoptions: {
			title: 'banks',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/banks/:id' : {
		pageoptions: {
			title: 'banks',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/paymentmodes' : {
		pageoptions: {
			title: 'paymentmodes',
			type: 'List'
		},
		component: List
	},
	'/createPaymentmode' : {
		pageoptions: {
			title: 'paymentmodes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/paymentmodes/:id' : {
		pageoptions: {
			title: 'paymentmodes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/expensecategories' : {
		pageoptions: {
			title: 'expensecategories',
			type: 'List'
		},
		component: List
	},
	'/createExpenseCategory' : {
		pageoptions: {
			title: 'expensecategories',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/expensecategories/:id' : {
		pageoptions: {
			title: 'expensecategories',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/holidays' : {
		pageoptions: {
			title: 'holidays',
			type: 'List'
		},
		component: List
	},
	'/createHoliday' : {
		pageoptions: {
			title: 'holidays',
			type: 'Details',
			controller: 'holidays'
		},
		component: DetailForm
	},
	'/details/holidays/:id' : {
		pageoptions: {
			title: 'holidays',
			type: 'Details',
			controller: 'holidays'
		},
		component: DetailForm
	},
	'/list/timesheetactivitytypes' : {
		pageoptions: {
			title: 'timesheetactivitytypes',
			type: 'List'
		},
		component: List
	},
	'/createTimeSheetActivityType' : {
		pageoptions: {
			title: 'timesheetactivitytypes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/timesheetactivitytypes/:id' : {
		pageoptions: {
			title: 'timesheetactivitytypes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/locations' : {
		pageoptions: {
			title: 'locations',
			type: 'List'
		},
		component: List
	},
	'/createLocation' : {
		pageoptions: {
			title: 'locations',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/locations/:id' : {
		pageoptions: {
			title: 'locations',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/departments' : {
		pageoptions: {
			title: 'departments',
			type: 'List'
		},
		component: List
	},
	'/createDepartment' : {
		pageoptions: {
			title: 'departments',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/departments/:id' : {
		pageoptions: {
			title: 'departments',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/paylevels' : {
		pageoptions: {
			title: 'paylevels',
			type: 'List'
		},
		component: List
	},
	'/createPayLevel' : {
		pageoptions: {
			title: 'paylevels',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/paylevels/:id' : {
		pageoptions: {
			title: 'paylevels',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/leavetypes' : {
		pageoptions: {
			title: 'leavetypes',
			type: 'List'
		},
		component: List
	},
	'/createLeaveType' : {
		pageoptions: {
			title: 'leavetypes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/leavetypes/:id' : {
		pageoptions: {
			title: 'leavetypes',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/leaveentitlements' : {
		pageoptions: {
			title: 'leaveentitlements',
			type: 'List'
		},
		component: List
	},
	'/createLeaveEntitlement' : {
		pageoptions: {
			title: 'leaveentitlements',
			type: 'Details',
			controller: 'leaveentitlements'
		},
		component: DetailForm
	},
	'/details/leaveentitlements/:id' : {
		pageoptions: {
			title: 'leaveentitlements',
			type: 'Details',
			controller: 'leaveentitlements'
		},
		component: DetailForm
	},
	'/list/payrollcomponents' : {
		pageoptions: {
			title: 'payrollcomponents',
			type: 'List'
		},
		component: List
	},
	'/createPayrollComponent' : {
		pageoptions: {
			title: 'payrollcomponents',
			type: 'Details',
			controller: 'payrollcomponents'
		},
		component: DetailForm
	},
	'/details/payrollcomponents/:id' : {
		pageoptions: {
			title: 'payrollcomponents',
			type: 'Details',
			controller: 'payrollcomponents'
		},
		component: DetailForm
	},
	'/list/countries' : {
		pageoptions: {
			title: 'countries',
			type: 'List'
		},
		component: List
	},
	'/createCountry' : {
		pageoptions: {
			title: 'countries',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/countries/:id' : {
		pageoptions: {
			title: 'countries',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/states' : {
		pageoptions: {
			title: 'states',
			type: 'List'
		},
		component: List
	},
	'/createState' : {
		pageoptions: {
			title: 'states',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/states/:id' : {
		pageoptions: {
			title: 'states',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/cities' : {
		pageoptions: {
			title: 'cities',
			type: 'List'
		},
		component: List
	},
	'/createCity' : {
		pageoptions: {
			title: 'cities',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/cities/:id' : {
		pageoptions: {
			title: 'cities',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/areas' : {
		pageoptions: {
			title: 'areas',
			type: 'List'
		},
		component: List
	},
	'/createArea' : {
		pageoptions: {
			title: 'areas',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/areas/:id' : {
		pageoptions: {
			title: 'areas',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/streets' : {
		pageoptions: {
			title: 'streets',
			type: 'List'
		},
		component: List
	},
	'/createStreet' : {
		pageoptions: {
			title: 'streets',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/streets/:id' : {
		pageoptions: {
			title: 'streets',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/costcentercategories' : {
		pageoptions: {
			title: 'costcentercategories',
			type: 'List'
		},
		component: List
	},
	'/createCostCenterCategories' : {
		pageoptions: {
			title: 'costcentercategories',
			type: 'Details',
			controller: 'costcentercategories'
		},
		component: DetailForm
	},
	'/details/costcentercategories/:id' : {
		pageoptions: {
			title: 'costcentercategories',
			type: 'Details',
			controller: 'costcentercategories.js'
		},
		component: DetailForm
	},
	'/createCostCenter' : {
		pageoptions: {
			title: 'costcenters',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/costcenters/:id' : {
		pageoptions: {
			title: 'costcenters',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/companymaster' : {
		pageoptions: {
			title: 'companymaster',
			type: 'List'
		},
		component: List
	},
	'/createCompany' : {
		pageoptions: {
			title: 'companymaster',
			type: 'Details',
			controller: 'companymaster'
		},
		component: DetailForm
	},
	'/details/companymaster/:id' : {
		pageoptions: {
			title: 'companymaster',
			type: 'Details',
			controller: 'companymaster'
		},
		component: DetailForm
	},
	'/list/accounts' : {
		pageoptions: {
			title: 'accounts',
			type: 'List'
		},
		component: List
	},
	'/createAccount' : {
		pageoptions: {
			title: 'accounts',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/accounts/:id' : {
		pageoptions: {
			title: 'accounts',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/taxexemptioncategories' : {
		pageoptions: {
			title: 'taxexemptioncategories',
			type: 'List'
		},
		component: List
	},
	'/createTaxExemptionCategory' : {
		pageoptions: {
			title: 'taxexemptioncategories',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/taxexemptioncategories/:id' : {
		pageoptions: {
			title: 'taxexemptioncategories',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/taxratemaster' : {
		pageoptions: {
			title: 'taxratemaster',
			type: 'List'
		},
		component: List
	},
	'/createTaxRate' : {
		pageoptions: {
			title: 'taxratemaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/taxratemaster/:id' : {
		pageoptions: {
			title: 'taxratemaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/taxtypemaster' : {
		pageoptions: {
			title: 'taxtypemaster',
			type: 'List'
		},
		component: List
	},
	'/createTaxType' : {
		pageoptions: {
			title: 'taxtypemaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/taxtypemaster/:id' : {
		pageoptions: {
			title: 'taxtypemaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/currency' : {
		pageoptions: {
			title: 'currency',
			type: 'List'
		},
		component: List
	},
	'/createCurrency' : {
		pageoptions: {
			title: 'currency',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/currency/:id' : {
		pageoptions: {
			title: 'currency',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/currencyexchangerates' : {
		pageoptions: {
			title: 'currencyexchangerates',
			type: 'List'
		},
		component: List
	},
	'/createCurrencyExchangeRate' : {
		pageoptions: {
			title: 'currencyexchangerates',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/currencyexchangerates/:id' : {
		pageoptions: {
			title: 'currencyexchangerates',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/landingcostmaster' : {
		pageoptions: {
			title: 'landingcostmaster',
			type: 'List'
		},
		component: List
	},
	'/createLandingCostMaster' : {
		pageoptions: {
			title: 'landingcostmaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/landingcostmaster/:id' : {
		pageoptions: {
			title: 'landingcostmaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/additionalchargesmaster' : {
		pageoptions: {
			title: 'additionalchargesmaster',
			type: 'List'
		},
		component: List
	},
	'/createAdditionalCharges' : {
		pageoptions: {
			title: 'additionalchargesmaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/additionalchargesmaster/:id' : {
		pageoptions: {
			title: 'additionalchargesmaster',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/leavecredits' : {
		pageoptions: {
			title: 'leavecredits',
			type: 'List'
		},
		component: List
	},
	'/createLeaveCredits' : {
		pageoptions: {
			title: 'leavecredits',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/leavecredits/:id' : {
		pageoptions: {
			title: 'leavecredits',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/purchaseorders' : {
		pageoptions: {
			title: 'purchaseorders',
			type: 'List'
		},
		component: List
	},
	'/createPurchaseOrder' : {
		pageoptions: {
			title: 'purchaseorders',
			type: 'Details',
			controller: 'purchaseorders'
		},
		component: DetailForm
	},
	'/details/purchaseorders/:id' : {
		pageoptions: {
			title: 'purchaseorders',
			type: 'Details',
			controller: 'purchaseorders'
		},
		component: DetailForm
	},
	'/list/taxexemptions' : {
		pageoptions: {
			title: 'taxexemptions',
			type: 'List'
		},
		component: List
	},
	'/createTaxExemptions' : {
		pageoptions: {
			title: 'taxexemptions',
			type: 'Details',
			controller: 'taxexemptions'
		},
		component: DetailForm
	},
	'/details/taxexemptions/:id' : {
		pageoptions: {
			title: 'taxexemptions',
			type: 'Details',
			controller: 'taxexemptions'
		},
		component: DetailForm
	},
	'/list/timesheets' : {
		pageoptions: {
			title: 'timesheets',
			type: 'List'
		},
		component: List
	},
	'/createTimeSheets' : {
		pageoptions: {
			title: 'timesheets',
			type: 'Details',
			controller: 'timesheets'
		},
		component: DetailForm
	},
	'/details/timesheets/:id' : {
		pageoptions: {
			title: 'timesheets',
			type: 'Details',
			controller: 'timesheets'
		},
		component: DetailForm
	},
	'/list/receiptnotes' : {
		pageoptions: {
			title: 'receiptnotes',
			type: 'List'
		},
		component: List
	},
	'/createReceiptNote' : {
		pageoptions: {
			title: 'receiptnotes',
			type: 'Details',
			controller: 'receiptnotes'
		},
		component: DetailForm
	},
	'/details/receiptnotes/:id' : {
		pageoptions: {
			title: 'receiptnotes',
			type: 'Details',
			controller: 'receiptnotes'
		},
		component: DetailForm
	},
	'/list/employees' : {
		pageoptions: {
			title: 'employees',
			type: 'List'
		},
		component: List
	},
	'/CreateEmployee' : {
		pageoptions: {
			title: 'employees',
			type: 'Details',
			controller: 'employees'
		},
		component: DetailForm
	},
	'/details/employees/:id' : {
		pageoptions: {
			title: 'employees',
			type: 'Details',
			controller: 'employees'
		},
		component: DetailForm
	},
	'/list/leadstages' : {
		pageoptions: {
			title: 'leadstages',
			type: 'List'
		},
		component: List
	},
	'/createLeadstage' : {
		pageoptions: {
			title: 'leadstages',
			type: 'Details',
			controller: 'leadstages'
		},
		component: DetailForm
	},
	'/details/leadstages/:id' : {
		pageoptions: {
			title: 'leadstages',
			type: 'Details',
			controller: 'leadstages'
		},
		component: DetailForm
	},
	'/list/projectstages' : {
		pageoptions: {
			title: 'projectstages',
			type: 'List'
		},
		component: List
	},
	'/createProjectstage' : {
		pageoptions: {
			title: 'projectstages',
			type: 'Details',
			controller: 'projectstages'
		},
		component: DetailForm
	},
	'/details/projectstages/:id' : {
		pageoptions: {
			title: 'projectstages',
			type: 'Details',
			controller: 'projectstages'
		},
		component: DetailForm
	},
	'/list/timesheetapproval' : {
 		pageoptions: {
 			title: 'timesheetapproval',
 			type: 'List'
 		},
 		component: List
 	},
 	'/createTimesheetApproval' : {
 		pageoptions: {
 			title: 'timesheetapproval',
 			type: 'Details',
 			controller: 'timesheetapproval'
 		},
 		component: DetailForm
 	},
 	'/details/timesheetapproval/:id' : {
 		pageoptions: {
 			title: 'timesheetapproval',
 			type: 'Details',
 			controller: 'timesheetapproval'
 		},
 		component: DetailForm
 	},
 	'/list/paystructures' : {
 		pageoptions: {
 			title: 'paystructures',
 			type: 'List'
 		},
 		component: List
 	},
 	'/createPayStructures' : {
 		pageoptions: {
 			title: 'paystructures',
 			type: 'Details',
 			controller: 'paystructures'
 		},
 		component: DetailForm
 	},
 	'/details/paystructures/:id' : {
 		pageoptions: {
 			title: 'paystructures',
 			type: 'Details',
 			controller: 'paystructures'
 		},
 		component: DetailForm
 	},
 	'/list/payrolls' : {
 		pageoptions: {
 			title: 'payrolls',
 			type: 'List'
 		},
 		component: List
 	},
 	'/createPayRoll' : {
 		pageoptions: {
 			title: 'payrolls',
 			type: 'Details',
 			controller: 'payrolls'
 		},
 		component: DetailForm
 	},
 	'/details/payrolls/:id' : {
 		pageoptions: {
 			title: 'payrolls',
 			type: 'Details',
 			controller: 'payrolls'
 		},
 		component: DetailForm
	},
	'/list/salesreturns' : {
		pageoptions: {
			title: 'salesreturns',
			type: 'List'
		},
		component: List
	},
	'/createSalesReturns' : {
		pageoptions: {
			title: 'salesreturns',
			type: 'Details',
			controller: 'salesreturns'
		},
		component: DetailForm
	},
	'/details/salesreturns/:id' : {
		pageoptions: {
			title: 'salesreturns',
			type: 'Details',
			controller: 'salesreturns'
		},
		component: DetailForm
	},
	'/list/purchasereturns' : {
		pageoptions: {
			title: 'purchasereturns',
			type: 'List'
		},
		component: List
	},
	'/createPurchaseReturns' : {
		pageoptions: {
			title: 'purchasereturns',
			type: 'Details',
			controller: 'purchasereturns'
		},
		component: DetailForm
	},
	'/details/purchasereturns/:id' : {
		pageoptions: {
			title: 'purchasereturns',
			type: 'Details',
			controller: 'purchasereturns'
		},
		component: DetailForm
	},
 	'/list/customercomplaints' : {
 		pageoptions: {
 			title: 'customercomplaints',
 			type: 'List'
 		},
 		component: List
 	},
 	'/createComplaints' : {
 		pageoptions: {
 			title: 'customercomplaints',
 			type: 'Details',
 			controller: 'customercomplaints'
 		},
 		component: DetailForm
 	},
 	'/details/customercomplaints/:id' : {
 		pageoptions: {
 			title: 'customercomplaints',
 			type: 'Details',
 			controller: 'customercomplaints'
 		},
 		component: DetailForm
 	},
 	'/list/competativequotes' : {
 		pageoptions: {
 			title: 'competativequotes',
 			type: 'List'
 		},
 		component: List
 	},
 	'/createCompetativeQuote' : {
 		pageoptions: {
 			title: 'competativequotes',
 			type: 'Details',
 			controller: 'competativequotes'
 		},
 		component: DetailForm
 	},
 	'/details/competativequotes/:id' : {
 		pageoptions: {
 			title: 'competativequotes',
 			type: 'Details',
 			controller: 'competativequotes'
 		},
 		component: DetailForm
	},
	'/createAddress' : {
		pageoptions: {
			title: 'addresses',
			type: 'Details',
			controller: 'addresses'
		},
		component: DetailForm
	},
	'/details/address/:id' : {
		pageoptions: {
			title: 'addresses',
			type: 'Details',
			controller: 'addresses'
		},
		component: DetailForm

	},
	'/list/contractenquiries' : {
		pageoptions: {
			title: 'contractenquiries',
			type: 'List'
		},
		component: List
	},
	'/createContractEnquiry' : {
		pageoptions: {
			title: 'contractenquiries',
			type: 'Details',
			controller: 'contractenquiries'
		},
		component: DetailForm
	},
	'/details/contractenquiries/:id' : {
		pageoptions: {
			title: 'contractenquiries',
			type: 'Details',
			controller: 'contractenquiries'
		},
		component: DetailForm
	},
	'/list/contracts' : {
		pageoptions: {
			title: 'contracts',
			type: 'List'
		},
		component: List
	},
	'/createContract' : {
		pageoptions: {
			title: 'contracts',
			type: 'Details',
			controller: 'contracts'
		},
		component: DetailForm
	},
	'/details/contracts/:id' : {
		pageoptions: {
			title: 'contracts',
			type: 'Details',
			controller: 'contracts'
		},
		component: DetailForm
	},
	'/list/purchaseinvoices' : {
		pageoptions: {
			title: 'purchaseinvoices',
			type: 'List'
		},
		component: List
	},
	'/createPurchaseInvoice' : {
		pageoptions: {
			title: 'purchaseinvoices',
			type: 'Details',
			controller: 'purchaseinvoices'
		},
		component: DetailForm
	},
	'/details/purchaseinvoices/:id' : {
		pageoptions: {
			title: 'purchaseinvoices',
			type: 'Details',
			controller: 'purchaseinvoices'
		},
		component: DetailForm
	},
	'/list/supplierquotation' : {
		pageoptions: {
			title: 'supplierquotation',
			type: 'List'
		},
		component: List
	},
	'/createSupplierQuotation' : {
		pageoptions: {
			title: 'supplierquotation',
			type: 'Details',
			controller: 'supplierquotation'
		},
		component: DetailForm
	},
	'/details/supplierquotation/:id' : {
		pageoptions: {
			title: 'supplierquotation',
			type: 'Details',
			controller: 'supplierquotation'
		},
		component: DetailForm
	},
	'/list/rfq' : {
		pageoptions: {
			title: 'rfq',
			type: 'List'
		},
		component: List
	},
	'/createRfq' : {
		pageoptions: {
			title: 'rfq',
			type: 'Details',
			controller: 'rfq'
		},
		component: DetailForm
	},
	'/details/rfq/:id' : {
		pageoptions: {
			title: 'rfq',
			type: 'Details',
			controller: 'rfq'
		},
		component: DetailForm
	},
	'/list/workorders' : {
		pageoptions: {
			title: 'workorders',
			type: 'List'
		},
		component: List
	},
	'/createWorkOrder' : {
		pageoptions: {
			title: 'workorders',
			type: 'Details',
			controller: 'workorders'
		},
		component: DetailForm
	},
	'/details/workorders/:id' : {
		pageoptions: {
			title: 'workorders',
			type: 'Details',
			controller: 'workorders'
		},
		component: DetailForm
	},
	'/list/email' : {
		pageoptions: {
			title: 'email',
			type: 'List'
		},
		component: List
	},
	'/details/email/:id' : {
		pageoptions: {
			title: 'email',
			type: 'Details',
			controller: 'email'
		},
		component: DetailForm
	},
	'/list/roles' : {
		pageoptions: {
			title: 'roles',
			type: 'List'
		},
		component: List
	},
	'/createRole' : {
		pageoptions: {
			title: 'roles',
			type: 'Details',
			controller: 'roles'
		},
		component: DetailForm
	},
	'/details/roles/:id' : {
		pageoptions: {
			title: 'roles',
			type: 'Details',
			controller: 'roles'
		},
		component: DetailForm
	},
	'/list/numberingseriesmaster' : {
		pageoptions: {
			title: 'numberingseriesmaster',
			type: 'List'
		},
		component: List
	},
	'/createNumberingseriesMaster' : {
		pageoptions: {
			title: 'numberingseriesmaster',
			type: 'Details',
			controller: 'numberingseriesmaster'
		},
		component: DetailForm
	},
	'/details/numberingseriesmaster/:id' : {
		pageoptions: {
			title: 'numberingseriesmaster',
			type: 'Details',
			controller: 'numberingseriesmaster'
		},
		component: DetailForm
	},
	'/list/termsandconditions' : {
		pageoptions: {
			title: 'termsandconditions',
			type: 'List'
		},
		component: List
	},
	'/createTermsandCondition' : {
		pageoptions: {
			title: 'termsandconditions',
			type: 'Details',
			controller: 'termsandconditions'
		},
		component: DetailForm
	},
	'/details/termsandconditions/:id' : {
		pageoptions: {
			title: 'termsandconditions',
			type: 'Details',
			controller: 'termsandconditions'
		},
		component: DetailForm
	},
	'/list/emailtemplates' : {
		pageoptions: {
			title: 'emailtemplates',
			type: 'List'
		},
		component: List
	},
	'/createEmailTemplate' : {
		pageoptions: {
			title: 'emailtemplates',
			type: 'Details',
			controller: 'emailtemplates'
		},
		component: DetailForm
	},
	'/details/emailtemplates/:id' : {
		pageoptions: {
			title: 'emailtemplates',
			type: 'Details',
			controller: 'emailtemplates'
		},
		component: DetailForm
	},
	'/list/projectquotes' : {
		pageoptions: {
			title: 'projectquotes',
			type: 'List'
		},
		component: List
	},
	'/createProjectQuote' : {
		pageoptions: {
			title: 'projectquotes',
			type: 'Details',
			controller: 'projectquotes'
		},
		component: DetailForm
	},
	'/details/projectquotes/:id' : {
		pageoptions: {
			title: 'projectquotes',
			type: 'Details',
			controller: 'projectquotes'
		},
		component: DetailForm
	},
	'/list/projects' : {
		pageoptions: {
			title: 'projects',
			type: 'List'
		},
		component: List
	},
	'/createProject' : {
		pageoptions: {
			title: 'projects',
			type: 'Details',
			controller: 'projects'
		},
		component: DetailForm
	},
	'/details/projects/:id' : {
		pageoptions: {
			title: 'projects',
			type: 'Details',
			controller: 'projects'
		},
		component: DetailForm
	},
	'/list/productionorders' : {
		pageoptions: {
			title: 'productionorders',
			type: 'List'
		},
		component: List
	},
	'/createProductionOrder' : {
		pageoptions: {
			title: 'productionorders',
			type: 'Details',
			controller: 'productionorders'
		},
		component: DetailForm
	},
	'/details/productionorders/:id' : {
		pageoptions: {
			title: 'productionorders',
			type: 'Details',
			controller: 'productionorders'

		},
		component: DetailForm
	},
	'/list/productionschedule' : {
		pageoptions: {
			title: 'productionschedule',
			type: 'List'
		},
		component: List
	},
	'/details/productionschedule/:id' : {
		pageoptions: {
			title: 'productionschedule',
			type: 'Details',
			controller: 'common'

		},
		component: DetailForm
	},
	'/list/stocktransfer' : {
		pageoptions: {
			title: 'stocktransfer',
			type: 'List'
		},
		component: List
	},
	'/createStockTransfer' : {
		pageoptions: {
			title: 'stocktransfer',
			type: 'Details',
			controller: 'stocktransfer'
		},
		component: DetailForm
	},
	'/details/stocktransfer/:id' : {
		pageoptions: {
			title: 'stocktransfer',
			type: 'Details',
			controller: 'stocktransfer'

		},
		component: DetailForm
	},
	'/list/receiptvouchers' : {
		pageoptions: {
			title: 'receiptvouchers',
			type: 'List'
		},
		component: List
	},
	'/createReceiptVoucher' : {
		pageoptions: {
			title: 'receiptvouchers',
			type: 'Details',
			controller: 'receiptvouchers'
		},
		component: DetailForm
	},
	'/details/receiptvouchers/:id' : {
		pageoptions: {
			title: 'receiptvouchers',
			type: 'Details',
			controller: 'receiptvouchers'

		},
		component: DetailForm
	},
	'/list/paymentvouchers' : {
		pageoptions: {
			title: 'paymentvouchers',
			type: 'List'
		},
		component: List
	},
	'/createPaymentVoucher' : {
		pageoptions: {
			title: 'paymentvouchers',
			type: 'Details',
			controller: 'paymentvouchers'
		},
		component: DetailForm
	},
	'/details/paymentvouchers/:id' : {
		pageoptions: {
			title: 'paymentvouchers',
			type: 'Details',
			controller: 'paymentvouchers'

		},
		component: DetailForm
	},
	'/list/journalvouchers' : {
		pageoptions: {
			title: 'journalvouchers',
			type: 'List'
		},
		component: List
	},
	'/createJournalVoucher' : {
		pageoptions: {
			title: 'journalvouchers',
			type: 'Details',
			controller: 'journalvouchers'
		},
		component: DetailForm
	},
	'/details/journalvouchers/:id' : {
		pageoptions: {
			title: 'journalvouchers',
			type: 'Details',
			controller: 'journalvouchers'

		},
		component: DetailForm
	},
	'/list/creditnotes' : {
		pageoptions: {
			title: 'creditnotes',
			type: 'List'
		},
		component: List
	},
	'/createCreditNote' : {
		pageoptions: {
			title: 'creditnotes',
			type: 'Details',
			controller: 'creditnotes'
		},
		component: DetailForm
	},
	'/details/creditnotes/:id' : {
		pageoptions: {
			title: 'creditnotes',
			type: 'Details',
			controller: 'creditnotes'

		},
		component: DetailForm
	},
	'/list/debitnotes' : {
		pageoptions: {
			title: 'debitnotes',
			type: 'List'
		},
		component: List
	},
	'/createDebitNote' : {
		pageoptions: {
			title: 'debitnotes',
			type: 'Details',
			controller: 'debitnotes'
		},
		component: DetailForm
	},
	'/details/debitnotes/:id' : {
		pageoptions: {
			title: 'debitnotes',
			type: 'Details',
			controller: 'debitnotes'

		},
		component: DetailForm
	},
	'/list/landingcosts' : {
		pageoptions: {
			title: 'landingcosts',
			type: 'List'
		},
		component: List
	},
	'/createLandingCost' : {
		pageoptions: {
			title: 'landingcosts',
			type: 'Details',
			controller: 'landingcosts'
		},
		component: DetailForm
	},
	'/details/landingcosts/:id' : {
		pageoptions: {
			title: 'landingcosts',
			type: 'Details',
			controller: 'landingcosts'

		},
		component: DetailForm
	},
	'/list/printmodules' : {
		pageoptions: {
			title: 'printmodules',
			type: 'List'
		},
		component: List
	},
	'/createPrintModule' : {
		pageoptions: {
			title: 'printmodules',
			type: 'Details',
			controller: 'printmodules'
		},
		component: DetailForm
	},
	'/details/printmodules/:id' : {
		pageoptions: {
			title: 'printmodules',
			type: 'Details',
			controller: 'printmodules'
		},
		component: DetailForm
	},
	'/list/utilitymodules' : {
		pageoptions: {
			title: 'utilitymodules',
			type: 'List'
		},
		component: List
	},
	'/createUtilityModule' : {
		pageoptions: {
			title: 'utilitymodules',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/utilitymodules/:id' : {
		pageoptions: {
			title: 'utilitymodules',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/emailaccounts' : {
		pageoptions: {
			title: 'emailaccounts',
			type: 'List'
		},
		component: List
	},
	'/createEmailAccounts' : {
		pageoptions: {
			title: 'emailaccounts',
			type: 'Details',
			controller: 'emailaccounts'
		},
		component: DetailForm
	},
	'/details/emailaccounts/:id' : {
		pageoptions: {
			title: 'emailaccounts',
			type: 'Details',
			controller: 'emailaccounts'
		},
		component: DetailForm
	},
	'/dashboardsetting' : {
		pageoptions: {
			title: 'dashboardsetting',
			type: 'Details',
			controller: 'dashboardsetting'
		},
		component: DetailForm
	},
	'/list/itemmaster' : {
		pageoptions: {
			title: 'itemmaster',
			type: 'List'
		},
		component: List
	},
	'/createItemMaster' : {
		pageoptions: {
			title: 'itemmaster',
			type: 'Details',
			controller: 'itemmaster'
		},
		component: DetailForm
	},
	'/details/itemmaster/:id' : {
		pageoptions: {
			title: 'itemmaster',
			type: 'Details',
			controller: 'itemmaster'
		},
		component: DetailForm
	},
	'/list/stockreservations' : {
		pageoptions: {
			title: 'stockreservations',
			type: 'List'
		},
		component: List
	},
	'/createStockReservation' : {
		pageoptions: {
			title: 'stockreservations',
			type: 'Details',
			controller: 'stockreservations'
		},
		component: DetailForm
	},
	'/details/stockreservations/:id' : {
		pageoptions: {
			title: 'stockreservations',
			type: 'Details',
			controller: 'stockreservations'
		},
		component: DetailForm
	},
	'/list/marketingevents' : {
		pageoptions: {
			title: 'marketingevents',
			type: 'List'
		},
		component: List
	},
	'/createMarketingEvent' : {
		pageoptions: {
			title: 'marketingevents',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/marketingevents/:id' : {
		pageoptions: {
			title: 'marketingevents',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/salestargets' : {
		pageoptions: {
			title: 'salestargets',
			type: 'List'
		},
		component: List
	},
	'/createSalesTarget' : {
		pageoptions: {
			title: 'salestargets',
			type: 'Details',
			controller: 'salestargets'
		},
		component: DetailForm
	},
	'/details/salestargets/:id' : {
		pageoptions: {
			title: 'salestargets',
			type: 'Details',
			controller: 'salestargets'
		},
		component: DetailForm
	},
	'/list/printtemplates' : {
		pageoptions: {
			title: 'printtemplates',
			type: 'List'
		},
		component: List
	},
	'/createPrintTemplate' : {
		pageoptions: {
			title: 'printtemplates',
			type: 'Details',
			controller: 'printtemplates'
		},
		component: DetailForm
	},
	'/details/printtemplates/:id' : {
		pageoptions: {
			title: 'printtemplates',
			type: 'Details',
			controller: 'printtemplates'
		},
		component: DetailForm
	},
	'/list/itembatches' : {
		pageoptions: {
			title: 'itembatches',
			type: 'List'
		},
		component: List
	},
	'/createItemBatch' : {
		pageoptions: {
			title: 'itembatches',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/details/itembatches/:id' : {
		pageoptions: {
			title: 'itembatches',
			type: 'Details',
			controller: 'common'
		},
		component: DetailForm
	},
	'/list/users' : {
		pageoptions: {
			title: 'users',
			type: 'List'
		},
		component: List
	},
	'/CreateUser' : {
		pageoptions: {
			title: 'users',
			type: 'Details',
			controller: 'users'
		},
		component: DetailForm
	},
	'/details/users/:id' : {
		pageoptions: {
			title: 'users',
			type: 'Details',
			controller: 'users'
		},
		component: DetailForm
	},
	'/list/taxcodemaster' : {
		pageoptions: {
			title: 'taxcodemaster',
			type: 'List'
		},
		component: List
	},
	'/createTaxCodeMaster' : {
		pageoptions: {
			title: 'taxcodemaster',
			type: 'Details',
			controller: 'taxcodemaster'
		},
		component: DetailForm
	},
	'/details/taxcodemaster/:id' : {
		pageoptions: {
			title: 'taxcodemaster',
			type: 'Details',
			controller: 'taxcodemaster'
		},
		component: DetailForm
	},
	'/list/financialyears' : {
		pageoptions: {
			title: 'financialyears',
			type: 'List'
		},
		component: List
	},
	'/createFinancialyear' : {
		pageoptions: {
			title: 'financialyears',
			type: 'Details',
			controller: 'financialyears'
		},
		component: DetailForm
	},
	'/details/financialyears/:id' : {
		pageoptions: {
			title: 'financialyears',
			type: 'Details',
			controller: 'financialyears'
		},
		component: DetailForm
	},
	'/list/pricelists' : {
		pageoptions: {
			title: 'pricelists',
			type: 'List'
		},
		component: List
	},
	'/createPriceList' : {
		pageoptions: {
			title: 'pricelists',
			type: 'Details',
			controller: 'pricelists'
		},
		component: DetailForm
	},
	'/details/pricelists/:id' : {
		pageoptions: {
			title: 'pricelists',
			type: 'Details',
			controller: 'pricelists'
		},
		component: DetailForm
	},
	'/details/pricelistversion/:id' : {
		pageoptions: {
			title: 'pricelistversions',
			type: 'Details',
			controller: 'pricelistversions'
		},
		component: DetailForm
	},
	'/createBIconfig' : {
		pageoptions: {
			title: 'biconfigurations',
			type: 'Details',
			controller: 'biconfigurations'
		},
		component: DetailForm
	},
	'/details/biconfigurations/:id' : {
		pageoptions: {
			title: 'biconfigurations',
			type: 'Details',
			controller: 'biconfigurations'
		},
		component: DetailForm
	},
	'/leadsreport' : {
		component: ReportForm,
		title: 'Leads Report'
	},
	'/deliverynoteitemsreport' : {
		component: ReportForm,
		title: 'Delivery Note Items Report'
	},
	'/receiptnoteitemsreport' : {
		component: ReportForm,
		title: 'Receipt Note Items Report'
	},
	'/servicereportdetailsreport' : {
		component: ReportForm,
		title: 'Service Report Details Report'
	},
	'/salesactivityreport' : {
		component: SalesActivityReportForm,
		title: 'Sales Activity Report'
	},
	'/salesregisterreport' : {
		component: SalesRegisterReportForm,
		title: 'Sales Register Report'
	},
	'/quotationitemsreport' : {
		component: QuotationItemsReportForm,
		title: 'Quotation Items Report'
	},
	'/orderitemsreport' : {
		component: OrderItemsReportForm,
		title: 'Order Items Report'
	},
	'/leaditemsreport' : {
		component: LeadItemsReportForm,
		title: 'Lead Items Report'
	},
	'/projectquoteitemsreport' : {
		component: ProjectQuoteItemsReportForm,
		title: 'Project Quote Items Report'
	},
	'/projectsreport' : {
		component: ProjectsReportForm,
		title: 'Project Items Report'
	},
	'/projectprofitabilityreport' : {
		component: ProjectsProfitabilityReportForm,
		title: 'Project Profitability Report'
	},
	'/purchaseregisterreport' : {
		component: PurchaseRegisterReportForm,
		title: 'Purchase Register'
	},
	'/stocktransactionhistory' : {
		component: StockTransactionHistoryReportForm,
		title: 'Stock Transaction History'
	},
	'/salesinvoiceitemsreport' : {
		component: SalesInvoiceItemsReportForm,
		title: 'Sales Invoice Items Report'
	},
	'/salesprofitabilityreport' : {
		component: SalesProfitabilityReportForm,
		title: 'Sales Profitability Report'
	},
	'/smsreport' : {
		component: ReportForm,
		title: 'SMS Report'
	},
	'/leaveledgerreport' : {
		component: ReportForm,
		title: 'Leave Ledger Report'
	},
	'/projectmaterialmovementreport' : {
		component: ReportForm,
		title: 'Project Material Movement'
	},
	'/hsncodesreport' : {
		component: ReportForm,
		title: 'HSN Codes Report'
	},
	'/engineersbufferstockreport' : {
		component: ReportForm,
		title: 'Engineer Buffer Stock Report'
	},
	'/landingcostreport' : {
		component: ReportForm,
		title: 'Landing Cost Report'
	},
	'/spareconsumptionreport' : {
		component: ReportForm,
		title: 'Spare Consumption Report'
	},
	'/contractbillingschedulereport' : {
		component: ReportForm,
		title: 'Contract Billing Schedule Report'
	},
	'/timesheetreport' : {
		component: TimesheetReportForm,
		title: 'Time Sheet Report'
	},
	'/attendancereport' : {
		component: AttendanceReportForm,
		title: 'Attendance Report'
	},
	'/contactsreport' : {
		component: ContactsReportForm,
		title: 'Contacts Report'
	},
	'/projectpurchaseplannerreport' : {
		component: ProjectPurchasePlannerReportForm,
		title: 'Project Purchase Planner Report'
	},
	'/contractitemsreport' : {
		component : ContractItemsReportForm,
		title: 'Contract Items Report'
	},
	'/contractprofitabilityreport' : {
		component: ReportForm,
		title: 'Contract Profitability Report'
	},
	'/equipmentschedulereport' : {
		component : EquipmentScheduleReportForm,
		title: 'Equipment Schedule Report'
	},
	'/contractsdueforrenewalreport' : {
		component : ContractDueforRenewalReportForm,
		title: 'Contract Renewal Report'
	},
	'/unsettledreceiptspaymentsreport' : {
		component : UnsettledReceiptPaymentReportForm,
		title: 'Unsettled Receipt and Payments'
	},
	'/servicecallallotmentreport' : {
		component : ServicecallAllotmentReportForm,
		title: 'Service Call Assignment'
	},
	'/engineerperformancereport' : {
		component : EngineerPerformanceReportForm,
		title: 'Engineer Performance Report'
	},
	'/accountsreceivablereport' : {
		component : AccountReceivableReportForm,
		title: 'Accounts Receivable Report'
	},
	'/accountspayablereport' : {
		component : AccountPayableReportForm,
		title: 'Accounts Payable Report'
	},
	'/generalledgerreport' : {
		component : GeneralLedgerReportForm,
		title: 'General Ledger Report'
	},
	'/generalledgercostsplitupreport' : {
		component : GeneralLedgerCostSplitUpReportForm,
		title: 'General Ledger Cost Splitup Report'
	},
	'/accountbalancereport' : {
		component : AccountBalanceReportForm,
		title: 'Accounts Balance Report'
	},
	'/cashbookreport' : {
		component : CashBookReportForm,
		title: 'Cash Book Report'
	},
	'/salesperformancereport' : {
		component : SalesPerformanceReportForm,
		title: 'Sales Performance Report'
	},
	'/stockstatementreport' : {
		component : StockStatementReportForm,
		title: 'Stock Statement'
	},
	'/contractenquiryitemsreport' : {
		component : ContractEnquiryItemsReportForm,
		title: 'Contract Enquiry Items Report'
	},
	'/productionorderstatusreport' : {
		component: ReportForm,
		title: 'Production Order Status Report'
	},
	'/pendingservicecallreport' : {
		component: ReportForm,
		title: 'Pending Service Calls Report'
	},
	'/servicecalldetailsreport' : {
		component: ReportForm,
		title: 'Service Call Details Report'
	},
	'/servicecallprofitabilityreport' : {
		component: ReportForm,
		title: 'Service Call Profitability Report'
	},
	'/serialnostatusreport' : {
		component : SerialnoStatusReportForm,
		title: 'Serial No Status Report'
	},
	'/serialnohistoryreport' : {
		component : SerialnoHistoryReportForm,
		title: 'Serial No History Report'
	},
	'/itemrequestitemsreport' : {
		component : ItemRequestItemsReportForm,
		title: 'Item Request Items Report'
	},
	'/purchaseorderitemsreport' : {
		component : ReportForm,
		title: 'Purchase Order Items Report'
	},
	'/purchaseplannerreport' : {
		component : PurchasePlannerReportForm,
		title: 'Purchase Planner Report'
	},
	'/stockageingreport' : {
		component : StockAgeingReportForm,
		title: 'Stock Ageing Report'
	},
	'/purchaseinvoiceitemsreport' : {
		component : PurchaseInvoiceItemsReportForm,
		title: 'Purchase Invoice Items Report'
	},
	'/materialavailabilityreport' : {
		component : MaterialAvailabilityReportForm,
		title: 'Material Availability Report'
	},
	'/stockasondatereport' : {
		component : StockAsonDateReportForm,
		title: 'Stock Valuation Report'
	},
	'/customersearchreport' : {
		component : CustomerSearchReportForm,
		title: 'Customer Search'
	},
	'/servicedashboard' : {
		component: ServiceDashboardForm,
		title: 'Service Dashboard'
	},
	'/stockbybatchreport' : {
		component: StockBatchReportForm,
		title: 'Stock by Batch Report'
	},
	'/stockbatchhistoryreport' : {
		component: StockBatchHistoryReportForm,
		title: 'Stock by Batch History'
	},
	'/activitycalendar' : {
		component: ActivityCalendarForm,
		title: 'Activity Calendar'
	},
	'/customerstatementreport' : {
		component: CustomerStatementForm,
		title: 'Customer Statement'
	},
	'/collectionstatusreport' : {
		component: CollectionStatusReportForm,
		title: 'Collection Status'
	},
	'/loginregisterreport' : {
		component: ReportForm,
		title: 'Login Register'
	},
	'/targetactualsreport' : {
		component: TargetActualsReportForm,
		title: 'Target vs Actuals'
	},
	'/bankreconciliationreport' : {
		component: BankReconciliationReportForm,
		title: 'Bank Reconciliation Report'
	},
	'/employeelocationsreport' : {
		component: EmployeeLocationsReportForm,
		title: 'Employee Locations Report'
	},
	'/attendancecapture' : {
		component: AttendanceCapture,
		title: 'Attendance Capture'
	},
	'/createAttendanceCapture' : {
		pageoptions: {
			title: 'attendancecapture',
			type: 'Details',
			controller: 'attendancecapture'
		},
		component: DetailForm
	},
	'/details/attendancecapture/:id' : {
		pageoptions: {
			title: 'attendancecapture',
			type: 'Details',
			controller: 'attendancecapture'
		},
		component: DetailForm
	},
	'/documentlibrary' : {
		component: DocumentLibraryForm,
		title: 'Document Library'
	},
	'/settings' : {
		component: Settings,
		title: 'Module Settings'
	},
	'/permissions' : {
		component: Permissions,
		title: 'Permisssions'
	},
	'/teamstructure' : {
		component: TeamStructure,
		title: 'Team Structure'
	},
	'/createTeamStructure' : {
		pageoptions: {
			title: 'teamstructure',
			type: 'Details',
			controller: 'teamstructure'
		},
		component: DetailForm
	},
	'/details/teamstructure/:id' : {
		pageoptions: {
			title: 'teamstructure',
			type: 'Details',
			controller: 'teamstructure'
		},
		component: DetailForm
	},
	"/deliverynoteplannerreport" : {
		component: DeliveryNotePlannerReportForm,
		title: 'Delivery Note Planner'
	},
	'/balancesheetreport' : {
		component: BalanceSheetReportForm,
		title: 'Balance Sheet'
	},
	'/profitlossreport' : {
		component: ProfitLossReportForm,
		title: 'Profit and Loss'
	},
	"/receiptnoteplannerreport" : {
		component: ReceiptNotePlannerReportForm,
		title: 'Receipt Note Planner'
	},
	"/salesinvoicetoolreport" : {
		component: SalesInvoicePlannerReportForm,
		title: 'Sales Invoice Tool'
	},
	"/purchaseinvoicetoolreport" : {
		component: PurchaseInvoicePlannerReportForm,
		title: 'Purchase Invoice Tool'
	},
	'/list/accountsfollowup' : {
		pageoptions: {
			title: 'accountsfollowup',
			type: 'List'
		},
		component: List
	},
	'/details/accountsfollowup/:id' : {
		pageoptions: {
			title: 'accountsfollowup',
			type: 'Details',
			controller: 'accountsfollowup'
		},
		component: DetailForm
	},
	/*"/importCustomers" : {
		component: ImportCustomer,
		title: 'Import Customer'
	},
	"/importItemMaster" : {
		component: ImportItemMaster,
		title: 'Import Items'
	},*/
	"/importSalesInvoice" : {
		component: ImportSalesInvoice,
		title: 'Import Sales Invoice'
	},
	"/importPurchaseInvoice" : {
		component: ImportPurchaseInvoice,
		title: 'Import Purchase Invoice'
	},
	"/accountingformsreport" : {
		component: AccountingFormsReportForm,
		title: 'Accounting Forms'
	},
	"/gstr1" : {
		component: GSTR1ReportForm,
		title: 'GSTR-1 Report'
	},
	'/customreport/:name' : {
		component: CustomReportForm,
		title: 'Report'
	},
	'/salespipelinereport' : {
		component: SalesPipelineReportForm,
		title: 'Sales Pipe Line Report'
	},
	'/changepassword' : {
		component: ChangePasswordForm,
		title: 'Change Password'
	},
	'/preferences' : {
		component: PreferencesForm,
		title: 'Preferences'
	},
	'/list/orderacknowledgements' : {
		pageoptions: {
			title: 'orderacknowledgements',
			type: 'List'
		},
		component: List
	},
	'/details/orderacknowledgements/:id' : {
		component: OrderAcknowledgement
	},
	'/list/orderchecklists' : {
		component: OrderCheckList
	},
	'/createProjectSchedules' : {
		pageoptions: {
			title: 'projectschedules',
			type: 'Details',
			controller: 'projectschedules'
		},
		component: DetailForm
	},
	'/details/projectschedules/:id' : {
		pageoptions: {
			title: 'projectschedules',
			type: 'Details',
			controller: 'projectschedules'
		},
		component: DetailForm
	},
	'/productionschedulereport' : {
		component: ProductionScheduleReportForm,
		title: 'Production Schedule Report'
	},
	'/ordercalendar' : {
		component: OrderCalendarForm,
		title: 'Order Calendar'
	},
	'/list/payrollbatch' : {
 		pageoptions: {
 			title: 'payrollbatch',
 			type: 'List'
 		},
 		component: List
 	},
	'/createPayrollBatch' : {
		title: 'Payroll Batch',
		component: PayrollBatchForm
	},
	'/details/payrollbatch/:id' : {
		title: 'Payroll Batch',
		component: PayrollBatchForm
	},
	"/payrollreport" : {
		component: PayrollReportForm,
		title: 'Payroll Report'
	},
	"/deliveryrouteplannerreport" : {
		component: DeliveryRoutePlannerReportForm,
		title: 'Delivery Route Planner Report'
	},
	'/list/projectestimations' : {
		pageoptions: {
			title: 'projectestimations',
			type: 'List'
		},
		component: List
	},
	'/createProjectEstimation' : {
		pageoptions: {
			title: 'projectestimations',
			type: 'Details',
			controller: 'projectestimations'
		},
		component: DetailForm
	},
	'/details/projectestimations/:id' : {
		pageoptions: {
			title: 'projectestimations',
			type: 'Details',
			controller: 'projectestimations'
		},
		component: DetailForm
	},
	'/list/projectestimationtemplate' : {
		pageoptions: {
			title: 'projectestimationtemplate',
			type: 'List'
		},
		component: List
	},
	'/createProjectEstimationTemplate' : {
		pageoptions: {
			title: 'projectestimationtemplate',
			type: 'Details',
			controller: 'projectestimationtemplate'
		},
		component: DetailForm
	},
	'/details/projectestimationtemplate/:id' : {
		pageoptions: {
			title: 'projectestimationtemplate',
			type: 'Details',
			controller: 'projectestimationtemplate'
		},
		component: DetailForm
	},
	'/projectdeliveryplannerreport' : {
		component: ProjectDeliveryPlannerReportForm,
		title: 'Project Delivery Planner Reprt'
	},
	'/list/workprogress' : {
		pageoptions: {
			title: 'workprogress',
			type: 'List'
		},
		component: List
	},
	'/createWorkProgress' : {
		pageoptions: {
			title: 'workprogress',
			type: 'Details',
			controller: 'workprogress'
		},
		component: DetailForm
	},
	'/details/workprogress/:id' : {
		pageoptions: {
			title: 'workprogress',
			type: 'Details',
			controller: 'workprogress'
		},
		component: DetailForm
	},
	'/contractorpaymentreport' : {
		component: ProjectContractorPaymentReportForm,
		title: 'Contractor Payment Report'
	},
	'/projectcostvariancereport' : {
		component: ProjectCostVarianceReportForm,
		title: 'Project Cost Variance Report'
	},
	'/projectprofitvariancereport' : {
		component: ProjectProfitVarianceReportForm,
		title: 'Project Profit Variance Report'
	},
	'/list/equipmentschedules' : {
		pageoptions: {
			title: 'equipmentschedules',
			type: 'List'
		},
		component: List
	},
	'/details/equipmentschedules/:id' : {
		pageoptions: {
			title: 'equipmentschedules',
			type: 'Details',
			controller : 'equipmentschedules'
		},
		component: DetailForm
	},
	'/createListView' : {
		component: ListviewComponent,
		title: 'Create List View'
	},
	'/details/views/:id' : {
		component: ListviewComponent,
		title: 'Edit List View'
	},
	'/list/dataimports' : {
		pageoptions: {
			title: 'dataimports',
			type: 'List'
		},
		component: List
	},
	'/createDataImport' : {
		title: 'New Data Import',
		component: DataImport
	},
	'/details/dataimports/:id' : {
		component: DataImport
	},
	'/callstatusreport' : {
		component: CallStatusReportForm,
		title: 'Call Status Report'
	},
	'/list/datasets' : {
		pageoptions: {
			title: 'datasets',
			type: 'List'
		},
		component: List
	},
	'/createDataset' : {
		title: 'Datasets',
		component: DataSet
	},
	'/details/datasets/:id' : {
		title: 'Datasets',
		component: DataSet
	},
	'/list/reports' : {
		pageoptions: {
			title: 'reports',
			type: 'List'
		},
		component: List
	},
	'/createReport' : {
		title: 'Report Builder',
		component: ReportBuilderConfigForm
	},
	'/details/reports/:id' : {
		title: 'Report Builder',
		component: ReportBuilderConfigForm
	},
	'/reportbuilder/:id' : {
		component: ReportBuilderForm
	},
	'/list/milestonestages' : {
		pageoptions: {
			title: 'milestonestages',
			type: 'List'
		},
		component: List
	},
	'/createMilestoneStage' : {
		pageoptions: {
			title: 'milestonestages',
			type: 'Details',
			controller: 'milestonestages'
		},
		component: DetailForm
	},
	'/details/milestonestages/:id' : {
		pageoptions: {
			title: 'milestonestages',
			type: 'Details',
			controller: 'milestonestages'
		},
		component: DetailForm
	},
	'/list/milestonetemplates' : {
		pageoptions: {
			title: 'milestonetemplates',
			type: 'List'
		},
		component: List
	},
	'/createMilestoneTemplate' : {
		pageoptions: {
			title: 'milestonetemplates',
			type: 'Details',
			controller: 'milestonetemplates'
		},
		component: DetailForm
	},
	'/details/milestonetemplates/:id' : {
		pageoptions: {
			title: 'milestonetemplates',
			type: 'Details',
			controller: 'milestonetemplates'
		},
		component: DetailForm
	},
	'/list/milestoneprogress' : {
		pageoptions: {
			title: 'milestoneprogress',
			type: 'List'
		},
		component: List
	},
	'/createMilestoneProgress' : {
		pageoptions: {
			title: 'milestoneprogress',
			type: 'Details',
			controller: 'milestoneprogress'
		},
		component: DetailForm
	},
	'/details/milestoneprogress/:id' : {
		pageoptions: {
			title: 'milestoneprogress',
			type: 'Details',
			controller: 'milestoneprogress'
		},
		component: DetailForm
	},
	'/milestoneprogresssummaryreport' : {
		component: ProjectMilestoneProgressSummaryReportForm,
		title: 'Milestone Progress Summary Report'
	},
	'/list/leavecreditbatch' : {
		pageoptions: {
			title: 'leavecreditbatch',
			type: 'List'
		},
		component: List
	},
	'/createLeaveCreditBatch' : {
		title: 'Leave Batch',
		component: LeaveCreditBatchForm
	},
	'/details/leavecreditbatch/:id' : {
		title: 'Leave Batch',
		component: LeaveCreditBatchForm
	},
	'/reconcilewip' : {
		component : ReconcileWIP,
		title: 'Reconcile WIP'
	},
	'/forcechangepassword' : {
		component: ForceChangePasswordForm,
		title: 'Force Change Password'
	},
	'/list/performancetargets' : {
		pageoptions: {
			title: 'performancetargets',
			type: 'List'
		},
		component: List
	},
	'/createPerformanceTarget' : {
		title: 'Performance Targets',
		component: PerformanceTarget
	},
	'/details/performancetargets/:id' : {
		title: 'Performance Targets',
		component: PerformanceTarget
	},
	'/performancetargetdashboard' : {
		title: 'My Performance Targets',
		component: PerformanceTargetDashBoard
	},
	'/performancetargetreport' : {
		title: 'Performance Target Report',
		component: PerformanceTargetReport
	},
	'/list/dashboardbuilder': {
		pageoptions: {
			title: 'dashboard',
			type: 'List'
		},
		component: List
	},
	'/createDashBoard': {
		title: 'New Dashboard Builder',
		component: DashboardBuilderForm
	},
	'/details/dashboardbuilder/:id': {
		title: 'Dashboard Builder',
		component: DashboardBuilderForm
	},
	'/dashboardwidget/:id': {
		component: DashboardWidgetForm
	},
	'/createDataAccess' : {
		title: 'Data Access',
		component: DataAccessForm
	},
	'/list/dataaccess' : {
		pageoptions: {
			title: 'dataaccess',
			type: 'List'
		},
		component: List
	},
	'/details/dataaccess/:id' : {
		title: 'Data Access',
		component: DataAccessForm
	},
	'/list/proformainvoices' : {
		pageoptions: {
			title: 'proformainvoices',
			type: 'List'
		},
		component: List
	},
	'/createProformaInvoice' : {
		pageoptions: {
			title: 'proformainvoices',
			type: 'Details',
			controller: 'proformainvoices'
		},
		component: DetailForm
	},
	'/details/proformainvoices/:id' : {
		pageoptions: {
			title: 'proformainvoices',
			type: 'Details',
			controller: 'proformainvoices'
		},
		component: DetailForm
	},
	'/list/activities' : {
		pageoptions: {
			title: 'activities',
			type: 'List'
		},
		component: List
	},
	'/createActivity' : {
		pageoptions: {
			title: 'activities',
			type: 'Details',
			controller: 'activities'
		},
		component: DetailForm
	},
	'/details/activities/:id' : {
		pageoptions: {
			title: 'activities',
			type: 'Details',
			controller: 'activities'
		},
		component: DetailForm
	},
	'/serviceengineerassignment': {
		component: ServiceEngineerAssignment
	},
	'/userlogreport' : {
		title: 'User Log Report',
		component: UserLogReportForm
	}
}
