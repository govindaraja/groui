import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';
import { currencyFilter } from '../utils/filter';
import { LocalSelect } from '../components/utilcomponents';
import Loadingcontainer from '../components/loadingcontainer';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			basedon : this.props.app.feature.runInCRMMode ? 'defaultcost' : (this.props.app.feature.quoteprofitabilitybasedon ? this.props.app.feature.quoteprofitabilitybasedon : 'averagecost'),
			loaderflag: true,
			profitarray: [],
			costarray: []
		};
		this.getProfitAnalysis = this.getProfitAnalysis.bind(this);
		this.basedonOnChange = this.basedonOnChange.bind(this);
		this.getStockdetails = this.getStockdetails.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
	}

	componentWillMount() {
		this.callCalculate()
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	callCalculate() {
		if(this.props.app.feature.runInCRMMode) {
			this.getProfitAnalysis((itemArray) => {
				this.calculateProfit(itemArray);
			});
		} else {
			this.getProfitAnalysis((itemArray, itemidArray, warehouseArray) => {
				if(this.state.basedon == 'averagecost') {
					this.getStockdetails(itemArray, itemidArray, warehouseArray, () => {
						this.calculateProfit(itemArray);
					});
				} else if(this.state.basedon == 'averagelandingcost') {
					this.getReceiptRatedetails(itemArray, itemidArray, warehouseArray, () => {
						this.calculateProfit(itemArray);
					});
				} else {
					this.calculateProfit(itemArray);
				}
			});
		}
	}

	calculateProfit(itemArray) {
		var costnotFound = false, revenue = 0, cost = 0, profit, profitpercentage;
		itemArray.map((a) => {
			if(a.kitArray.length > 0) {
				let cost = a.itemid_hasaddons ? (a.cost == null ? 0 : a.cost) : 0;
				a.kitArray.map((b) => {
					cost += ((b.cost != null ? b.cost : 0) * b.qty);
				});
				a.cost = cost;
			} else {
				if(a.cost != null)
					a.cost = ((a.cost * a.costconversionfactor) / a.rateconversionfactor)
			}
			revenue += (a.rate * a.qty);
			if(a.cost == null) {
				costnotFound = true;
			} else {
				cost += (a.cost * a.qty);
				a.profit = a.rate - a.cost;
				if(a.cost)
					a.profitpercentage = a.rate > 0 ? Number(((a.profit / a.rate) * 100).toFixed(2)) : null;
				else
					costnotFound = true;
			}
		});
		if(!costnotFound) {
			profit = revenue - cost;
			profitpercentage = revenue > 0 ? Number(((profit / revenue) * 100).toFixed(2)) : null;
		}

		this.setState({
			profitarray: itemArray,
			revenue,
			cost,
			profit,
			profitpercentage
		});
		this.updateLoaderFlag(false);
	}

	basedonOnChange(value) {
		this.updateLoaderFlag(true);
		this.setState({
			basedon : value
		}, () => {
			this.callCalculate();
		});
	}

	getStockdetails(itemArray, itemidArray, warehouseArray, callback) {
		
		let filterString = [`itemid=${itemidArray}`];
		if(warehouseArray.length > 0)
			filterString.push(`warehouseid=${warehouseArray}`);

		axios.get(`/api/query/stockquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let costObjWW = {};
				let costObj = {};
				response.data.main.map((a) => {
					if(a.marate !== null) {
						if(!costObj[a.itemid])
							costObj[a.itemid] = {};
						costObj[a.itemid][a.warehouseid] = {
							marate : a.marate ? a.marate : 0,
							onhandqty: a.onhandqty ? a.onhandqty : 0
						};
					}
				});
				for(var prop in costObj) {
					var tempQty = 0;
					var tempValue = 0;
					for(var secprop in costObj[prop]) {
						tempQty += costObj[prop][secprop].onhandqty;
						tempValue += (costObj[prop][secprop].onhandqty * costObj[prop][secprop].marate);
					}
					costObjWW[prop] = tempValue/tempQty;
				}
				itemArray.map((a) => {
					if(a.kitArray.length > 0) {
						a.kitArray.map((b) => {
							if(a.warehouseid > 0) {
								if(costObj[b.itemid] && costObj[b.itemid][a.warehouseid]) {
									b.cost = costObj[b.itemid][a.warehouseid].marate;
									b.costconversionfactor = 1;
								}
							} else if(costObjWW[b.itemid]) {
								b.cost = costObjWW[b.itemid];
								b.costconversionfactor = 1;
							}
						});
					}
					if(a.kitArray.length == 0 || a.itemid_hasaddons){
						if(a.warehouseid > 0) {
							if(costObj[a.itemid] && costObj[a.itemid][a.warehouseid]) {
								a.cost = costObj[a.itemid][a.warehouseid].marate;
								a.costconversionfactor = 1;
							}
						} else if(costObjWW[a.itemid]) {
							a.cost = costObjWW[a.itemid];
							a.costconversionfactor = 1;
						}
					}
				});
				callback();
			} else {
				var apiResponse = commonMethods.apiResult(response);
				modalService[apiResponse.methodName](apiResponse.message);
			}
		});
	}

	getReceiptRatedetails(itemArray, itemidArray, warehouseArray, callback) {
		
		let filterString = [`itemid=${itemidArray}`];
		if(warehouseArray.length > 0)
			filterString.push(`warehouseid=${warehouseArray}`);

		axios.get(`/api/query/getreceiptitemratequery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let costObjWW = {};
				let costObj = {};
				response.data.main.map((a) => {
					if(a.marate !== null) {
						if(!costObj[a.itemid])
							costObj[a.itemid] = {};
						if(warehouseArray.length > 0) {
							costObj[a.itemid][a.warehouseid] = {
								marate : a.marate ? a.marate : 0,
								onhandqty: a.onhandqty ? a.onhandqty : 0,
								landingcostmultiple: a.landingcostmultiple ? a.landingcostmultiple : 1
							};
						} else {
							costObj[a.itemid] = {
								marate : a.marate ? a.marate : 0,
								onhandqty: a.onhandqty ? a.onhandqty : 0,
								landingcostmultiple: a.landingcostmultiple ? a.landingcostmultiple : 1
							};
						}
					}
				});
				for(var prop in costObj) {
					var tempQty = 0;
					var tempValue = 0;
					if(warehouseArray.length > 0) {
						for(var secprop in costObj[prop]) {
							tempQty += costObj[prop][secprop].onhandqty;
							tempValue += (costObj[prop][secprop].onhandqty * costObj[prop][secprop].marate);
						}
					} else {
						tempQty += costObj[prop].onhandqty;
						tempValue += (costObj[prop].onhandqty * costObj[prop].marate);
					}
					costObjWW[prop] = tempValue/tempQty;
				}
				itemArray.map((a) => {
					if(a.kitArray.length > 0) {
						a.kitArray.map((b) => {
							if(a.warehouseid > 0) {
								if(costObj[b.itemid] && costObj[b.itemid][a.warehouseid]) {
									b.cost = costObj[b.itemid][a.warehouseid].marate * costObj[b.itemid][a.warehouseid].landingcostmultiple;
									b.costconversionfactor = 1;
								}
							} else if(costObjWW[b.itemid]) {
								b.cost = costObjWW[b.itemid] * costObj[b.itemid].landingcostmultiple;
								b.costconversionfactor = 1;
							}
						});
					}
					if(a.kitArray.length == 0 || a.itemid_hasaddons){
						if(a.warehouseid > 0) {
							if(costObj[a.itemid] && costObj[a.itemid][a.warehouseid]) {
								a.cost = costObj[a.itemid][a.warehouseid].marate * costObj[a.itemid][a.warehouseid].landingcostmultiple;
								a.costconversionfactor = 1;
							}
						} else if(costObjWW[a.itemid]) {
							a.cost = costObjWW[a.itemid] * costObj[a.itemid].landingcostmultiple;
							a.costconversionfactor = 1;
						}
					}
				});
				callback();
			} else {
				var apiResponse = commonMethods.apiResult(response);
				modalService[apiResponse.methodName](apiResponse.message);
			}
		});
	}

	getProfitAnalysis(callback) {
		let itemArray = [],
		warehouseArray = [],
		itemDetailArray = [],
		childName = '',
		kitChildName = '';
		if(this.props.param == 'quotes')
			childName = 'quoteitems', kitChildName='kititemquotedetails';
		if(this.props.param == 'orders')
			childName = 'orderitems', kitChildName='kititemorderdetails';

		for (var i = 0; i < this.props.resource[childName].length; i++) {
			let temprate = this.props.resource[childName][i].rate ? this.props.resource[childName][i].finalratelc : 0;
			var tempObj = {
				itemid : this.props.resource[childName][i].itemid,
				itemid_name : this.props.resource[childName][i].itemid_name,
				itemid_hasaddons : this.props.resource[childName][i].itemid_hasaddons,
				warehouseid : this.props.resource[childName][i].warehouseid,
				rate : temprate,
				rateconversionfactor: this.props.resource[childName][i].uomconversionfactor ? this.props.resource[childName][i].uomconversionfactor : 1,
				cost : null,
				amount : temprate * this.props.resource[childName][i].quantity,
				qty : this.props.resource[childName][i].quantity,
				kitArray : []
			};

			for(var j = 0; j < this.props.resource[kitChildName].length; j++) {
				if(this.props.resource[kitChildName][j].rootindex == this.props.resource[childName][i].index && !this.props.resource[kitChildName][j].itemid_issaleskit) {
					tempObj.kitArray.push({
						itemid : this.props.resource[kitChildName][j].itemid,
						itemid_name : this.props.resource[kitChildName][j].itemid_name,
						rateconversionfactor: 1,
						cost : null,
						qty : this.props.resource[kitChildName][j].quantity / this.props.resource[childName][i].quantity
					});
					itemArray.push(this.props.resource[kitChildName][j].itemid);
				}
			}

			itemDetailArray.push(tempObj);
			if(this.props.param == 'orders' && !warehouseArray.includes(this.props.resource[childName][i].warehouseid))
				warehouseArray.push(this.props.resource[childName][i].warehouseid);
			itemArray.push(this.props.resource[childName][i].itemid);
		}
		axios.get(`/api/itemmaster?field=id,defaultpurchasecost,purchaseuomid,stockuomid,landingcostmultiple,allowpurchase&filtercondition=itemmaster.id IN (${itemArray.join()})`).then((response) => {
			if (response.data.message == 'success') {
				axios.get(`/api/alternativeuoms?field=id,parentid,uomid,conversionfactor&filtercondition=alternativeuoms.parentid IN (${itemArray.join()})`).then((secresponse) => {
					if(response.data.message == 'success') {
						internalGetProfitAnalysis(response, secresponse);
					} else {
						var apiResponse = commonMethods.apiResult(response);
						modalService[apiResponse.methodName](apiResponse.message);
					}
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				modalService[apiResponse.methodName](apiResponse.message);
			}

			let internalGetProfitAnalysis = (response, secresponse) => {
				let uomObj = {};
				secresponse.data.main.forEach((uomitem) => {
					if(!uomObj[uomitem.parentid])
						uomObj[uomitem.parentid] = {};

					uomObj[uomitem.parentid][uomitem.uomid] = uomitem.conversionfactor;
				});
				let costObj = {};
				for (var k = 0; k < response.data.main.length; k++)
					costObj[response.data.main[k].id] = {
						cost: response.data.main[k].defaultpurchasecost,
						purchaseuomid: response.data.main[k].allowpurchase ? response.data.main[k].purchaseuomid : response.data.main[k].stockuomid,
						stockuomid: response.data.main[k].stockuomid,
						landingcostmultiple: response.data.main[k].landingcostmultiple
					};

				itemDetailArray.map((a) => {
					a.cost = ['averagecost', 'averagelandingcost'].includes(this.state.basedon) ? null : (costObj[a.itemid] && costObj[a.itemid].cost != null) ? costObj[a.itemid].cost * (costObj[a.itemid].landingcostmultiple || 1) : null;
					a.purchaseuomid = (costObj[a.itemid] && costObj[a.itemid].purchaseuomid != null) ? costObj[a.itemid].purchaseuomid : null;
					a.stockuomid = (costObj[a.itemid] && costObj[a.itemid].stockuomid != null) ? costObj[a.itemid].stockuomid : null;
					a.costconversionfactor = costObj[a.itemid].stockuomid == costObj[a.itemid].purchaseuomid ? 1 : (uomObj[a.itemid][costObj[a.itemid].purchaseuomid] ? uomObj[a.itemid][costObj[a.itemid].purchaseuomid] : 1);
					a.kitArray.map((b) => {
						b.cost = ['averagecost', 'averagelandingcost'].includes(this.state.basedon) ? null : (costObj[b.itemid] && costObj[b.itemid].cost != null) ? costObj[b.itemid].cost * (costObj[b.itemid].landingcostmultiple || 1) : null;
						b.purchaseuomid = (costObj[b.itemid] && costObj[b.itemid].purchaseuomid != null) ? costObj[b.itemid].purchaseuomid : null;
						b.stockuomid = (costObj[b.itemid] && costObj[b.itemid].stockuomid != null) ? costObj[b.itemid].stockuomid : null;
						b.costconversionfactor = costObj[b.itemid].stockuomid == costObj[b.itemid].purchaseuomid ? 1 : (uomObj[b.itemid][costObj[b.itemid].purchaseuomid] ? uomObj[b.itemid][costObj[b.itemid].purchaseuomid] : 1);
					});
				});
				callback(itemDetailArray, itemArray, warehouseArray);
			}
		});
	}

	renderBasedOn() {
		if(this.props.app.feature.runInCRMMode)
			return null;
		return (
			<div className="col-md-8 form-group">
				<label className="labelclass">Base On</label>
				<LocalSelect options={[{label: 'Average Cost', value: 'averagecost'}, {label: 'Default Cost', value: 'defaultcost'}, {label: 'Average Landing Cost', value: 'averagelandingcost'}]} label="label" valuename="value" value={this.state.basedon} onChange={this.basedonOnChange} />
			</div>
		);
	}

	render() {
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Profitability Analysis</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row justify-content-center">
						{this.renderBasedOn()}
					</div>
					<div>
						<div className="col-md-12 col-sm-12 col-xs-12">
							<table className="table">
								<thead>
									<tr>
										<th className="text-center">Item</th>
										<th className="text-right">Selling Price</th>
										<th className="text-right">Cost Price</th>
										<th className="text-right">Gross Margin %</th>
									</tr>
								</thead>
								<tbody>
									 {this.state.profitarray.map((item, index) => {
										return (<tr key={index}>
											<td>{item.itemid_name}</td>
											<td className="text-right">{currencyFilter(item.rate, null, this.props.app)}</td>
											<td className="text-right">{currencyFilter(item.cost, null, this.props.app)}</td>
											<td className="text-right">{item.profitpercentage}</td>
										</tr>)
									 })}
								</tbody>
							</table>
						</div>
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="col-md-8 pull-right bgColor-gray borderradius-6">
								<table className="width-100 lineheight-2">
									<tbody>
										<tr>
											<td className="text-right">Total Revenue</td>
											<td>:</td>
											<td className="text-right">{currencyFilter(this.state.revenue, null, this.props.app)}</td>
										</tr>
										{this.state.profit ? <tr>
											<td className="text-right">Total Cost</td>
											<td>:</td>
											<td className="text-right">{currencyFilter(this.state.cost, null, this.props.app)}</td>
										</tr> : null }
										{this.state.profit ? <tr>
											<td className="text-right">Gross Margin</td>
											<td>:</td>
											<td className="text-right">{currencyFilter(this.state.profit, null, this.props.app)}</td>
										</tr> : null }
										{this.state.profitpercentage ? <tr>
											<td className="text-right">Gross Margin %</td>
											<td>:</td>
											<td className="text-right">{this.state.profitpercentage}</td>
										</tr> : null }
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
