import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import Dropzone from 'react-dropzone'

import { updateFormState, updateAppState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import { LocalSelect, Buttongroup } from '../components/utilcomponents';
import Loadingcontainer from '../components/loadingcontainer';
import { currencyFilter, datetimeFilter, dateFilter } from '../utils/filter';
import { checkActionVerbAccess } from '../utils/utils';

class DocumentLibraryForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getFolderAndFiles = this.getFolderAndFiles.bind(this);
		this.openGoogleDrive = this.openGoogleDrive.bind(this);
		this.onPicked = this.onPicked.bind(this);
		this.DropBoxFiles = this.DropBoxFiles.bind(this);
		this.uploadFile = this.uploadFile.bind(this);
		this.checkFileExist = this.checkFileExist.bind(this);
		this.uploadFilefn = this.uploadFilefn.bind(this);
		this.libraryAssetsOperation = this.libraryAssetsOperation.bind(this);
		this.getDestinationIcon = this.getDestinationIcon.bind(this);
		this.getCustomFiles = this.getCustomFiles.bind(this);
		this.downloadFile = this.downloadFile.bind(this);
		this.renderImageFolder = this.renderImageFolder.bind(this);
		this.renderOtherFolder = this.renderOtherFolder.bind(this);
	}

	componentWillMount() {
		let tempObj = {
			documentList: [],
			imageList: [],
			breadcrumbs: [],
			library: {},
		};
		this.props.initialize(tempObj);
		setTimeout(this.getFolderAndFiles, 0);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	getFolderAndFiles = (item) => {
		this.updateLoaderFlag(true);
		let { breadcrumbs } = this.props.resource;
		let documentList = [], imageList = [];
		let library = {};
		let parentId = ((item && item.id > 0) ? item.id : null);		

		axios.get(`api/query/getlibraryquery?parentId=${parentId}`).then((response)=> {
			if (response.data.message == 'success') {
				documentList = response.data.main;

				documentList.sort((a,b)=> {
					if(a.isparent && b.isparent) {
						if(a.name.toLowerCase() > b.name.toLowerCase()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						if(a.isparent) {
							return -1;
						} else if (b.isparent) {
							return 1;
						} else {
							if(a.name.toLowerCase() > b.name.toLowerCase()) {
								return 1;
							} else {
								return -1;
							}
						}
					}
				});

				let accessFound = false;
				let accessDetails;

				documentList.forEach(function (libItem) {
					if(libItem.parentid == parentId) {
						accessFound = true;
						accessDetails = libItem;
					}
				});

				if(!accessFound && parentId > 0) {
					accessDetails = {
						readaccess : true,
						saveaccess : true,
						deleteaccess : true
					}
				}


				library.parentid = parentId;
				library.fullpath = ((item && item.fullpath) ? item.fullpath : '');
				library.readaccess = (parentId>0 ? accessDetails.readaccess : true);
				library.saveaccess = (parentId>0 ? accessDetails.saveaccess : true);
				library.deleteaccess = (parentId>0 ? accessDetails.deleteaccess : true);

				if (library.parentid > 0) {
					let itemFound = false;
					for (var i = 0; i < breadcrumbs.length; i++) {
						if (breadcrumbs[i].id == library.parentid) {
							itemFound = true;
							break;
						}
					}

					if (itemFound) {
						breadcrumbs.splice(i + 1, (breadcrumbs.length - i));
					} else {
						breadcrumbs.push({
							id : item.id,
							name : item.name,
							readaccess : item.readaccess,
							saveaccess : item.saveaccess,
							deleteaccess : item.deleteaccess
						});
					}
				} else {
					breadcrumbs = [{
							id : 0,
							name : 'Libraries',
							readaccess : true,
							saveaccess : true,
							deleteaccess : true
						}
					];
					imageList = [{
						id : 'Images',
						name : 'Images'
					}];
				}

				this.props.updateFormState(this.props.form, {
					breadcrumbs,
					imageList,
					documentList,
					library
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	};

	openGoogleDrive = () => {
		commonMethods.openGoogleDrive(this.props, this.onPicked);
	};

	onPicked = (docs) => {
		let files = [];

		docs.forEach((file, index) => {
			if(file.uploadId == undefined)
				files.push(file);
		});

		if(files.length == 1) {
			files[0].destination = 'googledrive';
			this.libraryAssetsOperation('Save', files[0]);
		}
	};

	DropBoxFiles = () => {
		let options = {
			success: (files) => {
				let dropBoxfiles = [];
				dropBoxfiles.push(files[0]);
				dropBoxfiles[0].destination = 'dropbox';

				this.libraryAssetsOperation('Save', dropBoxfiles[0]);
			},
			cancel: () => {
				console.log("CANCEL")
			},
			linkType: "preview",
			multiselect: false,
		};
		Dropbox.choose(options);
	};

	libraryAssetsOperation = (param, data) => {
		this.updateLoaderFlag(true);
		let tempObj = {};
		if(param == 'Save' && data.destination == 'googledrive') {
			tempObj = {
				parentresource : 'libraries',
				resourceName : 'libraries',
				type : 'File',
				destination : data.destination,
				name : data.name,
				isparent : false,
				fileid : data.id,
				parentid : this.props.resource.library.parentid,
				fileurl : data.url
			};
		}

		if(param == 'Save' && data.destination == 'dropbox') {
			tempObj = {
				parentresource : 'libraries',
				resourceName : 'libraries',
				type : 'File',
				destination : data.destination,
				name : data.name,
				isparent : false,
				fileid : data.id.split(':')[1],
				parentid : this.props.resource.library.parentid,
				fileurl : data.link
			};
		}

		axios({
			method : 'POST',
			data : {
				actionverb : param,
				data : (param == 'Save' && (['googledrive', 'dropbox'].indexOf(data.destination) >= 0)) ? tempObj : data
			},
			url : '/api/libraries'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			if (response.data.message == 'success') {
				this.getFolderAndFiles({
					id : this.props.resource.library.parentid
				});
			}
			this.updateLoaderFlag(false);
		});
	};

	uploadFile = (files) => {
		if (files && files.length > 1)
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Please drag and drop one file at a time",
				btnArray : ["Ok"]
			}));

		let fileData = {};
		if(!files)
			fileData = files;
		else
			fileData = files[0];

		if(this.props.resource.library.parentid == 'Images' || this.props.resource.library.parentid == 'private') {
			if(!fileData.name.match(/\.(jpg|jpeg|png|gif)$/))
				return this.props.openModal(modalService.infoMethod({
					header : "Error",
					body : "Please upload image file only",
					btnArray : ["Ok"]
				}));
			else
				this.checkFileExist(fileData);
		} else
			this.checkFileExist(fileData);
	};

	checkFileExist = (fileData) => {
		let { imageList, documentList } = this.props.resource;
		let paramFound = false;
		for (var i = 0; i < documentList.length; i++) {
			if (fileData.name == documentList[i].name && documentList[i].destination == 'libraries') {
				fileData['id'] = documentList[i].id;
				fileData['fileurl'] = documentList[i].fileurl;
				fileData['modified'] = documentList[i].modified;
				paramFound = true;
				break;
			}
		}
		for (var i = 0; i < imageList.length; i++) {
			if (fileData.name == imageList[i].name) {
				paramFound = true;
				break;
			}
		}

		if(paramFound) {
			this.props.openModal(modalService['confirmMethod']({
				header : 'File Replacement',
				body : 'A file already exists with the same name. Do you want to replace it?',
				btnArray : ['Yes', 'No']
			}, (param) => {
				if(param) {
					this.uploadFilefn(fileData);
				}
			}));
		} else {
			this.uploadFilefn(fileData);
		}
	};

	uploadFilefn = (fileData) => {
		this.updateLoaderFlag(true);
		let tempData = {
			parentresource : 'libraries',
			resourceName : 'libraries',
			type : 'File',
			parentid : this.props.resource.library.parentid,
			destination : 'libraries',
			filesize : fileData.size
		};

		if(fileData.id > 0) {
			tempData.id = fileData.id;
			tempData.fileurl = fileData.fileurl;
			tempData.modified = fileData.modified;
			tempData.name = fileData.name;
		}

		const formData = new FormData();
		formData.append('file', fileData);
		formData.append('data', JSON.stringify(tempData));
		formData.append('actionverb', 'Save');

		const config = {
			headers: {
				'content-type': 'multipart/form-data'
			}
		};

		let fileName = fileData.name.substr(0, fileData.name.lastIndexOf('.'));

		if(!(/^[a-zA-Z0-9\.\,\_\-\'\!\*\(\)\ ]*$/.test(fileName))) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Invalid file name. Please change file name. file name contain invalid specical characters. Allowed special characters are '_ - . , ! ( ) *'",
				btnArray : ["Ok"]
			}));
		}

		let tempurl = (this.props.resource.library.parentid == 'Images' || this.props.resource.library.parentid == 'private') ? '/publicimgupload' : '/upload'

		axios.post(tempurl, formData, config).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

			if (response.data.message == 'success') {
				(this.props.resource.library.parentid == 'Images' || this.props.resource.library.parentid == 'private') ? this.getCustomFiles({id : this.props.resource.library.parentid, name : this.props.resource.library.parentid}, 'get') : this.getFolderAndFiles({id : this.props.resource.library.parentid});
			}
			this.updateLoaderFlag(false);
		});
	};

	getCustomFiles = (item, type) => {
		this.updateLoaderFlag(true);
		let foldername = "", filterSrting = "";
		let documentList = [], imageList = [];
		let { library, breadcrumbs } = this.props.resource;

		if(type == 'get') {
			foldername = item.name
			filterSrting = `param=get&foldername=${item.name}`;
		}

		if(type == 'delete') {
			foldername = library.parentid;
			filterSrting = `param=delete&foldername=${library.parentid}&imagename=${item.name}`;
		}

		axios.get(`/api/query/getcustomimagequery?${filterSrting}`).then((response) => {
			if (response.data.message == 'success') {
				imageList = response.data.main;
				imageList.sort((a,b) => {
					if(!a.name.match(/\.(jpg|jpeg|png|gif)$/) > !b.name.match(/\.(jpg|jpeg|png|gif)$/)) {
						return -1;
					} else {
						return 1;
					}
				});
				library.parentid = foldername;
				if (library.parentid) {
					let itemFound = false;
					for (var i = 0; i < breadcrumbs.length; i++) {
						if (breadcrumbs[i].id == library.parentid) {
							itemFound = true;
							break;
						}
					}

					if (itemFound) {
						breadcrumbs.splice(i + 1, (breadcrumbs.length - i));
					} else {
						breadcrumbs.push({
							id : foldername,
							name : foldername
						});
					}
				} else {
					breadcrumbs = [{
						id : 0,
						name : 'Libraries'
					}];
					imageList = [{
						id : 'Images',
						name : 'Images'
					}];
				}

				this.props.updateFormState(this.props.form, {
					breadcrumbs,
					imageList,
					documentList,
					library
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	};

	downloadFile = (item) => {
		this.updateLoaderFlag(true);
		let win;

		if(['googledrive', 'dropbox'].indexOf(item.destination) >= 0)
			win = window.open(item.fileurl, '_blank');
		else
			win = window.open(`/documents/${item.name}?url=${item.fileurl}`, '_blank');

		if (!win)
			this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				body : "Popup Blocker is enabled! Please add this site to your exception list.",
				btnArray : ['Ok']
			}));

		this.updateLoaderFlag(false);
	};

	getDestinationIcon = (destination) => {
		if(['googledrive', 'dropbox'].indexOf(destination) >= 0) {
			if (destination == 'googledrive') {
				return 'fa-google';
			}
			else if (destination == 'dropbox') {
				return 'fa-dropbox';
			}
		} else
			return "fa-file";
	};

	openLibraryModal = (type, item) => {
		if(type == 'Folder' && (this.props.resource.library.parentid == 'Images' || this.props.resource.library.parentid == 'private')) {
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Sorry, you cannot create folder",
				btnArray : ["Ok"]
			}));
		} else {
			this.props.openModal({
				render: (closeModal) => {
					return <AssetModal assetType={type} parentLibrary={this.props.resource.library} documentList={this.props.resource.documentList} imageList={this.props.resource.imageList} editasset={item ? JSON.parse(JSON.stringify(item)) : null} app={this.props.app} callback={(assetType, data) => {
						data.parentid = data.id ? data.parentid : this.props.resource.library.parentid;
						data.destination = data.id ? data.destination : 'libraries';
						return this.libraryAssetsOperation('Save', data);
					}} openModal={this.openModal} closeModal={closeModal} />
				}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
			});
		}
	};

	renderImageFolder = () => {
		return this.props.resource.imageList.map((item, index) => {
			return (
				<div key={index} className="col-md-2 col-sm-4 col-xs-6 text-center" align="center" style={{padding: '5px', maxHeight: '180px', marginBottom: '10px'}}>
					<div className="col-md-12 col-sm-12 col-xs-12">
						{!item.path ? <span onClick={() => this.getCustomFiles(item, 'get')} style={{cursor: 'pointer'}} className="fa fa-4x fa-folder-o"></span> : null }
						{item.path ? <img src={`${item.path}`} style={{maxHeight: '100px', maxWidth: '100px', height: '90px'}}/> : null }
						<div style={{wordWrap: 'break-word'}}>
							{item.name}
						</div>
					</div>
					{item.path ? <div className="col-md-12 col-sm-12 col-xs-12">
						<button type="button" className="btn btn-sm gs-btn-danger" onClick={() => this.getCustomFiles(item, 'delete')} rel="tooltip" title="Delete"><span className="fa fa-trash-o"></span></button>
					</div> : null }
				</div>
			);
		});
	};

	renderOtherFolder = () => {
		return this.props.resource.documentList.map((item, index)=> {
			return (
				<div key={index} className="col-md-2 col-sm-3 col-xs-6 text-center" style={{padding:'5px',maxHeight:'125px',marginBottom: '10px'}} >
					<div className="col-md-12 col-sm-12 col-xs-12">
						{(!item.destination || ['googledrive', 'dropbox'].indexOf(item.destination) == -1) && item.readaccess ? <span onClick={() => (item.isparent) ? this.getFolderAndFiles(item) : this.downloadFile(item)} style={{cursor: 'pointer'}} className={`${(item.isparent) ? 'fa fa-4x fa-folder-o' : 'fa fa-4x fa-file'}`} rel='tooltip' title={`${item.name}
Uploaded On : ${datetimeFilter(item.created)}`}></span> : null }
						{item.isparent && !item.parentid && !item.readaccess ? <img src="/images/Private-Folder-icon.png" alt="You have no access" style={{maxWidth: '60px', maxHeight: '60px'}} rel='tooltip' title='You have no access to view'/> : null }
						{['googledrive', 'dropbox'].indexOf(item.destination) >= 0 ? <span onClick={() => (item.isparent) ? this.getFolderAndFiles(item) : this.downloadFile(item)} style={{cursor: 'pointer'}} className={`fa fa-4x ${this.getDestinationIcon(item.destination)}`} rel='tooltip' title={`${item.name} 
Uploaded On : ${datetimeFilter(item.created)}`}></span> : null }
						<div style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}} rel='tooltip' title={`${item.name}
Uploaded On : ${datetimeFilter(item.created)}`}>{item.name}</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12">
						{(item.saveaccess && checkActionVerbAccess(this.props.app, 'libraries', 'Save')) ? <button type='button' className='btn btn-sm gs-btn-outline-success' onClick={() => this.openLibraryModal("Edit", item)} rel='tooltip' title='Edit' style={{marginRight: '3px'}}><span className='fa fa-pencil'></span></button> : null }
						{(item.deleteaccess && checkActionVerbAccess(this.props.app, 'libraries', 'Delete')) ? <button type='button' className='btn btn-sm gs-btn-outline-danger' onClick={() => this.libraryAssetsOperation("Delete", item)} rel='tooltip' title='Delete'><span className='fa fa-trash-o'></span></button> : null }
					</div>
				</div>
			);
		});
	};

	render() {
		if(!this.props.resource)
			return null;
		let { library, breadcrumbs, imageList, documentList } = this.props.resource;
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 form-group bg-white" style={{borderBottom: '1px solid #ddd'}}>
							<div className="float-left" style={{marginTop: '15px', marginBottom: '15px', fontSize: '18px'}}>Document Library</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-9 col-sm-12 col-xs-12">
							<ol className="breadcrumb">
								{breadcrumbs.map((item, index)=> {
									return(
										<li key={index} className="breadcrumb-item">
											<span className="gs-anchor" onClick={()=>this.getFolderAndFiles(item, true)}>{item.name}</span>
										</li>
									);
								})}
							</ol>
						</div>
						<div className="col-md-3 col-sm-6 col-xs-12 margintop-5">
							{(library.saveaccess && checkActionVerbAccess(this.props.app, 'libraries', 'Save')) ? <button type='button' className='btn btn-sm btn-width gs-btn-outline-success' onClick={() => this.openLibraryModal("Folder")}><i className="fa fa-plus"></i>Folder</button> : null }
							{(library.saveaccess && checkActionVerbAccess(this.props.app, 'libraries', 'Save')) ? <button type='button' className='btn btn-sm btn-width gs-btn-outline-warning dropdown-toggle' data-toggle="dropdown"><i className="fa fa-upload"></i>Upload File</button> : null }

							<ul className="dropdown-menu">
								<li className="dropdown-item font-14 cursor-pointer">
									<input type="file" id="uploadfile" className="form-control hide" value={this.props.resource.image} onChange={(evt) => {this.uploadFile(evt.target.files)}} required />
									<label className="gs-doclib-upload cursor-pointer" htmlFor="uploadfile"><i className="fa fa-desktop marginright-5"></i> My Computer</label>
								</li>
								{library.parentid != 'Images' ? <li className="divider"></li> : null }
								{library.parentid != 'Images' ? <li className="dropdown-item font-14 cursor-pointer"><span onClick={this.openGoogleDrive}><i className="fa fa-google marginright-5"></i> Google Drive</span></li> : null}
								{library.parentid != 'Images' ? <li className="divider"></li> : null }
								{library.parentid != 'Images' ? <li className="dropdown-item font-14 cursor-pointer"><span onClick={this.DropBoxFiles}><i className="fa fa-dropbox marginright-5"></i> Drop Box</span></li> : null }
							</ul>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="card">
								<div className="card-header">
									<b><i className="fa fa-folder-open"></i> File Manager</b>
								</div>
								<div className="card-body" style={{maxHeight:'75vh',overflowY:'scroll'}}>
									<Dropzone onDrop={(files)=>this.uploadFile(files)} noClick={true} multiple={false} style={{width: '100%', border: 'none'}}>
										{({getRootProps, getInputProps}) => (
											<div {...getRootProps()} className="row" style={{outline: 'none'}}>
												<input {...getInputProps()} />
												{this.renderImageFolder()}
												{documentList.length > 0 ? this.renderOtherFolder() : null}
											</div>
										)}
									</Dropzone>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		);
	}
}

class AssetModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			editModel: false,
			asset: {
				name : '',
				filename : '',
			},
			usersArray: []
		};
		this.renameAsset = this.renameAsset.bind(this);
		this.addNewFolder = this.addNewFolder.bind(this);
		this.getUserdetails = this.getUserdetails.bind(this);
		this.getRoles = this.getRoles.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
		this.renameBtn = this.renameBtn.bind(this);
		this.btnToggle = this.btnToggle.bind(this);
		this.accessCallback = this.accessCallback.bind(this);
		this.close = this.close.bind(this);
	}

	componentWillMount() {
		if(this.props.assetType == 'Edit') {
			this.state.editModel = true;
			this.state.asset = this.props.editasset;
			this.state.asset.filename = this.state.asset.isparent ? this.state.asset.name : this.state.asset.name.substr(0, this.state.asset.name.lastIndexOf("."));
			let fileextarr = this.state.asset.name.split(".");
			this.state.asset.fileext = fileextarr[fileextarr.length-1];
			this.state.asset.type = ((this.state.asset.isparent) ? 'Folder' : 'File');
			this.state.asset.renameFlag = false;
		} else {
			this.state.asset = {
				type : this.props.assetType
			};
			this.state.parentLibrary = this.props.parentLibrary;
			this.state.editModel = false;
		}
		this.getUserdetails();
	}

	getUserdetails = () => {
		let usersArray = [];
		axios.get(`/api/users?&field=id,displayname&filtercondition=`).then((response) => {
			usersArray = response.data.main;
			this.setState({ usersArray }, () => {
				this.getRoles();
			});
		});
	};

	getRoles = () => {
		let rolesArray = [];
		axios.get(`/api/roles?&field=id,name&filtercondition=`).then((response) => {
			rolesArray = response.data.main;
			this.setState({ rolesArray });
		});
	};

	addNewFolder = () => {
		this.props.callback(this.props.assetType, this.state.asset);
		this.props.closeModal();
	};

	renameAsset = () => {
		let { asset } = this.state;
		if(!asset.isrestrict) {
			['readroleids', 'writeroleids', 'deleteroleids', 'readuserids', 'writeuserids', 'deleteuserids'].forEach((item) => {
				asset[item] = [];
			});
			this.setState({ asset });
		}

		if(['googledrive', 'dropbox'].indexOf(asset.destination) == -1) {
			asset.filename = asset.filename.replace(/\//g, '').replace(/\\/g, '');
			asset.name = asset.isparent ? asset.filename : asset.filename+"."+asset.fileext;
			this.setState({ asset });
			this.props.callback(this.props.assetType, asset);
			this.props.closeModal();
		}
	};

	inputOnChange = (evt) => {
		let { asset } = this.state;
		asset[evt.target.name] = evt.target.value.replace(/\//g, '').replace(/\\/g, '');
		this.setState({ asset });
	};

	renameBtn = () => {
		let { asset } = this.state;
		asset.renameFlag = !asset.renameFlag;
		this.setState({ asset });
	};

	btnToggle = (param) => {
		let { asset } = this.state;
		asset.isrestrict = param;
		this.setState({ asset });
	};

	accessCallback = (val, field) => {
		let { asset } = this.state;
		asset[field] = val;
		this.setState({ asset });
	};

	close() {
		this.props.closeModal();
	}

	render() {
		let { editModel, asset, usersArray, rolesArray } = this.state;
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">{`${!asset.id ? 'Create Folder' : asset.type}`} Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					{asset.type=='Folder' && !editModel ? <div className="row">
						<div className="col-md-6 col-sm-12 col-xs-12 offset-md-3">
							<label className="labelclass">Folder Name</label>
							<input type="text" className="form-control" name="name" value={asset.name} onChange={(evt) => this.inputOnChange(evt)} required />
						</div>
					</div> : null }

					{editModel ? <div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="col-md-6 col-sm-12 col-xs-12 offset-md-3">
								<div className="row">
									<div className="col-md-9">
										<label className="labelclass">{asset.type} Name</label>
										{asset.type=='File' ? <div className="input-group">
											<input type="text" className="form-control" name="filename" value={asset.filename} onChange={(evt) => this.inputOnChange(evt)} required disabled={!asset.renameFlag || (['googledrive', 'dropbox'].indexOf(asset.destination) >= 0)} />
											<div className="input-group-append">
												<span className="input-group-text">. {asset.fileext}</span>
											</div>
										</div> : null }
										{asset.type=='Folder' ? <div>
											<input type="name" className="form-control" name="filename" value={asset.filename} onChange={(evt) => this.inputOnChange(evt)} required disabled={!asset.renameFlag || (['googledrive', 'dropbox'].indexOf(asset.destination) >= 0)}/>
										</div> : null }
									</div>
									<div className="col-md-3">
										{!asset.renameFlag && (['googledrive', 'dropbox'].indexOf(asset.destination) == -1) && asset.saveaccess ? <button type="button" className="btn gs-btn-outline-info btn-sm btn-width margintop-25" onClick={this.renameBtn}>Rename</button> : null }
									</div>
								</div>
							</div>
						</div>
					</div> : null }

					
					{asset.isparent && !asset.parentid ? <div className="row">
						<div className="form-group col-sm-6 col-md-3 col-xs-12 offset-md-5 margintop-20">
							<div className="btn-group">
								<button type="button" className={`btn btn-sm btn-outline-secondary ${(false == asset.isrestrict) ? 'active' : ''}`}  onClick={()=>this.btnToggle(false)} disabled={this.props.app.user.roleid.indexOf(1) == -1} >Public</button>
								<button type="button" className={`btn btn-sm btn-outline-secondary ${(true == asset.isrestrict) ? 'active' : ''}`} onClick={()=>this.btnToggle(true)} disabled={this.props.app.user.roleid.indexOf(1) == -1} >Private</button>
							</div>
						</div>
						{asset.isrestrict && this.props.app.user.roleid.indexOf(1) >=0 ? <div className="form-group col-md-12 col-sm-12 col-xs-12 margintop-20">
							<div className="card">
								<div className="card-header">Access Based Roles</div>
								<div className="card-body">
									<div className="row">
										<div className="form-group col-md-4 col-sm-8">
											<label className="labelclass">Read Access</label>
											<LocalSelect options={rolesArray} value={asset.readroleids} multiselect={true} onChange={(val) => this.accessCallback(val, 'readroleids')} label="name" valuename="id" />
										</div>
										<div className="form-group col-md-4 col-sm-8">
											<label className="labelclass">Save Access</label>
											<LocalSelect options={rolesArray} value={asset.writeroleids} multiselect={true} onChange={(val) => this.accessCallback(val, 'writeroleids')} label="name" valuename="id" />
										</div>
										<div className="form-group col-md-4 col-sm-8">
											<label className="labelclass">Delete Access</label>
											<LocalSelect options={rolesArray} value={asset.deleteroleids} multiselect={true} onChange={(val) => this.accessCallback(val, 'deleteroleids')} label="name" valuename="id" />
										</div>
									</div>
								</div>
							</div>
						</div> : null }
						{asset.isrestrict && this.props.app.user.roleid.indexOf(1) >= 0 ? <div className="col-md-12 col-sm-12 col-xs-12 ">
							<div className="card">
								<div className="card-header">Access Based Users</div>
								<div className="card-body">
									<div className="row">
										<div className="form-group col-md-4 col-sm-8">
											<label className="labelclass">Read Access</label>
											<LocalSelect options={usersArray} value={asset.readuserids} multiselect={true} onChange={(val) => this.accessCallback(val, 'readuserids')} label="displayname" valuename="id" />
										</div>
										<div className="form-group col-md-4 col-sm-8">
											<label className="labelclass">Save Access</label>
												<LocalSelect options={usersArray} value={asset.writeuserids} multiselect={true} onChange={(val) => this.accessCallback(val, 'writeuserids')} label="displayname" valuename="id" />
										</div>
										<div className="form-group col-md-4 col-sm-8">
											<label className="labelclass">Delete Access</label>
											<LocalSelect options={usersArray} value={asset.deleteuserids} multiselect={true} onChange={(val) => this.accessCallback(val, 'deleteuserids')} label="displayname" valuename="id" />
										</div>
									</div>
								</div>
							</div>
						</div> : null }
					</div> : null}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								{(asset.type=='Folder' && !editModel && this.props.parentLibrary.saveaccess && checkActionVerbAccess(this.props.app, 'libraries', 'Save')) ? <button type='button' className='btn gs-btn-success btn-sm btn-width' onClick={this.addNewFolder} disabled={!asset.name}><i className="fa fa-plus"></i>Create</button> : null }
								{(editModel && asset.saveaccess && checkActionVerbAccess(this.props.app, 'libraries', 'Save')) ? <button type='button' className='btn gs-btn-success btn-sm btn-width' onClick={this.renameAsset} disabled={!asset.name}><i className='fa fa-check'></i>Save</button> : null }
								<button type='button' className='btn btn-secondary btn-sm btn-width' onClick={this.props.closeModal}><i className='fa fa-times'></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

DocumentLibraryForm = connect(
	(state, props) => {
		let formName = 'documentlibrary';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState, updateAppState}
)(reduxForm()(DocumentLibraryForm));

export default DocumentLibraryForm;
