import React, { Component } from 'react';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';
import moment from 'moment';

import { commonMethods, modalService } from '../utils/services';
import { dateFilter, datetimeFilter, timeFilter} from '../utils/filter';
import { LocalSelect, DateElement } from '../components/utilcomponents';
import RecordingplayModal from '../components/details/recording';

const ServiceActivitySection = connect((state)=>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showactivity: false,
			addActivityShow: false,
			activity: {
				skip: 1
			},
			serviceactivities : []
		};
		this.showAddActivity = this.showAddActivity.bind(this);
		this.getactivities = this.getactivities.bind(this);
		this.addActivities = this.addActivities.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
		this.save = this.save.bind(this);
		this.playRecording = this.playRecording.bind(this);
	}

	componentWillMount() {
		this.getactivities();
	}

	playRecording(activity) {
		this.props.openModal({
			render: (closeModal) => {
				return <RecordingplayModal
						callid = {activity.callid}
						closeModal = {closeModal} 
						openModal = {this.props.openModal}/>
			},
			className: {
				content: 'react-modal-custom-class-30',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	getactivities(param) {
		let { activity, serviceactivities, showactivity } = this.state;
		if(this.props.servicecallid) {
			if(param) {
				activity.skip += 1;
			} else {
				serviceactivities = [];
				activity.skip = 1;
			}

			var queryString = `/api/serviceactivity?field=id,created,users/displayname/createdby,users/displayname/modifiedby,outcome,activitytype,nextactivitydate,activitydate,callid&pagelength=3&skip=${((this.state.activity.skip -1) * 3)}&filtercondition=serviceactivity.servicecallid=${this.props.servicecallid}`;

			axios.get(queryString).then((response)=> {
				for (var i = 0; i < response.data.main.length; i++)
					serviceactivities.push(response.data.main[i]);
				if (serviceactivities.length >= response.data.count)
					showactivity = false;
				else
					showactivity = true;
				this.setState({
					serviceactivities : serviceactivities,
					activity : activity,
					showactivity: showactivity
				});
			});
		}
	}

	save(){
		this.addActivities();
	}

	addActivities(dialerconfirm) {
		if(this.props.app.feature.enableDialerIntegration && this.props.app.callid && !dialerconfirm){
			return this.props.openModal(modalService['confirmMethod']({
				header : 'Alert',
				body : 'Would you like to link the activity with this call?',
				btnArray : ['Ok', 'Cancel']
			}, (param1)=> {
				if(param1) {
					let { activity } = this.state;
					activity.callid = this.props.app.callid;
					this.setState({ activity });
					setTimeout(()=>{this.addActivities(true);},0);
				} else
					this.addActivities(true);
			}));
		}
		
		let { activity } = this.state;
		activity.servicecallid = this.props.servicecallid;

		let tempData = {
			actionverb : 'Save',
			data : activity
		};
		axios({
			method : 'post',
			data : tempData,
			url : '/api/serviceactivity'
		}).then((response)=> {
			if (response.data.message == 'success') {
				this.setState({
					activity: {
						activitytype: null,
						activitydate: null,
						outcome: "",
						nextactivitydate: null
					},
					addActivityShow: false
				}, () => {
					this.getactivities();
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}
	
	showAddActivity() {
		this.setState({addActivityShow : !this.state.addActivityShow});
	}

	inputOnChange(value, field) {
		let { activity } = this.state;
		activity[field] = value;
		this.setState({ activity });
	}

	render() {
		let { activity } = this.state;
		return (
			<div className="col-md-12">
				<div className="card gs-card marginbottom-15">
					<div className="card-header card-header-custom gs-card-header margintop-5">Activities
						<div className="float-right">
							<a className="btn gs-form-btn-primary btn-sm btn-width" onClick={this.showAddActivity}><i className="fa fa-plus"></i>Activity</a>
						</div>
					</div>
					<div className="card-body">
						<div className="row">
							{!this.state.addActivityShow && this.state.serviceactivities.length == 0 ? <div className={`col-md-12 col-sm-12 marginbottom-10`}>
								<div className="text-center">No activities created</div>
							</div> : null}
							{this.state.addActivityShow ? <div className={`col-md-12 col-sm-12 marginbottom-10 `}>
								<div className="row">
									<div className="form-group col-md-3 col-sm-6">
										<label className="labelclass">Activity Type</label>
										<LocalSelect className={`${!activity.activitytype ? 'errorinput' : ''}`} options={["Customer Call", "Service Call"]} value={activity.activitytype} onChange={(value) =>this.inputOnChange(value, 'activitytype')} required/>
									</div>
									<div className="form-group col-md-3 col-sm-6">
										<label className="labelclass">Activity Date</label>
										<DateElement className={`form-control ${!activity.activitydate ? 'errorinput' : ''}`} value={activity.activitydate} onChange={(value) => this.inputOnChange(value, 'activitydate')} required/>
									</div>
									<div className="form-group col-md-3 col-sm-6">
										<label className="labelclass">Outcome</label>
										<textarea className={`form-control ${!activity.outcome ? 'errorinput' : ''}`} value={activity.outcome} onChange={(evt) => this.inputOnChange(evt.target.value, 'outcome')} required/>
									</div>
									<div className="form-group col-md-3 col-sm-6">
										<label className="labelclass">Next Activity Date</label>
										<DateElement className={`form-control`} value={activity.nextactivitydate} onChange={(value) => this.inputOnChange(value, 'nextactivitydate')} />
									</div>
								</div>
								<div className="row">
									<div className="form-group col-md-12 col-sm-12 text-center">
										<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.save} disabled={!activity.activitytype && !activity.activitydate && !activity.outcome}><i className="fa fa-plus"></i>Add Activity</button>
										<button type="button" className="btn btn-sm btn-secondary" onClick={()=> {this.setState({addActivityShow : !this.state.addActivityShow, activity: {activitytype: null, activitydate: null, outcome: "", nextactivitydate: null}});}} ><span className="fa fa-chevron-up"></span></button>
									</div>
								</div>
							</div> : null}
							<div className="col-md-12 col-sm-12 col-xs-12" style={{maxHeight:'300px',overflowY:'auto'}}>
								{this.state.serviceactivities.map((item, index)=> {
									return (
										<div className="row" key={index}>
											<div className="col-md-12">
												<div>
													<span>{item.outcome}</span>
													{
														(this.props.app.feature.enableDialerIntegration && item.callid) ?
															<a className="marginleft-15" onClick={(e)=>{this.playRecording(item)}}><i className="fa fa-headphones"></i></a>
														: null
													}
												</div>
												<div className="font-12">
													<span className="text-muted">Created by {item.createdby_displayname}  </span>
													<span className="text-muted">On : {dateFilter(item.created)} @ {timeFilter(item.created)}</span>
													
													{item.nextactivitydate ? <span className="text-muted float-right">Next Activity on {dateFilter(item.nextactivitydate)}</span>: null}
												</div>
												{(this.state.serviceactivities.length-1) > index ? <hr className="marginbottom-8 margintop-8" style={{display:'inline-block',width: '100%'}}/> : null }
											</div>
										</div>
									);
								})}
							</div>
							{this.state.showactivity ? <div className="col-md-12 text-center" style={{backgroundColor: '#f8f8f8',border: '1px solid #efefef'}}>
								<a onClick={()=>this.getactivities(true)} className="btn showActivity">Show More</a>
							</div> : null }
						</div>
					</div>
				</div>
			</div>
		);
	}
});

export default ServiceActivitySection;