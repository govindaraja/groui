import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';
import { booleanfilter, dateFilter, datetimeFilter, timeFilter } from '../utils/filter';
import { SelectAsync, DateElement, TimeAgo, TaskDueDateFilter, AudioPlayerEle, LocalSelect } from '../components/utilcomponents';
import { Buttongroup } from '../components/formelements';
import { checkActionVerbAccess } from '../utils/utils';

import DocumentuploadModal from './documentuploadmodal';
import Loadingcontainer from '../components/loadingcontainer';
import ViewExistingEmailModal from '../components/details/viewexistingemailmodal';

export const AdditionalInformationSection = connect((state)=>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderflag: false,
			ndtlist: {
				all: {
					isactive: true,
					img: 'gs-ndt-glance-icon',
					title: 'Super Board',
					array: []
				},
				notes: {
					img: 'gs-ndt-notes-icon',
					title: 'Notes',
					array: []
				},
				documents: {
					img: 'gs-ndt-document-icon',
					title: 'Documents',
					array: []
				},
				activities: {
					img: 'gs-ndt-activity-icon',
					title: 'Activities',
					array: []
				},
				emails: {
					img: 'gs-ndt-mail-icon',
					title: 'Emails',
					array: []
				},
				changelog: {
					img: 'gs-ndt-changelog-icon',
					title: 'Change Log',
					array: []
				}
			},
			ndt: {
				notes: [],
				documents: [],
				tasks: [],
				emails: [],
				audits: []
			},
			newnote: {
				isrestricted : false
			},
			newtask: {},
			showNotes : false,
			allactivities: []
		};
		this.getnotes = this.getnotes.bind(this);
		this.getdocuments = this.getdocuments.bind(this);
		this.getActivities = this.getActivities.bind(this);
		this.getemails = this.getemails.bind(this);
		this.getfiletype = this.getfiletype.bind(this);
		this.getFileUrl = this.getFileUrl.bind(this);
		this.deleteDocument = this.deleteDocument.bind(this);
		this.openLibraryUploadModal = this.openLibraryUploadModal.bind(this);
		this.renderNotes = this.renderNotes.bind(this);
		this.renderDocuments = this.renderDocuments.bind(this);
		this.renderTasks = this.renderTasks.bind(this);
		this.renderEmails = this.renderEmails.bind(this);
		this.openEmail = this.openEmail.bind(this);
		this.openActivity = this.openActivity.bind(this);
		this.getResourceName = this.getResourceName.bind(this);
		this.getList = this.getList.bind(this);
		this.updateField =this.updateField.bind(this);
		this.addNotes =this.addNotes.bind(this);
		this.showAddNote = this.showAddNote.bind(this);
		this.renderTimeLine = this.renderTimeLine.bind(this);
		this.getTimeLineData = this.getTimeLineData.bind(this);
		this.renderHeaderBar = this.renderHeaderBar.bind(this);
		this.createActivity = this.createActivity.bind(this);
		this.renderAddNote = this.renderAddNote.bind(this);
		this.playAudio = this.playAudio.bind(this);
	}

	componentWillMount() {
		let audits = [], notes = [];
		this.props.ndt.notes.map((note) => {
			if(note.action) {
				audits.push(note);
			}
			if(note.notes && !note.action) {
				notes.push(note);
			}
		});
		this.state.ndt.notes = notes.sort((a, b) => {
			return b.id - a.id;
		});
		this.state.ndt.audits = audits.sort((a, b) => {
			return b.id - a.id;
		});
		this.state.ndt.documents = this.props.ndt.documents ? this.props.ndt.documents.sort((a, b)=>{
			return b.id - a.id;
		}) : [];
		this.state.ndt.activities = this.props.ndt.tasks ? this.props.ndt.tasks.sort((a, b)=>{
			return b.id - a.id;
		}) : [];
		this.getemails(0);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	createActivity() {
		let tempObj = {
			parentresource:  this.getResourceName(),
			parentid: this.props.parentid,
			tempactivitycategory : 'General',
			assignedto: this.props.app.user.issalesperson ? this.props.app.user.id: null
		}
	
		this.props.createOrEdit('/createActivity', null, tempObj, (responseobj) => {
			this.getActivities();
		});
	}

	openActivity (activity) {
		this.props.history.push(`/details/activities/${activity.id}`);
	}

	getList(param) {
		let { ndtlist } = this.state;

		Object.keys(ndtlist).forEach(ele => {
			ndtlist[ele].isactive = ele == param ? true : false;
		});

		this.setState({ ndtlist,showNotes:false });
	}

	getResourceName() {
		return this.props.app.myResources[this.props.parentresource] && this.props.app.myResources[this.props.parentresource].type == 'lookup' ? this.props.app.myResources[this.props.parentresource].lookupResource : this.props.parentresource;
	}

	showAddNote() {
		this.setState({
			showNotes: !this.state.showNotes
		})
	}

	updateField(value, field) {
		let { newnote } = this.state;
		newnote[field] = value;
		this.setState({ newnote });
	}

	addNotes() {
		let { newnote } = this.state;
		this.updateLoaderFlag(true);
		newnote['parentresource'] = this.getResourceName();
		newnote['parentid'] = this.props.parentid;

		let tempData = {
			actionverb : 'Save',
			data : newnote,
			childResourceToUpdate : []
		};
		axios({
			method : 'post',
			data : tempData,
			url : '/api/notes'
		}).then((response)=> {
			newnote = {notes : "",isrestricted:false};
			this.setState({ newnote, showNotes: false }, ()=> {
				this.getnotes();
			});
			this.updateLoaderFlag(false);
		});
	}

	getnotes() {
		this.updateLoaderFlag(true);
		let { ndt } = this.state;

		let filterString = `notes.parentresource='${this.getResourceName()}' and notes.parentid=${this.props.parentid}`;
		filterString = encodeURIComponent(filterString);

		let queryString = `/api/notes?field=id,users/displayname/createdby,users/displayname/modifiedby,notes,isrestricted,created,action,audittrialhistory&filtercondition=${filterString}`;

		axios.get(queryString).then((response)=> {
			if(response.data.message=='success') {
				let audits = [], notes = [];
				response.data.main.map((note) => {
					if(note.action) {
						audits.push(note);
					}
					if(note.notes && !note.action) {
						notes.push(note);
					}
				});
				ndt.notes = notes.sort((a, b) => {
					return b.id - a.id;
				});
				ndt.audits = audits.sort((a, b) => {
					return b.id - a.id;
				});
				this.setState({ndt},()=>{
					this.updateLoaderFlag(false);
					this.getTimeLineData();
				});
			}
		});
	}

	getdocuments() {
		this.updateLoaderFlag(true);
		let { ndt } = this.state;
		let filterString = `documents.parentresource='${this.getResourceName()}' and documents.parentid=${this.props.parentid}`;
		filterString = encodeURIComponent(filterString);

		var queryString = `/api/documents?field=id,path,users/displayname/createdby,name,created,destination,filesize,parentresource,parentid&filtercondition=${filterString}`;

		axios.get(queryString).then((response)=> {
			ndt.documents = response.data.main;
			this.setState({ndt},()=>{
				this.updateLoaderFlag(false);
				this.getTimeLineData();
			});
		});
	}

	getemails(accordionNum) {
		this.updateLoaderFlag(true);
		let { ndt } = this.state;
		var queryString = `/api/query/getsalesactivitydetailsquery?parentid=${this.props.parentid}&parentresource=${this.getResourceName()}&userid=${this.props.app.user.id}&ndtemail=true`;
		axios.get(queryString).then((response)=> {
			ndt.emails = response.data.main;
			this.setState({ndt}, ()=> {
				this.updateLoaderFlag(false);
				this.getTimeLineData();
			});
		});
	}

	getActivities() {
		this.updateLoaderFlag(true);
		let { ndt } = this.state;

		let filterString = ` and salesactivities.parentresource='${this.getResourceName()}' and salesactivities.parentid=${this.props.parentid}`;
		filterString = encodeURIComponent(filterString);

		let queryString = `/api/salesactivities?field=id,created,modified,description,activitytypeid,activitytypes/name/activitytypeid,duedate,plannedstarttime,startdatetime,enddatetime,status,users/displayname/createdby,users/displayname/assignedto,checkindetails,checkoutdetails&filtercondition=activitytypes_activitytypeid.category = 'General'${filterString}`;

		axios.get(queryString).then((response)=> {
			if(response.data.message=='success') {
				ndt.activities = response.data.main.sort((a, b) => {
					return b.id - a.id;
				});
				
				this.setState({ndt}, ()=>{
					this.updateLoaderFlag(false);
					this.getTimeLineData();
				});
			}
		});
	}

	getTimeLineData() {
		let { notes, documents, activities, audits, emails } = this.state.ndt;
		let allactivities = [];

		notes.forEach(note => {note.param = 'notes'; allactivities.push(note);});
		documents.forEach(doc => {
			let fileextension = doc.path.substr(doc.path.lastIndexOf('.')+1, doc.path.length-1);
			doc.isaudiofile = ['mp3', 'wav', 'ogg'].includes(fileextension) ? true : false;
			doc.param = 'documents';
			allactivities.push(doc);
		});
		activities.forEach(acty => {acty.param = 'activities'; allactivities.push(acty);});
		emails.forEach(mail => {mail.param = 'emails'; mail.created = mail.emaileddate; allactivities.push(mail);});

		allactivities.sort((a,b) => new Date(b.created) - new Date(a.created));

		this.setState({ allactivities });
	}

	deleteDocument(param, resource) {
		this.updateLoaderFlag(true);
		var tempData = {
			actionverb : 'Delete',
			data : param
		};
		axios({
			method : 'post',
			data : tempData,
			url : `/api/${resource}`
		}).then((response)=> {
			this.updateLoaderFlag(false);
			if(resource == 'documents')
				this.getdocuments();
			else
				this.getnotes();
		});
	}

	getfiletype (fileParam) {
		if (fileParam) {
			var filetype = fileParam.name.split('.')[fileParam.name.split('.').length-1];
			if (filetype == 'txt' || filetype =='csv' || filetype == 'rtf') {
				return 'fa-file-text-o';
			}
			else if (filetype == 'gzip' || filetype == 'zip' || filetype == 'rar' || filetype == 'tar') {
				return 'fa-file-zip-o';
			}
			else if (filetype == 'pdf') {
				return 'fa-file-pdf-o';
			}
			else if (filetype == 'jpg' || filetype == 'png' || filetype == 'gif' || filetype == 'jpeg' || filetype == 'ico' || filetype == 'bmp') {
				return 'fa-file-image-o';
			}
			else if (filetype == 'doc' || filetype == 'docx' || filetype == 'dot' || filetype == 'dotx') {
				return 'fa-file-word-o';
			}
			else if (filetype == 'xlsx' || filetype == 'xls') {
				return 'fa-file-excel-o';
			}
			else if (filetype == 'pptx' || filetype == 'pptm') {
				return 'fa-file-powerpoint-o';
			}
			else {
				return "fa-file";
			}
		}
		return "fa-file";
	}

	getFileUrl (item) {
		if(['googledrive', 'dropbox'].indexOf(item.destination) >= 0)
			return item.path;
		else
			return 'documents/' + encodeURIComponent(item.name) + '?url=' + item.path;
	}

	openLibraryUploadModal() {
		this.props.openModal({render: (closeModal) => {return <DocumentuploadModal {...this.props} resource={{library:[]}} multiple={false} localdoc={[]} hidelocaldoc={true} closeModal={closeModal} callback={this.getdocuments} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
	}

	openEmail(emailobj) {
		this.props.openModal({render: (closeModal) => {
			return <ViewExistingEmailModal emailobj={emailobj} app={this.props.app} openModal={this.props.openModal} closeModal={closeModal} ndtemail={true} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}, confirmModal: true});
	}

	playAudio(doc) {
		doc.audioIsPlaying = !doc.audioIsPlaying
		this.setState({
			ndt: this.state.ndt
		});
	}

	calculateSidebarHeight(className) {
		let window_height = $(window).height(),
		nav_bar_height = $('.navbar').outerHeight() + ($('.gs-detail-alert-section').outerHeight() || 0) + $('.ndt-icon-container').outerHeight() + $('.ndt-timeline-header').outerHeight(),
		sidebar_height = window_height - nav_bar_height - 13;
		return sidebar_height;
	}

	renderHeaderBar(prop) {
		let titleName, saveAccess = false, addBtn, onClick, addBtnClass = ``;
		if(prop == 'timeline') {
			titleName = 'Super-board';
			saveAccess = true;
			onClick = () => { this.setState({showNotes : false}); };
			addBtnClass = this.state.showNotes ? 'change' : '';
		}
		if(prop == 'notes') {
			titleName = this.state.showNotes ? 'Add Note' : 'Notes';
			saveAccess = checkActionVerbAccess(this.props.app, 'notes', 'Save');
			onClick = this.showAddNote;
			addBtnClass = this.state.showNotes ? 'change' : '';
		}
		if(prop == 'documents') {
			titleName = 'Files';
			saveAccess = checkActionVerbAccess(this.props.app, 'documents', 'Save');
			onClick = this.openLibraryUploadModal;
		}
		if(prop == 'activities') {
			titleName = 'Activities';
			saveAccess = checkActionVerbAccess(this.props.app, 'salesactivities', 'Save');
			onClick = this.createActivity;
		}
		if(prop == 'email') {
			titleName = 'Emails';
		}
		if(prop == 'audit') {
			titleName = 'Change Log';
		}
		addBtn = () => {
			if(prop == 'timeline' && !this.state.showNotes) {
				return (
					<div key={1} className="dropdown">
						<div className={`ndt-add-btn d-flex align-items-center justify-content-center ${this.state.showNotes ? 'change' : ''}`} data-toggle="dropdown">
							<div className={`fa ${!this.state.showNotes ? 'fa-plus' : 'fa-times'}`}></div>
						</div>
						<div className='dropdown-menu font-14 react-ndt-popper' style={{boxShadow: '0 0 4px 0px rgba(0, 0, 0, 0.5)', border: 'none'}}>
							<div className="dropdown-item cursor-ptr" onClick={()=>{this.setState({showNotes : true});}}>Add Note</div>
							<div className="dropdown-item cursor-ptr" onClick={this.openLibraryUploadModal}>Attach File</div>
							{
								(this.getResourceName() != 'salesactivities') ? <div className="dropdown-item cursor-ptr" onClick={this.createActivity}>Create Activity</div> : null
							}
						</div>
					</div>
				);
			}

			return (
				<div key={2}  className={`ndt-add-btn d-flex align-items-center justify-content-center ${addBtnClass}`} onClick={onClick}>
					<div className={`fa ${!this.state.showNotes ? 'fa-plus' : 'fa-times'}`}></div>
				</div>
			);
		};
		return (
			<div className="col-md-12">
				<div className="d-block ndt-timeline-header" style={{borderBottom: '1px solid rgba(226, 226, 226, .5)', padding: '15px 25px'}}>
					<div className="row">
						<div className="col-md-8 d-flex align-items-center" style={{height: '30px'}}>
							<div className="font-18 fontWeight">{titleName}</div>
						</div>
						<div className = {`col-md-4 text-right ${saveAccess ? '' : 'hide'}`}>
							{addBtn()}
						</div>
					</div>
				</div>
			</div>
		);
	}

	renderAddNote(){
		let { ndtlist,newnote } = this.state;

		return (
			<div className={`px-3 ${this.state.showNotes ? 'd-block' : 'd-none'}`} style={{borderBottom: '1px solid rgba(226, 226, 226, .5)', paddingTop: '14px', paddingBottom: '14px'}}>
				<div className="row no-gutters">
					<div className="col-md-12 form-group">
						<textarea className="form-control ndtclass" value={newnote.notes} rows={"3"} placeholder="Type something..." onChange={(evt) => this.updateField(evt.target.value, 'notes')} />
					</div>
					<div className="col-md-9 d-flex align-items-center ">
						<div className="mr-2" style={{whiteSpace: 'nowrap'}}>Who can see</div>
						<div style={{width: '100%', paddingRight: '15px'}}>
							<LocalSelect 
								className="ndtclass"
								options={[{label: 'Anyone', value: false}, {label: 'Only Me', value: true}]} 
								label="label" 
								valuename="value" 
								value={newnote.isrestricted} 
								required={true}
								onChange={(value) => this.updateField(value, 'isrestricted')} />
						</div>
					</div>
					<div className="col-md-3">
						<button type="button" className="btn btn-sm gs-btn-success  ndtclass" onClick={this.addNotes} disabled={!newnote.notes}>Add Note</button>
					</div>
				</div>
			</div>
		);
	}

	renderTimeLine() {
		let docSaveAccessClass = checkActionVerbAccess(this.props.app, 'documents', 'Save') ? '' : 'hide';
		let docDeleteAccessClass = checkActionVerbAccess(this.props.app, 'documents', 'Delete') ? '' : 'hide';
		let noteSaveAccessClass = checkActionVerbAccess(this.props.app, 'notes', 'Delete') ? '' : 'hide';
		let noteDeleteAccessClass = checkActionVerbAccess(this.props.app, 'notes', 'Delete') ? '' : 'hide';
		let activitySaveAccessClass = checkActionVerbAccess(this.props.app, 'salesactivities', 'Delete') ? '' : 'hide';
		let activityDeleteAccessClass = checkActionVerbAccess(this.props.app, 'salesactivities', 'Delete') ? '' : 'hide';
		let { ndtlist } = this.state;

		let sidebar_height = this.calculateSidebarHeight('.ndt-timeline-header');
		return (
			<div className="row no-gutters">
				{this.renderHeaderBar('timeline')}
				<div className="col-md-12">
					<div className="d-block gs-scrollbar" style={{maxHeight: `${sidebar_height}px`, overflowY: 'auto', height: `${sidebar_height}px`}}>
						{this.renderAddNote()}
						{
							this.state.allactivities.length > 0 ? this.state.allactivities.map((acty, index) => {

								if(acty.param == 'notes')
									return this.renderNotesCard(acty, index, noteDeleteAccessClass);
								if(acty.param == 'documents')
									return this.renderDocumentsCard(acty, index, docDeleteAccessClass);
								if(acty.param == 'activities' && this.getResourceName() != 'salesactivities')
									return this.renderActivityCard(acty, index, 'hide');
								if(acty.param == 'emails' && this.getResourceName() != 'salesactivities')
									return this.renderEmailCard(acty, index, 'hide');

								return null;
							}) : <div className="px-3 d-flex flex-column align-items-center justify-content-center h-100" style={{paddingTop: '14px', paddingBottom: '14px'}}>
								<div className="form-group"><img src="/images/ndt/gs-ndt-empty-state-icon.svg" /></div>
								<div>Nothing to show here.</div>
							</div>
						}
					</div>
				</div>
			</div>
		);
	}

	renderNotes() {
		let deleteAccessClass = checkActionVerbAccess(this.props.app, 'notes', 'Delete') ? '' : 'hide';
		let sidebar_height = this.calculateSidebarHeight('.ndt-notes-header');

		return (
			<div className="row no-gutters">
				{this.renderHeaderBar('notes')}
				<div className="col-md-12">
					<div className="d-block gs-scrollbar" style={{maxHeight: `${sidebar_height}px`, overflowY: 'auto', height: `${sidebar_height}px`}}>
						{this.renderAddNote()}
						{
							this.state.ndt.notes.length > 0 ? this.state.ndt.notes.map((note, index) => {
								return this.renderNotesCard(note, index, deleteAccessClass);
							}) : <div className="px-3 d-flex flex-column align-items-center justify-content-center h-100" style={{paddingTop: '14px', paddingBottom: '14px'}}>
								<div className="form-group"><img src="/images/ndt/gs-ndt-empty-state-icon.svg" /></div>
								<div>No Notes Found!</div>
							</div>
						}
					</div>
				</div>
			</div>
		);
	}

	renderNotesCard(note, index, deleteAccessClass) {
		return (
			<div className="px-3" style={{borderBottom: '1px solid rgba(226, 226, 226, .5)', paddingTop: '14px', paddingBottom: '14px'}} key={index}>
				<div className="row no-gutters">
					<div className="col-md-1">
						<img src={`/images/ndt/gs-ndt-notes-icon.svg`} />
					</div>
					<div className="col-md-11 mb-2">
						<span>{note.notes}</span>
					</div>
					<div className="col-md-11 offset-md-1 font-11 text-muted d-flex align-items-center justify-content-between">
						<div>{note.isrestricted ? <span className="fa fa-lock"></span> : null } Created by {note.createdby_displayname} </div>
						<div className="d-flex flex-row align-items-center">
							<TimeAgo date={note.created} />
							<div className={`text-danger ml-2 cursor-ptr font-14 ${deleteAccessClass}`}
								onClick={()=>this.deleteDocument(note,'notes')}>
								<span className="fa fa-trash-o"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	renderChangelog() {
		if(this.state.ndt.audits == 0)
			return null;

		let sidebar_height = this.calculateSidebarHeight('.ndt-changelog-header');

		return (
			<div className="row no-gutters">
				{this.renderHeaderBar('audit')}
				<div className="col-md-12">
					<div className="d-block gs-scrollbar" style={{maxHeight: `${sidebar_height}px`, overflowY: 'auto', height: `${sidebar_height}px`}}>
						{
							this.state.ndt.audits.length > 0 ? this.state.ndt.audits.map((audit, index) => {
								return this.renderChangelogCard(audit, index);
							}) : <div className="px-3 d-flex flex-column align-items-center justify-content-center h-100" style={{paddingTop: '14px', paddingBottom: '14px'}}>
								<div className="form-group"><img src="/images/ndt/gs-ndt-empty-state-icon.svg" /></div>
								<div>No Change Log Found!</div>
							</div>
						}
					</div>
				</div>
			</div>
		);
	}

	renderChangelogCard(audit, index) {
		if(!audit.action)
			return null;

		return (
			<div className="px-3" style={{borderBottom: '1px solid rgba(226, 226, 226, .5)', paddingTop: '14px', paddingBottom: '14px'}} key={index}>
				<div className="row no-gutters">
					<div className="col-md-1">
						<img src={`/images/ndt/gs-ndt-changelog-icon.svg`} />
					</div>
					<div className="col-md-11 mb-2">
						<div>Action Taken : {audit.action}</div>
						{audit.notes ? <div>Remarks : {audit.notes}</div> : null }
						{audit.audittrialhistory ? <div style={{paddingTop:'10px'}}>
							{audit.audittrialhistory.showObject.map((history, historyindex)=> {
								if(history.type=="json")
									return null;
								return (<div key={historyindex}>
									<span>{history.name}: </span>{history.type=='date' && new Date(history.value) != 'Invalid Date' ? (history.value ? dateFilter(history.value) : '') : (history.type=="boolean" ? booleanfilter(history.value) : history.value)}
								</div>)
							})}
						</div> : null }
					</div>
					<div className="col-md-11 offset-md-1 font-11 text-muted">
						Created by {audit.createdby_displayname} 
						<TimeAgo className="float-right" date={audit.created} />
					</div>
				</div>
			</div>
		);
	}

	renderDocuments() {
		let saveAccessClass = checkActionVerbAccess(this.props.app, 'documents', 'Save') ? '' : 'hide';
		let deleteAccessClass = checkActionVerbAccess(this.props.app, 'documents', 'Delete') ? '' : 'hide';
		let sidebar_height = this.calculateSidebarHeight('.ndt-documents-header');

		return (
			<div className="row no-gutters">
				{this.renderHeaderBar('documents')}
				<div className="col-md-12">
					<div className="d-block gs-scrollbar" style={{maxHeight: `${sidebar_height}px`, overflowY: 'auto', height: `${sidebar_height}px`}}>
						{
							this.state.ndt.documents.length > 0 ? this.state.ndt.documents.map((doc, index) => {
								return this.renderDocumentsCard(doc, index, deleteAccessClass)
							}) : <div className="px-3 d-flex flex-column align-items-center justify-content-center h-100" style={{paddingTop: '14px', paddingBottom: '14px'}}>
								<div className="form-group"><img src="/images/ndt/gs-ndt-empty-state-icon.svg" /></div>
								<div>No Documents Found!</div>
							</div>
						}
					</div>
				</div>
			</div>
		);
	}

	renderDocumentsCard(doc, index, deleteAccessClass) {
		return (
			<div className="px-3" style={{borderBottom: '1px solid rgba(226, 226, 226, .5)', paddingTop: '14px', paddingBottom: '14px'}} key={index}>
				<div className="row no-gutters">
					<div className="col-md-1">
						<img src={`/images/ndt/gs-ndt-document-icon.svg`} />
					</div>
					<div className="col-md-11 mb-2">
						{
							doc.isaudiofile ? <div className="d-flex flex-column align-items-center p-2" 
							style={{border: '1px solid rgba(201, 201, 201, 0.5)', borderRadius: '3px', textDecoration: 'none', color: '#444444'}}>
								<div className="d-flex align-items-center">
									<div>{doc.name}</div>
									<div className={`text-primary fa fa-2x fa-${doc.audioIsPlaying ? 'stop' : 'play'}-circle-o`} onClick={()=>this.playAudio(doc)}></div>
								</div>
								<AudioPlayerEle src={this.getFileUrl(doc)} playing={doc.audioIsPlaying} callback={()=> this.playAudio(doc)} />
							</div> : 
							<a className="d-flex align-items-center p-2" href={this.getFileUrl(doc)} target="_blank"
							style={{border: '1px solid rgba(201, 201, 201, 0.5)', borderRadius: '3px', textDecoration: 'none', color: '#444444'}}>
							<span className={`fa fa-2x ${this.getfiletype(doc)} mr-2`}></span>
							<span>{doc.name}</span></a>
						}
					</div>
					<div className="col-md-11 offset-md-1 font-11 text-muted">
						Uploaded by {doc.createdby_displayname} 
						<div className="float-right d-flex flex-row align-items-center">
							<TimeAgo date={doc.created} />
							<div className={`text-danger ml-2 cursor-ptr font-14 ${deleteAccessClass}`}
								onClick={()=>this.deleteDocument(doc, 'documents')}>
								<span className="fa fa-trash-o"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	renderTasks() {
		let saveAccessClass = checkActionVerbAccess(this.props.app, 'salesactivities', 'Save') ? '' : 'hide';
		let deleteAccessClass = checkActionVerbAccess(this.props.app, 'salesactivities', 'Delete') ? '' : 'hide';
		let sidebar_height = this.calculateSidebarHeight('.ndt-tasks-header');

		return (
			<div className="row no-gutters">
				{this.renderHeaderBar('activities')}
				<div className="col-md-12">
					<div className="d-block gs-scrollbar" style={{maxHeight: `${sidebar_height}px`, overflowY: 'auto', height: `${sidebar_height}px`}}>
						{
							this.state.ndt.activities.length > 0 ? this.state.ndt.activities.map((activity, index) => {
								return this.renderActivityCard(activity, index, deleteAccessClass);
							}) : <div className="px-3 d-flex flex-column align-items-center justify-content-center h-100" style={{paddingTop: '14px', paddingBottom: '14px'}}>
								<div className="form-group"><img src="/images/ndt/gs-ndt-empty-state-icon.svg" /></div>
								<div>No Activities Found!</div>
							</div>
						}
					</div>
				</div>
			</div>
		);
	}

	renderActivityCard(activity, index, deleteAccessClass) {
		return (
			<div className="px-3 cursor-ptr" style={{borderBottom: '1px solid rgba(226, 226, 226, .5)', paddingTop: '14px', paddingBottom: '14px'}} key={index} onClick={()=>{this.openActivity(activity)}}>
				<div className="row no-gutters">
					<div className="col-md-1">
						<img src={`/images/ndt/gs-ndt-activity-icon.svg`} />
					</div>
					<div className="col-md-11">
						<div className="row">
							<div className="col-md-12">
								<div className="semi-bold" style={{width: '70%', float: 'left', textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden'}}>{activity.activitytypeid_name}</div>
								<div className="font-12 float-right" style={{"borderRadius": "2px","backgroundColor": "#7878781a","color": "#454545","textAlign": "center","padding": "1.5px 8px"}}>{activity.status}</div>
							</div>
						</div>
						<div className="row mb-2">
							<div className="col-md-12" style={{letterSpacing: '-0.34px'}}>{activity.description}</div>
						</div>
						<div className="row font-12 mb-2">
							<div className="col-md-12 semi-bold">
								<span style={{"borderRadius": "2px","backgroundColor": "#7878781a","color": "#444444","textAlign": "center","padding": "2px 8px"}}>
									<span className="fa fa-user-o"></span>
									<span style={{marginLeft: '5px'}}>{activity.assignedto_displayname}</span>
								</span>
								{(activity.duedate || activity.plannedstarttime) && activity.status != 'Completed' && activity.status != 'Cancelled' ? <TaskDueDateFilter className="float-right" duedate={activity.duedate || activity.plannedstarttime} /> : null}
							</div>
						</div>
						<div className="col-md-12 font-11 text-muted pl-0 pr-0">
							Created by {activity.createdby_displayname} 
							<TimeAgo className="float-right" date={activity.created} />
						</div>
					</div>
				</div>
			</div>
		);
	}

	renderEmails() {
		let sidebar_height = this.calculateSidebarHeight('.ndt-emails-header');

		return (
			<div className="row no-gutters">
				{this.renderHeaderBar('email')}
				<div className="col-md-12">
					<div className="d-block gs-scrollbar" style={{maxHeight: `${sidebar_height}px`, overflowY: 'auto', height: `${sidebar_height}px`}}>
						{
							this.state.ndt.emails.length > 0 ? this.state.ndt.emails.map((mail, index)=> {
								return this.renderEmailCard(mail, index);
							}) : <div className="px-3 d-flex flex-column align-items-center justify-content-center h-100" style={{paddingTop: '14px', paddingBottom: '14px'}}>
								<div className="form-group"><img src="/images/ndt/gs-ndt-empty-state-icon.svg" /></div>
								<div>No Emails Found!</div>
							</div>
						}
					</div>
				</div>
			</div>
		);
	}

	renderEmailCard(mail, index) {
		if(!mail.clientemailid)
			return null;

		return (
			<div className="px-3" style={{borderBottom: '1px solid rgba(226, 226, 226, .5)', paddingTop: '14px', paddingBottom: '14px'}} key={index}>
				<div className="row no-gutters">
					<div className="col-md-1">
						{mail.inreplyto ? <img title={mail.subject} src="/images/ndt/gs-ndt-incoming-mail-icon.svg" /> : null }
						{!mail.inreplyto ? <img title={mail.subject} src="/images/ndt/gs-ndt-outgoing-mail-icon.svg" /> : null }
					</div>
					<div className="col-md-11 mb-2 cursor-ptr" onClick={()=>this.openEmail(mail)}>
						<div className="d-block">{mail.subject}</div>
						<div className="d-block text-muted">{mail.body}</div>
					</div>
					<div className="col-md-11 offset-md-1 font-12 text-muted">
						{mail.inreplyto ? <span>Received from {mail.tomail} </span> : null }
						{!mail.inreplyto ? <span>Sent by {mail.frommail} </span> : null }
						<TimeAgo date={mail.emaileddate} className="float-right" />
					</div>
				</div>
			</div>
		);
	}

	render() {
		let { ndtlist } = this.state;
		let ndtDetailContainerHeight = $(window).height() - $('.navbar').outerHeight() - $('.gs-detail-alert-section').outerHeight() - $('.ndt-icon-container').outerHeight() - 10;
		return (
			<div className="ndt-section-wrapper">
				<Loadingcontainer isloading={this.state.loaderflag} isAbsolute={true}></Loadingcontainer>
				<div className="ndt-icon-container">
					{
						Object.keys(ndtlist).map((ele, index) => {
							if((ele == 'emails' || ele == 'activities') && this.getResourceName() == 'salesactivities'){
								return null;
							}
							return <div className={`ndt-icon-list cursor-ptr ${ndtlist[ele].isactive ? 'active' : ''} ${ndtlist[ele].img ? 'd-flex align-items-center justify-content-center' : ''}`} key={index} onClick={()=>this.getList(ele)} title={ndtlist[ele].title}>{ndtlist[ele].icon ? <i className={`fa fa-${ndtlist[ele].icon}`}></i> : null}{ndtlist[ele].img ? <div style={{mask: `url(/images/ndt/${ndtlist[ele].img}.svg) no-repeat center`, WebkitMask: `url(/images/ndt/${ndtlist[ele].img}.svg) no-repeat center`, width: "100%", height: "17px", backgroundColor: `${ndtlist[ele].isactive ? '#1dbb99': '#000000'}`}}></div> : null}</div>
						})
					}
				</div>
				<div className="ndt-details-container bg-white" style={{height: `${ndtDetailContainerHeight}px`}}>
					{ ndtlist.all.isactive ? this.renderTimeLine() : null }
					{ ndtlist.notes.isactive ? this.renderNotes() : null }
					{ ndtlist.documents.isactive ? this.renderDocuments() : null }
					{ ndtlist.activities.isactive ? this.renderTasks() : null }
					{ ndtlist.emails.isactive ? this.renderEmails() : null }
					{ ndtlist.changelog.isactive ? this.renderChangelog() : null }
				</div>
			</div>
		);
	}
});