import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import axios from 'axios';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import { updateFormState, updatePageJSONViewState } from '../actions/actions';
import { updateListViewState } from '../actions/list';

import { WithContext as ReactTags } from 'react-tag-input';
import { checkArray, checkActionVerbAccess } from '../utils/utils';
import { commonMethods, modalService} from '../utils/services';
import { LocalSelect, SelectAsync, AutoSelect } from '../components/utilcomponents';

import Listviewlistcontainer from '../components/list/listviewlistcontainer';
import Loadingcontainer from '../components/loadingcontainer';
import { InputEle, NumberEle, SpanEle, DateEle, textareaEle, selectAsyncEle, localSelectEle, autoSelectEle, emailEle, checkboxEle, autosuggestEle, ButtongroupEle, TimepickerEle, passwordEle } from '../components/formelements';
import {stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation } from '../utils/utils';


class ListPageEditModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag: true,
			searchresourcefield: "",
			resourcejsonfields: [],
			basicFilterArray: [],
			usersArray: [],
			rolesArray: [],
			operators: {
				'date' : ['Equals', 'Not Equals', 'More than','More than or equal', 'Less than','Less than or equal', 'Between', 'Is Empty', 'Is Not Empty'],
				'string' : ['Equals', 'Not Equals', 'Like', 'Is Empty', 'Is Not Empty'],
				'autocomplete' : ['Equals', 'Not Equals', 'Is Empty', 'Is Not Empty'],
				'select' : ['Equals', 'Not Equals','In', 'Is Empty', 'Is Not Empty'],
				'selectasync' : ['Equals', 'Not Equals', 'Is Empty', 'Is Not Empty'],
				'integer' : ['Equals', 'Not Equals', 'More than','More than or equal', 'Less than','Less than or equal', 'Is Empty', 'Is Not Empty'],
				'boolean' : ['Equals', 'Not Equals', 'Is Empty', 'Is Not Empty']
			},
			dateOptions: [{
				name : 'Today',
				id : '<today>'
			},{
				name : 'Today + 1',
				id : '<today+1>'
			},{
				name : 'Today - 1',
				id : '<today-1>'
			},{
				name : 'Today + 3',
				id : '<today+3>'
			},{
				name : 'Today - 7',
				id : '<today-7>'
			},{
				name : 'Today + 30',
				id : '<today+30>'
			},{
				name : 'Today + 90',
				id : '<today+90>'
			},{
				name : 'First Day of Month',
				id : '<firstdayofmonth>'
			},{
				name : 'Last Day of Month',
				id : '<lastdayofmonth>'
			}],
			userOptions: [{
				id: '<currentuser>',
				name: 'Me'
			}],
			advancedFilterArray: []
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getUserdetails = this.getUserdetails.bind(this);
		this.getRoles = this.getRoles.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getOperators = this.getOperators.bind(this);
		this.addRow = this.addRow.bind(this);
		this.deleteRow = this.deleteRow.bind(this);
		this.operatorOnChange = this.operatorOnChange.bind(this);
		this.visibilityOnchange = this.visibilityOnchange.bind(this);
		this.valuetypeOnChange = this.valuetypeOnChange.bind(this);
		this.showSelectAsyncFilter = this.showSelectAsyncFilter.bind(this);
		this.renderViewConditionalfield = this.renderViewConditionalfield.bind(this);
		this.renderConditionalFilter = this.renderConditionalFilter.bind(this);
		this.basicFilterCallback = this.basicFilterCallback.bind(this);
		this.roleFilterCallback = this.roleFilterCallback.bind(this);
		this.userFilterCallback = this.userFilterCallback.bind(this);
		this.save = this.save.bind(this);
		this.getListView = this.getListView.bind(this);
		this.onDragEnd = this.onDragEnd.bind(this);
	}

	componentWillMount() {
		if(this.props.location.params ) {
			this.setState({
				resourcejson : this.props.app.myResources[this.props.location.params.resourcename],
				listTitle: this.props.location.params.listTitle
			});
			if(this.props.match.params && this.props.match.params.id > 0)
				this.getListView();
			else {
				this.props.initialize(this.props.location.params.currentview);
				this.getUserdetails();
			}
		} else {
			this.props.history.replace('/home');
		}
	}

	getListView() {
		axios.get(`/api/views/${this.props.match.params.id}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.initialize(response.data.main);
				this.getUserdetails();
			}
		});
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag)
			this.updateLoaderFlag(false);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	getUserdetails() {
		axios({
			method: 'get',
			url: '/api/users?field=id,displayname&filtercondition='
		}).then((response)=> {
			if(response.data.message == 'success') {
				this.setState({
					usersArray: response.data.main
				});
			}
			this.getRoles();
		});
	}

	getRoles() {
		axios({
			method: 'get',
			url: '/api/roles?field=id,name&filtercondition='
		}).then((response)=> {
			if(response.data.message == 'success') {
				this.setState({
					rolesArray: response.data.main
				});
			}
			this.onLoad();
		});
	}

	onLoad() {
		let { resourcejsonfields, basicFilterArray, advancedFilterArray } = this.state;
		let { resource } = this.props;
 
		for(var prop in this.state.resourcejson.fields) {
			if(this.state.resourcejson.fields[prop].filterformat) {
				if(this.state.resourcejson.fields[prop].isForeignKey) {
					var foreignkeyResourcename = this.state.resourcejson.fields[prop]['foreignKeyOptions'].resource;
					var mainfieldname = this.state.resourcejson.fields[prop]['foreignKeyOptions'].mainField.split('.')[1];
					resourcejsonfields.push({
						field: `${prop}/${mainfieldname}`,
						displayName: this.state.resourcejson.fields[prop].displayName,
						jsonname: "resourcejson"
					});
				} else {
					resourcejsonfields.push({
						field: prop,
						displayName: this.state.resourcejson.fields[prop].displayName,
						jsonname: "resourcejson"
					});
				}

				if(this.state.resourcejson.fields[prop].showInAdvanceSearch && !this.state.resourcejson.fields[prop].isrestrict) {
					basicFilterArray.push({
						name :this.state.resourcejson.fields[prop].displayName,
						field : prop
					});
				}

				if(this.state.resourcejson.fields[prop].isForeignKey) {
					var tempresourcename = this.state.resourcejson.fields[prop].foreignKeyOptions.resource;

					for(var tempfieldname in this.props.app.myResources[tempresourcename].fields) {
						if(this.props.app.myResources[tempresourcename].fields[tempfieldname].showInAdvanceSearch && !this.props.app.myResources[tempresourcename].fields[tempfieldname].isrestrict) {
							basicFilterArray.push({
								name : this.state.resourcejson.fields[prop].displayName+"'s "+this.props.app.myResources[tempresourcename].fields[tempfieldname].displayName,
								field : `${prop}_${tempfieldname}`
							});
						}
					}
				}
			}
		}

		for(var prop in this.state.resourcejson.fields) {
			if(this.state.resourcejson.fields[prop].filterformat) {
				if(this.state.resourcejson.fields[prop].isForeignKey) {
					var foreignkeyResourcename = this.state.resourcejson.fields[prop]['foreignKeyOptions'].resource;
					var mainfieldname = this.state.resourcejson.fields[prop]['foreignKeyOptions'].mainField.split('.')[1];

					for(var foreignkeyField in this.props.app.myResources[foreignkeyResourcename].fields) {
						if(mainfieldname != foreignkeyField && !this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].isForeignKey && !this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].isrestrict && this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].filterformat) {
							resourcejsonfields.push({
								field:  `${prop}/${foreignkeyField}`,
								displayName : this.state.resourcejson.fields[prop].displayName +"'s "+this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].displayName,
								jsonname : "resourcejson"
							});
						}
					}
				}
			}
		}

		var parentJson = this.state.resourcejson;
		for (var prop in parentJson.fields) {
			if (checkArray(this.props.app.user.roleid, parentJson.fields[prop].readroleid)) {
				if(parentJson.fields[prop].showInAdvanceSearch) {
					var tempObj = parentJson.fields[prop];
					tempObj.displayNameFilter = prop;
					advancedFilterArray.push(tempObj);
				}
			}
		}
		advancedFilterArray = advancedFilterArray.sort((advfilfield1, advfilfield2) => {
			return (advfilfield1.displayName.toLowerCase() > advfilfield2.displayName.toLowerCase() ? 1 : (advfilfield1.displayName.toLowerCase() < advfilfield2.displayName.toLowerCase() ? -1 : 0));
		});

		let viewjsonFields = [], viewjsonBasicFilter = [], viewjsoncondition = [];

		let resourceJsonFieObj = {}, basicFilterObj = {}, conditionObj = {};

		resourcejsonfields.forEach(item => { resourceJsonFieObj[item.field] = true });
		basicFilterArray.forEach(item => { basicFilterObj[item.field] = true });
		advancedFilterArray.forEach(item => { conditionObj[item.displayNameFilter] = true });

		resource.viewjson.fields.forEach((field) => {
			if(resourceJsonFieObj[field])
				viewjsonFields.push(field);
		});
		resource.viewjson.basicfilter.forEach((field) => {
			if(basicFilterObj[field])
				viewjsonBasicFilter.push(field);
		});
		resource.viewjson.condition.forEach((item) => {
			if(conditionObj[item.field])
				viewjsoncondition.push(item);
		});

		this.props.updateFormState(this.props.form, {
			[`viewjson.fields`]: viewjsonFields,
			[`viewjson.basicfilter`]: viewjsonBasicFilter,
			[`viewjson.condition`]: viewjsoncondition,
		});

		this.setState({
			resourcejsonfields,
			basicFilterArray,
			advancedFilterArray
		}, () => {
			for (var i = 0; i < this.props.resource.viewjson.condition.length; i++) {
				this.getOperators(this.props.resource.viewjson.condition[i], false, this.props.resource.viewjson.condition[i].value, `viewjson.condition[${i}]`);
			}
			this.updateLoaderFlag(false);	
		});
	}

	getOperators (item, emptyTheValue, itemval, itemstr) {
		let tempObj = {};
		var itemFound = false;
		item.options = {};
		if (emptyTheValue) {
			tempObj[`${itemstr}.field`] = itemval;
			tempObj[`${itemstr}.operator`] = null;
			tempObj[`${itemstr}.valuetype`] = null;
			tempObj[`${itemstr}.value`] = null;
			tempObj[`${itemstr}.betvalue`] = null;
		} else {
			tempObj[`${itemstr}.field`] = item.field;
			tempObj[`${itemstr}.value`] = itemval;
		}

		for (var i = 0; i < this.state.advancedFilterArray.length; i++) {
			if (tempObj[`${itemstr}.field`] == this.state.advancedFilterArray[i].displayNameFilter) {
				itemFound = true;

				tempObj[`${itemstr}.resourceName`] = this.state.advancedFilterArray[i].resourceName;
				if (this.state.advancedFilterArray[i].group == 'autocomplete' || this.state.advancedFilterArray[i].group == 'selectasync') {
					tempObj[`${itemstr}.operatorOptions`] = this.state.operators[this.state.advancedFilterArray[i].group];
				} else {
					if (this.state.advancedFilterArray[i].type == 'date' || this.state.advancedFilterArray[i].type == 'integer') {
						tempObj[`${itemstr}.operatorOptions`] = this.state.operators[this.state.advancedFilterArray[i].type];;
					} else {
						if (this.state.advancedFilterArray[i].type == 'string' && this.state.advancedFilterArray[i].group == 'localselect') {
							tempObj[`${itemstr}.operatorOptions`] = this.state.operators.select;
						} else if (this.state.advancedFilterArray[i].type == 'string' && this.state.advancedFilterArray[i].group == 'simple') {
							tempObj[`${itemstr}.operatorOptions`] = this.state.operators.string;
						} else if (this.state.advancedFilterArray[i].type == 'boolean' && this.state.advancedFilterArray[i].group == 'simple') {
							tempObj[`${itemstr}.operatorOptions`] = this.state.operators.boolean;
						} else {
							tempObj[`${itemstr}.operatorOptions`] = [];
						}
					}
				}
				tempObj[`${itemstr}.options`] = this.state.advancedFilterArray[i];
				break;
			}
		}
		if (itemFound == false) {
			tempObj[`${itemstr}.options`] = {};
		}
		this.props.updateFormState(this.props.form, tempObj);
	}

	addRow() {
		let condition = [...this.props.resource.viewjson.condition];
		condition.push({
			index : condition.length,
			operator : 'Equals'
		});
		this.props.updateFormState(this.props.form, {
			[`viewjson.condition`]: condition 
		});
	}

	operatorOnChange (item, value, itemstr) {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.valuetype`]: null,
			[`${itemstr}.value`]: null,
			[`${itemstr}.betvalue`]: null,
		});
	}

	deleteRow (index) {
		let condition = [...this.props.resource.viewjson.condition];
		condition.splice(index, 1);
		this.props.updateFormState(this.props.form, {
			[`viewjson.condition`]: condition 
		});
	}

	showSelectAsyncFilter(item) {
		if(item.options.foreignKeyOptions.resource == 'numberingseriesmaster')
			return `numberingseriesmaster.resource= '${pagejson.resourcename}'`;

		return '';
	}

	visibilityOnchange(val) {
		this.props.updateFormState(this.props.form, {
			userids: [],
			roleids: []
		});
	}

	valuetypeOnChange(item, value, itemstr) {
		this.props.updateFormState(this.props.form, {
			[`${itemstr}.value`]: value == 'defaultvalue' ? (item.options.type != 'date' ? '<currentuser>' : null) : null
		});
	}

	renderConditionalFilter() {
		return this.props.resource.viewjson.condition.map((conditionalitem, key) => {
			return (
				<div className="row" key={key}>
					<div className="col-xs-2 col-md-1 col-sm-1" style={{paddingRight: '0px'}}>
						<button type="button" onClick={()=>{this.deleteRow(key)}} className="btn gs-btn-danger btn-sm" rel="tooltip" title="Remove"><span className="fa fa-trash-o"></span></button>
					</div>
					<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-9">
						<Field name={`viewjson.condition[${key}].field`} props={{
							valuename: 'displayNameFilter',
							label: 'displayName',
							required: true,
							disabled: this.props.resource.issystemview,
							onChange: (value) => this.getOperators(conditionalitem, true, value, `viewjson.condition[${key}]`),
							options: [...this.state.advancedFilterArray]
						}} component={localSelectEle} validate={[stringNewValidation({
							required: true
						})]} />
					</div>
					<div style={{marginBottom: '5px'}} className="col-md-2 col-sm-2 col-xs-12">
						<Field name={`viewjson.condition[${key}].operator`} props={{
							valuename: 'displayNameFilter',
							label: 'displayName',
							required: true,
							disabled: this.props.resource.issystemview,
							onChange: (value) => this.operatorOnChange(conditionalitem, value, `viewjson.condition[${key}]`),
							options: conditionalitem.operatorOptions ? [...conditionalitem.operatorOptions] : []
						}} component={localSelectEle} validate={[stringNewValidation({
							required: true
						})]} />
					</div>
					{this.renderValueType(conditionalitem, key, `viewjson.condition[${key}]`)}
					{this.renderDefaultValue(conditionalitem, key, `viewjson.condition[${key}]`)}
					{this.renderViewConditionalfield(conditionalitem, key, `viewjson.condition[${key}]`)}
					{this.renderBetweenValue(conditionalitem, key, `viewjson.condition[${key}]`)}
				</div>
			);
		});
	}

	renderValueType(item, key, itemstr) {
		if(!item.options)
			return null;
		if(item.operator == 'Is Empty' || item.operator == 'Is Not Empty')
			return null;
		if((item.options.type == "date" && item.operator != "Between") || (item.options.type == "integer" && item.options.group == "selectasync" && item.options.foreignKeyOptions.resource == "users")) {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.valuetype`} props={{
						required: true,
						disabled: this.props.resource.issystemview,
						onChange: (value) => this.valuetypeOnChange(item, value, itemstr),
						options: [{name:'Custom Value', id:'customvalue'}, {name:'Default Value', id:'defaultvalue'}]
					}} component={localSelectEle} validate={[stringNewValidation({
						required: true
					})]} />
				</div>
			);
		}
	}

	renderDefaultValue(item, key, itemstr) {
		if(!item.options || item.valuetype != 'defaultvalue')
			return null;
		if((item.options.type == "date" && item.operator != "Between") || (item.options.type == "integer" && item.options.group == "selectasync" && item.options.foreignKeyOptions.resource == "users")) {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.value`} props={{
						required: true,
						disabled: this.props.resource.issystemview,
						options: item.options.type == "date" ? [...this.state.dateOptions] : [...this.state.userOptions]
					}} component={localSelectEle} validate={[stringNewValidation({
						required: true
					})]} />
				</div>
			);
		}
	}

	renderViewConditionalfield(item, key, itemstr) {
		if(!item.options)
			return null;

		if(item.operator == "Is Empty" || item.operator == "Is Not Empty")
			return null;

		if(item.options.type == "string" && item.options.group == "localselect" && item.operator != "In") {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.value`} props={{
						required: true,
						disabled: this.props.resource.issystemview,
						options: item.options.localOptions
					}} component={localSelectEle} validate={[stringNewValidation({
						required: true
					})]} />
				</div>
			);
		}

		if(item.options.type == "string" && item.options.group == "localselect" && item.operator == "In") {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.value`} props={{
						required: true,
						disabled: this.props.resource.issystemview,
						multiselect: true,
						options: item.options.localOptions
					}} component={localSelectEle} validate={[stringNewValidation({
						required: true
					})]} />
				</div>
			);
		}

		if(item.options.type == "integer" && item.options.group == "autocomplete") {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.value`} props={{
						resource: item.options.foreignKeyOptions.resource,
						fields: `id,${item.options.foreignKeyOptions.mainField.split('.')[1]}`,
						label: item.options.foreignKeyOptions.mainField.split('.')[1],
						required: true,
						disabled: this.props.resource.issystemview
					}} component={autoSelectEle} validate={[numberNewValidation({
						required: true
					})]} />
				</div>
			);
		}

		if(item.options.type == "boolean" && item.options.group == "simple") {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.value`} props={{
						options:[{id:true, name:'Yes'}, {id:false, name:'No'}],
						required: true,
						disabled: this.props.resource.issystemview
					}} component={localSelectEle} validate={[stringNewValidation({
						required: true
					})]} />
				</div>
			);
		}

		if(item.options.type == "integer" && item.options.group == "simple") {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.value`} props={{
						required: true,
						disabled: this.props.resource.issystemview
					}} component={NumberEle} validate={[numberNewValidation({
						required: true
					})]} />
				</div>
			);
		}

		if(item.options.type == "string" && item.options.group == "simple") {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.value`} props={{
						required: true,
						disabled: this.props.resource.issystemview
					}} component={InputEle} validate={[stringNewValidation({
						required: true
					})]} />
				</div>
			);
		}

		if(item.options.type == "date" && (item.operator == "Between" || item.valuetype == 'customvalue')) {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.value`} props={{
						required: true,
						disabled: this.props.resource.issystemview
					}} component={DateEle} validate={[dateNewValidation({
						required: true
					})]} />
				</div>
			);
		}

		if(item.options && item.options.type == "integer" && item.options.group == "selectasync" && (item.options.foreignKeyOptions.resource != 'users' || (item.options.foreignKeyOptions.resource == 'users' && item.valuetype == "customvalue"))) {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.value`} props={{
						resource: item.options.foreignKeyOptions.resource,
						fields: `id,${item.options.foreignKeyOptions.mainField.split('.')[1]}`,
						label: item.options.foreignKeyOptions.mainField.split('.')[1],
						required: true,
						disabled: this.props.resource.issystemview
					}} component={selectAsyncEle} validate={[numberNewValidation({
						required: true
					})]} />
				</div>
			);
		}
	}

	renderBetweenValue(item, key, itemstr) {
		if(!item.options)
			return null;

		if(item.options.type == "integer" && item.options.group == "simple" && item.operator == "Between") {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.value`} props={{
						required: true,
						disabled: this.props.resource.issystemview
					}} component={NumberEle} validate={[numberNewValidation({
						required: true
					})]} />
				</div>
			);
		}
		if(item.options.type == "date" && item.options.group == "simple" && item.operator == "Between") {
			return (
				<div style={{marginBottom: '5px'}} className="col-md-3 col-sm-3 col-xs-12">
					<Field name={`${itemstr}.betvalue`} props={{
						required: true,
						disabled: this.props.resource.issystemview
					}} component={DateEle} validate={[dateNewValidation({
						required: true
					})]} />
				</div>
			);
		}
	}

	onDragEnd(result) {
		const { source, destination, draggableId } = result;
		let jsonNameArr = draggableId.split('-');

		let viewjsonFields = [...this.props.resource.viewjson.fields]

		if (!destination || (destination.droppableId === source.droppableId && source.droppableId == 'column-1')) {
			return;
		}

		if (source.droppableId === destination.droppableId && source.droppableId != 'column-1') {
			let data = viewjsonFields[source.index];
			viewjsonFields.splice(source.index, 1);
			viewjsonFields.splice(destination.index, 0, data);

			this.props.updateFormState(this.props.form, {
				[`viewjson.fields`]: viewjsonFields
			});
			return;
		}

		if(jsonNameArr[1] == 'resourcejson') {
			if(viewjsonFields.length > 14) {
				alert("Sorry. You can add only 15 columns");
			} else {
				viewjsonFields.splice(destination.index, 0, jsonNameArr[0]);
				
				this.props.updateFormState(this.props.form, {
					[`viewjson.fields`]: viewjsonFields
				});
			}
		} else if(jsonNameArr[1] == 'pagejson') {
			viewjsonFields.splice(source.index, 1);

			this.props.updateFormState(this.props.form, {
				[`viewjson.fields`]: viewjsonFields
			});
		}
	}

	basicFilterCallback(val) {
		this.state.pagejson.viewjson.basicfilter = val;
		this.setState({pagejson: this.state.pagejson});
	}

	roleFilterCallback(value) {
		this.state.pagejson.roleids = value;
		this.setState({pagejson: this.state.pagejson});
	}

	userFilterCallback(value) {
		this.state.pagejson.userids = value;
		this.setState({pagejson: this.state.pagejson});
	}

	save(param) {
		if(this.props.resource.viewjson.basicfilter.length > 4) {
			alert("Please Choose only 4 basic filter");
		} else {
			this.updateLoaderFlag(true);
			let tempObj = JSON.parse(JSON.stringify(this.props.resource));
			let newcondition = [];
			tempObj.viewjson.condition.forEach((item) => {
				newcondition.push({
					"field":  item.field,
					"operator":  item.operator,
					"valuetype": item.valuetype,
					"value": item.value,
					"betvalue": item.betvalue
				});
			});
			tempObj.viewjson.condition = [...newcondition];

			axios({
				method : 'post',
				data : {
					actionverb : param,
					data : tempObj
				},
				url : '/api/views'
			}).then((response) => {
				if (response.data.message == 'success') {
					if(param != 'Delete')
						tempObj = response.data.main;
					else
						tempObj.deleteParam = true;

					this.props.updatePageJSONViewState(tempObj.pagename, 'List', tempObj);
					if(this.state.listTitle)
						this.props.updateListViewState(this.state.listTitle,  param == 'Delete' ? null : tempObj);

					setTimeout(() => {
						this.updateLoaderFlag(false);
						this.props.history.goBack();
					}, 0);
 
 					/*this.props.closeModal();
					if(param == 'Delete')
						this.props.callback({id: this.props.resource.id}, true);
					else
						this.props.callback(response.data.main);*/
				} else {
					this.updateLoaderFlag(false);
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}
	}

	close() {
		this.props.history.goBack();
	}

	renderDragDropContext(filteredFields) {
		const {app, resource} = this.props;
		const {basicFilterArray, rolesArray, usersArray, searchresourcefield, resourcejsonfields, resourcejson} = this.state;
		return (
			<DragDropContext onDragEnd={this.onDragEnd} >
				<div style={{width:"100%"}}>
					<div className = "list-view-body-column">
						<div className="list-search-column">
							<input type="text" className="form-control" value={searchresourcefield} onChange={(e)=>{this.setState({searchresourcefield: e.target.value})}} placeholder={'Search Column'}/>
						</div>
						<Droppable droppableId={'column-1'}>
							{(DroppableProvided, DroppableSnapshot) => (
								<div className="col-md-12 list-available-column" ref={DroppableProvided.innerRef}>
									<Listviewlistcontainer  jsonfieldarray={filteredFields}  searchterm={searchresourcefield} isreadable={resource.issystemview} rootapp={app}/>
									{DroppableProvided.placeholder}
								</div>
							)}
						</Droppable>
					</div>
					<div className="list-view-select-cloumns">
						<div className="col-md-12" style={{paddingRight:'10px'}}>
							<div className="list-select-column-title">SELECTED COLUMNS<span style={{marginLeft:'10px',fontSize:'12px',color:'#6eb5e2',fontWeight:'normal'}}>Drag here to select Cloumns</span></div>
							<Droppable droppableId={'column-2'}>
								{(DroppableProvided, DroppableSnapshot) => (
									<div ref={DroppableProvided.innerRef}>
										<div className="listcontainer-column">
										<Listviewlistcontainer  jsonfieldarray={resource.viewjson.fields.map((field) => {return {field, jsonname:'pagejson'}}) }  resourcename={resourcejson.resourceName} rootapp={app} isreadable={resource.issystemview} />
										</div>
									</div>
								)}
							</Droppable>
						</div>
					</div>
				</div>
			</DragDropContext>
		);
	}

	render() {
		const {app, resource} = this.props;
		const {basicFilterArray, rolesArray, usersArray, searchresourcefield, resourcejsonfields, resourcejson} = this.state;

		if(!resource)
			return null;

		let filteredFields = resourcejsonfields.filter((field) => {
			return resource.viewjson.fields.indexOf(field.field) == -1 ? true : false
		});

		let saveAccessClass = checkActionVerbAccess(this.props.app, 'views', 'Save') ? true : false;
		let deleteAccessClass = checkActionVerbAccess(this.props.app, 'views', 'Delete') ? true : false;

		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className={`col-sm-12 affixmenubar col-md-12 col-lg-9 d-sm-block d-md-flex justify-content-md-between  align-items-center`}>
							<div className="font-16 d-flex align-items-center">
								<a className="affixanchor float-left marginright-10 semi-bold-custom" onClick={this.close}><span className="fa fa-arrow-left "></span></a>
								<div className="gs-uppercase float-left semi-bold-custom">{resource.id > 0 ? 'Edit' : 'Create'} List View</div>
								<div className={`ml-3 ${resource.issystemview ? 'showsection' : 'hidesection'}`} style={{fontSize:'12px'}}>
										<span>( This is System View. You cannot save this view )</span>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-12 col-md-12 col-lg-9">
							<div className="row" style={{marginTop:'50px'}}>
								<div className="col-md-12">
									<div className="card" style={{marginBottom:'10px'}}>
										<div className="card-header listpage-title">VIEW DETAILS</div>
										<div className="card-body">
											<div className="row">
												<div className="col-md-6 col-sm-12 col-xs-12">
													<div className="row">
														<div className="form-group col-md-3 col-sm-6">
															<label>View Name</label>
														</div>
														<div className="form-group col-md-9 col-sm-6">
															<Field name={'viewname'} props={{
																required: true,
																disabled: resource.issystemview
															}} component={InputEle} validate={[stringNewValidation({
																required: true
															})]} />
														</div>
													</div>
												</div>
												<div className="col-md-6 col-sm-12 col-xs-12">
													<div className="row">
														<div className="form-group col-md-3 col-sm-6">
															<label>Basic Filters</label>
														</div>
														<div className="form-group col-md-9 col-sm-6">
															<Field name={'viewjson.basicfilter'} props={{
																valuename: 'field',
																label: 'name',
																required: true,
																disabled: resource.issystemview,
																multiselect: true,
																options: [...basicFilterArray]
															}} component={localSelectEle} validate={[multiSelectNewValidation({
																required: true
															})]} />
														</div>
													</div>
												</div>
												{app.user.roleid.indexOf(1) >=0 ? <div className="col-md-6 col-sm-12 col-xs-12">
													<div className="row">
														<div className="form-group col-md-3 col-sm-6">
															<label>Visible To</label>
														</div>
														<div className="form-group col-md-9 col-sm-6">
															<Field name={'visibility'} props={{
																required: true,
																disabled: resource.issystemview,
																onChange: this.visibilityOnchange,
																options: [{name:'MySelf', id:'myself'}, {name:'All Users', id:'all'}, {name:'Selected Roles', id:'roles'}, {name:'Selected Users', id:'users'}]
															}} component={localSelectEle} validate={[stringNewValidation({
																required: true
															})]} />
														</div>
													</div>
												</div> : null}
												{(app.user.roleid.indexOf(1) >= 0 && resource.visibility == 'roles') ? <div className="col-md-6 col-sm-12 col-xs-12">
													<div className="row">
														<div className="form-group col-md-3 col-sm-6">
															<label>Roles</label>
														</div>
														<div className="form-group col-md-9 col-sm-6">
															<Field name={'roleids'} props={{
																required: true,
																multiselect: true,
																options: [...rolesArray],
																disabled: resource.issystemview
															}} component={localSelectEle} validate={[multiSelectNewValidation({
																required: true
															})]} />
														</div>
													</div>
												</div> : null}
												{(app.user.roleid.indexOf(1) >= 0 && resource.visibility == 'users') ? <div className="col-md-6 col-sm-12 col-xs-12">
													<div className="row">
														<div className="form-group col-md-3 col-sm-6">
															<label>Users</label>
														</div>
														<div className="form-group col-md-9 col-sm-6">
															<Field name={'userids'} props={{
																label: 'displayname',
																required: true,
																disabled: resource.issystemview,
																multiselect: true,
																options: [...usersArray]
															}} component={localSelectEle} validate={[multiSelectNewValidation({
																required: true
															})]} />
														</div>
													</div>
												</div> : null}
												<div className="col-md-6 col-sm-12 col-xs-12">
													<div className="row">
														<div className="form-group col-md-3 col-sm-6">
															<label>Is Default</label>
														</div>
														<div className="form-group col-md-9 col-sm-6">
															<div className="col-md-1">
																<div>
																	<Field name={'isdefault'} component={checkboxEle} />
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className="col-md-12">
									<div className="card" style={{marginBottom:'10px'}}>
										<div className="card-header listpage-title">FILTER CONDITIONS</div>
										<div className="card-body">
											{this.renderConditionalFilter()}
											<div className="row">
												<div className="form-group col-md-12 col-sm-12 col-xs-12">
													<button type="button" onClick={()=>{this.addRow()}} rel="tooltip" title="Add Row"  className="btn btn-sm btn-gs btn-outline-primary filter-button" disabled={resource.issystemview}><span className="fa fa-plus" ></span> Add Filter</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className="col-md-12 col-sm-12 col-xs-12 displayeven">
									<div className ="card" style={{marginBottom:'3rem'}}>
										<div className="card-header listpage-title">
											<div className="listpage-title-name">SELECTED COLUMN TO BE DISPLAYED
											</div>
										</div>
										<div className="card-body" style={{paddingTop:'15px'}}>
											<div className="col-md-12 col-sm-12 col-xs-12" style={{paddingTop:'0px'}}>
												<div className="row">
													{this.renderDragDropContext(filteredFields)}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-12 col-md-12 col-lg-9">
							<div className="muted credit text-center sticky-footer actionbtn-bar" style={{boxShadow:'none'}}>
								<button
									type="button"
									className="btn btn-sm btn-width btn-secondary"
									onClick={()=>{this.close();}}>
									<i className="fa fa-times marginright-5"></i>Close
								</button>
								{saveAccessClass && !resource.issystemview ? <button
									type="button"
									className="btn btn-width btn-sm gs-btn-success"
									disabled={this.props.invalid}
									onClick={()=>{this.save('Save');}}>
									<i className="fa fa-save"></i>Save
								</button> : null}
								{deleteAccessClass && resource.id && !resource.issystemview && (resource.createdby == this.props.app.user.id || this.props.app.user.roleid.indexOf(1) >= 0) ? <button
									type="button"
									className="btn btn-width btn-sm gs-btn-danger"
									onClick={() => this.save('Delete')}
									disabled={this.props.invalid}>
									<i className="fa fa-trash-o"></i>Delete
								</button> : null}
							</div>
 						</div>
					</div>
				</form>
			</div>
		);
	}
};

ListPageEditModal = connect(
	(state, props) => {
		let formName = 'List_View';
		return {
			app : state.app,
			form : formName,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState, updatePageJSONViewState, updateListViewState }
)(reduxForm()(ListPageEditModal));

export default ListPageEditModal;
