import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Modal from 'react-modal';

import {closeModalState} from '../actions/actions';

class ModalManager extends Component {
	constructor(props) {
		super(props);
		this.renderModal = this.renderModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal(modal) {
		this.props.closeModalState(modal);
	}

	renderModal() {
		return this.props.modal.map((modal, key) => {
			return (
				<Modal
					key={key}
					isOpen={modal.isOpen}
					onRequestClose={() => {modal.confirmModal ? null : this.closeModal(modal)}}
					shouldCloseOnOverlayClick={modal.confirmModal ? false : true}
					ariaHideApp={false}
					aria={{
					labelledby: "heading",
					describedby: "full_description"}}
					className={modal.className.content}
					overlayClassName={modal.className.overlay}>
					{modal.render(() => this.closeModal(modal))}
				</Modal>
			);
		});
	}

	render() {
		return <div>{this.renderModal()}</div>
	}
}

export default connect((state)=>{
	return {modal: state.modal, app: state.app}
}, (dispatch) => {
	return bindActionCreators({closeModalState}, dispatch);
}) (ModalManager);
