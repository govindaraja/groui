import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import {updateAppState} from '../actions/actions';
import { commonMethods, modalService, accessResource } from '../utils/services';
import { Buttongroup } from '../components/formelements';
import { currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';
import { TimeAgo, TaskDueDateFilter } from '../components/utilcomponents';


class DashboardForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dashboardFullObj: {
				tasks: []
			},
			task: {
				options : 'Overdue Tasks'
			},
			iconObj: {
				Sales : 'fa fa-usd marginright-10',
				Purchase : 'fa fa-shopping-cart marginright-10',
				Stock : 'fa fa-archive marginright-10',
				Accounts : 'fa fa-pencil-square-o marginright-10',
				Locations : 'fa fa-globe marginright-10',
				Projects : 'fa fa-briefcase marginright-10',
				Service : 'fa fa-cogs marginright-10',				
				Production : 'fa fa-spinner marginright-10',
				HR : 'fa fa-group marginright-10',
				Admin : 'fa fa-cog marginright-10'
			}
		};
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.onLoadDashboard = this.onLoadDashboard.bind(this);
		this.onLoadTasks = this.onLoadTasks.bind(this);
		this.tabonChange = this.tabonChange.bind(this);
		this.openLink = this.openLink.bind(this);
		this.activityOnClick = this.activityOnClick.bind(this);
		this.btngroupOnChange = this.btngroupOnChange.bind(this);
		this.viewAllTask = this.viewAllTask.bind(this);
		this.renderActivityStatus = this.renderActivityStatus.bind(this);
		this.createActivity = this.createActivity.bind(this);
	}

	componentWillMount() {
		document.getElementById("pagetitle").innerHTML = 'Dashboard';
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad = () => {
		if(localStorage.getItem("pageafterlogin")) {
			let redirectPath = localStorage.getItem("pageafterlogin");
			localStorage.setItem("pageafterlogin", '');
			this.props.history.replace(`${redirectPath}`);
		} else {
			this.onLoadDashboard();
			this.onLoadTasks();
		}
		this.updateLoaderFlag(false);
	}

	onLoadDashboard = () => {
		let { dashboardFullObj } = this.state;
		axios.get(`/api/initialize/initializedashboard`).then((response) => {
			dashboardFullObj.types = {
				Alerts : {
					Sales : {count: 0, items: []},
					Projects : {count: 0, items: []},
					Purchase : {count: 0, items: []},
					Stock : {count: 0, items: []},
					Accounts : {count: 0, items: []},
					Service : {count: 0, items: []},
					Production : {count: 0, items: []},
					HR : {count: 0, items: []},
					Admin : {count: 0, items: []},
					count : 0,
					active: true
				},
				Approvals : {
					Sales : {count: 0, items: []},
					Projects : {count: 0, items: []},
					Purchase : {count: 0, items: []},
					Stock : {count: 0, items: []},
					Accounts : {count: 0, items: []},
					Service : {count: 0, items: []},					 
					Production : {count : 0, items: []},
					HR : {count: 0, items: []},
					Admin : {count: 0, items: []},
					count : 0,
					active: false
				}
			};

			let dashboardArr = response.data;
			let tempCount = 0;
			dashboardArr.forEach((item) => {
				item.count = Number(item.count) || 0;
				if (item.count > 0) {
					tempCount += Number(item.count);
				}
			});
			let notifyAlert = tempCount;

			dashboardArr.forEach((dashitem) => {
				dashboardFullObj.types[dashitem.type].count += dashitem.count;
				if(dashboardFullObj.types[dashitem.type] && dashboardFullObj.types[dashitem.type][dashitem.displayGroup]) {
					dashboardFullObj.types[dashitem.type][dashitem.displayGroup].count += dashitem.count;
					dashboardFullObj.types[dashitem.type][dashitem.displayGroup].items.push(dashitem);
				}
			});

			this.setState({ dashboardFullObj });
		});
	}

	onLoadTasks = () => {
		let { dashboardFullObj } = this.state;
		if (this.props.app.user && accessResource('salesactivities', 'Read', this.props.app)) {

			axios.get(`/api/salesactivities?field=id,activitytypeid,plannedstarttime,plannedendtime,partnerid,contactid,partners/name/partnerid,contacts/name/contactid,activitytypes/category/activitytypeid,activitytypes/name/activitytypeid,users/displayname/createdby,users/displayname/createdby,users/displayname/assignedto,description,status,duedate,plannedstarttime,plannedendtime,startdatetime,enddatetime,travelstarttime,travelendtime,created,completedon,completionremarks&filtercondition=(salesactivities.assignedto=${this.props.app.user.id})&pagelength=10`).then((response) => {
				if (response.data.message == 'success') {
					response.data.main.forEach((item) => {
						let dueDate = new Date(item.duedate).setHours(0,0,0,0);
						let today = new Date().setHours(0,0,0,0);
						if(dueDate < today) {
							item.classname = "text-danger";
						} else if(dueDate == today) {
							item.classname = "text-primary";
						} else if(dueDate > today) {
							item.classname = "text-success";
						}
					});
					dashboardFullObj.activities = response.data.main;
					dashboardFullObj.activitiesCount = response.data.count;
					this.setState({ dashboardFullObj });
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}
	}

	tabonChange = (type) => {
		let { dashboardFullObj, alertCollapsed, approvalCollapsed } = this.state;
		alertCollapsed = false;
		approvalCollapsed = false;

		if (type == 'Alerts')
			alertCollapsed = true;
		else if (type == 'Approvals')
			approvalCollapsed = true;

		for (var prop in dashboardFullObj.types)
			dashboardFullObj.types[prop].active = false;

		dashboardFullObj.types[type].active = true;
		this.setState({ dashboardFullObj, alertCollapsed, approvalCollapsed });
	}

	openLink = (prop) => {
		let tempobj = {
			...this.props.app.listFilterObj,
			[prop.resource]: prop.filter
		};
		this.props.updateAppState('listFilterObj', tempobj);
		var newRouteObj = {
			'/complaints': '/list/customercomplaints',
			'/deliveryPlannerReport': '/deliverynoteplannerreport',
			'/receiptNotePlanner': '/receiptnoteplannerreport',
			'/salesInvoiceTool': '/salesinvoicetoolreport',
			'/accountsrecivablereport': '/accountsreceivablereport',
			'/accountsfollowup': '/list/accountsfollowup',
			'/purchaseplanner': '/purchaseplannerreport',
			'/activities': '/list/activities',
			'/collectionstatus': 'collectionstatusreport',
			'/salesReturns': '/list/salesreturns',
			'/purchaseReturns': '/list/purchasereturns',
			'/list/creditnote': '/list/creditnotes',
			'/list/debitnote': '/list/debitnotes'
		};
		let route = newRouteObj[prop.route] ? newRouteObj[prop.route] : prop.route;
		this.props.history.push(route);
	};

	activityOnClick = (taskObj) => {
		this.props.history.push(`/details/activities/${taskObj.id}`);
	}

	btngroupOnChange = (val) => {
		let { task } = this.state;
		task.options = val;
		this.setState({ task });
	};

	viewAllTask = () => {
		let tempobj = {
			'salesactivities': [{
				"fieldname" : "assignedto",
				"operator" : "Equal",
				"value" : this.props.app.user.id,
				"displayValue": this.props.app.user.displayname
			}]
		};
		this.props.updateAppState('listFilterObj', tempobj);
		this.props.history.push(`/list/activities`);
	};

	createActivity() {
		this.props.history.push({pathname: '/createActivity', params: {assignedto: this.props.app.user.issalesperson ? this.props.app.user.id: null}});
	}

	renderActivityStatus(activity) {
		let statusObj={
			"Open" :{
				statusClass : "gs-bg-info text-primary semi-bold"
			},
			"Planned" : {
				statusClass : "gs-bg-info text-primary semi-bold"
			},
			"Inprogress" : {
				statusClass : "gs-bg-warning gs-text-warning semi-bold"
			},
			"Completed" : {
				statusClass : "gs-bg-success text-success semi-bold"
			},
			"Cancelled" : {
				statusClass : "gs-bg-danger gs-text-danger semi-bold"
			}
		};

		if(activity.duedate && ['Open','Planned'].includes(activity.status))
			return <span className="gs-bg-danger gs-text-danger font-12" style={{padding: '2px 5px', borderRadius: '2px', whiteSpace: 'nowrap'}}>{`Due on ${dateFilter(activity.duedate)}`}</span>;
		if(activity.completedon && activity.status == 'Completed')
			return <span className="gs-bg-success text-success font-12" style={{padding: '2px 5px', borderRadius: '2px', whiteSpace: 'nowrap'}}>{`Completed `}<TimeAgo date={activity.completedon} /></span>;
		if(activity.enddatetime && activity.status == 'Completed')
			return <span className="gs-bg-success text-success font-12" style={{padding: '2px 5px', borderRadius: '2px', whiteSpace: 'nowrap'}}>{`Completed `}<TimeAgo date={activity.enddatetime} /></span>;
		if(activity.status == 'Planned' && activity.plannedstarttime)
			return <span className="gs-bg-info text-primary font-12" style={{padding: '2px 5px', borderRadius: '2px', whiteSpace: 'nowrap'}}>{`Planned for ${datetimeFilter(activity.plannedstarttime)}`}</span>;

		return <span className={`font-12 ${statusObj[activity.status] ? statusObj[activity.status].statusClass : 'gs-bg-success text-success'}`} style={{"borderRadius": "2px","textAlign": "center","padding": "2px 5px", whiteSpace: 'nowrap'}}>{activity.status}</span>
	}

	render() {
		let { dashboardFullObj } = this.state;
		let windowHeight = $(window).height();
		if(!dashboardFullObj || !dashboardFullObj.types)
			return null;

		return(
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row" style={{marginTop: '10px'}}>
					<div className="col-md-9 col-dm-12">
						<ul className="nav nav-tabs custom-nav-tabs">
							{Object.keys(dashboardFullObj.types).map((type) => {
								let tabClass = (type == 'Alerts') ? 'alert-active' : 'approval-active';

								return (
									<li key={type} className={`nav-item nav-link ${tabClass} ${dashboardFullObj.types[type].active ? 'active' : ''}`} onClick={() => this.tabonChange(type)}>
										<span className="semi-bold-custom font-16">{dashboardFullObj.types[type].count > 100  ? '99+' : (dashboardFullObj.types[type].count < 10 ? `0${dashboardFullObj.types[type].count}` : dashboardFullObj.types[type].count)}</span>
										<span className="marginleft-10 font-16 gs-uppercase">{type}</span>
									</li>
								)
							})}
						</ul>
						<div className="tab-content dashboard-tab-content gs-scrollbar" style={{height:`${windowHeight - 100}px`, overflowY: 'scroll', overflowX: 'hidden'}}>
							{Object.keys(dashboardFullObj.types).map((type) => {
								return (
									<div key={type} className={`tab-pane fade ${dashboardFullObj.types[type].active ? 'show active': ''}`} style={{padding: '15px 0px'}}>
										{Object.keys(dashboardFullObj.types[type]).map((key) => {
											if(key == 'active' || key == 'count')
												return null;
											if(dashboardFullObj.types[type][key].count > 0) {
												return (
													<div key={key} className="row">
														<div className="col-md-12 col-ms-12">
															<div className="card marginbottom-10" style={{border: 'none'}}>
																<div className="card-header card-header-custom gs-card-header paddingleft-5">{key}</div>
																<div className="card-body" style={{padding: '5px 15px'}}>
																	<div className="row">
																		{dashboardFullObj.types[type][key].items.map((item, itemkey) => {
																			if(item.count > 0) {
																				return (
																					<div key={itemkey} className="form-group col-md-4 col-sm-6 col-xs-12">
																						<ul className="list-group menu-list">
																							<li className="list-group-item listitem anchorclass dashboard-listitem" style={{border: '0px'}} onClick={() => this.openLink(item)}><span className="text-danger semi-bold-custom marginright-5" style={{width: '24px', display: 'inline-block'}}>{item.count > 100  ? '99+' : (item.count < 10 ? `0${item.count}` : item.count)}</span> {item.displayName}</li>
																						</ul>
																					</div>
																				)
																			}
																		})}
																	</div>
																</div>
															</div>
														</div>
													</div>
												)
											}
										})}
									</div>
								)
							})}
						</div>
					</div>
					<div className="col-md-3 col-sm-12 paddingleft-0">
						<div style={{padding: '8px 8px 10px 8px'}} className="text-color-custom semi-bold-custom font-16 gs-uppercase">Activities
							<div className="text-right float-right">
								<button type="button" className="btn btn-sm gs-form-btn-success gs-btn-lg marginright-15" onClick={this.createActivity} style={{fontSize: '14px', textTransform : 'capitalize'}}>Add Activity</button>
							</div>
						</div>
						<div className="row">
							<div className="col-md-12 col-sm-12">
								<div className="gs-scrollbar bg-white" style={{height:`${windowHeight - 100}px`, overflowY: 'auto', overflowX: 'hidden'}}>
									{dashboardFullObj.activities && dashboardFullObj.activities.length > 0 ? <ul className="list-group menu-list">
										{dashboardFullObj.activities.map((activity, key) => {
											return (
												<li key={key} className="list-group-item anchorclass" onClick={() => this.activityOnClick(activity)} >
													<div className="d-flex flex-row flex-wrap" style={{wordBreak: 'break-word', marginBottom: '6px'}}>
														<span className="semi-bold mr-2">{activity.activitytypeid_name}</span>
														<span className="gs-badge-default font-12 text-center" style={{"borderRadius": "2px","padding": "2px 5px"}}>{activity.status}</span>
													</div>
													{activity.partnerid ? <div style={{marginBottom: '6px'}}>
														<span className="fa fa-building-o mr-2"></span>
														<span>{activity.partnerid_name}</span>
													</div> : null}
													{activity.contactid ? <div style={{marginBottom: '6px'}}>
														<span className="fa fa-id-card-o mr-2"></span>
														<span>{activity.contactid_name}</span>
													</div> : null}
													<div className="text-ellipsis-line-2" style={{marginBottom: '6px', display: 'block'}}>{activity.description}</div>
													<div>
														<span className="gs-badge-default font-12 d-inline-block mr-2" style={{padding: '2px 5px', borderRadius: '2px'}}>
															<span className="fa fa-user-o mr-2"></span>{activity.assignedto_displayname}
														</span>
														{this.renderActivityStatus(activity)}
													</div>
												</li>
											)
										})}
										{dashboardFullObj.activities.length > 0 ? <li className="list-group-item menu-list-hide-hover text-center">
											<button type="button" className="btn btn-sm gs-btn-lg gs-form-btn-success" onClick={this.viewAllTask}>View All</button>
										</li> : null }
									</ul> : <div className="task-placeholder">
										<div className="list-error-image-box">
											<img src="../images/task-empty-placeholder.png" className="list-error-image" width="100" height="100" />
										</div>
										<div className="list-error-text-box text-center">
											<div className="font-14 form-group">You don't have any pending activities!</div>
											<div className="marginbottom-10"><button type="button" className="btn btn-sm gs-btn-outline-success gs-btn-lg" onClick={this.createActivity}>Add Activity</button></div>
											<div><button type="button" className="btn btn-sm gs-btn-outline-success gs-btn-lg" onClick={this.viewAllTask}>View All</button></div>
										</div>
									</div>}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	};
}

DashboardForm = connect((state) =>{
	return {app: state.app}
}, { updateAppState }) (DashboardForm);

export default DashboardForm
3