import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { Link } from 'react-router-dom';
import { v1 as uuidv1 } from 'uuid';
import moment from 'moment';
import ReactPaginate from 'react-paginate';

import { updateFormState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter, datetimeFilter } from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';
import { InputEle, NumberEle, SpanEle, DateEle, textareaEle, selectAsyncEle, localSelectEle, autoSelectEle, richtextEle, qtyEle, emailEle, checkboxEle, ContactEle, DisplayGroupEle, RateInline, StockInline, AddressEle, autosuggestEle, ButtongroupEle, RateField, DiscountField, TimepickerEle, alertinfoEle, PhoneElement, passwordEle, DocumentLinkEle, ExpenseRequestSummary } from '../components/formelements';
import {stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation } from '../utils/utils';

import { LocalSelect, SelectAsync, AutoSelect, DateElement } from '../components/utilcomponents';

const pageLengths = [20, 50, 100, 500];

class OrderCheckList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : true,
			filter: {}
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.save = this.save.bind(this);
		this.getData = this.getData.bind(this);
		this.renderPagination = this.renderPagination.bind(this);
		this.pagecountChange = this.pagecountChange.bind(this);
		this.handlePaginationClick = this.handlePaginationClick.bind(this);
		this.openHistory = this.openHistory.bind(this);
	}

	componentWillMount() {
		if(!this.props.resource) {
			this.props.initialize({
				page: 0,
				pagelength: 20,
				orderObjArray: []
			});
		} else {
			this.setState({
				filter: this.props.resource.filter ? this.props.resource.filter : {}
			});
		}
		setTimeout(this.getData, 0);
	}

	filterOnChange(value, param) {
		this.state.filter[param] = value;
		this.setState({
			filter: this.state.filter
		});
	}

	pagecountChange(pagelength) {
		this.updateLoaderFlag(true);
		this.props.updateFormState(this.props.form, { pagelength });
		setTimeout(this.getData, 0);
	}

	handlePaginationClick(page) {
		this.updateLoaderFlag(true);
		this.props.updateFormState(this.props.form, { page: page.selected });
		setTimeout(this.getData, 0);
	}

	reset() {
		this.updateLoaderFlag(true);
		this.setState({
			filter: {}
		});
		this.props.updateFormState(this.props.form, {
			queryString: null
		});
		setTimeout(this.getData, 0);
	}

	search() {
		this.updateLoaderFlag(true);
		let filterArray = [];
		if(this.state.filter.orderid > 0)
			filterArray.push(` orderchecklists.orderid=${this.state.filter.orderid} `);
		if(this.state.filter.followupdate)
			filterArray.push(` orderchecklists.followupdate::DATE='${new Date(this.state.filter.followupdate).toDateString()}'::DATE `);
		if(this.state.filter.outcome)
			filterArray.push(` orderchecklists.outcome='${this.state.filter.outcome}' `);

		this.props.updateFormState(this.props.form, {
			queryString: filterArray.join(' AND '),
			filter: this.state.filter

		});
		setTimeout(this.getData, 0);
	}

	getData() {
		this.updateLoaderFlag(true);
		var tempSkip = (this.props.resource.page) * this.props.resource.pagelength;

		var filterQuery = `orders_orderid.status='Approved'`;

		if(this.props.resource.queryString)
			filterQuery += ` AND ${this.props.resource.queryString}`;

		var queryString = `/api/orderchecklists?pagelength=${this.props.resource.pagelength}&field=id,orderno,orderid,itemdescription,itemid,outcome,remarks,followupdate,history,checklistitem,orderchecklistmaster/checklist/checklistitem,modified,created,orders/status/orderid,users/displayname/createdby,users/displayname/modifiedby&skip=${tempSkip}&filtercondition=${filterQuery}`


		axios.get(queryString).then((response) => {
			if(response.data.message == 'success') {
				let orderObjArray = [];
				let resultarray = response.data.main;
				for (var i = 0; i < resultarray.length; i++) {
					var itemFound = false;
					for (var j = 0; j < orderObjArray.length; j++) {
						if (resultarray[i].orderid == orderObjArray[j].orderid && resultarray[i].itemid == orderObjArray[j].itemid) {
							itemFound = true;
							orderObjArray[j].orderitems.push(resultarray[i]);
							break
						}
					}
					if (!itemFound) {
						var tempObject = {
							orderid : resultarray[i].orderid,
							orderno : resultarray[i].orderno,
							itemid : resultarray[i].itemid,
							itemdescription : resultarray[i].itemdescription,
							orderitems : []
						};
						tempObject.orderitems.push(resultarray[i]);
						orderObjArray.push(tempObject);
					}
				}
				this.props.updateFormState(this.props.form, {
					orderObjArray: orderObjArray,
					count: response.data.count
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	save (order) {
		this.updateLoaderFlag(true);

		order.orderitems.forEach((orderitem) => {
			if(orderitem.outcome != 'Followup')
				orderitem.followupdate = '';
		});

		axios({
			method : 'post',
			data : {
				actionverb : 'Save',
				data : order.orderitems
			},
			url : '/api/orderchecklists'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

			if(response.data.message == 'success')
				this.getData();
			else
				this.updateLoaderFlag(false);
		});
	}

	openHistory(orderitem) {
		this.props.openModal({
			render: (closeModal) => {
				return <HistoryModal orderitem={orderitem} closeModal={closeModal}/>
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	renderPagination() {
		if(this.props.resource && Math.ceil(this.props.resource.count / this.props.resource.pagelength) > 1)
			return <ReactPaginate previousLabel={<span>&laquo;</span>}
				nextLabel={<span>&raquo;</span>}
				breakLabel={<a></a>}
				breakClassName={"break-me"}
				pageCount={Math.ceil(this.props.resource.count / this.props.resource.pagelength)}
				marginPagesDisplayed={0}
				pageRangeDisplayed={5}
				onPageChange={this.handlePaginationClick}
				containerClassName={"list-pagination"}
				subContainerClassName={"pages pagination"}
				activeClassName={"active"}
				forcePage={this.props.resource.page} />
		return null;
	}

	render() {
		return(
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="col-md-12 marginbottom-10 bg-white list-title-bar" style={{borderBottom: '1px solid #DEE2E6'}}>
						<div className="row">
							<div className="col-md-7" style={{display:'inline-block',marginTop: '10px',marginBottom: '10px',verticalAlign: 'middle',lineHeight: '2'}}>
								<div className="row">
									<div className="col-md-12">
										<div style={{float: 'left', paddingLeft: '10px'}}>
											<label className="gs-text-color gs-uppercase semi-bold-custom" style={{fontSize: '16px', marginRight: '15px'}}>Order Checklist</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className={`col-md-12 bg-white marginbottom-10`} style={{border: '1px solid #F5F5F5'}}>
						<div className="row" style={{margin: '0 auto',padding: '10px'}}>
								<div className="col-md-3">
									<AutoSelect value={this.state.filter.orderid} onChange={(value) => this.filterOnChange(value, 'orderid')} resource="orders" fields="id,orderno" label="orderno" placeholder="Search by Order no" />
								</div>
								<div className="col-md-3">
									<DateElement className="form-control" value={this.state.filter.followupdate} onChange={(value) => this.filterOnChange(value, 'followupdate')}  placeholder="Search by Follow up" />
								</div>
								<div className="col-md-3">
									<LocalSelect value={this.state.filter.outcome} onChange={(value) => this.filterOnChange(value, 'outcome')} options={['Available', 'Not Required', 'Followup']} placeholder="Search by Outcome" />
								</div>
								<div className="col-md-3">
									<button type="button" className="btn gs-form-btn-primary btn-sm btn-width marginleft-5" onClick={() => this.search()}>Search</button> <button type="button" className="btn gs-form-btn-secondary btn-sm btn-width marginleft-5" onClick={() => this.reset()}>Reset</button>
								</div>
						</div>
					</div>
					<div className="col-md-12">
						{this.props.resource ? <div className="col-md-12 form-group bg-white" style={{paddingLeft: 0, paddingRight: 0, borderRight:'1px solid #f3f3f3d7', borderLeft:'1px solid #f3f3f3d7', borderBottom:'1px solid #f3f3f3d7'}}>
							<table className="table table-bordered" style={{marginBottom:0}}>
								<thead>
									<tr>
										<th>Checklist Item</th>
										<th>Outcome</th>
										<th>Followup Date</th>
										<th>Remarks</th>
										<th>History</th>
									</tr>
								</thead>
								{this.props.resource.orderObjArray.map((order, orderindex) => {
									return (
										<tbody key={orderindex}>
											<tr>
												<td colSpan={5}><b>Order No : {order.orderno}  ( Item :  {order.itemdescription})</b></td>
											</tr>
											{order.orderitems.map((orderitem, orderitemindex) => {
												let tempModel = `orderObjArray[${orderindex}].orderitems[${orderitemindex}]`;
												return (
													<tr key={orderitemindex}>
														<td>{orderitem.checklistitem_checklist}</td>
														<td>
															<Field name={`${tempModel}.outcome`} props={{
																buttons: [{"title": "Available", "value": "Available"}, {"title": "Not Required", "value": "Not Required"}, {"title": "Followup", "value": "Followup"}]
															}} component={ButtongroupEle}  validate={[]}/>
														</td>
														<td>
															<Field name={`${tempModel}.followupdate`} props={{
																disabled: orderitem.outcome != 'Followup'
															}} component={DateEle}  validate={[dateNewValidation({
																required: `resource.${tempModel}.outcome == 'Followup'`
															})]}/>
														</td>
														<td>
															<Field name={`${tempModel}.remarks`} props={{}} component={InputEle}  validate={[]}/>
														</td>
														<td>
															{orderitem.history ? <button type="button" className="btn btn-sm btn-secondary" onClick={() => this.openHistory(orderitem)}>History</button> : null}
														</td>
													</tr>
												);
											})}
											<tr>
												<td className="text-center" colSpan={5}>
													<button type="button" className="btn btn-success" onClick={() => this.save(order)}><i className="fa fa-check"></i>Save</button>
												</td>
											</tr>
										</tbody>
									);
								})}
							</table>
						</div> : null}
						{this.props.resource ? <div className="row list-footer-bar">
							<div className="col-md-6 col-sm-6 col-12 marginbottom-xs-10 btn-group btn-group-sm list-pagecount float-left" role="group">
								{pageLengths.map((count, btnkey) => {
									return <button type="button" className={`btn btn-sm gs-btn-light ${this.props.resource.pagelength == count ? 'active' : ''}`} key={btnkey} onClick={() =>{this.pagecountChange(count)}}>{count}</button>
								})}
							</div>
							<div className="col-md-6 col-sm-6 col-12 btn-group text-right gs-text-left-xs">
								{this.renderPagination()}
							</div>
						</div> : null}
					</div>
				</div>
			</div>
		);
	}
}

class HistoryModal extends Component {
	constructor(props) {
		super(props);

		let historyarr = [];
		for (var prop in this.props.orderitem.history)
			historyarr.push(this.props.orderitem.history[prop]);
		
		this.state = {
			historyarr : historyarr
		};
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Version History</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="form-group col-md-12">
							<table className="table table-hover">
								<thead>
									<tr>
										<th className="text-center">Outcome</th>
										<th className="text-center">Followup Date</th>
										<th className="text-center">Remarks</th>
										<th className="text-center">Modified By</th>
										<th className="text-center">Modified</th>
									</tr>
								</thead>
								<tbody>
									{this.state.historyarr.map((history, index) => {
										return (
											<tr key={index}>
												<td className="text-center">{history.outcome}</td>
												<td className="text-center">{dateFilter(history.followupdate)}</td>
												<td className="text-center">{history.remarks}</td>
												<td className="text-center">{history.modifiedby}</td>
												<td className="text-center">{datetimeFilter(history.modified)}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

OrderCheckList = connect(
	(state, props) => {
		let formName = 'OrderCheckList';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState}
)(reduxForm()(OrderCheckList));

export default OrderCheckList;
