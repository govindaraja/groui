import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { reduxForm, Field,formValueSelector } from 'redux-form';
import { Prompt } from "react-router-dom";
import { updateFormState, updateReportFilter } from '../actions/actions';
import { commonMethods, modalService, generateExcel } from '../utils/services';
import { LocalSelect, SelectAsync, AutoSelect, DateElement, ChildEditModal, AutoSuggest ,AutoMultiSelect} from '../components/utilcomponents';
import Loadingcontainer from '../components/loadingcontainer';
import { InputEle, localSelectEle, autoSelectEle, checkboxEle, NumberEle, DateEle, selectAsyncEle, dateTimeEle, autosuggestEle } from '../components/formelements';
import {stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, requiredNewValidation, checkActionVerbAccess, checkArray } from '../utils/utils';
import {currencyFilter,dateFilter} from '../utils/filter';

import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class PerformanceTargetDashBoard extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag: false,
			filterToggleOpen: true
		};

		this.getReportData = this.getReportData.bind(this);
		this.renderTarget = this.renderTarget.bind(this);
		this.openViewMoreModal = this.openViewMoreModal.bind(this);
		this.targetperiod = this.targetperiod.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.dateOnChange = this.dateOnChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag)
			this.updateLoaderFlag(false);
	}

	componentWillMount() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				currencyid : this.props.app.defaultCurrency,
				companyid : this.props.app.selectedcompanyid,
				month : new Date().getMonth(),
				year : new Date().getFullYear(),
				userid : this.props.app.user.id
			};
		
		this.props.initialize(tempObj);

		setTimeout(() => {
			this.dateOnChange();
		}, 0);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	targetperiod(target){
		let period = [],currentperiod = '';
		let monthObj = {
			'0' : 'Jan','1' : 'Feb','2' : 'Mar','3' : 'Apr','4' : 'May','5' : 'Jun','6' : 'Jul','7' : 'Aug','8' : 'Sep','9' : 'Oct','10' : 'Nov','11' : 'Dec'
		};
		let startdate = new Date(target.targetstart);
		let startmonth = new Date(target.targetstart).getMonth();
		let startyear = new Date(target.targetstart).getFullYear();
		let enddate = new Date(target.targetend);
		let endmonth = new Date(target.targetend).getMonth();
		let endyear = new Date(target.targetend).getFullYear();

		let startyearShort = `${startyear}`;
		startyearShort = startyearShort.substr(startyearShort.length - 2, startyearShort);
		let endyearShort = `${endyear}`;
		endyearShort = endyearShort.substr(endyearShort.length - 2, endyearShort);

		if(startmonth == endmonth && startyear == endyear){
			return `${monthObj[startmonth]} ${startyear}`;
		}

		return `${monthObj[startmonth]} ${startyearShort} - ${monthObj[endmonth]} ${endyearShort}`;
	}

	getReportData() {
		this.updateLoaderFlag(true);
		axios.get(`/api/query/getperformancetargetdashboardquery?userid=${this.props.resource.userid}&month=${this.props.resource.month}&year=${this.props.resource.year}`).then((response) => {
			if (response.data.message == 'success') {
				let targets = response.data.main;
				this.setState({ targets });

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(response.data.main.length > 0)
					this.updateToggleState(false);
			} else {
				this.updateLoaderFlag(false);
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	dateOnChange(){
		if(this.props.resource.month >=0 && this.props.resource.year && (this.props.resource.year).toString().length == 4 ){
			this.getReportData()
		}
	}

	resetFilter () {
		let tempObj = {
			month : null,
			year : null
		};

		this.setState({ targets : null });
		this.props.updateFormState(this.props.form, tempObj);
	}

	cancel() {
		this.props.history.goBack();
	}

	openViewMoreModal(target){
		let getBody = (closeModal) => {
			return (
				<OpenViewMoreDetails />
			)
		}
		this.props.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} app={this.props.app} target={target} updateFormState={this.props.updateFormState} openModal={this.props.openModal} callback={this.addItem} closeModal={closeModal} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'},confirmModal: true});
	}

	renderTarget() {
		let {targets} = this.state;

		return Object.keys(targets).map((targetid, index) => {
			let target = targets[targetid];
			return (
				<div key={index} className="form-group col-sm-4 col-md-4" style={{paddingRight: '0px'}}>
					<div className="bg-white float-left" style={{padding: '14px',border: '1px solid #ddd',borderRadius: '0px',width:'100%'}}>
						<div className ="float-left marginbottom-10" style={{width: '100%'}}>
							<div style={{width: '60%',float: 'left',textAlign: 'left',whiteSpace: 'nowrap',textOverflow: 'ellipsis',overflow: 'hidden',fontWeight:'600'}} title={target.name}>
								{target.name}
							</div>
							<div style={{width: '40%',float: 'right',textAlign: 'right'}}>
								{this.targetperiod(target)}
							</div>
						</div>
						<div className ="float-left d-flex flex-row align-items-baseline" style={{width: '100%'}}>
							<div style={{width: '60%',float: 'left',textAlign: 'left',fontSize: '20px'}}>
								<b>{currencyFilter(target.showtargetcompleted, 'qty', this.props.app)}</b>
							</div>
							<div style={{width: '40%',float: 'right',textAlign: 'right'}}>
								{currencyFilter(target.showtargetvalue, 'qty', this.props.app)}
							</div>
						</div>
						<div className ="float-left marginbottom-10" style={{width: '100%'}}>
							<div style={{width: '50%',float: 'left',textAlign: 'left',fontSize: '10px'}}>
								Achieved
							</div>
							<div style={{width: '50%',float: 'right',textAlign: 'right',fontSize: '10px'}}>
								Target
							</div>
						</div>
						<div className ="float-left" style={{width: '100%'}}>
							<div className="progress targetsummaryprogresscss" style={{height:'10px'}}>
								<div className="progress-bar" role="progressbar" style={{width:`${target.showtargetpercentagecompleted}%`}}></div>
							</div>
							<div className="float-left" style={{fontSize: '12px',fontWeight: '600', marginTop: '7px', marginLeft: '5px'}}>{target.showtargetpercentagecompleted}% Achieved</div>
						</div>
						<div className ="float-left" style={{width: '100%'}}>
							<hr className="removemargin"></hr>
							<button type="button" style={{float : 'right',fontSize:'12px'}} className="btn btn-sm gs-btn-outline-primary" onClick={()=>{this.openViewMoreModal(target)}}>VIEW MORE</button>
						</div>
					</div>
				</div>
			);
		});
	}

	render() {
		let { targets } = this.state;

		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">My Performance Targets</div>
						</div>
						<div className="col-md-12 paddingright-5">
							
							 <div className="row">
								<div className="col-sm-12 col-md-12 col-lg-12" style={{paddingLeft:'50px'}}>
									<div className="card-body">
										<div className="row responsive-form-element" style={{marginTop: '10px'}}>
											{false ? <div className="form-group col-sm-4 col-md-4 col-lg-4">
													<label className="labelclass">User</label>
													<Field
														name = {`userid`}
														props = {{
															resource: 'users',
															fields: 'id,displayname',
															label: 'displayname',
															required : true,
															disabled: !this.props.app.user.roleid.includes(1)
														}}
														component = {autoSelectEle}
														validate = {[numberNewValidation({
															required: true
														})]}
													/>
												</div> : null}
												<div className="form-group col-sm-12 col-md-6 col-lg-4">
													<div className="row d-flex flex-row align-items-center">
														<div className="col-sm-2 col-md-2 col-lg-2 paddingright-lg-0">
															<span>Year</span>
														</div>
														<div className="col-sm-10 col-md-10 col-lg-10 paddingleft-lg-0">
															<Field name={'year'} props={{required : true,onChange : ()=>this.dateOnChange()}} component={NumberEle} validate={[numberNewValidation({required:true, model: "Year"})]}/>
														</div>
													</div>
												</div>
												<div className="form-group col-sm-12 col-md-6 col-lg-4">
													<div className="row d-flex flex-row align-items-center">
														<div className="col-sm-2 col-md-2 col-lg-2 paddingright-lg-0">
															<span>Month</span>
														</div>
														<div className="col-sm-10 col-md-10 col-lg-10 paddingleft-lg-0">
															<Field
																name={'month'}
																props={{
																	options: [{ value : 0, label : "January" }, { value : 1, label : "February" }, { value : 2, label : "March" }, { value : 3, label : "April" }, { value : 4, label : "May" }, { value : 5, label : "June" }, { value : 6, label : "July" }, { value : 7, label : "August" }, { value : 8, label : "September" }, { value : 9, label : "October" }, { value : 10, label : "November" }, { value : 11, label : "December" }],
																	label: "label",
																	valuename:"value",
																	required: true,
																	onChange : ()=>this.dateOnChange()
																}}
																component={localSelectEle}
																validate={[numberNewValidation({required:true, model: "Month"})]}/>
														</div>
													</div>
												</div>
										</div>
										<ReportPlaceholderComponent reportdata={this.state.targets} />
										{
											(targets) ? <div className="row responsive-form-element">
												{(Object.keys(targets).length > 0) ? this.renderTarget() : null}
											</div> : null
										}
										
									</div>
								</div>
							</div> 
						</div>
					</div>
				</form>
			</>
		);
	}
}

class OpenViewMoreDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currenttarget : {},
			cumulativetarget : {} ,
			overalltarget : {},
			loading : false
		};

		this.summaryData = this.summaryData.bind(this);
		this.renderTargetItems = this.renderTargetItems.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
	}

	exportExcel(){
		let coldefArray = [{
			"name" : "Period",
			"key" : "targetperiod",
			"cellClass" : "text-center",
			"width" : 150
		}, {
			"name" : "Current Target",
			"key" : "targetvalue",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Current Achieved",
			"key" : "targetcompleted",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Current Achieved%",
			"key" : "percentagecompleted",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Cumulative Target",
			"key" : "cummulativetarget",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Cumulative Achieved",
			"key" : "cummulativetargetcompleted",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Cumulative Achieved%",
			"key" : "cummulativepercentagecompleted",
			"cellClass" : "text-center",
			"width" : 150
		}];
		
		generateExcel(this.props.target.name, coldefArray,this.props.target.performancetargetitems, this.props.app, () => {});
	}

	summaryData(data, type) {
		return (
			<div className="col-md-4 col-sm-4">
				<div className="bg-white float-left" style={{padding: '12px', border: '1px solid #ddd', width:'100%'}}>
					<div className ="float-left marginbottom-10" style={{width: '100%'}}>
						<div style={{width: '60%',float: 'left',textAlign: 'left'}}>
							{type}
						</div>
					</div>
					<div className ="float-left d-flex flex-row align-items-baseline" style={{width: '100%'}}>
						<div style={{width: '60%',float: 'left',textAlign: 'left',fontSize: '20px'}}>
							<b>{currencyFilter(data.targetcompleted, 'qty', this.props.app)}</b>
						</div>
						<div style={{width: '40%',float: 'right',textAlign: 'right'}}>
							{currencyFilter(data.targetvalue, 'qty', this.props.app)}
						</div>
					</div>
					<div className ="float-left marginbottom-10" style={{width: '100%'}}>
						<div style={{width: '50%',float: 'left',textAlign: 'left',fontSize: '10px'}}>
							Achieved
						</div>
						<div style={{width: '50%',float: 'right',textAlign: 'right',fontSize: '10px'}}>
							Target
						</div>
					</div>
					<div className ="float-left" style={{width: '100%',position: 'relative'}}>
						<div className="progress targetsummaryprogresscss" style={{height:'10px'}}>
							<div className="progress-bar" role="progressbar" style={{width:`${data.percentagecompleted}%`}}></div>
						</div>
						{
							(data.cumulativetarget && data.cumulativetarget>0) ? 
								<div className="float-left progressbar-indicator" style={{width:`${data.cumulativetarget}%`}}></div>
							 : null
						}
						<div className="float-left" style={{fontSize: '12px',fontWeight: '600', marginTop: '7px', marginLeft: '5px'}}>{data.percentagecompleted}% Achieved</div>
					</div>
				</div>
			</div>
		);
	}

	renderTargetItems() {
		return this.props.target.performancetargetitems.map((data, index) => {
			return (
				<tr key={index}>
					<td className="text-center" style={{color: '#7e7e7e'}}>{data.targetperiod}</td>
					<td className="text-center" style={{fontWeight:'600'}}>{currencyFilter(data.targetvalue, 'qty', this.props.app)}</td>
					<td className="text-center" style={{fontWeight:'600'}}>{currencyFilter(data.targetcompleted, 'qty', this.props.app)}</td>
					<td>
						<div className ="float-left pl-2 pr-2" style={{width: '100%'}}>
							<div className="progress targetdetailprogresscss margintop-5 marginbottom-5" style={{height:'4px'}}>
								<div className="progress-bar" role="progressbar" style={{width:`${data.percentagecompleted}%`}}></div>
							</div>
							<div className="float-left" style={{fontSize: '10px',fontWeight: '600'}}>{data.percentagecompleted}% Achieved</div>
						</div>
					</td>
					<td className="text-center" style={{fontWeight:'600'}}>{currencyFilter(data.cummulativetarget, 'qty', this.props.app)}</td>
					<td className="text-center" style={{fontWeight:'600'}}>{currencyFilter(data.cummulativetargetcompleted, 'qty', this.props.app)}</td>
					<td>
						<div className ="float-left pl-2 pr-2" style={{width: '100%'}}>
							<div className="progress targetdetailprogresscss margintop-5 marginbottom-5" style={{height:'4px'}}>
								<div className="progress-bar" role="progressbar" style={{width:`${data.cummulativepercentagecompleted}%`}}></div>
							</div>
							<div className="float-left" style={{fontSize: '10px',fontWeight: '600'}}>{data.cummulativepercentagecompleted}% Achieved</div>
						</div>
					</td>
				</tr>
			);
		});
	}

	render() {
		let currentItemIndex = this.props.target.performancetargetitems.length - 1;

		this.props.target.performancetargetitems.forEach((target, targetindex) => {
			if(target.currentPeriod)
				currentItemIndex = targetindex;
		});

		let currentItem = this.props.target.performancetargetitems[currentItemIndex];
		let cummulativeItem = {
			targetvalue: currentItem.cummulativetarget,
			targetcompleted: currentItem.cummulativetargetcompleted,
			percentagecompleted: currentItem.cummulativepercentagecompleted
		};
		let overallItem = {
			targetvalue: this.props.target.overalltarget,
			targetcompleted: this.props.target.targetcompleted,
			percentagecompleted: this.props.target.percentagecompleted,
			cumulativetarget : Number(((currentItem.cummulativetarget*100)/this.props.target.overalltarget).toFixed(this.props.app.roundOffPrecision))
		};

    	return (
			<div className="react-outer-modal">
				<div className="react-modal-header marginbottom-20">
					<div className="w-100 d-inline-block">
						<div className="float-left" style={{paddingTop : '4px',paddingLeft: '3px'}}>
							<h6 className="modal-title" style ={{color :'#1cbc9c'}}><b>{this.props.target.name}</b></h6>
						</div>
						<div className ="float-right">
							<button type="button" className="btn btn-sm gs-form-btn-primary" onClick={() => this.exportExcel()}><span className="fa fa-file-excel-o"/> Export Excel</button>
						</div>
					</div>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						{this.summaryData(currentItem, 'CURRENT')}
						{this.summaryData(cummulativeItem, 'CUMULATIVE')}
						{this.summaryData(overallItem, 'OVERALL')}
					</div>
					<div className="row responsive-form-element margintop-20">
						<div className="col-md-12 col-sm-12">
							<div className="table-responsive">
								<table className="table table-bordered table-sm gs-item-table tablefixed">
									<thead>
										<tr>
											<th className="text-center font-weight-normal">PERIOD</th>
											<th className="text-center font-weight-normal" colSpan="3">CURRENT</th>
											<th className="text-center font-weight-normal" colSpan="3">CUMULATIVE</th>
										</tr>
										<tr style={{backgroundColor: '#e8f1f9'}}>
											<th className="text-center font-weight-normal" style={{borderRight: 'none'}}></th>
											<th className="text-center font-weight-normal" style={{border: 'none'}}>Target</th>
											<th className="text-center font-weight-normal" style={{border: 'none'}}>Achieved</th>
											<th className="text-center font-weight-normal" style={{border: 'none'}}>Achieved%</th>
											<th className="text-center font-weight-normal" style={{border: 'none'}}>Target</th>
											<th className="text-center font-weight-normal" style={{border: 'none'}}>Achieved</th>
											<th className="text-center font-weight-normal" style={{borderLeft: 'none'}}>Achieved%</th>
										</tr>
									</thead>
									<tbody>
										{this.renderTargetItems()}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
 				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 text-center">
							<div className="muted credit text-center">
								<button className="btn btn-sm btn-secondary btn-width" onClick={() => this.props.closeModal()}>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

PerformanceTargetDashBoard = connect(
	(state, props) => {
		let formName = `Performance Target Dashboard`;
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(PerformanceTargetDashBoard));

export default PerformanceTargetDashBoard;