import React, { Component } from 'react';
import { Field } from 'redux-form';

import { RateInline, StockInline, checkboxEle } from '../components/formelements';
import { checkActionVerbAccess } from '../utils/utils';
import { Link } from "react-router-dom";

class TableCell extends Component {
	constructor(props) {
		super(props);

		this.tableCellRef = React.createRef();
	}

	getSnapshotBeforeUpdate(prevProps, prevState) {
		if (!this.tableCellRef) {
			return null;
		}

		const isDragStarting = this.props.isDragOccurring && !prevProps.isDragOccurring ? true : false;

		if (!isDragStarting) {
			return null;
		}

		const { width, height } = this.tableCellRef.current.getBoundingClientRect();
		const snapshot = { width, height};
		return snapshot;
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		let tableCellRef =  this.tableCellRef.current;
		if (snapshot !== null) {
			if (tableCellRef.style.width === snapshot.width) {
				return;
			}
			tableCellRef.style.width = `${snapshot.width}px`;
			tableCellRef.style.height = `${snapshot.height}px`;
			return;
		}

		if (this.props.isDragOccurring || tableCellRef.style.width == null) {
			return;
		}

		tableCellRef.style.removeProperty('height');
		tableCellRef.style.removeProperty('width');
	}

	render() {
		return (
			<td style={this.props.style} className={this.props.className} colSpan={this.props.colSpan} ref={this.tableCellRef} onClick={this.props.onClick}>{this.props.children}</td>
		)
	}
}

class TableRow extends Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.renderTableBody = this.renderTableBody.bind(this);
	}	

	getItemBodyField(member) {
		var key = 0;
		return this.props.json.body.map((field)=> {
			key++;
			return this.props.getFieldString(key, field, member, this.props.parentitemstr);
		});
	}

	renderTableBody(member, index) {
		let key=1;
		var columnArray = [];
		let item = this.props.checkCondition('value', `{resource.${member}}`);
		let addRow = this.props.checkCondition('if', this.props.json.addrow);
		let showdeletebtn = this.props.checkCondition('if', this.props.json.deletebtn, item);

		let columnShowArray = this.props.json.headers.map((a, headerindex)=> {
			if(a.type == 'button')
				return true;
			return this.props.checkCondition('if', a.if);
		});

		if(this.props.json.body && this.props.json.body.length && this.props.editObj[index] == true) {
			let headerLength = columnShowArray.filter(show => show).length;
			
			columnArray.push(<TableCell colSpan={`${this.props.json.withitemtype == true ? headerLength+1 : headerLength+2}`} key="0" style={{backgroundColor:'#F0F8FF'}} isDragOccurring={this.props.snapshot.isDragging}>
				<div className="row">
					<div className="form-group col-md-12 paddingbottom-5" style={{borderBottom: '1px solid #dee2e6'}}>
						Row - {index+1}
						<div className="float-right">
						{showdeletebtn ? <button type="button" className="btn btn-sm gs-form-btn-danger hide-item-btn marginright-3" onClick={()=>{this.props.deleteRow(index, this.props.parentitemstr)}}><span className="fa fa-trash-o"></span></button> : null}
						{addRow ? <button type="button" className="btn btn-sm gs-form-btn-primary btnhide marginright-3" onClick={()=>this.props.addNewAboveRow(index)}><span className="fa fa-plus"></span></button> : null }
						{this.props.json.withitemtype == true && item.itemtype == 'Item' && item.id ? <button type="button" className="btn btn-sm gs-form-btn-primary marginright-3" onClick={()=>this.props.estimationDetailedfn(this.props.index)} title="View Estimation"><span className="fa fa-calculator"></span></button> : null}
						{this.props.json.showestimationaddbtn && (item.estimationtype == 'Detailed' || item.isoverhead) ? <button type="button" className="btn btn-sm gs-form-btn-success btnhide marginright-3" onClick={()=>this.props.addEstimationDetailedItemfn(this.props.index)} title="Add Detailed Estimation Item"><span className="fa fa-plus"></span></button> : null}
						<button type="button" className="btn btn-sm gs-form-btn-secondary marginright-3" onClick={()=>{this.props.openRow()}}><span className="fa fa-chevron-up"></span></button>
						</div>
					</div>
				</div>
				<div className="row">
					{this.getItemBodyField.bind(this)(member)}
				</div>
			</TableCell>);
		} else {
			let showStockButton = checkActionVerbAccess(this.props.app, 'getstockquery', 'Read') ? true : false;
			let showAnalyseRateButton = checkActionVerbAccess(this.props.app, this.props.resource.resourceName == 'purchaseorders' ? 'analyseitemquerypurchase' : 'analyseitemquery', 'Read') ? true : false;

			columnArray = this.props.json.headers.map((a, headerindex)=> {
				let tempTD = '', tdClassName= '', tdColspan = '';
				let tempOnClick = () => {};
				
				if(!columnShowArray[headerindex])
					return null;
				
				if(a.type == 'text' && ['rate', 'stock'].indexOf(a.element) == -1)
					tempOnClick = () => this.props.openRow(index);
				if(a.type == 'text')
					tempTD = this.props.checkCondition('value', a.format, item, member);
				if(a.type == 'link') {
					let tempTDFormat = this.props.checkCondition('value', a.format, item, member);
					tempTD = <Link to={tempTDFormat.url}>{tempTDFormat.text}</Link>
				}
				if(a.type == 'element' && a.element == 'rate')
					tempTD = showAnalyseRateButton ? <RateInline btnclick={this.props.checkCondition('value', "{()=>{controller.analyseitemRate(item)}}", item, member)}></RateInline> : null;
				if(a.type == 'element' && a.element == 'stock')
					tempTD = (showStockButton && (item.itemid_keepstock || item.itemid_issaleskit || item.itemid_hasaddons)) ? <StockInline btnclick={this.props.checkCondition('value', "{()=>{controller.openStockDetails(item)}}", item, member)}></StockInline> : null;

				if(a.type == 'element' && a.element == 'checkbox')
					tempTD = <Field name={`${member}.${a.model}`} props={{}} component={checkboxEle} />

				if(a.type == 'button') {
					let buttonClick = a.click ? this.props.checkCondition('value', a.click, item, member, index) : () => {};
					let accessClass = '';
					if(a.access && a.actionverb) {
						accessClass = checkActionVerbAccess(this.props.app, a.access, a.actionverb) ? '' : 'hide';
					}

					let buttonclass = this.props.checkCondition('value', a.buttonclass, item);
					let showHideClass = `${this.props.checkCondition('hide', a.hide, item)} ${this.props.checkCondition('show', a.show, item)} ${accessClass}`;
					let buttondisabled = this.props.checkCondition('disabled', a.buttondisabled, item);

					if(this.props.checkCondition('if', a.if, item) && accessClass == '')
						tempTD = <button type="button" className={`btn btn-sm ${buttonclass} ${showHideClass}`} onClick={buttonClick} disabled={buttondisabled}><span className={`fa ${a.icon}`}></span>{a.buttonlabel}</button>
				}

				if(this.props.json.withitemtype == true) {
					if(headerindex > 1) {
						tdClassName += item.itemtype == 'Section' ? 'hide ' : '';
					} else {
						tdColspan = (item.itemtype == 'Section' && headerindex == 1)  ? (this.props.json.headers.length - headerindex) : 1
					}
				}

				if(a.label == 'Amount')
					tdClassName += ' text-right';
				key++;

				return (
					<TableCell key={key} onClick={this.props.json.body && this.props.json.body.length ? tempOnClick : () => {}} className={`${tdClassName}`} colSpan={tdColspan ? tdColspan : ''} isDragOccurring={this.props.snapshot.isDragging}>
						<div className={`${typeof(tempTD) == 'string' ? 'text-ellipsis' : ''}`} title={`${typeof(tempTD) == 'number' || typeof(tempTD) == 'string' ? tempTD : ''}`}>
							{tempTD}
						</div>
					</TableCell>
				);
			});
		}
		return columnArray;
	}

	render() {
		let item = this.props.checkCondition('value', `{resource.${this.props.member}}`);
		let addRow = this.props.checkCondition('if', this.props.json.addrow);
		let showdeletebtn = this.props.checkCondition('if', this.props.json.deletebtn, item);
		const { snapshot, provided } = this.props;
		
		if(!snapshot || !provided)
			return null;

		let trRow = (<tr className={`${snapshot.isDragging ? 'dragndrop_shadow' : ''} gs-child-table-row`} key={this.props.index} ref={provided.innerRef} isdragging={snapshot.isDragging.toString()} {...provided.draggableProps} {...provided.dragHandleProps}>
				{this.props.editObj[this.props.index] != true && !this.props.json.withitemtype && this.props.resourcename != 'projectestimations' ? <TableCell style={{whiteSpace: 'nowrap'}} isDragOccurring={this.props.snapshot.isDragging}>{this.props.index+1}</TableCell> : null }
				{this.renderTableBody(this.props.member, this.props.index)}
				{this.props.editObj[this.props.index] != true ? <TableCell isDragOccurring={this.props.snapshot.isDragging} style={{whiteSpace: 'nowrap'}}>
					{addRow && !this.props.json.withitemtype ? <button type="button" className="btn btn-sm gs-form-btn-primary btnhide marginright-3" onClick={()=>this.props.addNewAboveRow(this.props.index)}><span className="fa fa-plus"></span></button> : null }
					{this.props.json.withitemtype == true && item.itemtype == 'Item' && item.id ? <button type="button" className="btn btn-sm gs-form-btn-primary marginright-3" onClick={()=>this.props.estimationDetailedfn(this.props.index)} title="View Estimation"><span className="fa fa-calculator"></span></button> : null}
					{this.props.json.showestimationaddbtn && (item.estimationtype == 'Detailed' || item.isoverhead) ? <button type="button" className="btn btn-sm gs-form-btn-success btnhide marginright-3" onClick={()=>this.props.addEstimationDetailedItemfn(this.props.index)} title="Add Detailed Estimation Item"><span className="fa fa-plus"></span></button> : null}
					{showdeletebtn ? <button type="button" className="btn btn-sm gs-form-btn-danger hide-item-btn marginleft-3" onClick={()=>{this.props.deleteRow(this.props.index, this.props.parentitemstr)}}><span className="fa fa-trash-o"></span></button> : null}
				</TableCell> : null }
			</tr>);
		if(this.props.editObj[this.props.index])
			return trRow;

		return trRow;
	}
}

export default TableRow;
