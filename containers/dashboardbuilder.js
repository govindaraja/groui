import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { updateFormState, updateReportFilter } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { DateEle, localSelectEle, autoSelectEle, selectAsyncEle, checkboxEle, textareaEle, InputEle, NumberEle, SpanEle } from '../components/formelements';
import { customfieldAssign, numberNewValidation, dateNewValidation, stringNewValidation, checkActionVerbAccess, multiSelectNewValidation, requiredNewValidation } from '../utils/utils';
import { dateFilter, datetimeFilter, timeFilter} from '../utils/filter';
import { ChildEditModal, AlertView, AutoSelect, AccountBalance, LocalSelect, NumberElement } from '../components/utilcomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import { Prompt } from 'react-router-dom';
import { AdditionalInformationSection } from '../containers/ndtsection1';
import DashboardComponent from '../components/dashboardcomponent';
import ReactGridLayout from "react-grid-layout";

class DashboardBuilder extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag: true,
			showDashboardWidget: false,
			createParam: this.props.match.params.id > 0 ? false : true,
			rolesArray: []
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.initialize = this.initialize.bind(this);
		this.getItemById = this.getItemById.bind(this);
		this.getRoles = this.getRoles.bind(this);
		this.renderDashBoard = this.renderDashBoard.bind(this);
		this.renderDashBoardWidget = this.renderDashBoardWidget.bind(this);
		this.renderLayout = this.renderLayout.bind(this);
		this.save = this.save.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);

		if (this.props.match.path == '/createDashBoard')
			this.initialize();
		else
			this.getItemById();
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag)
			this.updateLoaderFlag(false);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	initialize() {
		this.props.initialize({
			dashboarditems: []
		});

		setTimeout(this.getRoles, 0);

		this.updateLoaderFlag(false);
	}

	getItemById() {
		axios.get(`/api/dashboard/${this.props.match.params.id}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.initialize(response.data.main);

				document.getElementById('pagetitle').innerHTML = `Dashboard Builder - ${response.data.main.name}`;

				this.setState({
					ndt: {
						notes: response.data.notes,
						documents: response.data.documents,
						tasks: response.data.tasks
					}
				});

				this.getRoles();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	getRoles() {
		let rolesArray = [];

		axios.get(`/api/roles?&field=id,name&filtercondition=`).then((response) => {
			if (response.data.message == 'success') {
				rolesArray = response.data.main;

				rolesArray.sort(
					(a, b) => {
						return (a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0;
					}
				);
			}

			this.setState({
				rolesArray
			});
		});
	}

	save(param, confirm) {
		this.updateLoaderFlag(true);

		axios({
			method: 'post',
			data: {
				actionverb: param,
				data: this.props.resource,
				ignoreExceptions: confirm ? true : false
			},
			url: '/api/dashboard'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.save(param, true);
			}));

			if (response.data.message == 'success') {
				let resource = {
					...response.data.main
				};

				if (this.props.match.path == '/createDashBoard')
					this.props.history.replace(`/details/dashboardbuilder/${response.data.main.id}`);
				else if (param == 'Delete')
					this.props.history.replace('/list/dashboardbuilder');
				else
					this.props.initialize(resource);

				/*let appWidgetArray = [],
					widgetFound = false;

				this.props.app.widgetArray.forEach((item) => {
					if (item.id == resource.id) {
						widgetFound = true;

						if (resource.isactive)
							appWidgetArray.push(item);
					} else
						appWidgetArray.push(item);
				});

				if (!widgetFound && resource.isactive)
					appWidgetArray.push({
						id: resource.id,
						name: resource.name,
						displayorder: resource.displayorder,
						allowedroles: resource.allowedroles,
						remarks: resource.remarks
					})

				this.props.updateAppState('widgetArray', appWidgetArray);*/
			}

			this.updateLoaderFlag(false);
		});
	}

	renderDashBoard() {
		const { app, resource} = this.props;
		let { createParam, rolesArray } = this.state;

		if (!resource)
			return null;

		return (
			<>
				<div className = 'row'>
					<div className = 'col-sm-12 col-md-12 col-lg-9' style = {{position: 'fixed', height: '45px', backgroundColor: '#FAFAFA',  zIndex: 5}}>
						<div className = 'row'>
							<div className = 'col-md-6 col-sm-12 paddingleft-md-30'>
								<h6 className = 'margintop-15 semi-bold-custom'>
									<a className = 'affixanchor float-left marginright-10' onClick={()=>{this.props.history.goBack()}}>
										<span className = 'fa fa-chevron-left'></span>
									</a> {!resource.id ? `New Dashboard` : `Dashboard: ${resource.name}`}
								</h6>
							</div>
						</div>
					</div>
				</div>
				<form>
					<div className = 'row'>
						<div className = 'col-sm-12 col-md-12 col-lg-9'>
							<div className = 'row' style={{marginTop:'50px'}}>
								<div className = 'col-md-12'>
									<div className = 'card' style={{marginBottom:'10px'}}>
										<div className = 'card-header listpage-title'>
											<div className = 'row'>
												<div className = 'col-md-6 gs-uppercase'>
													Dashboard Details
												</div>
											</div>
										</div>
										<div className = 'card-body'>
											<div className = {`row`}>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Name</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'name'}
																props = {{
																	required: true
																}}
																component = {InputEle}
																validate = {[stringNewValidation({
																	required: true,
																	title : 'Name'
																})]}
															/>
														</div>
													</div>
												</div>

												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Roles</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'allowedroles'}
																props = {{
																	multiselect: true,
																	options: [...rolesArray],
																	required: true
																}}
																component = {localSelectEle}
																validate = {[multiSelectNewValidation({
																	required: true,
																	title : 'Allowed Roles'
																})]}
															/>
														</div>
													</div>
												</div>

												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Remarks</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'remarks'}
																component = {textareaEle}
															/>
														</div>
													</div>
												</div>

												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Display Order</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'displayorder'}
																props = {{
																	required: true
																}}
																component = {NumberEle}
																validate = {[numberNewValidation({
																	required: true
																})]}
															/>
														</div>
													</div>
												</div>

												{ resource.id ? <div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Active</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'isactive'}
																component = {checkboxEle}
															/>
														</div>
													</div>
												</div> : null}
											</div>
										</div>
									</div>
								</div>

								<div className = 'col-md-12'>
									<div className = 'card' style={{marginBottom:'3rem'}}>
										<div className = 'card-header listpage-title gs-uppercase'>
											<div className = 'row'>
												<div className = 'col-md-9 margintop-5'>
													Dashboard layout
													<label style={{color: '#9B9B9B', textTransform: 'none', fontSize: '12px', marginLeft: '10px'}}>{ resource.dashboarditems && resource.dashboarditems.length == 0 ? 'You can only view layout after customising it.' : 'You can only view dashboard layout here. Please open the editor to customise the layout.'}</label>
												</div>
												{resource.dashboarditems && resource.dashboarditems.length > 0 ? <div className = 'col-md-3 text-right-md'>
													<button
														type = 'button'
														className = 'btn gs-form-btn-primary btn-sm btn-width marginleft-5'
														disabled = {this.props.invalid}
														onClick = {() => this.setState({
															showDashboardWidget: true
														})}>
														<i className = 'fa fa-external-link'></i>Open Editor
													</button>
												</div> : null}
											</div>
										</div>
										<div className = 'card-body'>
											{ resource.dashboarditems && resource.dashboarditems.length == 0 ? <div className='row responsive-form-element'>
												<div style = {{width: '100%', height: '300px'}}>
													<div style = {{
														textAlign: 'center',
														verticalAlign: 'middle',
														lineHeight: '40px',
														marginTop: '90px'
													}}>
														<button
															type = 'button'
															className = 'btn gs-form-btn-primary btn-sm btn-width marginleft-5'
															disabled = {this.props.invalid}
															onClick = {() => this.setState({
																showDashboardWidget: true
															})}>
															<i className = 'fa fa-external-link'></i>Open Editor
														</button>
														<p style={{color: '#9B9B9B', textTransform: 'none', fontSize: '12px'}}>
															Please open editor to customise & assemble dashboard widgets.
														</p>
													</div>
												</div>
											</div> : this.renderLayout()}
										</div>
									</div>
								</div>

							</div>
						</div>
						<div className={`col-sm-12 ndt-section col-md-12 col-lg-3 paddingleft-md-0 margintop-5 `} >
							{resource && resource.id && this.state.ndt ? <AdditionalInformationSection
								ndt = {this.state.ndt}
								key = {0}
								parentresource = {'dashboard'}
								openModal = {this.props.openModal}
								parentid = {resource.id}
								relatedpath = {this.props.match.path}
								createOrEdit = {this.props.createOrEdit} 
								app ={this.props.app} 
								history={this.props.history}
							/> : null }
						</div>
					</div>
					<div className = 'row'>
						<div className = 'col-sm-12 col-md-12 col-lg-9'>
							<div className = 'muted credit text-center sticky-footer actionbtn-bar' style={{boxShadow:'none'}}>
								<div style = {{width: '80%', float: 'left'}}>
									<button
										type = 'button'
										className = 'btn btn-sm btn-width btn-secondary'
										onClick={() => this.props.history.goBack()}>
										<i className = 'fa fa-times marginright-5'></i>Close
									</button>
									{checkActionVerbAccess(this.props.app, 'dashboard', 'Save') ? <button
										type = 'button'
										className = 'btn btn-width btn-sm gs-btn-success'
										disabled = {this.props.invalid}
										onClick = {()=>{this.save('Save');}}>
										<i className = 'fa fa-save'></i>Save
									</button> : null}
								</div>

								{ resource.id && checkActionVerbAccess(this.props.app, 'dashboard', 'Delete') && (resource.createdby == this.props.app.user.id || this.props.app.user.roleid.indexOf(1) >= 0) ? <div className = 'btn-group dropup' style={{float:'left', width: '20%'}}>
									<button
										type = 'button'
										className = 'btn btn-sm btn-width gs-btn-info dropdown-toggle'
										data-toggle = 'dropdown'
										aria-haspopup = 'true'
										aria-expanded = 'false'>More <span className = 'sr-only'>Toggle Dropdown</span>
									</button>
									<div className = 'dropdown-menu gs-morebtn-dropdown-menu'>
										<div className = {`dropdown-item cursor-ptr gs-text-danger`}
											onClick = {() => this.save('Delete')}>
											<i className = {`fa fa-trash-o marginright-5`}></i> Delete
										</div>
									</div>
								</div> : null}
							</div>
 						</div>
					</div>
				</form>
		   </>
		);
	}

	renderDashBoardWidget() {
		return <DashboardWidgetView
			resource = {this.props.resource}
			form = {this.props.form}
			history = {this.props.history}
			app = {this.props.app}
			updateFormState = {this.props.updateFormState}
			rolesArray = {this.state.rolesArray}
			openModal = {this.props.openModal}
			callback = {(value) => {
				this.setState({
					showDashboardWidget: value
				})
			}}
		/>;
	}

	renderLayout () {
		let { resource } = this.props,
			layoutItems = [];

		resource.dashboarditems.forEach((item) => {
			layoutItems.push({
				reportid: item.reportid,
				reportid_name: item.reportid_name,
				reportid_analyticstype: item.reportid_config.analyticstype,
				i: item.reportid.toString(),
				x: item.gridposition[0],
				y: item.gridposition[1],
				w: item.gridposition[2],
				h: item.gridposition[3],
				static: true
			})
		});

		const chartTextStyle = {
			cursor: 'default',
			position: 'absolute',
			bottom: '10px',
			right: '10px',
			color: '#BBBBBB',
			border: '1px solid #BBBBBB',
			padding: '2px',
			borderRadius: '2px',
			fontSize: '11px'
		};

		return (
			<ReactGridLayout
				className = "layout"
				cols = {4}
				rowHeight = {Math.round($(window).height() / 6) > 60 ? Math.round($(window).height() / 6) : 60}
				width = {document.getElementsByClassName('layout').length > 0 ? document.getElementsByClassName('layout')[0].offsetWidth : 800}
				layouts = {{x:0, y: 0, w: 1, h: 1}}
			>
				{layoutItems.map((el) => {
					return (
						<div key={el.i} data-grid={el}>
							<div className = 'marginleft-10 margintop-10'>
								<span className = 'text'>{el.reportid_name}</span>
							</div>
							<div style = {chartTextStyle}>
								<span>{el.reportid_analyticstype}</span>
							</div>
						</div>
					);
				})}
			</ReactGridLayout>
		)
	}

	render() {
		const { app, resource} = this.props;
		let { createParam, rolesArray, showDashboardWidget } = this.state;

		if (!resource)
			return null;

		return (
			<>
				<Loadingcontainer isloading = {this.state.loaderflag}></Loadingcontainer>
				<Prompt when={!createParam && this.props.dirty && this.props.anyTouched} message={`Unsaved Changes`} />

				{showDashboardWidget ? this.renderDashBoardWidget() : this.renderDashBoard()}
		   </>
		);
	}
}

class DashboardWidgetView extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag: false,
			widgetArray: [],
			layoutItems: [],
			layout: []
		};

		this.onLoad = this.onLoad.bind(this);
		this.onRemoveItem = this.onRemoveItem.bind(this);
		this.createLayout = this.createLayout.bind(this);
		this.onLayoutChange = this.onLayoutChange.bind(this);
		this.addWidgetModal = this.addWidgetModal.bind(this);
		this.addDashboardItems = this.addDashboardItems.bind(this);
		this.checkArray = this.checkArray.bind(this);
		this.checkGridValid = this.checkGridValid.bind(this);
		this.onSave = this.onSave.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);

		let { layoutItems } = this.state,
			{ resource, app } = this.props;

		layoutItems = [];

		if (resource.dashboarditems.length > 0)
			resource.dashboarditems.forEach((item) => {
				layoutItems.push({
					...item,
					i: item.reportid.toString(),
					x: item.gridposition[0],
					y: item.gridposition[1],
					w: item.gridposition[2],
					h: item.gridposition[3],
					minW: 1,
					maxW: item.reportid_config.analyticstype == 'KPI' ? 1 : 4,
					minH: item.reportid_config.analyticstype == 'KPI' ? 1 : 2,
					maxH: item.reportid_config.analyticstype == 'KPI' ? 1 : 6,
					isAccess: this.checkArray(app.user.roleid, item.reportid_allowedroles)
				});
			});

		this.onLoad(layoutItems);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	onLoad(layoutItems) {
		let { widgetArray } = this.state;

		widgetArray = [];

		axios.get(`/api/reports?&field=id,name,type,datasetid,module,foldername,allowedroles,hasexcelexport,config,displayorder,remarks&filtercondition=reports.type='Analytics'`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.length > 0)
					widgetArray = JSON.parse(JSON.stringify(response.data.main));

				this.setState({
					widgetArray,
					layoutItems
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	addWidgetModal () {
		let { widgetArray, layoutItems } = this.state;

		this.props.openModal({
			render: (closeModal) => {
				return <ChildEditModal
					openModal = {this.props.openModal}
					form = {this.props.form}
					resource = {this.props.resource}
					history = {this.props.history}
					app = {this.props.app}
					widgetArray = {widgetArray}
					layoutItems = {layoutItems}
					updateFormState = {this.props.updateFormState}
					callback = {(items) => this.addDashboardItems(items)}
					closeModal = {closeModal}
					getBody = {() => {return <WidgetSelectModal />}} />
			}, className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			},
			confirmModal: true
		});
	}

	addDashboardItems(item) {
		let { layoutItems } = this.state,
			{ resource, app } = this.props;

		layoutItems.push({
			reportid: item.id,
			reportid_name: item.name,
			reportid_config: item.config,
			reportid_allowedroles: item.allowedroles,
			i: item.id.toString(),
			x: layoutItems.length % 4,
			y: Infinity, // puts it at the bottom
			w: item.config.analyticstype == 'KPI' ? 1 : 1,
			h: item.config.analyticstype == 'KPI' ? 1 : 2,
			minW: 1,
			maxW: item.config.analyticstype == 'KPI' ? 1 : 4,
			minH: item.config.analyticstype == 'KPI' ? 1 : 2,
			maxH: item.config.analyticstype == 'KPI' ? 1 : 6,
			isAccess: this.checkArray(app.user.roleid, item.allowedroles)
		});

		this.setState({
			layoutItems
		});
	}

	checkArray(arrayA, arrayB) {
		if (arrayB) {
			for (let i = 0; i < arrayA.length; i++) {
				let itemFound = false;

				for (let j = 0; j < arrayB.length; j++) {
					if (arrayA[i] == arrayB[j]) {
						itemFound = true;
						break;
					}
				}

				if (itemFound)
					return true;
			}
		} else
			return false;

		return false;
	}

	createLayout(el) {
		let isKPI = el.reportid_config.analyticstype == 'KPI' ? true : false;

		return (
			<div key={el.i} data-grid={el} id={`id_${el.reportid}`} className = {`widget-${el.reportid} ${!isKPI ? 'gs-react-grid-item' : ''}`}>
				{el.isAccess ? <div style = {{background: 'rgb(239, 239, 239)', padding: '2px', height: '100%'}}>
					<div className = 'bg-white' style = {{height: '100%', padding: `${isKPI ? '12px 14px 12px 20px' : '18px 14px 12px 20px'}`}}>
					<DashboardComponent
						reportid = {el.reportid}
						resource = {this.props.resource}
						openModal = {this.props.openModal}
						maximizeWidget = {(val, flag) => this.onRemoveItem(val, flag)}
						history={this.props.history}
						isCreatePage = {true}
						isMaximize = {false}
						styleProps = {{
							width: document.getElementById(`id_${el.reportid}`) ? document.getElementById(`id_${el.reportid}`).style.width : '100px',
							height: document.getElementById(`id_${el.reportid}`) ? document.getElementById(`id_${el.reportid}`).style.height : '100px'
						}} />
					</div>
					</div> : <div style = {{background: 'rgb(239, 239, 239)', padding: '2px', height: '100%'}}>
							<div className = 'bg-white' style = {{height: '100%', padding: '16px 14px 12px 20px'}}>
								<div className = 'row no-gutters h-100'>
									<div className = 'col-md-6 overflowtxt gs-dashboard-textColor'>
										{el.reportid_name}
									</div>
									<div className = 'col-md-6 text-right'>
										<span className = 'fa fa-times marginleft-10' style={{color: '#B5B5B5', cursor: 'pointer'}} onClick={() => this.onRemoveItem(el.reportid, false)}></span>
									</div>
									<div className = 'col-md-12 d-flex flex-column align-items-center justify-content-center' style = {{height: '100%'}}>
										<img src = '../images/dashboard_noaccess.png' className = 'list-error-image' style = {{width: '28px'}}/>
										<div className = 'padding-10 font-12 form-group'>You have no access to view this widget</div>
									</div>
								</div>
							</div>
						</div>
					}
			</div>
		);
	}

	onLayoutChange(layout) {
		this.setState({
			layout
		});
	}

	onRemoveItem(val, flag) {
		let { layoutItems } = this.state;

		let layout = [];

		layoutItems.forEach((lay) => {
			if (lay.reportid != val)
				layout.push(lay)
		});

		this.setState({
			layoutItems: layout
		});
	}

	onSave () {
		let { layoutItems, layout } = this.state,
			layoutObj = {},
			dashboarditems = [];

		if (layoutItems && layoutItems.length > 10)
			return this.props.openModal(modalService.infoMethod({
				header: "Warning",
				body: "Selected widgets should not be exceed than 10",
				btnArray: ["Ok"]
			}));

		layout.forEach((item) => {
			layoutObj[Number(item.i)] = Array(item.x, item.y, item.w, item.h);
		});

		layoutItems.forEach((item) => {
			dashboarditems.push({
				...item,
				gridposition: layoutObj[item.reportid]
			});
		});

		this.props.updateFormState(this.props.form, {
			dashboarditems
		});

		this.props.callback(false);
	}

	checkGridValid() {
		const { resource } = this.props;
		let { layout } = this.state;

		let tempMaxRow = 6,
			tempMaxY = 6,
			tempMaxColumn = 4,
			tempMaxX = 4,
			tempArray = [];

		let tempLayout = layout.sort((a, b) => {
			return (Number(`${a.y}${a.x}`) < Number(`${b.y}${b.x}`)) ? -1 : (Number(`${a.y}${a.x}`) > Number(`${b.y}${b.x}`)) ? 1 : 0;
		});

		tempLayout.forEach((item) => {
			tempMaxRow = tempMaxRow < item.h ? item.h : tempMaxRow;
			tempMaxY = tempMaxY < item.y ? item.y : tempMaxY;
			tempMaxColumn = tempMaxColumn < item.w ? item.w : tempMaxColumn;
			tempMaxX = tempMaxX < item.x ? item.x : tempMaxX;

			let itemFound = false;

			if (tempArray.length > 0)
				tempArray.some((child) => {
					if (child.min <= item.x && child.max > item.x) {
						child.max = child.max < item.w ? item.w : child.max;
						child.total += item.h;

						itemFound = true;
						return true;
					} else
						return false;
				});

			if (!itemFound)
				tempArray.push({
					min: item.x,
					max: item.w,
					total: item.h
				});
		});

		let maxRowValue = tempMaxRow > 6 ? tempMaxRow : (tempMaxY > 6 ? tempMaxY : 6);

		let maxColumnValue = tempMaxColumn > 4 ? tempMaxColumn : (tempMaxX > 6 ? tempMaxX : 4);

		let MaxHeight = Math.max.apply(Math, tempArray.map((obj) => {
			return obj.total;
		})) || 6;

		return (maxColumnValue > 4 ? 'errorinput' : '');
	}

	render () {
		const { app, resource, rolesArray } = this.props;
		let { layoutItems, layout } = this.state;

		if (!resource)
			return null;

		let errorClass = this.checkGridValid();

		let Window_Width = (document.getElementsByClassName('layout').length > 0 ? document.getElementsByClassName('layout')[0].offsetWidth : $(window).width());

		let containerWidth = Window_Width > 700 ? Window_Width : 700;

		let gridcontainerWidth = Math.round(containerWidth / 4),
			gridcontainerHeight = Math.round($(window).height() / 6) > 100 ? Math.round($(window).height() / 6) : 100;

		return (
			<div>
				<div className = 'row'>
					<div className = "col-md-12 bg-white report-header">
						<div className = "row">
							<div className = "col-md-8 margintop-15 report-header-title">
								<div style = {{textTransform : 'none', color:'#000'}}>
									{!resource.name ? `New Dashboard Widget` : `${resource.name}`}
									<label style={{color: '#9B9B9B', fontSize: '12px', marginLeft: '10px'}}>{layoutItems.length == 0 ? 'Customise and assemble your widgets here to view in your dashboard.' : 'Resize or Drag Widget anywhere to assemble your Dashboard.'}</label>
								</div>
							</div>
							<div className = "col-md-4">
								<div className="report-header-btnbar float-right">
									<button
										type = "button"
										className = "btn gs-btn-primary btn-sm marginleft-5"
										onClick = {() => this.addWidgetModal()}>
										<i className="fa fa-plus"></i>Add Widget
									</button>

									<button
										type = "button"
										onClick = {() => this.onSave()}
										disabled = {errorClass ? true : false}
										className = "btn gs-btn-success btn-sm marginleft-5">
										<i className="fa fa-check"></i>Done
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className = 'row'>
				<div className = 'col-md-12' style = {{
					maxHeight:`${$(window).height() - 110}px`,
					overflowX: 'hidden',
					overflowY: 'auto'
				}}>
				<div className = 'dashboard-grid-container' style = {{
					backgroundSize: `${gridcontainerWidth-1}px ${gridcontainerHeight}px`,
					backgroundImage: 'linear-gradient(to right, #EFEFEF 2px, transparent 2px), linear-gradient(to bottom, #EFEFEF 2px, transparent 2px)',
					minHeight: `${(gridcontainerHeight) * 6}px`,
					float: 'left',
					width: '100%'
				}}>
					<ReactGridLayout
						className = "layout"
						cols = {4}
						//cols = {{ lg: 4, md: 4, sm: 2, xs: 1, xxs: 1 }}
						//preventCollision = {true}
						rowHeight = {gridcontainerHeight}
						width = {containerWidth}
						//margin = {[10,15]}
						margin = {[0,0]}
						onLayoutChange = {
							(l) => {
								this.onLayoutChange(l)
							}
						}
					>
						{this.state.layoutItems.map((el) => this.createLayout(el))}
					</ReactGridLayout>

					{layoutItems.length == 0 ? <div className = 'bg-white col-sm-12 col-md-8 offset-md-2'>
						<div style = {{
							textAlign: 'center',
							verticalAlign: 'middle',
							lineHeight: '80px',
							marginTop: '200px',
						}}>
							<p style={{color: '#9B9B9B'}}>
								Click "Add Widget" button to add widgets in your Dashboard.
							</p>
						</div>
					</div> : null}
				</div></div>
				</div>
			</div>
		);
	}
}

class WidgetSelectModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag: false,
			widgetDetails: {
				sales: [],
				project: [],
				purchase: [],
				stock: [],
				accounts: [],
				service: [],
				production: [],
				hr: [],
				admin: []
			},
			tabActive: 'sales',
			reportListActive: null
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.renderGallery = this.renderGallery.bind(this);
		this.renderGalleryItems = this.renderGalleryItems.bind(this);
		this.renderReportList = this.renderReportList.bind(this);
		this.setActiveTab = this.setActiveTab.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);

		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	onLoad () {
		let { widgetDetails } = this.state;
		let { resource, layoutItems } = this.props;

		let reportIdArr = [];

		if (layoutItems.length > 0)
			layoutItems.forEach((item) => {
				reportIdArr.push(item.reportid);
			});

		if (this.props.widgetArray && this.props.widgetArray.length > 0)
			this.props.widgetArray.forEach((item) => {
				if (!reportIdArr.includes(item.id) && widgetDetails[item.module.toLowerCase()])
					widgetDetails[item.module.toLowerCase()].push(JSON.parse(JSON.stringify(item)));
			});

		for (let prop in widgetDetails) {
			widgetDetails[prop].sort((a, b) => {
				return (a.name.toLowerCase() < b.name.toLowerCase()) ? -1 : (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : 0;
			});
		}

		this.setState({
			widgetDetails
		});

		this.updateLoaderFlag(false);
	}

	setActiveTab (keyvalue) {
		this.setState({
			tabActive: keyvalue,
			reportListActive: null
		});
	}

	renderReportList (item, index) {
		let { resource, widgetArray } = this.props,
			{ widgetDetails, tabActive } = this.state;

		this.setState({
			reportListActive: index
		}, () => {
			this.props.callback(item);
			this.props.closeModal();
		});
	}

	renderGallery () {
		let { widgetDetails, tabActive } = this.state;

		return (
			<div className = 'col-md-12'>
				<div className = 'row no-gutters'>
					<div className = 'col-md-2 paddingleft-0 paddingright-0' style = {{borderRight: '1px solid #e5e5e5'}}>
						<ul className = 'nav flex-column'>
							{ Object.keys(widgetDetails).map((keyvalue, index) => {
								if (widgetDetails[keyvalue].length > 0)
									return <li className = 'nav-item' key = {index}>
										<a className = {`nav-link gs-dataset-nav-link pl-4 ${tabActive == keyvalue ? 'active'  : 'gs-print-tab-inactive'}`}
											onClick = {() => this.setActiveTab(keyvalue)}
										>
											<span className = 'gs-uppercase'>{keyvalue}</span>
										</a>
									</li>
							})}
						</ul>
					</div>
					<div className = 'col-md-10 paddingleft-0 paddingright-0' style = {{overflowY: 'auto', maxHeight: '380px'}}>
						<div className = 'tab-content'>
							{ Object.keys(widgetDetails).map((keyvalue, keyindex) => {
								if (widgetDetails[keyvalue].length > 0)
									return (
										<div className = {`tab-pane fade ${tabActive == keyvalue ? 'active show'  : ''}`} key = {keyindex}>
											<ul className = 'list-group'>
												{this.renderGalleryItems(widgetDetails[keyvalue])}
											</ul>
										</div>
									);
							})}
						</div>
					</div>
				</div>
			</div>
		);
	}

	renderGalleryItems (galleryItems) {
		let { reportListActive } = this.state;

		return galleryItems.map((item, index) => {
			return (
				<li className={`gs-dashboard-list-group-item border-left-0 border-right-0 rounded-0 gs-dashboard-hoverDiv ${reportListActive == index ? 'active' : ''} ${index == 0 ? 'border-top-0' : '' }`}  key={index} onClick = {() => this.renderReportList(item, index)}>
					<div>
						<b style = {{fontWeight: 600, fontSize: '16px'}}>{item.name}</b>
						<label className="pull-right gs-dashboard-badge marginleft-10 marginbottom-0">{item.config.analyticstype}</label>
					</div>
				</li>
			);
		});
	}

	render() {
		let { reportListActive, widgetDetails } = this.state,
			{ resource, layoutItems } = this.props,
			reportIdArr = [],
			filterString = `reports.type = 'Analytics'`;

		if (layoutItems.length > 0) {
			layoutItems.forEach((item) => {
				reportIdArr.push(item.reportid);
			});

			filterString = filterString + ` AND NOT(reports.id = ANY(array[${reportIdArr}]))`;
		}

		return (
			<div className = 'react-outer-modal'>
				<div className = 'react-modal-header p-4'>
					<div className = 'row'>
						<div className = 'col-md-6'>
							<h5 className = 'modal-title gs-text-color'>Select Widget</h5>
						</div>
						<div className = 'col-md-6'>
							<div className = 'pull-right' style={{fontWeight:'100', fontSize: '25px', color: '#959595', cursor: 'pointer'}} onClick={this.props.closeModal}>x</div>
						</div>
					</div>
				</div>
				<div className = 'react-modal-body p-0' style = {{overflowY: 'auto', maxHeight: '450px'}}>
					<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
					<div className="row no-gutters">
						<div className="col-md-8 offset-md-2 form-group pt-2">
							<AutoSelect
								resource = {'reports'}
								fields = {'id,name,type,datasetid,module,foldername,allowedroles,hasexcelexport,config,displayorder,remarks'}
								filter = {filterString}
								onChange = {(value, valueobj) => {
									this.props.callback(valueobj);
									this.props.closeModal();
								}}
								placeholder = {'Search'}
							/>
						</div>
						{this.renderGallery()}
					</div>
				</div>
			</div>
		);
	}
}

DashboardBuilder = connect(
	(state, props) => {
		let formName = props.match.params.id > 0 ? `DashboardBuilder_${props.match.params.id}` : 'DashboardBuilder_create';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			touchOnChange: true,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(DashboardBuilder));

export default DashboardBuilder;