import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field } from 'redux-form';
import { checkActionVerbAccess } from '../utils/utils';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { RichText } from '../components/formelements';

class PreferencesForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			users: {},
			loaderflag : true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.save = this.save.bind(this);
		this.cancel = this.cancel.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();	
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	onLoad() {
		let users = {};
		axios.get(`/api/users?field=id,modified,emailsignature&filtercondition=users.id=${this.props.app.user.id}`).then((response)=> {
			users = {
				...response.data.main[0]
			};

			this.setState({ users });
			this.updateLoaderFlag(false);
		});
	}

	save () {
		this.updateLoaderFlag(true);
		let tempData = {
			actionverb : 'Change EmailSign',
			data : this.state.users
		};

		axios({
			method : 'post',
			data : tempData,
			url : '/api/users'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			let users = {
				...response.data.main
			};
			this.setState({ users });

			this.updateLoaderFlag(false);
		});
	}

	inputOnChange(value, name) {
		let { users } = this.state;
		users[name] = value;
		this.setState({ users });
	}

	cancel () {
		this.props.history.push('/dashboard');
	}

	render() {
		let accessClass = checkActionVerbAccess(this.props.app, 'users', 'Change EmailSign') ? '' : 'hide';
		let { users } = this.state;

		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row form-element-style" style={{marginTop: `${((document.getElementsByClassName('navbar').length > 0 ? document.getElementsByClassName('navbar')[0].clientHeight : 0) + 10)}px`}}>
					<div className="col-md-10 offset-md-1">
						<div className="card borderradius-0 gs-card">
							<h4 className="card-header borderradius-0 card-header-custom gs-card-header">Preferences</h4>
							<div className="card-body">
								<div className="row">
									<div className="col-md-2 form-group">
										<label className="labelclass margintop-50">Email Signature</label>
									</div>
									<div className="col-md-10 form-group">
										<RichText value={this.state.users.emailsignature} onChange={(value)=>{this.inputOnChange(value, 'emailsignature')}} />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-12">
						<div className="muted credit text-center sticky-footer actionbtn-bar" style={{width: '100%'}}>
							<button type="button" className="btn btn-sm btn-width btn-secondary" onClick={this.cancel}><i className="fa fa-times"></i>Close</button>
							<button type="button" className={`btn btn-sm btn-width gs-btn-success ${accessClass}`} onClick={this.save} disabled={!this.state.users.emailsignature} ><i className="fa fa-save"></i>Save</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

PreferencesForm = connect((state) =>{
	return {app: state.app}
}) (PreferencesForm);

export default PreferencesForm;
