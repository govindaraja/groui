import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { updateFormState, updateReportFilter } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { DateEle, localSelectEle, autoSelectEle, selectAsyncEle, checkboxEle, textareaEle, InputEle, NumberEle, SpanEle } from '../components/formelements';
import { numberNewValidation, dateNewValidation, stringNewValidation, checkActionVerbAccess } from '../utils/utils';
import { dateFilter, datetimeFilter, timeFilter} from '../utils/filter';
import { Reactuigrid } from '../components/reportcomponents';
import { ChildEditModal } from '../components/utilcomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import { Prompt } from 'react-router-dom';
import { AdditionalInformationSection } from '../containers/ndtsection1';

class LeaveCreditBatch extends Component {
	constructor (props) {
		super(props);

		this.state = {
			loaderflag: true,
			createParam: this.props.match.params.id > 0 ? false : true,
			lc_columns: [{
					name: '',
					headerformat: 'checkbox',
					key: 'ischecked',
					locked: true,
					format: 'checkbox',
					cellClass: 'text-center',
					onChange: '{report.checkboxOnChange}',
					restrictToExport: true,
					width: 100,
					if: !this.props.match.params.id
				}, {
					name: 'Employee Name',
					key: 'displayname',
					locked: true,
					width: 200
				}, {
					name: 'Employee Code',
					key: 'employeecode',
					cellClass: 'text-center',
					locked: true,
					width: 150
				}, {
					name: 'Pay Level',
					key: 'paylevelid_name',
					cellClass: 'text-center',
					width: 150
				}, {
					name: 'Location',
					key: 'locationid_name',
					cellClass: 'text-center',
					width: 150
				}
			],
			monthObj: {
				'Jan': 'January',
				'Feb': 'February',
				'Mar': 'March',
				'Apr': 'April',
				'May': 'May',
				'Jun': 'June',
				'Jul': 'July',
				'Aug': 'August',
				'Sep': 'September',
				'Oct': 'October',
				'Nov': 'November',
				'Dec': 'December'
			}
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.initialize = this.initialize.bind(this);
		this.getItemById = this.getItemById.bind(this);
		this.getLeaveCreditDetails = this.getLeaveCreditDetails.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.save = this.save.bind(this);
		this.deleteEmployee = this.deleteEmployee.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);

		if (this.props.match.path == '/createLeaveCreditBatch')
			this.initialize();
		else
			this.getItemById();
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag)
			this.updateLoaderFlag(false);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	initialize() {
		let tempObj = {
			lcbatchdate: new Date(),
			locationid: null,
			leavecreditsArray: [],
			columns: []
		};

		axios.get(`/api/employees?&field=id,displayname,locationid,paylevelid&filtercondition=employees.userid=${this.props.app.user.id}`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.length > 0)
					tempObj['locationid'] = response.data.main[0].locationid;

				this.props.initialize(tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	getItemById() {
		axios.get(`/api/leavecreditbatch/${this.props.match.params.id}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.initialize({
					...response.data.main,
					leavecreditsArray: [],
					columns: []
				});

				this.setState({
					ndt: {
						notes: response.data.notes,
						documents: response.data.documents,
						tasks: response.data.tasks
					}
				});

				setTimeout(() => {
					this.getLeaveCreditDetails()
				}, 0);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	getLeaveCreditDetails(flag) {
		let { lc_columns } = this.state;

		if (flag)
			this.props.updateFormState(this.props.form, {
				leavecreditsArray: [],
				allocationIdArray: [],
				expiryIdArray: [],
				originalRows: [],
				columns: []
			});

		this.updateLoaderFlag(true);

		let filterString = [];

		if (this.props.resource && this.props.resource.id)
			filterString.push(`batchid=${this.props.resource.id}`, `type=Details`);
		else {
			if (!this.props.resource.transactiondate) {
				this.updateLoaderFlag(false);
				return null;
			}

			['locationid', 'transactiondate'].forEach((item) => {
				if (this.props.resource[item])
					filterString.push(`${item}=${this.props.resource[item]}`);
			});

			filterString.push(`type=Create`);
		}

		axios.get(`/api/query/getbatchleavecreditdetailsquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.length > 0) {
					response.data.main.sort((a, b) => {
						return a.displayname.toLowerCase() < b.displayname.toLowerCase() ? -1 : (a.displayname.toLowerCase() > b.displayname.toLowerCase() ? 1 : 0)
					});

					if (lc_columns.length > 5)
						lc_columns.splice(5, lc_columns.length-1);

					if (this.props.resource && !this.props.resource.id)
						lc_columns.push({
							name: 'Entitlement No',
							key: 'entitlementno',
							format: 'button',
							onClick: '{report.btnOnClick}',
							buttonname: '{item.entitlementno}',
							buttonclass: 'btn-link',
							cellClass: 'text-center',
							restrictToExport: false,
							width: 150
						}, {
							name: 'Effective From',
							key: 'effectivefrom',
							format: 'date',
							cellClass: 'text-center',
							width: 150
						});

					if (response.data.allocationIdArray.length > 0) {
						response.data.allocationIdArray.forEach((item) => {
							if (this.props.resource && !this.props.resource.id)
								lc_columns.push({
									name: response.data.leaveObject[item],
									key: `allocation_${item}`,
									cellClass: "text-right",
									width: 150
								});
							else
								lc_columns.push({
									name: response.data.leaveObject[item],
									key: `allocation_${item}`,
									format: 'anchortag',
									transactionname: 'leavecredits',
									transactionid: `allocation_${item}_key`,
									cellClass: "text-right",
									width: 150
								});
						});

						lc_columns.push({
							name: 'Total Allocation',
							key: 'totalAllocation',
							cellClass: "text-right",
							width: 150
						});
					}

					if (response.data.expiryIdArray.length > 0) {
						response.data.expiryIdArray.forEach((item) => {
							if (this.props.resource && !this.props.resource.id)
								lc_columns.push({
									name: response.data.leaveObject[item],
									key: `expiry_${item}`,
									cellClass: "text-right",
									width: 150
								});
							else
								lc_columns.push({
									name: response.data.leaveObject[item],
									key: `expiry_${item}`,
									format: 'anchortag',
									transactionname: 'leavecredits',
									transactionid: `expiry_${item}_key`,
									cellClass: "text-right",
									width: 150
								});
						});

						lc_columns.push({
							name: 'Total Expiry',
							key: 'totalExpiry',
							cellClass: "text-right",
							width: 150
						});
					}

					if (this.props.resource && this.props.resource.id)
						lc_columns.push({
							name: 'Remarks',
							key: 'remarks',
							width: 200
						});

					this.setState({
						lc_columns
					}, () => {
						this.props.updateFormState(this.props.form, {
							leavecreditsArray: response.data.main,
							allocationIdArray: response.data.allocationIdArray,
							expiryIdArray: response.data.expiryIdArray,
							originalRows: response.data.main,
							columns: this.state.lc_columns
						});
					});

					this.props.updateReportFilter(this.props.form, this.props.resource);
				} else
					this.props.openModal(modalService['infoMethod']({
						header: 'Error',
						body: 'No employee found for your search!',
						btnArray: ['Ok']
					}));
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	save(param, confirm) {
		this.updateLoaderFlag(true);

		axios({
			method: 'post',
			data: {
				actionverb: param,
				data: this.props.resource,
				ignoreExceptions: confirm ? true : false
			},
			url: '/api/leavecreditbatch'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.save(param, true);
			}));

			if (response.data.message == 'success') {
				let resource = {
					...response.data.main
				};

				if (this.props.match.path == '/createLeaveCreditBatch')
					this.props.history.replace(`/details/leavecreditbatch/${response.data.main.id}`);
				else
					this.props.initialize(resource);

				if (this.props.match.path != '/createLeaveCreditBatch')
					setTimeout(this.getLeaveCreditDetails,0);
			}

			this.updateLoaderFlag(false);
		});
	}

	checkboxHeaderOnChange(param) {
		let checkCount = 0;

		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if (param)
				item.ischecked = true;
		});

		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		this.refs.grid.forceRefresh();
	}

	checkboxOnChange(value, item) {
		item.ischecked = value;

		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		this.refs.grid.refresh();
	}

	deleteEmployee() {
		let originalRows = this.props.resource.originalRows,
		itemFound = false,
		finalEmpArray = [],
		empIdArray = [];

		originalRows.forEach((item) => {
			if (item.ischecked) {
				itemFound = true;
				empIdArray.push(item);
			}

			if (!item.ischecked)
				finalEmpArray.push(item);
		});

		if (!itemFound)
			this.props.openModal(modalService['infoMethod']({
				header: 'Error',
				body: 'Please Choose atleast one employee to delete',
				btnArray: ['Ok']
			}));
		else if (empIdArray.length == originalRows.length)
			this.props.openModal(modalService['infoMethod']({
				header: 'Error',
				body: 'Need atleast one Employee',
				btnArray: ['Ok']
			}));
		else {
			let message = {
				header: 'Warning',
				body: 'Do you want to Delete selected employee(s)?',
				btnArray: ['Yes', 'No']
			};

			this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if (param)
					this.props.updateFormState(this.props.form, {
						originalRows: finalEmpArray,
						leavecreditsArray: finalEmpArray
					});
			}));
		}
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	btnOnClick(item) {
		this.props.createOrEdit('/details/leaveentitlements/:id', item.leaveentitlementid, {}, ()=>{this.getLeaveCreditDetails()});
	}

	render() {
		const { app, resource} = this.props;
		let { createParam, monthObj } = this.state;

		if (!resource)
			return null;

		let runFlag = resource.originalRows && resource.originalRows.length > 0 ? true : false,
			disableclass = resource && resource.id ? 'disablediv' : '';

		return (
			<div>
				<Loadingcontainer isloading = {this.state.loaderflag}></Loadingcontainer>
				<Prompt when={!createParam && this.props.dirty && this.props.anyTouched} message={`Unsaved Changes`} />
				<div className = 'row'>
					<div className = 'col-sm-12 affixmenubar col-md-12 col-lg-9 d-sm-block d-md-flex justify-content-md-between '>
						<div className = 'semi-bold-custom font-16 d-flex align-items-center'>
							<a className = 'affixanchor float-left marginright-10' onClick={()=>{this.props.history.goBack()}}>
								<span className = 'fa fa-chevron-left'></span>
							</a>
							<div className="gs-uppercase float-left">Leave Batch : </div>
							{resource.id ? <div className="float-left marginleft-10" style={{maxWidth: '300px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis'}} title={`${resource.lcbatchno}`}>{`${resource.lcbatchno}`}</div> : null}
							<label className = 'badge gs-badge-success ml-2 mb-0'>{resource.status}</label>
						</div>
					</div>
				</div>
				<form>
					<div className = 'row'>
						<div className = 'col-sm-12 col-md-12 col-lg-9' style={{marginTop:`${((document.getElementsByClassName('affixmenubar').length > 0 ? document.getElementsByClassName('affixmenubar')[0].clientHeight : 0) + 3)}px`}}>
							<div className = 'row' >
								<div className = 'col-md-12'>
									<div className = 'card' style={{marginBottom:'10px'}}>
										<div className = 'card-header listpage-title'>
											<div className = 'row'>
												<div className = 'col-md-6 gs-uppercase'>
													Basic Information
												</div>
												{app.feature.leaveCalendarStartingMonth ? <div className = 'col-md-6 text-right-md'>
													<label style={{color: '#329cce'}}>Leave Calendar Starting Month - <b>{monthObj[app.feature.leaveCalendarStartingMonth]}</b></label>
												</div> : null}
											</div>
										</div>
										<div className = 'card-body'>
											<div className = {`row ${disableclass}`}>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Numbering Series</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'numberingseriesmasterid'}
																props = { {
																		resource: 'numberingseriesmaster',
																		fields: 'id,name,format,isdefault,currentvalue',
																		filter: `numberingseriesmaster.resource='leavecreditbatch'`
																	}
																}
																createParamFlag = {true}
																defaultValueUpdateFn = {
																	(value) => this.props.updateFormState(this.props.form, {
																		numberingseriesmasterid: value
																	})
																}
																component = {selectAsyncEle}
																validate = {[numberNewValidation({required: true, title : 'Numbering Series'})]}
															/>
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Batch Date</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'lcbatchdate'}
																props = {{
																	required: true
																}}
																component = {DateEle}
																validate = {[dateNewValidation({required:  true, title : 'Batch Date'})]}
															/>
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Transaction Date</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'transactiondate'}
																props = {{
																	required: true,
																	onChange: () => {this.getLeaveCreditDetails(true)}
																}}
																component = {DateEle}
																validate = {[dateNewValidation({required:  true, title : 'Transaction Date'})]}
															/>
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Location</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'locationid'}
																props = {{
																	resource: 'locations',
																	fields: 'id,name',
																	onChange: () => {this.getLeaveCreditDetails(true)}
																}}
																component = {autoSelectEle}
															/>
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Remarks</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'remarks'}
																component = {textareaEle}
															/>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								{ runFlag ? <div className = 'col-md-12'>
									<div className = 'card' style={{marginBottom:'3rem'}}>
										<div className = 'card-header listpage-title gs-uppercase'>
											<div className = 'row'>
												<div className = 'col-md-6'>
													Employee Leave Details
												</div>
												<div className = 'col-md-6 text-right-md'>
													{ createParam ? <button
														type = 'button'
														className = 'btn gs-form-btn-danger btn-sm btn-width marginleft-5'
														onClick = {() => this.deleteEmployee()}>
															<i className = 'fa fa-trash-o'></i>Delete
													</button> : null }
													<button
														type = 'button'
														className = 'btn gs-form-btn-success btn-sm btn-width marginleft-5'
														onClick = {() => this.refs.grid.exportExcel()}>
														<i className = 'fa fa-file-excel-o'></i>Export Data
													</button>
												</div>
											</div>
										</div>
										<div className = 'card-body'>
											<div className='row responsive-form-element'>
												<Reactuigrid
													excelname = {'Leave Batch'}
													app = {app}
													ref = 'grid'
													report = {this}
													height = {450}
													gridprops = {resource}
													updateFormState = {this.props.updateFormState}
													form = {this.props.form}
													updateReportFilter = {this.props.updateReportFilter}
													openTransaction = {this.openTransaction}
													checkboxOnChange = {this.checkboxOnChange}
													checkboxHeaderOnChange = {this.checkboxHeaderOnChange}
												/>
											</div>
										</div>
									</div>
								</div> : <div className = 'col-md-12 margintop-50'>
									<div className = 'col-md-10 offset-md-1 alert alert-warning text-center'>
										No employee found !
									</div>
								</div>}
							</div>
						</div>
						<div className={`col-sm-12 ndt-section col-md-12 col-lg-3 paddingleft-md-0 margintop-5 `} >
							{resource && resource.id && this.state.ndt ? <AdditionalInformationSection
								ndt = {this.state.ndt}
								key = {0}
								parentresource = {'leavecreditbatch'}
								openModal = {this.props.openModal}
								parentid = {resource.id}
								relatedpath = {this.props.match.path}
								createOrEdit = {this.props.createOrEdit} 
								app ={this.props.app} 
								history={this.props.history}
							/> : null }
						</div>
					</div>
					<div className = 'row'>
						<div className = 'col-sm-12 col-md-12 col-lg-9'>
							<div className = 'muted credit text-center sticky-footer actionbtn-bar' style={{boxShadow:'none'}}>
								<div style = {{width: '80%', float: 'left'}}>
									<button
										type = 'button'
										className = 'btn btn-sm btn-width btn-secondary'
										onClick={() => this.props.history.goBack()}>
										<i className = 'fa fa-times marginright-5'></i>Close
									</button>
									{checkActionVerbAccess(this.props.app, 'leavecreditbatch', 'Approve') && createParam ? <button
										type = 'button'
										className = 'btn btn-width btn-sm gs-btn-success'
										disabled = {this.props.invalid || !runFlag}
										onClick = {()=>{this.save('Approve');}}>
										<i className = 'fa fa-save'></i>Approve
									</button> : null}
								</div>

								{resource.status == 'Approved' && checkActionVerbAccess(this.props.app, 'leavecreditbatch', 'Cancel') && (resource.createdby == this.props.app.user.id || this.props.app.user.roleid.indexOf(1) >= 0) ? <div className = 'btn-group dropup' style = {{float: 'left', width: '20%'}}>
									<button
										type = 'button'
										className = 'btn btn-sm btn-width gs-btn-info dropdown-toggle'
										data-toggle = 'dropdown'
										aria-haspopup = 'true'
										aria-expanded = 'false'>More <span className = 'sr-only'>Toggle Dropdown</span>
									</button>
									<div className = 'dropdown-menu gs-morebtn-dropdown-menu'>
										<div className = {`dropdown-item gs-text-warning`}
											onClick = {() => this.save('Cancel')}>
											<i className = {`fa fa-ban`}></i> Cancel</div>
									</div>
								</div> : null}
							</div>
 						</div>
					</div>
				</form>
		   </div>
		);
	}
}

LeaveCreditBatch = connect(
	(state, props) => {
		let formName = 'LeaveCreditBatch';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			touchOnChange: true,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(LeaveCreditBatch));

export default LeaveCreditBatch;