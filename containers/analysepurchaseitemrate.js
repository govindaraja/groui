import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';
import { currencyFilter, dateFilter, uomFilter } from '../utils/filter';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			stateupdated: false,
			showItemdetails : true,
			currentVendor: [],
			allVendor: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.close = this.close.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		if(this.props.item.itemid) {
			let queryString = `/api/query/analyseitemquerypurchase?itemid=${this.props.item.itemid}&partnerid=${this.props.partnerid}&warehouseid=${this.props.item.warehouseid}`;

			axios.get(queryString).then((response) => {
				if (response.data.message == 'success') {
					
					this.setState({
						analyseitem: response.data.analyseitem,
						currentVendor: response.data.currentvendor,
						allVendor: response.data.allvendor,
						stateupdated: true
					});

					if (Object.keys(response.data.analyseitem).length == 0 || Object.keys(response.data.analyseitem).length < 0) {
						this.setState({showItemdetails: false});
					}
				} else {
					var apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}
	}

	close() {
		this.props.closeModal();
	}

	render() {
		if(!this.state.stateupdated)
			return null;
		return (
			<div>
				<div className="react-modal-header">
					<h6 className="modal-title gs-text-color semi-bold-custom">Recent Purchase Prices</h6>
				</div>
				<div className="react-modal-body modalmaxheight">
					{this.state.showItemdetails ? <div className="row">
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Item Name</label>
							<input type="text" className="form-control" value={this.state.analyseitem.itemid_name || ""} disabled />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">UOM</label>
							<input type="text" className="form-control" value={this.state.analyseitem.uomid_name || ""} disabled />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Stock In Hand</label>
							<input type="text" className="form-control" value={this.state.analyseitem.onhandqty || ""} disabled />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Effective Qty</label>
							<input type="text" className="form-control" value={this.state.analyseitem.effectiveqty || ""} disabled />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Safety Stock</label>
							<input type="text" className="form-control" value={this.state.analyseitem.safetystock || ""} disabled />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Reorder Qty</label>
							<input type="text" className="form-control" value={this.state.analyseitem.reorderqty || ""} disabled />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Lead Time</label>
							<input type="text" className="form-control" value={this.state.analyseitem.itemid_leadtime || ""} disabled />
						</div>
					</div> : null }
					{this.state.currentVendor.length > 0 ? <div className="row"><div className="col-md-12 col-sm-12 col-xs-12">
						<h5>{this.props.partnerid_name}</h5>
						<div className="table-responsive">
							<table className="table table-sm gs-table gs-table-bordered">
								<thead className="thead-light">
									<tr>
										<th className="text-center">PO Number</th>
										<th className="text-center">PO Date</th>
										{(this.props.app.feature.useMakeInTransactions) ? <th className="text-center">Make</th> : null}
										<th className="text-center">Quantity</th>
										<th className="text-center">UOM</th>
										<th className="text-center">Rate</th>
										<th className="text-center">Discount</th>
										<th className="text-center">Final Rate</th>
									</tr>
								</thead>
								<tbody>
									{this.state.currentVendor.map((quote, key) => {
										return <tr key = {key}>
											<td>{quote.ponumber}</td>
											<td className="text-center">{dateFilter(quote.podate)}</td>
											{(this.props.app.feature.useMakeInTransactions) ? <td className="text-center">{quote.itemmakeid_name}</td> : null}
											<td className="text-center">{quote.quantity}</td>
											<td className="text-center">{quote.uomid_name}</td>
											<td className="text-right">{currencyFilter(quote.rate, quote.currencyid, this.props.app)}</td>
											<td className="text-right">
												{quote.discountmode != "Percentage" && quote.discountquantity ? <span>{currencyFilter(quote.discountquantity, quote.currencyid, this.props.app)}</span> : null }
												{quote.discountmode == "Percentage" && quote.discountquantity ? <span>{quote.discountquantity} %</span> : null }
											</td>
											<td className="text-right">{currencyFilter(quote.finalrate, quote.currencyid, this.props.app)}</td>
										</tr>
									})}
								</tbody>
								
							</table>
						</div>
					</div></div> : null}
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<h5>All Suppliers</h5>
							<div className="table-responsive">
								<table className="table table-sm gs-table gs-table-bordered">
									<thead className="thead-light">
										<tr>
											<th className="text-center">Supplier</th>
											<th className="text-center">PO Number</th>
											<th className="text-center">PO Date</th>
											{(this.props.app.feature.useMakeInTransactions) ? <th className="text-center">Make</th> : null}
											<th className="text-center">Quantity</th>
											<th className="text-center">UOM</th>
											<th className="text-center">Rate</th>
											<th className="text-center">Discount</th>
											<th className="text-center">Final Rate</th>
										</tr>
									</thead>
									<tbody>
										{this.state.allVendor.map((compquote, key) => {
											return <tr key = {key}>
												<td>{compquote.name}</td>
												<td className="text-center">{compquote.ponumber}</td>
												<td className="text-center">{dateFilter(compquote.podate)}</td>
												{(this.props.app.feature.useMakeInTransactions) ? <td className="text-center">{compquote.itemmakeid_name}</td> : null}
												<td className="text-center">{compquote.quantity}</td>
												<td className="text-center">{compquote.uomid_name}</td>
												<td className="text-right">{currencyFilter(compquote.rate, compquote.currencyid, this.props.app)}</td>
												<td className="text-right">
													{compquote.discountmode != "Percentage" && compquote.discountquantity ? <span>{currencyFilter(compquote.discountquantity, compquote.currencyid, this.props.app)}</span> : null }
													{compquote.discountmode == "Percentage" && compquote.discountquantity ? <span>{compquote.discountquantity} %</span> : null}
												</td>
												<td className="text-right">{currencyFilter(compquote.finalrate, compquote.currencyid, this.props.app)}</td>
											</tr>
										})}
									</tbody>
								
								</table>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
