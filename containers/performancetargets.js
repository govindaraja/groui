import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { reduxForm, Field,formValueSelector } from 'redux-form';
import { Prompt ,matchPath } from "react-router-dom";
import { updateFormState, updateAppState } from '../actions/actions';
import { commonMethods, modalService,generateExcel } from '../utils/services';
import { LocalSelect, SelectAsync, AutoSelect, DateElement, ChildEditModal, AutoSuggest ,AutoMultiSelect} from '../components/utilcomponents';
import Loadingcontainer from '../components/loadingcontainer';
import { InputEle, localSelectEle, autoSelectEle, checkboxEle, NumberEle, DateEle, selectAsyncEle, dateTimeEle, autosuggestEle, autoMultiSelectEle } from '../components/formelements';
import {stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, requiredNewValidation, checkActionVerbAccess, checkArray } from '../utils/utils';
import {currencyFilter} from '../utils/filter';

const dateFormatterTypeArr = [{
	id: 'month-year',
	name: 'Month - Year'
}, {
	id: 'financialquarter-financialyear',
	name: 'Financial Quarter - Year'
}, {
	id: 'calendarquarter-calendaryear',
	name: 'Calendar Quarter - Year'
}];

const formatTargerPeriod = (targetperiod) => targetperiod.replace(/ /g, '').replace(/-/g, '');

class PerformanceTarget extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : false,
			localTocuhed: false,
			createParam: this.props.match.params.id > 0 ? false : true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.cancel = this.cancel.bind(this);
		this.getItemById = this.getItemById.bind(this);
		this.addItem = this.addItem.bind(this);
		this.selector = formValueSelector(this.props.form);
		this.periodOnChange = this.periodOnChange.bind(this);
		this.renderTargetDetails = this.renderTargetDetails.bind(this);
		this.renderTargetDefinition = this.renderTargetDefinition.bind(this);
		this.renderTable = this.renderTable.bind(this);
		this.renderTitleTd = this.renderTitleTd.bind(this);
		this.renderTotalTD = this.renderTotalTD.bind(this);
		this.renderTargetItemsTD = this.renderTargetItemsTD.bind(this);
		this.targetValueOnChange = this.targetValueOnChange.bind(this);
		this.deleteItem = this.deleteItem.bind(this);
		this.listbodyRef = this.listbodyRef.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
		this.reportOnChange = this.reportOnChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag)
			this.updateLoaderFlag(false);
	}

	componentWillMount() {
		if(this.state.createParam)
			this.initialiseState();
		else
			this.getItemById();
	}

	initialiseState() {
		let tempObj = {
			performancetargetitems : [],
			userRows: [],
			targetItemsHeader : []
		};
		
		this.props.initialize(tempObj);
		this.updateLoaderFlag(false);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	getItemById() {
		this.updateLoaderFlag(true);
		axios.get(`/api/performancetargets/${this.props.match.params.id}`).then((response) => {
			if (response.data.message == 'success') {
				let tempObj = { ...response.data.main,
					userRows: [],
					targetItemsHeader: []
				};
				this.props.initialize(tempObj);
				setTimeout(() => {
					this.periodOnChange(true);
				}, 0);
				this.updateLoaderFlag(false);
			} else {
				this.updateLoaderFlag(false);
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	periodOnChange(firstTime) {
		this.updateLoaderFlag(true);

		if(!this.props.resource.targetstart || !this.props.resource.targetend || !this.props.resource.periodicity) {
			this.props.updateFormState(this.props.form, {
				targetItemsHeader : []
			});
			return this.updateLoaderFlag(false);
		}

		axios.get(`/api/query/getperformancetargetperiodicity?fromdate=${this.props.resource.targetstart}&todate=${this.props.resource.targetend}&periodicity=${this.props.resource.periodicity}`).then((response) => {
			if(response.data.message == 'success') {
				let userRows = this.props.resource.userRows;

				if(firstTime)
					userRows = commonMethods.sortUserRowsPerformanceTarget(this.props.resource.performancetargetitems, response.data.main);

				this.props.updateFormState(this.props.form, {
					userRows: userRows,
					targetItemsHeader : commonMethods.sortHeaderRowsPerformanceTarget(response.data.main, this.props.resource.periodicity)
				});
				setTimeout(() => {
					this.targetValueOnChange(null, true);
				}, 0);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			return this.updateLoaderFlag(false);
		});
	}

	reportOnChange(value,valueObj){
		if(value > 0){
			this.updateLoaderFlag(true);
			let field = valueObj.config.fields.periodicity;
			axios.get(`/api/dataset/info/${valueObj.datasetid}`).then((response) => {
				if(response.data.message == 'success') {
					if(response.data.main.fields[field]){
						this.props.updateFormState(this.props.form, {
							periodicity : response.data.main.fields[field].configuredProp.formatter
						});
						this.periodOnChange();
					}
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
				this.updateLoaderFlag(false);
			});
		}
	}

	cancel() {
		this.props.history.goBack();
	}

	save(actionverb, confirm) {
		this.updateLoaderFlag(true);

		let resource = {
			...this.props.resource,
			performancetargetitems: []
		};

		let targetObj = {};
		this.props.resource.performancetargetitems.forEach(item => {
			let type = item.userid > 0 ? 'user' : 'group';

			targetObj[`${type}_${type == 'group' ? item.groupname : item.userid}_${item.targetperiod}`] = item;
		});

		let errorArray = [];

		resource.userRows.forEach((item) => {
			if(item.type != 'user')
				return null;

			let totaltarget = 0;
			let targetvalueerror = false
			resource.targetItemsHeader.forEach(targetperiod => {
				let newtargetperiod = formatTargerPeriod(targetperiod);

				let targetvalue = (item[newtargetperiod] ? item[newtargetperiod] : 0);

				if(targetvalue === 0 && !targetvalueerror) {
					targetvalueerror = true;
					errorArray.push(`Target value should be greater than zero for user - ${item.userid_displayname}`);
				}

				totaltarget += targetvalue;
			});

			if(totaltarget != item.totaltarget)
				errorArray.push(`Total target should be sum of splited targets for user - ${item.userid_displayname}`);
		});

		if(errorArray.length > 0) {
			let apiResponse = commonMethods.apiResult({
				data: {
					message: 'failure',
					error: errorArray
				}
			});
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		}

		resource.userRows.forEach(user => {
			resource.targetItemsHeader.forEach(targetperiod => {
				let newtargetperiod = formatTargerPeriod(targetperiod);
				let targetItemObj = {
					groupname: user.groupname ? user.groupname : null,
					index: user.index > 0 ? user.index : null,
					rootindex: user.rootindex > 0 ? user.rootindex : null,
					userid: user.userid > 0 ? user.userid : null,
					userid_displayname: user.userid > 0 ? user.userid_displayname : null,
					targetperiod: targetperiod,
					targetvalue: user[newtargetperiod]
				};

				let propvalue = `${targetItemObj.userid > 0 ? 'user' : 'group'}_${targetItemObj.userid > 0 ? targetItemObj.userid : targetItemObj.groupname}_${targetperiod}`;

				if(targetObj[propvalue]) {
					targetItemObj.id = targetObj[propvalue].id;
					targetItemObj.parentresource = targetObj[propvalue].parentresource;
					targetItemObj.parentid = targetObj[propvalue].parentid;
					targetItemObj.modified = targetObj[propvalue].modified;
					targetItemObj.modifiedby = targetObj[propvalue].modifiedby;
					targetItemObj.created = targetObj[propvalue].created;
					targetItemObj.createdby = targetObj[propvalue].createdby;
					targetItemObj.isactive = targetObj[propvalue].isactive;
					targetItemObj.isdeleted = targetObj[propvalue].isdeleted;
				}
				resource.performancetargetitems.push(targetItemObj);
			});
		});

		axios({
			method : 'post',
			data : {
				actionverb : actionverb,
				data : resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/performancetargets'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
				if (resparam)
					this.save(actionverb, true);
			});
			if (response.data.message == 'success') {
				if (this.state.createParam)  {
					this.props.history.replace(`/details/performancetargets/${response.data.main.id}`);
				}else{
					if(actionverb == 'Delete')
						this.props.history.replace('/list/performancetargets');
					else
						this.props.initialize(response.data.main);
				}
			}
			this.updateLoaderFlag(false);
		});
	}

	targetValueOnChange(itemkey, firstTime) {
		let targetitems = [...this.props.resource.userRows];

		if(itemkey !== undefined && itemkey !== null) {
			let splitValue = Number((targetitems[itemkey].totaltarget > 0 ? targetitems[itemkey].totaltarget / this.props.resource.targetItemsHeader.length : 0).toFixed(this.props.app.roundOffPrecisionStock));

			this.props.resource.targetItemsHeader.forEach(targetperiod => {
				let newtargetperiod = formatTargerPeriod(targetperiod);

				targetitems[itemkey][newtargetperiod] = splitValue;
			});
		}

		let leafNodes = {};
		let recursiveLeafNodes = (index) => {
			targetitems.forEach((secitem, secitemindex) => {
				if(secitem.rootindex == index) {
					if(secitem.type == 'group')
						recursiveLeafNodes(secitem.index);
					else
						leafNodes[secitemindex] = 1;
				}
			});
		};

		targetitems.forEach((item, itemindex) => {
			if(item.type == 'group') {
				item.totaltarget = 0;
				this.props.resource.targetItemsHeader.forEach(targetperiod => {
					let newtargetperiod = formatTargerPeriod(targetperiod);
					item[newtargetperiod] = 0;
				});
			}

			if(item.type == 'group')
				recursiveLeafNodes(item.index);
			else
				leafNodes[itemindex] = 1;
		});

		let recursivePTValue = (childitem, lastchilditem) => {
			targetitems.forEach(secitem => {
				if(secitem.index == childitem.rootindex && secitem.type == 'group') {
					let totaltarget = 0;
					this.props.resource.targetItemsHeader.forEach(targetperiod => {
						let newtargetperiod = formatTargerPeriod(targetperiod);

						secitem[newtargetperiod] += (lastchilditem[newtargetperiod] > 0 ? lastchilditem[newtargetperiod] : 0);

						totaltarget += secitem[newtargetperiod];
					});

					secitem.totaltarget = totaltarget;
					if(secitem.rootindex > 0)
						recursivePTValue(secitem, lastchilditem);
				}
			});
		}

		targetitems.forEach((item, itemindex) => {
			if(leafNodes[itemindex]) {
				if(item.type == 'group') {
					this.props.resource.targetItemsHeader.forEach(targetperiod => {
						let newtargetperiod = formatTargerPeriod(targetperiod);

						item[newtargetperiod] = 0;
					});
				}
				if(firstTime && item.type == 'user') {
					let totaltarget = 0;
					this.props.resource.targetItemsHeader.forEach(targetperiod => {
						let newtargetperiod = formatTargerPeriod(targetperiod);

						totaltarget += (item[newtargetperiod] ? item[newtargetperiod] : 0);
					});
					item.totaltarget = totaltarget
				}
				if(item.rootindex > 0) {
					targetitems.forEach(secitem => {
						if(secitem.index == item.rootindex && secitem.type == 'group') {
							let totaltarget = 0;
							this.props.resource.targetItemsHeader.forEach(targetperiod => {
								let newtargetperiod = formatTargerPeriod(targetperiod);

								secitem[newtargetperiod] += (item[newtargetperiod] > 0 ? item[newtargetperiod] : 0);

								totaltarget += secitem[newtargetperiod];
							});
							secitem.totaltarget = totaltarget;

							if(secitem.rootindex > 0)
								recursivePTValue(secitem, item);
						}
					});
				}
			}
		});

		this.props.updateFormState(this.props.form, { userRows: targetitems, '_updateCount': (this.props.resource._updateCount ? this.props.resource._updateCount : 0) + 1 });
	}

	listbodyRef(node) {
		if(node) {
			node.addEventListener('scroll', (evt) => {
				this.refs.listheader.scrollLeft = evt.srcElement.scrollLeft;
			})
		}
	}

	openAddItemModal(type){
		let getBody = (closeModal) => {
			return (
				<OpenAddTargetItems />
			)
		};

		this.props.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} app={this.props.app} type={type} resource={this.props.resource} updateFormState={this.props.updateFormState} openModal={this.openModal} callback={this.addItem} closeModal={closeModal} getBody={getBody} />}, className: {content: 'react-modal-custom-class-40', overlay: 'react-modal-overlay-custom-class'},confirmModal: true});
	}

	addItem(secitem, teamParam) {
		let userRows = [...this.props.resource.userRows];

		let index = 0;

		userRows.forEach((item) => {
			if(item.index > index)
				index = item.index;
		});

		index++;

		let level = 0;
		let insertIndex = null;
		let childStart = false;
		if(secitem[0].rootindex) {
			for(var i=0;i<userRows.length;i++) {
				if(secitem[0].rootindex == userRows[i].index) {
					level = userRows[i].level + 1;
					insertIndex = i;
				}
				if(secitem[0].rootindex == userRows[i].rootindex) {
					insertIndex = i;
					childStart = true;
				} else {
					if(childStart && userRows[i].level > level)
						insertIndex = i;
					else
						childStart = false;
				}
			}
		}

		let checkForChild = (teamitem, runningIndexObj, level, rootindex) => {
			teamitem.users.forEach((useritem) => {
				let tempObj = {
					type: 'user',
					groupname: null,
					userid: useritem.id,
					userid_displayname: useritem.displayname,
					index: null,
					rootindex: rootindex,
					level: level + 1
				};

				if(runningIndexObj.insertIndex !== null) {
					userRows.splice(runningIndexObj.insertIndex + 1, 0, tempObj);
					runningIndexObj.insertIndex = runningIndexObj.insertIndex + 1;
				} else {
					userRows.push(tempObj);
				}
			});

			teamitem.children.forEach((teamitem) => {
				runningIndexObj.index = runningIndexObj.index + 1;
				let tempObj = {
					type: 'group',
					groupname: teamitem.groupname,
					userid: null,
					userid_displayname: null,
					index: runningIndexObj.index,
					rootindex: rootindex,
					level: level + 1
				};

				if(runningIndexObj.insertIndex !== null) {
					userRows.splice(runningIndexObj.insertIndex + 1, 0, tempObj);
					runningIndexObj.insertIndex = runningIndexObj.insertIndex + 1;
				} else {
					userRows.push(tempObj);
				}
				checkForChild(teamitem, runningIndexObj, level + 1, runningIndexObj.index);
			});
		};

		secitem.forEach(item => {
			let tempObj = {
				type: item.userid > 0 ? 'user' : 'group',
				groupname: item.userid > 0 ? null : item.groupname,
				userid: item.userid > 0 ? item.userid : null,
				userid_displayname: item.userid > 0 ? item.userid_displayname : null,
				index: item.userid > 0 ? null : index,
				rootindex: item.rootindex,
				level: level
			};

			let itemFound = false;
			for(var i=0;i<userRows.length;i++) {
				let name = tempObj.type == 'user' ? tempObj.userid : tempObj.groupname.toLowerCase();
				let comparename = userRows[i].type == 'user' ? userRows[i].userid : userRows[i].groupname.toLowerCase();

				if(name == comparename) {
					itemFound = true;
					break;
				}
			}

			if(itemFound && tempObj.type == 'group' && !teamParam) {
				let apiResponse = commonMethods.apiResult({
					data: {
						message: 'failure',
						error: ["Group Name / User should be unique"]
					}
				});
				return this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			if(insertIndex !== null) {
				userRows.splice(insertIndex + 1, 0, tempObj);
				insertIndex++;
			} else {
				userRows.push(tempObj);
			}

			if(teamParam) {
				let runningIndexObj = {
					index: index,
					insertIndex: insertIndex
				};
				checkForChild(item, runningIndexObj, level, tempObj.index);
			}
		});

		this.props.updateFormState(this.props.form, { userRows });

		setTimeout(() => {
			this.targetValueOnChange(null);
		}, 0);
	}

	deleteItem (item, itemkey) {
		this.updateLoaderFlag(true);
		let userRows = [...this.props.resource.userRows];

		userRows[itemkey].deleteFlag = true;

		let recursiveDeleteFn = (index) => {
			for (var i = 0; i < userRows.length; i++) {
				if (index == userRows[i].rootindex) {
					userRows[i].deleteFlag = true;
					if (userRows[i].type == 'group')
						recursiveDeleteFn(userRows[i].index);
				}
			}
		}

		if(item.type == 'group')
			recursiveDeleteFn(item.index);

		let newUserRows = userRows.filter((newitem) => !newitem.deleteFlag);

		this.props.updateFormState(this.props.form, { userRows: newUserRows });
		this.updateLoaderFlag(false);
		setTimeout(() => {
			this.targetValueOnChange(null);
		}, 0);
	}

	toggleTableRow(item, itemkey) {
		this.updateLoaderFlag(true);
		let userRows = [...this.props.resource.userRows];

		item.hideChildren = item.hideChildren ? false : true;

		let recursiveDeleteFn = (index) => {
			for (var i = 0; i < userRows.length; i++) {
				if (index == userRows[i].rootindex) {
					userRows[i].hide = item.hideChildren;
					if (userRows[i].type == 'group' && !userRows[i].hideChildren)
						recursiveDeleteFn(userRows[i].index);
				}
			}
		}

		recursiveDeleteFn(item.index);

		this.props.updateFormState(this.props.form, { userRows, '_updateCount': (this.props.resource._updateCount ? this.props.resource._updateCount : 0) + 1 });
		this.updateLoaderFlag(false);
	}

	exportExcel(){
		this.updateLoaderFlag(true);
		let coldefArray = [{
			"name" : "Group",
			"key" : "name",
			"cellClass" : "text-center",
			"width" : 150
		}, {
			"name" : "Total Target",
			"key" : "totaltarget",
			"cellClass" : "text-center",
			"width" : 150
		}];
		this.props.resource.targetItemsHeader.forEach(header=>{
			coldefArray.push({
				"name" : header,
				"key" : header.replace(/-/g, ''),
				"width" : 250
			});
		});

		let exceldata = this.props.resource.userRows;
		exceldata.forEach((data)=>{
			let prefix = '';
			for(let i=0;i<data.level;i++){
				prefix += '-';
			}
			data.name = `${prefix} ${data.groupname || data.userid_displayname}`;
		});
		generateExcel(this.props.resource.name, coldefArray,exceldata, this.props.app, () => {
			this.updateLoaderFlag(false);
		});
	}

	renderTargetDetails() {
		let periodicityprops = {
			options: [...dateFormatterTypeArr],
			onChange : (value) => {this.periodOnChange(value,true)},
			required: true,
			disabled : true
		};

		return (
			<div className="row responsive-form-element">
				<div className="form-group col-md-6">
					<label className="labelclass">Target Name</label>
					<Field name="name" component={InputEle} props={{required: true}} validate={stringNewValidation({required: true})} />
				</div>
				<div className="form-group col-md-6">
					<label className="labelclass">Start Date</label>
					<Field name="targetstart"  component={DateEle} props={{onChange : (value) => {this.periodOnChange(value,true)}}} validate={stringNewValidation({required: true})} />
				</div>
				<div className="form-group col-md-6">
					<label className="labelclass">End Date</label>
					<Field name="targetend" component={DateEle} props={{onChange : (value)=>{this.periodOnChange(value,true)}}}  validate={stringNewValidation({required: true})} />
				</div>
				<div className="form-group col-md-6">
					<label className="labelclass">Select Report</label>
					<Field name="reportid" props={{
						resource : 'reports',
						fields: 'id,name,datasetid,config',
						filter: "reports.type='Performance Target'",
						required : true,
						onChange: (value,valueObj) => {this.reportOnChange(value,valueObj)}
					}} component={autoSelectEle}  validate={[numberNewValidation({required: true})]}/>
				</div>
				<div className="form-group col-md-6">
					<label className="labelclass">Periodicity</label>
					<Field name="periodicity" props={periodicityprops} component={localSelectEle} validate={stringNewValidation({required: true})} />
				</div>
				<div className="form-group col-md-6">
					<label className="labelclass">Allowed Roles</label>
					<Field name="allowedroles" props = {{
						resource: 'roles',
						fields: 'id,name',
						label: 'name',
						required : true
					}}
					component = {autoMultiSelectEle}
					validate = {[multiSelectNewValidation({
						required: true,
						model: 'allowedroles'
					})]} />
				</div>
				
			</div>
		);
	}

	renderTargetDefinition() {
		return (
			<div className="col-md-12 col-sm-12">
				<div className="card borderradius-0 gs-card">
					<div className="card-header borderradius-0 card-header-custom gs-card-header">
						Target Definition
						<button type="button" className="btn btn-sm gs-form-btn-primary marginleft-10 float-right" onClick={() => this.exportExcel()} disabled={!this.props.resource.id || !this.props.resource.userRows || this.props.resource.userRows.length ==0 }><span className="fa fa-file-excel-o"/></button>
						<button type="button" className="btn btn-sm gs-form-btn-primary marginleft-10 float-right" onClick={() => this.openAddItemModal('team')}>Copy Team Structure</button>
						<button type="button" className="btn btn-sm gs-form-btn-primary marginleft-10 float-right" onClick={() => this.openAddItemModal('group')}>Add Group</button>
						<button type="button" className="btn btn-sm gs-form-btn-primary marginleft-10 float-right" onClick={() => this.openAddItemModal('user')}>Add User</button>
					</div>
					{
						(this.props.resource.userRows.length > 0) ?
							<div className="card-body pt-0">
								{this.renderTable()}
							</div> : null
					}
				</div>
				{
					(!this.props.resource.userRows || this.props.resource.userRows.length == 0) ?
						<div style={{width: '100%',position: 'relative',height: '43vh',display: 'flex',flexDirection: 'column',alignItems: 'center',justifyContent: 'center',backgroundColor:'#fff'}}> "Add group or user to define targets"</div> : null
				}
			</div>
		);
	}

	renderTable() {
		let resource = this.props.resource;
		let tbody_div_style = {
			width: `100%`,
			height: `480px`
		};

		return (
			<div className="table-responsive">
				<div className="gs-list-table-header-container" ref="listheader" style={{width: `100%`}}>
					<table className="table gs-table gs-item-table-bordered gs-item-table tablefixed" style={{marginBottom:0}}>
						<thead>
							<tr>
								<th className="text-center" style={{width:'250px'}}>Group</th>
								<th className="text-center" style={{width:'100px'}}>Total Target</th>
								{resource.targetItemsHeader.map((header, headerkey) => <th key={headerkey} className="text-center" style={{width:'100px'}}>{header}</th>
									)}
							</tr>
						</thead>
					</table>
				</div>
				<div className="gs-list-table-body-container gs-scrollbar" style={tbody_div_style} ref={this.listbodyRef}>
					<table className="table table-sm gs-table-sm gs-list-table gs-list-table-body">
						<tbody>
							{resource.userRows.map((item, itemkey) => {
								let trStyle = {};
								if(item.type == 'group')
									trStyle.backgroundColor = '#f8fbfe';
								return (
									<tr key={itemkey} className={`${item.hide ? 'hide' : ''}`} style={trStyle}>
										<td style={{paddingLeft:`${20*item.level}px`, width:'250px',verticalAlign: 'middle'}} className="text-left">{this.renderTitleTd(item, itemkey)}</td>
										<td className="text-center" style={{width:'100px'}}>{this.renderTotalTD(item, itemkey)}</td>
										{this.renderTargetItemsTD(item, itemkey)}
									</tr>
								)
							})
						}
						</tbody>
					</table>
				</div>
			</div>
		);
	}

	renderTitleTd(item, itemkey) {
		return (
			<div>
				{item.type == 'group' ? <div style={{float:"left"}}>
					<a href="javascript:void(0);" style={{paddingRight: '5px'}} onClick={() => this.toggleTableRow(item, itemkey)}><i className={`fa fa-${item.hideChildren ? 'caret-right' : 'caret-down'}`}></i></a>
				</div> : null}
				<div style={{float:"left"}}>
					{item.type == 'group' ? <a href="javascript:void(0);" onClick={() => this.toggleTableRow(item, itemkey)}>{item.groupname}</a> : item.userid_displayname}
				</div>
				<div style={{float:"right"}}>
					<div className="performancetarget-delete-box" onClick={() => this.deleteItem(item, itemkey)}><div className="performancetarget-delete-icon"></div></div>
				</div>
			</div>
		);
	}

	renderTotalTD(item, itemkey) {
		if(item.type == 'group') {
			return (
				<div className="fontWeight gs-compare-green">
					{(item.totaltarget > 0) ? currencyFilter(item.totaltarget, 'qty', this.props.app) : '-'}</div>
			);
		}
		return (
			<Field name={`userRows[${itemkey}].totaltarget`} props={{
				required : true,
				onChange : value => this.targetValueOnChange(itemkey)
			}} component={NumberEle} validate={[numberNewValidation({required: true})]} />
		);
	}

	renderTargetItemsTD(item, itemkey) {
		return this.props.resource.targetItemsHeader.map((header, headerkey) => {
			let newheader = formatTargerPeriod(header);
			if(item.type == 'group') {
				return (
					<td key={headerkey} style={{width:'100px'}} className="text-center fontWeight gs-compare-green">
						<div className="fontWeight gs-compare-green">
							{(item[newheader] > 0) ? currencyFilter(item[newheader], 'qty', this.props.app):'-'}
						</div>
					</td>
				);
			}
			return (
				<td key={headerkey} style={{width:'100px'}} className="text-center">
					<Field name={`userRows[${itemkey}].${newheader}`} props={{
						required : true,
						onChange : value => this.targetValueOnChange()
					}} component={NumberEle} validate={[numberNewValidation({required: true})]} />
				</td>
			);
		});
	}

	renderFooter() {
		return (
			<div className="muted credit text-center sticky-footer actionbtn-bar">
				<div style={{width:'80%', float: 'left'}}>
					<button
						type="button"
						className="btn btn-sm btn-width btn-secondary"
						onClick={() => this.cancel()}>
						<i className="fa fa-times marginright-5"></i>Close
					</button>
					{checkActionVerbAccess(this.props.app, 'performancetargets', 'Save') ?<button
						type="button"
						className="btn btn-sm gs-btn-success btn-width"
						disabled={this.props.invalid}
						onClick={() => this.save('Save')}>
						<i className="fa fa-save"></i>Save
					</button> : null}
				</div>
				{checkActionVerbAccess(this.props.app, 'performancetargets', 'Delete') ? <div className="btn-group dropup" style={{float:'left', width: '20%'}}>
					<button type="button" className="btn btn-sm btn-width gs-btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More <span className="sr-only">Toggle Dropdown</span>
					</button>
					<div className="dropdown-menu gs-morebtn-dropdown-menu">
						<div className={`dropdown-item cursor-ptr gs-text-danger`} onClick={() => this.save('Delete')}><i className={`fa fa-trash-o`}></i> Delete</div>
					</div>
				</div> : null}
			</div>
		);
	}

	render() {
		let { resource } = this.props;

		if(!resource)
			return null;

		let fixed_style_body = {};
		if($(window).outerWidth() > 767) {
			fixed_style_body = {
				marginTop: `${this.props.isModal ? '0' : ((document.getElementsByClassName('affixmenubar').length > 0 ? document.getElementsByClassName('affixmenubar')[0].clientHeight : 0) + 3)}px`,
				marginBottom: `${this.props.isModal ? '0' : (document.getElementsByClassName('actionbtn-bar').length > 0 ? (document.getElementsByClassName('actionbtn-bar')[0].clientHeight - 40) : 40)}px`
			}
		}

		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<Prompt when={!this.state.createParam && this.props.dirty && this.props.anyTouched} message={`Unsaved Changes`} />
					<div className="row">
						<div className="col-sm-12 affixmenubar col-md-12 col-lg-9">
							<div className="row">
								<div className="col-md-7 col-sm-12">
									<h6 className="margintop-15 semi-bold-custom">
										<a className="affixanchor float-left marginright-10" onClick={() => this.props.history.push('/list/performancetargets')}><span className="fa fa-arrow-left "></span></a>
										<div className="gs-uppercase float-left" title={resource.name} style={{maxWidth: '400px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis'}}>Performance Target - {resource.name}</div>
									</h6>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-12 col-md-12 col-lg-9">
							<div className="row"  style={fixed_style_body}>
								<div className="col-md-12 col-sm-12">
									<div className="card borderradius-0 gs-card">
										<div className="card-header borderradius-0 card-header-custom gs-card-header">Target Details</div>
										<div className="card-body">
											{this.renderTargetDetails()}
										</div>
									</div>
								</div>
								{this.props.resource.targetItemsHeader.length > 0 ? this.renderTargetDefinition() : null}
							</div>
						</div>
					</div>
					<div className="row" style={{paddingTop: '40px'}}>
						<div className="col-md-12 col-lg-9">
							{this.renderFooter()}
						</div>
					</div>
				</form>
			</div>
		);
	}
}

class OpenAddTargetItems extends Component {
	constructor(props) {
		super(props);
		this.state = {
			groupname : '',
			userid : null,
			parentgroup : '',
			loaderflag: false,
			type : {
				group : {
					title : "Group"
				},
				user : {
					title : "User"
				},
				team : {
					title : "Team"
				}
			}
		}

		this.addItem = this.addItem.bind(this);
		this.valueOnChange = this.valueOnChange.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
	}

	componentWillMount () {
		let groupArray = [];

		this.props.resource.userRows.forEach((item,index)=>{
			if(item.type == 'group')
				groupArray.push({
					index: item.index,
					groupname: item.groupname
				});
		});

		this.setState({ groupArray });
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	valueOnChange(field, val, valobj) {
		let tempstate = this.state;
		tempstate[field] = val;
		if(field == 'userid' && valobj) {
			tempstate['userid_displayname'] = valobj;
		}
		if(field == 'parentgroup' && valobj){
			tempstate['rootindex'] = valobj.index;
			tempstate['level'] = valobj.level;
			tempstate['displaylevel'] = valobj.displaylevel;
		}
		this.setState(tempstate);
	}

	addItem() {
		if(['user', 'group'].includes(this.props.type)) {
			let userArray = this.state.userid && this.state.userid.length > 0 ? [...this.state.userid_displayname] : [{groupname: this.state.groupname}];

			userArray.forEach((item) => {
				item.rootindex = this.state.rootindex;
				item.userid = item.id > 0 ? item.id : null;
				item.userid_displayname = item.userid > 0 ? item.displayname : null
			});

			this.props.callback(userArray);
			return this.props.closeModal();
		}

		this.updateLoaderFlag(true);
		axios.get(`/api/users?pagelength=10000&includeinactive=true&field=id,displayname,salesteamlevels,isactive`).then((response)=> {
			if (response.data.message == 'success') {
				this.getTeamStructureData(response.data.main);
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getTeamStructureData(userArray) {
		axios.get(`/api/teamstructure?field=id,name,type,parentid,isparent&pagelength=10000`).then((response)=> {
			if (response.data.message == 'success') {
				response.data.main.sort((a,b) => a.id > b.id);

				let teamArray = response.data.main.map((team) => {
					return {
						id : team.id,
						groupname: team.name,
						parentid : team.parentid,
						users : [],
						children : []
					}
				});

				teamArray.forEach((team) => {
					userArray.forEach((user) => {
						if(user.salesteamlevels && user.salesteamlevels.length > 0 && user.salesteamlevels.indexOf(team.id) >= 0)
							team.users.push(user);
					});
				});

				this.generateTreeArray(teamArray);

			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	generateTreeArray(teamArray) {
		let treeArray = [];
		let teamObj = {};
		teamArray.forEach((team) => {
			teamObj[team.id] = team;
		});

		teamArray.forEach((team) => {
			if(team.id == this.state.teamid)
				treeArray.push(team);
			else if(team.parentid > 0)
				teamObj[team.parentid].children.push(team);
		});

		treeArray[0].rootindex = this.state.rootindex;

		this.props.callback(treeArray, true);
		this.props.closeModal();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Choose {this.state.type[this.props.type].title}</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row responsive-form-element">
						{(this.props.type == 'group') ? <div className="form-group col-sm-12 col-md-12">
							<label className="labelclass">Group Name</label>
							<input className={`form-control ${!this.state.groupname ? 'errorinput' : ''}`} type="text" onChange={(e) => this.valueOnChange('groupname', e.target.value)}  value={this.state.groupname} required />
						</div> :null}
						{(this.props.type == 'user') ? <div className="form-group col-sm-12 col-md-12">
							<label className="labelclass">User</label>
							<AutoMultiSelect
								className={!this.state.userid || this.state.userid.length == 0 ? 'errorinput' : ''}
								resource = {'users'}
								fields = {'id,displayname'}
								label = {'displayname'}
								value = {this.state.userid}
								onChange = {(val, valObj)=>{this.valueOnChange('userid', val, valObj)}}
								placeholder = {'Select User'}
								required = {true} />
						</div> : null}
						{(this.props.type == 'team') ? <div className="form-group col-sm-12 col-md-12">
							<label className="labelclass">Team</label>
							<AutoSelect
								className={!this.state.teamid ? 'errorinput' : ''}
								resource = {'teamstructure'}
								fields = {'id,fullname'}
								label = {'fullname'}
								value = {this.state.teamid}
								onChange = {(val, valObj) => this.valueOnChange('teamid', val, valObj)}
								placeholder = {'Select Team'}
								required = {true} />
						</div> : null}
						<div className="form-group col-sm-12 col-md-12">
							<label className="labelclass">Parent Group</label>
							<LocalSelect
								options={this.state.groupArray}
								value={this.state.rootindex}
								label='groupname' 
								valuename='index'
								onChange = {(val, valObj) => this.valueOnChange('parentgroup', val, valObj)} />
						</div>
 					</div>
				</div>
 				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 text-center">
							<div className="muted credit text-center">
								<button className="btn btn-sm btn-secondary btn-width" style={{backgroundColor:'white',color:'gray',borderColor:'gray'}} onClick={() => this.props.closeModal()}>CANCEL</button>
								<button className="btn btn-sm gs-btn-success btn-width" disabled={(!this.state.userid || this.state.userid.length == 0) && !this.state.groupname && !this.state.teamid} onClick={()=> {this.addItem()}}>OK</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}



PerformanceTarget = connect(
	(state, props) => {
		let formName = `Performance Target-${props.match.params.id > 0 ? props.match.params.id : 'create'}`;
		return {
			app : state.app,
			form : formName,
			touchOnChange: true,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState, updateAppState }
)(reduxForm()(PerformanceTarget));

export default PerformanceTarget;