import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { Link } from 'react-router-dom';
import { v1 as uuidv1 } from 'uuid';
import moment from 'moment';

import { updateFormState, updateAppState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { LocalSelect } from '../components/utilcomponents';
import {stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation } from '../utils/utils';

export default connect((state, props) => {
	return {
		app: state.app
	}
}, {updateAppState})(class Settings extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.initialize = this.initialize.bind(this);
		this.resourceOnChange = this.resourceOnChange.bind(this);
		this.rolesonload = this.rolesonload.bind(this);
		this.callBackResource = this.callBackResource.bind(this);
		this.validation = this.validation.bind(this);
		this.save = this.save.bind(this);
		this.checkCRMResourceValidation = this.checkCRMResourceValidation.bind(this);
	}

	componentWillMount() {
		this.initialize();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	checkCRMResourceValidation(resource) {
		if(!this.props.app.feature.runInCRMMode)
			return true;

		return resource.isCRMResource ? true : false;
	}

	initialize(item) {
		let resourceObj = {
			...this.props.app.myResources
		};

		let groupArray = [],
			subgroupArray = {},
			resourceArray = {},
			resourceSecArray = [];

		for (let prop in resourceObj) {
			if(!resourceObj[prop].hideInPermission && this.checkFeatureAccess(resourceObj[prop]) && this.checkCRMResourceValidation(resourceObj[prop])) {

				let group = resourceObj[prop].group ? resourceObj[prop].group : 'Unidentified';

				let subgroup = resourceObj[prop].subgroup ? resourceObj[prop].subgroup : 'Unidentified';

				if(groupArray.indexOf(group) == -1) {
					subgroupArray[group] = [];
					groupArray.push(group);
					resourceArray[group] = {};
				}
				if(subgroupArray[group].indexOf(subgroup) == -1)
					subgroupArray[group].push(subgroup);

				if(!resourceArray[group][subgroup])
					resourceArray[group][subgroup] = [];

				resourceArray[group][subgroup].push({
					group  : group,
					subgroup  : subgroup,
					resourceName : resourceObj[prop].resourceName,
					displayName : resourceObj[prop].displayName
				});

				resourceSecArray.push({
					group  : group,
					subgroup  : subgroup,
					resourceName : resourceObj[prop].resourceName,
					displayName : resourceObj[prop].displayName
				});
			}
		}

		for (let prop in resourceArray) {
			for(let prop1 in resourceArray[prop]) {
				resourceArray[prop][prop1].sort(
					(a, b) => {
						return (a.displayName > b.displayName) ? 1 : (a.displayName < b.displayName) ? -1 : 0;
					}
				)
			}
		}

		resourceSecArray.sort(
			(a, b) => {
				return (a.displayName > b.displayName) ? 1 : (a.displayName < b.displayName) ? -1 : 0;
			}
		);

		groupArray = groupArray.sort();
		this.setState({
			resourceObj,
			groupArray,
			subgroupArray,
			resourceArray,
			resourceSecArray
		});

		this.rolesonload(item);
	}

	rolesonload(item) {

		let rolesObj = {},
			rolesArray = [];

		axios.get(`/api/roles?field=id,name,description`).then((response) => {
			if(response.data.message == 'success') {
				response.data.main.forEach((item) => {
					rolesObj[item.id] = item;
					rolesArray.push(item)
				})

				rolesArray.sort(
					(a, b) => {
						return (a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0;
					}
				);

				this.setState({
					rolesObj,
					rolesArray
				});
				if(item)
					this.callBackResource(item.resourcename);
				else
					this.updateLoaderFlag(false);

			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	callBackResource(value) {
		this.setState({
			resourcename: this.state.resourceObj[value] ? this.state.resourceObj[value].resourceName : '',
			group: this.state.resourceObj[value] ? this.state.resourceObj[value].group : '',
			subgroup: this.state.resourceObj[value] ? this.state.resourceObj[value].subgroup : '',
		});

		this.resourceOnChange();
	}

	resourceOnChange() {
		let actionverb = [],
		rolemapping = {},
		ignoreReadPermissionDiv = false;

		let prop = this.state.resourcename;
		let { resourceObj } = this.state;

		if (resourceObj[prop]) {
			ignoreReadPermissionDiv = resourceObj[prop].ignoreReadPermission ? resourceObj[prop].ignoreReadPermission : false;

			for (let val in resourceObj[prop].authorization)
				actionverb.push(val);

			for (let retprop in this.state.rolesObj)
				rolemapping[retprop] = {};

			for (let retprop in resourceObj[prop].authorization) {
				resourceObj[prop].authorization[retprop].forEach((roleid) => {
					if(rolemapping[roleid])
						rolemapping[roleid][retprop] = true;
				});
			}
		}

		this.setState({
			actionverb,
			rolemapping,
			ignoreReadPermissionDiv
		});

		this.updateLoaderFlag(false);
	}

	checkboxOnChange(value, roleid, actionverb) {
		this.state.rolemapping[roleid][actionverb] = value;
		if(actionverb == 'Menu' && value){
			this.state.rolemapping[roleid]['Read'] = value;
		}
		this.setState({
			rolemapping: this.state.rolemapping
		});
	}

	validation () {
		this.updateLoaderFlag(true);

		let keyValueError = '',
			errorFoundRoleMapping = false,
			tempActionverb = {};

		for (let i = 0; i < this.state.actionverb.length; i++)
			tempActionverb[this.state.actionverb[i]] = [];

		for (let prop in tempActionverb) {
			for (let name in this.state.rolemapping) {
				if (this.state.rolemapping[name][prop])
					tempActionverb[prop].push(name)
			}
		}

		for (let prop in tempActionverb) {
			if (tempActionverb[prop].length == 0) {
				keyValueError = prop;
				errorFoundRoleMapping = true;
				break;
			}
		}

		this.save(tempActionverb);
	}

	save(tempActionverb) {
		let tempData = [{
				resourcename : this.state.resourcename,
				modified : this.props.app.myResources[this.state.resourcename].modified,
				actionverbauthorization : tempActionverb
			}
		];

		let childArray = [];

		let combineArrays = (array1,array2) => {
			for(var icom=0;icom<array2.length;icom++) {
				if(array1.indexOf(array2[icom]) == -1)
					array1.push(array2[icom]);
			}
		}

		for(let prop in this.props.app.myResources) {

			let tempResObj = this.props.app.myResources[prop];

			if(tempResObj && tempResObj.dependency && tempResObj.dependency.indexOf(this.state.resourcename) >= 0) {
				childArray.push(prop);

				let tempObj = {
					resourcename : prop,
					modified : this.props.app.myResources[prop].modified,
					actionverbauthorization : JSON.parse(JSON.stringify(tempResObj.authorization))
				};

				let temppropFound = false;

				if(tempData[0].actionverbauthorization['Read']) {
					temppropFound = true;
				} else {
					for(let i=0; i<tempResObj.dependency.length; i++) {
						if(this.props.app.myResources[tempResObj.dependency[i]] && this.props.app.myResources[tempResObj.dependency[i]].authorization['Read'])
							temppropFound = true;
					}
				}

				if(temppropFound) {
					tempObj.actionverbauthorization['Read'] = tempData[0].actionverbauthorization['Read'] ? JSON.parse(JSON.stringify(tempData[0].actionverbauthorization['Read'])) : [];

					for(var i=0;i<tempResObj.dependency.length;i++) {
						if(tempResObj.dependency[i] != this.state.resourcename && this.props.app.myResources[tempResObj.dependency[i]] && this.props.app.myResources[tempResObj.dependency[i]].authorization['Read']) {
							combineArrays(tempObj.actionverbauthorization['Read'], this.props.app.myResources[tempResObj.dependency[i]].authorization['Read']);
						}
					}
				}
				if(this.props.app.myResources[this.state.resourcename].type == 'lookup') {
					for(var actionverbProp in tempObj.actionverbauthorization)
						tempObj.actionverbauthorization[actionverbProp] = tempData[0].actionverbauthorization[actionverbProp] ? JSON.parse(JSON.stringify(tempData[0].actionverbauthorization[actionverbProp])) : [];

					for (var i = 0; i < tempResObj.dependency.length; i++) {
						if (tempResObj.dependency[i] != this.state.resourcename && this.props.app.myResources[tempResObj.dependency[i]]) {
							for(var actionverbProp in this.props.app.myResources[tempResObj.dependency[i]].authorization) {
								if(tempObj.actionverbauthorization[actionverbProp] && this.props.app.myResources[tempResObj.dependency[i]].authorization[actionverbProp]) {
									console.log(tempObj.actionverbauthorization[actionverbProp], this.props.app.myResources[tempResObj.dependency[i]].authorization[actionverbProp]);
									combineArrays(tempObj.actionverbauthorization[actionverbProp], this.props.app.myResources[tempResObj.dependency[i]].authorization[actionverbProp]);
									console.log(tempObj.actionverbauthorization[actionverbProp]);
								}
							}
						}
					}
				}
				tempData.push(tempObj);
			}
		}

		axios({
			method : 'post',
			data : tempData,
			url : '/api/common/methods/saveResourceJson'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

			if(response.data.message == 'success') {
				let myResources = {...this.props.app.myResources};
				for(var i=0;i<response.data.main.length;i++)
					myResources[response.data.main[i].resourcename] = response.data.main[i].newjson;

				this.props.updateAppState('myResources', myResources);

				setTimeout(() => {
					this.initialize(response.data.main[0]);	
				}, 0);
			} else {
				this.updateLoaderFlag(false);
			}
		});
	}

	checkFeatureAccess (props) {
		if (props.feature != null && props.feature != '' && props.feature != 'undefined') {
			if(props.feature.indexOf(',') == -1)
				return this.props.app.feature[props.feature];
			else {
				let tempFeatureArray = props.feature.split(',');

				for(let i = 0;i < tempFeatureArray.length; i++) {
					if(this.props.app.feature[tempFeatureArray[i]])
						return true;
				}

				return false;
			}
		}

		return true;
	}

	render() {
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="col-sm-12 col-md-12" style={{position: 'fixed', height: '45px', backgroundColor: '#FAFAFA',  zIndex: 5}}>
						<div className="row">
							<div className="col-md-6 col-sm-12 paddingleft-md-30">
								<h6 className="margintop-15 semi-bold-custom">Permissions</h6>
							</div>
						</div>
					</div>
				</div>
				<form>
					<div className="row">
						<div className="col-sm-12 col-md-12" style={{marginTop: `48px`}}>
							<div className="card marginbottom-15 borderradius-0">
								<h6 className="card-header borderradius-0 card-header-custom gs-card-header">Choose Resource</h6>
								<div className="card-body">
									<div className="row responsive-form-element">
										<div className="form-group col-sm-12 col-md-4">
											<label className="labelclass">Group</label>
											<LocalSelect options={this.state.groupArray} value={this.state.group} onChange={(group) => this.setState({group, subgroup : null, resourcename : null})} className={`${!this.state.group ? 'errorinput' : ''}`} required={true} />
										</div>
										<div className="form-group col-sm-12 col-md-4">
											<label className="labelclass">Sub Group</label>
											<LocalSelect options={this.state.subgroupArray[this.state.group]} value={this.state.subgroup} onChange={(subgroup) => this.setState({subgroup, resourcename : null})} className={`${!this.state.subgroup ? 'errorinput' : ''}`} required={true} />
										</div>
										<div className="form-group col-sm-12 col-md-4">
											<label className="labelclass">Resource</label>
											<LocalSelect options={this.state.resourceArray[this.state.group] ? this.state.resourceArray[this.state.group][this.state.subgroup] : [] } label='displayName' valuename='resourceName' value={this.state.resourcename} onChange={(resourcename) => this.setState({resourcename}, () => this.resourceOnChange())} className={`${!this.state.resourcename ? 'errorinput' : ''}`} required={true} />
										</div>
										<div className="form-group col-sm-12 col-md-6 offset-md-6">
											<label className="labelclass">OR</label>
										</div>
										<div className="form-group col-sm-12 col-md-4 offset-md-4">
											<label className="labelclass">Resource</label>
											<LocalSelect options={this.state.resourceSecArray} label='displayName' valuename='resourceName' value={this.state.resourcename} onChange={(resourcename) => this.setState({resourcename}, () => this.callBackResource(resourcename))} className={`${!this.state.resourcename ? 'errorinput' : ''}`} required={true} />
										</div>
										{this.state.ignoreReadPermissionDiv ? <div className="col-md-12">
											<div className="alert alert-info"> Note: This Resource has default 'Read' access to everyone. </div>
										</div> : null}
									</div>
								</div>
							</div>
						</div>
						{this.state.rolemapping ? <div className="col-sm-12 col-md-12">
							<div className="card marginbottom-15 borderradius-0">
								<h6 className="card-header borderradius-0 card-header-custom gs-card-header">Role Mapping</h6>
								<div className="card-body">
									<div className="row">
										<div className="col-md-12">
											<div className="table-responsive">
											<table className="table table-bordered table-hover gs-list-table permission-table" style={{marginBottom:'0px'}}>
												<thead style={{backgroundColor: '#ddd'}}>
													<tr>
														<th></th>
														{ this.state.actionverb.map((verb, index) => {
															if(!this.state.ignoreReadPermissionDiv || (this.state.ignoreReadPermissionDiv && verb != 'Read')) {
																return <th key={index}>{verb}</th>
															}
														})}
													</tr>
												</thead>
												<tbody style={{height: '400px'}}>
													{ this.state.rolesArray.map((item, itemIndex) => {
														return (
															<tr key={itemIndex}>
																<td style={{textAlign: 'center', verticalAlign: 'middle'}}>{item.name}</td>
																{ this.state.actionverb.map((acverb, acindex) => {
																	if(!this.state.ignoreReadPermissionDiv || (this.state.ignoreReadPermissionDiv && acverb != 'Read')) {
																		return <td key={acindex} style={{textAlign: 'center', verticalAlign: 'middle'}} ><input type="checkbox" checked={this.state.rolemapping[item.id][acverb] ? true : false} onChange={(e) => this.checkboxOnChange(e.target.checked, item.id, acverb)} disabled={(this.state.rolemapping[item.id]['Menu'] && acverb == 'Read') ? true : false}/></td>
																	}
																})}
															</tr>
														)
													})}
												</tbody>
											</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> : null}
					</div>
					<div className="row" style={{paddingTop: '40px'}}>
						<div className="col-md-12">
							<div className="muted credit text-center sticky-footer" style={{width: '100%'}}>
								<button type="button" className="btn btn-secondary btn-sm btn-width" onClick={() => this.props.history.push('/')}><span className="fa fa-times marginright-5" />Close</button> <button type="button" className="btn gs-btn-success btn-sm btn-width" disabled={!this.state.resourcename} onClick={this.validation}><span className="fa fa-save marginright-5" />Save</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		);
	}
});
