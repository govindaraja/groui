import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { commonMethods, modalService } from '../utils/services';
import { DateEle, localSelectEle, autoSelectEle, selectAsyncEle, checkboxEle, textareaEle, NumberEle, SpanEle, InputEle } from '../components/formelements';
import { currencyFilter, dateFilter, datetimeFilter, timeFilter } from '../utils/filter';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation, checkActionVerbAccess, requiredNewValidation } from '../utils/utils';
import { ReportNumberEle, ReportBooleanEle, ReportDateEle, ReportLocalSelectEle, ReportAutoSelectEle, ReportAutoMultiSelectEle } from '../reportbuilder/reportformelements';

const IndexParameters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

export default class ReportFilterComponent extends Component {
	constructor (props) {
		super (props);

		this.state = {
			loaderflag: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.addRow = this.addRow.bind(this);
		this.deleteRow = this.deleteRow.bind(this);
		this.renderConditions = this.renderConditions.bind(this);
		this.getFieldType = this.getFieldType.bind(this);
		this.renderOperators = this.renderOperators.bind(this);
	};

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	};

	addRow() {
		let condition = [...this.props.resource.dataaccessitems];

		if (condition.length >= 26) {
			this.updateLoaderFlag(false);

			return this.props.openModal(modalService.infoMethod({
				header: 'Error',
				body: 'Number of Access Conditions should not exceed 26',
				btnArray: ['Ok']
			}));
		}

		this.props.fields.push({
			fieldandvalue: {
				field: null,
				value: null
			}
		});

		this.props.updateFormState(this.props.form, {
			conditionorder: IndexParameters.slice(0, condition.length + 1).join(' AND ')
		});
	};

	deleteRow(index) {
		let condition = [...this.props.resource.dataaccessitems];

		this.props.fields.remove(index);

		this.props.updateFormState(this.props.form, {
			conditionorder: IndexParameters.slice(0, condition.length - 1).join(' AND ')
		});
	};

	getFieldType (item, itemval, itemstr, index) {
		let { resource, filterObj, app } = this.props;

		let fieldValue = null;

		if (filterObj[itemval].isForeignKey && filterObj[itemval].foreignKeyOptions.resource == 'companymaster')
			fieldValue = 'Current Company'

		this.props.updateFormState(this.props.form, {
			[`${itemstr}.fieldandvalue`]: {
				field: itemval,
				value: fieldValue
			}
		});
	};

	renderConditions () {
		let resource = this.props.resource;

		return (
			<div className="form-group col-md-12">
				<table className="table gs-table gs-item-table-bordered">
					<thead>
						<tr>
							<th style = {{width: '15%'}}>Index</th>
							<th style = {{width: '30%'}}>Field</th>
							<th style = {{width: '45%'}}>Value</th>
							<th className = "text-center" style = {{width: '10%'}}></th>
						</tr>
					</thead>
					<tbody>
						{this.props.fields.map((member, index) => {
							let tempchilditem = this.props.selector(this.props.fullstate, member);
							return (
								<tr key={index}>
									<td>
										{IndexParameters[index].toUpperCase()}
									</td>
									<td>
										<Field
											name = {`${member}.fieldandvalue.field`}
											props = {{
												valuename: 'field',
												label: 'displayName',
												required: true,
												onChange: (value) => this.getFieldType(tempchilditem, value, member, index),
												options: this.props.filterArray
											}}
											component = {localSelectEle}
											validate = {[stringNewValidation({
												required: true
											})]}
										/>
									</td>
									<td>
										{ tempchilditem.fieldandvalue && tempchilditem.fieldandvalue.field ? this.renderOperators (tempchilditem.fieldandvalue.field, index, `dataaccessitems[${index}]`) : null}
									</td>
									<td className = "text-center">
										<button
											type = "button"
											onClick = {() => {this.deleteRow(index)}}
											className="btn btn-sm gs-form-btn-danger marginleft-3"
											rel = "tooltip"
											title = "Delete Condition">
											<span className = "fa fa-trash-o"></span>
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		);
	};

	renderOperators (itemName, index, itemstr) {
		console.log(this.props, itemName)
		let { resource, filterObj, filterArray } = this.props,
			renderType = '',
			fieldProps = {
				required: true,
				placeholder: filterObj[itemName].displayName
			},
			validateProps = {
				required: true,
				title : filterObj[itemName].displayName
			};

		let tempvalidate = [requiredNewValidation(validateProps)];

		if (filterObj[itemName].type == 'string' && filterObj[itemName].group == 'localselect') {
			renderType = ReportLocalSelectEle;
			fieldProps.options = filterObj[itemName].localOptions;
		}

		if (filterObj[itemName].type == 'string' && filterObj[itemName].group == 'resource') {
			renderType = ReportLocalSelectEle;
			fieldProps.options = this.props.resourceArray;
		}

		if (filterObj[itemName].type == 'boolean') {
			renderType = ReportBooleanEle;
		}

		if (filterObj[itemName].type == 'date') {
			renderType = ReportDateEle;
		}

		if (filterObj[itemName].type == 'integer' && !filterObj[itemName].isForeignKey) {
			renderType = ReportNumberEle;
		}

		if (filterObj[itemName].isForeignKey) {
			renderType = ReportAutoSelectEle;

			fieldProps.resource = filterObj[itemName].foreignKeyOptions.resource;
			fieldProps.fields = `id,${filterObj[itemName].foreignKeyOptions.mainField.split('.')[1]}`;
			fieldProps.label = `${filterObj[itemName].foreignKeyOptions.mainField.split('.')[1]}`;

			if (filterObj[itemName].foreignKeyOptions.resource == 'numberingseriesmaster' && filterObj[itemName].numberingseries_resource)
				fieldProps.filter = `numberingseriesmaster.resource = '${filterObj[itemName].numberingseries_resource}'`;
		}

		if (filterObj[itemName].isArrayForeignKey) {
			renderType = ReportAutoMultiSelectEle;
			fieldProps.resource = filterObj[itemName].foreignKeyArrayOptions.resource;
			fieldProps.fields = `id,${filterObj[itemName].foreignKeyArrayOptions.mainField}`;
			fieldProps.label = `${filterObj[itemName].foreignKeyArrayOptions.mainField}`;
		}

		return (
			<Field
				name = {`${itemstr}.fieldandvalue.value`}
				props = {fieldProps}
				component = {renderType}
				validate = {tempvalidate}
			/>
		);
	};

	render() {
		const { app, resource} = this.props;

		if (!resource || !this.props.filterObj || this.props.filterArray.length == 0)
			return null;

		return (
			<div className = "col-md-12">
				{this.renderConditions()}
				<div className = "row">
					<div className = "form-group col-md-6 col-sm-12 col-xs-12">
						<button
							type = "button"
							onClick = {() => {this.addRow()}}
							rel = "tooltip"
							title = "Add Condition"
							className = "btn btn-sm btn-gs btn-outline-primary filter-button">
							<span className = "fa fa-plus" ></span> Add Condition
						</button>
					</div>
					<div className = "form-group col-md-6 col-sm-12 col-xs-12">
						<div className = "row">
							<div className = "form-group col-md-6 col-sm-6 col-xs-12 text-right">
								<label>Condition Order</label>
							</div>
							<div className = "form-group col-md-6 col-sm-6 col-xs-12">
								<Field
									name = {'conditionorder'}
									props = {{
										required: true
									}}
									component = {InputEle}
									validate = {[stringNewValidation({
										required: true,
										title : 'Condition Order'
									})]}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	};
}