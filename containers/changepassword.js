import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { checkActionVerbAccess } from '../utils/utils';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';

class ChangePasswordForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			users: {},
			errorArray: [],
			loaderflag : true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getUserPassword = this.getUserPassword.bind(this);
		this.comparePassword = this.comparePassword.bind(this);
		this.encryptPassword = this.encryptPassword.bind(this);
		this.updatePassword = this.updatePassword.bind(this);
		this.cancel = this.cancel.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();	
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	onLoad() {
		let users = {};
		axios.get(`/api/initialize/initialize`).then((response)=> {
			users.email = response.data.user.email;
			users.id = response.data.user.id;
			this.setState({ users });
			this.updateLoaderFlag(false);
		});
	}

	getUserPassword () {
		this.updateLoaderFlag(false);
		let errorArray = [];
		let { users } = this.state;
		if (this.state.users.newpassword == this.state.users.confpassword && this.state.users.newpassword != null) {
			axios.get(`/api/common/methods/getUserPassword?id=${this.state.users.id}`).then((response) => {
				users.decryptpassword = response.data[0].password;
				users.modified = response.data[0].modified;
				this.setState({ users }, () => {
					this.comparePassword();
				});
			});
		} else {
			errorArray.push("New password not match with confirm password");
			this.state({ errorArray }, () => {
				this.comparePassword();
			});
		}
	}

	comparePassword (decryptpass) {
		let { errorArray } = this.state;
		if (this.state.errorArray.length == 0) {
			axios.get(`/api/common/methods/checkPassword?password=${this.state.users.decryptpassword}&oldpassword=${encodeURIComponent(this.state.users.oldpassword)}`).then((response) => {
				if (response.data.result) {
					this.encryptPassword();
				} else {
					errorArray.push("Old password not match with your password");
					this.state({ errorArray }, () => {
						this.encryptPassword();
					});
				}
			});
		} else
			this.encryptPassword();
	}

	encryptPassword () {
		let { users } = this.state;
		axios.get(`/api/common/methods/encryptpassword?newpassword=${encodeURIComponent(this.state.users.newpassword)}`).then((response) => {
			users.password = response.data.password;
			if (this.state.errorArray.length > 0) {
				this.updateLoaderFlag(false);
				let response = {
					data : {
						message : 'failure',
						error : this.state.errorArray
					}
				}
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			} else {
				this.setState({ users }, () => {
					this.updatePassword();
				});
			}
		});
	}

	updatePassword () {
		let tempData = {
			actionverb : 'Change Password',
			data : this.state.users
		};

		axios({
			method : 'post',
			data : tempData,
			url : '/api/users'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			if (response.data.message == 'success')
				this.props.history.push('/dashboard');

			this.updateLoaderFlag(false);
		});
	}

	inputOnChange(evt) {
		let { users } = this.state;
		users[evt.target.name] = evt.target.value;
		this.setState({ users });
	}

	cancel () {
		this.props.history.push('/dashboard');
	}

	render() {
		let accessClass = checkActionVerbAccess(this.props.app, 'users', 'Change Password') ? '' : 'hide';
		let { users } = this.state;
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row form-element-style" style={{marginTop: `${((document.getElementsByClassName('navbar').length > 0 ? document.getElementsByClassName('navbar')[0].clientHeight : 0) + 10)}px`}}>
					<div className="col-md-6 offset-md-3">
						<div className="card borderradius-0 gs-card">
							<h6 className="card-header borderradius-0 card-header-custom gs-card-header">Change Password</h6>
							<div className="card-body">
								<div className="row">
									<div className="col-md-3 form-group">
										<label className="labelclass">User Name</label>
									</div>
									<div className="col-md-9 form-group">
										<span className="form-control">{users.email}</span>
									</div>
								</div>
								<div className="row">
									<div className="col-md-3 form-group">
										<label className="labelclass">Old password</label>
									</div>
									<div className="col-md-9 form-group">
										<input type="password" className={`form-control ${!this.state.users.oldpassword ? 'errorinput' : ''}`} name="oldpassword" value={users.oldpassword} onChange={(evt) => this.inputOnChange(evt)} autoComplete="off" required />
									</div>
								</div>
								<div className="row">
									<div className="col-md-3 form-group">
										<label className="labelclass">New password</label>
									</div>
									<div className="col-md-9 form-group">
										<input type="password" className={`form-control ${!this.state.users.newpassword ? 'errorinput' : ''}`} name="newpassword" value={users.newpassword} onChange={(evt) => this.inputOnChange(evt)} autoComplete="off" required />
									</div>
								</div>
								<div className="row">
									<div className="col-md-3 form-group">
										<label className="labelclass">Confirm password</label>
									</div>
									<div className="col-md-9 form-group">
										<input type="password" className={`form-control ${!this.state.users.confpassword ? 'errorinput' : ''}`} name="confpassword" value={users.confpassword} onChange={(evt) => this.inputOnChange(evt)} autoComplete="off" required />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-12">
						<div className="muted credit text-center sticky-footer actionbtn-bar" style={{width: '100%'}}>
							<button type="button" className="btn btn-sm btn-width btn-secondary" onClick={this.cancel}><i className="fa fa-times"></i>Close</button>
							<button type="button" className={`btn btn-sm btn-width gs-btn-success ${accessClass}`} onClick={this.getUserPassword} disabled={!this.state.users.oldpassword || !this.state.users.newpassword || !this.state.users.confpassword} ><i className="fa fa-save"></i>Change Password</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

ChangePasswordForm = connect((state) =>{
	return {app: state.app}
}) (ChangePasswordForm);

export default ChangePasswordForm;
