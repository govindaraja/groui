import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

import { updateFormState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { XLSXReader } from '../utils/excelutils';

class ImportCustomer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : true,
			foreignKeyobj: {},
			isValid: false
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getUsers = this.getUsers.bind(this);
		this.getRecAccounts = this.getRecAccounts.bind(this);
		this.getPayAccounts = this.getPayAccounts.bind(this);
		this.getTerritories = this.getTerritories.bind(this);
		this.getPriceLists = this.getPriceLists.bind(this);
		this.getCustomerGroups = this.getCustomerGroups.bind(this);
		this.getCustomers = this.getCustomers.bind(this);
		this.validateFn = this.validateFn.bind(this);
		this.sampleFn = this.sampleFn.bind(this);
		this.fileOnChange = this.fileOnChange.bind(this);
		this.import = this.import.bind(this);
		this.importtran = this.importtran.bind(this);
	}

	componentWillMount() {
		let customFields = [];
		for(var prop in this.props.app.myResources.partners.fields) {
			if(prop.indexOf('zzz') == 0) {
				var tempObj = JSON.parse(JSON.stringify(this.props.app.myResources.partners.fields[prop]));
				tempObj.fieldName = prop;
				customFields.push(tempObj);
			}
		}
		this.setState({
			customFields
		});
		this.getUsers({});	
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	getUsers(foreignKeyobj) {
		axios.get(`/api/users?field=id,email`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.users = {};
				response.data.main.forEach((item) => {
					foreignKeyobj.users[item.email] = item
				});
				this.getRecAccounts(foreignKeyobj);
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getRecAccounts(foreignKeyobj) {
		axios.get(`/api/accounts?field=id,name&filtercondition=accounts.type='Asset' and accounts.isledger`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.recaccounts = {};
				response.data.main.forEach((item) => {
					foreignKeyobj.recaccounts[item.name] = item
				});
				this.getPayAccounts(foreignKeyobj);
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getPayAccounts(foreignKeyobj) {
		axios.get(`/api/accounts?field=id,name&filtercondition=accounts.type='Asset' and accounts.isledger`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.payaccounts = {};
				response.data.main.forEach((item) => {
					foreignKeyobj.payaccounts[item.name] = item
				});
				this.getTerritories(foreignKeyobj);
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getTerritories(foreignKeyobj) {
		axios.get(`/api/territories?field=id,territoryname&filtercondition=`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.territories = {};
				response.data.main.forEach((item) => {
					foreignKeyobj.territories[item.territoryname] = item
				});
				this.getPriceLists(foreignKeyobj);
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getPriceLists(foreignKeyobj) {
		axios.get(`/api/pricelists?field=id,name&filtercondition=`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.pricelists = {};
				response.data.main.forEach((item) => {
					foreignKeyobj.pricelists[item.name] = item
				});
				this.getCustomerGroups(foreignKeyobj);
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getCustomerGroups(foreignKeyobj) {
		axios.get(`/api/customergroups?field=id,name`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.customergroups = {};
				response.data.main.forEach((item) => {
					foreignKeyobj.customergroups[item.name] = item
				});
				this.getCustomers(foreignKeyobj);
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getCustomers(foreignKeyobj, param) {
		axios.get(`/api/partners?field=id,name`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.customers = {};
				response.data.main.forEach((item) => {
					foreignKeyobj.customers[item.name.toLowerCase()] = item
				});
				if(param) {
					this.setState({
						foreignKeyobj,
					}, () => {
						this.validateFn(true);
					});
				} else {
					this.setState({
						foreignKeyobj,
						loaderflag: false
					});
				}
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	validateFn(param) {
		this.updateLoaderFlag(true);
		let customerObj = {};
		let customerDupObj = {};
		let errors = [];

		let {sheets, foreignKeyobj} = this.state;

		if(!sheets.Customers)
			errors.push('Sheet Customers is not there in Excel or No Customers Found');
		if(!sheets.Contacts)
			sheets.Contacts = [];
		if(!sheets.Addresses)
			sheets.Addresses = [];
		if(sheets.Customers && sheets.Customers.length == 0)
			errors.push('Customers must be atleast one item');
		if(errors.length > 0) {
			this.updateLoaderFlag(false);
			this.setState({
				errors: errors
			});
			document.getElementById('filecustomer').value='';
			this.fileOnChange([]);
			return false;
		}

		var customerArray = sheets.Customers;
		var mandatoryTextArray = ['Name*', 'Legal Name*'];
		var mandatoryBooleanArray = ['Is Individual*', 'Is Customer*', 'Is Supplier*', 'Is Competitor*'];
		var numberCheckArray = ['Purchase Credit Period', 'Sales Credit Period', 'Credit Limit', 'Latitude', 'Longitude'];

		for (var i = 0; i < this.state.customFields.length; i++) {
			if (this.state.customFields[i].type == 'integer')
				numberCheckArray.push(this.state.customFields[i].displayName);
		}

		customerArray.forEach((item, index) => {
			let rowIndex = index + 2;

			mandatoryTextArray.forEach((field) => {
				if(typeof(item[field]) != 'string')
					errors.push(`In Customers ${field} is mandatory & must be a string for row - ${rowIndex}`);
			});

			mandatoryBooleanArray.forEach((field) => {
				if(typeof(item[field]) != 'boolean')
					item[field] = item[field] === "TRUE" ? true : (item[field] === "FALSE" ? false : null);

				if(item[field] != true && item[field] != false)
					errors.push(`In Customers ${field} must contains the value TRUE or FALSE for row - ${rowIndex}`);
			});

			numberCheckArray.forEach((field) => {
				if(item[field]) {
					item[field] = Number(item[field]);
					if(isNaN(item[field]))
						errors.push(`In Customers ${field} must contains the number value for row - ${rowIndex}`);
				} else {
					if(item[field] == '')
						item[field] = null;
				}
			});

			if(item['Sales Person']) {
				if(!foreignKeyobj.users[item['Sales Person']])
					errors.push(`Invalid Sales Person for row - ${rowIndex}`);
				else
					item.salesperson = foreignKeyobj.users[item['Sales Person']].id;
			}

			if(item['Territory']) {
				if(!foreignKeyobj.territories[item['Territory']])
					errors.push(`Invalid Territory for row - ${rowIndex}`);
				else {
					item.territory = foreignKeyobj.territories[item['Territory']].id;
					item.territory_territoryname = foreignKeyobj.territories[item['Territory']].territoryname;
				}
			}

			if(item['Customer Group']) {
				if(!foreignKeyobj.customergroups[item['Customer Group']])
					errors.push(`Invalid Customer Group for row - ${rowIndex}`);
				else
					item.partnergroupid = foreignKeyobj.customergroups[item['Customer Group']].id;
			}

			if(item['Purchase Price List']) {
				if(!foreignKeyobj.pricelists[item['Purchase Price List']])
					errors.push(`Invalid Purchase Price List for row - ${rowIndex}`);
				else
					item.purchasepricelistid = foreignKeyobj.pricelists[item['Purchase Price List']].id;
			}

			if(item['Sales Price List']) {
				if(!foreignKeyobj.pricelists[item['Sales Price List']])
					errors.push(`Invalid Sales Price List for row - ${rowIndex}`);
				else
					item.salespricelistid = foreignKeyobj.pricelists[item['Sales Price List']].id;
			}

			if(item['Receivable Account']) {
				if(!foreignKeyobj.recaccounts[item['Receivable Account']])
					errors.push(`Invalid Receivable Account for row - ${rowIndex}`);
				else
					item.receivableaccountid = foreignKeyobj.recaccounts[item['Receivable Account']].id;
			}

			if(item['Payable Account']) {
				if(!foreignKeyobj.payaccounts[item['Payable Account']])
					errors.push(`Invalid Payable Account for row - ${rowIndex}`);
				else
					item.payableaccountid = foreignKeyobj.payaccounts[item['Payable Account']].id;
			}

			if(item['Collection Rep']) {
				if(!foreignKeyobj.users[item['Collection Rep']])
					errors.push(`Invalid Collection Rep for row - ${rowIndex}`);
				else
					item.collectionrep = foreignKeyobj.users[item['Collection Rep']].id;
			}

			if(item['GSTIN']) {
				if(!item['GST Registration Type'])
					errors.push(`In Customers GST Registration Type is mandatory for GSTIN for row - ${rowIndex}`);
				else if(item['GST Registration Type'] && ['Composition', 'Consumer', 'Regular', 'Unregistered', 'Embassy', 'SEZ', 'Export/Import', 'Deemed Export'].indexOf(item['GST Registration Type']) == -1)
					errors.push(`Invalid GST Registration Type for row - ${rowIndex}`);
				else if(['Composition', 'Regular'].indexOf(item['GST Registration Type']) >= 0 && ! /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9A-Z]{1}Z[0-9A-Z]{1}$/.test(item['GSTIN']))
					errors.push(`Invalid GSTIN Format for row - ${rowIndex}`);
			} else {
				if(item['GST Registration Type'] && ['Composition', 'Consumer', 'Regular', 'Unregistered', 'Embassy', 'SEZ', 'Export/Import', 'Deemed Export'].indexOf(item['GST Registration Type']) == -1)
					errors.push(`Invalid GST Registration Type for row - ${rowIndex}`);
			}

			if(!item['Is Customer*'] && !item['Is Supplier*'] && !item['Is Competitor*'])
				errors.push(`Choosing one of the following - Is Customer, Is Supplier, Is Competitor - is required for row - ${rowIndex}`);

			if(customerDupObj[item['Name*'].toLowerCase()])
				errors.push(`Duplicate Customer Name - ${item['Name*']}`);
			else if (foreignKeyobj.customers[item['Name*'].toLowerCase()])
				errors.push(`Duplicate Customer Name - ${item['Name*']}`);
			else {
				customerObj[item['Name*']] = JSON.parse(JSON.stringify(item));
				customerDupObj[item['Name*'].toLowerCase()] = JSON.parse(JSON.stringify(item));
				customerObj[item['Name*']].contactsArray = [];
				customerObj[item['Name*']].addressArray = [];
			}
		});

		if(errors.length > 0) {
			this.updateLoaderFlag(false);
			this.setState({
				errors: errors
			});
			document.getElementById('filecustomer').value='';
			this.fileOnChange([]);
			return false;
		}

		var contactsArray = sheets.Contacts;
		var mandatoryConTextArray = ['Customer Name*', 'Name*'];
		var mandatoryBooleanArray = ['Default'];

		contactsArray.forEach((item, index) => {
			let rowIndex = index + 2;
			mandatoryConTextArray.forEach((field) => {
				if(!item[field])
					errors.push(`In Contacts ${field} is must for row - ${rowIndex}`);
			});
			mandatoryBooleanArray.forEach((field) => {
				if(typeof(item[field]) != 'boolean')
					item[field] = (item[field] === "TRUE") ? true : (item[field] === "FALSE" ? false : null);
				if(item[field] != true && item[field] != false)
					errors.push(`In Contacts ${field} must contains the value TRUE or FALSE for row - ${rowIndex}`);
			});
			if(!customerObj[item['Customer Name*']])
				errors.push(`Invalid Customer Name in contacts for row - ${rowIndex}`);
			else
				customerObj[item['Customer Name*']].contactsArray.push(JSON.parse(JSON.stringify(item)));

			if(item['Title'] && ['Mr.','Mrs.','Ms.','Dr.','Capt.'].indexOf(item['Title']) == -1)
				errors.push(`Invalid Contact Title for row ${rowIndex}`);
		});


		var addressArray = sheets.Addresses;
		var mandatoryAddTextArray = ['Customer Name*', 'Title*', 'First Line*'];
		var mandatoryBooleanArray = ['Default'];
		
		addressArray.forEach((item, index) => {
			let rowIndex = index + 2;
			mandatoryAddTextArray.forEach((field) => {
				if(!item[field])
					errors.push(`In Addresses ${field} is must for row - ${rowIndex}`);
			});
			mandatoryBooleanArray.forEach((field) => {
				if(typeof(item[field]) != 'boolean')
					item[field] = (item[field] === "TRUE") ? true : (item[field] === "FALSE" ? false : null);
				if(item[field] != true && item[field] != false)
					errors.push(`In Addresses ${field} must contains the value TRUE or FALSE for row - ${rowIndex}`);
			});
			if(!customerObj[item['Customer Name*']])
				errors.push(`Invalid Customer Name in addresses for row - ${rowIndex}`);
			else
				customerObj[item['Customer Name*']].addressArray.push(JSON.parse(JSON.stringify(item)));
		});

		for(var prop in customerObj) {
			var isconprimaryContactCount = 0;
			var isaddprimaryContactCount = 0;

			customerObj[prop].contactsArray.forEach((item) => {
				if(item['Default'])
					isconprimaryContactCount++;
			});
			customerObj[prop].addressArray.forEach((item) => {
				if(item['Default'])
					isaddprimaryContactCount++;
			});

			if(isconprimaryContactCount > 1)
				errors.push(`Customer ${customerObj[prop]['Name*']} has more than one Default(TRUE) contact.Customer must have only one Default(TRUE) contact.`);

			if(isaddprimaryContactCount > 1)
				errors.push(`Customer ${customerObj[prop]['Name*']} has more than one Default(TRUE) address.Customer must have only one Default(TRUE) address.`);
		}

		if(errors.length > 0) {
			this.updateLoaderFlag(false);
			this.setState({
				errors: errors
			});
			document.getElementById('filecustomer').value='';
			this.fileOnChange([]);
			return false;
		}

		let isValid = true;
		if(param)
			this.importtran(customerObj)
		else
			this.setState({
				isValid,
				loaderflag: false
			});
	}

	import() {
		this.updateLoaderFlag(true);
		let foreignKeyobj = {
			...this.state.foreignKeyobj
		};
		this.getCustomers(foreignKeyobj, true);
	}

	importtran(customerObj) {
		var propertiesCustomerArray = [
			{
				"name" : "Name*",
				"column" : "name"
			}, {
				"name" : "Legal Name*",
				"column" : "legalname"
			}, {
				"name" : "Is Individual*",
				"column" : "isindividual"
			}, {
				"name" : "Is Customer*",
				"column" : "iscustomer"
			}, {
				"name" : "Is Supplier*",
				"column" : "issupplier"
			}, {
				"name" : "Is Competitor*",
				"column" : "iscompetitor"
			}, {
				"name" : "Territory",
				"column" : "territory",
				"columnname" : "territory_territoryname",
				"type" : "fk"
			}, {
				"name" : "Sales Person",
				"column" : "salesperson",
				"type" : "fk"
			}, {
				"name" : "Description",
				"column" : "description"
			}, {
				"name" : "Customer Group",
				"column" : "partnergroupid",
				"type" : "fk"
			}, {
				"name" : "Fax No",
				"column" : "faxno"
			}, {
				"name" : "Tin No",
				"column" : "tinno"
			}, {
				"name" : "Pan No",
				"column" : "panno"
			}, {
				"name" : "Purchase Price List",
				"column" : "purchasepricelistid",
				"type" : "fk"
			}, {
				"name" : "Purchase Credit Period",
				"column" : "purchasecreditperiod"
			}, {
				"name" : "Supplier Code",
				"column" : "suppliercode"
			}, {
				"name" : "Cst No",
				"column" : "cstno"
			}, {
				"name" : "ECC Number",
				"column" : "eccnumber"
			}, {
				"name" : "Sales Credit Period",
				"column" : "salescreditperiod"
			}, {
				"name" : "Sales Price List",
				"column" : "salespricelistid",
				"type" : "fk"
			}, {
				"name" : "Credit Limit",
				"column" : "creditlimit"
			}, {
				"name" : "Receivable Account",
				"column" : "receivableaccountid",
				"type" : "fk"
			}, {
				"name" : "Payable Account",
				"column" : "payableaccountid",
				"type" : "fk"
			}, {
				"name" : "Service Tax No",
				"column" : "tngstno"
			}, {
				"name" : "Excise Range",
				"column" : "exciserange"
			}, {
				"name" : "Excise Division",
				"column" : "excisedivision"
			}, {
				"name" : "Excise Commissionerate",
				"column" : "commissionerate"
			}, {
				"name" : "Payment Terms",
				"column" : "paymentterms"
			}, {
				"name" : "Mode of Dispatch",
				"column" : "modeofdispatch"
			}, {
				"name" : "Freight",
				"column" : "freight"
			}, {
				"name" : "Collection Rep",
				"column" : "collectionrep",
				"type" : "fk"
			}, {
				"name" : "Latitude",
				"column" : "latitude"
			}, {
				"name" : "Longitude",
				"column" : "longitude"
			}, {
				"name" : "Nature of Purchase",
				"column" : "natureofpurchaseid",
				"type" : "fk"
			}, {
				"name" : "Importer Exporter Code",
				"column" : "importerexportercode"
			}, {
				"name" : "Website",
				"column" : "website"
			}, {
				"name" : "GSTIN",
				"column" : "gstin"
			}, {
				"name" : "GST Registration Type",
				"column" : "gstregtype"
			}
		];
		this.state.customFields.forEach((cusfield) => {
			propertiesCustomerArray.push({
				"name": cusfield.displayName,
				"column": cusfield.fieldName
			});
		});
		var propertiesContactsArray = [
			{
				"name" : "Customer Name*",
				"column" : "customerid"
			}, {
				"name" : "Name*",
				"column" : "name"
			}, {
				"name" : "Designation",
				"column" : "designation"
			}, {
				"name" : "Email",
				"column" : "email"
			}, {
				"name" : "Phone",
				"column" : "phone"
			}, {
				"name" : "Mobile",
				"column" : "mobile"
			}, {
				"name" : "Notes",
				"column" : "additionalnotes"
			}, {
				"name" : "Fax",
				"column" : "faxno"
			}, {
				"name" : "Default",
				"column" : "isdefault"
			}
		];
		var propertiesAddressesArray = [
			{
				"name" : "Customer Name*",
				"column" : "customerid"
			}, {
				"name" : "Title*",
				"column" : "title"
			}, {
				"name" : "First Line*",
				"column" : "firstline"
			}, {
				"name" : "Second Line",
				"column" : "secondline"
			}, {
				"name" : "City",
				"column" : "city"
			}, {
				"name" : "State",
				"column" : "state"
			}, {
				"name" : "Country",
				"column" : "country"
			}, {
				"name" : "Postal Code",
				"column" : "postalcode"
			}, {
				"name" : "Default",
				"column" : "isdefault"
			}, {
				"name" : "Display Address",
				"column" : "displayaddress"
			}
		];
		var dataArray = [];
		for(var prop in customerObj) {
			var tempObj = {};

			propertiesCustomerArray.forEach((field) => {
				if(field.type) {
					tempObj[field.column] = customerObj[prop][field.column];
					tempObj[field.columnname] = customerObj[prop][field.columnname];
				} else {
					tempObj[field.column] = customerObj[prop][field.name];
				}
			});
			tempObj.contactsArray = customerObj[prop].contactsArray;
			tempObj.addressArray = customerObj[prop].addressArray;
			dataArray.push(JSON.parse(JSON.stringify(tempObj)));
		};
		
		console.log(dataArray);
		axios({
			method : 'post',
			data : {
				actionverb : 'UploadCustomer',
				data : dataArray
			},
			url : '/api/importtransaction'
		}).then((response) => {
			if(response.data.message == 'success') {
				this.setState({
					isValid: false
				}, () => {
					this.getCustomers({...this.state.foreignKeyobj});
				});
			}
			var apiResponse = commonMethods.apiResult(response);
			modalService[apiResponse.methodName](apiResponse.message);
			this.updateLoaderFlag(false);
		});
	}

	sampleFn() {
		window.open("/lib/import_customer_sample.xlsx", '_blank');
	}

	fileOnChange(files) {
		if(files.length > 0) {
			this.updateLoaderFlag(true);
			XLSXReader(files[0], true, true, (data) => {
				this.setState({
					file: files[0],
					filename: files[0].name,
					sheets: data.sheets,
					errors: [],
					loaderflag: false,
					isValid: false
				});
			});
		} else {
			this.setState({
				file: null,
				filename: null,
				//errors: [],
				sheets: null,
				isValid: false
			});
		}
	}

	render() {
		let { sheets } = this.state;
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row" style={{marginTop: `${(document.getElementsByClassName('navbar').length > 0 ? document.getElementsByClassName('navbar')[0].clientHeight : '0')}px`}}>
					<div className="col-sm-12 col-md-12" style={{position: 'fixed', height: '45px', backgroundColor: '#FAFAFA',  zIndex: 5}}>
						<div className="row">
							<div className="col-md-6 col-sm-12 paddingleft-md-30">
								<h6 className="margintop-15 semi-bold-custom">Import Customers</h6>
							</div>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-12 col-md-12" style={{marginTop: `48px`}}>
						<div className="card marginbottom-15 borderradius-0">
							<div className="card-body">
								<div className="row responsive-form-element">
									<div className="form-group col-sm-12 col-md-4">
										<label className="labelclass">Excel File</label>
										<input type="file" accept={'.xlsx'} id="filecustomer" className="form-control" onChange={(evt) => this.fileOnChange(evt.target.files)} />
									</div>
									<div className="form-group col-sm-12 col-md-8">
										<button type="button"  className="btn btn-sm btn-width gs-btn-warning" onClick={() => this.validateFn()} disabled={!sheets} ><span className="fa fa-ticket"></span> Validate</button>   <button type="button"  className="btn btn-sm btn-width gs-btn-success" onClick={this.import} disabled={!this.state.isValid}><span className="fa fa-upload"></span> Import</button>   <button type="button" className="btn btn-sm btn-width gs-btn-info" onClick={this.sampleFn}><span className="fa fa-download"></span> Sample Excel </button>
									</div>
									<div className="col-md-12">
										{sheets ? <div className="alert alert-info">
											<p>{sheets.Customers ? sheets.Customers.length : 0} Customers Found </p>
											<p>{sheets.Contacts ? sheets.Contacts.length : 0} Contacts Found </p>
											<p>{sheets.Addresses ? sheets.Addresses.length : 0} Addresses Found </p>
										</div> : null}
									</div>
									<div className="col-md-12">
										{this.state.isValid ? <div className="alert alert-success">
											<ul>
												<li>Validated Successfull</li>
											</ul>
										</div> : null}
									</div>
									<div className="col-md-12">
										{this.state.errors && this.state.errors.length>0 ? <div className="alert alert-success">
											<ul>
												{this.state.errors.map((item, index) => <li key={index}>{item}</li>)}
											</ul>
										</div> : null}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

ImportCustomer = connect(
	(state, props) => {
		let formName = 'ImportCustomer';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState}
)(reduxForm()(ImportCustomer));

export default ImportCustomer;
