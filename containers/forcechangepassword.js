import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Prompt } from "react-router-dom";

import { checkActionVerbAccess } from '../utils/utils';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import {updateAppState} from '../actions/actions';

class ForceChangePasswordForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			user: {},
			errorArray: [],
			loaderflag : false
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.updatePassword = this.updatePassword.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
	}

	componentWillMount() {
		if(!this.props.app.user.forcepasswordchange)
			return this.props.history.replace('/');
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	updatePassword (evt) {
		evt.preventDefault();
		if(this.state.user.newpassword && this.state.user.confpassword && this.state.user.newpassword != this.state.user.confpassword) {
			this.setState({
				errormsg: 'New password does not match with Confirm password'
			});
			return true;
		}

		this.updateLoaderFlag(true);
		let tempData = {
			newpassword: this.state.user.newpassword,
			confpassword: this.state.user.confpassword,
		};

		axios({
			method : 'post',
			data : tempData,
			url : '/forcepassword'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			if (response.data.message == 'success') {
				let user = this.props.app.user;
				user.forcepasswordchange = false;
				this.props.updateAppState({ user });
				setTimeout(() => {
					this.props.history.push('/dashboard');
				}, 0);
			} else {
				this.updateLoaderFlag(false);
			}
		});
	}

	inputOnChange(evt) {
		let { user } = this.state;
		user[evt.target.name] = evt.target.value;
		this.setState({ user });
	}

	render() {
		let { user } = this.state;
		return (
			<div>
				<Prompt when={this.props.app.user.forcepasswordchange} message={`Always Required`} />
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="forcechge-pwd-login-limiter">
					<div className="forcechge-pwd-login-container">
						<div className="forcechge-pwd-login-wrapper">
							<form class="forcechge-pwd-form">
								<div className="forcechge-pwd-login-image">
									<img src="/images/clientlogo.png" style={{maxWidth: '200px', maxHeight: '100px'}} />
								</div>
								{this.state.errormsg ? <div className="alert alert-danger">{this.state.errormsg}</div> : null}
								<div style={{marginBottom: '30px'}}>
									<span>You must change your password to continue</span>
								</div>
								<div class="forcechge-pwd-form-group">
									<span style={{color:'#1dbb99', fontWeight: 'bold'}}>Please enter a new password</span>
								</div>
								<div className="forcechge-pwd-form-group">
									<input type="password" className={`form-control ${!this.state.user.newpassword ? 'errorinput' : ''}`} placeholder="Enter new password" name="newpassword" value={user.newpassword} onChange={(evt) => this.inputOnChange(evt)} autoComplete="off" required />
								</div>
								<div className="forcechge-pwd-form-group">
									<input type="password" className={`form-control ${!this.state.user.confpassword ? 'errorinput' : ''}`} placeholder="Confirm new password" name="confpassword" value={user.confpassword} onChange={(evt) => this.inputOnChange(evt)} autoComplete="off" required />
								</div>
								<div className="forcechge-pwd-btn">
									<button type="submit" className="btn btn-lg btn-block gs-btn-success font-14" style={{borderRadius: '3px'}} onClick={this.updatePassword} disabled={!this.state.user.newpassword || !this.state.user.confpassword}>Change Password</button>
								</div>
							</form>
						</div>
						<div className="forcechge-pwd-login-footer text-center">
							<span>Copyright © 2019 GROWSMART SMB SOLUTIONS PRIVATE LIMITED.  </span>
							<a href="http://growsmartsmb.com/terms.html" style={{color: '#53be9d'}}>Terms of Use.</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

ForceChangePasswordForm = connect((state) =>{
	return {app: state.app}
}, { updateAppState }) (ForceChangePasswordForm);

export default ForceChangePasswordForm;
