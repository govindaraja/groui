import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

import { updateFormState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { XLSXReader } from '../utils/excelutils';

class ImportItemMaster extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : true,
			foreignKeyobj: {},
			isValid: false
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getIncAccounts = this.getIncAccounts.bind(this);
		this.getExpAccounts = this.getExpAccounts.bind(this);
		this.getUOM = this.getUOM.bind(this);
		this.getItemGroups = this.getItemGroups.bind(this);
		this.getItemCategory = this.getItemCategory.bind(this);
		this.getTaxes = this.getTaxes.bind(this);
		this.getContractTypes = this.getContractTypes.bind(this);
		this.getSuppliers = this.getSuppliers.bind(this);
		this.getHSNCodes = this.getHSNCodes.bind(this);
		this.getRoutings = this.getRoutings.bind(this);
		this.getItemmaster = this.getItemmaster.bind(this);
		this.validateFn = this.validateFn.bind(this);
		this.sampleFn = this.sampleFn.bind(this);
		this.fileOnChange = this.fileOnChange.bind(this);
		this.import = this.import.bind(this);
		this.importtran = this.importtran.bind(this);
	}

	componentWillMount() {
		let customFields = [], itemcategories, useSubLocations;

		for(let prop in this.props.app.myResources.itemmaster.fields) {
			if(prop.indexOf('zzz') == 0) {
				let tempObj = JSON.parse(JSON.stringify(this.props.app.myResources.itemmaster.fields[prop]));
				tempObj.fieldName = prop;
				customFields.push(tempObj);
			}
		}

		this.props.app.appSettings.forEach((item) => {
			if(item.module == 'Stock' && item.name == 'itemCategories')
				itemcategories = item.value.value;

			if(item.module == 'Stock' && item.name == 'useSubLocations')
				useSubLocations = item.value.value;
		});

		this.setState({
			customFields,
			itemcategories,
			useSubLocations
		});

		this.getIncAccounts({});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	getIncAccounts(foreignKeyobj) {
		axios.get(`/api/accounts?field=id,name&filtercondition=accounts.type='Income' and accounts.isledger`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.incaccounts = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.incaccounts[item.name] = item
				});

				this.getExpAccounts(foreignKeyobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getExpAccounts(foreignKeyobj) {
		axios.get(`/api/accounts?field=id,name&filtercondition=accounts.type='Expense' and accounts.isledger`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.expaccounts = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.expaccounts[item.name] = item
				});

				this.getUOM(foreignKeyobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getUOM(foreignKeyobj) {
		axios.get(`/api/uom?field=id,name,parentid`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.uom = {};
				foreignKeyobj.uomgroups = {};
				foreignKeyobj.uomobj = {};

				response.data.main.forEach((item) => {
					if(!item.parentid) {
						foreignKeyobj.uomgroups[item.name] = item;
						foreignKeyobj.uomobj[item.id] = {};
					}
				});

				response.data.main.forEach((item) => {
					foreignKeyobj.uom[item.name] = item;

					if(item.parentid > 0)
						foreignKeyobj.uomobj[item.parentid][item.id] = item;
				});

				this.getItemGroups(foreignKeyobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getItemGroups(foreignKeyobj) {
		axios.get(`/api/itemgroups?field=id,groupname,fullname`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.itemgroups = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.itemgroups[item.fullname] = item
				});

				this.getItemCategory(foreignKeyobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getItemCategory(foreignKeyobj) {
		axios.get(`/api/itemcategorymaster?field=id,name`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.itemcategorymaster = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.itemcategorymaster[item.name] = item
				});

				this.getTaxes(foreignKeyobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getTaxes(foreignKeyobj) {
		axios.get(`/api/taxcodemaster?field=id,name&filtercondition=taxcodemaster.type='Expense'`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.taxcodemaster = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.taxcodemaster[item.name] = item
				});

				this.getContractTypes(foreignKeyobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getContractTypes(foreignKeyobj) {
		axios.get(`/api/contracttypes?field=id,name&filtercondition=contracttypes.type='Warranty'`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.contracttypes = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.contracttypes[item.name] = item
				});

				this.getSuppliers(foreignKeyobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getSuppliers(foreignKeyobj) {
		axios.get(`/api/partners?field=id,name&filtercondition=partners.issupplier`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.partners = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.partners[item.name] = item
				});

				this.getHSNCodes(foreignKeyobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getHSNCodes(foreignKeyobj) {
		axios.get(`/api/hsncodes?field=id,code`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.hsncodes = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.hsncodes[item.code] = item
				});

				this.getRoutings(foreignKeyobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getRoutings(foreignKeyobj) {
		axios.get(`/api/routings?field=id,name`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.routings = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.routings[item.code] = item
				});

				this.getItemmaster(foreignKeyobj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getItemmaster(foreignKeyobj, param) {
		axios.get(`/api/itemmaster?field=id,name`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.itemmaster = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.itemmaster[item.name.toLowerCase()] = item
				});

				if(param) {
					this.setState({
						foreignKeyobj,
					}, () => {
						this.validateFn(true);
					});
				} else {
					this.setState({
						foreignKeyobj,
						loaderflag: false
					});
				}
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	validateFn(param) {
		this.updateLoaderFlag(true);
		let itemmasterObj = {};
		let itemmasterDupObj = {};
		let errors = [];

		let {sheets, foreignKeyobj, useSubLocations } = this.state;

		if(!sheets.Itemmaster)
			errors.push('Sheet Itemmaster is not there in Excel or No Items Found');

		if(!sheets.AlternativeUOM)
			sheets.AlternativeUOM = [];

		if(sheets.Itemmaster && sheets.Itemmaster.length == 0)
			errors.push('Itemmaster must be atleast one item');

		if(errors.length > 0) {
			this.updateLoaderFlag(false);
			this.setState({
				errors: errors
			});
			document.getElementById('fileitem').value='';
			this.fileOnChange([]);
			return false;
		}

		let itemmasterArray = sheets.Itemmaster;
		let mandatoryTextArray = ['Name*', 'Description*','Item Type*','UOM Group*'];
		let mandatoryBooleanArray = ['Allow Sales*', 'Allow Purchase*', 'Keep Stock*', 'Has Serial*','Has Batch*','Is Equipment*','Allow Back Order*','Allow Production*'];
		let numberCheckArray = ['Reorder Qty', 'Safety Stock', 'Lead Time', 'Minimum Selling Price', 'Recommended Selling Price','Default Purchase Cost','Warranty Duration(Days)','Serial No Current Value'];

		for (let i = 0; i < this.state.customFields.length; i++) {
			if (this.state.customFields[i].type == 'integer')
				numberCheckArray.push(this.state.customFields[i].displayName);
		}

		itemmasterArray.forEach((item, index) => {
			let rowIndex = index + 2;

			mandatoryTextArray.forEach((field) => {
				if(typeof(item[field]) != 'string')
					errors.push(`In Itemmaster ${field} is mandatory & must be a string for row - ${rowIndex}`);
			});

			mandatoryBooleanArray.forEach((field) => {
				if(typeof(item[field]) != 'boolean')
					item[field] = item[field] === "TRUE" ? true : (item[field] === "FALSE" ? false : null);

				if(item[field] != true && item[field] != false)
					errors.push(`In Itemmaster ${field} must contains the value TRUE or FALSE for row - ${rowIndex}`);
			});

			numberCheckArray.forEach((field) => {
				if(item[field]) {
					item[field] = Number(item[field]);
					if(isNaN(item[field]))
						errors.push(`In Itemmaster ${field} must contains the number value for row - ${rowIndex}`);
				} else {
					if(item[field] == '')
						item[field] = null;
				}
			});

			if(item['Item Group']) {
				if(!foreignKeyobj.itemgroups[item['Item Group']])
					errors.push(`Invalid Item Group for row - ${rowIndex}`);
				else {
					item.itemgroupid = foreignKeyobj.itemgroups[item['Item Group']].id;
					item.itemgroupid_fullname = foreignKeyobj.itemgroups[item['Item Group']].fullname;
				}
			}

			if(item['Item Category']) {
				if(!foreignKeyobj.itemcategorymaster[item['Item Category']])
					errors.push(`Invalid Item Category for row - ${rowIndex}`);
				else {
					item.itemcategorymasterid = foreignKeyobj.itemcategorymaster[item['Item Category']].id;
					item.itemcategorymasterid_name = foreignKeyobj.itemcategorymaster[item['Item Category']].name;
				}
			}

			if(item['UOM Group*']) {
				if(!foreignKeyobj.uomgroups[item['UOM Group*']])
					errors.push(`Invalid UOM Group for row - ${rowIndex}`);
				else
					item.uomgroupid = foreignKeyobj.uomgroups[item['UOM Group*']].id;
			}

			if(item['Sales UOM']) {
				if(!foreignKeyobj.uom[item['Sales UOM']])
					errors.push(`Invalid Sales UOM for row - ${rowIndex}`);
				else
					item.salesuomid = foreignKeyobj.uom[item['Sales UOM']].id;
			}

			if(item['Purchase UOM']) {
				if(!foreignKeyobj.uom[item['Purchase UOM']])
					errors.push(`Invalid Purchase UOM for row - ${rowIndex}`);
				else
					item.purchaseuomid = foreignKeyobj.uom[item['Purchase UOM']].id;
			}

			if(item['Stock UOM']) {
				if(!foreignKeyobj.uom[item['Stock UOM']])
					errors.push(`Invalid Stock UOM for row - ${rowIndex}`);
				else
					item.stockuomid = foreignKeyobj.uom[item['Stock UOM']].id;
			}

			if(item['Income Account']) {
				if(!foreignKeyobj.incaccounts[item['Income Account']])
					errors.push(`Invalid Income Account for row - ${rowIndex}`);
				else
					item.incomeaccountid = foreignKeyobj.incaccounts[item['Income Account']].id;
			}

			if(item['Expense Account']) {
				if(!foreignKeyobj.expaccounts[item['Expense Account']])
					errors.push(`Invalid Expense Account for row - ${rowIndex}`);
				else
					item.expenseaccountid = foreignKeyobj.expaccounts[item['Expense Account']].id;
			}

			if(item['Excise Tax Code']) {
				if(!foreignKeyobj.taxcodemaster[item['Excise Tax Code']])
					errors.push(`Invalid Excise Tax Code for row - ${rowIndex}`);
				else
					item.excisetaxcodeid = foreignKeyobj.taxcodemaster[item['Excise Tax Code']].id;
			}

			if(item['Warranty Contract']) {
				if(!foreignKeyobj.contracttypes[item['Warranty Contract']])
					errors.push(`Invalid Warranty Contract for row - ${rowIndex}`);
				else
					item.warrantycontracttypeid = foreignKeyobj.contracttypes[item['Warranty Contract']].id;
			}

			if(item['Default Supplier']) {
				if(!foreignKeyobj.partners[item['Default Supplier']])
					errors.push(`Invalid Default Supplier for row - ${rowIndex}`);
				else
					item.defaultsupplierid = foreignKeyobj.partners[item['Default Supplier']].id;
			}

			if(item['HSN/SAC Code']) {
				if(!foreignKeyobj.hsncodes[item['HSN/SAC Code']])
					errors.push(`Invalid HSN/SAC Code for row - ${rowIndex}`);
				else
					item.hsncodeid = foreignKeyobj.hsncodes[item['HSN/SAC Code']].id;
			}

			if(item['Routing']) {
				if(!foreignKeyobj.routings[item['Routing']])
					errors.push(`Invalid Routing for row - ${rowIndex}`);
				else
					item.routingid = foreignKeyobj.routings[item['Routing']].id;
			}

			if(itemmasterDupObj[item['Name*'].toLowerCase()])
				errors.push(`Duplicate Item Name - ${item['Name*']}`);
			else if (foreignKeyobj.itemmaster[item['Name*'].toLowerCase()])
				errors.push(`Duplicate itemmaster Name - ${item['Name*']}`);
			else {
				itemmasterObj[item['Name*']] = JSON.parse(JSON.stringify(item));
				itemmasterDupObj[item['Name*'].toLowerCase()] = JSON.parse(JSON.stringify(item));
				itemmasterObj[item['Name*']].alternativeuomArray = [];
				itemmasterObj[item['Name*']].alternativeuomidArray = [];
			}

			if(['Product', 'Service', 'Project', 'Expense'].indexOf(item['Item Type*']) == -1)
				errors.push(`Item Type must be Product or Service or Project or Expense for row - ${rowIndex}`);

			item['Is Sales Kit*'] = false;
			item['Quantity Type*'] = 'Simple';

			if (item['Item Type*'] != 'Product') {
				item['Allow Back Order*'] = false;
				item['Keep Stock*'] = false;
				item['Is Equipment*'] = false;
				item['Has Serial*'] = false;
				item['Has Batch*'] = false;
				item['Enable Spare Warranty'] = false;
				item['Allow Production*'] = false;
			}

			if(item['Allow Sales*'])
				if(!item['Sales UOM'])
					errors.push(`For Sales Item Sales UOM is must for row - ${rowIndex}`);

			if(item['Allow Purchase*']) {
				if(!item['Purchase UOM'])
					errors.push(`For Purchase Item Purchase UOM is must for row - ${rowIndex}`);
			} else
				item.defaultsupplierid = null;


			if(item['Keep Stock*']) {
				if(!item['Stock UOM'])
					errors.push(`For Stock Item Stock UOM is must for row - ${rowIndex}`);
				else {
					if(item.stockuomid != item.uomgroupid && foreignKeyobj.uomobj[item.uomgroupid][item.stockuomid])
						errors.push(`Stock UOM must be in the group of UOM Group for row - ${rowIndex}`);
				}
			}

			if(item['Recommended Selling Price'] > 0 && item['Minimum Selling Price'] > 0 && item['Minimum Selling Price'] > item['Recommended Selling Price'])
				errors.push(`Recommended Selling Price should be more than Minimum Selling Price for row - ${rowIndex}`);


			if(item['Is Equipment*']) {
				if (typeof item['Installation Applicable'] != 'boolean')
					item['Installation Applicable'] = (item['Installation Applicable'] == "TRUE") ? true : false;

				if (typeof item['Warranty Applicable'] != 'boolean')
					item['Warranty Applicable'] = (item['Warranty Applicable'] == "TRUE") ? true : false;

				if(!item['Has Serial*'])
					errors.push(`For Equipment item must have serial number setting enabled for row - ${rowIndex}`);

				if(item['Warranty Applicable']) {
					if(!item['Warranty Contract'])
						errors.push(`For Equipment Item Warranty Contract is must for row - ${rowIndex}`);

					if(item['Warranty Based On'] != 'Installation Date' && item['Warranty Based On'] != 'Invoice Date')
						errors.push(`Warranty Based On must be Installation Date or Invoice Date for row - ${rowIndex}`);
				} else {
					item.warrantycontracttypeid = null;
					item['Warranty Contract'] = null;
					item['Warranty Based On'] = null;
				}
			} else {
				item['Installation Applicable'] = null;
				item['Warranty Applicable'] = null;
				item.warrantycontracttypeid = null;
				item['Warranty Contract'] = null;
				item['Warranty Based On'] = null;
			}


			if(item['Has Serial*']) {
				if (typeof item['Auto Generate Serial Number'] != 'boolean')
					item['Auto Generate Serial Number'] = (item['Auto Generate Serial Number'] == "TRUE") ? true : false;
				if(item['Auto Generate Serial Number']) {
					if(item['Has Batch*'] || useSubLocations)
						errors.push(`Serial No Auto Generate must be false, If Item Batch or Sub Locations is enabled for row - ${rowIndex}`);
					else {
						if(item['Serial No Format']) {
							if(item['Serial No Format'].indexOf('#') > -1) {
								let formatsplit = item['Serial No Format'].split('#');
								let isnumber = Number(formatsplit[1][0]);
								if(formatsplit.length > 2)
									errors.push(`Serial No Format you have entered ##. it should be one # for row - ${rowIndex}`);
								else if (isNaN(isnumber))
									errors.push(`Serial No Format you have entered after # should be a number for row - ${rowIndex}`);
								else if (isNaN(item['Serial No Current Value']))
									errors.push(`Serial No Current Value should be a number for row - ${rowIndex}`);
								else if (!(item['Serial No Current Value'] >= 0))
									errors.push(`Serial No Current Value should be Greater than or equal to 0 for row - ${rowIndex}`);
							} else
								errors.push(`Format is invalid.please refer the  below instruction. Specify # followed by number of digits in the series along with prefix and suffix. Example: SN/#5 becomes SN/00012 for row - ${rowIndex}`);
						} else
							errors.push(`Serial No Format is must,  If Auto Generate Serial No is enabled for row - ${rowIndex}`);
					}
				} else {
					item['Serial No Format'] = null;
					item['Serial No Current Value'] = null;
				}
			} else {
				item['Auto Generate Serial Number'] = null;
				item['Serial No Format'] = null;
				item['Serial No Current Value'] = null;
			}


			if (typeof item['Enable Spare Warranty'] != 'boolean')
				item['Enable Spare Warranty'] = (item['Enable Spare Warranty'] == "TRUE") ? true : (item['Enable Spare Warranty'] == "FALSE" ? false : null);

			if(item['Enable Spare Warranty']) {
				if(!item['Has Serial*'] || item['Is Equipment*'])
					errors.push(`Spare Warranty is only for Serial Item and Non Equipment for Row - ${rowIndex}`);
				else {
					if(item['Warranty Duration(Days)']) {
						item['Warranty Duration(Days)']=Number(item['Warranty Duration(Days)']);
						if(isNaN(item['Warranty Duration(Days)']))
							errors.push(`In Itemmaster Warranty Duration(Days) must contains the number value for row - ${rowIndex}`);
					} else
						errors.push(`In Itemmaster Warranty Duration(Days) must contains the number value for row - ${rowIndex}`);
				}
			}
		});

		if(errors.length > 0) {
			this.updateLoaderFlag(false);
			this.setState({
				errors: errors
			});
			document.getElementById('fileitem').value='';
			this.fileOnChange([]);
			return false;
		}

		let alternativeuomArray = sheets.AlternativeUOM;
		let mandatoryAltUomTextArray = ['Item Name*', 'UOM*','Conversion Type*'];

		alternativeuomArray.forEach((item, index) => {
			let rowIndex = index + 2;

			mandatoryAltUomTextArray.forEach((field) => {
				if(!item[field])
					errors.push(`In Alternative UOM ${field} is must for row - ${rowIndex}`);
			});

			if(item['Conversion Type*'] != 'Fixed' && item['Conversion Type*'] != 'Variable')
				errors.push(`Converion Type must be Fixed or Variable for row - ${rowIndex}`);

			item['Conversion Factor*'] = Number(item['Conversion Factor*']);

			if (isNaN(item['Conversion Factor*']))
				errors.push(`Converion Factor must be a number for row - ${rowIndex}`);
			
			if(!foreignKeyobj.uom[item['UOM*']])
				errors.push(`Invalid Alternative UOM for row - ${rowIndex}`);
			else
				item.uomid = foreignKeyobj.uom[item['UOM*']].id;

			if(!itemmasterObj[item['Item Name*']])
				errors.push(`Invalid Item Name in Alternative UOM for row - ${rowIndex}`);
			else
				itemmasterObj[item['Item Name*']].alternativeuomArray.push(JSON.parse(JSON.stringify(item)));

			if(item.uomid == itemmasterObj[item['Item Name*']].uomgroupid || foreignKeyobj.uomobj[itemmasterObj[item['Item Name*']].uomgroupid][item.uomid])
				errors.push(`Alternative UOM must not belongs to the group of uom group of item for row - ${rowIndex}`);

			itemmasterObj[item['Item Name*']].alternativeuomidArray.push(item.uomid);
		});

		for(let prop in itemmasterObj) {
			if(itemmasterObj[prop]['Allow Sales*']) {
				if(itemmasterObj[prop].salesuomid != itemmasterObj[prop].uomgroupid && !foreignKeyobj.uomobj[itemmasterObj[prop].uomgroupid][itemmasterObj[prop].salesuomid] && itemmasterObj[prop].alternativeuomidArray.indexOf(itemmasterObj[prop].salesuomid) == -1)
					errors.push(`Sales UOM must be in the group of UOM Group or Alternative UOM Group for item - ${prop}`);
			}

			if(itemmasterObj[prop]['Allow Purchase*']) {
				if(itemmasterObj[prop].purchaseuomid != itemmasterObj[prop].uomgroupid && !foreignKeyobj.uomobj[itemmasterObj[prop].uomgroupid][itemmasterObj[prop].purchaseuomid] && itemmasterObj[prop].alternativeuomidArray.indexOf(itemmasterObj[prop].purchaseuomid) == -1) 
					errors.push(`Purchase UOM must be in the group of UOM Group or Alternative UOM Group for item - ${prop}`);
			}
		}

		if(errors.length > 0) {
			this.updateLoaderFlag(false);
			this.setState({
				errors: errors
			});
			document.getElementById('fileitem').value='';
			this.fileOnChange([]);
			return false;
		}

		let isValid = true;
		if(param)
			this.importtran(itemmasterObj)
		else
			this.setState({
				isValid,
				loaderflag: false
			});
	}

	import() {
		this.updateLoaderFlag(true);
		let foreignKeyobj = {
			...this.state.foreignKeyobj
		};
		this.getItemmaster(foreignKeyobj, true);
	}

	importtran(itemmasterObj) {
		let { itemcategories } = this.state;

		let propertiesItemmasterArray = [{
				"name" : "Name*",
				"column" : "name"
			}, {
				"name" : "Description*",
				"column" : "description"
			}, {
				"name" : "Item Type*",
				"column" : "itemtype"
			}, {
				"name" : "Item Group",
				"column" : "itemgroupid",
				"type" : "fk",
				"columnname" : "itemgroupid_fullname"
			}, {
				"name" : "Item Category",
				"column" : "itemcategorymasterid",
				"type" : "fk",
				"columnname" : "itemcategorymasterid_name"
			}, {
				"name" : "Quantity Type*",
				"column" : "quantitytype"
			}, {
				"name" : "Allow Sales*",
				"column" : "allowsales"
			}, {
				"name" : "Allow Purchase*",
				"column" : "allowpurchase"
			}, {
				"name" : "Keep Stock*",
				"column" : "keepstock"
			}, {
				"name" : "Has Serial*",
				"column" : "hasserial"
			}, {
				"name" : "Has Batch*",
				"column" : "hasbatch"
			}, {
				"name" : "Auto Generate Serial Number",
				"column" : "isautogenerateserialno"
			}, {
				"name" : "Serial No Format",
				"column" : "serialnoformat"
			}, {
				"name" : "Serial No Current Value",
				"column" : "serialnocurrentvalue"
			}, {
				"name" : "Is Sales Kit*",
				"column" : "issaleskit"
			}, {
				"name" : "Is Equipment*",
				"column" : "isequipment"
			}, {
				"name" : "Installation Applicable",
				"column" : "isinstallationapplicable"
			}, {
				"name" : "Warranty Applicable",
				"column" : "iswarrantyapplicable"
			}, {
				"name" : "Warranty Contract",
				"column" : "warrantycontracttypeid",
				"type" : "fk"
			}, {
				"name" : "Warranty Based On",
				"column" : "warrantycontractbasedon"
			}, {
				"name" : "UOM Group*",
				"column" : "uomgroupid",
				"type" : "fk"
			}, {
				"name" : "Purchase UOM",
				"column" : "purchaseuomid",
				"type" : "fk"
			}, {
				"name" : "Sales UOM",
				"column" : "salesuomid",
				"type" : "fk"
			}, {
				"name" : "Stock UOM",
				"column" : "stockuomid",
				"type" : "fk"
			}, {
				"name" : "Recommended Selling Price",
				"column" : "recommendedsellingprice"
			}, {
				"name" : "Minimum Selling Price",
				"column" : "minimumsellingprice"
			}, {
				"name" : "Allow Back Order*",
				"column" : "allowbackorder"
			}, {
				"name" : "Lead Time",
				"column" : "leadtime"
			}, {
				"name" : "Safety Stock",
				"column" : "safetystock"
			}, {
				"name" : "Reorder Qty",
				"column" : "reorderqty"
			}, {
				"name" : "Default Purchase Cost",
				"column" : "defaultpurchasecost"
			}, {
				"name" : "Barcode",
				"column" : "barcode"
			}, {
				"name" : "Enable Spare Warranty",
				"column" : "enablesparewarranty"
			}, {
				"name" : "Warranty Duration(Days)",
				"column" : "sparewarrantyduration"
			}, {
				"name" : "Default Supplier",
				"column" : "defaultsupplierid",
				"type" : "fk"
			}, {
				"name" : "Income Account",
				"column" : "incomeaccountid",
				"type" : "fk"
			}, {
				"name" : "Expense Account",
				"column" : "expenseaccountid",
				"type" : "fk"
			}, {
				"name" : "Drawing No",
				"column" : "drawingno"
			}, {
				"name" : "Tariff Code",
				"column" : "tariffcode"
			}, {
				"name" : "TDC Number",
				"column" : "tdcnumber"
			}, {
				"name" : "Commodity Code",
				"column" : "commoditycode"
			}, {
				"name" : "Tariff Sub Heading Number",
				"column" : "tariffsubheadingnumber"
			}, {
				"name" : "Rate Of Duty",
				"column" : "rateofduty"
			}, {
				"name" : "Excise Tax Code",
				"column" : "excisetaxcodeid",
				"type" : "fk"
			}, {
				"name" : itemcategories['category1'].name,
				"column" : "category1"
			}, {
				"name" : itemcategories['category2'].name,
				"column" : "category2"
			}, {
				"name" : itemcategories['category3'].name,
				"column" : "category3"
			}, {
				"name" : itemcategories['category4'].name,
				"column" : "category4"
			}, {
				"name" : itemcategories['category5'].name,
				"column" : "category5"
			}, {
				"name" : itemcategories['category6'].name,
				"column" : "category6"
			}, {
				"name" : itemcategories['category7'].name,
				"column" : "category7"
			}, {
				"name" : "HSN/SAC Code",
				"column" : "hsncodeid",
				"type" : "fk"
			}, {
				"name" : "Allow Production*",
				"column" : "allowproduction"
			}, {
				"name" : "Routing",
				"column" : "routingid",
				"type" : "fk"
			}
		];

		this.state.customFields.forEach((cusfield) => {
			propertiesItemmasterArray.push({
				"name": cusfield.displayName,
				"column": cusfield.fieldName
			});
		});

		let propertiesAlternativeUOMArray = [{
				"name" : "Item Name*",
				"column" : "parentid"
			}, {
				"name" : "UOM*",
				"column" : "uomid",
				"type" : "fk"
			}, {
				"name" : "Conversion Factor*",
				"column" : "conversionfactor"
			}, {
				"name" : "Conversion Type*",
				"column" : "conversiontype"
			}
		];

		let dataArray = [];

		for(let prop in itemmasterObj) {
			let tempObj = {};

			propertiesItemmasterArray.forEach((field) => {
				if(field.type) {
					tempObj[field.column] = itemmasterObj[prop][field.column];
					tempObj[field.columnname] = itemmasterObj[prop][field.columnname];
				} else
					tempObj[field.column] = itemmasterObj[prop][field.name];
			});
			tempObj.alternativeuomArray = itemmasterObj[prop].alternativeuomArray;
			dataArray.push(JSON.parse(JSON.stringify(tempObj)));
		};

		axios({
			method : 'post',
			data : {
				actionverb : 'UploadItemmaster',
				data : dataArray
			},
			url : '/api/importtransaction'
		}).then((response) => {
			if(response.data.message == 'success') {
				this.setState({
					isValid: false
				}, () => {
					this.getItemmaster({...this.state.foreignKeyobj});
				});
			}
			let apiResponse = commonMethods.apiResult(response);
			modalService[apiResponse.methodName](apiResponse.message);
			this.updateLoaderFlag(false);
		});
	}

	sampleFn() {
		window.open("/lib/import_itemmaster_sample.xlsx", '_blank');
	}

	fileOnChange(files) {
		if(files.length > 0) {
			this.updateLoaderFlag(true);
			XLSXReader(files[0], true, true, (data) => {
				this.setState({
					file: files[0],
					filename: files[0].name,
					sheets: data.sheets,
					errors: [],
					loaderflag: false,
					isValid: false
				});
			});
		} else {
			this.setState({
				file: null,
				filename: null,
				//errors: [],
				sheets: null,
				isValid: false
			});
		}
	}

	render() {
		let { sheets } = this.state;
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row" style={{marginTop: `${(document.getElementsByClassName('navbar').length > 0 ? document.getElementsByClassName('navbar')[0].clientHeight : '0')}px`}}>
					<div className="col-sm-12 col-md-12" style={{position: 'fixed', height: '45px', backgroundColor: '#FAFAFA',  zIndex: 5}}>
						<div className="row">
							<div className="col-md-6 col-sm-12 paddingleft-md-30">
								<h6 className="margintop-15 semi-bold-custom">Import Itemmaster</h6>
							</div>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-12 col-md-12" style={{marginTop: `48px`}}>
						<div className="card marginbottom-15 borderradius-0">
							<div className="card-body">
								<div className="row responsive-form-element">
									<div className="form-group col-sm-12 col-md-4">
										<label className="labelclass">Excel File</label>
										<input type="file" accept={'.xlsx'} id="fileitem" className="form-control" onChange={(evt) => this.fileOnChange(evt.target.files)} />
									</div>
									<div className="form-group col-sm-12 col-md-8">
										<button type="button"  className="btn btn-sm btn-width gs-btn-warning" onClick={() => this.validateFn()} disabled={!sheets} ><span className="fa fa-ticket"></span> Validate</button>   <button type="button"  className="btn btn-sm btn-width gs-btn-success" onClick={this.import} disabled={!this.state.isValid}><span className="fa fa-upload"></span> Import</button>   <button type="button" className="btn btn-sm btn-width gs-btn-info" onClick={this.sampleFn}><span className="fa fa-download"></span> Sample Excel </button>
									</div>
									<div className="col-md-12">
										{sheets ? <div className="alert alert-info">
											<p>{sheets.Itemmaster ? sheets.Itemmaster.length : 0} Itemmaster Found </p>
										</div> : null}
									</div>
									<div className="col-md-12">
										{this.state.isValid ? <div className="alert alert-success">
											<ul>
												<li>Validated Successfull</li>
											</ul>
										</div> : null}
									</div>
									<div className="col-md-12">
										{this.state.errors && this.state.errors.length>0 ? <div className="alert alert-success">
											<ul>
												{this.state.errors.map((item, index) => <li key={index}>{item}</li>)}
											</ul>
										</div> : null}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

ImportItemMaster = connect(
	(state, props) => {
		let formName = 'ImportItemMaster';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState}
)(reduxForm()(ImportItemMaster));

export default ImportItemMaster;
