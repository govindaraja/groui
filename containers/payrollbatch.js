import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { updateFormState, updateReportFilter } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { DateEle, localSelectEle, autoSelectEle, selectAsyncEle, checkboxEle, textareaEle, InputEle, NumberEle, SpanEle, autoMultiSelectEle, costcenterAutoMultiSelectEle } from '../components/formelements';
import { numberNewValidation, dateNewValidation, stringNewValidation, checkActionVerbAccess } from '../utils/utils';
import { dateFilter, datetimeFilter, timeFilter} from '../utils/filter';
import { Reactuigrid } from '../components/reportcomponents';
import { ChildEditModal } from '../components/utilcomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class PayrollBatch extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderflag: true,
			attendance_columns: [{
					"name": "",
					"headerformat": "checkbox",
					"key": "ischecked",
					"locked": true,
					"format": "checkbox",
					"cellClass": "text-center",
					"onChange": "{report.checkboxOnChange}",
					"restrictToExport": true,
					"width": 100
				}, {
					"name": "Employee Name",
					"key": "displayname",
					"locked": true,
					"width": 200
				}, {
					"name": "Employee Code",
					"key": "employeecode",
					"cellClass" : "text-center",
					"locked": true,
					"width": 120
				}, {
					"name": "Employee Payable Account",
					"key": "emppayableaccountid_name",
					"cellClass": "text-center",
					"width": 150,
					"if": this.props.app.feature.useEmployeeLevelAccounting
				}, {
					"name": "Location",
					"key": "locationid_name",
					"cellClass": "text-center",
					"width": 150
				}, {
					"name": "Department",
					"key": "departmentid_name",
					"cellClass": "text-center",
					"width": 150
				}, {
					"name": "Total Working Days",
					"key": "paydaystotal",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Attendance Days",
					"key": "presentdays",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Weekoffs / Holidays",
					"key": "weekoffholidayCount",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Paid Leaves",
					"key": "paidleaves",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "LOP Days",
					"key": "lossofpays",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Computed Pay Days",
					"key": "defaultpaydaypaid",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Unsettled Days",
					"key": "nodata",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Payable Pay Days",
					"key": "paydaypaid",
					"cellClass": "text-right",
					"format": "numberinput",
					"onChange" : "{(value) => report.onChangeColumn('paydaypaid', value, item)}",
					"width": 150
				}, {
					"name": "Reason for Change",
					"key": "reason",
					"format": "textinput",
					"onChange" : "{(value) => report.onChangeColumn('reason', value, item)}",
					"width": 200
				}
			],
			payroll_columns: [{
					"name": "",
					"headerformat": "checkbox",
					"key": "ischecked",
					"locked": true,
					"format": "checkbox",
					"cellClass": "text-center",
					"onChange": "{report.checkboxOnChange}",
					"restrictToExport": true,
					"width": 100
				}, {
					"name": "Employee Name",
					"key": "displayname",
					"locked": true,
					"width": 200
				}, {
					"name": "Employee Code",
					"key": "employeecode",
					"cellClass" : "text-center",
					"locked": true,
					"width": 120
				}, {
					"name" : "Payroll No",
					"key": "payrollno",
					"format" : "button",
					"onClick" : "{report.btnOnClick}",
					"buttonname" : '{item.payrollno}',
					"buttonclass" : "btn-link",
					"cellClass" : "text-center",
					"restrictToExport" : false,
					"locked": true,
					"width" : 150
				}, {
					"name": "Total Pay Days",
					"key": "paydaystotal",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Computed Pay Days",
					"key": "defaultpaydaypaid",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Leave Days Without Pay",
					"key": "lopdays",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Payable Pay Days",
					"key": "paydaypaid",
					"cellClass": "text-right",
					"width": 150
				}
			]
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.initialize = this.initialize.bind(this);
		this.getItemById = this.getItemById.bind(this);
		this.getEmployeeDetails = this.getEmployeeDetails.bind(this);
		this.getPayrollDetails = this.getPayrollDetails.bind(this);
		this.currencyOnChange = this.currencyOnChange.bind(this);
		this.save = this.save.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.deleteEmployee = this.deleteEmployee.bind(this);
		this.cbEmployeeDetails = this.cbEmployeeDetails.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.cbPayRollDetails = this.cbPayRollDetails.bind(this);
		this.addComponent = this.addComponent.bind(this);
		this.createJournal = this.createJournal.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.getConfirmation = this.getConfirmation.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);

		if (this.props.match.path == '/createPayrollBatch')
			this.initialize();
		else
			this.getItemById();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	initialize() {
		let tempObj = {
			payrolldate: new Date(),
			companyid: this.props.app.selectedcompanyid,
			currencyid: this.props.app.defaultCurrency,
			roundoffmethod: this.props.app.defaultRoundOffMethod,
			employeeArray: [],
			locationid: null,
			departmentid: null,
			payrollArray: [],
			paymentArray: [],
			outStandingArray: [],
			filters: {},
			hiddencols: [],
			columns: []
		};

		axios.get(`/api/employees?&field=id,displayname,locationid,departmentid&filtercondition=employees.userid=${this.props.app.user.id} AND (employees.employeestatus NOT IN ('Resigned', 'Terminated') OR employees.employeestatus IS NULL)`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.length > 0)
					tempObj['locationid'] = response.data.main[0].locationid > 0 ? [response.data.main[0].locationid] : null;

				this.props.initialize(tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	getItemById() {
		axios.get(`/api/payrollbatch/${this.props.match.params.id}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.initialize({
					...response.data.main,
					employeeArray: [],
					payrollArray: [],
					paymentArray: [],
					filters: {},
					hiddencols: [],
					columns: []
				});

				setTimeout(() => {
					this.getPayrollDetails()
				}, 0);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	onChangeColumn(key, value, item) {
		item[key] = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		this.refs.grid.refresh();
	}

	getEmployeeDetails() {
		this.updateLoaderFlag(true);

		let filterString = [];

		['periodstart', 'periodend', 'locationid', 'departmentid'].forEach((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`);
		});

		filterString.push(`type=Create`);

		axios.get(`/api/query/getbatchpayrolldetailsquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.length > 0) {
					this.props.updateFormState(this.props.form, {
						employeeArray: response.data.main,
						originalRows: response.data.main,
						columns: this.state.attendance_columns
					});

					this.props.updateReportFilter(this.props.form, this.props.resource);
				} else
					this.props.openModal(modalService['infoMethod']({
						header: 'Error',
						body: 'No employee found for your search!',
						btnArray: ['Ok']
					}));
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	getPayrollDetails() {
		let { payroll_columns } = this.state;

		this.updateLoaderFlag(true);

		let filterString = [];

		if (this.props.resource.id)
			filterString.push(`batchid=${this.props.resource.id}`);

		filterString.push(`type=Details`);

		axios.get(`/api/query/getbatchpayrolldetailsquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {

				if (payroll_columns.length > 8)
					payroll_columns.splice(8, payroll_columns.length-1);

				/*response.data.componentEarningArray.sort((a, b) => {
					return a.toLowerCase() < b.toLowerCase() ? -1 : (a.toLowerCase() > b.toLowerCase() ? 1 : 0)
				});

				response.data.componentDeductionArray.sort((a, b) => {
					return a.toLowerCase() < b.toLowerCase() ? -1 : (a.toLowerCase() > b.toLowerCase() ? 1 : 0)
				});*/

				if (response.data.componentEarningArray.length > 0)
					response.data.componentEarningArray.forEach((item) => {
						payroll_columns.push({
							"name": response.data.componentObject[item],
							"key": item,
							"cellClass": "text-right",
							"width": 150
						})
					});

				if (response.data.componentDeductionArray.length > 0)
					response.data.componentDeductionArray.forEach((item) => {
						payroll_columns.push({
							"name": response.data.componentObject[item],
							"key": item,
							"cellClass": "text-right",
							"width": 150
						})
					});

				if (response.data.componentEmployerContributionArray.length > 0)
					response.data.componentEmployerContributionArray.forEach((item) => {
						payroll_columns.push({
							"name": response.data.componentObject[item],
							"key": item,
							"cellClass": "text-right",
							"width": 150
						})
					});

				if (response.data.componentRecoveryArray.length > 0)
					response.data.componentRecoveryArray.forEach((item) => {
						payroll_columns.push({
							"name": response.data.componentObject[item],
							"key": item,
							"cellClass": "text-right",
							"width": 150
						})
					});

				payroll_columns.push({
					"name": "Total Earning",
					"key": "totalearnings",
					"cellClass": "text-right",
					"footertype" : "sum",
					"width": 180
				}, {
					"name": "Total Deduction",
					"key": "totaldeductions",
					"cellClass": "text-right",
					"footertype" : "sum",
					"width": 180
				}, {
					"name": "Total Employer Contributions",
					"key": "totalemployercontributions",
					"cellClass": "text-right",
					"footertype" : "sum",
					"width": 180
				}, {
					"name": "Net Pay",
					"key": "netpay",
					"cellClass": "text-right",
					"footertype" : "sum",
					"width": 200
				}, {
					"name": "Payment Status",
					"key": "payment_status",
					"cellClass": "text-center",
					"width": 200,
					"if" : this.props.resource.status == 'Approved'
				}, {
					"name": "Out Standing Amount",
					"key": "outstandingamount",
					"cellClass": "text-right",
					"width": 200,
					"footertype" : "sum",
					"if" : this.props.resource.status == 'Approved'
				}, {
					"name" : "Voucher Type",
					"key" : "journaltype",
					"cellClass" : "text-center",
					"width" : 150,
					"if" : this.props.resource.status == 'Approved'
				}, {
					"name" : "Voucher No",
					"key" : "journalno",
					"cellClass" : "text-center",
					"width" : 150,
					"if" : this.props.resource.status == 'Approved'
				}, {
					"name" : "Voucher Date",
					"key" : "journaldate",
					"cellClass" : "text-center",
					"width" : 150,
					"if" : this.props.resource.status == 'Approved'
				}, {
					"name": "Voucher Status",
					"key": "journalstatus",
					"cellClass": "text-center",
					"width": 150,
					"if" : this.props.resource.status == 'Approved'
				},{
					"name": "Bank",
					"key": "bankid_name",
					"cellClass": "text-center",
					"width": 180
				}, {
					"name": "Location",
					"key": "locationid_name",
					"cellClass": "text-center",
					"width": 180
				}, {
					"name": "Department",
					"key": "departmentid_name",
					"cellClass": "text-center",
					"width": 180
				}, {
					"name": "Pay Structure",
					"key": "paystructureid_name",
					"cellClass": "text-center",
					"width": 200
				}, {
					"name": "Payroll Status",
					"key": "payroll_status",
					"cellClass": "text-center",
					"width": 150
				});

				if (['Approved', 'Completed'].indexOf(this.props.resource.status) >= 0) {
					let paymentNotFound = false;

					for (let i = 0; i < response.data.main.length; i++) {
						if (response.data.main[i].payment_status == 'No') {
							paymentNotFound = true;
							break;
						}
					}

					this.setState({
						payroll_columns
					}, () => {
						this.props.updateFormState(this.props.form, {
							paymentArray: response.data.main,
							originalRows: response.data.main,
							columns: this.state.payroll_columns,
							isPaymentPending: paymentNotFound
						});
					});
				} else
					this.setState({
						payroll_columns
					}, () => {
						this.props.updateFormState(this.props.form, {
							payrollArray: response.data.main,
							componentEarningArray: response.data.componentEarningArray,
							componentDeductionArray: response.data.componentDeductionArray,
							componentEmployerContributionArray: response.data.componentEmployerContributionArray,
							componentRecoveryArray: response.data.componentRecoveryArray,
							originalRows: response.data.main,
							columns: this.state.payroll_columns
						});
					});

				this.props.updateReportFilter(this.props.form, this.props.resource);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	currencyOnChange() {
		if (this.props.resource.currencyid != this.props.app.defaultCurrency) {
			let promise = commonMethods.getCurrencyExchangeRate(this.props.resource.currencyid);
			promise.then((returnRateObject) => {
				this.props.updateFormState(this.props.form, {
					currencyexchangerate: returnRateObject.rate
				});
			}, (reason) => {});
		} else
			this.props.updateFormState(this.props.form, {
				currencyexchangerate: null
			});
	}

	save(param, confirm) {
		this.updateLoaderFlag(true);

		axios({
			method: 'post',
			data: {
				actionverb: param,
				data: this.props.resource,
				ignoreExceptions: confirm ? true : false
			},
			url: '/api/payrollbatch'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message, (resparam) => {
				if (resparam)
					this.save(param, true);
			}));

			if (response.data.message == 'success') {
				let resource = {
					...response.data.main
				};

				delete resource.componentid;
				delete resource.addComponentObj;
				delete resource.addCompAmount;
				delete resource.delete_Param;

				if (this.props.match.path == '/createPayrollBatch')
					this.props.history.replace(`/details/payrollbatch/${response.data.main.id}`);
				else
					this.props.initialize(resource);

				if(this.props.match.path != '/createPayrollBatch')
					setTimeout(this.getPayrollDetails,0);
			}

			this.updateLoaderFlag(false);
		});
	}

	checkboxHeaderOnChange(param) {
		let checkCount = 0;

		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if (param)
				item.ischecked = true;
		});

		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		this.refs.grid.forceRefresh();
	}

	checkboxOnChange(value, item) {
		item.ischecked = value;

		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		this.refs.grid.refresh();
	}

	deleteEmployee() {
		let originalRows = this.props.resource.originalRows,
		itemFound = false,
		finalEmpArray = [],
		empIdArray = [];

		originalRows.forEach((item) => {
			if (item.ischecked) {
				itemFound = true;
				empIdArray.push(item);
			}

			if (!item.ischecked)
				finalEmpArray.push(item);
		});

		if (!itemFound)
			this.props.openModal(modalService['infoMethod']({
				header: 'Error',
				body: 'Please Choose atleast one employee to delete',
				btnArray: ['Ok']
			}));
		else if (empIdArray.length == originalRows.length)
			this.props.openModal(modalService['infoMethod']({
				header: 'Error',
				body: 'Need atleast one Employee',
				btnArray: ['Ok']
			}));
		else {
			let message = {
				header: 'Warning',
				body: 'Do you want to Delete selected employee(s)?',
				btnArray: ['Yes', 'No']
			};

			this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if (param)
					this.props.updateFormState(this.props.form, {
						originalRows: finalEmpArray
					});
			}));
		}
	}

	cbEmployeeDetails() {
		let {
			originalRows
		} = this.props.resource;

		this.props.updateFormState(this.props.form, {
			employeeArray: [...originalRows]
		});

		setTimeout(() => {
			this.save('Save')
		}, 0);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	btnOnClick(item) {
		this.props.createOrEdit('/details/payrolls/:id', item.id, {}, ()=>{this.getPayrollDetails()});
	}

	cbPayRollDetails(param) {
		let originalRows = this.props.resource.originalRows;

		if (param == 'Delete') {
			let itemFound = false,
			payIdArray = [];

			for (let i = 0; i < originalRows.length; i++) {
				if (originalRows[i].ischecked) {
					itemFound = true;
					payIdArray.push(originalRows[i]);
				}
			}

			if (!itemFound)
				this.props.openModal(modalService['infoMethod']({
					header: 'Error',
					body: 'Please Choose atleast one employee payroll to delete',
					btnArray: ['Ok']
				}));
			else if (payIdArray.length == originalRows.length)
				this.props.openModal(modalService['infoMethod']({
					header: 'Error',
					body: 'Need atleast one employee payroll details',
					btnArray: ['Ok']
				}));
			else {
				let message = {
					header: 'Warning',
					body: 'Do you want to Delete selected employee(s)?',
					btnArray: ['Yes', 'No']
				};

				this.props.openModal(modalService['confirmMethod'](message, (param) => {
					if (param) {
						this.props.updateFormState(this.props.form, {
							payrollArray: [...originalRows],
							delete_Param: true
						});

						setTimeout(() => {
							this.save('Save')
						}, 0);
					}
				}));
			}
		} else {
			this.props.updateFormState(this.props.form, {
				payrollArray: [...originalRows]
			});

			setTimeout(() => {
				this.save(param)
			}, 0);
		}
	}

	addComponent () {
		let {
			originalRows
		} = this.props.resource;

		let itemFound = false;
		for (let i = 0; i < originalRows.length; i++) {
			if (originalRows[i].ischecked) {
				itemFound = true;
				break;
			}
		}

		this.props.openModal({
			render: (closeModal) => {
				return <ChildEditModal
					form = {this.props.form}
					history = {this.props.history}
					resource = {this.props.resource}
					ischeckedFound = {itemFound}
					app = {this.props.app}
					updateFormState = {this.props.updateFormState}
					callback = {() => this.cbPayRollDetails('Addcomponent')}
					closeModal = {closeModal}
					getBody = {() => {return <ComponentEditModal />}} />
			}, className: {
				content: 'react-modal-custom-class',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	createJournal(param) {
		let journalItemArray = [], errorFound = false;

		for (let i = 0; i < this.props.resource.originalRows.length; i++) {
			if (this.props.resource.originalRows[i].ischecked)
				journalItemArray.push(this.props.resource.originalRows[i]);
		}
		
		/*if(journalItemArray.length > 50) {
			errorFound=true;
			return this.props.openModal(modalService["infoMethod"]({
				header : "Error",
				body : "Please Choose only 50 Vouchers",
				btnArray : ["Ok"]
				}));
		}*/

		if (!errorFound) {
			if (journalItemArray.length > 0) {

				this.props.openModal({
					render: (closeModal) => {
						return <ChildEditModal
							form = {this.props.form}
							history = {this.props.history}
							resource = {this.props.resource}
							journalItemArray = {journalItemArray}
							app = {this.props.app}
							updateFormState = {this.props.updateFormState}
							openModal = {this.props.openModal}
							closeModal = {closeModal}
							getBody = {() => {return <CreateJournalVoucherModal />}} />
					}, className: {
						content: 'react-modal-custom-class',
						overlay: 'react-modal-overlay-custom-class'
					}
				});
			} else {
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please Choose atleast one item',
					btnArray : ['Ok']
				}));
			}
		}
	}

	getConfirmation() {
		this.props.openModal(modalService['confirmMethod']({
			warningModal : true,
			header: 'Unsaved Changes !!',
			body: 'You have unsaved changes.',
			btnTitle: 'Do you want to leave the page without saving?',
			btnArray: ['Yes', 'No']
		}, (param) => {
			if (param)
				this.props.history.goBack();
		}));
	}

	renderSearchEmployees() {
		let disableclass = this.props.resource && this.props.resource.id ? 'disablediv' : '';

		return (
			<form>
				<div className="bg-white"  style={{marginTop: `48px`}}>
					<div className={`row col-md-12 col-sm-12 col-xs-12 ${disableclass}`}>
						<div className="form-group col-md-12 col-sm-12 col-xs-12 report-header">
							<div className="report-header-title paddingleft-md-30">Payroll Batch Details</div>
						</div>
						<div className="row responsive-form-element col-md-10  offset-md-1 col-sm-12 col-xs-12 margintop-20">
							<div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Numbering Series</label>
								<Field
									name={'numberingseriesmasterid'}
									props = { {
											resource: "numberingseriesmaster",
											fields: "id,name,format,isdefault,currentvalue",
											filter: "numberingseriesmaster.resource='payrollbatch'"
										}
									}
									createParamFlag = {true}
									defaultValueUpdateFn = {
										(value) => this.props.updateFormState(this.props.form, {
											numberingseriesmasterid: value
										})
									}
									component={selectAsyncEle}
									validate={[numberNewValidation({required: true, title : 'Series Type'})]}/>
							</div>
							<div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Payroll Date</label>
								<Field
									name={'payrolldate'}
									props={{required: true}}
									component={DateEle}
									validate={[dateNewValidation({required:  true, title : 'Payroll Date'})]} />
							</div>
							<div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Period Start</label>
								<Field
									name={'periodstart'}
									props={{required: true}}
									component={DateEle}
									validate={[dateNewValidation({required:  true, title : 'Period Start'})]} />
							</div>
							<div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Period End</label>
								<Field
									name={'periodend'}
									props={{
										required: true,
										min: this.props.resource.periodstart
									}}
									component={DateEle}
									validate={[dateNewValidation({required: true, title : 'Period End', min: '{resource.periodstart}'})]} />
							</div>
							<div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Description</label>
								<Field
									name={'description'}
									component={textareaEle}/>
							</div>
							<div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Location</label>
								<Field
									name={'locationid'}
									props={{
										resource: "locations",
										fields: "id,name"
									}}
									component={autoMultiSelectEle}/>
							</div>
							<div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Department</label>
								<Field
									name={'departmentid'}
									props={{
										resource: "departments",
										fields: "id,name"
									}}
									component={autoSelectEle}/>
							</div>
							{ this.props.app.feature.enableCostCenter ? <div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Cost Center</label>
								<Field
									name={'defaultcostcenter'}
									props = {{}}
									component={costcenterAutoMultiSelectEle}
								/>
							</div> : null }
							{ this.props.app.feature.useMultiCurrency ? <div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Currency</label>
								<Field
									name={'currencyid'}
									props = { {
											resource: "currency",
											fields: "id,name",
											onChange: () => {this.currencyOnChange()},
											required: true
										}
									}
									component={selectAsyncEle}
									validate={[numberNewValidation({required: true, title : 'Currency'})]}/>
							</div> : null }
							{ this.props.resource.currencyid != this.props.app.defaultCurrency && this.props.resource.currencyid ? <div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Currency Exchange Rate</label>
								<Field
									name={'currencyexchangerate'}
									props={{required: true}}
									component={NumberEle}
									validate={[numberNewValidation({required: true, title : 'Currency Exchange Rate'})]}/>
							</div> : null }
							<div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Round Off Method</label>
								<Field
									name={'roundoffmethod'}
									props={{
										options: [{value: "none", label: "None"}, {value: "up", label: "Up"}, {value: "down", label: "Down"}, {value: "even", label: "Even"}],
										label : "label",
										valuename: "value",
										required: true
									}}
									component={localSelectEle}
									validate={[stringNewValidation({required:  true, title : 'Round Off Method'})]} />
							</div>
							<div className="form-group col-md-6 col-sm-12">
								<label className="labelclass">Payroll Numbering Series</label>
								<Field
									name={'payrollnumberingseriesid'}
									props = { {
											resource: "numberingseriesmaster",
											fields: "id,name,format,isdefault,currentvalue",
											filter: "numberingseriesmaster.resource='payrolls'"
										}
									}
									createParamFlag = {true}
									defaultValueUpdateFn = {
										(value) => this.props.updateFormState(this.props.form, {
											payrollnumberingseriesid: value
										})
									}
									component={selectAsyncEle}
									validate={[numberNewValidation({required: true, title : 'Payroll Series Type'})]}/>
							</div>
						</div>
					</div>
					<div className="row" style={{paddingTop: '40px'}}>
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center sticky-footer" style={{width: '100%', zIndex: '100'}}>
								<button
									type="button"
									className="btn btn-sm btn-width btn-secondary"
									onClick={() => this.getConfirmation()}>
									<i className="fa fa-times marginright-5"></i>Close
								</button>
								<button
									type="button"
									onClick={() => {
										this.getEmployeeDetails()   
									}}
									className="btn btn-width btn-sm gs-btn-success"
									disabled={!this.props.valid}>
									<i className="fa fa-chevron-right"></i>Next
								</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		);
	}

	renderReviewAttendance () {

		if(!this.props.resource.columns || this.props.resource.columns.length == 0)
			return null;

		return (
			<form>
				<div className="row bg-white margintop-30">
					<div className="col-md-12 col-sm-12 col-xs-12 margintop-15" style={{marginBottom: '50px'}}>
						{this.props.resource ? <Reactuigrid excelname={'Batch Payroll (Attendance Details)'} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} /> : null }
					</div>
					<div className="row" style={{paddingTop: '40px'}}>
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center sticky-footer" style={{width: '100%', zIndex: '100'}}>
								<button
									type="button"
									className="btn btn-sm btn-width btn-secondary"
									onClick = {
										() => this.props.updateFormState(this.props.form, {
											employeeArray: [],
											originalRows : []
										})
									}>
									<i className="fa fa-times marginright-5"></i>Close
								</button>
								{ this.props.resource && checkActionVerbAccess(this.props.app, 'payrollbatch', 'Save') ? <button
									type="button"
									onClick={() => {
										this.cbEmployeeDetails()   
									}}
									className="btn btn-width btn-sm gs-btn-success"
									disabled={!this.props.valid}>
									<i className="fa fa-save"></i>Save
								</button> : null }
							</div>
						</div>
					</div>
				</div>
			</form>
		);
	}

	renderReviewPayroll () {

		if(!this.props.resource.columns || this.props.resource.columns.length == 0)
			return null;

		return (
			<form>
				<div className="row bg-white margintop-30">
					<div className="col-md-12 col-sm-12 col-xs-12 margintop-15" style={{marginBottom: '50px'}}>
						{this.props.resource ? <Reactuigrid excelname={'Batch Payroll (Payroll Details)'} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} btnOnClick={this.btnOnClick} /> : null }
					</div>
					<div className="row" style={{paddingTop: '40px'}}>
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center sticky-footer" style={{width: '100%', zIndex: '100'}}>
								<button
									type="button"
									className="btn btn-sm btn-width btn-secondary"
									onClick={() => this.props.history.goBack()}>
									<i className="fa fa-times marginright-5"></i>Close
								</button>
								{ this.props.resource && this.props.resource.status == 'Draft' && checkActionVerbAccess(this.props.app, 'payrollbatch', 'Save') ? <button
									type="button"
									onClick={() => {
										this.cbPayRollDetails('Save')
									}}
									className="btn btn-width btn-sm gs-btn-success"
									disabled={!this.props.valid}>
									<i className="fa fa-save"></i>Save
								</button> : null }
								{ this.props.resource && (this.props.resource.status == 'Draft' || this.props.resource.status == 'Revise')&& checkActionVerbAccess(this.props.app, 'payrollbatch', 'Submit') ? <button
									type="button"
									onClick={() => {
										this.cbPayRollDetails('Submit')
									}}
									className="btn btn-width btn-sm gs-btn-success"
									disabled={!this.props.valid}>
									<i className="fa fa-check"></i>Submit
								</button> : null }
								{ this.props.resource && this.props.resource.status == 'Submitted' && checkActionVerbAccess(this.props.app, 'payrollbatch', 'Revise') ? <button
									type="button"
									onClick={() => {
										this.cbPayRollDetails('Revise')
									}}
									className="btn btn-width btn-sm gs-btn-success"
									disabled={!this.props.valid}>
									<i className="fa fa-check"></i>Revise
								</button> : null }
								{ this.props.resource && ['Draft', 'Submitted'].indexOf(this.props.resource.status) >= 0 && checkActionVerbAccess(this.props.app, 'payrollbatch', 'Approve') ? <button
									type="button"
									onClick={() => {
										this.cbPayRollDetails('Approve')
									}}
									className="btn btn-width btn-sm gs-btn-success"
									disabled={!this.props.valid}>
									<i className="fa fa-check"></i>Approve
								</button> : null }
							</div>
						</div>
					</div>
				</div>
			</form>
		);
	}

	renderReviewPayment () {

		if(!this.props.resource.columns || this.props.resource.columns.length == 0)
			return null;

		return (
			<form>
				<div className="row bg-white margintop-30">
					<div className="col-md-12 col-sm-12 col-xs-12 margintop-15" style={{marginBottom: '50px'}}>
						{ this.props.resource ? <Reactuigrid excelname={'Batch Payroll (Payment Details)'} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} btnOnClick={this.btnOnClick} /> : null }
					</div>
					<div className="row" style={{paddingTop: '40px'}}>
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center sticky-footer" style={{width: '100%', zIndex: '100'}}>
								<button
									type="button"
									className="btn btn-sm btn-width btn-secondary"
									onClick={() => this.props.history.goBack()}>
									<i className="fa fa-times marginright-5"></i>Close
								</button>
								{ this.props.resource && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && this.props.resource.status == 'Approved' && checkActionVerbAccess(this.props.app, 'journals', 'Save') && this.props.resource.isPaymentPending ? <button
									type="button"
									onClick={this.createJournal}
									className="btn btn-width btn-sm gs-btn-success"
									disabled={!this.props.valid}>
									<i className="fa fa-plus"></i>Journal
								</button> : null }
								{ this.props.resource && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && this.props.resource.status == 'Approved' && checkActionVerbAccess(this.props.app, 'payrollbatch', 'Complete') ? <button
									type="button"
									onClick={() => {
										this.cbPayRollDetails('Complete')
									}}
									className="btn btn-width btn-sm gs-btn-success"
									disabled={!this.props.valid}>
									<i className="fa fa-check"></i>Complete
								</button> : null }
								{ this.props.resource && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && this.props.resource.status == 'Completed' && checkActionVerbAccess(this.props.app, 'payrollbatch', 'Reopen') ? <button
									type="button"
									onClick={() => {
										this.cbPayRollDetails('Reopen')
									}}
									className="btn btn-width btn-sm gs-btn-success"
									disabled={!this.props.valid}>
									<i className="fa fa-check"></i>Reopen
								</button> : null }
							</div>
						</div>
					</div>
				</div>
			</form>
		);
	}

	render() {

		if(!this.props.resource)
			return null;

		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="bg-white col-md-12 col-sm-12 col-md-12" style={{position: 'fixed', height: '55px',  zIndex: 5}}>
						<div className="form-group col-md-12 report-header">
							<div className="report-header-title">
								{this.props.match.path == '/createPayrollBatch' ? 'New Payroll Batch' : <div style={{textTransform : 'none'}}>
									{this.props.resource ? <div style={{color:'#000'}}>
										PAYROLL BATCH: {this.props.resource.payrollbatchno}
										<label className="badge gs-badge-success marginleft-10">{this.props.resource.status}</label>
									</div> : null}
								</div>}
							</div>
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <div className="report-header-rightpanel">
								{ this.props.resource && this.props.resource.employeeArray.length > 0 && this.props.resource.payrollArray.length == 0 && this.props.resource.paymentArray.length == 0 ? <button type="button" className="btn gs-form-btn-danger btn-sm btn-width marginleft-5" onClick={()=>this.deleteEmployee()}>
									<i className="fa fa-trash-o"></i>Delete
								</button> : null }
								{ this.props.resource && this.props.resource.payrollArray.length > 0 && this.props.resource.paymentArray.length == 0 && this.props.resource.status == 'Draft' ? <button type="button" className="btn gs-form-btn-danger btn-sm btn-width marginleft-5" onClick={()=>this.cbPayRollDetails('Delete')}>
									<i className="fa fa-trash-o"></i>Delete
								</button> : null }
								{(['Draft', 'Revise', 'Submitted'].indexOf(this.props.resource.status) >= 0) ? 
								<button type="button" className="btn gs-form-btn-success btn-sm btn-width marginleft-5" onClick={() => this.addComponent()}>
									<i className="fa fa-plus "></i>Add / Update Component
								</button> : null}
								<button type="button" className="btn gs-form-btn-success btn-sm btn-width marginleft-5" onClick={() => this.refs.grid.exportExcel()}>
									<i className="fa fa-file-excel-o"></i>Export Data
								</button>
							</div> : null}
						</div>
					</div>
				</div>
				<div className="col-md-12 col-sm-12 col-xs-12">
					<div className="col-md-12 col-sm-12 col-xs-12" style={{marginTop:'70px'}}>
						<div className="form-bootstrapWizard">
							<ul className="bootstrapWizard form-wizard">
								<li className={`${(this.props.resource.employeeArray.length > 0 || this.props.resource.payrollArray.length > 0 || this.props.resource.paymentArray.length > 0) ? 'active' : (this.props.resource.employeeArray.length == 0 ? 'current' : '')}`}>
									<div>{(this.props.resource.employeeArray.length > 0 || this.props.resource.payrollArray.length > 0 || this.props.resource.paymentArray.length > 0) ? <span className="step active fa fa-check"></span> : <span className={`${this.props.resource.employeeArray.length == 0 ? 'current' : ''} step`}>1</span>}
									<span className={`${(this.props.resource.employeeArray.length > 0 || this.props.resource.payrollArray.length > 0 || this.props.resource.paymentArray.length > 0) ? 'active' : (this.props.resource.employeeArray.length == 0 ? 'current' : '')} title`}>Select Employees</span></div>
								</li>
								<li className={`${(this.props.resource.payrollArray.length > 0 || this.props.resource.paymentArray.length > 0)? 'active' : (this.props.resource.employeeArray.length > 0 ? 'current' : '')}`}>
									<div>{(this.props.resource.payrollArray.length > 0 || this.props.resource.paymentArray.length > 0) ? <span className="step active fa fa-check"></span> : <span className={`${(this.props.resource.employeeArray.length > 0 ? 'current' : '')} step`}>2</span>}
									<span className={`${(this.props.resource.payrollArray.length > 0 || this.props.resource.paymentArray.length > 0)? 'active' : (this.props.resource.employeeArray.length > 0 ? 'current' : '')} title`}>Review Attendance</span></div>
								</li>
								<li className={`${(this.props.resource.paymentArray.length > 0 || this.props.resource.status == 'Completed') ? 'active' : (this.props.resource.id && this.props.resource.paymentArray.length == 0 ? 'current' : '')}`}>
									<div>{(this.props.resource.paymentArray.length > 0 || this.props.resource.status == 'Completed') ? <span className="step active fa fa-check"></span> : <span className={`${(this.props.resource.id && this.props.resource.paymentArray.length == 0 ? 'current' : '')} step`}>3</span>}
									<span className={`${(this.props.resource.paymentArray.length > 0 || this.props.resource.status == 'Completed') ? 'active' : (this.props.resource.id && this.props.resource.paymentArray.length == 0 ? 'current' : '')} title`}>Review Payroll</span></div>
								</li>
								<li  className={`${((this.props.resource.paymentArray.length > 0 && !this.props.resource.isPaymentPending) || this.props.resource.status == 'Completed') ? 'active' : (this.props.resource.status == 'Approved' && this.props.resource.isPaymentPending ? 'current' : '')}`}>
									<div>{((this.props.resource.paymentArray.length > 0 && !this.props.resource.isPaymentPending) || this.props.resource.status == 'Completed') ? <span className="step active fa fa-check"></span> : <span className={`${(this.props.resource.status == 'Approved' && this.props.resource.isPaymentPending ? 'current' : '')} step`}>4</span>}
									<span className={`${((this.props.resource.paymentArray.length > 0 && !this.props.resource.isPaymentPending) || this.props.resource.status == 'Completed') ? 'active' : (this.props.resource.status == 'Approved' && this.props.resource.isPaymentPending ? 'current' : '')} title`}>Make Payment</span></div>
								</li>
							</ul>
							<div className="clearfix"></div>
						</div>
					</div>
					{ (this.props.resource && this.props.resource.employeeArray.length == 0 && this.props.resource.payrollArray.length == 0 && this.props.resource.paymentArray.length == 0) ? this.renderSearchEmployees() : null}
					{ (this.props.resource && this.props.resource.employeeArray.length > 0 && this.props.resource.payrollArray.length == 0 && this.props.resource.paymentArray.length == 0) ? this.renderReviewAttendance() : null}
					{ (this.props.resource && this.props.resource.payrollArray.length > 0 && this.props.resource.paymentArray.length == 0 && this.props.resource.status != 'Completed') ? this.renderReviewPayroll() : null}
					{ ((this.props.resource && this.props.resource.paymentArray.length > 0) || this.props.resource.status == 'Completed') ? this.renderReviewPayment() : null}
				</div>
		   </div>
		);
	}
}

class ComponentEditModal extends Component {
	constructor(props) {
		super(props);

		this.state = {};

		this.update = this.update.bind(this);
		this.callBackComponent = this.callBackComponent.bind(this);
		this.callBackAmount = this.callBackAmount.bind(this);
		this.callBackItemAmount = this.callBackItemAmount.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);

		let tempObj = this.props.resource;

		delete tempObj.componentid;
		delete tempObj.addComponentObj;
		delete tempObj.addCompAmount;

		tempObj.originalRows.forEach((item) => {
			if (item.addCompObjValue) {
				delete item.addCompObjValue;
				delete item.isValueEdit;
			}
		});

		this.props.updateFormState(this.props.form, tempObj);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	callBackComponent(value, valueobj) {
		let addComponentObj = {
			componentid: valueobj.id,
			name: valueobj.name,
			type: valueobj.type,
			componentid_calculationtype: valueobj.calculationtype,
			tdsapplicable: valueobj.tdsapplicable,
			taxexempt: valueobj.taxexempt,
			pfapplicable: valueobj.pfapplicable,
			esiapplicable: valueobj.esiapplicable,
			utilitymoduleid: valueobj.utilitymoduleid,
			componentid_accountid: valueobj.accountid,
			componentid_accrualaccountid: valueobj.accrualaccountid,
			componentid_roundoffmethod: valueobj.roundoffmethod,
			value: null,
			defaultvalue: null,
			ismanuallyoverride: false
		};

		this.props.updateFormState(this.props.form, {
			addComponentObj,
			addCompAmount: null
		});

		setTimeout(() => {
			this.callBackAmount(null)
		}, 0);
	}

	callBackAmount(value) {
		let {
			originalRows
		} = this.props.resource;

		if (this.props.ischeckedFound)
			originalRows.forEach((item) => {
				if (item.ischecked)
					item['addCompObjValue'] = !item['isValueEdit'] ? (item[this.props.resource.componentid] ? item[this.props.resource.componentid] : value) : item['addCompObjValue'];
			});
		else
			originalRows.forEach((item) => {
				item['addCompObjValue'] = !item['isValueEdit'] ? (item[this.props.resource.componentid] ? item[this.props.resource.componentid] : value) : item['addCompObjValue'];
			});

		this.props.updateFormState(this.props.form, {
			payrollArray: [...originalRows],
			originalRows
		});
	}

	callBackItemAmount(index) {
		this.props.updateFormState(this.props.form, {
			[`originalRows[${index}].isValueEdit`]: true
		});
	}

	update() {
		let {
			originalRows
		} = this.props.resource;

		this.props.updateFormState(this.props.form, {
			payrollArray: [...originalRows]
		});

		setTimeout(() => {
			this.props.closeModal();
			this.props.callback();
		}, 0);
	}

	render() {

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title gs-text-color">Add or Update Component</h5>
				</div>
				<div className="row react-modal-body">
					<div className="row responsive-form-element col-md-12 col-sm-12 col-xs-12 margintop-20">
						<div className="form-group col-md-6 col-sm-12">
							<label className="labelclass">Component</label>
							<Field
								name={'componentid'}
								props = { {
										resource: "payrollcomponents",
										fields: "id,name,type,category,statutorytype,accountid,accrualaccountid,calculationtype,tdsapplicable,taxexempt,pfapplicable,esiapplicable,utilitymoduleid,roundoffmethod",
										filter: "payrollcomponents.calculationtype='Manual' AND payrollcomponents.type != 'Recovery'",
										onChange: (value, valueobj) => {this.callBackComponent(value, valueobj)},
										required: true
									}
								}
								component={selectAsyncEle}
								validate={[numberNewValidation({required: true, title : 'Component'})]}/>
						</div>
						<div className="form-group col-md-6 col-sm-12">
							<label className="labelclass">Amount</label>
							<Field
								name={'addCompAmount'}
								props={{
									onChange: (value) => {this.callBackAmount(value)}
								}}
								component={NumberEle}/>
						</div>
					</div>
					<div className="col-md-12 col-sm-12 col-xs-12 margintop-20" style={{overflowY:'auto',maxHeight:'400px'}}>
						<table className="table-condensed" style={{lineHeight: '45px',
border: '1px solid #ddd'}}>
							<thead className='card-header'>
								<tr>
									<th className="text-center" style={{width: '70%'}}>Employee Name</th>
									<th className="text-center">Amount</th>
								</tr>
							</thead>
							<tbody>
								{ this.props.ischeckedFound && this.props.resource.originalRows && this.props.resource.originalRows.map((item, index) => {
									if(item.ischecked)
										return (
											<tr key={index}>
												<td className="text-center" style={{wordWrap: 'break-word',wordBreak: 'break-all'}}>{item.displayname}</td>
												<td className="text-right" style={{paddingRight: '10px'}}>
													<Field
														name={`originalRows[${index}].addCompObjValue`}
														props={{
															required: true,
															onChange: () => {this.callBackItemAmount(index)}
														}}
														component={NumberEle}
														validate={[numberNewValidation({required: true, title : 'Amount'})]}/>
												</td>
											</tr>
										)
								})}
								{ !this.props.ischeckedFound && this.props.resource.originalRows && this.props.resource.originalRows.map((item, index) => {
									return (
										<tr key={index}>
											<td className="text-center" style={{wordWrap: 'break-word',wordBreak: 'break-all'}}>{item.displayname}</td>
											<td className="text-right" style={{paddingRight: '10px'}}>
												<Field
													name={`originalRows[${index}].addCompObjValue`}
													props={{
														required: true,
														onChange: () => {this.callBackItemAmount(index)}
													}}
													component={NumberEle}
													validate={[numberNewValidation({required: true, title : 'Amount'})]}/>
											</td>
										</tr>
									)
								})}
							</tbody>
						</table>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-right">
								<button
									type="button"
									className="btn btn-sm btn-width btn-secondary"
									onClick={this.props.closeModal}>
									<i className="fa fa-times marginright-5"></i>Close
								</button>
						 		<button
									type="button"
									disabled={!this.props.valid}
									onClick={()=>{this.update()}}
									className="btn btn-sm btn-width gs-btn-success"><i className="fa fa-save"></i>Add</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class CreateJournalVoucherModal extends Component {
	constructor(props) {
		super(props);

		this.state = {};

		this.callBasedOn = this.callBasedOn.bind(this);
		this.callPaymentMode = this.callPaymentMode.bind(this);
		this.createJournalVoucher = this.createJournalVoucher.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);

		let tempObj = this.props.resource;

		delete tempObj.journalbasedon;
		delete tempObj.paymentmodeid;
		delete tempObj.instrumentno;
		delete tempObj.instrumentdate;
		delete tempObj.journalaccountid;
		delete tempObj.journalaccountid_group;

		this.props.updateFormState(this.props.form, tempObj);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	callBasedOn () {
		this.props.updateFormState(this.props.form, {
			paymentmodeid: null,
			instrumentno: null,
			instrumentdate: null,
			journalaccountid: null,
			journalaccountid_group: null
		});
	}

	callPaymentMode (value, valueobj) {
		this.props.updateFormState(this.props.form, {
			paymentmodeid_name: valueobj.name,
			journalaccountid: valueobj.accountid,
			journalaccountid_group: valueobj.accountid_accountgroup
		});
	}

	createJournalVoucher() {
		let voucherArray = [],
			totalOutstandingAmount = 0,
			errorFound = false;

		this.props.journalItemArray.forEach((item) => {
			if (item.outstandingamount > 0) {
				totalOutstandingAmount += item.outstandingamount;

				voucherArray.push({
					accountid: item.emppayableaccountid,
					employeeid: item.employeeid,
					employeeid_displayname: item.displayname,
					currencyid: item.currencyid,
					debit: item.outstandingamount,
					vouchertype: 'Pay Roll',
					payrollid: item.id,
					payrollno: item.payrollno,
					payrolldate: item.payrolldate,
					payrollbankid: item.payrollbankid,
					remarks: `Payment against Payroll Batch: ${this.props.resource.payrollbatchno}`
				});
			}
		});

		let tempObj = {
			accountid: this.props.resource.journalaccountid,
			credit: totalOutstandingAmount,
			vouchertype: 'Pay Roll',
			currencyid: this.props.resource.currencyid,
			defaultcostcenter: this.props.resource.defaultcostcenter,
			remarks: `Payment against Payroll Batch: ${this.props.resource.payrollbatchno}`
		};

		if (this.props.resource.journalaccountid_group && this.props.resource.journalaccountid_group == 'Bank') {
			if (!this.props.resource.instrumentno || !this.props.resource.instrumentdate)
				errorFound = true;
			else {
				tempObj['instrumentno'] = this.props.resource.instrumentno;
				tempObj['instrumentdate'] = this.props.resource.instrumentdate;
			}
		}

		voucherArray.splice(0, 0, tempObj);

		if (errorFound)
			this.props.openModal(modalService["infoMethod"]({
				header : "Error",
				body : `Instrument No & Instrument Date is required for Payment Mode : ${this.props.resource.paymentmodeid_name}`,
				btnArray : ["Ok"]
			}));
		else {
			this.props.closeModal();
			this.props.history.push({
				pathname: '/createJournalVoucher',
				params: {
					voucherArray : voucherArray,
					param: 'Payroll Batch'
				}
			});
		}
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title gs-text-color">Create Journal</h5>
				</div>
				<div className="row react-modal-body">
					<div className="row responsive-form-element col-md-12 col-sm-12 col-xs-12 margintop-20">
						<div className="form-group col-md-10 offset-md-1 col-sm-12">
							<label className="labelclass">Journal (Based On)</label>
							<Field
								name={`journalbasedon`}
								props={{
									options: ["Transfer from bank", "Transfer to Account"],
									onChange: this.callBasedOn,
									required: true
								}}
								component={localSelectEle}
								validate={[stringNewValidation({required: true, model: 'Journal (Based On)'})]}/>
						</div>
						{ this.props.resource.journalbasedon == 'Transfer from bank' ? <div className="form-group col-md-10 offset-md-1 col-sm-12">
							<label className="labelclass">Payment Mode</label>
							<Field
								name={'paymentmodeid'}
								props = { {
										resource: "paymentmodes",
										fields: "id,name,accountid,allowpayment,allowreceipt,description,accounts/accountgroup/accountid",
										onChange: (value, valueobj) => {this.callPaymentMode(value, valueobj)},
										required: true
									}
								}
								component={selectAsyncEle}
								validate={[numberNewValidation({required: true, title : 'Component'})]}/>
						</div> : null }
						{ this.props.resource.journalbasedon == 'Transfer from bank' ? <div className="form-group col-md-10 offset-md-1 col-sm-12">
							<label className="labelclass">Instrument No</label>
							<Field
								name={'instrumentno'}
								component={InputEle}
								validate={[stringNewValidation({required: '{resource.journalaccountid_group && resource.journalaccountid_group == "Bank"}', title : 'Instrument No'})]}/>
						</div> : null }
						{ this.props.resource.journalbasedon == 'Transfer from bank' ? <div className="form-group col-md-10 offset-md-1 col-sm-12">
							<label className="labelclass">Instrument Date</label>
							<Field
								name={'instrumentdate'}
								component={DateEle}
								validate={[dateNewValidation({required: '{resource.journalaccountid_group && resource.journalaccountid_group == "Bank"}', title : 'Instrument Date'})]}/>
						</div> : null }
						{ this.props.resource.journalbasedon == 'Transfer to Account' ? <div className="form-group col-md-10 offset-md-1 col-sm-12">
							<label className="labelclass">Account</label>
							<Field
								name={'journalaccountid'}
								props = { {
										resource: "accounts",
										fields: "id,name,type,accountgroup",
										filter: "accounts.isledger and ((accounts.accountgroup not in ('Bank','Cash','Receivable','Payable','Employee Receivable','Employee Payable')) or accounts.accountgroup is null)",
										required: true
									}
								}
								component={selectAsyncEle}
								validate={[numberNewValidation({required: true, title : 'Account'})]}/>
						</div>: null}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-right">
								<button
									type="button"
									className="btn btn-sm btn-width btn-secondary"
									onClick={this.props.closeModal}>
									<i className="fa fa-times marginright-5"></i>Close
								</button>
								<button
									type="button"
									disabled={!this.props.valid}
									onClick={()=>{this.createJournalVoucher()}}
									className="btn btn-sm btn-width gs-btn-success"><i className="fa fa-save"></i>Proceed</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

PayrollBatch = connect(
	(state, props) => {
		let formName = 'PayrollBatch';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(PayrollBatch));

export default PayrollBatch;
