import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { reduxForm, Field,formValueSelector } from 'redux-form';
import { Prompt } from "react-router-dom";
import { updateFormState, updateReportFilter } from '../actions/actions';
import { commonMethods, modalService,generateExcel } from '../utils/services';
import { LocalSelect, SelectAsync, AutoSelect, DateElement, ChildEditModal, AutoSuggest ,AutoMultiSelect} from '../components/utilcomponents';
import Loadingcontainer from '../components/loadingcontainer';
import { InputEle, localSelectEle, autoSelectEle, checkboxEle, NumberEle, DateEle, selectAsyncEle, dateTimeEle, autosuggestEle } from '../components/formelements';
import {stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, requiredNewValidation, checkActionVerbAccess, checkArray } from '../utils/utils';
import {currencyFilter,dateFilter} from '../utils/filter';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class PerformanceTargetReport extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag: false,
			userRows: [],
			performancetargets : [],
			allowedtargets : []
		};

		this.initialiseState();

		this.renderTarget = this.renderTarget.bind(this);
		this.openViewMoreModal = this.openViewMoreModal.bind(this);
		this.targetperiod = this.targetperiod.bind(this);
		this.ptOnChange = this.ptOnChange.bind(this);
		this.targetValueOnChange = this.targetValueOnChange.bind(this);
		this.renderTable = this.renderTable.bind(this);
		this.renderTitleTd = this.renderTitleTd.bind(this);
		this.toggleTableRow = this.toggleTableRow.bind(this);
		this.getTargets = this.getTargets.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
		this.dateOnChange = this.dateOnChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag)
			this.updateLoaderFlag(false);
	}

	initialiseState() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				currencyid : this.props.app.defaultCurrency,
				companyid : this.props.app.selectedcompanyid,
				year : new Date().getFullYear(),
				month : new Date().getMonth()
			};
		
		this.props.initialize(tempObj);

		setTimeout(() => {
			if(this.props.reportdata) {
				this.ptOnChange(this.props.resource.performancetargetid, {
					name: this.props.resource.performancetargetid_name
				});
			}
			this.getTargets();
		}, 0);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	exportExcel(){
		this.updateLoaderFlag(true);
		let coldefArray = [{
			"name" : "Group",
			"key" : "name",
			"cellClass" : "text-center",
			"width" : 150
		}, {
			"name" : "Current Target",
			"key" : "currenttarget",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Current Achieved",
			"key" : "currenttargetcompleted",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Current Achieved%",
			"key" : "currenttargetcompletedpercentage",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Cumulative Target",
			"key" : "cummulativetarget",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Cumulative Achieved",
			"key" : "cumulativetargetcompleted",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Cumulative Achieved%",
			"key" : "cumulativetargetcompletedpercentage",
			"cellClass" : "text-center",
			"width" : 150
		}];

		let exceldata = this.state.userRows;
		exceldata.forEach((data)=>{
			let prefix = '';
			for(let i=0;i<data.level;i++){
				prefix += '-';
			}
			data.name = `${prefix} ${data.groupname || data.userid_displayname}`;
		});
		generateExcel(this.props.resource.performancetargetid_name, coldefArray,exceldata, this.props.app, () => {
			this.updateLoaderFlag(false);
		});
	}

	getTargets(){
		this.updateLoaderFlag(true);
		axios.get(`/api/query/getperformancetargetsquery`).then((response) => {
			if (response.data.message == 'success') {
				let allowedtargets = response.data.main;
				this.setState({ allowedtargets });
				this.dateOnChange(true);
			} else {
				this.updateLoaderFlag(false);
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	dateOnChange(firsttime){
		let performancetargets = [];
		if(this.props.resource.month >=0 && this.props.resource.year && (this.props.resource.year).toString().length == 4 ){
			let year = Number(this.props.resource.year),month = Number(this.props.resource.month);
			let startDate = new Date(year, month, 1).toDateString();
			let endDate = new Date(year, month + 1, 0).toDateString();

			this.state.allowedtargets.forEach((target)=>{
				if((new Date(startDate) <= new Date(target.targetend) && new Date(endDate) >= new Date(target.targetend))  || (new Date(endDate) >= new Date(target.targetstart) && new Date(endDate) <= new Date(target.targetend))){
					performancetargets.push(target);
				}	
			});
		}
		if(firsttime){
			this.setState({ performancetargets});
		}else{
			this.setState({ performancetargets,userRows:[] });
			this.props.updateFormState(this.props.form, {
				performancetargetid : null,
				performancetargetid_name: null
			});
		}
	}

	listbodyRef(node) {
		if(node) {
			node.addEventListener('scroll', (evt) => {
				this.refs.listheader.scrollLeft = evt.srcElement.scrollLeft;
			})
		}
	}

	toggleTableRow(item, itemkey) {
		this.updateLoaderFlag(true);
		let userRows = [...this.state.userRows];

		item.hideChildren = item.hideChildren ? false : true;

		let recursiveDeleteFn = (index) => {
			for (var i = 0; i < userRows.length; i++) {
				if (index == userRows[i].rootindex) {
					userRows[i].hide = item.hideChildren;
					if (userRows[i].type == 'group' && !userRows[i].hideChildren)
						recursiveDeleteFn(userRows[i].index);
				}
			}
		}

		recursiveDeleteFn(item.index);
		this.updateLoaderFlag(false);
	}

	targetperiod(target){
		let period = [],currentperiod = '';
		let monthObj = {
			'0' : 'Jan','1' : 'Feb','2' : 'Mar','3' : 'Apr','4' : 'May','5' : 'Jun','6' : 'Jul','7' : 'Aug','8' : 'Sep','9' : 'Oct','10' : 'Nov','11' : 'Dec'
		};
		let startdate = new Date(target.targetstart);
		let startmonth = new Date(target.targetstart).getMonth();
		let startyear = new Date(target.targetstart).getFullYear();
		let enddate = new Date(target.targetend);
		let endmonth = new Date(target.targetend).getMonth();
		let endyear = new Date(target.targetend).getFullYear();

		let startyearShort = `${startyear}`;
		startyearShort = startyearShort.substr(startyearShort.length - 2, startyearShort);
		let endyearShort = `${endyear}`;
		endyearShort = endyearShort.substr(endyearShort.length - 2, endyearShort);

		if(startmonth == endmonth && startyear == endyear){
			return `${monthObj[startmonth]} ${startyear}`;
		}

		return `${monthObj[startmonth]} ${startyearShort} - ${monthObj[endmonth]} ${endyearShort}`;
	}

	ptOnChange(value, valueObj) {
		this.props.updateFormState(this.props.form, {
			performancetargetid_name: valueObj.name
		});
		this.setState({
			userRows: []
		});
		if(!this.props.resource.performancetargetid)
			return null;

		this.updateLoaderFlag(true);

		axios.get(`/api/query/getperformancetargetreportquery?targetid=${this.props.resource.performancetargetid}`).then((response) => {
			if (response.data.message == 'success') {
				let targetItemsHeader = response.data.main.performancetargetitems[0].perioditems.map(item => item.targetperiod);
				let userRows = commonMethods.sortUserRowsPerformanceTarget(response.data.main.performancetargetitems, targetItemsHeader, true);

				this.targetValueOnChange(userRows);

				this.props.updateReportFilter(this.props.form, this.props.resource);

				this.setState({ userRows });
			} else {
				this.updateLoaderFlag(false);
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	targetValueOnChange(targetitems) {
		let leafNodes = {};
		let recursiveLeafNodes = (index) => {
			targetitems.forEach((secitem, secitemindex) => {
				if(secitem.rootindex == index) {
					if(secitem.type == 'group')
						recursiveLeafNodes(secitem.index);
					else
						leafNodes[secitemindex] = 1;
				}
			});
		};

		let highestLevel = 0;

		targetitems.forEach((item, itemindex) => {
			if(item.level > highestLevel)
				highestLevel = item.level;
		});

		for (var j=highestLevel; j >= 0; j--) {
			for(var k=0; k<targetitems.length; k++) {
				if(targetitems[k].level == j && targetitems[k].type == 'group') {
					let childFound = false;

					for (var m=0; m < targetitems.length; m++) {
						if(targetitems[m].rootindex == targetitems[k].index) {
							childFound = true;
							break;
						}
					}

					if(!childFound) {
						targetitems.splice(k, 1);
						k = k - 1;
					}
				}
			}
		}

		targetitems.forEach((item, itemindex) => {
			if(item.type == 'group')
				recursiveLeafNodes(item.index);
			else
				leafNodes[itemindex] = 1;
		});

		let recursivePTValue = (childitem, lastchilditem) => {
			targetitems.forEach(secitem => {
				if(secitem.index == childitem.rootindex && secitem.type == 'group') {
					secitem.totaltarget += lastchilditem.totaltarget;
					secitem.targetcompleted += lastchilditem.totaltargetcompleted;
					secitem.currenttarget += lastchilditem.currenttarget;
					secitem.currenttargetcompleted += lastchilditem.currenttargetcompleted;
					secitem.cummulativetarget += lastchilditem.cummulativetarget;
					secitem.cumulativetargetcompleted += lastchilditem.cumulativetargetcompleted;

					if(secitem.rootindex > 0)
						recursivePTValue(secitem, lastchilditem);
				}
			});
		}

		targetitems.forEach((item, itemindex) => {
			if(leafNodes[itemindex]) {
				if(item.type == 'group') {
					item.totaltarget = 0;
					item.totaltargetcompleted = 0;
					item.currenttarget = 0;
					item.currenttargetcompleted = 0;
					item.cummulativetarget = 0;
					item.cumulativetargetcompleted = 0;
				}
				if(item.rootindex > 0) {
					targetitems.forEach(secitem => {
						if(secitem.index == item.rootindex && secitem.type == 'group') {
							secitem.totaltarget += item.totaltarget;
							secitem.targetcompleted += item.totaltargetcompleted;
							secitem.currenttarget += item.currenttarget;
							secitem.currenttargetcompleted += item.currenttargetcompleted;
							secitem.cummulativetarget += item.cummulativetarget;
							secitem.cumulativetargetcompleted += item.cumulativetargetcompleted;

							if(secitem.rootindex > 0)
								recursivePTValue(secitem, item);
						}
					});
				}
			}
		});

		targetitems.forEach((item, itemindex) => {
			item.targetcompletedpercentage = item.totaltarget>0 ? Number(((item.totaltargetcompleted / item.totaltarget) * 100).toFixed(this.props.app.roundOffPrecision)) : 0;
			item.currenttargetcompletedpercentage = item.currenttarget ? Number(((item.currenttargetcompleted / item.currenttarget) * 100).toFixed(this.props.app.roundOffPrecision)) :0;
			item.cumulativetargetcompletedpercentage = item.cummulativetarget > 0 ? Number(((item.cumulativetargetcompleted / item.cummulativetarget) * 100).toFixed(this.props.app.roundOffPrecision)) : 0;
		});
	}

	cancel() {
		this.props.history.goBack();
	}

	openViewMoreModal(target){
		let getBody = (closeModal) => {
			return (
				<OpenViewMoreDetails />
			)
		}
		this.props.openModal({render: (closeModal) => {return <ChildEditModal form={this.props.form} app={this.props.app} target={target} resource={this.props.resource} updateFormState={this.props.updateFormState} openModal={this.props.openModal} callback={this.addItem} closeModal={closeModal} getBody={getBody} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'},confirmModal: true});
	}

	renderTarget() {
		let {targets} = this.state;

		return Object.keys(targets).map((targetid, index) => {
			let target = targets[targetid];
			return (
				<div key={index} className="form-group col-sm-4 col-md-4" style={{paddingRight: '0px'}}>
					<div className="bg-white float-left" style={{padding: '12px',border: '1px solid #ddd',borderRadius: '0px',width:'100%'}}>
						<div className ="float-left marginbottom-10" style={{width: '100%'}}>
							<div style={{width: '60%',float: 'left',textAlign: 'left',whiteSpace: 'nowrap',textOverflow: 'ellipsis',overflow: 'hidden'}} title={target.name}>
								<b>{target.name}</b>
							</div>
							<div style={{width: '40%',float: 'right',textAlign: 'right'}}>
								{this.targetperiod(target)}
							</div>
						</div>
						<div className ="float-left d-flex flex-row align-items-baseline" style={{width: '100%'}}>
							<div style={{width: '60%',float: 'left',textAlign: 'left',fontSize: '20px'}}>
								<b>{currencyFilter(target.targetcompleted, 'qty', this.props.app)}</b>
							</div>
							<div style={{width: '40%',float: 'right',textAlign: 'right'}}>
								{currencyFilter(target.overalltarget, 'qty', this.props.app)}
							</div>
						</div>
						<div className ="float-left marginbottom-10" style={{width: '100%'}}>
							<div style={{width: '50%',float: 'left',textAlign: 'left',fontSize: '10px'}}>
								Achieved
							</div>
							<div style={{width: '50%',float: 'right',textAlign: 'right',fontSize: '10px'}}>
								Target
							</div>
						</div>
						<div className ="float-left" style={{width: '100%'}}>
							<div className="progress targetsummaryprogresscss" style={{height:'10px'}}>
								<div className="progress-bar" role="progressbar" style={{width:`${target.percentagecompleted}%`}}></div>
							</div>
							<div className="float-left" style={{fontSize: '12px',fontWeight: '600', marginTop: '7px', marginLeft: '5px'}}>{target.percentagecompleted}% Achieved</div>
						</div>
						<div className ="float-left" style={{width: '100%'}}>
							<hr className="removemargin"></hr>
							<button type="button" style={{float : 'right'}} className="btn btn-sm gs-btn-outline-primary" onClick={()=>{this.openViewMoreModal(target)}}>View More</button>
						</div>
					</div>
				</div>
			);
		});
	}

	renderTable() {
		let resource = this.state;
		let tbody_div_style = {
			width: `100%`,
			height: `480px`
		};

		return (
			<div className="table-responsive">
				<div className="gs-list-table-header-container" ref="listheader" style={{width: `100%`}}>
					<table className="table gs-table gs-item-table-bordered gs-item-table tablefixed" style={{marginBottom:0}}>
						<thead>
							<tr>
								<th className="text-center font-weight-normal" style={{width:'250px',borderTop:'white'}}>GROUP</th>
								<th className="text-center font-weight-normal" colSpan="3" style={{width:'300px',borderTop:'white'}}>CURRENT</th>
								<th className="text-center font-weight-normal" colSpan="3" style={{width:'300px',borderTop:'white'}}>CUMULATIVE</th>
							</tr>
							<tr>
								<th className="text-center font-weight-normal" style={{width:'250px'}}></th>
								<th className="text-center font-weight-normal" style={{width:'100px', backgroundColor: '#e8f1f9'}}>Target</th>
								<th className="text-center font-weight-normal" style={{width:'100px', backgroundColor: '#e8f1f9'}}>Achieved</th>
								<th className="text-center font-weight-normal" style={{width:'100px', backgroundColor: '#e8f1f9'}}>Achieved%</th>
								<th className="text-center font-weight-normal" style={{width:'100px', backgroundColor: '#e8f1f9'}}>Target</th>
								<th className="text-center font-weight-normal" style={{width:'100px', backgroundColor: '#e8f1f9'}}>Achieved</th>
								<th className="text-center font-weight-normal" style={{width:'100px', backgroundColor: '#e8f1f9'}}>Achieved%</th>
							</tr>
						</thead>
					</table>
				</div>
				<div className="gs-list-table-body-container gs-scrollbar" style={tbody_div_style} ref={this.listbodyRef}>
					<table className="table table-sm gs-table-sm gs-list-table gs-list-table-body">
						<tbody>
							{resource.userRows.map((item, itemkey) => {
								let trStyle = {};
								if(item.type == 'group')
									trStyle.backgroundColor = '#f8fbfe';
								return (
									<tr key={itemkey} className={`${item.hide ? 'hide' : ''}`} style={trStyle}>
										<td style={{paddingLeft:`${20*item.level}px`, width:'250px'}} className="text-left">{this.renderTitleTd(item, itemkey)}</td>
										<td style={{width:'100px',fontWeight:'600'}} className="text-center">{currencyFilter(item.currenttarget, 'qty', this.props.app)}</td>
										<td style={{width:'100px',fontWeight:'600'}} className="text-center">{currencyFilter(item.currenttargetcompleted, 'qty', this.props.app)}</td>
										<td style={{width:'100px'}} className="text-center">
											<div className ="float-left pl-2 pr-2" style={{width: '100%'}}>
												<div className="progress targetdetailprogresscss margintop-5 marginbottom-5" style={{height:'4px'}}>
													<div className="progress-bar" role="progressbar" style={{width:`${item.currenttargetcompletedpercentage}%`}}></div>
												</div>
												<div className="float-left" style={{fontSize: '10px',fontWeight: '600'}}>{item.currenttargetcompletedpercentage}% Achieved</div>
											</div>
										</td>
										<td style={{width:'100px',fontWeight:'600'}} className="text-center">{currencyFilter(item.cummulativetarget, 'qty', this.props.app)}</td>
										<td style={{width:'100px',fontWeight:'600'}} className="text-center">{currencyFilter(item.cumulativetargetcompleted, 'qty', this.props.app)}</td>
										<td style={{width:'100px'}} className="text-center">
											<div className ="float-left pl-2 pr-2" style={{width: '100%'}}>
												<div className="progress targetdetailprogresscss margintop-5 marginbottom-5" style={{height:'4px'}}>
													<div className="progress-bar" role="progressbar" style={{width:`${item.cumulativetargetcompletedpercentage}%`}}></div>
												</div>
												<div className="float-left" style={{fontSize: '10px',fontWeight: '600'}}>{item.cumulativetargetcompletedpercentage}% Achieved</div>
											</div>
										</td>
									</tr>
								)
							})
						}
						</tbody>
					</table>
				</div>
			</div>
		);
	}

	renderTitleTd(item, itemkey) {
		return (
			<div>
				{item.type == 'group' ? <div style={{float:"left"}}>
					<a style={{paddingRight: '5px'}} onClick={() => this.toggleTableRow(item, itemkey)}><i className={`fa fa-${item.hideChildren ? 'caret-right' : 'caret-down'}`}></i></a>
				</div> : null}
				<div style={{float:"left"}}>
					{item.type == 'group' ? <a onClick={() => this.toggleTableRow(item, itemkey)}>{item.groupname}</a> : item.userid_displayname}
				</div>
				{item.type == 'group' ? null : <div style={{float:"right"}}>
					<div className="performancetarget-info-icon" title="Info" onClick={() => this.openViewMoreModal(item, itemkey)}></div>
				</div>}
			</div>
		);
	}

	render() {
		let { targets } = this.state;

		return (
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 col-sm-12 col-lg-12 bg-white " style={{    paddingBottom: '10px'}}>
							<div className="row">
								<div className="col-md-12 col-sm-12">
									<h6 className="margintop-15 semi-bold-custom">
										<div className="gs-card-header float-left">Performance Target Report</div>
									</h6>
									<div>
										{
											(this.state.userRows && this.state.userRows.length > 0) ?
											<button type="button" className="btn btn-sm gs-form-btn-primary marginleft-10 float-right" onClick={() => this.exportExcel()}><span className="fa fa-file-excel-o"/> Export Excel</button> : null
										}
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="row col-sm-12 col-md-12 col-lg-12 responsive-form-element" style={{marginTop: '10px'}}>
						<div className="form-group col-sm-12 col-md-6 col-lg-4">
							<div className="row d-flex flex-row align-items-center">
								<div className="col-sm-2 col-md-2 col-lg-2 paddingright-lg-0">
									<span>Year</span>
								</div>
								<div className="col-sm-10 col-md-10 col-lg-10 paddingleft-lg-0">
									<Field name={'year'} props={{required : true,onChange : ()=>this.dateOnChange()}} component={NumberEle} validate={[numberNewValidation({required:true, model: "Year"})]}/>
								</div>
							</div>
						</div>
						<div className="form-group col-sm-12 col-md-6 col-lg-4">
							<div className="row d-flex flex-row align-items-center">	
								<div className="col-sm-2 col-md-2 col-lg-2 paddingright-lg-0">
									<span>Month</span>
								</div>
								<div className="col-sm-10 col-md-10 col-lg-10 paddingleft-lg-0">
									<Field
										name={'month'}
										props={{
											options: [{ value : 0, label : "January" }, { value : 1, label : "February" }, { value : 2, label : "March" }, { value : 3, label : "April" }, { value : 4, label : "May" }, { value : 5, label : "June" }, { value : 6, label : "July" }, { value : 7, label : "August" }, { value : 8, label : "September" }, { value : 9, label : "October" }, { value : 10, label : "November" }, { value : 11, label : "December" }],
											label: "label",
											valuename:"value",
											required: true,
											onChange: (value, valueObj) => this.dateOnChange()
										}}
										component={localSelectEle}
										validate={[numberNewValidation({required:true, model: "Month"})]}/>
								</div>
							</div>
						</div>
						<div className="form-group col-sm-12 col-md-6 col-lg-4">
							<div className="row d-flex flex-row align-items-center">
								<div className="col-sm-2 col-md-2 col-lg-3 paddingright-lg-0">
									<span>Target Name</span>
								</div>
								<div className="col-sm-10 col-md-10 col-lg-9 paddingleft-lg-0">
									<Field
										name = {`performancetargetid`}
										props = {{
											options: this.state.performancetargets,
											label: 'name',
											valuename: 'id',
											required : true,
											onChange: (value, valueObj) => this.ptOnChange(value, valueObj)
										}}
										component = {localSelectEle}
										validate = {[numberNewValidation({
											required: true
										})]}
									/>
								</div>
							</div>
						</div>
					</div>
					<ReportPlaceholderComponent reportdata={this.state.userRows} />
					{this.state.userRows.length > 0 ? <div className="col-md-12 col-sm-12">
						<div className="card borderradius-0 gs-card">
							<div className="card-body pt-0">
								{this.renderTable()}
							</div>
						</div>
					</div> : null}
				</form>
			</>
		);
	}
}

class OpenViewMoreDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currenttarget : {},
			cumulativetarget : {} ,
			overalltarget : {},
			loading : false,
			loaderflag: true
		};

		this.onLoad();

		this.summaryData = this.summaryData.bind(this);
		this.renderTargetItems = this.renderTargetItems.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
	}

	onLoad() {
		axios.get(`/api/query/getperformancetargetdashboardquery?targetid=${this.props.target.performancetargetid}&userid=${this.props.target.userid}`).then((response) => {
			if (response.data.message == 'success') {
				let target = response.data.main[0];
				this.setState({ target });
			} else {
				this.updateLoaderFlag(false);
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	summaryData(data, type) {
		return (
			<div className="col-md-4 col-sm-4">
				<div className="bg-white float-left" style={{padding: '12px', border: '1px solid #ddd', width:'100%'}}>
					<div className ="float-left marginbottom-10" style={{width: '100%'}}>
						<div style={{width: '60%',float: 'left',textAlign: 'left'}}>
							{type}
						</div>
					</div>
					<div className ="float-left d-flex flex-row align-items-baseline" style={{width: '100%'}}>
						<div style={{width: '60%',float: 'left',textAlign: 'left',fontSize: '20px'}}>
							<b>{currencyFilter(data.targetcompleted, 'qty', this.props.app)}</b>
						</div>
						<div style={{width: '40%',float: 'right',textAlign: 'right'}}>
							{currencyFilter(data.targetvalue, 'qty', this.props.app)}
						</div>
					</div>
					<div className ="float-left marginbottom-10" style={{width: '100%'}}>
						<div style={{width: '50%',float: 'left',textAlign: 'left',fontSize: '10px'}}>
							Achieved
						</div>
						<div style={{width: '50%',float: 'right',textAlign: 'right',fontSize: '10px'}}>
							Target
						</div>
					</div>
					<div className ="float-left" style={{width: '100%',position: 'relative'}}>
						<div className="progress targetsummaryprogresscss" style={{height:'10px'}}>
							<div className="progress-bar" role="progressbar" style={{width:`${data.percentagecompleted}%`}}></div>
						</div>
						{
							(data.cumulativetarget && data.cumulativetarget>0) ?
								<div className="float-left progressbar-indicator" style={{width:`${data.cumulativetarget}%`}}></div>
							 : null
						}
						<div className="float-left" style={{fontSize: '12px',fontWeight: '600', marginTop: '7px', marginLeft: '5px'}}>{data.percentagecompleted}% Achieved</div>
					</div>
				</div>
			</div>
		);
	}

	renderTargetItems(target) {
		return target.performancetargetitems.map((data, index) => {
			return (
				<tr key={index}>
					<td className="text-center" style={{color: '#7e7e7e'}}>{data.targetperiod}</td>
					<td className="text-center" style={{fontWeight:'600'}}>{currencyFilter(data.targetvalue, 'qty', this.props.app)}</td>
					<td className="text-center" style={{fontWeight:'600'}}>{currencyFilter(data.targetcompleted, 'qty', this.props.app)}</td>
					<td>
						<div className ="float-left pl-2 pr-2" style={{width: '100%'}}>
							<div className="progress targetdetailprogresscss margintop-5 marginbottom-5" style={{height:'4px'}}>
								<div className="progress-bar" role="progressbar" style={{width:`${data.percentagecompleted}%`}}></div>
							</div>
							<div className="float-left" style={{fontSize: '10px',fontWeight: '600'}}>{data.percentagecompleted}% Achieved</div>
						</div>
					</td>
					<td className="text-center" style={{fontWeight:'600'}}>{currencyFilter(data.cummulativetarget, 'qty', this.props.app)}</td>
					<td className="text-center" style={{fontWeight:'600'}}>{currencyFilter(data.cummulativetargetcompleted, 'qty', this.props.app)}</td>
					<td>
						<div className ="float-left pl-2 pr-2" style={{width: '100%'}}>
							<div className="progress targetdetailprogresscss margintop-5 marginbottom-5" style={{height:'4px'}}>
								<div className="progress-bar" role="progressbar" style={{width:`${data.cummulativepercentagecompleted}%`}}></div>
							</div>
							<div className="float-left" style={{fontSize: '10px',fontWeight: '600'}}>{data.cummulativepercentagecompleted}% Achieved</div>
						</div>
					</td>
				</tr>
			);
		});
	}

	exportExcel(){
		let coldefArray = [{
			"name" : "Period",
			"key" : "targetperiod",
			"cellClass" : "text-center",
			"width" : 150
		}, {
			"name" : "Current Target",
			"key" : "targetvalue",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Current Achieved",
			"key" : "targetcompleted",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Current Achieved%",
			"key" : "percentagecompleted",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Cumulative Target",
			"key" : "cummulativetarget",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Cumulative Achieved",
			"key" : "cummulativetargetcompleted",
			"cellClass" : "text-center",
			"width" : 150
		},{
			"name" : "Cumulative Achieved%",
			"key" : "cummulativepercentagecompleted",
			"cellClass" : "text-center",
			"width" : 150
		}];
		generateExcel(`${this.props.resource.performancetargetid_name}-${this.props.target.userid_displayname}`, coldefArray,this.state.target.performancetargetitems, this.props.app, () => {});
	}

	render() {
		if(!this.state.target) {
			return (
				<div className="react-outer-modal">
					<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				</div>
			);
		}
		let target = this.state.target;
		let currentItemIndex = target.performancetargetitems.length - 1;

		target.performancetargetitems.forEach((target, targetindex) => {
			if(target.currentPeriod)
				currentItemIndex = targetindex;
		});

		let currentItem = target.performancetargetitems[currentItemIndex];
		let cummulativeItem = {
			targetvalue: currentItem.cummulativetarget,
			targetcompleted: currentItem.cummulativetargetcompleted,
			percentagecompleted: currentItem.cummulativepercentagecompleted
		};
		let overallItem = {
			targetvalue: target.overalltarget,
			targetcompleted: target.targetcompleted,
			percentagecompleted: target.percentagecompleted,
			cumulativetarget : Number(((currentItem.cummulativetarget*100)/target.overalltarget).toFixed(this.props.app.roundOffPrecision))
		};

    	return (
			<div className="react-outer-modal">
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header marginbottom-20">
					<div className="w-100 d-inline-block">
						<div className="float-left" style={{paddingTop : '4px',paddingLeft: '3px'}}>
							<h6 className="modal-title" style ={{color :'#1cbc9c'}}><b>{this.props.resource.performancetargetid_name} - {this.props.target.userid_displayname}</b></h6>
						</div>
						<div className ="float-right">
							<button type="button" className="btn btn-sm gs-form-btn-primary" onClick={() => this.exportExcel()}><span className="fa fa-file-excel-o"/> Export Excel</button>
						</div>
					</div>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						{this.summaryData(currentItem, 'CURRENT')}
						{this.summaryData(cummulativeItem, 'CUMULATIVE')}
						{this.summaryData(overallItem, 'OVERALL')}
					</div>
					<div className="row responsive-form-element margintop-20">
						<div className="col-md-12 col-sm-12">
							<div className="table-responsive">
								<table className="table table-bordered table-sm gs-item-table tablefixed">
									<thead>
										<tr>
											<th className="text-center font-weight-normal">PERIOD</th>
											<th className="text-center font-weight-normal" colSpan="3">CURRENT</th>
											<th className="text-center font-weight-normal" colSpan="3">CUMULATIVE</th>
										</tr>
										<tr style={{backgroundColor: '#e8f1f9'}}>
											<th className="text-center font-weight-normal" style={{borderRight: 'none'}}></th>
											<th className="text-center font-weight-normal" style={{border: 'none'}}>Target</th>
											<th className="text-center font-weight-normal" style={{border: 'none'}}>Achieved</th>
											<th className="text-center font-weight-normal" style={{border: 'none'}}>Achieved%</th>
											<th className="text-center font-weight-normal" style={{border: 'none'}}>Target</th>
											<th className="text-center font-weight-normal" style={{border: 'none'}}>Achieved</th>
											<th className="text-center font-weight-normal" style={{borderLeft: 'none'}}>Achieved%</th>
										</tr>
									</thead>
									<tbody>
										{this.renderTargetItems(target)}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
 				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 text-center">
							<div className="muted credit text-center">
								<button className="btn btn-sm btn-secondary btn-width" onClick={() => this.props.closeModal()}>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

PerformanceTargetReport = connect(
	(state, props) => {
		let formName = `Performance Target Report`;
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(PerformanceTargetReport));

export default PerformanceTargetReport;