import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

import { updateFormState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { AutoSelect } from '../components/utilcomponents';
import { XLSXReader } from '../utils/excelutils';

class ImportPurchaseInvoice extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : true,
			foreignKeyobj: {},
			isValid: false,
			data : {}
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getSuppliers = this.getSuppliers.bind(this);
		this.validateFn = this.validateFn.bind(this);
		this.sampleFn = this.sampleFn.bind(this);
		this.fileOnChange = this.fileOnChange.bind(this);
		this.import = this.import.bind(this);
		this.importtran = this.importtran.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
	}

	componentWillMount() {
		let { data } = this.state;

		let payableaccountid, expenseaccountid;

		this.props.app.appSettings.forEach((item) => {
			if (item.module == 'Accounts' && item.name == 'Payable Account')
				data['payableaccountid'] = item.value.value;

			if (item.module == 'Accounts' && item.name == 'Expense Account')
				data['expenseaccountid'] = item.value.value;
		});

		this.setState({ data });

		this.getSuppliers({});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	getSuppliers(foreignKeyobj, param) {
		axios.get(`/api/partners?field=id,name&filtercondition=partners.issupplier`).then((response) => {
			if (response.data.message == 'success') {
				foreignKeyobj.partners = {};

				response.data.main.forEach((item) => {
					foreignKeyobj.partners[item.name.toLowerCase()] = item
				});

				if(param) {
					this.setState({
						foreignKeyobj,
					}, () => {
						this.validateFn(true);
					});
				} else {
					this.setState({
						foreignKeyobj,
						loaderflag: false
					});
				}
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	inputonChange(value, field) {
		let { data } = this.state;

		data[field] = value;
		this.setState({ data });
	}

	callbackProduct (value, valueObj) {
		let { data } = this.state;

		data['itemid'] = value;
		data['itemid_description'] = valueObj.description,
		data['itemid_uomid'] = valueObj.uomgroupid

		this.setState({ data });
	};

	validateFn(param) {
		this.updateLoaderFlag(true);
		let invArray = [];
		let errors = [];

		let {sheets, foreignKeyobj } = this.state;

		if(!sheets.Purchaseinvoice)
			errors.push('Sheet Purchaseinvoice is not there in Excel or No Items Found');

		if(sheets.Purchaseinvoice && sheets.Purchaseinvoice.length == 0)
			errors.push('Purchaseinvoice must be atleast one item');

		if(errors.length > 0) {
			this.updateLoaderFlag(false);
			this.setState({
				errors: errors
			});
			document.getElementById('fileitem').value='';
			this.fileOnChange([]);
			return false;
		}

		let piArray = sheets.Purchaseinvoice;
		let mandatoryTextArray = ['Invoice NO*', 'Supplier Invoie NO*'];
		let numberCheckArray = ['Invoice Amount*'];
		let mandatoryDateArray = ['Invoice Date*', 'Payment Due Date*', 'Supplier Invoie Date*'];

		piArray.forEach((item, index) => {
			let rowIndex = index + 2;

			mandatoryTextArray.forEach((field) => {
				if (typeof(item[field]) == 'number')
					item[field] = item[field].toString();

				if (typeof(item[field]) != 'string')
					errors.push(`In Purchase Invoice ${field} is mandatory & must be a string for row - ${rowIndex}`);
			});

			mandatoryDateArray.forEach((field) => {
				if (item[field]) {
					if(Object.prototype.toString.call(item[field]) != '[object Date]') {
						let formattedDateString = Number(item[field].split("/")[1]) + "/" + Number(item[field].split("/")[0]) + "/" + Number(item[field].split("/")[2]);

						if (item[field].split('/').length != 3 || new Date(formattedDateString) == 'Invalid Date' || Object.prototype.toString.call(new Date(formattedDateString)) != '[object Date]')
							errors.push(`Invalid ${field} for row - ${rowIndex}`);
						else
							item[field] = new Date(formattedDateString);

					} else if(item[field] == 'Invalid Date')
						errors.push(`Invalid ${field} for row - ${rowIndex}`);
				} else
					errors.push(`Invalid ${field} for row - ${rowIndex}`);
			});

			numberCheckArray.forEach((field) => {
				item[field]=Number(item[field]);

				if (isNaN(item[field]))
					errors.push(`In Purchase Invoice ${field} must contains the number value for row - ${rowIndex}`);
			});

			if(item['Supplier*']) {
				if(!foreignKeyobj.partners[item['Supplier*'].toLowerCase()])
					errors.push(`Invalid Supplier for row - ${rowIndex}`);
				else
					item.partnerid = foreignKeyobj.partners[item['Supplier*'].toLowerCase()].id;
			} else
				errors.push(`Invalid Supplier for row - ${rowIndex}`);

			if(!item['Invoice Amount*'] > 0)
				errors.push(`Invoice Amount must be greater than Zero for row - ${rowIndex}`);

			if(new Date(item['Invoice Date*']) > new Date(item['Payment Due Date*']))
				errors.push(`Payment Due Date must be greater than Invoice Date for row - ${rowIndex}`);

			invArray.push(JSON.parse(JSON.stringify(item)));
		});

		if(errors.length > 0) {
			this.updateLoaderFlag(false);
			this.setState({
				errors: errors
			});
			document.getElementById('fileitem').value='';
			this.fileOnChange([]);
			return false;
		}

		let isValid = true;
		if(param)
			this.importtran(invArray)
		else
			this.setState({
				isValid,
				loaderflag: false
			});
	}

	import() {
		this.updateLoaderFlag(true);
		let foreignKeyobj = {
			...this.state.foreignKeyobj
		};
		this.getSuppliers(foreignKeyobj, true);
	}

	importtran(invArray) {
		let { itemcategories } = this.state;

		let propertiesPIArray = [{
				"name" : "Supplier*",
				"column" : "partnerid",
				"type" : "fk"
			}, {
				"name" : "Invoice NO*",
				"column" : "invoiceno"
			}, {
				"name" : "Invoice Date*",
				"column" : "invoicedate"
			}, {
				"name" : "Supplier Invoie NO*",
				"column" : "partnerreference"
			}, {
				"name" : "Supplier Invoie Date*",
				"column" : "supplierinvoicedate"
			}, {
				"name" : "Payment Due Date*",
				"column" : "paymentduedate"
			}, {
				"name" : "Invoice Amount*",
				"column" : "finaltotal"
			}
		];

		let dataArray = [];

		invArray.forEach((item, index) => {
			let tempObj = {};

			propertiesPIArray.forEach((field) => {
				if (field.type)
					tempObj[field.column] = item[field.column];
				else
					tempObj[field.column] = item[field.name];
			});

			dataArray.push(JSON.parse(JSON.stringify(tempObj)));
		});

		axios({
			method : 'post',
			data : {
				actionverb : 'UploadPurchaseinvoice',
				data : {
					dataArray : dataArray,
					importdata : this.state.data
				}
			},
			url : '/api/importtransaction'
		}).then((response) => {
			if(response.data.message == 'success') {
				this.setState({
					isValid: false
				}, () => {
					this.getSuppliers({...this.state.foreignKeyobj});
				});
			}
			let apiResponse = commonMethods.apiResult(response);
			modalService[apiResponse.methodName](apiResponse.message);
			this.updateLoaderFlag(false);
		});
	}

	sampleFn() {
		window.open("/lib/import_purchaseinvoice_sample.xlsx", '_blank');
	}

	fileOnChange(files) {
		if(files.length > 0) {
			this.updateLoaderFlag(true);
			XLSXReader(files[0], true, true, (data) => {
				this.setState({
					file: files[0],
					filename: files[0].name,
					sheets: data.sheets,
					errors: [],
					loaderflag: false,
					isValid: false
				});
			});
		} else {
			this.setState({
				file: null,
				filename: null,
				//errors: [],
				sheets: null,
				isValid: false
			});
		}
	}

	render() {
		let { sheets } = this.state;
		return (
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="col-sm-12 col-md-12" style={{position: 'fixed', height: '45px', backgroundColor: '#FAFAFA',  zIndex: 5}}>
						<div className="row">
							<div className="col-md-6 col-sm-12 paddingleft-md-30">
								<h6 className="margintop-15 semi-bold-custom">Import Purchase Invoices</h6>
							</div>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-12 col-md-12" style={{marginTop: `48px`}}>
						<div className="card marginbottom-15 borderradius-0">
							<div className="card-body">
								<div className="row responsive-form-element">
									<div className="form-group col-sm-12 col-md-4">
										<label className="labelclass">Excel File</label>
										<input type="file" accept={'.xlsx'} id="fileitem" className="form-control" onChange={(evt) => this.fileOnChange(evt.target.files)} />
									</div>
									<div className="form-group col-sm-12 col-md-8">
										<button type="button"  className="btn btn-sm btn-width gs-btn-warning" onClick={() => this.validateFn()} disabled={!sheets} ><span className="fa fa-ticket"></span> Validate</button>   <button type="button"  className="btn btn-sm btn-width gs-btn-success" onClick={this.import} disabled={!this.state.isValid || !this.state.data.itemid}><span className="fa fa-upload"></span> Import</button>   <button type="button" className="btn btn-sm btn-width gs-btn-info" onClick={this.sampleFn}><span className="fa fa-download"></span> Sample Excel </button>
									</div>
									<div className="row col-md-12">
										<div className="form-group col-md-3 col-sm-6">
											<label className="labelclass">Payable Account</label>
											<AutoSelect resource={'accounts'} fields={'id,name'} value={this.state.data.payableaccountid} valuename={'id'} filter={`accounts.isledger and accounts.type='Liability' and accounts.accountgroup in ('Payable')`} onChange={(value)=>{this.inputonChange(value, 'payableaccountid')}} className={this.state.data.payableaccountid > 0 ? '' : 'errorinput'} required/>
										</div>
										<div className="form-group col-md-3 col-sm-6">
											<label className="labelclass">Expense Account</label>
											<AutoSelect resource={'accounts'} fields={'id,name'} value={this.state.data.expenseaccountid} valuename={'id'} filter={`accounts.isledger and accounts.type='Expense'`} onChange={(value)=>{this.inputonChange(value, 'expenseaccountid')}} className={this.state.data.expenseaccountid > 0 ? '' : 'errorinput'} required/>
										</div>
										<div className="form-group col-md-3 col-sm-6">
											<label className="labelclass">Item Name</label>
											<AutoSelect resource={'itemmaster'} fields={'id,name,description,uomgroupid'} value={this.state.data.itemid} valuename={'id'} filter={`itemmaster.allowsales=true`} onChange={(value, valueobj)=>{this.callbackProduct(value, valueobj)}} className={this.state.data.itemid > 0 ? '' : 'errorinput'} required/>
										</div>
									</div>
									<div className="col-md-12">
										{sheets ? <div className="alert alert-info">
											<p>{sheets.Purchaseinvoice ? sheets.Purchaseinvoice.length : 0} Purchaseinvoice Found </p>
										</div> : null}
									</div>
									<div className="col-md-12">
										{this.state.isValid ? <div className="alert alert-success">
											<ul>
												<li>Validated Successfull</li>
											</ul>
										</div> : null}
									</div>
									<div className="col-md-12">
										{this.state.errors && this.state.errors.length>0 ? <div className="alert alert-success">
											<ul>
												{this.state.errors.map((item, index) => <li key={index}>{item}</li>)}
											</ul>
										</div> : null}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</>
		);
	}
}

ImportPurchaseInvoice = connect(
	(state, props) => {
		let formName = 'ImportPurchaseInvoice';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState}
)(reduxForm()(ImportPurchaseInvoice));

export default ImportPurchaseInvoice;
