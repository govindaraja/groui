import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import CalendarToolbar from '../components/calendartoolbar';

const localizer = momentLocalizer(moment);

class OrderCalendarForm extends Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getCalendarData = this.getCalendarData.bind(this);
		this.onView = this.onView.bind(this);
		this.onNavigate = this.onNavigate.bind(this);
		this.updateTimes = this.updateTimes.bind(this);
		this.openOrder = this.openOrder.bind(this);
		this.gotoListPage = this.gotoListPage.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata) {
			tempObj = {
				...this.props.reportdata
			};
		} else {
			tempObj = {
				salesperson : this.props.app.user.id,
				fromdate : moment(new Date()).startOf('day')._d,
				todate : moment(new Date()).endOf('day')._d,
				defaultDate: new Date(),
				defaultView: 'day',
				events: [],
				eventSources: []
			};
		}

		this.props.initialize(tempObj);
		setTimeout(() => {
			this.getCalendarData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	onView(view) {
		this.props.updateFormState(this.props.form, {
			defaultView: view
		});
		setTimeout(() => {
			this.updateTimes();
		});
	}

	onNavigate(date, view) {
		const new_date = moment(date);
		this.props.updateFormState(this.props.form, {
			defaultDate: new_date
		});
		setTimeout(() => {
			this.updateTimes();
		});
	}

	updateTimes(date = this.props.resource.defaultDate, view = this.props.resource.defaultView) {
		let start, end;
	
		if(view === 'day'){
			start = moment(date).startOf('day');
			end   = moment(date).endOf('day');
		} else if(view === 'week'){
			start = moment(date).startOf('week');
			end   = moment(date).endOf('week');
		} else if(view === 'month') {
			start = moment(date).startOf('month').startOf('week');
			end = moment(date).endOf('month').endOf('week');
		}
		else if(view === 'agenda') {
			start = moment(date).startOf('day');
			end   = moment(date).endOf('day').add(1, 'month');
		}

		this.props.updateFormState(this.props.form, {
			fromdate: start._d,
			todate: end._d
		});
		setTimeout(() => {
			this.getCalendarData();
		}, 0);
	}
	
	getCalendarData() {
		this.updateLoaderFlag(true);
		let events = [];
		let year = new Date().getFullYear();
		let month = new Date().getMonth();
		let date = new Date().getDate();

		var queryString = `/api/orders?&field=id,orderno,partners/name/customerid,deliverydate&filtercondition=orders.status='Approved' and orders.deliverydate::date >= '${new Date(this.props.resource.fromdate).toDateString()}'::date and orders.deliverydate::date <= '${new Date(this.props.resource.todate).toDateString()}'::date and (orders.deliveredpercent is null or orders.deliveredpercent < 100)`;

		axios.get(`${queryString}`).then((response) => {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.main.length; i++) {
					let nextFollowupdate = new Date(response.data.main[i].deliverydate);
					events.push({
						id : response.data.main[i].id,
						title : response.data.main[i].customerid_name+" -- "+response.data.main[i].orderno,
						start : nextFollowupdate,
						end : nextFollowupdate,
						className : 'label-green',
						allDay : false
					});
				}

				this.props.updateFormState(this.props.form, {
					events
				});
				this.props.updateReportFilter(this.props.form, this.props.resource);
			}
			this.updateLoaderFlag(false);
		});
	}

	openOrder(order) {
		this.props.history.push(`/details/orders/${order.id}`);
	}

	gotoListPage() {
		this.props.history.goBack();
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row" style={{marginTop: `${(document.getElementsByClassName('navbar').length > 0 ? document.getElementsByClassName('navbar')[0].clientHeight : '0')}px`}}>
						<div className="col-md-12 form-group bg-white"  style={{borderBottom: '1px solid #ddd'}}>
							<div className="float-left" style={{marginTop: '15px', marginBottom: '15px', fontSize: '18px'}}><a className="affixanchor" onClick={this.gotoListPage}><span className="fa fa-chevron-left marginright-10"></span></a>  Order Calendar</div>
						</div>
						<div className="col-md-12 bg-white padding-10" style={{height: '500px'}}>
							<Calendar localizer={localizer} events={this.props.resource.events} views={["month", "week", "day"]} defaultView={this.props.resource.defaultView} defaultDate={new Date(this.props.resource.defaultDate)} showMultiDayTimes components = {{toolbar : CalendarToolbar}} onView={this.onView} onNavigate={this.onNavigate} onSelectEvent={event => this.openOrder(event)} />
						</div>
					</div>
				</form>
			</div>
		);
	};
}


OrderCalendarForm = connect(
	(state, props) => {
		let formName = 'ordercalendar';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(OrderCalendarForm));

export default OrderCalendarForm;
