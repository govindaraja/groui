import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { updateFormState } from '../actions/actions';
import { WithContext as ReactTags } from 'react-tag-input';
import withDragDropContext from '../utils/withDragDropContext';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { DateEle, localSelectEle, autoSelectEle, selectAsyncEle, checkboxEle, textareaEle, InputEle, NumberEle, SpanEle, autosuggestEle, ButtongroupEle } from '../components/formelements';
import { customfieldAssign, numberNewValidation, dateNewValidation, stringNewValidation, checkActionVerbAccess, multiSelectNewValidation, requiredNewValidation } from '../utils/utils';
import { dateFilter, datetimeFilter, timeFilter} from '../utils/filter';
import DndColumns from '../components/list/listviewlistcontainer';
import { Prompt } from 'react-router-dom';
import DataAccessItemsComponent from './dataaccessitemcomponent';
import { AdditionalInformationSection } from '../containers/ndtsection1';

class DataAccessForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderflag: true,
			createParam: this.props.match.params.id > 0 ? false : true,
			resourceArray: [],
			rolesArray: [],
			columnsArray: [],
			filterArray: [],
			filterObj: {},
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.selector = formValueSelector(this.props.form);
		this.initialize = this.initialize.bind(this);
		this.getItemById = this.getItemById.bind(this);
		this.getRoles = this.getRoles.bind(this);
		this.getResourceName = this.getResourceName.bind(this);
		this.getColumns = this.getColumns.bind(this);
		this.resourceOnChange = this.resourceOnChange.bind(this);
		this.gotoListpage = this.gotoListpage.bind(this);
		this.applicableforOnChange = this.applicableforOnChange.bind(this);
		this.save = this.save.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.getResourceName();

		if (this.props.match.path == '/createDataAccess')
			this.initialize();
		else
			this.getItemById();
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag)
			this.updateLoaderFlag(false);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	initialize() {
		let tempObj = {
			applicablefor: 'Selected Roles',
			applyforread: true,
			applyforwrite: true,
			dataaccessitems: []
		};

		this.props.initialize(tempObj);

		setTimeout(()=> {
			this.getRoles();
		}, 0);

		this.updateLoaderFlag(false);
	}

	getItemById() {
		axios.get(`/api/dataaccess/${this.props.match.params.id}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.initialize({
					...response.data.main
				});

				this.setState({
					ndt: {
						notes: response.data.notes,
						documents: response.data.documents,
						tasks: response.data.tasks
					}
				});

				setTimeout(this.getRoles, 0);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getResourceName () {
		let resourceArray = [];
		let resourceOptionsArray = [];

		let myResources = this.props.app.myResources;

		for (let prop in myResources) {
			if (myResources[prop].type != 'query' && myResources[prop].type != 'view' && !myResources[prop].hideInPermission)
				resourceArray.push({
					name : prop,
					displayName : myResources[prop].displayName
				});

			if (myResources[prop].type != 'query')
				resourceOptionsArray.push({
					id : prop,
					name : myResources[prop].displayName
				});
		}

		resourceArray.sort((a, b) => {
			return (a.displayName.toLowerCase() < b.displayName.toLowerCase()) ? -1 : (a.displayName.toLowerCase() > b.displayName.toLowerCase()) ? 1 : 0;
		});

		this.setState({ resourceArray, resourceOptionsArray });
	}

	getRoles() {
		let rolesArray = [];

		axios.get(`/api/roles?&field=id,name&filtercondition=`).then((response) => {
			if (response.data.message == 'success') {
				rolesArray = response.data.main;

				rolesArray.sort((a, b) => {
					return (a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0;
				});

				this.setState({ rolesArray }, () => {
					this.getColumns();
				});
			}
		});
	};

	getColumns() {
		this.updateLoaderFlag(true);

		let columnsArray = [],
		filterArray = [],
		filterObj = {};

		if (this.props.resource && this.props.resource.resourcename) {
			let resourceObj = this.props.app.myResources[this.props.resource.resourcename];

			filterObj = {
				...resourceObj.fields
			};

			for (let prop in resourceObj.fields) {
				columnsArray.push({
					field: prop,
					jsonname: 'resourcejson',
					...resourceObj.fields[prop]
				});

				if ((resourceObj.fields[prop].type == 'string' && (resourceObj.fields[prop].group == 'localselect' || resourceObj.fields[prop].group == 'resource')) || ['boolean', 'date', 'integer'].includes(resourceObj.fields[prop].type) || resourceObj.fields[prop].isArrayForeignKey)
					filterArray.push({
						field: prop,
						jsonname: 'resourcejson',
						...resourceObj.fields[prop]
					});

				//if (resourceObj.fields[prop].filterformat) {
					if (resourceObj.fields[prop].isForeignKey) {
						let foreignkeyResourcename = resourceObj.fields[prop]['foreignKeyOptions'].resource;
						let mainfieldname = resourceObj.fields[prop]['foreignKeyOptions'].mainField.split('.')[1];

						for (let foreignkeyField in this.props.app.myResources[foreignkeyResourcename].fields) {
							if (mainfieldname != foreignkeyField && !this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].isrestrict && this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].filterformat) {
								if ((this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].type == 'string' && this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].group == 'localselect') || ['boolean', 'date', 'integer'].includes(this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].type)) {
									filterArray.push({
										field: `${prop}/${foreignkeyField}`,
										jsonname: 'resourcejson',
										displayName : resourceObj.fields[prop].displayName +"'s "+this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].displayName
									});

									filterObj[`${prop}/${foreignkeyField}`] = {
										...this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField],
										displayName : resourceObj.fields[prop].displayName +"'s "+this.props.app.myResources[foreignkeyResourcename].fields[foreignkeyField].displayName
									}
								}
							}
						}
					}
				//}
			}
		}

		this.setState({
			columnsArray,
			filterArray,
			filterObj
		}, ()=> {
			if(!this.props.resource.resourcename)
				this.props.updateFormState(this.props.form, {
					dataaccessitems: []
				});
		});

		this.updateLoaderFlag(false);
	};

	resourceOnChange(value) {
		this.props.updateFormState(this.props.form, {
			dataaccessitems: []
		});

		setTimeout(this.getColumns, 0);
	}

	applicableforOnChange(value) {
		this.props.updateFormState(this.props.form, {
			applicableroles: []
		});
	}

	save(param, confirm) {
		this.updateLoaderFlag(true);

		axios({
			method: 'post',
			data: {
				actionverb: param,
				data: this.props.resource,
				ignoreExceptions: confirm ? true : false
			},
			url: '/api/dataaccess'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
				if (resparam)
					this.save(param, true);
			});

			if (response.data.message == 'success') {
				let resource = {
					...response.data.main
				};

				if (this.state.createParam)
					this.props.history.replace(`/details/dataaccess/${response.data.main.id}`);
				else if (param == 'Delete')
					this.props.history.replace('/list/dataaccess');
				else
					this.props.initialize(resource);
			}

			this.updateLoaderFlag(false);
		});
	}

	gotoListpage() {
		this.props.history.push('/list/dataaccess');
	}

	render() {
		const { app, resource } = this.props;
		const { rolesArray, resourceArray, columnsArray } = this.state;

		if (!resource)
			return null;

		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<Prompt when={!this.state.createParam && this.props.dirty && this.props.anyTouched} message={`Unsaved Changes`} />
				<div className="row">
					<div className = 'col-sm-12 affixmenubar col-md-12 col-lg-9 d-sm-block d-md-flex justify-content-md-between '>
						<div className = 'semi-bold-custom font-16 d-flex align-items-center'>
							<a className = 'affixanchor float-left marginright-10' onClick={()=>{this.gotoListpage()}}>
								<span className = 'fa fa-chevron-left'></span>
							</a>
							<div className="gs-uppercase float-left">DATA ACCESS RULE : </div>
							{resource.id ? <div className="float-left marginleft-10" style={{maxWidth: '300px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis'}} title={`${resource.name}`}>{`${resource.name}`}</div> : null}
						</div>
					</div>
				</div>
				<form>
					<div className = 'row'>
						<div className = 'col-sm-12 col-md-12 col-lg-9'>
							<div className = 'row' style={{marginTop:'50px'}}>
								<div className = 'col-md-12'>
									<div className = 'card' style={{marginBottom:'10px'}}>
										<div className = 'card-header listpage-title gs-uppercase'>Rule Details</div>
										<div className = 'card-body'>
											<div className = 'row'>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Name</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'name'}
																props = {{
																	required: true
																}}
																component = {InputEle}
																validate = {[stringNewValidation({
																	required: true,
																	title : 'Name'
																})]}
															/>
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Resource</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'resourcename'}
																props = {{
																	label: 'displayName',
																	valuename: 'name',
																	options: [...resourceArray],
																	onChange: (value) => this.resourceOnChange(value),
																	required: true
																}}
																component = {localSelectEle}
																validate = {[stringNewValidation({
																	required:  true,
																	title : 'Resource'
																})]} />
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Applicable For</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'applicablefor'}
																props = {{
																	options: ["Selected Roles", "Everyone"],
																	onChange: (value) => this.applicableforOnChange(value),
																	required:  true
																}}
																component = {localSelectEle}
																validate = {[stringNewValidation({
																	required:  true,
																	title : 'Applicable For'
																})]}
															/>
														</div>
													</div>
												</div>
												{resource.applicablefor == "Selected Roles" ? <div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Applicable Roles</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'applicableroles'}
																props = {{
																	multiselect: true,
																	required:  true,
																	options: [...rolesArray]
																}}
																component = {localSelectEle}
																validate = {[multiSelectNewValidation({
																	required:  true,
																	title : 'Applicable Roles'
																})]}
															/>
														</div>
													</div>
												</div> : null}
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Exempted Roles</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'exemptedroles'}
																props = {{
																	multiselect: true,
																	options: [...rolesArray]
																}}
																component = {localSelectEle}
																validate = {[multiSelectNewValidation({
																	title : 'Exempted Roles'
																})]}
															/>
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Applies to View and Change</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field name={'applyforread'} props={{}} component={checkboxEle}/>
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Applies Only to Change</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field name={'applyforwrite'} props={{}} component={checkboxEle}/>
														</div>
													</div>
												</div>
												<div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Remarks</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'remarks'}
																component = {textareaEle}
															/>
														</div>
													</div>
												</div>
												{ resource.id ? <div className = 'col-md-6 col-sm-12 col-xs-12'>
													<div className = 'row'>
														<div className = 'form-group col-md-3 col-sm-6'>
															<label>Active</label>
														</div>
														<div className = 'form-group col-md-9 col-sm-6'>
															<Field
																name = {'isactive'}
																component = {checkboxEle}
															/>
														</div>
													</div>
												</div> : null}
											</div>
										</div>
									</div>
								</div>
								
								<div className = 'col-md-12'>
									<div className = 'card' style={{marginBottom:'3rem'}}>
										<div className = 'card-header listpage-title gs-uppercase'>Access Conditions</div>
										<div className = 'card-body'>
											<div className='row responsive-form-element'>
												<FieldArray
													name = {`dataaccessitems`}
													resource = {this.props.resource}
													array = {this.props.array}
													app = {this.props.app}
													selector = {this.selector}
													updateFormState = {this.props.updateFormState}
													form = {this.props.form}
													fullstate = {this.props.fullstate}
													component = {DataAccessItemsComponent}
													filterArray = {this.state.filterArray}
													resourceArray={this.state.resourceOptionsArray}
													filterObj = {this.state.filterObj}
													openModal = {this.props.openModal} />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className={`col-sm-12 ndt-section col-md-12 col-lg-3 paddingleft-md-0 `} >
							{this.props.resource && this.props.resource.id && this.state.ndt ? <AdditionalInformationSection ndt={this.state.ndt} key={0} parentresource={'dataaccess'} openModal={this.props.openModal} parentid={this.props.resource.id} relatedpath={this.props.match.path} createOrEdit = {this.props.createOrEdit} app ={this.props.app} history={this.props.history}/> : null }
						</div>
					</div>
					<div className = 'row'>
						<div className = 'col-sm-12 col-md-12 col-lg-9'>
							<div className = 'muted credit text-center sticky-footer actionbtn-bar' style={{boxShadow:'none'}}>
								<button
									type = 'button'
									className = 'btn btn-sm btn-width btn-secondary'
									onClick={() => this.props.history.goBack()}>
									<i className = 'fa fa-times marginright-5'></i>Close
								</button>
								{checkActionVerbAccess(this.props.app, 'dataaccess', 'Save') ? <button
									type = 'button'
									className = 'btn btn-width btn-sm gs-btn-success'
									disabled = {this.props.invalid}
									onClick = {()=>{this.save('Save');}}>
									<i className = 'fa fa-save'></i>Save
								</button> : null}
								{resource.id && checkActionVerbAccess(this.props.app, 'dataaccess', 'Delete') ? <button
									type = 'button'
									className = 'btn btn-width btn-sm gs-btn-danger'
									onClick = {() => this.save('Delete')}
									disabled = {this.props.invalid}>
									<i className = 'fa fa-trash-o'></i>Delete
								</button> : null}
							</div>
 						</div>
					</div>
				</form>
			</div>
		);
	}
}

DataAccessForm = connect(
	(state, props) => {
		let formName = props.match.params.id > 0 ? `DataAccess_${props.match.params.id}` : 'DataAccess_create';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			touchOnChange: true,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState }
)(reduxForm()(withDragDropContext(DataAccessForm)));

export default DataAccessForm;