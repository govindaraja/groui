import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';
import { dateFilter, currencyFilter } from '../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			stockDetails : {},
			stockArray: null,
			tabClasses: ["", "", ""],
			direction : true,
			orderProp : 'onhandqty'
		};
		this.onLoad = this.onLoad.bind(this);
		this.close = this.close.bind(this);
		this.openItem = this.openItem.bind(this);
		this.closeItem = this.closeItem.bind(this);
		this.setActiveTab = this.setActiveTab.bind(this);
		this.getTabPaneClass = this.getTabPaneClass.bind(this);
		this.getTabClass = this.getTabClass.bind(this);
		this.sortBy = this.sortBy.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let isPO = false;

		let warehouseHCArray = [];

		if('stocktransferitems' in this.props.resource) {
			if(this.props.resource.sourcewarehouseid > 0)
				warehouseHCArray.push(this.props.resource.sourcewarehouseid);
			if(this.props.resource.destinationwarehouseid > 0)
				warehouseHCArray.push(this.props.resource.destinationwarehouseid);
		}

		let warehouseStr = warehouseHCArray.length > 0 ? `&warehouseidarr=${warehouseHCArray.join()}`: ``;

		let queryString = `/api/query/getstockquery?itemid=${this.props.item.itemid}&uomid=${this.props.item.uomid}&uomname=${this.props.item.uomid_name}&projectid=${this.props.resource.projectid ? this.props.resource.projectid : this.props.item.projectid}${warehouseStr}`;

		if(this.props.item.itemid_issaleskit)
			queryString += `&issaleskit=true`;
		if(this.props.item.itemid_hasaddons)
			queryString += `&hasaddons=true`;
		if ('purchaseorderitems' in this.props.resource) {
			queryString += `&resource=po`;
			isPO = true;
		}

		axios.get(queryString).then((response) => {
			if (response.data.message == 'success') {
				let responseArr = response.data.main.sort((a, b)=>{
					return b.onhandqty - a.onhandqty;
				});

				responseArr.map((a)=>{
					a.salesOrders && a.salesOrders.sort((a, b)=>{
						return (a.deliverydate < b.deliverydate) ? -1 : (a.deliverydate > b.deliverydate ? 1 : 0);
					});
					a.purchaseOrders && a.purchaseOrders.sort((a, b)=>{
						return (a.deliverydate < b.deliverydate) ? -1 : (a.deliverydate > b.deliverydate ? 1 : 0);
					});
					a.stockreservations && a.stockreservations.sort((a, b)=>{
						return (a.expiredate < b.expiredate) ? -1 : (a.expiredate > b.expiredate ? 1 : 0);
					});
				})

				this.setState({
					stockArray: responseArr
				});
				if (isPO) {
					this.setState({
						safetystock: response.data.safetystock,
						reorderqty: response.data.reorderqty,
						leadtime: response.data.leadtime,
						isPO
					});
				}
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	sortBy(item) {
		let { stockArray, direction, orderProp } = this.state;

		stockArray.sort((a, b)=>{
			if(direction)
				return (a[`${item}`] < b[`${item}`]) ? -1 : (a[`${item}`] > b[`${item}`] ? 1 : 0);
			else
				return (a[`${item}`] < b[`${item}`]) ? 1 : (a[`${item}`] > b[`${item}`] ? -1 : 0);
		});

		direction = !direction;

		this.setState({
			stockArray,
			direction,
			orderProp : item
		});
	}

	openItem(item) {
		if(this.props.item.itemid_issaleskit || this.props.item.itemid_hasaddons)
			return null;

		this.setState({stockDetails : item});
		this.setActiveTab(1);
	};

	closeItem() {
		this.setState({stockDetails: {}});
	}

	getTabClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	getTabPaneClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	setActiveTab(tabNum) {
		let { tabClasses } = this.state;
		tabClasses =  ["", "", ""]
		tabClasses[tabNum] = "show active";
		this.setState({tabClasses}, ()=>{
			this.getTabPaneClass(tabNum);
		});
	}

	close() {
		this.props.closeModal();
	}

	renderAllWarehouse() {
		let item = this.props.item;
		let {stockArray, stockDetails} = this.state;
		return (
			<div className="col-md-12 col-sm-12 col-xs-12">
				{!item.itemid_issaleskit && !item.itemid_hasaddons ? <div style={{fontSize:'13px', paddingBottom: '10px'}}>Click on a Warehouse to show details !!!</div> : null}
				<div className="table-responsive">
					<table className="table table-condensed">
						<thead>
							<tr>
								<th className="text-center">Warehouse
									<button className = "btn btn-sm btn-outline-light text-dark pull-right" onClick={() => this.sortBy('warehouseid_name')}><span className={`${(this.state.orderProp=='warehouseid_name' && this.state.direction) ? 'show' : 'hide'} fa fa-caret-down`}></span><span className={`${(this.state.orderProp=='warehouseid_name' && !this.state.direction) ? 'show' : 'hide'} fa fa-caret-up`}></span><span className={`${this.state.orderProp!='warehouseid_name' ? 'show' : 'hide'} fa fa-sort`}></span></button>
								</th>
								<th className="text-center">Onhand Qty
									<button className = "btn btn-sm btn-outline-light text-dark pull-right" onClick={() => this.sortBy('onhandqty')}><span className={`${(this.state.orderProp=='onhandqty' && this.state.direction) ? 'show' : 'hide'} fa fa-caret-down`}></span><span className={`${(this.state.orderProp=='onhandqty' && !this.state.direction) ? 'show' : 'hide'} fa fa-caret-up`}></span><span className={`${this.state.orderProp!='onhandqty' ? 'show' : 'hide'} fa fa-sort`}></span></button>
								</th>
								<th className="text-center">Effective Qty
									<button className = "btn btn-sm btn-outline-light text-dark pull-right" onClick={() => this.sortBy('effectiveqty')}><span className={`${(this.state.orderProp=='effectiveqty' && this.state.direction) ? 'show' : 'hide'} fa fa-caret-down`}></span><span className={`${(this.state.orderProp=='effectiveqty' && !this.state.direction) ? 'show' : 'hide'} fa fa-caret-up`}></span><span className={`${this.state.orderProp!='effectiveqty' ? 'show' : 'hide'} fa fa-sort`}></span></button>
								</th>
								<th className="text-center">Reserved Qty
									<button className = "btn btn-sm btn-outline-light text-dark pull-right" onClick={() => this.sortBy('reservedqty')}><span className={`${(this.state.orderProp=='reservedqty' && this.state.direction) ? 'show' : 'hide'} fa fa-caret-down`}></span><span className={`${(this.state.orderProp=='reservedqty' && !this.state.direction) ? 'show' : 'hide'} fa fa-caret-up`}></span><span className={`${this.state.orderProp!='reservedqty' ? 'show' : 'hide'} fa fa-sort`}></span></button>
								</th>
							</tr>
						</thead>
						<tbody>
							{stockArray.map((stock, key) => {
								return <tr key={key} className="hoverDiv" onClick={() => this.openItem(stock)}>
									<td className="text-center">{stock.warehouseid_name}</td>
									<td className="text-right">{stock.onhandqty}</td>
									<td className="text-right">{stock.effectiveqty}</td>
									<td className="text-right">{stock.reservedqty}</td>
								</tr>
							})}
						</tbody>
					</table>
				</div>
			</div>
		);
	}

	renderParticularWarehouse() {
		let {stockArray, stockDetails} = this.state;
		return (
			<div className="col-md-12 col-sm-12 col-xs-12">
				<ul className="nav nav-tabs">
					<li className="nav-item">
						<a className={`nav-link ${this.getTabClass(1)}`} href="javascript:void(0);" onClick={()=>this.setActiveTab(1)} >SO Qty <span className="badge badge-secondary">{stockDetails.soqty} {stockDetails.uomid_name}</span></a>
					</li>
					<li className="nav-item">
						<a className={`nav-link ${this.getTabClass(2)}`} href="javascript:void(0);" onClick={()=>this.setActiveTab(2)} >PO Qty <span className="badge badge-secondary">{stockDetails.poqty} {stockDetails.uomid_name}</span></a>
					</li>
					<li className="nav-item">
						<a className={`nav-link ${this.getTabClass(3)}`} href="javascript:void(0);"  onClick={()=>this.setActiveTab(3)} >Reserved Qty <span className="badge badge-secondary">{stockDetails.reservedqty} {stockDetails.uomid_name}</span></a>
					</li>
				</ul>
				<div className="tab-content">
					<div className={`tab-pane fade ${this.getTabPaneClass(1)}`} role="tabpanel">
						<div className="row margintop-25">
							{stockDetails.salesOrders.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												<th className="text-center">Resource</th>
												<th className="text-center">SO Number</th>
												<th className="text-center">Customer</th>
												<th className="text-center">Delivery Date</th>
												<th className="text-center">Quantity</th>
											</tr>
										</thead>
										<tbody>
											{stockDetails.salesOrders.map((order, index) => {
												return (
													<tr key={index}>
														<td className="text-center">{order.reference}</td>
														<td className="text-center">{order.transactionno}</td>
														<td>{order.partnerid_name}</td>
														<td className="text-center">{dateFilter(order.deliverydate)}</td>
														<td className="text-center">{order.transactionqty} {order.transactionuomid_name}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</div>
							</div> : <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 alert alert-danger text-center">
									No SO Details found!!!
								</div>
							</div> }
						</div>
					</div>
					<div className={`tab-pane fade ${this.getTabPaneClass(2)}`} role="tabpanel">
						<div className="row margintop-25">
							{stockDetails.purchaseOrders.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												<th className="text-center">Resource</th>
												<th className="text-center">PO Number</th>
												<th className="text-center">Supplier</th>
												<th className="text-center">Delivery Date</th>
												<th className="text-center">Quantity</th>
											</tr>
										</thead>
										<tbody>
											{stockDetails.purchaseOrders.map((purchaseorder, index) => {
												return (
													<tr key={index}>
														<td className="text-center">{purchaseorder.reference}</td>
														<td className="text-center">{purchaseorder.transactionno}</td>
														<td>{purchaseorder.partnerid_name}</td>
														<td className="text-center">{dateFilter(purchaseorder.deliverydate)}</td>
														<td className="text-center">{purchaseorder.transactionqty} {purchaseorder.transactionuomid_name}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</div>
							</div> :  <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 alert alert-danger text-center">
									No PO Details found!!!
								</div>
							</div> }
						</div>
					</div>
					<div className={`tab-pane fade ${this.getTabPaneClass(3)}`} role="tabpanel">
						<div className="row margintop-25">
							{stockDetails.stockreservations.length > 0 ?  <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												<th className="text-center">Resource</th>
												<th className="text-center">PO Number</th>
												<th className="text-center">Supplier</th>
												<th className="text-center">Delivery Date</th>
												<th className="text-center">Quantity</th>
											</tr>
										</thead>
										<tbody>
											{stockDetails.stockreservations.map((stockreservation, index) => {
												return (
													<tr key={index}>
														<td className="text-center">{stockreservation.reference}</td>
														<td className="text-center">{stockreservation.transactionno}</td>
														<td>{stockreservation.customerid_name}</td>
														<td className="text-center">{dateFilter(stockreservation.expiredate)}</td>
														<td className="text-center">{stockreservation.quantity} {stockreservation.uomid_name}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</div>
							</div> : <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 alert alert-danger text-center">
									No Stock Reservation Details found!!!
								</div>
							</div> }
						</div>
					</div>
				</div>
			</div>
		);
	}

	render() {
		let item = this.props.item;
		let {stockArray, stockDetails} = this.state;

		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Stock Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					{stockArray ? (stockArray.length > 0 ? <div className="row">
						<div className="form-group col-md-3 col-sm-6">
							Item Name : <b className="text-primary">{item.itemid_name}</b>
						</div>
						{stockDetails.warehouseid > 0 ? <div className="form-group col-md-3 col-sm-6">
							Onhand Qty : <b className="text-success">{stockDetails.onhandqty} {stockDetails.uomid_name}</b>
						</div> : null}
						{stockDetails.warehouseid > 0 ? <div className="form-group col-md-3 col-sm-6">
							Effective Qty : <b className="text-danger">{stockDetails.effectiveqty} {stockDetails.uomid_name} </b>
						</div> : null}
						{stockDetails.warehouseid > 0 ? <div className="form-group col-md-3 col-sm-6">
							Reserved Qty : <b className="text-danger">{stockDetails.reservedqty} {stockDetails.uomid_name}</b>
						</div> : null}
						{item.itemid_issaleskit ? <div className="form-group col-md-3 col-sm-6">
							UOM : <b className="text-danger">{item.uomid_name}</b>
						</div> : null}
						{(this.state.isPO && !item.itemid_issaleskit) ? <div className="form-group col-md-3 col-sm-6">
							Lead Time (In Days) : <b className="text-danger">{this.state.leadtime}</b>
						</div> : null}
						{(this.state.isPO && !item.itemid_issaleskit) ? <div className="form-group col-md-3 col-sm-6">
							Safety Stock : <b className="text-danger">{this.state.safetystock}</b>
						</div> : null}
						{(this.state.isPO && !item.itemid_issaleskit) ? <div className="form-group col-md-3 col-sm-6">
							Reorder Qty : <b className="text-danger">{this.state.reorderqty}</b>
						</div> : null}
						{stockDetails.warehouseid > 0 ? <div className="form-group col-md-12 col-sm-12 col-xs-12 text-center" style={{paddingTop: '10px'}}>
							<span>Selected Warehouse : </span><b className="text-success">{stockDetails.warehouseid_name}</b><span className="gs-anchor" onClick={this.closeItem} style={{paddingLeft:'10px'}}>View All</span>
						</div> : null}
						{item.itemid_issaleskit || item.itemid_hasaddons ? this.renderAllWarehouse() : null}
						{!item.itemid_issaleskit && !item.itemid_hasaddons && !stockDetails.warehouseid ? this.renderAllWarehouse() : null}
						{!item.itemid_issaleskit && !item.itemid_hasaddons && stockDetails.warehouseid > 0 ? this.renderParticularWarehouse() : null}
					</div> : <div className="form-group col-md-12 col-sm-12 col-xs-12 text-center">
					  	<div className="alert alert-warning" >Item {item.itemid_name} is not available in any warehouse</div>
				   </div>) : <div className="form-group col-md-12 col-sm-12 col-xs-12 text-center">
						<div className="alert alert-warning" >Loading... Please wait...</div>
				   </div>}
				</div>
				<div className="react-modal-footer">
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="muted credit text-center">
							{stockDetails.warehouseid > 0 ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.closeItem}><i className="fa fa-arrow-left"></i>Back</button> : null}
							<button type="button" className="btn btn-sm btn-secondary btn-width"onClick={this.close}><i className="fa fa-times"></i>Close</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
