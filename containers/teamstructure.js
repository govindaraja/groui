import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

import { updateFormState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { checkActionVerbAccess } from '../utils/utils';

class TeamStructure extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getUsers = this.getUsers.bind(this);
		this.getTeamStructureData = this.getTeamStructureData.bind(this);
		this.generateTreeArray = this.generateTreeArray.bind(this);
		this.openTeam = this.openTeam.bind(this);
	}

	componentWillMount() {
		this.getUsers();	
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	getUsers() {
		axios.get(`/api/users?pagelength=10000&includeinactive=true&field=id,displayname,salesteamlevels,isactive`).then((response)=> {
			if (response.data.message == 'success') {
				this.getTeamStructureData(response.data.main);
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getTeamStructureData(userArray) {
		axios.get(`/api/teamstructure?field=id,name,type,parentid,isparent&pagelength=10000`).then((response)=> {
			if (response.data.message == 'success') {

				response.data.main.sort((a,b) => a.id > b.id);
				
				let teamArray = response.data.main.map((team) => {
					return {
						id : team.id,
						label : team.name,
						parentid : team.parentid,
						type : team.type,
						isparent : team.isparent,
						users : [],
						children : []
					}
				});

				teamArray.forEach((team) => {
					userArray.forEach((user) => {
						if(user.salesteamlevels && user.salesteamlevels.length > 0 && user.salesteamlevels.indexOf(team.id) >= 0)
							team.users.push(user);
					});
				});

				this.generateTreeArray(teamArray);

			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	generateTreeArray(teamArray) {
		let treeArray = [];
		let teamObj = {};
		teamArray.forEach((team) => {
			teamObj[team.id] = team;
		});

		teamArray.forEach((team) => {
			if(team.parentid == null)
				treeArray.push(team);
			else
				teamObj[team.parentid].children.push(team);
		});
		this.setState({
			treeArray
		});
	}

	openTeam(id, parentid) {
		if(id > 0)
			this.props.createOrEdit('/details/teamstructure/:id', id, {}, (valueObj) => {this.getUsers();});
		else
			this.props.createOrEdit('/createTeamStructure', id, parentid > 0 ? {
				parentid: parentid
			} : null, (valueObj) => {this.getUsers();});
	}

	renderTeam(team, index) {
		return (
			<td key={index} className="nor-td">
				<table>
					<tbody>
					 	<tr>
					 		<td className="nor-td" colSpan={team.children.length}>
								<div className="con-box">
									<table className="teamstructure-table">
										<tbody>
											<tr>
												<td style={{width : '60%', textAlign: 'left'}}>
													<b>{team.label}</b>
												</td>
												<td style={{width : '40%',verticalAlign : 'inherit'}}>
													 <button className="btn btn-light  pull-right" onClick={() => this.openTeam('new', team.id)} style={{padding: '0px'}}><span style={{margin: '2px'}} className="fa fa-plus"></span></button> <button className="btn btn-light pull-right" onClick={() => this.openTeam(team.id)} style={{padding: '0px', marginRight:'2px'}}><span style={{margin: '2px'}} className="fa fa-pencil-square-o"></span></button>
												</td>
											</tr>
										</tbody>
									</table>
									<div>
										<ul className="teamstructure-ullist text-left">
											{team.users.map((user, userindex) => {
												return <li key={userindex}>{user.displayname}</li>
											})}
										</ul>
									</div>
								</div>
					 		</td>
					 	</tr>
						{team.children.length > 0 ? <tr>
							<td className="inter-first-td" colSpan={team.children.length}>
								<div className="vert-box">
								</div>
							</td>
						</tr> : null}
						<tr>
							{team.children.map((secteam, secindex) => {
								return (
									<td key={secindex} className="inter-center-td">
										<div className={`inter-first-${secindex == 0 ? (secindex == team.children.length-1 ? 'only' : 'right') : (secindex == team.children.length-1 ? 'left' : 'center')}-div`}></div>
									</td>
								);
							})}
						</tr>
						<tr>
							{team.children.map((secteam, secindex) => {
								return (
									<td key={secindex} className="inter-second-td">
										<div className={`inter-second-${index+1%2 == 1 ? 'right' : 'left'}-div`}></div>
									</td>
								);
							})}
						</tr>
						<tr>
							{team.children.map((secteam, secindex) => this.renderTeam(secteam, secindex))}
						</tr>
					</tbody>
				</table>
			</td>
		)
	}

	render() {
		return (
			<div className="row">
				<div className="col-md-12">
					<div className="panel panel-default">
						<div className="panel-body">
							<div className="col-md-12">
								<h4 className="pull-left">Team Structure</h4>
								<div className="pull-right margintop-10 marginbottom-10">
									<button onClick={() => this.openTeam('new')} className={`btn btn-sm gs-btn-success ${checkActionVerbAccess(this.props.app, 'teamstructure', 'Save') ? '' : 'hide'}`}><span className="fa fa-plus marginright-5"></span>Level</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="col-md-12">
					<div className="alert alert-info ng-binding"> Note: Users that are not part of the Team Structure will have access to all transactions. Please add only users that need restricted access as part of the Team Structure. </div>
				</div>
				<div className="col-md-12" style={{marginBottom:'50px'}}>
					<div className="fulldiv">
						<table style={{marginBottom: '25px'}}>
							<tbody>
								<tr>
									{this.state.treeArray ? this.state.treeArray.map((team, index) => this.renderTeam(team, index)) : null}
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
}

TeamStructure = connect(
	(state, props) => {
		let formName = 'TeamStructure';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState}
)(reduxForm()(TeamStructure));

export default TeamStructure;
