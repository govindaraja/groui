import React, { Component } from 'react';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';
import moment from 'moment';

import { commonMethods, modalService } from '../utils/services';
import { booleanfilter, dateFilter, datetimeFilter, timeFilter} from '../utils/filter';
import { SelectAsync } from '../components/utilcomponents';
import { ButtongroupEle } from '../components/formelements';

import DocumentuploadModal from './documentuploadmodal';
import EmailModal from '../components/details/emailmodal';
import ViewExistingEmailModal from '../components/details/viewexistingemailmodal';
import RecordingplayModal from '../components/details/recording';

import { Newactivityhistorysection } from '../components/details/activitysections';
import NextActivityConfirmModal from '../components/details/nextactivityalertmodal';

const ActivitySection = connect((state)=>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showactivity: false,
			addActivityShow: true,
			activity: {
				mindate: new Date(),
				skip: 1
			},
			salesactivities : [],
			activityhistorydetails : []
		};
		this.addActivity = this.addActivity.bind(this);
		this.openActivity = this.openActivity.bind(this);
		this.getactivities = this.getactivities.bind(this);
		this.sendEmail = this.sendEmail.bind(this);
		this.emailFn = this.emailFn.bind(this);
		this.openEmail = this.openEmail.bind(this);
		this.playRecording = this.playRecording.bind(this);
	}

	componentWillMount() {
		this.getactivities();
	}

	playRecording(activity) {
		this.props.openModal({
			render: (closeModal) => {
				return <RecordingplayModal
						callid = {activity.callid}
						closeModal = {closeModal} 
						openModal = {this.props.openModal}/>
			},
			className: {
				content: 'react-modal-custom-class-30',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	getactivities(param) {
		let { activity, salesactivities, showactivity } = this.state;
		if(this.props.parentid) {
			salesactivities = [];
			let pagelengthStr = '';
			if(!param) {
				pagelengthStr += `&pagelength=3`;
			}

			var queryString = `/api/query/getsalesactivitydetailsquery?parentid=${this.props.parentid}&parentresource=${this.props.resourcename}&leadid=${this.props.leadid}&userid=${this.props.app.user.id}${pagelengthStr}`;

			axios.get(queryString).then((response)=> {
				for (var i = 0; i < response.data.main.length; i++)
					salesactivities.push(response.data.main[i]);

				if (salesactivities.length >= response.data.count)
					showactivity = false;
				else
					showactivity = true;
				this.setState({salesactivities : salesactivities, activity : activity, showactivity: showactivity,activityhistorydetails : salesactivities});
			});
		}
	}

	addActivity() {

		let tempObj = {
			parentresource: this.props.resourcename,
			parentid: this.props.parentid,
			partnerid: this.props.customerid,
			tempactivitycategory : 'Sales Followup',
			contactid: this.props.contactid,
			disableinput: true,
			hidehistorypartnersection: true
		};

		if(this.props.showrelatedto) {
			tempObj.showrelatedtosection = true;
		} else {
			tempObj.showrelatedtosection = false;
		}
		if(this.props.resourcename == 'leads') {
			tempObj.quoteid = this.props.quoteid;
			tempObj.leadid = this.props.parentid;
		}
		if(this.props.resourcename == 'quotes') {
			tempObj.leadid = this.props.leadid;
			tempObj.quoteid = this.props.parentid;
		}
		if(this.props.resourcename == 'projectquotes') {
			tempObj.leadid = this.props.leadid;
			tempObj.projectquoteid = this.props.parentid;
		}
		this.props.createOrEdit('/createActivity', null, tempObj, (responseobj) => {
			if(this.props.modifiedcallback && responseobj && Object.keys(responseobj).length > 0) {
			this.props.modifiedcallback(responseobj.modified);
			}
			this.getactivities();
		});
	}

	openActivity = function(activity) {
		this.props.createOrEdit('/details/activities/:id',  activity.salesactivityid , {
			hidehistorypartnersection: true
		}, (responseobj) => {
			if(this.props.modifiedcallback && responseobj && Object.keys(responseobj).length > 0) {
				this.props.modifiedcallback(responseobj.modified);
			}
			this.getactivities();
		});
	}

	addActivities() {
		let { activity } = this.state;

		this.updateLoaderFlag(true);
		if(activity.nextfollowup) {
			if (activity.calltime) {
				activity.nextfollowup.setHours(activity.calltime.getHours());
				activity.nextfollowup.setMinutes(activity.calltime.getMinutes());
			} else {
				activity.nextfollowup.setHours(new Date().getHours());
				activity.nextfollowup.setMinutes(new Date().getMinutes());
			}
		}
		activity.parentresource = this.props.resourcename;
		activity.parentid = this.props.parentid;
		if(this.state.resource == 'leads') {
			activity.quoteid = this.props.quoteid;
			activity.leadid = this.props.parentid;
		}
		if(this.props.resourcename == 'quotes') {
			activity.leadid = this.props.leadid;
			activity.quoteid = this.props.parentid;
		}
		if(this.props.resourcename == 'projectquotes') {
			activity.leadid = this.props.leadid;
			activity.projectquoteid = this.props.parentid;
		}

		var tempData = {
			actionverb : 'Save',
			data : activity
		};
		axios({
			method : 'post',
			data : tempData,
			url : '/api/salesactivities'
		}).then((response)=> {
			if (response.data.message == 'success') {
				if(this.props.parentid && this.props.expectedclosuredate!=null && new Date(this.props.expectedclosuredate) < new Date(activity.nextfollowup)) {
					var message = {
						header : 'Warning',
						body : 'Expected closure date is before the next follow up date. Please revise your expected closure date',
						btnName : ['Ok']
					};
					this.props.openModal(modalService.infoMethod(message));
				}
				this.setState({
					activity: {},
					addActivityShow: true
				}, ()=>{
					this.getactivities();
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	sendEmail(emaildetails) {
		let parentresource = {
			companyid : this.props.app.selectedcompanyid,
			id: this.props.parentid
		};
		let initializeObj = {
			toaddress: this.props.contactemail,
			id: this.props.parentid,
			startdatetime: new Date()
		};

		if(emaildetails && Object.keys(emaildetails).length > 0) {
			initializeObj.subject = emaildetails.subject;
			initializeObj.inreplyto = emaildetails.inreplyto;
			initializeObj.inreplyto_subject = emaildetails.inreplyto_subject;
			initializeObj.emailaccountid = emaildetails.emailaccountid;
			initializeObj.fromaddress = emaildetails.frommail;
			initializeObj.toaddress = emaildetails.tomail;
			initializeObj.cc = emaildetails.cc;
			initializeObj.bcc = emaildetails.bcc;
		}
		this.props.openModal({render: (closeModal) => {return <EmailModal parentresource={parentresource} app={this.props.app} resourcename={this.props.resourcename} activityemail={true} firsttimefalse={true} openModal={this.props.openModal} closeModal={closeModal} callback={this.emailFn} initializeObj={initializeObj} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
	}

	emailFn (emailObj, callback) {
		if(!emailObj.logactivity) {
			emailObj.activitytypeid = null;
			emailObj.startdatetime = null;
			emailObj.enddatetime = null;
		} else {
			emailObj.enddatetime = emailObj.enddatetime ? emailObj.enddatetime : new Date();
		}

		axios({
			method : 'post',
			data : {
				actionverb : 'Send Email',
				data : emailObj
			},
			url : `/api/${this.props.resourcename}`
		}).then((response) => {
			if (response.data.message == 'success') {
				callback(null, true);
				this.getactivities();
			}
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		});
	}

	openEmail(emailobj) {
		this.props.openModal({render: (closeModal) => {
			return <ViewExistingEmailModal emailobj={emailobj} app={this.props.app} openModal={this.props.openModal} closeModal={closeModal} callback={(emaildetails) => {this.sendEmail(emaildetails);}} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}, confirmModal: true});
	}

	render() {
		return (
			<div className="col-md-12">
				<div className="card gs-card">
					<div className="card-header gs-card-header">Activities and Emails
						<div className="float-right">
							<div className="btn gs-form-btn-primary btn-sm btn-width" onClick={this.addActivity}><i className="fa fa-plus"></i>Activity</div>
							<div className="btn gs-form-btn-primary btn-sm btn-width" onClick={() => this.sendEmail()}><i className="fa fa-envelope"></i>Email</div>
						</div>
					</div>
					<div className="card-body">
						<div className="row">
							{/*<div className={`col-md-12 col-sm-12 marginbottom-10 ${this.state.addActivityShow ? 'showsection' : 'hidesection'}`}>
															{this.state.salesactivities == '' ? <div className="text-center">No activities created</div> : null }
														</div>*/}
							<Newactivityhistorysection resource={this.state} getActivities={this.getactivities} openactivity={this.openActivity} openemail={this.openEmail}/>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

export default ActivitySection;
