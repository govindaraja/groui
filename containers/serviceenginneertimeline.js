import React, {Component} from 'react';
import moment from 'moment';
import { dateFilter, datetimeFilter, timeFilter, arrayFilter} from '../utils/filter';
import { Rnd } from "react-rnd";
import Avatar from 'react-avatar';

import {Field, reduxForm } from 'redux-form';
import { DateEle, InputEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, search } from '../utils/utils';

const style = {
	position: "absolute",
	transform: "",
	border: "solid 1px #ddd",
	background: "#fff"
};

export default class Basic extends Component {
	constructor(props){
		super(props);

		this.rnd = {}

		this.state = {
			search: {},
			height: this.props.height > 25 ? this.props.height : 60,
			hourWidth: this.props.hourWidth > 25 ? this.props.hourWidth : 90,
		};

		this.rndRefresh = {};

		this.renderEvents = this.renderEvents.bind(this);
		this.onDragFinished = this.onDragFinished.bind(this);
		this.onResizeStop = this.onResizeStop.bind(this);
		this.timelinediv = this.timelinediv.bind(this);
		this.timeslotonClick = this.timeslotonClick.bind(this);
		this.engineerOnSelect = this.engineerOnSelect.bind(this);
	}

	componentWillReceiveProps(nextprops) {
		let allocationObj = {};
		Object.keys((this.props.employeeAllocationObj || {})).forEach(key => {
			this.props.employeeAllocationObj[key].forEach(item => {
				allocationObj[item.val.id] = item;
			});
		});
		Object.keys((nextprops.employeeAllocationObj || {})).forEach(key => {
			nextprops.employeeAllocationObj[key].forEach(item => {
				if(allocationObj[item.val.id] && item.x != allocationObj[item.val.id].x && this.rnd[item.val.id]) {
					this.rndRefresh[item.val.id] = (this.rndRefresh[item.val.id] ? this.rndRefresh[item.val.id] : 0) + 1;
				}
			});
		});
	}

	onDragFinished(allocation, e, d) {
		if(allocation.val.status != 'Planned')
			return this.props.onAllocationSelect(allocation.val);

		console.log("onDragFinished");

		let duration = Math.round(new Date(allocation.val.end) - new Date(allocation.val.start)) / (60 * 1000);

		let starthour = Math.floor(d.x / this.state.hourWidth);
		let startminutesacc = Number(((d.x % this.state.hourWidth) / (this.state.hourWidth / 60)).toFixed());
		let startminutes = Math.round((d.x % this.state.hourWidth) / (this.state.hourWidth / 4)) * 15;

		let startTime = new Date(new Date(allocation.val.start).setHours(starthour, startminutes, 0, 0));
		let startTimeAcc = new Date(new Date(allocation.val.start).setHours(starthour, startminutesacc, 0, 0));
		let endTime = new Date(new Date(startTime).setMinutes(startTime.getMinutes() + duration, 0, 0, 0));

		let draggedTime = Math.abs(new Date(allocation.val.start) - new Date(startTime)) / (60 * 1000);
		let draggedTimeAcc = Math.abs(new Date(allocation.val.start) - new Date(startTimeAcc)) / (60 * 1000);

		if(draggedTimeAcc > 5 && draggedTime > 0)
			this.props.onTimelineChange(allocation.val, startTime, endTime);
		else
			this.props.onAllocationSelect(allocation.val);
	}

	onResizeStop(allocation, e, direction, ref, delta, position) {
		console.log("onResizeStop");
		let durationHour = Math.floor(Math.abs(delta.width) / this.state.hourWidth);
		//let durationMinutes = Number(((Math.abs(delta.width) % this.state.hourWidth) / (this.state.hourWidth / 60)).toFixed());
		let durationMinutes = Math.round((Math.abs(delta.width) % this.state.hourWidth) / (this.state.hourWidth / 4)) * 15;

		let totalMinutes = ((durationHour * 60) + durationMinutes) * (delta.width > 0 ? 1 : '-1');

		let startTime, endTime;

		if(direction == 'left') {
			startTime = new Date(new Date(allocation.val.start).setMinutes(new Date(allocation.val.start).getMinutes() - totalMinutes));
			endTime = new Date(allocation.val.end);
		} else {
			startTime = new Date(allocation.val.start);
			endTime = new Date(new Date(allocation.val.end).setMinutes(new Date(allocation.val.end).getMinutes() + totalMinutes));
		}

		let draggedStartTime = Math.abs(new Date(allocation.val.start) - new Date(startTime)) / (60 * 1000);
		let draggedEndTime = Math.abs(new Date(allocation.val.end) - new Date(endTime)) / (60 * 1000);

		if(draggedStartTime > 5 || draggedEndTime > 5)
			this.props.onTimelineChange(allocation.val, startTime, endTime);
	}

	updatePosition(item) {
		if(this.rnd[item.val.id]) {
			this.rnd[item.val.id].updatePosition({
				x: item.x,
				y: 0
			});
		}
	}

	renderEvents(el, allocationArray, stepObj, step) {
		return allocationArray.map((allocation, index) => {
			let itemstyle = {
				...style,
				transform: "translate(0px, 0px)",
				opacity: allocation.val.status == 'Completed' ? 0.6 : 1
			};
			let draggingStyle = {};
			if(allocation.val.status != 'Planned') {
				//draggingStyle.disableDragging = true;
				draggingStyle.enableResizing = false;
			}

			let isPlanned = allocation.val.plannedstarttime && allocation.val.status == 'Planned' && !allocation.val.travelstarttime && !allocation.val.startdatetime ? true : false;
			let isDriving = allocation.val.status == 'Planned' && allocation.val.travelstarttime && !allocation.val.startdatetime ? true : false;
			let isOngoing = allocation.val.startdatetime && !allocation.val.enddatetime ? true : false;
			let isCompleted = allocation.val.status == 'Completed' && allocation.val.startdatetime && allocation.val.enddatetime ? true : false;

			let topBarColor = allocation.val.servicecallid && !allocation.val.relatedactivityid ? (allocation.val.servicecallid_calltype == 'Service' ? '#ffad3e' : '#9a00ff') : '#7f7f7f';

			if(step != (allocation.y / this.state.height))
				return null;

			let key = `${allocation.val.id}_${this.rndRefresh[allocation.val.id] ? this.rndRefresh[allocation.val.id] : 0}`;

			return (
 				<Rnd
				key = {key}
				ref = {c => {this.rnd[allocation.val.id] = c;}}
				style = {itemstyle}
				size = {{width: allocation.width,height: this.state.height - 10}}
				position = {{x: allocation.x,y: 5}}
				bounds = {"parent"}
				maxHeight = {`${this.state.height-10}px`}
				minHeight = {`${this.state.height-10}px`}
				dragGrid={[10, 10]}
				lockAspectRatioExtraWidth={10}
				title={`${allocation.val.activitytypeid_category} - ${allocation.val.status} ${allocation.val.servicecallid_servicecallno ? ` - ${allocation.val.servicecallid_servicecallno}` : ''} - ${allocation.val.assignedto_displayname} - ${datetimeFilter(allocation.val.start)} - ${datetimeFilter(allocation.val.end)}`}
				{...draggingStyle}
				onDragStop = {(e, d) => this.onDragFinished(allocation, e, d)}
				onResizeStop = {(e, direction, ref, delta, position) => this.onResizeStop(allocation, e, direction, ref, delta, position)}>
					<div style={{height: '8px', width: '100%', backgroundColor: topBarColor}}></div>
					<div className="d-flex flex-row" style={{height: '80%'}}>
						<div className="d-flex" style={{padding: '6px', textAlign: 'center'}}>
							{isPlanned ? <img src="/images/svc-eng-assign-scheduled-icon.svg" /> : null}
							{isDriving ? <img src="/images/svc-eng-assign-driving-icon.svg" /> : null}
							{isOngoing ? <img src="/images/svc-eng-assign-ongoing-icon.svg" /> : null}
							{isCompleted ? <img src="/images/svc-eng-assign-completed-icon.svg" /> : null}
						</div>
						<div className="font-12" style={{whiteSpace: 'nowrap', textOverflow: 'ellipsis', overflow: 'hidden'}}>
							<div style={{whiteSpace: 'nowrap', textOverflow: 'ellipsis', overflow: 'hidden'}} title={allocation.val.serviceCallDetails ? allocation.val.serviceCallDetails.customerid_name : null}>
								{allocation.val.serviceCallDetails ? allocation.val.serviceCallDetails.customerid_name : null}
							</div>
							<div style={{whiteSpace: 'nowrap', textOverflow: 'ellipsis', overflow: 'hidden'}} title={allocation.val.servicecallid ? allocation.val.servicecallid_calltype : allocation.val.activitytypeid_name}>
								{allocation.val.serviceCallDetails && allocation.val.serviceCallDetails.installationaddressid_city ? `${allocation.val.serviceCallDetails.installationaddressid_city} .` : null} {allocation.val.servicecallid ? allocation.val.servicecallid_calltype : allocation.val.activitytypeid_name}
								</div>
						</div>
					</div>
				</Rnd>
			);
		});
	}

	renderTimeline(step, el, employeeAllocationObj, bgcolor) {
		let timeLineArray = [];

		let HalfDayleavehours = [];
		if(el.allvalue.employeeid_leave && el.allvalue.employeeid_leavetype == 'Half Day') {
			let date1 = new Date(el.allvalue.employeeid_workstarttime).getTime();
			let date2 = new Date(el.allvalue.employeeid_workendtime).getTime();
			let hours = (date2 - date1)/1000/60/60;
			let halfHours = hours/2;
			let startHour = new Date(el.allvalue.employeeid_workstarttime).getHours();
			HalfDayleavehours.push(startHour);
			for(var i=1;i<halfHours;i++) {
				HalfDayleavehours.push(startHour+i);
			}
		}

		let current_hour_left = Number(((((new Date().getHours() * 60) + new Date().getMinutes()) / 60) * this.state.hourWidth).toFixed());

		if(new Date(this.props.resource.allocationdate).setHours(0,0,0,0) == new Date().setHours(0,0,0,0))
			timeLineArray.push(<div key={"timeline"} className="text-center" style={{width: '1px', height: `${this.state.height * step}px`, position: "absolute", borderLeft: "1px solid #ff0000", left: `${current_hour_left}px`}}></div>);

		for(var i=0;i<24;i++) {
			let timelinestyle = {
				width: `${this.state.hourWidth}px`,
				position: "absolute",
				height: `${this.state.height * step}px`,
				borderLeft: "2px dashed #eaebeb",
				left: `${(this.state.hourWidth * i)}px`
			};
			if(i == 23)
				timelinestyle.borderRight = "2px dashed #eaebeb";

			if(el.allvalue.employeeid_leave && el.allvalue.employeeid_leavetype == 'Full Day' || (HalfDayleavehours.length > 0 && HalfDayleavehours.includes(i)))
				timelinestyle.background = `repeating-linear-gradient(41deg, #ddd, #ddd 1px, ${bgcolor} 1px, ${bgcolor} 20px)`;


			timeLineArray.push(<div data-key={i} key={i} style={{...timelinestyle}} onClick={(evt)=>this.timeslotonClick(evt, el, (employeeAllocationObj[el.id] || []))}></div>);
		}

		return timeLineArray;
	}

	timeslotonClick(evt,el, allocationArray) {
		let x = evt.pageX - $(`*[data-key=${evt.target.getAttribute('data-key')}]`).offset().left;
		let divWidth = $(`*[data-key=${evt.target.getAttribute('data-key')}]`)[0].offsetWidth;

		let divWidthMin = divWidth/60;
		let currentMin = x / divWidthMin;

		let plannedstarttime;

		if(allocationArray.length > 0) {
			let tempPlannedTime = new Date(new Date(this.props.resource.allocationdate).setHours(evt.target.getAttribute('data-key'), currentMin));

			let latestdateObj = allocationArray.sort((a, b) => {
				return new Date(b.val.end).getTime() - new Date(a.val.end).getTime();
			})[0];
			let latestdate = new Date(latestdateObj.val.end);
			let diff = (tempPlannedTime.getTime() - latestdate.getTime()) / 1000 / 60;
			if((diff < 0) || (diff < 30 && (evt.target.getAttribute('data-key') == latestdate.getHours())))
				plannedstarttime = latestdate;
			else {
				currentMin = currentMin < 15 ? 0 : ((currentMin < 30 || currentMin < 45) ? 30 : (currentMin > 45 ? 60 : currentMin));
				plannedstarttime = new Date(new Date(this.props.resource.allocationdate).setHours(evt.target.getAttribute('data-key'), currentMin));
			}
		} else {
			currentMin = currentMin < 15 ? 0 : ((currentMin < 30 || currentMin < 45) ? 30 : (currentMin > 45 ? 60 : currentMin));
			plannedstarttime = new Date(new Date(this.props.resource.allocationdate).setHours(evt.target.getAttribute('data-key'), currentMin));
		}

		this.props.updateFormState(this.props.form, {
			selectedplannedstarttime: plannedstarttime
		});
		setTimeout(()=>this.props.createAllocation(el), 0);
	}

	engineerOnSelect(el) {
		let plannedstarttime = null;
		this.props.updateFormState(this.props.form, {
			selectedplannedstarttime: plannedstarttime
		});
		setTimeout(() => this.props.createAllocation(el), 0);
	}

	renderTime(step) {
		let timeArray = [];

		for(var i=0;i<24;i++) {
			timeArray.push(
				<div key={i} className="text-center" style={{width: `${this.state.hourWidth / (i == 0 ? 2 : 1)}px`, height: `30px`, float: 'left'}}>
					{i == 0 ? null : ''}
					{i > 0 && i <= 11 ? `${i} AM` : ''}
					{i == 12 ? `12 PM` : ''}
					{i > 12 ? `${i % 12} PM` : ''}
				</div>);
		}

		return timeArray;
	}

	timelinediv(node) {
		if(node) {
			node.addEventListener('scroll', (evt) => {
				this.refs.timediv.scrollLeft = evt.srcElement.scrollLeft;
			});
		}
	}

	renderStep(step, employeeAllocationObj, allocationStepObj, el) {
		let stepArray = [];

		for(var i=0; i<step; i++) {
			stepArray.push(
				<div key={i} style={{width: `${this.state.hourWidth * 24}px`, height: `${this.state.height}px`, float: 'left'}}>
					{employeeAllocationObj ? this.renderEvents(el, (employeeAllocationObj[el.id] || []), allocationStepObj[el.id], i) : null}
				</div>
			);
		}

		return stepArray;
	}

	render() {
		let {employeeAllocationObj, employeeStepObj, allocationStepObj} =  this.props;

		let filterengineerArray = arrayFilter([...(this.props.employees || [])], this.props.resource.engineersearch, ['userid_displayname']);
		let filterendidObj = {};
		filterengineerArray.forEach(eng => {
			filterendidObj[eng.id] = 1;
		});
		let engineerArray = [...(this.props.employees || [])];
		let pageHeight = $(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - $('.gs-eng-assign-gantt-header1').outerHeight() - $('.gs-eng-assign-gantt-header2').outerHeight() - 20;

		let empfirstcount = -1, empsecondcount = -1;

		return (
			<div className="row no-gutters">
				<div className="col-md-3 pr-0 gs-eng-assign-gantt-header1" style={{borderTop: '1px solid rgba(0,0,0,.125)', borderRight: '1px solid rgba(0,0,0,.125)', borderLeft: '1px solid rgba(0,0,0,.125)'}}>
					<div className="bg-white gs-uppercase semi-bold" style={{padding: '8px 12px', height: '100%', color: '#1ABC9C'}}>Service Engineers</div>
				</div>
				<div className="col-md-9 pr-0"  style={{borderTop: '1px solid rgba(0,0,0,.125)', borderRight: '1px solid rgba(0,0,0,.125)', borderBottom: '1px solid rgba(0,0,0,.125)'}}>
					<div className="bg-white" style={{padding: '8px 12px', height: '100%'}}>
						<div className="form-group mb-1 mr-2" style={{width: '200px', float: 'left'}}>
							<Field name={'allocationdate'} classname="gs-datepicker-remove-close-icon" component={DateEle} props={{onChange: this.props.change.allocationDate, required: true}} validate={[dateNewValidation({required:  true, title : 'Allocation Date'})]}/>
						</div>
						<div className="form-group mb-1" style={{width: '200px', float: 'left'}}>
							<button type="button" className="btn btn-sm gs-form-btn-success mr-2" onClick={()=>this.props.change.prev()}><span className="fa fa-caret-left"></span></button>
							<button type="button" className="btn btn-sm gs-form-btn-success" onClick={()=>this.props.change.next()}><span className="fa fa-caret-right"></span></button>
						</div>
						<div className="form-group mb-1 mr-2" style={{width: '200px', float: 'right'}}>
							<Field name={'viewtype'} props={{options: ['All Activities', 'Service Assignment'], onChange: this.props.change.viewtype, required: true}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'View Type'})]}/>
						</div>
					</div>
				</div>
				<div className="col-md-3 pr-0 gs-eng-assign-gantt-header2" style={{borderRight: '1px solid rgba(0,0,0,.125)', borderLeft: '1px solid rgba(0,0,0,.125)'}}>
					<div className="bg-white" style={{padding: '0px 12px 8px 12px'}}>
						<Field name={'engineersearch'} component={InputEle} validate={[stringNewValidation({title : 'engineersearch'})]} />
					</div>
				</div>
				<div className="col-md-9 pl-0  bg-white" ref="timediv" style={{overflow: 'hidden', borderBottom: '1px solid rgba(0,0,0,.125)'}}>
					<div style={{width: `${this.state.hourWidth * 24}px`, float: 'left', paddingTop: '8px'}} >
						{this.renderTime()}
					</div>
				</div>
				<div className="row no-gutters gs-scrollbar bg-white" style={{height: `${pageHeight}px`, maxHeight: `${pageHeight}px`, overflow: 'auto'}}>
					<div className="col-md-3 pr-0" style={{border: '1px solid rgba(0,0,0,.125)'}}>
						{engineerArray.map((el, index) => {
							let filterClassName = filterendidObj[el.id] ? '' : 'hide';
							let step = employeeStepObj ? (employeeStepObj[el.id] || 1) : 1;

							if(filterClassName != 'hide')
								empfirstcount++;

							return (
								<div key={index} style={{width: '100%',height: `${step * this.state.height}px`, padding: '12px 12px 12px 21px', cursor: 'pointer', backgroundColor: el.isselected ? 'rgba(26,188,156,0.1)' : (empfirstcount % 2 == 0 ? '#f6fafd' : '#f7f7f7')}} className={filterClassName}>
									<div className={`d-flex flex-row justify-content-center align-items-center h-100`}>
										<div style={{marginRight: '15px'}}>
											<Avatar size="35" round="50%" name={el.userid_displayname} src={`/documents/getfile?url=${el.userid_profileimageurl}`} />
										</div>
										<div style={{flex: '1 1 auto', color: '#042140'}}><div>{el.userid_displayname}</div>
											<div style={{color: el.allvalue.intime ? '#259F79' : '#EE607A', fontSize: '12px'}}>{el.allvalue.intime ? 'Checked in' : 'Not Checked in'}</div>
										</div>
										{el.id ? <div className="svccall-assignment-addbtn" onClick={()=>this.engineerOnSelect(el)}><span className="fa fa-plus"></span></div> : null}
									</div>
								</div>
							);
						})}
					</div>
					<div className="col-md-9 pl-0 gs-gantt-timeline gs-scrollbar" style={{overflow: 'auto hidden'}} ref={this.timelinediv}>
						{engineerArray.map((el, index) => {
							let filterClassName = filterendidObj[el.id] ? '' : 'hide';
							let step = employeeStepObj ? (employeeStepObj[el.id] || 1) : 1;

							if(filterClassName != 'hide')
								empsecondcount++;

							return (
								<div key={index} style={{width: `${this.state.hourWidth * 24}px`, height: `${step * this.state.height}px`, float: 'left', backgroundColor: empsecondcount % 2 == 0 ? '#f6fafd' : '#f7f7f8', position: 'relative'}}  className={filterClassName} >
									{this.renderTimeline(step, el, employeeAllocationObj, empsecondcount % 2 == 0 ? '#f6fafd' : '#f7f7f8')}
									{this.renderStep(step, employeeAllocationObj, allocationStepObj, el)}
								</div>
							);
						})}
					</div>
				</div>
			</div>
		);
	}
}