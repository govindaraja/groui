import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { Link } from 'react-router-dom';
import { v1 as uuidv1 } from 'uuid';
import moment from 'moment';

import { updateFormState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter } from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';
import { InputEle, NumberEle, SpanEle, DateEle, textareaEle, selectAsyncEle, localSelectEle, autoSelectEle, richtextEle, qtyEle, emailEle, checkboxEle, ContactEle, DisplayGroupEle, RateInline, StockInline, AddressEle, autosuggestEle, ButtongroupEle, RateField, DiscountField, TimepickerEle, alertinfoEle, PhoneElement, passwordEle, DocumentLinkEle, ExpenseRequestSummary } from '../components/formelements';
import {stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation } from '../utils/utils';

import { AdditionalInformationSection } from './ndtsection1';
import Printmodal from '../components/details/printmodal';
import EmailModal from '../components/details/emailmodal';

const statusArray = ['Approved','Cancelled','Sent To Customer','Sent To Supplier','Closed','Hold','Won','Lost','Dispatched','Disqualified','Junk','Received', 'Unhold', 'Accepted','Completed','Converted', 'Disbursed'];

class OrderAcknowledgement extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.save = this.save.bind(this);
		this.initialize = this.initialize.bind(this);
		this.printfunction = this.printfunction.bind(this);
		this.emailFn = this.emailFn.bind(this);
	}

	componentWillMount() {
		this.initialize();
	}

	initialize() {
		axios.get(`/api/orderacknowledgements/${this.props.match.params.id}`).then((response) => {
			if(response.data.message == 'success') {
				response.data.main.companyid = response.data.main.orderid_companyid;
				this.props.initialize(response.data.main);
				this.setState({
					ndt: {
						notes: response.data.notes,
						documents: response.data.documents,
						tasks: response.data.tasks
					}
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	openemail() {
		let emailobj = {
			firsttime: true,
			sendemail: true
		};
		this.props.openModal({render: (closeModal) => {return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'orderacknowledgements'} activityemail={false} callback={this.emailFn} openModal={this.props.openModal} closeModal={closeModal} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true});
	}

	emailFn (emailObj, callback) {
		emailObj.id = this.props.resource.id;
		emailObj.modified = this.props.resource.modified;
		let extradocuments = emailObj.extradocuments;

		axios({
			method : 'post',
			data : {
				actionverb : 'Send To Customer',
				data : emailObj
			},
			url : '/api/orderacknowledgements'
		}).then((response) => {
			if (response.data.message == 'success') {
				if (emailObj.firsttime) {
					if (emailObj.sendemail) {
						emailObj = response.data.main;
						emailObj.toaddress = this.props.resource.orderid_email;
						emailObj.bcc = response.data.main.bcc;
						emailObj.cc = response.data.main.cc;
						emailObj.firsttime = false;
						emailObj.extradocuments = extradocuments;
						callback(emailObj, false);
					} else {
						callback(null, true);
						this.props.updateFormState(this.props.form, {
							modified: response.data.main.modified
						});
						let apiResponse = commonMethods.apiResult(response);
						this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
					}
				} else {
					callback(null, true);
					this.props.updateFormState(this.props.form, {
						modified: response.data.main.modified
					});
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			} else {
				callback(null, true);
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	save (param) {
		if (param == 'Send To Customer')
			return this.openemail();

		this.updateLoaderFlag(true);

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : this.props.resource
			},
			url : '/api/orderacknowledgements'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

			if(response.data.message == 'success')
				this.initialize();
			else
				this.updateLoaderFlag(false);
		});
	}

	printfunction() {
		this.props.openModal({
			render: (closeModal) => {
				return <Printmodal app={this.props.app} resourcename={'orderacknowledgements'} companyid={this.props.resource.orderid_companyid} id={this.props.resource.id} openModal={this.props.openModal} closeModal={closeModal}/>
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	render() {
		let taxDiffered = false;
		if(this.props.resource) {
			if (Object.keys(this.props.resource.details.taxes.quote).length == Object.keys(this.props.resource.details.taxes.order).length) {
				for (var prop in this.props.resource.details.taxes.quote) {
					if (!this.props.resource.details.taxes.order[prop])
						taxDiffered = true;
				}
			} else {
				taxDiffered = true;
			}
		}
		let disableclass = this.props.resource && statusArray.indexOf(this.props.resource.status) >= 0 ? 'disablediv' : '';
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row" >
					<div className="col-sm-12 col-md-12 col-lg-9" style={{position: 'fixed', height: '45px', backgroundColor: '#FAFAFA',  zIndex: 5}}>
						<div className="row">
							<div className="col-md-6 col-sm-12 paddingleft-md-30">
								<h6 className="margintop-15 semi-bold-custom"><a className="affixanchor float-left marginright-10" onClick={() => this.props.history.push(`/list/orderacknowledgements`)}><span className="fa fa-chevron-left"></span></a> Order Acknowledgements / {this.props.resource ? this.props.resource.id : null} /{this.props.resource ? <label className="badge gs-badge-success marginleft-10">{this.props.resource.status}</label> : null}</h6>
							</div>
							<div className="col-md-6 col-sm-12  margintop-6 text-right-md">
								<div className="row">
									<div className="col-md-12 col-sm-12  text-right-md">
										<div className="btn btn-sm affixmenu-btn" onClick={this.printfunction}><i className="fa fa-print"></i>Print</div>
									</div>										
								</div>
							</div>
						</div>
					</div>
				</div>
				{this.props.resource ? <form>
					<div className={`row ${disableclass}`}>
						<div className="col-md-12 col-md-9 col-lg-9">
							<div className="col-sm-12 col-md-12" style={{marginTop: `48px`}}>
								<div className="card marginbottom-15 borderradius-0">
									<h6 className="card-header borderradius-0 card-header-custom card-bg-color-custom semi-bold-custom">Order Acknowledgement Details</h6>
									<div className="card-body">
										<div className="row responsive-form-element">
											<div className="form-group col-sm-12 col-md-4">
												<label className="labelclass">Order No</label>
												<Field name={'orderid_orderno'} props={{disabled: true}} component={InputEle} />
											</div>
											<div className="form-group col-sm-12 col-md-4">
												<label className="labelclass">Quote No</label>
												<Field name={'quoteid_quoteno'} props={{disabled: true}} component={InputEle} />
											</div>
											<div className="form-group col-sm-12 col-md-12">
												<table className="table table-bordered">
													<thead>
														<tr>
															<th></th>
															<th>Our Quote</th>
															<th>Your Order</th>
															<th>Remarks</th>
														</tr>
													</thead>
													<tbody>
														<tr className={taxDiffered ? 'difference' : ''}>
															<td>Taxes</td>
															<td>{Object.keys(this.props.resource.details.taxes.quote).map((key) => this.props.resource.details.taxes.quote[key].description).join(', ')}</td>
															<td>{Object.keys(this.props.resource.details.taxes.order).map((key) => this.props.resource.details.taxes.order[key].description).join(', ')}</td>
															<td><Field name={'details.taxes.remarks'} props={{}} component={InputEle} /></td>
														</tr>
														<tr className={this.props.resource.details.paymentterms.quote != this.props.resource.details.paymentterms.order ? 'difference' : ''}>
															<td>Payment Terms</td>
															<td>{this.props.resource.details.paymentterms.quote}</td>
															<td>{this.props.resource.details.paymentterms.order}</td>
															<td><Field name={'details.paymentterms.remarks'} props={{}} component={InputEle} /></td>
														</tr>
														<tr className={this.props.resource.details.modeofdespatch.quote != this.props.resource.details.modeofdespatch.order ? 'difference' : ''}>
															<td>Mode Of Despatch</td>
															<td>{this.props.resource.details.modeofdespatch.quote}</td>
															<td>{this.props.resource.details.modeofdespatch.order}</td>
															<td><Field name={'details.modeofdespatch.remarks'} props={{}} component={InputEle} /></td>
														</tr>
														<tr className={this.props.resource.details.freight.quote != this.props.resource.details.freight.order ? 'difference' : ''}>
															<td>Freight</td>
															<td>{this.props.resource.details.freight.quote}</td>
															<td>{this.props.resource.details.freight.order}</td>
															<td><Field name={'details.freight.remarks'} props={{}} component={InputEle} /></td>
														</tr>
														<tr>
															<td colSpan={2} className="text-right">Notes</td>
															<td colSpan={2}><Field name={'details.remarks'} props={{}} component={InputEle} /></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="col-sm-12 col-md-12">
								<div className="card marginbottom-15 borderradius-0">
									<h6 className="card-header borderradius-0 card-header-custom card-bg-color-custom semi-bold-custom">Item Details</h6>
									<div className="card-body">
										<div className="row responsive-form-element">
											<div className="form-group col-sm-12 col-md-12">
												{this.props.resource.details.itemarray.map((item, index) => {
													return (
														<table key={index} className="table table-bordered">
															<thead>
																<tr>
																	<th>Item : {item.itemid_name}</th>
																	<th>Our Quote</th>
																	<th>Your Order</th>
																	<th>Remarks</th>
																</tr>
															</thead>
															<tbody>
																<tr className={item.itemdescription.quote != item.itemdescription.order ? 'difference' : ''}>
																	<td>Item Description</td>
																	<td>{item.itemdescription.quote}</td>
																	<td>{item.itemdescription.order}</td>
																	<td><Field name={`details.itemarray[${index}].itemdescription.remarks`} props={{}} component={InputEle} /></td>
																</tr>
																<tr className={item.price.quote != item.price.order ? 'difference' : ''}>
																	<td>Price</td>
																	<td>{item.price.quote}</td>
																	<td>{item.price.order}</td>
																	<td><Field name={`details.itemarray[${index}].price.remarks`} props={{}} component={InputEle} /></td>
																</tr>
																<tr className={item.quantity.quote != item.quantity.order ? 'difference' : ''}>
																	<td>Quantity</td>
																	<td>{item.quantity.quote}</td>
																	<td>{item.quantity.order}</td>
																	<td><Field name={`details.itemarray[${index}].quantity.remarks`} props={{}} component={InputEle} /></td>
																</tr>
																<tr className={item.deliverydate.quote != item.deliverydate.order ? 'difference' : ''}>
																	<td>Delivery Date</td>
																	<td>{dateFilter(item.deliverydate.quote)}</td>
																	<td>{dateFilter(item.deliverydate.order)}</td>
																	<td><Field name={`details.itemarray[${index}].deliverydate.remarks`} props={{}} component={InputEle} /></td>
																</tr>
																<tr>
																	<td colSpan={2} className="text-right">Notes</td>
																	<td colSpan={2}><Field name={`details.itemarray[${index}].remarks`} props={{}} component={InputEle} /></td>
																</tr>
															</tbody>
														</table>	
													)
												})}
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="col-md-9 col-lg-9">
								<div className="muted credit text-center sticky-footer">
									<button type="button" className="btn btn-sm btn-secondary" onClick={() => this.props.history.goBack()}><span className="fa fa-times marginright-5" />Close</button> {this.props.resource.status == 'Draft' ? <button type="button" className="btn btn-sm gs-btn-success" disabled={this.props.invalid} onClick={() => this.save('Save')}><span className="fa fa-save marginright-5" />Save</button> : null} {this.props.resource.status == 'Draft' ? <button type="button" className="btn btn-sm gs-btn-success" disabled={this.props.invalid} onClick={() => this.save('Submit')}><span className="fa fa-check marginright-5" />Submit</button> : null} {this.props.resource.status == 'Draft' || this.props.resource.status == 'Submitted' ? <button type="button" className="btn btn-sm gs-btn-success" disabled={this.props.invalid} onClick={() => this.save('Approve')}><span className="fa fa-check marginright-5" />Approve</button> : null} {this.props.resource.status == 'Approved' ? <button type="button" className="btn btn-sm gs-btn-success" disabled={this.props.invalid} onClick={() => this.save('Send To Customer')}><span className="fa fa-envelope marginright-5" />Send To Customer</button> : null}
								</div>
							</div>
						</div>
						<div className={`col-sm-12 ndt-section col-md-12 col-lg-3 paddingleft-md-0 margintop-5 `} >
							{this.props.resource && this.props.resource.id && this.state.ndt ? <AdditionalInformationSection ndt={this.state.ndt} key={0} parentresource={'orderacknowledgements'} openModal={this.props.openModal} parentid={this.props.resource.id} relatedpath={this.props.match.path} createOrEdit = {this.props.createOrEdit} app ={this.props.app} history={this.props.history} /> : null }
						</div>
					</div>
				</form> : null}
			</div>
		);
	}
}

OrderAcknowledgement = connect(
	(state, props) => {
		let formName = 'Order Acknowledgements';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState}
)(reduxForm()(OrderAcknowledgement));

export default OrderAcknowledgement;
