import React, { Component } from 'react';

export default class TableRow extends Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.addItemtype = this.addItemtype.bind(this);
	}

	checkboxOnChange(evt) {
		this.setState({
			itemtype: evt.target.name
		});
	}

	addItemtype() {
		console.log(this.state);
		this.props.callback(this.state.itemtype);
		this.props.closeModal();
	}

	render() {		
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Item Type</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-8 offset-md-2">
							<div className="row">
								<div className="col-md-6 form-check">
									<input className="form-check-input" type="radio" name="Section" id="itemtype1" value={this.state.itemtype} checked={this.state.itemtype === 'Section'} onChange={(evt)=>this.checkboxOnChange(evt)} />
									<label className="gs-form-check-label" htmlFor="itemtype1">Section</label>
								</div>
								<div className="col-md-6 form-check">
									<input className="form-check-input" type="radio" name="Item" id="itemtype2" value={this.state.itemtype} checked={this.state.itemtype === 'Item'} onChange={(evt)=>this.checkboxOnChange(evt)} />
									<label className="gs-form-check-label" htmlFor="itemtype2">Item</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addItemtype}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}