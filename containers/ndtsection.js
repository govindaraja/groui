import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';
import { booleanfilter, dateFilter, datetimeFilter, timeFilter} from '../utils/filter';
import { SelectAsync, DateElement } from '../components/utilcomponents';
import { Buttongroup } from '../components/formelements';
import { checkActionVerbAccess } from '../utils/utils';

import DocumentuploadModal from './documentuploadmodal';
import Loadingcontainer from '../components/loadingcontainer';
import ViewExistingEmailModal from '../components/details/viewexistingemailmodal';

export const AdditionalInformationSection = connect((state)=>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			notesSelected : true,
			ndt: {
				notes: [],
				documents: [],
				tasks: [],
				emails: [],
				audits: []
			},
			newnote: {
				isrestricted : false
			},
			newtask: {},
			showNotes : false,
			showTasks : false,
			showEmails : false,
			showAudits : false,
			accordionClasses : ["", "", "", "", "", ""]
		};
		this.getnotes = this.getnotes.bind(this);
		this.getdocuments = this.getdocuments.bind(this);
		this.gettasks = this.gettasks.bind(this);
		this.getemails = this.getemails.bind(this);
		this.selectTab = this.selectTab.bind(this);
		this.getDestinationIcon = this.getDestinationIcon.bind(this);
		this.getfiletype = this.getfiletype.bind(this);
		this.getFileUrl = this.getFileUrl.bind(this);
		this.deleteDocument = this.deleteDocument.bind(this);
		this.openLibraryUploadModal = this.openLibraryUploadModal.bind(this);
		this.renderNotes = this.renderNotes.bind(this);
		this.renderDocuments = this.renderDocuments.bind(this);
		this.renderTasks = this.renderTasks.bind(this);
		this.renderEmails = this.renderEmails.bind(this);
		this.openAddNote = this.openAddNote.bind(this);
		this.openAddTask = this.openAddTask.bind(this);
		this.recalculateHeight = this.recalculateHeight.bind(this);
		this.getActiveAccordion = this.getActiveAccordion.bind(this);
		this.getActiveAccordion = this.getActiveAccordion.bind(this);
		this.collapseAccordion = this.collapseAccordion.bind(this);
		this.openEmail = this.openEmail.bind(this);
		this.getResourceName = this.getResourceName.bind(this);
	}

	componentWillMount() {
		let audits = [], notes = [];
		this.props.ndt.notes.map((note) => {
			if(note.action) {
				audits.push(note);
			}
			if(note.notes && !note.action) {
				notes.push(note);
			}
		});
		this.state.ndt.notes = notes.sort((a, b) => {
			return b.id - a.id;
		});
		this.state.ndt.audits = audits.sort((a, b) => {
			return b.id - a.id;
		});
		this.state.ndt.documents = this.props.ndt.documents ? this.props.ndt.documents.sort((a, b)=>{
			return b.id - a.id;
		}) : [];
		this.state.ndt.tasks = this.props.ndt.tasks ? this.props.ndt.tasks.sort((a, b)=>{
			return b.id - a.id;
		}) : [];
		this.getemails(0);
	}

	selectTab(param) {
		if(param == 'notes') {
			this.setState({
				notesSelected : true,
				documentsSelected : false,
				tasksSelected : false,
				emailsSelected : false
			});
		} else if(param == 'documents') {
			this.setState({
				notesSelected : false,
				documentsSelected : true,
				tasksSelected : false,
				emailsSelected : false
			});
		} else if(param == 'tasks') {
			this.setState({
				notesSelected : false,
				documentsSelected : false,
				tasksSelected : true,
				emailsSelected : false
			});
		} else if(param == 'emails') {
			this.setState({
				notesSelected : false,
				documentsSelected : false,
				tasksSelected : false,
				emailsSelected : true
			});
			this.renderEmails();
		}
	}

	getResourceName() {
		return this.props.app.myResources[this.props.parentresource] && this.props.app.myResources[this.props.parentresource].type == 'lookup' ? this.props.app.myResources[this.props.parentresource].lookupResource : this.props.parentresource;
	}

	getnotes() {
		let { ndt } = this.state;

		let filterString = `notes.parentresource='${this.getResourceName()}' and notes.parentid=${this.props.parentid}`;
		filterString = encodeURIComponent(filterString);

		let queryString = `/api/notes?field=id,users/displayname/createdby,users/displayname/modifiedby,notes,isrestricted,created,action,audittrialhistory&filtercondition=${filterString}`;

		axios.get(queryString).then((response)=> {
			if(response.data.message=='success') {
				let audits = [], notes = [];
				response.data.main.map((note) => {
					if(note.action) {
						audits.push(note);
					}
					if(note.notes && !note.action) {
						notes.push(note);
					}
				});
				ndt.notes = notes.sort((a, b) => {
					return b.id - a.id;
				});
				ndt.audits = audits.sort((a, b) => {
					return b.id - a.id;
				});
				this.setState({ndt});
			}
		});
	}

	getdocuments() {
		let { ndt } = this.state;
		let filterString = `documents.parentresource='${this.getResourceName()}' and documents.parentid=${this.props.parentid}`;
		filterString = encodeURIComponent(filterString);

		var queryString = `/api/documents?field=id,path,users/displayname/createdby,name,created,destination,filesize,parentresource,parentid&filtercondition=${filterString}`;

		axios.get(queryString).then((response)=> {
			ndt.documents = response.data.main;
			this.setState({ndt});
		});
	}

	gettasks() {
		let { ndt } = this.state;
		let filterString = `tasks.parentresource='${this.getResourceName()}' and tasks.parentid=${this.props.parentid}`;
		filterString = encodeURIComponent(filterString);

		var queryString = `/api/tasks?field=id,users/displayname/createdby,users/displayname/createdby,users/displayname/assignedto,description,status,dueby,created,completedon&filtercondition=${filterString}`;

		axios.get(queryString).then((response)=> {
			ndt.tasks = response.data.main;
			this.setState({ndt});
		});
	}

	getemails(accordionNum) {
		let { ndt } = this.state;
		var queryString = `/api/query/getsalesactivitydetailsquery?parentid=${this.props.parentid}&parentresource=${this.getResourceName()}&userid=${this.props.app.user.id}&ndtemail=true`;

		axios.get(queryString).then((response)=> {
			ndt.emails = response.data.main;
			this.setState({ndt}, ()=> {
				if(accordionNum == 0)
					this.collapseAccordion(0);
			});
		});
	}

	deleteDocument(param, resource) {
		var tempData = {
			actionverb : 'Delete',
			data : param
		};
		axios({
			method : 'post',
			data : tempData,
			url : `/api/${resource}`
		}).then((response)=> {
			if(resource == 'documents')
				this.getdocuments();
			else
				this.getnotes();
		});
	}

	getDestinationIcon (destination) {
		if(['googledrive', 'dropbox'].indexOf(destination) >= 0) {
			if (destination == 'googledrive') {
				return 'fa-google';
			}
			else if (destination == 'dropbox') {
				return 'fa-dropbox';
			}
		} else
			return "fa-file";
	}

	getfiletype (fileParam) {
		if (fileParam) {
			var filetype = fileParam.name.split('.')[fileParam.name.split('.').length-1];
			if (filetype == 'txt' || filetype =='csv' || filetype == 'rtf') {
				return 'fa-file-text-o';
			}
			else if (filetype == 'gzip' || filetype == 'zip' || filetype == 'rar' || filetype == 'tar') {
				return 'fa-file-zip-o';
			}
			else if (filetype == 'pdf') {
				return 'fa-file-pdf-o';
			}
			else if (filetype == 'jpg' || filetype == 'png' || filetype == 'gif' || filetype == 'jpeg' || filetype == 'ico' || filetype == 'bmp') {
				return 'fa-file-image-o';
			}
			else if (filetype == 'doc' || filetype == 'docx' || filetype == 'dot' || filetype == 'dotx') {
				return 'fa-file-word-o';
			}
			else if (filetype == 'xlsx' || filetype == 'xls') {
				return 'fa-file-excel-o';
			}
			else if (filetype == 'pptx' || filetype == 'pptm') {
				return 'fa-file-powerpoint-o';
			}
			else {
				return "fa-file";
			}
		}
		return "fa-file";
	}

	getFileUrl (item) {
		if(['googledrive', 'dropbox'].indexOf(item.destination) >= 0)
			return item.path;
		else
			return 'documents/' + encodeURIComponent(item.name) + '?url=' + item.path;
	}

	openLibraryUploadModal() {
		this.props.openModal({render: (closeModal) => {return <DocumentuploadModal {...this.props} resource={{library:[]}} multiple={false} localdoc={[]} hidelocaldoc={true} closeModal={closeModal} callback={this.getdocuments} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
	}

	openAddNote() {
		this.props.openModal({
			render: (closeModal) => {
				return <AddNotesModal {...this.props} getResourceName={this.getResourceName} closeModal={closeModal} callback={this.getnotes} />
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	openAddTask() {
		this.props.openModal({
			render: (closeModal) => {
				return <AddTasksModal {...this.props} getResourceName={this.getResourceName} closeModal={closeModal} callback={this.gettasks} />
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	openEmail(emailobj) {
		this.props.openModal({render: (closeModal) => {
			return <ViewExistingEmailModal emailobj={emailobj} app={this.props.app} openModal={this.props.openModal} closeModal={closeModal} ndtemail={true} />
		}, className: {
			content: 'react-modal-custom-class-80',
			overlay: 'react-modal-overlay-custom-class'
		}, confirmModal: true});
	}

	renderNotes() {
		let deleteAccessClass = checkActionVerbAccess(this.props.app, 'notes', 'Delete') ? '' : 'hide';
		return (
			<div>
				<div className="timeline-centered">
					<div className="timeline-line"></div>
					<div className="timeline-entry">
						<div className="timeline-entry-inner">
							<div className="timeline-icon timeline-add-icon anchorclass" onClick={this.openAddNote}>
								<i className="fa fa-plus"></i>
							</div>
							<div className="timeline-label anchorclass" onClick={this.openAddNote}>
								<span className="text-color-custom semi-bold-custom">Add New Note</span>
							</div>
						</div>
					</div>
					{this.state.ndt.notes.map((note, index)=> {
						if(index < 5 || this.state.showNotes) {
							return (
								<div className="timeline-entry" key={index}>
									<div className="timeline-entry-inner">
										<div className="timeline-icon"></div>
										<div className="timeline-label">
											<div>
												<div>
													<span>{note.notes}</span>
													<span className={`text-danger cursor-ptr float-right ${deleteAccessClass}`} onClick={()=>this.deleteDocument(note,'notes')} style={{textDecoration:'none'}}><span className="fa fa-trash-o"></span></span>
												</div>
												<div className="small">
													{note.isrestricted ? <i className="fa fa-lock">&nbsp;</i> : null } Created by <b> {note.createdby_displayname}</b> @ {datetimeFilter(note.created)}
												</div>
											</div>
										</div>
									</div>
								</div>
							);
						}
					})}
				</div>
			</div>
		)
	}

	renderAudits() {
		if(this.state.ndt.audits == 0)
			return null;
		return (
			<div className="timeline-centered">
				<div className="timeline-line"></div>
				{this.state.ndt.audits.map((audit, index)=> {
					if(index < 5 || this.state.showAudits) {
						return (
							<div className="timeline-entry" key={index}>
								<div className="timeline-entry-inner">
									<div className="timeline-icon"></div>
									<div className="timeline-label">
										{audit.action ? <div>
											<span>Action Taken : {audit.action}</span>
											{audit.notes ? <div>Remarks : {audit.notes}</div> : null }
											{audit.audittrialhistory ? <div style={{paddingTop:'10px'}}>
												{audit.audittrialhistory.showObject.map((history, historyindex)=> {
													if(history.type=="json")
														return null;
													return (<div key={historyindex}>
														<b>{history.name} : </b>{history.type=='date' ? dateFilter(history.value) : (history.type=="boolean" ? booleanfilter(history.value) : history.value)}
													</div>)
												})}
											</div> : null }
											<div className="small">{audit.isrestricted ? <i className="fa fa-lock">&nbsp;</i> : null } Created by <b> {audit.createdby_displayname}</b> @ {datetimeFilter(audit.created)}</div>
										</div> : null}
									</div>
								</div>
							</div>
						);
					}
				})}
			</div>
		)
	}

	renderDocuments() {
		let saveAccessClass = checkActionVerbAccess(this.props.app, 'documents', 'Save') ? '' : 'hide';
		let deleteAccessClass = checkActionVerbAccess(this.props.app, 'documents', 'Delete') ? '' : 'hide';

		return (
			<div>
				<div className="timeline-centered">
					<div className="timeline-line"></div>
						<div className={`timeline-entry ${saveAccessClass}`}>
							<div className="timeline-entry-inner">
								<div className="timeline-icon timeline-add-icon anchorclass" onClick={this.openLibraryUploadModal}>
									<i className="fa fa-plus"></i>
								</div>
								<div className="timeline-label anchorclass" onClick={this.openLibraryUploadModal}>
									<span className="text-color-custom semi-bold-custom">Upload File</span>
								</div>
							</div>
						</div>
					{this.state.ndt.documents.length > 0 && this.state.ndt.documents.map((document, index) => {
						return (
							<div className="timeline-entry" key={index}>
								<div className="timeline-entry-inner">
									<div className="timeline-icon"></div>
									<div className="timeline-label">
										<table className="documentstable" key={index} style={{width: '100%'}}>
											<tbody>
												<tr>
													<td>
														<a className="fnt-bld" href={this.getFileUrl(document)} target="_blank">{document.name}</a>
													</td>
													<td className="text-right" style={{width: '10%'}}>
														<a className={`text-danger cursor-ptr ${deleteAccessClass}`} onClick={()=>this.deleteDocument(document, 'documents')} style={{textDecoration:'none',cursor:'pointer'}}><span className="fa fa-trash-o"></span></a>
													</td>
												</tr>
												<tr>
													<td colSpan="2"><span className="small">Uploaded by <b> {document.createdby_displayname} </b> @ {datetimeFilter(document.created)}</span></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						);
					})}
				</div>
			</div>
		);
	}

	renderTasks() {
		return (
			<div>
				<div className="timeline-centered">
					<div className="timeline-line"></div>
					<div className="timeline-entry">
						<div className="timeline-entry-inner">
							<div className="timeline-icon timeline-add-icon anchorclass" onClick={this.openAddTask}>
								<i className="fa fa-plus"></i>
							</div>
							<div className="timeline-label anchorclass" onClick={this.openAddTask}>
								<span className="text-color-custom">Add New Task</span>
							</div>
						</div>
					</div>
					{this.state.ndt.tasks.map((task, index)=> {
						if(index < 5 || this.state.showTasks) {
							return (
								<div className="timeline-entry" key={index}>
									<div className="timeline-entry-inner">
										<div className="timeline-icon"></div>
										<div className="timeline-label">
											<div>{task.description}</div>
											<div>
												<b>Assigned To: </b><span>{task.assignedto_displayname}</span><br></br>
												<b>Due By: </b><span>{dateFilter(task.dueby)}</span><br></br>
												<b>Status: </b><span>{task.status}</span><br></br>
												<b>Created by : </b><span>{task.createdby_displayname}</span>
											</div>
										</div>
									</div>
								</div>
							);
						}
					})}
				</div>
			</div>
		);
	}

	renderEmails() {
		if(this.state.ndt.emails.length == 0)
			return <div className="alert alert-info text-center">No Emails Found</div>;
		return this.state.ndt.emails.map((email, index)=> {
			if(index < 5 || this.state.showEmails) {
				return (
					<div key={index}>
						{email.clientemailid ? <div className="col-md-12 anchorclass" onClick={()=>this.openEmail(email)}>
							{email.inreplyto ? <i rel="tooltip" title={email.subject} className="fa fa-reply marginright-10"></i> : null }
							{!email.inreplyto ? <i rel="tooltip" title={email.subject} className="fa fa-envelope marginright-10"></i> : null }
							<span>{email.subject}</span>
							<br></br>
							<span className="text-muted" style={{fontSize:'13px'}}>{email.body}</span>
							<br></br>
							<span className="text-muted" style={{fontSize:'13px',marginLeft:'10px'}}>From: {email.frommail}</span>
							<span className="text-muted" style={{fontSize:'13px',marginLeft:'10px'}}>To: {email.tomail}</span>
							<span className="text-muted" style={{fontSize:'13px',marginLeft:'10px'}}>On: {dateFilter(email.emaileddate)} @ {timeFilter(email.emaileddate)}</span>
							{(this.state.ndt.emails.length-1) > index ? <hr className="marginbottom-8 margintop-8" style={{display: 'inline-block', width: '100%'}}></hr> : null }
						</div> : null } 
					</div>
				);
			}
		});
	}

	recalculateHeight = () => {
		let windowHeight = $(window).outerHeight();
		//let ndtHeight = $('.ndt-section').outerHeight();
		let alertHeight = $('.alert-section').outerHeight();
		let accordionHeight = (windowHeight - (alertHeight || 0)) - 50;
		$('.accordion-container').attr('style', 'height: ' + accordionHeight + 'px !important');
		let accordiongroupHeight = $('.accordion-toggle').first().outerHeight();
		let totaloggle = $('.accordion-toggle').length;
		let accordionBodyHeight = accordionHeight - totaloggle * (accordiongroupHeight + 4);

		$(".sidebar-accordion-content.active").attr('style', 'height: ' + accordionBodyHeight + 'px !important');
	};

	getActiveAccordion(accordionNum) {
		return this.state.accordionClasses[accordionNum];
	}

	collapseAccordion(accordionNum) {
		let { accordionClasses } = this.state;
		accordionClasses = accordionClasses.map((item, index) => {
			 return item = (index == accordionNum) ? 'active' : '';
		});
		this.setState({accordionClasses}, () => {
			this.getActiveAccordion(accordionNum);
			this.recalculateHeight();
			if(accordionNum == 3)
				this.getemails();
		});
	}


	render() {
		return (
			<div className="accordion-container">
				<div className="sidebar-accordion">
					<div className="accordion-toggle" onClick={() => this.collapseAccordion(0)}>
						<span>Notes</span>
						<span className="marginleft-5">[{this.state.ndt.notes.length}]</span>
						<i className={`fa fa-${this.state.accordionClasses[0] == 'active' ? 'caret-down' : 'caret-right'}`}></i>
					</div>
					<div className={`sidebar-accordion-content ${this.getActiveAccordion(0)}`}>
						{this.state.ndt.notes.length == 0 ? 
							<div className="ndt-placeholder-box">
								<div>
									<img src="/images/empty-placeholder.png" style={{background: 'none'}} width="80" height="80" />
								</div>
								<div className="marginbottom-15">No notes added!</div>
								<div><button type="button" className="btn btn-sm gs-btn-success" onClick={this.openAddNote}><i className="fa fa-plus"></i>Add Note</button></div>
							</div> : <div className="row">
								<div className="col-md-12 col-sm-12 col-xs-12">
									{this.renderNotes()}
								</div>
								{this.state.ndt.notes && this.state.ndt.notes.length > 5 && !this.state.showNotes ? <div className="col-md-12 col-sm-6 col-xs-12 text-right">
									<a className="text-color-custom semi-bold-custom" onClick={()=>{this.setState({showNotes: true});}}>View More</a>
								</div> : null }
						</div>}
					</div>
				</div>
				<div className="sidebar-accordion">
					<div className="accordion-toggle" onClick={() => this.collapseAccordion(1)}>
						<span>Documents</span>
						<span className="marginleft-5">[{this.state.ndt.documents.length}]</span>
						<i className={`fa fa-${this.state.accordionClasses[1] == 'active' ? 'caret-down' : 'caret-right'}`}></i>
					</div>
					<div className={`sidebar-accordion-content ${this.getActiveAccordion(1)}`}>
						{this.state.ndt.documents.length == 0 ? 
							<div className="ndt-placeholder-box">
								<div>
									<img src="/images/empty-placeholder.png" style={{background: 'none'}} width="80" height="80" />
								</div>
								<div className="marginbottom-15">No documents uploaded!</div>
								<div><button type="button" className="btn btn-sm gs-btn-success" onClick={this.openLibraryUploadModal}><i className="fa fa-upload"></i>Upload Document</button></div>
							</div> : <div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								{this.renderDocuments()}
							</div>
						</div>}
					</div>
				</div>
				<div className="sidebar-accordion">
					<div className="accordion-toggle" onClick={() => this.collapseAccordion(2)}>
						<span>Tasks</span>
						<span className="marginleft-5">[{this.state.ndt.tasks.length}]</span>
						<i className={`fa fa-${this.state.accordionClasses[2] == 'active' ? 'caret-down' : 'caret-right'}`}></i>
					</div>
					<div className={`sidebar-accordion-content ${this.getActiveAccordion(2)}`}>
						{this.state.ndt.tasks.length == 0 ? 
							<div className="ndt-placeholder-box">
								<div>
									<img src="/images/empty-placeholder.png" style={{background: 'none'}} width="80" height="80" />
								</div>
								<div className="marginbottom-15">No task created!</div>
								<div><button type="button" className="btn btn-sm gs-btn-success" onClick={this.openAddTask}><i className="fa fa-plus"></i>Create Task</button></div>
							</div> : <div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								{this.renderTasks()}
							</div>
							{this.state.ndt.tasks && this.state.ndt.tasks.length > 5 && !this.state.showTasks ? <div className="col-md-12 col-sm-12 form-group text-right">
								<a className="text-color-custom semi-bold-custom" onClick={()=>{this.setState({showTasks: true});}}>View More</a>
							</div> : null }
						</div>}
					</div>
				</div>
				<div className="sidebar-accordion">
					<div className="accordion-toggle" onClick={() => this.collapseAccordion(3)}>
						<span>Emails</span>
						<span className="marginleft-5">[{this.state.ndt.emails.length}]</span>
						<i className={`fa fa-${this.state.accordionClasses[3] == 'active' ? 'caret-down' : 'caret-right'}`}></i>
					</div>
					<div className={`sidebar-accordion-content ${this.getActiveAccordion(3)}`}>
						{this.state.ndt.emails.length == 0 ? 
							<div className="ndt-placeholder-box">
								<div>
									<img src="/images/empty-placeholder.png" style={{background: 'none'}} width="80" height="80" />
								</div>
								<div>No emails found!</div>
							</div> : <div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								{this.renderEmails()}
							</div>
							{this.state.ndt.emails && this.state.ndt.emails.length > 5 && !this.state.showEmails ? <div className="col-md-12 col-sm-6 col-xs-12 text-right">
								<a className="text-color-custom semi-bold-custom" onClick={()=>{this.setState({showEmails: true});}}>View More</a>
							</div> : null }
						</div>}
					</div>
				</div>
				<div className="sidebar-accordion">
					<div className="accordion-toggle" onClick={() => this.collapseAccordion(4)}>
						<span>Audits</span>
						<span className="marginleft-5">[{this.state.ndt.audits.length}]</span>
						<i className={`fa fa-${this.state.accordionClasses[4] == 'active' ? 'caret-down' : 'caret-right'}`}></i>
					</div>
					<div className={`sidebar-accordion-content ${this.getActiveAccordion(4)}`}>
						<div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								{this.renderAudits()}
							</div>
							{this.state.ndt.audits && this.state.ndt.audits.length > 5 && !this.state.showAudits ? <div className="col-md-12 col-sm-6 col-xs-12 text-right">
								<a className="text-color-custom semi-bold-custom" onClick={()=>{this.setState({showAudits: true});}}>View More</a>
							</div> : null }
						</div>
					</div>
				</div>
			</div>
		);
	}
});

class AddNotesModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			newnote: {
				isrestricted: false
			}
		};
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.updateField = this.updateField.bind(this);
		this.addNotes = this.addNotes.bind(this);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	updateField(value, field) {
		let { newnote } = this.state;
		newnote[field] = value;
		this.setState({ newnote });
	}

	addNotes() {
		let { newnote } = this.state;
		this.updateLoaderFlag(true);
		newnote['parentresource'] = this.props.getResourceName();
		newnote['parentid'] = this.props.parentid;

		let tempData = {
			actionverb : 'Save',
			data : newnote,
			childResourceToUpdate : []
		};
		axios({
			method : 'post',
			data : tempData,
			url : '/api/notes'
		}).then((response)=> {
			newnote = {notes : ""};
			this.setState({newnote}, ()=> {
				this.props.callback();
				this.props.closeModal();
			});
			this.updateLoaderFlag(false);
		});
	}

	render() {
		let { newnote } = this.state;
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-outer-modal">
					<div className="react-modal-header">
						<h5 className="modal-title">Add Note</h5>
					</div>
					<div className="react-modal-body">
						<div className="row">
							<div className="col-md-10 offset-md-1 form-group">
								<textarea className="form-control" value={newnote.notes} placeholder="Description" onChange={(evt) => this.updateField(evt.target.value, 'notes')} />
							</div>
						</div>
						<div className="row">
							<div className="col-md-12 form-group text-center">
								<Buttongroup buttons={[{title: 'Public', value: false}, {title:'Personal', value: true}]} buttonclass={'btn-secondary btn-sm'} value={newnote.isrestricted} onChange={(val) => this.updateField(val, 'isrestricted')}/>
							</div>
						</div>
					</div>
					<div className="react-modal-footer">
						<div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								<div className="muted credit text-center">
									<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
									<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addNotes} disabled={!newnote.notes}><i className="fa fa-check"></i>Add Note</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class AddTasksModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			newtask: {}
		};
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.updateField = this.updateField.bind(this);
		this.addTasks = this.addTasks.bind(this);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	updateField(value, field) {
		let { newtask } = this.state;
		newtask[field] = value;
		this.setState({ newtask });
	}

	addTasks() {
		this.updateLoaderFlag(true);
		let { newtask } = this.state;
		newtask['parentresource'] = this.props.getResourceName();
		newtask['parentid'] = this.props.parentid;
		newtask['relatedpath'] = this.props.relatedpath;
		let tempData = {
			actionverb : 'Save',
			data : newtask,
			childResourceToUpdate : []
		};
		axios({
			method : 'post',
			data : tempData,
			url : '/api/tasks'
		}).then((response)=> {
			newtask = {};
			this.setState({ newtask }, () => {
				this.props.callback();
				this.props.closeModal();
			});
			this.updateLoaderFlag(false);
		});
	}

	render() {
		let { newtask } = this.state;
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-outer-modal">
					<div className="react-modal-header">
						<h5 className="modal-title">Add Task</h5>
					</div>
					<div className="react-modal-body">
						<div className="row">
							<div className="col-md-3 form-group">
								<label className="labelclass">Title</label>
							</div>
							<div className="col-md-9 form-group">
								<input className={`form-control ndtclass ${!newtask.description ? 'errorinput' : ''}`} value={newtask.description} autoComplete="off" onChange={(e)=>this.updateField(e.target.value, 'description')} required="true" />
							</div>
						</div>
						<div className="row">
							<div className="col-md-3 form-group">
								<label className="labelclass">Assigned To</label>
							</div>
							<div className="col-md-9 form-group">
								<SelectAsync className={`${!newtask.assignedto ? 'errorinput' : ''}`} resource="users" fields="id,displayname" label="displayname" valuename="id" value={newtask.assignedto} onChange={(val) => this.updateField(val, 'assignedto')} required="true" />
							</div>
						</div>
						<div className="row">
							<div className="col-md-3 form-group">
								<label className="labelclass">Due Date</label>
							</div>
							<div className="col-md-9 form-group">
								<DateElement className={`form-control ${!newtask.dueby ? 'errorinput' : ''}`} placeholder="Choose Date" value={newtask.dueby} onChange={(val) => this.updateField(val, 'dueby')} />
							</div>
						</div>
					</div>
					<div className="react-modal-footer">
						<div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								<div className="muted credit text-center">
									<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
									<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addTasks} disabled={!newtask.description || !newtask.assignedto || !newtask.dueby || this.state.loaderflag}><i className="fa fa-check"></i>Save</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
