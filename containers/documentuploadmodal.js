import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import Dropzone from 'react-dropzone'

import Loadingcontainer from '../components/loadingcontainer';
import { commonMethods, modalService } from '../utils/services';
import { currencyFilter } from '../utils/filter';
import { checkActionVerbAccess } from '../utils/utils';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag: false,
			library: {},
			localdocuments: [],
			documentList: [],
			transactiondocuments: this.props.localdoc ? this.props.localdoc : [],
			selectmultiple: this.props.multiple,
			hidelocaldoc: this.props.hidelocaldoc,
			files: [],
			breadCrumbs: [],
			tabClasses: ["", "", "", "", "", ""]
		};
		if(this.props.resource && !this.props.resource.library) {
			this.props.change('library', []);
		}
		this.onLoad = this.onLoad.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getdocuments = this.getdocuments.bind(this);
		this.getFolderAndFiles = this.getFolderAndFiles.bind(this);
		this.getTabClass = this.getTabClass.bind(this);
		this.setActiveTab = this.setActiveTab.bind(this);
		this.getTabPaneClass = this.getTabPaneClass.bind(this);
		this.getfiletype = this.getfiletype.bind(this);
		this.getDestinationIcon = this.getDestinationIcon.bind(this);
		this.multipleAttachFile = this.multipleAttachFile.bind(this);
		this.multipleLocAttachFile = this.multipleLocAttachFile.bind(this);
		this.multipleTransAttachFile = this.multipleTransAttachFile.bind(this);
		this.deleteLibraryLink = this.deleteLibraryLink.bind(this);
		this.deleteLink = this.deleteLink.bind(this);
		this.attachLocalDoc = this.attachLocalDoc.bind(this);
		this.canAttachFile = this.canAttachFile.bind(this);
		this.DropBoxFiles = this.DropBoxFiles.bind(this);
		this.openGoogleDrive = this.openGoogleDrive.bind(this);
		this.onPicked = this.onPicked.bind(this);
		this.googleDrive_Dropbox_Docs = this.googleDrive_Dropbox_Docs.bind(this);
		this.getResourceName = this.getResourceName.bind(this);
	}

	componentWillMount() {
		this.onLoad();
		if(this.props.parentid > 0)
	 		this.getdocuments();
		if(this.state.hidelocaldoc) {
			this.setActiveTab(2);
		} else {
			this.setActiveTab(1);
		}
	}

	onLoad() {
		if (this.props.resource && this.props.resource.referencetype == 'libraries') {
			axios.get(`/api/query/getdoclibrarydetailsquery?libraryid=${this.props.resource.referenceid}`).then((response)=> {
				if (response.data.message == 'success') {
					if(response.data.main.length > 0) {
						let breadCrumbs = [];

						var tempObj = {
							id : response.data.main[0].id,
							name : response.data.main[0].name,
							fullpath : response.data.main[0].fullpath
						};

						for(var i=response.data.main.length-1;i>0;i--) {
							var breadcrumbObj = {
								id:response.data.main[i].id,
								name:response.data.main[i].name
							};
							breadCrumbs.push(breadcrumbObj);
						}
						breadCrumbs.splice(0, 0, {id: 0,name: 'Libraries'});
						this.setState({breadCrumbs},()=> {
							this.getFolderAndFiles(tempObj);
						});
					} else {
						this.getFolderAndFiles(null);
					}
				}
			});
		} else {
			this.getFolderAndFiles(null);
		}
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	getResourceName() {
		return this.props.app.myResources[this.props.parentresource] && this.props.app.myResources[this.props.parentresource].type == 'lookup' ? this.props.app.myResources[this.props.parentresource].lookupResource : this.props.parentresource;
	}

	getdocuments() {
		let { localdocuments } = this.state;
		let filterString = `documents.parentresource='${this.getResourceName()}' and documents.parentid=${this.props.parentid}`;
		filterString = encodeURIComponent(filterString);

		let queryString = `/api/documents?field=id,path,users/displayname/createdby,name,created,destination,filesize&filtercondition=${filterString}`;

		axios.get(queryString).then((response)=> {
			if (response.data.message == 'success') {
				localdocuments = response.data.main;

				if (this.props.resource) {
					if(this.props.resource.referencetype == 'documents') {
						for (var i = 0; i < localdocuments.length; i++) {
							if (localdocuments[i].id == this.props.resource.referenceid) {
								localdocuments[i].ischecked = true;
								break;
							}
						}
					}
				}

				if(this.props.resource.library && this.props.resource.library.length > 0) {
					for (var i = 0; i < localdocuments.length; i++) {
						for (var j = 0; j < this.resource.props.library.length; j++) {
							if (localdocuments[i].id == this.resource.props.library[j].referenceid && this.props.resource.library[j].referencetype == 'documents') {
								localdocuments[i].ischecked = true;
							}
						}
					}
				}

				this.setState({localdocuments});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getFolderAndFiles(item) {
		let { documentList, breadCrumbs } = this.state
		var parentId = ((item && item.id > 0) ? item.id : null);
		let selecteddocument = {};

		axios.get(`api/query/getlibraryquery?parentId=${parentId}`).then((response)=> {
			if (response.data.message == 'success') {
				documentList = response.data.main;

				documentList.sort((a,b)=> {
					if(a.isparent && b.isparent) {
						if(a.name.toLowerCase() > b.name.toLowerCase()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						if(a.isparent) {
							return -1;
						} else if (b.isparent) {
							return 1;
						} else {
							if(a.name.toLowerCase() > b.name.toLowerCase()) {
								return 1;
							} else {
								return -1;
							}
						}
					}
				});
				selecteddocument.parentid = parentId;
				selecteddocument.fullpath = ((item && item.fullpath) ? item.fullpath : '');
				if (selecteddocument.parentid > 0) {
					var itemFound = false;
					for (var i = 0; i < breadCrumbs.length; i++) {
						if (breadCrumbs[i].id == selecteddocument.parentid) {
							itemFound = true;
							break;
						}
					}

					if (itemFound) {
						breadCrumbs.splice(i + 1, (breadCrumbs.length - i));
					} else {
						breadCrumbs.push({
							id : item.id,
							name : item.name
						});
					}
				} else {
					breadCrumbs = [{
							id : 0,
							name : 'Libraries'
						}
					];
				}
				if (this.props.resource) {
					if(this.props.resource.referencetype == 'libraries') {
						for (var i = 0; i < documentList.length; i++) {
							if (documentList[i].id == this.props.resource.referenceid) {
								documentList[i].ischecked = true;
								break;
							}
						}
					}
				}
				if(this.props.resource.library && this.props.resource.library.length > 0) {
					for (var i = 0; i < documentList.length; i++) {
						for (var j = 0; j < this.props.resource.library.length; j++) {
							if (documentList[i].id == this.props.resource.library[j].referenceid && this.props.resource.library[j].referencetype == 'libraries') {
								documentList[i].ischecked = true;
							}
						}
					}
				}
				this.setState({documentList, breadCrumbs});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getTabClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	getTabPaneClass(tabNum) {
		return "tab-pane " + this.state.tabClasses[tabNum];
	}

	setActiveTab(tabNum) {
		let { tabClasses } = this.state;
		tabClasses =  ["", "", "", "", "", ""]
		tabClasses[tabNum] = "show active";
		this.setState({tabClasses}, ()=>{
			this.getTabPaneClass(tabNum);
		});
	}

	getfiletype(fileParam) {
		if (fileParam) {
			var filetype = fileParam.name.split('.')[fileParam.name.split('.').length-1];
			if (filetype == 'txt' || filetype =='csv' || filetype == 'rtf') {
				return 'fa-file-text-o';
			}
			else if (filetype == 'gzip' || filetype == 'zip' || filetype == 'rar' || filetype == 'tar') {
				return 'fa-file-zip-o';
			}
			else if (filetype == 'pdf') {
				return 'fa-file-pdf-o';
			}
			else if (filetype == 'jpg' || filetype == 'png' || filetype == 'gif' || filetype == 'jpeg' || filetype == 'ico' || filetype == 'bmp') {
				return 'fa-file-image-o';
			}
			else if (filetype == 'doc' || filetype == 'docx' || filetype == 'dot' || filetype == 'dotx') {
				return 'fa-file-word-o';
			}
			else if (filetype == 'xlsx' || filetype == 'xls') {
				return 'fa-file-excel-o';
			}
			else if (filetype == 'pptx' || filetype == 'pptm') {
				return 'fa-file-powerpoint-o';
			}
			else {
				return "fa-file";
			}
		}
		return "fa-file";
	}

	getDestinationIcon (destination) {
		if(['googledrive', 'dropbox'].indexOf(destination) >= 0) {
			if (destination == 'googledrive') {
				return 'fa-google';
			}
			else if (destination == 'dropbox') {
				return 'fa-dropbox';
			}
		} else
			return "fa-file";
	}

	attachLocalDoc(document, index) {
		let { localdocuments } = this.state;
		if (document) {
			localdocuments[index].ischecked = !document.ischecked;
			if(localdocuments[index].ischecked) {
				if(['googledrive', 'dropbox'].indexOf(document.destination) >= 0)
					this.props.callback({
						name : document.name,
						referencetype : "documents",
						referenceid : document.id,
						destination : document.destination,
						fileurl : document.path
					});
				else
					this.props.callback({
						name : document.name,
						referencetype : "documents",
						referenceid : document.id,
						destination : document.destination,
						fileurl : 'documents/documents/'+document.id+'/'+document.name
					});
			} else {
				this.props.callback({});
			}

			this.setState({localdocuments});
		}
		this.props.closeModal();
	}

	upload($files) {
		let { tab } = this.state;
		let libraryArr = this.props.resource.library;
		if ($files && $files.length > 1)
			return modalService.infoMethod({
				header : "Error",
				body : "Please drag and drop <b class='text-info'>one</b> file at a time",
				btnName : ["Ok"]
			});

		let fileData = $files[0];

		let document = {
			parentresource : this.getResourceName(),
			parentid : this.props.parentid,
			resourceName : 'documents',
			destination : 'documents'
		};
		let fileName = fileData.name.substr(0, fileData.name.lastIndexOf('.'));

		if(!(/^[a-zA-Z0-9\.\,\_\-\'\!\*\(\)\ ]*$/.test(fileName))) {
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Invalid file name. Please change file name. file name contain invalid specical characters. Allowed special characters are '_ - . , ! ( ) *'",
				btnArray : ["Ok"]
			}));
		}

		if (fileData['size'] > 10485760) {
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "File Size should not be exceed than <b class='text-danger'>10 MB</b>",
				btnName : ["Ok"]
			}));
		} else
			document['filesize'] = fileData['size'];


		var tempData = {
			actionverb : 'Save',
			data : document,
			childResourceToUpdate : ""
		};

		this.updateLoaderFlag(true);

		const formData = new FormData();
		formData.append('file', fileData);
		formData.append('data', JSON.stringify(document));
		formData.append('actionverb', 'Save');
		formData.append('childResourceToUpdate', '');

		const config = {
			headers: {
				'content-type': 'multipart/form-data'
			}
		};

		axios.post('/upload', formData, config).then((response)=> {
			if (response.data.message == 'success') {
				if(this.state.selectmultiple) {
					var tempObj = {
						referencetype : 'documents',
						referenceid : response.data.main.id,
						name : response.data.main.name,
						destination : response.data.main.destination,
						fileurl : response.data.main.path
					};
					libraryArr.push(tempObj);
					this.props.change('library', libraryArr);
					this.getdocuments();
				} else {
					if(['googledrive', 'dropbox'].indexOf(response.data.main.destination) >= 0) {
						this.props.callback({
							name : response.data.main.name,
							referencetype : "documents",
							referenceid : response.data.main.id,
							destination : response.data.main.destination,
							fileurl : response.data.main.path
						});
					} else {
						this.props.callback({
							name : response.data.main.name,
							referencetype : "documents",
							referenceid : response.data.main.id,
							destination : response.data.main.destination,
							fileurl : 'documents/documents/'+response.data.main.id+'/'+response.data.main.name
						});
					}

					this.props.closeModal();
				}
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	canAttachFile(item, index) {
		let { documentList } = this.state;
		if (item) {
			documentList[index].checked = !item.checked;
			if(documentList[index].checked) {
				let tempObj = {};
				if(['googledrive', 'dropbox'].indexOf(item.destination)>= 0) {
					tempObj = {
						name : item.name,
						referencetype : "libraries",
						referenceid : item.id,
						fileurl : item.fileurl,
						destination : item.destination
					}
				} else {
					tempObj = {
						name : item.name,
						referencetype : "libraries",
						referenceid : item.id,
						fileurl : '/documents/libraries/'+item.id+'/'+item.name,
						destination : item.destination
					};
				}
				this.props.callback(tempObj);
			} else {
				this.props.callback({});
			}
			this.setState({documentList});
		}
		this.props.closeModal();
	}

	multipleAttachFile(item, index) {
		let { documentList } = this.state;
		let libraryArr = this.props.resource.library;
		if (item) {
			documentList[index].ischecked = !item.ischecked;

			if(documentList[index].ischecked) {
				var itemFound = false;
				for (var j = 0; j < this.props.resource.library.length; j++) {
					if (item.id == this.props.resource.library[j].referenceid && this.props.resource.library[j].referencetype == 'libraries') {
						itemFound = true;
						break;
					}
				}

				if (!itemFound) {
					let tempObj = {};
					if(['googledrive', 'dropbox'].indexOf(item.destination) >= 0) {
						tempObj = {
							name : item.name,
							referencetype : "libraries",
							referenceid : item.id,
							fileurl : item.fileurl,
							destination : item.destination
						};
					} else {
						tempObj = {
							name : item.name,
							referencetype : "libraries",
							referenceid : item.id,
							fileurl : '/documents/libraries/'+item.id+'/'+item.name,
							destination : item.destination
						};
					}
					libraryArr.push(tempObj)
					this.props.change('library', libraryArr);
				}
			} else {
				for (var j = 0; j < this.props.resource.library.length; j++) {
					if (item.id == this.props.resource.library[j].referenceid && this.props.resource.library[j].referencetype == 'libraries') {
						libraryArr.splice(j, 1);
						this.props.change('library', libraryArr);
						break;
					}
				}
			}
			this.setState({documentList});
		}
	}

	multipleLocAttachFile(document, index) {
		let { localdocuments } = this.state;
		let libraryArr = this.props.resource.library;
		if (document) {
			localdocuments[index].ischecked = !document.ischecked;
			if(localdocuments[index].ischecked) {
				var itemFound = false;
				for (var j = 0; j < libraryArr.length; j++) {
					if (document.id == libraryArr[j].referenceid && libraryArr[j].referencetype == 'documents') {
						itemFound = true;
						break;
					}
				}

				if (!itemFound) {
					let tempObj = {};
					if(['googledrive', 'dropbox'].indexOf(document.destination) >= 0) {
						tempObj = {
							referencetype : 'documents',
							referenceid : document.id,
							name :  document.name,
							destination : document.destination,
							fileurl : document.path
						};
					} else {
						tempObj = {
							referencetype : 'documents',
							referenceid : document.id,
							name :  document.name,
							destination : document.destination,
							fileurl : 'documents/documents/'+document.id+'/'+document.name
						};
					}

					libraryArr.push(tempObj);
					this.props.change('library', libraryArr);
				}
			} else {
				for (var j = 0; j < libraryArr.length; j++) {
					if (document.id == libraryArr[j].referenceid && libraryArr[j].referencetype == 'documents') {
						libraryArr.splice(j, 1);
						this.props.change('library', libraryArr);
						break;
					}
				}
			}

			this.setState({localdocuments});
		}
	}

	multipleTransAttachFile(document, index) {
		let { transactiondocuments } = this.state;
		let libraryArr = this.props.resource.library;
		if (document) {
			transactiondocuments[index].ischecked = !document.ischecked;
			if(transactiondocuments[index].ischecked) {
				var itemFound = false;
				for (var j = 0; j < libraryArr.length; j++) {
					if (document.referenceid == libraryArr[j].referenceid && document.referencetype == libraryArr[j].referencetype) {
						itemFound = true;
						break;
					}
				}

				if (!itemFound) {
					libraryArr.push(document);
					this.props.change('library', libraryArr);
				}
			} else {
				for (var j = 0; j < libraryArr.length; j++) {
					if (document.referenceid == libraryArr[j].referenceid && document.referencetype == libraryArr[j].referencetype) {
						libraryArr.splice(j, 1);
						this.props.change('library', libraryArr);
						break;
					}
				}
			}
			this.setState({transactiondocuments});
		}
	}

	deleteLink() {
		this.props.callback(null);
		this.props.closeModal();
	};

	deleteLibraryLink(item) {
		let { localdocuments, documentList, transactiondocuments } = this.state;
		let libraryArr = this.props.resource.library;
		if(item.referencetype == 'libraries') {
			for (var j = 0; j < libraryArr.length; j++) {
				if (item.referenceid == libraryArr[j].referenceid && item.referencetype == libraryArr[j].referencetype) {
					libraryArr.splice(j, 1);
					this.props.change('library', libraryArr);
					for (var i = 0; i < documentList.length; i++) {
						 if(item.referenceid == documentList[i].id) {
							documentList[i].ischecked = false;
						}
					}
					for (var i = 0; i < transactiondocuments.length; i++) {
						 if(item.referenceid == transactiondocuments[i].referenceid && item.referencetype == transactiondocuments[i].referencetype) {
							transactiondocuments[i].ischecked = false;
						}
					}
				}
			}

			this.setState({transactiondocuments, documentList});
		} else if(item.referencetype == 'documents') {
			for (var j = 0; j < libraryArr.length; j++) {
				if (item.referenceid == libraryArr[j].referenceid && item.referencetype == libraryArr[j].referencetype) {
					libraryArr.splice(j, 1);
					this.props.change('library', libraryArr);
					for (var i = 0; i < localdocuments.length; i++) {
						 if(item.referenceid == localdocuments[i].id) {
							localdocuments[i].ischecked = false;
						}
					}
					for (var i = 0; i < transactiondocuments.length; i++) {
						 if(item.referenceid == transactiondocuments[i].referenceid && item.referencetype == transactiondocuments[i].referencetype) {
							transactiondocuments[i].ischecked = false;
						}
					}
				}
			}

			this.setState({transactiondocuments, localdocuments});
		}
	}

	DropBoxFiles () {
		let libraryArr = this.props.resource.library;
		let options = {
			success: (files) => {
				let dropBoxfiles = [];
				dropBoxfiles.push(files[0]);

				dropBoxfiles[0].destination = 'dropbox';
				if(!this.state.selectmultiple)
					this.googleDrive_Dropbox_Docs(dropBoxfiles[0]);
				else {
					let tempObj = {
						name : dropBoxfiles[0].name,
						fileurl : dropBoxfiles[0].link,
						destination : dropBoxfiles[0].destination
					};

					let itemFound = false;
					for (var i = 0; i < libraryArr.length; i++) {
						if (libraryArr[i].name == tempObj.name && libraryArr[i].destination == tempObj.destination) {
							itemFound = true;
							break;
						}
					}

					if(!itemFound) {
						libraryArr.push(tempObj);
						this.props.change('library', libraryArr);
					} else {
						return this.openModal(modalService.infoMethod({
							header : "Error",
							body : "File name already exists!",
							btnName : ["Ok"]
						}));
					}
					this.props.closeModal();
				}
			},
			cancel: () => {
				console.log("CANCEL")
			},
			linkType: "preview",
			multiselect: false,
		};
		Dropbox.choose(options);
	}

	openGoogleDrive () {
		commonMethods.openGoogleDrive(this.props, this.onPicked);
	}

	onPicked (docs) {
		let files = [];
		let libraryArr = this.props.resource.library;

		docs.forEach((file, index) => {
			if(file.uploadId == undefined)
				files.push(file);
		});

		if(files.length == 1) {
			files[0].destination = 'googledrive';
			if(!this.state.selectmultiple)
				this.googleDrive_Dropbox_Docs(files[0]);
			else {
				let tempObj = {
					name : files[0].name,
					fileurl : files[0].url,
					destination : files[0].destination
				};

				let itemFound = false;
				for (var i = 0; i < libraryArr.length; i++) {
					if (libraryArr[i].name == tempObj.name && libraryArr[i].destination == tempObj.destination) {
						itemFound = true;
						break;
					}
				}

				if(!itemFound) {
					libraryArr.push(tempObj);
					this.props.change('library', libraryArr);
				} else {
					return this.props.openModal(modalService.infoMethod({
						header : "Error",
						body : "File name already exists!",
						btnArray : ["Ok"]
					}));
				}
				this.props.closeModal();
			}
		}
	}

	googleDrive_Dropbox_Docs (data) {
		let tempObj = {};

		if(data.destination == 'googledrive') {
			tempObj = {
				parentresource : this.getResourceName(),
				parentid : this.props.parentid,
				resourceName : 'documents',
				destination : data.destination,
				name : data.name,
				fileid : data.id,
				path : data.url
			}
		}

		if(data.destination == 'dropbox') {
			tempObj = {
				parentresource : this.getResourceName(),
				parentid : this.props.parentid,
				resourceName : 'documents',
				destination : data.destination,
				name : data.name,
				fileid : data.id.split(':')[1],
				path : data.link
			}
		}

		axios({
			method : 'POST',
			data : {
				actionverb : 'Save',
				data : tempObj
			},
			url : '/api/documents'
		}).then((response) => {
			if (response.data.message == 'success') {
				if(['googledrive', 'dropbox'].indexOf(response.data.main.destination) >= 0)
					this.props.callback({
						name : response.data.main.name,
						referencetype : "documents",
						referenceid : response.data.main.id,
						destination : response.data.main.destination,
						fileurl : response.data.main.path
					});
				else
					this.props.callback({
						name : response.data.main.name,
						referencetype : "documents",
						referenceid : response.data.main.id,
						destination : response.data.main.destination,
						fileurl : 'documents/documents/'+data.id+'/'+data.name
					});

				this.props.closeModal();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	render() {
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<div className="row">
						<div className="col-md-6"><h5 className="modal-title">Documents</h5></div>
						{!this.state.selectmultiple && this.props.resource.name ? <div className="col-md-6">
							<div className="row">
								<div className="col-md-4 text-right" style={{padding:'5px'}}>Selected File : </div>
								<div className="input-group col-md-8">
									<span className="form-control un-editable overflowtxt" disabled>{this.props.resource.name}</span>
									<span className="input-group-append">
										<button type="button" className="btn gs-form-btn-danger ndtclass" onClick={this.deleteLink}><span className="fa fa-trash-o"></span></button>
									</span>
								</div>
							</div>
						</div> : null }
					</div>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-3">
							<ul className="list-group">
								{!this.state.hidelocaldoc ? <li className={`list-group-item list-group-item-action ${this.getTabClass(1)}`} onClick={()=>this.setActiveTab(1)}><div><i className="fa fa-desktop"></i> Local Documents</div></li> : null }
								{!this.state.selectmultiple ? <li className={`list-group-item list-group-item-action ${this.getTabClass(2)}`} onClick={()=>this.setActiveTab(2)}><div><i className="fa fa-desktop"></i> My Computer</div></li> : null }
								{(!this.state.hidelocaldoc && checkActionVerbAccess(this.props.app, 'libraries', 'Read')) ? <li className={`list-group-item list-group-item-action ${this.getTabClass(3)}`} onClick={()=>this.setActiveTab(3)}><div><i className="fa fa-list-alt"></i> Document Library</div></li> : null }
								<li className={`list-group-item list-group-item-action ${this.getTabClass(4)}`} onClick={()=>this.setActiveTab(4)}><div onClick={this.openGoogleDrive}><i className="fa fa-google"></i> Google Drive</div></li>
								<li className={`list-group-item list-group-item-action ${this.getTabClass(5)}`} onClick={()=>this.setActiveTab(5)}><div onClick={this.DropBoxFiles}><i className="fa fa-dropbox"></i> Drop Box</div></li>
								{this.state.selectmultiple && !this.state.hidelocaldoc ? <li className={`list-group-item list-group-item-action ${this.getTabClass(6)}`} onClick={()=>this.setActiveTab(6)} ><div><i className="fa fa-list"></i> Selected Files</div></li> : null }
							</ul>
						</div>
						<div className="col-md-9">
							<div className="tab-content">
								{this.state.selectmultiple ? <div className={`${this.getTabPaneClass(1)}`} id="localfile">
									{this.state.transactiondocuments.length > 0 || this.state.localdocuments.length > 0 ? <div className="row" style={{maxHeight:'400px',overflowY:'scroll'}}>
										{this.state.transactiondocuments.length > 0 ? <div className="col-md-12 col-xs-12 col-sm-12">
											<h5 style={{textDecoration: 'underline'}}>Transaction Documents</h5>
											{this.state.transactiondocuments.map((item, index)=> {
												return (<div key={index} className={`form-group col-md-4 col-sm-6 col-xs-12 overflowtxt ${item.ischecked ? 'doclinkactive' : ''}`} onClick={()=>this.multipleTransAttachFile(item, index)} rel='tooltip' title={item.name} style={{cursor:'pointer',padding:'10px',borderRadius: '10px',height:'40px'}}>
														{['googledrive', 'dropbox'].indexOf(item.destination) == -1 ? <span className={`fa ${this.getfiletype(item)}`}></span> : null }
														{['googledrive', 'dropbox'].indexOf(item.destination) >= 0 ? <span className={`fa ${this.getDestinationIcon(item.destination)}`}></span> : null }
														{item.name}
													</div>
												);
											})}
										</div> : null }
										{this.state.localdocuments.length > 0 ? <div className="col-md-12 col-xs-12 col-sm-12">
											<h5 style={{textDecoration: 'underline'}}>Local Documents</h5>
											<div className="row">
												{this.state.localdocuments.map((item, index)=> {
													return (
														<div key={index} className={`form-group col-md-4 col-sm-6 col-xs-12 marginright-5 overflowtxt ${item.ischecked ? 'doclinkactive' : ''}`} onClick={()=>this.multipleLocAttachFile(item, index)}  rel='tooltip' title={item.name} style={{cursor:'pointer',padding:'10px',borderRadius: '10px', height:'40px'}}>
															{['googledrive', 'dropbox'].indexOf(item.destination) == -1 ? <span className={`fa ${this.getfiletype(item)} marginright-5`}></span> : null }
															{['googledrive', 'dropbox'].indexOf(item.destination) >= 0 ? <span className={`fa ${this.getDestinationIcon(item.destination)} marginright-5`}></span> : null }
															{item.name}
														</div>
													);
												})}
											</div>
										</div> : null }
									</div> : null }
									{this.state.transactiondocuments.length == 0 && this.state.localdocuments.length == 0 ? <div className="col-md-12 col-xs-12 col-sm-6 text-center" style={{marginTop:'90px'}}>
										<i>No files found!!!!</i>
									</div> : null }
								</div> : null }

								{!this.state.selectmultiple ? <div className={`${this.getTabPaneClass(1)}`} id="localfile">
									{this.state.localdocuments && this.state.localdocuments.length > 0 ? <div className="col-md-12 col-xs-12 col-sm-12">
										{this.state.localdocuments.map((item, index)=>{
											return (
												<div key={index} className={`form-group col-md-4 col-sm-6 col-xs-12 overflowtxt ${item.ischecked ? 'doclinkactive' : ''}`} onClick={()=>this.attachLocalDoc(item, index)}  rel='tooltip' title={item.name} style={{cursor:'pointer',padding:'10px',borderRadius: '10px',height:'40px'}}>
													{['googledrive', 'dropbox'].indexOf(item.destination) == -1 ? <span className={`fa ${this.getfiletype(item)} marginright-5`}></span> : null }
													{['googledrive', 'dropbox'].indexOf(item.destination) >= 0 ? <span className={`fa ${this.getDestinationIcon(item.destination)} marginright-5`}></span> : null }
													{item.name}
												</div>
											);
										})}
									</div> : null }
									{this.state.localdocuments && this.state.localdocuments.length == 0 ? <div className="col-md-12 col-xs-12 col-sm-6 text-center" style={{marginTop:'90px'}}>
										<i>No files found!!!!</i>
									</div> : null }
								</div> : null }

								{!this.state.selectmultiple ? <div className={`${this.getTabPaneClass(2)}`} id="mycomputer">
									<div className="col-md-12 col-xs-12 col-sm-6">
										<Dropzone onDrop={(files)=>this.upload(files)}  multiple={false}>
											{({getRootProps, getInputProps}) => (
												<div className="filedropzone">
													<div {...getRootProps()} style={{width: '100%', height: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center', flexFlow: 'column', outline: 'none'}}>
														<input {...getInputProps()} />
														<h3>Select files to upload</h3> or drop files here.
													</div>
												</div>
											)}
										</Dropzone>
									</div>
								</div> : null }

								<div className={`${this.getTabPaneClass(3)}`} id="documentlibrary">     
									<div className="col-md-12 col-xs-12 col-sm-6">
										<div className="row">
											<div className="col-md-12 col-sm-12 col-xs-12">
												<ol className="breadcrumb">
													{this.state.breadCrumbs.map((item, index)=> {
														return(
															<li key={index} className="breadcrumb-item">
																<a onClick={()=>this.getFolderAndFiles(item, true)}>{item.name}</a>
															</li>
														);
													})}
												</ol>
											</div>
											<div className="col-md-12 col-sm-12 col-xs-12">
												<div className="card">
													<div className="card-header">
														<b><i className="fa fa-folder-open"></i> File Manager</b>
													</div>
													<div className="card-body" style={{maxHeight:'290px',overflowY:'scroll'}}>
														{this.state.documentList.length>0 ? <div className="row">
															{this.state.documentList.map((item, index)=> {
																return (
																	<div key={index} className={`col-md-3 col-sm-3 col-xs-6 ${checkActionVerbAccess(this.props.app, 'libraries', 'Read') ? '' : 'hide'}`}  align="center" onMouseEnter={()=>item.show=true} onMouseLeave={()=>item.show=false} style={{padding:'5px',height:'125px',marginBottom: '10px'}}>
																		<div className="col-md-12 col-sm-12 col-xs-12">
																			{!this.state.selectmultiple ? <div>
																				{item.isparent && (!item.destination || (['googledrive', 'dropbox'].indexOf(item.destination) == -1)) && item.readaccess ? <span onClick={()=>this.getFolderAndFiles(item, true)} style={{cursor:'pointer'}} className="fa fa-4x fa-folder-o" rel="tooltip" title={item.name}></span> : null }
																				{item.isparent && !item.parentid && !item.readaccess ? <img src="/images/Private-Folder-icon.png" alt="You have no access" style={{maxWidth:'60px',maxHeight:'60px'}} rel='tooltip' title='You have no access to view'/> : null }
																				{item.isparent ? <div className="overflowtxt" rel="tooltip" title={item.name}>
																					<label>{item.name}</label>
																				</div> : null }
																				{!item.isparent ? <div className={`text-center ${item.ischecked ? 'doclinkactive' : ''}`} style={{padding: '3px'}}>
																					{['googledrive', 'dropbox'].indexOf(item.destination) == -1 ? <span onClick={()=>this.canAttachFile(item, index)} style={{cursor:'pointer'}} className="fa fa-4x fa-file" rel="tooltip" title={item.name} ></span> : null }
																					{['googledrive', 'dropbox'].indexOf(item.destination) >= 0 ? <span onClick={()=>this.canAttachFile(item, index)} style={{cursor:'pointer'}} className={`fa fa-4x ${this.getDestinationIcon(item.destination)}`} rel='tooltip' title={item.name} ></span> : null }
																					<div className="overflowtxt" rel="tooltip" title={item.name}>
																						<label className="checkbox overflowtxt" style={{padding: '3px', marginBottom: '0px',marginTop: '5px'}}>
																							{item.name}
																						</label>
																					</div>
																				</div> : null }
																			</div> : null }
																			{this.state.selectmultiple ? <div className={`${item.ischecked ? 'doclinkactive' : ''}`} style={{padding: '3px'}}>
																				{(!item.destination || (['googledrive', 'dropbox'].indexOf(item.destination) == -1)) && item.readaccess ? <span onClick={()=>{(item.isparent) ? this.getFolderAndFiles(item, true) : this.multipleAttachFile(item, index)}} style={{cursor:'pointer'}} className={`${(item.isparent) ? 'fa fa-4x fa-folder-o':'fa fa-4x fa-file'}`} rel="tooltip" title={item.name} ></span> : null }
																				{item.isparent && !item.parentid && !item.readaccess ? <img src="/images/Private-Folder-icon.png" alt="You have no access" style={{maxWidth:'60px',maxHeight:'60px'}} rel='tooltip' title='You have no access to view'/> : null }
																				{['googledrive', 'dropbox'].indexOf(item.destination) >= 0 ? <span onClick={()=>{(item.isparent) ? this.getFolderAndFiles(item, true) : this.multipleAttachFile(item, index)}} style={{cursor:'pointer'}} className={`fa fa-4x ${this.getDestinationIcon(item.destination)}`} rel='tooltip' title={item.name}></span> : null }
																				<div className="overflowtxt" rel="tooltip" title={item.name}>
																					{!item.isparent ? <label className="checkbox overflowtxt" style={{padding: '3px', marginBottom: '0px', marginTop: '5px'}}>{item.name}</label> : null }
																					{item.isparent ? <label>{item.name}</label> : null }
																				</div>
																			</div> : null }
																		</div>
																	</div>
																);
															})}
														</div> : null }
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div className={`${this.getTabPaneClass(6)}`} id="selectedfiles">
									{this.state.selectmultiple && this.props.resource.library.length>0 ? <div className="col-md-12 col-sm-12 col-xs-12">
										<div className="row">
											{this.props.resource.library.map((item, index)=> {
												return (<div className="form-group col-md-4 col-sm-6 col-xs-12 hover-btn"  rel='tooltip' title={item.name} key={index}>
														<div className="row">
															<div className="col-md-10 overflowtxt" style={{paddingLeft:'0px',paddingRight:'0px',paddingTop:'2px',paddingBottom:'2px'}}>
																<a href={item.fileurl} target="_blank">
																	{['googledrive', 'dropbox'].indexOf(item.destination) == -1 ? <span className={`fa ${this.getfiletype(item)}`} style={{cursor:'pointer'}} rel="tooltip" title={item.name}></span> : null }
																	{['googledrive', 'dropbox'].indexOf(item.destination) >= 0 ? <span className={`fa ${this.getDestinationIcon(item.destination)}`}></span> : null }
																{item.name}
																</a>
															</div>
															<div className="col-md-2" style={{paddingLeft:'0px',paddingRight:'0px'}}>
																<button type='button' onClick={()=>this.deleteLibraryLink(item)} className='btn btn-sm gs-form-btn-danger delete-btn'>
																	<span className='fa fa-trash-o'></span>
																</button>
															</div>
														</div>
													</div>
												);
											})}
										</div>
									</div> : null }
									{this.state.selectmultiple && this.props.resource.library.length==0 ? <div className="col-md-12 col-xs-12 col-sm-6 text-center" style={{marginTop:'90px'}}>
										<i>No files found!!!!</i>
									</div> : null }
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="text-center" >
						<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal} ><i className="fa fa-times"></i>Close</button>
					</div>
				</div>
			</div>
		);
	}
}
