import React, { Component } from 'react';
import axios from 'axios';
import { LocalSelect, SelectAsync, AutoSelect } from '../components/utilcomponents';
 
class CreateListViewOptionModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.CreatelistviewsOnChange= this.CreatelistviewsOnChange.bind(this);
		this.CopyOnChange = this.CopyOnChange.bind(this);
		this.save = this.save.bind(this);		 
	}

	CreatelistviewsOnChange(param) {
		this.setState({viewtype:  param});
 	}

 	CopyOnChange(value) {
		this.setState({
			selectedviewid: value
		});
 	}

 	save() {
		if(this.state.viewtype == 'Edit') {
			let currentView;
			for(var i=0; i < this.props.pagejson.views.length; i++) {
				if(this.props.pagejson.views[i].id == this.state.selectedviewid) {
					currentView = this.props.pagejson.views[i].viewjson
					break;
				}
			}
			this.props.callback(currentView);
		} else {
			this.props.callback();
		}

  		this.props.closeModal();
 	}

	render() {
    		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title text-center" style={{color:'#29b99a',fontWeight:'600'}}>Create View</h5>
				</div>
				<div className="react-modal-body">
					<div className="row" style={{height:'150px'}}>
						<div className="col-md-12" style={{width:'100%',marginTop:'20px',color:'#1dbb99'}}>
							<div className="col-md-6" style={{width:'50%',float:'left'}}>
								<label>
									<input style={{marginRight:'10px'}} type="radio" name="viewtype" checked={this.state.viewtype == 'Create'}  onChange={(evt)=>this.CreatelistviewsOnChange('Create')}/>Create New View
								</label>
							</div>
							<div className="col-md-6" style={{width:'50%',float:'right'}}>
								<label>
									<input style={{marginRight:'10px'}} type="radio" name="viewtype"  checked={this.state.viewtype == 'Edit'} onChange={(evt)=>this.CreatelistviewsOnChange('Edit')}/>Copy Existing View
								</label>
							</div>
						</div>
						{this.state.viewtype == 'Edit' ? <div className="localselect" style={{width:'75%',marginLeft:'40px'}}>
 							<LocalSelect className={`${!this.state.selectedviewid  ? 'errorinput' : ''}`} placeholder='Select View' options={this.props.pagejson.views} label='viewname' valuename='id' value={this.state.selectedviewid} onChange={(value) => {this.CopyOnChange(value)}} required={true}/>
 						</div>: null}
 					</div>
				</div>
 				<div className="react-modal-footer" style={{borderTop:'none'}}>
					<div className="row">
						<div className="col-md-12 text-center">
							<button className="btn btn-sm btn-secondary btn-width" style={{float:'left',marginLeft:'50px',backgroundColor:'white',color:'gray',borderColor:'gray'}} onClick={this.props.closeModal}>CANCEL</button>
							<button className="btn btn-sm gs-btn-success btn-width" disabled={!this.state.viewtype || (this.state.viewtype=='Edit' && !this.state.selectedviewid)} onClick={()=>this.save()}>OK</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default CreateListViewOptionModal; 