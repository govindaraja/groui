import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../utils/services';
import { currencyFilter, dateFilter, uomFilter } from '../utils/filter';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pricefor : 'all',
			orderArray: [],
			estimationArray: [],
			quotearray: [],
			competativequotearray: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.close = this.close.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		this.setState({
			orderArray: [],
			estimationArray: [],
			quotearray: [],
			competativequotearray: []
		});
		let queryString = `/api/query/analyseitemquery?itemid=${this.props.item.itemid}&customerid=${this.props.customerid}&pricefor=${this.state.pricefor}&param=${this.props.param}`;

		axios.get(queryString).then((response) => {
			if (response.data.message == 'success') {
				if(this.props.param == 'orders')
					this.setState({
						orderArray: response.data.orderArray
					});
				else if(this.props.param == 'estimations')
					this.setState({
						estimationArray: response.data.orderArray
					});
				else
					this.setState({
						quotearray: response.data.quotearray,
						competativequotearray: response.data.competativequotearray
					});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				modalService[apiResponse.methodName](apiResponse.message);
			}
		});
	}

	close() {
		this.props.closeModal();
	}

	render() {
		let item = this.props.item;
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title gs-text-color semi-bold-custom">Price History</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row justify-content-between">
						<div className="form-group col-md-8 col-sm-12">
							<div>Price History For <b>{item.itemid_name}</b></div>
						</div>
						<div className="form-group col-md-3 col-sm-12">
							<select className="form-control" value={this.state.pricefor} onChange={(e) => this.setState({pricefor:e.target.value}, () => this.onLoad())}>
								<option value="all">All Customer</option>
								<option value="current">Current Customer</option>
							</select>
						</div>
					</div>
					<div className="row">
					{this.props.param == 'quotes' && this.state.quotearray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
						<div className="table-responsive">
							<table className="table table-sm gs-table gs-table-bordered">
								<thead className="thead-light">
									<tr>
										<th className="text-center">Customer</th>
										<th className="text-center">Quote No</th>
										<th className="text-center">Quote Date</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">Rate</th>
										<th className="text-center">Discount</th>
										<th className="text-center">Final Rate</th>
										<th className="text-center">Status</th>
									</tr>
								</thead>
								<tbody>
									{this.state.quotearray.map((quote, key) => {
										return <tr key = {key}>
											<td className="text-center">{quote.customerid_name}</td>
											<td className="text-center">{quote.quoteno}</td>
											<td className="text-center">{dateFilter(quote.quotedate)}</td>
											<td className="text-center">{quote.quantity} {uomFilter(quote.uomid, this.props.app.uomObj)}</td>
											<td className="text-right">{currencyFilter(quote.rate, quote.currencyid, this.props.app)}</td>
											<td className="text-right">
												{quote.discountquantity ? (quote.discountmode != "Percentage" ? currencyFilter(quote.discountquantity, quote.currencyid, this.props.app) : `${quote.discountquantity} %`) : ''}
											</td>
											<td className="text-right">{currencyFilter(quote.finalrate, quote.currencyid, this.props.app)}</td>
											<td className="text-center">{quote.status}</td>
										</tr>
									})}
								</tbody>
								
							</table>
						</div>
					</div> : this.props.param == 'quotes' ? <div className="col-md-12"><div className="alert alert-info text-center margintop-10">
							<span>Quote History not available</span>
						</div></div> : null}
					{this.props.app.feature.useCompetitiveQuotations && this.props.param == 'quotes' && this.state.competativequotearray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
						<div className="table-responsive">
							<table className="table table-sm gs-table gs-table-bordered">
								<thead className="thead-light">
									<tr>
										<th className="text-center">Competitor</th>
										<th className="text-center">Quote Date</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">Rate</th>
										<th className="text-center">Discount</th>
										<th className="text-center">Final Rate</th>
										<th className="text-center">Outcome</th>
									</tr>
								</thead>
								<tbody>
									{this.state.competativequotearray.map((quote, key) => {
										return <tr key = {key}>
											<td className="text-center">{quote.name}</td>
											<td className="text-center">{dateFilter(quote.quotedate)}</td>
											<td className="text-center">{quote.quantity} {uomFilter(quote.uomid, this.props.app.uomObj)}</td>
											<td className="text-right">{currencyFilter(quote.rate, quote.currencyid, this.props.app)}</td>
											<td className="text-right">
												{quote.discountquantity ? (quote.discountmode != "Percentage" ? currencyFilter(quote.discountquantity, quote.currencyid, this.props.app) : `${quote.discountquantity} %`) : ''}
											</td>
											<td className="text-right">{currencyFilter(quote.finalrate, quote.currencyid, this.props.app)}</td>
											<td className="text-center">{quote.outcome}</td>
										</tr>
									})}
								</tbody>
								
							</table>
						</div>
					</div> : this.props.app.feature.useCompetitiveQuotations && this.props.param == 'quotes' ? <div className="col-md-12"><div className="alert alert-info text-center margintop-10">
							<span>Competitor History not available</span>
						</div></div> : null}
					{this.props.param == 'orders' && this.state.orderArray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
						<div className="table-responsive">
							<table className="table table-sm gs-table gs-table-bordered">
								<thead className="thead-light">
									<tr>
										<th className="text-center">Customer</th>
										<th className="text-center">Order No</th>
										<th className="text-center">Order Date</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">Rate</th>
										<th className="text-center">Discount</th>
										<th className="text-center">Final Rate</th>
									</tr>
								</thead>
								<tbody>
									{this.state.orderArray.map((order, key) => {
										return <tr key = {key}>
											<td className="text-center">{order.customerid_name}</td>
											<td className="text-center">{order.orderno}</td>
											<td className="text-center">{dateFilter(order.orderdate)}</td>
											<td className="text-center">{order.quantity} {uomFilter(order.uomid, this.props.app.uomObj)}</td>
											<td className="text-right">{currencyFilter(order.rate, order.currencyid, this.props.app)}</td>
											<td className="text-right">
												{order.discountquantity ? (order.discountmode != "Percentage" ? currencyFilter(order.discountquantity, order.currencyid, this.props.app) : `${order.discountquantity} %`) : ''}
											</td>
											<td className="text-right">{currencyFilter(order.finalrate, order.currencyid, this.props.app)}</td>
										</tr>
									})}
								</tbody>
								
							</table>
						</div>
					</div> : this.props.param == 'orders' ? <div className="col-md-12"><div className="alert alert-info text-center margintop-10">
							<span>Previous order history not available!!!</span>
						</div></div> : null}
					{this.props.param == 'estimations' && this.state.estimationArray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
						<div className="table-responsive">
							<table className="table table-sm gs-table gs-table-bordered">
								<thead className="thead-light">
									<tr>
										<th className="text-center">Customer</th>
										<th className="text-center">Estimation No</th>
										<th className="text-center">Estimation Date</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">Rate</th>
										<th className="text-center">Discount</th>
										<th className="text-center">Final Rate</th>
									</tr>
								</thead>
								<tbody>
									{this.state.estimationArray.map((estimation, key) => {
										return <tr key = {key}>
											<td className="text-center">{estimation.customerid_name}</td>
											<td className="text-center">{estimation.estimationno}</td>
											<td className="text-center">{dateFilter(estimation.estimationdate)}</td>
											<td className="text-center">{estimation.quantity} {uomFilter(estimation.uomid, this.props.app.uomObj)}</td>
											<td className="text-right">{currencyFilter(estimation.rate, estimation.currencyid, this.props.app)}</td>
											<td className="text-right">
												{estimation.discountquantity ? (estimation.discountmode != "Percentage" ? currencyFilter(estimation.discountquantity, estimation.currencyid, this.props.app) : `${estimation.discountquantity} %`) : ''}
											</td>
											<td className="text-right">{currencyFilter(estimation.finalrate, estimation.currencyid, this.props.app)}</td>
										</tr>
									})}
								</tbody>
								
							</table>
						</div>
					</div> : this.props.param == 'estimations' ? <div className="col-md-12"><div className="alert alert-info text-center margintop-10">
							<span>Previous estimation history not available!!!</span>
						</div></div> : null}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
