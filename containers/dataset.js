import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import async from 'async';
import { reduxForm, Field } from 'redux-form';
import { Prompt } from "react-router-dom";

import { updateFormState, updateAppState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import { LocalSelect, SelectAsync, AutoSelect, DateElement, ChildEditModal, AutoSuggest } from '../components/utilcomponents';
import Loadingcontainer from '../components/loadingcontainer';
import { InputEle, localSelectEle, autoSelectEle, checkboxEle, NumberEle, DateEle, selectAsyncEle, dateTimeEle, autosuggestEle } from '../components/formelements';
import {stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation, requiredNewValidation, checkActionVerbAccess, checkArray } from '../utils/utils';
import { AdditionalInformationSection } from './ndtsection1';
import { ReportNumberEle, ReportBooleanEle, ReportDateEle, ReportLocalSelectEle, ReportAutoSelectEle, ReportAutoMultiSelectEle } from '../reportbuilder/reportformelements';

const columnFieldArr = [{
	id: 'string',
	name: 'Text'
}, {
	id: 'integer',
	name: 'Number'
}, {
	id: 'date',
	name: 'Date'
}, {
	id: 'datetime',
	name: 'Date Time'
}];

/*const calculationTypeArr = {
	date: [{
		id: 'coalesce',
		name: 'Either Any One'
	}, {
		id: 'dateformatter',
		name: 'Date Formatter'
	}],
	datetime: [{
		id: 'coalesce',
		name: 'Either Any One'
	}, {
		id: 'dateformatter',
		name: 'Date Formatter'
	}],
	default: [{
		id: 'coalesce',
		name: 'Either Any One'
	}]
};*/

const calculationTypeArr = [{
	id: 'coalesce',
	name: 'Either Any One'
}, {
	id: 'dateformatter',
	name: 'Date Formatter'
}, {
	id: 'datedifference',
	name: 'Date Difference'
}, {
	id: 'range',
	name: 'Range'
}, {
	id: 'mathexpression',
	name: 'Formula'
}, {
	id: 'fixedvalue',
	name: 'Fixed Value'
}];

const dateFormatterTypeArr = [{
	id: 'financialyear',
	name: 'Financial Year'
}, {
	id: 'calendaryear',
	name: 'Calendar Year'
}, {
	id: 'month-year',
	name: 'Month-Year'
}, {
	id: 'month',
	name: 'Month'
}, {
	id: 'financialquarter',
	name: 'Financial Quarter'
}, {
	id: 'financialquarter-financialyear',
	name: 'Financial Quarter - Year'
}, {
	id: 'calendarquarter',
	name: 'Calendar Quarter'
}, {
	id: 'calendarquarter-calendaryear',
	name: 'Calendar Quarter - Year'
}, {
	id: 'date',
	name: 'Date'
}, {
	id: 'datetime',
	name: 'Date Time'
}, {
	id: 'time',
	name: 'Time'
}];

class DataSet extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : false,
			localTocuhed: false,
			createParam: this.props.match.params.id > 0 ? false : true,
			galleryArray: [],
			mathExpParams: {
				0: 'a', 1: 'b', 2: 'c',3: 'd',4: 'e', 5:'f',6: 'g',7: 'h',8: 'i',9: 'j',10: 'k',11: 'l',12: 'm',13: 'n',14: 'o',15: 'p',16: 'q',17: 'r',18: 's',19: 't',20: 'u',21: 'v',22: 'w',23: 'x',24: 'y',25: 'z'
			}
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getColumns = this.getColumns.bind(this);
		this.renderCreate = this.renderCreate.bind(this);
		this.cancel = this.cancel.bind(this);
		this.getItemById = this.getItemById.bind(this);
		this.create = this.create.bind(this);
		this.renderFilter = this.renderFilter.bind(this);
		this.addFilter = this.addFilter.bind(this);
		this.deleteFilter = this.deleteFilter.bind(this);
		this.addColumn = this.addColumn.bind(this);
		this.deleteColumn = this.deleteColumn.bind(this);
		this.editCB = this.editCB.bind(this);
		this.groupByOnChange = this.groupByOnChange.bind(this);
		this.injectDataSetCB = this.injectDataSetCB.bind(this);
		this.baseResourceCB = this.baseResourceCB.bind(this);
		this.createReport = this.createReport.bind(this);
		this.listReport = this.listReport.bind(this);
		this.renderConfigurableColumnItem = this.renderConfigurableColumnItem.bind(this);
		this.getGalleryDetails = this.getGalleryDetails.bind(this);
		this.galleryNameOnChange = this.galleryNameOnChange.bind(this);
		this.renderMathExpParams = this.renderMathExpParams.bind(this);
		this.addMathParam = this.addMathParam.bind(this);
		this.deleteMathParam = this.deleteMathParam.bind(this);
		this.import = this.import.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag)
			this.updateLoaderFlag(false);
	}

	componentWillMount() {
		let resourceArray = [];
		let myResources = JSON.parse(JSON.stringify(this.props.app.myResources));
		let resourceOptionsArray = [];
		this.doFieldFormatting(myResources);
		for(var prop in myResources) {
			if (myResources[prop].type != 'query')
				resourceOptionsArray.push({
					id : prop,
					name : myResources[prop].displayName
				});

			if(myResources[prop].type != 'query' && !myResources[prop].hideInReports)
				resourceArray.push(myResources[prop]);
		}

		resourceArray.sort((a, b) => {
			return (a.displayName.toLowerCase() < b.displayName.toLowerCase()) ? -1 : (a.displayName.toLowerCase() > b.displayName.toLowerCase()) ? 1 : 0;
		});

		let datasetidsURL = `/api/datasets?field=id,name,identifier&skip=0`;
		if(this.props.match.params.id > 0)
			datasetidsURL += `&filtercondition=datasets.id<>${this.props.match.params.id}`;

		axios.get(datasetidsURL).then((response) => {
			if (response.data.message == 'success') {
				let dataSetObj = {};
				let dataSetIDObj = {};

				response.data.main.sort((a, b) => {
					return (a.name.toLowerCase() < b.name.toLowerCase()) ? -1 : (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : 0;
				});

				response.data.main.forEach((dataitem) => {
					dataSetObj[dataitem.identifier] = dataitem.id;
					dataSetIDObj[dataitem.id] = dataitem.identifier;
				});
				this.setState({ dataSetArray:  response.data.main, dataSetObj, dataSetIDObj });
				if(this.state.createParam) {
					this.props.initialize({ '_dummy': 'dummyvalue'});

					this.datasetCreateModal();
				} else
					this.getItemById();

				this.getGalleryDetails();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});

		this.setState({ resourceArray, resourceOptionsArray, myResources });
	}

	datasetCreateModal() {
		this.props.openModal({
			render: (closeModal) => {
				return <DatasetCreateOptionModal closeModal = {
					closeModal
				}
				callback = {
					(val) => {
						if (val) {
							if (val == 'gallery')
								this.galleryNameOnChange();
							else if (val == 'import')
								this.import();
							else
								this.renderCreate();
						} else
							this.props.history.goBack();

						this.updateLoaderFlag(false);
					}
				}
				/>
			},
			className: {
				content: 'react-modal-custom-class-40',
				overlay: 'react-modal-overlay-custom-class'
			},
			confirmModal: true
		});
	}

	getGalleryDetails () {
		let { galleryArray } = this.state;

		galleryArray = [];

		axios.get(`/api/common/methods/getDatasetTemplates`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.length > 0)
					galleryArray = JSON.parse(JSON.stringify(response.data.main));

				this.setState({galleryArray});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	doFieldFormatting(myResources) {
		if(this.props.app.feature.useMasterForAddresses) {
			myResources.countries.hideInReports = false;
			myResources.states.hideInReports = false;
			myResources.cities.hideInReports = false;
			myResources.areas.hideInReports = false;
			myResources.streets.hideInReports = false;

			myResources.addresses.fields.countryid.hideInReports = false;
			myResources.addresses.fields.stateid.hideInReports = false;
			myResources.addresses.fields.cityid.hideInReports = false;
			myResources.addresses.fields.areaid.hideInReports = false;
			myResources.addresses.fields.streetid.hideInReports = false;

			myResources.addresses.fields.secondline.hideInReports = true;
			myResources.addresses.fields.city.hideInReports = true;
			myResources.addresses.fields.state.hideInReports = true;
			myResources.addresses.fields.country.hideInReports = true;
			myResources.addresses.fields.postalcode.hideInReports = true;
		} else {
			myResources.countries.hideInReports = true;
			myResources.states.hideInReports = true;
			myResources.cities.hideInReports = true;
			myResources.areas.hideInReports = true;
			myResources.streets.hideInReports = true;

			myResources.addresses.fields.countryid.hideInReports = true;
			myResources.addresses.fields.stateid.hideInReports = true;
			myResources.addresses.fields.cityid.hideInReports = true;
			myResources.addresses.fields.areaid.hideInReports = true;
			myResources.addresses.fields.streetid.hideInReports = true;

			myResources.addresses.fields.secondline.hideInReports = false;
			myResources.addresses.fields.city.hideInReports = false;
			myResources.addresses.fields.state.hideInReports = false;
			myResources.addresses.fields.country.hideInReports = false;
			myResources.addresses.fields.postalcode.hideInReports = false;
		}
		for(var prop in myResources) {
			let resObj = myResources[prop];
			if(resObj.fields && resObj.fields.parentresource && resObj.fields.parentid) {
				let parentResourceArray = [];
				for (var secprop in myResources) {
					if(myResources[secprop].childResource && myResources[secprop].childResource.length > 0) {
						for(var i=0;i<myResources[secprop].childResource.length;i++) {
							if(myResources[secprop].childResource[i].name == prop) {
								parentResourceArray.push(secprop);
								break;
							}
						}
					}
				}

				if(parentResourceArray.length > 0) {
					resObj.fields.parentresource.parentResourceArray = parentResourceArray;

					delete resObj.fields.parentresource;
					delete resObj.fields.parentid;
					parentResourceArray.forEach((parentres) => {
						if(myResources[parentres].hideInReports)
							return null;

						let titleField = 'id';
						for(var prop in myResources[parentres].fields) {
							if(myResources[parentres].fields[prop].title) {
								titleField = prop;
								break;
							}
						}
						resObj.fields[`parent${parentres}`] = {
							type : "integer",
							displayName : `Parent ${myResources[parentres].displayName}`,
							showInAdvanceSearch : true,
							restrictToCopy : true,
							isForeignKey : true,
							group : "selectasync",
							isParentResource: true,
							isOnlyOneParentResource: parentResourceArray.length == 1 ? true : false,
							foreignKeyOptions : {
								resource : parentres,
								mainField : `${parentres}_parent${parentres}.${titleField}`,
								additionalField : []
							},
							filterformat : "text",
							emailtype : "simple"
						}
					});
				}
			}
			for(var fieldprop in resObj.fields) {
				if(fieldprop == 'numberingseriesmasterid')
					resObj.fields[fieldprop].numberingseries_resource = prop;

				if(resObj.fields[fieldprop].title)
					resObj.fields[fieldprop].title_resource = resObj.resourceName;

				if(resObj.fields[fieldprop].isDataSetSplitResField) {
					resObj.fields[fieldprop].dsSplitResources.forEach(splitres => {
						let titleField = 'id';
						for(var prop in myResources[splitres].fields) {
							if(myResources[splitres].fields[prop].title) {
								titleField = prop;
								break;
							}
						}
						resObj.fields[`split${splitres}`] = {
							type : "integer",
							displayName : `${resObj.fields[fieldprop].dsSplitDisplayName} ${myResources[splitres].displayName}`,
							showInAdvanceSearch : true,
							restrictToCopy : true,
							isForeignKey : true,
							group : "selectasync",
							splitIDField: `${resObj.fields[fieldprop].dsSplitIDField}`,
							isParentResource: true,
							foreignKeyOptions : {
								resource : splitres,
								mainField : `${splitres}_source${splitres}.${titleField}`,
								additionalField : []
							},
							filterformat : "text",
							emailtype : "simple"
						}
					});
				}

				if(resObj.fields[fieldprop] && (fieldprop == 'isdeleted' || resObj.fields[fieldprop].hideInReports))
					delete resObj.fields[fieldprop];
			}
		}
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	getItemById() {
		this.updateLoaderFlag(true);
		axios.get(`/api/datasets/${this.props.match.params.id}`).then((response) => {
			if (response.data.message == 'success') {
				response.data.main.tempresource = response.data.main.config.dataSetArray[0].resource;
				response.data.main.config.configurablecolumns = response.data.main.config.configurablecolumns ? response.data.main.config.configurablecolumns : [];
				this.getDatasetInfo(response.data.main.datasetids, () => {
					this.props.initialize(response.data.main);
					this.setState({
						ndt: {
							notes: response.data.notes,
							documents: response.data.documents,
							tasks: response.data.tasks
						}
					});
					this.updateLoaderFlag(false);
				});
			} else {
				this.updateLoaderFlag(false);
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getDatasetInfo(datasetids, cb) {
		datasetids = datasetids ? datasetids : [];
		this.updateLoaderFlag(true);
		let datasetObj = {};
		async.each(datasetids, (datasetid, eachcb) => {
			if(this.state.myResources[this.state.dataSetIDObj[datasetid]]) {
				datasetObj[`${this.state.myResources[this.state.dataSetIDObj[datasetid]].identifier}`] = this.state.myResources[this.state.dataSetIDObj[datasetid]];
				return eachcb(null);
			}
			axios.get(`/api/dataset/info/${datasetid}`).then((response) => {
				if(response.data.message == 'success') {
					datasetObj[`${response.data.main.identifier}`] = response.data.main;
					eachcb(null);
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
					eachcb(true);
				}
			});
		}, (err) => {
			if(err)
				return null;

			let { resourceArray, myResources } = this.state;

			for(var prop in myResources) {
				if(myResources[prop].type == 'dataset')
					delete myResources[prop];
			}
			resourceArray = [];
			for(var prop in myResources) {
				if(myResources[prop].type != 'query' && !myResources[prop].hideInReports)
					resourceArray.push(myResources[prop]);
			}

			resourceArray.sort((a, b) => {
				return (a.displayName.toLowerCase() < b.displayName.toLowerCase()) ? -1 : (a.displayName.toLowerCase() > b.displayName.toLowerCase()) ? 1 : 0;
			});

			for(var prop in datasetObj) {
				resourceArray.push(datasetObj[prop]);
				myResources[prop] = datasetObj[prop];
			}
			this.setState({ resourceArray, myResources });

			if(cb)
				return cb();
			else
				return this.updateLoaderFlag(false);
		});
	}

	resourceOnChange(datasetStr) {
		this.props.updateFormState(this.props.form, {
			[`${datasetStr}.joinedresources`]: [],
			[`${datasetStr}.columns`]: []
		});
	}

	groupByOnChange() {
		let config = {
			...this.props.resource.config
		};

		let fnChangeAggType = (dataset) => {
			(dataset.columns || []).forEach((col) => {
				delete col.aggregationType;
			});

			(dataset.joinedresources || []).forEach((joi) => {
				fnChangeAggType(joi);
			});
		}

		fnChangeAggType(config.dataSetArray[0]);
		config.groupby = null;
		//config.configurablecolumns = [];

		this.props.updateFormState(this.props.form, { config });
	}

	galleryNameOnChange () {
		let { galleryArray } = this.state;

		this.props.openModal({
			render: (closeModal) => {
				return <ChildEditModal
					openModal = {this.props.openModal}
					form = {this.props.form}
					resource = {this.props.resource}
					history = {this.props.history}
					app = {this.props.app}
					galleryArray = {galleryArray}
					updateFormState = {this.props.updateFormState}
					callback = {(modalClose) => this.create(true, modalClose)}
					closeModal = {closeModal}
					getBody = {() => {return <GalleryReportsModal />}} />
			}, className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			},
			confirmModal: true
		});
	}

	import (fileJSON) {
		let importCallback = (fileImpJSON, modalClose) => {
			if(fileImpJSON) {
				return this.import(fileImpJSON);
				modalClose();
			}

			this.importCreate(modalClose);
		};

		this.props.openModal({
			render: (closeModal) => {
				return <ChildEditModal
					openModal = {this.props.openModal}
					form = {this.props.form}
					resource = {this.props.resource}
					history = {this.props.history}
					app = {this.props.app}
					fileJSON={fileJSON}
					updateFormState = {this.props.updateFormState}
					callback = {(firstParam, modalClose) => importCallback(firstParam, modalClose)}
					closeModal = {closeModal}
					getBody = {() => {return <ImportModal />}} />
			}, className: {
				content: `react-modal-custom-class-${fileJSON ? '80' : '40'}`,
				overlay: 'react-modal-overlay-custom-class'
			},
			confirmModal: true
		});
	}

	cancel() {
		this.props.history.goBack();
	}

	create(modalFlag, modalClose) {

		this.updateLoaderFlag(true);

		let resource = {
			...this.props.resource
		},
		confirm = modalClose ? false : modalFlag;

		resource.config = {
			isgroupby: false,
			dataSetArray: [{
				"resource": this.props.resource.tempresource,
				"children": []
			}]
		};

		if(this.props.resource.tempresource && this.state.myResources[this.props.resource.tempresource].fields.companyid)
			resource.config.dataSetArray[0].columns = [{
				field: 'companyid'
			}];

		axios({
			method : 'post',
			data : {
				actionverb : 'Save',
				data : resource,
				ignoreExceptions : confirm
			},
			url : '/api/datasets'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
				if (resparam)
					this.create(true);
			});

			if (modalFlag && modalClose)
				modalClose(response.data.message == 'success' ? true : false);

			if (response.data.message == 'success')
				this.props.history.replace(`/details/datasets/${response.data.main.id}`);

			this.updateLoaderFlag(false);
		});
	}

	importCreate(modalClose) {
		this.updateLoaderFlag(true);

		let resource = {
			...this.props.resource
		};

		axios({
			method : 'post',
			data : {
				actionverb : 'Save',
				data : resource
			},
			url : '/api/datasets'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

			modalClose(response.data.message == 'success' ? true : false);

			if (response.data.message == 'success')
				this.props.history.replace(`/details/datasets/${response.data.main.id}`);

			this.updateLoaderFlag(false);
		});
	}

	validateDataset(actionverb) {
		if(actionverb != 'Save')
			return this.save(actionverb);

		let emptyFieldResArray = [], errorArray = [];

		let checkResourceFields = (dataset, parentdataset) => {
			if((dataset.columns || []).length == 0) {
				if(!emptyFieldResArray.includes(this.state.myResources[dataset.resource].displayName))
					emptyFieldResArray.push(this.state.myResources[dataset.resource].displayName);
			}

			if(!parentdataset) {
				let companyfieldarray = [];
				let companyFieldFound = false;
				if(this.state.myResources[dataset.resource].fields.companyid) {
					companyfieldarray.push('companyid');
					for(var i=0;i<(dataset.columns || []).length;i++) {
						if(dataset.columns[i].field == 'companyid') {
							companyFieldFound = true;
							break;
						}
					}
				} else {
					let parentResourceArr = [];
					for(var prop in this.state.myResources[dataset.resource].fields) {
						let fieldObj = this.state.myResources[dataset.resource].fields[prop];
						if(fieldObj.isParentResource)
							parentResourceArr.push({field: prop, resource: fieldObj.foreignKeyOptions.resource});
					}
					parentResourceArr.forEach((parentres) => {
						if(this.state.myResources[parentres.resource].fields.companyid)
							companyfieldarray.push(`${parentres.field}_companyid`);

						for(var i=0;i<dataset.joinedresources.length;i++) {
							if(dataset.joinedresources[i].type == 'related' && dataset.joinedresources[i].resource == parentres.resource && dataset.joinedresources[i].field == parentres.field) {
								for(var j=0;j<(dataset.joinedresources[i].columns || []).length;j++) {
									if(dataset.joinedresources[i].columns[j].field == 'companyid') {
										companyFieldFound = true;
										break;
									}
								}
							}
						}
					});

				}
				let companyFilterFound = false;
				for (var i = 0; i < companyfieldarray.length; i++) {
					for (var j = 0; j < (this.props.resource.config.filters || []).length; j++) {
						if (companyfieldarray[i] == this.props.resource.config.filters[j].field) {
							companyFilterFound = true;
							break;
						}
					}
				}

				/*if(companyfieldarray.length > 0 && !companyFilterFound)
					errorArray.push(`You have not added company filter. This will show data for all companies. We strongly recommend that you add company filter.`);*/

				if(companyfieldarray.length > 0 && !companyFieldFound)
					errorArray.push(`You have not added company field. We strongly recommend that you add company field.`);
			}

			(dataset.joinedresources || []).forEach((chidataset) => {
				checkResourceFields(chidataset, dataset);
			});
		}
		checkResourceFields(this.props.resource.config.dataSetArray[0]);

		if(emptyFieldResArray.length > 0)
			errorArray.push(`You have not selected any columns for ${emptyFieldResArray.join(', ')}.`);

		if(errorArray.length == 0)
			return this.save(actionverb);

		let message = {
			header : 'Warning',
			bodyArray: [...errorArray],
			btnTitle : ` Do you want to continue?`,
			btnArray : ['Yes','No']
		};
		this.props.openModal(modalService['confirmMethod'](message, (param) => {
			if(param) {
				this.save(actionverb);
			}
		}));
	}

	save(actionverb, confirm) {
		this.updateLoaderFlag(true);

		let resource = {
			...this.props.resource
		};

		axios({
			method : 'post',
			data : {
				actionverb : actionverb,
				data : resource,
				ignoreExceptions : confirm ? true : false
			},
			url : '/api/datasets'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message), (resparam) => {
				if (resparam)
					this.save(actionverb, true);
			});

			if (response.data.message == 'success') {
				this.setState({
					localTocuhed: false
				});
				if(actionverb == 'Delete')
					this.props.history.replace('/list/datasets');
				else
					this.props.initialize(response.data.main);
			}
			this.updateLoaderFlag(false);
		});
	}

	changeInjectDataset() {
		this.props.openModal({
			render: (closeModal) => {
				return <InjectDatasetEdit dataSetArray={this.state.dataSetArray} datasetids={this.props.resource.datasetids} closeModal={closeModal} openModal={this.props.openModal} callback={this.injectDataSetCB} />
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true
		});
	}

	injectDataSetCB(datasetids) {
		this.updateLoaderFlag(true);
		this.getDatasetInfo(datasetids, () => {
			let tempObj = {
				datasetids,
				config: {
					...this.props.resource.config
				}
			};
			let checkResourceIsDataset = (dataset) => {
				if (this.state.dataSetObj[dataset.resource]) {
					let datasetid = this.state.dataSetObj[dataset.resource];
					if (tempObj.datasetids.indexOf(datasetid) == -1) {
						return false;
					}
				}
				return true;
			}
			let loopResourceIsDataset = (joiresArray) => {
				for (var i = 0; i < joiresArray.length; i++) {
					let checkAccess = checkResourceIsDataset(joiresArray[i]);
					if (!checkAccess) {
						joiresArray.splice(i, 1);
						i = i - 1;
					} else {
						loopResourceIsDataset((joiresArray[i].joinedresources || []));
					}
				}
			}
			loopResourceIsDataset((tempObj.config.dataSetArray[0].joinedresources || []));
			this.props.updateFormState(this.props.form, tempObj);
			this.setState({
				localTocuhed: true
			});
			this.updateLoaderFlag(false);
		});
	}

	changeBaseResource(dataset) {
		this.props.openModal({
			render: (closeModal) => {
				return <BaseResourceEdit baseresource={dataset.resource} resourceArray={this.state.resourceArray} closeModal={closeModal} openModal={this.props.openModal} callback={this.baseResourceCB} />
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true
		});
	}

	baseResourceCB(newresource) {
		let tempObj = {
			config: {
				...this.props.resource.config
			}
		};

		tempObj.config.dataSetArray = [{
			resource: newresource,
			children: []
		}];

		if(this.state.myResources[newresource].fields.companyid)
			tempObj.config.dataSetArray[0].columns = [{
				field: 'companyid'
			}];

		tempObj.config.isgroupby = false;
		tempObj.config.filters = [];
		tempObj.config.groupby = [];

		this.props.updateFormState(this.props.form, tempObj);
		this.setState({
			'localTocuhed' : true
		});
		this.updateLoaderFlag(false);
	}

	joinResource(dataset, datasetStr, param) {
		this.props.openModal({
			render: (closeModal) => {
				return <DataSetEdit dataset={dataset} datasetStr={datasetStr} myResources={this.state.myResources} app={this.props.app} closeModal={closeModal} openModal={this.props.openModal} updateFormState={this.props.updateFormState} callback={this.editCB} form={this.props.form} param={param} />
			}, className: {content: 'react-modal-custom-class-60', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true
		});
	}

	deleteResource(parentdataset, index, parentdatasetStr) {
		let joinedresources = [...parentdataset.joinedresources];
		joinedresources.splice(index, 1);
		this.props.updateFormState(this.props.form, {
			[`${parentdatasetStr}.joinedresources`]: joinedresources
		});
		this.setState({
			'localTocuhed' : true
		});
	}

	editCB() {
		this.updateLoaderFlag(true);
		setTimeout(() => {
			let columnArray = [];
			if(this.props.resource.config.dataSetArray[0].resource)
				this.getColumns(columnArray, this.props.resource.config.dataSetArray[0], 'config.dataSetArray[0]');

			let columnObj = {};
			columnArray.forEach(column => columnObj[column.fieldName] = column);

			let ccArray = (this.props.resource.config.configurablecolumns || []).map(col => col.field);

			let tempObj = {};
			let groupby = null, filters = [];
			if(this.props.resource.config.isgroupby && this.props.resource.config.groupby && this.props.resource.config.groupby.length > 0) {
				groupby = [...this.props.resource.config.groupby];

				for (var i = 0; i < groupby.length; i++) {
					if (!columnObj[groupby[i]] && !ccArray.includes(groupby[i])) {
						groupby.splice(i, 1);
						i = -1;
					}
				}
			}
			if(this.props.resource.config.filters && this.props.resource.config.filters.length > 0) {
				filters = [...this.props.resource.config.filters];

				for (var i = 0; i < filters.length; i++) {
					if (!columnObj[filters[i].field]) {
						filters.splice(i, 1);
						i = i - 1;
					}
				}
			}

			this.props.updateFormState(this.props.form, {
				[`config.groupby`]: groupby,
				[`config.filters`]: filters
			});
			this.setState({
				'localTocuhed' : true
			});

			this.updateLoaderFlag(false);

		}, 0);
	}

	addFilter() {
		let filters = [...(this.props.resource.config.filters || [])];

		filters.push({});

		this.props.updateFormState(this.props.form, {
			[`config.filters`]: filters
		});
	}

	deleteFilter(filterindex) {
		let filters = [...(this.props.resource.config.filters || [])];

		filters.splice(filterindex, 1);

		this.props.updateFormState(this.props.form, {
			[`config.filters`]: filters
		});
	}

	addColumn() {
		let configurablecolumns = [...(this.props.resource.config.configurablecolumns || [])];

		configurablecolumns.push({
			field: `cc${Math.floor(Math.random() * 10000)}`
		});

		this.props.updateFormState(this.props.form, {
			[`config.configurablecolumns`]: configurablecolumns
		});
	}

	deleteColumn(filterindex) {
		let configurablecolumns = [...(this.props.resource.config.configurablecolumns || [])];

		configurablecolumns.splice(filterindex, 1);

		this.props.updateFormState(this.props.form, {
			[`config.configurablecolumns`]: configurablecolumns
		});
	}

	copyfunction() {
		let copyOptionCB = (createObj) => {
			this.updateLoaderFlag(true);

			let resource = JSON.parse(JSON.stringify(this.props.resource));

			delete resource.id;
			delete resource.created;
			delete resource.modified;
			delete resource.createdby;
			delete resource.modifiedby;
			delete resource.isactive;
			delete resource.isdeleted;
			delete resource.identifier;

			resource.name = createObj.name;

			axios({
				method : 'post',
				data : {
					actionverb : 'Save',
					data : resource
				},
				url : '/api/datasets'
			}).then((response) => {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

				if (response.data.message == 'success') {
					this.props.history.replace(`/details/datasets/${response.data.main.id}`);
				}

				this.updateLoaderFlag(false);
			});
		};

		this.props.openModal({
			render: (closeModal) => {
				return <DataSetCopyModal openModal={this.props.openModal} closeModal={closeModal} callback={copyOptionCB} />
			}, className: {content: 'react-modal-custom-class-30', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	exportfunction() {
		this.updateLoaderFlag(true);
		axios.get(`/api/common/methods/exportDataSet?datasetid=${this.props.resource.id}`).then((response) => {
			if (response.data.message == 'success') {
				let a = document.createElement('a');
				a.href = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(response.data.main));
				a.target = `_blank`;
				a.download = `${response.data.main.name.replace(/[^A-Za-z0-9_()\s-@]/g, '_')}.json`;
				document.body.appendChild(a);
				a.click();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	renderDataSet(datasetStr, dataset, index, parentdataset, childlength, parentdatasetStr) {

		let childrenArray = [];

		let baseRes, baseResField, baseResObj, joinRes, joinResField, joinResObj, relatedDisplayName;

		if(parentdataset && (parentdataset.type == 'related' || parentdataset.type == 'child')) {
			baseRes = parentdataset.resource;
			baseResField = parentdataset.field;
			baseResObj = this.state.myResources[baseRes];
		} else if(parentdataset) {
			baseResObj = this.state.myResources[parentdataset.resource];
		}

		if(dataset.type == 'related' || dataset.type == 'child') {
			joinRes = dataset.resource;
			joinResField = dataset.field;
			joinResObj = this.state.myResources[joinRes];
			if(dataset.type == 'related')
				relatedDisplayName = `${baseResObj.fields[joinResField].displayName}-${joinResObj.displayName}`;
			else
				relatedDisplayName = `${joinResObj.displayName}-${joinResObj.fields[joinResField].displayName}`;
		} else {
			joinResObj = this.state.myResources[dataset.resource];
			relatedDisplayName = `${joinResObj.displayName}`;
		}

		(dataset.joinedresources || []).forEach((joires) => {
			childrenArray.push(joires);
		});

		return (
			<div key={index} className="gs-dataset-outerbox">
				{parentdataset ? <div className={`gs-dataset-innerbox gs-dataset-innerbox-${childlength == 1 ? 'one' : (0 == index ? 'first' : (index == (childlength - 1) ? 'last' : 'mid'))}`}>
					<div className={`gs-dataset-innerbox-inner ${index == 0 ? 'gs-dataset-innerbox-inner-first' : ''}`}>

					</div>
				</div> : null}
				<div className="card borderradius-0 marginbottom-15 gs-card gs-dataset-cardbox">
					<div className="gs-dataset-inner-cardbox">
						<label className="gs-dataset-label-resource">{parentdataset ? null : <button type="button" onClick={() => this.changeBaseResource(dataset)} rel="tooltip" title="Edit Base Resource" className="btn btn-sm gs-form-btn-default gs-btn-default marginright-3" style={{marginTop: '-3px'}}> <span className="fa fa-edit" ></span></button>}<b>{relatedDisplayName}</b></label>
						<div style={{display: 'flex'}}>
							<button type='button' className="btn btn-sm gs-form-btn-primary marginright-3" onClick={() => this.joinResource(dataset, datasetStr, 'joinresource')}><span style={{margin: '2px'}} className="fa fa-plus"></span></button>
							{parentdataset ? <button type='button' className="btn btn-sm gs-form-btn-danger marginleft-3" onClick={() => this.deleteResource(parentdataset, index, parentdatasetStr)}><span style={{margin: '2px'}} className="fa fa-trash"></span></button> : null}
						</div>
					</div>
					<div className="gs-dataset-columnbox">
						<div className="gs-anchor" onClick={() => this.joinResource(dataset, datasetStr, 'columns')}>
							{(dataset.columns || []).length > 0 ? <div>
								<label className="badge gs-badge-primary marginright-10">
									{(dataset.columns || []).length}
								</label>
								Columns Selected
							</div> : 'Choose columns'}
						</div>
					</div>
				</div>
				{childrenArray.length > 0 ? <div className="gs-dataset-child-seperator">

				</div> : null}
				<div style={{display: 'flex', flexDirection: 'column'}}>
					{childrenArray.map((secdataset, secindex) => {
						return this.renderDataSet(`${datasetStr}.joinedresources[${secindex}]`, secdataset, secindex, dataset, childrenArray.length, datasetStr);
					})}
				</div>
			</div>
		);
	}

	renderFilter(filter, filterstr, columnObj) {

		if(!filter.field || !columnObj[filter.field])
			return null;

		let fieldObj = columnObj[filter.field].fieldObj;
		let renderType = null;
		let tempvalidate = [requiredNewValidation({
			required: true,
			title : `Select.....`
		})];

		let fieldProps = {
			required: true,
			placeholder: `Select...`
		};

		if (fieldObj.type == 'string' && fieldObj.group == 'localselect') {
			renderType = ReportLocalSelectEle;
			fieldProps.options = fieldObj.localOptions;
		}

		if (fieldObj.type == 'string' && fieldObj.group == 'resource') {
			renderType = ReportLocalSelectEle;
			fieldProps.options = this.state.resourceOptionsArray;
		}

		if (fieldObj.type == 'boolean') {
			renderType = ReportBooleanEle;
		}

		if (fieldObj.type == 'date') {
			renderType = ReportDateEle;
		}

		if (fieldObj.type == 'integer' && !fieldObj.isForeignKey) {
			renderType = ReportNumberEle;
		}

		if (fieldObj.isForeignKey) {
			renderType = ReportAutoSelectEle;
			fieldProps.resource = fieldObj.foreignKeyOptions.resource;
			fieldProps.fields = `id,${fieldObj.foreignKeyOptions.mainField.split('.')[1]}`;
			fieldProps.label = `${fieldObj.foreignKeyOptions.mainField.split('.')[1]}`;

			if (fieldObj.foreignKeyOptions.resource == 'numberingseriesmaster' && fieldObj.numberingseries_resource)
				fieldProps.filter = `numberingseriesmaster.resource = '${fieldObj.numberingseries_resource}'`;
		}

		if (fieldObj.isArrayForeignKey) {
			renderType = ReportAutoMultiSelectEle;
			fieldProps.resource = fieldObj.foreignKeyArrayOptions.resource;
			fieldProps.fields = `id,${fieldObj.foreignKeyArrayOptions.mainField}`;
			fieldProps.label = `${fieldObj.foreignKeyArrayOptions.mainField}`;
		}

		return (
			<Field
				name = {`${filterstr}.value`}
				props = {fieldProps}
				component = {renderType}
				validate = {tempvalidate}
			/>
		);

		return null;
	}

	getColumns(columnArray, dataset, datasetStr, prefixstr, prefixdisplaystr) {
		let prefixstring = prefixstr ? `${prefixstr}_` : ``;
		let prefixdisplaystring = prefixdisplaystr ? `${prefixdisplaystr}` : ``;
		let resObj = this.state.myResources[dataset.resource];

		(dataset.columns || []).forEach((fie, fieIndex) => {
			columnArray.push({
				datasetStr: `${datasetStr}.columns[${fieIndex}]`,
				fieldName: `${prefixstring}${fie.field}`,
				displayName: `${resObj.fields[fie.field].displayName}${prefixdisplaystring ? (' (' + prefixdisplaystring +')') : ''}`,
				aliasName: fie.aliasName,
				fieldObj: resObj.fields[fie.field]
			});
		});

		(dataset.joinedresources || []).forEach((joidataset, joidataindex) => {
			if(joidataset.type == 'related')
				this.getColumns(columnArray, joidataset, `${datasetStr}.joinedresources[${joidataindex}]`, `${prefixstring}${joidataset.field}`, `${prefixdisplaystring ? (prefixdisplaystring + ' > ') : ''}${resObj.fields[joidataset.field].displayName}`);
			else if(joidataset.type == 'child') {
				let joiresObj = this.state.myResources[joidataset.resource];
				this.getColumns(columnArray, joidataset, `${datasetStr}.joinedresources[${joidataindex}]`, `${prefixstring}${joidataset.resource}_${joidataset.field}`, `${prefixdisplaystring ? (prefixdisplaystring + ' > ') : ''}${joiresObj.displayName}(${joiresObj.fields[joidataset.field].displayName})`);
			}
		});
	}

	renderCreate() {
		let resourceProps = {
			options: this.state.resourceArray,
			label: 'displayName',
			valuename: 'resourceName'
		};

		let datasetProps = {
			options: this.state.dataSetArray,
			label: 'name',
			multiselect: true,
			valuename: 'id'
		};

		return (
			<div>
				<div className="row">
					<div className="col-sm-12 col-md-5" style={{marginTop: '100px', marginLeft: '30%'}}>
						<div className="card marginbottom-15 borderradius-0" style={{boxShadow: '0 0px 4px 0 rgba(0, 0, 0, 0.25)'}}>
							<div className="card-body" style={{padding: '2rem'}}>
								<div style={{marginBottom: '25px'}}><h5 className="gs-text-color">New Data Set</h5></div>
								<div className="row responsive-form-element">
									<div className="form-group col-sm-12">
										<label className="labelclass">Name</label>
										<Field name="name" component={InputEle} props={{required: true}} validate={stringNewValidation({required: true})} />
									</div>
									<div className="form-group col-sm-12">
										<label className="labelclass">Base Resource</label>
										<Field name="tempresource" component={localSelectEle} props={resourceProps} validate={stringNewValidation({required: true})} />
									</div>
									<div className="form-group col-sm-12">
										<label className="labelclass">Inject Dataset</label>
										<Field name="datasetids" component={localSelectEle} props={datasetProps} />
									</div>
								</div>
								<div className="col-sm-12 text-center margintop-15">
									<button
										type="button"
										className="btn btn-sm btn-width btn-secondary"
										onClick={() => this.cancel()}>
										<i className="fa fa-times marginright-5"></i>Close
									</button>
									{checkActionVerbAccess(this.props.app, 'datasets', 'Save') ? <button
										type="button"
										className="btn btn-sm gs-btn-success btn-width"
										disabled={this.props.invalid}
										onClick={() => this.create()}>
										<i className="fa fa-save"></i>Save
									</button> : null}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	getPromptMessage(location) {
		return `Unsaved Changes`;
	}

	createReport () {
		this.props.history.push({pathname: '/createReport', params: {...this.props.resource, param: 'DataSet'}});
	}

	listReport () {
		this.props.openModal({
			render: (closeModal) => {
				return <ReportListModal
					openModal = {this.props.openModal}
					form = {this.props.form}
					history = {this.props.history}
					resource = {this.props.resource}
					app = {this.props.app}
					updateFormState = {this.props.updateFormState}
					closeModal = {closeModal}
				/>
			}, className: {
				content: 'react-modal-custom-class',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	addMathParam (column, columnindex) {
		let { mathExpParams } = this.state,
			tempColumns = {...column};

		if (tempColumns.mathexpression_count+1 > 26)
			this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				body : 'You have to select only 26 params',
				btnArray : ['Ok']
			}));
		else {
			tempColumns.mathexpression_param[mathExpParams[tempColumns.mathexpression_count]] = null;
			tempColumns.mathexpression_count++;

			this.props.updateFormState(this.props.form, {
				[`config.configurablecolumns[${columnindex}]`]: tempColumns
			});
		}
	};

	deleteMathParam (column, columnindex) {
		let { mathExpParams } = this.state,
			tempColumns = {...column};

		if (tempColumns.mathexpression_count-1 >= 0) {
			delete tempColumns.mathexpression_param[mathExpParams[tempColumns.mathexpression_count-1]];
			tempColumns.mathexpression_count--;

			this.props.updateFormState(this.props.form, {
				[`config.configurablecolumns[${columnindex}]`]: tempColumns
			});
		}
	};

	renderConfigurableColumnItem(column, columnindex, columnArray) {
		let calculationtypeProps = {
			options: [...calculationTypeArr],
			onChange: () => {
				let tempObj = {
					[`config.configurablecolumns[${columnindex}].coalesce_fields`]: null,
					[`config.configurablecolumns[${columnindex}].coalesce_defaultvalue`]: null,
					[`config.configurablecolumns[${columnindex}].dateformatter_field`]: null,
					[`config.configurablecolumns[${columnindex}].dateformatter_formatter`]: null,
					[`config.configurablecolumns[${columnindex}].range_basedon`]: null,
					[`config.configurablecolumns[${columnindex}].range_max`]: null,
					[`config.configurablecolumns[${columnindex}].range_min`]: null,
					[`config.configurablecolumns[${columnindex}].range_value`]: null,
					[`config.configurablecolumns[${columnindex}].mathexpression_expression`]: null,
					[`config.configurablecolumns[${columnindex}].mathexpression_param`]: {},
					[`config.configurablecolumns[${columnindex}].mathexpression_count`]: 0,
					[`config.configurablecolumns[${columnindex}].fixedvalue_value`]: null,
					[`config.configurablecolumns[${columnindex}].datedifference_field1type`]: null,
					[`config.configurablecolumns[${columnindex}].datedifference_field1`]: null,
					[`config.configurablecolumns[${columnindex}].datedifference_field2type`]: null,
					[`config.configurablecolumns[${columnindex}].datedifference_field2`]: null
				};
				this.props.updateFormState(this.props.form, tempObj);
			},
			required: true
		};
		let columnArrayProps = {
			options: columnArray,
			label: 'displayName',
			valuename: 'fieldName',
			required: true,
			multiselect: true
		};
		let columnArraySingleProps = {
			options: [...columnArray, ...(this.props.resource.config.configurablecolumns.map(column => {return {fieldName: column.field, displayName: column.name ? column.name : column.field};}))],
			label: 'displayName',
			valuename: 'fieldName',
			required: true	
		};
		let dateformatterProps = {
			options: [...dateFormatterTypeArr],
			required: true
		};

		let aggregationnumberprops = {
			options: [{name: 'Count', id: 'count'}, {name: 'Sum', id: 'sum'}, {name: 'Min', id: 'min'}, {name: 'Max', id: 'max'}, {name: 'Avg', id: 'avg'}, {name: 'On Aggregate', id: 'onAggregate'}],
			label: 'name',
			valuename: 'id',
			required: true
		};

		return (
			<td>
				<div className="row">
					<div className="col-md-12">
						<div className="row">
							<div className="form-group col-md-6">
								<label># {columnindex + 1}</label>
							</div>
							<div className="form-group col-md-6">
								<button type="button" onClick={() => this.deleteColumn(columnindex)} rel="tooltip" title="Delete Column" className="btn btn-sm gs-form-btn-danger pull-right marginleft-3"> <span className="fa fa-trash-o" ></span></button>
							</div>
						</div>
					</div>
					<div className="col-md-12">
						<div className="row">
							<div className="form-group col-md-6">
								<label className="labelclass">Column ID</label>
								<Field name={`config.configurablecolumns[${columnindex}].field`} props={{required: true, disabled: true}} component={InputEle} validate={stringNewValidation({required: true})} />
							</div>
							<div className="form-group col-md-6">
								<label className="labelclass">Column Name</label>
								<Field name={`config.configurablecolumns[${columnindex}].name`} props={{required: true}} component={InputEle} validate={stringNewValidation({required: true})} />
							</div>
						</div>
					</div>
					<div className="col-md-12">
						<div className="row">
							<div className="form-group col-md-6">
								<label className="labelclass">Calculation Type</label>
								<Field name={`config.configurablecolumns[${columnindex}].calculationtype`} props={calculationtypeProps} component={localSelectEle} validate={stringNewValidation({required: true})} />
							</div>
							{column.calculationtype == 'coalesce' ? <div className="form-group col-md-6">
								<label className="labelclass">Columns</label>
								<Field name={`config.configurablecolumns[${columnindex}].coalesce_fields`} props={columnArrayProps} component={localSelectEle} validate={multiSelectNewValidation({required: true})} />
							</div> : null}
							{column.calculationtype == 'dateformatter' ? <div className="form-group col-md-6">
								<label className="labelclass">Column</label>
								<Field name={`config.configurablecolumns[${columnindex}].dateformatter_field`} props={columnArraySingleProps} component={localSelectEle} validate={stringNewValidation({required: true})} />
							</div> : null}
							{this.renderCoalesceDefaultValue(column, columnindex)}
							{column.calculationtype == 'dateformatter' ? <div className="form-group col-md-6">
								<label className="labelclass">Date Formatter</label>
								<Field name={`config.configurablecolumns[${columnindex}].dateformatter_formatter`} props={dateformatterProps} component={localSelectEle} validate={stringNewValidation({required: true})} />
							</div> : null}

							{column.calculationtype == 'range' ? <div className="form-group col-md-6">
								<label className="labelclass">Based On</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].range_basedon`}
									props = {columnArraySingleProps}
									component = {localSelectEle}
									validate = {stringNewValidation({
										required: true
									})}
								/>
							</div> : null}
							{column.calculationtype == 'range' ? <div className="form-group col-md-6">
								<label className="labelclass">Min</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].range_min`}
									props = {{
										required: false
									}}
									component = {NumberEle}
									validate = {numberNewValidation({
										required: false
									})}
								/>
							</div> : null}
							{column.calculationtype == 'range' ? <div className="form-group col-md-6">
								<label className="labelclass">Max</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].range_max`}
									props = {{
										required: false
									}}
									component = {NumberEle}
									validate = {numberNewValidation({
										required: false
									})}
								/>
							</div> : null}
							{column.calculationtype == 'range' ? <div className="form-group col-md-6">
								<label className="labelclass">Value Column</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].range_value`}
									props = {columnArraySingleProps}
									component = {localSelectEle}
									validate = {stringNewValidation({
										required: true
									})}
								/>
							</div> : null}
							{column.calculationtype == 'mathexpression' ? <div className="form-group col-md-6 hide">
								<label className="labelclass">Expression Count</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].mathexpression_count`}
									props = {{
										disabled: true,
										hide: true
									}}
									component = {NumberEle}
								/>
							</div> : null}
							{column.calculationtype == 'mathexpression' ? this.renderMathExpParams(column, columnindex, columnArray, columnArraySingleProps) : null}
							{column.calculationtype == 'mathexpression' ? <div className="form-group col-md-6">
								<button
									type = 'button'
									onClick = {() => this.addMathParam(column, columnindex)}
									rel = 'tooltip'
									title = 'Add parameter'
									className = 'btn btn-width btn-sm gs-btn-primary'>
										<i className = 'fa fa-plus'></i>Add parameter
								</button>
							</div> : null}
							{column.calculationtype == 'mathexpression' ? <div className="form-group col-md-6">
								<label className="labelclass">Expression</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].mathexpression_expression`}
									props = {{
										required: true
									}}
									component={InputEle}
									validate = {stringNewValidation({
										required: true
									})}
								/>
							</div> : null}
							{column.calculationtype == 'fixedvalue' ? <div className="form-group col-md-6">
								<label className="labelclass">Show</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].fixedvalue_value`}
									props = {{
										required: true
									}}
									component={InputEle}
									validate = {stringNewValidation({
										required: true
									})}
								/>
							</div> : null}
							{column.calculationtype == 'datedifference' ? <div className="form-group col-md-6">
								<label className="labelclass">Field Type 1</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].datedifference_field1type`}
									props = {{
										options: [{value: "today", label: "Today"}, {value: "custom", label: "Custom"}],
										label: "label",
										valuename: "value",
										onChange: () => {
											this.props.updateFormState(this.props.form, {
												[`config.configurablecolumns[${columnindex}].datedifference_field1`]: null
											});
										},
										required: true
									}}
									component = {localSelectEle}
									validate = {stringNewValidation({
										required: true
									})}
								/>
							</div> : null}
							{column.calculationtype == 'datedifference' && column.datedifference_field1type == 'custom' ? <div className="form-group col-md-6">
								<label className="labelclass">Field 1</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].datedifference_field1`}
									props = {columnArraySingleProps}
									component = {localSelectEle}
									validate = {stringNewValidation({
										required: true
									})}
								/>
							</div> : null}
							{column.calculationtype == 'datedifference' ? <div className="form-group col-md-6">
								<label className="labelclass">Field Type 2</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].datedifference_field2type`}
									props = {{
										options: [{value: "today", label: "Today"}, {value: "custom", label: "Custom"}],
										label: "label",
										valuename: "value",
										onChange: () => {
											this.props.updateFormState(this.props.form, {
												[`config.configurablecolumns[${columnindex}].datedifference_field2`]: null
											});
										},
										required: true
									}}
									component = {localSelectEle}
									validate = {stringNewValidation({
										required: true
									})}
								/>
							</div> : null}
							{column.calculationtype == 'datedifference' && column.datedifference_field2type == 'custom' ? <div className="form-group col-md-6">
								<label className="labelclass">Field 2</label>
								<Field
									name = {`config.configurablecolumns[${columnindex}].datedifference_field2`}
									props = {columnArraySingleProps}
									component = {localSelectEle}
									validate = {stringNewValidation({
										required: true
									})}
								/>
							</div> : null}
							{this.props.resource.config.isgroupby && (!this.props.resource.config.groupby || this.props.resource.config.groupby.indexOf(column.field) == -1) ? <div className="form-group col-md-6">
								<label className="labelclass">Aggregation Type</label>
								<Field
									name={`config.configurablecolumns[${columnindex}].aggregationType`}
									component={localSelectEle}
									props={aggregationnumberprops}
									validate={stringNewValidation({required: true})} />
							</div> : null}
						</div>
					</div>
				</div>
			</td>
		);
	}

	renderCoalesceDefaultValue(column, columnindex) {
		if(column.calculationtype != 'coalesce')
			return null;

		return (
			<div className="form-group col-md-6">
				<label className="labelclass">Default Value</label>
				<Field name={`config.configurablecolumns[${columnindex}].coalesce_defaultvalue`} props={{}} component={InputEle} />
			</div>
		);

		if(column.type == 'reference')
			return null;

		return (
			<div className="form-group col-md-6">
				<label className="labelclass">Default Value</label>
				{column.type == 'string' ? <Field name={`config.configurablecolumns[${columnindex}].coalesce_defaultvalue`} props={defaultValueProps} component={InputEle} validate={stringNewValidation({required: true})} /> : null}
				{column.type == 'integer' ? <Field name={`config.configurablecolumns[${columnindex}].coalesce_defaultvalue`} props={defaultValueProps} component={NumberEle} validate={numberNewValidation({required: true})} /> : null}
				{column.type == 'date' ? <Field name={`config.configurablecolumns[${columnindex}].coalesce_defaultvalue`} props={defaultValueProps} component={DateEle} validate={dateNewValidation({required: true})} /> : null}
				{column.type == 'datetime' ? <Field name={`config.configurablecolumns[${columnindex}].coalesce_defaultvalue`} props={defaultValueProps} component={dateTimeEle} validate={dateNewValidation({required: true})} /> : null}
			</div>
		);
	}

	renderMathExpParams (column, columnindex, columnArray, columnArraySingleProps) {
		let { mathExpParams } = this.state,
			paramLength = column.mathexpression_count,
			expressionArray = [];

		if (column.mathexpression_count == 0)
			return null;

		for (let i = 0; i < column.mathexpression_count; i++) {
			expressionArray.push({
				prop: mathExpParams[i],
				value:`config.configurablecolumns[${columnindex}].mathexpression_param[${mathExpParams[i]}]`
			});
		}

		return expressionArray.map((mathParam, mathIndex) => {
			return (
				<div key = {mathIndex} className = 'form-group col-md-6'>
					<label className="labelclass">Parameter - {mathParam.prop}</label>
					<div className = 'row'>
						<div className = {`padding-0 ${(mathIndex ==  paramLength - 1) ? 'col-md-11 input-group' : 'col-md-12'}`}>
							<div style={{flex: '1'}}>
							<Field
								name = {`${mathParam.value}`}
								props = {columnArraySingleProps}
								component = {localSelectEle}
								validate = {stringNewValidation({
									required: true
								})}
							/>
							</div>
						</div>
						{(mathIndex ==  paramLength - 1) ? <span className = 'input-group-append'>
							<div
								onClick = {() => this.deleteMathParam(column, columnindex)}
								rel = 'tooltip'
								title = 'Delete Parameter'
								className = 'btn btn-sm gs-form-btn-danger disabled ndtclass'>
								<span style = {{verticalAlign: 'middle'}}>x</span>
							</div>
						</span> : null}
					</div>
				</div>
			);
		});
	};

	render() {
		let { resource } = this.props;
		if(!resource)
			return null;

		if(this.state.createParam) {
			return (
				<div className="col-md-12">
					<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
					{this.renderCreate()}
				</div>
			);
		}

		let columnArray = [];
		if(resource.config.dataSetArray[0].resource)
			this.getColumns(columnArray, resource.config.dataSetArray[0], 'config.dataSetArray[0]');

		let columnObj = {};
		columnArray.forEach((col) => {
			columnObj[col.fieldName] = col;
		});

		let filterArray = (resource.config.filters || []);
		let configurablecolumns = (resource.config.configurablecolumns || []);

		let filterFieldsArray = [];
		columnArray.forEach((col) => {
			if(col.fieldObj.type == 'string' && col.fieldObj.group == 'localselect')
				filterFieldsArray.push(col);

			if(col.fieldObj.type == 'string' && col.fieldObj.group == 'resource')
				filterFieldsArray.push(col);

			if(col.fieldObj.type == 'boolean' || col.fieldObj.type == 'date' || col.fieldObj.type == 'integer')
				filterFieldsArray.push(col);

			if(col.fieldObj.isArrayForeignKey)
				filterFieldsArray.push(col);
		});

		let groupbyprops = {
			options: [...columnArray, ...(configurablecolumns.map(column => {return {fieldName: column.field, displayName: column.name ? column.name : column.field};}))],
			label: 'displayName',
			valuename: 'fieldName',
			required: true,
			multiselect: true
		};

		let isgroupbyprops = {
			required: true,
			onChange: (value) => this.groupByOnChange()
		};

		let aggregationnumberprops = {
			options: [{name: 'Count', id: 'count'}, {name: 'Sum', id: 'sum'}, {name: 'Min', id: 'min'}, {name: 'Max', id: 'max'}, {name: 'Avg', id: 'avg'}, {name: 'Not Applicable', id: 'notApplicable'}],
			label: 'name',
			valuename: 'id',
			required: true
		};
		let aggregationdateprops = {
			options: [{name: 'Count', id: 'count'}, {name: 'Min', id: 'min'}, {name: 'Max', id: 'max'}, {name: 'Not Applicable', id: 'notApplicable'}],
			label: 'name',
			valuename: 'id',
			required: true
		};

		let resourceProps = {
			options: this.state.resourceArray,
			label: 'displayName',
			valuename: 'resourceName',
			required: true
		};

		let datasetProps = {
			options: this.state.dataSetArray,
			label: 'name',
			multiselect: true,
			disabled: true,
			valuename: 'id'
		};

		let renderAggTypeField = (column) => {
			if(!resource.config.groupby || resource.config.groupby.indexOf(column.fieldName) == -1)
				return <Field name={`${column.datasetStr}.aggregationType`} component={localSelectEle} props={['date', 'datetime'].includes(column.fieldObj.type) ? aggregationdateprops : aggregationnumberprops} validate={stringNewValidation({required: true})} />

			return null;
		}

		let fixed_style_body = {};
		if($(window).outerWidth() > 767) {
			fixed_style_body = {
				marginTop: `${this.props.isModal ? '0' : ((document.getElementsByClassName('affixmenubar').length > 0 ? document.getElementsByClassName('affixmenubar')[0].clientHeight : 0) + 3)}px`,
				marginBottom: `${this.props.isModal ? '0' : (document.getElementsByClassName('actionbtn-bar').length > 0 ? (document.getElementsByClassName('actionbtn-bar')[0].clientHeight - 40) : 40)}px`
			}
		}

		let renderFilterTable = () => {
			return (
				<table className="table">
					<thead>
						<tr>
							<th style={{width: '40%'}}>Field</th>
							<th style={{width: '50%'}}>Value</th>
							<th style={{width: '10%'}}></th>
						</tr>
					</thead>
					<tbody>
						{filterArray.map((filter, filterindex) => {
							let filterprops = {
								options: filterFieldsArray,
								label: 'displayName',
								valuename: 'fieldName',
								onChange: (val) => {
									let fieldValue = null,
										fieldObj = columnObj[val].fieldObj;

									if (fieldObj.isForeignKey && fieldObj.foreignKeyOptions.resource == 'companymaster')
										fieldValue = 'Current Company';

									this.props.updateFormState(this.props.form, {
										[`config.filters[${filterindex}].value`] : fieldValue
									});
								},
								required: true
							};
							return (
								<tr key={filterindex}>
									<td>
										<Field name={`config.filters[${filterindex}].field`} props={filterprops} component={localSelectEle} validate={stringNewValidation({required: true})} />
									</td>
									<td>
										{this.renderFilter(filter, `config.filters[${filterindex}]`, columnObj)}
									</td>
									<td>
										<button type="button" onClick={() => this.deleteFilter(filterindex)} rel="tooltip" title="Delete Filter" className="btn btn-sm gs-form-btn-danger marginleft-3"> <span className="fa fa-trash-o" ></span></button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			);
		}
		let renderConfigurableColumns = () => {
			return (
				<table className="table table-bordered">
					<tbody>
						{configurablecolumns.map((column, columnindex) => {
							return (
								<tr key={columnindex}>
									{this.renderConfigurableColumnItem(column, columnindex, columnArray)}
								</tr>
							);
						})}
					</tbody>
				</table>
			);
		}
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<Prompt when={(this.props.dirty && this.props.anyTouched) || this.state.localTocuhed} message={() => this.getPromptMessage()} />
					<div className="row">
						<div className="col-sm-12 affixmenubar col-md-12 col-lg-9 d-sm-block d-md-flex justify-content-md-between  align-items-center">
							<div className="semi-bold-custom font-16">
								<a className="affixanchor float-left marginright-10" onClick={() => this.props.history.push('/list/datasets')}><span className="fa fa-arrow-left "></span></a>
								<div className="gs-uppercase float-left">Data Set - {resource.identifier} - {resource.name}</div>
							</div>
							<div className="d-block">
								<div className="btn btn-sm affixmenu-btn" onClick={() => this.exportfunction()}><i className="fa fa-download"></i>Export</div>
								<div className="btn btn-sm affixmenu-btn" onClick={() => this.copyfunction()}><i className="fa fa-files-o"></i>Copy</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-12 col-md-12 col-lg-9">
							<div className="row"  style={fixed_style_body}>
								<div className="col-md-12" style={{marginBottom:'50px'}}>
									<div className="gs-dataset-container">
										<div className="gs-dataset-inner-container">
											{resource.config.dataSetArray ? resource.config.dataSetArray.map((dataset, index) => this.renderDataSet(`config.dataSetArray[${index}]`, dataset, index, null, 1)) : null}
										</div>
									</div>
								</div>
								<div className="col-md-12 col-sm-12">
									<div className="card marginbottom-15 borderradius-0 gs-card">
										<div className="card-header borderradius-0 card-header-custom gs-card-header">Config Details</div>
										<div className="card-body">
											<div className="row responsive-form-element">
												<div className="form-group col-md-6">
													<label className="labelclass">Name</label>
													<Field name="name" component={InputEle} props={{required: true}} validate={stringNewValidation({required: true})} />
												</div>
												<div className="form-group col-md-6">
													<label className="labelclass">
														Inject Dataset
														<button type="button" onClick={() => this.changeInjectDataset()} rel="tooltip" title="Edit Inject Dataset" className="btn btn-sm gs-btn-default gs-form-btn-default marginleft-3"> <span className="fa fa-edit" ></span></button>
													</label>
													<Field name="datasetids" component={localSelectEle} props={datasetProps} />
												</div>
												<div className="form-group col-md-6">
													<label className="labelclass">Is Group By</label>
													<Field name="config.isgroupby" component={checkboxEle} props={isgroupbyprops} validate={stringNewValidation({required: true})} />
												</div>
												{resource.config.isgroupby ? <div className="form-group col-md-6">
													<label className="labelclass">Group By</label>
													<Field name="config.groupby" props={groupbyprops} component={localSelectEle} validate={multiSelectNewValidation({required: true})} />
												</div> : null}
											</div>
										</div>
									</div>
								</div>
								<div className="col-md-12 col-sm-12">
									<div className="card marginbottom-15 borderradius-0 gs-card">
										<div className="card-header borderradius-0 card-header-custom gs-card-header">Configurable Columns</div>
										<div className="card-body">
											<div className="row responsive-form-element">
												<div className="form-group col-md-12">
													{renderConfigurableColumns()}
												</div>
												<div className="form-group col-md-12 col-sm-12 col-xs-12">
													<button type="button" onClick={() => this.addColumn()} rel="tooltip" title="Add Column"  className="btn btn-sm btn-gs btn-outline-primary filter-button"> <span className="fa fa-plus" ></span> Add Column</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className="col-md-12 col-sm-12">
									<div className="card marginbottom-15 borderradius-0 gs-card">
										<div className="card-header borderradius-0 card-header-custom gs-card-header">Filter Details</div>
										<div className="card-body">
											<div className="row responsive-form-element">
												<div className="form-group col-md-12">
													{renderFilterTable()}
												</div>
												<div className="form-group col-md-12 col-sm-12 col-xs-12">
													<button type="button" onClick={() => this.addFilter()} rel="tooltip" title="Add Filter"  className="btn btn-sm btn-gs btn-outline-primary filter-button"> <span className="fa fa-plus" ></span> Add Filter</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className="col-md-12 col-sm-12">
									<div className="card marginbottom-15 borderradius-0 gs-card">
										<div className="card-header borderradius-0 card-header-custom gs-card-header">Column Details</div>
										<div className="card-body" style = {{overflowY: 'auto', maxHeight: '400px'}}>
											<div className="row responsive-form-element">
												<div className="form-group col-md-12">
													<table className="table">
														<thead>
															<tr>
																<th>Name</th>
																<th style = {{width: '30%'}}>Alias Name</th>
																{resource.config.isgroupby ? <th style = {{width: '30%'}}>Aggregate type</th> : null}
															</tr>
														</thead>
														<tbody>
															{columnArray.map((column, columnindex) => {
																return (
																	<tr key={columnindex}>
																		<td>{column.displayName}</td>
																		<td>
																			<Field name={`${column.datasetStr}.aliasName`} component={InputEle} />
																		</td>
																		{resource.config.isgroupby ? <td>{renderAggTypeField(column)}</td> : null}
																	</tr>
																);
															})}
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="col-sm-12 ndt-section col-md-12 col-lg-3 paddingleft-md-0 margintop-5">
							{this.state.ndt ? <AdditionalInformationSection updateAppState = {this.props.updateAppState} ndt={this.state.ndt} key={0} parentresource={'datasets'} openModal={this.props.openModal} parentid={this.props.resource.id} relatedpath={this.props.match.path} createOrEdit = {this.props.createOrEdit} app ={this.props.app} history={this.props.history} /> : null }
						</div>
					</div>
					<div className="row" style={{paddingTop: '40px'}}>
						<div className="col-md-12 col-lg-9">
							<div className="muted credit text-center sticky-footer actionbtn-bar">
								<div style={{width:'80%', float: 'left'}}>
									<button
										type="button"
										className="btn btn-sm btn-width btn-secondary"
										onClick={() => this.cancel()}>
										<i className="fa fa-times marginright-5"></i>Close
									</button>
									{checkActionVerbAccess(this.props.app, 'datasets', 'Save') ?<button
										type="button"
										className="btn btn-sm gs-btn-success btn-width"
										disabled={this.props.invalid}
										onClick={() => this.validateDataset('Save')}>
										<i className="fa fa-save"></i>Save
									</button> : null}
									{ checkActionVerbAccess(this.props.app, 'reports', 'Save') ? <button
										type="button"
										onClick={this.createReport}
										className="btn btn-width btn-sm gs-btn-success"
										disabled={this.props.invalid}>
										<i className="fa fa-plus"></i>Reports
									</button> : null }
									{ checkActionVerbAccess(this.props.app, 'reports', 'Read') ? <button
										type="button"
										onClick={this.listReport}
										className="btn btn-width btn-sm gs-btn-primary"
										disabled={this.props.invalid}>
										<i className="fa fa-list"></i>Created Reports
									</button> : null }
								</div>
								{checkActionVerbAccess(this.props.app, 'datasets', 'Delete') ? <div className="btn-group dropup" style={{float:'left', width: '20%'}}>
									<button type="button" className="btn btn-sm btn-width gs-btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More <span className="sr-only">Toggle Dropdown</span>
									</button>
									<div className="dropdown-menu gs-morebtn-dropdown-menu">
										<a className={`dropdown-item gs-text-danger`} onClick={() => this.save('Delete')}><i className={`fa fa-trash-o`}></i> Delete</a>
									</div>
								</div> : null}
							</div>
						</div>
					</div>
				</form>
			</div>
		);
	}
}

class DataSetEdit extends Component {
	constructor(props) {
		super(props);

		let relatedresources = [], columns = [], childresources = [];

		(this.props.dataset.joinedresources || []).forEach((joires) => {
			if(joires.type == 'related')
				relatedresources.push(`${joires.field}-${joires.resource}`);
			if(joires.type == 'child')
				childresources.push(`${joires.resource}-${joires.field}`);
		});

		(this.props.dataset.columns || []).forEach((colfie) => {
			columns.push(colfie.field);
		});

		this.state = {
			dataset: this.props.dataset,
			checkall: false,
			relatedresources: relatedresources,
			childresources: childresources,
			columns: columns
		};

		this.datasetOnChange = this.datasetOnChange.bind(this);
		this.save = this.save.bind(this);
	}

	componentWillMount() {
		let resResource = this.state.dataset.resource;
		let resourceObj = this.props.myResources[resResource];
		let joinAvailableResourceArray = [];
		let childAvailableResourceArray = [];
		let availableColumnsArray = [];

		for(var prop in resourceObj.fields) {
			let fieldObj = resourceObj.fields[prop];
			if(fieldObj.isForeignKey && fieldObj.foreignKeyOptions) {
				let foreignResource = this.props.myResources[fieldObj.foreignKeyOptions.resource];
				if(!foreignResource.hideInReports)
					joinAvailableResourceArray.push({
						type: 'joinresources',
						field: prop,
						resource: `${prop}-${fieldObj.foreignKeyOptions.resource}`,
						displayName: `${fieldObj.displayName}-${foreignResource.displayName}`
					});
			}
			fieldObj.fieldName = prop;
			availableColumnsArray.push(fieldObj);
		}

		for(var prop in this.props.myResources) {
			if(!this.props.myResources[prop].fields || this.props.myResources[prop].hideInReports)
				continue;

			for (var secprop in this.props.myResources[prop].fields) {
				let secFieObj = this.props.myResources[prop].fields[secprop];
				if(secFieObj.isForeignKey && secFieObj.foreignKeyOptions && secFieObj.foreignKeyOptions.resource == resResource) {
					childAvailableResourceArray.push({
						type: 'childresources',
						field: secprop,
						resource: `${prop}-${secprop}`,
						displayName: `${this.props.myResources[prop].displayName}-${secFieObj.displayName}`
					});
				}
			}
		}

		this.setState({ resourceObj, joinAvailableResourceArray, availableColumnsArray, childAvailableResourceArray });
	}

	datasetOnChange(prop, value) {
		this.setState({
			[prop]: value
		});
	}

	checkBoxOnchange(param, value) {
		let columns = [...this.state.columns], checkall = this.state.checkall;
		if(param == '<checkall>') {
			columns = [];
			checkall = value;
			if(value)
				columns = this.state.availableColumnsArray.map(column => column.fieldName);
		} else {
			if(value && columns.indexOf(param) == -1)
				columns.push(param);

			if(!value)
				columns.splice(columns.indexOf(param), 1);
		}
		this.setState({ columns, checkall });
	}

	save() {
		let joinResourceArray = (this.state.dataset.joinedresources || []);
		let joinNewResourceArray = [];
		for (var i = 0; i < this.state.relatedresources.length; i++) {
			let itemFound = false;
			let splitArray = this.state.relatedresources[i].split('-');
			let relresource = splitArray.length === 3 ? `${splitArray[1]}-${splitArray[2]}` : splitArray[1];
			let relfield = splitArray[0];
			for (var j = 0; j < joinResourceArray.length; j++) {
				if (joinResourceArray[j].type == 'related' && relfield == joinResourceArray[j].field && relresource == joinResourceArray[j].resource) {
					itemFound = true;
					joinNewResourceArray.push(joinResourceArray[j]);
					break;
				}
			}
			if (!itemFound) {
				let tempJoinResObj = {
					resource: relresource,
					field: relfield,
					type: 'related'
				};
				if(this.props.datasetStr == 'config.dataSetArray[0]') {
					let resResource = this.state.dataset.resource;
					let resourceObj = this.props.myResources[resResource];
					if(resourceObj.fields[relfield].isParentResource) {
						if(this.props.myResources[relresource].fields.companyid) {
							tempJoinResObj.columns=[{
								field: 'companyid'
							}];
						}
					}
				}
				joinNewResourceArray.push(tempJoinResObj);
			}
		}

		for (var i = 0; i < this.state.childresources.length; i++) {
			let itemFound = false;
			let splitArray = this.state.childresources[i].split('-');
			let relresource = splitArray.length === 3 ? `${splitArray[0]}-${splitArray[1]}` : splitArray[0];
			let relfield = splitArray.length === 3 ? splitArray[2] : splitArray[1];
			for (var j = 0; j < joinResourceArray.length; j++) {
				if (joinResourceArray[j].type == 'child' && relresource == joinResourceArray[j].resource && relfield == joinResourceArray[j].field) {
					itemFound = true;
					joinNewResourceArray.push(joinResourceArray[j]);
					break;
				}
			}
			if (!itemFound)
				joinNewResourceArray.push({
					resource: relresource,
					field: relfield,
					type: 'child'
				});
		}

		let columnArray = (this.state.dataset.columns || []);
		let columnNewArray = [];
		for (var i = 0; i < this.state.columns.length; i++) {
			let itemFound = false;
			for (var j = 0; j < columnArray.length; j++) {
				if (this.state.columns[i] == columnArray[j].field) {
					itemFound = true;
					columnNewArray.push(columnArray[j]);
					break;
				}
			}
			if (!itemFound)
				columnNewArray.push({
					field: this.state.columns[i]
				});
		}

		this.props.updateFormState(this.props.form, {
			[`${this.props.datasetStr}.joinedresources`]: joinNewResourceArray,
			[`${this.props.datasetStr}.columns`]: columnNewArray
		});

		this.props.callback();
		this.props.closeModal();
	}

	renderColumns() {
		let divArray = [<div key="all" className="form-group col-md-12 col-sm-12">
				<label><input type="checkbox" className="form-check-input" checked={this.state.checkall ? true : false} onChange={(evt) => this.checkBoxOnchange('<checkall>', evt.target.checked)} />Select all columns</label>
			</div>];

		this.state.availableColumnsArray.forEach((column, columnindex) => {
			divArray.push(<div key={columnindex} className="form-group col-md-4 col-sm-6">
				<label className="gs-dataset-columnlabel" rel="tooltip" title={column.displayName}><input type="checkbox" className="form-check-input" checked={this.state.columns.includes(column.fieldName)} onChange={(evt) => this.checkBoxOnchange(column.fieldName, evt.target.checked)} />{column.displayName}</label>
			</div>);
		});

		return divArray;
	}

	renderJoinResource() {
		let { joinAvailableResourceArray, relatedresources, childAvailableResourceArray, childresources } = this.state;

		let divArray = [];
		divArray.push(<div key="0" className="form-group col-md-6 col-sm-6">
			<label className="labelclass">Joined Resources</label>
			<LocalSelect label="displayName" valuename="resource" options={joinAvailableResourceArray} value={relatedresources} onChange={(value, valueObj) => this.datasetOnChange('relatedresources', value)} multiselect={true} />
		</div>);
		divArray.push(<div key="1" className="form-group col-md-6 col-sm-6">
			<label className="labelclass">Child Resources</label>
			<LocalSelect label="displayName" valuename="resource" options={childAvailableResourceArray} value={childresources} onChange={(value, valueObj) => this.datasetOnChange('childresources', value)} multiselect={true} />
		</div>);

		return divArray;
	}

	render() {
		if(!this.state.resourceObj)
			return null;

		let { resourceObj, joinAvailableResourceArray, relatedresources, availableColumnsArray, columns, childAvailableResourceArray, childresources } = this.state;
		let { dataset } = this.props;

		return (
			<div>
				{this.props.param == 'columns' ? <div className="react-modal-header  borderbottom-0">
					<h5 className="gs-text-color">Select columns</h5>
				</div> : null}
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						{this.props.param == 'columns' ? this.renderColumns() : this.renderJoinResource()}
					</div>
				</div>
				<div className="react-modal-footer text-center bordertop-0">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type='button' className='btn btn-sm gs-btn-success btn-width' onClick={this.save}><i className='fa fa-times'></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class InjectDatasetEdit extends Component {
	constructor(props) {
		super(props);

		this.state = {
			datasetids: this.props.datasetids || []
		};

	}

	render() {
    		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title text-center" style={{color:'#29b99a',fontWeight:'600'}}>Edit Inject Data Set</h5>
				</div>
				<div className="react-modal-body">
					<div className="row responsive-form-element">
						<div className="form-group col-sm-12 col-md-12">
							<label className="labelclass">Inject Dataset</label>
							<LocalSelect
								options={this.props.dataSetArray}
								value={this.state.datasetids}
								onChange={(datasetids) => this.setState({datasetids})}
								multiselect={true} />
						</div>
 					</div>
				</div>
 				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 text-center">
							<div className="muted credit text-center">
								<button className="btn btn-sm btn-secondary btn-width" style={{backgroundColor:'white',color:'gray',borderColor:'gray'}} onClick={() => this.props.closeModal()}>CANCEL</button>
								<button className="btn btn-sm gs-btn-success btn-width" onClick={()=> {this.props.callback(this.state.datasetids);this.props.closeModal();}}>OK</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class BaseResourceEdit extends Component {
	constructor(props) {
		super(props);

		this.state = {
			baseresource: this.props.baseresource
		};

	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title text-center" style={{color:'#29b99a',fontWeight:'600'}}>Edit Base Resource</h5>
				</div>
				<div className="react-modal-body">
					<div className="row responsive-form-element">
						<div className="form-group col-sm-12 col-md-12">
							<label className="labelclass">Base Resource</label>
							<LocalSelect
								className={`${this.state.baseresource ? '' : 'errorinput'}`}
								options={this.props.resourceArray}
								required={true}
								label={'displayName'}
								valuename={'resourceName'}
								value={this.state.baseresource}
								onChange={(baseresource) => this.setState({baseresource})} />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 text-center">
							<div className="muted credit text-center">
								<button className="btn btn-sm btn-secondary btn-width" style={{backgroundColor:'white',color:'gray',borderColor:'gray'}} onClick={() => this.props.closeModal()}>CANCEL</button>
								<button className="btn btn-sm gs-btn-success btn-width" disabled={!this.state.baseresource} onClick={()=> {this.props.callback(this.state.baseresource);this.props.closeModal();}}>OK</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class DataSetCopyModal extends Component {
	constructor(props) {
		super(props);

		this.state = {};

	}

 	save() {
		this.props.callback({
			name: this.state.name
		});
		this.props.closeModal();
 	}

	render() {
    		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title text-center" style={{color:'#29b99a',fontWeight:'600'}}>Copy Data Set</h5>
				</div>
				<div className="react-modal-body">
					<div className="row responsive-form-element">
						<div className="form-group col-sm-12 col-md-12">
							<label className="labelclass">Name</label>
							<input type="text" className={`form-control ${this.state.name ? '' : 'errorinput'}`} value={this.state.name || ''} onChange={(evt) => this.setState({name: evt.target.value})} autoComplete="off" />
						</div>
 					</div>
				</div>
 				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12">
							<div className="muted credit text-center">
								<button className="btn btn-sm btn-secondary btn-width" style={{backgroundColor:'white',color:'gray',borderColor:'gray'}} onClick={() => this.props.closeModal()}>CANCEL</button>
								<button className="btn btn-sm gs-btn-success btn-width" disabled={!this.state.name} onClick={()=>this.save()}>OK</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class ReportListModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			reportList: []
		};

		this.onLoad = this.onLoad.bind(this);
		this.openReport = this.openReport.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);

		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	onLoad () {
		let { reportList } = this.state;

		reportList = [];

		axios.get(`/api/reports?&field=id,name,type,datasetid,module,foldername,allowedroles,hasexcelexport,remarks,config,displayorder&filtercondition=reports.datasetid=${this.props.resource.id}`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.length > 0) {
					response.data.main.forEach((item) => {
						if (item.type == 'Performance Target' || checkArray(item.allowedroles, this.props.app.user.roleid))
							reportList.push(item);
					});

					reportList.sort((a, b) => {
						return a.displayorder < b.displayorder ? -1 : a.displayorder > b.displayorder ? 1 : 0;
					});
				}

				this.setState({reportList});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	openReport (item, ref) {
		if (ref == 'Report Config')
			this.props.history.push(`/details/reports/${item.id}`);
		else {
			if (item.type == 'Analytics' && item.config.analyticstype == 'KPI')
				return null;

			this.props.history.push(`/reportbuilder/${item.id}`);
		}

		this.props.closeModal();
	}

	render() {
		let { reportList } = this.state;

		return (
			<div className = 'react-outer-modal'>
				<div className = 'react-modal-header'>
					<h5 className = 'modal-title gs-text-color'>Reports from Dataset</h5>
				</div>
				<div className = 'row react-modal-body'>
					<div className = 'col-md-12 col-sm-12 col-xs-12' style = {{overflowY:'auto',maxHeight:'400px'}}>
						{ reportList.length > 0 ? <table className = 'table-condensed' style = {{lineHeight: '35px', width: '100%'}}>
							<tbody>
								{ reportList.map((item, index) => {
									return (
										<tr key = {index}>
											<td className = 'text-center' style = {{verticalAlign: 'baseline', width: '7%'}}>{index+1}</td>
											<td style = {{wordWrap: 'break-word', wordBreak: 'break-all', width: '53%', verticalAlign: 'baseline'}}>
												{item.name}
												<label className="badge gs-form-btn-secondary marginleft-10">{item.type}</label>
											</td>
											<td className = 'text-center font-12' style = {{width: '20%', verticalAlign: 'baseline'}}>
												{
													(checkActionVerbAccess(this.props.app, 'reports', 'Menu')) ? <a href="javascript:void(0);" title = 'Open Report Configuration'
													onClick = {() => this.openReport(item, 'Report Config')}
												>Customise Report</a> : null
												}
											</td>
											<td className = 'text-center font-12' style = {{width: '20%', verticalAlign: 'baseline'}}>
												{
													(item.type != 'Performance Target') ? 
														<a title = 'Open Report'
															href="javascript:void(0);"
															onClick = {() => this.openReport(item, 'Report')}
															className = {`${ item.type == 'Analytics' && item.config.analyticstype == 'KPI' ? 'anchor-Disabled' : ''}`}
														>View Report</a> : null
												}
											</td>
										</tr>
									)
								})}
							</tbody>
						</table> : <div className = 'alert alert-warning text-center'>
							Reports Not Available
						</div> }
					</div>
				</div>
				<div className = 'react-modal-footer'>
					<div className = 'row'>
						<div className = 'col-md-12 col-sm-12 col-xs-12'>
							<div className = 'muted credit text-center'>
								<button
									type = 'button'
									className = 'btn btn-sm btn-width btn-secondary'
									onClick = {this.props.closeModal}>
									<i className = 'fa fa-times marginright-5'></i>Close
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class GalleryReportsModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : false,
			rolesArray: [],
			moduleOptions: ['Sales', 'Project', 'Purchase', 'Stock', 'Accounts', 'Service', 'Production', 'HR', 'Admin'],
			galleryDetails: {},
			tabActive: 'sales',
			tabOpenIndex: 0,
			isReportListFound: false,
			itemProp: {},
			reportListActive: null,
			reportList: [],
			reportName: ''
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getRoles = this.getRoles.bind(this);
		this.closeGallery = this.closeGallery.bind(this);
		this.renderGallery = this.renderGallery.bind(this);
		this.renderGalleryItems = this.renderGalleryItems.bind(this);
		this.renderReportList = this.renderReportList.bind(this);
		this.setActiveTab = this.setActiveTab.bind(this);
		this.renderGalleryReportList = this.renderGalleryReportList.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
		this.onSave = this.onSave.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);

		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	onLoad () {
		let { galleryDetails, moduleOptions } = this.state;

		galleryDetails = {};

		if (this.props.galleryArray && this.props.galleryArray.length > 0) {
			this.props.galleryArray.forEach((item) => {
				if (galleryDetails[item.module.toLowerCase()])
					galleryDetails[item.module.toLowerCase()].push(JSON.parse(JSON.stringify(item)));
				else
					galleryDetails[item.module.toLowerCase()] = [JSON.parse(JSON.stringify(item))];
			});
		}

		this.setState({
			galleryDetails
		}, () => {
			this.getRoles();
		});
	}

	getRoles () {
		let { rolesArray } = this.state;

		rolesArray = [];

		axios.get(`/api/roles?&field=id,name&filtercondition=`).then((response) => {
			if (response.data.message == 'success') {
				rolesArray = response.data.main;

				rolesArray.sort(
					(a, b) => {
						return (a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0;
					}
				);
			}

			this.setState({ rolesArray });

			this.updateLoaderFlag(false);
		});
	}

	setActiveTab (keyvalue) {
		this.setState({
			tabActive: keyvalue,
			reportListActive: null,
			reportName: '',
			itemProp: {}
		});
	}

	renderReportList (item, index) {
		let { resource, galleryArray } = this.props,
		{ galleryDetails, rolesArray, moduleOptions, tabActive } = this.state;

		this.props.updateFormState(this.props.form, {
			reports: [],
			tempresource: null,
			datasetids: []
		});

		this.setState({
			reportListActive: index,
			itemProp: {...item},
			reportName: item.name
		});
	}

	inputOnChange (value, prop, index) {
		let { reportList } = this.state;

		reportList[index][prop] = value;

		this.setState({
			reportList
		});
	}

	onSave () {
		let { isReportListFound, itemProp, reportListActive, reportList } = this.state;

		if (isReportListFound)
			this.closeGallery();
		else {
			reportList = [];

			if (itemProp.reports && itemProp.reports.length > 0)
				reportList = itemProp.reports.map((item) => {
					return {... item,
						aliasname: item.name,
						checked: true
					}
				});

			this.setState({
				isReportListFound: true,
				reportList
			});
		}
	}

	closeGallery () {
		let { reportList, reportName, itemProp } = this.state,
			checkFound = false,
			reports = [];

		if (reportList && reportList.length > 0) {
			this.updateLoaderFlag(true);

			for (let i = 0; i < reportList.length; i++) {
				if (reportList[i].checked)
					reports.push(reportList[i]);
			}

			if (reports.length == 0) {
				this.props.openModal(modalService['infoMethod']({
					header: 'Error',
					body: 'Please select at-least one report',
					btnArray: ['Ok']
				}));

				this.updateLoaderFlag(false);
			} else {
				this.props.updateFormState(this.props.form, {
					name: reportName,
					galleryname: itemProp.name,
					reports
				});

				setTimeout(() => {
					this.props.callback((value) => {
						if (value)
							this.props.closeModal();

						this.updateLoaderFlag(false);
					});
				}, 0);
			}
		} else
			this.props.closeModal();
	}

	renderGallery () {
		let { galleryDetails, tabActive } = this.state;

		return (
			<div className = 'col-md-12'>
				<div className = 'row'>
					<div className = 'col-md-2 paddingleft-0 paddingright-0' style = {{borderRight: '1px solid #e5e5e5'}}>
						<ul className = 'nav flex-column'>
							{ Object.keys(galleryDetails).map((keyvalue, index) => {
								return <li className = 'nav-item' key = {index}>
									<a className = {`nav-link gs-dataset-nav-link pl-4 ${tabActive == keyvalue ? 'active'  : 'gs-print-tab-inactive'}`}
										onClick = {() => this.setActiveTab(keyvalue)}
									>
										<span className = 'gs-uppercase'>{keyvalue}</span>
									</a>
								</li>
							})}
						</ul>
					</div>
					<div className = 'col-md-10 paddingleft-0 paddingright-0' style = {{overflowY: 'auto', maxHeight: '380px'}}>
						<div className = 'tab-content'>
							{ Object.keys(galleryDetails).map((keyvalue, keyindex) => {
								return (
									<div className = {`tab-pane fade ${tabActive == keyvalue ? 'active show'  : ''}`} key = {keyindex}>
										{ galleryDetails[keyvalue] && galleryDetails[keyvalue].length > 0 ? <ul className = 'list-group'>{this.renderGalleryItems(galleryDetails[keyvalue])}</ul> : <div className = 'alert alert-warning text-center'>
												No Data found !!!
											</div>
										}
									</div>
								);
							})}
						</div>
					</div>
				</div>
			</div>
		);
	}

	renderGalleryItems (galleryItems) {
		let { reportListActive } = this.state;

		return galleryItems.map((item, index) => {
			return (
				<li className={`list-group-item border-left-0 border-right-0 rounded-0 gs-hoverDiv ${reportListActive == index ? 'active' : ''} ${index == 0 ? 'border-top-0' : '' }`}  key={index} onClick = {() => this.renderReportList(item, index)}>
					<div>
						<h6 style = {{fontWeight: 600}}>{item.name}</h6>
					</div>
					<div>
						<span>{item.description ? item.description : ''}</span>
					</div>
				</li>
			);
		});
	}

	renderGalleryReportList () {
		let { rolesArray, moduleOptions, tabOpenIndex, reportList } = this.state;

		return (
			<div className = 'col-md-12 col-sm-12 col-xs-12'>
				<ul className = 'list-group'>
				{ reportList && reportList.length > 0 ? reportList.map((item, index) => {
					return (
						<li className={`gs-dataset-list-group-item `}  key={index}>
							<div className = 'row'>
								<div className = 'col-md-12 col-sm-12 col-xs-12' onClick = {() => {
									this.setState({
										tabOpenIndex: index
									})
								}}>
									<div className = 'row'>
										<div className = 'col-md-6 col-sm-12 col-xs-12'>
											<div className = 'row'>
												<div className = 'col-md-1 col-sm-6'>
													<input
														type = 'checkbox'
														onChange = {(e) =>{this.inputOnChange(e.target.checked, 'checked', index)}}
														checked = {item.checked || false}
														className = 'form-check-input'
													/>
												</div>
												<div className = 'col-md-11 col-sm-6'>
													<b>{item.name}</b>
													<label className="badge gs-form-badge-secondary marginleft-10 marginbottom-0">{item.type}</label>
												</div>
											</div>
										</div>
										<div className = 'col-md-6 col-sm-12 col-xs-12'>
											{item.remarks}
										</div>
									</div>
								</div>
							</div>
							{(index == tabOpenIndex) ? <div className = 'row'>
								<div className = 'col-md-12 col-sm-12 col-xs-12 mt-4'>
									<div className = 'row'>
										<div className = 'col-md-6 col-sm-12 col-xs-12'>
											<div className = 'row'>
												<div className = 'form-group col-md-3 col-sm-6'>
													<label>Report Name</label>
												</div>
												<div className = 'form-group col-md-9 col-sm-6'>
													<input
														type = 'text'
														className = {`form-control ${item.checked && !item.aliasname ? 'errorinput' : ''}`}
														name = 'aliasname'
														value = {item.aliasname || ''}
														required = {item.checked}
														placeholder = 'Enter Report Name'
														onChange = {(evt) =>{this.inputOnChange(evt.target.value, 'aliasname', index)}}
														autoComplete = 'off'
													/>
												</div>
											</div>
										</div>
										<div className = 'col-md-6 col-sm-12 col-xs-12'>
											<div className = 'row'>
												<div className = 'form-group col-md-3 col-sm-6'>
													<label>Allowed Roles</label>
												</div>
												<div className = 'form-group col-md-9 col-sm-6'>
													<LocalSelect
														options = {rolesArray}
														value = {item.allowedroles}
														onChange = {(val) =>{this.inputOnChange(val, 'allowedroles', index)}}
														placeholder = {'Select Options'}
														multiselect = {true}
														className = {`${item.checked && !item.allowedroles ? 'errorinput' : ''}`}
														required = {item.checked}
													/>
												</div>
											</div>
										</div>
										<div className = 'col-md-6 col-sm-12 col-xs-12'>
											<div className = 'row'>
												<div className = 'form-group col-md-3 col-sm-6'>
													<label>Select Module</label>
												</div>
												<div className = 'form-group col-md-9 col-sm-6'>
													<LocalSelect
														options = {moduleOptions}
														value = {item.module}
														onChange = {(val) =>{this.inputOnChange(val, 'module', index)}}
														placeholder = {'Select Options'}
														className = {`${item.checked && !item.module ? 'errorinput' : ''}`}
														required = {item.checked}
													/>
												</div>
											</div>
										</div>
										<div className = 'col-md-6 col-sm-12 col-xs-12'>
											<div className = 'row'>
												<div className = 'form-group col-md-3 col-sm-6'>
													<label>Select Folder</label>
												</div>
												<div className = 'form-group col-md-9 col-sm-6'>
													<AutoSuggest
														resource = {'reports'}
														field = {'foldername'}
														value = {item.foldername}
														placeholder = {'Select Folder Name'}
														required = {item.checked}
														onChange = {(val) =>{this.inputOnChange(val, 'foldername', index)}}
														className = {`${item.checked && !item.foldername ? 'errorinput' : ''}`}
													/>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> : null}
						</li>
					)
				}) : <div className = 'alert alert-warning text-center'>
					Reports Not Available
				</div> }
				</ul>
			</div>
		);
	}

	render() {
		let { isReportListFound, itemProp, reportListActive, reportList, reportName } = this.state,
			invalidFlag = false;

		if (isReportListFound && reportList && reportList.length > 0) {
			for (let i = 0; i < reportList.length; i++) {
				if (reportList[i].checked) {
					['aliasname', 'allowedroles', 'module', 'foldername'].forEach((item) => {
						if (!reportList[i][item])
							invalidFlag = true;
					});

					if (invalidFlag)
						break;
				}
			}
		}

		let disabledValue = isReportListFound ? invalidFlag : (reportListActive == null || reportListActive == undefined ? true : false);

		return (
			<div className = 'react-outer-modal'>
				<div className = 'react-modal-header p-4'>
					<div className = 'row'>
						<div className = 'col-md-6'>
							<h5 className = 'modal-title gs-text-color'>Choose from defaults</h5>
						</div>
						{reportListActive != null ? <div className = 'col-md-6 pull-right'>
							<input
								type = 'text'
								className = {`form-control ${!reportName ? 'errorinput' : ''}`}
								name = 'reportName'
								value = {reportName || ''}
								required = {true}
								placeholder = 'Enter Dataset Name'
								onChange = {(evt) =>{
									this.setState({
										reportName: evt.target.value
									})
								}}
								autoComplete = 'off'
							/>
						</div> : null}
					</div>
				</div>
				<div className = 'react-modal-body p-0' style = {{overflowY: 'auto', maxHeight: '400px'}}>
					<div style={{display: 'flex', flexWrap: 'wrap'}}>
						<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
						{isReportListFound ? this.renderGalleryReportList() : this.renderGallery()}
					</div>
				</div>
				<div className = 'react-modal-footer'>
					<div className = 'row'>
						<div className = 'col-md-12 col-sm-12 col-xs-12'>
							<div className = 'muted credit text-right'>
								<button
									type = 'button'
									className = 'btn btn-sm btn-width btn-secondary'
									onClick = {() => {
										if (isReportListFound)
											this.setState({
												isReportListFound: false,
												itemProp: {},
												reportListActive: null,
												tabOpenIndex: 0
											});
										else {
											this.props.history.replace('/list/datasets');
											this.props.closeModal();
										}
									}}>
									<i className = 'fa fa-times marginright-5'></i>Close
								</button>
								<button
									type = 'button'
									className = 'btn btn-sm gs-btn-success btn-width'
									disabled = {disabledValue}
									onClick = {this.onSave}>
									<i className = 'fa fa-check marginright-5'></i>Ok
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class DatasetCreateOptionModal extends Component {
	constructor(props) {
		super(props);

		this.state = {};

		this.selectOnChange = this.selectOnChange.bind(this);
	};

	selectOnChange(value) {
		this.props.closeModal();
		this.props.callback(`${value ? value : ''}`);
	}

	render() {
		let styleObj = {
			width: '100%',
		    height: '90px',
			display: 'flex',
			flexDirection: 'column',
			alignItems: 'center',
			justifyContent: 'center'
		};
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<div className = 'row'>
						<div className = 'gs-text-color col-md-6'>
							<h6 className="modal-title">Choose an option</h6>
						</div>
						<div className = 'col-md-6'>
							<div className = 'pull-right' style={{fontWeight:'600', fontSize: '17px', color: '#959595', cursor: 'pointer'}} onClick={() => this.selectOnChange()}>x</div>
						</div>
					</div>
				</div>
				<div className="react-modal-body" style={{padding:"20px"}}>
					<div className="row">
						<div className="col-md-4 text-center expensereqtypemodal">
							<div className={`border rounded float-left p-2 reqtype gs-text-color ${this.state.type == 'local' ? 'active' : ''}`} onClick={()=>this.selectOnChange('local')} style={{width: '100%'}}>
								<div className = 'text-center' style={styleObj}>
									<img src={`../images/fromdefault.png`}/>
								</div>
								Create new
							</div>
						</div>
						<div className="col-md-4 text-center expensereqtypemodal"  style={{paddingLeft: '0px'}}>
							<div className={`vmarginleft-10 border rounded float-right p-2 reqtype gs-text-color ${this.state.type == 'gallery' ? 'active' : ''}`} onClick={()=>this.selectOnChange('gallery')} style={{width: '100%'}}>
								<div className = 'text-center' style={styleObj}>
									<img src={`../images/fromgallery.png`}/>
								</div>
								Add from gallery
							</div>
						</div>
						<div className="col-md-4 text-center expensereqtypemodal"  style={{paddingLeft: '0px'}}>
							<div className={`vmarginleft-10 border rounded float-right p-2 reqtype gs-text-color ${this.state.type == 'import' ? 'active' : ''}`} onClick={()=>this.selectOnChange('import')} style={{width: '100%'}}>
								<div className = 'text-center' style={styleObj}>
									<img src={`../images/fromimport.png`}/>
								</div>
								Import file
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class ImportModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			resource: this.props.fileJSON ? this.props.fileJSON : null,
			firstModal: this.props.fileJSON ? false : true,
			firstParam: this.props.fileJSON ? false : true,
			loaderflag : true,
			rolesArray: [],
			tabOpenIndex: 0,
			moduleOptions: ['Sales', 'Project', 'Purchase', 'Stock', 'Accounts', 'Service', 'Production', 'HR', 'Admin']
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getRoles = this.getRoles.bind(this);
		this.fileOnChange = this.fileOnChange.bind(this);
		this.validate = this.validate.bind(this);
		this.throwError = this.throwError.bind(this);
		this.validateFn = this.validateFn.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
		this.onSave = this.onSave.bind(this);
		this.renderGalleryReportList = this.renderGalleryReportList.bind(this);
	}

	componentWillMount() {
		this.onLoad();
		this.getRoles();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	onLoad () {

	}

	getRoles () {
		let { rolesArray } = this.state;

		rolesArray = [];

		axios.get(`/api/roles?&field=id,name&filtercondition=`).then((response) => {
			if (response.data.message == 'success') {
				rolesArray = response.data.main;

				rolesArray.sort(
					(a, b) => {
						return (a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0;
					}
				);
			}

			this.setState({ rolesArray });

			this.updateLoaderFlag(false);
		});
	}

	fileOnChange(files) {
		if(files.length > 0) {
			this.updateLoaderFlag(true);
			this.setState({
				file: files[0],
				resource: null
			}, () => {
				this.validate();
			});
			this.updateLoaderFlag(false);
		} else {
			this.setState({
				file: null,
				resource: null
			});
		}
	}

	validate() {
		const fr = new FileReader();
		fr.onload = e => {
			try {
				let fileJSON = JSON.parse(e.target.result);
				this.validateFn(fileJSON);
			} catch(err) {
				console.log(err);
				let apiResponse = commonMethods.apiResult({
					data: {
						message: "Failure in JSON conversion"
					}
				});
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		};

		fr.readAsText(this.state.file);
	}

	throwError(errorArray) {
		let apiResponse = commonMethods.apiResult({
			data: {
				message: "Sorry, there was an error",
				error: errorArray
			}
		});
		return this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
	}

	validateFn(fileJSON) {
		let resource = {}, errorArray = [];

		resource.name = typeof(fileJSON.name) == 'string' && fileJSON.name ? fileJSON.name : null;
		resource.tempdatasetids = fileJSON.datasetids && Array.isArray(fileJSON.datasetids) ? fileJSON.datasetids : [];
		resource.tempdatasetarray = fileJSON.datasetarray && Array.isArray(fileJSON.datasetarray) ? fileJSON.datasetarray : [];

		let invalidDataSetFound = false;
		resource.tempdatasetarray.forEach(injds => {
			if(!resource.tempdatasetids.includes(injds.id))
				invalidDataSetFound = true;
		})

		if(invalidDataSetFound) {
			errorArray.push("Invalid Injected Dataset Configuration");
			return this.throwError(errorArray);
		}

		if(typeof(fileJSON.config) != 'object') {
			errorArray.push("Invalid Configuration");
			return this.throwError(errorArray);
		}

		if(typeof(fileJSON.config.isgroupby) != 'boolean')
			errorArray.push("Invalid Is Group By");

		if(fileJSON.config.isgroupby === true && (!fileJSON.config.groupby || !Array.isArray(fileJSON.config.groupby)))
			errorArray.push("Invalid Group By");

		if(fileJSON.config.configurablecolumns && !Array.isArray(fileJSON.config.configurablecolumns))
			errorArray.push("Invalid Configurable Columns");

		if(fileJSON.config.filters && !Array.isArray(fileJSON.config.filters))
			errorArray.push("Invalid Filters");

		if(fileJSON.config.dataSetArray && !Array.isArray(fileJSON.config.dataSetArray))
			errorArray.push("Invalid Data Set Configuraion");

		if(errorArray.length > 0)
			return this.throwError(errorArray);

		resource.config = {
			isgroupby: fileJSON.config.isgroupby,
			groupby: fileJSON.config.isgroupby ? fileJSON.config.groupby : null,
			configurablecolumns: fileJSON.config.configurablecolumns ? fileJSON.config.configurablecolumns : [],
			filters: fileJSON.config.filters ? fileJSON.config.filters : [],
			dataSetArray: fileJSON.config.dataSetArray ? fileJSON.config.dataSetArray : []
		};

		resource.reportArray = (fileJSON.reportArray || []).map(item => {
			return {
				reportName: item.name ? item.name : null,
				aliasname: item.name ? item.name : null,
				type: item.type ? item.type : null,
				module: item.module ? item.module : null,
				foldername: item.foldername ? item.foldername : null,
				allowedroles: [],
				hasexcelexport: item.hasexcelexport ? true : false,
				remarks: item.remarks ? item.remarks : null,
				config: item.config ? item.config : {},
				checked: true,
				enabledrilldown: item.enabledrilldown ? true : false,
				displayorder: item.displayorder ? item.displayorder : null
			};
		});

		this.setState({ resource });
	}

	inputOnChange(name, value, item, valueObj) {
		let { resource } = this.state;
		if(item)
			item[name] = value;
		else
			resource[name] = value;

		if(name == 'newdatasetid')
			item.newidentifier = valueObj.identifier;

		this.setState({ resource });
	}

	onSave () {
		if(this.state.firstParam) {
			console.log(this.state.resource);
			if(this.state.resource.reportArray.length == 0 && this.state.resource.tempdatasetarray.length == 0) {
				console.log("asdfasdfasdfasdfasdfasd");
				this.setState({
					firstParam: false
				});
				return true;
			} else {
				this.props.callback(this.state.resource, this.props.closeModal);
				this.props.closeModal();
				return true;
			}
		}
		this.updateLoaderFlag(true);

		let { resource } = this.state;

		resource.datasetids = resource.tempdatasetarray.map((item) => item.newdatasetid);

		let reports = resource.reportArray.filter(item => item.checked);

		reports = JSON.stringify(reports);
		resource.config = JSON.stringify(resource.config);

		resource.tempdatasetarray.forEach(item => {
			reports = reports.replace(new RegExp(item.identifier, 'g'), item.newidentifier);
			resource.config = resource.config.replace(new RegExp(item.identifier, 'g'), item.newidentifier);
		});

		reports = JSON.parse(reports);
		resource.config = JSON.parse(resource.config);

		let confirmCB = () => {
			this.props.updateFormState(this.props.form, {
				name: resource.name,
				config: resource.config,
				datasetids: resource.datasetids,
				isimport: true,
				reports
			});

			setTimeout(() => {
				this.props.callback(false, (value) => {
					if (value)
						this.props.closeModal();

					this.updateLoaderFlag(false);
				});
			}, 0);
		};

		if(resource.reportArray.length > 0 && reports.length == 0) {
			let confirmModalRes = {
				"methodName": "confirmMethod",
				"message": {
					"header": "Confirmation",
					"btnArray": ["Yes, Continue", "No, Select Report"],
					"bodyArray": [],
					"btnTitle": "No reports selected in this Import. Do you want to continue?"
				}
			};
			this.props.openModal(modalService[confirmModalRes.methodName](confirmModalRes.message, (resparam) => {
				if (resparam)
					confirmCB();
				else
					this.updateLoaderFlag(false);
			}));
		} else {
			confirmCB();
		}
	}

	renderInjectedDataSet(injds, index) {
		return(
			<div className="form-group col-md-6" key={index}>
				<label className="labelclass">Inject for {injds.name}</label>
				<AutoSelect
					resource = {'datasets'}
					fields = {'id,name,identifier'}
					value = {injds.newdatasetid}
					placeholder = {'Select Dataset'}
					labelKey={'name'}
					valueKey={'identifier'}
					required = {true}
					onChange= {(value, valueObj) => this.inputOnChange('newdatasetid', value, injds, valueObj)}
					className = {`${!injds.newdatasetid ? 'errorinput' : ''}`}
				/>
			</div>
		);
	}

	renderGalleryReportList (item, index) {
		let { rolesArray, moduleOptions, tabOpenIndex, reportList } = this.state;

		return (
			<div className = 'card' style = {{marginBottom:'10px'}} key={index}>
				<div className = 'gs-card-section-header'>
					<div className = 'row'>
						<div className = 'col-md-6 col-sm-12 col-xs-12'>
							<div className = 'row'>
								<div className = 'col-md-1 col-sm-6'>
									<input
										type = 'checkbox'
										onChange = {(e) =>{this.inputOnChange('checked', e.target.checked, item)}}
										checked = {item.checked ? item.checked : false}
										className = 'form-check-input'
									/>
								</div>
								<div className = 'col-md-11 col-sm-6'>
									<b>{item.reportName}</b>
									<label className="badge gs-form-badge-secondary marginleft-10 marginbottom-0">{item.type}</label>
								</div>
							</div>
						</div>
						<div className = 'col-md-6 col-sm-12 col-xs-12'>
							{item.remarks}
						</div>
					</div>
				</div>
				<div className="card-body">
					<div className = 'row'>
						<div className = 'col-md-12 col-sm-12 col-xs-12 mt-4'>
							<div className = 'row'>
								<div className = 'col-md-6 col-sm-12 col-xs-12'>
									<div className = 'row'>
										<div className = 'form-group col-md-3 col-sm-6'>
											<label>Report Name</label>
										</div>
										<div className = 'form-group col-md-9 col-sm-6'>
											<input
												type = 'text'
												className = {`form-control ${item.checked && !item.aliasname ? 'errorinput' : ''}`}
												name = 'aliasname'
												value = {item.aliasname || ''}
												required = {item.checked}
												placeholder = 'Enter Report Name'
												onChange = {(evt) =>{this.inputOnChange('aliasname', evt.target.value, item)}}
												autoComplete = 'off'
											/>
										</div>
									</div>
								</div>
								<div className = 'col-md-6 col-sm-12 col-xs-12'>
									<div className = 'row'>
										<div className = 'form-group col-md-3 col-sm-6'>
											<label>Allowed Roles</label>
										</div>
										<div className = 'form-group col-md-9 col-sm-6'>
											<LocalSelect
												options = {rolesArray}
												value = {item.allowedroles}
												onChange = {(val) =>{this.inputOnChange('allowedroles', val, item)}}
												placeholder = {'Select Options'}
												multiselect = {true}
												className = {`${item.checked && (!item.allowedroles || item.allowedroles.length == 0) ? 'errorinput' : ''}`}
												required = {item.checked}
											/>
										</div>
									</div>
								</div>
								<div className = 'col-md-6 col-sm-12 col-xs-12'>
									<div className = 'row'>
										<div className = 'form-group col-md-3 col-sm-6'>
											<label>Select Module</label>
										</div>
										<div className = 'form-group col-md-9 col-sm-6'>
											<LocalSelect
												options = {moduleOptions}
												value = {item.module}
												onChange = {(val) => { this.inputOnChange('module', val, item)}}
												placeholder = {'Select Options'}
												className = {`${item.checked && !item.module ? 'errorinput' : ''}`}
												required = {item.checked}
											/>
										</div>
									</div>
								</div>
								<div className = 'col-md-6 col-sm-12 col-xs-12'>
									<div className = 'row'>
										<div className = 'form-group col-md-3 col-sm-6'>
											<label>Select Folder</label>
										</div>
										<div className = 'form-group col-md-9 col-sm-6'>
											<AutoSuggest
												resource = {'reports'}
												field = {'foldername'}
												value = {item.foldername}
												placeholder = {'Select Folder Name'}
												required = {item.checked}
												onChange = {(val) =>{this.inputOnChange('foldername', val, item)}}
												className = {`${item.checked && !item.foldername ? 'errorinput' : ''}`}
											/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	render() {
		let { resource } = this.state;

		let invalidFlag = false;

		let fileElement = document.getElementById('filejsondatasetimport');

		let fileName = fileElement && fileElement.files && fileElement.files.length > 0 ? fileElement.files[0].name : '';

		if (resource) {
			if(!resource.name)
				invalidFlag = true;

			for (let i = 0; i < resource.tempdatasetarray.length; i++) {
				if(!resource.tempdatasetarray[i].newdatasetid || !resource.tempdatasetarray[i].newidentifier) {
					invalidFlag = true;
					break;
				}
			}

			for (let i = 0; i < resource.reportArray.length; i++) {
				if (resource.reportArray[i].checked) {
					['aliasname', 'allowedroles', 'module', 'foldername'].forEach((item) => {
						if (!resource.reportArray[i][item])
							invalidFlag = true;

						if(item == 'allowedroles' && resource.reportArray[i].allowedroles && resource.reportArray[i].allowedroles.length == 0)
							invalidFlag = true;
					});

					if (invalidFlag)
						break;
				}
			}
		}
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title gs-text-color">Import File</h5>
				</div>
				<div className={`react-modal-body ${this.state.firstModal ? '' : 'modalmaxheight'}`}>
					<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
					<div className={`row responsive-form-element ${this.state.firstParam ? '' : 'marginbottom-15'}`}>
						{this.state.firstParam ? <div className="form-group col-md-10 offset-md-1 col-sm-5 "><div className="input-group">
							<input type="file" accept={'.json'} id="filejsondatasetimport" className="form-control hide" onChange={(evt) => this.fileOnChange(evt.target.files)} />
							<input type="text" id="filejsondatasetimporttext" htmlFor={`filejsondatasetimport`} className="form-control" value={fileName} onClick={() => {document.getElementById('filejsondatasetimport').click();document.getElementById('filejsondatasetimporttext').blur();}} />
							<div className="input-group-append">
								<button className='btn btn-sm btn-outline-success' style={{paddingLeft: '15px', paddingRight: '15px'}} onClick={() => {document.getElementById('filejsondatasetimport').click();}}>Choose File</button>
							</div>
						</div></div> : null}
						{this.state.firstModal && !this.state.firstParam  ? <div style={{textAlign: 'center', width: '100%', marginBottom: '15px'}}>
							<span>No Reports found in this file.</span>
							<br />
							<span>You can rename the dataset name below and Click Import</span>
						</div> : null}
						{this.state.resource && !this.state.firstParam ? <div className={`col-md-${this.state.firstModal ? '10' : '6'} col-sm-12 ${this.state.firstModal ? 'offset-md-1' : ''}`}>
							<label className="labelclass" style={{marginTop: '5px'}}><b>Dataset Name</b></label>
							<input
								type = 'text'
								className = {`form-control ${!resource.name ? 'errorinput' : ''}`}
								name = 'name'
								value = {resource.name || ''}
								required = {true}
								placeholder = 'Enter Dataset Name'
								onChange = {(evt) =>{
									this.inputOnChange('name', evt.target.value);
								}}
								autoComplete = 'off'
							/>
						</div> : null}
					</div>
					{!this.state.firstParam && resource && resource.tempdatasetarray.length > 0 ? <div className="row responsive-form-element">
						<div className="gs-card-section borderradius-0">
							<div className="gs-card-section-header borderradius-0 card-header-custom">Select Inject Dataset</div>
							<div className="card-body">
								<div className="row responsive-form-element">
									{resource.tempdatasetarray.map((injds, index) => {
										return this.renderInjectedDataSet(injds, index);
									})}
								</div>
							</div>
						</div>
					</div> : null}
					{!this.state.firstParam && resource && resource.reportArray.length > 0 ? <div className="row responsive-form-element">
						<div className="gs-card-section marginbottom-15 borderradius-0">
							<div className="gs-card-section-header borderradius-0 card-header-custom">Select Reports</div>
							<div className="card-body">
								<ul className = 'list-group'>
									{resource.reportArray.map((report, index) => this.renderGalleryReportList(report, index))}
								</ul>
							</div>
						</div>
					</div> : null}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={() => {this.props.closeModal();this.props.history.replace('/list/datasets');}}>Cancel</button>
								{resource ? <button
									type = 'button'
									className = 'btn btn-sm gs-btn-success btn-width'
									disabled = {!this.state.firstParam && invalidFlag}
									onClick = {this.onSave}>
									{this.state.firstParam ? 'Ok' : 'Import'}
								</button> : null}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

DataSet = connect(
	(state, props) => {
		let formName = `DataSet-${props.match.params.id > 0 ? props.match.params.id : 'create'}`;
		return {
			app : state.app,
			form : formName,
			touchOnChange: true,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState, updateAppState }
)(reduxForm()(DataSet));

export default DataSet;
