import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { HashRouter as Router, Link} from 'react-router-dom';
import axios from 'axios';
import RouterBar from '../components/routerbar';
import {updateAppState, resetReportFilter} from '../actions/actions';
import Q from 'q';
import { commonMethods, modalService, accessMenuResource } from '../utils/services';
import { checkArray } from '../utils/utils';
import {ReviseModal, ReasonModal, SessionExpireModal, ConnectionErrorModal, TimeOutModal, ExcelSyncModal} from '../components/utilcomponents';
import { v1 as uuidv1 } from 'uuid';
import Modal from 'react-modal';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Home from './home';
import Dashboard from './dashboard';
import DetailForm from './details';
import Loadingcontainer from '../components/loadingcontainer';
import Dialer from '../components/dialer';
import NewDialer from '../components/newdialer';

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			modalArray: [],
			resizecount: 0
		};

		this.initializeResponse = this.initializeResponse.bind(this);
		this.pageInitializeCallback = this.pageInitializeCallback.bind(this);
		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.createOrEdit = this.createOrEdit.bind(this);
		this.renderModal = this.renderModal.bind(this);
		this.getConfirmation = this.getConfirmation.bind(this);
		this.getUserDetails = this.getUserDetails.bind(this);
		this.getNavResult = this.getNavResult.bind(this);
		this.checkAccess = this.checkAccess.bind(this);
		this.updateBookMark = this.updateBookMark.bind(this);
		this.reloadpage = this.reloadpage.bind(this);
		this.mainappRef = this.mainappRef.bind(this);
		this.getAnnouncementFile = this.getAnnouncementFile.bind(this);
		this.getDashboardDetails = this.getDashboardDetails.bind(this);
	}

	componentDidCatch(error, info) {
		this.setState({
			hasError: true,
			errorStack: error.stack,
			errorInfo: info
		});
		console.log(error, info);
	}

	componentWillMount() {
		axios.interceptors.request.use((config) => {
			//config.timeout = 5000;
			var actionverbArray = ['Delete', 'Cancel'];
			var remarksActionArray = ['Amend','Disqualified','Junk','Lost','Close','Hold','Unhold','Reopen','Reject','Reopen Valuation'];

			if (config.method.toLowerCase() == 'post')
				config.headers['x-gs-dedupeid'] = uuidv1();

			if (config.method.toLowerCase() == 'post' && actionverbArray.indexOf(config.data.actionverb) > -1) {
				this.props.updateAppState('pageloaderFlag', true);
				var deferred = Q.defer();
				let tempModalObj = {
					warningModal : true,
					header : 'Warning',
					btnTitle: `Do you want to ${config.data.actionverb} this item?`,
					btnArray : ['Ok', 'Cancel']
				};
				this.openModal(modalService['confirmMethod'](tempModalObj, (param) => {
					if(param) {
						this.props.updateAppState({
							pageloaderFlag : false,
							usepageloaderFlag : false
						});
						return deferred.resolve(config)
					} else {
						this.props.updateAppState({
							pageloaderFlag : false,
							usepageloaderFlag : true
						});
						return deferred.reject();
					}
				}));
				return deferred.promise;
			} else if(config.method.toLowerCase() == 'post' && remarksActionArray.indexOf(config.data.actionverb) > -1 && !config.data.ignoreExceptions && (config.data.data.resourceName != 'financialyears' || (config.data.actionverb != 'Close' && config.data.data.resourceName == 'financialyears'))) {
				this.props.updateAppState('pageloaderFlag', true);
				var deferred = Q.defer();
				let reviseCallback = (param, reasontext, lostreasonid, competitorid) => {
					if(param) {
						if(['Disqualified', 'Junk', 'Lost'].indexOf(config.data.actionverb) >= 0)
							config.data.data.outcomereason = reasontext;
						if(config.data.actionverb == 'Lost') {
							config.data.data.lostreasonid = lostreasonid;
							config.data.data.competitorid = competitorid;
						}
						if(config.data.actionverb == 'Reject')
							config.data.data.rejectionreason = reasontext;
						if(config.data.actionverb == 'Reopen' && (config.data.data.resourceName == 'servicecalls' || config.data.data.resourceName == 'projects' || config.data.data.resourceName == 'salesactivities'))
							config.data.data.reopenreason = reasontext;
						config.data.data.closeremarks = reasontext;
						this.props.updateAppState({
							pageloaderFlag : false,
							usepageloaderFlag : false
						});
						return deferred.resolve(config)
					} else {
						this.props.updateAppState({
							pageloaderFlag : false,
							usepageloaderFlag : true
						});
						return deferred.reject();
					}
				}
				this.openModal({
					render: (closeModal) => {
						return <ReasonModal app={this.props.app} actionverb={config.data.actionverb} competitorid={config.data.data.competitorid} callback={reviseCallback} closeModal={closeModal} />
					},
					className: {
						content: 'react-modal-custom-class-40',
						overlay: 'react-modal-overlay-custom-class'
					},
					confirmModal: true
				});
				return deferred.promise;
			} else if(config.method.toLowerCase() == 'post' && ["/api/quotes", "/api/quotes/", "/api/orders", "/api/orders/", "/api/purchaseorders", "/api/purchaseorders/", "/api/contractenquiries",  "/api/contractenquiries/", "/api/contracts", "/api/contracts/", "/api/projects", "/api/projects/"].indexOf(config.url) >= 0 && config.data.actionverb == 'Revise') {
				this.props.updateAppState('pageloaderFlag', true);
				var deferred = Q.defer();
				let reviseCallback = (param, reasontext) => {
					if(param) {
						config.data.data.comment = reasontext;
						this.props.updateAppState({
							pageloaderFlag : false,
							usepageloaderFlag : false
						});
						return deferred.resolve(config)
					} else {
						this.props.updateAppState({
							pageloaderFlag : false,
							usepageloaderFlag : true
						});
						return deferred.reject();
					}
				}
				this.openModal({
					render: (closeModal) => {
						return <ReviseModal callback={reviseCallback} closeModal={closeModal} />
					},
					className: {
						content: 'react-modal-custom-class-30',
						overlay: 'react-modal-overlay-custom-class'
					},
					confirmModal: true
				});
				return deferred.promise;
			} else  {
				//For Other POST requests and GET Requests
				return config;
			}
		}, function (error) {
			return Promise.reject(error);
		});
		axios.interceptors.response.use((response) => {
			if(response && response.config && response.config.method.toLowerCase() == 'post') {
				let resourceName = response.config.url.split('api/')[1];
				if(this.props.app.myResources[resourceName])
					response.resourceObj = this.props.app.myResources[resourceName];
			}
		    return response;
		}, (error) => {
			if(!error)
				return Promise.reject(error);
			if(error.response && error.response.status == 401) {
				this.openModal({
					render: (closeModal) => {
						return <SessionExpireModal closeModal={closeModal} />
					},
					className: {
						content: 'react-modal-custom-class-40',
						overlay: 'react-modal-overlay-custom-class'
					}, confirmModal: true
				});
				return Promise.reject(error);
			} if(error.response && error.response.status == 504) {
				this.openModal({
					render: (closeModal) => {
						return <TimeOutModal closeModal={closeModal} />
					},
					className: {
						content: 'react-modal-custom-class-40',
						overlay: 'react-modal-overlay-custom-class'
					}, confirmModal: true
				});
				return Promise.reject(error);
			} else if (error.toString() == 'Error: Network Error' || (error.response && error.response.status == 0)) {
				this.props.updateAppState('pageloaderFlag', true);
				let connectionErrorCallback = () => {
					this.props.updateAppState({
						pageloaderFlag : false,
						usepageloaderFlag : true
					});
					//return deferred.reject();
				}
				this.openModal({
					render: (closeModal) => {
						return <ConnectionErrorModal callback={connectionErrorCallback}  closeModal={closeModal} />
					},
					className: {
						content: 'react-modal-custom-class-40',
						overlay: 'react-modal-overlay-custom-class'
					}
				});
				return Promise.reject(error);
			} else {
				return Promise.reject(error);
			}
		});
		axios.get('/api/initialize/initialize').then(this.initializeResponse);
	}

	initializeResponse(response) {
		var selectedCompanyDetails;
		response.data.company.map(function(a) {
			if(a.id == response.data.user.selectedcompanyid)
				selectedCompanyDetails = a;
		});

		if(response.data.user.forcepasswordchange)
			window.location.href = "#/forcechangepassword";

		let tempAppObj = {
			companyArray: response.data.company,
			user: response.data.user,
			selectedcompanyid: response.data.user.selectedcompanyid,
			selectedCompanyDetails: selectedCompanyDetails,
		};

		axios.get('/api/initialize/pageinitialize').then((secresponse) => this.pageInitializeCallback(secresponse, tempAppObj));
	}

	pageInitializeCallback(response, tempAppObj) {
		for(var prop in response.data.main)
			tempAppObj[prop] = response.data.main[prop];
		tempAppObj.gstinpattern = '/^[0-9]{2}[A-Z]{4}[0-9A-Z]{1}[0-9]{4}[A-Z]{1}[0-9A-Z]{1}[0-9A-Z]{2}$/';
		tempAppObj.inactivetaxerrorthrown = false;
		tempAppObj.listFilterObj = {};
		tempAppObj.calldetails = {};
		this.props.updateAppState(tempAppObj);
		setTimeout(this.getDashboardDetails, 0);
	}

	getDashboardDetails() {
		let widgetArray = [];

		axios.get(`/api/query/getdashboardquery?type=app`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.length > 0)
					widgetArray = response.data.main;

				this.props.updateAppState('widgetArray', widgetArray);

				setTimeout(this.getUserDetails, 0);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getUserDetails() {
		axios.get(`/api/users?field=id,modified,newnavigation&filtercondition=users.id=${this.props.app.user.id}`).then((response) => {
			if (response.data.message == 'success') {
				response.data.main[0].newnavigation = response.data.main[0].newnavigation ? response.data.main[0].newnavigation : [];
				this.setState({
					user: response.data.main[0]
				});

				this.getNavResult(this.props.app.userNewNavigation, response.data.main[0].newnavigation);
				if(this.props.app.user.announcementref)
					this.getAnnouncementFile();
				this.props.updateAppState('initialLoaderFlag', true);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}
	
	getAnnouncementFile() {
		this.openModal({
			render: (closeModal) => {
				return <AnnouncementDetail announcementref={this.props.app.user.announcementref} closeModal={closeModal} />
			},
			className: {content: 'react-modal-custom-class-announcement', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true
		});
	}

	getNavResult(navigation, usernav) {
		let newnavigation = this.checkAccess(navigation, usernav);
		this.props.updateAppState('accessNavigation', newnavigation);
	}

	checkAccess(navigation, usernav) {
		let newnavigation = JSON.parse(JSON.stringify(navigation));
		let activefound = false;
		newnavigation.modules.forEach((module) => {
			if(module.active)
				activefound = true;
			if(accessMenuResource.checkFeatureAccess(module, this.props.app)) {
				module._featureaccess = true;
				module._access = false;
				module._checkedaccess = false;
				module.child.forEach((submodule) => {
					submodule._access = false;
					submodule._checkedaccess = false;
					submodule.links.forEach((link) => {
						if(link.type == 'folder') {
							module._access = true;
							submodule._access = true;
							link._access = true;
							link.ischecked = true;
							link.reportlinks.forEach((reportlink) => {
								reportlink.ischecked = false;
								if(usernav.indexOf(reportlink.url) >= 0) {
									reportlink.ischecked = true;
									submodule._checkedaccess = true;
									module._checkedaccess = true;
								}
							});
						} else {
							if(accessMenuResource.checkFeatureAccess(link, this.props.app) && accessMenuResource.check(link.resource,'Menu', link.roles, this.props.app)) {
								module._access = true;
								submodule._access = true;
								link._access = true;
								link.ischecked = false;
								if(usernav.indexOf(link.url) >= 0) {
									link.ischecked = true;
									submodule._checkedaccess = true;
									module._checkedaccess = true;
								}
							} else {
								link._access = false;
							}
						}
					});
				});
			} else {
				module._access = false;
			}
		});
		if(!activefound)
			newnavigation.modules[0].active = 'active';
		return newnavigation;
	}

	updateBookMark(link, callback) {
		let newnav = [...this.state.user.newnavigation];
		if(link.ischecked)
			newnav.splice(newnav.indexOf(link.url), 1);
		else
			newnav.push(link.url);

		let tempData = {
			actionverb : 'Bookmark',
			data : {
				id: this.state.user.id,
				modified: this.state.user.modified,
				newnavigation: newnav
			}
		};

		axios({
			method : 'post',
			data : tempData,
			url : '/api/users'
		}).then((response) => {
			if (response.data.message == "success") {
				let user = this.state.user;
				user.modified = response.data.main.modified;
				user.newnavigation = response.data.main.newnavigation;
				this.setState({ user });
				this.getNavResult(this.props.app.accessNavigation, user.newnavigation);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			callback();
		});
	}

	closeModal(modal) {
		let key = -1;
		this.state.modalArray.map((item, index) => {
			if(item.id == modal.id)
				key = index;
		});
		if(this.state.modalArray.length > -1)
			this.state.modalArray.splice(key, 1);
		this.setState({
			modalArray :  [...this.state.modalArray]	
		});
	}

	openModal(modal) {
		setTimeout(() => {
			modal.id = uuidv1();
			modal.isOpen = true;
			if(modal.dontShow)
				this.setState({
					modalArray :  [...this.state.modalArray]
				});
			else
				this.setState({
					modalArray :  [...this.state.modalArray, modal]
				});
		}, 0);
	}

	createOrEdit(url, id, params, callback, closeCallback) {
	 	var param = {
			isModal: true,
			callback: callback,
			match: {
				params: {
					id : id
				},
				path: url
			},
			location: params ? {
				params
			} : {}
		}
		this.openModal({
			render: (closeModal) => {
				let closeModalInternal = () => {
					closeModal();
					if(closeCallback)
						closeCallback();
				}
				return <DetailForm {...param} closeModal={closeModalInternal} openModal={this.openModal} createOrEdit={this.createOrEdit} />
			},
			className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'},
			isDetailForm: true
		});
	}

  	renderModal(index) {
  		if(!this.state.modalArray[index])
  			return null;

		let modal = this.state.modalArray[index];
		return (
			<Modal
				isOpen={modal.isOpen}
				onRequestClose={() => {modal.confirmModal ? null : this.closeModal(modal)}}
				shouldCloseOnOverlayClick={modal.confirmModal ? false : true}
				ariaHideApp={false}
				aria={{
				labelledby: "heading",
				describedby: "full_description"}}
				className={modal.className.content}
				overlayClassName={modal.className.overlay}>
				{modal.render(() => this.closeModal(modal))}
				{this.renderModal(index + 1)}
			</Modal>
		);
	}

	getConfirmation(message, callback) {
		//const allowTransition = window.confirm("asdfasdfasdfasd")
		let header = 'Unsaved Changes !!';
		let body = 'You have unsaved changes.';
		let btnTitle = 'Do you want to leave the page without saving?';
		if(message != 'Unsaved Changes') {
			let msg = JSON.parse(message);
			header = msg.header ? msg.header : header;
			body = msg.body ? msg.body : body;
			btnTitle = msg.btnTitle ? msg.btnTitle : btnTitle;
		}
		this.openModal(modalService['confirmMethod']({
			warningModal : true,
			header,
			body,
			btnTitle,
			btnArray: ['Yes', 'No']
		}, (param) => {
			callback(param);
		}));
	}

	reloadpage() {
		window.location.reload();
	}

	mainappRef(node) {
		window.addEventListener('resize', (evt) => {
			this.setState({ resizecount: this.state.resizecount++ });
		});
	}
	
	render() {
		if(this.state.hasError)
			return (
				<div className="offset-md-2 col-md-8" style={{marginTop: '20px'}}>
					<div className="text-center"><img src="../images/error.png" style={{background: 'transparent', width: '150px', height: '150px'}}/></div>
					<div className="text-center font-16 form-group semi-bold-custom">Something went wrong. We're sorry for the inconvenience!</div>
					<div className="alert alert-info">{this.state.errorStack.split('\n').map((item) => {
						return <div>{item}<br/></div>
					})}
					</div>
					<div className="text-center marginbottom-30">Kindly send a screenshot of this page to support for resolving this issue.</div>
					<div className="text-center form-group">
						<button type="button" className="btn btn-sm gs-btn-outline-success gs-list-add-btn" onClick={this.reloadpage}>Reload page now</button>
					</div>
					{this.props.app.enableoldui ? <div className="text-center">If reloading page does not solve the issue, Please <a href="/oldspa#/dashboard">switch to old interface</a> temporarily</div> : null}
				</div>
			);
		if(!this.props.app.initialLoaderFlag)
			return (
				<div className="col-md-12">
					<div style={{textAlign:"center"}} className="col-md-4">
						<div><div className="loader-overlay"></div><svg className="circular" viewBox="25 25 50 50"><circle className="path" cx="50" cy="50" r="20" fill="none" strokeWidth="3" strokeMiterlimit="10"/></svg></div>
					</div>
					<div className="col-md-4">
					</div>
				</div>
			);

		return (
			<>
				<ToastContainer />
				<Router  getUserConfirmation={this.getConfirmation} >
					<>
						{this.props.app.user.forcepasswordchange ? null : <NavigationBar app={this.props.app} updateAppState={this.props.updateAppState} resetReportFilter={this.props.resetReportFilter} openModal={this.openModal} updateUserDialerStatus={this.dialer ? this.dialer.updateUserDialerStatus : null} />}
						<div className="container-fluid"  style={{paddingTop: `${(document.getElementsByClassName('navbar').length > 0 ? document.getElementsByClassName('navbar')[0].clientHeight : '40')}px`}}>
							<div id="main" ref={this.mainappRef}>
								<RouterBar openModal={this.openModal} closeModal={this.closeModal} createOrEdit={this.createOrEdit} updateBookMark={this.updateBookMark} />
								<div>{this.renderModal(0)}</div>
							</div>
							<div id="date-body-cont"></div>
							{
								(this.props.app.feature.enableDialerIntegration && this.props.app.user.extension) ?
									<NewDialer wrappedComponentRef={c => {this.dialer = c}} app={this.props.app} pagejson={this.props.pagejson} updateAppState={this.props.updateAppState} openModal={this.openModal}/>
								:null
							}
						</div>
					</>
				</Router>
			</>
		);
	}
}


class NavigationBar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderflag: false,
			isuserdialeravailable: false,
			showSidePanel: false,
			showSidebarSubnav: false,
			showSidebarDashWidgetnav: false,
			sidebarsubnavmodule: []
		}
		this.onCompanyChange = this.onCompanyChange.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.updateUserCallStatus = this.updateUserCallStatus.bind(this);
		//this.showReportFolderNav = this.showReportFolderNav.bind(this);
		this.openExcelSyncOTP = this.openExcelSyncOTP.bind(this);
		this.monitorWindowResize = this.monitorWindowResize.bind(this);
		this.showhideSidePanel = this.showhideSidePanel.bind(this);
		this.renderMobileSubNavModules = this.renderMobileSubNavModules.bind(this);
		this.showMobileSubNavigation = this.showMobileSubNavigation.bind(this);
		this.outsideOnClick = this.outsideOnClick.bind(this);
		this.renderHomeWorkListNav = this.renderHomeWorkListNav.bind(this);
		this.showMobileDashboardWidget = this.showMobileDashboardWidget.bind(this);
		this.showUserNav = this.showUserNav.bind(this);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	componentWillMount() {
		setTimeout(() => {
			if($(window).outerWidth() < 992) {
				$(".nav-item a.dropdown-item, .nav-item a.custom-nav-link:not(.dropdown-toggle)").click(function(event) {
					$(".navbar-collapse").collapse('hide');
				});
			}
		}, 1000);
		this.monitorWindowResize()
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.app.extensionstatus != this.state.isuserdialeravailable) {
			this.setState({
				isuserdialeravailable: nextProps.app.extensionstatus
			});
		}
	}

	componentDidMount() {
		window.addEventListener("resize", this.monitorWindowResize);
	}
	
	monitorWindowResize() {
		this.setState({showMobileNav: window.innerWidth <= 760}, () => {
			if(!this.state.showMobileNav)
				this.outsideOnClick();
		});
	}

	openSupport () {
		axios.get('/support').then((response) => {
			if (response.data.message == 'success') {
				var win = window.open("", '_blank');
				if (!win)
					modalService['infoMethod']({
						header : 'Warning',
						body : "Popup Blocker is enabled! Please add this site to your exception list.",
						btnName : ['Ok']
					});
				else {
					win.location.href = response.data.url;
				}
			}
		});
	}

	openGSSupport () {
		var win = window.open("/gssupport", '_blank');
		if (!win)
			modalService['infoMethod']({
				header : 'Warning',
				body : "Popup Blocker is enabled! Please add this site to your exception list.",
				btnName : ['Ok']
			});
	}

	openExcelSyncOTP () {
		this.props.openModal({
			render: (closeModal) => {
				return <ExcelSyncModal app={this.props.app} openModal={this.openModal} closeModal={closeModal} />
			},
			className: {
				content: 'react-modal-custom-class-40',
				overlay: 'react-modal-overlay-custom-class'
			},
			confirmModal: true
		});
	}

	showBadgeNotification () {
		if(this.props.app.notifyAlert) {
			return (
				<li>
					<Link to="/dashboard" style={{'marginTop':'2px'}}><i className="fa fa-bell"><span className="badge notify">{this.props.app.notifyAlert}</span></i></Link>
				</li>
			)
		}
		return '';
	}

	showDashboardWidgets () {
		let { widgetArray } = this.props.app;

		if (widgetArray.length == 0)
			return null;

		if(this.state.showMobileNav)
			return (
				<li className="gs-main-navbar-list" onClick={this.showMobileDashboardWidget}>Dashboard</li>
			);

		return (
			<li className={`nav-item custom-nav-item dropdown`}>
				<a className="nav-link custom-nav-link dropdown-toggle" href="#" data-toggle="dropdown"><i className="nav-icon"></i><span className="navbar-fonttext">Dashboard</span><span className="fa fa-chevron-down ml-1 font-9"></span></a>
				<div className={`dropdown-menu column-1 navdropdownmenu`} style={{width: '275px'}}>
					<div className="container">
						<div className="li-type-Div">
							<span className="text-color-custom">Widgets</span>
						</div>

						{widgetArray.map((item, index) => {
							return (
								<Link className="dropdown-item custom-dropdown-item" to={`/dashboardwidget/${item.id}`} key={index}> {item.name}</Link>
							)
						})}
					</div>
				</div>
			</li>
		);
	}

	renderHomeWorkListNav() {
		if(this.state.showMobileNav)
			return (
				<>
					<li><Link className="gs-main-navbar-list-link" to="/home" onClick={this.outsideOnClick}>Home</Link></li>
					<li><Link className="gs-main-navbar-list-link" to="/dashboard" onClick={this.outsideOnClick}>Worklist</Link></li>
				</>
			)
		return (
			<>
				<li className="nav-item custom-nav-item">
					<Link className="nav-link custom-nav-link" to="/home"><i className=" nav-icon"></i><span className="navbar-fonttext">Home</span></Link>
				</li>
				<li className="nav-item custom-nav-item">
					<Link className="nav-link custom-nav-link" to="/dashboard"><i className=" nav-icon"></i><span className="navbar-fonttext">Worklist</span></Link>
				</li>
			</>
		);
	}

	showNavModules () {
		return this.props.app.accessNavigation.modules.map((module, index) => {
			if(module._featureaccess && module._checkedaccess) {
				let columnValue = [];
				module.child.map((submodule) => {
					if(submodule._checkedaccess)
						columnValue.push(submodule);
				});

				if(this.state.showMobileNav)
					return (
						<li key={index} className="gs-main-navbar-list" onClick={()=>this.showMobileSubNavigation(module)}>
							<span>{module.name.length > 2 ? `${module.name.charAt(0)}${module.name.slice(1).toLowerCase()}` : module.name}</span>
							<span className="fa fa-chevron-down ml-1 font-9"></span>
						</li>
					);

				return (
					<li className={`nav-item custom-nav-item dropdown ${(index+3 >= this.props.app.accessNavigation.modules.length) ? `gs-nav-item-${index+1}` : ''}`} key={index}>
						<a className="nav-link custom-nav-link dropdown-toggle" href="#" data-toggle="dropdown"><i className="nav-icon"></i><span className="navbar-fonttext">{module.name.length > 2 ? `${module.name.charAt(0)}${module.name.slice(1).toLowerCase()}` : module.name}</span><span className="fa fa-chevron-down ml-1  font-9"></span></a>
						<div className={`dropdown-menu column-${columnValue.length} navdropdownmenu`}>
							<div className="container">
								<div className="row w-100">
									{this.showNavSubModules(module)}
								</div>
							</div>
						</div>
					</li>
				);
			}
			return null;
		});	
	}
	
	showMobileSubNavigation(module) {
		this.setState((prevState) => {
			return {
				showSidebarSubnav: !prevState.showSidebarSubnav,
				sidebarsubnavmodule: prevState.showSidebarSubnav ? [] : module
			};
		});
	}

	showMobileDashboardWidget() {
		this.setState((prevState) => {
			return {
				showSidebarDashWidgetnav: !prevState.showSidebarDashWidgetnav
			};
		});
	}

	renderMobileSubNavModules() {
		if((!this.state.showSidebarSubnav && this.state.sidebarsubnavmodule.length == 0) && !this.state.showSidebarDashWidgetnav)
			return null;

		if(this.state.showSidebarDashWidgetnav)
			return (
				<>
					<li className="gs-main-navbar-list align-items-center justify-content-center" onClick={this.showMobileDashboardWidget} style={{backgroundColor: 'hsla(210, 29%, 36%, 1)'}}><span className="fa fa-chevron-left" style={{position: 'absolute', left: '32px'}}></span><span>Dashboard</span></li>
					<li className="gs-main-navbar-list">
						<span className="text-color-custom">Widgets</span>
					</li>
					<Fragment>
						{
							this.props.app.widgetArray.map((item, index) => {
								return (
									<li key={index}>
										<Link className="gs-main-navbar-list-link" to={`/dashboardwidget/${item.id}`} onClick={this.outsideOnClick}>{item.name}</Link>
									</li>
								)
							})
						}
					</Fragment>
				</>
			);

		return (
			<>
				<li className="gs-main-navbar-list align-items-center justify-content-center" onClick={this.showMobileSubNavigation} style={{backgroundColor: 'hsla(210, 29%, 36%, 1)'}}><span className="fa fa-chevron-left" style={{position: 'absolute', left: '32px'}}></span><span>{this.state.sidebarsubnavmodule.name}</span></li>
				<Fragment>
					{
						this.state.sidebarsubnavmodule.child.map((submodule, index) => {
							if(!submodule._checkedaccess)
								return null;
							return (
								<Fragment key={index}>
									<li className="gs-main-navbar-list">
										<span className="text-color-custom">{submodule.name}</span>
									</li>
									{this.showNavLinks(submodule)}
								</Fragment>
							);
						})
					}
				</Fragment>
			</>
		);
	}

	outsideOnClick() {
		document.body.style.overflow = 'auto';
		this.setState({
			showSidebarSubnav: false,
			showSidePanel: false,
			showSidebarDashWidgetnav: false,
			sidebarsubnavmodule: []
		});
	}

	showNavSubModules(module) {
		let colKey = [];
		module.child.map((submodule, index) => {
			if(submodule._checkedaccess)
				colKey.push(submodule);
		});
		return module.child.map((submodule, index) => {
			if(!submodule._checkedaccess)
				return null;
			return (
				<div className={`col-md-${colKey.length > 3 ? '4' : Math.round(12/colKey.length)} ${index > 0 ? 'menucol-splitup' : ''}`} key={index}>
					<div className="li-type-Div">
						<span className="text-color-custom">{submodule.name}</span>
					</div>
					{this.showNavLinks(submodule)}
				</div>
			);
		})
	}
	
	showNavLinks(submodule) {
		return submodule.links.map((link, index) => {
			if(!link.ischecked || !link._access)
				return null;

			if(link.type == 'folder') {
				return link.reportlinks.map((reportlink, reportindex) => {
					if(!reportlink.ischecked)
						return null;

					if(this.state.showMobileNav && this.state.showSidebarSubnav)
						return <li key={reportindex}><Link className="gs-main-navbar-list-link" to={`${reportlink.url}`} onClick={this.outsideOnClick}>{reportlink.name}</Link></li>

					return <Link className="dropdown-item custom-dropdown-item" to={`${reportlink.url}`} key={reportindex}> {reportlink.name}</Link>
				});
			} else {
				if(this.state.showMobileNav && this.state.showSidebarSubnav)
					return <li key={index}><Link className="gs-main-navbar-list-link" to={`${link.url}`} onClick={this.outsideOnClick}> {link.name}</Link></li>

				return <Link className="dropdown-item custom-dropdown-item" to={`${link.url}`} key={index}> {link.name}</Link>
			}
		});
	}

	showCompany() {
		let mobCompanyDropdownStyle = {}
		if(this.state.showMobileNav)
			mobCompanyDropdownStyle = {
				position: 'absolute',
				top: '38px'
			};

		return (
			<li className="nav-item custom-nav-item dropdown">
				<a className="nav-link custom-nav-link dropdown-toggle" href="#" data-toggle="dropdown"><i className="fa fa-building" style={{marginRight: '10px'}}></i><span className="navbar-fonttext">{this.props.app.selectedCompanyDetails.name}</span><span className="fa fa-chevron-down ml-1 font-9"></span></a>
				<div className="dropdown-menu dropdown-menu-right navdropdownmenu-right" style={mobCompanyDropdownStyle}>
					{
						this.props.app.companyArray.map((company, index) => {
							return (
								<a className="dropdown-item custom-dropdown-item" href="#/home" key={index} onClick={()=>{this.onCompanyChange(company.id)}}>{company.name}</a>
							);
						})
					}
				</div>
			</li>
		);
	}

	showUserNav() {
		let mobCompanyDropdownStyle = {}
		if(this.state.showMobileNav)
			mobCompanyDropdownStyle = {
				position: 'absolute',
				top: '38px'
			};

		return (
			<>
				<li className="nav-item custom-nav-item dropdown ">
					<a className="nav-link custom-nav-link dropdown-toggle" href="#" data-toggle="dropdown" style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '110px', fontWeight: '600'}}><i className="fa fa-user mr-1"></i>{this.props.app.user.displayname}<span className="fa fa-chevron-down ml-1 font-9"></span></a>
					<div className="dropdown-menu dropdown-menu-right navdropdownmenu-right" style={mobCompanyDropdownStyle}>
						<a className="dropdown-item custom-dropdown-item" href="#/changepassword">Change Password</a>
						<div className="li-type-Div"></div>
						{(this.props.app.feature.useProjects && this.props.app.feature.itemMasterExcelSyncRoles && checkArray(this.props.app.feature.itemMasterExcelSyncRoles,this.props.app.user.roleid)) ? <a className="dropdown-item custom-dropdown-item cursor-ptr" onClick={this.openExcelSyncOTP}>Excel Sync OTP</a> : null}
						{(this.props.app.feature.useProjects && this.props.app.feature.itemMasterExcelSyncRoles && checkArray(this.props.app.feature.itemMasterExcelSyncRoles,this.props.app.user.roleid)) ? <div className="li-type-Div"></div> : null}
						<a className="dropdown-item custom-dropdown-item" href="#/preferences">Preferences</a>
						<div className="li-type-Div"></div>
						{this.props.app.enableoldui == true ? <a className="dropdown-item custom-dropdown-item" href="/oldspa#/dashboard">Switch to Old UI</a> : null}
						{this.props.app.enableoldui ? <div className="li-type-Div"></div> : null}
						<a className="dropdown-item custom-dropdown-item cursor-ptr" onClick={this.openSupport}>Support</a>
						{window.location.host.split('.')[0] == 'gsprint' ? <a className="dropdown-item custom-dropdown-item" onClick={this.openGSSupport}>GS Support</a> : null}
						<div className="li-type-Div"></div>
						<a className="dropdown-item custom-dropdown-item" href="/logout">Logout</a>
					</div>
				</li>
			</>
		);
	}

	updateUserCallStatus() {
		if(this.props.updateUserDialerStatus){
			this.setState({
				isuserdialeravailable: !this.state.isuserdialeravailable
			}, () => {
				this.props.updateUserDialerStatus(this.state.isuserdialeravailable);
			});
		}else{
			this.props.openModal(modalService['infoMethod']({
				header : 'Error',
				body : 'Unable to connect with Dialer. Please contact admin.',
				btnArray : ['Ok']
			}));
		}
	}

	onCompanyChange(value) {
		this.updateLoaderFlag(true);
		axios({
			method : 'post',
			data : {
				companyid: value
			},
			url : '/companyChangeLogIn'
		}).then((response) => {
			if (response.data.message == 'success') {
				this.props.updateAppState('selectedcompanyid', value);
				this.props.updateAppState('user', {
					...this.props.app.user,
					selectedcompanyid: value
				});
				let selectedCompanyDetails;
				this.props.app.companyArray.map(function(a) {
					if(a.id == value)
						selectedCompanyDetails = a;
				});
				this.props.updateAppState('selectedCompanyDetails', selectedCompanyDetails);
				this.props.resetReportFilter();
			}
			this.updateLoaderFlag(false);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		});
	}

	showhideSidePanel() {
		this.setState((prevState) => {
			document.body.style.overflow = !prevState.showSidePanel ? 'hidden' : 'auto';
			return {
				showSidePanel: !prevState.showSidePanel
			}
		});
	}

	render() {
		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<nav className="navbar navbar-expand-lg navbar-dark custom-navbar-bg fixed-top main-navbar">
					<button type="button" className={`gs-navbar-icon ${this.state.showSidePanel ? 'active' : ''} ${this.state.showMobileNav ? 'd-block' : 'd-none'}`} onClick={()=>this.showhideSidePanel()}>
						<div className={`gs-main-navbar-hamburger-icon gs-main-navbar-hamburger-close1 ${this.state.showSidePanel ? 'open' : ''}`}>
							<div className="gs-main-navbar-hamburger_icon">
								<div className="gs-main-navbar-hamburger_line gs-main-navbar-hamburger_line-1"></div>
								<div className="gs-main-navbar-hamburger_line gs-main-navbar-hamburger_line-2"></div>
								<div className="gs-main-navbar-hamburger_line gs-main-navbar-hamburger_line-3"></div>
							</div>
						</div>
					</button>

					{/* <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span className="navbar-toggler-icon"></span>
					</button> */}
					<div className="collapse navbar-collapse" id="navbarNav">
						<ul className="navbar-nav mr-auto">
							{this.renderHomeWorkListNav()}
							{this.showDashboardWidgets()}
							<li className="nav-item custom-nav-item" style = {{color: '#fff', padding: '0px'}}>|</li>
							{this.showNavModules()}
						</ul>
						<ul className="navbar-nav">
							{this.showCompany()}
							{this.showUserNav()}
						</ul>
					</div>
					<div className={`${this.state.showSidePanel ? 'gs-main-navbar-overlay d-block' : 'd-none'}`} onClick={this.outsideOnClick}></div>
					<div className={`${this.state.showSidePanel ? 'd-block' : 'd-none'}`}>
						<div className="sidepanel" style={{width: this.state.showSidePanel ? '70%' : '0%'}}>
							<ul style={{padding:'0px'}}>
								{this.renderHomeWorkListNav()}
								{this.showDashboardWidgets()}
								{this.showNavModules()}
							</ul>
						</div>
						<div className="sidepanel" style={{width: this.state.showSidebarSubnav || this.state.showSidebarDashWidgetnav ? '70%' : '0%'}}>
							<ul style={{padding:'0px'}}>
								{this.renderMobileSubNavModules()}
							</ul>
						</div>
					</div>
					<div className={`${this.state.showMobileNav ? 'd-block' : 'd-none'}`}>
						<ul className="navbar-nav flex-row">
							{this.showCompany()}
							{this.showUserNav()}
						</ul>
					</div>
				</nav>
			</>
		);
	}
}

class AnnouncementDetail extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let iframeHeight = $(window).outerHeight() - 240;
		return(
			<div>
				<div className="react-modal-header text-center" style={{backgroundColor: 'rgba(26,188,156,0.1)'}}>
					<div className="gs-text-color" style={{fontSize: '25px'}}>Important Update! (20-Mar-2020)</div>
				</div>
				<div className="react-modal-body">
					<iframe src={`/getannouncement/${this.props.announcementref}`} width="650px" height={`${iframeHeight}px`} scrolling="auto" style={{border: 'none', overflowY: 'scroll'}}></iframe>
				</div>
				<div className="react-modal-footer text-center">
					<button type="button" className="btn btn-sm gs-btn-success" onClick={this.props.closeModal} style={{padding: '0.3rem 4rem'}}>Ok</button>
				</div>
			</div>
		)
	}
}

export default connect((state)=>{
	return {app : state.app, pagejson : state.pagejson}
}, (dispatch) => {
	return bindActionCreators({updateAppState, resetReportFilter}, dispatch);
})(App);
