import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm, formValueSelector } from 'redux-form';
import { createPortal } from 'react-dom';
import moment from 'moment';
import axios from 'axios';
import Avatar from 'react-avatar';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, search } from '../utils/utils';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter, datetimeFilter, arrayFilter } from '../utils/filter';
import { DateEle, InputEle, localSelectEle, autoSelectEle, ReportDateRangeField, checkboxEle, ButtongroupEle, autoMultiSelectEle } from '../components/formelements';
import ServiceEnginneerTimeLine from './serviceenginneertimeline';
import MapForm from '../components/servicecallallotmentmapviewcomponent';

import Loadingcontainer from '../components/loadingcontainer';

import Popover from 'react-popover';

import{CheckMouseEventOutSide} from '../reportbuilder/filtercomponents';

const InfoIconEle = React.forwardRef((props, ref) => {
	return (
		<span className="fa fa-info-circle gs-eng-assign-info-icon" ref={ref} style={{color: '#405FFF', cursor: 'pointer', padding: '10px', borderRadius: '50%'}} onClick={(event) => {event.stopPropagation();props.onMouseEnter()}}></span>
	);
});

class ServiceEngineerAssignment extends Component {
	filterElePortalNode = '';
	constructor(props) {
		super(props);
		this.state = {
			firstTime: true,
			search: {},
			height: this.props.height > 25 ? this.props.height : 60,
			hourWidth: this.props.hourWidth > 25 ? this.props.hourWidth : 90,
		};

		this.filterEleBtnRef = React.createRef();

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getEngineers = this.getEngineers.bind(this);
		this.renderServiceCallDetails = this.renderServiceCallDetails.bind(this);
		this.allocationdateOnChange = this.allocationdateOnChange.bind(this);
		this.prev = this.prev.bind(this);
		this.next = this.next.bind(this);
		this.getServiceAllocationDetails = this.getServiceAllocationDetails.bind(this);
		this.renderGanttTimeline = this.renderGanttTimeline.bind(this);
		this.selector = formValueSelector(this.props.form);
		this.selectEmployee = this.selectEmployee.bind(this);
		this.selectServiceCall = this.selectServiceCall.bind(this);
		this.createAllocation = this.createAllocation.bind(this);
		this.onTimelineChange = this.onTimelineChange.bind(this);
		this.viewswitchOnChange = this.viewswitchOnChange.bind(this);
		this.onAllocationSelect = this.onAllocationSelect.bind(this);
		this.renderServiceEngineer = this.renderServiceEngineer.bind(this);
		this.renderServiceCallListDetails = this.renderServiceCallListDetails.bind(this);
		this.createListViewAllocation = this.createListViewAllocation.bind(this);
		this.renderFilter = this.renderFilter.bind(this);
		this.showInfoDetails = this.showInfoDetails.bind(this);
		this.renderServiceCallEngineerInfo = this.renderServiceCallEngineerInfo.bind(this);
		this.openServiceCall = this.openServiceCall.bind(this);
		this.searchFilter = this.searchFilter.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
		this.clearFilter = this.clearFilter.bind(this);
		this.allocationStatusOnChange = this.allocationStatusOnChange.bind(this);
		this.handleFilerBtnOnClick = this.handleFilerBtnOnClick.bind(this);
		this.getAttendanceCaptureDetails = this.getAttendanceCaptureDetails.bind(this);
	};

	componentWillMount() {
		document.getElementById("pagetitle").innerHTML = `Service Engineer Assignment`;
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	componentDidMount() {
		window.addEventListener('scroll', this.handleScroll);
		this.filterElePortalNode = document.createElement('div');
		this.filterElePortalNode.className = "servicecallengineerassignment_filter";
		document.body.appendChild(this.filterElePortalNode);
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll);
		document.body.removeChild(this.filterElePortalNode);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		this.updateLoaderFlag(true);
		let tempObj = {
			allocationdate: new Date(),
			viewtype: 'All Activities',
			viewswitch: 'timeline',
			showdetails: true,
			filter: {
				calltype: 'All',
				allocationstatus: 'All'
			},
			openServiceCallDetails: [],
			engineerDetails: [],
			serviceAllocationDetails: []
		};

		axios.get(`/api/servicecalls?field=id,servicecallno,servicecalldate,calltype,customerid,partners/name/customerid,engineerid,users/displayname/engineerid,installationaddress,complaintcategoryid,complaintcategory/name/complaintcategoryid,appointmentdate,nextfollowup,status,installationaddressid,addresses/displayname/installationaddressid,addresses/displayaddress/installationaddressid,addresses/city/installationaddressid,addresses/secondline/installationaddressid,addresses/latitude/installationaddressid,addresses/longitude/installationaddressid&filtercondition=servicecalls.status IN ('Open', 'Assigned')`).then((response) => {
			if(response.data.message == 'success') {
				tempObj.openServiceCallDetails = response.data.main;
				this.getEngineers(tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				this.updateLoaderFlag(false);
			}
		});
	}

	getEngineers(tempObj) {
		axios.get(`/api/query/employeelocationsquery?locationdate=${new Date(new Date().setHours(0,0,0,0))}`).then((response) => {
			if(response.data.message == 'success') {
				let filteredArray = response.data.main.filter(item => item.isengineer);
				filteredArray.sort((a, b) => a.userid - b.userid);
				filteredArray.splice(0, 0, {userid: null, userid_displayname: 'Not Assigned'});
				tempObj.engineerDetails = filteredArray.map((item) => {
					return {
						id: item.userid,
						userid_displayname: item.userid_displayname,
						userid_profileimageurl: item.userid_profileimageurl,
						allvalue: {
							...item
						}
					};
				});
				tempObj.refreshcount = 1;
				tempObj.showdetails = true;
				tempObj.mapviewfilter = 'Calls and Engineers';
				this.props.initialize(tempObj);
				setTimeout(() => {
					this.getAttendanceCaptureDetails();
				}, 0);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getAttendanceCaptureDetails() {
		let employeeidArr = [];

		this.props.resource.engineerDetails.forEach(item => {
			if(item.allvalue && item.allvalue.employeeid) {
				employeeidArr.push(item.allvalue.employeeid);
			}
		});

		axios.get(`/api/attendancecapture?field=id,employeeid,leave,leavetype,intime,outtime,employees/workstarttime/employeeid,employees/workendtime/employeeid&filtercondition=attendancecapture.date::DATE='${new Date(this.props.resource.allocationdate).toDateString()}'::DATE AND attendancecapture.employeeid IN (${employeeidArr.join()})`).then((response) => {
			if(response.data.message == 'success') {
				let employeeObj = {};
				let engineerDetails = [...this.props.resource.engineerDetails];
				response.data.main.forEach(item => {
					employeeObj[item.employeeid] = item;
				});

				engineerDetails.forEach(eng => {
					eng.allvalue.employeeid_leave = employeeObj[eng.allvalue.employeeid] ? employeeObj[eng.allvalue.employeeid].leave : null;
					eng.allvalue.employeeid_leavetype = employeeObj[eng.allvalue.employeeid] ? employeeObj[eng.allvalue.employeeid].leavetype : null;
					eng.allvalue.employeeid_intime = employeeObj[eng.allvalue.employeeid] ? employeeObj[eng.allvalue.employeeid].intime : null;
					eng.allvalue.employeeid_outtime = employeeObj[eng.allvalue.employeeid] ? employeeObj[eng.allvalue.employeeid].outtime : null;
					eng.allvalue.employeeid_workstarttime = employeeObj[eng.allvalue.employeeid] ? employeeObj[eng.allvalue.employeeid].employeeid_workstarttime : null;
					eng.allvalue.employeeid_workendtime = employeeObj[eng.allvalue.employeeid] ? employeeObj[eng.allvalue.employeeid].employeeid_workendtime : null;
				});

				this.props.updateFormState(this.props.form, { engineerDetails });

				setTimeout(this.getServiceAllocationDetails, 0);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	onTimelineChange(activity, plannedstarttime, plannedendtime) {
		let activityindex = -1;
		for(var i=0;i<this.props.resource.serviceAllocationDetails.length;i++) {
			if(this.props.resource.serviceAllocationDetails[i].id == activity.id) {
				activityindex = i;
				break;
			}
		}

		if(activityindex >= 0) {
			this.props.updateFormState(this.props.form, {
				[`serviceAllocationDetails[${activityindex}].start`]: plannedstarttime,
				[`serviceAllocationDetails[${activityindex}].plannedstarttime`]: plannedstarttime,
				[`serviceAllocationDetails[${activityindex}].end`]: plannedendtime,
				[`serviceAllocationDetails[${activityindex}].plannedendtime`]: plannedendtime
			});
			this.generateTimeLine();
		}

		this.updateLoaderFlag(true);
		let tempObj = {
			...activity,
			plannedstarttime,
			plannedendtime
		};

		axios({
			method: 'post',
			data: {
				actionverb: 'Update PlannedTime',
				data: tempObj
			},
			url: '/api/salesactivities'
		}).then((response) => {
			if (response.data.message != 'success') {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				this.updateLoaderFlag(true);
			}
			this.getServiceAllocationDetails();
			this.updateLoaderFlag(false);
		});
	}

	searchFilter(param) {
		this.updateLoaderFlag(true);
		let openServiceCallDetails = [], errorArray = [], calltypArr = [];

		let filterString = '';
		if(this.props.resource.filter && this.props.resource.filter.territoryid && this.props.resource.filter.territoryid.length > 0)
			filterString += ` AND servicecalls.territoryid IN (${this.props.resource.filter.territoryid})`;

		if (this.props.resource.filter && this.props.resource.filter.fromdate && this.props.resource.filter.todate) {
			if(new Date(this.props.resource.filter.todate).setHours(0, 0, 0, 0) < new Date(this.props.resource.filter.fromdate).setHours(0, 0, 0, 0)) {
				errorArray.push(`Service Call from date can't be more than to date`);
			} else {
				filterString += ` AND servicecalls.servicecalldate::DATE between '${new Date(this.props.resource.filter.fromdate).toDateString()}'::DATE AND '${new Date(this.props.resource.filter.todate).toDateString()}'::DATE`;
			}
		}

		if(this.props.resource.filter.calltype == 'All')
			calltypArr.push(`'PMS', 'Installation', 'Service'`);
		if(this.props.resource.filter.calltype == 'PMS')
			calltypArr.push(`'PMS'`);
		if(this.props.resource.filter.calltype == 'Installation')
			calltypArr.push(`'Installation'`);
		if(this.props.resource.filter.calltype == 'Service')
			calltypArr.push(`'Service'`);

		if(calltypArr.length > 0) {
			filterString += ` AND servicecalls.calltype = ANY(array[${calltypArr}])`;
		}

		if(this.props.resource.filter.allocationstatus == 'Not Scheduled')
			filterString += ` AND servicecalls.nextfollowup IS NULL`;
		if(this.props.resource.filter.allocationstatus == 'Scheduled')
			filterString += ` AND servicecalls.nextfollowup IS NOT NULL`;

		if(errorArray.length > 0) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : errorArray,
				btnArray : ["Ok"]
			}));
		}

		axios.get(`/api/servicecalls?field=id,servicecallno,servicecalldate,calltype,customerid,partners/name/customerid,engineerid,users/displayname/engineerid,installationaddress,complaintcategoryid,complaintcategory/name/complaintcategoryid,appointmentdate,nextfollowup,status,installationaddressid,addresses/displayname/installationaddressid,addresses/latitude/installationaddressid,addresses/longitude/installationaddressid&filtercondition=servicecalls.status IN ('Open', 'Assigned')${filterString}`).then((response) => {
			if(response.data.message == 'success') {
				this.props.updateFormState(this.props.form, {
					openServiceCallDetails: response.data.main
				});
				if(!param)
					this.setState((prevState) => ({
						showFilterComponent: !prevState.showFilterComponent 
					}));
				setTimeout(this.getServiceAllocationDetails, 0);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	renderServiceCallDetails() {
		let serviceCallArray = arrayFilter((this.props.resource.openServiceCallDetails || []), this.props.resource.servicecallsearch, ['customerid_name', 'installationaddress', 'calltype', 'servicecallno']);

		if(serviceCallArray.length == 0)
			return (
				<div className="d-flex align-items-center justify-content-center" style={{borderTop: '1px solid rgba(0,0,0,.125)', padding: '6px 12px', height: '100%'}}>
					<span>{`There is ${this.state.search.customerid_name ? ' no search result' : 'no service call'} found`}</span>
				</div>
			);

		return serviceCallArray.map((svccall, callindex) => {
			let callstatus = svccall.nextfollowup ? 'Scheduled' : 'Not Scheduled';
			svccall.temprefindex = callindex;

			return (
				<div key={svccall.id} style={{padding: '6px 12px', cursor: 'pointer', borderBottom: svccall.isselected || (this.props.resource.selectedservicecallid == svccall.id) ? '1px solid #405FFF' : '1px solid rgba(214, 214, 214, .5)', borderTop: svccall.isselected || (this.props.resource.selectedservicecallid == svccall.id) ? '1px solid #405FFF' : '', borderLeft: svccall.isselected || (this.props.resource.selectedservicecallid == svccall.id) ? '1px solid #405FFF' : '', borderRight: svccall.isselected || (this.props.resource.selectedservicecallid == svccall.id) ? '1px solid #405FFF' : '', backgroundColor : svccall.isselected ? '#f8f9fe' : ''}} onClick={(evt)=> {this.selectServiceCall(svccall);}}>
					<div className="mb-2 semi-bold" style={{color: '#042140'}}>{svccall.customerid_name}</div>
					<div className="mb-2">
						<span style={{color: 'rgba(5, 33, 64, .7)'}}>{datetimeFilter(svccall.servicecalldate)}</span>
						<span className={`float-right`} style={{borderRadius: '2px', paddingLeft: '5px', paddingRight: '5px', fontSize: '12px', color: `${svccall.nextfollowup ? '#C28529' : '#506479'}`, backgroundColor: `${svccall.nextfollowup ? 'rgba(194, 133, 41, 0.1)' : 'rgba(80, 100, 121, 0.1)'}`}}>{callstatus}</span>
					</div>
					<div className="mb-2" style={{color: 'rgba(5, 33, 64, .7)'}}>{svccall.installationaddress}</div>
					<div className="d-flex flex-row justify-content-between">
						<div className="font-12">
							<span style={{color: 'rgba(5, 33, 64, .7)'}}>{svccall.servicecallno}</span>
							{svccall.calltype ? <span style={{padding: '2px 5px', color: '#405FFF', backgroundColor: 'rgba(64, 95, 255, .1)', marginLeft: '10px', borderRadius: '4px'}}>{svccall.calltype}</span> : null}
						</div>
						<div>
							<Popover
								className = {'gs-serviceassignement-popover'}
								preferPlace = {'end'}
								place={'end'}
								enterExitTransitionDurationMs = {100}
								offset={4}
								isOpen = {svccall.showInfoDetails}
								tipSize = {0.1}
								onOuterAction = {(mouseevent) => {
									if(CheckMouseEventOutSide(mouseevent))
										this.handleScroll();
								}}
								body = {this.renderServiceCallInfoDetails(svccall, 'timeline')}
							>
								<InfoIconEle onMouseEnter={() => this.showInfoDetails(svccall)} />
							</Popover>
						</div>
					</div>
				</div>
			);
		});
	}

	showInfoDetails(svccall) {
		this.props.resource.openServiceCallDetails.forEach(item => {
			item.showInfoDetails = item.id == svccall.id ? !item.showInfoDetails : false;
		});
		this.props.updateFormState(this.props.form, {
			openServiceCallDetails: this.props.resource.openServiceCallDetails,
			refresh: !this.props.resource.refresh
		});
	}

	renderServiceCallInfoDetails(svccall, param) {
		return (
			<div className={`${param == 'timeline' ? 'gs-eng-assign-info-modal' : 'gs-eng-assign-list-info-modal'}`}>
				<div className="row no-gutters">
					<div className="col-md-12">
						<div>
							<span className="semi-bold">{svccall.customerid_name}</span>
							<span className="float-right gs-eng-assign-info-icon" style={{padding:'5px',border: '1px solid #ddd', borderRadius: '3px', color: '#444444'}} onClick={()=>this.openServiceCall(svccall)}>{svccall.servicecallno}<i className="fa fa-share-square-o ml-2"></i></span>
						</div>
						<div className="form-group">{svccall.installationaddress}</div>
						{this.renderServiceCallEngineerInfo(svccall)}
					</div>
				</div>
			</div>
		);
	}

	handleScroll() {
		this.props.resource.openServiceCallDetails.forEach(item => {
			item.showInfoDetails = false;
		});
		this.props.updateFormState(this.props.form, {
			openServiceCallDetails: this.props.resource.openServiceCallDetails,
			refresh: !this.props.resource.refresh
		});
	}

	openServiceCall(svccall) {
		this.props.history.push(`/details/servicecalls/${svccall.id}`);
	}

	renderServiceCallEngineerInfo(svccall) {
		return this.props.resource.serviceAllocationDetails.map((item, index) => {
			if(item.parentid == svccall.id) {
				let statusClass = ['Open', 'Planned'].includes(item.status) ? 'gs-bg-info text-primary' : (item.status == 'Inprogress' ? 'gs-bg-warning gs-text-warning' : 'gs-bg-success text-success');  
				return (
					<div key={index} className="form-group">
						<div>{item.assignedto_displayname}</div>
						<span className="font-12 text-muted">{`${datetimeFilter(item.start)} - ${datetimeFilter(item.end)}`}</span>
						<span className={`${statusClass} float-right font-12`}>{item.status}</span>
					</div>
				);
			}
		});
	}
 
	allocationdateOnChange(value) {
		this.props.updateFormState(this.props.form, {
			serviceAllocationDetails: []
		});
		this.updateLoaderFlag(true);
		setTimeout(this.getAttendanceCaptureDetails, 0);
	}

	prev() {
		let { resource } = this.props;
		let tempAllocationdate = new Date(new Date(resource.allocationdate).setDate(new Date(resource.allocationdate).getDate() - 1));
		this.props.updateFormState(this.props.form, {
			allocationdate: tempAllocationdate,
			serviceAllocationDetails: []
		});

		this.updateLoaderFlag(true);
		setTimeout(this.getAttendanceCaptureDetails, 0);
	}

	next() {
		let { resource } = this.props;
		let tempAllocationdate = new Date(new Date(resource.allocationdate).setDate(new Date(resource.allocationdate).getDate() + 1));
		this.props.updateFormState(this.props.form, {
			allocationdate: tempAllocationdate,
			serviceAllocationDetails: []
		});

		this.updateLoaderFlag(true);
		setTimeout(this.getAttendanceCaptureDetails, 0);
	}

	getServiceAllocationDetails() {
		//this.updateLoaderFlag(true);
		if(this.timeoutFn)
			clearTimeout(this.timeoutFn);

		let filterString = `(salesactivities.plannedstarttime::date='${new Date(this.props.resource.allocationdate).toDateString()}'::date OR salesactivities.startdatetime::date='${new Date(this.props.resource.allocationdate).toDateString()}'::date)`;

		if(this.props.resource.viewtype == 'Service Assignment')
			filterString += ` AND activitytypes_activitytypeid.category='Service Allocation'`;

		let latestMTime = null;
		(this.props.resource.serviceAllocationDetails || []).forEach((item) => {
			if(!latestMTime || item.modified > latestMTime)
				latestMTime = item.modified;
		});

		if(latestMTime)
			filterString += ` AND salesactivities.modified::timestamp with time zone > '${new Date(latestMTime).toISOString()}'::timestamp with time zone`;

		axios.get(`/api/salesactivities?field=id,activitytypeid,assignedto,status,plannedstarttime,plannedendtime,travelstarttime,travelendtime,startdatetime,enddatetime,customerid,contactid,partners/name/partnerid,contacts/name/contactid,users/displayname/assignedto,activitytypes/name/activitytypeid,activitytypes/category/activitytypeid,modified,parentresource,parentid,servicecallid,servicecalls/servicecallno/servicecallid,servicecalls/calltype/servicecallid,relatedactivityid,checkindetails&filtercondition=${filterString}`).then((response) => {
			if(response.data.message == 'success') {
				let serviceAllObj = {};
				(this.props.resource.serviceAllocationDetails || []).forEach((item) => {
					serviceAllObj[item.id] = item;
					if(!latestMTime || item.modified > latestMTime)
						latestMTime = item.modified;
				});

				let tempServiceCallDetails = {};
				this.props.resource.openServiceCallDetails.forEach(item => {
					tempServiceCallDetails[item.id] = item;
				});

				let serviceAllocationDetails = [];

				response.data.main.forEach(item => {
					item.serviceCallDetails = tempServiceCallDetails[item.servicecallid] ? tempServiceCallDetails[item.servicecallid] : null;

					let timeStart = item.plannedstarttime ? new Date(item.plannedstarttime).getTime() : 0;
					let timeEnd = item.plannedendtime ? new Date(item.plannedendtime).getTime() : 0;
					let timeDiff = Math.abs(timeEnd - timeStart);

					let minutes = Math.floor(timeDiff / 1000 / 60);

					if(item.status == 'Planned' && new Date(item.plannedstarttime).setHours(0,0,0,0) == new Date(this.props.resource.allocationdate).setHours(0,0,0,0)) {
						serviceAllObj[item.id] = item;
						//serviceAllocationDetails.push(item);
						item.start = item.plannedstarttime;
						item.end = item.plannedendtime ? item.plannedendtime : new Date(new Date(item.plannedstarttime).setHours(new Date(item.plannedstarttime).getHours() + 1))
					}

					if(['Inprogress', 'Completed'].includes(item.status) && new Date(item.startdatetime).setHours(0,0,0,0) == new Date(this.props.resource.allocationdate).setHours(0,0,0,0)) {
						serviceAllObj[item.id] = item;
						//serviceAllocationDetails.push(item);
						item.start = item.travelstarttime ? item.travelstarttime : item.startdatetime;
						item.end = item.enddatetime ? item.enddatetime : new Date(new Date(item.start).getTime() + minutes * 60000);
					}
				});

				for (var prop in serviceAllObj)
					serviceAllocationDetails.push(serviceAllObj[prop]);

				this.props.updateFormState(this.props.form, {
					serviceAllocationDetails,
					refreshcount: this.props.resource.refreshcount + 1
				});
				this.generateTimeLine();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
			this.timeoutFn = setTimeout(this.getServiceAllocationDetails, 20000);
		});
	}

	selectEmployee(engineer) {
		let engineerDetails = [...this.props.resource.engineerDetails];
		let selectedengineerid = null;

		engineerDetails.forEach(item=> {
			item.isselected = item.id == engineer.id ? !item.isselected : false;
			if (item.isselected) {
				selectedengineerid = item.id;
			}
		});

		this.props.updateFormState(this.props.form, {
			engineerDetails,
			selectedengineerid,
			refresh: !this.props.resource.refresh
		});
	}

	selectServiceCall(svccall) {
		let openServiceCallDetails = [...this.props.resource.openServiceCallDetails];
		let selectedservicecallid = null, selectedcustomerid = null;

		openServiceCallDetails.forEach(item => {
			item.isselected = item.id == svccall.id ? !item.isselected : false;
			if (item.isselected) {
				selectedservicecallid = item.id;
				selectedcustomerid = item.customerid;
			}
		});

		this.props.updateFormState(this.props.form, {
			openServiceCallDetails,
			selectedservicecallid,
			selectedcustomerid,
			refresh: !this.props.resource.refresh
		});
	}

	createAllocation(engineer) {
		/*if(!this.props.resource.selectedservicecallid)
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : ['Please select Service Call and then continue'],
				btnArray : ['Ok']
			}));*/

		this.props.createOrEdit('/createActivity', null, {
			parentid: this.props.resource.selectedservicecallid,
			servicecallid: this.props.resource.selectedservicecallid,
			partnerid: this.props.resource.selectedcustomerid,
			plannedstarttime: this.props.resource.selectedplannedstarttime,
			assignedto: engineer ? engineer.id : this.props.resource.selectedengineerid,
			parentresource: this.props.resource.selectedservicecallid ? 'servicecalls' : null,
			tempactivitycategory: this.props.resource.selectedservicecallid ? 'Service Allocation' : null,
			param: this.props.resource.selectedservicecallid ? 'servicecalls' : null
		}, (valueObj) => {
			this.updateLoaderFlag(true);
			this.searchFilter(true);
		});
	}

	onAllocationSelect(activity) {
		let activitycallback = () => {
			this.updateLoaderFlag(true);
			this.getServiceAllocationDetails();
		}
		this.props.createOrEdit('/details/activities/:id',  activity.id , {}, activitycallback, activitycallback);
	}

	generateTimeLine() {
		let employeeAllocationObj = {}, employeeStepObj = {}, allocationStepObj = {};

		let resource = {
			serviceAllocationDetails: this.selector(this.props.fullstate, 'serviceAllocationDetails'),
			engineerDetails: this.selector(this.props.fullstate, 'engineerDetails')
		};

		resource.serviceAllocationDetails.forEach(item => {
			if(!employeeAllocationObj[item.assignedto]) {
				employeeAllocationObj[item.assignedto] = [];
				employeeStepObj[item.assignedto] = 1;
				allocationStepObj[item.assignedto] = {};
			}

			employeeAllocationObj[item.assignedto].push({val: item}); 
		});

		resource.engineerDetails.forEach((item) => {
			let allocationArray = employeeAllocationObj[item.id] ? employeeAllocationObj[item.id] : [];

			allocationArray.sort((a, b) => new Date(a.val.start) > new Date(b.val.start) ? 1 : (new Date(a.val.start) < new Date(b.val.start) ? -1 : 0));

			let prevallocation;
			employeeStepObj[item.id] = 1;

			allocationArray.forEach(allocation => {
				let newStep;
				
				if(!prevallocation) {
					newStep = 1;
					allocationStepObj[allocation.val.assignedto][allocation.val.id] = 1;
				} else {
					if(new Date(allocation.val.start) >= new Date(prevallocation.val.start) && new Date(allocation.val.start) <= new Date(prevallocation.val.end)) {
						newStep = allocationStepObj[allocation.val.assignedto][prevallocation.val.id] + 1;
					} else {
						newStep = 1;
						allocationStepObj[allocation.val.assignedto][allocation.val.id] = 1;
					}
				}

				if(newStep > employeeStepObj[item.id])
					employeeStepObj[item.id] = newStep;

				allocationStepObj[allocation.val.assignedto][allocation.val.id] = newStep;

				prevallocation = allocation;

				let startDate = new Date(allocation.val.start);
				let endDate = new Date(allocation.val.end);

				let dayoverlap = `${startDate.getYear()}-${startDate.getMonth()}-${startDate.getDate()}` != `${endDate.getYear()}-${endDate.getMonth()}-${endDate.getDate()}` 

				let leftStartX = Number(((((startDate.getHours() * 60) + startDate.getMinutes()) / 60) * this.state.hourWidth).toFixed());
				let leftEndX = Number((((dayoverlap ? (24 * 60) : ((endDate.getHours() * 60) + endDate.getMinutes())) / 60) * this.state.hourWidth).toFixed());

				allocation.x = leftStartX;
				allocation.y = 0;
				allocation.width = leftEndX - leftStartX;

				if(newStep > 1)
					allocation.y = this.state.height * (newStep -1);
			});
		});

		//console.log(employeeAllocationObj, employeeStepObj, allocationStepObj);

		let firstTime = this.state.firstTime;
		this.setState({ employeeAllocationObj, employeeStepObj, allocationStepObj, allocations: this.state.allocations, firstTime: false }, () => {
			if(!this.refs.timeline)
				return null;

			/*Object.keys(employeeAllocationObj).forEach(key => {
				employeeAllocationObj[key].forEach(item => {
					//this.refs.timeline.updatePosition(item);
				});
			});*/
			if(firstTime)
				$(".gs-gantt-timeline").scrollLeft(689);
		});
	}

	viewswitchOnChange(value) {
		this.props.updateFormState(this.props.form, {
			mapviewfilter : this.props.resource.mapviewfilter ? this.props.resource.mapviewfilter : 'Calls and Engineers',
			allocationdate: this.props.resource.allocationdate ? new Date(this.props.resource.allocationdate) : new Date(),
			serviceAllocationDetails: []
		});

		setTimeout(() => this.getServiceAllocationDetails(), 0);
	}

	createListViewAllocation(svccall) {
		this.props.updateFormState(this.props.form, {
			selectedservicecallid: svccall.id,
			selectedcustomerid: svccall.customerid
		});
		setTimeout(this.createAllocation, 0);
	}

	renderGanttTimeline() {
		return (
			<ServiceEnginneerTimeLine 
				ref="timeline"
				change= {{
					allocationDate: this.allocationdateOnChange,
					viewtype: this.getServiceAllocationDetails,
					prev: this.prev,
					next: this.next
				}}
				resource={this.props.resource}
				employees={this.props.resource.engineerDetails}
				employeeAllocationObj={this.state.employeeAllocationObj}
				employeeStepObj={this.state.employeeStepObj}
				allocationStepObj={this.state.allocationStepObj}
				selectEmployee={this.selectEmployee}
				onAllocationSelect={this.onAllocationSelect}
				createAllocation={this.createAllocation}
				onTimelineChange={this.onTimelineChange}
				updateFormState = {this.props.updateFormState}
				form = {this.props.form} />
		);
	}

	renderServiceEngineer() {
		let serviceEngineerArray = arrayFilter((this.props.resource.engineerDetails || []), this.props.resource.serviceengineersearch, ['userid_displayname']);

		serviceEngineerArray.forEach(engineer => {
			engineer.totalallocation = 0;
			this.props.resource.serviceAllocationDetails.forEach(allocation => {
				if(allocation.assignedto == engineer.id) {
					engineer.totalallocation += 1;
				}
			});
		});

		if(serviceEngineerArray.length == 0)
			return (
				<div className="d-flex align-items-center justify-content-center" style={{borderTop: '1px solid rgba(0,0,0,.125)', padding: '6px 12px', height: '100%'}}>
					<span>{`There is engineer found`}</span>
				</div>
			);

		return serviceEngineerArray.map((engineer, engineerindex) => {
			if(!engineer.id)
				return null;

			return (
				<div key={engineer.id} style={{width: '100%', padding: '12px 21px', cursor: 'pointer', backgroundColor: engineer.isselected ? 'rgba(26,188,156,0.1)' : ''}} onClick={()=>this.selectEmployee(engineer)}>
					<div className={`d-flex flex-row justify-content-center align-items-center h-100`}>
						<div style={{marginRight: '15px'}}>
							<Avatar size="35" round="50%" name={engineer.userid_displayname} src={`/documents/getfile?url=${engineer.userid_profileimageurl}`} />
						</div>
						<div style={{flex: '1 1 auto', color: '#042140'}}>
							<div>{engineer.userid_displayname}</div>
							<div style={{color: engineer.allvalue.intime ? '#259F79' : '#EE607A', fontSize: '12px'}}>{engineer.allvalue.intime ? 'Checked in' : 'Not Checked in'}</div>
						</div>
						{engineer.totalallocation && engineer.totalallocation > 0 ? <div style={{width: '22px', height: '22px', lineHeight: '20px', backgroundColor: 'rgba(64, 95, 255, .1)', borderRadius: '50%', color: '#405FFF', display: 'flex', alignItems: 'center', justifyContent: 'center', fontSize: '12px'}}>{engineer.totalallocation}</div> : null}
					</div>
				</div>
			);
		});
	}

	renderServiceCallListDetails() {
		let serviceCallArray = arrayFilter((this.props.resource.openServiceCallDetails || []), this.props.resource.servicecallsearch, ['customerid_name', 'installationaddress', 'calltype', 'servicecallno']);

		if(serviceCallArray.length == 0)
			return (
				<tr>
					<td colSpan="8" className="text-center">
						<span>{`There is ${this.state.search.customerid_name ? ' no search result' : 'no service call'} found`}</span>
					</td>
				</tr>
			);

		return serviceCallArray.map((svccall, callindex) => {
			let callstatus = svccall.nextfollowup ? 'Scheduled' : 'Not Scheduled';
			svccall.temprefindex = callindex;
			return (
				<tr key={svccall.id}>
					<td className="text-center">{svccall.servicecallno}</td>
					<td className="text-center nowrap">{dateFilter(svccall.servicecalldate)}</td>
					<td>{svccall.customerid_name}</td>
					<td className="text-center">{svccall.calltype}</td>
					<td>{svccall.installationaddress}</td>
					<td className="nowrap text-center">
						<span className={`${svccall.nextfollowup ? 'gs-bg-warning gs-text-warning': 'gs-bg-default text-default'}`} style={{borderRadius: '2px', paddingLeft: '5px', paddingRight: '5px', fontSize: '12px', color: `${svccall.nextfollowup ? '#C28529' : '#506479'}`, backgroundColor: `${svccall.nextfollowup ? 'rgba(194, 133, 41, 0.1)' : 'rgba(80, 100, 121, 0.1)'}`}}>{callstatus}</span>
					</td>
					<td>
						<div className="d-inline-block">
							<Popover
								className = {'gs-serviceassignement-popover'}
								preferPlace = {'left'}
								place={'left'}
								enterExitTransitionDurationMs = {100}
								offset={4}
								isOpen = {svccall.showInfoDetails}
								tipSize = {0.1}
								onOuterAction = {(mouseevent) => {
									if(CheckMouseEventOutSide(mouseevent))
										this.handleScroll();
								}}
								body = {this.renderServiceCallInfoDetails(svccall, 'list')}
							>
								<InfoIconEle onMouseEnter={() => this.showInfoDetails(svccall)} />
							</Popover>
						</div>
					</td>
					<td className="text-center"><button type="button" className="btn btn-sm gs-btn-outline-danger" onClick={()=>this.createListViewAllocation(svccall)} style={{fontSize: '12px', padding: '4px 25px'}}>Allocate</button></td>
				</tr>
			);
		});
	}

	handleFilterModalOutsideClick = evt => {
		if (this.filterEleBtnRef && !this.filterEleBtnRef.current.contains(evt.target)) {
			this.handleFilerBtnOnClick();
		}
	};

	handleFilerBtnOnClick() {
		this.setState(state => ({
			showFilterComponent: !state.showFilterComponent
		}));
	}

	renderFilter() {
		if(!this.state.showFilterComponent || !this.filterEleBtnRef)
			return null;

		const rect = this.filterEleBtnRef.current.getBoundingClientRect();

		let filterModalHeight = $(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - 10;

		let filterModalContainer =  (
			<ServiceEngineerAssignmentFilter onOutsideClick={this.handleFilterModalOutsideClick}>
				<div className="gs-eng-filter-modal" style={{ left: rect.left - 100, top: rect.bottom + 10, maxHeight: `${filterModalHeight}px`, overflow: 'scroll' }}>
					<div className="row no-gutters">
						<div className="col-md-12 form-group" style={{paddingBottom: '10px', borderBottom: '1px solid #D6D6D6'}}>
							<span className="gs-uppercase" style={{color: '#1ABC9C'}}>Filters</span>
							{Object.keys(this.props.resource.filter).length > 0 ? <span className="gs-anchor float-right" onClick={this.clearFilter}>Clear All</span> : null}
						</div>
						<div className="col-md-12 form-group" style={{padding: "0px 10px"}}>
							<label>Territory</label>
							<div>
								<Field name={'filter.territoryid'} props={{resource: 'territories', fields: 'id,territoryname', label:'territoryname', multiselect: true}} component={autoMultiSelectEle} />
							</div>
						</div>
						<div className="col-md-12 form-group" style={{padding: "0px 10px"}}>
							<label>Call From Date</label>
							<div>
								<Field name={'filter.fromdate'} props={{}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({title : 'From Date'})]}/>
							</div>
						</div>
						<div className="col-md-12 form-group" style={{padding: "0px 10px"}}>
							<label>Call To Date</label>
							<div>
								<Field name={'filter.todate'} props={{required: true, min: this.props.resource.filter.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  '{resource.filter.fromdate ? true : false}', title : 'To Date', min: '{resource.filter.fromdate}'})]}/>
							</div>
						</div>
						<div className="col-md-12 form-group" style={{padding: "0px 10px"}}>
							<label>Call Type</label>
							<Field name={'filter.calltype'} props={{required: true, options: ['All', 'PMS', 'Service', 'Installation']}} component={localSelectEle} />
						</div>
						<div className="col-md-12 form-group" style={{padding: "0px 10px"}}>
							<label>Allocation Status</label>
							<Field name={'filter.allocationstatus'} props={{required: true, options: ['All', 'Not Scheduled', 'Scheduled']}} component={localSelectEle} />
						</div>
						<div className="col-md-12">
							<button type="button" className="btn btn-sm btn-block gs-btn-success" onClick={this.searchFilter} disabled={!this.props.valid} style={{lineHeight: '2'}}>Apply</button>
						</div>
					</div>
				</div>
			</ServiceEngineerAssignmentFilter>
		);


		return createPortal(filterModalContainer, this.filterElePortalNode);
	}

	clearFilter() {
		this.props.updateFormState(this.props.form, {
			filter: {}
		});
	}

	allocationStatusOnChange(val, param) {
		let { filter } = this.props.resource;
		let paramObj = {
			'notscheduled': 'scheduled',
			'scheduled': 'notscheduled'
		}

		filter[paramObj[param]] = false;
		this.props.updateFormState(this.props.form, { filter });
	}

	renderFilterAppliedLabel = () => {
		let filterArray = [{label: 'Territory', key: 'territoryid'}, {label: 'Call From Date', key: 'fromdate'}, {label: 'Call To Date', key: 'todate'}, , {label: 'Call Type', key: 'calltype'}, {label: 'Allocation Status', key: 'allocationstatus'}];

		return filterArray.map((item, index) => {
			if(this.props.resource.filter[item.key]) {
				return (
					<div className="list-filter-card" key={index}>
						<span className="list-filter-card-content">{item.label} = {item.key == 'territoryid' ?  this.props.resource.filter[item.key].join() : this.props.resource.filter[item.key]}</span>
					</div>
				);
			}
		});
	}

	render() {
		if(!this.props.resource)
			return null;

		let pageHeight = $(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - $('.gs-eng-assign-scf-header').outerHeight() - $('.gs-eng-assign-scs-header').outerHeight() - 20;

		let viewOptionArray = [{
			"title": "Scheduler",
			"value": 'timeline'
		}, {
			"title": "List",
			"value": 'list'
		}];

		if(this.props.app.feature.licensedTrackingUserCount > 0) {
			viewOptionArray.splice(1, 0, {
				"title": "Map",
				"value": 'map'
			});
		}

		return(
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className={`col-md-12 bg-white report-header`} style={{marginBottom: '10px'}}>
							<div className="report-header-title gs-uppercase">Service Engineer Assignment</div>
							<div className="float-right d-flex align-items-center" style={{marginBottom: '10px', marginTop: '10px'}}>
								<div className="d-flex">
									<div className="mr-2">{this.renderFilterAppliedLabel()}</div>
									<button type="button" className="btn btn-sm gs-form-btn-primary gs-btn-lg mr-3" ref={this.filterEleBtnRef} onClick={this.handleFilerBtnOnClick}>Filter</button>
									{this.renderFilter()}
									<Field name={'viewswitch'} props={{buttons: viewOptionArray, buttonclass: 'gs-btn-lg svceng_assign_rpt_btn_group', onChange: (val)=>this.viewswitchOnChange(val)}} component={ButtongroupEle} />
								</div>
							</div>
						</div>
						<div className="col-md-12 pl-2 pr-2">
							<div className="row no-gutters">
								{this.props.resource.viewswitch != 'list' ? <div className="col-md-3 pl-0 pr-2">
									<div className="d-block bg-white" style={{border: '1px solid rgba(0,0,0,.125)', borderRadius: '0.25rem'}}>
										<div className="gs-eng-assign-scf-header gs-uppercase semi-bold" style={{padding: '8px 12px', color: '#1ABC9C'}}>Service Calls</div>
										<div className="gs-eng-assign-scs-header" style={{padding: '0px 12px 8px 12px'}}>
											<Field name={'servicecallsearch'} component={InputEle} validate={[stringNewValidation({title : 'engineersearch'})]} />
										</div>
										<div className="gs-scrollbar" style={{overflow: 'auto', height: `${pageHeight}px`}} onScroll={this.handleScroll}>{this.renderServiceCallDetails()}</div>
									</div>
								</div> : null}
								{this.props.resource.viewswitch == 'timeline' ? <div className="col-md-9 pl-0 pr-0">
									{this.renderGanttTimeline()}
								</div> : null}
								{this.props.resource.viewswitch == 'map' ? <div className="col-md-9 pl-0 pr-0">
									<MapForm app={this.props.app} resource={this.props.resource} openModal={this.props.openModal} history = {this.props.history} updateFormState={this.props.updateFormState} form={this.props.form} />
								</div> : null}
								{this.props.resource.viewswitch == 'list' ? <div className="col-md-3 pl-0 pr-2">
									<div className="d-block bg-white" style={{border: '1px solid rgba(0,0,0,.125)', borderRadius: '0.25rem'}}>
										<div className="gs-eng-assign-scf-header gs-uppercase semi-bold" style={{padding: '8px 12px', color: '#1ABC9C'}}>Service Engineers</div>
										<div className="gs-eng-assign-scs-header" style={{padding: '0px 12px 8px 12px'}}>
											<Field name={'serviceengineersearch'} component={InputEle} validate={[stringNewValidation({title : 'serviceengineersearch'})]} />
										</div>
										<div className="gs-scrollbar" style={{overflow: 'auto', height: `${pageHeight}px`}}>{this.renderServiceEngineer()}</div>
									</div>
								</div> : null}
								{this.props.resource.viewswitch == 'list' ? <div className="col-md-9 pl-0 pr-0">
									<div className="d-block bg-white" style={{border: '1px solid rgba(0,0,0,.125)', borderRadius: '0.25rem'}}>
										<div className="gs-eng-assign-scf-header gs-uppercase semi-bold" style={{padding: '8px 12px', color: '#1ABC9C'}}>Service Calls</div>
										<div className="gs-eng-assign-scs-header" style={{padding: '0px 12px 8px 12px'}}>
											<Field name={'servicecallsearch'} component={InputEle} placeholder="Search for Service Call, Customer, Call Type" validate={[stringNewValidation({title : 'engineersearch'})]} />
										</div>
										<div className="gs-scrollbar" style={{overflow: 'auto', height: `${pageHeight}px`}} onScroll={this.handleScroll}>
											<table className="table table-condensed" style={{color: '#042140'}}>
												<thead style={{fontSize: '12px'}}>
													<tr>
														<th className="text-center" style={{fontWeight: '400', padding: '0.50rem'}}>Call No</th>
														<th className="text-center" style={{fontWeight: '400', padding: '0.50rem'}}>Date</th>
														<th style={{fontWeight: '400', padding: '0.50rem'}}>Customer</th>
														<th className="text-center" style={{fontWeight: '400', padding: '0.50rem'}}>Service Type</th>
														<th style={{width: '20%', fontWeight: '400', padding: '0.50rem'}}>Service Location</th>
														<th className="text-center" style={{fontWeight: '400', padding: '0.50rem'}}>Allocation Status</th>
														<th style={{padding: '0.50rem'}}></th>
														<th style={{width: '12%', fontWeight: '400', padding: '0.50rem'}}>Engineer Allocation</th>
													</tr>
												</thead>
												<tbody>
													{this.renderServiceCallListDetails()}
												</tbody>
											</table>
										</div>
									</div>
								</div> : null}
							</div>
						</div>
					</div>
				</form>
			</div>
		);
	};
}

class ServiceEngineerAssignmentFilter extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		document.addEventListener("mousedown", this.handleBodyClick);
	}

	componentWillUnmount() {
		document.removeEventListener("mousedown", this.handleBodyClick);
	}

	handleBodyClick = event => {
		if(event.target.className == 'filter-overlay') {
			this.props.onOutsideClick(event);
		}
	};

	render() {
		let styleProp = {
			width: '100%',
			height: '100%',
			background: '#80808026',
			position: 'fixed',
			top: 0,
			zIndex: '10'
		};
		return (
			<>
				<div className="filter-overlay" style={styleProp}></div>
				<div>{this.props.children}</div>
			</>
		);
	}
}

ServiceEngineerAssignment = connect(
	(state, props) => {
		let formName = 'serviceengineerassignment';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ServiceEngineerAssignment));

export default ServiceEngineerAssignment;
