import React, { Component } from 'react';
import { v1 as uuidv1 } from 'uuid';
import Modal from 'react-modal';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import _ from 'lodash';
import { BrowserRouter as Router, Route, Link, Prompt, matchPath } from "react-router-dom";
import ReactPaginate from 'react-paginate';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import routes from '../routeconfig';
import {updatePageJSONState, updateFormState, updateAppState } from '../actions/actions';
import {getPageJson, getPageOptions, getFormName, checkPageCreateMode, getControllerName, checkAccess, checkActionVerbAccess, numberValidation, dateValidation, multiSelectValidation, requiredValidation, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation } from '../utils/utils';
import { InputEle, NumberEle, SpanEle, DateEle, textareaEle, dateTimeEle, selectAsyncEle, localSelectEle, autoSelectEle, richtextEle, qtyEle, emailEle, checkboxEle, ContactEle, DisplayGroupEle, RateInline, StockInline, AddressEle, autosuggestEle, ButtongroupEle, RateField, DiscountField, TimepickerEle, alertinfoEle, PhoneElement, passwordEle, DocumentLinkEle, ExpenseRequestSummary, UUIDEle, ContractEle, customrichtextEle, EmailAttachmentDownloadEle, autoMultiSelectEle, LocationAddressEle, costcenterAutoMultiSelectEle } from '../components/formelements';
import { Init, AlertView, AutoSelect, AccountBalance } from '../components/utilcomponents';
import Loadingcontainer from '../components/loadingcontainer';
import { AdditionalInformationSection } from './ndtsection1';
import ActivitySection from './activitysection';
import ServiceActivitySection from './serviceactivitysection';
import * as filter from '../utils/filter';
import { Newactivitysection, Nextactivitysection, Activityhistorysection, Newactivityhistorysection } from '../components/details/activitysections';
import { Totalsection } from '../components/details/totalsection';
import Contactsection from '../components/details/contactsection';
import Addresssection from '../components/details/addresssection';
import DocTraceModal from '../components/details/doctracemodal';
import { commonMethods, modalService } from '../utils/services';
import Printmodal from '../components/details/printmodal';
import Billingschedulesection from '../components/details/billingschedules';
import Equipmentschedulesection from '../components/details/equipmentschedules';
import Projectschedulesection from '../components/details/projectschedules';
import Rolemappingsection from '../components/details/rolemapping';
import Configureprintsection from '../components/details/configureprint';
import TableRow from './tablerow';
let questr = require('querystring');
import { ProjectEstimationCostSummaryDetails } from '../components/details/projectestimationscostsummarydetails';
import EmailTagSelectionDetails from '../components/details/emailtagselectiondetails';
import ProjectItemTableAddmodal from './projectitemtableaddmodal';
import RecordingplayModal from '../components/details/recording';
import AmendCompareModel from '../components/details/amendcomparemodel';
import EmployeeLeaveBalanceDetails from '../components/details/employeeleavebalancedetails';
import BOQImportExportButtonComponent from '../components/details/boqimportexportbuttoncomponents';
import ProjectEstimationImportExportButtonComponent from '../components/details/projectestimationimportexportbuttoncomponents';
import ServiceVisitTable from '../components/details/servicevisittable';

const buttonMarkup = {
	'Save' : {
		'buttonclass' : "success",
		'icon' : "fa-save",
		'condition' : "resource.status == '' || resource.status == null || resource.status == 'Draft' ||  resource.status == 'Open' || (resource.status == 'Inprogress' && pagejson.resourcename != 'equipmentschedules')  || resource.status == 'Quotation' || resource.status == 'Delivered' || resource.status == 'Available' || resource.status=='Assigned' || resource.status=='Pending' || (resource.status == 'Planned' && pagejson.resourcename == 'salesactivities')"
	},
	'Submit' : {
		'buttonclass' : "success",
		'icon' : "fa-check",
		'condition' : "resource.status == '' || resource.status == null || resource.status == 'Draft' || resource.status == 'Revise' || resource.status == 'Sent For Modification' || resource.status == 'Open'"
	},
	'Revise' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "resource.status=='Submitted'"
	},
	'Approve' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "resource.status == '' || resource.status == null || resource.status == 'Draft' || resource.status == 'Submitted' || resource.status == 'Open'"
	},
	'Cancel' : {
		'buttonclass' : "warning",
		'isMoreButton' : true,
		'icon' : 'fa-ban',
		'condition' : "resource.status == 'Resolved' || resource.status == 'Quotation' || resource.status == 'Converted' || resource.status == 'Approved' || resource.status == 'Received' || resource.status == 'Sent To Customer' || resource.status == 'Sent To Supplier' || resource.status == 'Open' || resource.status == 'Dispatched' || resource.status == 'Disbursed' || resource.status=='Closed' || resource.status=='Suspended' || resource.status=='Accepted' || resource.status=='Assigned' || (resource.status == 'Planned' && pagejson.resourcename == 'salesactivities')"
	},
	'Delete' : {
		'buttonclass' : "danger",
		'isMoreButton' : true,
		'icon' : 'fa-trash-o',
		'condition' : "resource.id && (resource.status == 'Draft' || resource.status == 'Submitted' || resource.status == 'Revise' || resource.status == 'Cancelled' || resource.status == 'Sent For Modification' || resource.status == '' || !resource.status || resource.status == 'Rejected' || (pagejson.resourcename == 'salesactivities' && resource.status == 'Open')) && !resource.amendinprogress"
	},
	'Send To Supplier' : {
		'buttonclass' : "success",
		'icon' : 'fa-envelope',
		'condition' : "(resource.status=='Approved' || resource.status == 'Sent To Supplier' || resource.status == 'Received') && (pagejson.resourcename != 'journalvouchers' || (pagejson.resourcename == 'journalvouchers' && pagejson.pagename == 'Payment Voucher'))"
	},
	'Send To Customer' : {
		'buttonclass' : "success",
		'icon' : 'fa-envelope',
		'condition' : "(resource.status == 'Approved' || resource.status == 'Dispatched' || resource.status == 'Sent To Customer') && (pagejson.resourcename != 'journalvouchers' || (pagejson.resourcename == 'journalvouchers' && pagejson.pagename == 'Receipt Voucher'))"
	},
	'Open' : {
		'buttonclass' : "success",
		'icon' : 'fa-plus',
		'condition' : "!resource.id"
	},
	'Amend' : {
		'buttonclass' : "success",
		'icon' : 'fa-edit',
		'condition' : "resource.status=='To Amend'"
	},
	'Close' : {
		'buttonclass' : "danger",
		'isMoreButton' : true,
		'buttonname' : '{pagejson.resourcename == "tasks" ? "Close" : (pagejson.resourcename == "financialyears" ? "Close Financial Year" : "Pre Close")}',
		'icon' : 'fa-times-circle',
		'condition' : "(['tasks', 'financialyears'].indexOf(pagejson.resourcename) == -1 && (resource.status == 'Open' || (resource.status == 'Approved' && !resource.preclosed) || (resource.status=='Sent To Supplier' && !resource.preclosed))) || (pagejson.resourcename == 'tasks' && resource.status == 'Completed' && resource.createdby == user.id)  || (pagejson.resourcename == 'financialyears' && resource.status == 'Open' && resource.id > 0)"
	},
	'Lost' : {
		'buttonclass' : "danger",
		'icon' : 'fa-thumbs-down',
		'condition' : "resource.status == 'Sent To Customer' || resource.status == 'Open' || resource.status == 'Quotation'"
	},
	'Won' : {
		'buttonclass' : "success",
		'icon' : 'fa-thumbs-up',
		'condition' : "resource.status == 'Sent To Customer' || resource.status == 'Open' || resource.status == 'Quotation'"
	},
	'Receive' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "resource.status == 'Approved'"
	},
	'Update' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "resource.status == 'Submitted' || resource.status == 'Sent To Customer' || resource.status=='Approved' || resource.status=='Dispatched' || resource.status=='Won' || resource.status=='Lost' || resource.status == 'Sent To Supplier' || resource.status=='Completed' || resource.status == 'Received' || (resource.status == 'Pending' && pagejson.resourcename == 'productionschedule')"
	},
	'Dispatch' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "resource.status == 'Approved'"
	},
	'Complete' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "(((resource.status=='Open' || resource.status=='Inprogress' || resource.status=='Resolved') && pagejson.resourcename != 'salesactivities') && (resource.assignedto == user.id || resource.parentresource=='productionorders')) || resource.status=='Assigned' || resource.status=='Approved'"
	},
	'Inprogress' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "resource.status=='Open' && resource.assignedto == user.id && pagejson.resourcename != 'salesactivities'"
	},
	'Resolve' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "resource.status=='Open' || resource.status=='Inprogress'"
	},
	'Send For Modification' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "resource.status=='Submitted'"
	},
	'Reject' : {
		'buttonclass' : "warning",
		'icon' : 'fa-thumbs-down',
		'condition' : "resource.status=='Submitted'"
	},
	'Hold' : {
		'buttonclass' : "primary",
		'isMoreButton' : true,
		'icon' : 'fa-pause',
		'condition' : "(resource.status=='Approved' || resource.status=='Sent To Supplier') && !resource.preclosed"
	},
	'Unhold' : {
		'buttonclass' : "success",
		'icon' : 'fa-play',
		'condition' : "resource.status=='Hold'"
	},
	'Disburse' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "resource.status == 'Approved'"
	},
	'Reopen' : {
		'buttonclass' : "success",
		'isMoreButton' : true,
		'icon' : 'fa-check',
		'condition' : "resource.status == 'Won' || resource.status=='Lost' || resource.status == 'Closed' || resource.status == 'Accepted' || ((resource.status=='Sent To Supplier' || resource.status=='Approved') && (resource.preclosed)) || (resource.status=='Completed' && pagejson.resourcename != 'salesactivities')"
	},
	'Send Acknowledgement' : {
		'buttonclass' : "success",
		'icon' : 'fa-envelope',
		'condition' : "resource.status == 'Approved' && !resource.preclosed"
	},
	'Accepted' : {
		'buttonclass' : "success",
		'icon' : 'fa-check',
		'condition' : "resource.status == 'Sent To Customer'"
	},
	'Update Feedback' : {
		'buttonclass' : "success",
		'icon' : 'fa-comment',
		'condition' : "resource.status=='Completed'"
	},
	'Suspend' : {
		'buttonclass' : "danger",
		'isMoreButton' : true,
		'icon' : 'fa-times-circle',
		'condition' : "resource.status == 'Open'"
	},
	'Expire' : {
		'buttonclass' : "danger",
		'isMoreButton' : true,
		'icon' : 'fa-thumbs-down',
		'condition' : "(resource.status == 'Approved' || resource.status == 'Sent To Customer') && resource.contractstatus == 'Active'"
	},
	'Reserve' : {
		'buttonclass' : "primary",
		'icon' : 'fa-check',
		'condition' : "pagejson.resourcename == 'productionorders' && resource.status == 'Approved'"
	},
	'Unreserve' : {
		'buttonclass' : "primary",
		'icon' : 'fa-check',
		'condition' : "resource.status == 'Approved' && resource.materialstatus != 'Not Planned'"
	},
	'Start' : {
		'buttonclass' : "primary",
		'icon' : 'fa-check',
		'condition' : "resource.status == 'Pending'"
	},
	'Retry' : {
		'buttonclass' : "warning",
		'icon' : 'fa-repeat',
		'condition' : "resource.status == 'Not Sent' && resource.relatedresource == 'notifications'"
	},
	'Send Mail' : {
		'buttonclass' : "success",
		'icon' : 'fa-envelope',
		'condition' : "resource.status == 'Approved'"
	}
};


const statusArray = ['Approved','Cancelled','Sent To Customer','Sent To Supplier','Closed','Hold','Won','Lost','Dispatched','Disqualified','Junk','Received', 'Unhold', 'Accepted','Completed','Converted', 'Disbursed'];

class DetailForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			modalArray : [],
			localCompState: {},
			tabActive: null
		};

		this.pageJSONCallback = this.pageJSONCallback.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.renderPageJSON = this.renderPageJSON.bind(this);
		this.renderSectionItems = this.renderSectionItems.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.openModal = this.props.openModal.bind(this);
		this.checkCondition = this.checkCondition.bind(this);
		this.getFieldString = this.getFieldString.bind(this);
		this.getActionButton = this.getActionButton.bind(this);
		this.getMoreActionButton = this.getMoreActionButton.bind(this);
		this.getButton = this.getButton.bind(this);
		this.customFieldsOperation = this.customFieldsOperation.bind(this);
		this.getCustomFields = this.getCustomFields.bind(this);
		this.selector = formValueSelector(this.props.form);
		this.getPromptMessage = this.getPromptMessage.bind(this);
		this.renderAlert = this.renderAlert.bind(this);
		this.renderEachAlert = this.renderEachAlert.bind(this);
		let controllerName = getControllerName(this.props);
		delete require.cache[require.resolve(`../controller/${controllerName}`)];
		let tempcontroller = require(`../controller/${controllerName}`);

		this.controller = {};
		for(var prop in tempcontroller)
			this.controller[prop] = tempcontroller[prop].bind(this);

		this.controller.customFunctions = {};
	}

	componentWillMount() {
		if(!this.props.isModal)
			this.props.updateAppState('inactivetaxerrorthrown', false);
		this.updateLoaderFlag(true);
		getPageJson(this.props, getPageOptions(this.props.match.path), this.pageJSONCallback);

		let urlQuery = this.props.isModal ? null : this.props.location.search.split('?')[1];
		let queryParam = questr.parse(urlQuery, '&', '=');

		this.setState({
			viewtype : queryParam && queryParam.viewtype == 'book' && queryParam.listurl && queryParam.redirecturl ? {
			listurl : queryParam.listurl,
			redirecturl : queryParam.redirecturl
			} : null
		}, () => {
			if(!this.props.isModal && this.state.viewtype)
				this.props.history.replace(this.props.location.pathname);
		});
	}

	componentWillReceiveProps(nextProps) {
		/*if(nextProps.app.pageloaderFlag == false && nextProps.app.usepageloaderFlag)
			this.updateLoaderFlag(false);*/
	}

	pageJSONCallback(pagejson) {
		this.setState({pagejson : pagejson, createParam : checkPageCreateMode(this.props)}, function() {
			if(!this.props.isModal)
				document.getElementById("pagetitle").innerHTML = `${this.state.createParam ? 'New ' : ''} ${pagejson.pagename}`;
			this.onLoad();
		});
	}

	onLoad() {
		let cus_modalService = modalService;
		let cus_commonMethods = commonMethods;
		let cus_axios = axios;
		let cus_ = _;
		let {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} = filter;
		for (var prop in this.state.pagejson.customFunctionsObj) {
			let condition = '('+this.state.pagejson.customFunctionsObj[prop]+')';
			this.controller.customFunctions[prop] = eval(`try{${condition}}catch(e){}`);
		}
		this.controller.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	updateLocalCompState(prop, value) {
		this.state.localCompState[prop] = value;
		this.setState({
			localCompState: this.state.localCompState
		});
	}

	checkCondition(type, condition, item, itemstr, parentitem, parentitemstr) {
		// Gloabals
		let app = this.props.app;
		let feature = this.props.app.feature;
		let user = this.props.app.user;
		let state = this.state;
		let pagejson = this.state.pagejson;
		let initFn = (tempObj) => tempObj ? this.props.updateFormState(this.props.form, tempObj) : null;
		let changeFn = (tempObj) => tempObj ? this.props.updateFormState(this.props.form, tempObj) : null;
		let {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter, itemmasterDisplaynamefilter} = filter;
		let cus_modalService = modalService;
		let cus_commonMethods = commonMethods;
		let cus_axios = axios;
		let cus_ = _;

		let formdisabled = (this.state.loaderflag || this.props.invalid);

		// Strict to Page
		let controller = this.controller;
		let resource = this.props.resource;

		if(type == 'if')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 || (['string', 'boolean'].indexOf(typeof(condition)) >= 0 && eval(`try{${condition}}catch(e){}`));
		if(type == 'hide')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? 'hide' : '');
		if(type == 'show')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? '' : 'hide');
		if(type == 'required')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'disabled')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'value') {
			//console.log(condition)
			if(typeof(condition) == 'string') {
				if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
					return eval(`try{${condition}}catch(e){}`);
				else
					return condition;
			}
			return condition;
		}
		return null;
	}

	renderPageJSON() {
		let pagejson = this.state.pagejson;
		let key = 0, sectionArray = [];
		let tabKey = 0, tabFound = false;

		let tabArray = pagejson.sections.map((a, index) => {
			if(a.type != 'tab' || !this.checkCondition('if', a.if))
				return null;

			let hideClass = this.checkCondition('hide', a.hide);
			let showClass = this.checkCondition('show', a.show);

			let tabHeaderName = this.checkCondition('value', a.title);
			let tabbadge = '';
			if(a.tabbadge)
				tabbadge = this.checkCondition('value', a.tabbadge);
			let tabActive = (this.state.tabActive == tabHeaderName || (this.state.tabActive == null && tabKey == 0));
			tabKey++;
			return {
				headerName: tabHeaderName,
				tabActive: tabActive,
				tabbadge: tabbadge,
				className: `${hideClass} ${showClass}`,
				body: (<div style={{display:`${ tabActive ? 'block' : 'none'}`}} className={`${hideClass} ${showClass}`} key={tabKey}>
					<div className={`col-md-12 col-sm-12`}>
						{this.renderSectionItems(a.fields)}
					</div>
				</div>)
			}
		});
		tabArray = tabArray.filter((tab) => {
			return tab ? true : false
		});

		sectionArray = pagejson.sections.map((a, index)=> {
			key++;
			if(!this.checkCondition('if', a.if))
				return null;

			let hideClass = this.checkCondition('hide', a.hide);
			let showClass = this.checkCondition('show', a.show);
			if(a.type == 'tab' && tabFound)
				return null;

			if(a.type == 'tab' && tabArray.length > 0) {
				tabFound = true
				return (
					<div className={`col-md-12 col-sm-12`} key={key}>
						<div className="card marginbottom-15 borderradius-0 gs-card">
							<div className="card-body">
								<div className="row responsive-form-element">
									<div className="col-md-12" style={{marginBottom: '20px'}}>
										<ul className="nav nav-tabs gs-home-navtabs">
											{tabArray.map((tab, tabindex) => {
												return (
													<li className={`nav-item ${tab.className}`} key={tabindex} style={{minWidth: `${Math.ceil(100/tabArray.length)}px`, whiteSpace: 'nowrap'}}>
														<a className={`nav-link gs-details-tab-nav-link ${tab.tabActive ? 'active' : ''}`} onClick={() => this.setState({tabActive: tab.headerName})}>{tab.headerName} <span className="badge badge-secondary">{tab.tabbadge}</span></a>
													</li>
												)
											})}
										</ul>
									</div>
									<div className="col-md-12">
										<div className="tab-content">
											<div className="tab-pane fade show active">
												{tabArray.map((tab, tabindex) => {
													return tab.body
												})}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				);
			}

			return (
				<div className={`col-md-${a.type == 'section' ? '6' : '12'} col-sm-12 ${hideClass} ${showClass}`} key={key}>
					<div className="card gs-card">
						<div className="card-header gs-card-header">{this.checkCondition('value', a.title)}</div>
						<div className="card-body">
							<div className="row responsive-form-element">{this.renderSectionItems(a.fields)}</div>
						</div>
					</div>
				</div>
			);
		});

		//if(pagejson.showndt && this.props.resource && this.props.resource.id && this.state.ndt) {
		//	key++;
		//	sectionArray.push(<AdditionalInformationSection ndt={this.state.ndt} key={key} parentresource={pagejson.resourcename} parentid={this.props.resource.id} />);
		//}
		return sectionArray;
	}

	renderSectionItems(fields) {
		//return null;
		let key=0;
		let fieldArray = [];
		return fields.map((field) => {
			key++;
			return this.getFieldString(key, field);
		});
	}

	getFieldString(key, field, itemstr, parentitemstr) {
		let item = this.checkCondition('value', `{resource.${itemstr}}`);
		let parentitem = this.checkCondition('value', `{resource.${parentitemstr}}`);

		if(!this.checkCondition('if', field.if, item, itemstr, parentitem, parentitemstr))
			return null;

		let accessClass = field.checkaccess ? (checkAccess(this.props.app, field.accessoption.resourcename, field.accessoption.fields) ? '' : 'hide') : (field.access && field.actionverb) ? (checkActionVerbAccess(this.props.app, field.access, field.actionverb) ? '' : 'hide') : '';

		let showHideClass = `${this.checkCondition('hide', field.hide, item, itemstr, parentitem, parentitemstr)} ${this.checkCondition('show', field.show, item, itemstr, parentitem, parentitemstr)} ${accessClass}`;

		let colLayoutClass = itemstr ? 'col-md-6' : 'col-md-6';

		let pagejson = this.state.pagejson;
		let fieldProps = {};
		let temptitle = `${this.checkCondition('value', field.title, item, itemstr, parentitem, parentitemstr)}`;
		field.type = `${this.checkCondition('value', field.type, item, itemstr, parentitem, parentitemstr)}`;
		let tempmodel = itemstr ? `${itemstr}.${field.model}` : `${field.model}`;

		if(this.checkCondition('required', field.required, item, itemstr, parentitem, parentitemstr))
			fieldProps.required = true;

		if(this.checkCondition('disabled', field.disabled, item, itemstr, parentitem, parentitemstr))
			fieldProps.disabled = true;
		let globaldisabled = this.props.resource && statusArray.indexOf(this.props.resource.status) >= 0 ? true : false;
		let isupdatefield = false;
		if(field.isupdatefield)
			isupdatefield = this.checkCondition('if', field.isupdatefield, item, itemstr, parentitem, parentitemstr) ? true : false;
		fieldProps.disabled = fieldProps.disabled || (globaldisabled && !isupdatefield);

		if(field.pattern)
			fieldProps.pattern = this.checkCondition('value', field.pattern, item, itemstr, parentitem, parentitemstr);
		if(field.buttonclass)
			fieldProps.buttonclass = field.buttonclass;
		if(field.type == 'spaninput')
			fieldProps.spaninputmodel = this.checkCondition('value', field.model, item, itemstr, parentitem, parentitemstr);
		if(field.focus)
			fieldProps.onFocus = this.checkCondition('value', field.focus, item, itemstr, parentitem, parentitemstr);
		if(field.init)
			fieldProps.initfn = this.checkCondition('value', field.init, item, itemstr, parentitem, parentitemstr);
		if(field.blur)
			fieldProps.onBlur = this.checkCondition('value', field.blur, item, itemstr, parentitem, parentitemstr);
		if(field.click)
			fieldProps.click = this.checkCondition('value', field.click, item, itemstr, parentitem, parentitemstr);
		if(field.resource)
			fieldProps.resource = this.checkCondition('value', field.resource, item, itemstr, parentitem, parentitemstr);
		if(field.fields)
			fieldProps.fields = this.checkCondition('value', field.fields, item, itemstr, parentitem, parentitemstr);
		if(field.field)
			fieldProps.field = this.checkCondition('value', field.field, item, itemstr, parentitem, parentitemstr);
		if(field.additionalFields)
			fieldProps.additionalFields = this.checkCondition('value', field.additionalFields, item, itemstr, parentitem, parentitemstr);
		if(field.filter)
			fieldProps.filter = this.checkCondition('value', field.filter, item, itemstr, parentitem, parentitemstr);
		if(field.localoptions)
			fieldProps.options = this.checkCondition('value', field.localoptions, item, itemstr, parentitem, parentitemstr);
		if(field.label)
			fieldProps.label = this.checkCondition('value', field.label, item, itemstr, parentitem, parentitemstr);
		if(field.value)
			fieldProps.valuename = this.checkCondition('value', field.value, item, itemstr, parentitem, parentitemstr);
		if(field.displaylabel)
			fieldProps.displaylabel = this.checkCondition('value', field.displaylabel, item, itemstr, parentitem, parentitemstr);
		if(field.change)
			fieldProps.onChange = this.checkCondition('value', field.change, item, itemstr, parentitem, parentitemstr);
		if(field.buttons)
			fieldProps.buttons = this.checkCondition('value', field.buttons, item, itemstr, parentitem, parentitemstr);
		if(field.isupdatefield)
			fieldProps.classname = `${this.checkCondition('if', field.isupdatefield, item, itemstr, parentitem, parentitemstr) ? 'ndtclass' : ''}`;
		if(field.min) {
			fieldProps.min = this.checkCondition('value', field.min, item, itemstr, parentitem, parentitemstr);
		}
		if(field.max) {
			fieldProps.max = this.checkCondition('value', field.max, item, itemstr, parentitem, parentitemstr);
		}
		if(field.linkdisplayname) {
			fieldProps.linkdisplayname = this.checkCondition('value', field.linkdisplayname, item, itemstr, parentitem, parentitemstr);
		}
		if(field.multiselect)
			fieldProps.multiselect = this.checkCondition('value', field.multiselect, item, itemstr, parentitem, parentitemstr);
		if(field.type == 'selectasync') {
			fieldProps.createParamFlag = this.state.createParam ? true : false;
			fieldProps.defaultValueUpdateFn = (value) => this.props.updateFormState(this.props.form, {
				[tempmodel]: value
			});
		}
		if(field.type == 'richtext' || field.type == 'customrichtext') {
			fieldProps.createParamFlag = this.state.createParam ? true : false;
			fieldProps.changeValueUpdateFn = (value) => this.props.updateFormState(this.props.form, {
				[tempmodel]: value
			});
		}
		if(field.type == 'selectasync' || field.type == 'selectauto') {
			fieldProps.createOrEdit = this.props.createOrEdit;
			fieldProps.showadd = field.showadd ? true : false;
			fieldProps.showedit = field.showedit ? true : false;
		}
		if(field.type == 'quantity') {
			fieldProps.btnclick = this.checkCondition('value', '{()=>{controller.openStockDetails(item)}}', item, itemstr, parentitem, parentitemstr);
			let actionVerbAccess = checkActionVerbAccess(this.props.app, 'getstockquery', 'Read') ? true : false;
			fieldProps.showstockbutton =  (actionVerbAccess && (item.itemid_keepstock || item.itemid_issaleskit || item.itemid_hasaddons)) ? true : false;
		}
		if(field.type == 'rate') {
			fieldProps.btnclick = this.checkCondition('value', '{()=>{controller.analyseitemRate(item)}}', item, itemstr, parentitem, parentitemstr);
			fieldProps.showanalyseratebutton = checkActionVerbAccess(this.props.app, this.props.resource.resourceName == 'purchaseorders' ? 'analyseitemquerypurchase' : 'analyseitemquery', 'Read') ? true : false;
		}
		if(field.type == 'discount') {
			fieldProps.model = [`${itemstr}.discountquantity`, `${itemstr}.discountmode`];
			if(!fieldProps.onChange)
				fieldProps.onChange = this.checkCondition('value', "{controller.computeFinalRate}", item, itemstr, parentitem, parentitemstr);
			fieldProps.currencytitle = this.checkCondition('value', "{`${app.currency[resource.currencyid].symbol}`}", item, itemstr, parentitem, parentitemstr);
			fieldProps.discountmodedisabled = this.checkCondition('disabled', field.discountmodedisabled, item, itemstr, parentitem, parentitemstr);
		}
		if(field.type == 'displaygroup') {
			fieldProps.child = this.checkCondition('value', field.child, item, itemstr, parentitem, parentitemstr);
		}
		if(field.type == 'contact') {
			fieldProps.title = temptitle;
			fieldProps.model = this.checkCondition('value', field.model, item, itemstr, parentitem, parentitemstr);
			fieldProps.emailrequired = this.checkCondition('required', field.emailrequired, item, itemstr, parentitem, parentitemstr);
			fieldProps.mobilerequired = this.checkCondition('required', field.mobilerequired, item, itemstr, parentitem, parentitemstr);
			fieldProps.phonerequired = this.checkCondition('required', field.phonerequired, item, itemstr, parentitem, parentitemstr);
			fieldProps.parentresource = this.checkCondition('value', field.parentresource, item, itemstr, parentitem, parentitemstr);
			fieldProps.parentid = this.checkCondition('value', field.parentid, item, itemstr, parentitem, parentitemstr);
		}
		if(field.type == 'contract') {
			fieldProps.title = temptitle;
			fieldProps.model = this.checkCondition('value', field.model, item, itemstr, parentitem, parentitemstr);
		}
		if(field.type == 'alertinfo') {
			fieldProps.message = `${this.checkCondition('value', field.message, item, itemstr, parentitem, parentitemstr)}`;
			fieldProps.messageclass = `${this.checkCondition('value', field.alertclass, item, itemstr, parentitem, parentitemstr)}`;
		}
		if(field.type == 'payrolloutstandinginfo') {
			fieldProps.linkOnclick = this.checkCondition('value', '{()=>{controller.getRecoveryComponentDetails()}}', item, itemstr, parentitem, parentitemstr);
		}
		if(field.type == 'autosuggest') {
			if(fieldProps.resource == 'addresses' && fieldProps.field == 'state' && this.props.resource)
				fieldProps.address_country = this.props.resource.country;
			if(field.country)
				fieldProps.address_country = this.checkCondition('value', field.country, item, itemstr, parentitem, parentitemstr);
			if(fieldProps.resource == 'feedbackcontactperson')
				fieldProps.feedbackContactArray = this.state.feedbackContactArray;
		}
		if(field.type == 'linkbutton' || field.type == 'relatedto') {
			fieldProps.getURL = this.checkCondition('value', field.getURL, item, itemstr, parentitem, parentitemstr);
		}
		if(field.type == 'address') {
			fieldProps.temptitle = `${this.checkCondition('value', field.title, item, itemstr, parentitem, parentitemstr)}`;
			fieldProps.model = this.checkCondition('value', field.model, item, itemstr, parentitem, parentitemstr);
			fieldProps.parentresource = this.checkCondition('value', field.parentresource, item, itemstr, parentitem, parentitemstr);
			fieldProps.parentid = this.checkCondition('value', field.parentid, item, itemstr, parentitem, parentitemstr);
			fieldProps.orderid = this.checkCondition('value', field.orderid, item, itemstr, parentitem, parentitemstr);
			fieldProps.projectid = this.checkCondition('value', field.projectid, item, itemstr, parentitem, parentitemstr);
		}
		if(field.type == 'locationaddress') {
			fieldProps.temptitle = `${this.checkCondition('value', field.title, item, itemstr, parentitem, parentitemstr)}`;
			fieldProps.model = this.checkCondition('value', field.model, item, itemstr, parentitem, parentitemstr);
		}
		if(field.type == 'accountbalance') {
			fieldProps.accountdetails = this.checkCondition('value', field.accountdetails, item, itemstr, parentitem, parentitemstr);
		}
		let tempvalidate = [];
		/*if(fieldProps.required || fieldProps.pattern || (fieldProps.min != null && fieldProps.min != '') || (fieldProps.max != null && fieldProps.max != '') || ['email'].indexOf(field.type) >= 0) {
			let options = {
				required: fieldProps.required ? true : false,
				model: temptitle
			};
			if(fieldProps.pattern)
				options.pattern = fieldProps.pattern;
			if(fieldProps.min != null && fieldProps.min != '')
				options.min = fieldProps.min;
			if(fieldProps.max != null && fieldProps.max != '')
				options.max = fieldProps.max;

			if(['number', 'selectauto', 'inputbuttongroup', 'selectauto', 'selectasync', 'time', 'discount', 'quantity'].indexOf(field.type) >= 0)
				tempvalidate = [numberNewValidation(options)];
			else if(['date'].indexOf(field.type) >= 0)
				tempvalidate = [dateNewValidation(options)];
			else if(['select'].indexOf(field.type) >= 0 && fieldProps.multiselect)
				tempvalidate = [multiSelectNewValidation(options)];
			else if(['email'].indexOf(field.type) >= 0)
				tempvalidate = [emailNewValidation(options)];
			else
				tempvalidate = [stringNewValidation(options)];
		}*/
		if(field.required || field.pattern || (field.min != null && field.min != '') || (field.max != null && field.max != '') || ['email'].indexOf(field.type) >= 0) {
			let state = {...this.state};
			delete state.loaderflag;
			let options = {
				required: field.required ? field.required : false,
				model: temptitle,
				itemstr: itemstr ? itemstr : '',
				app: JSON.stringify({
					feature: this.props.app.feature,
					user: this.props.app.user,
					gstinpattern: this.props.app.gstinpattern
				}),
				//state: JSON.stringify({...state}),
				state: JSON.stringify({}),
				pagejson: JSON.stringify(this.state.pagejson)
			};
			if(field.pattern)
				options.pattern = field.pattern;
			if(field.min != null && field.min != '')
				options.min = field.min;
			if(field.max != null && field.max != '')
				options.max = field.max;

			if(['number', 'selectauto', 'inputbuttongroup', 'selectauto', 'selectasync', 'time', 'discount', 'quantity'].indexOf(field.type) >= 0)
				tempvalidate = [numberNewValidation(options)];
			else if(['date'].indexOf(field.type) >= 0)
				tempvalidate = [dateNewValidation(options)];
			else if(['select'].indexOf(field.type) >= 0 && fieldProps.multiselect)
				tempvalidate = [multiSelectNewValidation(options)];
			else if(['email'].indexOf(field.type) >= 0)
				tempvalidate = [emailNewValidation(options)];
			else
				tempvalidate = [stringNewValidation(options)];
		}

		if(field.type == 'text') {
			return(
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field type="text" name={tempmodel} props={fieldProps} component={InputEle} validate={tempvalidate} />
				</Init>
			);
		}
		if(field.type == 'number') {
			return(
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={NumberEle}  validate={tempvalidate} />
				</Init>
			);
		}
		if(field.type == 'date') {
			return(
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					{/*<Field name={tempmodel} props={fieldProps} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={tempvalidate}/>*/}

					<Field name={tempmodel} props={fieldProps} component={DateEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'repeatabletable') {
			return (
				<div key={key} className={` col-sm-12 col-md-12 ${showHideClass}`}>
					<FieldArray name={field.json} json={pagejson[field.json]} getFieldString={this.getFieldString} checkCondition={this.checkCondition} computeFinalRate={this.controller.computeFinalRate} onLoadKit={this.controller.onLoadKit} getimportMilestoneItems = {this.controller.getimportMilestoneItems} resource={this.props.resource} resourcename={this.state.pagejson.resourcename} updateFormState={this.props.updateFormState} form={this.props.form} openModal={this.props.openModal} array={this.props.array} app={this.props.app} component={ChildElement} />
				</div>
			);
		}
		if(field.type == 'additionalcharges') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 ${showHideClass}`} style={{marginTop:'15px'}}>
					<FieldArray name='additionalcharges' json={pagejson[field.json]} getFieldString={this.getFieldString} checkCondition={this.checkCondition} taxarray={['debitnotes', 'purchaseinvoices', 'purchaseorders', 'supplierquotation', 'workorders'].indexOf(getPageOptions(this.props.match.path).title) >= 0 ? this.props.app.purchasetaxarray : this.props.app.salestaxarray} updateFormState={this.props.updateFormState} form={this.props.form} resource={this.props.resource} computeFinalRate={this.controller.computeFinalRate} component={AdditionalChargesElement} />
				</div>
			);
		}
		if(field.type == 'pricelistversions') {
			return (
				<div key={key} className={` col-sm-12 col-md-12 ${showHideClass}`}>
					<FieldArray name={field.type} json={pagejson[field.json]} getFieldString={this.getFieldString} checkCondition={this.checkCondition} updateFormState={this.props.updateFormState} form={this.props.form} resource={this.props.resource} copyPriceListVersion={this.controller.copyPriceListVersion} getPriceVersionDetails={this.controller.getPriceVersionDetails} component={PriceListVersionsElement} />
				</div>
			);
		}
		if(field.type == 'pricelistitems') {
			return (
				<div key={key} className={` col-sm-12 col-md-12 ${showHideClass}`}>
					<PriceListItemsElement resource={this.props.resource} app={this.props.app} updateLoaderFlag={this.updateLoaderFlag} refresh={this.state.pricelistVersionItemsRefreshCount}  openModal={this.props.openModal}  />
				</div>
			);
		}
		if(field.type == 'textarea') {
			return (
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={textareaEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'alertinfo') {
			return (
				<Init className={` col-md-12 col-sm-12 col-xs-12 ${showHideClass}`} key={key}>
					<Field name={tempmodel} props={fieldProps} component={alertinfoEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'payrolloutstandinginfo') {
			return (
				<div className={` col-md-12 col-sm-12 col-xs-12 ${showHideClass}`} key={key}>
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className={`text-center alert alert-warning col-md-12 col-sm-12 col-xs-12`} >This employee has outstanding loan / advance. Click to add <a onClick={fieldProps.linkOnclick} className='alert-link'>Recovery</a>
						</div>
					</div>
				</div>
			);
		}
		if(field.type == 'spaninput') {
			return (
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field name={fieldProps.spaninputmodel || ""} props={fieldProps} component={SpanEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'checkbox') {
			return (
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={checkboxEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'password') {
			return (
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={passwordEle}  validate={tempvalidate}/>
				</div>
			);
		}
		if(field.type == 'email') {
			return (
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={emailEle}  validate={tempvalidate}/>
				</div>
			);
		}
		if(field.type == 'phone') {
			fieldProps.openModal = this.props.openModal;
			fieldProps.title = temptitle;
			return(
				<div className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key}>
					<label className="labelclass">{temptitle}</label>
					<PhoneElement key={key} {...fieldProps} model={tempmodel} resource={this.props.resource} updateAppState={this.props.updateAppState} />
				</div>
			);
		}
		if(field.type == 'selectasync') {
			return (
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass} ${fieldProps.resource == 'companymaster' ? 'hide' : ''}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={selectAsyncEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'selectauto') {
			fieldProps.resourceobj = this.props.resource;
			fieldProps.transactionresourcename = this.state.pagejson.resourcename;
			return (
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={autoSelectEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'multiselectauto') {
			fieldProps.resourceobj = this.props.resource;
			fieldProps.transactionresourcename = this.state.pagejson.resourcename;
			return (
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={autoMultiSelectEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'autosuggest') {
			return (
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={autosuggestEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'datetime') {
			return(
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={dateTimeEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'select' && field.localoptions && field.localoptions.length > 0) {
			return(
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={localSelectEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'buttongroup') {
			return(
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<Field name={tempmodel} props={fieldProps} component={ButtongroupEle}  validate={tempvalidate}/>
				</Init>
			);
		}
		if(field.type == 'richtext') {
			return(
				<div key={key} className={`form-group col-sm-12 col-md-12 ${showHideClass}`}>
					{temptitle!='undefined' ? <label className="labelclass">{temptitle}</label> : null}
					<Field name={tempmodel} props={fieldProps} component={richtextEle}  validate={tempvalidate}/>
				</div>
			);
		}
		if(field.type == 'customrichtext') {
			fieldProps.openModal = this.props.openModal;
			fieldProps.title = temptitle;
			return(
				<div className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key}>
					{temptitle!='undefined' ? <label className="labelclass">{temptitle}</label> : null}
					<Field name={tempmodel} props={fieldProps} component={customrichtextEle}  validate={tempvalidate}/>
				</div>
			);
		}
		if(field.type == 'quantity') {
			return(
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={qtyEle} validate={tempvalidate}/>
				</div>
			);
		}
		if(field.type == 'discount') {
			return(
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<DiscountField {...fieldProps} />
				</Init>
			);
		}

		if(field.type == 'rate') {
			return(
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<RateField name={tempmodel} {...fieldProps} />
				</div>
			);
		}
		if(field.type == 'button') {
			return(
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<button type="button" onClick={fieldProps.click} disabled={fieldProps.disabled} className={`btn btn-width btn-sm ${field.buttonclass}`} ><i className={`fa ${field.glyphicon}`}></i>{temptitle}</button>
				</div>
			);
		}
		if(field.type == 'linkbutton') {
			return(
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<Link to={fieldProps.getURL()} className={`btn btn-width btn-sm ${field.buttonclass}`} onClick={fieldProps.click} disabled={fieldProps.disabled}><i className={`fa ${field.glyphicon}`}></i>{temptitle}</Link>
				</div>
			);
		}

		if(field.type == 'relatedto') {
			return(
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<button type="button" onClick={fieldProps.click} className={`btn gs-form-btn-primary btn-width btn-sm ${field.buttonclass}`} >{fieldProps.linkdisplayname} <i className={`fa ${field.glyphicon}`}></i></button>
				</div>
			);
		}

		if(field.type == 'displaygroup') {
			return (
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={DisplayGroupEle}  validate={tempvalidate}/>
				</div>
			);
		}
		if(field.type == 'contact') {
			fieldProps.openModal = this.props.openModal;
			fieldProps.createOrEdit = this.props.createOrEdit;
			return (
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<ContactEle key={key} {...fieldProps} resource={this.props.resource} form={this.props.form} updateFormState={this.props.updateFormState} updateAppState={this.props.updateAppState} />
				</div>
			);
		}
		if(field.type == 'contract') {
			return (
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<ContractEle key={key} {...fieldProps} itemstr={itemstr} />
				</div>
			);
		}
		if(field.type == 'address') {
			fieldProps.openModal = this.props.openModal;
			fieldProps.createOrEdit = this.props.createOrEdit;
			return (
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<AddressEle key={key} {...fieldProps} resource={this.props.resource} />
				</div>
			);
		}
		if(field.type == 'locationaddress') {
			fieldProps.openModal = this.props.openModal;
			fieldProps.createOrEdit = this.props.createOrEdit;
			fieldProps.updateFormState = this.props.updateFormState;
			fieldProps.form = this.props.form;
			return (
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<LocationAddressEle key={key} {...fieldProps} resource={this.props.resource} />
				</div>
			);
		}
		if(field.type == 'time') {
			return (
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={TimepickerEle} format={(value) => {return (value == null || value == '') ? null : moment(value)}} parse={(value) => {return (value == null || value == '') ? null : moment(value)._d}} validate={tempvalidate}/>
				</div>
			);
		}
		if(field.type == 'accountbalance') {
			return(
				<Init className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key} initfn={fieldProps.initfn}>
					<label className="labelclass">{temptitle}</label>
					<AccountBalance app={this.props.app} updateFn={(value) => this.updateLocalCompState(`${itemstr}.accountbalance`, value)} currencyid={this.props.resource.currencyid} item={fieldProps.accountdetails} />
				</Init>
			);
		}
		if(field.type == 'uuid') {
			return (
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<UUIDEle {...fieldProps} model={tempmodel} resource={this.props.resource} form={this.props.form} updateFormState={this.props.updateFormState} />
				</div>
			);
		}
		if(field.type == 'newactivitysection') {
			return (
				<Newactivitysection key={key} resource={this.props.resource} change={this.props.change}/>
			);
		}
		if(field.type == 'nextactivitysection') {
			return (
				<FormSection key={key} name="nextactivity" >
					<Nextactivitysection resource={this.props.resource} change={this.props.change}/>
				</FormSection>
			);
		}
		if(field.type == 'activityhistorysection') {
			return (
				<Newactivityhistorysection key={key} resource={this.props.resource} getActivities={this.controller.getActivities}/>
			);
		}
		if(field.type == 'totalsection') {
			return (
				<Totalsection key={key} resource={this.props.resource} computeFinalRate={this.controller.computeFinalRate}/>
			);
		}
		if(field.type == 'expenserequestsummary') {
			return (
				<ExpenseRequestSummary key={key} resource={this.props.resource} app={this.props.app} />
			);
		}
		if(field.type == 'contactsection') {
			return (
				<Contactsection key={key} contacts={this.props.resource.contacts} addNewContact={this.controller.addNewContact} getContactDetails={this.controller.getContactDetails}/>
			);
		}
		if(field.type == 'addresssection') {
			return (
				<Addresssection key={key} addresses={this.props.resource.addresses} addNewAddress={this.controller.addNewAddress} getAddressDetails={this.controller.getAddressDetails}/>
			);
		}
		if(field.type == 'accountingforms') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 ${showHideClass}`}>
					<FieldArray name='accountingforms' json={pagejson[field.json]} checkCondition={this.checkCondition} resource={this.props.resource} component={AccountingformsElement} />
				</div>
			);
		}
		if(field.type == 'billingschedules') {
 			return (
				<Billingschedulesection key={key} resource = {this.props.resource} app ={this.props.app} array = { this.props.array } form = { this.props.form } updateFormState = { this.props.updateFormState } openModal = {this.props.openModal} />
 			);
 		}
 		if(field.type == 'contractschedules') {
 			return (
				<Equipmentschedulesection key={key} resource = {this.props.resource} contractState = {this.state} array = { this.props.array } form = { this.props.form } updateFormState = { this.props.updateFormState }/>
 			);
 		}
		if(field.type == 'projectschedules') {
			return (
				<Projectschedulesection key={key} resource = {this.props.resource} array = { this.props.array } form = { this.props.form } updateFormState = { this.props.updateFormState } createOrEdit = {this.props.createOrEdit}/>
			);
		}
		if(field.type == 'paymentreceiptvoucheraccounttotal') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 text-right ${showHideClass}`}>
					<i>Total : <b className="text-primary">{this.checkCondition('value', '{currencyFilter(this.controller.getAllocatedAmount("totalaccountamount"), resource.currencyid, app)}')}</b></i>
				</div>
			);
		}
		if(field.type == 'paymentreceiptvoucherinvoicetotal') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 text-right ${showHideClass}`}>
					<i>Total : <b className="text-primary">{this.checkCondition('value', '{currencyFilter(this.controller.getAllocatedAmount("totalinvoiceamount"), resource.currencyid, app)}')}</b></i>
				</div>
			);
		}
		if(field.type == 'paymentreceiptvoucherdeductiontotal') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 text-right ${showHideClass}`}>
					<i>Total : <b className="text-primary">{this.checkCondition('value', '{currencyFilter(this.controller.getAllocatedAmount("totaldeductionamount"), resource.currencyid, app)}')}</b></i>
				</div>
			);
		}
		if(field.type == 'paymentreceiptvoucherexpensetotal') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 text-right ${showHideClass}`}>
					<i>Total : <b className="text-primary">{this.checkCondition('value', '{currencyFilter(this.controller.getAllocatedAmount("totalexpenseamount"), resource.currencyid, app)}')}</b></i>
				</div>
			);
		}
		if(field.type == 'paymentreceiptvouchersummary') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 text-right ${showHideClass}`}>
					<Paymentreceiptvouchersummarydetails key={key} resource = {this.props.resource} app={this.props.app} controller={this.controller} checkCondition={this.checkCondition} />
				</div>
			);
		}
		if(field.type == 'landingcostreceipttotal') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 text-right ${showHideClass}`}>
					<i>Total : <b className="text-primary">{this.checkCondition('value', '{currencyFilter(this.controller.getAllocatedAmount("landingcostreceipttotal"), resource.currencyid, app)}')}</b></i>
				</div>
			);
		}
		if(field.type == 'landingcostitemstotal') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 text-right ${showHideClass}`}>
					<i>Total : <b className="text-primary">{this.checkCondition('value', '{currencyFilter(this.controller.getAllocatedAmount("landingcostitemstotal"), resource.currencyid, app)}')}</b></i>
				</div>
			);
		}
		if(field.type == 'vouchersalert') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 ${showHideClass}`}>
					<div className="card card-body bg-light">
						<div className="row">
							<div className="col-md-4 text-center">
								Credit : <b className="text-success">{this.checkCondition('value', '{currencyFilter(this.controller.getVoucherDetails("creditSum"), resource.currencyid, app)}')}</b>
							</div>
							<div className="col-md-4 text-center">
								Debit : <b className="text-danger">{this.checkCondition('value', '{currencyFilter(this.controller.getVoucherDetails("debitSum"), resource.currencyid, app)}')}</b>
							</div>
							<div className="col-md-4 text-center">
								Balance : <b className="text-primary">{this.checkCondition('value', '{currencyFilter(this.controller.getVoucherDetails("balanceSum"), resource.currencyid, app)}')} - {this.controller.getVoucherDetails("balanceType")}</b>
							</div>
						</div>
					</div>
				</div>
			);
		}
		if(field.type == 'rolemapping') {
			return (
				<Rolemappingsection key={key} resource = {this.props.resource} array = { this.props.array } form = { this.props.form } roleArray={this.state.roleArray || []} selectedroleids={this.controller.getRoleIds()} onRoleChange={this.controller.onRoleChange} updateFormState = { this.props.updateFormState } app = { this.props.app } openModal = {this.props.openModal}/>
			);
		}
		if(field.type == 'resourcepermissions') {
			return (
				<Permissionsection key={key} resource = {this.props.resource} array = { this.props.array } form = { this.props.form } updateFormState = { this.props.updateFormState }/>
			);
		}
		if(field.type == 'activitytypebuttonicon') {
			return (
				<Activitytypebuttonicon key={key} resource = {this.props.resource} form = { this.props.form } updateFormState = { this.props.updateFormState }/>
			);
		}
		if(field.type == 'itemmasterimgupload') {
			let imageModel = field.model ? field.model : 'imageurl';
			return (
				<div key={key} className="form-group col-md-6 col-sm-6">
					{this.props.resource[imageModel] ? <div className="img-container">
						{field.accept == 'word' ? <a href={`/${this.props.resource.id ? 'documents' : 'print'}/${this.props.resource[imageModel].split('/').pop()}?url=${this.props.resource[imageModel]}`}><i className="fa fa-file-word-o fa-3x"></i>{this.props.resource[imageModel].split('/').pop()}</a> : <img className="img-thumbnail" src={`/${this.props.resource.id ? 'documents' : 'print'}/getfile?url=${this.props.resource[imageModel]}`} width="100" height="100"/>}
						<div className="imgdeletebtn"><button type="button" className="btn btn-sm gs-btn-danger" onClick={() => this.controller.removeImage(imageModel)}><span className="fa fa-trash"></span></button></div>
					</div> : null }
					<input type="file" accept={field.accept == 'word' ? `application/msword,
  application/vnd.openxmlformats-officedocument.wordprocessingml.document` : `image/*`} id={`uploadfile${imageModel}`} className="form-control hide" value={this.props.resource.image} onChange={(evt) => {if(evt.target.files.length > 0){this.controller.imageUpload(evt.target.files[0], imageModel)}evt.target.value=null;}} required />
					<label htmlFor={`uploadfile${imageModel}`} className='btn btn-sm gs-btn-info'>{temptitle != 'undefined' ? temptitle : 'Upload Image'}</label>
				</div>
			);
		}
		if(field.type == 'itemimage') {
			return (
				<div key={key} className="form-group col-md-6 col-sm-6" style={{maxWidth: '200px', maxHeight:'200px'}}>
					<img className="img-thumbnail" src={`/documents/getfile?url=${item.itemid_imageurl}`} style={{maxWidth: '100%', height: '100px'}}/>
				</div>
			);
		}
		if(field.type == 'imagetag') {
			return (
				<div key={key} className="form-group col-md-6 col-sm-6">
					<div  style={{maxWidth: '200px', maxHeight:'200px'}}>
						<img className="img-thumbnail" src={`/documents/getfile?url=${this.props.resource.imageurl}`} style={{maxWidth: '100%', height: '100px'}}/>
					</div>
				</div>
			);
		}
		if(field.type == 'excelimport') {
			return (
				<div key={key} className="form-group col-md-6 col-sm-6">
					<input type="file" accept='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel' id="uploadexcelfile" className="form-control hide" value={this.props.resource.excelfileurl} onChange={(evt) => {this.controller.uploadExcel(evt.target.files[0])}} required />
					<label htmlFor="uploadexcelfile" className='btn btn-sm gs-btn-info'><i className="fa fa-file-excel-o"></i>Import</label>
				</div>
			);
		}
		if(field.type == 'configureprint') {
			return (
				<Configureprintsection key={key} resource = {this.props.resource} state={this.state} array = { this.props.array } form = { this.props.form } updateFormState = { this.props.updateFormState }/>
			);
		}
		if(field.type == 'employeeimgupload') {
			return (
				<div key={key} className="form-group col-md-6 col-sm-6">
					{this.props.resource.imageurl ? <label className="labelclass">Image</label> : null}
					<div className="empprofileimg">
						<a>
							{this.props.resource.imageurl ? <img className="img-thumbnail" src={`/${this.props.resource.id ? 'documents' : 'print'}/getfile?url=${this.props.resource.imageurl}`} width="100" height="100"/> : null }
							{!this.props.resource.imageurl ? <img src="../images/defaultprofilepic.png" className="uploadedImg" /> : null }
							<span id="imguploadlabel"><b>Change</b></span>
							<input type="file" accept='image/*' className="imguploadinput" onChange={(evt) => {this.controller.uploadImageFile(evt.target.files[0])}} />
						</a>
					</div>					
				</div>
			);
		}
		if(field.type == 'usercompanydetails') {
			return (
				<UserCompaniesDetails key={key} resource = {this.props.resource} array = { this.props.array } form = { this.props.form } updateFormState = { this.props.updateFormState }/>
			);
		}

		if(field.type == 'link') {
			fieldProps.parentresource = this.state.pagejson.resourcename;
			fieldProps.parentid = this.props.resource.id;
			fieldProps.openModal = this.props.openModal;
			fieldProps.app = this.props.app;
			return (
				<div className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`} key={key}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={DocumentLinkEle} validate={tempvalidate} />
				</div>
			);
		}

		if(field.type == 'teammembersection') {
			return (
				<div key={key} className='col-sm-12 col-md-12'>
					<div className="row">
						<div className='col-md-6 col-sm-6' style={{marginBottom: '10px'}}>
							<label className="labelclass">User</label>
							<AutoSelect className={`${this.state.user ? '' : 'errorinput'}`} resource="users" fields="id,displayname,salesteamlevels,modified" valuename="id" label="displayname" value={this.state.user ? this.state.user.id : null} onChange={this.controller.userCB} />
						</div>
						<div className='col-md-6 col-sm-6' style={{marginBottom: '10px'}}>
							<button className="btn gs-btn-success" type="button" onClick={() => this.controller.userOperation("Add", this.state.user)} disabled={!this.state.user}>
								<i className="fa fa-plus"></i> Add
							</button>
						</div>
						{this.props.resource.users ? this.props.resource.users.map((user, index) => {
							return (
								<div className="col-md-4 col-sm-6" key={index}>
									<div className="input-group mb-3">
										<input type="text" className="form-control" value={user.displayname} readOnly />
										<div className="input-group-append">
											<button onClick={() => this.controller.userOperation("Delete", user)} className="btn btn-outline-secondary" type="button"><i className="fa fa-times"></i></button>
										</div>
									</div>
								</div>
							);
						}) : null}
					</div>
				</div>
			);
		}
		if(field.type == "projectestimationscostsummary") {
			return (
				<ProjectEstimationCostSummaryDetails key={key} resource={this.props.resource} form={this.props.form} updateFormState={this.props.updateFormState} openModal={this.props.openModal} />
			);
		}
		if(field.type == 'emailtagselection') {
			return (
				<EmailTagSelectionDetails key={key} resource={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} />
			);
		}

		if(field.type == 'emailattachment') {
			return (
				<div key={key} className={`form-group col-sm-12 col-md-12 ${showHideClass}`}>
					{temptitle!='undefined' ? <label className="labelclass">{temptitle}</label> : null}
					<EmailAttachmentDownloadEle resource={this.props.resource} />
				</div>
			);
		}

		if(field.type == 'expenserequestsettlementalert') {
			return (
				<div className="col-lg-12 col-md-12">
					{this.state.currentemployeeadvance > 0 ? <div className="alert alert-info"><span className="fa fa-info-circle" style={{marginRight: '5px'}}></span>{`${this.props.resource.employeeid_displayname} has unsettled amount of ${filter.currencyFilter(this.state.currentemployeeadvance, this.props.resource.currencyid, this.props.app)}`} (<span className="gs-anchor" onClick={()=>this.controller.getExpenseAdvancePickModal()}>Click to settle</span>)</div> : null }
					{this.state.currentemployeeadvance == 0 ? <div className="alert alert-info"><span className="fa fa-info-circle" style={{marginRight: '5px'}}></span>{`${this.props.resource.employeeid_displayname} has no advances`}</div> : null }
				</div>
			);
		}
		if(field.type == 'costcentermultiselectauto') {
			return (
				<div key={key} className={`form-group col-sm-12 ${colLayoutClass} ${showHideClass}`}>
					<label className="labelclass">{temptitle}</label>
					<Field name={tempmodel} props={fieldProps} component={costcenterAutoMultiSelectEle}  validate={tempvalidate}/>
				</div>
			);
		}
		if(field.type == 'contractcovereditemtotal') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 text-right ${showHideClass}`}>
					<i>Total : <b className="text-primary">{this.checkCondition('value', '{currencyFilter(this.controller.calculateCoveredItemsTotal(), resource.currencyid, app)}')}</b></i>
				</div>
			);
		}
		if(field.type == 'costcenterdetailstable') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 ${showHideClass}`} style={{marginTop:'15px'}}>
					<FieldArray name='costcenters' json={pagejson[field.json]} getFieldString={this.getFieldString} checkCondition={this.checkCondition}  
					openModal={this.openModal} updateLoaderFlag={this.updateLoaderFlag} updateFormState={this.props.updateFormState} form={this.props.form} resource={this.props.resource} component={CostenterTableElement} />
				</div>
			);
		}

		if(field.type == 'servicevisitrepeatabletable') {
			return (
				<div key={key} className={`col-sm-12 col-md-12 ${showHideClass}`} style={{marginTop:'15px'}}>
					<ServiceVisitTable resource={this.props.resource} openServiceVisit={this.controller.openServiceVisit} />
				</div>
			);
		}
		return null;
	}

	getMoreActionButton() {
		const { resourcename } = this.state.pagejson;
		var morebuttonArr = [];

		var removeArray=(this.state.pagejson.restrictedbuttons && this.state.pagejson.restrictedbuttons.length>0) ? this.state.pagejson.restrictedbuttons : [];

		var newButtonObj = this.props.app.myResources[resourcename].authorization;
		
		var key = 0;
		for (var prop in newButtonObj) {
			if (buttonMarkup[prop] && removeArray.indexOf(prop) < 0 && buttonMarkup[prop].isMoreButton) {
				key++;
				let buttonObj = this.getButton(buttonMarkup[prop], prop, key);
				if(buttonObj)
					morebuttonArr.push(buttonObj);
			}
		}
		if(morebuttonArr.length == 0)
			return null;
		return (
			<div className="btn-group dropup" style={{float:'left', width: '20%'}}>
			  <button type="button" className="btn btn-sm btn-width gs-btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More <span className="sr-only">Toggle Dropdown</span>
			  </button>
			  <div className="dropdown-menu gs-morebtn-dropdown-menu">
			    {morebuttonArr}
			  </div>
			</div>
		);
	}

	getActionButton() {
		const { resourcename } = this.state.pagejson;
		var bodybuttonArr = [];

		var removeArray=(this.state.pagejson.restrictedbuttons && this.state.pagejson.restrictedbuttons.length>0) ? this.state.pagejson.restrictedbuttons : [];

		var additionalArray=(this.state.pagejson.additionalbuttons && this.state.pagejson.additionalbuttons.length>0) ? this.state.pagejson.additionalbuttons : [];

		var newButtonObj = this.props.app.myResources[resourcename].authorization;

		bodybuttonArr.push(<button type="button" key={0} className="btn btn-sm btn-secondary btn-width closebtn" onClick={()=>{this.controller.cancel()}}><i className="fa fa-times"></i>Close</button> );
		var key = 1;
		for (var prop in newButtonObj) {
			if (buttonMarkup[prop] && removeArray.indexOf(prop) < 0 && !buttonMarkup[prop].isMoreButton) {
				key++;
				let buttonObj = this.getButton(buttonMarkup[prop], prop, key);
				if(buttonObj)
					bodybuttonArr.push(buttonObj);
			}
		}
		for (var i = 0 ; i < additionalArray.length; i++) {
			key++;
			additionalArray[i].isOutLine = true;
			bodybuttonArr.push(this.getButton(additionalArray[i], null, key));
		}
		return bodybuttonArr;
	}

	getButton(buttonObj, buttonname, key) {
		const { resourcename } = this.state.pagejson;
		let buttonName = !buttonObj.buttonname ? buttonname : buttonObj.buttonname;
		let buttonClick = buttonObj.click ? this.checkCondition('value', buttonObj.click) : () => {this.controller.save(buttonname)};
		buttonName = this.checkCondition('value', buttonName);
		let accessClass = '';
		if((buttonObj.access || resourcename) && (buttonObj.actionverb || buttonname)) {
			let btnResourceName = (buttonObj.access || resourcename);

			if (btnResourceName == 'partners') {
				if (this.props.match.path.indexOf('details/customers') >= 0 || this.props.match.path.indexOf('createCustomer') >= 0)
					btnResourceName = 'customers';

				if (this.props.match.path.indexOf('details/suppliers') >= 0 || this.props.match.path.indexOf('createSupplier') >= 0)
					btnResourceName = 'suppliers';

				if (this.props.match.path.indexOf('details/competitors') >= 0 || this.props.match.path.indexOf('createCompetitor') >= 0)
					btnResourceName = 'competitors';
			}

			accessClass = checkActionVerbAccess(this.props.app, (btnResourceName), (buttonObj.actionverb || buttonname)) ? '' : 'hide';
		}

		let buttondisabled = (this.state.loaderflag);
		if(['Cancel', 'Delete', 'Cancel', 'Hold', 'Unhold', 'Update', 'Expire', 'Send To Customer', 'Reopen', 'Close']. indexOf(buttonname) == -1  && buttonname != "" && buttonname != null  && buttonname != undefined)
			buttondisabled = (this.state.loaderflag || this.props.invalid);

		if(buttonObj.disableCondition)
			buttondisabled = this.checkCondition('if', buttonObj.disableCondition);

		if(this.checkCondition('if', buttonObj.condition) && accessClass == '')
			if(buttonObj.isMoreButton)
				return (<div key={key} className={`dropdown-item cursor-ptr gs-text-${buttonObj.buttonclass} ${accessClass}`} onClick={buttonClick} disabled={buttondisabled}><i className={`fa ${buttonObj.icon}`}></i> {buttonName}</div>);
			else
				return (<button type="button" key={key} className={`btn btn-sm gs-btn-${buttonObj.buttonclass} btn-width ${accessClass}`} onClick={buttonClick} disabled={buttondisabled}><i className={`fa ${buttonObj.icon}`}></i>{buttonName}</button>);
	}

	customFieldsOperation(resourcename, destObj, souObj,destinationresourcename, itemstr) {
		commonMethods.customFieldsOperation(this.props.app.myResources, resourcename, destObj, souObj, destinationresourcename, itemstr);
	}

	getCustomFields(resourcename, returntype, restrictJSONField) {
		return commonMethods.getCustomFields(this.props.app.myResources, resourcename, returntype, restrictJSONField);
	}

	/*closeModal(modal) {
		let key = -1;
		this.state.modalArray.map((item, index) => {
			if(item.id == modal.id)
				key = index;
		});
		if(this.state.modalArray.length > -1)
			this.state.modalArray.splice(key, 1);
		this.setState({
			modalArray :  [...this.state.modalArray]	
		});
	}

	openModal(modal) {
		modal.id = uuidv1();
		modal.isOpen = true;
		this.setState({
			modalArray :  [...this.state.modalArray, modal]	
		});
	}

	renderModal() {
		return this.state.modalArray.map((modal, key) => {
			return (
				<Modal
					key={key}
					isOpen={modal.isOpen}
					onRequestClose={() => {modal.confirmModal ? null : this.closeModal(modal)}}
					shouldCloseOnOverlayClick={modal.confirmModal ? false : true}
					ariaHideApp={false}
					aria={{
					labelledby: "heading",
					describedby: "full_description"}}
					className={modal.className.content}
					overlayClassName={modal.className.overlay}>
					{modal.render(() => this.closeModal(modal))}
				</Modal>
			);
		});
	}*/

	getPromptMessage(location) {
		if(this.state.createParam) {
			for(var prop in routes) {
				let matchedRoute = matchPath(location.pathname, {
					path: prop,
					exact: true,
					strict: false
				});
				if(matchedRoute) {
					if(routes[this.props.match.path].pageoptions.title == routes[prop].pageoptions.title && routes[this.props.match.path].pageoptions.type == routes[prop].pageoptions.type)
						return true;
				}
			}
		}
		return `Unsaved Changes`;
	}

	renderEachAlert(alert, index) {
		if(alert.type == 'directive')
			return <AlertView key={index} status={this.props.resource.status} notes={this.state.ndt.notes} />
		if(alert.type == 'normal') {
			let alertIf = alert.if ? this.checkCondition('if', alert.if) : false;
			let alertBody = this.checkCondition('value', alert.body);
			if(alertIf) {
				return <div key={index} className="alert alert-info">{alertBody()}</div>
			}
		}

		return null;
	}

	renderAlert() {
		if(!this.state.pagejson.alertarray || this.state.pagejson.alertarray.length == 0)
			return null;

		return (
			<div className="alert-section gs-detail-alert-section">
				{this.state.pagejson.alertarray.map((alert, index) => {
					return this.renderEachAlert(alert, index);
				})}
			</div>
		);
	}

	render() {
		if(!this.state.pagejson || !this.props.resource)
			return <Loadingcontainer isloading={true}></Loadingcontainer>

		let disableclass = this.props.resource && statusArray.indexOf(this.props.resource.status) >= 0 ? 'disablediv' : '';
		let fixed_style_body = {};
		if($(window).outerWidth() > 767) {
			fixed_style_body = {
				marginTop: `${this.props.isModal ? '0' : ((document.getElementsByClassName('affixmenubar').length > 0 ? document.getElementsByClassName('affixmenubar')[0].clientHeight : 0) + 3)}px`,
				marginBottom: `${this.props.isModal ? '0' : (document.getElementsByClassName('actionbtn-bar').length > 0 ? (document.getElementsByClassName('actionbtn-bar')[0].clientHeight - 40) : 40)}px`
			}
		}
		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<Prompt when={!this.state.dontDirtyCheck && this.props.dirty && this.props.anyTouched} message={this.getPromptMessage} />
					<Affixmenu ref="AffixMenu" resourcejson={this.props.app.myResources[this.state.pagejson.resourcename]} resource={this.props.resource} pagejson={this.state.pagejson} isModal={this.props.isModal} openModal={this.props.openModal} updateFormState={this.props.updateFormState} form={this.props.form} initialize={this.props.initialize} app={this.props.app} history={this.props.history} viewtype={this.state.viewtype} updateLoaderFlag={this.updateLoaderFlag} savefn = {this.controller.save} checkCondition={this.checkCondition} />
					<div className={`${this.props.isModal ? '' : 'row'} ${disableclass}`}>
						<div className={`${this.props.isModal ? '' : `col-sm-12 col-md-12 col-lg-${this.props.isModal ? '12' : '9'}`}`}>
							<div className={`${this.props.isModal ? 'react-modal-body react-modal-body-scroll' : 'row'}`}  style={fixed_style_body}>
								{this.state.pagejson.showsalesactivity && this.checkCondition('if', this.state.pagejson.showsalesactivity.if) ? <ActivitySection 
								parentid = {this.props.resource.id} 
								resourcename = {this.state.pagejson.resourcename} 
								leadid = {this.checkCondition('value', this.state.pagejson.showsalesactivity.leadid)}
								quoteid = {this.checkCondition('value', this.state.pagejson.showsalesactivity.quoteid)} 
								customerid = {this.checkCondition('value', this.state.pagejson.showsalesactivity.customerid)} 
								contactid = {this.checkCondition('value', this.state.pagejson.showsalesactivity.contactid)} 
								email = {this.checkCondition('value', this.state.pagejson.showsalesactivity.email)}
								expectedclosuredate = {this.checkCondition('value', this.state.pagejson.showsalesactivity.expectedclosuredate)} 
								modifiedcallback = {this.checkCondition('value', this.state.pagejson.showsalesactivity.modifiedcallback)} 
								openModal = {this.props.openModal}
								createOrEdit = {this.props.createOrEdit} 
								contactemail = {this.props.resource.email}
								/> : null }
								{this.state.pagejson.showserviceactivity && this.checkCondition('if', this.state.pagejson.showserviceactivity.if) ? <ServiceActivitySection servicecallid = {this.props.resource.id} openModal={this.props.openModal} /> : null}

								{this.state.pagejson.showleavebalancedetails && this.checkCondition('if', this.state.pagejson.showleavebalancedetails.if) && checkActionVerbAccess(this.props.app, this.state.pagejson.showleavebalancedetails.access, this.state.pagejson.showleavebalancedetails.actionverb) ? <EmployeeLeaveBalanceDetails resource={this.props.resource} /> : null }

								{this.props.isModal ? <div className="row">{this.renderPageJSON()}</div> : this.renderPageJSON()}
							</div>
						</div>
						{this.props.isModal ? null : <div className={`col-sm-12 ndt-section col-md-12 col-lg-${this.props.isModal ? '12 margintop-5 ' : '3'} pl-lg-0 `} >
							{!this.props.isModal && this.props.resource && this.props.resource.id && this.state.ndt ? this.renderAlert() : null}
							{this.state.pagejson.showndt && !this.props.isModal && this.props.resource && this.props.resource.id && this.state.ndt ? <AdditionalInformationSection updateAppState = {this.props.updateAppState} ndt={this.state.ndt} key={0} parentresource={this.state.pagejson.resourcename} openModal={this.props.openModal} parentid={this.props.resource.id} relatedpath={this.props.match.path} createOrEdit = {this.props.createOrEdit} app ={this.props.app} history={this.props.history}/> : null }
						</div> }
					</div>
					{this.props.isModal ? <div className="react-modal-footer text-center">
						<div className="row">
							<div className="col-md-10">{this.getActionButton()}</div>
							<div className="col-md-2">{this.getMoreActionButton()}</div>
						</div>
					</div> : <div className="row" style={{paddingTop: '40px'}}>
						<div className="col-md-12 col-lg-9">
							<div className="muted credit text-center sticky-footer actionbtn-bar">
								<div style={{width:'80%', float: 'left'}}>{this.getActionButton()}</div>
								{this.getMoreActionButton()}
							</div>
						</div>
					</div>}
				</form>
			</>
		);
	};
}

export class ChildElement extends Component {
	constructor(props) {
		super(props);
		this.state = {
			editObj : {}
		};

		this.renderTableHead = this.renderTableHead.bind(this);
		this.addNewRow = this.addNewRow.bind(this);
		this.addNewAboveRow = this.addNewAboveRow.bind(this);
		this.openRow = this.openRow.bind(this);
		this.deleteRow = this.deleteRow.bind(this);
		this.reorderitem = this.reorderitem.bind(this);
		this.getResponsiveClass = this.getResponsiveClass.bind(this);
		this.estimationDetailedfn = this.estimationDetailedfn.bind(this);
		this.addEstimationDetailedItemfn = this.addEstimationDetailedItemfn.bind(this);
		this.addEquipment = this.addEquipment.bind(this);
		this.addItem = this.addItem.bind(this);
		this.onDragEnd = this.onDragEnd.bind(this);
	}

	addNewRow(param) {
		var addfn = this.props.checkCondition('value', this.props.json.addcallback);

		if(typeof(addfn) == 'function') {
			addfn();
		} else {
			let { editObj } = this.state;
			let tempObj = {};
			if(param) {
				tempObj.itemtype = param;
			}
			this.props.fields.push(tempObj);
			this.props.fields.map((a, key)=> {
				editObj[key] = false;
			});
			editObj[this.props.fields.length] = true;
			this.setState({editObj: editObj});
		}
	}

	addNewAboveRow(index) {
		var addfn = this.props.checkCondition('value', this.props.json.addcallback);

		if(this.props.json.withitemtype) {
			this.props.openModal({
				render: (closeModal) => {
					return <ProjectItemTableAddmodal openModal={this.props.openModal} closeModal={closeModal} callback={(itemtype) => {
							let { editObj } = this.state;
							this.props.fields.insert(index, {
								itemtype: itemtype
							});
							this.props.fields.map((a, key)=> {
								editObj[key] = false;
							});
							editObj[index] = true;
							this.setState({editObj: editObj});
					}}/>
				}, className: {content: 'react-modal-custom-class-30', overlay: 'react-modal-overlay-custom-class'}
			});
		} else {
			if(typeof(addfn) == 'function') {
				addfn();
			} else {
				let { editObj } = this.state;
				this.props.fields.insert(index, {});
				this.props.fields.map((a, key)=> {
					editObj[key] = false;
				});
				editObj[index] = true;
				this.setState({editObj: editObj});
			}
		}
	}

	estimationDetailedfn(index) {
		let estimationdetailedfn = this.props.checkCondition('value', this.props.json.estimationdetailedcallback);

		if(typeof(estimationdetailedfn) == 'function') {
			estimationdetailedfn(index);
		}
	}

	addEstimationDetailedItemfn(index) {
		let addEstimationDetailedItemfn = this.props.checkCondition('value', this.props.json.addEstimationDetailedItem);

		if(typeof(addEstimationDetailedItemfn) == 'function') {
			addEstimationDetailedItemfn(index);
		}
	}

	openRow(index) {
		let { editObj } = this.state;
		this.props.fields.map((a, key)=> {
			editObj[key] = (key == index) ? true : false;
		});
		this.setState({editObj: editObj});
	}

	deleteRow(index, parentitemstr) {
		var deletefn = this.props.checkCondition('value', this.props.json.deletecallback);

		if(typeof(deletefn) == 'function') {
			deletefn(index, parentitemstr);
		} else {
			let { editObj } = this.state;
			this.props.fields.remove(index);
			this.props.fields.map((a, key)=> {
				editObj[key] = false;
			});

			this.setState({editObj: editObj});
			setTimeout(() => {
				this.props.computeFinalRate ? this.props.computeFinalRate() : null;
			}, 0);
		}
	}

	reorderitem(item) {
		let removeitemObj = this.props.checkCondition('value', `{resource.${this.props.fields.name}[${item.sourceindex}]}`);
		this.props.array.splice(this.props.fields.name, item.sourceindex, 1);
		this.props.array.splice(this.props.fields.name, item.index, 0, removeitemObj);
	}

	renderTableHead() {
		let key=0;
		return this.props.json.headers.map((a)=> {
			key++;
			if(!this.props.checkCondition('if', a.if) && a.type !='button')
				return null;

			let tempheaderlabel = '';
			tempheaderlabel = this.props.checkCondition('value', a.label);
			let styleObj = {};
			if(a.width)
				styleObj.width = `${a.width}%`;
			return <th style={styleObj} className={a.label == 'Amount' ? 'text-right' : (a.align == 'center' ? 'text-center' : '')} key={key}>{tempheaderlabel}</th>
		});
	}
	
	getResponsiveClass() {
		let { editObj } = this.state;
		if(Object.keys(editObj).length > 0) {
			for(var key in editObj) {
				if(editObj[key] == true)
					return '';
			}
			return 'table-responsive';
		}
		return 'table-responsive';
	}

	addEquipment(evt) {
		evt.preventDefault();
		var addfn = this.props.checkCondition('value', this.props.json.addcallback);
		if(typeof(addfn) == 'function')
			addfn();
	}

	addItem() {
		let { editObj } = this.state;
		let tempObj = {};
		this.props.fields.push(tempObj);
		this.props.fields.map((a, key)=> {
			editObj[key] = false;
		});
		editObj[this.props.fields.length] = true;
		this.setState({editObj: editObj});
	}
	
	onDragEnd = (result) => {
		if (!result.destination || result.destination.index === result.source.index)
			return;

		let removeitemObj = this.props.checkCondition('value', `{resource.${this.props.fields.name}[${result.source.index}]}`);
		this.props.array.splice(this.props.fields.name, result.source.index, 1);
		this.props.array.splice(this.props.fields.name, result.destination.index, 0, removeitemObj);
	};

	render() {
		let addRow = this.props.checkCondition('if', this.props.json.addrow);
		let showdeletebtn = this.props.checkCondition('if', this.props.json.deletebtn);
		let grideditcallback = this.props.checkCondition('value', this.props.json.grideditcallback);
		let showimportexportbtn = this.props.checkCondition('if', this.props.json.showimportexportbtn);
		let showmarkupbtn = this.props.checkCondition('if', this.props.json.showmarkupbtn);
		let markupbtncallback = this.props.checkCondition('value', this.props.json.markupbtncallback);
		let showInvoiceMilestoneProgressExportExcelBtn = this.props.checkCondition('value', this.props.json.showinvoicemilestoneprogressexportexcelbtn);
		let invoicemilestoneprogressexcelexportfn = this.props.checkCondition('value', this.props.json.invoicemilestoneprogressexcelexportfn);

		return (
			<div className={this.getResponsiveClass()}>
				{this.props.json.showgrideditbtn || this.props.json.withitemtype ? <div className="float-right form-group">
					{this.props.json.showgrideditbtn ? <button type="button" className="btn btn-sm gs-form-btn-success btn-width" onClick={()=>grideditcallback()}><i className="fa fa-pencil-square-o"></i>Grid View</button> : null}
					{showimportexportbtn && (this.props.resourcename == 'projects' || this.props.resourcename == 'projectquotes') ? <BOQImportExportButtonComponent resourcename={this.props.resourcename} resource = {this.props.resource}  updateFormState = {this.props.updateFormState}  computeFinalRate = {this.props.computeFinalRate} onLoadKit = {this.props.onLoadKit} getimportMilestoneItems= {this.props.getimportMilestoneItems} form = {this.props.form} openModal={this.props.openModal} /> : null}
					{(showimportexportbtn && this.props.resourcename == 'projectestimations') ? <ProjectEstimationImportExportButtonComponent resourcename={this.props.resourcename} resource = {this.props.resource}  updateFormState = {this.props.updateFormState}  computeFinalRate = {this.props.computeFinalRate} form = {this.props.form} openModal={this.props.openModal} /> : null}
					{showmarkupbtn && (this.props.resourcename == 'projects' || this.props.resourcename == 'projectquotes') ? <button type="button" className="btn btn-sm gs-form-btn-success btn-width" onClick={() => markupbtncallback()}>Add Markup</button> : null}
				</div> : null}
				{showInvoiceMilestoneProgressExportExcelBtn  && checkActionVerbAccess(this.props.app, 'salesinvoices', 'Print') ? <div className="float-right form-group">
					<button type="button" className="btn btn-sm gs-form-btn-success btn-width" onClick={()=>invoicemilestoneprogressexcelexportfn()}><i className="fa fa-download"></i>Download Running Bill</button> 
				</div> : null}
				<DragDropContext
					onDragEnd={this.onDragEnd}
				>
				<table className="table gs-table gs-item-table-bordered gs-item-table tablefixed">
					<thead>
						<tr>
							{!this.props.json.withitemtype && this.props.resourcename != 'projectestimations' ? <th style={{width: '3%'}}>#</th> : null}
							{this.renderTableHead()}
							<th style={{width: '7%'}}></th>
						</tr>
					</thead>
					<Droppable droppableId="table">
						{(droppableProvided, droppableSnapshot) => (
							<tbody ref={droppableProvided.innerRef} {...droppableProvided.droppableProps}>
								{this.props.fields.map((member, index) => {
									return (
										<Draggable draggableId={`draggable-${index}`} index={index} key={index} isDragDisabled={this.state.editObj[index]}
										>
											{(draggableProvided, draggableSnapshot) => (
												<TableRow key={index} index={index} member={member} {...this.props} editObj={this.state.editObj} addNewRow={this.addNewRow} addNewAboveRow={this.addNewAboveRow} openRow={this.openRow} deleteRow={this.deleteRow} reorderitem={this.reorderitem} app={this.props.app} estimationDetailedfn={this.estimationDetailedfn} addEstimationDetailedItemfn={this.addEstimationDetailedItemfn} parentitemstr={this.props.parentitemstr} provided={draggableProvided} snapshot={draggableSnapshot} />
											)}
										</Draggable>
									);
								})}
								{droppableProvided.placeholder}
							</tbody>
						)}
					</Droppable>
				</table>
				</DragDropContext>
				{addRow && !this.props.json.withitemtype ? <button type="button" className="btn btn-sm gs-form-btn-primary btn-width btnhide" onClick={() => this.addNewRow()}><i className="fa fa-plus"></i>Add</button> : null }
				{this.props.json.withitemtype == true ? <div><button type="button" className="btn btn-sm gs-form-btn-primary btn-width btnhide" onClick={() => this.addNewRow('Item')}><i className="fa fa-plus"></i>Item</button><button type="button" className="btn btn-sm gs-form-btn-primary btn-width btnhide" onClick={() => this.addNewRow('Section')}><i className="fa fa-plus"></i>Section</button></div> : null }
				{this.props.resourcename == "contractenquiries" && this.props.fields.name == 'contractenquiryitems' ? <div>
					<button type="button" className="btn btn-sm gs-form-btn-primary btn-width btnhide" onClick={this.addEquipment}><i className="fa fa-plus"></i>Add Equipment</button>
					{this.props.resource.contractfor == 'Equipment' ? <button type="button" className="btn btn-sm gs-form-btn-primary btn-width btnhide" onClick={() => this.addItem()}><i className="fa fa-plus"></i>Add Item</button> : null }
				</div> : null }
			</div>
		);
	}
}

class AdditionalChargesElement extends Component {
	constructor(props) {
		super(props);

		this.addNewRow = this.addNewRow.bind(this);
		this.deleteRow = this.deleteRow.bind(this);
	}

	addNewRow() {
		this.props.fields.push({});
	}

	deleteRow(index) {
		this.props.fields.remove(index);
		setTimeout(() => {
			this.props.computeFinalRate ? this.props.computeFinalRate() : null;
		}, 0);
	}

	addChaCB(value, valueObj, member) {
		var tempObj = {
			[`${member}.description`] : valueObj.description,
			[`${member}.defaultpercentage`] : valueObj.defaultpercentage
		};
		this.props.updateFormState(this.props.form, tempObj);
		setTimeout(this.props.computeFinalRate, 0);
	}

	render() {
		return (
			<div className="table-responsive">
				<table className="table gs-table gs-item-table-bordered">
					<thead>
						<tr>
							<th colSpan='6' className="gs-text-color bordertop-0">Additional Charges</th>
						</tr>
						<tr>
							<th className="text-center" style={{width:'25%'}}>Name</th>
							<th className="text-center" style={{width:'30%'}}>Description</th>
							<th className="text-center" style={{width:'10%'}}>Default %</th>
							<th className="text-center" style={{width:'15%'}}>Amount</th>
							<th className="text-center" style={{width:'20%'}}>Tax</th>
						<th></th>
						</tr>
					</thead>
					<tbody>
						 {this.props.fields.map((member, index) => {
							let addChaObj = this.props.checkCondition('value', `{resource.${member}}`);
							return (
								<tr key={index}>
									<td>
										<Field name={`${member}.additionalchargesid`} props={{
											resource : 'additionalchargesmaster',
											fields: 'id,name,description,applyforvaluation,defaultpercentage,displayorder',
											required : true,
											onChange: (value, valueObj) => this.addChaCB(value, valueObj, member)
										}} component={selectAsyncEle}  validate={[numberNewValidation({required: true})]}/>
									</td>
									<td>
										<Field name={`${member}.description`} props={{
											required : true
										}} component={InputEle}  validate={[stringNewValidation({required: true})]}/>
									</td>
									<td>
										<Field name={`${member}.defaultpercentage`} props={{
											required : true,
											onChange : this.props.computeFinalRate
										}} component={NumberEle} />
									</td>
									<td>
										<Field name={`${member}.amount`} props={{
											required : true,
											disabled: addChaObj.defaultpercentage ? true : false,
											onChange : this.props.computeFinalRate
										}} component={NumberEle} validate={[numberNewValidation({required: true})]} />
									</td>
									<td>
										<Field name={`${member}.taxcodeid`} props={{
											options: this.props.taxarray,
											label: 'taxcode',
											valuename : 'id',
											onChange : this.props.computeFinalRate
										}} component={localSelectEle} />
									</td>
									<td>
										<button type='button' className='btn gs-form-btn-danger btn-sm btndisable' onClick={() => this.deleteRow(index)}><span className='fa fa-trash-o'></span></button>
									</td>
								</tr>
							)
						 })}
					</tbody>
				</table>
				<button type="button" className="btn btn-sm gs-form-btn-primary btn-width btnhide" onClick={this.addNewRow}><i className="fa fa-plus"></i>Add</button>
			</div>
		);
	}
}

class PriceListVersionsElement extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		if(this.props.fields.length == 0)
			return (<div className="alert alert-info text-center">No versions created for this pricelist</div>);
		return (
			<div className="table-responsive">
				<table className="table table-hover">
					<thead className="thead-light">
						<tr>
							<th className="text-center" style={{width:'10%'}}>Version No</th>
							<th className="text-center" style={{width:'15%'}}>Effective Date</th>
							<th className="text-center" style={{width:'15%'}}>Expiry Date</th>
							<th className="text-center">Description</th>
							<th className="text-center" style={{width:'15%'}}>Status</th>
							<th style={{width:'10%'}}></th>
						</tr>
					</thead>
					<tbody>
						 {this.props.fields.map((member, index) => {
							let versionObj = this.props.checkCondition('value', `{resource.${member}}`);
							let effectivedate = this.props.checkCondition('value', `{dateFilter(item.effectivedate)}`, versionObj);
							let expirydate = this.props.checkCondition('value', `{dateFilter(item.expirydate)}`, versionObj);
							return (
								<tr key={index}>
									<td onClick={() => this.props.getPriceVersionDetails(versionObj)}>{versionObj.versionno}</td>
									<td onClick={() => this.props.getPriceVersionDetails(versionObj)}>{effectivedate}</td>
									<td onClick={() => this.props.getPriceVersionDetails(versionObj)}>{expirydate}</td>
									<td onClick={() => this.props.getPriceVersionDetails(versionObj)}>{versionObj.remarks}</td>
									<td onClick={() => this.props.getPriceVersionDetails(versionObj)}>{versionObj.status}</td>
									<td>
										{!this.props.resource.disableversion ? <button type='button' className='btn btn-secondary btn-sm btn-width' onClick={() => this.props.copyPriceListVersion(versionObj)} disabled={this.props.resource.disableversion} title="Copy Price List Version"><span className='fa fa-clipboard'></span></button> : null}
									</td>
								</tr>
							)
						 })}
					</tbody>
				</table>
			</div>
		);
	}
}

class PriceListItemsElement extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pricelistitems: [],
			filterArray: [],
			search: {},
			refresh: this.props.refresh,
			itemtable: {
				pagelength: 20,
				page: 0,
				counts: [20, 50, 100, 500]
			}
		};
		this.renderPageCounts = this.renderPageCounts.bind(this);
		this.renderPagination = this.renderPagination.bind(this);
		this.handlePaginationClick = this.handlePaginationClick.bind(this);
		this.pagecountChange = this.pagecountChange.bind(this);
		this.getPriceListItems = this.getPriceListItems.bind(this);
		this.filterInputChange = this.filterInputChange.bind(this);
		this.refreshFilter = this.refreshFilter.bind(this);
	}

	componentWillMount() {
		this.getPriceListItems();
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.refresh != this.state.refresh) {
			this.setState({
				refresh: nextProps.refresh
			}, () => {
				this.getPriceListItems();
			});
		}
	}

	getPriceListItems() {
		this.props.updateLoaderFlag(true);
		let { itemtable } = this.state;

		let queryString = `/api/pricelistitems?pagelength=${this.state.itemtable.pagelength}&field=id,itemid,lastpurchasecost,defaultpurchasecost,recommendedsellingprice,minimumsellingprice,itemmaster/name/itemid,itemmaster/description/itemid&skip=${this.state.itemtable.page * this.state.itemtable.pagelength}&sortstring=pricelistitems.id&filtercondition=pricelistitems.parentid=${this.props.resource.id} ${this.state.filterArray.length > 0 ? ` and ${this.state.filterArray.join(' and ')}` : '' }`;

		axios.get(`${queryString}`).then((response) => {
			if (response.data.message == 'success') {
				itemtable.count = response.data.count;
				this.setState({
					pricelistitems: response.data.main,
					itemtable
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.props.updateLoaderFlag(false);
		});
	}

	handlePaginationClick(obj) {
		let {itemtable} = this.state;
		itemtable.page = obj.selected;
		this.setState({
			itemtable: itemtable
		});
		this.getPriceListItems();
	}

	pagecountChange(count) {
		let {itemtable} = this.state;
		itemtable.pagelength = count;
		itemtable.page = 0;
		this.setState({
			itemtable: itemtable
		});
		this.getPriceListItems();
	}

	renderPageCounts() {
		if(!this.state.itemtable || !this.state.pricelistitems)
			return null;
		return [20, 50, 100, 500].map((count, btnkey) => {
			return <button type="button" className={`btn btn-sm btn-secondary ${this.state.itemtable.pagelength == count ? 'active' : ''}`} key={btnkey} onClick={() =>{this.pagecountChange(count)}}>{count}</button>
		});
	}

	renderPagination() {
		if(this.state.itemtable && Math.ceil(this.state.itemtable.count / this.state.itemtable.pagelength) > 1)
			return <ReactPaginate previousLabel={<span>&laquo;</span>}
				nextLabel={<span>&raquo;</span>}
				breakLabel={<a></a>}
				breakClassName={"break-me"}
				pageCount={Math.ceil(this.state.itemtable.count / this.state.itemtable.pagelength)}
				marginPagesDisplayed={0}
				pageRangeDisplayed={5}
				onPageChange={this.handlePaginationClick}
				containerClassName={"list-pagination"}
				subContainerClassName={"pages pagination"}
				activeClassName={"active"}
				forcePage={this.state.itemtable.page} />
		return null;
	}

	filterInputChange(value, field) {
		let { search, filterArray } = this.state;
		search[field] = value;
		filterArray.push(`${(field == 'itemid') ? `pricelistitems.itemid = ${value}` : `itemmaster_itemid.${field} = ${value}`}`);
		this.setState({ search, filterArray });
	}

	refreshFilter() {
		this.setState({search: {}, filterArray: []}, () => {
			this.getPriceListItems();
		});
	}

	render() {
		return (
			<div>
			{this.state.pricelistitems.length > 0 || Object.keys(this.state.search).length > 0 ? <div className="row">
				<div className="form-group col-md-3">
					<AutoSelect className="ndtclass" value={this.state.search.itemid} resource={'itemmaster'} fields={'id,name,displayname'} label={'displayname'} displaylabel={'name'} placeholder={'Select Item Name'} onChange={(value) => this.filterInputChange(value, 'itemid')} />
				</div>
				<div className="form-group col-md-3">
					<AutoSelect className="ndtclass" value={this.state.search.itemgroupid} resource={'itemgroups'} fields={'id,fullname'} label={'fullname'}  placeholder={'Select Item Group'} onChange={(value) => this.filterInputChange(value, 'itemgroupid')} />
				</div>
				<div className="form-group col-md-3">
					<AutoSelect className="ndtclass" value={this.state.search.itemcategorymasterid} resource={'itemcategorymaster'} fields={'id,name'} label={'name'}  placeholder={'Select Item Category'} onChange={(value) => this.filterInputChange(value, 'itemcategorymasterid')} />
				</div>
				<div className="form-group col-md-3">
					<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.getPriceListItems} >Search</button>
					<button type="button" className="btn btn-sm gs-btn-info btn-width" onClick={this.refreshFilter} ><span className="fa fa-refresh"></span></button>
				</div>
			</div> : null}
			{this.state.pricelistitems.length > 0 ? <div className="row">
				<div className="col-md-12">
					<div className="table-responsive">
						<table className="table table-sm">
							<thead className="thead-light">
								<tr>
									<th className="text-center">Name</th>
									<th className="text-center">Description</th>
									<th className="text-center">Last Purchase Cost</th>
									<th className="text-center">Default Purchase Cost</th>
									<th className="text-center">Recommended Selling Price</th>
									<th className="text-center">Minimum Selling Price</th>
								</tr>
							</thead>
							<tbody>
								 {this.state.pricelistitems.map((itemObj, index) => {
									return (
										<tr key={index}>
											<td>{itemObj.itemid_name}</td>
											<td>{itemObj.itemid_description}</td>
											<td>{filter.currencyFilter(itemObj.lastpurchasecost, null, this.props.app)}</td>
											<td>{filter.currencyFilter(itemObj.defaultpurchasecost, null, this.props.app)}</td>
											<td>{filter.currencyFilter(itemObj.recommendedsellingprice, this.props.resource.currencyid, this.props.app)}</td>
											<td>{filter.currencyFilter(itemObj.minimumsellingprice, this.props.resource.currencyid, this.props.app)}</td>
										</tr>
									)
								 })}
							</tbody>
						</table>
					</div>
				</div>
				<div className="col-md-12">
					<div className="btn-group btn-group-sm list-pagecount float-left" role="group">
						{this.renderPageCounts()}
					</div>
					<div className="btn-group float-right">
						{this.renderPagination()}
					</div>
				</div>
			</div> : null }
			</div>
		);
	}
}

class AccountingformsElement extends Component {
	constructor(props) {
		super(props);

		this.addNewRow = this.addNewRow.bind(this);
		this.deleteRow = this.deleteRow.bind(this);
	}

	addNewRow() {
		this.props.fields.push({
			type: 'Receive'
		});
	}

	deleteRow(index) {
		this.props.fields.remove(index);
	}

	render() {
		return (
			<div className="table-responsive">
				<table className="table">
					<thead className="thead-light">
						<tr>
							<th className="text-center" style={{width:'15%'}}>Form Name</th>
							<th className="text-center" style={{width:'15%'}}>Type</th>
							<th className="text-center" style={{width:'25%'}}>Serial No</th>
							<th className="text-center" style={{width:'20%'}}>Form No</th>
							<th className="text-center" style={{width:'20%'}}>Form Date</th>
						<th></th>
						</tr>
					</thead>
					<tbody>
						 {this.props.fields.map((member, index) => {
							let accformObj = this.props.checkCondition('value', `{resource.${member}}`);
							return (
								<tr key={index}>
									<td>
										<Field name={`${member}.formid`} props={{
											resource : 'accountingformsmaster',
											fields: 'id,formname,applyforsales,applyforpurchase',
											label: "formname",
											required : true
										}} component={selectAsyncEle}  validate={[numberValidation]}/>
									</td>
									<td>
										<Field name={`${member}.type`} props={{
											options: ["Receive", "Issue"]
										}} component={localSelectEle} />
									</td>
									<td>
										<Field name={`${member}.seriesnumber`} component={InputEle} />
									</td>
									<td>
										<Field name={`${member}.formnumber`} props={{
											required : accformObj.seriesnumber!=null ? true : false
										}} component={InputEle} validate={accformObj.seriesnumber!=null ? [requiredValidation] : []}/>
									</td>
									<td>
										<Field name={`${member}.formdate`} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} />
									</td>
									<td>
										<button type='button' className='btn gs-btn-danger btn-sm btndisable' onClick={() => this.deleteRow(index)}><span className='fa fa-trash-o'></span></button>
									</td>
								</tr>
							)
						 })}
					</tbody>
				</table>
				<button type="button" className="btn btn-sm gs-btn-info btn-width btnhide" onClick={this.addNewRow}><i className="fa fa-plus"></i>Add</button>
			</div>
		);
	}
}

class Affixmenu extends Component {
	constructor(props) {
		super(props);
		let titleField = '';
		for(var prop in this.props.resourcejson.fields) {
			if(this.props.resourcejson.fields[prop].title) {
				titleField = this.props.resourcejson.fields[prop].isForeignKey ? `${prop}_${this.props.resourcejson.fields[prop].foreignKeyOptions.mainField.split('.')[1]}` : prop;
			}
		}
		if(this.props.resource.id && !this.props.isModal)
			document.getElementById("pagetitle").innerHTML = `${this.props.pagejson.pagename} - ${this.props.resource[titleField] != null ? this.props.resource[titleField] : ''}`;
		this.state = {
			titleField
		};
		this.gotoListPage = this.gotoListPage.bind(this);
		this.printfunction = this.printfunction.bind(this);
		this.amendfunction = this.amendfunction.bind(this);
		this.amendCallBack = this.amendCallBack.bind(this);
		this.copyfunction = this.copyfunction.bind(this);
		this.copyCallBack = this.copyCallBack.bind(this);
		this.doctraceFunction = this.doctraceFunction.bind(this);
		this.openNavLink = this.openNavLink.bind(this);
		this.playRecording = this.playRecording.bind(this);
		this.amendcompare = this.amendcompare.bind(this);
	}

	gotoListPage() {
		if(this.props.pagejson.resourcename == 'partners')
			return this.props.history.goBack();

		this.props.history.push(this.props.pagejson.backpage);
	}

	printfunction(templatetype) {
		this.props.openModal({
			render: (closeModal) => {
				return <Printmodal templatetype={templatetype} app={this.props.app} resourcename={this.props.pagejson.resourcename} companyid={this.props.resource.companyid} id={this.props.resource.id} amendparentid={this.props.resource.amendparentid} openModal={this.props.openModal} closeModal={closeModal}/>
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	versioncallback (obj, versionNo) {
		if(this.props.pagejson.resourcename == 'estimations') {
			let tempObj =  obj;
			tempObj.status = 'Version History';
			tempObj.componentitems = [];
			tempObj.serviceitems = [];
			tempObj.expenseitems = [];
			for (var i = 0; i < tempObj.estimationitems.length; i++) {
				if (tempObj.estimationitems[i].itemid_itemtype == 'Product') {
					tempObj.componentitems.push(tempObj.estimationitems[i]);
				}
				if(tempObj.estimationitems[i].itemid_itemtype == 'Service'){
					tempObj.serviceitems.push(tempObj.estimationitems[i]);
				}
				if(tempObj.estimationitems[i].itemid_itemtype == 'Expense') {
					tempObj.expenseitems.push(tempObj.estimationitems[i]);
				}
			}
			this.props.initialize(tempObj);
		} else if(this.props.pagejson.resourcename == 'projectestimations') {
			let tempEstimationObj = obj;
			tempEstimationObj.status = 'Version History';

			tempEstimationObj.estimationitems = [];

			let itemprop = tempEstimationObj.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';
			let uniqueItemsArr = [];
			let uniqueItemObj = {};
			let overheaditems = [];

			tempEstimationObj.projectestimationitems.forEach((item) => {
				if(item[itemprop] > 0) {
					if(!uniqueItemObj[item[itemprop]]) {
						uniqueItemObj[item[itemprop]] = {
							[`${itemprop}`]: item[itemprop],
							[`${itemprop}_displayorder`]: item[`${itemprop}_displayorder`],
							items: []
						};
					}
					uniqueItemObj[item[itemprop]].items.push(item);
				} else {
					item.internalrefno = 'Overhead';
					overheaditems.push(item);
				}
			});

			uniqueItemsArr = Object.keys(uniqueItemObj).map(prop => uniqueItemObj[prop]);

			//uniqueItemsArr.sort((a, b) => a[`${itemprop}_displayorder`] - b[`${itemprop}_displayorder`] );

			uniqueItemsArr.forEach((item) => {
				let tempParentobj = {
					...item.items[0],
					estimationtype : item.items.length > 1 || (item.items[0].itemid != item.items[0].boqitemid) ? 'Detailed' : 'Simple',
					isparent: true
				};

				tempEstimationObj.estimationitems.push(tempParentobj);

				if(tempParentobj.estimationtype == 'Detailed') {
					item.items.forEach((propitem) => {
						propitem.isparent = false;
						tempEstimationObj.estimationitems.push(propitem);
					});
				}
			});

			tempEstimationObj.estimationitems.push({
				isoverhead: true,
				internalrefno: 'Overhead',
				rate: 0
			});
			tempEstimationObj.estimationitems.push(...overheaditems);
			this.props.initialize(tempEstimationObj);
		} else {
			let tempObj =  obj;
			tempObj.status = 'Version History';
			this.props.initialize(tempObj);
		}
		if(versionNo) {
			this.setState({amendVersionNo: versionNo});
		}
	}

	amendCallBack () {
		this.props.savefn('Amend');
	}

	amendfunction() {
		this.props.openModal({
			render: (closeModal) => {
				return <AmendModal pagename={this.props.pagejson.pagename} resourcename={this.props.pagejson.resourcename} resource={this.props.resource} callback={this.amendCallBack} openselectedVersion={(obj, versionNo) => {this.versioncallback(obj, versionNo)}}  openModal={this.props.openModal} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	copyfunction() {
		this.props.openModal({
			render: (closeModal) => {
				return <CopyModal callback={this.copyCallBack} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	copyCallBack() {
		let tempObject = {};
		let resource = this.props.app.myResources[this.props.pagejson.resourcename];
		for (var prop in resource['fields']) {
			for (var prop1 in this.props.resource) {
				if (prop == prop1) {
					if (!resource.fields[prop].restrictToCopy) {
						tempObject[prop] = this.props.resource[prop1];

						if (resource['fields'][prop]['foreignKeyOptions']) {
							if (resource.fields[prop]['foreignKeyOptions']['mainField']) {
								let tempmainfield = resource.fields[prop]['foreignKeyOptions']['mainField'].split('_')[1];
								tempmainfield = tempmainfield.split('.').join('_');
								tempObject[tempmainfield] = this.props.resource[tempmainfield];
							}
							if (resource.fields[prop]['foreignKeyOptions']['additionalField']) {
								for (var l = 0; l < resource.fields[prop]['foreignKeyOptions']['additionalField'].length; l++) {
									let tempadditionalfield = resource.fields[prop]['foreignKeyOptions']['additionalField'][l].split('_')[1];
									tempadditionalfield = tempadditionalfield.split('.').join('_');
									tempObject[tempadditionalfield] = this.props.resource[tempadditionalfield];
								}
							}

						}
					}
				}
			}
		}

		for (var i = 0; i < resource.childResource.length; i++) {
			if (resource.childResource[i].isUpdate) {
				let childresource = this.props.app.myResources[resource.childResource[i].name];
				let itemarray = this.props.resource[resource.childResource[i].name];
				tempObject[resource.childResource[i].name] = [];
				for (var j = 0; j < itemarray.length; j++) {
					let temp = {};
					for (prop in childresource['fields']) {
						for (prop1 in itemarray[j]) {
							if (prop == prop1) {
								if (!childresource.fields[prop].restrictToCopy) {
									temp[prop] = itemarray[j][prop1];
									if (childresource['fields'][prop]['foreignKeyOptions']) {
										if (childresource.fields[prop]['foreignKeyOptions']['mainField']) {
											let tempmainfield = childresource.fields[prop]['foreignKeyOptions']['mainField'].split('_')[1];
											tempmainfield = tempmainfield.split('.').join('_');
											temp[tempmainfield] = itemarray[j][tempmainfield];
										}
										if (childresource.fields[prop]['foreignKeyOptions']['additionalField']) {
											for (var l = 0; l < childresource.fields[prop]['foreignKeyOptions']['additionalField'].length; l++) {
												let tempadditionalfield = childresource.fields[prop]['foreignKeyOptions']['additionalField'][l].split('_')[1];
												tempadditionalfield = tempadditionalfield.split('.').join('_');
												temp[tempadditionalfield] = itemarray[j][tempadditionalfield];
											}
										}

									}
								}
							}
						}
					}
					tempObject[resource.childResource[i].name][j] = temp;
				}
			}
		}
		tempObject.param = 'copy';
		this.props.history.push({pathname: this.props.pagejson.redirectpage, params: tempObject});
	}

	doctraceFunction() {
		let removeResources = ['creditnotes', 'debitnotes', 'paymentvouchers', 'receiptvouchers', 'journals', 'journalvouchers'];

		this.props.openModal({
			render: (closeModal) => {
				return <DocTraceModal app={this.props.app} parentresource={removeResources.includes(this.props.pagejson.resourcename) ? this.props.pagejson.pagename : this.props.pagejson.resourcename} parentid={this.props.resource.id} history={this.props.history} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	openNavLink(param) {
		this.props.updateLoaderFlag(true);
		let url = this.props.viewtype.listurl.replace(encodeURIComponent('<navurlfilter>'), (param=='previous' ? 'greaterThan' : 'lessThan')).replace(encodeURIComponent(`"<idvalue>"`), this.props.resource.id);

		if(param == 'previous')
			url += `&reversalOrder=true`;

		axios.get(url).then((res) => {
			if(res.data.message == 'success') {
				if(res.data.main.length > 0) {
					this.props.history.replace(`${this.props.viewtype.redirecturl.replace('<id>', res.data.main[0].id)}?viewtype=book&redirecturl=${this.props.viewtype.redirecturl}&listurl=${encodeURIComponent(this.props.viewtype.listurl)}`);
				} else {
					this.props.updateLoaderFlag(false);
					this.props.openModal(modalService["infoMethod"]({
						header : "Error",
						body : `No more ${param} transaction`,
						btnArray : ["Ok"],
						isToast: true
					}));
				}
			}
		});
	}


	playRecording() {
		this.props.openModal({
			render: (closeModal) => {
				return <RecordingplayModal
						callid = {this.props.resource.callid}
						closeModal = {closeModal} 
						openModal = {this.props.openModal}/>
			},
			className: {
				content: 'react-modal-custom-class-30',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}
	
	amendcompare() {
		this.props.openModal({
			render: (closeModal) => {
				return <AmendCompareModel pagename={this.props.pagejson.pagename} resourcename={this.props.pagejson.resourcename} resource={this.props.resource} pagejson={this.props.pagejson} app={this.props.app} openModal={this.props.openModal} closeModal={closeModal} checkCondition={this.props.checkCondition} />
			}, className: {content: 'react-modal-custom-class-90', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	render() {
		let transactionnotitle = '', statustitle = '';

		if(this.props.resource && this.props.resource.id) {
			transactionnotitle = this.props.resource[this.state.titleField];
			statustitle = this.props.resource.status;
		}

		if(this.props.isModal)
			return (
				<div className="react-modal-header">
					<div className="d-flex flex-row">
						<div className="marginright-10">
							<h5>{this.props.pagejson.pagename} {transactionnotitle ? <span> : {transactionnotitle}</span> : null } {statustitle ? <span className="badge gs-badge-success marginleft-10 font-12"> {statustitle}</span> : null }</h5>
						</div>
						{
							(this.props.pagejson.showrecording &&  this.props.resource.id && this.props.app.feature.enableDialerIntegration && this.props.resource.callid) ? 
								<div className="btn btn-sm affixmenu-btn" onClick={this.playRecording}><span className="fa fa-headphones"></span></div> 
							: null 
						}
					</div>
				</div>
			);

		return (
			<div className="row">
				<div className={`col-sm-12 affixmenubar col-md-12 col-lg-${this.props.isModal ? '12' : '9'} d-sm-block d-md-flex justify-content-md-between  align-items-center`}>
						<div className="semi-bold-custom font-16">
								{this.props.pagejson.backpage ? <a className="affixanchor float-left marginright-10" onClick={this.gotoListPage}><span className="fa fa-arrow-left "></span></a> : null}  <div className="gs-uppercase float-left">{this.props.pagejson.pagename} : </div> {transactionnotitle ? <div className="float-left marginleft-10" style={{maxWidth: '300px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis'}} title={transactionnotitle}>{transactionnotitle}</div> : null } {statustitle ? <label className="badge gs-badge-success marginleft-10  mb-0">{statustitle}</label> : null }{this.props.resource.preclosed ? <label className="badge gs-badge-danger marginleft-10">Pre Closed</label> : null }{this.props.pagejson.resourcename=='contacts' && this.props.resource.stage ? <label className={`badge gs-badge-${this.props.resource.stage == 'Junk' || this.props.resource.stage == 'Not Interested' ? 'danger' : 'success'} marginleft-10 mb-0`}>{this.props.resource.stage}</label>: null}
								{this.props.viewtype ? <a className="gs-form-btn-success affixnextprevious marginright-10 marginleft-10" onClick={() => this.openNavLink('previous')} rel="tooltip" title="Go Previous" style={{paddingTop:'2px', paddingBottom:'2px', paddingLeft:'10px', paddingRight:'10px'}}><span className="affixnextpreviousicon fa fa-caret-left"></span></a> : null}
								{this.props.viewtype ? <a className="gs-form-btn-success affixnextprevious marginright-10 " onClick={() => this.openNavLink('next')} rel="tooltip" title="Go Next" style={{paddingTop:'2px', paddingBottom:'2px', paddingLeft:'10px', paddingRight:'10px'}}><span className="affixnextpreviousicon fa fa-caret-right"></span></a> : null }
						</div>
						<div className="d-block">
							{this.props.pagejson.showrecording &&  this.props.resource.id && this.props.app.feature.enableDialerIntegration && this.props.resource.callid ? <div className="btn btn-sm affixmenu-btn" onClick={this.playRecording}><span className="fa fa-headphones"></span></div> : null }
							{this.props.pagejson.showcopy &&  this.props.resource.id && this.props.resource.status != 'Version History' ? <div className="btn btn-sm affixmenu-btn" onClick={this.copyfunction}><i className="fa fa-files-o"></i>Copy</div> : null }
							{this.props.pagejson.showprint &&  this.props.resource.id ? <div className="btn btn-sm affixmenu-btn" onClick={()=>this.printfunction()}><i className="fa fa-print"></i>Print</div> : null }
							{this.props.pagejson.showprint && this.props.app.feature.useRawPrinting &&  this.props.resource.id ? <div className="btn btn-sm affixmenu-btn" onClick={()=>this.printfunction('Raw Print')} title="Print Label"><i className="fa fa-barcode"></i>Label</div> : null }
							{(this.props.pagejson.showamend && ['Approved','Sent To Customer','Sent To Supplier','Won','Lost'].indexOf(this.props.resource.status) > -1) ? <div className="btn btn-sm affixmenu-btn" onClick={this.amendfunction}><i className="fa fa-pencil-square-o"></i>Amend</div> : null }
							{this.props.pagejson.showamend && this.props.resource.id && this.props.resource.amendinprogress ? <div className="btn btn-sm affixmenu-btn" onClick={() => this.amendcompare()}><i className="fa fa-clone"></i>Compare Versions</div> : null }
							{this.props.pagejson.showdoctrace && this.props.resource.id && this.props.resource.status != 'Version History' ? <div className="btn btn-sm affixmenu-btn" onClick={this.doctraceFunction}><i className="fa fa-share-alt"></i>Trace</div> : null }
						</div>
				</div>
			</div>
		);
	}
}

class AmendModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			versionarray: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.openitem = this.openitem.bind(this);
		this.create = this.create.bind(this);
		this.showamendbtn = this.showamendbtn.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad () {
		let { versionarray } = this.state;
		this.setState({loaderFlag : true});
		axios.get(`/api/common/methods/getAmendments?resourcename=${this.props.resourcename}&id=${this.props.resource.id}`).then((response) => {
			if (response.data.message == 'success') {
				versionarray = [];
				for(var prop in response.data.main) {
					versionarray.push(response.data.main[prop]);
				}
				this.setState({versionarray});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.setState({loaderFlag : false});
		});
	}

	openitem (item) {
		this.setState({loaderFlag : true});
		axios.get(`/api/common/methods/getAmendments?resourcename=${this.props.resourcename}&id=${this.props.resource.id}&versionid=${item.id}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.openselectedVersion(response.data.main, item.versionno);
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.setState({loaderFlag : false});
			this.props.closeModal();
		});
	}

	create () {
		this.props.closeModal();
		this.props.callback();
	}

	showamendbtn() {
		if(['Approved','Sent To Customer','Sent To Supplier'].indexOf(this.props.resource.status) > -1) {
			return true;
		}
		return false;
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">{this.props.pagename}  Amendment History</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							{this.state.loaderFlag ? <p>Amendment History is Loading....</p> : null }


							{!this.state.loaderFlag && this.state.versionarray && this.state.versionarray.length ? <div className="table-responsive">
								<table className="table table-bordered table-hover table-condensed text-center">
									<thead>
										<tr>
											<th>Version No</th>
											<th>Date</th>
											<th>Remarks</th>
											<th>Author</th>
										</tr>
									</thead>
									<tbody>
										{this.state.versionarray.map((item, index) => {
											return (
												<tr key={index} onClick={()=>this.openitem(item)}>
													<td>{item.versionno}</td>
													<td>{filter.datetimeFilter(item.versiondate)}</td>
													<td>{item.amendremarks}</td>
													<td>{item.authorname}</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div> : null }
							{(this.state.versionarray && this.state.versionarray.length == 0) ? <p>No amendments found for this {this.props.pagename}</p> : null }
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm gs-btn-warning btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Cancel</button>
								{this.showamendbtn() ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.create}><i className="fa fa-edit"></i>Create Amendment</button> : null }
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}


class CopyModal extends Component {
	constructor(props) {
		super(props);
		this.copyrecord = this.copyrecord.bind(this);
	}

	copyrecord() {
		this.props.callback();
		this.props.closeModal();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Copy Record</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							<p>Do you want to copy this record?</p>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm gs-btn-warning btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Cancel</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.copyrecord}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class Paymentreceiptvouchersummarydetails extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let tempObj = {
			creditSum: 0,
			debitSum: 0,
			balanceSum: 0,
			balanceType: 'Cr',
			totaldeductionamount: 0,
			totalinvoiceamount: 0,
			totalaccountamount: 0,
			totalexpenseamount: 0
		};
		let resource = this.props.resource;
		let {currencyFilter} = filter;

		for (var i = 0; i < resource.invoiceitems.length; i++) {
			let tempamount = (resource.invoiceitems[i][resource.vouchertype == 'Receipt' ? 'credit' : 'debit'] || 0);
			tempObj[resource.vouchertype == 'Receipt' ? 'creditSum' : 'debitSum'] += tempamount;
			tempObj.totalinvoiceamount += tempamount;
		}

		for (var i = 0; i < resource.otheraccounts.length; i++) {
			let tempamount = (resource.otheraccounts[i][resource.vouchertype == 'Receipt' ? 'credit' : 'debit'] || 0);
			tempObj[resource.vouchertype == 'Receipt' ? 'creditSum' : 'debitSum'] += tempamount;
			tempObj.totalaccountamount += tempamount;
		}

		for (var i = 0; i < resource.deductions.length; i++) {
			let tempamount = (resource.deductions[i][resource.vouchertype == 'Receipt' ? 'debit' : 'credit'] || 0);
			tempObj[resource.vouchertype == 'Receipt' ? 'debitSum' : 'creditSum'] += tempamount;
			tempObj.totaldeductionamount += tempamount;
		}

		if(resource.vouchertype == 'Payment') {
			for (var i = 0; i < resource.employeeoutstandings.length; i++) {
				let tempamount = (resource.employeeoutstandings[i][resource.vouchertype == 'Receipt' ? 'credit' : 'debit'] || 0);
				tempObj[resource.vouchertype == 'Receipt' ? 'creditSum' : 'debitSum'] += tempamount;
				tempObj.totalexpenseamount += tempamount;
			}
		}

		if(resource.vouchertype == 'Receipt') {
			for (var i = 0; i < resource.employeeadvances.length; i++) {
				let tempamount = (resource.employeeadvances[i]['credit'] || 0);
				tempObj['creditSum'] += tempamount;
				tempObj.totalexpenseamount += tempamount;
			}
		}

		tempObj[resource.vouchertype == 'Receipt' ? 'debitSum' : 'creditSum'] += resource.amount || 0;
		tempObj[resource.vouchertype == 'Receipt' ? 'creditSum' : 'debitSum'] += resource.onaccountamount || 0;
		
		tempObj.creditSum = Number(tempObj.creditSum.toFixed(this.props.app.roundOffPrecision));
		tempObj.debitSum = Number(tempObj.debitSum.toFixed(this.props.app.roundOffPrecision));

		if (tempObj.creditSum > tempObj.debitSum) {
			tempObj.balanceSum = Number((tempObj.creditSum - tempObj.debitSum).toFixed(this.props.app.roundOffPrecision));
			tempObj.balanceType = 'Dr';
		} else {
			tempObj.balanceSum = Number((tempObj.debitSum - tempObj.creditSum).toFixed(this.props.app.roundOffPrecision));
			tempObj.balanceType = 'Cr';
		}
		for(var prop in tempObj) {
			if(tempObj[prop] !== null && typeof(tempObj[prop]) == 'number' && tempObj[prop] > 0)
				tempObj[prop] = Number(tempObj[prop].toFixed(this.props.app.roundOffPrecision));
		}
		return (
			<div className="table-responsive">
				<table className="table table-bordered">
					<thead>
						<tr>
							<th className="text-center">Particulars</th>
							<th className="text-center">Debits</th>
							<th className="text-center">Credits</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td className="text-center">{resource.vouchertype == 'Payment' ? 'Paid Amount' : 'Received Amount'}</td>
							<td className="text-right">{currencyFilter((resource.amount || 0), resource.currencyid, this.props.app)}</td>
							<td className="text-right"></td>
						</tr>
						<tr>
							<td className="text-center">Deductions</td>
							<td className="text-right">{currencyFilter((tempObj.totaldeductionamount || 0), resource.currencyid, this.props.app)}</td>
							<td className="text-right"></td>
						</tr>
						<tr>
							<td className="text-center">Invoice Allocations</td>
							<td className="text-right"></td>
							<td className="text-right">{currencyFilter((tempObj.totalinvoiceamount || 0), resource.currencyid, this.props.app)}</td>
						</tr>
						{resource.vouchertype == 'Payment' ? <tr>
							<td className="text-center">Employee Outstanding Allocations</td>
							<td className="text-right"></td>
							<td className="text-right">{currencyFilter((tempObj.totalexpenseamount || 0), resource.currencyid, this.props.app)}</td>
						</tr> : null}
						{resource.vouchertype == 'Receipt' ? <tr>
							<td className="text-center">Employee Advance Allocations</td>
							<td className="text-right"></td>
							<td className="text-right">{currencyFilter((tempObj.totalexpenseamount || 0), resource.currencyid, this.props.app)}</td>
						</tr> : null}
						<tr>
							<td className="text-center">Account Allocations</td>
							<td className="text-right"></td>
							<td className="text-right">{currencyFilter((tempObj.totalaccountamount || 0), resource.currencyid, this.props.app)}</td>
						</tr>
						<tr>
							<td className="text-center">On Account</td>
							<td className="text-right"></td>
							<td className="text-right">{currencyFilter((resource.onaccountamount || 0), resource.currencyid, this.props.app)}</td>
						</tr>
						<tr style={{background : `${tempObj.balanceSum > 0 ? '#d68c8c' : ''}`}}>
							<td className="text-center">Difference</td>
							<td className="text-right">{currencyFilter((tempObj.balanceType == "Dr" ? tempObj.balanceSum : null), resource.currencyid, this.props.app)}</td>
							<td className="text-right">{currencyFilter((tempObj.balanceType == "Cr" ? tempObj.balanceSum : null), resource.currencyid, this.props.app)}</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}

class UserCompaniesDetails extends Component {
	constructor(props) {
		super(props);
	};

	render() {

		return (
			<div className="row col-md-12 col-sm-12 col-xs-12">
				{ this.props.resource.companiesArray.map((company, index) => {
					return (
						<div className="col-md-4 col-sm-6" key={index}>
							<label style={{marginTop: '2px',marginBottom: '2px',display:'inline-block'}}>
								<Field  style={{marginRight: '5px'}} name={`companiesArray[${index}].checked`} component={checkboxEle} />{company.name}
							</label>
						</div>
					)
				})}
			</div>
		);
	}
}

class Activitytypebuttonicon extends Component {
	constructor(props) {
		super(props);
		this.updateIcon = this.updateIcon.bind(this);
	};

	updateIcon(param) {
		this.props.updateFormState(this.props.form, {
			icontext: param
		});
	}

	render() {
		return (
			<div className="form-group col-md-6 col-sm-12">
				<div className="btn-group">
					<button type="button" className={`btn btn-sm btn-secondary ${this.props.resource.icontext == 'phone-square' ? 'active' : ''}`} onClick={() => this.updateIcon('phone-square')}><span className="fa fa-phone-square"></span></button>
					<button type="button" className={`btn btn-sm btn-secondary ${this.props.resource.icontext == 'users' ? 'active' : ''}`} onClick={() => this.updateIcon('users')}><span className="fa fa-users"></span></button>
					<button type="button" className={`btn btn-sm btn-secondary ${this.props.resource.icontext == 'briefcase' ? 'active' : ''}`} onClick={() => this.updateIcon('briefcase')}><span className="fa fa-briefcase"></span></button>
					<button type="button" className={`btn btn-sm btn-secondary ${this.props.resource.icontext == 'taxi' ? 'active' : ''}`} onClick={() => this.updateIcon('taxi')}><span className="fa fa-taxi"></span></button>
					<button type="button" className={`btn btn-sm btn-secondary ${this.props.resource.icontext == 'bell' ? 'active' : ''}`} onClick={() => this.updateIcon('bell')}><span className="fa fa-bell"></span></button>
					<button type="button" className={`btn btn-sm btn-secondary ${this.props.resource.icontext == 'bullseye' ? 'active' : ''}`} onClick={() => this.updateIcon('bullseye')}><span className="fa fa-bullseye"></span></button>
					<button type="button" className={`btn btn-sm btn-secondary ${this.props.resource.icontext == 'coffee' ? 'active' : ''}`} onClick={() => this.updateIcon('coffee')}><span className="fa fa-coffee"></span></button>
				</div>
			</div>
		);
	}
}

class CostenterTableElement extends Component {
	constructor(props) {
		super(props);
		this.state = {
			editObj: {}
		};

		this.addNewRow = this.addNewRow.bind(this);
		this.editRow = this.editRow.bind(this);
		this.costcenterOnLoad = this.costcenterOnLoad.bind(this);
		this.saveCostcenter = this.saveCostcenter.bind(this);
	}

	addNewRow() {
		let { editObj } = this.state;
		this.props.fields.push({
			categoryid: this.props.resource.id,
			isactive: true
		});
		this.props.fields.map((a, key)=> {
			editObj[key] = false;
		});
		editObj[this.props.fields.length] = true;
		this.setState({editObj: editObj});
	}

	editRow(index) {
		let { editObj } = this.state;
		this.props.fields.map((a, key)=> {
			editObj[key] = (key == index) ? true : false;
		});
		this.setState({ editObj });
	}

	costcenterOnLoad() {
		let { editObj } = this.state;
		if(Object.keys(editObj).length > 0) {
			for(var key in editObj) {
				if(editObj[key] == true)
					return '';
			}
			return 'table-responsive';
		}
		return 'table-responsive';
	}

	saveCostcenter(index, param) {
		this.props.updateLoaderFlag(true);
		let { costcenters } = this.props.resource;
		let { editObj } = this.state;
		let costcenterObj = costcenters[index];

		if(param == 'Delete' && !costcenterObj.id) {
			this.props.fields.remove(index);
			this.props.updateLoaderFlag(false);
			return true;
		}

		axios({
			method : 'post',
			data : {
				actionverb : param,
				data : costcenterObj
			},
			url : '/api/costcenters'
		}).then((response) => {
			if(response.data.message != "success") {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			} else {
				if(param == 'Delete')
					this.props.fields.remove(index);
				else {
					let costcenter = [...this.props.resource.costcenters];
					costcenters[index] = response.data.main;
					this.props.updateFormState(this.props.form, {
						costcenter: [...costcenter]
					});
				}
				this.props.fields.forEach((a, key)=> {
					editObj[key] = false;
				});
				this.setState({ editObj });
			}

			this.props.updateLoaderFlag(false);
		});
	}

	render() {
		return (
			<div className={this.costcenterOnLoad()}>
				<table className="table gs-table gs-item-table-bordered">
					<thead>
						<tr>
							<th className="text-center" style={{width: '5%'}}>S.No</th>
							<th className="text-center" style={{width:'70%'}}>Name</th>
							<th className="text-center" style={{width:'20%'}}>Active</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						 {this.props.fields.map((member, index) => {
							let costcenterObj = this.props.checkCondition('value', `{resource.${member}}`);
							return (
								<tr key={index}>
									<td>{index+1}</td>
									<td>
										{this.state.editObj[index] ? <div>
												<div style={{width: '50%', float: 'left'}}>
													<Field name={`${member}.name`} props={{
													required : true
												}} component={InputEle}  validate={[stringNewValidation({required: true})]}/>
											</div><div style={{width: '10%', float: 'left', textAlign: 'center'}}><button type="button" className="btn btn-sm gs-btn-transparent text-primary" onClick={()=>this.saveCostcenter(index, 'Save')} disabled={!costcenterObj.name}>Save</button></div></div> : <div>
												<div className="float-left">{costcenterObj.name}</div>
												<div className="float-left anchorclass ml-3" onClick={()=>this.editRow(index)}><i className="fa fa-pencil"></i></div>
											</div>
										}
									</td>
									<td className="text-center">
										<Field name={`${member}.isactive`} props={{disabled: !costcenterObj.id, onChange: ()=>this.saveCostcenter(index, 'Save')}} component={checkboxEle} />
									</td>
									<td>
										<button type='button' className='btn gs-form-btn-danger btn-sm btndisable' onClick={() => this.saveCostcenter(index, 'Delete')}><span className='fa fa-trash-o'></span></button>
									</td>
								</tr>
							)
						 })}
					</tbody>
				</table>
				<button type="button" className="btn btn-sm gs-form-btn-primary btn-width btnhide" onClick={this.addNewRow}><i className="fa fa-plus"></i>Add</button>
			</div>
		);
	}
}

DetailForm = connect(
	(state, props) => {
	  let formName = getFormName(state, props);
	  return {
		  app : state.app,
		  pagejson : state.pagejson,
		  form : formName,
		  touchOnChange: true,
		  //destroyOnUnmount: false, //this will useful for maintain a state
		  resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : {}) : null,
		  formData : state.form,
		  fullstate : state
	  }
	}, {updatePageJSONState, updateFormState, updateAppState}
)(reduxForm()(DetailForm));

export default DetailForm;