import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { Link } from 'react-router-dom';
import { v1 as uuidv1 } from 'uuid';
import moment from 'moment';

import { updateFormState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { InputEle, NumberEle, SpanEle, DateEle, textareaEle, selectAsyncEle, localSelectEle, autoSelectEle, richtextEle, qtyEle, emailEle, checkboxEle, ContactEle, DisplayGroupEle, RateInline, StockInline, AddressEle, autosuggestEle, ButtongroupEle, RateField, DiscountField, TimepickerEle, alertinfoEle, PhoneElement, passwordEle, DocumentLinkEle, ExpenseRequestSummary, autoMultiSelectEle } from '../components/formelements';
import {stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation } from '../utils/utils';

class Settings extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : true,
			modules : [{name : 'Sales',isselected : true},{name : 'Project',isselected : false},{name : 'Purchase',isselected : false},{name : 'Stock',isselected : false},{name : 'Accounts',isselected : false},{name : 'Service',isselected : false},{name : 'HR',isselected : false},{name : 'Common',isselected : false},{name : 'Security',isselected : false},{name : 'ServSmart',isselected : false}],
			deliverypolicy : '',
			activeclass: 'Sales',
			checkinoutsidegeofence: 'Stop',
			checkoutoutsidegeofence: 'Stop'
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.save = this.save.bind(this);
		this.initialize = this.initialize.bind(this);
		this.resetGUID = this.resetGUID.bind(this);
		this.validateDisplayAddressFormat = this.validateDisplayAddressFormat.bind(this);
		this.getRoles = this.getRoles.bind(this);
		this.onDeliveryPolicyChange = this.onDeliveryPolicyChange.bind(this);
		this.getCompanies = this.getCompanies.bind(this);
		this.onServSmartCompanyChange = this.onServSmartCompanyChange.bind(this);
		this.onCheckinOutstidegeofenceChange = this.onCheckinOutstidegeofenceChange.bind(this);
		this.onCheckoutOutstidegeofenceChange = this.onCheckoutOutstidegeofenceChange.bind(this);
	}

	modal(name){
		let callback = (param) => {
				this.props.updateFormState(this.props.form, {
					[name]: param
				});
		};
		let message = {
			header : "You can't disable later!",
			body : "If you enable this feature, you will NOT be able to disable it. This is an irreversible decision. Please reach out to support if you are not sure. Are you sure you want to continue?",
			btnArray : ["Yes, Enable", "Cancel"]
		};
		this.props.openModal(modalService['confirmMethod'](message,callback));
	}

	onSelect(item) {
		let modules = [...this.state.modules];
		this.setState({ activeclass: item.name });
		modules.map((i)=>{
			if(i.name == item.name)
				i.isselected = true;
			else
				i.isselected = false;
		})
	}

	onDeliveryPolicyChange(name) {
		this.setState({deliverypolicy: name}, ()=> {
			this.props.updateFormState(this.props.form, {
				[`sales.deliverypolicy`]: name
			})
		})
	}

	onCheckinOutstidegeofenceChange(name) {
		this.setState({checkinoutsidegeofence: name}, ()=> {
			this.props.updateFormState(this.props.form, {
				[`hr.enableAttendanceGeofencing.checkinOutsideGeofence`]: name
			})
		});
	}

	onCheckoutOutstidegeofenceChange(name) {
		this.setState({checkoutoutsidegeofence: name}, ()=> {
			this.props.updateFormState(this.props.form, {
				[`hr.enableAttendanceGeofencing.checkoutOutsideGeofence`]: name
			})
		});
	}

	onServSmartCompanyChange(){
		this.props.updateFormState(this.props.form, {
			[`service.appServiceCallNumberingSeries`]: null
		});
	}

	componentWillMount() {
		this.getRoles();
		this.getCompanies();
	}
	
	getRoles() {
		axios.get(`/api/roles?field=id,name`).then((response) => {
			if(response.data.message == 'success') {
				this.setState({rolesArray: response.data.main});
				this.initialize();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getCompanies() {
		axios.get(`/api/companymaster?field=id,legalname`).then((response) => {
			if(response.data.message == 'success') {
				this.setState({companyArray: response.data.main});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	initialize() {
		let tempObj = {
			sales: {},
			purchase: {},
			account: {},
			hr: {},
			security: {
				iprestrict: {},
				workhourrestrict: {}
			},
			common: {},
			stock: {},
			service: {},
			project: {}
		};
		axios.get(`/api/settings?field=id,module,name,value`).then((response) => {
			if(response.data.message == 'success') {
				response.data.main.map((item) => {
					if(item.module == 'Sales') {
						if(item.name == 'Pricelistid')
							tempObj.sales.pricelistid = item.value.value;
						if(item.name == 'Taxid')
							tempObj.sales.taxid = item.value.value;
						if(item.name == 'Default Delivery Policy')
							tempObj.sales.deliverypolicy = item.value.value;
						if(item.name == 'Quote Profitability Based On')
							tempObj.sales.quoteprofitabilitybasedon = item.value.value;
					}
					if(item.module == 'Purchase') {
						if(item.name == 'Taxid')
							tempObj.purchase.taxid = item.value.value;
						if(item.name == 'useSupplierQuotations')
							tempObj.purchase.useSupplierQuotations = item.value.value;
					}
					if(item.module == 'Accounts') {
						if(item.name == 'Income Account')
							tempObj.account.incomeaccount = item.value.value;
						if(item.name == 'Expense Account')
							tempObj.account.expenseaccount = item.value.value;
						if(item.name == 'Receivable Account')
							tempObj.account.receivableaccount = item.value.value;
						if(item.name == 'Payable Account')
							tempObj.account.payableaccount = item.value.value;
						if(item.name == 'Default Expense Round Off Account')
							tempObj.account.roundoffaccount = item.value.value;
						if(item.name == 'Goods Received But Not Invoiced')
							tempObj.account.goodsreceived = item.value.value;
						if(item.name == 'Rounding Off Precision')
							tempObj.account.roundoffprecision = item.value.value;
						if(item.name == 'usePerpetualInventory')
							tempObj.account.usePerpetualInventory = item.value.value;
						if(item.name == 'Work In Progress Account')
							tempObj.account.workinprogressaccount = item.value.value;
						if(item.name == 'Stock In Transit')
							tempObj.account.stockintransitaccount = item.value.value;
						if(item.name == 'Employee Receivable Account')
							tempObj.account.empreceivableaccount = item.value.value;
						if(item.name == 'Employee Payable Account')
							tempObj.account.emppayableaccount = item.value.value;
						if(item.name == 'Job Work In Progress Account')
							tempObj.account.jobworkinprogressaccount = item.value.value;
						if(item.name == 'useMultiCurrency')
							tempObj.account.useMultiCurrency = item.value.value;
						if(item.name == 'Currency Loss Account')
							tempObj.account.currencylossaccount = item.value.value;
						if(item.name == 'Currency Gain Account')
							tempObj.account.currencygainaccount = item.value.value;
						if(item.name == 'Currency')
							tempObj.account.currency = item.value.value;
					}

					if(item.module == 'HR') {
						if(item.name == 'Attendance API Key')
							tempObj.hr.attendanceapikey = item.value.value;
						if(item.name == 'Timesheet Activity for Sales Activity')
							tempObj.hr.timesheetactivityforsalesactivity = item.value.value;
						if(item.name == 'Timesheet Activity for Service Report')
							tempObj.hr.timesheetactivityforservicereport = item.value.value;
						if(item.name == 'Enable Attendance Geo-fencing')
							tempObj.hr.enableAttendanceGeofencing = item.value;
					}

					if(item.module == 'Stock') {
						if(item.name == 'enableReceiptPickingInDelivery')
							tempObj.stock.enablereceiptpicking = item.value.value;
						if(item.name == 'useAlternativeUOMs')
							tempObj.stock.useAlternativeUOMs = item.value.value;
						if(item.name == 'useItemNameVariations')
							tempObj.stock.useItemNameVariations = item.value.value;
						if(item.name == 'useItemSerialNumbers')
							tempObj.stock.useItemSerialNumbers = item.value.value;
						if(item.name == 'useSubLocations')
							tempObj.stock.useSubLocations = item.value.value;
						if(item.name == 'allowExcessDeliveryItemGroups')
							tempObj.stock.allowExcessDeliveryItemGroups = item.value.value;
					}

					if(item.module == 'Common') {
						if(item.name == 'Email Subscription')
							tempObj.common.emailenabled = item.value.value;
						if(item.name == 'Use Common Email Address')
							tempObj.common.usecommonemailaddress = item.value.value;
						if(item.name == 'Common Email Address') {
							tempObj.common.commonemailaddress = item.value.value;
							tempObj.common.commonemailaddressname = item.value.name;
						}
						if(item.name == 'Enable Email Triggers for')
							tempObj.common.enableEmailTriggersFor = item.value.value;
						if(item.name == 'Lead API Key')
							tempObj.common.commonleadapikey = item.value.value;
						if(item.name == 'Default Round Off Method')
							tempObj.common.defaultroundoffmethod = item.value.value;
						if(item.name == 'Date Format')
							tempObj.common.dateformat = item.value.value;
						if(item.name == 'displayAddressFormat')
							tempObj.common.displayAddressFormat = item.value.value;
						if(item.name == 'Leave Request Days Calculation')
							tempObj.common.leaverequestdayscalculation = item.value.value;
						if(item.name == 'Address Master Required')
							tempObj.common.commonaddressmasterrequired = item.value.value;
						if(item.name == 'enableJobWork')
							tempObj.common.enableJobWork = item.value.value;
						if(item.name == 'dialerAPIKey')
							tempObj.common.commondialerapikey = item.value.value;
						if(item.name == 'useTally')
							tempObj.common.useTally = item.value.value;
						if(item.name == 'Country')
							tempObj.common.country = item.value.value;
						if(item.name == 'Leave Calendar Starting Month')
							tempObj.common.leavecalendarstartingmonth = item.value.value;
						if(item.name == 'Indiamart')
							tempObj.common.enableindiamart = item.value.value ? item.value.value : false;
						if(item.name == 'Activity Overwrite Roles')
							tempObj.common.allowActivityOverwrite = item.value.value;
					}
					
					if (item.module == 'Security') {
						if (item.name == 'IP Restriction') {
							tempObj.security.iprestrict.ipwhitelist = item.value.ipwhitelist;
							tempObj.security.iprestrict.suspendtill = item.value.suspendtill;
						}
						if (item.name == 'Work Time Restriction') {
							tempObj.security.workhourrestrict.officestarttime = item.value.officestarttime ? new Date(item.value.officestarttime) : null;
							tempObj.security.workhourrestrict.officeendtime = item.value.officeendtime ? new Date(item.value.officeendtime) : null;
						}
						if (item.name == 'Login Timeout')
							tempObj.security.logintimeout = item.value.value;
					}

					if(item.module == 'Service') {
						if(item.name == 'enableInHouseService')
							tempObj.service.enableInHouseService = item.value.value;
						if(item.name == 'autoCloseServiceCalls')
							tempObj.service.autoCloseServiceCalls = item.value.value;
						if(item.name == 'Check Contract Entitlements')
							tempObj.service.checkContractEntitlements = item.value.value;
						if(item.name == 'useCheckinAndCheckout')
							tempObj.service.useCheckinAndCheckout = item.value.value;
						if(item.name == 'captureEsignature')
							tempObj.service.captureEsignature = item.value.value;

						if(item.name == 'appServiceReportPrintTemplate')
							tempObj.service.appServiceReportPrintTemplate = item.value.value;
						if(item.name == 'appServiceCallNumberingSeries')
							tempObj.service.appServiceCallNumberingSeries = item.value.value;
						if(item.name == 'appServiceCompany')
							tempObj.service.appServiceCompany = item.value.value;
						if(item.name == 'appOTPMessage')
							tempObj.service.appOTPMessage = item.value.value;
						if(item.name == 'appSupportContactNo')
							tempObj.service.appSupportContactNo = item.value.value;
						if(item.name == 'appSupportContactEmail')
							tempObj.service.appSupportContactEmail = item.value.value;
						if(item.name == 'Use Capacity in Contract Pricing')
							tempObj.service.useCapacityInContractPricing = item.value.value;
						if(item.name == 'Use Facility Management')
							tempObj.service.useFacilityManagement = item.value.value;
					}

					if(item.module == 'Project') {
						if(item.name == 'Project Warehouse Account')
							tempObj.project.projectwarehouseaccountid = item.value.value;
						if(item.name == 'Restrict Estimation Commercials')
							tempObj.project.restrictprojectestimationrole = item.value.value;
						if(item.name == 'useMakeInTransactions')
							tempObj.project.useMakeInTransactions = item.value.value;
						if(item.name == 'itemMasterExcelSyncRoles')
							tempObj.project.itemMasterExcelSyncRoles = item.value.value;
						if(item.name == 'useRateOnlyItems')
							tempObj.project.useRateOnlyItems = item.value.value;
					}
				});
				this.props.initialize(tempObj);
				this.setState({
					deliverypolicy: tempObj.sales.deliverypolicy
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	resetGUID (param) {
		let tempObj = {};
		if(param == 'lead')
			tempObj[`common.commonleadapikey`] = uuidv1();
		else if(param == 'dialer')
			tempObj[`common.commondialerapikey`] = uuidv1();
		else
			tempObj[`hr.attendanceapikey`] = uuidv1();

		this.props.updateFormState(this.props.form, tempObj);
	}

	validateDisplayAddressFormat () {
		let errorFound = true,
			placeHolder = this.props.app.feature.useMasterForAddresses ? ['First Line', 'Second Line', 'Area', 'City', 'State', 'Country', 'Postal Code'] : ['First Line', 'Second Line', 'City', 'State', 'Country', 'Postal Code'],
			addressArray = this.props.resource.common.displayAddressFormat.match(new RegExp('{{(.*?)}}', 'gm'));

		for (let i = 0; i < addressArray.length; i++) {
			let value = addressArray[i].replace(/[{}]/g,'');

			if(placeHolder.indexOf(value) == -1) {
				errorFound = false;
				break;
			}
		}

		return errorFound;
	}

	save (param) {
		this.updateLoaderFlag(true);

		if (this.props.resource.common.displayAddressFormat) {
			let valid = this.validateDisplayAddressFormat(this.props.resource.common.displayAddressFormat),
			message;

			if(!valid) {
				this.updateLoaderFlag(false);

				if (this.props.app.feature.useMasterForAddresses)
					message = {
						header : 'Error',
						body : 'Address format is invalid. Please use only the following tags: {{First Line}},{{Second Line}},{{Area}},{{City}},{{State}},{{Country}},{{Postal Code}}',
						btnArray : ['Ok']
					};
				else
					message = {
						header : 'Error',
						body : 'Address format is invalid. Please use only the following tags: {{First Line}},{{Second Line}},{{City}},{{State}},{{Country}},{{Postal Code}}',
						btnArray : ['Ok']
					};

				this.props.openModal(modalService['infoMethod'](message));
				return false;
			}
		}

		axios({
			method : 'post',
			data : {
				actionverb : 'Save',
				data : this.props.resource
			},
			url : '/api/settings'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

			if(response.data.message == 'success')
				this.initialize();
			else
				this.updateLoaderFlag(false);
		});
	}

	render() {

		let height = $(window).outerHeight() - $('.settings-header').outerHeight() - $('.main-navbar').outerHeight() - 50;

		if(!this.props.resource)
			return null;
	
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
					<div>
						<div className="row">
							<div className="col-md-12 settings-header">
								<div className="settings-header-title">Module Settings</div>
							</div>
						</div>
						<div className="row no-gutters">
							<div className="col-lg-2 col-md-2 col-3 bg-white" style={{height: `${height}px`, fontSize: '16px', border: '1px solid #eee'}}>
								{this.state.modules.map((item, index)=><div key={index} className={`row no-gutters ${(item.name == 'Project' ? (this.props.app.feature.useProjects ? 'd-block' : 'd-none') : '') || (item.name == 'Service' ? (this.props.app.feature.useServiceFlow ? 'd-block' : 'd-none') : '') || (item.name == 'ServSmart' ? (this.props.app.feature.useCustomerServiceApp ? 'd-block' : 'd-none') : '')}`}>
									<div key={index} className={`col-lg-12 col-md-12 col-12 cursor-pointer semi-bold ${item.isselected ? "activemodule" : ""}`} onClick={()=>this.onSelect(item)} style={{padding: '12px 0px 12px 0px'}}>
										<div className="col-md-11 offset-md-1">{item.name}</div>
									</div>
								</div>)}
							</div>
							<form className="col-xl-9 col-lg-9 col-md-9 col-6 marginleft-30" style={{border: '1px solid #eee'}}>
								<div className="row no-gutters">
									<div className="col-md-12 col-lg-12 col-sm-12 col-12 bg-white" style={{height: `${height-55}px`, maxHeight: `${height-55}px`, overflowY: 'auto'}}>
										{this.state.activeclass == 'Sales' ? <div className="row no-gutters">
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Sales Price List</div>
													<div className="col-xl-4 col-lg-4 col-md-5 col-11">
														<Field name={'sales.pricelistid'} props={{
															resource: 'pricelists',
															fields: 'id,name,description',
														}} component={autoSelectEle} validate={[numberNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This will be the default price list in transactions. If the customer or customer group does not have an associated Price List, the default Price List is used.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Delivery Policy</div>
													<div className="col-xl-8 col-lg-8 col-md-7 col-11">
														<div className="setting-radio-input float-left marginright-20">
															<input type="radio" className="marginright-10" id="order" name="After Order" value={this.state.deliverypolicy} onChange={(e)=>this.onDeliveryPolicyChange(e.target.name)} checked={this.props.resource.sales.deliverypolicy == 'After Order'} />
															<label htmlFor="order">After Order</label>
														</div>
														<div className="setting-radio-input float-left marginright-20">
															<input type="radio" className="marginright-10" id="invoice" name="After Invoice" value={this.state.deliverypolicy} onChange={(e)=>this.onDeliveryPolicyChange(e.target.name)} checked={this.props.resource.sales.deliverypolicy == 'After Invoice'}/>
															<label htmlFor="invoice">After Invoice</label>
														</div>
														<div className="setting-radio-input float-left marginright-20">
															<input type="radio" className="marginright-10" id="payment" name="After Payment" value={this.state.deliverypolicy} onChange={(e)=>this.onDeliveryPolicyChange(e.target.name)} checked={this.props.resource.sales.deliverypolicy == 'After Payment'}/>
															<label htmlFor="payment">After Payment</label>
														</div>
														<div className="setting-radio-input float-left marginright-20">
															<input type="radio" className="marginright-10" id="demand" name="On Demand" value={this.state.deliverypolicy} onChange={(e)=>this.onDeliveryPolicyChange(e.target.name)} checked={this.props.resource.sales.deliverypolicy == 'On Demand'}/>
															<label htmlFor="demand">On Demand</label>
														</div>
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This Delivery Policy will be automatically selected when you create a Sales Order. Recommended to keep this on "After Order".
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Webform Lead API Key</div>
													<div className="col-xl-8 col-lg-8 col-md-7 col-11">
														<div className="row">
															<div className="col-xl-6 col-lg-8 col-md-8">
																<textarea className="form-control" value={this.props.resource.common.commonleadapikey} readOnly disabled aria-label="With textarea" style={{top: '0px', height : '40px'}}></textarea>
															</div>
															<div className="col-xl-4 col-lg-4 col-md-8">
																<button type="button" className="btn gs-btn-border-success"  onClick={() => this.resetGUID('lead')}>Generate new key</button>
															</div>
														</div>
													</div>
													<div className="col-xl-12 col-lg-12 col-md-11 col-11 margintop-10 font-11 text-muted">API Key to be used for integrating leads from website contact forms. Generate a new key and use it.
													</div>
												</div>
											</div>
											{/*<div className="col-md-12 settings-list">
												<div className="row">
													<p className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold">Enable Indiamart Integration</p>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.enableindiamart'} props={{}} component={checkboxEle} validate={[stringNewValidation({
																required: true
															})]} />
													</div>
												</div>
												<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This feature enables leads from Indiamart portal to be automatically created in GrowSmart.
												</div>
											</div>*/}
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Profitability Based On</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'sales.quoteprofitabilitybasedon'} props={{
															options: [{id:'averagecost', name: 'Average Cost'},{id:'defaultcost', name: 'Default Cost'}, {id:'averagelandingcost', name: 'Average Landing Cost'}]
														}}
														component={localSelectEle}
														validate={[stringNewValidation({required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This option will be automatically selected when you check profitabilty analysis in Quotes and Sales Order. Recommended to keep this on "Average Cost".
													</div>
												</div>
											</div>
										</div> : null}
										{this.props.app.feature.useProjects ? <div>
											{this.state.activeclass == 'Project' ? <div className="row no-gutters">
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Estimation Commercial Restricted Roles</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'project.restrictprojectestimationrole'} props={{
																value: 'id',
																label: 'name',
																multiselect: true,
																options: this.state.rolesArray
															}} component={localSelectEle} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">If you want certain users to work on estimation, but not view the commercial information, add them to a role and select the role here. These rolls can add items and quantities, but not view or change the cost information.
														</div>
													</div>
												</div>
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Select Make in Transactions</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'project.useMakeInTransactions'} props={{}} component={checkboxEle} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">
														</div>
													</div>
												</div>
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Item Master Excel Sync Roles</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'project.itemMasterExcelSyncRoles'} props={{
																value: 'id',
																label: 'name',
																multiselect: true,
																options: this.state.rolesArray
															}} component={localSelectEle} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">If you would like to enable the option to sync item master in excel templates for Project Estimation and Projects, please select the roles you want to permit. Leave this blank if you do not want to enable this option.
														</div>
													</div>
												</div>
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Rate Only (RO) Items</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'project.useRateOnlyItems'} props={{}} component={checkboxEle} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">If you enable this option, you can mark certain items in a Project Quotation "Rate Only". Quantity is not required to be specified for Rate Only items.
														</div>
													</div>
												</div>
											</div> : null}
										</div> : null}
										{this.state.activeclass == 'Purchase' ? <div className="row no-gutters">
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable RFQ Feature</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'purchase.useSupplierQuotations'} onChange={()=>this.modal('purchase.useSupplierQuotations')} props={{
																disabled: this.props.resource.purchase.useSupplierQuotations
															}} component={checkboxEle} validate={[stringNewValidation({
																required: true
															})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Request for Quotes (RFQ) feature helps in best price discovery from multiple supplier quotations. You can't disabled this feature once enabled.
													</div>
												</div>
											</div>
										</div> : null}
										{this.state.activeclass == 'Stock' ? <div className="row no-gutters">
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Alternative UOMs</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'stock.useAlternativeUOMs'} onChange={()=>this.modal('stock.useAlternativeUOMs')} props={{
																disabled: this.props.resource.stock.useAlternativeUOMs
															}} component={checkboxEle} validate={[stringNewValidation({
																required: true
															})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Helps in managing the same item in multiple units of measure. You can't disabled this feature once enabled.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Serial Number Tracking</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'stock.useItemSerialNumbers'} onChange={()=>this.modal('stock.useItemSerialNumbers')} props={{
																disabled: this.props.resource.stock.useItemSerialNumbers
															}} component={checkboxEle} validate={[stringNewValidation({
																required: true
															})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Serial number tracking helps in traceability. You can enable serial number tracking for required items when this feature is enabled. You can't disabled this feature once enabled.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Item Name Variations</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'stock.useItemNameVariations'} props={{}} component={checkboxEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Define customer or vendor specific names for the same item.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Allow Excess Delivery and Return for</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'stock.allowExcessDeliveryItemGroups'} props={{
																value: 'id',
																label: 'groupname',
																resource: 'itemgroups',
																fields: 'id,groupname'
															}} component={autoMultiSelectEle} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">Sometimes you need to deliver more than the requested quantity for certain items. Example - items that are cut and used at the site. To enable this feature, select item groups that you want to allow for over-delivery.
														</div>
													</div>
												</div>
										</div> : null}
										{this.state.activeclass == 'Accounts' ? <div className="row no-gutters">
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Account Base Currency</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.currency'} props={{
															resource: 'currency',
															fields: 'id,name,description',
															}} readOnly disabled component={autoSelectEle} validate={[numberNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Base currency for the account. For reference only, can't be changed.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Multiple Currencies</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.useMultiCurrency'} onChange={()=>this.modal('account.useMultiCurrency')} props={{
															disabled: this.props.resource.account.useMultiCurrency
														}} component={checkboxEle} validate={[stringNewValidation({
																required: true
															})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Enable this option if you import or export. You can't disabled this feature once enabled.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Tally Integration</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.useTally'} props={{}} component={checkboxEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Integrate financial transactions from GrowSmart to Tally.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Sales Tax</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'sales.taxid'} props={{
															value: 'id',
															label: 'taxcode',
															multiselect: true,
															options: this.props.app.salestaxarray
														}} component={localSelectEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Default tax to be applied for sales transactions. This is used only when there is no other tax setting for the item or item group.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Purchase Tax</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'purchase.taxid'} props={{
															value: 'id',
															label: 'taxcode',
															multiselect: true,
															options: this.props.app.purchasetaxarray
														}} component={localSelectEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Default tax to be applied for purchase transactions. This is used only when there is no other tax setting for the item or item group.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Sales Account</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.incomeaccount'} props={{
															resource: 'accounts',
															fields: 'id,name',
															filter: "accounts.isledger=true and accounts.type='Income'"
														}} component={autoSelectEle} validate={[numberNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">The default account to be used for booking income from sales. This is used when the item or item group does not have a Sales Account selected.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Purchase Account</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.expenseaccount'} props={{
															resource: 'accounts',
															fields: 'id,name',
															filter: "accounts.isledger=true and accounts.type='Expense'"
														}} component={autoSelectEle} validate={[numberNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">The default account to be used for booking expenses of purchases / delivery. This is used when the item or item group does not have a Purchase Account selected.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Receivable Account</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.receivableaccount'} props={{
															resource: 'accounts',
															fields: 'id,name',
															filter: "accounts.isledger=true and accounts.type='Asset' and accounts.accountgroup in ('Receivable')"
														}} component={autoSelectEle} validate={[numberNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">The default account to be used for customer receivables when you create an invoice. Used if the customer or customer group does not have a receivable account configured.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Payable Account</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.payableaccount'} props={{
															resource: 'accounts',
															fields: 'id,name',
															filter: "accounts.isledger=true and accounts.type='Liability' and accounts.accountgroup in ('Payable')"
														}} component={autoSelectEle} validate={[numberNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">The default account to be used for supplier payable when you create a purchase invoice. Used if the supplier does not have a receivable account configured.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Account for Goods Received Not Invoiced</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.goodsreceived'} props={{
															resource: 'accounts',
															fields: 'id,name',
															filter: "accounts.isledger=true and accounts.type='Liability'"
														}} component={autoSelectEle}  validate={[numberNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This is a temporary account, which holds the value of goods received through a Receipt Note, until the Purchase Invoice is created.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Stock In Transit Account</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.stockintransitaccount'} props={{
															resource: 'accounts',
															fields: 'id,name',
															filter: "accounts.isledger=true and accounts.type='Asset' and accounts.accountgroup is null"
														}} component={autoSelectEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This account is used to hold the value of goods after a Delivery Note is created against a stock transfer. Reversal is made when Receipt Note is created at the destination warehouse.
													</div>
												</div>
											</div>
											{this.props.app.feature.useProduction ? <div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Work In Progress Account</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'account.workinprogressaccount'} props={{
																resource: 'accounts',
																fields: 'id,name',
																filter: "accounts.isledger=true and accounts.type='Asset'"
															}} component={autoSelectEle} />
														</div>
														<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This account holds the value of raw materials issued to a production order, until Finished Product receipt note is created and "Confirm Valuation" action is completed.
														</div>
													</div>
											</div>: null}
											{this.props.app.feature.useProjects ? <div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Account for Project Warehouses</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'project.projectwarehouseaccountid'} props={{
																resource: 'accounts',
																fields: 'id,name',
																filter: "accounts.type='Asset' and accounts.accountgroup='Warehouse' and accounts.isledger=true"
															}} component={autoSelectEle} />
														</div>
														<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This account will be used to show the value of goods in project warehouses automatically created by the system.
														</div>
													</div>
											</div> : null}
											{this.props.app.feature.enableJobWork ? <div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Job Work In Progress Account</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'account.jobworkinprogressaccount'} props={{
																resource: 'accounts',
																fields: 'id,name',
																filter: "accounts.isledger=true and accounts.type='Asset' and accounts.accountgroup is null"
															}} component={autoSelectEle} />
														</div>
														<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This account holds the value of raw materials issued to a vendor against a Job Work Orders, until Finished Product receipt note is created and "Confirm Valuation" action is completed.
														</div>
													</div>
											</div> : null}
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Account for Round Off</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.roundoffaccount'} props={{
															resource: 'accounts',
															fields: 'id,name',
															filter: "accounts.isledger=true and accounts.type='Expense'"
														}} component={autoSelectEle} validate={[numberNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This account is used for booking rounding off values in invoices and other transactions.
													</div>
												</div>
											</div>
											{this.props.app.feature.useMultiCurrency ? <div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Account for Currency Gains</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'account.currencygainaccount'} props={{
																resource: 'accounts',
																fields: 'id,name',
																filter: "accounts.isledger=true and accounts.type='Income'"
															}} component={autoSelectEle} validate={[numberNewValidation({
																required: true
															})]} />
														</div>
														<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This account is used when there is a gain on account of currency exchange rate fluctuation between invoicing and payments.
														</div>
													</div>
												</div> : null}
											{this.props.app.feature.useMultiCurrency ? <div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Account for Currency Loss</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'account.currencylossaccount'} props={{
																resource: 'accounts',
																fields: 'id,name',
																filter: "accounts.isledger=true and accounts.type='Expense'"
															}} component={autoSelectEle} validate={[numberNewValidation({
																required: true
															})]} />
														</div>
														<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">This account is used when there is a loss on account of currency exchange rate fluctuation between invoicing and payments.
														</div>
													</div>
												</div> : null}
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Employee Receivable Account</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.empreceivableaccount'} props={{
															resource: 'accounts',
															fields: 'id,name',
															filter: "accounts.isledger=true and accounts.type='Asset' and accounts.accountgroup in ('Employee Receivable')"
														}} component={autoSelectEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">The default account to be used for employee receivables when you create a deduction such as loan repayment.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Employee Payable Account</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'account.emppayableaccount'} props={{
															resource: 'accounts',
															fields: 'id,name',
															filter: "accounts.isledger=true and accounts.type='Liability' and accounts.accountgroup in ('Employee Payable')"
														}} component={autoSelectEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">The default account to be used for employee payable when you create a payroll.
													</div>
												</div>
											</div>
										</div> : null}
										{this.props.app.feature.useServiceFlow ? <div>
											{this.state.activeclass == 'Service' ? <div className="row no-gutters">
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<p className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Close Service Call from Service Report</p>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'service.autoCloseServiceCalls'} props={{}} component={checkboxEle} validate={[stringNewValidation({
																	required: true
																})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">If you enable this option, when a service report is approved with a call status of "Completed", service call will be automatically completed. If this option is not enabled, you have to manually complete the service call.
														</div>
													</div>
												</div>
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Engineer Check-in and Checkout</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'service.useCheckinAndCheckout'} props={{}} component={checkboxEle} validate={[stringNewValidation({required: true})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">When this feature is enabled, engineers can check-in and check-out against service calls from the mobile app. Location and timing are automatically captured.
														</div>
													</div>
												</div>
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Customer Signature Capture</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'service.captureEsignature'} props={{}} component={checkboxEle} validate={[stringNewValidation({required: true})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">When this feature is enabled, engineers can provide a preview of the service report and get customer's signature from their tablet / mobile.
														</div>
													</div>
												</div>
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Inhouse Service</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'service.enableInHouseService'} props={{}} component={checkboxEle} validate={[stringNewValidation({
																	required: true
																})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">This feature helps in tracking service status if you bring customer's equipment to your workstation and provide service.
														</div>
													</div>
												</div>
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Contract Entitlement Checks</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'service.checkContractEntitlements'} props={{}} component={checkboxEle} validate={[stringNewValidation({
																	required: true
																})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">If you have set up covered parts in Contract type master, enable this feature to control free issue of service spares based on entitlement status. Helps in ensuring that billable items are not issued under free of charge.
														</div>
													</div>
												</div>
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Contract Pricing Based On Capacity</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'service.useCapacityInContractPricing'} props={{disabled: this.props.resource.service.useCapacityInContractPricing}} component={checkboxEle} validate={[stringNewValidation({
																	required: true
																})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">If you want to price your service contracts based on the capacity of the equipments (eg. KW, HP, TR), please enable this option. You cannot disable this feature once enabled.
														</div>
													</div>
												</div>
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Use Facility Management</div>
														<div className="col-xl-4 col-lg-5 col-md-6 col-11">
															<Field name={'service.useFacilityManagement'} props={{disabled: this.props.resource.service.useFacilityManagement}} component={checkboxEle} validate={[stringNewValidation({
																	required: true
																})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">Enable this option if you want to create 'Facility' and 'O&M 'Contracts. 'Facility' type Contracts will include different kinds of Equipments and Items installed in one location. For e.g. All equipments and items installed in a branch. 'O&M' type Contracts will include operator details for Operation and Maintenance of a facility.  You cannot disable this feature, once enabled.
														</div>
													</div>
												</div>
											</div> : null}
										</div> : null}
										{this.state.activeclass == 'HR' ? <div className="row no-gutters">
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Leave Calendar Start Month</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.leavecalendarstartingmonth'} props={{}} readOnly disabled component={InputEle}  />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Starting month of your leave year. This month will be used for allocating yearly leaves, and for expiring unused leaves when you run the leave batch.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Biometric API Key</div>
													<div className="col-xl-7 col-lg-7 col-md-6 col-11">
														<div className="row">
															<div className="col-xl-7 col-lg-7 col-md-8">
																<textarea className="form-control" value={this.props.resource.hr.attendanceapikey} readOnly disabled aria-label="With textarea" style={{top: '0px', height : '40px'}}></textarea>
															</div>
															<div className="col-xl-4 col-lg-4 col-md-8">
																<button type="button" className="btn gs-btn-border-success"  onClick={() => this.resetGUID()}>Generate new key</button>
															</div>
														</div>
													</div>
													<div className="col-xl-12 col-lg-12 col-md-11 col-11 margintop-10 font-11 text-muted">API key to be used for configuring biometric device integration for attendance data. Generate a new key and use it.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Sales Activity Timesheet Type</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'hr.timesheetactivityforsalesactivity'} props={{
															resource: 'timesheetactivitytypes',
															fields: 'id,name'
														}} component={autoSelectEle} />
													</div>
													<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">If you use the timesheet module in HR, you can have completed sales activities automatically create a timesheet entry. Choose the timesheet activity type that should be used while creating the automated entries.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Service Report Timesheet Type</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'hr.timesheetactivityforservicereport'} props={{
															resource: 'timesheetactivitytypes',
															fields: 'id,name'
														}} component={autoSelectEle} />
													</div>
													<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">If you use the timesheet module in HR, you can have completed service reports automatically create a timesheet entry. Choose the timesheet activity type that should be used while creating the automated entries.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Weekend Setting</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.leaverequestdayscalculation'} props={{
															resource: 'utilitymodules',
															fields: 'id,name',
															filter: "utilitymodules.type='Leave Requests'"
														}} component={autoSelectEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Choose the required weekend setting. Used for payday calculation in payroll.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Attendance Geo-fencing</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'hr.enableAttendanceGeofencing.value'} props={{}} component={checkboxEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Enable this option if you want to restrict your employees to check-in and check-out only within the marked office boundary.
													</div>
												</div>
											</div>
											{this.props.resource.hr.enableAttendanceGeofencing.value ? <div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Check-in outside geofence</div>
													<div className="col-xl-8 col-lg-8 col-md-7 col-11">
														<div className="setting-radio-input float-left marginright-20">
															<input type="radio" className="marginright-10" id="checkinoutsidegeofence_stop" name="checkinoutsidegeofence" value={this.state.checkinoutsidegeofence} onChange={(e)=>this.onCheckinOutstidegeofenceChange('Stop')} checked={this.props.resource.hr.enableAttendanceGeofencing.checkinOutsideGeofence == 'Stop'} />
															<label htmlFor="checkinoutsidegeofence_stop">Stop</label>
														</div>
														<div className="setting-radio-input float-left marginright-20">
															<input type="radio" className="marginright-10" id="checkinoutsidegeofence_allow" name="checkinoutsidegeofence" value={this.state.checkinoutsidegeofence} onChange={(e)=>this.onCheckinOutstidegeofenceChange('Allow')} checked={this.props.resource.hr.enableAttendanceGeofencing.checkinOutsideGeofence == 'Allow'}/>
															<label htmlFor="checkinoutsidegeofence_allow">Allow</label>
														</div>
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">If you want to stop your employees from checking-in outside the geofence, enable the STOP button, else enable the ALLOW button.
													</div>
												</div>
											</div> : null}
											{this.props.resource.hr.enableAttendanceGeofencing.value ? <div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Check-out outside geofence</div>
													<div className="col-xl-8 col-lg-8 col-md-7 col-11">
														<div className="setting-radio-input float-left marginright-20">
															<input type="radio" className="marginright-10" id="checkoutoutsidegeofence_stop" name="checkoutoutsidegeofence" value={this.state.checkoutoutsidegeofence} onChange={(e)=>this.onCheckoutOutstidegeofenceChange('Stop')} checked={this.props.resource.hr.enableAttendanceGeofencing.checkoutOutsideGeofence == 'Stop'} />
															<label htmlFor="checkoutoutsidegeofence_stop">Stop</label>
														</div>
														<div className="setting-radio-input float-left marginright-20">
															<input type="radio" className="marginright-10" id="checkoutoutsidegeofence_allow" name="checkoutoutsidegeofence" value={this.state.checkoutoutsidegeofence} onChange={(e)=>this.onCheckoutOutstidegeofenceChange('Allow')} checked={this.props.resource.hr.enableAttendanceGeofencing.checkoutOutsideGeofence == 'Allow'}/>
															<label htmlFor="checkoutoutsidegeofence_allow">Allow</label>
														</div>
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">If you want to stop your employees from checking-out outside the geofence, enable the STOP button. Else enable the ALLOW button.
													</div>
												</div>
											</div> : null}
											{this.props.resource.hr.enableAttendanceGeofencing.value ? <div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Geofencing Radius</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'hr.enableAttendanceGeofencing.geofencingRadius'} props={{
															options: [{id: 50, name: '50 Mtr'},{id: 100, name: '100 Mtr'}],
															required: true
														}}
														component={localSelectEle}
														validate={[stringNewValidation({required: true
														})]}/>
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Select the distance from your office within which the employees will be allowed to mark their attendance.
													</div>
												</div>
											</div> : null}
										</div> : null}
										{this.state.activeclass == 'Common' ? <div className="row no-gutters">
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Date Format for Print</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.dateformat'} props={{
															options: ['DD/MM/YYYY', 'DD/MMM/YYYY', 'MM/DD/YYYY', 'MMM/DD/YYYY', 'YYYY/MM/DD', 'YYYY/MMM/DD', 'DD-MM-YYYY', 'DD-MMM-YYYY', 'MM-DD-YYYY', 'MMM-DD-YYYY', 'YYYY-MM-DD', 'YYYY-MMM-DD']
														}} component={localSelectEle} validate={[stringNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Choose your preferred date format. This format will be used in all printed documented (pdf copies) for data based fields.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Address Display Format</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.displayAddressFormat'} props={{}} component={textareaEle} validate={[stringNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">{`Format of the address text. You can move the parts and rearrange them in required number of lines. Allowed parts are {{First Line}}, {{Second Line}}, {{City}}, {{State}}, {{Postal Code}}.`}
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Default Round Off Method</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.defaultroundoffmethod'} props={{
															options: [{id:'none', name: 'None'},{id:'up', name: 'Up'}, {id:'down', name: 'Down'},{id:'even', name: 'Even'}]
														}} component={localSelectEle} validate={[stringNewValidation({
															required: true
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Roundoff method to be automatically selected in transactions. You will still be able to modify it per transaction. If you don't want any rounding, use None.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Account Country</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.country'} props={{}} readOnly disabled component={InputEle}  />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Base country of your account. For reference only.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Allow Unstructured Addresses</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.commonaddressmasterrequired'} props={{}} component={checkboxEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">If you want to freely type addresses without the master or address form, you can enable this option. We strongly recommend not allowing unstructured addresses.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Email</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.emailenabled'} props={{}} component={checkboxEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">Enable sending emails from GrowSmart to customer, suppliers, and employees.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Enable Email Triggers for</div>
													<div className="col-xl-4 col-lg-4 col-md-5 col-11">
														<Field name={'common.enableEmailTriggersFor'} props={{
															value: 'id',
															label: 'name',
															resource: 'emailaccounts',
															fields: 'id,name,email',
															filter: 'emailaccounts.isincoming'
														}} component={autoMultiSelectEle} />
													</div>
													<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">If you would like to enable the email triggers, please select the email accounts you want to trigger. Leave this blank if you do not want to enable this option.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Activity Overwrite Roles</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'common.allowActivityOverwrite'} props={{
																value: 'id',
																label: 'name',
																multiselect: true,
																options: this.state.rolesArray
															}} component={localSelectEle} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">If you want the ability to modify the system captured times in an activity, you can use manual overwrite option. Specify the roles that need to have access to the overwrite option.
													</div>
												</div>
											</div>
										</div> : null}
										{this.state.activeclass == 'Security' ? <div className="row no-gutters">
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Allowed IP Addresses</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'security.iprestrict.ipwhitelist'} props={{}} component={InputEle} />
													</div>
													<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">If you use IP based login restriction, use this field to specify the IP addresses from where employees can login. If you have multiple office locations, you can enter all IP addresses by separating them with comma.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Work Start Time</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'security.workhourrestrict.officestarttime'} props={{}} format={(value) => {return (value == null || value == '') ? null : moment(value)}} parse={(value) => {return (value == null || value == '') ? null : moment(value)._d}} component={TimepickerEle}  validate={[dateNewValidation({
															required: true
														})]}/>
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Work End Time</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'security.workhourrestrict.officeendtime'} format={(value) => {return (value == null || value == '') ? null : moment(value)}} parse={(value) => {return (value == null || value == '') ? null : moment(value)._d}} props={{}} component={TimepickerEle} validate={[dateNewValidation({
															required: '{resource.security.workhourrestrict.officestarttime}'
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">If you use time based login, login will be allowed only between the work start time and end time configured here.
													</div>
												</div>
											</div>
											<div className="col-md-12 settings-list">
												<div className="row no-gutters">
													<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Session Timeout (min)</div>
													<div className="col-xl-4 col-lg-5 col-md-6 col-11">
														<Field name={'security.logintimeout'} props={{}} component={NumberEle} validate={[numberNewValidation({
															min: 1
														})]} />
													</div>
													<div className="col-xl-12 col-lg-12 col-md-12 col-12 margintop-10 font-11 text-muted">After you login, if you do not use GrowSmart for a certain duration, you will be automatically logged out for security reasons. Enter the required duration in minutes here.
													</div>
												</div>
											</div>
										</div> : null}
										{this.props.app.feature.useCustomerServiceApp ? <div>
											{this.state.activeclass == 'ServSmart' ? <div className="row no-gutters">
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Service Company for App</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'service.appServiceCompany'} props={{
																value: 'id',
																label: 'legalname',
																options: this.state.companyArray,
																onChange : ()=>{this.onServSmartCompanyChange()}
															}} component={localSelectEle} validate={[stringNewValidation({
																required: true
															})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">Choose the company to be used in the customer mobile app.
														</div>
													</div>
												</div>
												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Service Call Numbering Series</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'service.appServiceCallNumberingSeries'} props={{
																resource: 'numberingseriesmaster',
																fields: 'id,name',
																filter : `numberingseriesmaster.resource='servicecalls' and numberingseriesmaster.companyid=${this.props.resource.service.appServiceCompany}`
															}} component={selectAsyncEle} validate={[numberNewValidation({
																required: true
															})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">Choose the numbering series to be used when a service call is created by your customers using the app.
														</div>
													</div>
												</div>

												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Service Report Print Template</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'service.appServiceReportPrintTemplate'} props={{
																resource: 'templates',
																fields: 'id,name',
																filter : "templates.resource='servicereports' and templates.templatetype='MS Word'"
															}} component={selectAsyncEle} validate={[numberNewValidation({
																required: true
															})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">Choose the print template to be used when your customers download a Service Report from the app.
														</div>
													</div>
												</div>

												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">OTP Message</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'service.appOTPMessage'} props={{}} component={textareaEle} validate={[stringNewValidation({
																required: true
															})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">SMS template string for OTP message. You may need to ensure that the template is approved by the SMS gateway.
														</div>
													</div>
												</div>

												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Support Phone No</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'service.appSupportContactNo'} props={{}} component={InputEle} validate={[stringNewValidation({
																required: true
															})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">This phone number will be available in the app for your customers to tap and call your Call Center if required.
														</div>
													</div>
												</div>

												<div className="col-md-12 settings-list">
													<div className="row no-gutters">
														<div className="col-xl-4 col-lg-4 col-md-5 col-11 semi-bold font-16">Support Email</div>
														<div className="col-xl-4 col-lg-4 col-md-5 col-11">
															<Field name={'service.appSupportContactEmail'} props={{}} component={InputEle} validate={[stringNewValidation({
																required: true
															})]} />
														</div>
														<div className="col-xl-11 col-lg-11 col-md-11 col-11 margintop-10 font-11 text-muted">This email will be shown in the app for your customers to tap and send an email to your Customer Care if required.
														</div>
													</div>
												</div>
											</div> : null}
										</div> : null}
									</div>
									<div className="col-md-12 col-lg-12 col-sm-12 col-12" >
										<div className="w-100 p-2 float-left bg-white" style={{borderTop: "1px solid #ddd"}}>
											<button type="button" className="btn gs-btn-success1 marginright-20 pull-right" onClick={()=>this.save()} disabled={this.state.loaderflag || this.props.invalid}>Save changes</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
			</div>
		);
	}
}

Settings = connect(
	(state, props) => {
		let formName = 'Settings';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState}
)(reduxForm()(Settings));

export default Settings;