import React, { Component, Fragment } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import Select, { components } from 'react-select';
import AsyncSelect from 'react-select/async';
import Highlighter from 'react-highlight-words';

import { updateAppState } from '../actions/actions';
import { LocalSelect } from '../components/utilcomponents';
import Loadingcontainer from '../components/loadingcontainer';
import { commonMethods, modalService, accessMenuResource } from '../utils/services';

export default connect((state) => {
	return {
		app: state.app
	}
}, { updateAppState }) (class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderflag : false,
			searchterm : '',
			userNav: {},
			navigationmenu: {},
			navigationlink: [],
			linksarray: []
		};

		$("html, body").scrollTop(0);
		this.routeChange = this.routeChange.bind(this);
		this.onSearchChange = this.onSearchChange.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.renderNavMenu = this.renderNavMenu.bind(this);
		this.renderNavLink = this.renderNavLink.bind(this);
		this.renderNavSubMenu = this.renderNavSubMenu.bind(this);
		this.renderSubMenuLink = this.renderSubMenuLink.bind(this);
		this.updateModuleActive = this.updateModuleActive.bind(this);
		this.userUpdate = this.userUpdate.bind(this);
		this.getLinkArray = this.getLinkArray.bind(this);
		this.renderFolderReportNav = this.renderFolderReportNav.bind(this);
	}

	componentWillMount() {
		document.getElementById("pagetitle").innerHTML = 'Home';
		this.getLinkArray();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	updateModuleActive(index) {
		let navigation = this.props.app.accessNavigation;
		navigation.modules.forEach((module) => {
			module.active = null;
		});
		navigation.modules[index].active = 'active';
		this.props.updateAppState('accessNavigation', navigation);
	}

	routeChange(url) {
		this.props.history.push(url);
	}

	onSearchChange(url) {
		this.props.history.push(url);
	}

	userUpdate(link) {
		this.updateLoaderFlag(true);
		this.props.updateBookMark(link, () => {
			this.updateLoaderFlag(false);	
		});
	}

	renderNavMenu() {
		if(!this.props.app.accessNavigation)
			return null;
		let navigation = this.props.app.accessNavigation;

		return navigation.modules.map((module, index) => {
			if(module._access) {
				return (
					<li className="nav-item" key={index}>
						<a className={`nav-link ${module.active}`} onClick={()=>{this.updateModuleActive(index)}}>{module.name.length > 2 ? `${module.name.charAt(0)}${module.name.slice(1).toLowerCase()}` : module.name}</a>
					</li>
				);
			}
		});
	}

	renderNavLink() {
		if(!this.props.app.accessNavigation)
			return null;
		let navigation = this.props.app.accessNavigation;
		let searchterm = '';

		return navigation.modules.map((module, index) => {
			if(module._access) {
				return (
					<div style={{display:`${module && module.active && !searchterm ? 'block' : 'none'}`}} key={index}>
						{this.renderNavSubMenu(module)}
					</div>
				);
			}
		});
	}

	renderNavSubMenu(module) {
		return module.child.map((submodule, index) => {
			return (
				<Fragment key={index}>
					{submodule._access ? <><div style={{paddingTop: '15px', paddingBottom: '15px', color:"#959595"}}>{submodule.name}</div><div className="row">{this.renderSubMenuLink(submodule)}</div></> : null}
				</Fragment>
			);
		});
	}

	renderSubMenuLink(submodule) {
		return submodule.links.map((link, index) => {
			if(!link._access)
				return null;
			return (
				<div className="col-md-3 col-sm-4 form-group" key={index}>
					{link.type == 'folder' ? <div className="gs-home-menu-link">
						<div className="dropdown" style={{padding: '10px', width: '100%'}}>
							<div className="dropdown-toggle" data-toggle="dropdown" id="dropdownMenuOffset">{link.name}</div>
							<div className="dropdown-menu" aria-labelledby="dropdownMenuOffset">
								{this.renderFolderReportNav(link)}
							</div>
						</div>
					</div> : <div key={index} className="gs-home-menu-link" onClick={() => {this.routeChange(link.url)}}>
						<div className="btn-star" onClick={(e)=>{e.stopPropagation();this.userUpdate(link);}}>
							<input type="checkbox" style={{display:'none'}} checked={link.ischecked ? true : false} readOnly />
							<span className={`fa ${link.ischecked ? 'fa-star' : 'fa-star-o'}`}></span>
						</div>
						<div className="menu-link-text" alt={link.name}>{link.name}</div>
					</div>}
				</div>
			);
		});
	}

	renderFolderReportNav(link) {
		return link.reportlinks.map((reportlink, index) => {
			return (
				<div className="gs-home-menu-link-dropdown" key={index} onClick={() => {this.routeChange(reportlink.url)}}>
					<div className="btn-star" onClick={(e)=>{e.stopPropagation();this.userUpdate(reportlink);}}>
						<input type="checkbox" style={{display:'none'}} checked={reportlink.ischecked ? true : false} readOnly />
						<span className={`fa ${reportlink.ischecked ? 'fa-star' : 'fa-star-o'}`}></span>
					</div>
					<div className="menu-link-text" alt={reportlink.name}>{reportlink.name}</div>
				</div>
			);
		})
	}

	getNavigationLink(navigationLink) {
		return navigationLink.map((propvalue, key) =>
			{return <div className="col-md-3 col-sm-4 form-group" key={key}>
				<ul className="list-group menu-list">
					<li className="list-group-item listitem" style={{cursor: 'pointer'}} onClick={() => {this.routeChange(propvalue.url)}}>
						<span alt={propvalue.name}>{propvalue.name}</span>
					</li>
				</ul>
			</div>}
		);
	}

	getLinkArray() {
		let linksarray = [],
		reportlinksObj = {
			label: "Custom Report",
			options: []
		};
		this.props.app.accessNavigation.modules.map((moduleitem) => {
			moduleitem.child.map((childitem) => {
				childitem.links.map((linkitem) => {
					if(linkitem.type == 'folder') {
						linkitem.reportlinks.map((reportlinkitem) => {
							let reportLinkFound = false;
							reportlinksObj.options.every((item) => {
								if(item.url == reportlinkitem.url) {
									reportLinkFound = true;
									return false;
								}
								return true;
							});
							if(!reportLinkFound && linkitem._access) {
								reportlinksObj.options.push(reportlinkitem);
							}
						});
					} else {
						let linkFound = false;
						linksarray.every((item) => {
							if(item.url == linkitem.url) {
								linkFound = true;
								return false;
							}
							return true;
						});
						if(!linkFound && linkitem._access) {
							linksarray.push(linkitem);
						}
					}
				});
			});
		});

		if(reportlinksObj.options.length > 0)
			linksarray.push(reportlinksObj);

		this.setState({ linksarray });
	}

	optionRenderer = (props, searchValue) => {
		return (
			<components.Option {...props}>
				<Highlighter className="select-option" highlightClassName="select-text-highlighter" autoEscape={true} searchWords={[searchValue]} textToHighlight={props.data.name} />
			</components.Option>
		);
	}

	render() {
		if(!this.state && !this.state.navigationmenu)
			return null;
		let getOptions = (input, callback) => {
			let searchresults = [...this.state.linksarray].filter(link => {
				return link.options || (link.name && link.name.toLowerCase().includes(input.toLowerCase()));
			}).sort((a, b) => {
				if(a.options)
					return 1;
				if(a.name.toLowerCase().indexOf(input.toLowerCase()) > b.name.toLowerCase().indexOf(input.toLowerCase())) {
					return 1;
				} else if (a.name.toLowerCase().indexOf(input.toLowerCase()) < b.name.toLowerCase().indexOf(input.toLowerCase())) {
					return -1;
				} else {
					if(a.name > b.name)
						return 1;
					else
						return -1;
				}
			});

			callback(searchresults);
		}
		const customFilterOption = (option, rawInput) => {
			this.filterinputvalue = rawInput;

			const words = rawInput.split(' ');
			return words.reduce(
				(acc, cur) => acc && option.label.toLowerCase().includes(cur.toLowerCase()),
				true
			);
		};
		return (
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row justify-content-center" style={{backgroundColor: '#f6f6f6', borderBottom: '1px solid #c7c7c7'}}>
					<div className="col-md-8" style={{padding: '15px'}}>
						<AsyncSelect
							blurInputOnSelect={true}
							autoFocus={true}
							components={{
								Option: (props) => this.optionRenderer(props, this.filterinputvalue)
							}}
							getOptionLabel={({name}) => name}
							getOptionValue={({url}) => url}
							loadOptions={getOptions}
							value={this.state.searchterm}
							defaultOptions
							filterOption={customFilterOption}
							onChange={(val) => this.onSearchChange(val.url)}
							placeholder={'Type to Search....'}
							pageSize={10} />
					</div>
					<div className="col-md-12">
						<div className="d-flex justify-content-center">
							<ul className="nav nav-tabs gs-home-navtabs gs-home-navbar-scrollx">
								{this.renderNavMenu()}
							</ul>
						</div>
					</div>
				</div>
				<div className="row bg-white">
					<div className="col-md-10 offset-md-1">
						<div className="tab-content">
							<div className="tab-pane fade show active">
								{this.renderNavLink()}
							</div>
						</div>
					</div>
				</div>
			</>
		);
	};
});
