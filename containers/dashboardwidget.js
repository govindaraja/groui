import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, Fields, FieldArray, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { updateFormState, updateReportFilter } from '../actions/actions';
import { requiredNewValidation, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation, checkActionVerbAccess } from '../utils/utils';
import { InputEle, SpanEle, DateEle, textareaEle, selectAsyncEle, localSelectEle, autoSelectEle, checkboxEle,  autosuggestEle, autoMultiSelectEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import DashboardComponent from '../components/dashboardcomponent';

class DashboardWidgetForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			widgetArray: [],
			dashboardObj: {},
			widgetId: null,
			isMaximize: false
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.maximizeWidget = this.maximizeWidget.bind(this);
		this.renderWidget = this.renderWidget.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	onLoad() {
		let { widgetArray, dashboardObj } = this.state;

		widgetArray = [];
		dashboardObj = {};

		let dashboardid = null;

		axios.get(`/api/query/getdashboardquery?type=dashboard&dashboardid=${this.props.match.params.id}`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.length > 0) {
					widgetArray = response.data.main;

					if (this.props.match.params.id != 'get') {
						let isAccess = false;

						dashboardid = this.props.match.params.id;

						widgetArray.some((item) => {
							if (item.id == this.props.match.params.id) {
								dashboardObj = item;
								isAccess = true;

								return true;
							} else
								return false;
						});

						if (!isAccess) {
							this.updateLoaderFlag(false);

							return this.props.openModal(modalService.infoMethod({
								header: "Error",
								body: "Invalid Widget is choosen",
								btnArray: ["Ok"]
							}));
						}

						document.getElementById('pagetitle').innerHTML = (widgetArray.length > 0 && dashboardObj) ? `Dashboard - ${dashboardObj.name}` : 'Dashboard Widget';

						this.setState({
							widgetArray,
							dashboardObj
						}, () => {
							this.props.initialize({
								dashboardid
							});
						});
					} else {
						dashboardObj = response.data.main[0];
						dashboardid = response.data.main[0].id;

						this.props.history.replace("/dashboardwidget/" + dashboardid);
					}
				}
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	maximizeWidget(val, flag) {
		if (flag)
			$('body').css('overflow', 'hidden');
		else
			$('body').css('overflow', '');

		this.setState({
			widgetId: flag ? val : null,
			isMaximize: flag
		});
	}

	renderWidget () {
		let { widgetArray, dashboardObj, isMaximize, widgetId } = this.state;

		let Window_Width = (document.getElementsByClassName('layout').length > 0 ? document.getElementsByClassName('layout')[0].offsetWidth : $(window).width());

		let temp_width = (Window_Width > 700 ? Window_Width : 700)- 30;

		let width = Math.round(temp_width / dashboardObj.columns);

		let temp_height = Math.round($(window).height() / dashboardObj.rows);

		let height = temp_height > 110 ? temp_height : 110;

		let dashboarditems = dashboardObj.dashboarditems.sort((a, b) => {
			return (a.sortValue < b.sortValue) ? -1 : (a.sortValue > b.sortValue) ? 1 : 0;
		});

		return dashboarditems.map((widget) => {
			let widgetWidth = Math.round(width * widget.w),
				widgetHeight = Math.round(height * widget.h),
				translateWidth = Math.round((width * widget.x)+ 10),
				translateHeight = Math.round(((height) * widget.y) + 10);

			let maximizeStyle = {
				position: 'fixed',
				width: '100%',
				float: 'left',
				height: $(window).height(),
				padding: '25px',
				top: 0,
				left: 0,
				right: 0,
				bottom: 0,
				zIndex: 99999999,
				overflow: 'hidden',
				backgroundColor: 'rgba(0, 0, 0, 0.5)'
			};

			let widgetStyle = {
				width: `${widgetWidth-15}px`,
				height: `${widgetHeight-10}px`,
				//paddingRight: '15px',
				position: 'absolute',
				transform: `translate(${translateWidth}px, ${translateHeight}px)`,
				borderRadius: '5px',
				padding: widget.reportid_config.analyticstype == 'KPI' ? '12px 14px 12px 20px' : '18px 14px 12px 20px'
			};

			let conditionFlag = (isMaximize && widgetId == widget.reportid) ? true : false;

			return (
				<div className = {`widget-${widget.reportid} ${!conditionFlag ? 'bg-white' : ''}`} key = {widget.reportid} style = {conditionFlag ? maximizeStyle : widgetStyle}>
					{ widget.isAccess ? <DashboardComponent
						reportid = {widget.reportid}
						resource = {this.props.resource}
						openModal = {this.props.openModal}
						history={this.props.history}
						maximizeWidget = {(val, flag) => this.maximizeWidget(val, flag)}
						isMaximize = {conditionFlag}
						dashboardid = {dashboardObj.id}/> : null
					}

					{ !widget.isAccess ? <div className = 'row no-gutters h-100' >
						<div className = 'col-md-12'>
							<div className = {`overflowtxt gs-dashboard-textColor ${isMaximize ? 'isMaximize' : ''}`}>
								{widget.reportid_name}
							</div>
							<div className = 'd-flex flex-column align-items-center justify-content-center' style = {{height: '100%'}}>
									<img src = '../images/dashboard_noaccess.png' className = 'list-error-image' style = {{width: '28px'}}/ >
									<div className = 'padding-10 font-12 form-group'>You have no access to view this widget</div>
							</div>
						</div>
					</div> : null }
				</div>
			)
		});
	};

	render() {
		let { widgetArray, dashboardObj, isMaximize, widgetId } = this.state,
			isAccess = this.props.match.params.id == 'get' ? true : false;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						{ widgetArray.length > 0 ? <div className="col-md-12 bg-white report-header">
							<div className="d-flex flex-row justify-content-center float-left" style={{width: '20%'}}>
								<div className = 'gs-uppercase gs-select-border-0 semi-bold-custom' style={{width: '100%', display: 'block', margin: '10px 0px 10px 10px', fontSize: '16px'}}>
									{dashboardObj.name}
								</div>
							</div>
						</div> : null }
						{ (dashboardObj && dashboardObj.dashboarditems) ?
							<div className = 'col-md-12' style={{backgroundColor: '#F1F1F1', paddingBottom: '20px'}}>
								<div className = 'row'>
									<div className="col-md-12 layout" style={{height:`${$(window).height() - 120}px`, overflowY: 'auto', overflowX: 'hidden'}}>
										{this.renderWidget()}
									</div>
								</div>
							</div> : <div className = 'widget-wrapper col-md-12 form-group' style = {{
									height: `${$(window).outerHeight() - (document.getElementsByClassName('navbar').length > 0 ? document.getElementsByClassName('navbar')[0].clientHeight : 50) - 50}px`
								}}
							>
								<div className = 'widget-center d-flex flex-column align-items-center justify-content-center'>
									<div className = 'list-error-image-box'>
										{ isAccess ? <img src = '/images/empty-placeholder.png' className = 'list-error-image' width = '150' height = '150' /> : <img src="/images/dashboard_noaccess.png" className="report-error-image" style={{width:"80px", height:"80px", marginBottom: "15px" }}/>}
									</div>
									<div className = 'list-error-text-box'>
										<div className = 'list-error-text'>
											<div className = 'marginbottom-15'>{isAccess ? 'No Dashboard Widget available. Create a Dashboard Widget.' : 'Sorry, You have no access to view this widget'}</div>
											{isAccess && checkActionVerbAccess(this.props.app, 'dashboard', 'Save') ? <div>
												<Link to = {`/createDashBoard`} className = 'btn btn-sm gs-btn-success'><span className = 'fa fa-plus marginright-5'></span>Create Dashboard</Link>
											</div> : null }
										</div>
									</div>
								</div>
							</div>
						}
					</div>
				</form>
			</>
		);
	};
}

DashboardWidgetForm = connect(
	(state, props) => {
		let formName = 'dashboardwidget';

		return {
			app: state.app,
			form: formName,
			destroyOnUnmount: false,
			resource: state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData: state.form,
			fullstate: state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(DashboardWidgetForm));

export default DashboardWidgetForm;