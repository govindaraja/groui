import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { updateFormState } from '../actions/actions';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import { DateEle, autoSelectEle } from '../components/formelements';
import { numberNewValidation, dateNewValidation } from '../utils/utils';
import { dateFilter, datetimeFilter, timeFilter} from '../utils/filter';

class AttendanceCapture extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loaderflag : true,
			tempWindow: null
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.save = this.save.bind(this);
		this.initialize = this.initialize.bind(this);
		this.getLeaveRequest = this.getLeaveRequest.bind(this);
		this.getEmployee = this.getEmployee.bind(this);
		this.validation = this.validation.bind(this);
		this.showHistory = this.showHistory.bind(this);
		this.openAttendance = this.openAttendance.bind(this);
		this.updateItem = this.updateItem.bind(this);
		this.dateOnChange = this.dateOnChange.bind(this);
		this.mapView = this.mapView.bind(this);
	}

	componentWillMount() {
		this.initialize();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	initialize() {
		let tempObj = {
			date : new Date(),
			employeeArray : [],
			weekfoff_holiday : [],
			leaveArray : [],
			historyArray : [],
			disable : false,
			locationid : null
		};

		axios.get(`/api/employees?&field=id,displayname,locationid&filtercondition=employees.userid=${this.props.app.user.id}`).then((response)=> {
			if (response.data.message == 'success') {
				if(response.data.main.length > 0)
					tempObj['locationid'] = response.data.main[0].locationid;

				this.props.initialize(tempObj);

				setTimeout(()=>{
					this.getLeaveRequest(this.props.resource.locationid);
				});

			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	getLeaveRequest (Locid) {
		this.updateLoaderFlag(true);
		axios.get(`/api/leaverequests?field=id,employeeid,reason,leavetypeid,noofdays,ishalfday&filtercondition='${new Date(this.props.resource.date).toDateString()}'::date between leaverequests.fromdate and leaverequests.todate and leaverequests.status='Approved'`).then((response)=> {
			if (response.data.message == 'success') {
				this.props.updateFormState(this.props.form, {
					leaveArray : response.data.main
				});

				if(Locid)
					this.getEmployee(Locid);
				else
					this.getEmployee();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	getEmployee(Locid) {
		this.updateLoaderFlag(true);

		let filterArray = [`date=${new Date(this.props.resource.date).toDateString()}`];

		let locationid = Locid ? Locid : this.props.resource.locationid;

		if(locationid)
			filterArray.push(`locationid=${locationid}`);

		if (this.props.app.user.roleid.indexOf(20) >= 0 && !locationid) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Location Name is required !!!",
				btnArray : ["Ok"]
			}));
		}

		axios.get(`/api/query/attendancecapturequery?${filterArray.join('&')}`).then((response)=> {
			if (response.data.message == 'success') {

				response.data.main.forEach((item) => {
					for (let i = 0; i < this.props.resource.leaveArray.length; i++) {
						if(item.employeeid == this.props.resource.leaveArray[i].employeeid) {
							if (!item.intime && item.leave) {
								item.leave = true;
								item.leavetype = this.props.resource.leaveArray[i].ishalfday ? 'Half Day' : 'Full Day';
							} else
								item.leave = false;

							break;
						}
					}

					item['date'] = new Date(this.props.resource.date);
				});

				response.data.main.sort((a, b) => {
					return (a.employeeid_displayname.toLocaleLowerCase() < b.employeeid_displayname.toLocaleLowerCase()) ? -1 : (a.employeeid_displayname.toLocaleLowerCase() > b.employeeid_displayname.toLocaleLowerCase() ? 1 : 0);
				});

				this.props.updateFormState(this.props.form, {
					employeeArray : response.data.main,
					weekfoff_holiday : response.data.weekfoff_holiday,
					disable : (new Date(new Date(this.props.resource.date).toDateString()) < new Date(new Date().toDateString())) ? true : false
				});

			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	validation (type, item, index) {
		this.updateLoaderFlag(true);

		let error = [];

		if (type == 'out') {
			if (!item.intime || item.intime == null || item.intime == 'undefined')
				error.push('Outtime must have intime');
		}

		if (error.length > 0) {
			let response = {
				data : {
					message : 'failure',
					error : error
				}
			};
			this.updateLoaderFlag(false);
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		} else
			this.save(type, item, index);
	}

	save (type, item, index) {

		let attendanceCapture = {
			intime : item.intime ? item.intime : null,
			outtime : item.outtime ? item.outtime : null,
			employeeid : item.employeeid
		};
		let date = new Date(this.props.resource.date).setHours(new Date().getHours(),new Date().getMinutes());

		if(type == 'in')
			attendanceCapture['intime'] = new Date(date);

		if(type == 'out')
			attendanceCapture['outtime'] = new Date(date);

		if (type == 'absent') {
			attendanceCapture['intime'] = null;
			attendanceCapture['outtime'] = null;
			attendanceCapture['absent'] = true;
		}

		if (item.id) {
			attendanceCapture['absent'] = type == 'absent' ? true : item.absent;
			attendanceCapture['leave'] = item.leave;
			attendanceCapture['leavetype'] = item.leavetype;
			attendanceCapture['onduty'] = item.onduty;
			attendanceCapture['permission'] = item.permission;
		}

		attendanceCapture['date'] = new Date(date).toDateString();

		axios({
			method : 'post',
			data : {
				actionverb : 'Save',
				data : attendanceCapture
			},
			url : '/api/attendancecapture'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

			if(response.data.message == 'success')
				this.updateItem(response.data.main, index);

			this.updateLoaderFlag(false);
		});
	}

	updateItem (item, index) {
		this.props.updateFormState(this.props.form, {
			[`employeeArray[${index}].absent`] : item.absent,
			[`employeeArray[${index}].id`] : item.id,
			[`employeeArray[${index}].onduty`] : item.onduty,
			[`employeeArray[${index}].intime`] : item.intime,
			[`employeeArray[${index}].outtime`] : item.outtime,
			[`employeeArray[${index}].remarks`] : item.remarks,
			[`employeeArray[${index}].permission`] : item.permission,
			[`employeeArray[${index}].history`] : item.history
		});

		this.updateLoaderFlag(false);
	}

	showHistory (item) {
		if (item.history) {
			let historyArray = item.history.details;

			historyArray.sort((a, b) => {
				return (a.modified < b.modified) ? 1 : (a.modified > b.modified ? -1 : 0);
			});

			this.props.updateFormState(this.props.form, { historyArray });

			setTimeout(()=> {
				this.props.openModal({
					render: (closeModal) => {
						return <RenderHistory
							resource = {this.props.resource}
							historyArray = {this.props.resource.historyArray}
							closeModal = {closeModal}
							openModal = {this.openModal} />
					}, className: {
						content: 'react-modal-custom-class-80',
						overlay: 'react-modal-overlay-custom-class'
					}
				});
			}, 0);
		}
	}

	openAttendance(item, index) {
		if(item.id > 0)
			this.props.createOrEdit('/details/attendancecapture/:id', item.id, {}, (valueObj) => {this.updateItem(valueObj, index);});
		else
			this.props.createOrEdit('/createAttendanceCapture', item.id, item, (valueObj) => {this.updateItem(valueObj, index);});
	}

	dateOnChange () {
		this.props.updateFormState(this.props.form, {
			employeeArray: [],
			weekfoff_holiday: []
		});
	};

	mapView (type, item, index) {
		let { tempWindow } = this.state;

		if (tempWindow) {
			tempWindow.close();
			tempWindow = null
		}

		let latlng;

		if (type == 'in')
			latlng = `${item.loginlatitude},${item.loginlongitude}`;

		if (type == 'out')
			latlng = `${item.logoutlatitude},${item.logoutlongitude}`;

		tempWindow = window.open(`https://www.google.com/maps/?q=${latlng}`);

		tempWindow.focus();

		this.setState({
			tempWindow
		});
	}

	renderAttendance(item, index) {
		return (
			<tr key={index}>
				<td>{item.employeeid_displayname}</td>
				<td>{item.locationid_name}</td>
				<td className="text-center">
					{item.intime ? <div>{timeFilter(item.intime)}
						<button type="button" className="btn btn-sm btn-outline-dark" style={{float:"right"}} disabled={!item.loginlatitude || !item.loginlongitude} onClick={()=> {this.mapView('in', item, index)}}><span className="fa fa-globe"></span></button>
					</div> : <button type="button" className="btn btn-width btn-sm btn-dark" onClick={()=> {this.validation('in', item, index)}} disabled={item.absent || (item.leavetype == "Full Day") || this.props.resource.disable}><i className="fa fa-arrow-circle-right"></i>Check In</button>}
				</td>
				<td className="text-center">
					{item.outtime ? <div>{timeFilter(item.outtime)}
						<button type="button" className="btn btn-sm btn-outline-dark" disabled={!item.logoutlatitude || !item.logoutlongitude} style={{float:"right"}} onClick={()=> {this.mapView('out', item, index)}}><span className="fa fa-globe"></span></button>
					</div> : <button type="button" className="btn btn-width btn-sm btn-dark" onClick={()=> {this.validation('out',item, index)}} disabled={item.absent || (item.leavetype == "Full Day") || !item.intime || this.props.resource.disable}><i className="fa fa-arrow-circle-left"></i>Check Out</button>}
				</td>
				<td className="text-center">
					<button type="button" className={`btn btn-width btn-sm ${item.absent ? 'gs-btn-danger' : 'btn-dark'}`} onClick={()=> {this.validation('absent',item, index)}} disabled={item.absent || (item.leavetype == "Full Day") || item.intime}><i className="fa fa-ban"></i>Absent</button>
				</td>
				<td className="text-center">{item.permission ? item.permission : '-'}</td>
				<td className="text-center">{item.leavetype ? item.leavetype : '-'}</td>
				<td className="text-center">{item.onduty ? 'Yes' : '-'}</td>
				{(this.props.app.user.roleid.indexOf(20) >= 0 || this.props.app.user.roleid.indexOf(1) >= 0) ? <td className="text-center">
					<button type="button" className="btn btn-sm btn-outline-dark" onClick={()=> {this.openAttendance(item, index)}}><span className="fa fa-edit"></span></button>
				</td> : null}
				{(this.props.app.user.roleid.indexOf(20) >= 0 || this.props.app.user.roleid.indexOf(1) >= 0) ? <td className="text-center">
					<button type="button" className="btn btn-sm btn-outline-dark" onClick={()=> {this.showHistory(item)}} disabled={!item.history} rel="tooltip" title="View History"><span className="fa fa-edit"></span></button>	
				</td> : null}
			</tr>
		)
	}

	render() {
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="form-group col-md-12 col-sm-12 col-xs-12 bg-white report-header" style={{position: 'fixed', height: '45px', backgroundColor: '#FAFAFA',  zIndex: 5}}>
						<div className="report-header-title paddingleft-md-30">Attendance</div>
					</div>
					<div className="form-group bg-white col-md-12 col-sm-12 col-xs-12" style={{marginTop:'60px'}}>
						<div className="row col-md-10 col-sm-10 col-xs-10 offset-md-1">
							<div className="form-group col-md-3 col-sm-6 col-xs-6">
								<label className="labelclass">Date</label>
								<Field name={'date'} props={{required: true, max: new Date(new Date().setHours(0, 0, 0, 0)),onChange: () => {this.dateOnChange()}}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'Date'})]} />
							</div>
							{ this.props.app.user.roleid.indexOf(20) >= 0 ? <div className="form-group col-md-3 col-sm-6 col-xs-6">
								<label className="labelclass">Location</label>
								<Field name={'locationid'} props={{resource: "locations", fields: "id,name",onChange: () => {this.dateOnChange()}}} component={autoSelectEle} validate={[numberNewValidation({required: true})]}/>
							</div> : null }
							<div className="form-group col-md-3 col-sm-6 col-xs-6 margintop-25">
								<button type="button" onClick={() => {
									this.getLeaveRequest()	
								}} className="btn btn-width btn-sm gs-btn-outline-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
							</div>
						</div>
					</div>
					{(this.props.resource && this.props.resource.weekfoff_holiday.length > 0) ? <div className="col-md-12 col-sm-12 col-xs-12 text-center bg-white">
						<h4 style={{color:'green'}}>{this.props.resource.weekfoff_holiday[0]}</h4>
					</div> : null}
					<div className="col-md-12 col-sm-12 col-xs-12 margintop-25">
						<div className="col-md-12 col-sm-12 col-xs-12">
							{this.props.resource && this.props.resource.employeeArray.length > 0 ?
								<table className="table table-bordered table-hover bg-white">
									<thead>
										<tr>
											<th className="text-center">Employee</th>
											<th className="text-center">Location</th>
											<th className="text-center">In Time</th>
											<th className="text-center">Out Time</th>
											<th className="text-center">Absent</th>
											<th className="text-center">Permission</th>
											<th className="text-center">Leave</th>
											<th className="text-center">On Duty</th>
											{(this.props.app.user.roleid.indexOf(20) >= 0 || this.props.app.user.roleid.indexOf(1) >= 0) ? <th className="text-center">Edit</th> : null}
											{(this.props.app.user.roleid.indexOf(20) >= 0 || this.props.app.user.roleid.indexOf(1) >= 0) ? <th className="text-center">History</th> : null}
										</tr>
									</thead>
									<tbody>
										{this.props.resource.employeeArray ? this.props.resource.employeeArray.map((item, index) => this.renderAttendance(item, index)) : null}
									</tbody>
								</table> : null
							}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class RenderHistory extends Component {
	constructor(props) {
		super(props);
	};

	render() {
		if(this.props.historyArray.length == 0)
			return null;

		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Attendance History</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-hover table-condensed">
								<thead>
									<tr>
										<th className="text-center">Date</th>
										<th className="text-center">Modified Details</th>
										<th className="text-center">In Time</th>
										<th className="text-center">Out Time</th>
										<th className="text-center">Leave</th>
										<th className="text-center">Permission</th>
										<th className="text-center">On Duty</th>
										<th className="text-center">Remarks</th>
									</tr>
								</thead>
								<tbody>
									{ this.props.historyArray.map((item, index) => {
										return (
											<tr key={index}>
												<td style={{width:'100px',whiteSpace:'nowrap'}}>{dateFilter(item.date)}</td>
												<td style={{width:'100px',whiteSpace:'nowrap'}}>{item.modifiedby_displayname}<br /><i>({datetimeFilter(item.modified)})</i></td>
												<td className="text-center">{timeFilter(item.intime)}</td>
												<td className="text-center">{timeFilter(item.outtime)}</td>
												<td className="text-center">{item.leavetype ? item.leavetype : '-'}</td>
												<td className="text-center">{item.permission ? item.permission : '-'}</td>
												<td className="text-center">{item.onduty ? 'Yes' : '-'}</td>
												<td style={{width:'150px',wordWrap:'break-word',wordBreak:'break-all'}}>{item.remarks}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

AttendanceCapture = connect(
	(state, props) => {
		let formName = 'AttendanceCapture';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state
		}
	}, { updateFormState}
)(reduxForm()(AttendanceCapture));

export default AttendanceCapture;
