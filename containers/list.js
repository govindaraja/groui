import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import moment from 'moment';
import Select from 'react-select';
import Modal from 'react-modal';
import { Link, matchPath } from 'react-router-dom';
import XLSX from 'xlsx';
import { saveAs } from 'file-saver';

import {updatePageJSONState, updateAppState} from '../actions/actions';
import {updateListState} from '../actions/list';

import {getPageJson, getPageOptions, checkArray, getListTitle, checkActionVerbAccess} from '../utils/utils';
import {commonMethods, modalService} from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter ,multiSelectAutoFilter, resourceFilter, itemmasterDisplaynamefilter} from '../utils/filter';
import { excel_sheet_from_array_of_arrays, excel_Workbook, excel_s2ab } from '../utils/excelutils';
import Loadingcontainer from '../components/loadingcontainer';
import { LocalSelect, SelectAsync, AutoSelect, DateElement, NumberElement, AutoMultiSelect } from '../components/utilcomponents';
import KanbanBoard from '../components/list/kanbanboard';
import Calendarview from '../components/list/calendarview';
import Treeview from '../components/list/treeview';
import ListPageEditModal from './listview';
import CreateListViewOptionModal from './createlistviewoptionmodal';
import routes from '../routeconfig';

// List Default Values
const filterOperators = {
	'date': ['Equals', 'Not Equals', 'More than', 'More than or equal', 'Less than', 'Less than or equal', 'Between', 'Is Empty', 'Is Not Empty'],
	'string': ['Equals', 'Not Equals', 'Like', 'Is Empty', 'Is Not Empty'],
	'autocomplete': ['Equals', 'Not Equals', 'Is Empty', 'Is Not Empty'],
	'select': ['Equals', 'Not Equals', 'In', 'Is Empty', 'Is Not Empty'],
	'selectasync': ['Equals', 'Not Equals', 'Is Empty', 'Is Not Empty'],
	'integer': ['Equals', 'Not Equals', 'More than', 'More than or equal', 'Less than', 'Less than or equal', 'Is Empty', 'Is Not Empty'],
	'boolean': ['Equals', 'Not Equals', 'Is Empty', 'Is Not Empty'],
	'multiselectauto': ['In', 'Is Empty', 'Is Not Empty']
};
const operatorSymbols = {
	'Equals': '=',
	'Not Equals': '<>',
	'More than': '>',
	'More than or equal': '>=',
	'Less than': '<',
	'Less than or equal': '<=',
	'Like': 'Like',
	'In' : 'In',
	'Between' : 'Between',
	'Is Empty' : 'Is Empty',
	'Is Not Empty' : 'Is Not Empty'
};
const pageLengths = [20, 50, 100, 500];
const defaultPageLength = 20;
const defaultListView = 'List';

class List extends Component {
	constructor(props) {
		super(props);

		let resourceArray = [];

		let myResources = this.props.app.myResources;

		for (let prop in myResources) {
			if (myResources[prop].type != 'query')
				resourceArray.push({
					id : prop,
					name : myResources[prop].displayName
				});
		}

		this.state = {
			title: this.props.title,
			resourceArray: resourceArray,
			basicFilter: [],
			advancedFilterFields: [],
			advancedFilter: [],
			viewFilter: [],
			advFilterObj: {},
			isAdvanceFilterEnabled: false,
			showAdvanceFilter: false,
			showAdvanceFilterModal: false,
			advanceFilterIndexArr: [],
			isModalopen: false,
			userlicensedetails: {},
			expensereqdetails: {},
			kanbansalesperson: this.props.app.user.issalesperson ? [this.props.app.user.id]: []
		};
		this.pageJSONCallback = this.pageJSONCallback.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getView = this.getView.bind(this);
		this.getFilterObj = this.getFilterObj.bind(this);
		this.getDataFn = this.getDataFn.bind(this);
		this.renderTbody = this.renderTbody.bind(this);
		this.pagecountChange = this.pagecountChange.bind(this);
		this.handlePaginationClick = this.handlePaginationClick.bind(this);
		this.tableRowClick = this.tableRowClick.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getCellDisplayName = this.getCellDisplayName.bind(this);
		this.checkArray = this.checkArray.bind(this);
		this.getOperators = this.getOperators.bind(this);
		this.basicSearchFunction = this.basicSearchFunction.bind(this);
		this.advanceSearchFunction = this.advanceSearchFunction.bind(this);
		this.advancedFunction = this.advancedFunction.bind(this);
		this.filterInputChange = this.filterInputChange.bind(this);
		this.insertAdvFilter = this.insertAdvFilter.bind(this);
		this.removeAdvFilter = this.removeAdvFilter.bind(this);
		this.operatorOnChange = this.operatorOnChange.bind(this);
		this.showSelectAsyncFilter = this.showSelectAsyncFilter.bind(this);
		this.filterBetweenInputChange = this.filterBetweenInputChange.bind(this);
		this.changeView = this.changeView.bind(this);
		this.listviewsOnChange = this.listviewsOnChange.bind(this);
		this.handleCloseModal = this.handleCloseModal.bind(this);
		//this.onViewUpdate = this.onViewUpdate.bind(this);
		this.renderTableCell = this.renderTableCell.bind(this);
		this.renderFilter = this.renderFilter.bind(this);
		this.renderBetweenFilter = this.renderBetweenFilter.bind(this);
		this.renderTableHead = this.renderTableHead.bind(this);
		this.renderAdvancedFilter = this.renderAdvancedFilter.bind(this);
		this.renderTitleBar = this.renderTitleBar.bind(this);
		this.renderMultipleViews = this.renderMultipleViews.bind(this);
		this.renderAddButton = this.renderAddButton.bind(this);
		this.renderBasicFilter = this.renderBasicFilter.bind(this);
		this.renderPageCounts = this.renderPageCounts.bind(this);
		this.renderPagination = this.renderPagination.bind(this);
		this.renderFilterTags = this.renderFilterTags.bind(this);
		this.validateAdvFilter = this.validateAdvFilter.bind(this);
		this.reset = this.reset.bind(this);
		this.closeListViewModal = this.closeListViewModal.bind(this);
		this.download = this.download.bind(this);
		this.listbodyRef = this.listbodyRef.bind(this);
		this.getUsersLicenseDetails = this.getUsersLicenseDetails.bind(this);
		this.addAdvancedFilterfn = this.addAdvancedFilterfn.bind(this);
		this.addAdvancedfilterField = this.addAdvancedfilterField.bind(this);
		this.renderAdvancedFilterModal = this.renderAdvancedFilterModal.bind(this);
		this.openFilterTagEditModal = this.openFilterTagEditModal.bind(this);
		this.renderAdvancedFilterContainer = this.renderAdvancedFilterContainer.bind(this);
		this.handleEnter = this.handleEnter.bind(this);
		this.advanceFilterReset = this.advanceFilterReset.bind(this);
		this.getExpenseRequestDetails = this.getExpenseRequestDetails.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		getPageJson(this.props, getPageOptions(this.props.location.pathname), this.pageJSONCallback);
	}

	pageJSONCallback(pagejson) {
		document.getElementById("pagetitle").innerHTML = pagejson.title;
		this.setState({
			pagejson: pagejson,
			resource: this.props.app.myResources[pagejson.resourcename]
		}, function() {
			this.onLoad();
		});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let currView = this.getView();
		let filter;
		let advanceFilterIndexArr = [], showAdvanceFilter = false;
		if(this.props.list.initial) {
			let viewsArr = ['List'];

			if(this.state.pagejson.calendarOptions)
				viewsArr.push('Calendar');

			if(this.state.pagejson.kanbanView)
				viewsArr.push('Kanban');

			if(this.state.pagejson.treeView)
				viewsArr.push('Tree');

			this.props.list.viewsArray = viewsArr;
			this.props.list.listview = defaultListView;
			this.props.list.currentview = currView;
			this.props.list.initial = false;
		} else {
			filter = {
				advancedFilter: this.props.list.filter.advancedFilter,
				isAdvanceFilterEnabled: this.props.list.filter.advancedFilter.length > 0 ? true : false,
				_basicFilter: this.props.list.filter.basicFilter,
				kanbansalesperson: this.props.list.filter.kanbansalesperson
			}
			this.props.list.currentview = this.props.list.currentview ? this.props.list.currentview : currView;
			this.props.list.filter.advancedFilter.forEach((item) => {
				advanceFilterIndexArr.push(false);
			});
			showAdvanceFilter = true;
		}
		this.setState({
			currentviewid: this.props.list.currentview.id,
			advanceFilterIndexArr,
			showAdvanceFilter,
			...filter
		}, () => {
			this.getFilterObj();
			if(this.state.pagejson.resourcename == 'users')
				this.getUsersLicenseDetails();
			if(this.state.pagejson.resourcename == 'expenserequests')
				this.getExpenseRequestDetails();
		});
	}

	getUsersLicenseDetails () {
		this.updateLoaderFlag(true);
		axios.get('/api/query/userlicensequery').then((response) => {
			if (response.data.message == 'success') {
				let userlicensedetails = {};
				userlicensedetails.user = response.data.main;
				userlicensedetails.track = response.data.track;
				userlicensedetails.ess = response.data.ess;
				this.setState({userlicensedetails})
			} else {
				let apiResponse = commonMethods.apiResult(res);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getExpenseRequestDetails() {
		this.updateLoaderFlag(true);
		axios.get('/api/query/expenserequestuserdetailsquery').then((response) => {
			if (response.data.message == 'success') {
				let expensereqdetails = {};
				response.data.main.forEach((item) => {
					if(item.displaytitle == 'Awaiting Reimbursement')
						expensereqdetails.awaitingreimbursement = item.amount;
					if(item.displaytitle == 'Claims to be Approved')
						expensereqdetails.claimstobeapproved = item.amount;
					if(item.displaytitle == 'Advances with me')
						expensereqdetails.advanceswithme = item.amount;
				});
				this.setState({ expensereqdetails });
			} else {
				let apiResponse = commonMethods.apiResult(res);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getView() {
		let views = this.state.pagejson.views;
		for (var i = 0; i < views.length; i++) {
			if (views[i].userids.indexOf(this.props.app.user.id) >= 0 && views[i].isdefault)
				return views[i];
			if (views[i].roleids.length > 0 && views[i].isdefault) {
				return views[i];
			}
			if (views[i].visibility == 'all' && views[i].isdefault)
				return views[i];
		}
		return views[0];
	}

	getFilterObj() {
		let filterStringDefaults = {
			'<today>': new Date().toDateString(),
			'<today+1>': new Date(new Date().setDate(new Date().getDate() + 1)).toDateString(),
			'<today-1>': new Date(new Date().setDate(new Date().getDate() - 1)).toDateString(),
			'<today+3>': new Date(new Date().setDate(new Date().getDate() + 3)).toDateString(),
			'<today-7>': new Date(new Date().setDate(new Date().getDate() - 7)).toDateString(),
			'<today+15>': new Date(new Date().setDate(new Date().getDate() + 15)).toDateString(),
			'<today+30>': new Date(new Date().setDate(new Date().getDate() + 30)).toDateString(),
			'<today-90>': new Date(new Date().setDate(new Date().getDate() + 90)).toDateString(),
			'<firstdayofmonth>': new Date(new Date().getFullYear(), new Date().getMonth(), 1).toDateString(),
			'<lastdayofmonth>': new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toDateString()  
		};
		let filterNumberDefaults = {
			'<currentuser>': this.props.app.user.id  
		};
		let filterNumberDisplayDefaults = {
			'<currentuser>': this.props.app.user.displayname
		};

		let resourceJson = JSON.parse(JSON.stringify(this.props.app.myResources[this.state.pagejson.resourcename]));
		let currentview = this.props.list.currentview;
		let basicFilterArr = currentview.viewjson.basicfilter ? currentview.viewjson.basicfilter : [];
		let pageBasicFilterObj = {};
		if(this.state._basicFilter) {
			this.state._basicFilter.forEach((_filter) => {
				pageBasicFilterObj[_filter.displayNameFilter] = _filter.filterValue;
			});
			this.state._basicFilter = [];
		}
		for (var i = 0; i < basicFilterArr.length; i++) {
			let splitField = basicFilterArr[i].split("_");
			let tempField = resourceJson.fields[splitField[0]];
			if (tempField && this.checkArray(this.props.app.user.roleid, tempField.readroleid)) {
				if(splitField.length == 1) {
					tempField.displayNameFilter = splitField[0];
					tempField.filterValue = pageBasicFilterObj[tempField.displayNameFilter] != null ? pageBasicFilterObj[tempField.displayNameFilter] : null;
					this.state.basicFilter.push(tempField);
				} else {
					if(resourceJson.fields[splitField[0]] && resourceJson.fields[splitField[0]].isForeignKey) {
						let foreignResource = resourceJson.fields[splitField[0]].foreignKeyOptions.resource;
						let foreignField = this.props.app.myResources[foreignResource].fields[splitField[1]];
						if(foreignField) {
							var tempObj = JSON.parse(JSON.stringify(foreignField));
							tempObj.displayNameFilter = basicFilterArr[i];
							tempObj.resourceName = foreignResource + "_" + splitField[0];
							tempObj.displayName = tempField.displayName + "'s "+ foreignField.displayName;
							tempObj.filterValue = pageBasicFilterObj[tempObj.displayNameFilter] != null ? pageBasicFilterObj[tempObj.displayNameFilter] : null; 
							this.state.basicFilter.push(tempObj);
							this.state.advancedFilterFields.push(tempObj);
						}
					}
				}
			}
		}

		for(var prop in resourceJson.fields) {
			if(resourceJson.fields[prop].showInAdvanceSearch) {
				var tempObj = resourceJson.fields[prop];
				tempObj.displayNameFilter = prop;
				this.state.advancedFilterFields.push(tempObj);
			}
		}

		this.state.advancedFilterFields = this.state.advancedFilterFields.sort((advfilfield1, advfilfield2) => {
			return (advfilfield1.displayName.toLowerCase() > advfilfield2.displayName.toLowerCase() ? 1 : (advfilfield1.displayName.toLowerCase() < advfilfield2.displayName.toLowerCase() ? -1 : 0));
		});

		/*for (var i = 0; i < this.state.basicFilter.length; i++) {
			this.state.basicFilter[i].filterValue = "";
		}*/

		if (this.props.app.listFilterObj[this.state.pagejson.resourcename]) {
			this.state.advancedFilter = [];
			this.state.advanceFilterIndexArr = [];
			this.state.showAdvanceFilter = true;
			this.props.app.listFilterObj[this.state.pagejson.resourcename].forEach((advfilteritem) => {
				let filterVal = advfilteritem.value;
				let filterDisplayVal = '';
				if(advfilteritem.displayValue != null)
					filterDisplayVal = advfilteritem.displayValue;

				if(typeof(filterVal) != 'object' && typeof(filterVal) != 'number') {
					for(var prop in filterStringDefaults)
						filterVal = filterVal.replace(prop, filterStringDefaults[prop]);

					for(var prop in filterNumberDefaults) {
						if(filterVal.indexOf(prop) > -1) {
							filterVal = filterNumberDefaults[prop];
							filterDisplayVal = filterNumberDisplayDefaults[prop];
						}
					}
				}

				let tempoperatorObj = {"Equal" : "Equals", "NotEqual" : "Not Equals", "GreaterThan" : "More than", "LessThan" : "Less than", "GreaterThanOrEqual" : "More than or equal", "LessThanOrEqual" : "Less than or equal", "Between" : "Between", "In" : "In","Like" : "Like"};
				let tempFilterObj = {
					index : this.state.advancedFilter.length,
					displayNameFilter : advfilteritem.fieldname,
					operator : (filterVal == '' || filterVal == null) ? (advfilteritem.operator == 'Equal' ? 'Is Empty' : (advfilteritem.operator == 'NotEqual') ? 'Is Not Empty' : tempoperatorObj[advfilteritem.operator]) : tempoperatorObj[advfilteritem.operator],
					filterValue : filterVal,
					filterDisplayValue: filterDisplayVal
				};
				this.getOperators(tempFilterObj);
				this.state.advancedFilter.push(tempFilterObj);
				this.state.isAdvanceFilterEnabled = true;
				this.state.advanceFilterIndexArr.push(false);
			});

			delete this.props.app.listFilterObj[this.state.pagejson.resourcename];

			this.searchFunction(this.state.advancedFilter, null, true);
		}

		this.state.viewFilter = [];
		for (var i = 0; i < currentview.viewjson.condition.length; i++)  {
			let tempCondition = currentview.viewjson.condition[i];
			var filterValue = tempCondition.value;
			var filterBetValue = tempCondition.betvalue;
			if(typeof(filterValue) == 'string') {
				for(var prop in filterStringDefaults)
					filterValue = filterValue.replace(prop, filterStringDefaults[prop]);

				for(var prop in filterNumberDefaults)
					filterValue = filterValue.indexOf(prop) > -1 ? filterNumberDefaults[prop] : filterValue;
			}

			let tempFilterObj = {
				index : this.state.advancedFilter.length,
				displayNameFilter : tempCondition.field,
				operator : tempCondition.operator,
				filterValue : filterValue,
				filterBetValue : filterBetValue
			};
			this.getOperators(tempFilterObj);
			if(tempFilterObj.options)
				this.state.viewFilter.push(tempFilterObj);
		}
		this.searchFunction(this.state.viewFilter, true);
	}

	getDataFnOld() {
		this.updateLoaderFlag(true);
		let currentview = this.props.list.currentview;
		let tempSkip = this.props.list.page * this.props.list.pagelength;
		let filterString = this.props.list.filterString;
		//let conditionfilterString = this.props.list.conditionalfilterString;

		let queryString = `/api/${this.state.pagejson.resourcename}?pagelength=${this.props.list.pagelength}&skip=${tempSkip}&field=`;

		let idFound = currentview.viewjson.fields.indexOf('id') >= 0 ? true : false;
		let currencyFound = this.state.resource.fields['currencyid'] ? true : false;

		for(var i=0; i< currentview.viewjson.fields.length; i++) {
			var splitedFields = currentview.viewjson.fields[i].split('/');
			if(splitedFields.length == 1) {
				queryString += splitedFields[0]+",";
			} else {
				if(this.state.resource.fields[splitedFields[0]] && this.state.resource.fields[splitedFields[0]].isForeignKey) {
					let joinResourceName = this.state.resource.fields[splitedFields[0]].foreignKeyOptions.resource;
					queryString += joinResourceName+"/"+splitedFields[1]+"/"+splitedFields[0]+",";
				}
			}
		}

		if(!idFound)
			queryString += 'id,';
		if(currencyFound)
			queryString += 'currencyid,';

		queryString = queryString.substr(0,queryString.length-1);

		if(this.state.pagejson.includeinactive)
			queryString += "&includeinactive=true";

		queryString += "&filtercondition=";
		var conditionalfilterAdded = false;
		var urlconditionFilterAdded = false;
		var stocklocationFilterAdded = false, filterAdded = false;
		if(this.props.list.conditionalfilterString) {
			queryString += this.props.list.conditionalfilterString;
			conditionalfilterAdded = true;
		}
		if(this.state.pagejson.resourcename == 'stocklocations' && !this.props.app.feature.useSubLocations) {
			queryString += (conditionalfilterAdded==true ? " and " : "") + " stocklocations.parentid is null ";
			stocklocationFilterAdded = true;
		}
		if(this.state.pagejson.urlcondition) {
			queryString += (conditionalfilterAdded==true || stocklocationFilterAdded==true ? " and " : "") + this.state.pagejson.urlcondition;
			urlconditionFilterAdded = true;
		}
		if(filterString) {
			queryString += ((conditionalfilterAdded==true || stocklocationFilterAdded==true || urlconditionFilterAdded==true) ? " and " : "") + filterString;
			filterAdded = true;
		}
		let listurl = `${queryString}${((conditionalfilterAdded==true || stocklocationFilterAdded==true || urlconditionFilterAdded==true || filterAdded==true) ? " and " : "")}${this.state.pagejson.resourcename}.id<navurlfilter>&sortstring=${this.state.pagejson.resourcename}.id`;
		if(this.state.pagejson.sortstring) {
			queryString += "&sortstring="+this.state.pagejson.sortstring
		}

		axios.get(queryString)
		.then((res) => {
			if(res.data.message == 'success') {
				this.props.list.data = res.data.main;
				this.props.list.count = res.data.count;
				this.props.list.filter = {};
				this.props.list.filter.basicFilter = this.state.basicFilter;
				this.props.list.filter.advancedFilter = this.state.advancedFilter;
				this.props.list.filter.advancedFilterFields = this.state.advancedFilterFields;
				this.props.list.listurl = listurl;
				this.props.updateListState(this.state.title, this.props.list);
				this.props.updateAppState('listFilterObj', this.props.app.listFilterObj);
				this.updateLoaderFlag(false);
			} else {
				let apiResponse = commonMethods.apiResult(res);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getDataFn() {
		this.updateLoaderFlag(true);
		let currentview = this.props.list.currentview;
		let tempSkip = this.props.list.page * this.props.list.pagelength;
		let filterArray = [];
		let urllistConditionArray = [];

		let queryString = `/listapi2/${this.state.pagejson.resourcename}?length=${this.props.list.pagelength}&skip=${tempSkip}&fields=`;

		let idFound = currentview.viewjson.fields.indexOf('id') >= 0 ? true : false;
		let currencyFound = this.state.resource.fields['currencyid'] ? true : false;

		for(let i=0; i< currentview.viewjson.fields.length; i++) {
			let strFields = currentview.viewjson.fields[i].replace('/', '_');
			queryString += strFields+",";
		}

		if (!idFound)
			queryString += `id,`;
		if (currencyFound)
			queryString += `currencyid,`;

		queryString = queryString.substr(0,queryString.length-1);

		if(this.state.pagejson.includeinactive)
			queryString += `&includeinactive=true`;

		queryString += `&condition=`;

		if (this.props.list.conditionalfilterString && this.props.list.conditionalfilterString.length > 0)
			filterArray.push(...this.props.list.conditionalfilterString);

		if (this.state.pagejson.resourcename == 'stocklocations' && !this.props.app.feature.useSubLocations)
			filterArray.push({"field" : "parentid", "operator" : "isEmpty"});

		if (this.state.pagejson.urlconditionnew && this.state.pagejson.urlconditionnew.length > 0)
			filterArray.push(...this.state.pagejson.urlconditionnew);

		if (this.props.list.filterString && this.props.list.filterString.length > 0)
			filterArray.push(...this.props.list.filterString);

		urllistConditionArray.push({"field" : "id", "operator" : "<navurlfilter>", "value" : "<idvalue>"});

		if (filterArray.length > 0)
			urllistConditionArray.push(...filterArray);

		let urlqueryString = `${queryString}${encodeURIComponent(JSON.stringify(urllistConditionArray))}`;

		let listurl = `${urlqueryString}`;

		queryString += encodeURIComponent(JSON.stringify(filterArray));
		axios.get(queryString)
		.then((res) => {
			if(res.data.message == 'success') {
				this.props.list.data = res.data.main;
				this.props.list.count = res.data.count;
				this.props.list.filter = {};
				this.props.list.filter.basicFilter = this.state.basicFilter;
				this.props.list.filter.advancedFilter = this.state.advancedFilter;
				this.props.list.filter.advancedFilterFields = this.state.advancedFilterFields;
				this.props.list.filter.kanbansalesperson = this.state.kanbansalesperson;
				this.props.list.listurl = listurl;
				this.props.updateListState(this.state.title, this.props.list);
				this.props.updateAppState('listFilterObj', this.props.app.listFilterObj);
				this.updateLoaderFlag(false);
			} else {
				let apiResponse = commonMethods.apiResult(res);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	closeListViewModal() {
		this.setState({isModalopen: false});
	}

	listviewsOnChange(value) {
		if(value == 'Create') {
			let createListViewOptionCB = (viewobj) => {
				let tempObj = {
					currentview : {
						pagename : this.props.list.currentview.pagename,
						userids: [],
						roleids: [],
						isdefault: false,
						viewjson : viewobj ? viewobj : {
							basicfilter : [],
							condition : [],
							fields : []
						}
					},
					resourcename: this.state.pagejson.resourcename,
					listTitle: this.state.title
				};
				this.props.history.push({ pathname : '/createListView', params : tempObj });

				/*this.props.openModal({
					render: (closeModal) => {
						return <ListPageEditModal resourcename={this.state.pagejson.resourcename} currentview={currentview} openModal={this.props.openModal} closeModal={closeModal} callback={(res, deleteparam)=>{this.onViewUpdate(res, deleteparam)}} />
					}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
				});*/
			};
			this.props.openModal({
				render: (closeModal) => {
					return <CreateListViewOptionModal resourcename={this.state.pagejson.resourcename} pagejson={this.state.pagejson} openModal={this.props.openModal} closeModal={closeModal} callback={createListViewOptionCB} />
				}, className: {content: 'react-modal-custom-class-30', overlay: 'react-modal-overlay-custom-class'}
			});
		} else if (value == 'Edit') {

			let currentview =  this.props.list.currentview;
			let resourcename = this.state.pagejson.resourcename;
			let tempObj = {
				resourcename: this.state.pagejson.resourcename,
				listTitle: this.state.title
			};

			this.props.history.push({ pathname: `/details/views/${currentview.id}`, params : tempObj });

			/*this.props.openModal({
				render: (closeModal) => {
					return <ListPageEditModal resourcename={this.state.pagejson.resourcename} currentview={currentview} openModal={this.props.openModal} closeModal={closeModal} callback={(res, deleteparam)=>{this.onViewUpdate(res, deleteparam)}} />
				}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
			});*/
		} else {
			for(var i=0; i < this.state.pagejson.views.length; i++) {
				if(this.state.pagejson.views[i].id == value) {
					this.setState({
						currentviewid: value
					});
					this.props.list.currentview = this.state.pagejson.views[i];
					break;
				}
			}
			this.state.basicFilter = [];
			this.state.advancedFilterFields = [];
			this.state.advancedFilter = [];
			this.props.list.filterString = "";
			this.props.list.conditionalfilterString = "";
			this.props.list.filter = {};
			this.props.list.page = 0;
			this.state.isAdvanceFilterEnabled = false;
			this.props.updateListState(this.state.title, this.props.list);
			this.getFilterObj(this.props.list.currentview);
		}
	}

	/*onViewUpdate(result, deleteparam) {
		let itemFound = false;
		for(var i=0; i < this.state.pagejson.views.length; i++) {
			if(this.state.pagejson.views[i].id == result.id) {
				if(deleteparam) {
					this.state.pagejson.views.splice(i, 1);
					this.props.list.currentview = this.getView();
				} else {
					this.state.pagejson.views[i] = result;
					this.props.list.currentview = result;
				}
				itemFound = true;
				break;
			}
		}
		if(!itemFound) {
			var checkAccess = false;
			if(result.userids.length > 0 && result.userids.indexOf(this.props.app.user.id) == -1) {
				checkAccess = true;
			} else if(result.roleids.length > 0 && result.roleids.indexOf(this.props.app.user.roleid) == -1) {
				checkAccess = true;
			}
			if(!checkAccess) {
				this.state.pagejson.views.splice(this.state.pagejson.views.length-1, 0, result);
				this.props.list.currentview = result;
				this.props.list[this.state.title].currentview = this.state.pagejson.currentview;
			}
		}
		this.state.currentviewid = this.props.list.currentview.id;
		this.state.basicFilter = [];
		this.state.advancedFilterFields = [];
		this.state.advancedFilter = [];
		this.props.list.filterString = "";
		this.props.list.conditionalfilterString = "";
		this.props.list.filter = {};
		this.props.updateListState(this.state.title, this.props.list);
		this.getFilterObj(this.props.list.currentview);
	}*/

	changeView(param) {
		if(param == 'Calendar') {
			if(this.state.pagejson.resourcename != 'orders')
				this.props.history.push({pathname: "/activitycalendar", params: {
					activitiesfor: 'Sales Followup'
				}});
			else
				this.props.history.push("/ordercalendar");
		} else {
			this.props.list.listview = param;
			this.props.list.conditionalfilterString = "";
			this.props.updateListState(this.state.title, this.props.list);
		}
	}

	getOperators (item, emptyTheValue) {
		let itemFound = false;
		item.options = {};
		if (emptyTheValue) {
			item.filterValue = null;
			item.filterBetValue = null;
		}
		for (var i = 0; i < this.state.advancedFilterFields.length; i++) {
			let tempAdvFilter = this.state.advancedFilterFields[i];
			if (item.displayNameFilter == tempAdvFilter.displayNameFilter) {
				itemFound = true;
				item.resourceName = tempAdvFilter.resourceName;
				if (['autocomplete', 'selectasync', 'multiselectauto'].indexOf(tempAdvFilter.group) >= 0) {
					item.operatorOptions = filterOperators[tempAdvFilter.group];
				} else if(['date', 'integer'].indexOf(tempAdvFilter.type) >= 0) {
					item.operatorOptions = filterOperators[tempAdvFilter.type];
				} else if(tempAdvFilter.type == 'string' && tempAdvFilter.group == 'localselect') {
					item.operatorOptions = filterOperators.select;
				} else if(tempAdvFilter.type == 'string' && tempAdvFilter.group == 'simple') {
					item.operatorOptions = filterOperators.string;
				} else if(tempAdvFilter.type == 'boolean' && tempAdvFilter.group == 'simple') {
					item.operatorOptions = filterOperators.boolean;
				} else {
					item.operatorOptions = [];
				}
				item.options = this.state.advancedFilterFields[i];
				break;
			}
		}
		if (itemFound == false)
			item.options = null;
		this.setState({
			advancedFilter: this.state.advancedFilter,
			viewFilter: this.state.viewFilter,
			advFilterObj: this.state.advFilterObj
		});
	}

	insertAdvFilter(key) {
		let { advanceFilterIndexArr, showAdvanceFilterModal, advancedFilter } = this.state;
		if(key || key == 0) {
			advanceFilterIndexArr[key] = !advanceFilterIndexArr[key];
			advancedFilter = [...this.state.advancedFilter];
		} else {
			advanceFilterIndexArr[advanceFilterIndexArr.length-1] = !advanceFilterIndexArr[advanceFilterIndexArr.length-1];
			showAdvanceFilterModal = !showAdvanceFilterModal;
			advancedFilter = [...this.state.advancedFilter, this.state.advFilterObj];
		}

		this.setState({
			advancedFilter,
			advFilterObj: {},
			showAdvanceFilterModal,
			advanceFilterIndexArr
		}, () => {
			this.props.list.page = 0;
			this.advanceSearchFunction();
		});
	}

	removeAdvFilter(index, closeModalParam) {
		let { advanceFilterIndexArr } = this.state;
		if(closeModalParam)
			advanceFilterIndexArr[index] = !advanceFilterIndexArr[index];
		
		if(this.state.advancedFilter.length > 0)
			this.state.advancedFilter.splice(index, 1);
		this.setState({
			advancedFilter: this.state.advancedFilter,
			advanceFilterIndexArr
		}, () => {
			this.props.list.page = 0;
			this.advanceSearchFunction();
		});
	}

	operatorOnChange(item) {
		delete item.filterValue;
		delete item.filterBetValue;
		this.setState({
			advancedFilter: this.state.advancedFilter,
			viewFilter: this.state.viewFilter,
			advFilterObj: this.state.advFilterObj
		});
	}

	basicSearchFunction() {
		this.props.list.page = 0;
		this.searchFunction(this.state.basicFilter, false);
	}

	advanceSearchFunction() {
		/*this.state.isAdvanceFilterEnabled = false;
		this.setState({
			isAdvanceFilterEnabled: this.state.isAdvanceFilterEnabled
		});*/
		this.searchFunction(this.state.advancedFilter, false);
	}

	searchFunctionOld (filterArray, conditionalfilter, dontCallGetData) {
		if(filterArray.length == 0) {
			if(conditionalfilter)
				this.props.list.conditionalfilterString = '';
			else
				this.props.list.filterString = '';
			return this.getDataFn();
		}
		var filterString = '';
		for (var i = 0; i < filterArray.length; i++) {
			let parentJson = filterArray[i].resourceName ? filterArray[i].resourceName : this.state.pagejson.resourcename;

			let resourceNameIth = (this.props.app.myResources[parentJson] && this.props.app.myResources[parentJson].type == 'lookup') ? this.props.app.myResources[parentJson].lookupResource : parentJson;

			if (filterArray[i].operator == 'Equals' && filterArray[i].options) {
				if ((filterArray[i].options.type != 'boolean' && filterArray[i].filterValue) || (filterArray[i].options.type == 'boolean' && (filterArray[i].filterValue == true || filterArray[i].filterValue == false || filterArray[i].filterValue == 'true' || filterArray[i].filterValue == 'false'))) {
					if (filterArray[i].options.type == 'date') {
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "::date ='" + new Date(filterArray[i].filterValue).toDateString() + "'::date and ";
					} else {
						var tempValue = "";
						if (filterArray[i].options.type == 'boolean' && filterArray[i].options.group == 'simple') {
							tempValue = "";
							if (filterArray[i].filterValue == true || filterArray[i].filterValue == 'true') {
								tempValue = filterArray[i].filterValue;
							}
							if (filterArray[i].filterValue == false || filterArray[i].filterValue == 'false') {
								tempValue = filterArray[i].filterValue;
							}
						}

						if (filterArray[i].options.type == 'string') {
							tempValue = "'" + filterArray[i].filterValue.replace(/'/g,"''") + "'";
						}
						if (filterArray[i].options.type == 'boolean') {
							tempValue = "'" + filterArray[i].filterValue + "'";
						}
						if(filterArray[i].options.type == 'integer' && filterArray[i].options.group != 'simple') {
							tempValue = filterArray[i].filterValue;
						}
						
						if(filterArray[i].options.type == 'integer' && filterArray[i].options.group == 'simple') {
							filterString += "COALESCE(" + resourceNameIth + "." + filterArray[i].displayNameFilter + ", 0) = " + filterArray[i].filterValue + " and ";
						} else if (tempValue != "") {
							filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + " = " + tempValue + " and ";
						}
					}
				} else {
					if(filterArray[i].options.type == 'integer' && filterArray[i].options.group == 'simple' && filterArray[i].filterValue === 0)
						filterString += "COALESCE(" + resourceNameIth + "." + filterArray[i].displayNameFilter + ", 0) = 0 and ";
					else
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + " is null and ";
				}
			}
			if (filterArray[i].operator == 'Not Equals') {
				if ((filterArray[i].options.type != 'boolean' && filterArray[i].filterValue) || (filterArray[i].options.type == 'boolean' && (filterArray[i].filterValue == true || filterArray[i].filterValue == false || filterArray[i].filterValue == 'true' || filterArray[i].filterValue == 'false'))) {
					if (filterArray[i].options.type == 'date') {
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "::date <>'" + new Date(filterArray[i].filterValue).toDateString() + "'::date and ";
					} else {
						var tempValue = "";

						if (filterArray[i].options.type == 'boolean' && filterArray[i].options.group == 'simple') {
							tempValue = "";
							if (filterArray[i].filterValue == true || filterArray[i].filterValue == 'true') {
								tempValue = filterArray[i].filterValue;
							}
							if (filterArray[i].filterValue == false || filterArray[i].filterValue == 'false') {
								tempValue = filterArray[i].filterValue;
							}
						}

						if (filterArray[i].options.type == 'string') {
							tempValue = "'" + filterArray[i].filterValue.replace(/'/g,"''") + "'";
						}
						if (filterArray[i].options.type == 'boolean') {
							tempValue = "'" + filterArray[i].filterValue + "'";
						}
						if(filterArray[i].options.type == 'integer' && filterArray[i].options.group != 'simple') {
							tempValue = filterArray[i].filterValue;
						}
						
						if(filterArray[i].options.type == 'integer' && filterArray[i].options.group == 'simple') {
							filterString += "COALESCE(" + resourceNameIth + "." + filterArray[i].displayNameFilter + ", 0) <> " + filterArray[i].filterValue + " and ";
						} else if (tempValue != "") {
							filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "<>" + tempValue + " and ";
						}
					}
				} else {
					if(filterArray[i].options.type == 'integer' && filterArray[i].options.group == 'simple' && filterArray[i].filterValue === 0)
						filterString += "COALESCE(" + resourceNameIth + "." + filterArray[i].displayNameFilter + ", 0) <> 0 and ";
					else
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + " is not null and ";
				}
			}
			if (filterArray[i].operator == 'Like') {
				if (filterArray[i].filterValue) {
					filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + " ILIKE '%" + filterArray[i].filterValue.replace(/'/g,"''") + "%' and ";
				}
			}
			if (filterArray[i].operator == 'Is Empty') {
				filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + " IS NULL and ";
			}
			if (filterArray[i].operator == 'Is Not Empty') {
				filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + " IS NOT NULL and ";
			}
			if (filterArray[i].operator == 'More than') {
				if (filterArray[i].filterValue) {
					if (filterArray[i].options.type == 'date') {
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "::date >'" + new Date(filterArray[i].filterValue).toDateString() + "'::date and ";
					} else {
						filterString += "COALESCE(" + resourceNameIth + "." + filterArray[i].displayNameFilter + ", 0) >" + filterArray[i].filterValue + " and ";
					}
				}
			}
			if (filterArray[i].operator == 'Less than') {
				if (filterArray[i].filterValue) {
					if (filterArray[i].options.type == 'date') {
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "::date <'" + new Date(filterArray[i].filterValue).toDateString() + "'::date and ";
					} else {
						filterString += "COALESCE(" + resourceNameIth + "." + filterArray[i].displayNameFilter + ", 0) <" + filterArray[i].filterValue + " and ";
					}
				}
			}
			if (filterArray[i].operator == 'Between') {
				if (filterArray[i].filterValue) {
					if (filterArray[i].options.type == 'date') {
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "::date >='" + new Date(filterArray[i].filterValue).toDateString() + "'::date and ";
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "::date <='" + new Date(filterArray[i].filterBetValue).toDateString() + "'::date and ";
					} else {
						filterString += "COALESCE(" + resourceNameIth + "." + filterArray[i].displayNameFilter + ", 0) >=" + filterArray[i].filterValue + " and ";
						filterString += "COALESCE(" + resourceNameIth + "." + $filterArray[i].displayNameFilter + ", 0) <=" + filterArray[i].filterBetValue + " and ";
					}
				}
			}
			if (filterArray[i].operator == 'Less than or equal') {
				if (filterArray[i].filterValue) {
					if (filterArray[i].options.type == 'date') {
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "::date <='" + new Date(filterArray[i].filterValue).toDateString() + "'::date and ";
					} else {
						filterString += "COALESCE(" + resourceNameIth + "." + filterArray[i].displayNameFilter + ", 0) <=" + filterArray[i].filterValue + " and ";
					}
				}
			}
			if (filterArray[i].operator == 'More than or equal') {
				if (filterArray[i].filterValue) {
					if (filterArray[i].options.type == 'date') {
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "::date >='" + new Date(filterArray[i].filterValue).toDateString() + "'::date and ";
					} else {
						filterString += "COALESCE(" + resourceNameIth + "." + filterArray[i].displayNameFilter + ", 0) >=" + filterArray[i].filterValue + " and ";
					}
				}
			}
			if(filterArray[i].operator == 'In') {
				if (filterArray[i].filterValue) {
					if(filterArray[i].filterValue.length>0) {
						filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + " in (";
						for(var m=0;m < filterArray[i].filterValue.length;m++) {
							filterString += "'"+ filterArray[i].filterValue[m] +"',";
						}
						filterString = filterString.substr(0,filterString.length-1);
						filterString += ") and ";
					}
				}
			}
			if(!filterArray[i].operator) {
				if (filterArray[i].type == 'date' && filterArray[i].filterValue != null && filterArray[i].filterValue != '') {
					filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "::date='" + new Date(filterArray[i].filterValue).toDateString() + "'::date and ";
				} else {
					if (filterArray[i].filterValue || filterArray[i].type == 'boolean') {
						var tempValue = "";
						if (filterArray[i].type == 'string' && filterArray[i].group == 'simple') {
							tempValue = filterArray[i].filterValue;
							filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + " ILIKE '%" + tempValue.replace(/'/g,"''") + "%' and ";
						}
						if (filterArray[i].type == 'boolean') {
							if (filterArray[i].filterValue === true || filterArray[i].filterValue === false || filterArray[i].filterValue === "true" || filterArray[i].filterValue === "false") {
								filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + " =" + filterArray[i].filterValue + " and ";
							}
						}
						if (filterArray[i].type == 'string' && filterArray[i].group == 'localselect') {
							tempValue = filterArray[i].filterValue;
							filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "='" + tempValue.replace(/'/g,"''") + "' and ";
						}
						if (filterArray[i].type == 'integer') {
							tempValue = filterArray[i].filterValue;
							filterString += resourceNameIth + "." + filterArray[i].displayNameFilter + "=" + tempValue + " and ";
						}
					}
				}
			}
		}
		filterString = filterString.substr(0, filterString.length - 4);

		filterString = encodeURIComponent(filterString);
		if(conditionalfilter)
			this.props.list.conditionalfilterString = filterString;
		else
			this.props.list.filterString = filterString;

		if(!dontCallGetData) {
			this.props.updateListState(this.state.title, this.props.list);
			this.getDataFn();
		}
	}

	searchFunction (filterArray, conditionalfilter, dontCallGetData) {
		if(filterArray.length == 0) {
			if(conditionalfilter)
				this.props.list.conditionalfilterString = [];
			else
				this.props.list.filterString = [];
			return this.getDataFn();
		}

		let filterString = [];

		let filterOperatorObj = {
			"Equals": "equal",
			"Not Equals": "notEqual",
			"More than": "greaterThan",
			"Less than": "lessThan",
			"Less than or equal": "lesserOrEqual",
			"More than or equal": "greaterOrEqual",
			"In": "in",
			"Like": "contains",
			"Is Empty": "isEmpty",
			"Is Not Empty": "isNotEmpty"
		};

		filterArray.forEach((filter) => {
			if(!['Is Empty', 'Is Not Empty'].includes(filter.operator) && (filter.filterValue === null || filter.filterValue === "" || filter.filterValue === undefined || (Array.isArray(filter.filterValue) && filter.filterValue.length == 0)))
				return null;

			let operatorStr = "";
			let filterValue = filter.filterValue;
			let itemOptions = filter.options ? filter.options : filter;

			if(filter.operator)
				operatorStr = filterOperatorObj[filter.operator];
			else
				operatorStr = filter.type == 'string' && filter.group == 'simple' ? 'contains' : (filter.type == 'array' ? 'in' : 'equal');

			if(itemOptions.type == 'date')
				filterValue = new Date(filter.filterValue).toDateString();

			if(itemOptions.type == 'boolean')
				filterValue = filter.filterValue === true || filter.filterValue === 'true' ? true : false;

			if (filter.operator == 'Between' && itemOptions.type == 'date')
				filterString.push({
					field: filter.displayNameFilter,
					considerNullAsZero: null,
					operator: 'greaterOrEqual',
					value: filterValue
				}, {
					field: filter.displayNameFilter,
					considerNullAsZero: null,
					operator: 'lesserOrEqual',
					value: new Date(filter.filterBetValue).toDateString()
				});
			else
				filterString.push({
					field: filter.displayNameFilter,
					considerNullAsZero: (itemOptions.type == 'integer' && itemOptions.group == 'simple' && !['Is Empty', 'Is Not Empty'].includes(filter.operator)) ? true : null,
					operator: operatorStr,
					value: filterValue
				});
		});

		if (conditionalfilter)
			this.props.list.conditionalfilterString = filterString;
		else
			this.props.list.filterString = filterString;

		if (!dontCallGetData) {
			this.props.updateListState(this.state.title, this.props.list);
			this.getDataFn();
		}
	}

	advancedFunction() {
		this.state.isAdvanceFilterEnabled = !this.state.isAdvanceFilterEnabled;
		this.state.showAdvanceFilter = false;
		this.state.advanceFilterIndexArr = [];
		if(this.state.isAdvanceFilterEnabled) {
			for (var i = 0; i < this.state.basicFilter.length; i++) {
				this.state.basicFilter[i].filterValue = Array.isArray(this.state.basicFilter[i].filterValue) && this.state.basicFilter[i].filterValue.length == 0 ? null : this.state.basicFilter[i].filterValue;
				if (this.state.basicFilter[i].filterValue) {
					this.state.advancedFilter.push({
						index : this.state.advancedFilter.length,
						displayNameFilter : this.state.basicFilter[i].displayNameFilter,
						resourceName : this.state.basicFilter[i].resourceName,
						operator : this.state.basicFilter[i].type == 'string' && this.state.basicFilter[i].group == 'simple' ? 'Like' : (this.state.basicFilter[i].type == 'array' ? 'In' : 'Equals') ,
						filterValue : this.state.basicFilter[i].filterValue,
						filterDisplayValue: this.state.basicFilter[i].filterDisplayValue
					});
					this.getOperators(this.state.advancedFilter[this.state.advancedFilter.length - 1]);
					this.state.advanceFilterIndexArr.push(false);
				}
				this.state.basicFilter[i].filterValue = null;
			}
			this.state.showAdvanceFilter = this.state.advancedFilter.length > 0 ? true : false;
			this.setState({
				isAdvanceFilterEnabled: this.state.isAdvanceFilterEnabled,
				showAdvanceFilter: this.state.showAdvanceFilter,
				advanceFilterIndexArr: this.state.advanceFilterIndexArr,
				advFilterObj: {
					operator : 'Equals'
				}
			});
		} else {
			for (var i = 0; i < this.state.basicFilter.length; i++)
				this.state.basicFilter[i].filterValue = null;

			this.props.list.filterString = "";
			this.setState({
				isAdvanceFilterEnabled: this.state.isAdvanceFilterEnabled,
				showAdvanceFilter: this.state.showAdvanceFilter,
				basicFilter: this.state.basicFilter,
				advancedFilter: []
			}, () => {
				this.getDataFn();
			});
		}
	}

	showSelectAsyncFilter(item) {
		let journalMatching = {
			'Receipt Vouchers': 'receipt',
			'Payment Vouchers': 'payment',
			'Credit Notes': 'credit',
			'Debit Notes': 'debit',
			'Journal Vouchers': 'journalvouchers'
		};
		if((item.options && item.options.foreignKeyOptions.resource == 'numberingseriesmaster') || (item.foreignKeyOptions && item.foreignKeyOptions.resource == 'numberingseriesmaster')) {
			return `numberingseriesmaster.resource='${this.state.pagejson.resourcename}' ${this.state.pagejson.resourcename == 'journalvouchers' ? (journalMatching[this.state.pagejson.title] ? " and numberingseriesmaster.type = '"+journalMatching[this.state.pagejson.title]+"'" : ""): ""}`;
		}
		if((item.options && item.options.foreignKeyOptions.resource == 'employees') || (item.foreignKeyOptions && item.foreignKeyOptions.resource == 'employees')) {
			let tempitem = item.options ? item.options : item;
			if(tempitem.displayName == 'Engineer')
				return `employees.isengineer`;
		}
		return '';
	}

	/**Compare two array **/
	checkArray(arrayA, arrayB) {
		if (arrayB) {
			for (var i = 0; i < arrayA.length; i++) {
				var itemFound = false;
				for (var j = 0; j < arrayB.length; j++) {
					itemFound = (arrayA[i] == arrayB[j]) ? true : itemFound;
					if (itemFound) {
						return true;
					}
				}
			}
		} else {
			return true;
		}
	}

	renderTableCell(filterformat, classname, input, currencyid) {
		if(filterformat == 'currency')
			return (<div className="text-right nowrap">{currencyFilter(input, currencyid, this.props.app)}</div>);
		if(filterformat == 'percentage')
			return (<div className="text-right nowrap">{currencyFilter(input, 'percentage', this.props.app)}</div>);
		if(filterformat == 'date')
			return (<div className="text-center nowrap">{dateFilter(input)}</div>);
		if(filterformat == 'datetime')
			return (<div className="text-center nowrap">{datetimeFilter(input)}</div>);
		if(filterformat == 'time')
			return (<div className="text-center nowrap">{timeFilter(input)}</div>);
		if(filterformat == 'boolean')
			return (<div className="text-center nowrap">{booleanfilter(input)}</div>);
		if(filterformat == 'tax')
			return (<div className="text-center">{taxFilter(input, this.props.app.taxObject)}</div>);
		if(filterformat == 'deliveryreceiptfor')
			return (<div className="text-center">{deliveryreceiptforfilter(input)}</div>);
		if(filterformat == 'multiselectauto')
			return (<div className="text-center">{multiSelectAutoFilter(input)}</div>);
		if(filterformat == 'resource')
			return (<div className="text-left">{resourceFilter(input, this.props.app.myResources)}</div>);
		if(filterformat == 'itemmasterdisplayname')
			return (<div className="text-left">{itemmasterDisplaynamefilter(input, this.props.app)}</div>);
		return (<div className={classname}>{input}</div>);
	}

	renderTableRow(fieldArray, item) {
		return fieldArray.map((field, fieldkey) => {
				return (
					<td key={fieldkey} style={{width: `${field.width}px`}}>
						{this.renderTableCell(field.filterformat, field.className, (field.filterformat == 'multiselectauto') ? item[`text${field.field}`] : item[field.field.replace('/','_')], item.currencyid)}
					</td>
				);
		})
	}

	renderTbody(fieldArray, addExtraCell) {
		if(!this.props.list || !this.props.list.data)
			return null;

		return this.props.list.data.map((item, trkey) => {
			return (
				<tr key={trkey} onClick={() =>{this.tableRowClick(item)}}>
					{this.renderTableRow(fieldArray, item)}
					{addExtraCell ? (<td></td>) : null}
				</tr>
			);
		});
	}

	pagecountChange(count) {
		this.props.list.pagelength = count;
		this.props.list.page = 0;
		this.getDataFn();
	}

	handlePaginationClick(obj) {
		this.props.list.page = obj.selected;
		this.getDataFn();
	}

	tableRowClick(rowObj) {
		let url = this.state.pagejson.tableonclick.href.replace('<id>', rowObj.id);
		for(var prop in routes) {
			let matchedRoute = matchPath(url, {
				path: prop,
				exact: true,
				strict: false
			});
			if(matchedRoute) {
				if(routes[matchedRoute.path].pageoptions && routes[matchedRoute.path].pageoptions.title && routes[matchedRoute.path].pageoptions.type) {
					url += `?viewtype=book&redirecturl=${this.state.pagejson.tableonclick.href}&listurl=${encodeURIComponent(this.props.list.listurl)}`;
				}
				break;
			}
		}
		this.props.history.push(url);
	}

	renderTableHead(fieldArray) {
		return fieldArray.map((thData, thkey) => {
			return (
				<th key={thkey} className={`${thData.className}`} style={{width: `${thData.width}px`}}>
					{thData.displayName}
				</th>
			)
		});
	}

	getCellDisplayName(fieldName, resourceName) {
		let splitFieldName = fieldName.split('/');
		if(splitFieldName.length == 1) {
			if(this.state.resource.fields[splitFieldName[0]]) {
				return this.state.resource.fields[splitFieldName[0]].displayName;
			}
		} else if(this.state.resource.fields[splitFieldName[0]] && this.state.resource.fields[splitFieldName[0]].isForeignKey) {
			let foreignkeyResourceName = this.state.resource.fields[splitFieldName[0]].foreignKeyOptions.resource;
			if(splitFieldName[1] == this.state.resource.fields[splitFieldName[0]].foreignKeyOptions.mainField.split('.')[1]) {
				return this.state.resource.fields[splitFieldName[0]].displayName;
			} else {
				if(this.props.app.myResources[foreignkeyResourceName].fields[splitFieldName[1]]) {
					return this.state.resource.fields[splitFieldName[0]].displayName + "'s " + this.props.app.myResources[foreignkeyResourceName].fields[splitFieldName[1]].displayName;
				}
			}
		}
		return '';
	}

	renderFilter(item, key, forAdvFilter) {
		if(!item.options && forAdvFilter)
			return false;
		let type = forAdvFilter ? item.options.type : item.type;
		let group = forAdvFilter ? item.options.group : item.group;
		let localOptions = forAdvFilter ? item.options.localOptions : item.localOptions;
		let foreignKeyOptions = forAdvFilter ? item.options.foreignKeyOptions : item.foreignKeyOptions;
		let foreignKeyArrayOptions = forAdvFilter ? item.options.foreignKeyArrayOptions : item.foreignKeyArrayOptions
		let placeholder = forAdvFilter ? '' : `Search by ${item.displayName}`;
		let className = forAdvFilter ? (item.filterValue == null ? 'errorinput' : '') : '';
		let autoFocusFlag = forAdvFilter ? true : false;
		if(forAdvFilter && type == "string" && group == "localselect" && item.operator == "In") {
			return (
				<LocalSelect key={key} autoFocus={autoFocusFlag} options={item.options.localOptions} value={item.filterValue} onChange={(value) =>{this.filterInputChange(value, item)}} multiselect={true} />
			);
		}
		if(type == "string" && group == "localselect" && ['Is Empty', 'Is Not Empty'].indexOf(item.operator) == -1)
			return (
				<LocalSelect key={key} autoFocus={autoFocusFlag} options={localOptions} value={item.filterValue} placeholder={`${placeholder}`} onChange={(value) => this.filterInputChange(value, item)} />
			);

		if(type == "boolean" && group == "simple" && ['Is Empty', 'Is Not Empty'].indexOf(item.operator) == -1)
			return (
				<LocalSelect key={key} autoFocus={autoFocusFlag} options={[{id:'true', value:'True'}, {id:'false', value:'False'}]} label="value" valuename="id" value={item.filterValue} placeholder={`${placeholder}`} onChange={(value) => this.filterInputChange(value, item)} />
			);

		if(type == "integer" && group == "selectasync" && ['Is Empty', 'Is Not Empty'].indexOf(item.operator) == -1) {
			var tempOptions = {
				"resource": foreignKeyOptions.resource,
				"fields": "id,"+foreignKeyOptions.mainField.split('.')[1],
				"label": foreignKeyOptions.mainField.split('.')[1]
			};
			return (
				<SelectAsync key={key} autoFocus={autoFocusFlag} value={item.filterValue} resource={tempOptions.resource} fields={tempOptions.fields} label={tempOptions.label} placeholder={`${placeholder}`} filter={this.showSelectAsyncFilter(item)}  onChange={(value, valueObj) => this.filterInputChange(value, item, valueObj)} />
			);
		}

		if(type == "integer" && group == "autocomplete" && ['Is Empty', 'Is Not Empty'].indexOf(item.operator) == -1) {
			var tempOptions = {
				"resource": foreignKeyOptions.resource,
				"fields": "id,displayname," + foreignKeyOptions.mainField.split('.')[1],
				"label": foreignKeyOptions.resource == 'itemmaster' || foreignKeyOptions.resource=='partners' || foreignKeyOptions.resource=='projects' ? 'displayname' : foreignKeyOptions.mainField.split('.')[1],
				"filter": null,
				"displaylabel": foreignKeyOptions.mainField.split('.')[1]
			};
			return (
				<AutoSelect key={key} autoFocus={autoFocusFlag} value={item.filterValue} resource={tempOptions.resource} fields={tempOptions.fields} label={tempOptions.label} filter={tempOptions.filter} placeholder={`${placeholder}`} onChange={(value, valueObj) => this.filterInputChange(value, item, valueObj)}/>
			);
		}

		if(type == "array" && group == "multiselectauto" && ['Is Empty', 'Is Not Empty'].indexOf(item.operator) == -1) {
			var tempOptions = {
				"resource": foreignKeyArrayOptions.resource,
				"fields": "id,displayname," + foreignKeyArrayOptions.mainField,
				"label": foreignKeyArrayOptions.resource == 'itemmaster' || foreignKeyArrayOptions.resource=='partners' ? 'displayname' : foreignKeyArrayOptions.mainField,
				"filter": null,
				"displaylabel": foreignKeyArrayOptions.mainField
			};
			return (
				<AutoMultiSelect key={key} autoFocus={autoFocusFlag} value={item.filterValue} resource={tempOptions.resource} fields={tempOptions.fields} label={tempOptions.label} filter={tempOptions.filter} placeholder={`${placeholder}`} onChange={(value, valueObj) => this.filterInputChange(value, item, valueObj)}/>
			);
		}
		if(type == "integer" && group == "simple" && ['Is Empty', 'Is Not Empty'].indexOf(item.operator) == -1)
			return (
				<NumberElement autoFocus={autoFocusFlag} className={`form-control`} placeholder={placeholder} value={item.filterValue} onChange={(value) => this.filterInputChange(value, item)} />
			);

		if(type == "string" && group == "simple" && ['Is Empty', 'Is Not Empty'].indexOf(item.operator) == -1)
			return (
				<input type="text"  autoFocus={autoFocusFlag} className={`form-control`} key={key} value={item.filterValue || ''} placeholder={`${placeholder}`} onChange={(event) => this.filterInputChange(event.target.value, item)} />
			);

		if(type == "date" && ['Is Empty', 'Is Not Empty'].indexOf(item.operator) == -1)
			return (
				<DateElement key={key} autoFocus={autoFocusFlag} className={`form-control`} value={item.filterValue} onChange={(value) => this.filterInputChange(value, item)} placeholder={`${placeholder}`} />
			);

		if(type == "string" && group == "resource" && ['Is Empty', 'Is Not Empty'].indexOf(item.operator) == -1)
			return (
				<LocalSelect key={key} autoFocus={autoFocusFlag} options={this.state.resourceArray} value={item.filterValue} placeholder={`${placeholder}`} onChange={(value) => this.filterInputChange(value, item)} />
			);
	}

	renderBetweenFilter(item, key) {
		if(!item.options || item.operator != "Between")
			return false;
		let className = item.filterBetValue == null ? 'errorinput' : '';
		if(item.options.type == "date" && item.operator == "Between") {
			return (
				<div className="col-md-12">
					<DateElement key={key} className={`form-control`} value={item.filterBetValue} onChange={(value) => this.filterBetweenInputChange(value, item)} />
				</div>
			);
		}
		if(item.options.type == "integer" && item.options.group == "simple" && item.operator == "Between") {
			return (
				<div className="col-md-12">
					<input type="number" step="any" className="form-control" key={key} value={item.filterBetValue} placeholder={`Enter ${item.options.displayName}`}  onChange={(event) =>{this.filterBetweenInputChange(event.target.value, item);}}/>
				</div>
			);
		}
	}
	
	renderAdvancedFilterContainer() {
		if(this.state.showAdvanceFilter)
			return this.renderAdvancedFilterField();
		else
			return (
				 <button type="button" className="btn btn-sm gs-dotted-border" onClick={this.addAdvancedFilterfn}><i className="fa fa-plus"></i>Add Filter</button>
			);
	}
	
	addAdvancedFilterfn() {
		this.setState({
			showAdvanceFilter : !this.state.showAdvanceFilter
		});
	}
	
	renderAdvancedFilterField() {
		let item = this.state.advFilterObj;
		return (		
			<div className="col-md-3 col-sm-3 col-xs-9" style={{display: 'inline-block'}}>
				<div className={`${this.state.showAdvanceFilterModal ? "gs-list-advanced-filter-modal" : ""}`}>
					{!this.state.showAdvanceFilterModal ? <LocalSelect autoFocus={true} options={this.state.advancedFilterFields} valuename="displayNameFilter" label="displayName" value={item.displayNameFilter} onChange={(value, valueobj) =>{item.displayNameFilter=value;item.operator=(valueobj.group == 'multiselectauto' ? 'In' : 'Equals');this.getOperators(item, true);this.addAdvancedfilterField();}} placeholder="Select Field" className={`select-control-no-border select-hide-select-arrow ${item.displayNameFilter ? '' : ''}`} /> : this.renderAdvancedFilterModal()}
				</div>
			</div>
		);
	}
	
	addAdvancedfilterField() {
		let { advanceFilterIndexArr } = this.state;
		advanceFilterIndexArr.forEach((item, index) => {
 			advanceFilterIndexArr[index] = false;
 		});
		advanceFilterIndexArr.push(!this.state.showAdvanceFilterModal);

		this.setState({
			advFilterObj: this.state.advFilterObj,
			showAdvanceFilterModal : !this.state.showAdvanceFilterModal,
			advanceFilterIndexArr
		});
	}
	
	renderAdvancedFilterModal(filter, key) {
		let item = filter ? filter : this.state.advFilterObj;
		return (
			<form style={{padding: '10px 0px'}} ref="advancefilterform" onSubmit={(event)=>{event.preventDefault();this.insertAdvFilter(key)}}>
				<div className="form-group col-md-12" style={{borderBottom: '1px solid #ddd', paddingBottom: '10px'}}><span>{item.options.displayName}</span><span style={{width: '17px', fontSize: '18px', float: 'right', color: '#999', cursor: 'pointer'}} onClick={() => {this.closeAdvFilterModal(key)}}>×</span></div>
				<div className="form-group col-md-12">
					<LocalSelect options={item.operatorOptions} value={item.operator} onChange={(value) => {item.operator=value;this.operatorOnChange(item);this.handleEnter()}} placeholder="Select operator"  className={`select-filter-operator-color select-control-no-border ${item.operator ? '' : (item.displayNameFilter ? 'errorinput' : '')}`}/>
				</div>
				{item.options ? <div className="form-group col-md-12">
					{this.renderFilter(item, null, true)}
				</div> : null}
				<div className="form-group">
					{this.renderBetweenFilter(item, null)}
				</div>
				<div className="col-md-12">
					<button type="submit" className="btn btn-sm btn-block gs-btn-success" disabled={this.validateAdvFilter(filter)}>Apply</button>
				</div>
			</form>
		);
	}

	handleEnter() {
		const form = this.refs.advancefilterform;
		setTimeout(()=> {
			form.elements[1].focus();
		}, 0);
	}
	
	closeAdvFilterModal(index) {
		let { advanceFilterIndexArr } = this.state;
		if(index || index == 0)
			advanceFilterIndexArr[index] = !advanceFilterIndexArr[index];

		this.setState({
			advFilterObj: {},
			advanceFilterIndexArr,
			showAdvanceFilterModal: false
		});
	}

	renderAdvancedFilter() {
		let item = this.state.advFilterObj;
		return (
			<div className="row">
				<div className="col-md-3 col-sm-3 col-xs-9">
					<LocalSelect options={this.state.advancedFilterFields} valuename="displayNameFilter" label="displayName" value={item.displayNameFilter} onChange={(value) =>{item.displayNameFilter=value;this.getOperators(item, true)}} placeholder="Select Field" className={`${item.displayNameFilter ? '' : ''}`} />
				</div>
				<div className="col-md-3 col-sm-3 col-xs-12">
					<LocalSelect options={item.operatorOptions} value={item.operator} onChange={(value) => {item.operator=value;this.operatorOnChange(item)}} placeholder="Select operator"  className={`${item.operator ? '' : (item.displayNameFilter ? 'errorinput' : '')}`}/>
				</div>
				{item.options ? <div className="col-md-3 col-sm-3 col-xs-12">
					{this.renderFilter(item, null, true)}
				</div> : null}
				{this.renderBetweenFilter(item, null)}
				<button type="button" onClick={() => {this.insertAdvFilter(item)}} className="btn btn-sm gs-btn-success"  rel="tooltip" title="Add" disabled={this.validateAdvFilter()}>Add</button>
			</div>
		);
	}

	validateAdvFilter(filter) {
		let validateObj = filter ? filter : this.state.advFilterObj;

		if(validateObj.displayNameFilter == null || validateObj.operator == null)
			return true;
		if(['Is Empty', 'Is Not Empty'].indexOf(validateObj.operator) == -1 && validateObj.filterValue == null)
			return true;
		if(validateObj.operator == 'Between' && !validateObj.filterBetValue)
			return true;
		return false;
	}

	filterInputChange(value, item, valueObj) {
		const form = this.refs.advancefilterform;
		item.filterValue = value;
		if(item && item.options &&  ['selectasync', 'autocomplete'].indexOf(item.options.group) >= 0)
			item.filterDisplayValue = valueObj[item.options.foreignKeyOptions.mainField.split('.')[1]];

		if(['selectasync', 'autocomplete'].indexOf(item.group) >= 0)
			item.filterDisplayValue = valueObj[item.foreignKeyOptions.mainField.split('.')[1]];

		if((item && item.options && item.options.group == 'multiselectauto') || (item && item.group == 'multiselectauto')) {
			let tempforeignKeyArrayOptions = item.options ? item.options.foreignKeyArrayOptions : item.foreignKeyArrayOptions;
			item.filterDisplayValue = valueObj.map(a => a[tempforeignKeyArrayOptions.mainField]).join(', ');
		}

		this.setState({
			basicFilter: this.state.basicFilter,
			advancedFilter: this.state.advancedFilter,
			advFilterObj: this.state.advFilterObj
		}, () => {
			if(form) {
				if(item.operator == 'Between') {
					form.elements[form.elements.length-2].focus();
				} else if(item.options.group != 'simple') {
					form.elements[form.elements.length-1].focus();
				}
			}
		});
	}

	filterBetweenInputChange(value, item) {
		const form = this.refs.advancefilterform;
		item.filterBetValue = value;
		this.setState({
			basicFilter: this.state.basicFilter,
			advancedFilter: this.state.advancedFilter,
			advFilterObj: this.state.advFilterObj
		}, () => {
			form.elements[form.elements.length-1].focus();
		});
	}

	handleCloseModal () {
		this.setState({
			showListvieweditModal: false
		});
	}

	reset() {
		for (var i = 0; i < this.state.basicFilter.length; i++)
			this.state.basicFilter[i].filterValue = null;
		this.props.list.filterString = "";
		this.setState({
			basicFilter: this.state.basicFilter,
			advancedFilter: [],
			isAdvanceFilterEnabled: false
		});
		this.getDataFn();
	}

	advanceFilterReset() {
		for (var i = 0; i < this.state.basicFilter.length; i++)
			this.state.basicFilter[i].filterValue = null;

		this.props.list.filterString = "";
		this.setState({
			basicFilter: this.state.basicFilter,
			advancedFilter: []
		});
		this.getDataFn();	
	}

	download() {
		this.updateLoaderFlag(true);
		var reportData = [[{v : ''}, {
				v : 'Report Name'
			}, {
				v : this.state.pagejson.title
			}
		],
		[],
		[{
				v : ''
			}, {
				v : 'Company Name'
			}, {
				v : this.props.app.selectedCompanyDetails.legalname
			}
		],
		[],
		[]];
		var widthArray = [];
		var thData = $(".gs-list-table tr")[0].children;
		for (var i = 0; i < thData.length; i++) {
			reportData[4][i] = {
				v : thData[i].innerHTML
			};
			widthArray.push({
				wch : $(thData[i]).width()/6
			});
		}

		for (var i = 1; i < $(".gs-list-table tr").length; i++) {
			var tempArrData = [];
			for (var j = 0; j < $(".gs-list-table tr")[i].children.length; j++) {
				var dataNode = $(".gs-list-table tr")[i].children[j];
				var data = $(dataNode).text();
				for(var prop in this.props.app.currency){
					if(data.indexOf(this.props.app.currency[prop].symbol) >= 0){
						var tempSplitData = data.replace(this.props.app.currency[prop].symbol, '');
						var tempSplitData1 = tempSplitData.split(' ').join('');
						var tempData = tempSplitData1.split(',').join('');

						if(!isNaN(Number(tempData))){
							data = tempData;
						}
						break;
					}
				}
				var dataCSV = data.trim();
				if (dataCSV != ''  && dataCSV != null && !isNaN(Number(dataCSV))) {
					dataCSV = Number(dataCSV)
				}
				tempArrData.push({
					v : dataCSV
				});
			}
			reportData.push(tempArrData);
		}

		var wb = {
			SheetNames : [],
			Sheets : {}
		};
		var ws = excel_sheet_from_array_of_arrays(reportData);
		let sheetName = this.state.pagejson.title.replace(/[^A-Za-z0-9_ ]/g, '');
		wb.SheetNames.push(sheetName);
		wb.Sheets[sheetName] = ws;
		ws['!cols'] = widthArray;
		//ws['!merges'] = [{"s": {"c": 1, "r": 5 },"e": {"c": 5, "r": 5 }},{"s": {"c": 1, "r": 6 },"e": {"c": 5, "r": 6 }}];
		var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
		saveAs(new Blob([excel_s2ab(wbout)],{type:"application/octet-stream"}), this.state.pagejson.title + "_" + (dateFilter(new Date())) + ".xlsx");
		this.updateLoaderFlag(false);
	}

	renderTitleBar() {
		return (
			<div className="row">
				<div className="col-md-7" style={{display:'inline-block',marginTop: '10px',marginBottom: '10px',verticalAlign: 'middle',lineHeight: '2'}}>
					<div className="row">
						<div className="col-md-12">
							<div style={{float: 'left', paddingLeft: '10px'}}>
								<label className="gs-text-color gs-uppercase semi-bold-custom" style={{fontSize: '16px', marginRight: '15px'}}>{this.state.pagejson.title}</label>
							</div>
							{this.renderViewSelector()}
						</div>
						<div className="col-md-12">
							<div className="row no-gutters">
								{this.renderFilterTags(this.state.viewFilter)}
							</div>
						</div>
					</div>
				</div>
				<div className="col-md-5 margintop-10 marginbottom-10">
					<div className="row">
						<div className="col-md-6 col-sm-6 col-6 text-right gs-text-left-xs">
							{this.renderMultipleViews()}
						</div>
						<div className="col-md-6 col-sm-6 col-6 text-right">
							{this.renderAddButton()}
						</div>
					</div>
				</div>
			</div>
		);
	}

	kanbansalespersonOnChange = (value) => {
		this.setState({kanbansalesperson: value});
		this.props.list.filter.kanbansalesperson = value;
		this.props.updateListState(this.state.title, this.props.list);
	} 

	renderViewSelector() {
		let downloadaccessClass = checkActionVerbAccess(this.props.app, this.state.pagejson.resourcename, 'Download') ? '' : 'hide';

		if(this.state.pagejson.kanbanView && this.props.list.listview == 'Kanban')
			return (
				<div className="float-left gs-list-header-view-wrapper d-flex flex-row align-items-center">
					<div style={{width: '30%', textAlign: 'center'}}><span>Sales Person</span></div>
					<div style={{width: '70%'}}>
						<AutoMultiSelect value={this.state.kanbansalesperson} resource={'users'} fields={'id,displayname'} label={'displayname'} filter={'users.issalesperson=true'} onChange={this.kanbansalespersonOnChange} multiselect={true} required={true}/>
					</div>
				</div>
			);

		return (
			<div className="float-left gs-list-header-view-wrapper">
				<div style={{float: 'left', width: '70%'}}>
					<LocalSelect options={[...this.state.pagejson.views, {id:'Create', viewname:'Create View...'}]} label='viewname' valuename='id' value={this.state.currentviewid} onChange={(value) => {this.listviewsOnChange(value)}} clearable={false} required={true} />
				</div>
				<div style={{float: 'left', marginLeft: '15px'}}>
					<button type="button" className="btn btn-sm gs-form-btn-success" onClick={() =>{this.listviewsOnChange('Edit')}}><span className="fa fa-edit"></span></button>
				</div>
				{this.props.list && this.props.list.data && this.props.list.data.length ? <div style={{float: 'left', marginLeft: '10px'}}>
					<button type="button" className={`btn btn-sm gs-form-btn-success ${downloadaccessClass}`} onClick={() =>{this.download()}}><span className="fa fa-download"></span></button>
				</div> : null}
			</div>
		)
	}

	renderMultipleViews() {
		if(this.props.list.viewsArray.length > 1) {
			return (
				<ul className="list-pagination">
					{this.props.list.viewsArray.map((viewValue, viewKey) => 
						<li key={viewKey} className={`${viewValue == this.props.list.listview ? 'active' : ''}`} onClick={() =>{this.changeView(viewValue)}}><a><span className={`fa fa-${viewValue == 'List' ? 'th-list' : (viewValue == 'Calendar' ? 'calendar' : (viewValue == 'Tree' ? 'tree' : 'th-large'))}`}></span></a></li>
					)}
				</ul>
			);
		}
		return null;
	}

	renderAddButton() {
		let createButtonName = this.state.pagejson.createbuttonname ? this.state.pagejson.createbuttonname : this.state.pagejson.title;

		if(this.state.pagejson.addbuttonoptions) {
			return <Link to={`/${this.state.pagejson.addbuttonoptions.href}`} className="btn btn-sm gs-btn-success gs-list-add-btn"><span className="fa fa-plus marginright-5"></span>{createButtonName}</Link>
		}
	}

	renderBasicFilter() {
		return this.state.basicFilter.map((basicfilter, basicfilterkey) => {
			return <div key={basicfilterkey} rel="tooltip" title={`Search by ${basicfilter.displayName}`} className="col-md-3 paddingLeft gs-marginbottom-10-xs">{this.renderFilter(basicfilter, basicfilterkey)}</div>
		});
	}

	renderPageCounts() {
		if(!this.props.list || !this.props.list.data)
			return null;
		return pageLengths.map((count, btnkey) => {
			return <button type="button" className={`btn btn-sm gs-btn-light ${this.props.list.pagelength == count ? 'active' : ''}`} key={btnkey} onClick={() =>{this.pagecountChange(count)}}>{count}</button>
		});
	}

	renderPagination() {
		if(this.props.list && Math.ceil(this.props.list.count / this.props.list.pagelength) > 1)
			return <ReactPaginate previousLabel={<span>&laquo;</span>}
				nextLabel={<span>&raquo;</span>}
				breakLabel={<a></a>}
				breakClassName={"break-me"}
				pageCount={Math.ceil(this.props.list.count / this.props.list.pagelength)}
				marginPagesDisplayed={0}
				pageRangeDisplayed={5}
				onPageChange={this.handlePaginationClick}
				containerClassName={"list-pagination"}
				subContainerClassName={"pages pagination"}
				activeClassName={"active"}
				forcePage={this.props.list.page} />
		return null;
	}

	renderFilterTags(filterArray, deleteCB) {
		return filterArray.map((filter, key) => {
			var displayValue = filter.filterValue;
			var displayBetValue = filter.filterBetValue;
			if(filter.options && filter.options.type == 'date') {
				displayValue = displayValue ? moment(displayValue).format("DD-MMM-YYYY") : displayValue;
				displayBetValue = displayBetValue ? moment(displayBetValue).format("DD-MMM-YYYY") : displayBetValue;
			}
			if(filter.operator == 'In')
				displayValue = (displayValue) ? displayValue.join(', ') : displayValue;
			if(['Is Empty', 'Is Not Empty'].indexOf(filter.operator) >= 0)
				displayValue = '';

			if(filter.options && filter.options.type == 'integer' && filter.options.group == 'selectasync' && filter.filterDisplayValue)
				displayValue = filter.filterDisplayValue;
			if(filter.options && filter.options.type == 'integer' && filter.options.group == 'autocomplete' && filter.filterDisplayValue)
				displayValue = filter.filterDisplayValue;
			if(filter.options && filter.options.type == 'array' && filter.options.group == 'multiselectauto' && filter.filterDisplayValue)
				displayValue = filter.filterDisplayValue;

			let description = `${filter.options ? filter.options.displayName : filter.displayNameFilter} ${operatorSymbols[filter.operator]} ${displayValue}`;
			if(filter.operator == 'Between')
				description = `${description} and ${displayBetValue}`;

			return (
				<div key={key} style={{position: 'relative', width: 'auto'}}>
					<div className="list-filter-card">
						{deleteCB ? <span onClick={() => deleteCB(key)} className="list-filter-card-close-icon">x</span> : null}
						{deleteCB ? <span className="list-filter-card-content" title={`${description}`}  onClick={()=> this.openFilterTagEditModal(key)}>{description}</span> : <span className="list-filter-card-content" title={`${description}`}>{description}</span>}
					</div>
					{this.state.advanceFilterIndexArr[key] && deleteCB ? <div className="gs-list-advanced-filter-modal gs-list-advanced-filter-modal-hasvalue">{this.renderAdvancedFilterModal(filter, key)}</div>: null}
				</div>
			);
		});
	}
	
	openFilterTagEditModal(key) {
		let { advanceFilterIndexArr } = this.state;
		advanceFilterIndexArr.forEach((item, index) => {
			advanceFilterIndexArr[index] = false;
		});
		advanceFilterIndexArr[key] = !advanceFilterIndexArr[key];
		this.setState({
			advanceFilterIndexArr
		});
	}

	listbodyRef(node) {
		if(node) {
			node.addEventListener('scroll', (evt) => {
				this.refs.listheader.scrollLeft = evt.srcElement.scrollLeft;
			})
		}
	}

	render() {
		let TotalPageLength;

		if(this.props.list.data) {
			if(this.props.list.pagelength == this.props.list.data.length)
				TotalPageLength = `${Math.round(this.props.list.pagelength * this.props.list.page)+1} - ${this.props.list.pagelength * (this.props.list.page+1)} of ${this.props.list.count}`;
			else
				TotalPageLength = `${Math.round(this.props.list.pagelength * this.props.list.page)+1} - ${this.props.list.count} of ${this.props.list.count}`;
		}

		if(!this.props.list || !this.props.list.currentview)
			return <Loadingcontainer isloading={true}></Loadingcontainer>

		let tableWidth = Math.ceil(($(window).width() - 32) / 2);
		let tableWidth2 = Math.ceil((($(window).width() - 32) * 3) / 4);
		let tablecellWidth = 0;
		let fieldArray = [];
		this.props.list.currentview.viewjson.fields.forEach((thData, thkey) => {
			let field = thData.split('/').length == 1 ? thData.split('/')[0] : thData.split('/')[1];
			let fieldName = thData.split('/')[0];
			if(!this.state.resource.fields[fieldName]) {
				return null;
			}
			let joinFieldName = thData.split('/').length == 1 ? null : thData.split('/')[1];
			if(joinFieldName && !this.state.resource.fields[fieldName].foreignKeyOptions) {
				return null;
			}
			let joinFieldResource = thData.split('/').length == 1 ? null : this.props.app.myResources[this.state.resource.fields[thData.split('/')[0]].foreignKeyOptions.resource];

			if(joinFieldName && !joinFieldResource.fields[joinFieldName]) {
				return null;
			}

			let resource = thData.split('/').length == 1 ? this.state.resource : this.props.app.myResources[this.state.resource.fields[thData.split('/')[0]].foreignKeyOptions.resource];
			let longArray = ["outcome", "paymentterms", "addonhelptext", "finaltotalinwords", "outcomereason", "subject", "suspendremarks", "resolution", "reopenreason", "ratingremarks", "pendingreason", "amountinwords", "amountinwordslc", "msgid", "errordetails", "reason", "rejectionreason"];
			let shortArray = ["hsncodeid", "defaultuomid", "purchaseuomid", "salesuomid", "stockuomid", "uomgroupid", "uomid", "loginstatus", "gsmobileappstatus", "trackerstatus", "trackerfrequency", "gstregtype", "indiamartpriority", "title", "type", "postalcode", "pincode", "symbol", "symbolposition", "fractionname", "gstuqc", "icontext", "bloodgroup", "itemtype", "roundoffmethod", "priority", "contractstatus", "servicereportstatus", "calltype", "severity", "customergstregtype", "equipmentwizardstatus", "inorout", "timespent", "firsttimesyncstatus", "firsttimesyncduration", "addordeduct", "target", "statutorytype", "calculationtype", "completedon", "periodtype", "duration"];

			let tempObj = {
				field: thData,
				filterformat: resource.fields[field].filterformat,
				className: 'text-left',
				displayName: this.getCellDisplayName(thData, this.state.pagejson.resourcename),
				width: longArray.indexOf(field) > -1 ? 300 : (shortArray.indexOf(field) > -1 ? 100 : 200)
			};
			if(resource.fields[field].filterformat == 'currency') {
				tempObj.className = 'text-right';
				tempObj.width = 150;
			}
			if(resource.fields[field].filterformat == 'percentage') {
				tempObj.className = 'text-right';
				tempObj.width = 130;
			}
			if(resource.fields[field].filterformat == 'date' ||resource.fields[field].filterformat == 'time' ||resource.fields[field].filterformat == 'boolean') {
				tempObj.className = 'text-center';
				tempObj.width = 130;
			}
			if(resource.fields[field].filterformat == 'datetime') {
				tempObj.className = 'text-center';
				tempObj.width = 180;
			}
			if(resource.fields[field].filterformat == 'deliveryreceiptfor' || resource.fields[field].filterformat == 'tax' || resource.fields[field].filterformat == 'multiselectauto') {
				tempObj.className = 'text-center';
				tempObj.width = 200;
			}
			if(field == 'id') {
				tempObj.className = 'text-center';
				tempObj.width = 100;
			}
			if(field == 'status') {
				tempObj.className = 'text-center';
				tempObj.width = 200;
			}
			tablecellWidth += tempObj.width;
			fieldArray.push(tempObj);
		});
		let responsiveWidth = '100%', addExtraCell = false;
		if(tableWidth > tablecellWidth)
			addExtraCell = true;
		else if(tableWidth2 > tablecellWidth)
			addExtraCell = true;

		let tbody_div_style = {
			width: `${responsiveWidth}`
		};
		if($(window).outerHeight() - $('.list-title-bar').outerHeight() - ($('.user-license-container').length > 0 ? $('.user-license-container').outerHeight() + 10 : 0) - $('.navbar').outerHeight() - $('.list-filter-bar').outerHeight() - $('.list-footer-bar').outerHeight() - $('.gs-list-table-header-container').outerHeight() - 44 > 300) {
			tbody_div_style.height = `${$(window).outerHeight() - $('.list-title-bar').outerHeight() - ($('.user-license-container').length > 0 ? $('.user-license-container').outerHeight() + 10 : 0) - $('.navbar').outerHeight() - $('.list-filter-bar').outerHeight() - $('.list-footer-bar').outerHeight() - $('.gs-list-table-header-container').outerHeight() - 44}px`;
			tbody_div_style.maxHeight = `${$(window).outerHeight() - $('.list-title-bar').outerHeight() - ($('.user-license-container').length > 0 ? $('.user-license-container').outerHeight() + 10 : 0) - $('.navbar').outerHeight() - $('.list-filter-bar').outerHeight() - $('.list-footer-bar').outerHeight() - $('.gs-list-table-header-container').outerHeight() - 44}px`;
		}

		let createButtonName = this.state.pagejson.createbuttonname ? this.state.pagejson.createbuttonname : this.state.pagejson.title;

		return(
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="col-md-12 marginbottom-10 bg-white list-title-bar" style={{borderBottom: '1px solid #DEE2E6'}}>
						{this.renderTitleBar()}
					</div>
					<div className={`col-md-12 ${(this.props.list.listview == 'List') ? 'showsection' : 'hidesection'}`}>
						{this.state.pagejson.resourcename == 'users' && Object.keys(this.state.userlicensedetails).length > 0 ? <div className="col-md-12 marginbottom-10 bg-white user-license-container">
							<div className="row">
								<div className="col-lg-2 col-md-12 col-sm-12 gs-user-details-inner-div gs-text-color font-12 text-center semi-bold-custom d-flex align-items-center justify-content-center">
									LICENSE DETAILS
								</div>
								<div className="col-lg-10 col-md-12 col-sm-12">
									<div className="row">
										<div className="col-lg-4 col-md-12 gs-user-details-inner-div">
											<div className="row">
												<div className="col-lg-10 offset-lg-1 col-md-12 col-sm-12 d-flex flex-row justify-content-between ">
													<div><img src="./images/gs_logo.svg" alt="GrowSmart Logo" className="marginright-5"/>GrowSmart</div>
													<div><span className="font-11 marginright-5">TOTAL</span> <span className="badge gs-form-badge-primary font-14">{this.state.userlicensedetails.user.authorized}</span></div>
													<div><span className="font-11 marginright-5">AVAILABLE</span> <span className="badge gs-form-badge-success font-14">{this.state.userlicensedetails.user.available}</span></div>
												</div>
											</div>
										</div>
										<div className="col-lg-4 col-md-12 gs-user-details-inner-div">
											<div className="row">
												<div className="col-lg-10 offset-lg-1 col-md-12 col-sm-12 d-flex flex-row justify-content-between ">
													<div className="d-flex flex-row align-items-center"><div className="d-flex marginright-5"><img src="./images/ismart_logo.svg" alt="iSmart Logo" style={{width:'13px', height: '13px'}}/></div>i'Smart</div>
													<div><span className="font-11 marginright-5">TOTAL</span> <span className="badge gs-form-badge-primary font-14">{this.state.userlicensedetails.ess.authorized}</span></div>
													<div><span className="font-11 marginright-5">AVAILABLE</span> <span className="badge gs-form-badge-success font-14">{this.state.userlicensedetails.ess.available}</span></div>
												</div>
											</div>
										</div>
										<div className="col-lg-4 col-md-12 gs-user-details-inner-div">
											<div className="row">
												<div className="col-lg-10 offset-lg-1 col-md-12 col-sm-12 d-flex flex-row justify-content-between ">
													<div><img src="./images/fieldsmart_logo.svg" alt="iSmart Logo" className="marginright-5"/>FieldSmart</div>
													<div><span className="font-11 marginright-5">TOTAL</span> <span className="badge gs-form-badge-primary font-14">{this.state.userlicensedetails.track.authorized}</span></div>
													<div><span className="font-11 marginright-5">AVAILABLE</span> <span className="badge gs-form-badge-success font-14">{this.state.userlicensedetails.track.available}</span></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> : null}
						{this.state.pagejson.resourcename == 'expenserequests' ? <div className="col-md-12 marginbottom-10 bg-white user-license-container">
							<div className="row expenserequests-reimbursement-container">
								<div className="col-md-4 text-muted expenserequests-reimbursement-card">
									<div className="float-left d-flex flex-row align-items-center">
										<div className="expenserequests-reimbursement-dot float-left bg-danger"></div>
										<div className="float-left">AWAITING REIMBURSEMENT</div>
									</div>
									<div className="float-right expensereq-list-amount">
										{currencyFilter(this.state.expensereqdetails.awaitingreimbursement, this.props.app.defaultCurrency, this.props.app)}
									</div>
								</div>
								<div className="col-md-4 text-muted expenserequests-reimbursement-card">
									<div className="float-left d-flex flex-row align-items-center">
										<div className="expenserequests-reimbursement-dot float-left bg-warning"></div>
										<div className="float-left">CLAIMS TO BE APPROVED</div>
									</div>
									<div className="float-right expensereq-list-amount">
										{currencyFilter(this.state.expensereqdetails.claimstobeapproved, this.props.app.defaultCurrency, this.props.app)}
									</div>
								</div>
								<div className="col-md-4 text-muted expenserequests-reimbursement-card">
									<div className="float-left d-flex flex-row align-items-center">
										<div className="expenserequests-reimbursement-dot float-left bg-success"></div>
										<div className="float-left">ADVANCES WITH ME</div>
									</div>
									<div className="float-right expensereq-list-amount">
										{currencyFilter(this.state.expensereqdetails.advanceswithme, this.props.app.defaultCurrency, this.props.app)}
									</div>
								</div>
							</div>
						</div> : null}
						{(this.props.list.data && this.props.list.data.length > 0) || this.props.list.filterString ? <div className={`col-md-12 bg-white marginbottom-10 list-filter-bar`} style={{border: '1px solid #F5F5F5'}}>
							<div className="row" style={{margin: '0 auto',padding: '10px'}}>
								{this.state.isAdvanceFilterEnabled ? null : <div className="col-md-9 d-none d-lg-block">
									<div className="row">
										{this.renderBasicFilter()}
									</div>
								</div>}
								{this.state.isAdvanceFilterEnabled ? <div className="col-md-11">
									<div className="row">
										{this.renderFilterTags(this.state.advancedFilter, this.removeAdvFilter)}
										{this.renderAdvancedFilterContainer()}
									</div>
								</div> : null}
								<div className={`col-md-${this.state.isAdvanceFilterEnabled ? '1' : '3'}`}>
									{this.state.isAdvanceFilterEnabled ? null : <div className="float-left d-none d-lg-block">
										<button type="button" className="btn gs-form-btn-primary btn-sm btn-width marginleft-5" onClick={() =>{this.basicSearchFunction()}}>Search</button>
										{/*<button type="button" className="btn btn-sm gs-form-btn-primary btn-width marginleft-5" onClick={() =>{this.advancedFunction()}}>Advanced Search</button>*/}
										{this.props.list.filterString ? <button type="button" className="btn btn-sm gs-form-btn-primary marginleft-5" onClick={this.reset}><span className="fa fa-refresh"></span></button> : null}
									</div>}
									<div className="float-right" style={{position: 'relative', display: 'flex', top: '5px'}}>
										{this.state.advancedFilter.length > 0 && this.state.isAdvanceFilterEnabled ? <div className="marginright-15"><button type="button" className="btn btn-sm gs-form-btn-primary marginleft-5" onClick={this.advanceFilterReset}><span className="fa fa-refresh"></span></button></div> : null }
										<span className="marginright-10">Adv.Search</span>
										<div>
											<label className="gs-checkbox-switch">
												<input className="form-check-input" type="checkbox" checked={this.state.isAdvanceFilterEnabled} onChange={() => this.advancedFunction()} />
												<span className="gs-checkbox-switch-slider"></span>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div> : null}
						<Modal
							isOpen={false && this.state.isAdvanceFilterEnabled}
							ariaHideApp={false}
							aria={{
							labelledby: "heading",
							describedby: "full_description"}}
							className={`react-modal-custom-class-80`}
							overlayClassName={`react-modal-overlay-custom-class`}>
							<div>
								<div className='react-modal-header'>
									<h5>More Filters</h5>
								</div>
								<div className='react-modal-body'>
									{this.renderAdvancedFilter()}
								</div>
								<div className='react-modal-footer text-center'>
									<button type="button" rel="tooltip" title="Close"  className="btn btn-sm gs-btn-info btn-width" onClick={()=>{this.setState({isAdvanceFilterEnabled: false})}}><span className="fa fa-times"></span> Close</button>
									{/*<button type="button" rel="tooltip" title="Search"  className="btn btn-sm gs-btn-success btn-width" onClick={this.advanceSearchFunction}><span className="fa fa-search"></span> Search</button>*/}
									<a className="float-right" onClick={this.reset}>Clear all filters</a>
								</div>
							</div>
						</Modal>
						{this.props.list.data && this.props.list.data.length > 0 ? <div className="col-md-12 form-group bg-white" style={{paddingLeft: 0, paddingRight: 0, borderRight:'1px solid #f3f3f3d7', borderLeft:'1px solid #f3f3f3d7', borderBottom:'1px solid #f3f3f3d7'}}>
							<div className="gs-list-table-header-container" ref="listheader" style={{width: `${responsiveWidth}`}}>
							<table className="table table-sm gs-table-sm gs-list-table gs-list-table-header" style={{marginBottom:0}}>
								<thead>
									<tr>
										{this.renderTableHead(fieldArray)}
										{addExtraCell ? <th></th> : null}
									</tr>
								</thead>
							</table>
							</div>
							<div className="gs-list-table-body-container gs-scrollbar" ref={this.listbodyRef} style={tbody_div_style}>
							<table className="table table-hover table-sm gs-table-sm gs-list-table gs-list-table-body" style={{marginBottom:0}}>
								<tbody>
									{this.renderTbody.bind(this)(fieldArray, addExtraCell)}
								</tbody>
							</table>
							</div>
						</div> : null}
						{this.props.list.data && this.props.list.data.length > 0 ? <div className="row list-footer-bar">
							<div className="col-md-4 col-sm-6 col-12 marginbottom-xs-10 btn-group btn-group-sm list-pagecount float-left" role="group">
								{this.renderPageCounts()}
							</div>
							<div className="col-md-4 col-sm-6 col-12 marginbottom-xs-10 btn-group text-center margintop-5 gs-text-left-xs">
								{TotalPageLength}
							</div>
							<div className="col-md-4 col-sm-6 col-12 btn-group text-right gs-text-left-xs">
								{this.renderPagination()}
							</div>
						</div> : null }
						{((this.props.list.data && this.props.list.data.length == 0) && !this.props.list.filterString) ? <div className="col-md-12 form-group">
							<div className="list-error-image-box">
								<img src="/images/empty-placeholder.png" className="list-error-image" width="150" height="150" />
							</div>
							<div className="list-error-text-box">
								<div className="list-error-text">
									<div className="marginbottom-15">No {createButtonName} available. Create a {createButtonName}.</div>
									{this.state.pagejson.addbuttonoptions ? <div>
										<Link to={`/${this.state.pagejson.addbuttonoptions.href}`} className="btn btn-sm gs-btn-success"><span className="fa fa-plus marginright-5"></span>Add {createButtonName}</Link>
									</div> : null }
								</div>
							</div>
						</div> : null}
						{((this.props.list.data && this.props.list.data.length == 0) && this.props.list.filterString) ? <div className="col-md-12 form-group">
							<div className="list-error-image-box">
								<img src="/images/no-search-results-placeholder.png" className="list-error-image" width="150" height="150" />
							</div>
							<div className="list-error-text-box">
								<div className="list-error-text">
									<div>No results found for your search!</div>
									<div>You can reset, or change your search criteria.</div>
								</div>
							</div>
						</div> : null}
					</div>
					{(this.state.pagejson.calendarOptions && this.props.list.listview == 'Calendar') ? <div className="col-md-12">
						<Calendarview calendarlist={this.state.pagejson} rootapp={this.props} />
					</div> : null}
					{(this.state.pagejson.kanbanView && this.props.list.listview == 'Kanban') ? <div className="col-md-12">
						<KanbanBoard json={this.state.pagejson.kanbanView.json} history={this.props.history} openModal={this.props.openModal} createOrEdit={this.props.createOrEdit} app={this.props.app} updateAppState={this.props.updateAppState} salesperson={this.state.kanbansalesperson} list={this.props.list} />
					</div> : null }
					{(this.state.pagejson.treeView && this.props.list.listview == 'Tree') ? <div className="col-md-12">
						<Treeview pagejson={this.state.pagejson} rootapp={this.props.app} history={this.props.history} />
					</div> : null }
				</div>
			</div>
		);
	};
}

export default connect((state, props) => {
	let listTitle = getListTitle(state, props);
	let initialList = {
		pagelength: defaultPageLength,
		page: 0,
		listview: defaultListView,
		counts: pageLengths,
		initial: true
	};
	return {
		app: state.app,
		pagejson: state.pagejson,
		title: listTitle,
		list: state.list ? (state.list[listTitle] ? state.list[listTitle] : initialList) : initialList
	}
}, (dispatch) => {
	return bindActionCreators({
		updatePageJSONState,
		updateAppState,
		updateListState
	}, dispatch);
})(List);
