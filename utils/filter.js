import moment from 'moment';
export default module.exports;

export function currencyFilter(input, currencyid, app) {
	if ((!currencyid || app.currency[currencyid] == "undefined" || !app.currency[currencyid]) && currencyid != 'percentage' && currencyid != 'qty')
		currencyid = app.defaultCurrency;

	input = typeof(input) == 'string' && input !== '' && !isNaN(Number(input)) ? Number(input) : input;
	if (!isNaN(input) && input != 0 && input != null) {
		input = Number(input.toFixed(app.roundOffPrecision)) == 0 ? 0 : input;
		let result = "";
		if(currencyid == 'percentage')
			result = formatNumber('percentage', input, '1,23,456.00');
		else if(currencyid == 'qty')
			result = formatNumber('percentage', input, '1,23,456.00');
		else
			result = formatNumber('currency', input, app.currency[currencyid].format, app.currency[currencyid].symbolposition, app.currency[currencyid].symbol);

		return result;
	} else if (input == 0 && currencyid != 'percentage' && currencyid != 'qty' && input !== '') {
		if (app.currency[currencyid].symbolposition == 'before') {
			return app.currency[currencyid].symbol + ' 0.' + '00000'.substr(0, app.roundOffPrecision);
		} else {
			return '0.' + '00000'.substr(0, app.roundOffPrecision) + ' ' + app.currency[currencyid].symbol;
		}
	}  else if (input == 0 && currencyid == 'qty' && input !== '') {
		return "0";
	} else
		return "";

	function formatNumber(type, input, format, position, symbol) {
		let negativeValue = false;
		let separator,
		decimalSeparator,
		regexFormat,
		precision = (type == 'percentage') ? app.roundOffPrecisionStock : app.roundOffPrecision;
		if (format == '1,23,456.00' || format == '123,456.00') {
			separator = ',';
			decimalSeparator = '.';
			regexFormat = (format == '123,456.00') ? /\B(?=(\d{3})+(?!\d))/g : /\B(?=(\d{2})+(?!\d))/g;
		}
		if (format == '123.456,00') {
			separator = '.';
			decimalSeparator = ',';
			regexFormat = /\B(?=(\d{3})+(?!\d))/g;
		}
		if(input < 0) {
			input = Math.abs(input);
			negativeValue = true;
		}
		let number = input.toString();
		let numberaftersplit = number.split('.');
		let lastThree = numberaftersplit[0].substring(numberaftersplit[0].length - 3);
		let otherNumbers = numberaftersplit[0].substring(0, numberaftersplit[0].length - 3);
		if (otherNumbers != '')
			lastThree = separator + lastThree;

		let result = otherNumbers.replace(regexFormat, separator) + lastThree;
		if (numberaftersplit[1] && numberaftersplit[1] != 'undefined' && numberaftersplit[1] != '')
			result += decimalSeparator + (numberaftersplit[1]+'00000').substr(0, precision);
		else if(type != 'percentage')
			result += decimalSeparator + '00000'.substr(0, precision);

		if(position && symbol) {
			if (position == 'before')
				result = symbol + " " + result;
			else
				result = result + " " + symbol;
		}
		return negativeValue ? '-'+result : result;
	}
}

export function booleanfilter(input) {
	if(input == false)
		return 'No';
	else if(input == true)
		return 'Yes';
	return "";
}

export function uomFilter(input, uomObject) {
	if (input != '' && input !=null && uomObject[input])
		return uomObject[input].name;
	return '';
}

export function taxFilter(input, taxObject) {
	if (input != '' && input !=null && Array.isArray(input) && input.length>0) {
		let tempString = '';
		for(var i=0;i<input.length;i++)
			tempString += (taxObject[input[i]] ? (taxObject[input[i]].taxcode + ', ') : '');
		return tempString == '' ? '' : tempString.substr(0,tempString.length-2);
	} else if(input > 0) {
		return taxObject[input] ? taxObject[input].taxcode : '';
	}
	return "";
}

export function multiSelectAutoFilter(input, taxObject) {
	if (input != '' && input !=null && Array.isArray(input) && input.length>0)
		return input.join(', ');

	return "";
}

export function resourceFilter(input, resourceObject) {
	if (input != '' && input !=null && resourceObject && resourceObject[input])
		return resourceObject[input].displayName;

	return input || "";
}

export function deliveryreceiptforfilter(input) {
	if(input == 'Sales Orders for After Order' || input == 'Sales Orders for On Demand')
		return 'Sales Orders';
	if(input == 'Item Requests for Service Call' || input == 'Item Requests for Engineer' || input == 'Item Requests for Project')
		return 'Item Requests';
	return input;
}

export function dateFilter(input) {
	if (input != '' && input !=null) {
		let dateString = moment(input).format("DD-MMM-YYYY");
		return dateString;
	}
	return "";
}

export function datetimeFilter(input, format) {
	if (input != '' && input !=null) {
		let dateString = moment(input).format(format ? format : "DD-MMM-YYYY @ h:mm a");
		return dateString;
	}
	return "";
}

export function timeFilter(input) {
	if (input != '' && input !=null) {
		let dateString = moment(input).format("h:mm a");
		return dateString;
	}
	return "";
}

export function dateAgoFilter(input) {
	if (input != '' && input !=null) {
		let dateAgoString = moment(input).fromNow();
		return dateAgoString;
	}
	return "";
}

export function timeDurationFilter(input, flag) {
	if (input != '' && input !=null) {
		//let dateDurationString = moment.utc(moment.duration(input, 'minutes').asMilliseconds()).format('HH:mm');
		let hours = Math.floor(input/60),
			minutes = Math.floor(input%60);

		hours = hours.toString().length == 1 ? `0${hours}` : hours;
		minutes = minutes.toString().length == 1 ? `0${minutes}` : minutes;

		let dateDurationString = flag ? (`${Number(hours) > 0 ? hours + ' hours ' : ''}${Number(minutes) > 0 ? minutes + ' minutes' : ''}`) : `${hours}:${minutes}`;

		return dateDurationString;
	}

	return "";
}

export function arrayFilter(array, search, properties) {
	if(!array)
		return array;

	if(!search)
		return array;

	if(typeof(properties) == 'string')
		properties = [properties];

	let searchArray = search.split(' ');

	searchArray.forEach(s => {
		s = s.trim();
	});

	searchArray = searchArray.filter(s => s ? true : false);

	let search_length = searchArray.length;

	return array.filter((item) => {
		let string = `${properties.map(a => item[a]).join(' ').toLowerCase()}`;

		let oldIndex = -1;
		for (var i = 0; i < search_length; i++) {
			oldIndex = string.indexOf(searchArray[i]);
			if (oldIndex >= 0) {
				string = string.substr(oldIndex);
				if(i == search_length - 1)
					return true;
			} else {
				return false;
			}
		}

		return false;
	});
}

export function resourceFieldNameFilter(input, resourcename, app) {
	var splitField = input.split('/');
	if(splitField.length == 1) {
		if(app.myResources[resourcename].fields[splitField[0]]) {
			return app.myResources[resourcename].fields[splitField[0]].displayName;
		}
	} else {
		if(app.myResources[resourcename].fields[splitField[0]] && app.myResources[resourcename].fields[splitField[0]].isForeignKey) {
			var foreignkeyresourcename = app.myResources[resourcename].fields[splitField[0]].foreignKeyOptions.resource;
			if(splitField[1] == app.myResources[resourcename].fields[splitField[0]].foreignKeyOptions.mainField.split('.')[1]) {
				return app.myResources[resourcename].fields[splitField[0]].displayName;
			} else {
				if(app.myResources[foreignkeyresourcename].fields[splitField[1]]) {
					return app.myResources[resourcename].fields[splitField[0]].displayName +"'s "+ app.myResources[foreignkeyresourcename].fields[splitField[1]].displayName;
				}
			}
		}
	}
	return "";
}

export function orderByTax (input, field, reverse) {
	var filtered = [];
	if(input) {
		for(var prop in input) {
			filtered.push(input[prop]);
		}
		filtered.sort(function (a, b) {
			return (a[field] > b[field] ? 1 : -1);
		});
		if (reverse)
			filtered.reverse();
	}
	return filtered;
}


export function discountFilter(input, currencyid, mode, app) {
	if ((!currencyid || app.currency[currencyid] == "undefined" || !app.currency[currencyid]) && currencyid !='qty')
		currencyid = app.defaultCurrency;

	if (input != '' && input && typeof(input)=='number') {
		let returnstr = Number(input.toFixed('3'))
		if(mode == 'Percentage')
			return returnstr.toString()+' %';
		else
			return app.currency[currencyid].symbol + ' ' + returnstr;
	}
	return "";
}

export function itemmasterDisplaynamefilter(input, app) {
	if (input != '' && input != null && input != undefined) {
		return app.myResources.itemmaster.fields[`${input}`] ? app.myResources.itemmaster.fields[`${input}`].displayName : input;
	}
	return "";
}
