import React,{Component} from 'react';
import axios from 'axios';
import {toast} from './utils';
import Q from 'q';
import XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import moment from 'moment';

import * as utils from './utils';
import CheckTransactionExistModal from '../components/details/checktransactiondetails';
import { InfoModal, ConfirmModal } from '../components/utilcomponents';
import {currencyFilter, dateFilter, datetimeFilter,taxFilter,itemmasterDisplaynamefilter} from './filter';
import { excel_sheet_from_array_of_arrays, excel_Workbook, excel_s2ab } from './excelutils';

export const commonMethods = {
	getItemDetails : function(itemid, customerObj, pricelistid, transaction, uomid, alternateuom, conversionfactor, contractid, servicecallid, equipmentid) {
		var deferred = Q.defer();
		var filter;
		let tempcustomerid, temppartneraddressid;
		if(customerObj && typeof(customerObj) == 'object') {
			tempcustomerid = customerObj.customerid > 0 ? customerObj.customerid : null;
			temppartneraddressid = customerObj.partneraddressid > 0 ? customerObj.partneraddressid : null;
		} else {
			tempcustomerid = customerObj > 0 ? customerObj : null;
		}
		if (alternateuom) {
			filter = `itemid=${itemid}&customerid=${tempcustomerid}&partneraddressid=${temppartneraddressid}&pricelistid=${pricelistid}&transaction=${transaction}&uomid=${uomid}&alternateuom=${alternateuom}&conversionfactor=${conversionfactor}&contractid=${contractid}&equipmentid=${equipmentid}&servicecallid=${servicecallid}`;
		} else {
			filter = `itemid=${itemid}&customerid=${tempcustomerid}&partneraddressid=${temppartneraddressid}&pricelistid=${pricelistid}&transaction=${transaction}&uomid=${uomid}&conversionfactor=${conversionfactor}&contractid=${contractid}&equipmentid=${equipmentid}&servicecallid=${servicecallid}`;
		}

		var urlText = "/api/common/methods/getItemDetails?" + filter
		axios({
			method : 'GET',
			url : urlText
		}).then(function (response) {
			if(response.data.message == 'success'); {
				deferred.resolve(response.data);
			}
		});

		return deferred.promise;
	},

	getRate : function (rate, currencyid, currencyexchangerate, pricelistid, rootapp) {
		var roundOffPrecision = 5;

		for (var i = 0; i < rootapp.appSettings.length; i++) {
			if (rootapp.appSettings[i].module == 'Accounts' && rootapp.appSettings[i].name == 'Rounding Off Precision') {
				roundOffPrecision = rootapp.appSettings[i].value.value;
				break;
			}
		}

		if (currencyid && (rootapp.defaultCurrency == currencyid || rootapp.pricelistObj[pricelistid ? pricelistid : 0].currencyid == currencyid)) {
			return Number(rate.toFixed(roundOffPrecision));
		} else {
			if (currencyexchangerate)
				return Number((rate / currencyexchangerate).toFixed(roundOffPrecision));
			else
				return Number(rate.toFixed(roundOffPrecision));
		}
	},

	getCurrencyExchangeRate : function (currencyid, date) {
		var deferred = Q.defer();
		if (currencyid) {
			var urlText = `/api/common/methods/getCurrencyExchangeRate?currencyid=${currencyid}&date=${date}`;
			axios({
				method : 'GET',
				url : urlText
			}).then(function (response) {
				deferred.resolve(response.data);
			});
		} else {
			var message = {
				header : 'Warning',
				body : 'Please select Currency',
				btnName : ['Ok']
			};
			modalService.infoMethod(message);
			deferred.reject();
		}
		return deferred.promise;
	},

	numberToWord : function(app, amount, currency) {
		if (!currency || app.currency[currency] == "undefined" || !app.currency[currency])
			currency = app.defaultCurrency;

		var digits = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
		var tens = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
		var str = '', paise = '';
		var amountSplit = amount.toString().split('.');
		var num = amountSplit[0].toString();

		if (num.length > 12) {
			return 'overflow';
		}
		if (amountSplit[1]) {
			paise = ((amountSplit[1].toString() + '00').substr(0, 2)).match(/^(\d{2})$/);
		}

		str += app.currency[currency].name + " ";

		if (app.currency[currency].format == '1,23,456.00') {
			var rupees = ('000000000000' + num).substr(-12).match(/^(\d{2})(\d{1})(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);

			str += (rupees[1] != 0) ? (digits[Number(rupees[1])] || tens[rupees[1][0]] + ' ' + digits[rupees[1][1]]) + ' Thousand ' : '';
			str += (rupees[2] != 0) ? (digits[Number(rupees[2])] || tens[rupees[2][0]] + ' ' + digits[rupees[2][1]]) + ' Hundred ' : '';
			str += (rupees[3] != 0) ? (digits[Number(rupees[3])] || tens[rupees[3][0]] + ' ' + digits[rupees[3][1]]) + ' Crore ' : ((rupees[1] != 0) || (rupees[2] != 0)) ? ' Crore ' : '';
			str += (rupees[4] != 0) ? (digits[Number(rupees[4])] || tens[rupees[4][0]] + ' ' + digits[rupees[4][1]]) + ' Lakh ' : '';
			str += (rupees[5] != 0) ? (digits[Number(rupees[5])] || tens[rupees[5][0]] + ' ' + digits[rupees[5][1]]) + ' Thousand ' : '';
			str += (rupees[6] != 0) ? (digits[Number(rupees[6])] || tens[rupees[6][0]] + ' ' + digits[rupees[6][1]]) + ' Hundred ' : '';
			str += (rupees[7] != 0) ? (digits[Number(rupees[7])] || tens[rupees[7][0]] + ' ' + digits[rupees[7][1]]) + "" : '';
		} else {
			var rupees = ('000000000000' + num).substr(-12).match(/^(\d{1})(\d{2})(\d{1})(\d{2})(\d{1})(\d{2})(\d{1})(\d{2})$/);

			str += (rupees[1] != 0) ? (digits[Number(rupees[1])]) + ' Hundred ' : '';
			str += (rupees[2] != 0) ? (digits[Number(rupees[2])] || tens[rupees[2][0]] + ' ' + digits[rupees[2][1]]) + ' Billion ' : (rupees[1] != 0) ? ' Billion ' : '';
			str += (rupees[3] != 0) ? (digits[Number(rupees[3])]) + ' Hundred ' : '';
			str += (rupees[4] != 0) ? (digits[Number(rupees[4])] || tens[rupees[4][0]] + ' ' + digits[rupees[4][1]]) + ' Million ' : (rupees[3] != 0) ? ' Million ' : '';
			str += (rupees[5] != 0) ? (digits[Number(rupees[5])]) + ' Hundred ' : '';
			str += (rupees[6] != 0) ? (digits[Number(rupees[6])] || tens[rupees[6][0]] + ' ' + digits[rupees[6][1]]) + ' Thousand ' : (rupees[5] != 0) ? ' Thousand ' : '';
			str += (rupees[7] != 0) ? (digits[Number(rupees[7])]) + ' Hundred ' : '';

			str += (rupees[8] != 0) ? (digits[Number(rupees[8])] || tens[rupees[8][0]] + ' ' + digits[rupees[8][1]]) + " " : '';
		}

		if (paise) {
			str += (paise[1] != 0) ? " " + (digits[Number(paise[1])] || tens[paise[1][0]] + ' ' + digits[paise[1][1]]) + " " + app.currency[currency].fractionname : " "
		}
		return str;
	},

	apiResult : function (response) {
		if (response.data.message == 'success') {
			let generateCustBody = '';
			if(response.config && response.config.method.toLowerCase() == 'post' && response.resourceObj) {
				let configData = JSON.parse(response.config.data);
				let actionVerbObj = {
					'Save': 'Saved',
					'Submit': 'Submitted',
					'Approve': 'Approved',
					'Update': 'Updated',
					'Revise': 'Revised',
					'Cancel': 'Cancelled'
				};
				if(actionVerbObj[configData.actionverb]) {
					let titleField = '';
					for(var prop in response.resourceObj.fields) {
						if(response.resourceObj.fields[prop].title)
							titleField = prop;
					}
					generateCustBody = `${response.resourceObj.displayName} ${actionVerbObj[configData.actionverb]} Successfully`;
					if(titleField != '')
						generateCustBody += ` - ${response.data.main[titleField]}`;
				}
			}
			var returnMessage = {
				methodName : 'infoMethod'
			};
			var message = {
				header : 'Success!',
				body : generateCustBody != '' ? generateCustBody : 'Your operation is successful',
				bodyArray : [],
				btnArray : ['Ok'],
				isToast: true,
				btnTitle : ""
			};
			returnMessage.message = message;
			return returnMessage;
		} else if (response.data.hasAuthority) {
			var returnMessage = {};
			returnMessage.methodName = 'confirmMethod';
			var message = {
				header : 'Exception Approval!',
				btnArray : ['Approve', 'Cancel']
			};
			if (response.data.error) {
				message.bodyArray = response.data.error;
			}
			message.btnTitle = "Do you want to approve this exception?";
			returnMessage.message = message;
			return returnMessage;
		} else {
			var returnMessage = {};
			returnMessage.methodName = 'infoMethod';
			var message = {
				header : 'Error',
				btnArray : ['OK'],
				btnTitle : ""
			};

			if (response.data.message == 'failure')
				message.body = 'Sorry, there was an error!';
			else
				message.body = response.data.message;

			if (response.data.error) {
				message.bodyArray = response.data.error;
			}
			if(response.data.additionalErrorInformation) {
				message.additionalInfo = response.data.additionalErrorInformation;
			}
			returnMessage.message = message;
			return returnMessage;
		}
	},
	customFieldsOperation : function (resourceJSON, resourcename, destObj, souObj, destinationresourcename, itemstr) {
		if(!resourceJSON[resourcename] || !resourceJSON[resourcename].fields || !resourceJSON[destinationresourcename] || !resourceJSON[destinationresourcename].fields)
				return alert("Invalid Resource Names" , resourcename, destinationresourcename);

		for (var prop in resourceJSON[resourcename].fields) {
			if (prop.indexOf('zzz') == 0) {
				if(resourceJSON[destinationresourcename].fields[prop]) {
					if(!itemstr)
						destObj[prop] = souObj[prop];
					else
						destObj[`${itemstr}.${prop}`] = souObj[prop];
				}
			}
		}
	},
	getCustomFields : function (resourceJSON, resourcename, returntype, restrictJSONField) {
		if (resourceJSON[resourcename] && resourceJSON[resourcename].fields) {
			let customFieldArray = [];
			let customFieldStr = '';
			for (var prop in resourceJSON[resourcename].fields) {
				if (prop.indexOf('zzz') == 0 && (!restrictJSONField || (restrictJSONField && resourceJSON[resourcename].fields[prop].type != 'json'))) {
					customFieldArray.push(prop);
					customFieldStr += prop+",";
				}
			}
			customFieldStr = customFieldStr.substr(0, customFieldStr.length-1);
			if(returntype == 'string')
				return customFieldStr;
			else
				return customFieldArray;
		} else {
			alert("Invalid Resource Name" + resourcename);
		}
	},
	copyToClipBoard : function (text) {
		var copyElement = document.createElement("span");
		copyElement.appendChild(document.createTextNode(`${text}`));
		copyElement.id = "tempCopyToClipboard";
		document.body.appendChild(copyElement);

		var range = document.createRange();
		range.selectNode(copyElement);
		window.getSelection().removeAllRanges();
		window.getSelection().addRange(range);

		document.execCommand('copy');
		window.getSelection().removeAllRanges();
		copyElement.remove();
	},
	openGoogleDrive : function (props, callback) {
		if(props.app.gpWindow && props.app.gpWindow.closed)
			props.updateAppState('gpWindow', null);

		if(props.app.gpWindow)
			return props.app.gpWindow.focus();

		window.onPicked = (docs) => {
			callback(docs);
			props.app.gpWindow.close();
			props.updateAppState('gpWindow', null);
			document.domain = document.location.hostname;
		}

		document.domain = 'growsmartsmb.in';
		props.updateAppState('gpWindow', window.open("https://googlepicker.growsmartsmb.in/googlepicker.html", '_blank', 'width=1200,height=800'));
	},
	printBI : function (reportName, rendererName) {
		var reportWindow = window.open('', reportName, 'width=600,height=600');
		reportWindow.document.open();
		var pvtRenderArea = document.getElementsByClassName('pvtOutput')[0];
		if (rendererName != 'Table')
			pvtRenderArea.querySelectorAll('div')[0].style = 'width:100%;height:auto;';

		let htmlToPrint = '<style>table {border-collapse:collapse;} table th, table td {border:1px solid #000;} table td {text-align: right!important;}</style>';
		htmlToPrint += '<div class="printDiv"><p><b>' + reportName + '</b></p>' + pvtRenderArea.innerHTML + '</div>';
		reportWindow.document.write(htmlToPrint);
		let element = reportWindow.document.getElementsByClassName('modebar--hover')[0];
		if(element)
			element.parentNode.removeChild(element);
		if(reportWindow.document.getElementsByClassName("main-svg").length > 0) {
			reportWindow.document.getElementsByClassName("main-svg")[0].style.position = 'absolute';
			reportWindow.document.getElementsByClassName("main-svg")[0].style.top = '0px';
			reportWindow.document.getElementsByClassName("main-svg")[0].style.left = '0px';
			reportWindow.document.getElementsByClassName("main-svg")[1].style.position = 'absolute';
			reportWindow.document.getElementsByClassName("main-svg")[1].style.top = '0px';
			reportWindow.document.getElementsByClassName("main-svg")[1].style.left = '0px';
		}
		reportWindow.print();
		reportWindow.close();
	},
	downloadCanvasImage : function (reportName, rendererName) {
		var pvtRenderArea = document.getElementsByClassName('chartjs-render-monitor')[0];
		var lnk = document.createElement('a');
  		lnk.download = `${reportName}.png`;
  		lnk.href = pvtRenderArea.toDataURL('image/png').replace('image/png', 'image/octet-stream');
		lnk.click();
	},
	sortUserRowsPerformanceTarget : function (targetArray, periodArray, reportFlag) {
		let userRows = [], uniqueArray = [];

		let targetObj = {};
		targetArray.forEach(item => {
			let type = item.userid > 0 ? 'user' : 'group';

			let propvalue = `${type}_${type == 'group' ? item.groupname : item.userid}`;

			if(!targetObj[propvalue])
				targetObj[propvalue] = {};

			targetObj[propvalue][item.targetperiod] = item.targetvalue;
		});

		let recursivePTFn = (index, seclevel) => {

			for (var j = 0; j < targetArray.length; j++) {
				if (index == targetArray[j].rootindex) {
					let tempObj = {
						type: targetArray[j].userid > 0 ? 'user' : 'group',
						groupname: targetArray[j].userid > 0 ? null : targetArray[j].groupname,
						userid: targetArray[j].userid > 0 ? targetArray[j].userid : null,
						userid_displayname: targetArray[j].userid > 0 ? targetArray[j].userid_displayname : null,
						index: targetArray[j].userid > 0 ? null : targetArray[j].index,
						rootindex: targetArray[j].rootindex,
						level: seclevel + 1
					};

					if(reportFlag) {
						tempObj.totaltarget = targetArray[j].totaltarget;
						tempObj.totaltargetcompleted = targetArray[j].totaltargetcompleted;
						tempObj.currenttarget = targetArray[j].currenttarget;
						tempObj.currenttargetcompleted = targetArray[j].currenttargetcompleted;
						tempObj.cummulativetarget = targetArray[j].cummulativetarget;
						tempObj.cumulativetargetcompleted = targetArray[j].cumulativetargetcompleted;
						tempObj.performancetargetid = targetArray[j].id;
					}

					let uniqueValue = `${tempObj.type}_${tempObj.type == 'group' ? tempObj.groupname : tempObj.userid}`;

					if(!uniqueArray.includes(uniqueValue)) {
						uniqueArray.push(uniqueValue);
						let insertIndex = null;
						for(var k=0;k<userRows.length;k++) {
							if(userRows[k].index == targetArray[j].rootindex)
								insertIndex = k;

							if(userRows[k].rootindex == targetArray[j].rootindex)
								insertIndex = k;
						}

						if(insertIndex !== null)
							userRows.push(tempObj);
						else
							userRows.push(tempObj);

						if(tempObj.type == 'group')
							recursivePTFn(tempObj.index, seclevel + 1);
					}
				}
			}
		}

		targetArray.forEach(item => {
			if(item.rootindex == null) {
				let tempObj = {
					type: item.userid > 0 ? 'user' : 'group',
					groupname: item.userid > 0 ? null : item.groupname,
					userid: item.userid > 0 ? item.userid : null,
					userid_displayname: item.userid > 0 ? item.userid_displayname : null,
					index: item.userid > 0 ? null : item.index,
					rootindex: item.rootindex,
					level: 0
				};

				if(reportFlag) {
					tempObj.totaltarget = item.totaltarget;
					tempObj.totaltargetcompleted = item.totaltargetcompleted;
					tempObj.currenttarget = item.currenttarget;
					tempObj.currenttargetcompleted = item.currenttargetcompleted;
					tempObj.cummulativetarget = item.cummulativetarget;
					tempObj.cumulativetargetcompleted = item.cumulativetargetcompleted;
					tempObj.performancetargetid = item.id;
				}

				let uniqueValue = `${tempObj.type}_${tempObj.type == 'group' ? tempObj.groupname : tempObj.userid}`;

				if(!uniqueArray.includes(uniqueValue)) {
					uniqueArray.push(uniqueValue);
					userRows.push(tempObj);
					if(tempObj.type == 'group')
						recursivePTFn(tempObj.index, 0);
				}
			}
		});

		userRows.forEach((item) => {
			let propvalue = `${item.type}_${item.type == 'group' ? item.groupname : item.userid}`;

			if(!targetObj[propvalue])
				return null;

			for(var targetperiod in targetObj[propvalue]) {
				let newTargetPeriod = targetperiod.replace(/ /g, '').replace(/-/g, '');

				item[newTargetPeriod] = targetObj[propvalue][targetperiod];
			}
		});

		return userRows;
	},
	sortHeaderRowsPerformanceTarget : function (targetItemsHeader, periodicity) {
		return targetItemsHeader.sort((a, b) => {
			if(periodicity == 'month-year')
				return new Date(a) - new Date(b);

			if(periodicity == 'calendarquarter-calendaryear') {
				let aTargetPeriod = a.replace('Q1', 'Jan').replace('Q2', 'Apr').replace('Q3', 'Jul').replace('Q4', 'Oct');
				let bTargetPeriod = b.replace('Q1', 'Jan').replace('Q2', 'Apr').replace('Q3', 'Jul').replace('Q4', 'Oct');

				let aDate = moment(aTargetPeriod, 'MMM-YYYY').startOf('quarter')._d;
				let bDate = moment(bTargetPeriod, 'MMM-YYYY').startOf('quarter')._d;

				return new Date(aDate) - new Date(bDate);
			}

			if(periodicity == 'financialquarter-financialyear') {
				let aQuarter = (`${a.split('-')[0]}`).replace('Q1', 'Apr').replace('Q2', 'Jul').replace('Q3', 'Oct').replace('Q4', 'Jan');
				let aMonth = aQuarter == 'Jan' ? `${a.split('-')[2]}` : `${a.split('-')[1]}`;

				let bQuarter = (`${b.split('-')[0]}`).replace('Q1', 'Apr').replace('Q2', 'Jul').replace('Q3', 'Oct').replace('Q4', 'Jan');
				let bMonth = bQuarter == 'Jan' ? `${b.split('-')[2]}` : `${b.split('-')[1]}`;

				let aTargetPeriod = (`${aQuarter}-${aMonth}`);
				let bTargetPeriod = (`${bQuarter}-${bMonth}`);

				let aDate = moment(aTargetPeriod, 'MMM-YY').startOf('quarter')._d;
				let bDate = moment(bTargetPeriod, 'MMM-YY').startOf('quarter')._d;

				return new Date(aDate) - new Date(bDate);
			}

			return a-b;
		});
	}
}

export const modalService = {
	infoMethod : function (message, callback) {
		if(message.isToast) {
			toast(message, callback);
		}
		return {render: (closeModal) => {return <InfoModal {...message}  closeModal={closeModal} />}, dontShow: message.isToast ,className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}};
	},
	confirmMethod : function (message, callback) {
		return {render: (closeModal) => {return <ConfirmModal {...message} callback={callback} closeModal={closeModal} />}, className: {content: message.warningModal ? 'react-modal-custom-class-30' : 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true};
	}
}

export function taxEngine (props, parentName, childName, quantityparam, typeparam, splitTaxParam) {
	var taxmaster = props.app.taxObject;
	props[parentName].taxdetails = {};
	props[parentName].basictotal = 0,
	props[parentName].totalaftertax = 0;
	var errorArr = [];

	var roundOffPrecision = 5;
	var defaultCurrency = props.app.defaultCurrency;

	for (var i = 0; i < props.app.appSettings.length; i++) {
		if (props.app.appSettings[i].module == 'Accounts' && props.app.appSettings[i].name == 'Rounding Off Precision') {
			roundOffPrecision = props.app.appSettings[i].value.value;
		}
	}

	if(quantityparam) {
		var qtyupdateObj = {};
		for (var i = 0; i < props[parentName][childName].length; i++) {
			if(props[parentName][childName][i].capacityfield) {
				qtyupdateObj[`${childName}[${i}].quantity`] = Number(((props[parentName][childName][i].capacity ? props[parentName][childName][i].capacity : 1) * (props[parentName][childName][i].equipmentid ? 1 : (props[parentName][childName][i].qtyinnos ? props[parentName][childName][i].qtyinnos :  0))).toFixed(roundOffPrecision));
			} else {
				qtyupdateObj[`${childName}[${i}].quantity`] = (props[parentName][childName][i].equipmentid ? 1 : (props[parentName][childName][i].quantity ? props[parentName][childName][i].quantity : 0));
			}
			props[parentName][childName][i].quantity = qtyupdateObj[`${childName}[${i}].quantity`];
		}
		props.updateFormState(props.form, qtyupdateObj);
	}

	var itemdetailupdateObj = {};
	for (var i = 0; i < props[parentName][childName].length; i++) {
		if(!typeparam || (typeparam && props[parentName][childName][i].itemtype == 'Item')) {
			props[parentName][childName][i].amount = 0;

			if (props[parentName][childName][i].rate) {
				if (props[parentName][childName][i].discountquantity == null || props[parentName][childName][i].discountquantity == "") {
					props[parentName][childName][i].finalrate = props[parentName][childName][i].rate;

				} else {
					if (props[parentName][childName][i].discountmode == 'Percentage') {
						props[parentName][childName][i].finalrate = Number((props[parentName][childName][i].rate - (props[parentName][childName][i].rate * (props[parentName][childName][i].discountquantity / 100))).toFixed(roundOffPrecision));
					} else {
						props[parentName][childName][i].finalrate = Number((props[parentName][childName][i].rate - props[parentName][childName][i].discountquantity).toFixed(roundOffPrecision));
					}
				}

				if (props[parentName].currencyid == defaultCurrency) {
					props[parentName][childName][i].ratelc = props[parentName][childName][i].rate;
					props[parentName][childName][i].finalratelc = props[parentName][childName][i].finalrate;
				} else {
					if (props[parentName].currencyexchangerate) {
						props[parentName][childName][i].ratelc = props[parentName][childName][i].rate * props[parentName].currencyexchangerate;
						props[parentName][childName][i].finalratelc = props[parentName][childName][i].finalrate * props[parentName].currencyexchangerate;
					} else {
						props[parentName][childName][i].ratelc = 0;
						props[parentName][childName][i].finalratelc = 0;
					}
				}
			} else {
				props[parentName][childName][i].finalrate = 0;
				props[parentName][childName][i].finalratelc = 0;
				props[parentName][childName][i].ratelc = 0;
			}

			if (props[parentName][childName][i].quantity != null) {
				props[parentName][childName][i].amount = Number((props[parentName][childName][i].finalrate * props[parentName][childName][i].quantity).toFixed(roundOffPrecision));

				if (props[parentName].currencyid == defaultCurrency) {
					props[parentName][childName][i].amountlc = props[parentName][childName][i].amount;
				} else {
					if (props[parentName].currencyexchangerate) {
						props[parentName][childName][i].amountlc = Number((props[parentName][childName][i].amount * props[parentName].currencyexchangerate).toFixed(roundOffPrecision));
					} else {
						props[parentName][childName][i].amountlc = 0;
					}
				}
				if(splitTaxParam && props[parentName][childName][i].splitrate) {
					if(props[parentName][childName][i].labourrate)
						itemdetailupdateObj[`${childName}[${i}].labouramount`] = Number((props[parentName][childName][i].labourrate * props[parentName][childName][i].quantity).toFixed(roundOffPrecision));
					else
						itemdetailupdateObj[`${childName}[${i}].labouramount`] = 0;
					if(props[parentName][childName][i].materialrate)
						itemdetailupdateObj[`${childName}[${i}].materialamount`] = Number((props[parentName][childName][i].materialrate * props[parentName][childName][i].quantity).toFixed(roundOffPrecision));
					else
						itemdetailupdateObj[`${childName}[${i}].materialamount`] = 0;
				}
			}

			var itemsTaxValue = 0;
			props[parentName][childName][i].taxid = (props[parentName][childName][i].taxid) ? props[parentName][childName][i].taxid : [];
			props[parentName][childName][i].taxdetails = {};
			if(splitTaxParam && props[parentName][childName][i].splitrate) {
				props[parentName][childName][i].labourtaxid = (props[parentName][childName][i].labourtaxid) ? props[parentName][childName][i].labourtaxid : [];
				props[parentName][childName][i].labourtaxdetails = {};
				props[parentName][childName][i].materialtaxid = (props[parentName][childName][i].materialtaxid) ? props[parentName][childName][i].materialtaxid : [];
				props[parentName][childName][i].materialtaxdetails = {};
				props[parentName][childName][i].taxid = [];
				for(var tc = 0; tc < props[parentName][childName][i].labourtaxid.length;tc++) {
					if(props[parentName][childName][i].taxid.indexOf(props[parentName][childName][i].labourtaxid[tc]) == -1)
						props[parentName][childName][i].taxid.push(props[parentName][childName][i].labourtaxid[tc]);
				}
				for(var tc = 0; tc < props[parentName][childName][i].materialtaxid.length;tc++) {
					if(props[parentName][childName][i].taxid.indexOf(props[parentName][childName][i].materialtaxid[tc]) == -1)
						props[parentName][childName][i].taxid.push(props[parentName][childName][i].materialtaxid[tc]);
				}
				props[parentName][childName][i].taxdetails = {};


				itemdetailupdateObj[`${childName}[${i}].labourtaxid`] = props[parentName][childName][i].labourtaxid;
				itemdetailupdateObj[`${childName}[${i}].labourtaxdetails`] = props[parentName][childName][i].labourtaxdetails;
				itemdetailupdateObj[`${childName}[${i}].materialtaxid`] = props[parentName][childName][i].materialtaxid;
				itemdetailupdateObj[`${childName}[${i}].materialtaxdetails`] = props[parentName][childName][i].materialtaxdetails;
			}

			itemdetailupdateObj[`${childName}[${i}].taxid`] = props[parentName][childName][i].taxid;

			/****Tax Calculation for taxid in the item array ****/

			if(splitTaxParam && props[parentName][childName][i].splitrate)
				itemsTaxValue = calculateTypeInnerTax(props[parentName][childName][i]);
			else
				itemsTaxValue = calculateInnerTax(props[parentName][childName][i]);

			for (var prop in props[parentName].taxdetails) {
				props[parentName].taxdetails[prop].amount = Number(props[parentName].taxdetails[prop].amount.toFixed(roundOffPrecision));
				props[parentName].taxdetails[prop].taxamount = Number(props[parentName].taxdetails[prop].taxamount.toFixed(roundOffPrecision));
				props[parentName].taxdetails[prop].amountlc = Number((props[parentName].taxdetails[prop].amountlc).toFixed(roundOffPrecision));
				props[parentName].taxdetails[prop].taxamountlc = Number((props[parentName].taxdetails[prop].taxamountlc).toFixed(roundOffPrecision));

			}

			props[parentName][childName][i].taxvalue = Number(itemsTaxValue.toFixed(roundOffPrecision));
			props[parentName][childName][i].amountwithtax = Number((itemsTaxValue + props[parentName][childName][i].amount).toFixed(roundOffPrecision));

			props[parentName][childName][i].totalaftertax = props[parentName][childName][i].taxvalue + props[parentName][childName][i].amount;

			props[parentName][childName][i].totalaftertax = Number(props[parentName][childName][i].totalaftertax.toFixed(roundOffPrecision));

			props[parentName].basictotal = props[parentName].basictotal + props[parentName][childName][i].amount;
			props[parentName].totalaftertax += props[parentName][childName][i].totalaftertax;

			itemdetailupdateObj[`${childName}[${i}].finalrate`] = props[parentName][childName][i].finalrate;
			itemdetailupdateObj[`${childName}[${i}].finalratelc`] = props[parentName][childName][i].finalratelc;
			itemdetailupdateObj[`${childName}[${i}].ratelc`] = props[parentName][childName][i].ratelc;
			itemdetailupdateObj[`${childName}[${i}].amount`] = props[parentName][childName][i].amount;
			itemdetailupdateObj[`${childName}[${i}].amountlc`] = props[parentName][childName][i].amountlc;
			itemdetailupdateObj[`${childName}[${i}].taxdetails`] = props[parentName][childName][i].taxdetails;
			itemdetailupdateObj[`${childName}[${i}].taxvalue`] = props[parentName][childName][i].taxvalue;
			itemdetailupdateObj[`${childName}[${i}].amountwithtax`] = props[parentName][childName][i].amountwithtax;
			itemdetailupdateObj[`${childName}[${i}].totalaftertax`] = props[parentName][childName][i].totalaftertax;
		} else {
			delete props[parentName][childName][i].finalrate;
			delete props[parentName][childName][i].finalratelc;
			delete props[parentName][childName][i].rate;
			delete props[parentName][childName][i].ratelc;
			delete props[parentName][childName][i].quantity;
			delete props[parentName][childName][i].amount;
			delete props[parentName][childName][i].amountlc;
			delete props[parentName][childName][i].taxid;
			delete props[parentName][childName][i].taxdetails;
			delete props[parentName][childName][i].taxvalue;
			delete props[parentName][childName][i].amountwithtax;
			delete props[parentName][childName][i].totalaftertax;
		}
	}

	props.updateFormState(props.form, itemdetailupdateObj);

	props[parentName].taxtotal = (props[parentName].totalaftertax - props[parentName].basictotal);

	var additionalchargeObj = {};
	if(props[parentName].additionalcharges && props[parentName].additionalcharges.length>0) {
		for(var i=0;i<props[parentName].additionalcharges.length;i++) {
			if(props[parentName].additionalcharges[i].defaultpercentage)
				props[parentName].additionalcharges[i].amount = Number(((props[parentName].basictotal * props[parentName].additionalcharges[i].defaultpercentage) / 100).toFixed(roundOffPrecision));

			props[parentName].totalaftertax += props[parentName].additionalcharges[i].amount == null ? 0 : props[parentName].additionalcharges[i].amount;
			
			var itemsTaxValue = 0;
			props[parentName].additionalcharges[i].taxid = (props[parentName].additionalcharges[i].taxcodeid) ? [props[parentName].additionalcharges[i].taxcodeid] : [];
			props[parentName].additionalcharges[i].taxdetails = {};

			if(props[parentName].additionalcharges[i].amount != null)
				itemsTaxValue = calculateInnerTax(props[parentName].additionalcharges[i]);

			for (var prop in props[parentName].taxdetails) {
				props[parentName].taxdetails[prop].amount = Number(props[parentName].taxdetails[prop].amount.toFixed(roundOffPrecision));
				props[parentName].taxdetails[prop].taxamount = Number(props[parentName].taxdetails[prop].taxamount.toFixed(roundOffPrecision));
				props[parentName].taxdetails[prop].amountlc = Number((props[parentName].taxdetails[prop].amountlc).toFixed(roundOffPrecision));
				props[parentName].taxdetails[prop].taxamountlc = Number((props[parentName].taxdetails[prop].taxamountlc).toFixed(roundOffPrecision));

			}
			props[parentName].additionalcharges[i].taxvalue = Number(itemsTaxValue.toFixed(roundOffPrecision));
			props[parentName].additionalcharges[i].amountwithtax = Number((itemsTaxValue + props[parentName].additionalcharges[i].amount).toFixed(roundOffPrecision));

			props[parentName].additionalcharges[i].totalaftertax = props[parentName].additionalcharges[i].taxvalue + props[parentName].additionalcharges[i].amount;

			props[parentName].additionalcharges[i].totalaftertax = Number(props[parentName].additionalcharges[i].totalaftertax.toFixed(roundOffPrecision));

			props[parentName].totalaftertax += props[parentName].additionalcharges[i].taxvalue;
			props[parentName].taxtotal += props[parentName].additionalcharges[i].taxvalue;

			additionalchargeObj[`additionalcharges[${i}].amount`] = props[parentName].additionalcharges[i].amount;
			additionalchargeObj[`additionalcharges[${i}].taxid`] = props[parentName].additionalcharges[i].taxid;
			additionalchargeObj[`additionalcharges[${i}].taxdetails`] = props[parentName].additionalcharges[i].taxdetails;
			additionalchargeObj[`additionalcharges[${i}].taxvalue`] = props[parentName].additionalcharges[i].taxvalue;
			additionalchargeObj[`additionalcharges[${i}].amountwithtax`] = props[parentName].additionalcharges[i].amountwithtax;
			additionalchargeObj[`additionalcharges[${i}].totalaftertax`] = props[parentName].additionalcharges[i].totalaftertax;
		}
	}

	props.updateFormState(props.form, additionalchargeObj);

	function calculateInnerTax(prime) {
		var itemsTempTaxValue = 0;
		var tempdisplayOrder = 0;

		for(var j=0;j<prime.taxid.length;j++) {
			var temptaxid = prime.taxid[j]
			prime.taxdetails[temptaxid] = {};
			var taxValue = 0,
			baseAmountArray = [];
			baseAmountArray.push(prime.amount);
			if(!taxmaster[temptaxid].isactive) {
				errorArr.push({id: temptaxid, taxcode: taxmaster[temptaxid].taxcode});
			}

			for (var k = 0; k < taxmaster[temptaxid].template.length; k++) {
				var templateAmount = 0;
				for (var l = 0; l < taxmaster[temptaxid].template[k].basearray.length; l++) {
					templateAmount += baseAmountArray[taxmaster[temptaxid].template[k].basearray[l]];
				}
				templateAmount = templateAmount * taxmaster[temptaxid].template[k].basefactor;

				/***Calculating Tax value for Different Type****/
				var templateTaxValue = (taxmaster[temptaxid].template[k].type == 'Percentage') ? (templateAmount * (taxmaster[temptaxid].template[k].value / 100)) : taxmaster[temptaxid].template[k].value;
				baseAmountArray.push(templateTaxValue);
				taxValue += Number(templateTaxValue.toFixed(roundOffPrecision));
				var templateAmountlc,
				templateTaxValuelc;

				if (props[parentName].currencyid == defaultCurrency) {
					templateAmountlc = Number(templateAmount.toFixed(roundOffPrecision));
					templateTaxValuelc = Number(templateTaxValue.toFixed(roundOffPrecision));
				} else {
					if (props[parentName].currencyexchangerate) {
						templateAmountlc = Number(templateAmount.toFixed(roundOffPrecision)) * props[parentName].currencyexchangerate;
						templateTaxValuelc = Number(templateTaxValue.toFixed(roundOffPrecision)) * props[parentName].currencyexchangerate;
					} else {
						templateAmountlc = 0;
						templateTaxValuelc = 0;
					}
				}

				if (prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid]) {
					prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amount += templateAmount;

					prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamount += templateTaxValue;

					prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amountlc += templateAmountlc;

					prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc += templateTaxValuelc;

					prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amount = Number(prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amount.toFixed(roundOffPrecision));

					prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamount = Number(prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamount.toFixed(roundOffPrecision));

					prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amountlc = Number(prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amountlc.toFixed(roundOffPrecision));

					prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc = Number(prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc.toFixed(roundOffPrecision));
				} else {
					prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid] = {
						id : taxmaster[temptaxid].template[k].taxratemasterid,
						taxvalue : taxmaster[temptaxid].template[k].value,
						description : taxmaster[temptaxid].template[k].description,
						accountid : taxmaster[temptaxid].template[k].accountid,
						amount : Number(templateAmount.toFixed(roundOffPrecision)),
						taxamount : Number(templateTaxValue.toFixed(roundOffPrecision)),
						amountlc : Number(templateAmountlc.toFixed(roundOffPrecision)),
						taxamountlc : Number(templateTaxValuelc.toFixed(roundOffPrecision))
					};
				}

				if (props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid]) {
					props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid].amount += Number(templateAmount.toFixed(roundOffPrecision));
					props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid].taxamount += Number(templateTaxValue.toFixed(roundOffPrecision));
					props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid].amountlc += Number(templateAmountlc.toFixed(roundOffPrecision));
					props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc += Number(templateTaxValuelc.toFixed(roundOffPrecision));
				} else {
					props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid] = {
						id : taxmaster[temptaxid].template[k].taxratemasterid,
						taxvalue : taxmaster[temptaxid].template[k].value,
						description : taxmaster[temptaxid].template[k].description,
						accountid : taxmaster[temptaxid].template[k].accountid,
						amount : Number(templateAmount.toFixed(roundOffPrecision)),
						displayOrder : tempdisplayOrder + 1,
						taxamount : Number(templateTaxValue.toFixed(roundOffPrecision)),
						amountlc : Number(templateAmountlc.toFixed(roundOffPrecision)),
						taxamountlc : Number(templateTaxValuelc.toFixed(roundOffPrecision))
					};
					tempdisplayOrder++;
				}
			}
			itemsTempTaxValue += taxValue;
		}
		return itemsTempTaxValue;
	}

	function calculateTypeInnerTax(prime) {
		var itemsTempTaxValue = 0;
		var tempdisplayOrder = 0;
		var splitObj = {
			'labour' : 'labour',
			'material' : 'material'
		};
		for(var splitprop in splitObj) {
			for(var j=0;j<prime[splitprop+'taxid'].length;j++) {
				var temptaxid = prime[splitprop+'taxid'][j];
				prime[splitprop+'taxdetails'][temptaxid] = {};
				if(!prime.taxdetails[temptaxid])
					prime.taxdetails[temptaxid] = {};

				var taxValue = 0,
				baseAmountArray = [];
				baseAmountArray.push(prime[splitprop+'amount']);

				if(!taxmaster[temptaxid].isactive) {
					errorArr.push({id: temptaxid, taxcode: taxmaster[temptaxid].taxcode});
				}

				for (var k = 0; k < taxmaster[temptaxid].template.length; k++) {
					var templateAmount = 0;
					for (var l = 0; l < taxmaster[temptaxid].template[k].basearray.length; l++) {
						templateAmount += baseAmountArray[taxmaster[temptaxid].template[k].basearray[l]];
					}
					templateAmount = templateAmount * taxmaster[temptaxid].template[k].basefactor;

					/***Calculating Tax value for Different Type****/
					var templateTaxValue = (taxmaster[temptaxid].template[k].type == 'Percentage') ? (templateAmount * (taxmaster[temptaxid].template[k].value / 100)) : taxmaster[temptaxid].template[k].value;
					baseAmountArray.push(templateTaxValue);
					taxValue += Number(templateTaxValue.toFixed(roundOffPrecision));
					var templateAmountlc,
					templateTaxValuelc;

					if (props[parentName].currencyid == defaultCurrency) {
						templateAmountlc = Number(templateAmount.toFixed(roundOffPrecision));
						templateTaxValuelc = Number(templateTaxValue.toFixed(roundOffPrecision));
					} else {
						if (props[parentName].currencyexchangerate) {
							templateAmountlc = Number(templateAmount.toFixed(roundOffPrecision)) * props[parentName].currencyexchangerate;
							templateTaxValuelc = Number(templateTaxValue.toFixed(roundOffPrecision)) * props[parentName].currencyexchangerate;
						} else {
							templateAmountlc = 0;
							templateTaxValuelc = 0;
						}
					}

					if (prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid]) {
						prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amount += templateAmount;

						prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamount += templateTaxValue;

						prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amountlc += templateAmountlc;

						prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc += templateTaxValuelc;

						prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amount = Number(prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amount.toFixed(roundOffPrecision));

						prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamount = Number(prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamount.toFixed(roundOffPrecision));

						prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amountlc = Number(prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amountlc.toFixed(roundOffPrecision));

						prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc = Number(prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc.toFixed(roundOffPrecision));
					} else {
						prime[splitprop+'taxdetails'][temptaxid][taxmaster[temptaxid].template[k].taxratemasterid] = {
							id : taxmaster[temptaxid].template[k].taxratemasterid,
							taxvalue : taxmaster[temptaxid].template[k].value,
							description : taxmaster[temptaxid].template[k].description,
							accountid : taxmaster[temptaxid].template[k].accountid,
							amount : Number(templateAmount.toFixed(roundOffPrecision)),
							taxamount : Number(templateTaxValue.toFixed(roundOffPrecision)),
							amountlc : Number(templateAmountlc.toFixed(roundOffPrecision)),
							taxamountlc : Number(templateTaxValuelc.toFixed(roundOffPrecision))
						};
					}
					
					if (prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid]) {
						prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amount += templateAmount;

						prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamount += templateTaxValue;

						prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amountlc += templateAmountlc;

						prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc += templateTaxValuelc;

						prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amount = Number(prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amount.toFixed(roundOffPrecision));

						prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamount = Number(prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamount.toFixed(roundOffPrecision));

						prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amountlc = Number(prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].amountlc.toFixed(roundOffPrecision));

						prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc = Number(prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc.toFixed(roundOffPrecision));
					} else {
						prime.taxdetails[temptaxid][taxmaster[temptaxid].template[k].taxratemasterid] = {
							id : taxmaster[temptaxid].template[k].taxratemasterid,
							taxvalue : taxmaster[temptaxid].template[k].value,
							description : taxmaster[temptaxid].template[k].description,
							accountid : taxmaster[temptaxid].template[k].accountid,
							amount : Number(templateAmount.toFixed(roundOffPrecision)),
							taxamount : Number(templateTaxValue.toFixed(roundOffPrecision)),
							amountlc : Number(templateAmountlc.toFixed(roundOffPrecision)),
							taxamountlc : Number(templateTaxValuelc.toFixed(roundOffPrecision))
						};
					}

					if (props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid]) {
						props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid].amount += Number(templateAmount.toFixed(roundOffPrecision));
						props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid].taxamount += Number(templateTaxValue.toFixed(roundOffPrecision));
						props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid].amountlc += Number(templateAmountlc.toFixed(roundOffPrecision));
						props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid].taxamountlc += Number(templateTaxValuelc.toFixed(roundOffPrecision));
					} else {
						props[parentName].taxdetails[taxmaster[temptaxid].template[k].taxratemasterid] = {
							id : taxmaster[temptaxid].template[k].taxratemasterid,
							taxvalue : taxmaster[temptaxid].template[k].value,
							description : taxmaster[temptaxid].template[k].description,
							accountid : taxmaster[temptaxid].template[k].accountid,
							amount : Number(templateAmount.toFixed(roundOffPrecision)),
							displayOrder : tempdisplayOrder + 1,
							taxamount : Number(templateTaxValue.toFixed(roundOffPrecision)),
							amountlc : Number(templateAmountlc.toFixed(roundOffPrecision)),
							taxamountlc : Number(templateTaxValuelc.toFixed(roundOffPrecision))
						};
						tempdisplayOrder++;
					}
				}
				itemsTempTaxValue += taxValue;
			}
		}
		
		return itemsTempTaxValue;
	}

	var temptotalObj = {};

	temptotalObj[`taxdetails`] = props[parentName].taxdetails;
	temptotalObj[`basictotal`] = Number(props[parentName].basictotal.toFixed(roundOffPrecision));
	temptotalObj[`taxtotal`] = Number(props[parentName].taxtotal.toFixed(roundOffPrecision));
	props[parentName].totalaftertax = Number(props[parentName].totalaftertax.toFixed(roundOffPrecision));

	var roundOffvalue;
	var value = props[parentName].totalaftertax.toString().split('.');
	if (props[parentName]['roundoffmethod'] == "none")
		roundOffvalue = props[parentName].totalaftertax;
	else if (props[parentName]['roundoffmethod'] == "up")
		roundOffvalue = Number(value[0]) + (Number(value[1]) > 0 ? 1 : 0);
	else if (props[parentName]['roundoffmethod'] == "down")
		roundOffvalue = Number(value[0]);
	else if (props[parentName]['roundoffmethod'] == "even")
		roundOffvalue = Math.round(props[parentName].totalaftertax);
	else {
		props[parentName]['roundoffmethod'] = (props.app.defaultRoundOffMethod ? props.app.defaultRoundOffMethod : "even");
		roundOffvalue = Math.round(props[parentName].totalaftertax);
	}
	props[parentName]['roundoffvalue'] = Number((Number(roundOffvalue) - Number(props[parentName].totalaftertax)).toFixed(props.app.roundOffPrecision));
	props[parentName].finaltotal = roundOffvalue;

	if (props[parentName].currencyid == defaultCurrency)
		props[parentName].finaltotallc = props[parentName].finaltotal;
	else {
		if (props[parentName].currencyexchangerate) {
			props[parentName].finaltotallc = props[parentName].finaltotal * props[parentName].currencyexchangerate;
			props[parentName].finaltotallc = Number(props[parentName].finaltotallc.toFixed(props.app.roundOffPrecision));
		} else
			props[parentName].finaltotallc = 0;
	}
	props[parentName].finaltotalinwords = commonMethods.numberToWord(props.app, props[parentName].finaltotal, props[parentName].currencyid);
	props[parentName].finaltotalinwordslc = commonMethods.numberToWord(props.app, props[parentName].finaltotallc);

	temptotalObj[`finaltotallc`] = props[parentName].finaltotallc;
	temptotalObj[`finaltotal`] = props[parentName].finaltotal;
	temptotalObj[`roundoffvalue`] = props[parentName].roundoffvalue;
	temptotalObj[`roundoffmethod`] = props[parentName].roundoffmethod;
	temptotalObj[`totalaftertax`] = props[parentName].totalaftertax;
	temptotalObj[`finaltotalinwords`] = props[parentName].finaltotalinwords;
	temptotalObj[`finaltotalinwordslc`] = props[parentName].finaltotalinwordslc;

	props.updateFormState(props.form, temptotalObj);

	if(errorArr.length > 0 && !props.app.inactivetaxerrorthrown) {
		props.updateAppState('inactivetaxerrorthrown', true);
		var errorMessage = "The following taxes are selected in this transaction, but they are inactive. Please select active taxes to avoid errors!";

		let bodyArray = [];
		for (var i = 0; i < errorArr.length; i++) {
			bodyArray.push(errorArr[i].taxcode);
		}

		props.openModal(modalService['confirmMethod']({
			header: 'Error',
			body: errorMessage,
			bodyArray: bodyArray,
			btnTitle: "Do you want to deselect this tax",
			btnArray: ['Yes', 'No']
		}, function(param) {
			if(param) {
				var splitObj = {
					'labour' : 'labour',
					'material' : 'material'
				};
				for (var i = 0; i < props[parentName][childName].length; i++) {
					if(props[parentName][childName][i].taxid.length > 0) {
						for(var j=0;j<props[parentName][childName][i].taxid.length;j++) {
							for(var k=0;k<errorArr.length;k++) {
								if(props[parentName][childName][i].taxid[j] == errorArr[k].id) {
									props[parentName][childName][i].taxid.splice(j, 1);
								}
							}
						}
					}

					for(var splitprop in splitObj) {
						if(props[parentName][childName][i][splitprop+'taxid'] && props[parentName][childName][i][splitprop+'taxid'].length > 0) {
							for(var p=0;p<props[parentName][childName][i][splitprop+'taxid'].length;p++) {
								for(var q=0;q<errorArr.length;q++) {
									if(props[parentName][childName][i][splitprop+'taxid'][p] == errorArr[q].id) {
										props[parentName][childName][i][splitprop+'taxid'].splice(p, 1);
									}
								}
							}
						}
					}
				}

				for(var l=0;l<props[parentName].additionalcharges.length;l++) {
					for(var m=0;m<errorArr.length;m++) {
						if(props[parentName].additionalcharges[l].taxcodeid == errorArr[m].id) {
							props[parentName].additionalcharges[l].taxcodeid = null;
						}
					}
				}
				taxEngine(props, parentName, childName);
			}
		}));
	}
}

export function checkTransactionExist(destinationresource, sourceresource, resourceid, openModal, callbackfn) {
	axios.get(`/api/checktransactionexist/${destinationresource}/${sourceresource}/${resourceid}`).then((response)=> {
		if(response.data.message == 'success') {
			if(response.data.main.length > 0) {
				let resourcedetails = response.data.main;
				return openModal({render: (closeModal) => {return <CheckTransactionExistModal resourcedetails = {resourcedetails} closeModal={closeModal} callback={(param)=>{callbackfn(param);}} />}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}});
			} else {
				callbackfn(true);
			}
		} else {
			var apiResponse = commonMethods.apiResult(response);
			openModal(modalService[apiResponse.methodName](apiResponse.message));
			callbackfn(false);
		}
	});
}

export function pageValidation(props) {
	let errArray = [], childArray = [];

	let options = utils.getPageOptions(props.match.path);
	let pagejson = props.pagejson[options.title] && props.pagejson[options.title][options.type] ? props.pagejson[options.title][options.type] : null;

	if(pagejson) {
		for (var i = 0; i < pagejson.sections.length; i++) {
			for (var j = 0; j < pagejson.sections[i].fields.length; j++) {
				if (pagejson.sections[i].fields[j].type == 'repeatabletable') {
					if (pagejson[pagejson.sections[i].fields[j].json].body && pagejson[pagejson.sections[i].fields[j].json].body.length > 0) {
						let childtitle = checkCondition('value', pagejson.sections[i].title);
						childArray.push({
							array : pagejson.sections[i].fields[j].json,
							title : childtitle
						});
					}
				} else if(pagejson.sections[i].fields[j].type == 'button' && pagejson.sections[i].fields[j].enablevalidate  && pagejson.sections[i].fields[j].validateobj) {
					childArray.push({
						array : pagejson.sections[i].fields[j].validateobj,
						isObj: true,
						title : pagejson.sections[i].fields[j].validatetitle
					});
				}
			}
		}
	}

	for (var i = 0; i < childArray.length; i++) {
		validateChild(childArray[i]);
	}

	function validateChild(childObj, parentObj, parentrowindex, parentitemobj) {
		let parentStr = parentObj ? `${parentObj.itemstr}.` : ``;
		let arrayModelStr = `{resource.${parentStr}${childObj.array}}`;

		let childitemarray = checkCondition('value', arrayModelStr);
		if(childObj.isObj)
			childitemarray = [childitemarray];

		let subitemarray = pagejson[childObj.array].subitemarray ? pagejson[childObj.array].subitemarray : [];

		if (childitemarray && childitemarray.length > 0) {
			for (var j = 0; j < childitemarray.length; j++) {
				for(var k = 0; k < pagejson[childObj.array].body.length; k++) {
					if((checkCondition('required', pagejson[childObj.array].body[k].required, childitemarray[j], parentitemobj) || pagejson[childObj.array].body[k].required==true || pagejson[childObj.array].body[k].required=="true")) {
						if(!pagejson[childObj.array].body[k].if || (pagejson[childObj.array].body[k].if && checkCondition('if', pagejson[childObj.array].body[k].if, childitemarray[j], parentitemobj))) {

							let fieldName = `${pagejson[childObj.array].body[k].model}`;
							let modelStr = `{resource.${parentStr}${childObj.array}[${j}].${fieldName}}`;
							let model = checkCondition('value', modelStr, childitemarray[j], parentitemobj);

							let title = checkCondition('value', pagejson[childObj.array].body[k].title, childitemarray[j], parentitemobj);

							let parentErrStr = parentObj ? ` for ${parentObj.title} row-${parentObj.index+1}` : ``;

							if(pagejson[childObj.array].body[k].type == 'number' || pagejson[childObj.array].body[k].type == 'selectauto' || pagejson[childObj.array].body[k].type == 'inputbuttongroup' || pagejson[childObj.array].body[k].type == 'time') {
								if(typeof(model) != 'number') {
									errArray.push(`${childObj.title} Row-${j+1} ${title} is required ${parentErrStr}`);
								}
							} else if(pagejson[childObj.array].body[k].type == 'date') {
								if(!model || new Date(model) == "Invalid Date") {
									errArray.push(`${childObj.title} Row-${j+1} ${title} is required ${parentErrStr}`);
								}
							}  else if(pagejson[childObj.array].body[k].type == 'select' && (pagejson[childObj.array].body[k].multiselect == true || pagejson[childObj.array].body[k].multiselect == "true")) {
								if(model && model.length == 0) {
									errArray.push(`${childObj.title} Row-${j+1} ${title} is required ${parentErrStr}`);
								}
							} else {
								if(model == null || model == 'undefined') {
									errArray.push(`${childObj.title} Row-${j+1} ${title} is required ${parentErrStr}`);
								}
							}
						}
					}
				}
				subitemarray.forEach((subitem) => {
					let parentitemobj = childObj.isObj ? `{resource.${childObj.array}}` : `{resource.${parentStr}${childObj.array}[${j}]}`;
					validateChild({
						array: subitem,
						title: pagejson[subitem].title
					}, {
						itemstr : childObj.isObj ? `${childObj.array}` : `${parentStr}${childObj.array}[${j}]`,
						index: j,
						title: childObj.title
					}, parentitemobj);
				});
			}
		}
	}

	function checkCondition(type, condition, itemobj, parentitemobj) {
		let app = props.app;
		let feature = props.app.feature;
		let user = props.app.user;
		let resource = props.resource;
		let item = itemobj;
		let parentitem = parentitemobj;

		if(type == 'if')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 || (['string', 'boolean'].indexOf(typeof(condition)) >= 0 && eval(`try{${condition}}catch(e){}`));
		if(type == 'hide')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? 'hide' : '');
		if(type == 'show')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? '' : 'hide');
		if(type == 'required')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'disabled')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'value') {
			if(typeof(condition) == 'string') {
				if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
					return eval(`try{${condition}}catch(e){}`);
				else
					return condition;
			}
			return condition;
		}

		return null;
	}

	if (errArray.length > 0) {
		var apiResponse = commonMethods.apiResult({
			data : {
				message : "Validation Error !!!",
				error : errArray
			}
		});
		props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		return true;
	}
	return false;
}


export const detailsSVC = {
   	getPrimaryAddress : function(feature, parentid, callback) {
		if(parentid) {
			let addressArray = [];

			axios.get(`/api/addresses?field=id,title,displayaddress,isprimary,firstline,streetid,areaid,cityid,stateid,countryid,streets/name/streetid,areas/name/areaid,areas/pincode/areaid,cities/name/cityid,states/name/stateid,countries/name/countryid&filtercondition=addresses.id=${parentid}`).then((response) => {
				if(response.data.message == 'success') {
					if(feature.useMasterForAddresses) {
						for (var i = 0; i < response.data.main.length; i++) {
							let addressData = '';
							addressData = (response.data.main[i].firstline) ? addressData + (response.data.main[i].firstline) : addressData + '';
							addressData = (response.data.main[i].streetid_name) ? addressData + ',' + (response.data.main[i].streetid_name) : addressData + '';
							addressData = (response.data.main[i].areaid_name) ? addressData + ',\n' + (response.data.main[i].areaid_name) : addressData + '';
							addressData = (response.data.main[i].cityid_name) ? addressData + ',' + (response.data.main[i].cityid_name) : addressData + '';
							addressData = (response.data.main[i].stateid_name) ? addressData + ',\n' + (response.data.main[i].stateid_name) : addressData + '';
							addressData = (response.data.main[i].areaid_pincode) ? addressData + ',' + (response.data.main[i].areaid_pincode) : addressData + '';
							response.data.main[i].displayaddress = (addressData != '') ? addressData : response.data.main[i].displayaddress;
						}
					}
					addressArray = response.data.main;
					for (var i = 0; i < addressArray.length; i++) {
						if(addressArray[i].isprimary) {
							callback(addressArray[i]);
						}
					}
				}
			});
		}

	}
}

export const checkStatus = {
	salesinvoice : function (param) {
		let deferred = Q.defer();
		let urlStr = "orders.id in (";
		for (var i = 0; i < param.deliverynoteitems.length; i++) {
			if (param.deliverynoteitems[i].sourceresource == 'orderitems' && param.deliverynoteitems[i].sourceid > 0) {
				urlStr += param.deliverynoteitems[i].orderitemsid_parentid + ","
			}
		}
		urlStr = urlStr.substr(0, urlStr.length - 1);
		urlStr += ")";
		axios.get(`/api/orders?&field=id,status,orderno&filtercondition=${urlStr}`).then((response) => {
			for (var i = 0; i < param.deliverynoteitems.length; i++) {
				for (var j = 0; j < response.data.main.length; j++) {
					if (param.deliverynoteitems[i].orderitemsid_parentid == response.data.main[j].id) {
						response.data.main[j].itemid_name = param.deliverynoteitems[i].itemid_name;
					}
				}
			}
			deferred.resolve(response.data.main);
		});
		return deferred.promise;
	},

	deliverynote : function (param) {
		let deferred = Q.defer();
		let urlStr = "orders.id in (";
		for (var i = 0; i < param.salesinvoiceitems.length; i++) {
			if (param.salesinvoiceitems[i].sourceresource == 'orderitems' && param.salesinvoiceitems[i].sourceid > 0) {
				urlStr += param.salesinvoiceitems[i].orderitemsid_parentid + ","
			}
		}
		urlStr = urlStr.substr(0, urlStr.length - 1);
		urlStr += ")";
		axios.get(`/api/orders?&field=id,status,orderno&filtercondition=${urlStr}`).then((response) => {
			for (var i = 0; i < param.salesinvoiceitems.length; i++) {
				for (var j = 0; j < response.data.main.length; j++) {
					if (param.salesinvoiceitems[i].orderitemsid_parentid == response.data.main[j].id) {
						response.data.main[j].itemid_name = param.salesinvoiceitems[i].itemid_name;
					}
				}
			}
			deferred.resolve(response.data.main);
		});
		return deferred.promise;
	},

	purchaseinvoice : function (param) {
		let deferred = Q.defer();
		let urlStr = "purchaseorders.id in (";
		for (var i = 0; i < param.receiptnoteitems.length; i++) {
			if (param.receiptnoteitems[i].sourceresource == 'purchaseorderitems' && param.receiptnoteitems[i].sourceid > 0) {
				urlStr += param.receiptnoteitems[i].purchaseorderitemsid_parentid + ","
			}
		}
		urlStr = urlStr.substr(0, urlStr.length - 1);
		urlStr += ")";
		axios.get(`/api/purchaseorders?&field=id,status,ponumber&filtercondition=${urlStr}`).then((response) => {
			for (var i = 0; i < param.receiptnoteitems.length; i++) {
				for (var j = 0; j < response.data.main.length; j++) {
					if (param.receiptnoteitems[i].purchaseorderitemsid_parentid == response.data.main[j].id) {
						response.data.main[j].itemid_name = param.receiptnoteitems[i].itemid_name;
					}
				}
			}
			deferred.resolve(response.data.main);
		});
		return deferred.promise;
	}
}

export function checkAccountBalance (obj, item, itemstr, selector, tranprops) {
	setTimeout(() => {
		if(selector(tranprops.fullstate, itemstr)) {
			tranprops.updateFormState(tranprops.form, {
				[`${itemstr}._checkaccaountbalance`] : obj
			});
		}
	}, 0);
	if(!item._checkaccaountbalance || (item._checkaccaountbalance.companyid != obj.companyid || item._checkaccaountbalance.accountid != obj.accountid || item._checkaccaountbalance.type != obj.type || item._checkaccaountbalance.againstid != obj.againstid || item._checkaccaountbalance.partnerid != obj.partnerid || item._checkaccaountbalance.employeeid != obj.employeeid)) {
		axios.get(`/api/query/getaccountbalancequery?companyid=${obj.companyid}&accountid=${obj.accountid}&type=${obj.type}&againstid=${obj.againstid}&partnerid=${obj.partnerid}&employeeid=${obj.employeeid}`).then((response) => {
			if(response.data.message == 'success') {
				let credit = response.data.main.creditBalance;
				let debit = response.data.main.debitBalance;
				let currency = response.data.main.currency;
				let model = (obj.show == "both" || obj.show == "credit" ? currencyFilter(credit, currency, tranprops.app) : '') + (obj.show == "both" ? " Cr , " : '') + (obj.show == "both" || obj.show == "debit" ? currencyFilter(debit, currency, tranprops.app) : '') + (obj.show=="both" ? " Dr" : '');
				if(selector(tranprops.fullstate, itemstr)) {
					tranprops.updateFormState(tranprops.form, {
						[`${itemstr}.accountbalance`] : model
					});
				}
			}
		});
	}
}

export const accessMenuResource = {
	check : function (resourcename, actionverb, roles, app) {
		var accessFound = false;
		if((app.feature.runInCRMMode && app.myResources[resourcename].isCRMResource) || (!app.feature.runInCRMMode))
			accessFound = true;
		if(resourcename != 'customreportquery' && resourcename != 'customreportjsonquery') {
			if(accessFound) {
				if(actionverb == 'Menu' && checkMustArray(app.user.roleid, app.myResources[resourcename].authorization[actionverb])) {
					return true;
				}
			}
		} else {
			if(checkMustArray(app.user.roleid, roles ? roles : [])) {
				return true;
			}
		} 
		return false;
	},
	checkFeatureAccess: (prop, app) => {
		if(prop.feature!=null && prop.feature!='' && prop.feature!='undefined') {
			if(prop.feature.indexOf(',') == -1)
				return app.feature[prop.feature];
			else {
				let tempFeatureArray = prop.feature.split(',');
				for(var i = 0; i < tempFeatureArray.length; i++) {
					if(app.feature[tempFeatureArray[i]])
						return true;
				}
				return false;
			}
		}
		return true;
	}
}

function checkMustArray(arrayA, arrayB) {
	if (arrayB) {
		for (var i = 0; i < arrayA.length; i++) {
			var itemFound = false;
			for (var j = 0; j < arrayB.length; j++) {
				itemFound = (arrayA[i] == arrayB[j]) ? true : itemFound;
				if (itemFound) {
					return true;
				}
			}
		}
	} else {
		return false;
	}
}

export function generateExcel (reportName, colDefs, dataArray, app, callback, restrictSerialNo) {

	reportName = reportName.replace(/[^A-Za-z0-9_()\s-@]/g, '');

	let reportData = [[{v : ''}, {v : 'Report Name'}, {v : reportName}],[],[{v : ''}, {v : 'Company Name'}, {v : app.selectedCompanyDetails.legalname}], [], []];
	let needFooter = false;
	let widthArray = [];

	if (!restrictSerialNo) {
		reportData[4].push({
			v : 'Sl. No'
		});

		widthArray.push({
			wch : 6
		});
	}

	for (var i = 0; i < colDefs.length; i++) {
		if (!colDefs[i].restrictToExport && colDefs[i].hidden != true) {
			reportData[4].push({
				v : colDefs[i].name
			});
			widthArray.push({
				wch : colDefs[i].width ? colDefs[i].width / 10 : 10
			});
			if (['sum', 'custom'].indexOf(colDefs[i].footertype) >= 0) {
				needFooter = true;

				if (colDefs[i].footertype == 'sum')
					colDefs[i].custom_isSumAggregaion_Total = 0;
			}
		}
	}

	for (var i = 0; i < dataArray.length; i++) {
		let tempData = [];
		if (!restrictSerialNo)
			tempData.push({
				v : i + 1
			});

		for (var j = 0; j < colDefs.length; j++) {
			if (!colDefs[j].restrictToExport && colDefs[j].hidden != true) {
				var value = '',type = '';
				if ((colDefs[j].format == 'date' || colDefs[j].format == 'dateinput') && dataArray[i][colDefs[j].key]) {
					let tempdate = new Date(dataArray[i][colDefs[j].key]);
					value = new Date(tempdate.setTime(tempdate.getTime()+((330/60)*3600*1000))).toString();
					type='d';
				}
				else if (colDefs[j].format == 'datetime')
					value = datetimeFilter(dataArray[i][colDefs[j].key], 'DD/MM/YYYY hh:mm:ss a') || '';
				else if (colDefs[j].format == 'time')
					value = datetimeFilter(dataArray[i][colDefs[j].key], 'hh:mm a') || '';
				else if (colDefs[j].format == 'number')
					value = typeof(dataArray[i][colDefs[j].key]) == 'number' ? Number(dataArray[i][colDefs[j].key].toFixed(app.roundOffPrecision)) : null;
				else if (colDefs[j].format == 'boolean')
					value = typeof(dataArray[i][colDefs[j].key]) == 'boolean' ? (dataArray[i][colDefs[j].key] == true ? 'Yes' : 'No') : null;
				else if (colDefs[j].format == 'taxFilter'){
					value = taxFilter(dataArray[i][colDefs[j].key],app.taxObject) || '';
				} else if(colDefs[j].format == 'array') {
					if (dataArray[i][colDefs[j].key] && Array.isArray(dataArray[i][colDefs[j].key]))
						value = dataArray[i][colDefs[j].key].toString();
					else
						value = '';
				}
				else if (colDefs[j].format == 'location') {
					if (colDefs[j].fieldformat == 'datetime')
						value = datetimeFilter(dataArray[i][colDefs[j].fieldname], 'DD/MM/YYYY hh:mm:ss a') || '';
					else if (colDefs[j].fieldformat == 'time')
						value = datetimeFilter(dataArray[i][colDefs[j].fieldname], 'hh:mm a') || '';
				}else if(colDefs[j].format == 'itemmasterdisplayname'){
					value = itemmasterDisplaynamefilter(dataArray[i][colDefs[j].key],app) || '';
				}
				else
					value = dataArray[i][colDefs[j].key] || '';

				tempData.push({
					v : value,type:type
				});

				if (colDefs[j].footertype == 'sum' && !dataArray[i]['_restrictFromAggregation'])
					colDefs[j].custom_isSumAggregaion_Total += (typeof(dataArray[i][colDefs[j].key]) == 'number' ? Number(dataArray[i][colDefs[j].key].toFixed(app.roundOffPrecision)) : 0);
			}
		}
		reportData.push(tempData);
	}

	if (needFooter) {
		let tempArray = [];
		if (!restrictSerialNo)
			tempArray.push({
				v : ''
			});
		for (var i = 0; i < colDefs.length; i++) {
			if (!colDefs[i].restrictToExport && colDefs[i].hidden != true) {
				if (['sum', 'custom'].indexOf(colDefs[i].footertype) >= 0) {
					tempArray.push({
						v : colDefs[i].footertype == 'sum' ? Number(colDefs[i].custom_isSumAggregaion_Total.toFixed(app.roundOffPrecision)) : (!isNaN(colDefs[i].customfootervalue) ? Number(colDefs[i].customfootervalue) : colDefs[i].customfootervalue)
					});
				} else {
					tempArray.push({
						v : '',
					});
				}
			}
		}
		tempArray[restrictSerialNo ? 0 : 1] = {
			v : 'Total'
		};
		reportData.push(tempArray);
	}

	let wb = {
		SheetNames : [],
		Sheets : {}
	};

	let ws = excel_sheet_from_array_of_arrays(reportData);
	wb.SheetNames.push(reportName);
	wb.Sheets[reportName] = ws;
	ws['!cols'] = widthArray;

	let wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
	saveAs(new Blob([excel_s2ab(wbout)],{type:"application/octet-stream"}), reportName + "_" + (dateFilter(new Date())) + ".xlsx");
	callback();
}

export function accessResource (resourcename, actionverb, app) {
	if ((app.feature.runInCRMMode && app.myResources[resourcename].isCRMResource && checkMustArray(app.user.roleid, app.myResources[resourcename].authorization[actionverb])) || (!app.feature.runInCRMMode && checkMustArray(app.user.roleid, app.myResources[resourcename].authorization[actionverb]))) {
		return true;
	}
	return false;
}

export const biService = {
	getVariableObject : function(reportname, app) {
		let variableObject = {
			quotationitemsreport : {
				quotes : {
					'salesperson' : 'Sales Person',
					'territoryname' : 'Territory',
					'industryid_name' : 'Industry',
					'status' : 'Status',
					'customergroupid_name' : 'Customer Group',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'pricelistid_name' : 'Price List',
					'numberingseriesmasterid_name' : 'Numbering Series',
					'currencyname' : 'Currency',
					'lostreasonid_name' : 'Lost Reason',
					'customer' : 'Customer'
				},
				quoteitems : {
					'itemgroupid_fullname' : 'Item Group',
					'itemcategorymasterid_name' : 'Item Category',
					'customergroupid_name' : 'Customer Group',
					'salesperson' : 'Sales Person',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'itemname' : 'Item Name',
					'territoryname' : 'Territory',
					'industryid_name' : 'Industry',
					'customer' : 'Customer',
					'numberingseriesmasterid_name' : 'Numbering Series'
				}
			},
			orderitemsreport : {
				orders : {
					'salesperson' : 'Sales Person',
					'territoryname' : 'Territory',
					'industryid_name' : 'Industry',
					'status' : 'Status',
					'customergroupid_name' : 'Customer Group',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'pricelistid_name' : 'Price List',
					'numberingseriesmasterid_name' : 'Numbering Series',
					'currencyname' : 'Currency',
					'customer' : 'Customer'
				},
				orderitems : {
					'itemgroupid_fullname' : 'Item Group',
					'itemcategorymasterid_name' : 'Item Category',
					'customergroupid_name' : 'Customer Group',
					'salesperson' : 'Sales Person',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'itemname' : 'Item Name',
					'territoryname' : 'Territory',
					'industryid_name' : 'Industry',
					'customer' : 'Customer',
					'numberingseriesmasterid_name' : 'Numbering Series'
				}
			},
			projectquotationitemsreport : {
				projectquotes : {
					'salesperson' : 'Sales Person',
					'projectname' : 'Project Name',
					'territoryname' : 'Territory',
					'status' : 'Status',
					'customergroupid_name' : 'Customer Group',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'pricelistid_name' : 'Price List',
					'numberingseriesmasterid_name' : 'Numbering Series',
					'currencyname' : 'Currency',
					'lostreasonid_name' : 'Lost Reason',
					'customer' : 'Customer'
				},
				projectquoteitems : {
					'itemgroupid_fullname' : 'Item Group',
					'itemcategorymasterid_name' : 'Item Category',
					'customergroupid_name' : 'Customer Group',
					'salesperson' : 'Sales Person',
					'projectname' : 'Project Name',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'itemname' : 'Item Name',
					'territoryname' : 'Territory',
					'customer' : 'Customer',
					'numberingseriesmasterid_name' : 'Numbering Series'
				}
			},
			salesinvoiceitemsreport : {
				salesinvoices : {
					'salesperson_displayname' : 'Sales Person',
					'territoryname' : 'Territory',
					'status' : 'Status',
					'customergroupid_name' : 'Customer Group',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'pricelistid_name' : 'Price List',
					'numberingseriesmasterid_name' : 'Numbering Series',
					'currencyname' : 'Currency',
					'invoicetype' : 'Invoice Type',
					'customer' : 'Customer',
					'industryid_name' : 'Industry'
				},
				salesinvoiceitems : {
					'itemgroupid_fullname' : 'Item Group',
					'itemcategorymasterid_name' : 'Item Category',
					'customergroupid_name' : 'Customer Group',
					'salesperson_displayname' : 'Sales Person',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'itemid_name' : 'Item Name',
					'territoryname' : 'Territory',
					'customer' : 'Customer',
					'industryid_name' : 'Industry',
					'numberingseriesmasterid_name' : 'Numbering Series'
				}
			},
			projectsreport : {
				projects : {
					'salesperson' : 'Sales Person',
					'projectname' : 'Project Name',
					'territoryname' : 'Territory',
					'status' : 'Status',
					'customergroupid_name' : 'Customer Group',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'pricelistid_name' : 'Price List',
					'numberingseriesmasterid_name' : 'Numbering Series',
					'currencyname' : 'Currency',
					'customer' : 'Customer'
				},
				projectitems : {
					'itemgroupid_fullname' : 'Item Group',
					'itemcategorymasterid_name' : 'Item Category',
					'customergroupid_name' : 'Customer Group',
					'salesperson' : 'Sales Person',
					'projectname' : 'Project Name',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'itemname' : 'Item Name',
					'territoryname' : 'Territory',
					'customer' : 'Customer',
					'numberingseriesmasterid_name' : 'Numbering Series'
				}
			},
			contractitemsreport : {
				contracts : {
					'salesperson' : 'Sales Person',
					'territoryname' : 'Territory',
					'status' : 'Status',
					'customergroupid_name' : 'Customer Group',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'numberingseriesmasterid_name' : 'Numbering Series',
					'currencyname' : 'Currency',
					'customer' : 'Customer'
				},
				contractitems : {
					'itemgroupid_fullname' : 'Item Group',
					'itemcategorymasterid_name' : 'Item Category',
					'customergroupid_name' : 'Customer Group',
					'salesperson' : 'Sales Person',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'equipmentname' : 'Equipment Name',
					'territoryname' : 'Territory',
					'customer' : 'Customer',
					'numberingseriesmasterid_name' : 'Numbering Series'
				}
			},
			leaditemsreport : {
				leads : {
					'salesperson' : 'Sales Person',
					'territoryname' : 'Territory',
					'industryid_name' : 'Industry',
					'status' : 'Status',
					'customergroupid_name' : 'Customer Group',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'pricelistid_name' : 'Price List',
					'numberingseriesmasterid_name' : 'Numbering Series',
					'currencyname' : 'Currency',
					'lostreasonid_name' : 'Lost Reason',
					'customer' : 'Customer',
					'priority' : 'Priority',
					'leadsourcemaster_name' : 'Source',
					'stageid_name' : 'Lead Stage'
				},
				leaditems : {
					'itemgroupid_fullname' : 'Item Group',
					'itemcategorymasterid_name' : 'Item Category',
					'customergroupid_name' : 'Customer Group',
					'salesperson' : 'Sales Person',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'itemname' : 'Item Name',
					'territoryname' : 'Territory',
					'industryid_name' : 'Industry',
					'customer' : 'Customer',
					'leadsourcemaster_name' : 'Source',
					'stageid_name' : 'Lead Stage',
					'numberingseriesmasterid_name' : 'Numbering Series'
				}
			},
			contractenquiryitemsreport : {
				contractenquiries : {
					'salesperson' : 'Sales Person',
					'territoryname' : 'Territory',
					'status' : 'Status',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'numberingseriesmasterid_name' : 'Numbering Series',
					'customergroupid_name' : 'Customer Group',
					'currencyname' : 'Currency',
					'customer' : 'Customer'
				},
				contractenquiryitems : {
					'itemgroupid_fullname' : 'Item Group',
					'itemcategorymasterid_name' : 'Item Category',
					'salesperson' : 'Sales Person',
					'customergroupid_name' : 'Customer Group',
					'quarter' : 'Quarter',
					'month' : 'Month',
					'itemname' : 'Item Name',
					'territoryname' : 'Territory',
					'customer' : 'Customer',
					'numberingseriesmasterid_name' : 'Numbering Series'
				}
			}
		};

		let returnObject = variableObject[reportname];
		if(reportname == 'quotationitemsreport')
			doCustomFieldOperation('quotes', 'quoteitems');
		else if(reportname == 'orderitemsreport')
			doCustomFieldOperation('orders', 'orderitems');
		else if(reportname == 'projectquotationitemsreport')
			doCustomFieldOperation('projectquotes', 'projectquoteitems');
		else if(reportname == 'salesinvoiceitemsreport')
			doCustomFieldOperation('salesinvoices', 'salesinvoiceitems');
		else if(reportname == 'projectsreport')
			doCustomFieldOperation('projects', 'projectitems');
		else if(reportname == 'contractitemsreport')
			doCustomFieldOperation('contracts', 'contractitems');
		else if(reportname == 'leaditemsreport')
			doCustomFieldOperation('leads', 'leaditems');
		else if(reportname == 'contractenquiryitemsreport')
			doCustomFieldOperation('contractenquiries', 'contractenquiryitems');

		function doCustomFieldOperation(parentresource, childresource) {
			let parentCustomFields = commonMethods.getCustomFields(app.myResources, parentresource, 'array');
			for (var i = 0; i < parentCustomFields.length; i++) {
				if (app.myResources[parentresource].fields[parentCustomFields[i]].analytics == 'axis'){
					returnObject[parentresource][parentresource + '_' + (app.myResources[parentresource].fields[parentCustomFields[i]].isForeignKey ? app.myResources[parentresource].fields[parentCustomFields[i]].foreignKeyOptions.mainField.replace('.','_') : parentCustomFields[i])] = app.myResources[parentresource].fields[parentCustomFields[i]].displayName;
					returnObject[childresource][parentresource + '_' + (app.myResources[parentresource].fields[parentCustomFields[i]].isForeignKey ? app.myResources[parentresource].fields[parentCustomFields[i]].foreignKeyOptions.mainField.replace('.','_') : parentCustomFields[i])] = app.myResources[parentresource].fields[parentCustomFields[i]].displayName;
				}
			}
			let childCustomFields = commonMethods.getCustomFields(app.myResources, childresource, 'array');
			for (var i = 0; i < childCustomFields.length; i++) {
				if (app.myResources[childresource].fields[childCustomFields[i]].analytics == 'axis')
					returnObject[childresource][childresource + '_' + (app.myResources[childresource].fields[childCustomFields[i]].isForeignKey ? app.myResources[childresource].fields[childCustomFields[i]].foreignKeyOptions.mainField.replace('.','_') : childCustomFields[i])] = app.myResources[childresource].fields[childCustomFields[i]].displayName;
			}
		}

		return returnObject;
	},
	getOperatorObject : function () {
		return {
			'COUNT' : 'count',
			'SUM' : 'sum',
			'AVG' : 'average',
			'MIN' : 'min',
			'MAX' : 'max'
		};
	},
	getDataObject : function(reportname, app) {
		let dataObject = {
			quotationitemsreport : {
				quotes : {
					'basictotal' : 'Value Before Tax',
					'totalaftertax' : 'Value After Tax',
					'finaltotal' : 'Final Total',
					'finaltotallc' : 'Final Total (Local Currency)'
				},
				quoteitems : {
					'quantity' : 'Quantity',
					'rate' : 'Basic Rate',
					'finalrate' : 'Rate After Discount',
					'amount' : 'Value Before Tax',
					'amountwithtax' : 'Value After Tax',
					'ratelc' : 'Basic Rate (Local Currency)',
					'finalratelc' : 'Rate After Discount (Local Currency)',
					'valuebeforediscount' : 'Value Before Discount',
					'discountvalue' : 'Discount Value'
				}
			},
			orderitemsreport : {
				orders : {
					'basictotal' : 'Value Before Tax',
					'totalaftertax' : 'Value After Tax',
					'finaltotal' : 'Final Total',
					'finaltotallc' : 'Final Total (Local Currency)'
				},
				orderitems : {
					'quantity' : 'Quantity',
					'rate' : 'Basic Rate',
					'finalrate' : 'Rate After Discount',
					'pendingdeliveryqty' : 'Pending Delivery Qty',
					'pendingdeliveryvalue' : 'Pending Delivery Value',
					'pendingdeliveryvaluelc' : 'Pending Delivery Value (Local Currency)',
					'amount' : 'Value Before Tax',
					'amountwithtax' : 'Value After Tax',
					'ratelc' : 'Basic Rate (Local Currency)',
					'finalratelc' : 'Rate After Discount (Local Currency)',
					'valuebeforediscount' : 'Value Before Discount',
					'discountvalue' : 'Discount Value'
				}
			},
			projectquotationitemsreport : {
				projectquotes : {
					'basictotal' : 'Value Before Tax',
					'totalaftertax' : 'Value After Tax',
					'finaltotal' : 'Final Total',
					'finaltotallc' : 'Final Total (Local Currency)'
				},
				projectquoteitems : {
					'quantity' : 'Quantity',
					'rate' : 'Basic Rate',
					'finalrate' : 'Rate After Discount',
					'amount' : 'Value Before Tax',
					'amountwithtax' : 'Value After Tax',
					'ratelc' : 'Basic Rate (Local Currency)',
					'finalratelc' : 'Rate After Discount (Local Currency)',
					'valuebeforediscount' : 'Value Before Discount',
					'discountvalue' : 'Discount Value'
				}
			},
			salesinvoiceitemsreport : {
				salesinvoices : {
					'basictotal' : 'Value Before Tax',
					'totalaftertax' : 'Value After Tax',
					'finaltotal' : 'Final Total',
					'outstandingamount' : 'Outstanding Amount',
					'finaltotallc' : 'Final Total (Local Currency)'
				},
				salesinvoiceitems : {
					'quantity' : 'Quantity',
					'rate' : 'Basic Rate',
					'amount' : 'Value Before Tax',
					'amountwithtax' : 'Value After Tax',
					'ratelc' : 'Basic Rate (Local Currency)',
					'finalratelc' : 'Rate After Discount (Local Currency)',
					'valuebeforediscount' : 'Value Before Discount',
					'discountvalue' : 'Discount Value'
				}
			},
			projectsreport : {
				projects : {
					'basictotal' : 'Value Before Tax',
					'totalaftertax' : 'Value After Tax',
					'finaltotal' : 'Final Total',
					'finaltotallc' : 'Final Total (Local Currency)'
				},
				projectitems : {
					'quantity' : 'Quantity',
					'rate' : 'Basic Rate',
					'amount' : 'Value Before Tax',
					'amountwithtax' : 'Value After Tax',
					'ratelc' : 'Basic Rate (Local Currency)',
					'finalratelc' : 'Rate After Discount (Local Currency)',
					'valuebeforediscount' : 'Value Before Discount',
					'discountvalue' : 'Discount Value'
				}
			},
			contractitemsreport : {
				contracts : {
					'basictotal' : 'Value Before Tax',
					'totalaftertax' : 'Value After Tax',
					'finaltotal' : 'Final Total',
					'finaltotallc' : 'Final Total (Local Currency)'
				},
				contractitems : {
					'rate' : 'Basic Rate',
					'amount' : 'Value Before Tax',
					'amountwithtax' : 'Value After Tax',
					'ratelc' : 'Basic Rate (Local Currency)',
					'finalratelc' : 'Rate After Discount (Local Currency)',
					'valuebeforediscount' : 'Value Before Discount',
					'discountvalue' : 'Discount Value'
				}
			},
			leaditemsreport : {
				leads : {
					'potentialrevenue' : 'Potential Revenue',
					'basictotal' : 'Value Before Tax',
					'totalaftertax' : 'Value After Tax',
					'finaltotal' : 'Final Total',
					'finaltotallc' : 'Final Total (Local Currency)'
				},
				leaditems : {
					'quantity' : 'Quantity',
					'rate' : 'Basic Rate',
					'finalrate' : 'Rate After Discount',
					'amount' : 'Value Before Tax',
					'amountwithtax' : 'Value After Tax',
					'ratelc' : 'Basic Rate (Local Currency)',
					'finalratelc' : 'Rate After Discount (Local Currency)',
					'valuebeforediscount' : 'Value Before Discount',
					'discountvalue' : 'Discount Value'
				}
			},
			contractenquiryitemsreport : {
				contractenquiries : {
					'basictotal' : 'Value Before Tax',
					'totalaftertax' : 'Value After Tax',
					'finaltotal' : 'Final Total',
					'finaltotallc' : 'Final Total (Local Currency)'
				},
				contractenquiryitems : {
					'rate' : 'Basic Rate',
					'amount' : 'Value Before Tax',
					'amountwithtax' : 'Value After Tax',
					'ratelc' : 'Basic Rate (Local Currency)',
					'finalratelc' : 'Rate After Discount (Local Currency)',
					'valuebeforediscount' : 'Value Before Discount',
					'discountvalue' : 'Discount Value'
				}
			}
		};

		let returnObject = dataObject[reportname];
		if(reportname == 'quotationitemsreport')
			doCustomFieldOperation('quotes', 'quoteitems');
		else if(reportname == 'orderitemsreport')
			doCustomFieldOperation('orders', 'orderitems');
		else if(reportname == 'projectquotationitemsreport')
			doCustomFieldOperation('projectquotes', 'projectquoteitems');
		else if(reportname == 'salesinvoiceitemsreport')
			doCustomFieldOperation('salesinvoices', 'salesinvoiceitems');
		else if(reportname == 'projectsreport')
			doCustomFieldOperation('projects', 'projectitems');
		else if(reportname == 'contractitemsreport')
			doCustomFieldOperation('contracts', 'contractitems');
		else if(reportname == 'leaditemsreport')
			doCustomFieldOperation('leads', 'leaditems');
		else if(reportname == 'contractenquiryitemsreport')
			doCustomFieldOperation('contractenquiries', 'contractenquiryitems');

		function doCustomFieldOperation(parentresource, childresource) {
			let parentCustomFields = commonMethods.getCustomFields(app.myResources, parentresource, 'array');
			for (var i = 0; i < parentCustomFields.length; i++){
				if (app.myResources[parentresource].fields[parentCustomFields[i]].analytics == 'data')
					returnObject[parentresource][parentresource + '_' + (app.myResources[parentresource].fields[parentCustomFields[i]].isForeignKey ? app.myResources[parentresource].fields[parentCustomFields[i]].foreignKeyOptions.mainField.replace('.','_') : parentCustomFields[i])] = app.myResources[parentresource].fields[parentCustomFields[i]].displayName;
			}
			let childCustomFields = commonMethods.getCustomFields(app.myResources, childresource, 'array');
			for (var i = 0; i < childCustomFields.length; i++){
				if (app.myResources[childresource].fields[childCustomFields[i]].analytics == 'data')
					returnObject[childresource][childresource + '_' + (app.myResources[childresource].fields[childCustomFields[i]].isForeignKey ? app.myResources[childresource].fields[childCustomFields[i]].foreignKeyOptions.mainField.replace('.','_') : childCustomFields[i])] = app.myResources[childresource].fields[childCustomFields[i]].displayName;
			}
		}
		return returnObject;
	},
	sortByPeriod : function (reportArray, period) {
		let periodArray = [];
		reportArray.forEach((item) => {
			if (periodArray.indexOf(item[period]) < 0)
				periodArray.push(item[period]);
		});

		periodArray.sort((a, b) => {
			let date1 = new Date(),
			date2 = new Date(),
			months = {
				Jan : 0,
				Feb : 1,
				Mar : 2,
				Apr : 3,
				May : 4,
				Jun : 5,
				Jul : 6,
				Aug : 7,
				Sep : 8,
				Oct : 9,
				Nov : 10,
				Dec : 12
			};

			if (period == 'Quarter') {
				a = a.split(' To ')[0];
				b = b.split(' To ')[0];
			}

			let dateArray1 = a.split('-'),
			dateArray2 = b.split('-');

			date1.setDate(1);
			date1.setFullYear(dateArray1[1]);
			date1.setMonth(months[dateArray1[0]]);
			date2.setDate(1);
			date2.setFullYear(dateArray2[1]);
			date2.setMonth(months[dateArray2[0]]);

			return date1 - date2;
		});

		return periodArray;
	}
}
