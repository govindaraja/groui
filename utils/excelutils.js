import XLSX from 'xlsx';
import _ from 'lodash';

function datenum(v, date1904) {
	if(date1904) v+=1462;
	var epoch = Date.parse(v);
	return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
}

export function excel_sheet_from_array_of_arrays(data, opts) {
	var ws = {};
	var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
	for(var R = 0; R != data.length; ++R) {
		for(var C = 0; C != data[R].length; ++C) {
			if(range.s.r > R) range.s.r = R;
			if(range.s.c > C) range.s.c = C;
			if(range.e.r < R) range.e.r = R;
			if(range.e.c < C) range.e.c = C;
			var cell = {v: (typeof data[R][C].v !='object')?data[R][C].v:''};

			if(cell.v == null) continue;
			var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

			if(data[R][C].type=='d' || data[R][C].type=='dt'){
				cell.t = 'd';

			} else {
				if(typeof cell.v === 'number') cell.t = 'n';
				else if(typeof cell.v === 'boolean') cell.t = 'b';
				else if(cell.v instanceof Date) {
					cell.t = 'n'; cell.z = XLSX.SSF._table[14];
					cell.v = datenum(cell.v);
				}
				else cell.t = 's';
			}
			ws[cell_ref] = cell;
		}
	}
	if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
	return ws;
}

export function excel_Workbook() {
	if(!(this instanceof Workbook)) return new Workbook();
	this.SheetNames = [];
	this.Sheets = {};
}

export function excel_s2ab(s) {
	var buf = new ArrayBuffer(s.length);
	var view = new Uint8Array(buf);
	for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
	return buf;
}

export const XLSXReaderUtils = {
	intializeFromFile: function(obj, file, readCells, toJSON, handler) {
		var reader = new FileReader();
		console.log(XLSX)

		reader.onload = function(e) {
			var data = e.target.result;
			var workbook = XLSX.read(data, {
				type: 'binary'
			});

			obj.sheets = XLSXReaderUtils.parseWorkbook(workbook, readCells, toJSON);
			handler(obj);
		}

		reader.readAsBinaryString(file);
	},
	parseWorkbook: function(workbook, readCells, toJSON) {
		if (toJSON === true) {
			return XLSXReaderUtils.to_json(workbook);
		}

		var sheets = {};

		_.forEachRight(workbook.SheetNames, function(sheetName) {
			var sheet = workbook.Sheets[sheetName];
			sheets[sheetName] = XLSXReaderUtils.parseSheet(sheet, readCells);
		});

		return sheets;
	},
	parseSheet: function(sheet, readCells) {
		var range = XLSX.utils.decode_range(sheet['!ref']);
		var sheetData = [];

		if (readCells === true) {
			_.forEachRight(_.range(range.s.r, range.e.r + 1), function(row) {
				var rowData = [];
				_.forEachRight(_.range(range.s.c, range.e.c + 1), function(column) {
					var cellIndex = XLSX.utils.encode_cell({
						'c': column,
						'r': row
					});
					var cell = sheet[cellIndex];
					rowData[column] = cell ? cell.v : undefined;
				});
				sheetData[row] = rowData;
			});
		}

		return {
			'data': sheetData,
			'name': sheet.name,
			'col_size': range.e.c + 1,
			'row_size': range.e.r + 1
		}
	},
	to_json: function(workbook) {
		var result = {};
		workbook.SheetNames.forEach(function(sheetName) {
			var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
			if (roa.length > 0) {
				result[sheetName] = roa;
			}
		});
		return result;
	}
}

export function XLSXReader(file, readCells, toJSON, handler) {
	let obj = {};
	XLSXReaderUtils.intializeFromFile(obj, file, readCells, toJSON, handler);
	return obj;
}