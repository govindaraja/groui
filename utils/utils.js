import React,{Component} from 'react';
import axios from 'axios';
import moment from 'moment';
import routes from '../routeconfig';
import { addValidator } from 'redux-form-validators';
import { ToastContainer, toast as ToastFn, Slide } from 'react-toastify';
import { commonMethods } from './services';

export default module.exports;

export function getPageJson(props, options, callback) {
	if(props.pagejson[options.title] && props.pagejson[options.title][options.type]) {
		callback(props.pagejson[options.title][options.type]);
	} else {
		axios.get(`/api/initialize/initializenewpagejson?title=${options.title}&pagetype=${options.type}`).then(function(response) {
			if(response.data.message == 'success') {
				if(!props.pagejson[options.title])
					props.pagejson[options.title] = {};
				props.pagejson[options.title][options.type] = response.data.pagejson;
				props.updatePageJSONState(options.title, options.type, props.pagejson[options.title][options.type]);
				callback(props.pagejson[options.title][options.type]);
			} else {
				alert(response.data.message);
			}
		});	
	}
}

export function getReportJson(props, title, callback) {
	if(props.reportjson[title]) {
		callback(props.reportjson[title]);
	} else {
		axios.get(`/api/initialize/initializereportjson?title=${title}`).then(function(response) {
			if(response.data.message == 'success') {
				if(!props.reportjson[title])
					props.reportjson[title] = {};
				props.reportjson[title] = response.data.reportjson;
				props.updateReportJSONState(title, props.reportjson[title]);
				callback(props.reportjson[title]);
			} else {
				alert(response.data.message);
			}
		});
	}
}

export function getReportBuilderJson(props, id, callback) {
	let type = '';
	if(props.isDrillDown)
		type = '_drilldown';

	if(props.reportbuilderjson[`${id}_${type}`]) {
		callback(props.reportbuilderjson[id]);
	} else {
		axios.get(`/api/common/methods/getReportConfiguration?id=${id}`).then((response) => {
			if (response.data.message == 'success') {
				if(!props.reportbuilderjson[id])
					props.reportbuilderjson[id] = {};

				props.reportbuilderjson[`${id}`] = response.data.main;
				props.updateReportBuilderJSONState(`${id}`, props.reportbuilderjson[`${id}`]);
				if(response.data.main.type == 'Analytics' && response.data.main.enabledrilldown) {
					let newresponse = JSON.parse(JSON.stringify(response.data.main));
					newresponse.type = 'Normal';
					newresponse.config.fields = newresponse.config.drilldownfields;
					props.reportbuilderjson[`${id}_drilldown`] = newresponse;
					props.updateReportBuilderJSONState(`${id}_drilldown`, props.reportbuilderjson[`${id}_drilldown`]);
				}

				callback(props.reportbuilderjson[`${id}${type}`]);
			} else {
				alert(response.data.message);
			}
		});
	}
}

export function getPageOptions(url) {
	return routes[url] ? routes[url].pageoptions : '';
}

export function getFormName(state, props) {
	var name = `${routes[props.match.path].pageoptions.title}_`
	if(props.match.params.id > 0)
		name = `${name}${props.match.params.id}`;
	else
		name = `${name}create`;
	return name;
}

export function getListTitle(state, props) {
	return `${routes[props.match.path].pageoptions.title}`;
}

export function checkPageCreateMode(props) {
	return props.form.indexOf('_create') >= 0 ? true : false;
}

export function getControllerName(props) {
	return routes[props.match.path].pageoptions.controller;
}

export function checkAccess(app, resource, fields) {
	return true;
}

export function checkActionVerbAccess(app, resource, actionverb) {
	if(!app.myResources[resource].authorization[actionverb])
		return false;
	if(app.myResources[resource] && checkArray(app.myResources[resource].authorization[actionverb], app.user.roleid))
		return true;
}

export function checkArray(arrayA, arrayB) {
	if (arrayB) {
		for (var i = 0; i < arrayA.length; i++) {
			var itemFound = false;
			for (var j = 0; j < arrayB.length; j++) {
				itemFound = (arrayA[i] == arrayB[j]) ? true : itemFound;
				if (itemFound) {
					return true;
				}
			}
		}
	} else {
		return true;
	}
}

export function checkMustArray(arrayA, arrayB) {
	if (arrayB) {
		for (var i = 0; i < arrayA.length; i++) {
			var itemFound = false;
			for (var j = 0; j < arrayB.length; j++) {
				itemFound = (arrayA[i] == arrayB[j]) ? true : itemFound;
				if (itemFound) {
					return true;
				}
			}
		}
	} else {
		return false;
	}
}

export function assign(destObj, souObj, proparr) {
	proparr.map((a) => {
		if(typeof(a) == 'string')
			destObj[a] = souObj[a];
		else {
			for(var prop in a)
				destObj[prop] = souObj[a[prop]];
		}
	});
}


export function search(array, searchObj) {
	let tempArr = [];
	array.map((a) => {
		let filtered = true;
		for(var prop in searchObj) {
			if(searchObj[prop] != null && searchObj[prop] != '' && searchObj[prop] != undefined && prop != '_dontcheck') {
				let searchstring = '' + searchObj[prop] + '';
				let isDateObject = false;
				if(searchObj[prop] instanceof Date) {
					searchstring = '' + searchObj[prop].toDateString() + '';
					isDateObject = true;
				}
				if((a[prop] != null && a[prop] != '' && a[prop] != undefined) || typeof(a[prop]) == 'boolean') {
					let originalstring = '' + a[prop] + '';

					if(isDateObject && new Date(a[prop]) != 'Invalid Date')
						originalstring = '' + new Date(a[prop]).toDateString() + '';

					if(typeof(a[prop]) == 'boolean') {
						originalstring = a[prop] ? 'true' : 'false';
						searchstring = ['y', 'ye', 'yes', 'e', 'es', 's'].includes(searchstring.toLowerCase()) ? 'true' : (['n', 'no', 'o'].includes(searchstring.toLowerCase()) ? 'false' : searchstring);
					}

					if(originalstring.toLowerCase().indexOf(searchstring.toLowerCase()) == -1)
						filtered = false;
				} else {
					filtered = false;
				}
			}
		}
		if(filtered || a['_restrictFromFiltering'])
			tempArr.push(a);
	});
	return tempArr;
}

export function sort_by (array, fields) {
	function sortFunction (fields) {
		let n_fields = fields.length;

		return function(A, B) {
			let a, b, field, key, primer, reverse, result, sortType, refObj;

			for(let i = 0; i < n_fields; i++) {
				result = 0;
				field = fields[i];

				key = typeof field === 'string' ? field : field.name;
				sortType = typeof field === 'object' ? field.type : null;
				refObj = typeof field === 'object' ? field.refObj : null;

				a = key == '_' ? A : A[key];
				b = key == '_' ? B : B[key];

				if (typeof field.primer  !== 'undefined'){
					a = field.primer(a);
					b = field.primer(b);
				}

				reverse = (field.reverse) ? -1 : 1;

				a= (a === undefined) ? 'Undefined' : a;
				b= (b === undefined) ? 'Undefined' : b;

				if (['month-year', 'date'].includes(sortType)) {
					a = moment(a, 'MMM-YYYY')._d != 'Invalid Date' ? moment(a, 'MMM-YYYY')._d : new Date(1000,0,1);
					b = moment(b, 'MMM-YYYY')._d != 'Invalid Date' ? moment(b, 'MMM-YYYY')._d : new Date(1000,0,1);
				} else if (sortType == 'month' && refObj) {
					a = refObj[a] ? refObj[a] : 0;
					b = refObj[b] ? refObj[b] : 0;
				} else if (sortType == 'leadstages' && refObj) {
					a = refObj[a] ? refObj[a] : -10000;
					b = refObj[b] ? refObj[b] : -10000;
				} else {
					a = typeof a === 'string' ? a.toLocaleLowerCase() : a;
					b = typeof b === 'string' ? b.toLocaleLowerCase() : b;
				}

				if (a < b) result = reverse * -1;
				if (a > b) result = reverse * 1;
				if(result !== 0) break;
			}
			return result;
		}
	}

	return array.sort(sortFunction(fields));
}

/*export const numberNewValidation = addValidator({
	defaultMessage: "Number Validation",
	validator: function(options, value, allValues) {
		if (options.required && (value == '' || value == null || Number(value) == NaN)) {
			return false;
		}
		if(value != '' && value != null && Number(value) != NaN) {
			let tempvalue = Number(value);
			let max = Number(options.max);
			let min = Number(options.min);
			if(options.max != null && options.max != '' && tempvalue > max)
				return false;
			if(options.min != null && options.min != '' && tempvalue < min)
				return false;
		}
		return true;
	}
});

export const dateNewValidation = addValidator({
	defaultMessage: "Date Validation",
	validator: function(options, value, allValues) {
		if(options.required && (!value || new Date(value) == "Invalid Date")) {
			return false;
		}
		if(value && new Date(value) != "Invalid Date") {
			if(options.max != null && options.max != '') {
				let maxdate = new Date(options.max).setHours(0, 0, 0, 0);
				let currentdate = new Date(value).setHours(0, 0, 0, 0);
				if(currentdate > maxdate)
					return false;
			}
			if(options.min != null && options.min != '') {
				let mindate = new Date(options.min).setHours(0, 0, 0, 0);
				let currentdate = new Date(value).setHours(0, 0, 0, 0);
				if(mindate > currentdate)
					return false;
			}
		}
		return true;
	}
});

export const multiSelectNewValidation = addValidator({
	defaultMessage: "Multi Select Validation",
	validator: function(options, value, allValues) {
		if(options.required && (!value || (value && value.length == 0))) {
			return false;
		}
		return true;
	}
});

export const stringNewValidation = addValidator({
	defaultMessage: "String Validation",
	validator: function(options, value, allValues) {
		if(options.required && (value == null || value == 'undefined' || value == '')) {
			return false;
		}
		if(value != null && value != 'undefined' && value != '') {
			if(options.pattern) {
				let regexp = eval(`try{${options.pattern}}catch(e){}`);
				if(regexp && !regexp.test(value))
					return false;
			}
		}
		return true;
	}
});

export const emailNewValidation = addValidator({
	defaultMessage: "Email Validation",
	validator: function(options, value, allValues) {
		if(value != null && value != 'undefined' && value != '' && typeof(value) == 'string') {
			var localTempEmailArray = value.split(',');
			for(var i = 0; i < localTempEmailArray.length; i++) {
				localTempEmailArray[i] = localTempEmailArray[i].trim();
				if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(localTempEmailArray[i])))
					return false;
			}
		}
		if(options.required && (value == null || value == 'undefined' || value == '')) {
			return false;
		}
		return true;
	}
});*/

export const numberNewValidation = addValidator({
	defaultMessage: "Number Validation",
	validator: function(options, value, resource) {

		const checkCondition = (type, condition) => {
			let app = options.app ? JSON.parse(options.app) : null;
			let feature = app ? (app.feature ? app.feature : null) : null;
			let user = app ? (app.user ? app.user : null) : null;
			let state = options.state ? JSON.parse(options.state) : null;
			let pagejson = state ? (state.pagejson ? state.pagejson : null) : null;
			let item = options.itemstr ? eval(`try{resource.${options.itemstr}}catch(e){}`) : null;

			if(type == 'required')
				return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
			if(type == 'value') {
				if(typeof(condition) == 'string') {
					if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
						return eval(`try{${condition}}catch(e){}`);
					else
						return condition;
				}
				return condition;
			}
			return null;
		}

		let required = options.required ? checkCondition('required', options.required) : false;
		let optionsMin = options.min ? checkCondition('value', options.min) : null;
		let optionsMax = options.max ? checkCondition('value', options.max) : null;

		if (required && (value == '' || value == null || Number(value) == NaN)) {
			return false;
		}
		if(value != '' && value != null && Number(value) != NaN) {
			let tempvalue = Number(value);
			let max = Number(optionsMax);
			let min = Number(optionsMin);
			if(optionsMax != null && optionsMax != '' && tempvalue > max)
				return false;
			if(optionsMin != null && optionsMin != '' && tempvalue < min)
				return false;
		}
		return true;
	}
});

export const dateNewValidation = addValidator({
	defaultMessage: "Date Validation",
	validator: function(options, value, resource) {

		const checkCondition = (type, condition) => {
			let app = options.app ? JSON.parse(options.app) : null;
			let feature = app ? (app.feature ? app.feature : null) : null;
			let user = app ? (app.user ? app.user : null) : null;
			let state = options.state ? JSON.parse(options.state) : null;
			let pagejson = state ? (state.pagejson ? state.pagejson : null) : null;
			let item = options.itemstr ? eval(`try{resource.${options.itemstr}}catch(e){}`) : null;

			if(type == 'required')
				return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
			if(type == 'value') {
				if(typeof(condition) == 'string') {
					if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
						return eval(`try{${condition}}catch(e){}`);
					else
						return condition;
				}
				return condition;
			}
			return null;
		}

		let required = options.required ? checkCondition('required', options.required) : false;
		let optionsMin = options.min ? checkCondition('value', options.min) : null;
		let optionsMax = options.max ? checkCondition('value', options.max) : null;

		if(required && (!value || new Date(value) == "Invalid Date")) {
			return false;
		}
		if(value && new Date(value) != "Invalid Date") {
			if(optionsMax != null && optionsMax != '') {
				let maxdate = new Date(optionsMax).setHours(0, 0, 0, 0);
				let currentdate = new Date(value).setHours(0, 0, 0, 0);
				if(currentdate > maxdate)
					return false;
			}
			if(optionsMin != null && optionsMin != '') {
				let mindate = new Date(optionsMin).setHours(0, 0, 0, 0);
				let currentdate = new Date(value).setHours(0, 0, 0, 0);
				if(mindate > currentdate)
					return false;
			}
		}
		return true;
	}
});

export const multiSelectNewValidation = addValidator({
	defaultMessage: "Multi Select Validation",
	validator: function(options, value, resource) {

		const checkCondition = (type, condition) => {
			let app = options.app ? JSON.parse(options.app) : null;
			let feature = app ? (app.feature ? app.feature : null) : null;
			let user = app ? (app.user ? app.user : null) : null;
			let state = options.state ? JSON.parse(options.state) : null;
			let pagejson = state ? (state.pagejson ? state.pagejson : null) : null;
			let item = options.itemstr ? eval(`try{resource.${options.itemstr}}catch(e){}`) : null;

			if(type == 'required')
				return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
			if(type == 'value') {
				if(typeof(condition) == 'string') {
					if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
						return eval(`try{${condition}}catch(e){}`);
					else
						return condition;
				}
				return condition;
			}
			return null;
		}

		let required = options.required ? checkCondition('required', options.required) : false;

		if(required && (!value || (value && value.length == 0))) {
			return false;
		}
		return true;
	}
});

export const stringNewValidation = addValidator({
	defaultMessage: "String Validation",
	validator: function(options, value, resource) {

		const checkCondition = (type, condition) => {
			let app = options.app ? JSON.parse(options.app) : null;
			let feature = app ? (app.feature ? app.feature : null) : null;
			let user = app ? (app.user ? app.user : null) : null;
			let state = options.state ? JSON.parse(options.state) : null;
			let pagejson = state ? (state.pagejson ? state.pagejson : null) : null;
			let item = options.itemstr ? eval(`try{resource.${options.itemstr}}catch(e){}`) : null;

			if(type == 'required')
				return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
			if(type == 'value') {
				if(typeof(condition) == 'string') {
					if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
						return eval(`try{${condition}}catch(e){}`);
					else
						return condition;
				}
				return condition;
			}
			return null;
		}

		let required = options.required ? checkCondition('required', options.required) : false;
		let pattern = options.pattern ? checkCondition('value', options.pattern) : null;

		if(required && (value == null || value == 'undefined' || value == '')) {
			return false;
		}
		if(value != null && value != 'undefined' && value != '') {
			if(pattern) {
				let regexp = eval(`try{${pattern}}catch(e){}`);
				if(regexp && !regexp.test(value))
					return false;
			}
		}
		return true;
	}
});

export const emailNewValidation = addValidator({
	defaultMessage: "Email Validation",
	validator: function(options, value, resource) {

		const checkCondition = (type, condition) => {
			/*let app = options.app ? options.app : null;
			let feature = app ? (app.feature ? app.feature : null) : null;
			let user = app ? (app.user ? app.user : null) : null;*/
			let state = options.state ? options.state : null;
			let pagejson = state ? (state.pagejson ? state.pagejson : null) : null;
			let item = options.itemstr ? eval(`try{resource.${options.itemstr}}catch(e){}`) : null;

			if(type == 'required')
				return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
			if(type == 'value') {
				if(typeof(condition) == 'string') {
					if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
						return eval(`try{${condition}}catch(e){}`);
					else
						return condition;
				}
				return condition;
			}
			return null;
		}

		let required = options.required ? checkCondition('required', options.required) : false;

		if(value != null && value != 'undefined' && value != '' && typeof(value) == 'string') {
			var localTempEmailArray = value.split(',');
			for(var i = 0; i < localTempEmailArray.length; i++) {
				localTempEmailArray[i] = localTempEmailArray[i].trim();
				//if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(localTempEmailArray[i])))   //removed, because very slow
				if (!(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,})$/.test(localTempEmailArray[i])))
					return false;
			}
		}
		if(required && (value == null || value == 'undefined' || value == '')) {
			return false;
		}
		return true;
	}
});

export const requiredNewValidation = addValidator({
	defaultMessage: "Required Validation",
	validator: function(options, value, resource) {

		const checkCondition = (type, condition) => {
			let app = options.app ? JSON.parse(options.app) : null;
			let feature = app ? (app.feature ? app.feature : null) : null;
			let user = app ? (app.user ? app.user : null) : null;
			let state = options.state ? JSON.parse(options.state) : null;
			let pagejson = state ? (state.pagejson ? state.pagejson : null) : null;
			let item = options.itemstr ? eval(`try{resource.${options.itemstr}}catch(e){}`) : null;

			if(type == 'required')
				return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
			if(type == 'value') {
				if(typeof(condition) == 'string') {
					if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
						return eval(`try{${condition}}catch(e){}`);
					else
						return condition;
				}
				return condition;
			}
			return null;
		}

		let required = options.required ? checkCondition('required', options.required) : false;
		let pattern = options.pattern ? checkCondition('value', options.pattern) : null;

		if(required && (value == null || value == 'undefined' || value == '')) {
			return false;
		}

		return true;
	}
});

export function numberValidation(value, allvalues, props) {
	if(typeof(value) != 'number')
		return true;
}

export function dateValidation(value, allvalues, props) {
	if(!value || new Date(value) == "Invalid Date")
		return true;
}

export function multiSelectValidation(value, allvalues, props) {
	if(!value || (value && value.length == 0))
		return true;
}

export function requiredValidation(value, allvalues, props) {
	if(value == null || value == 'undefined' || value == '')
		return true;
}

export function validate(values, props) {
	//console.log("validate........", values, props);
	let errorObj = {};
	let childArray = [];
	let options = getPageOptions(props.match.path)

	let pagejson = props.pagejson[options.title] && props.pagejson[options.title][options.type] ? props.pagejson[options.title][options.type] : null;

	if(pagejson) {
		for (var i = 0; i < pagejson.sections.length; i++) {
			if(checkCondition('if', pagejson.sections[i].if)) {
				for(var j=0;j<pagejson.sections[i].fields.length;j++) {
					if (pagejson.sections[i].fields[j].type == 'repeatabletable') {
						if (pagejson[pagejson.sections[i].fields[j].json].body && pagejson[pagejson.sections[i].fields[j].json].body.length > 0) {
							childArray.push({
								array : pagejson.sections[i].fields[j].json,
								title : pagejson.sections[i].title
							});
						}
					} else if(pagejson.sections[i].fields[j].type != 'repeatabletable' && pagejson.sections[i].fields[j].type != 'additionalcharges' && pagejson.sections[i].fields[j].type != 'tax') {
						if(checkCondition('if', pagejson.sections[i].fields[j].if)) {
							if(checkCondition('required', pagejson.sections[i].fields[j].required)) {
								let modelstr = `{resource.${pagejson.sections[i].fields[j].model}}`
								let model = checkCondition('value', modelstr);

								if(pagejson.sections[i].fields[j].type == 'number' || pagejson.sections[i].fields[j].type == 'selectauto' || pagejson.sections[i].fields[j].type == 'inputbuttongroup' || pagejson.sections[i].fields[j].type == 'selectauto' || pagejson.sections[i].fields[j].type == 'selectasync' || pagejson.sections[i].fields[j].type == 'time') {
									if(typeof(model) != 'number') {
										errorObj[pagejson.sections[i].fields[j].model] = true;
									}
								} else if(pagejson.sections[i].fields[j].type == 'date') {
									if(!model || new Date(model) == "Invalid Date") {
										errorObj[pagejson.sections[i].fields[j].model] = true;
									}
								}  else if(pagejson.sections[i].fields[j].type == 'select' && (pagejson.sections[i].fields[j].multiselect == true || pagejson.sections[i].fields[j].multiselect == "true")) {
									if(model && model.length == 0) {
										errorObj[pagejson.sections[i].fields[j].model] = true;
									}
								} else {
									if(model == null || model == 'undefined') {
										errorObj[pagejson.sections[i].fields[j].model] = true;
									}
								}
							}
						}
					}
				}
			}
		}

		for (var i = 0; i < childArray.length; i++) {
			let arrayModelStr = `{resource.${childArray[i].array}}`;
			let childitemarray = checkCondition('value', arrayModelStr);
			let childitemErrorArr = [];

			if (childitemarray && childitemarray.length > 0) {
				for (var j = 0; j < childitemarray.length; j++) {
					let childitemErrorObj = {};
					for(var k=0;k<pagejson[childArray[i].array].body.length;k++) {
						if(checkCondition('required', pagejson[childArray[i].array].body[k].required) || pagejson[childArray[i].array].body[k].required == "true" || pagejson[childArray[i].array].body[k].required == true) {
							if(checkCondition('if', pagejson[childArray[i].array].body[k].if)) {
								let fieldName = `${pagejson[childArray[i].array].body[k].model}`;
								let modelStr = `{resource.${childArray[i].array}[${j}].${fieldName}}`;
								let model = checkCondition('value', modelStr);

								if(pagejson[childArray[i].array].body[k].type == 'number' || pagejson[childArray[i].array].body[k].type == 'selectauto' || pagejson[childArray[i].array].body[k].type == 'inputbuttongroup' || pagejson[childArray[i].array].body[k].type == 'time') {
									if(typeof(model) != 'number') {
										errorObj[`${childArray[i].array}[${j}].${fieldName}`] = true;
									}
								} else if(pagejson[childArray[i].array].body[k].type == 'date') {
									if(!model || new Date(model) == "Invalid Date") {
										errorObj[`${childArray[i].array}[${j}].${fieldName}`] = true;
									}
								}  else if(pagejson[childArray[i].array].body[k].type == 'select' && (pagejson[childArray[i].array].body[k].multiselect == true || pagejson[childArray[i].array].body[k].multiselect == "true")) {
									if(model && model.length == 0) {
										errorObj[`${childArray[i].array}[${j}].${fieldName}`] = true;
									}
								} else {
									if(model == null || model == 'undefined') {
										errorObj[`${childArray[i].array}[${j}].${fieldName}`] = true;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	var genErrorObj = {};
	for(var prop in errorObj) {
		if(prop.split('.').length > 1) {
			let newprop = prop.split('.')[0].split('[')[0];
			if(!genErrorObj[newprop]) {
				if(prop.split('.')[0].indexOf('[') >= 0)
					genErrorObj[prop.split('.')[0].split('[')[0]] = [];
				else
					genErrorObj[newprop] = {};
			}
			if(prop.split('.')[0].indexOf('[') >= 0 && !eval(`genErrorObj.${prop.split('.')[0]}`)) {
				console.log(eval(`JSON.stringify(genErrorObj.${newprop})`));
				eval(`genErrorObj.${prop.split('.')[0]} = {};`);
			}
		}
		eval(`genErrorObj.${prop} = ${errorObj[prop]};`);
	}
	//console.log(genErrorObj);
	return genErrorObj;

	function checkCondition(type, condition) {
		let app = props.app;
		let feature = props.app.feature;
		let user = props.app.user;
		let resource = props.resource;

		if(type == 'if')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 || (['string', 'boolean'].indexOf(typeof(condition)) >= 0 && eval(`try{${condition}}catch(e){}`));
		if(type == 'hide')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? 'hide' : '');
		if(type == 'show')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? '' : 'hide');
		if(type == 'required')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'disabled')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'value') {
			if(typeof(condition) == 'string') {
				if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
					return eval(`try{${condition}}catch(e){}`);
				else
					return condition;
			}
			return condition;
		}

		return null;
	}
}

/** Only for Web App **/
export function toast(message, callback) {
	ToastFn(<ToastBody message={message} />, {
		position: ToastFn.POSITION.TOP_RIGHT,
		autoClose: 4000,
		draggablePercent: 60,
		transition: Slide,
		type: message.toastType && message.toastType.toLocaleLowerCase() == 'error' ? ToastFn.TYPE.ERROR : ToastFn.TYPE.DEFAULT,
		hideProgressBar: true,
		closeOnClick: true,
		pauseOnHover: true,
		draggable: true,
		closeButton: false
	});
}
	
export class ToastBody extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div style={{width: '100%', display: 'flex', alignItems: 'center', verticalAlign: 'middle'}}>
				<div className={`float-left ${this.props.message.toastType && this.props.message.toastType.toLocaleLowerCase() == 'error' ? 'fa fa-exclamation-triangle gs-text-color-danger' : 'fa fa-check-circle gs-text-color'} fa-2x marginright-10`}></div>
				<div className="float-left">{this.props.message.body}</div>
			</div>
		);
	}
}

export function customfieldAssign(destArr, destItemArr, resourcename, resourceJSON, keyFlag) {
	let customfieldArr = commonMethods.getCustomFields(resourceJSON, resourcename, 'array');

	customfieldArr.map((a) => {
		if(!resourceJSON[resourcename].fields[a].islink && resourceJSON[resourcename].fields[a].type!='json')
			destArr.push({
				name : resourceJSON[resourcename].fields[a].displayName,
				key : resourceJSON[resourcename].fields[a].isForeignKey ? (`${resourcename}` + '_' + resourceJSON[resourcename].fields[a].foreignKeyOptions.mainField.replace('.','_')) : (resourceJSON[resourcename].fields[a].isArrayForeignKey ? `text${a}` : (keyFlag ? `${resourcename}_${a}` : `${a}`)),
				format : resourceJSON[resourcename].fields[a].type == 'date' ? 'date' : ((resourceJSON[resourcename].fields[a].type == 'boolean') ? 'boolean' : ((resourceJSON[resourcename].fields[a].type == 'array') ? 'array' : '')),
				cellClass : "text-center",
				width : 200
			});

		if(destItemArr && destItemArr.length && !resourceJSON[resourcename].fields[a].islink && resourceJSON[resourcename].fields[a].type!='json') {
			destItemArr.push({
				name : resourceJSON[resourcename].fields[a].displayName,
				key : resourceJSON[resourcename].fields[a].isForeignKey ? (`${resourcename}` + '_' + resourceJSON[resourcename].fields[a].foreignKeyOptions.mainField.replace('.','_')) : (keyFlag ? `${resourcename}_${a}` : `${a}`),
				format : resourceJSON[resourcename].fields[a].type == 'date' ? 'date' : ((resourceJSON[resourcename].fields[a].type == 'boolean') ? 'boolean' : ((resourceJSON[resourcename].fields[a].type == 'array') ? 'array' : '')),
				cellClass : "text-center",
				width : 200
			});
		}
	});
}

export function removeDuplicate(array) {
	let finalArray = [];

	for(let i = 0; i < array.length; i++) {
		if(finalArray.indexOf(array[i]) == -1)
			finalArray.push(array[i]);
	}

	return finalArray;
}

export function rptbuilderSorting (data, column, columnsinfo, ref, stagesObj, index, nameProp) {

	let	monthObj = {
			January: 1,
			February: 2,
			March: 3,
			April: 4,
			May: 5,
			June: 6,
			July: 7,
			August: 8,
			September: 9,
			October: 10,
			November: 11,
			December: 12
		},
		formatter = null;

	function getFormat (columnsinfo) {
		let formatter = null;

		if (!columnsinfo)
			return formatter;

		if (columnsinfo.isForeignKey && columnsinfo.foreignKeyOptions.resource == 'leadstages')
			formatter = 'leadstages';

		if (columnsinfo.isConfiguredColumn && columnsinfo.configuredProp.calculationtype == 'dateformatter')
			formatter = columnsinfo.configuredProp.formatter;
		else if (columnsinfo.type == 'date')
			formatter = 'date';

		return formatter;
	}

	function getRefObj (formatter) {
		if (!formatter)
			return null;

		let refObj = {};

		if (formatter == 'leadstages')
			refObj = stagesObj;
		else if (formatter == 'month')
			refObj = monthObj;

		return refObj;
	}

	formatter = getFormat(columnsinfo);

	if (formatter || ['pivot', 'Series'].includes(ref)) {
		let refObj = getRefObj(formatter);

		if (ref == 'pivot') {
			data = sort_by(data, [{name:'_', type: formatter, refObj}]);
			/*if (index == 0)
				data = sort_by(data, [{name:'name', type: formatter, refObj}]);
			else {
				let nameFormatter = getFormat(nameProp);

				data = sort_by(data, [{name: 'paramstr', type: nameFormatter, refObj: getRefObj(nameFormatter)}, {name:'name', type: formatter, refObj}]);
			}*/
		} else {
			if (ref == 'Axis')
				data = sort_by(data, [{name:'_', type: formatter, refObj}]);
			else
				data = sort_by(data, [{name:'label', type: formatter, refObj}]);
		}
	} else
		data.sort((a, b) => {
			return (a.toLowerCase() > b.toLowerCase() ? 1 : (a.toLowerCase() < b.toLowerCase() ? -1 : 0));
		});

	return data;
}