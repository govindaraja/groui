import * as qz from 'qz-tray';
import shajs from 'sha.js';
import { KJUR, KEYUTIL, stob64, hextorstr } from 'jsrsasign';

qz.api.setPromiseType(function promise(resolver) { return new Promise(resolver); });
qz.api.setSha256Type(function(data) {
   return shajs('sha256').update(data).digest('hex');
});

qz.security.setCertificatePromise(function(resolve, reject) {
        resolve(`-----BEGIN CERTIFICATE-----
MIIDqTCCApGgAwIBAgIUHyojlSO1fJWX4ooYdE/HUSHX9U0wDQYJKoZIhvcNAQEL
BQAwYzELMAkGA1UEBhMCSU4xCzAJBgNVBAgMAlROMRAwDgYDVQQHDAdDaGVubmFp
MRIwEAYDVQQKDAlHcm93c21hcnQxDTALBgNVBAsMBE5vbmUxEjAQBgNVBAMMCWxv
Y2FsaG9zdDAgFw0xOTA3MTIxMzM0MzJaGA8yMDUxMDEwNDEzMzQzMlowYzELMAkG
A1UEBhMCSU4xCzAJBgNVBAgMAlROMRAwDgYDVQQHDAdDaGVubmFpMRIwEAYDVQQK
DAlHcm93c21hcnQxDTALBgNVBAsMBE5vbmUxEjAQBgNVBAMMCWxvY2FsaG9zdDCC
ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMQt6oiidSXZqLxaw5l3D9tv
j1tHEQJ07YJq7LnQWe+5TErP0bS3ZVzlIEITZcBjWbsm3Zqoz0mAutaNguCxfdTv
G+uM65GWCIX4ycvAXH0dBsv9R90Vi/Wos8J3U35vE+rLeNhvfR48LL45QME6wCK6
qSF6afUZXqYNoE0/ipLamxDCWtjJUNbQmRb+6F4NpjK+Txr/Vw4Gb6Qfq2A8hX35
Fhdm3pKF/YKA0izKqeJYlwyRMYTbhqGvUOGKVklXqkqcunjm2/8QkYMbIBafwx0e
Cd5MKiCUZLwtKG9muta+q96q83XvDxJ+twm9nl4+ldzkZk2lGb6TW8oxvz8gZokC
AwEAAaNTMFEwHQYDVR0OBBYEFP8LnHn7nrxNZE6Zbz17p85BL3mmMB8GA1UdIwQY
MBaAFP8LnHn7nrxNZE6Zbz17p85BL3mmMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZI
hvcNAQELBQADggEBAC5EZnNAQ9jnSbF2RHlN6yjJIBeokQAcddH86yxif9ssLsBY
UzvJNXFmX6PlFapoitJcDLN9U6/+vol3KAoJQ18FGb3CbWJjY6YjxRk0/2wvYt7v
CzNzz2Lfn2PgGy0pqPSLZ9O2+7crPxcRFPYK3kT6LKoZJ8RFoIM3UpnnUNsNdP+9
77tTS1tGQS+xLGWKzo46wRRUdEJoBnbvqibAO7ClqMVL5rZ0e7XFfkajIB9tq4rB
k7out1XVdNZE1fHXNqfe6xyDwdx6JR++MY9+mxvfcBih4dd0hs0MsH4wTUtf7WQ1
Yaj1cZl8Tu078KH9gJcUlJtcCWbwSwxAYRfI3xE=
-----END CERTIFICATE-----
`);
});

let privateKey = `-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDELeqIonUl2ai8
WsOZdw/bb49bRxECdO2Cauy50FnvuUxKz9G0t2Vc5SBCE2XAY1m7Jt2aqM9JgLrW
jYLgsX3U7xvrjOuRlgiF+MnLwFx9HQbL/UfdFYv1qLPCd1N+bxPqy3jYb30ePCy+
OUDBOsAiuqkhemn1GV6mDaBNP4qS2psQwlrYyVDW0JkW/uheDaYyvk8a/1cOBm+k
H6tgPIV9+RYXZt6Shf2CgNIsyqniWJcMkTGE24ahr1DhilZJV6pKnLp45tv/EJGD
GyAWn8MdHgneTCoglGS8LShvZrrWvqveqvN17w8SfrcJvZ5ePpXc5GZNpRm+k1vK
Mb8/IGaJAgMBAAECggEAQVWRYEPKg/qDKYeI0NlIHKqAscNvNMRAPUji4dczvJSX
herESk0xoshj/91566KqxZth/hnuPupyTvX9+lriL0IaqE3cPjYbTJLYkdTUeI5s
Lw+BGW4NmJjAlSksU1Um6vEso9gtcR5VQFzM5g1s46QKo62jEA+M5/xtwIxC2DkE
T8+mFHllyd07oOfwQumaHxHVueH4KYlQGha+JNAcjx9ue4MBek4NTiSaMbF7N4SJ
fz4ExjOn1TyFFsL6RiS1h9RtMW0GEHXtlLfHqCBiw5GzFBqk3MtBfX0rcR2lIYnW
JPNRZvyyDzHclBI6wEMrTKTOKTvRZp6zZCtNYphJiQKBgQDlesUDC+63ECGcMYC5
YeYzew7rciGQFH2S6who1xvrne/IvStMdLWs68EpKL0aDYAUHq4z7i0gRlozM14M
dPF9HKaBY+tz2B5Bt8JuGdpoYqhfBcWuhf3n/JvAUF9Vneiz161hS7DXYIby/2Jv
2h0zlKdx5+JC94XBVWUwXOeliwKBgQDa2fLbwGFXg+LKJ5R0h1tlYqZBM+gxMxV5
qnFoQwHy0HvOeqhz0QRTitU85Kts3e1OmLEkPscx3sPVygpnC6e/A2K0l2hOWqYM
ZCM6wAl3fRxgrE9MM4jIyo/q/dKchdCBvdSb04RGbaKFaF4Os3JX8HTAn+oTw46B
oJBklvKuuwKBgQCzCVzJ6rZc9uXyEaEngVzzz0+tnAbZfVMXm+2cti8fXcJlcElm
xiuz9N9oOFOJDv1dNS6eb3Wfl6PqDGqU92wtO3wguQ9K+1Nn3HGCKwN+q3JHaJ55
cirNowT1Pv+SMbgLD/tCTKZ08ud2kI+BSr+7rYASW2Lo8mrVksV2vRUxzwKBgG7Q
LMhYgPFvNkZmBCV85CpR6Rzay5cd5qHUS9gfFb19EzNZqDH8g9RJV++dWa+mL6K8
bfWMVBBMW5zE0L+chzOQ40nN4GLMQ4fCrJPy6Ng34UaR+RT8gyYoavbxztKybwFC
vYSpdTC0Era6QeXvttYQSw296yhWvLGmO3azd+5BAoGBAMXrLjlyJoa/YobaHcAp
0JKkFTLQEPw3InghcOhigBTl1Q1jkgnnMNJKcwWAaoOThL8obKZwr29ctBfVlXis
q5KqCDyu85s3YROg13DOdsmX+GI2+Gz5ckHwAbxUC9/YJlHcIlvQW5holY9/PpC1
uuSZej6lG4tIFELOWebvyMqq
-----END PRIVATE KEY-----
`;
   
qz.security.setSignaturePromise(function(toSign) {
    return function(resolve, reject) {
        try {
            var pk = KEYUTIL.getKey(privateKey);
            var sig = new KJUR.crypto.Signature({"alg": "SHA1withRSA"});
            sig.init(pk); 
            sig.updateString(toSign);
            var hex = sig.sign();
            resolve(stob64(hextorstr(hex)));
        } catch (err) {
            console.error(err);
            reject(err);
        }
    };
});

qz.websocket.connect();

module.exports = qz;