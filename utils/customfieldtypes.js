import React, { Component } from 'react';
import axios from 'axios';

import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from './filter';

export class RenderLostLabel extends Component {
	constructor(props) {
		super(props);
	};

	render() {
		return (
			<div>
				{this.props.item.equipmentid ? this.props.item.equipmentid_displayname : this.props.item.itemid_name}
				<span className={`${this.props.item.islost ? 'show' : 'hide'} badge badge-pill badge-danger`} style={{marginLeft:'10px'}}>Lost</span>
			</div>
		);
	}
}

export class RenderReferenceButton extends Component {
	constructor(props) {
		super(props);
	};

	render() {
		let openDetails = '';

		if(this.props.item.type == 'Service Reports' && this.props.item.servicereportid)
			openDetails = `Open ${this.props.item.servicereportid_servicereportno}`;

		if(this.props.item.type == 'Sales Activity' && this.props.item.salesactivityid)
			openDetails = `Open ${this.props.item.type}`;

		if(this.props.item.type == 'Internal Labour' && this.props.item.workprogressid)
			openDetails = `Open ${this.props.item.type}`;

		return (
			<button type="button" className="btn btn-sm btn-outline-dark btn-width" onClick={() => this.props.callback(this.props.item)}><i className="fa fa-external-link"></i>{openDetails}</button>
		);
	}
}

export class RenderItemQty extends Component {
	constructor(props) {
		super(props);
	};

	render() {
		let quantity = this.props.item.quantity;
		let deliveredqty = this.props.resource.resourceName == 'workorders' ? (this.props.item.completedqty || 0) : (this.props.item.deliveredqty || 0);
		let invoicedqty = this.props.item.invoicedqty || 0;
		let closedqty = this.props.item.closedqty || 0;
		let usedqty = this.props.item.usedqty || 0;
		let deliveredPercent = ((deliveredqty + closedqty)/quantity) * 100;
		let invoicedPercent = ((invoicedqty + closedqty)/quantity) * 100;
		let usedPercent = (usedqty/quantity) * 100;

		let resObj = {
			orders : {
				first_title : `Delivered Qty(${deliveredqty}) Closed Qty(${closedqty})`,
				second_title : `Invoiced Qty(${invoicedqty}) Closed Qty(${closedqty})`
			},
			itemrequests : {
				first_title : `Delivered Qty(${deliveredqty}) Closed Qty(${closedqty})`,
				second_title : `Used Quantity(${usedqty})`
			},
			projects : {
				first_title : `Delivered Qty(${deliveredqty})`,
				second_title : `Invoiced Qty(${invoicedqty})`
			},
			purchaseorders : {
				first_title : `Received Qty(${deliveredqty}) Closed Qty(${closedqty})`,
				second_title : `Invoiced Qty(${invoicedqty}) Closed Qty(${closedqty})`
			},
			workorders : {
				first_title : `Completed Qty(${deliveredqty}) Closed Qty(${closedqty})`,
				second_title : `Invoiced Qty(${invoicedqty}) Closed Qty(${closedqty})`
			}
		};

		let condition1 = this.props.resource.resourceName == 'projects' ? (this.props.item.itemid_itemtype == 'Product' ? true : false) : true;

		let condition2 = this.props.resource.resourceName == 'itemrequests' ? ((this.props.resource.itemrequestfor == 'Service Call' || this.props.resource.itemrequestfor == 'Engineer'  || this.props.resource.itemrequestfor == 'Production Order') ? true : false) : (this.props.resource.resourceName == 'projects' ? (this.props.item.milestonetemplateid > 0 ? false : true) : true);

		return (
			<div>
				{`${quantity} ${this.props.resource.resourceName != 'itemrequests' ? uomFilter(this.props.item.uomid, this.props.uomObj) : ''}`}
				{(this.props.statusArray.indexOf(this.props.status) >= 0 || this.props.resource.amendinprogress ) ? <div className='progressfulldiv'>
					{condition1 ? <div rel='tooltip' title={`${resObj[this.props.resource.resourceName]['first_title']}`} className='progresshalfdiv progresspadding'>
						<div className="progress progresscss" style={{height:'4px'}}>
							<div className="progress-bar bg-success" role="progressbar" style={{width: `${deliveredPercent}%`}}></div>
						</div>
					</div> : null}
					{condition2 ? <div rel='tooltip' title={`${resObj[this.props.resource.resourceName]['second_title']}`}  className='progresshalfdiv progresspadding'>
						<div className="progress progresscss" style={{height:'4px'}}>
							<div className="progress-bar bg-success" role="progressbar" style={{width: `${this.props.resource.resourceName == 'itemrequests' ? usedPercent : invoicedPercent}%`}}></div>
						</div>
					</div> : null}
				</div> : null }
			</div>
		);
	}
}

export class RenderHistory extends Component {
	constructor(props) {
		super(props);
	};

	render() {
		if(this.props.historyArray.length == 0)
			return null;

		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Version History</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						{this.props.resource.title ? <div className="col-md-12 marginbottom-10">
							<div className="col-md-6">Title :  <b>{this.props.resource.title}</b></div>
						</div> : null}
						<div className="col-md-12">
							<table className="table table-bordered table-hover table-condensed">
								<thead>
									<tr>
										<th className="text-center">Outcome</th>
										<th className="text-center">Due Date</th>
										<th className="text-center">Status</th>
										<th className="text-center">Assigned To</th>
										<th className="text-center">Modified By</th>
										<th className="text-center">Modified On</th>
									</tr>
								</thead>
								<tbody>
									{ this.props.historyArray.map((item, index) => {
										return (
											<tr key={index}>
												<td>{item.outcome}</td>
												<td className="text-center">{dateFilter(item.duedate)}</td>
												<td className="text-center">{item.status}</td>
												<td className="text-center">{item.assignedto}</td>
												<td className="text-center">{item.modifiedby}</td>
												<td className="text-center">{datetimeFilter(item.modified)}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-info btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
