import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import { Route, Switch, Link, Redirect} from 'react-router-dom';
import Home from '../containers/home';
import Dashboard from '../containers/dashboard';
import routes from '../routeconfig';

export default class RouterBar extends Component {
	generateRoutes() {
		let tempRoutes = [];
		for(let prop in routes) {
			tempRoutes.push(
				<Route
					key={prop}
					path={prop}
					render={(routeProps) => {
						let MyComponent = routes[prop].component;
						let keyObj = {
							key: routeProps.location.pathname
						};
						if(routes[prop].title)
							document.getElementById("pagetitle").innerHTML = routes[prop].title;

						return <MyComponent openModal={this.props.openModal} createOrEdit={this.props.createOrEdit} {...routeProps} {...keyObj} />
					}} />
			);
		}
		return tempRoutes;
	}
	render() {
		let HomeComponent = (routeProps) => {
			return <Home openModal={this.props.openModal} createOrEdit={this.props.createOrEdit} {...routeProps} updateBookMark={this.props.updateBookMark} />
		}
		let DashboardComponent = (routeProps) => {
			return <Dashboard openModal={this.props.openModal} createOrEdit={this.props.createOrEdit} {...routeProps} />
		}
		return (
			<Switch>
				<Route exact path="/" render={HomeComponent} />
				<Route path="/home" render={HomeComponent} />
				<Route path="/dashboard" render={DashboardComponent} />
				{this.generateRoutes()}
				<Redirect to="/"/>
			</Switch>
		);
	}
}