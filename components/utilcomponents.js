import React, { Component } from 'react';
import { findDOMNode } from 'react-dom'
import { connect } from 'react-redux';
import axios from 'axios';

import {reduxForm} from 'redux-form';
import { Portal } from 'react-overlays';
import _ from 'lodash';
import moment from 'moment';

import Select, { components } from 'react-select';
import AsyncSelect  from 'react-select/async';
import AsyncCreatableSelect from 'react-select/async-creatable';
import CreatableSelect from 'react-select/creatable';

import { v1 as uuidv1 } from 'uuid';

import {checkAccess, checkActionVerbAccess } from '../utils/utils';

import { commonMethods, modalService } from '../utils/services';

import TetherComponent from 'react-tether';
import Dimensions from 'react-dimensions';

import TimeAgoLibrary from 'react-timeago';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import DateTime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';
import CalendarContainer from 'react-datetime/src/CalendarContainer';
import Highlighter from 'react-highlight-words';

import { currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../utils/filter';

const ReactSelectNormal = Select;
const ReactSelectAsync = AsyncSelect;
const ReactSelectAsyncCreatable = AsyncCreatableSelect;
const ReactSelectCreatable = CreatableSelect;

const selectCustomStyles = {
	menuPortal: base => ({ ...base, zIndex: 1000000 }),
	control: (base, state) => ({ ...base, boxShadow: "none", minHeight: '35px', borderColor: state.isDisabled ? 'transparent' : '#e2e2e2', borderRadius: '2px', backgroundColor: state.isDisabled ? '#fbfbfd' : '#fff', '&:hover': { borderColor: '#e2e2e2' } }),
	dropdownIndicator: base => ({ ...base, padding: '6px', cursor: 'pointer' }),
	clearIndicator: base => ({ ...base, padding: '6px', cursor: 'pointer' }),
	valueContainer: base => ({ ...base, padding: '0px 8px 0px 10px' }),
	option: (provided, state) => ({ ...provided, backgroundColor: state.isSelected || state.isFocused ? '#f5fafe' : '#fff', color: '#444444', '&:hover': { backgroundColor: '#ebf5ff' } }),
	placeholder: base => ({ ...base, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }),
	singleValue: (provided, state) => ({ color: state.isDisabled ? '#444444' : '#444444' }),
	multiValue: (provided, state) => ({ ...provided, backgroundColor: state.isDisabled ? '#eee' : 'rgba(0, 126, 255, 0.08)', color: state.isDisabled ? '#555555' : '#007eff' }),
	multiValueLabel: (provided, state) => ({ ...provided, color: state.isDisabled ? '#555555' : '#007eff' }),
	multiValueRemove: (provided, state) => ({ ...provided, display: state.isDisabled ? 'none' : 'flex', '&:hover': { backgroundColor: 'rgba(0, 113, 230, 0.08)', color: '#0071e6' }  })
}

const DateCalendarContainer = ({children}) => {
	const el = document.getElementById('date-body-cont')

	return (
		<Portal container={el}>
			{children}
		</Portal>
	)
};

const GetCustomFields = (app, resource) => {
	let returnStr = '';
	if(app.myResources[resource] && app.myResources[resource].fields) {
		let temparray = [];
		for(var prop in app.myResources[resource].fields) {
			if(prop.indexOf('zzz') == 0)
				temparray.push(prop);
		}
		returnStr = temparray.length > 0 ? (','+temparray.join()) : '';
	}
	return returnStr;
};

const OptionRenderer = (props, searchValue, labelkey) => {
	labelkey = labelkey ? labelkey : 'label';
	return (
		<components.Option {...props}>
			<Highlighter className="select-option" highlightClassName="select-text-highlighter" autoEscape={true} searchWords={[searchValue]} textToHighlight={props.data[labelkey]} />
		</components.Option>
	);
};

const LoadingIndicator = () => {
	return (
		<span className="gs-input-select-loading-zone" aria-hidden="true">
			<span className="gs-input-select-loading"></span>
		</span>
	);
}

const createResourceObj = {
	partners: {
		url: '/createCustomer',
	},
	customers: {
		url: '/createCustomer',
	},
	competitors: {
		url: '/createCompetitor',
	},
	suppliers: {
		url: '/createSupplier',
	},
	territories: {
		url: '/createTerritory',
	},
	leadsourcemaster: {
		url: '/createLeadSourceMaster',
	},
	customergroups: {
		url: '/createCustomerGroup',
	},
	itemcategorymaster: {
		url: '/createItemCategoryMaster',
	},
	itemgroups: {
		url: '/createItemGroup',
	},
	itemmaster: {
		url: '/createItemMaster',
	},
	banks: {
		url: '/createBank',
	},
	industries: {
		url: '/createIndustry',
	},
	contacts: {
		url: '/createContact',
	},
	countries: {
		url: '/createCountry',
	},
	states: {
		url: '/createState',
	},
	cities: {
		url: '/createCity',
	},
	areas: {
		url: '/createArea',
	},
	streets: {
		url: '/createStreet',
	}
};

export class NumberElement extends Component {
	constructor(props) {
		super(props);

		this.onChange = this.onChange.bind(this);
	}

	onChange(evt) {
		let value = evt.target.value;
		this.props.onChange((value == null || value === '') ? null : Number(value));
	}

	render() {
		return (
			<input
				type="number"
				className={this.props.className}
				value={this.props.value == null ? '' : this.props.value}
				placeholder={this.props.placeholder}
				onChange={this.onChange}
				required={this.props.required}
				disabled={this.props.disabled}
				autoFocus={this.props.autoFocus ? this.props.autoFocus : false}
				autoComplete="off" />
		);
	}
}

export class DateElement extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: this.props.value ? moment(this.props.value).format('DD-MMM-YYYY') : ''
		};

		this.clickOutside = this.clickOutside.bind(this);
	}

	componentWillReceiveProps(nextprops) {
		this.setState({
			value: nextprops.value ? moment(nextprops.value).format('DD-MMM-YYYY') : ''
		});
	}

	clickOutside() {
		this.refs.picker.cancelFocusInput();
		this.refs.picker.setOpen(false);
	}

	render() {
		return (
			<DatePicker
				isClearable={this.props.disabled ? false : true}
				ref="picker"
				className={this.props.className}
				selected={this.props.value ? new Date(this.props.value) : null}
				onChange={(value) => this.props.onChange((value == null || value == '') ? null : value)}
				onFocus={this.props.onFocus}
				required={this.props.required}
				disabled={this.props.disabled}
				minDate = {this.props.min ? new Date(this.props.min) : null}
				maxDate = {this.props.max ? new Date(this.props.max) : null}
				dateFormat="dd-MMM-yyyy"
				todayButton={"Today"}
				showMonthDropdown
				showYearDropdown
				disabledKeyboardNavigation
				popperContainer={DateCalendarContainer}
				dropdownMode="select"
				placeholderText={this.props.placeholder}
				onClickOutside={this.clickOutside}
				autoComplete="off"
				autoFocus={this.props.autoFocus ? this.props.autoFocus : false} />
		);
	}
}

export class DateTimeElement extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: this.props.value ? moment(this.props.value).format('DD-MMM-YYYY hh:mm A') : '',
			viewMode : 'days'
		};

		this.clickOutside = this.clickOutside.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	componentWillReceiveProps(nextprops) {
		this.setState({
			value: nextprops.value ? moment(nextprops.value).format('DD-MMM-YYYY hh:mm A') : ''
		});
	}

	onChange(value) {
		if(value && (this.props.value == null || this.props.value == '')) {
			let currentTime = moment(moment.now());
			if(value.get('hours') > 0 || value.get('minutes') > 0)
				currentTime = moment(value);

			value = value.set({
				hours: currentTime.get('hours'),
				minutes: currentTime.get('minutes')
			});
		}
		this.props.onChange((value == null || value == '') ? null : value._d)
	}

	clickOutside() {
		this.refs.picker.cancelFocusInput();
		this.refs.picker.setOpen(false);
	}

	render() {
		return (
			<DatePicker
				isClearable={this.props.disabled ? false : true}
				ref="picker"
				className={`form-control ${this.props.className}`}
				selected={this.props.value ? new Date(this.props.value) : null}
				onChange={(value) => this.onChange((value == null || value == '') ? null : moment(value))}
				onFocus={this.props.onFocus}
				required={this.props.required}
				disabled={this.props.disabled}
				minDate={this.props.min ? new Date(this.props.min) : null}
				maxDate={this.props.max ? new Date(this.props.max) : null}
				dateFormat="dd-MMM-yyyy hh:mm aa"
				timeIntervals={15}
				showTimeSelect
				showMonthDropdown
				showYearDropdown
				disabledKeyboardNavigation
				popperContainer={DateCalendarContainer}
				dropdownMode="select"
				placeholderText={this.props.placeholder}
				onClickOutside={this.clickOutside}
				autoComplete="off" />
		);
	}
}

export class TimeElement extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: this.props.value ? moment(this.props.value).format('hh:mm A') : ''
		};

		this.clickOutside = this.clickOutside.bind(this);
	}

	componentWillReceiveProps(nextprops) {
		this.setState({
			value: nextprops.value ? moment(nextprops.value).format('hh:mm A') : ''
		});
	}

	clickOutside() {
		this.refs.picker.cancelFocusInput();
		this.refs.picker.setOpen(false);
	}

	render() {
		return (
			<DatePicker
				isClearable={this.props.disabled ? false : true}
				ref="picker"
				className={`form-control ${this.props.className}`}
				selected={this.props.value ? new Date(this.props.value) : null}
				onChange={(value) => this.props.onChange((value == null || value == '') ? null : value)}
				required={this.props.required}
				disabled={this.props.disabled}
				dateFormat="hh:mm aa"
				timeIntervals={15}
				showTimeSelect
				showTimeSelectOnly
				disabledKeyboardNavigation
				popperContainer={DateCalendarContainer}
				dropdownMode="select"
				placeholderText={this.props.placeholder}
				onClickOutside={this.clickOutside}
				autoComplete="off" />
		);
	}
}

export class LocalSelect extends Component {
	constructor(props) {
		super(props);

		this.state = {
			uniqueid: uuidv1()
		};

		this.localselectref = React.createRef();

		this.onChange = this.onChange.bind(this);
		this.filterOption = this.filterOption.bind(this);
	}

	onChange(value) {
		if(this.props.multiselect) {
			this.props.onChange(value ? value.map((l) => l.value) : null, value ? value.map((l) => l.valueObj) : null);
			setTimeout(() => {
				this.setState({
					uniqueid: uuidv1()
				});
			}, 0);
		} else {
			this.props.onChange(value ? value.value : null, value ? (value.valueObj ? value.valueObj : {}) : {});
		}
	}

	filterOption(option, filter, currentValues) {
		this.filterinputvalue = filter;

		if(option.data.value == '<internal-option-create>')
			return true;

		let string = typeof(option.data.label) == 'string' ? option.data.label.toLowerCase() : '';

		return string.indexOf(filter.toLowerCase()) >= 0 ? true : false;
	}

	render() {
		let optionArray = [];
		let label = this.props.label ? this.props.label : 'name';
		let valuename = this.props.valuename ? this.props.valuename : 'id';
		if(this.props.options && this.props.options.length > 0 && typeof(this.props.options[0]) == 'string') {
			this.props.options.map(function(a) {
				optionArray.push({
					label : a,
					value : a
				});
			});
		} else {
			if(label && valuename && this.props.options) {
				optionArray = JSON.parse(JSON.stringify(this.props.options));
				optionArray.map(function(a) {
					a.valueObj = {...a};
					a.label = a[label];
					a.value = a[valuename];
					a.title = '';
				});
			}
		}

		let genvalue = null;

		let optionObj = {};
		optionArray.forEach(opt => {
			optionObj[opt.value] = opt;
		});

		if(this.props.multiselect) {
			if(this.props.value && this.props.value.length > 0) {
				genvalue = [];
				this.props.value.forEach((val) => {
					if(optionObj[val])
						genvalue.push(optionObj[val]);
					else
						genvalue.push({label: val, value: val});
				});
			}
		} else {
			if(this.props.value != null && this.props.value != undefined && this.props.value !== '') {
				if(optionObj[this.props.value])
					genvalue = optionObj[this.props.value];
				else
					genvalue = {label: this.props.value, value: this.props.value};
			}
		}

		return (
			<ReactSelectNormal
				closeMenuOnSelect={this.props.multiselect ? false : true}
				blurInputOnSelect={this.props.multiselect ? false : true}
				classNamePrefix="gs-react-select-perfix"
				className={`gs-react-select-${this.state.uniqueid} gs-react-select ${this.props.className} ${this.props.disabled ? 'ndtclass' : ''}`}
				options={optionArray}
				name={this.props.name}
				value={genvalue}
				isMulti={this.props.multiselect}
				components={{Option: (props) => OptionRenderer(props, this.filterinputvalue)}}
				filterOption={this.filterOption}
				onChange={this.onChange}
				placeholder={this.props.placeholder}
				isDisabled={this.props.disabled}
				required={this.props.required}
				isClearable={this.props.required ? false : true}
				autoFocus={this.props.autoFocus}
				menuPortalTarget={document.body}
				menuPosition={'absolute'}
				menuPlacement={'auto'}
				styles={selectCustomStyles}
				noOptionsMessage = {()=> 'No results found'}
				ref={this.localselectref} />
		);
	}
}

export const SelectAsync = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			optionArray : [],
			labelKey: this.props.label ? this.props.label : `name`,
			valueKey: this.props.valuename ? this.props.valuename : `id`,
			createResource: JSON.parse(JSON.stringify(createResourceObj))
		};

		this.getOptions = this.getOptions.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	componentWillMount() {
		this.getOptions();
	}

	componentWillReceiveProps(nextProps) {
		if(this.props.resource != nextProps.resource || this.props.filter != nextProps.filter  || this.props.refresh != nextProps.refresh) {
			this.setState({
				optionArray : [],
				labelKey: nextProps.label ? nextProps.label : `name`,
				valueKey: nextProps.valuename ? nextProps.valuename : `id`
			}, function() {
				this.getOptions();
			});
		}
	}

	getOptions() {
		if(this.props.resource == "companymaster") {
			return this.setState({
				optionArray : [this.props.app.selectedCompanyDetails]
			});
		}

		let createOptionArr = [];
		if(this.props.showadd && this.props.createOrEdit && this.state.createResource[this.props.resource] && checkActionVerbAccess(this.props.app, this.props.resource, 'Save'))
			createOptionArr.push({
				[`${this.state.labelKey}`]: 'Create New...',
				[`${this.state.valueKey}`]: '<internal-option-create>'
			});

		let fields = `${this.props.fields}${GetCustomFields(this.props.app, this.props.resource)}`;
		if(['leadstages', 'projectstages'].indexOf(this.props.resource) >= 0)
			fields += `,index`;

		let url = `/api/${this.props.resource}?field=${fields}&skip=0&pagelength=2000&filtercondition=${this.props.filter}`;
		if(this.props.app.myResources[this.props.resource].type == 'query')
			url = `/api/query/${this.props.resource}?${this.props.filter}`;

		axios.get(url).then((response) => {
			if(response.data.message != 'success')
				return this.setState({optionArray: []});

			response.data.main.sort((a, b) => {
				return (a[this.state.labelKey].toLowerCase() > b[this.state.labelKey].toLowerCase() ? 1 : (a[this.state.labelKey].toLowerCase() < b[this.state.labelKey].toLowerCase() ? -1 : 0));
			});
			if(['leadstages','projectstages'].indexOf(this.props.resource) >= 0)
				response.data.main.sort(function (a, b) {
					return (a.index < b.index) ? -1 : (a.index > b.index) ? 1 : 0;
				});
			if(['contractbillingschedules'].indexOf(this.props.resource) >= 0 && this.props.fields.indexOf('scheduleno') >= 0)
				response.data.main.sort(function (a, b) {
					return (a.scheduleno < b.scheduleno) ? -1 : (a.scheduleno > b.scheduleno) ? 1 : 0;
				});
			this.setState({
				optionArray: [...response.data.main, ...createOptionArr]
			});
			if(this.props.resource == 'numberingseriesmaster' && this.props.createParamFlag && !this.props.value) {
				if(response.data.main.length == 1)
					this.props.defaultValueUpdateFn(response.data.main[0].id, response.data.main[0]);
				else {
					for (var k = 0; k < response.data.main.length; k++) {
						if(response.data.main[k].isdefault) {
							this.props.defaultValueUpdateFn(response.data.main[k].id, response.data.main[k]);
							break;
						}
					}
				}
			}
		});
	}

	onChange(value, valueObj) {
		if(!value)
			return this.props.onChange(null, {});

		if(valueObj[this.state.valueKey] != '<internal-option-create>')
			return this.props.onChange(valueObj[this.state.valueKey], valueObj);

		if(this.props.createCallback)
			return this.props.createCallback();

		this.props.createOrEdit(this.state.createResource[this.props.resource].url, null, null, (valueObj) => {
			let optionArray = [...this.state.optionArray];
			optionArray.splice(optionArray.length-1, 0, valueObj);
			this.setState({
				optionArray
			});
			this.props.onChange(valueObj[this.state.valueKey], valueObj);
		});
	}

	render() {
		return (
			<LocalSelect 
				options={this.state.optionArray} 
				label={this.state.labelKey} 
				valuename={this.state.valueKey} 
				value={this.props.value}
				onChange={this.onChange} 
				placeholder={this.props.placeholder} 
				multiselect={this.props.multiselect} 
				autoFocus={this.props.autoFocus}
				className={this.props.className}
				required={this.props.required} 
				disabled={this.props.disabled} />
		);
	}
});

export const AutoSelect = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.autoselectref = React.createRef();

		this.state = {
			optionArray : [],
			labelKey: this.props.label ? this.props.label : `name`,
			displayLabelKey : this.props.displaylabel ? this.props.displaylabel : (this.props.label ? this.props.label : `name`),
			valueKey: this.props.valuename ? this.props.valuename : `id`,
			searchInputValue : '',
			createResource: JSON.parse(JSON.stringify(createResourceObj)),
			parentJson: (this.props.app.myResources[this.props.resource] && this.props.app.myResources[this.props.resource].type == 'lookup') ? this.props.app.myResources[this.props.resource].lookupResource : this.props.resource
		};

		if(this.props.value)
			this.getStateValue(this.props.value);

		this.getOptions = this.getOptions.bind(this);
		this.onChange = this.onChange.bind(this);
		this.renderSelect = this.renderSelect.bind(this);
		this.editItem = this.editItem.bind(this);
		this.inputRenderer = this.inputRenderer.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.value) {
			if(!this.state.value)
				this.getStateValue(nextProps.value);
			else if(nextProps.value != this.state.value[this.state.valueKey]) {
				this.getStateValue(nextProps.value);
			}
		} else {
			if(this.state.value)
				this.resetStateValue();
		}
	}

	getOptions(input, callback) {
		if(input == '' || input == undefined || input == null || input == '__' )
			return callback([]);

		let createOptionArr = [];
		if(this.props.showadd && this.props.createOrEdit && this.state.createResource[this.props.resource] && checkActionVerbAccess(this.props.app, this.props.resource, 'Save'))
			createOptionArr.push({
				[`${this.state.labelKey}`]: 'Create New...',
				[`${this.state.valueKey}`]: '<internal-option-create>'
			});

		let filterField = `${this.props.filter != "" && this.props.filter != undefined && this.props.filter != null ? this.props.filter : ''}`;
		filterField += `${filterField!="" ? ' and ' : ''} ${this.state.parentJson}.${this.state.labelKey}`;

		let filterString = `${filterField} ILIKE '%${input.replace(/ /g, '%%').replace(/'/g, "''")}%'`;

		let sortstring = `case when ${this.state.parentJson}.${this.state.labelKey} ILIKE '${input.replace(/'/g, "''")}' then 1 when ${this.state.parentJson}.${this.state.labelKey} ILIKE '${input.replace(/'/g, "''")}%' then 2 when ${this.state.parentJson}.${this.state.labelKey} ILIKE '%${input.replace(/'/g, "''")}%' then 3 else 4 end`;

		filterString = encodeURIComponent(filterString);
		sortstring = encodeURIComponent(sortstring);

		let url = `/api/${this.props.resource}?field=${this.props.fields}${GetCustomFields(this.props.app, this.props.resource)}&skip=0&pagelength=20&sortstring=${sortstring}&filtercondition=${filterString}`;

		if(this.props.app.myResources[this.props.resource].type == 'query')
			url = `/api/query/${this.props.resource}?${filterString}`;

		axios.get(url).then((response) => {
			if(response.data.message == 'success')
				callback([...response.data.main, ...createOptionArr]);
			else
				callback([]);
		});
	}

	onChange(value) {
		if(!value) {
			this.props.onChange(null, {});
			this.setState({
				value: null
			});
			return null;
		}

		if(value[this.state.valueKey] != '<internal-option-create>') {
			this.props.onChange(value[this.state.valueKey], value);
			this.setState({
				value: value
			});
			return null;
		}

		let tempObj = this.props.createParams ? this.props.createParams : null;

		if (['partners', 'customers', 'suppliers', 'competitors'].includes(this.props.resource)) {
			tempObj = {
				iscustomer: false,
				issupplier: false,
				iscompetitor: false
			};

			if (this.props.resource == 'customers') {
				tempObj.iscustomer = true;

				tempObj = {
					...tempObj,
					...this.props.resourceobj,
					param: this.props.transactionresourcename
				};
			}

			if (this.props.resource == 'suppliers')
				tempObj.issupplier = true;

			if (this.props.resource == 'competitors')
				tempObj.iscompetitor = true;
		}

		let routeURL = this.state.createResource[this.props.resource].url;

		this.props.createOrEdit(routeURL, null, tempObj, (valueObj) => {
			this.props.onChange(valueObj[this.state.valueKey], valueObj);
			this.setState({
				value: valueObj
			});
		});
	}

	editItem() {
		this.props.createOrEdit(this.state.createResource[this.props.resource].url, this.state.value[this.state.valueKey], null, (valueObj) => {
			this.setState({
				value: valueObj
			});
		});
	}

	getStateValue(selectedValue) {
		let filterString = `${this.state.parentJson}.${this.state.valueKey}=${selectedValue}`;
		let url = `/api/${this.props.resource}?field=${this.props.fields}&skip=0&pagelength=10&filtercondition=${filterString}`;

		if(this.props.app.myResources[this.props.resource].type == 'query')
			url = `/api/query/${this.props.resource}?${filterString}`;

		axios.get(url).then((response) => {
			let value = {
				[this.state.valueKey]: selectedValue,
				[this.state.labelKey]: selectedValue,
				[this.state.displayLabelKey]: selectedValue
			};
			if(response.data.message == 'success' && response.data.main.length > 0) {
				value = {
					[this.state.valueKey]: this.props.value,
					[this.state.labelKey]: response.data.main[0][this.state.labelKey],
					[this.state.displayLabelKey]: response.data.main[0][this.state.displayLabelKey]
				};
			}
			if(selectedValue && selectedValue == this.props.value)
				this.setState({value});
		});
	}

	resetStateValue() {
		setTimeout(function() {
			this.setState({value : null});
		}.bind(this), 0);
	}

	inputRenderer(args) {
		return <InputRenderer {...args} custvalueObj={{value: this.state.value, displayLabelKey:this.state.displayLabelKey, disabled: this.props.disabled, valueOnChange: this.onChange}} />
	}

	renderSelect() {
		return (
			<ReactSelectAsync
				blurInputOnSelect={true}
				getOptionValue={(option)=> option[this.state.valueKey]}
				getOptionLabel={(option)=> option[this.state.displayLabelKey]}
				loadOptions={_.debounce(this.getOptions, 200)}
				name={this.props.name}
				value={this.state.value}
				filterOption={() => true}
				inputValue={this.state.inputValue}
				onInputChange={inputValue => this.setState({ inputValue })}
				components={{
					Option: (props) => OptionRenderer(props, this.state.inputValue, this.state.labelKey),
					LoadingIndicator: LoadingIndicator,
					Input: this.inputRenderer
				}}
				onChange={this.onChange}
				classNamePrefix="gs-react-select-perfix"
				className={`gs-react-select ${this.props.className} ${this.props.disabled ? 'ndtclass' : ''} ${this.props.showedit ? 'gs-select-input-group' : ''}`}
				placeholder={this.props.placeholder}
				required={this.props.required}
				isClearable={this.props.required ? false : true}
				isDisabled={this.props.disabled}
				autoFocus={this.props.autoFocus}
				menuPortalTarget={document.body}
				menuPosition={'absolute'}
				menuPlacement={'auto'}
				styles={selectCustomStyles}
				noOptionsMessage = {()=> this.autoselectref && this.autoselectref.current.state && this.autoselectref.current.state.inputValue && this.autoselectref.current.state.inputValue != '__' ? 'No results found' : 'Type to search'}
				ref={this.autoselectref} />
		);
	}

	render() {
		if(this.props.showedit && this.props.createOrEdit && this.state.createResource[this.props.resource] && checkActionVerbAccess(this.props.app, this.props.resource, 'Save')) {
			return (
				<div className="input-group">
					{this.renderSelect()}
					<div className="input-group-append">
						<button className={`btn btn-sm gs-form-btn-success`}  type="button" disabled={this.state.value ? false : true} onClick={() => {this.editItem()}}><span className="fa fa-edit"></span></button>
					</div>
				</div>
			);
		}

		return this.renderSelect();
	}
});

export class InputRenderer extends Component {
	constructor(props) {
		super(props);

		this.state = {};
		this.onFocus = this.onFocus.bind(this);
		this.onBlur = this.onBlur.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	onFocus(evt) {
		let value = this.props.custvalueObj.value ? this.props.custvalueObj.value[this.props.custvalueObj.displayLabelKey] : '';

		this.state.value = value;
		this.setState({
			value: value
		});
		this.props.onFocus();
		if(value != '') {
			evt.target.value = this.props.custvalueObj.creatable ? value : '__';
			this.props.onChange(evt);
		} else if(this.props.custvalueObj.searchwo) {
			evt.target.value = '<searchwo>';
			this.props.onChange(evt);
		}
	}

	onBlur() {
		this.setState({
			value: ''
		});
		this.props.onBlur();
	}

	onChange(evt) {
		this.props.onChange(evt);

		if(evt.target.value == '')
			this.props.custvalueObj.valueOnChange(null);
	}

	render() {
		let allProps = {
			...this.props
		};
		delete allProps.custvalueObj;

		return (
			<components.Input
				{...allProps}
				onChange={this.onChange}
				value={this.props.value == '<searchwo>' ? '' : this.props.value == '__' ? this.state.value : this.props.value}
				onFocus={this.onFocus}
				onBlur={this.onBlur} />
		);
	}
}

export const AutoMultiSelect = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isAutoSelect: true,
			labelKey: this.props.label ? this.props.label : `name`,
			displayLabelKey : this.props.displaylabel ? this.props.displaylabel : (this.props.label ? this.props.label : `name`),
			valueKey: this.props.valuename ? this.props.valuename : `id`,
			searchInputValue : '',
			parentJson: (this.props.app.myResources[this.props.resource] && this.props.app.myResources[this.props.resource].type == 'lookup') ? this.props.app.myResources[this.props.resource].lookupResource : this.props.resource
		};

		this.checkSelectAsync();

		this.getOptions = this.getOptions.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.value) {
			if(!this.state.value)
				this.getStateValue([...nextProps.value]);
			else {
				let newValue = [...nextProps.value].sort().join();
				let oldValue = [...this.state.value].map(item => item[this.state.valueKey]).sort().join();
				if(newValue != oldValue)
					this.getStateValue([...nextProps.value]);
			}
		} else {
			if(this.state.value)
				this.resetStateValue();
		}
	}

	checkSelectAsync() {
		if (this.props.resource == "companymaster") {
			this.setState({
				isAutoSelect: false,
				defaultOptions: [...this.props.app.companyArray]
			});

			if(this.props.value)
				this.getStateValue(this.props.value);

			return null;
		}

		let filterField = `${this.props.filter != "" && this.props.filter != undefined && this.props.filter != null ? this.props.filter : ''}`;

		let filterString = encodeURIComponent(filterField);

		let url = `/api/${this.props.resource}?field=${this.props.fields}&skip=0&pagelength=200&filtercondition=${filterString}`;

		axios.get(url).then((response) => {
			if(response.data.message == 'success') {
				response.data.count = Number(response.data.count);
				if(response.data.count <= 200) {
					this.setState({
						isAutoSelect: false,
						defaultOptions: response.data.main
					});
				} else {
					this.setState({
						isAutoSelect: true
					});
					if(this.props.value)
						this.getStateValue(this.props.value);
				}
			}
		});
	}

	getOptions(input, callback) {
		if(input == '' || input == undefined || input == null || input == '__' || this.props.resource == 'companymaster')
			return callback(this.props.resource == 'companymaster' ? [...this.props.app.companyArray] : []);

		let filterField = `${this.props.filter != "" && this.props.filter != undefined && this.props.filter != null ? this.props.filter : ''}`;
		filterField += `${filterField!="" ? ' and ' : ''} ${this.state.parentJson}.${this.state.labelKey}`;

		let filterString = `${filterField} ILIKE '%${input.replace(/ /g, '%%').replace(/'/g, "''")}%'`;

		let sortstring = `case when ${this.state.parentJson}.${this.state.labelKey} ILIKE '${input.replace(/'/g, "''")}' then 1 when ${this.state.parentJson}.${this.state.labelKey} ILIKE '${input.replace(/'/g, "''")}%' then 2 when ${this.state.parentJson}.${this.state.labelKey} ILIKE '%${input.replace(/'/g, "''")}%' then 3 else 4 end`;

		filterString = encodeURIComponent(filterString);
		sortstring = encodeURIComponent(sortstring);

		let url = `/api/${this.props.resource}?field=${this.props.fields}&skip=0&pagelength=20&sortstring=${sortstring}&filtercondition=${filterString}`;

		axios.get(url).then((response) => {
			if(response.data.message == 'success') {
				callback([...response.data.main]);
			} else {
				callback([]);
			}
		});
	}

	onChange(value, valueobj) {
		if(!this.state.isAutoSelect)
			return this.props.onChange(value, valueobj);

		if(value) {
			this.props.onChange(value.map(item => item[[this.state.valueKey]]), value);
			this.state.value = value;
			this.setState({
				value: value
			});
		} else {
			this.props.onChange(null, {});
			this.setState({
				value: null
			});
		}
	}

	getStateValue(selectedValue) {
		if(selectedValue && selectedValue.length === 0)
			return null;

		let filterString = `${this.state.parentJson}.${this.state.valueKey} IN (${selectedValue.join(', ')})`;

		let url = `/api/${this.props.resource}?field=${this.props.fields}&skip=0&pagelength=10&filtercondition=${filterString}`;

		axios.get(url).then((response) => {
			let value = [];
			selectedValue.forEach(val => {
				value.push({
					[this.state.valueKey]: val,
					[this.state.labelKey]: val,
					[this.state.displayLabelKey]: val
				});
			});
			let currentvalue = [...(this.props.value || [])].sort();
			selectedValue.sort();
			if(response.data.message == 'success' && response.data.main.length > 0 && this.props.value && this.props.value.length > 0 && selectedValue.join() == currentvalue.join()) {
				let valresObj = {};
				response.data.main.forEach((resitem) => {
					valresObj[resitem[this.state.valueKey]] = resitem
				});

				value.forEach(val => {
					if(valresObj[val[this.state.valueKey]]) {
						val[this.state.labelKey] = valresObj[val[this.state.valueKey]][this.state.labelKey];
						val[this.state.displayLabelKey] = valresObj[val[this.state.valueKey]][this.state.displayLabelKey];
					}
				});
			}
			if (selectedValue && selectedValue.join() == currentvalue.join())
				this.setState({
					value
				});
		});
	}

	resetStateValue() {
		setTimeout(() => {
			this.setState({value : null});
		}, 0);
	}

	renderSelectAsync() {
		return (
			<ReactSelectAsync
				getOptionValue={(option)=> option[this.state.valueKey]}
				getOptionLabel={(option)=> option[this.state.displayLabelKey]}
				loadOptions={_.debounce(this.getOptions, 200)}
				name={this.props.name}
				value={this.state.value}
				filterOption={() => true}
				isMulti={true}
				components={{
					LoadingIndicator: LoadingIndicator,
					Option: (props) => OptionRenderer(props, this.state.searchInputValue, this.state.labelKey)
				}}
				onChange={this.onChange}
				classNamePrefix="gs-react-select-perfix"
				className={`gs-react-select ${this.props.className} ${this.props.disabled ? 'ndtclass' : ''} ${this.props.showedit ? 'gs-select-input-group' : ''}`}
				placeholder={this.props.placeholder}
				required={this.props.required}
				isClearable={this.props.required ? false : true}
				isDisabled={this.props.disabled}
				autoFocus={this.props.autoFocus}
				menuPortalTarget={document.body}
				menuPosition={'absolute'}
				menuPlacement={'auto'}
				noOptionsMessage = {()=> this.autoselectref && this.autoselectref.current.state && this.autoselectref.current.state.inputValue && this.autoselectref.current.state.inputValue != '__' ? 'No results found' : 'Type to search'}
				ref={this.autoselectref}
				styles={selectCustomStyles} />
		);
	}

	renderSelect() {
		return (
			<LocalSelect 
				options={this.state.defaultOptions} 
				label={this.state.labelKey} 
				valuename={this.state.valueKey} 
				value={this.props.value}
				onChange={this.onChange} 
				placeholder={this.props.placeholder} 
				multiselect={true} 
				autoFocus={this.props.autoFocus}
				className={this.props.className}
				required={this.props.required} 
				disabled={this.props.disabled} />
		);
	}

	render() {
		if(this.state.isAutoSelect)
			return this.renderSelectAsync();
		else
			return this.renderSelect();
	}
});

export const DisplayGroup = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.displaygroupref = React.createRef();

		this.state = {
			optionArray : this.genOptions(this.props.child)
		};

		this.getOptions = this.getOptions.bind(this);
		this.genOptions = this.genOptions.bind(this);
		this.onChange = this.onChange.bind(this);
		this.inputRenderer = this.inputRenderer.bind(this);
		this.filterOption = this.filterOption.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			optionArray : this.genOptions(nextProps.child)
		});
	}

	genOptions(array) {
		let displaygrouparray = [];
		if(!array || array.length == 0)
			return displaygrouparray;

		array.forEach(item => {
			if(!item.displaygroup)
				return null;

			if(!displaygrouparray.map(opt => opt.value).includes(item.displaygroup))
				displaygrouparray.push({
					label: item.displaygroup,
					value: item.displaygroup
				});
		});
		return displaygrouparray;
	}

	getOptions(input, callback) {
		let array = [...this.state.optionArray];

		if(input && input != '' && !array.map(opt => opt.label).includes(input))
			array = [{
				label: `Create Option "${input}"`,
				value: input
			}, ...this.state.optionArray];

		callback(array);
	}

	onChange(value) {
		this.props.onChange(value ? value.value : null);
	}

	inputRenderer(args) {
		let value = this.props.value ? {
			label: this.props.value,
			value: this.props.value
		} : null;

		return <InputRenderer {...args} custvalueObj={{value: value, displayLabelKey: 'label', disabled: this.props.disabled, valueOnChange: this.onChange, creatable: true}} />
	}

	filterOption(option, filter, currentValues) {
		this.state.searchInputValue = filter;

		let string = typeof(option.data.label) == 'string' ? option.data.label.toLowerCase() : '';

		return string.indexOf(filter.toLowerCase()) >= 0 ? true : false;
	}
	
	render() {
		let value = this.props.value ? {
			label: this.props.value,
			value: this.props.value
		} : null;
		return (
			<ReactSelectAsync
				blurInputOnSelect={true}
				loadOptions={_.debounce(this.getOptions, 200)}
				defaultOptions={true}
				options={this.state.optionArray}
				name={this.props.name}
				value={value}
				components={{
					Option: (props) => OptionRenderer(props, this.state.searchInputValue, this.state.labelKey),
					LoadingIndicator: LoadingIndicator,
					Input: this.inputRenderer
				}}
				filterOption={this.filterOption}
				onChange={this.onChange}
				classNamePrefix="gs-react-select-perfix"
				className={`gs-react-select ${this.props.className} ${this.props.disabled ? 'ndtclass' : ''} ${this.props.showedit ? 'gs-select-input-group' : ''}`}
				placeholder={this.props.placeholder}
				required={this.props.required}
				isClearable={this.props.required ? false : true}
				isDisabled={this.props.disabled}
				autoFocus={this.props.autoFocus}
				menuPortalTarget={document.body}
				menuPosition={'absolute'}
				menuPlacement={'auto'}
				styles={selectCustomStyles}
				noOptionsMessage = {()=> this.displaygroupref.current.state && this.displaygroupref.current.state.inputValue ? 'No results found' : 'Type to search'}
				ref={this.displaygroupref} />
		);
	}
});

export const AutoSuggest = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.state = {};

		this.getOptions = this.getOptions.bind(this);
		this.onChange = this.onChange.bind(this);
		this.inputRenderer = this.inputRenderer.bind(this);
		this.filterOption = this.filterOption.bind(this);
	}

	getOptions(input, callback) {
		if(input == '' || input == undefined || input == null)
			return callback([]);

		let internalCb = (array) => {
			if(input && input != '' && !array.map(opt => opt.label).includes(input))
				array = [{
					label: `Create Option "${input}"`,
					value: input
				}, ...array];

			return callback(array);
		};

		if(this.props.resource == 'addresses' && this.props.field == 'country') {
			var tempArray = [];
			this.props.app.utildata.countries.map(function(a) {
				tempArray.push({
					label: a.name,
					value: a.name
				});
			});
			return internalCb(tempArray);
		}

		if(this.props.resource == 'addresses' && this.props.field == 'state' && this.props.address_country == 'India') {
			var tempArray = [];
			this.props.app.utildata.india_states.map(function(a) {
				tempArray.push({
					label: a.name,
					value: a.name
				});
			});
			return internalCb(tempArray);
		}

		if(this.props.resource == 'feedbackcontactperson') {
			return internalCb(this.props.feedbackContactArray.map(item => {
				return {
					label: item.name,
					value: item.name
				};
			}));
		}

		var queryString = `/api/query/autosuggestquery?field=${this.props.field}&resource=${this.props.resource}&filter=${input}`;

		axios.get(queryString).then((response) => {
			if(response.data.message == 'success')
				internalCb(response.data.main.map(item => {
					return {
						label: item.name,
						value: item.name
					}
				}));
			else
				internalCb([]);
		});
	}

	onChange(value) {
		this.props.onChange(value ? value.value : null);
	}

	inputRenderer(args) {
		let value = this.props.value ? {
			label: this.props.value,
			value: this.props.value
		} : null;
		return <InputRenderer {...args} custvalueObj={{value: value, displayLabelKey: 'label', disabled: this.props.disabled, valueOnChange: this.onChange, creatable: true}} />
	}

	filterOption(option, filter, currentValues) {
		this.state.searchInputValue = filter;

		let string = typeof(option.data.label) == 'string' ? option.data.label.toLowerCase() : '';

		return string.indexOf(filter.toLowerCase()) >= 0 ? true : false;
	}

	render() {
		let value = this.props.value ? {
			label: this.props.value,
			value: this.props.value
		} : null;

		return (
			<ReactSelectAsync
				blurInputOnSelect={true}
				loadOptions={_.debounce(this.getOptions, 200)}
				filterOption={this.filterOption}
				name={this.props.name}
				value={value}
				components={{
					Option: (props) => OptionRenderer(props, this.state.searchInputValue, this.state.labelKey),
					LoadingIndicator: LoadingIndicator,
					Input: this.inputRenderer
				}}
				onChange={this.onChange}
				classNamePrefix="gs-react-select-perfix"
				className={`gs-react-select ${this.props.className} ${this.props.disabled ? 'ndtclass' : ''} ${this.props.showedit ? 'gs-select-input-group' : ''}`}
				placeholder={this.props.placeholder}
				required={this.props.required}
				isClearable={this.props.required ? false : true}
				isDisabled={this.props.disabled}
				autoFocus={this.props.autoFocus}
				menuPortalTarget={document.body}
				menuPosition={'absolute'}
				menuPlacement={'auto'}
				styles={selectCustomStyles} />
		);
	}
});

export class Init extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		if(this.props.initfn && typeof(this.props.initfn) == 'function')
			this.props.initfn();
	}

	render() {
		return (
			<div className={this.props.className}>
				{this.props.children}
			</div>
		);
	}
}

export class InfoModal extends Component {
	constructor(props) {
		super(props);
		this.renderBodyArray = this.renderBodyArray.bind(this);
		this.renderAdditionalInfo = this.renderAdditionalInfo.bind(this)
	}

	renderBodyArray() {
		if(!this.props.bodyArray || this.props.bodyArray.length == 0)
			return null;
		return (
			<ul>
				{this.props.bodyArray.map((item, index) => <li key={index}>{typeof(item) == 'string' ? item : (typeof(item) == 'object' ? item.toString() : item)}</li>)}
			</ul>
		);
	}

	renderAdditionalInfo() {
		if(!this.props.additionalInfo)
			return null;
		return (
			<div>
				<br></br>
				<br></br>
				<br></br>
				<span>Additional Information : {this.props.additionalInfo}</span>
			</div>
		);
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5>{this.props.header}</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<p>{this.props.body}</p>
					{this.renderBodyArray()}
					{this.renderAdditionalInfo()}
				</div>
				<div className="react-modal-footer text-right">
					<button type="button" className="btn btn-sm btn-primary" onClick={this.props.closeModal}>{this.props.btnArray ? this.props.btnArray[0] : 'Ok'}</button>
				</div>
			</div>
		);
	}
}

export class ConfirmModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			confirmloader: false
		};
		this.cancel = this.cancel.bind(this);
		this.renderBodyArray = this.renderBodyArray.bind(this);
	}

	renderBodyArray() {
		if(!this.props.bodyArray || this.props.bodyArray.length == 0)
			return null;
		return (
			<ul>
				{this.props.bodyArray.map((item, index) => <li key={index}>{typeof(item) == 'string' ? item : (typeof(item) == 'object' ? item.toString() : item)}</li>)}
			</ul>
		);
	}

	cancel (result) {
		this.setState({confirmloader: true}, ()=>{
			this.props.callback(result);
			this.props.closeModal();
		});
	}

	render() {
		return (
			<div>
				{!this.props.warningModal ? <div className="react-modal-header">
					<h5>{this.props.header}</h5>
				</div> : null}
				{this.props.warningModal ? <div className="react-modal-body react-modal-body-scroll-wt-height text-center">
					<img src="/images/warning.png" style={{width: "150px", height: "100px", background: "transparent"}} />
					<div><b>{this.props.body}</b></div>
					<div>{this.props.btnTitle}</div>
				</div> : <div className="react-modal-body react-modal-body-scroll-wt-height">
					<p>{this.props.body}</p>
					{this.renderBodyArray()}
					{this.props.bodyArray && this.props.bodyArray.length > 0 ? <br></br> : null}
					{this.props.btnTitle}
				</div> }
				<div className={`react-modal-footer ${this.props.warningModal ? 'text-center bordertop-0' : 'text-right'}`}>
					<button type="button" className="btn btn-sm gs-btn-warning btn-width" disabled={this.state.confirmloader} onClick={()=>this.cancel(false)}>{this.props.btnArray[1]}</button>
					<button type="button" className="btn btn-sm gs-btn-success btn-width" disabled={this.state.confirmloader} onClick={()=>this.cancel(true)}>{this.props.btnArray[0]}</button>
				</div>
			</div>
		);
	}
}

export class ReviseModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			confirmloader: false,
			reason : ''
		};
		this.cancel = this.cancel.bind(this);
	}

	cancel (result) {
		this.props.callback(result, this.state.reason);
		this.props.closeModal();
	}

	render() {
		let errorClass = this.state.reason != '' && this.state.reason != null ? '' : 'errorinput';
		return (
			<div>
				<div className="react-modal-header borderbottom-0">
					<h5 className="gs-text-color">Comments for Revision</h5>
				</div>
				<div className="react-modal-body">
					<div className="row justify-content-center">
						<div className="col-md-12 col-sm-12">
							<textarea rows="5" className={`form-control ${errorClass}`} value={this.state.reason} onChange={(event) => this.setState({reason: event.target.value})} ></textarea>
						</div>
					</div>
				</div>
				<div className="react-modal-footer text-center bordertop-0">
					<button type="button" className="btn btn-sm gs-form-btn-success btn-width" onClick={()=>this.cancel(false)}>Cancel</button>
					<button type="button" className="btn btn-sm gs-btn-success btn-width" disabled={errorClass == 'errorinput' ? true : false} onClick={()=>this.cancel(true)}>Ok</button>
				</div>
			</div>
		);
	}
}

export class ReasonModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			reason : '',
			competitorid : this.props.competitorid
		};
		this.cancel = this.cancel.bind(this);
	}

	cancel (result) {
		this.props.callback(result, this.state.reason, this.state.lostreasonid, this.state.competitorid);
		this.props.closeModal();
	}

	render() {
		let errorReasonClass = this.state.reason != '' && this.state.reason != null ? '' : 'errorinput';
		let errorlostClass = this.props.actionverb != 'Lost' || this.state.lostreasonid > 0 ? '' : 'errorinput';
		return (
			<div>
				<div className="react-modal-header borderbottom-0">
					<h5 className="gs-text-color">Reason For {this.props.actionverb}</h5>
				</div>
				<div className="react-modal-body">
					<div className="row justify-content-center">
						{this.props.actionverb == 'Lost' ? <div className="col-md-10 form-group">
							<label className="labelclass">Lost Reason</label>
							<SelectAsync className={`${errorlostClass}`} resource="saleslostreasons" fields="id,name" value={this.state.lostreasonid} onChange={(lostreasonid) => this.setState({lostreasonid})}></SelectAsync>
						</div> : null}
					</div>
					<div className="row justify-content-center">
						<div className="col-md-10 form-group">
							<label className="labelclass">Remarks</label>
							<textarea  rows="5" className={`form-control ${errorReasonClass}`} value={this.state.reason} onChange={(event) => this.setState({reason: event.target.value})} ></textarea>
						</div>
					</div>
					<div className="row justify-content-center">
						{this.props.actionverb == 'Lost' && this.props.app.feature.useCompetitiveQuotations ? <div className="col-md-10 form-group">
							<label className="labelclass">Competitor</label>
							<AutoSelect resource="partners" fields="id,name" valuename="id" label="name" value={this.state.competitorid} filterstring="partners.iscompetitor and partners.name" onChange={(competitorid) => this.setState({competitorid})}></AutoSelect>
						</div> : null}
					</div>
				</div>
				<div className="react-modal-footer text-center bordertop-0">
					<button type="button" className="btn btn-sm gs-btn-warning btn-width" onClick={()=>this.cancel(false)}>Cancel</button>
					<button type="button" className="btn btn-sm gs-btn-success btn-width" disabled={errorReasonClass == 'errorinput' || errorlostClass == 'errorinput' ? true : false} onClick={()=>this.cancel(true)}>Save</button>
				</div>
			</div>
		);
	}
}

export class ExcelSyncModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true
		};

		this.onLoad = this.onLoad.bind(this);
		this.copyOTP = this.copyOTP.bind(this);
		this.reducetimeRemaining = this.reducetimeRemaining.bind(this);

		this.onLoad();
	}

	onLoad() {
		axios.get(`/api/common/methods/generateTOTP`).then((response) => {
			if(response.data.message == 'success') {
				this.setState({
					result: response.data.main,
					loading: false
				});
				setTimeout(this.reducetimeRemaining, 1000);
			} else {
				this.setState({
					loading: false
				});
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	reducetimeRemaining() {
		let { result } = this.state;
		result.timeRemaining = result.timeRemaining - 1;

		this.setState({ result });

		if(result.timeRemaining > 0)
			setTimeout(this.reducetimeRemaining, 1000);
	}

	copyOTP() {
		if(this.state.result.timeRemaining <= 0) {
			return this.setState({
				loading: true
			}, () => {
				this.onLoad();
			});
		}

		commonMethods.copyToClipBoard(this.state.result.otp);

		modalService['infoMethod']({
			header : 'Success!',
			body : 'OTP copied',
			btnArray : ['Ok'],
			isToast: true
		});
	}

	render() {
		let { result, loading } = this.state;
		return (
			<div>
				<div className="react-modal-header d-flex justify-content-between" style={{padding: '16px 22px'}}>
					<div className="flex-column">
						<h6 className="modal-title gs-text-color">Excel sync OTP</h6>
					</div>
					<div className="flex-column">
						<button type="button" className="close" aria-label="Close" onClick={this.props.closeModal}><span aria-hidden="true">&times;</span></button>
					</div>
				</div>
				<div className="react-modal-body">
					<div className="row justify-content-center">
						<div className="col-md-6 col-sm-6 form-group">
							<button type="button" className="btn btn-sm gs-btn-outline-success btn-width" style={{minWidth: '160px', letterSpacing: (!loading && result.timeRemaining > 0 ? '3px' : '0px')}} onClick={() => this.copyOTP()} disabled={loading}> {loading ? 'Loading...' : (result.timeRemaining > 0 ? result.otp : 'Get OTP')}</button> {!loading && result.timeRemaining > 0 ? moment().startOf('day').add({s: result.timeRemaining}).format('mm:ss') : null}
						</div>
						<div className="col-md-10 col-sm-10 col-xs-10 text-center form-group">
							<span>For security purposes, this OTP is valid for only 2 minutes.</span>
							<span>Please use immediately.</span>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export const ChildEditModal = connect((state, props) => {
	return {
		form : props.form,
		destroyOnUnmount: false,
		resource : state.form[props.form] ? (state.form[props.form].values ? state.form[props.form].values : null) : null,
		formData : state.form,
		fullstate : state
	}
})(reduxForm()(class extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div>
				{React.cloneElement(this.props.getBody(), {...this.props})}
			</div>
		);
	}
}));

export class SessionExpireModal extends Component {
	constructor(props) {
		super(props);

		this.buttonOnClick = this.buttonOnClick.bind(this);
	}

	buttonOnClick() {
		window.location.reload();
		//this.props.closeModal();
	}

	render() {
		return (
			<div>
				<div className="react-modal-body text-center">
					<img src="/images/session-expired.png" style={{background: "transparent"}} />
					<div className="margintop-30 marginbottom-20"><b className="font-20">You have been logged out.</b></div>
					<div>Please login to use GrowSmart</div>
					<div>Please contact customer support for any queries.</div>
				</div>
				<div className="react-modal-footer bordertop-0 text-center">
					<button type="button" className="btn btn-sm btn-width gs-btn-success" onClick={this.buttonOnClick}>Login</button>
				</div>
			</div>
		);
	}
}

export class ConnectionErrorModal extends Component {
	constructor(props) {
		super(props);

		this.buttonOnClick = this.buttonOnClick.bind(this);
	}

	buttonOnClick() {
		this.props.callback();
		this.props.closeModal();
	}

	render() {
		return (
			<div>
				<div className="react-modal-body text-center">
					<img src="/images/no-internet.png" style={{background: "transparent"}} />
					<div className="margintop-30 marginbottom-20"><b className="font-20">Internet connectivity problem!</b></div>
					<div>Unable to connect with the server. Please check your internet and try again.</div>
				</div>
				<div className="react-modal-footer bordertop-0 text-center">
					<button type="button" className="btn btn-sm btn-width gs-btn-success" onClick={this.buttonOnClick}>Ok</button>
				</div>
			</div>
		);
	}
}

export class TimeOutModal extends Component {
	constructor(props) {
		super(props);

		this.buttonOnClick = this.buttonOnClick.bind(this);
	}

	buttonOnClick() {
		this.props.closeModal();
	}

	render() {
		return (
			<div>
				<div className="react-modal-body text-center">
					<img src="/images/timeout.png" style={{background: "transparent"}} />
					<div className="margintop-30 marginbottom-20"><b className="font-20">Taking longer than usual!</b></div>
					<div className="marginbottom-20">Sorry, it is taking longer than expected. Please try the following:</div>
					<ol>
						<li>Check your connectivity and try again.</li>
						<li>Reload the page once and try again.</li>
						<li>If you still face the issue, please reach out to support.</li>
					</ol>
				</div>
				<div className="react-modal-footer bordertop-0 text-center">
					<button type="button" className="btn btn-sm btn-width gs-btn-success" onClick={this.buttonOnClick}>Ok</button>
				</div>
			</div>
		);
	}
}

export class ItemRateField extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		if (this.props.item && !this.props.item.rate && typeof(this.props.item.rate)!='number') {
			return null;
		}

		let currencyid = this.props.currencyid;
		if ((!this.props.currencyid || this.props.app.currency[this.props.currencyid] == "undefined" || !this.props.app.currency[this.props.currencyid]) && this.props.currencyid !='qty') {
			currencyid = this.props.app.defaultCurrency;
		}

		let discountstr, calculatedFinalRate;
		if(this.props.item.discountquantity != null && typeof(this.props.item.discountquantity) == 'number') {
			discountstr = (this.props.item.discountmode == 'Percentage' ? (Number(this.props.item.discountquantity.toFixed(2)) + ' %') : currencyFilter(this.props.item.discountquantity, this.props.currencyid, this.props.app));
			calculatedFinalRate = this.props.item.discountmode == 'Percentage' ? Number((this.props.item.rate-(this.props.item.rate*(this.props.item.discountquantity/100))).toFixed(this.props.app.roundOffPrecision)) : Number((this.props.item.rate - this.props.item.discountquantity).toFixed(this.props.app.roundOffPrecision));
		} else {
			discountstr = (this.props.item.discountmode == 'Percentage' ? ('0 %') : currencyFilter(0, this.props.currencyid, this.props.app));
			calculatedFinalRate = this.props.item.rate;
		}

		return (
			<div>
				<span style={{color:'gray'}}>{currencyFilter(this.props.item.rate, this.props.currencyid, this.props.app)} - </span>
				<span style={{color:'#F19696'}}>{discountstr}</span>
				<span> = {currencyFilter(calculatedFinalRate, this.props.currencyid, this.props.app)}</span>
			</div>
		);
	}
}

export class AccountBalance extends Component {
	constructor(props) {
		super(props);

		this.state = {
			accountbalance: '',
			onRefreshCalled: false,
			item: this.props.item
		};
	}

	componentWillMount() {
		this.onLoad();
	}

	componentWillReceiveProps(nextprops) {
		if(nextprops.item.companyid != this.state.item.companyid || nextprops.item.accountid != this.state.item.accountid || nextprops.item.type != this.state.item.type || nextprops.item.againstid != this.state.item.againstid || nextprops.item.partnerid != this.state.item.partnerid || nextprops.item.employeeid != this.state.item.employeeid) {
			this.setState({
				item: nextprops.item
			},  () => {
				this.onLoad();
			});
		}
	}

	onLoad(param) {
		if(!this.props.item.companyid || !this.props.item.accountid || !this.props.item.type || !this.props.item.againstid || (!this.props.item.partnerid && !this.props.item.employeeid))
			return this.setState({
				accountbalance: ''
			});
		else if(this.props.balance != null)
			return this.updateAccountBalance(this.props.balance, this.props.balance);

		axios.get(`/api/query/getaccountbalancequery?companyid=${this.props.item.companyid}&accountid=${this.props.item.accountid}&type=${this.props.item.type}&againstid=${this.props.item.againstid}&partnerid=${this.props.item.partnerid}&employeeid=${this.props.item.employeeid}`).then((response) => {
			if(response.data.message == 'success') {
				let credit = response.data.main.creditBalance;
				let debit = response.data.main.debitBalance;
				let currency = response.data.main.currency;

				this.updateAccountBalance(credit, debit, currency, null);
			}
		});
	}

	updateAccountBalance(credit, debit, currency, onRefreshCalled) {
		currency = currency ? currency : this.props.currencyid;

		let model = (this.props.item.show == "both" || this.props.item.show == "credit" ? currencyFilter(credit, currency, this.props.app) : '') + (this.props.item.show == "both" ? " Cr , " : '') + (this.props.item.show == "both" || this.props.item.show == "debit" ? currencyFilter(debit, currency, this.props.app) : '') + (this.props.item.show=="both" ? " Dr" : '');

		this.setState({
			accountbalance: model
		});

		this.props.updateFn(model);
	}

	render() {
		if(this.props.showAs == 'text')
			return <span>{this.state.accountbalance}</span>

		return (
			<input type="text" className="form-control" value={this.state.accountbalance} disabled={true} />
		);
	}
}

export class AlertView extends Component {
	constructor(props) {
		super(props);

		this.state = {
			statusArray: ['Submitted','Revise','Reviewed','Send for Modification','Lost','Junk','Rejected']
		};
	}

	render() {
		let comments = null;
		let notes = [...this.props.notes];
		notes.sort((a, b)=>{
			return b.id - a.id;
		});
		if(this.state.statusArray.indexOf(this.props.status) >= 0) {
			for (var j = 0; j < notes.length; j++) {
				if (notes[j].notes != null && notes[j].notes != '') {
					comments = notes[j];
					break;
				}
			}
		}
		if(!comments)
			return null;
		return (
			<div>
				<div className="alert alert-info alert-dismissible" role="alert">
					<button type="button" className="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
						<span className="sr-only">Close</span>
					</button>
					<p className="fontMedium text-left">{comments.notes}</p>
					<span className="small" ><i ng-if="comments.isrestricted" className="fa fa-lock">&nbsp;</i>Commented by <b> {comments.createdby_displayname}</b> @ {comments.created}</span>
				</div>
			</div>
		);
	}
}

export class TimeAgo extends Component {
	constructor(props) {
		super(props);

		this.formatter = this.formatter.bind(this);
	}

	formatter(value, unit, suffix, epochSeconds) {
		let title = `${value} ${unit}${value > 1 ? 's' : ''} ${suffix}`;
		let tooltip = `${datetimeFilter(new Date(epochSeconds).toISOString())}`;
		return <span className={this.props.className} title={tooltip}>{title}</span>;
	}

	render() {
		return (
			<TimeAgoLibrary date={this.props.date} formatter={this.formatter} minPeriod={60} />
		);
	}
}

export class Timer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			start: 0,
			callendtime: this.props.callendtime
		};
		this.interval='';
	}

	componentWillMount() {
		this.interval = setInterval(() => {
			this.setState({
				start: this.state.start++
			});
		}, 1000);
	}

	componentWillReceiveProps(nextprops){
		if(nextprops.callendtime != this.state.callendtime){
			clearTimeout(this.interval);
			this.interval='';
		}
	}

	render() {
		let currentTime = this.props.callendtime ? new Date(this.props.callendtime) : new Date();
		let startTime = new Date(this.props.starttime);
		let totalsec = Math.abs(currentTime - startTime) / 1000;
		let hours = Math.floor(totalsec / 3600) % 2;
		let mins = Math.floor(totalsec / 60) % 60;
		let seconds = Math.floor(totalsec % 60);
		hours = hours < 10 ? '0' + hours : hours;
		mins = mins < 10 ? '0' + mins : mins;
		seconds = seconds < 10 ? '0' + seconds : seconds;
		let tempDuration = `${hours}:${mins}:${seconds}`;

		return (
			<span>{tempDuration}</span>
		);
	}
}

export const CostCenterAutoMultiSelect = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			multiselect: this.props.restrictToSingleSelect ? false : true,
			isAutoSelect: true,
			labelKey: this.props.categoryFilter ? `costcenterid_name` : `displayname`,
			displayLabelKey :  this.props.categoryFilter ? `costcenterid_name` : `displayname`,
			valueKey: `jsonstring`,
			optionArray: [],
			searchInputValue : ''
		};

		this.checkSelectAsync();

		this.getOptions = this.getOptions.bind(this);
		this.checkSelectAsync = this.checkSelectAsync.bind(this);
		this.filterOption = this.filterOption.bind(this);
		this.onChange = this.onChange.bind(this);
		this.inputRenderer = this.inputRenderer.bind(this);
		this.getStateValue = this.getStateValue.bind(this);
		this.resetStateValue = this.resetStateValue.bind(this);
		this.filterOption = this.filterOption.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(this.props.resource != nextProps.resource || this.props.categoryFilter != nextProps.categoryFilter) {
			this.setState({
				isAutoSelect: false,
				labelKey: nextProps.categoryFilter ? `costcenterid_name` : `displayname`,
				displayLabelKey :  nextProps.categoryFilter ? `costcenterid_name` : `displayname`
			}, function() {
				this.checkSelectAsync();
			});
		}
		if(nextProps.value) {
			if(!this.state.value)
				this.getStateValue(this.state.multiselect ? nextProps.value : [nextProps.value]);
			else {
				let nextPropsValue = this.state.multiselect ? nextProps.value : [nextProps.value];
				let newValue = nextPropsValue.sort((a,b) => a.categoryid > b.categoryid ? 1 : -1).map(a => `${a.categoryid}_${a.costcenterid}`).join();
				let oldValue = this.state.value.map(a => a.json).sort((a,b) => a.categoryid > b.categoryid ? 1 : -1).map(a => `${a.categoryid}_${a.costcenterid}`).join();
				if(newValue != oldValue)
					this.getStateValue(nextPropsValue);
			}
		} else {
			if(this.state.value)
				this.resetStateValue();
		}
	}

	checkSelectAsync() {
		let url = `/api/query/costcenterdetailquery`;

		let filterObj = {
			restrictCategoryId: [],
			onlyIncludeCategoryId: [],
			showUncategorized: this.props.showUncategorized ? true : false
		};

		if(this.props.categoryFilter > 0)
			filterObj.onlyIncludeCategoryId.push(this.props.categoryFilter);

		axios.get(`${url}?filter=${encodeURIComponent(JSON.stringify(filterObj))}`).then((response) => {
			if(response.data.message == 'success') {
				if(response.data.main.length <= 200) {
					response.data.main.forEach(item => {
						item.jsonstring = `${item.json.categoryid}_${item.json.costcenterid}`;
					});
					this.setState({
						isAutoSelect: false,
						optionArray: response.data.main
					});
				} else {
					this.setState({
						isAutoSelect: true
					});
				}
				if(this.props.value)
					this.getStateValue(this.state.multiselect ? this.props.value : [this.props.value]);
			}
		});
	}

	getOptions(input, callback) {
		if((input == '<searchwo>' || input == '__') && !this.state.isAutoSelect)
			return callback([...this.state.optionArray]);

		if(input == '' || input == undefined || input == null || input == '__' )
			return callback([]);

		let filterObj = {
			restrictCategoryId: [],
			onlyIncludeCategoryId: [],
			searchinput: input,
			showUncategorized: this.props.showUncategorized ? true : false
		};

		if(this.props.value) {
			(this.state.multiselect ? this.props.value : []).forEach(item => {
				filterObj.restrictCategoryId.push(item.categoryid);
			});
		}

		if(this.props.categoryFilter > 0)
			filterObj.onlyIncludeCategoryId.push(this.props.categoryFilter);

		let url = `/api/query/costcenterdetailquery?filter=${encodeURIComponent(JSON.stringify(filterObj))}`;

		axios.get(url).then((response) => {
			if(response.data.message == 'success') {
				response.data.main.forEach(item => {
					item.jsonstring = `${item.json.categoryid}_${item.json.costcenterid}`;
				});
				callback(response.data.main);
			} else {
				callback([]);
			}
		});
	}

	onChange(value) {
		if(value) {
			if(this.state.multiselect) {
				this.props.onChange(value.map(item => item.json), value);
				this.state.value = value;
			} else {
				this.props.onChange(value.json, value);
				this.state.value = [value];
			}
			this.setState({
				value: this.state.value
			});
		} else {
			this.props.onChange(null, {});
			this.setState({
				value: null
			});
		}
	}

	getStateValue(selectedValue) {
		if(selectedValue && selectedValue.length === 0)
			return null;

		let filterObj = {
			value: selectedValue,
			showUncategorized: this.props.showUncategorized ? true : false
		};

		let url = `/api/query/costcenterdetailquery?filter=${encodeURIComponent(JSON.stringify(filterObj))}`;

		axios.get(url).then((response) => {
			if(response.data.message == 'success') {
				let nextPropsValue = this.props.value ? (this.state.multiselect ? this.props.value : [this.props.value]) : null;
				if(response.data.main.length > 0 && nextPropsValue && nextPropsValue.length > 0) {
					let newValue = nextPropsValue.sort((a,b) => a.categoryid > b.categoryid ? 1 : -1).map(a => `${a.categoryid}_${a.costcenterid}`).join();
					let oldValue = selectedValue.sort((a,b) => a.categoryid > b.categoryid ? 1 : -1).map(a => `${a.categoryid}_${a.costcenterid}`).join();
					let responseValue = response.data.main.map(a => a.json).sort((a, b) => a.categoryid > b.categoryid ? 1 : -1).map(a => `${a.categoryid}_${a.costcenterid}`).join();
					if(newValue == oldValue && newValue == responseValue) {
						this.setState({ value: response.data.main });
					}
				}
			}
		});
	}

	resetStateValue() {
		setTimeout(() => {
			this.setState({value : null});
		}, 0);
	}

	filterOption(option, filter, currentValues) {
		filter = filter == '<searchwo>' ? '' : filter;
		if(!this.state.multiselect) {
			if(filter == '__')
				return true;

			let first_string = typeof(option.label) == 'string' ? option.label.toLowerCase() : '';

			return new RegExp(`${filter.replace(/ /, '.*')}`, 'ig').test(first_string);
		}

		let categoryidObj = {};
		(this.props.value || []).forEach(item => {
			categoryidObj[item.categoryid] = {};
		});

		if(!this.props.restrictToSingleSelect && categoryidObj[option.data.json.categoryid])
			return false;

		let second_string = typeof(option.label) == 'string' ? option.label.toLowerCase() : '';

		return new RegExp(`${filter.replace(/ /, '.*')}`, 'ig').test(second_string);
	}

	inputRenderer(args) {
		return <InputRenderer {...args} custvalueObj={{value: this.state.value ? this.state.value[0] : null, displayLabelKey: this.state.displayLabelKey, disabled: this.props.disabled, valueOnChange: this.onChange, searchwo: true}} />
	}

	render() {
		let value = null;
		if(this.state.value) {
			value = this.state.value;

			value.forEach(val => {
				val.jsonstring = `${val.json.categoryid}_${val.json.costcenterid}`;
			});
		}

		let componentObj = {};

		if(!this.state.multiselect)
			componentObj.Input = this.inputRenderer;

		return (
			<ReactSelectAsync
				closeMenuOnSelect={true}
				loadOptions={_.debounce(this.getOptions, 200)}
				getOptionValue={(option)=> option[this.state.valueKey]}
				getOptionLabel={(option)=> option[this.state.displayLabelKey]}
				name={this.props.name}
				isMulti={this.state.multiselect}
				value={value}
				components={{
					...componentObj,
					LoadingIndicator: LoadingIndicator,
					Option: (props) => OptionRenderer(props, this.state.searchInputValue, this.state.labelKey),
				}}
				filterOption={this.filterOption}
				onChange={(value) => this.onChange(value)}
				classNamePrefix="gs-react-select-perfix"
				className={`gs-react-select ${this.props.className} ${this.props.disabled ? 'ndtclass' : ''} ${this.props.showedit ? 'gs-select-input-group' : ''}`}
				placeholder={this.props.placeholder}
				required={this.props.required}
				isClearable={this.props.required ? false : true}
				isDisabled={this.props.disabled}
				autoFocus={this.props.autoFocus}
				menuPortalTarget={document.body}
				menuPosition={'absolute'}
				menuPlacement={'auto'}
				styles={selectCustomStyles} />
		);
	}
});

export class TaskDueDateFilter extends Component {
	constructor(props) {
		super(props);

		this.daysCalculation = this.daysCalculation.bind(this);
	}

	daysCalculation (date1,date2){
		var timediff = new Date(date1).setHours(0,0,0,0) - new Date(date2).setHours(0,0,0,0);
		var daysdiff = timediff / (1000 * 3600 * 24);
		return daysdiff;
	}

	render() {

		if(new Date().setHours(0,0,0,0) > new Date(this.props.duedate).setHours(0,0,0,0))
			return <span className={`gs-bg-danger ${this.props.className}`} style={{borderRadius: '2px', padding: '1.5px 8px'}}>{`Overdue by ${this.daysCalculation(new Date(),new Date(this.props.duedate))} days`}</span>;
		if(new Date().setHours(0,0,0,0) < new Date(this.props.duedate).setHours(0,0,0,0))
			return <span className={`gs-bg-info ${this.props.className}`} style={{borderRadius: '2px', padding: '1.5px 8px'}}>{`Due in ${this.daysCalculation(new Date(this.props.duedate),new Date())} days`}</span>;
		if(new Date().setHours(0,0,0,0) == new Date(this.props.duedate).setHours(0,0,0,0))
			return <span className={`gs-bg-info ${this.props.className}`} style={{borderRadius: '2px', padding: '1.5px 8px'}}>{`Due Today`}</span>;
		return null;
	}
}


export class AudioPlayerEle extends Component {
	constructor(props) {
		super(props);
		this.state = {
			player: "stopped",
			currentTime: 0,
			duration: 0
		};
		this.getTime = this.getTime.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(this.props.playing != nextProps.playing) {
			if(nextProps.playing)
				this.player.play();
			else {
				this.setState({
					player: 'stopped'
				}, ()=> {
					this.player.pause();
					this.player.currentTime = 0;
				});
			}
		}
	}

	componentDidMount() {
		this.player.addEventListener("timeupdate", e => {
			this.setState({
				currentTime: e.target.currentTime,
				duration: e.target.duration
			}, () => {
				if(this.state.currentTime == this.state.duration) {
					this.props.callback();
				}
			});
		});

		this.audioplayer_progressbar.addEventListener('click', e => {
			this.player.pause();
			let percent = (e.offsetX / e.currentTarget.offsetWidth) * this.player.duration;
			this.player.currentTime = percent;
			this.setState({
				currentTime: percent
			}, () => {
				this.player.play();
			});
		});
	}

	componentWillUnmount() {
		this.player.removeEventListener("timeupdate", () => {});
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.state.player !== prevState.player) {
			if (this.state.player === "paused") {
				this.player.pause();
			} else if (this.state.player === "stopped") {
				this.player.pause();
				this.player.currentTime = 0;
			} else if (this.state.player === "playing" && prevState.player === "paused") {
				this.player.play();
			}
		}
	}

	getTime(time) {
		if (!isNaN(time)) {
			return Math.floor(time / 60) + ":" + ("0" + Math.floor(time % 60)).slice(-2);
		}
	}

	render() {
		const currentTime = this.state.currentTime && this.state.duration ? ((this.state.currentTime / this.state.duration) * 100) : 0;
		return (
			<div className="w-100 text-center">
				<progress className={`gs-audio-player ${this.props.playing ? '' : 'd-none'}`} ref={ref => this.audioplayer_progressbar = ref} value={currentTime} max={100}></progress>
				<audio src={this.props.src} ref={ref => this.player = ref} className="d-none" controls/>
				<div className={`${this.props.playing ? '' : 'd-none'}`}>
					<span className={`text-muted float-left font-12`}>{this.getTime(this.state.currentTime)}</span>
					<span className={`text-muted float-right font-12`}>{this.getTime(this.state.duration)}</span>
				</div>
			</div>
		);
	}
}
