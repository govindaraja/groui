import React, { Component } from 'react';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';

import { dateFilter, timeFilter } from '../utils/filter';

export default class CalendarToolbar extends Component {
	constructor(props) {
		super(props);

		this.navigate = this.navigate.bind(this);
		this.viewNamesGroup = this.viewNamesGroup.bind(this);
	}

	navigate = action => {
		this.props.onNavigate(action);
	}

	viewNamesGroup() {
		let viewNames = this.props.views;
		const view = this.props.view;

		if (viewNames.length > 1) {
			return viewNames.map(name => (
				<button type="button" key={name} className={`btn btn-sm gs-btn-light ${view === name ? 'active' : ''}`} onClick={()=>this.props.onView(name)}>
					{name == 'agenda' ? 'List' : this.props.localizer.messages[name]}
				</button>
			));
		}
	}

	render() {
        return (
			<div className='d-flex align-items-center justify-content-end mb-3'>
				<div className="d-flex flex-row align-items-center mr-4">
					<div className="rbc-toolbar-label mr-2">
						{['day', 'agenda'].includes(this.props.view) ? <span className="font-weight-bold font-16">{moment(this.props.date).format('dddd')}, </span> : null}
						{
							['day', 'agenda'].includes(this.props.view)
							? <span className="font-weight-bold font-16">{moment(this.props.date).format('DD MMMM YYYY')}</span>
							: <div className="font-weight-bold font-16">{moment(this.props.date).format('MMMM YYYY')}</div>
						}
					</div>
					<div className="">
						<button type="button" className="btn btn-sm gs-form-btn-success mr-2" onClick={() => this.navigate('PREV')}><span className="fa fa-caret-left"></span></button>
						<button type="button" className="btn btn-sm gs-form-btn-success" onClick={() => this.navigate('NEXT')}><span className="fa fa-caret-right"></span></button>
					</div>
				</div>
				<div className="mr-4">
					<div className="cursor-ptr" onClick={() => this.navigate('TODAY')}>Go to Today</div>
				</div>
				<div className="btn-group">
					{this.viewNamesGroup()}
				</div>
			</div>
        );
    }
}