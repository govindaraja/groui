import React, { Component } from 'react';
import { Draggable } from 'react-beautiful-dnd';

import Listviewjsonlist from './listviewjsonlist';

class Listviewlistcontainer extends Component {

	render() {
		let jsonfieldarray = this.props.jsonfieldarray;
		let onClick = this.props.onClick ? this.props.onClick : null;

		if(this.props.searchterm) {
			jsonfieldarray = this.props.jsonfieldarray.filter((item) =>
				item.displayName.toLowerCase().search(this.props.searchterm.toLowerCase()) !== -1
			);
		}

		var jsonlistcontainer = jsonfieldarray.map((jsonfield, index)=> {
			return (
				<Draggable draggableId={`${jsonfield.field}-${jsonfield.jsonname}`} index={index} key={`${jsonfield.field}-${jsonfield.jsonname}`}>
					{(draggableProvided, draggableSnapshot) => (
						<Listviewjsonlist key={index} index={index} field={jsonfield} resourcename={this.props.resourcename} displayNameObj={this.props.displayNameObj} rootapp={this.props.rootapp} isreadable={this.props.isreadable} onClick={onClick} provided={draggableProvided} snapshot={draggableSnapshot}/>
					)}
				</Draggable>
			);
		});

		return jsonlistcontainer;
	}
}

export default Listviewlistcontainer;
