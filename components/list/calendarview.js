import React, { Component } from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import axios from 'axios';
import 'react-big-calendar/lib/css/react-big-calendar.css';

const localizer = momentLocalizer(moment);

export default class Calendarview extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activitycalendar: {},
			events: [],
			eventSources: []
		};
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getCalendarData = this.getCalendarData.bind(this);
		this.getFields = this.getFields.bind(this);
		this.getTitle = this.getTitle.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
	}

	componentWillMount() {
		this.getCalendarData();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	getFields() {
		var returnString = '';
		if(typeof(this.props.calendarlist.calendarOptions.title) == 'string') {
			return this.props.calendarlist.calendarOptions.title;
		} else {
			for(var i=0;i<this.props.calendarlist.calendarOptions.title.length;i++) {
				var splitedFields = this.props.calendarlist.calendarOptions.title[i].split('/');
				if(splitedFields.length == 1) {
					returnString += splitedFields[0]+",";
				} else {
					if(this.props.rootapp.app.myResources[this.props.calendarlist.resourcename].fields[splitedFields[0]] && this.props.rootapp.app.myResources[this.props.calendarlist.resourcename].fields[splitedFields[0]].isForeignKey)
					{
						let joinResourceName = this.props.rootapp.app.myResources[this.props.calendarlist.resourcename].fields[splitedFields[0]].foreignKeyOptions.resource;
						returnString += joinResourceName+"/"+splitedFields[1]+"/"+splitedFields[0]+","
					}
				}
			}
			returnString = returnString.substr(0,returnString.length-1);
			return returnString;
		}
	}

	getTitle(Obj) {
		var returnString = ' ';
		if(typeof(this.props.calendarlist.calendarOptions.title) == 'string') {
			returnString += Obj[this.props.calendarlist.calendarOptions.title];
			return returnString;
		} else {
			for(var l=0;l<this.props.calendarlist.calendarOptions.title.length;l++) {
				returnString += (returnString == ' ') ? returnString + Obj[this.props.calendarlist.calendarOptions.title[l].replace('/','_')] : " -- " +Obj[this.props.calendarlist.calendarOptions.title[l].replace('/','_')]; 
			}
			return returnString;
		}
	}
	
	getCalendarData() {
		this.updateLoaderFlag(true);
		let events = [];
		let year = new Date().getFullYear();
		let month = new Date().getMonth();
		let date = new Date().getDate();

		var filterCondition = this.props.calendarlist.calendarOptions.filter;
		filterCondition = filterCondition.replace('<currentuser>', this.props.rootapp.app.user.id);
		if (this.state.salesperson > 0 && ['leads','quotes'].indexOf(this.props.calendarlist.resourcename) >= 0) {
			filterCondition = filterCondition != '' ? (filterCondition + ' and ' + this.props.calendarlist.resourcename + '.salesperson=' + this.state.salesperson) : (this.props.calendarlist.resourcename + '.salesperson=' + this.state.salesperson);
		}

		var queryString = "/api/"+this.props.calendarlist.resourcename+"?&field=id,"+this.getFields()+","+this.props.calendarlist.calendarOptions.date+"&filtercondition=" + filterCondition;

		axios({
			method: 'get',
			url: queryString
		}).then((response)=> {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.main.length; i++) {
					var nextFollowupdate = new Date(response.data.main[i][this.props.calendarlist.calendarOptions.date]);
					events.push({
						id : response.data.main[i].id,
						title : this.getTitle(response.data.main[i]),
						start : nextFollowupdate,
						end : nextFollowupdate,
						className : 'label-green',
						allDay : false
					});
				}
				this.setState({events: events});
			}
			this.updateLoaderFlag(false);
		});
	}

	openTransaction(event) {
		let tempRoute = this.props.calendarlist.tableonclick.href.replace('<id>', event.id);
		this.props.rootapp.history.push(tempRoute);
	}

	render() {
		return (
			<div className="col-md-12 bg-white padding-10" style={{height: '500px'}}>
				<Calendar localizer={localizer} selectable events={this.state.events} views={["month", "week", "day"]} defaultView="day" showMultiDayTimes defaultDate={new Date()} onSelectEvent={event => this.openTransaction(event)} />
			</div>
		);
	}
}
