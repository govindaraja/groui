import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';

import Kanbancard from './kanbancard';

const cardDropSpec = {
	drop(props, monitor) {
		const item = monitor.getItem();
		props.callback.onDrop(item.stage, props.item);
	}
}

class KanbanList extends Component {
	render() {
		const { canDrop, isOver, connectDropTarget } = this.props;
		const isActive = canDrop && isOver;

		var kanbanstage_card = this.props.stages.map((stageval, index)=> {
			return (
				<Kanbancard key={index} id={stageval.id} currentstage={this.props.item} currentstageitem={stageval} stages={this.props.stages} callback={this.props.callback} json={this.props.json} />
			);
		});

		return connectDropTarget(
			<td style={{ borderRight: '1px solid #A7A4A4',backgroundColor: isActive ? '#e8e8e8' : ''}}>
				<div className="gs-scrollbar" style={{overflowY: 'scroll', height: `${$(window).outerHeight() - $('.list-title-bar').outerHeight() - $('.navbar').outerHeight() - $('.kanban-filter').outerHeight() - $('.kanbantable > thead').outerHeight() - ($('.kanban-filter').outerHeight() > 0 ? 85 : 55)}px`}}>
					<ul className="drag">
						{kanbanstage_card}
					</ul>
				</div>			
			</td>
		);
	}
}

export default DropTarget('kanbancard', cardDropSpec, (connect, monitor) => ({
	connectDropTarget: connect.dropTarget(),
	isOver: monitor.isOver(),
	canDrop: monitor.canDrop()
}))(KanbanList);
