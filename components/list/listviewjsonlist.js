import React, { Component } from 'react';

import { resourceFieldNameFilter } from '../../utils/filter';

class Listviewjsonlist extends Component {
	render() {
		const { field, snapshot, provided, displayNameObj } = this.props;

		let onClick = this.props.onClick ? () => this.props.onClick(field) : null;

		return (
			<div className={`listview-liststyle-column ${snapshot.isDragging ? 'dragndrop_shadow' : ''}`}  ref={provided.innerRef} isdragging={snapshot.isDragging.toString()} {...provided.draggableProps} {...provided.dragHandleProps} onClick={onClick}>
				{this.props.resourcename && this.props.resourcename != 'reports' ? 
				resourceFieldNameFilter(field.field, this.props.resourcename, this.props.rootapp) :  ((this.props.resourcename && this.props.resourcename == 'reports' && displayNameObj ? displayNameObj[field.field] : (this.props.resourcename && this.props.resourcename == 'reports' && !displayNameObj ? resourceFieldNameFilter(field.field, this.props.resourcename, this.props.rootapp) : field.displayName)) : field.displayName)}
			</div>
		);
	}
}

export default Listviewjsonlist;
