import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { v4 as uuid } from 'uuid';
import InfiniteScroll from 'react-infinite-scroll-component';

import KanbanList from './kanbanlist';
import KanbanActivityModal from '../details/kanbanactivitymodal';
import { SelectAsync, TaskDueDateFilter } from '../utilcomponents';
import {currencyFilter, dateFilter, datetimeFilter} from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			salesperson: this.props.list.filter.kanbansalesperson.length > 0 ? this.props.list.filter.kanbansalesperson : [],
			finalstages:[]
		};
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getLeadDetails = this.getLeadDetails.bind(this);
		this.openStage = this.openStage.bind(this);
		this.openActivity = this.openActivity.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		if(this.props.app.kanbanObj) {
			this.setState({
				salesperson: this.props.app.kanbanObj.salesperson,
				finalstages: this.props.app.kanbanObj.finalstages
			});
			setTimeout(() => {
				this.state.finalstages.forEach(stageitem => {
					this.getLeadDetails(stageitem);
				});
			}, 0);
		} else {
			this.onLoad();
		}
	}

	componentWillReceiveProps(nextProps) {
		if(this.state.salesperson != nextProps.list.filter.kanbansalesperson) {
			this.setState({ salesperson : nextProps.list.filter.kanbansalesperson });
			setTimeout(() => {
				this.state.finalstages.forEach(stageitem => {
					this.getLeadDetails(stageitem);
				});
			}, 0);
		}
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let finalstages = [];
		var queryString = `/api/${this.props.json}?field=id,name,probability,index`;
		axios.get(queryString)
		.then((response) => {
			if (response.data.message == 'success') {

				finalstages.push({
					name: 'No Stage',
					stageid: null,
					index: 0,
					probability: 100,
					skip: 0,
					totalcount: 0,
					potentialrevenue: 0,
					items:[]
				});

				response.data.main.forEach(stageitem => {
					finalstages.push({
						name: stageitem.name,
						stageid: stageitem.id,
						index: stageitem.index,
						probability: stageitem.probability,
						skip: 0,
						totalcount: 0,
						potentialrevenue: 0,
						items:[]
					});
				});

				finalstages.sort((a, b) =>
					a.index < b.index ? -1 : a.index > b.index ? 1 : 0
				);

				this.setState({ finalstages });
				finalstages.forEach(stageitem => {
					this.getLeadDetails(stageitem);
				});
			}
			this.updateLoaderFlag(false);
		});
	}

	getLeadDetails(tempstageObj) {
		this.updateLoaderFlag(true);
		tempstageObj.loader = true;
		let { finalstages } = this.state;

		let queryString = '';
		if(this.props.json == 'leadstages')
			queryString = `/api/leads?field=id,subject,customerid,partners/name/customerid,status,potentialrevenue,contactid,contacts/name/contactid,currencyid,currencyexchangerate,nextfollowup,followuptypeid,activitytypes/name/followuptypeid,activitytypes/icontext/followuptypeid,stageid,modified,priority&skip=${tempstageObj.skip * 10}&pagelength=10&filtercondition=leads.status not in ('Won','Lost','Cancelled') ${this.state.salesperson.length > 0 ? ` and leads.salesperson IN (${this.state.salesperson.join()})` : ""} and ${tempstageObj.stageid > 0 ? `leads.stageid=${tempstageObj.stageid}` : 'leads.stageid is null'}`;
		if(this.props.json == 'projectstages')
			queryString = `/api/projects?field=id,projectname,customerid,partners/name/customerid,status,startdate,enddate,stageid,salesperson,users/displayname/salesperson,modified&skip=${tempstageObj.skip * 10}&pagelength=10&filtercondition=projects.status='Approved' and ${tempstageObj.stageid > 0 ? `projects.stageid=${tempstageObj.stageid}` : 'projects.stageid is null'}`;

		axios.get(queryString).then((response) => {
			tempstageObj.loader = false;
			if (response.data.message == 'success') {
				let allLeadsidArr = [];
				tempstageObj.totalcount = response.data.count;

				if(tempstageObj.items.length > 0) {
					tempstageObj.items.forEach(leaditem => {
						allLeadsidArr.push(leaditem.id);
					});
				}

				response.data.main.forEach(data => {
					if(allLeadsidArr.length > 0 && !allLeadsidArr.includes(data.id)) {
						tempstageObj.potentialrevenue += data.potentialrevenue;
						tempstageObj.items.push(data);
						return;
					}
					if(allLeadsidArr.length > 0 && allLeadsidArr.includes(data.id)) {
						tempstageObj.potentialrevenue = 0;
						tempstageObj.items.forEach((leaditem, leaditemindex) => {
							tempstageObj.potentialrevenue += leaditem.potentialrevenue;
							if(leaditem.id == data.id) {
								tempstageObj.items[leaditemindex] = data;
							}
						});
						return;
					}
					tempstageObj.potentialrevenue += data.potentialrevenue;
					tempstageObj.items.push(data);
				});

				this.setState({ finalstages: this.state.finalstages });
				this.props.updateAppState('kanbanObj', this.state);
			}
			this.updateLoaderFlag(false);
		});
	}

	onDragEnd = (result, columns) => {

		if (!result.destination) return;

		const { source, destination } = result;
		let { finalstages } = this.state;

		if (source.droppableId !== destination.droppableId) {
			let data = {...finalstages[source.droppableId].items[source.index]};
			data.stageid = finalstages[destination.droppableId].stageid ? finalstages[destination.droppableId].stageid : null;
			finalstages[source.droppableId].items.splice(source.index, 1);
			finalstages[destination.droppableId].items.splice(destination.index, 0, data);

			let tempData = {
				actionverb : this.props.json == 'leadstages' ? 'Save' : 'Update',
				data : data
			};

			axios({
				method: 'post',
				data: tempData,
				url: `/api/${this.props.json == 'leadstages' ? 'leads' : 'projects'}`
			}).then((response)=> {
				if (response.data.message === 'success') {

					finalstages[destination.droppableId].items[destination.index].modified = response.data.main.modified;

					finalstages.forEach(stage => {
						stage.potentialrevenue = 0;
						stage.totalcount = stage.items.length;
						stage.items.forEach(item=> {
							stage.potentialrevenue += item.potentialrevenue;
						});
					});
				} else {
					let tempdata = {...finalstages[destination.droppableId].items[destination.index]};
					tempdata.stageid = finalstages[source.droppableId].stageid;
					finalstages[destination.droppableId].items.splice(destination.index, 1);
					finalstages[source.droppableId].items.splice(source.index, 0, tempdata);
				}
				this.setState({ finalstages });
				this.props.updateAppState('kanbanObj', this.state);
			});
		} else {
			finalstages[destination.droppableId].items.splice(destination.index, 0, finalstages[source.droppableId].items.splice(source.index, 1)[0]);
			this.setState({ finalstages });
			this.props.updateAppState('kanbanObj', this.state);
		}
	};

	openStage(id) {
		if(this.props.json == 'leadstages')
			this.props.history.push(`/details/leads/${id}`);
		else if(this.props.json == 'projectstages')
			this.props.history.push(`/details/projects/${id}`);
	}
	
	openActivity(param, column) {
		this.props.openModal({
			render: (closeModal) => {
				return <KanbanActivityModal param={param} openModal={this.props.openModal} createOrEdit={this.props.createOrEdit} closeModal={closeModal} callback={() => {
					this.getLeadDetails(column);
				}} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}
	
	render() {
		const { finalstages } = this.state;
		if(!finalstages || finalstages.length == 0)
			return null;

		let columnHeight = $(window).outerHeight() - $('.navbar').outerHeight() - $('.list-title-bar').outerHeight() - $('.kanban_header').outerHeight() - 30;

		return(
			<div className="d-flex" style={{backgroundColor: '#F1F3F5', overflowX: 'auto'}}>
				<DragDropContext onDragEnd={this.onDragEnd}>
					{
						finalstages.map((column, index) => {
							 return (
							 	<div className="d-flex flex-column" key={index}>
								 	<div className="kanban_header" style={{
										width:'270px',
										padding:'9px 14px',
										borderTop:'1px solid rgba(201, 201, 201, 0.5)',
										borderBottom:'1px solid rgba(201, 201, 201, 0.5)',
										borderLeft:'1px solid rgba(201, 201, 201, 0.5)',
										borderRight:this.state.finalstages.length - 1 == index ? '1px solid rgba(201, 201, 201, 0.5)' : 'none'}}>

										<div style={{fontWeight:'700'}}>
											<span>{column.name.toString().toUpperCase()}</span>
											{column.loader ? <span className="ml-2"><span className="fa fa-spinner fa-spin fa-fw"></span></span> : null}
										</div>

										<div className="d-flex justify-content-between">
											<span>{currencyFilter(column.potentialrevenue, column.currencyid, this.props.app)}</span>
											<span className="d-flex align-items-center justify-content-center" style={{backgroundColor: 'rgba(74, 74, 74, 0.1)',borderRadius:'11.5px',width:'28px',height:'23px'}}>{column.totalcount}</span>
										</div>
										{this.props.json == 'leadstages' ? <div className="text-muted font-10">
											<span className="marginright-8">Total Value :  {currencyFilter(((column.potentialrevenue * column.probability)/100), column.currencyid, this.props.app)}</span>
											<span>({column.probability} %)</span>
										</div> : null}

									</div>
								 	<div key={index} style={{width: '270px',borderLeft: '1px solid rgba(201, 201, 201, 0.5)',borderBottom:  '1px solid rgba(201, 201, 201, 0.5)'}}>
										<Droppable key={index} droppableId={index.toString()} >
											{
												(provided, snapshot) => {
													return (
														<div className={column.items.length == 0 ? "d-flex flex-column align-items-center justify-content-center" : ''} {...provided.droppableProps} ref={provided.innerRef} style={{padding: '8px 11px', height: `${columnHeight}px`, maxHeight: `${columnHeight}px`, overflowY: 'auto'}} id={`scrollableDiv_${index}`} >
															{!column.loader && column.items.length == 0 ? <div>No leads in this stage</div> : null}
															<InfiniteScroll
																dataLength={column.items.length}
																next={() => {
																	column.skip = column.skip+1;
																	this.getLeadDetails(column);
																}}
																hasMore={column.totalcount > column.items.length}
																loader={<div>Loading...</div>}
																scrollableTarget={`scrollableDiv_${index}`}
															>
																{
																	column.items.map((item, index) => {
																		return (
																			<Draggable key={uuid()}
																			draggableId={item.id.toString()}
																			index={index}>
																				{
																					(provided, snapshot) => {
																						return (
																							<div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps} 
																							style={{
																								boxShadow:'0px 1px 4px 0 #BABABA',
																								userSelect: "none",
																								borderRadius:'4px',
																								minHeight: "114px",
																								maxHeight: "114px",
																								marginBottom: '12px',
																								backgroundColor: snapshot.isDragging ? "rgba(201, 201, 201, 0.5)": "#fff",
																								padding: '7px 12px',
																								...provided.draggableProps.style
																							}}>
																								<div onClick={()=>this.openStage(item.id)} className="font-14 semi-bold mb-1" style={{cursor:'pointer'}}>{item.subject}</div>

																								<div className="mb-1">{item.customerid_name}{!item.customerid_name ? <span>{item.contactid_name}</span> : null}</div>

																								<div className="mb-1">{currencyFilter(item.potentialrevenue, item.currencyid, this.props.app)}</div>
																								<div className="d-flex justify-content-between">
																									{item.nextfollowup && new Date() <= new Date(item.nextfollowup) ? <span style={{cursor:'pointer', backgroundColor: 'rgba(36, 158, 120, .1)', padding: '3px 8px', fontSize: '12px'}} onClick={()=>this.openActivity(item, column)}>{datetimeFilter(item.nextfollowup)}</span> : null}
																									{item.nextfollowup && new Date() > new Date(item.nextfollowup) ? <span style={{cursor:'pointer'}} onClick={()=>this.openActivity(item, column)}><TaskDueDateFilter className="font-12" duedate={item.nextfollowup} /></span> : null}
																									{!item.nextfollowup ? <span onClick={()=>this.openActivity(item, column)} style={{ cursor:'pointer', backgroundColor:'rgba(246, 152, 10, .1)', borderRadius: '2px', fontSize: '12px', textAlign: 'center', padding: '1.5px 5px', justifyContent: 'center'}}>No Planned Activities</span> : null}
																									<span style={{ background: item.priority ?'rgba(80, 100, 121, .1)':'none', padding:'1.5px 8px', justifyContent:'center', textAlign:'center', fontSize:'12px'}}>{item.priority}</span>
																								</div>
																							</div>
																						)
																					}
																				}
																			</Draggable>
																		)
																	})
																}
																{provided.placeholder}
															</InfiniteScroll>
														</div>
													)
												}
											}
										</Droppable>
									</div>
								</div>
							)
						})
					}
				</DragDropContext>
			</div>
		)
	}
}