import React, { Component } from 'react';
import { DragSource, DropTarget } from 'react-dnd';

import {currencyFilter, dateFilter, datetimeFilter} from '../../utils/filter';

const kanbancardDragSpec = {
	beginDrag(props) {
        	return {
			id: props.currentstageitem.id,
			stage: props.currentstageitem,
		};
	},

	endDrag(props, monitor) {
		const item = monitor.getItem();
		//if(item.id != props.id) {
			props.callback.dropSuccessHandler(props.currentstage, item.stage);
		//}
	}
}

const kanbancardDropSpec = {
	drop(props, monitor) {
		const item = monitor.getItem();
		if(item.id != props.id) {
			props.callback.onDrop(item.stage, props.currentstage);
		}
	}
}

class Kanbancard extends Component {
	render() {
		const cardsprops = this.props;
		const { isDragging, connectDragSource, connectDropTarget, connectDragPreview } = this.props;

		return connectDropTarget(connectDragPreview(connectDragSource(
			<li style={{ opacity: isDragging ? 0.5 : 1 }}>
				<div className="block">
					<a className="stagedetails">
						<span className="anchorclass" onClick={()=>{cardsprops.callback.openStage(cardsprops.currentstageitem.id)}}>{cardsprops.json == 'leadstages' ? cardsprops.currentstageitem.subject : cardsprops.currentstageitem.projectname}</span>
						{cardsprops.currentstageitem.customerid ? 
							<div className="anchorclass help-text" onClick={()=>{cardsprops.callback.openStage(cardsprops.id)}}>
								<span className="fa fa-building text-info1 marginright-8"></span>{cardsprops.currentstageitem.customerid_name}
							</div> 
						: null}
						{cardsprops.currentstageitem.contactid_name ? 
							<div className="anchorclass help-text" onClick={()=>{cardsprops.callback.openStage(cardsprops.id)}}>
								<span className="fa fa-user text-info1 marginright-8"></span>{cardsprops.currentstageitem.contactid_name}
							</div> 
						: null}
						{cardsprops.json == 'leadstages' ? <div>
							<span className="help-text">{cardsprops.currentstageitem.potentialrevenue}</span>
							{cardsprops.currentstageitem.priority ? <span className="help-text"> | {cardsprops.currentstageitem.priority}</span> : null}
						</div> : null}
						{cardsprops.json == 'leadstages' && cardsprops.currentstageitem.nextfollowup ? 
							<div className="anchorclass" onClick={()=>{cardsprops.callback.openStage(cardsprops.id)}}>
								<span className={`fa fa-${cardsprops.currentstageitem.followuptypeid_icontext}`}></span>
								<span className="help-text">{dateFilter(cardsprops.currentstageitem.nextfollowup)}</span>
							</div> 
						: null}
						{cardsprops.json == 'leadstages' && !cardsprops.currentstageitem.nextfollowup ? <div><span className="anchorclass help-text" onClick={()=>{cardsprops.callback.openStage(cardsprops.id)}}><span className="fa fa-exclamation-triangle text-warning1"></span> No Activity</span></div> : null }

						{cardsprops.json == 'projectstages' && cardsprops.currentstageitem.salesperson ? <div className="anchorclass help-text" onClick={()=>{cardsprops.callback.openStage(cardsprops.id)}}>
							<span className="fa fa-user text-info1 marginright-8"></span>{cardsprops.currentstageitem.salesperson_displayname}
						</div>  : null}

						{cardsprops.json == 'projectstages' && cardsprops.currentstageitem.startdate ? <div className="help-text">
							<span>Start Date : </span>
							<span>{dateFilter(cardsprops.currentstageitem.startdate)}</span>
							<br></br>
							<span>End Date : </span>
							<span>{dateFilter(cardsprops.currentstageitem.enddate)}</span>
						</div>  : null}
					</a>
				</div>
			</li>
		)));
	}
}

const kanbanDragCard = DragSource('kanbancard', kanbancardDragSpec, (connect, monitor) => ({
	connectDragSource: connect.dragSource(),
	connectDragPreview: connect.dragPreview(),
	isDragging: monitor.isDragging()
}))(Kanbancard);

const kanbanDragDropCard = DropTarget('kanbancard', kanbancardDropSpec, (connect, monitor) => ({
	connectDropTarget: connect.dropTarget()
}))(kanbanDragCard);

export default kanbanDragDropCard;
