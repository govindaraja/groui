import React, { Component } from 'react';
import axios from 'axios';
import { Treebeard, decorators } from 'react-treebeard';
import { currencyFilter } from '../../utils/filter';

const treeviewStyles = {
	tree: {
		base: {
			listStyle: 'none',
			backgroundColor: '#fff',
			margin: 0,
			padding: 0,
			color: '#333'
		},
		node: {
			base: {
				position: 'relative'
			},
			link: {
				cursor: 'pointer',
				position: 'relative',
				padding: '0px 5px',
				display: 'block'
			},
			activeLink: {
				background: '#fff'
			},
			toggle: {
				base: {
					position: 'relative',
					display: 'inline-block',
					verticalAlign: 'top',
					marginLeft: '-5px',
					height: '24px',
					width: '24px'
				},
				wrapper: {
					position: 'absolute',
					top: '50%',
					left: '50%',
					margin: '-7px 0 0 -7px',
					height: '14px'
				},
				height: 14,
				width: 14,
				arrow: {
					fill: '#333',
					strokeWidth: 0
				}
			},
			header: {
				base: {
					display: 'inline-block',
					verticalAlign: 'top',
					color: '#333'
				},
				connector: {
					width: '2px',
					height: '12px',
					borderLeft: 'solid 2px black',
					borderBottom: 'solid 2px black',
					position: 'absolute',
					top: '0px',
					left: '-21px'
				},
				title: {
					lineHeight: '24px',
					verticalAlign: 'middle'
				}
			},
			subtree: {
				listStyle: 'none',
				paddingLeft: '19px'
			},
			loading: {
				color: '#E2C089'
			}
		}
	}
};

decorators.Header = ({style, node}) => {
	const iconType = node.children && node.children.length > 0 ? (node.toggled ? 'folder-open-o' : 'folder-o') : 'file-text-o';
	const iconClass = `fa fa-${iconType}`;
	const iconStyle = {marginRight: '5px'};

	return (
		<div style={style.base}>
			<div style={style.title} onMouseOver={() => node.setHover(node)}>
				<i className={iconClass} style={iconStyle}/>
				<span className={`${!node.isActive? 'linethrough' : ''}`}>
					{node.name}
					{(!node.parent && node.resourcename == 'accounts') ? <span className='marginleft-10 marginright-10 text-primary'>{node.balance}</span>: null}
					{node.resourcename == 'uom' ? <span className='marginleft-10 marginright-10 fa fa-exchange'> {node.conversionrate}</span>: null}
				</span>
				<a style={{marginLeft: '5px', color: 'white', fontSize: '10px', borderRadius: '4px', padding: '2px', fontWeight: 'bold'}} className={`btn-sm btn-primary ${node.hover ? 'show' : 'hide'}`} data-toggle="tooltip" title="Click to Open" onClick={()=>node.openDetails(node)}>Edit</a>
			</div>
		</div>
	);
};

export default class Treeview extends Component {
	constructor(props) {
		super(props);
		this.state = {
			treedata: []
		};
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getTreeData = this.getTreeData.bind(this);
		this.onToggletreeview = this.onToggletreeview.bind(this);
		this.openDetails = this.openDetails.bind(this);
		this.setHover = this.setHover.bind(this);
	}

	componentWillMount() {
		this.getTreeData();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	openDetails(item){
		this.props.history.push(`/details/${this.props.pagejson.resourcename}/${item.id}`);
	}

	setHover(item) {
		let { treedata } = this.state;

		for(let i = 0; i < treedata.length; i++) {
			if(treedata[i].children)
				checkItem(treedata[i].children);

			if(item.id == treedata[i].id)
				treedata[i].hover = true;
			else
				treedata[i].hover = false;
		}

		function checkItem(child) {
			for(let i = 0; i < child.length; i++) {
				if(child[i].children)
					checkItem(child[i].children);

				if(item.id == child[i].id)
					child[i].hover = true;
				else
					child[i].hover = false;
			}
		}

		this.setState({ treedata });
	}

	getTreeData() {
		this.updateLoaderFlag(true);

		let labelField = this.props.pagejson.treeViewOptions.labelField;
		let labelFieldStr = this.props.pagejson.resourcename == 'uom' ? (this.props.pagejson.treeViewOptions.labelField + ',conversionrate') : this.props.pagejson.treeViewOptions.labelField;
		let parentidField = this.props.pagejson.treeViewOptions.parentidField;
		let isparentField = this.props.pagejson.treeViewOptions.isparentField;
		let notisparentField = this.props.pagejson.treeViewOptions.notisparentField;
		let queryString = "/api/" + this.props.pagejson.resourcename + "?field=id," + labelFieldStr + "," + parentidField + "," + isparentField +"," + notisparentField +",isactive"
		if(this.props.pagejson.includeinactive)
			queryString += "&includeinactive=true";
		if(this.props.pagejson.resourcename == 'stocklocations' && !this.props.rootapp.feature.useSubLocations)
			queryString += "&filtercondition=stocklocations.parentid is null";
		if(this.props.pagejson.listAPIURL)
			queryString = this.props.pagejson.listAPIURL;
		let nodeArray = [];

		axios.get(queryString)
		.then((res) => {
			let treedata = [];
			let listdata = res.data.main;

			let treeview = listdata.sort((a, b) => {
				return (a[this.props.pagejson.treeViewOptions.labelField] < b[this.props.pagejson.treeViewOptions.labelField]) ? -1 : (a[this.props.pagejson.treeViewOptions.labelField] > b[this.props.pagejson.treeViewOptions.labelField]) ? 1 : 0;
			});

			for (var i = 0; i < treeview.length; i++) {
				nodeArray.push({
					id : treeview[i].id,
					name : treeview[i][this.props.pagejson.treeViewOptions.labelField],
					parentAccountId : treeview[i][this.props.pagejson.treeViewOptions.parentidField],
					parent : isparentField ? treeview[i][this.props.pagejson.treeViewOptions.isparentField] : !treeview[i][this.props.pagejson.treeViewOptions.notisparentField],
					active : treeview[i].isactive,
					isActive : treeview[i].isactive,
					balance : this.props.pagejson.resourcename == 'accounts' ? `${currencyFilter(Math.abs(treeview[i].balance), null, this.props.rootapp)} ${(treeview[i].balance >= 0) ? 'CR' : 'DR'}` : null,
					type : this.props.pagejson.resourcename == 'accounts' ? treeview[i].type : null,
					conversionrate : this.props.pagejson.resourcename == 'uom' ? treeview[i].conversionrate : null,
					children : [],
					toggled : true,
					setHover : this.setHover,
					openDetails : this.openDetails,
					resourcename : this.props.pagejson.resourcename
				});
			}

			var indexed_nodes = {};
			for (var i = 0; i < nodeArray.length; i++) {
				indexed_nodes[nodeArray[i].id] = nodeArray[i];
			}
			for (var i = 0; i < nodeArray.length; i++) {
				var parent_id = nodeArray[i].parentAccountId;
				if (nodeArray[i].parentAccountId === null) {
					treedata.push(nodeArray[i]);
				} else {
					indexed_nodes[parent_id].children.push(nodeArray[i]);
				}
			}

			for(var i=0;i<treedata.length;i++) {
				if(treedata[i].children.length == 0)
					delete treedata[i].children;
				else {
					recursivefn(treedata[i].children);
				}
			}
			function recursivefn(childArr) {
				for(var i=0;i<childArr.length;i++) {
					if(childArr[i].children.length == 0)
						delete childArr[i].children;
					else {
						recursivefn(childArr[i].children);
					}
				}
			}
			this.setState({treedata: treedata});
			this.updateLoaderFlag(false);
		});
	}

	onToggletreeview(node, toggled) {
		if(this.state.cursor) {
			this.state.cursor.active = false;
		}
		node.active = true;
		if(node.children) {
			node.toggled = toggled;
		}
		this.setState({ cursor: node });
	}

	render() {
		if(!this.state.treedata)
			return null;

		return (
			<div className="col-md-12">
				<Treebeard data={this.state.treedata} onToggle={this.onToggletreeview} decorators={decorators} style={treeviewStyles}/>
			</div>
		);
	}
}
