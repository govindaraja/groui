import React, { Component } from 'react';
import { connect } from 'react-redux';

import { currencyFilter, dateFilter, taxFilter } from '../utils/filter';
import { rptbuilderSorting } from '../utils/utils';

class GSPivotTableUI extends Component {

	constructor(props) {
		super(props);

		let uniqueObj = this.updateUniqueObj();
		this.state = {
			uniquerows: uniqueObj.uniquerows,
			uniquecols: uniqueObj.uniquecols,
			uniquerowcol: uniqueObj.uniquerowcol,
			colObjArray: uniqueObj.colObjArray,
			rowObjArray: uniqueObj.rowObjArray,
			trArray: uniqueObj.trArray,
			thArray: uniqueObj.thArray
		};

		this.renderFirstTR = this.renderFirstTR.bind(this);
		this.renderSecondTR = this.renderSecondTR.bind(this);
		this.renderInnerTable = this.renderInnerTable.bind(this);
		this.renderInnerTHR = this.renderInnerTHR.bind(this);
		this.renderInnerTBody = this.renderInnerTBody.bind(this);
		this.updateUniqueObj = this.updateUniqueObj.bind(this);
	}

	updateUniqueObj() {
		let uniquerows = {}, uniquecols = {}, uniquerowcol = {};

		let getValue = (datitem, prop) => {
			if(datitem[prop])
				return datitem[prop];
			else
				return '--Empty--';
		}

		let checkAndUpdate = (datitem, cols, colindex, obj, uniquearr) => {
			if(colindex == cols.length)
				return null;

			let colvalue = getValue(datitem, cols[colindex]);

			if(!obj[colvalue])
				obj[colvalue] = {};

			uniquearr.push(colvalue);

			checkAndUpdate(datitem, cols, colindex + 1, obj[colvalue], uniquearr);
		}

		let valtypeArr = this.props._vals.map((val, index) => {
			let columninfoobj = this.props.columnsinfo[val.substr(0, val.lastIndexOf('_'))];
			return this.props._vals[index] == 'count' || columninfoobj.type == 'integer' ? 'integer' : 'date'
		});
		let integerValTypeArr = valtypeArr.filter(a => a == 'integer');

		this.props.data.forEach((datitem) => {
			let tempDataUniqueArray = [];
			checkAndUpdate(datitem, this.props.cols, 0, uniquecols, tempDataUniqueArray);
			checkAndUpdate(datitem, this.props.rows, 0, uniquerows, tempDataUniqueArray);

			let uniqueStr = tempDataUniqueArray.join('<_>');
			uniquerowcol[uniqueStr] = {};

			this.props.vals.forEach(val => {
				uniquerowcol[uniqueStr][val] = datitem[val];
			});
		});

		let colObjArray = this.props.cols.map(col => []);
		let rowObjArray = this.props.rows.map(row => []);
		let getChildLength = (obj, secObj, colindex, colarr) => {
			if(colindex == colarr.length - 2)
				secObj.startlength += Object.keys(obj).length;

			for(var prop in obj) {
				getChildLength(obj[prop], secObj, colindex + 1, colarr);
			}
		};
		let getNextCol = (arr, colarr, colindex, obj, paramstr, colObjProp) => {
			if(colindex == colarr.length)
				return null;

			let columnname = colarr[colindex].lastIndexOf('_dn') == colarr[colindex].length - 3 ? colarr[colindex].substr(0, colarr[colindex].length - 3) : colarr[colindex];
			let insideArray = rptbuilderSorting(Object.keys(obj), columnname, this.props.columnsinfo[columnname], 'pivot', this.props.stagesObj, 0);

			insideArray.forEach(prop => {
				let secObj = {
					startlength: 0
				};
				if(colarr.length > 1)
					getChildLength(obj[prop], secObj, colindex, colarr);

				let colprop = `${paramstr ? (paramstr + '<_>') : ''}${prop}`,
					lastChild = colindex == colarr.length - 1 ? true : false,
					length = colObjProp ? (lastChild ? this.props.vals.length + secObj.startlength : this.props.vals.length * secObj.startlength) : (colarr.length > 1 ? secObj.startlength : 0)

				arr[colindex].push({
					name: prop,
					_name: columnname != '<dummy>' && this.props.columnsinfo[columnname].filterformat == 'tax' ? taxFilter(prop, this.props.app.taxObject) : prop,
					paramstr,
					colprop,
					lastChild,
					length
				});
				getNextCol(arr, colarr, colindex + 1, obj[prop], colprop, colObjProp);
			});
		}
		getNextCol(colObjArray, this.props._cols, 0, uniquecols, null, true);
		getNextCol(rowObjArray, this.props._rows, 0, uniquerows, null, false);

		let thArray = [];
		for (var i = 0; i < colObjArray.length; i++) {
			let trObj = {
				thno: thArray.length,
				tdArr: []
			};
			this.props.rows.map((row, rowindex) =>  {
				if(rowindex == 0) {
					trObj.tdArr.push({
						type: 'emptytd',
						length: this.props.rows.length
					});
				} else {
					trObj.tdArr.push({
						type: 'removetd'
					});
				}
			});
			trObj.tdArr.push({
				type: 'columntitle',
				value: this.props.cols[i] != '<dummy>' ? this.props.cols[i] : ''
			});
			for (var j = 0; j < colObjArray[i].length; j++) {
				trObj.tdArr.push({
					type: 'column',
					showValue: colObjArray[i][j]._name,
					value: colObjArray[i][j].name,
					lastChild: colObjArray[i][j].lastChild,
					length: colObjArray[i][j].length,
					col_length: this.props.vals.length
				});
				for(var k=1;k<colObjArray[i][j].length;k++) {
					trObj.tdArr.push({
						type: 'removetd'
					});
				}
			}
			if(i == 0 && this.props._cols[0] != '<dummy>') {
				if(integerValTypeArr.length > 0) {
					trObj.tdArr.push({
						type: 'columntotals',
						value: 'Totals',
						length: this.props.cols.length + (this.props.rows.length > 0 ? 1 : 0),
						col_length: integerValTypeArr.length
					});
				}
			} else if(this.props._cols[0] != '<dummy>') {
				trObj.tdArr.push({
					type: 'removetd'
				});
			}
			thArray.push(trObj);
		}

		let nextThobj = {
			thno: thArray.length,
			tdArr: []
		};
		this.props.rows.forEach((row) => {
			nextThobj.tdArr.push({
				type: 'columntitle',
				value: row
			});
		});
		nextThobj.tdArr.push({
			type: 'columntitle'
		});

		let lastRowLength = colObjArray[colObjArray.length-1].length+(this.props._cols[0] != '<dummy>' ? 1 : 0);

		for (var j = 0; j < lastRowLength; j++) {
			for (let i = 0; i < this.props.vals.length; i++) {
				if((j == colObjArray[colObjArray.length-1].length - 1 +(this.props._cols[0] != '<dummy>' ? 1 : 0) && valtypeArr[i] == 'integer') || j < colObjArray[colObjArray.length-1].length - 1 +(this.props._cols[0] != '<dummy>' ? 1 : 0) || this.props._cols[0] == '<dummy>') {
					nextThobj.tdArr.push({
						type: 'column',
						value: this.props.vals[i],
						lastChild: true,
						length: 1,
						isTotal: (lastRowLength - 1 == j) ? true : false
					});
				}
			}
		}

		thArray.push(nextThobj);

		let trArray = [];

		for (var i = rowObjArray.length - 1; i >= 0; i--) {
			let nexttrindex = 0;
			for (var j = 0; j < rowObjArray[i].length; j++) {
				if(i == this.props.rows.length - 1) {
					trArray.push({
						trno: trArray.length,
						tdArr: [rowObjArray[i][j], { type: 'removetd' }]
					});
				} else {
					trArray[nexttrindex].tdArr.splice(0, 0, rowObjArray[i][j]);

					for (var k = 1; k < rowObjArray[i][j].length; k++)
						trArray[nexttrindex + k].tdArr.splice(0, 0, {
							type: 'removetd'
						});

					nexttrindex += rowObjArray[i][j].length;
				}
			}
		}

		let lastrowindex = rowObjArray.length - 1;
		let lastcolindex = colObjArray.length - 1;

		if(rowObjArray.length > 0) {
			for (var i = 0; i < rowObjArray[lastrowindex].length; i++) {
				for (var j = 0; j < colObjArray[lastcolindex].length; j++) {
					let newprop = `${colObjArray[lastcolindex][j].colprop}<_>${rowObjArray[lastrowindex][i].colprop}`;

					this.props.vals.forEach((val, index) => {
						let newValue = uniquerowcol.hasOwnProperty(newprop) ? uniquerowcol[newprop][val] : null;
						let showValue;

						if(valtypeArr[index] == 'integer')
							showValue = currencyFilter(newValue, 'qty', this.props.app);
						else
							showValue = dateFilter(newValue, this.props.app);

						trArray[i].tdArr.push({
							type: 'value',
							valuetype: valtypeArr[index],
							prop: newprop,
							value: newValue,
							showValue,
							valueIndex: index
						});
					});
				}
			}
		} else {
			trArray.push({
				trno: trArray.length,
				tdArr: [{}] 
			});
			for (var j = 0; j < colObjArray[lastcolindex].length; j++) {
				let newprop = `${colObjArray[lastcolindex][j].colprop}`;

				this.props.vals.forEach((val, index) => {
					let newValue = uniquerowcol.hasOwnProperty(newprop) ? uniquerowcol[newprop][val] : null;
					let showValue;

					if(valtypeArr[index] == 'integer')
						showValue = currencyFilter(newValue, 'qty', this.props.app);
					else
						showValue = dateFilter(newValue, this.props.app);

					trArray[0].tdArr.push({
						type: 'value',
						prop: newprop,
						value: newValue,
						showValue,
						valueIndex: index
					});
				});
			}
		}

		let totalTrObj = {
			trno: trArray.length,
			tdArr: [{
				type: 'total',
				name: 'Totals',
				colprop: 'Totals'
			}, ...this.props.rows.map(() => {return {type: 'removetd'}})]
		};

		let tdTotal = {};
		let totaltdArraylength = trArray[0].tdArr.length;
		let totaltdArraylengthwl = trArray[0].tdArr.length + this.props.vals.length;
		trArray.forEach((tritem) => {
			let totalTRValue = {};

			for (let i = 0; i < this.props.vals.length; i++)
				totalTRValue[i] = 0;

			tritem.tdArr.forEach((tditem, tdindex) => {
				if(tditem.type != 'value')
					return null;

				let value = tditem.value ? tditem.value : 0;
				let tdDefIndex = totaltdArraylength > tritem.tdArr.length ? (tdindex + (totaltdArraylength - tritem.tdArr.length)) : tdindex;

				if(!tdTotal.hasOwnProperty(tdDefIndex))
					tdTotal[tdDefIndex] = 0;

				tdTotal[tdDefIndex] += value;

				if(tditem.valuetype != 'integer')
					tdTotal[tdDefIndex] = '';

				totalTRValue[tditem.valueIndex] += value;
			});

			this.props.vals.forEach((val, index) => {
				if(this.props._cols[0] == '<dummy>')
					return null;

				if(valtypeArr[index] == 'integer') {
					tritem.tdArr.push({
						type: 'totalvalue',
						value: totalTRValue[index],
						showValue: currencyFilter(totalTRValue[index], 'qty', this.props.app)
					});
				}
			});
			for (let i = 0; i < this.props.vals.length; i++) {
				let x = this.props.vals.length - 1;

				let lastindextd = tritem.tdArr.length - (i+1);
				let lastindextdDef = totaltdArraylengthwl > tritem.tdArr.length ? (lastindextd + (totaltdArraylengthwl - tritem.tdArr.length)) : lastindextd;
				if(!tdTotal.hasOwnProperty(lastindextdDef))
					tdTotal[lastindextdDef] = 0;

				tdTotal[lastindextdDef] += totalTRValue[x-i];
			}
		});

		if(this.props.rows.length > 0) {
			let totalKeys = Object.keys(tdTotal).map(totalkey => Number(totalkey));
			totalKeys.sort((a,b) => a - b);

			let dontIncludeArray = [];
			for (var i = 0; i < this.props.vals.length; i++) {
				if(valtypeArr[i] != 'integer')
					dontIncludeArray.push(totalKeys[totalKeys.length - 1] - this.props.vals.length + i + 1);
			}

			totalKeys.forEach(totalkey => {
				if(dontIncludeArray.includes(totalkey))
					return null;

				totalTrObj.tdArr.push({
					type: 'totalvalue',
					value: tdTotal[totalkey],
					showValue: currencyFilter(tdTotal[totalkey], 'qty', this.props.app)
				});
			});

			this.props.vals.forEach((val, index) => {
				if (this.props._cols[0] == '<dummy>')
					totalTrObj.tdArr.splice(totalTrObj.tdArr.length - 1, 1);
			});

			if(integerValTypeArr.length > 0) {
				trArray.push(totalTrObj);
			}
		}

		this.props.callback({ uniquerows, uniquecols, uniquerowcol, colObjArray, rowObjArray, trArray, thArray});

		return { uniquerows, uniquecols, uniquerowcol, colObjArray, rowObjArray, trArray, thArray};
	}

	renderFirstTR() {
		return (
			<tr className="gs-pivot-table-first-tr">
				<td></td>
				<td>{this.props.cols.map((col, colindex) => <span className="badge badge-secondary gs-pivot-table-badge" key={colindex}>{col}</span>)}</td>
			</tr>
		);
	}

	renderSecondTR() {
		return (
			<tr className="gs-pivot-table-secondf-tr">
				<td><div>{this.props.rows.map((row, rowindex) => <div key={rowindex}><span className="badge badge-secondary gs-pivot-table-badge">{row}</span></div>)}</div></td>
				<td>{this.renderInnerTable()}</td>
			</tr>
		);	
	}

	renderInnerTable() {
		return (
			<table className="table table-bordered gs-pivot-table-inner-table table-responsive">
				<thead>
					{this.renderInnerTHR()}
				</thead>
				<tbody>
					{this.renderInnerTBody()}
				</tbody>
			</table>
		);		
	}

	renderInnerTHR() {
		return this.state.thArray.map((tr) => {
			return (
				<tr key={tr.thno}>
					{tr.tdArr.map((td, tdindex) => {
						if(td.type == 'removetd') {
							return null;
						}
						if(td.type == 'emptytd') {
							return (
								<th className = 'gs-pivot-table-header-column' key={tdindex} colSpan={td.length}></th>
							);
						}
						if(td.type == 'column') {
							return (
								<th colSpan={td.length} className={`text-center ${td.isTotal ? 'gs-pivot-table-header-total' : 'gs-pivot-table-header-row'}`} key={tdindex}>{this.showName(td.showValue ? td.showValue : td.value)}</th>
							);
						}
						if(td.type == 'columntitle') {
							return (
								<th className = 'gs-pivot-table-header-column' key={tdindex}>{td.value}</th>
							);
						}
						if(td.type == 'columntotals') {
							return (
								<th className="text-center gs-pivot-table-header-total" key={tdindex} rowSpan={td.length - (this.props.rows.length > 0 ? 1 : 0)} colSpan={td.col_length}>Total</th>
							);
						}
						return (
							<th className = 'gs-pivot-table-header-column' key={tdindex}></th>
						);
					})}
				</tr>
			);
		});
	}

	renderInnerTBody() {
		return this.state.trArray.map((row, rowindex) => {
			return (
				<tr key={rowindex}>
					{row.tdArr.map((col, colindex) => {
						if(col.type == 'removetd') {
							return null;
						}
						if(col.type == 'value') {
							return (
								<td className="text-right gs-pivot-td" onClick={() => this.props.valueOnClick(col)} key={colindex}>{col.showValue}</td>
							);
						}
						if(col.type == 'totalvalue') {
							return (
								<td className="text-right gs-pivot-table-header-total" key={colindex}>{col.showValue}</td>
							);
						}
						if(col.type == 'total') {
							return (
								<th className="text-right gs-pivot-table-header-total" key={colindex} colSpan={this.props.rows.length + 1}>Totals</th>
							);
						}
						return (
							<th className = 'gs-pivot-table-header-column' key={colindex} rowSpan={col.length > 1 ? col.length : 1} colSpan={col.lastChild ? 2 : 1}>{this.showName(col._name ? col._name : col.name)}</th>
						);
					})}
				</tr>
			);
		});
	}

	showName(name) {
		if(name === '--Empty--')
			return '';

		return name;
	}

	render() {
		return (
			<div className="pvtOutput">
				{this.renderInnerTable()}
			</div>
		);
	}
}

export default connect((state) => {
	return {
		app: state.app
	}
})(GSPivotTableUI);