import React, { Component } from 'react';

export default class Reactfilter extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isOpen: this.props.isOpen ? true : false,
			isToggleClicked: false
		};
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.isOpen != this.state.isOpen)
			this.setState({
				isOpen: nextProps.isOpen ? true : false,
				isToggleClicked: true
			});
	}

	render() {
		let {isOpen, isToggleClicked }  = this.state; 
		return (
			<div ref="reportfiltercontainer" className={`reportfilter-container ${isOpen ? 'open' : ''} ${isToggleClicked ? 'reportfiltertoggle-animation' : ''}`} style={{marginTop: `${((document.getElementsByClassName('navbar').length > 0 ? document.getElementsByClassName('navbar')[0].clientHeight : '0') + (document.getElementsByClassName('report-header').length > 0 ? document.getElementsByClassName('report-header')[0].clientHeight : '0'))}px`}}>
				{isOpen ? <a className="reportfilter-toggle" onClick={() => this.props.updateToggle(false)} style={{marginTop : `${(document.getElementsByClassName('reportheader').length > 0 ? document.getElementsByClassName('reportheader')[0].clientHeight : '0')}px`}} ><i className={`fa fa-${isOpen ? 'chevron-left' : 'chevron-right'}`}></i></a> : null }
				{!isOpen ? <a className="reportfilter-collapsed-toggle" onClick={() => this.props.updateToggle(true)} style={{marginTop : `${(document.getElementsByClassName('reportheader').length > 0 ? document.getElementsByClassName('reportheader')[0].clientHeight : '0')}px`}} ><i className="fa fa-filter marginbottom-15"></i>FILTERS</a> : null }
				<div className="reportfilter-content">
					{this.props.children}
				</div>
			</div>
		);
	}

}
