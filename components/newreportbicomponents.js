import React, { Component } from 'react';
import { connect } from 'react-redux';
import PivotTableUI from 'react-pivottable/PivotTableUI';
import GSPivotTableUI from '../components/pivottable';
import 'react-pivottable/pivottable.css';
import TableRenderers from 'react-pivottable/TableRenderers';
import { aggregatorTemplates, numberFormat, sortAs } from 'react-pivottable/Utilities';
import { commonMethods, modalService } from '../utils/services';
import { Chart, Doughnut,Bar,Line } from 'react-chartjs-2';
import { biService } from '../utils/services';
import 'chartjs-plugin-colorschemes';
import 'chartjs-plugin-piechart-outlabels';
import XLSX from 'xlsx';
import { saveAs } from 'file-saver';

import { currencyFilter, dateFilter, datetimeFilter, timeFilter } from '../utils/filter';

import { rptbuilderSorting } from '../utils/utils';
import { excel_sheet_from_array_of_arrays, excel_Workbook, excel_s2ab } from '../utils/excelutils';

import axios from 'axios';

const donutLabelConfigurationObj = {
	value: '%l: %v',
	percentage: '%l: %p',
	valueandpercentage: '%l: %v (%p)'
};

const colorArray = ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"];

export default class ReactBIChart extends Component {
	constructor(props) {
		super(props);
		this.state = {
			stagesObj: {},
			redraw: false,
			exportpivotObj: {}
		}

		this.onLoad = this.onLoad.bind(this);
		this.getPivotobjFN = this.getPivotobjFN.bind(this);
		this.updatePivotData = this.updatePivotData.bind(this);
		this.print = this.print.bind(this);
		this.getDataFieldName = this.getDataFieldName.bind(this);
		this.renderBIComponent = this.renderBIComponent.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getColor = this.getColor.bind(this);
		this.setFilterValues = this.setFilterValues.bind(this);
		this.sortColumns = this.sortColumns.bind(this);
		this.generateExcelforImport = this.generateExcelforImport.bind(this);
		this.chartOnClick = this.chartOnClick.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	componentWillReceiveProps(nextProps) {
		let { pivotobj } = this.state,
			legendFlag = false;

		if (!pivotobj || !nextProps.isDashboard)
			return null;

		if((this.props.isMaximize == nextProps.isMaximize) && (nextProps.reportHeight == this.props.reportHeight))
			return false;

		let tempObj = {...pivotobj};

		legendFlag = nextProps.isMaximize;

		if (tempObj.chartdata.datasets.length == 1 && tempObj.analyticstype == 'Bar Chart')
			legendFlag = false;

		tempObj.chartoptions.legend.display = tempObj.analyticstype == 'Donut Chart' ? false : legendFlag;

		if (tempObj.analyticstype != 'Donut Chart') {
			tempObj.chartoptions.scales.yAxes[0].scaleLabel.display = nextProps.isMaximize;
			tempObj.chartoptions.scales.xAxes[0].scaleLabel.display = nextProps.isMaximize;
		} else {
			tempObj.chartoptions.layout.padding.bottom = nextProps.reportHeight > 380 ? 50 : 20;
			tempObj.chartoptions.layout.padding.top = nextProps.reportHeight > 380 ? 50 : 20;
		}

		this.setState({
			pivotobj: tempObj,
			redraw: true
		});

		//if(!legendFlag) {
			setTimeout(() => {
				this.setState({
					redraw: false
				});
			}, 1000);
		//}
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad () {
		let stagesObj = {};

		axios.get(`/api/leadstages?&field=id,name,index,probability&filtercondition=`).then((response) => {
			if (response.data.message == 'success') {
				response.data.main.forEach((item) => {
					stagesObj[item.name] = item.index
				});

				this.setState({
					stagesObj
				})
			}

			this.getPivotobjFN();
		});
	}

	getDataFieldName(field) {
		var columninfo = this.props.chartprops.columnsinfo[field];
		return columninfo.isForeignKey ? `${field}_dn` : field;
	}

	getColor(data) {
		let genColorArray = [];

		for (let i = 0; i < data; i++)
			genColorArray.push(colorArray[i%colorArray.length]);

		return genColorArray;
	}

	setFilterValues (filter, value) {
		let val = value;

		let isValid = false;

		if (['date', 'datetime', 'time'].includes(filter))
			isValid = (new Date(value) != 'Invalid Date') ? true : false;
		else if (['currency', 'number'].includes(filter))
			isValid = (typeof(value) == 'number') ? true : false;

		if (isValid) {
			if (filter == 'date')
				val = dateFilter(value);
			else if (filter == 'time')
				val = timeFilter(value);
			else if (filter == 'datetime')
				val = datetimeFilter(value);
			else if (['currency', 'number'].includes(filter))
				val = currencyFilter(value, 'qty', this.props.app);
		}

		return val;
	}

	getPivotobjFN() {
		let {reportjson,columnsinfo} = this.props.chartprops;

		Chart.defaults.global.defaultFontFamily = "'Open Sans', 'sans-serif !important'";

		let pivotobj = {
			analyticstype : reportjson.config.analyticstype,
		    rendererName: 'Table',
		    rows: [],
		    cols: [],
		    vals: [],
		    _rows: [],
		    _cols: [],
		    _vals: [],
		    __rows: [],
		    __cols: [],
		    rowsFilter: [],
		    colsFilter: [],
		    chartdata : {
				datasets:[],
				labels :[]
			},
			chartoptions: {
				maintainAspectRatio: false,
				layout: {
					padding: {
						left: 0,
						right: 0,
						top: reportjson.config.analyticstype == 'Donut Chart' ? (this.props.reportHeight > 380 ? 50 : 25) : 0,
						bottom: reportjson.config.analyticstype == 'Donut Chart' ? (this.props.reportHeight > 380 ? 50 : 25) : 0
					}
				},
				plugins: {
					colorschemes: {
						scheme: 'tableau.Tableau20'
					},
					outlabels: {
						text: '%l',
						stretch: 20,
						font: {
							minSize: 12
						}
					}
				},
				legend: {
					display: (!this.props.isDashboard || (this.props.isDashboard && this.props.isMaximize)) ? (reportjson.config.analyticstype == 'Donut Chart' ? false :  true) : false,
					position: 'bottom'
				},
				scales: {
					yAxes: [{
	        			scaleLabel: {
							display: (!this.props.isDashboard || (this.props.isDashboard && this.props.isMaximize)) ? true : false,
							labelString: '',
							fontColor: '#1abc9c',
							fontSize: 12
						},
						ticks: {
							beginAtZero: true,
							precision: 0,
							callback: (value, index, values) => {
								return currencyFilter(value, 'qty', this.props.app)
							}
						}
					}],
					xAxes: [{
						maxBarThickness: 100,
						scaleLabel: {
							display: (!this.props.isDashboard || (this.props.isDashboard && this.props.isMaximize)) ? true : false,
							labelString: '',
							fontColor: '#1abc9c',
							fontSize: 12
						}
					}]
				},
				tooltips: {
					callbacks: {
						label: (tooltipItem, chart) => {
							let rtnValue;

							if (['Bar Chart', 'Line Chart'].includes(reportjson.config.analyticstype)) {
								let val = this.setFilterValues('number', Number(tooltipItem.value));

								rtnValue = `${chart.datasets[tooltipItem.datasetIndex].label}: ${val}`
							}

							if (reportjson.config.analyticstype == 'Donut Chart') {
								return null;
								let val = this.setFilterValues('number', Number(chart.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]));

								rtnValue = `${chart.labels[tooltipItem.index]}: ${val}`;
							}

							return rtnValue;
						},
						title: (tooltipItem, chart) => {
							return [chart.labels[tooltipItem[0].index]];
						}
					}
				}
			}
		};

		let measureTypeObj = {
			'COUNT': 'Count',
			'SUM': 'Sum',
			'AVG': 'Avg',
			'MIN': 'Min',
			'MAX': 'Max'
		};

		if(reportjson.config.fields.series1) {
			pivotobj.__rows.push(reportjson.config.fields.series1);
			pivotobj._rows.push(this.getDataFieldName(reportjson.config.fields.series1));
			pivotobj.rows.push(columnsinfo[reportjson.config.fields.series1].displayName);
			pivotobj.rowsFilter.push(columnsinfo[reportjson.config.fields.series1].filterformat ? columnsinfo[reportjson.config.fields.series1].filterformat : 'text');
		}
		if(reportjson.config.fields.series2) {
			pivotobj.__rows.push(reportjson.config.fields.series2);
			pivotobj._rows.push(this.getDataFieldName(reportjson.config.fields.series2));
			pivotobj.rows.push(columnsinfo[reportjson.config.fields.series2].displayName);
			pivotobj.rowsFilter.push(columnsinfo[reportjson.config.fields.series2].filterformat ? columnsinfo[reportjson.config.fields.series2].filterformat : 'text');
		}

		if(reportjson.config.fields.axis1) {
			pivotobj.__cols.push(reportjson.config.fields.axis1);
			pivotobj._cols.push(this.getDataFieldName(reportjson.config.fields.axis1));
			pivotobj.cols.push(columnsinfo[reportjson.config.fields.axis1].displayName);
			pivotobj.colsFilter.push(columnsinfo[reportjson.config.fields.axis1].filterformat ? columnsinfo[reportjson.config.fields.axis1].filterformat : 'text');
			pivotobj.chartoptions.scales.xAxes[0].scaleLabel.labelString = columnsinfo[reportjson.config.fields.axis1].displayName;
		}
		if(reportjson.config.fields.axis2){
			pivotobj.__cols.push(reportjson.config.fields.axis2);
			pivotobj._cols.push(this.getDataFieldName(reportjson.config.fields.axis2));
			pivotobj.cols.push(columnsinfo[reportjson.config.fields.axis2].displayName);
			pivotobj.colsFilter.push(columnsinfo[reportjson.config.fields.axis2].filterformat ? columnsinfo[reportjson.config.fields.axis2].filterformat : 'text');
		}

		if(reportjson.config.aggregation.measuretype1) {
			pivotobj._vals.push(reportjson.config.aggregation.measuretype1 == 'COUNT' ? 'count' : `${reportjson.config.aggregation.measure1}_${reportjson.config.aggregation.measuretype1.toLowerCase()}`);
			pivotobj.vals.push(reportjson.config.aggregation.measuretype1 == 'COUNT' ? 'Count' : `${measureTypeObj[reportjson.config.aggregation.measuretype1]} of ${columnsinfo[reportjson.config.aggregation.measure1].displayName}`);
			pivotobj.chartoptions.scales.yAxes[0].scaleLabel.labelString = reportjson.config.aggregation.measuretype1 == 'COUNT' ? 'COUNT' : columnsinfo[reportjson.config.aggregation.measure1].displayName;
		}

		if (pivotobj.analyticstype == 'Table' && reportjson.config.aggregation.measuretype2) {
			pivotobj._vals.push(reportjson.config.aggregation.measuretype2 == 'COUNT' ? 'count' : `${reportjson.config.aggregation.measure2}_${reportjson.config.aggregation.measuretype2.toLowerCase()}`);
			pivotobj.vals.push(reportjson.config.aggregation.measuretype2 == 'COUNT' ? 'Count' : `${measureTypeObj[reportjson.config.aggregation.measuretype2]} of ${columnsinfo[reportjson.config.aggregation.measure2].displayName}`);
			pivotobj.chartoptions.scales.yAxes[0].scaleLabel.labelString = reportjson.config.aggregation.measuretype2 == 'COUNT' ? 'Count' : columnsinfo[reportjson.config.aggregation.measure2].displayName;
		}

		if (pivotobj.analyticstype == 'Table' && reportjson.config.aggregation.measuretype3) {
			pivotobj._vals.push(reportjson.config.aggregation.measuretype3 == 'COUNT' ? 'count' : `${reportjson.config.aggregation.measure3}_${reportjson.config.aggregation.measuretype3.toLowerCase()}`);
			pivotobj.vals.push(reportjson.config.aggregation.measuretype3 == 'COUNT' ? 'Count' : `${measureTypeObj[reportjson.config.aggregation.measuretype3]} of ${columnsinfo[reportjson.config.aggregation.measure3].displayName}`);
			pivotobj.chartoptions.scales.yAxes[0].scaleLabel.labelString = reportjson.config.aggregation.measuretype3 == 'COUNT' ? 'Count' : columnsinfo[reportjson.config.aggregation.measure3].displayName;
		}
	
		if(pivotobj.analyticstype == 'Table') {
			pivotobj.data = [];

			this.props.chartprops.originalRows.forEach((row) => {
				let tempObj = {};
				pivotobj._rows.forEach((field, fieldindex) => {
					tempObj[pivotobj.rows[fieldindex]] = row[field];
				});
				pivotobj._cols.forEach((field, fieldindex) => {
					tempObj[pivotobj.cols[fieldindex]] = row[field];
				});
				pivotobj._vals.forEach((field, fieldindex) => {
					tempObj[pivotobj.vals[fieldindex]] = row[field];
				});
				pivotobj.data.push(tempObj);
			});

			pivotobj.renderers = {
				'Table': TableRenderers['Table'],
				'Exportable TSV': TableRenderers['Exportable TSV']
			};
			pivotobj.hiddenAttributes = [...pivotobj._vals];

			pivotobj.aggregators = {};
			pivotobj.aggregators['SUM'] = () => {
				return aggregatorTemplates['sum']()([pivotobj.vals[0]]);
			};

			if (pivotobj._cols.length == 0) {
				pivotobj._cols = ['<dummy>'];
				pivotobj.cols = ['<dummy>'];
			}
		} else if(['Bar Chart', 'Line Chart'].includes(pivotobj.analyticstype)) {
			let uniquexobj = {}, uniqueyobj = {}, uniquevalue = {};

			this.props.chartprops.originalRows.forEach((row) => {
				let xvalue = pivotobj._cols[0] ? (row[pivotobj._cols[0]] ? row[pivotobj._cols[0]] : 'No Value') : '<empty>';
				let yvalue = pivotobj._rows[0] ? (row[pivotobj._rows[0]] ? row[pivotobj._rows[0]] : 'No Value') : '<empty>';

				if(!uniquexobj[xvalue])
					uniquexobj[xvalue] = true;

				if(!uniqueyobj[yvalue])
					uniqueyobj[yvalue] = true;

				uniquevalue[`${xvalue}_${yvalue}`] = row[pivotobj._vals[0]];
			});

			let sortedXArray = this.sortColumns(Object.keys(uniquexobj), pivotobj._cols[0], 'Axis');

			let sortedYArray = Object.keys(uniqueyobj);
			if(pivotobj._rows.length > 0)
				sortedYArray = this.sortColumns(sortedYArray, pivotobj._rows[0], 'Axis');

			sortedXArray.forEach((xfie, xfieindex) => {
				sortedYArray.forEach((yfie, yfieindex) => {
					if(pivotobj.chartdata.datasets[yfieindex]) {
						pivotobj.chartdata.datasets[yfieindex].data.push(uniquevalue[`${xfie}_${yfie}`] ? uniquevalue[`${xfie}_${yfie}`] : 0);
					} else {
						pivotobj.chartdata.datasets[yfieindex] = {
							label : pivotobj._rows[0] ? yfie : `${pivotobj.vals[0]}`,
							data : [uniquevalue[`${xfie}_${yfie}`] ? uniquevalue[`${xfie}_${yfie}`] : 0],
							fill : (pivotobj.analyticstype == 'Bar Chart') ? true :false
						};
					}
				});
			});

			pivotobj.chartdata.labels = sortedXArray;

			pivotobj.chartoptions.scales.xAxes[0].labels = sortedXArray.map((item) => {
				return item.length > 10 ? item.toString().substr(0, 10).trim()+'...' : item;
			});

			if(pivotobj.chartdata.datasets.length == 1 && pivotobj.analyticstype == 'Bar Chart') {
				pivotobj.chartdata.datasets[0].backgroundColor = this.getColor(pivotobj.chartdata.labels.length);
				pivotobj.chartoptions.legend.display = false;
			}

		} else if(pivotobj.analyticstype == 'Donut Chart') {
			let data = [], dataObj = {};

			this.props.chartprops.originalRows.forEach((row) => {
				let propnew = row[pivotobj._cols[0]] ? row[pivotobj._cols[0]] : 'No Value';
				pivotobj.chartdata.labels.push(propnew);
				dataObj[propnew] = row[pivotobj._vals[0]]
			});

			pivotobj.chartdata.labels = this.sortColumns(pivotobj.chartdata.labels, pivotobj._cols[0], 'Axis');

			pivotobj.chartdata.datasets.push({
				data : pivotobj.chartdata.labels.map(propnew => dataObj[propnew])
			});

			let temptotalvalue = 0;
			pivotobj.chartdata.datasets[0].data.forEach(val => temptotalvalue += val);

			let tempPercentValuesArray = pivotobj.chartdata.datasets[0].data.map(val => {
				val = Number(((val / temptotalvalue) * 100).toFixed(2));
				return val;
			});

			pivotobj.chartdata.rawlabels = pivotobj.chartdata.labels.map(label => label);
			pivotobj.chartdata.labels = pivotobj.chartdata.labels.map((label, labelindex) => {
				let donutValueToolTipTags = {
					"%l": label,
					"%v": pivotobj.chartdata.datasets[0].data[labelindex],
					"%p": `${tempPercentValuesArray[labelindex]} %`
				};

				let rtnValue = donutLabelConfigurationObj[(reportjson.config.donut_valuetype || 'value')];

				for(var prop in donutValueToolTipTags)
					rtnValue = rtnValue.replace(new RegExp(prop, 'g'), donutValueToolTipTags[prop]);

				return rtnValue;
			});

			delete pivotobj.chartoptions.scales;
	    } else if(pivotobj.analyticstype == 'KPI') {
			let kpiValue = 0,
				aggregation = reportjson.config.aggregation;

			let kpiProp = aggregation['measuretype1'] == 'COUNT' ? aggregation['measuretype1'].toLowerCase() : aggregation['measure1'].toLowerCase() + '_' + aggregation['measuretype1'].toLowerCase();

			if (this.props.chartprops.originalRows && this.props.chartprops.originalRows.length > 0)
				kpiValue = currencyFilter(this.props.chartprops.originalRows[0][kpiProp], 'qty', this.props.app);

			pivotobj.kpiValue = kpiValue;
	    }

		this.setState({ pivotobj });
	}

	updatePivotData(obj) {
		let pivotobj  = {...this.state.pivotobj};
		pivotobj.data = obj.data;
		pivotobj.rowsArray = obj.rows;
		pivotobj.cols = obj.cols;
		pivotobj.vals = obj.vals;
		pivotobj.hiddenAttributes = obj.hiddenAttributes;
		pivotobj.renderers = obj.renderers;
		pivotobj.aggregators = obj.aggregators;
		pivotobj.aggregatorName = obj.aggregatorName;
		pivotobj.rendererName = obj.rendererName;
		pivotobj.sorters = obj.sorters;
		
		this.setState({
			pivotobj
		});
	}

	print() {
		if(this.state.pivotobj.analyticstype == 'Table')
			this.generateExcelforImport(this.props.chartprops.reportdisplayName, this.props.app);
		else
			commonMethods.downloadCanvasImage(this.props.chartprops.reportdisplayName);
	}

	sortColumns (data, column, ref) {
		let { stagesObj } = this.state,
			columnsinfo = this.props.chartprops.columnsinfo;

		column = (columnsinfo[column]) ? column : columnsinfo[column.substr(0, column.lastIndexOf('_dn'))] ? column.substr(0, column.lastIndexOf('_dn')) : null;

		if (!column)
			return data;

		return rptbuilderSorting(data, column, columnsinfo[column], ref, stagesObj);
	}

	chartOnClick(element, event) {
		let { pivotobj } = this.state;
		let filterObj = {};

		if(['Line Chart', 'Bar Chart', 'Donut Chart'].includes(pivotobj.analyticstype)) {
			if(element.length != 1)
				return null;

			filterObj[pivotobj.__cols[0]] = {
				valueProp: pivotobj._cols[0],
				valueDisplayProp: pivotobj.cols[0],
				value: pivotobj.analyticstype == 'Donut Chart' ? pivotobj.chartdata.rawlabels[element[0]._index] : pivotobj.chartdata.labels[element[0]._index]
			};

			if(pivotobj.__rows.length > 0) {
				filterObj[pivotobj.__rows[0]] = {
					valueProp: pivotobj._rows[0],
					valueDisplayProp: pivotobj.rows[0],
					value: pivotobj.chartdata.datasets[element[0]._datasetIndex].label
				};
			}

			this.props.openDrillDown(filterObj);
		} else if(pivotobj.analyticstype == 'KPI') {
			this.props.openDrillDown(filterObj);
		} else {
			let tempValueArray = element.prop.split('<_>');

			let tempFieldArray = [];

			pivotobj.__cols.forEach((field, fieldindex) => {
				tempFieldArray.push({
					value: field,
					valueProp: pivotobj._cols[fieldindex],
					valueDisplayProp: pivotobj.cols[fieldindex]
				});
			});
			if(pivotobj.__cols.length == 0) {
				tempFieldArray.push({
					value: '__dummy',
					valueProp: '__dummy',
					valueDisplayProp: '__dummy'
				});
			}
			pivotobj.__rows.forEach((field, fieldindex) => {
				tempFieldArray.push({
					value: field,
					valueProp: pivotobj._rows[fieldindex],
					valueDisplayProp: pivotobj.rows[fieldindex]
				});
			});

			tempFieldArray.forEach((field, fieldindex) => {
				filterObj[field.value] = {
					valueProp: field.valueProp,
					valueDisplayProp: field.valueDisplayProp,
					value: tempValueArray[fieldindex]
				};
			});
			delete filterObj.__dummy;
			this.props.openDrillDown(filterObj);
		}
	}

	renderBIComponent(){
		let styleProps = {
			width: null,
			height: null
		};

		if(this.state.pivotobj.analyticstype == 'Table') {
			/*return <PivotTableUI data={this.state.pivotobj.data} rows={this.state.pivotobj.rows} cols={this.state.pivotobj.cols} vals={this.state.pivotobj.vals} hiddenAttributes={this.state.pivotobj.hiddenAttributes} renderers={this.state.pivotobj.renderers} aggregators={this.state.pivotobj.aggregators} aggregatorName = {'SUM'} rendererName = {this.state.pivotobj.rendererName} displayModeBar={false} sorters = {this.state.pivotobj.sorters} onChange={(a) => {this.updatePivotData(a)}} />;*/
		return <GSPivotTableUI data={this.state.pivotobj.data} rows={this.state.pivotobj.rows} cols={this.state.pivotobj.cols} vals={this.state.pivotobj.vals} hiddenAttributes={this.state.pivotobj.hiddenAttributes} renderers={this.state.pivotobj.renderers} aggregators={this.state.pivotobj.aggregators} aggregatorName = {'SUM'} rendererName = {this.state.pivotobj.rendererName} displayModeBar={false} sorters = {this.state.pivotobj.sorters} _rows={this.state.pivotobj._rows} _cols={this.state.pivotobj._cols} _vals={this.state.pivotobj._vals} stagesObj = {this.state.stagesObj} columnsinfo = {this.props.chartprops.columnsinfo} onChange={(a) => {this.updatePivotData(a)}} valueOnClick={this.chartOnClick} callback = {(val) => {
			this.setState({
				exportpivotObj: {...val}
			})
		}}/>;
		} else if(this.state.pivotobj.analyticstype == 'Line Chart') {
			return  <Line data={this.state.pivotobj.chartdata} {...styleProps} options={this.state.pivotobj.chartoptions} redraw={this.state.redraw} getElementAtEvent={this.chartOnClick} />;
		} else if(this.state.pivotobj.analyticstype == 'Bar Chart') {
			return <Bar data={this.state.pivotobj.chartdata} {...styleProps} options={this.state.pivotobj.chartoptions} redraw={this.state.redraw} getElementAtEvent={this.chartOnClick} /> ;
		} else if(this.state.pivotobj.analyticstype == 'Donut Chart') {
			return  <Doughnut data={this.state.pivotobj.chartdata} {...styleProps} options={this.state.pivotobj.chartoptions} redraw={this.state.redraw} getElementAtEvent={this.chartOnClick} />;
		} else if (this.state.pivotobj.analyticstype == 'KPI') {
			return(
				<div className = 'row no-gutters h-100'>
					<div className = 'col-md-12 d-flex align-items-end' style = {{color: '#3B99FF', fontSize: '26px', fontWeight: '600'}}>
						<span className = 'overflowtxt' rel = 'tooltip' title = {this.state.pivotobj.kpiValue} onClick={this.chartOnClick}>{this.state.pivotobj.kpiValue}</span>
					</div>
				</div>
			);
		} else {
			return null;
		}
	}

	render() {
		if(!this.state.pivotobj){
			return null;
		}

		let styleprop = {
			maxHeight: this.props.reportHeight,
			height: this.props.reportHeight,
			overflowX: 'hidden',
			overflowY: 'auto'
		};

		if (this.state.pivotobj.analyticstype != 'KPI' && !this.props.isDashboard)
			styleprop.paddingLeft = '50px';

		return (
			<div className = 'row'>
				<div className = 'col-md-12' style={styleprop}>
					{
						this.renderBIComponent()
					}
				</div>
			</div>
		);
	}

	showName(name) {
		if(name === '--Empty--')
			return '';

		return name;
	}

	generateExcelforImport (reportName, app) {
		let reportData = [[{v: ''}, {v: 'Report Name'}, {v: reportName}],[],[{v: ''}, {v: 'Company Name'}, {v: app.selectedCompanyDetails.legalname}],[],[]];
		let widthObj = {};
		let mergesArray = [];

		this.state.exportpivotObj.thArray.forEach((row, rowindex) => {
			let trRowObj = [], trRowIndex = reportData.length;
			row.tdArr.forEach((col, colindex) => {
				if(!widthObj[colindex]) {
					widthObj[colindex] = true;
				}
				if(col.type == 'removetd') {
					return trRowObj.push({
						v: ''
					});
				}
				if(col.type == 'columntitle') {
					return trRowObj.push({
						v: col.value
					});
				}
				if(col.type == 'emptytd') {
					trRowObj.push({
						v: ''
					});
					if(col.length > 1) {
						return mergesArray.push({
							s: {
								r: trRowIndex,
								c: trRowObj.length - 1
							},
							e: {
								r: trRowIndex,
								c: trRowObj.length - 1 - 1 + col.length
							}
						});
					} else {
						return null;
					}
				}
				if(col.type == 'column') {
					trRowObj.push({
						v: this.showName(col.value)
					});

					if(col.length > 1) {
						mergesArray.push({
							s: {
								r: trRowIndex,
								c: trRowObj.length - 1
							},
							e: {
								r: trRowIndex,
								c: trRowObj.length - 1 - 1 + col.length
							}
						});
					}
					return null;
				}
				if(col.type == 'columntotals') {
					trRowObj.push({
						v: 'Totals'
					});

					return mergesArray.push({
						s: {
							r: trRowIndex,
							c: trRowObj.length - 1
						},
						e: {
							r: trRowIndex - 1 + col.length - (this.state.pivotobj.rows.length > 0 ? 1 : 0),
							c: trRowObj.length - 1 + (col.col_length ? col.col_length - 1 : 0)
						}
					});
				}
				trRowObj.push({
					v: col.name
				});
			});
			reportData.push(trRowObj);
		});

		let widthArray = Object.keys(widthObj).map(item => {return { wch : 18 };});

		this.state.exportpivotObj.trArray.forEach((row, rowindex) => {
			let trRowObj = [], trRowIndex = reportData.length;
			row.tdArr.forEach((col, colindex) => {
				if(col.type == 'removetd') {
					return trRowObj.push({
						v: ''
					});
				}
				if(col.type == 'value') {
					return trRowObj.push({
						v: col.valuetype == 'integer' ? col.value : col.showValue
					});
				}
				if(col.type == 'totalvalue') {
					return trRowObj.push({
						v: col.value
					});
				}
				if(col.type == 'total') {
					trRowObj.push({
						v: 'Totals'
					});
					return mergesArray.push({
						s: {
							r: trRowIndex,
							c: trRowObj.length - 1
						},
						e: {
							r: trRowIndex,
							c: trRowObj.length - 1 + this.state.pivotobj.rows.length
						}
					});
				}
				trRowObj.push({
					v: this.showName(col.name)
				});
				if(col.lastChild) {
					mergesArray.push({
						s: {
							r: trRowIndex,
							c: trRowObj.length - 1
						},
						e: {
							r: trRowIndex,
							c: trRowObj.length
						}
					});
				}
				if(col.length > 1) {
					mergesArray.push({
						s: {
							r: trRowIndex,
							c: trRowObj.length - 1
						},
						e: {
							r: trRowIndex + col.length - 1,
							c: trRowObj.length - 1
						}
					});
				}
			});
			reportData.push(trRowObj);
		});

		let wb = {
			SheetNames : [],
			Sheets : {}
		};

		let ws = excel_sheet_from_array_of_arrays(reportData);
		wb.SheetNames.push(reportName);
		wb.Sheets[reportName] = ws;
		ws['!cols'] = widthArray;

		if (mergesArray.length > 0)
			wb.Sheets[reportName]['!merges'] = mergesArray;

		let wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
		saveAs(new Blob([excel_s2ab(wbout)],{type:"application/octet-stream"}), reportName + "_" + (dateFilter(new Date())) + ".xlsx");
	}
}
