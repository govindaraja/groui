import React, { Component } from 'react';
import { connect } from 'react-redux';
import PivotTableUI from 'react-pivottable/PivotTableUI';
import 'react-pivottable/pivottable.css';
import TableRenderers from 'react-pivottable/TableRenderers';
import { aggregatorTemplates, numberFormat, sortAs } from 'react-pivottable/Utilities';
import Plotly from 'plotly.js';

import { commonMethods, modalService } from '../utils/services';

//import Plot from 'react-plotly.js';
import createPlotlyComponent from 'react-plotly.js/factory';
const Plot = createPlotlyComponent(Plotly);


import createPlotlyRenderers from 'react-pivottable/PlotlyRenderers';

const PlotlyRenderers = createPlotlyRenderers(Plot);

import { biService } from '../utils/services';

export default class ReactBIChart extends Component {
	constructor(props) {
		super(props);
		this.state = {
			variableObject : biService.getVariableObject(this.props.form, this.props.app),
			operatorObject : biService.getOperatorObject(),
			dataObject : biService.getDataObject(this.props.form, this.props.app),
		};
		this.getpivotdata();
		this.getpivotdata = this.getpivotdata.bind(this);
		this.updatePivotData = this.updatePivotData.bind(this);
		this.print = this.print.bind(this);
	}

	componentWillReceiveProps(nextProps, prevProps) {
		if(nextProps.chartprops.originalRows && (!this.props.chartprops || !this.props.chartprops.originalRows || (this.props.chartprops.originalRows.length != nextProps.chartprops.originalRows.length))) {
			this.getpivotdata();
		}
	}


	getpivotdata() {
		let { pivotobj } = this.props.chartprops;
		pivotobj.data = [];

		for (var i = 0; i < this.props.chartprops.originalRows.length; i++) {
			let tempObj = {};
			for (var prop in this.state.variableObject[this.props.chartprops.resource]) {
				if (prop in this.props.chartprops.originalRows[i]) {
					tempObj[this.state.variableObject[this.props.chartprops.resource][prop]] = ((this.props.chartprops.originalRows[i][prop] != null) ? this.props.chartprops.originalRows[i][prop] : "");
				}
			}

			tempObj[pivotobj.operator.toLowerCase()] = this.props.chartprops.originalRows[i][pivotobj.operator.toLowerCase()];
			pivotobj.data.push(tempObj);
		}

		pivotobj.renderers = {
			'Table': TableRenderers['Table'],
			'Exportable TSV': TableRenderers['Exportable TSV'],
			'Bar Chart' : PlotlyRenderers['Grouped Column Chart'],
			'Line Chart' : PlotlyRenderers['Line Chart'],
		};
		pivotobj.hiddenAttributes = [pivotobj.operator.toLowerCase()];

		pivotobj.aggregators = {};
		pivotobj.aggregators[pivotobj.operator] = () => {
			if(pivotobj.operator=='COUNT') {
				return aggregatorTemplates['sum'](numberFormat({digitsAfterDecimal: 0}))([pivotobj.operator.toLowerCase()]);
			} else {
				return aggregatorTemplates[this.state.operatorObject[pivotobj.operator]]()([pivotobj.operator.toLowerCase()]);
			}
		};

		pivotobj.sorters = (attr) => {
			if(attr == 'Month' || attr == 'Quarter') {
				let sortArray = biService.sortByPeriod(pivotobj.data, attr);
				return sortAs(sortArray);
			}
		}

		if (pivotobj.data.length > 0) {
			pivotobj.rowsArray = [];
			pivotobj.cols = [];
			pivotobj.vals = [pivotobj.operator.toLowerCase()];
			for (var i = 0; i < pivotobj.rows.length; i++) {
				if (pivotobj.rows[i]) {
					pivotobj.rowsArray.push(this.state.variableObject[this.props.chartprops.resource][this.props.chartprops.pivotobj.rows[i]]);
				}
			}

			for (var i = 0; i < pivotobj.columns.length; i++) {
				if (pivotobj.columns[i]) {
					pivotobj.cols.push(this.state.variableObject[this.props.chartprops.resource][pivotobj.columns[i]]);
				}
			}
		}

		this.props.updateFormState(this.props.form, {
			pivotobj
		});
	}

	updatePivotData(obj) {
		let pivotobj  = {...this.props.chartprops.pivotobj};
		pivotobj.data = obj.data;
		pivotobj.rowsArray = obj.rows;
		pivotobj.cols = obj.cols;
		pivotobj.vals = obj.vals;
		pivotobj.hiddenAttributes = obj.hiddenAttributes;
		pivotobj.renderers = obj.renderers;
		pivotobj.aggregators = obj.aggregators;
		pivotobj.aggregatorName = obj.aggregatorName;
		pivotobj.rendererName = obj.rendererName;
		pivotobj.sorters = obj.sorters;
		this.props.updateFormState(this.props.form, {pivotobj});
	}

	print() {
		commonMethods.printBI(this.props.chartprops.configid_name, this.props.chartprops.pivotobj.rendererName);
	}

	render() {
		return (
			<div style={{paddingLeft: '50px'}}>
				<PivotTableUI data={this.props.chartprops.pivotobj.data} rows={this.props.chartprops.pivotobj.rowsArray} cols={this.props.chartprops.pivotobj.cols} vals={this.props.chartprops.pivotobj.vals} hiddenAttributes={this.props.chartprops.pivotobj.hiddenAttributes} renderers={this.props.chartprops.pivotobj.renderers} aggregators={this.props.chartprops.pivotobj.aggregators} aggregatorName = {this.props.chartprops.pivotobj.operator} rendererName = {this.props.chartprops.pivotobj.rendererName} displayModeBar={false} sorters = {this.props.chartprops.pivotobj.sorters} onChange={(a) => {this.updatePivotData(a)}} />
			</div>
		);
	}

}
