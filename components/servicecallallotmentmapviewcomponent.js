import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import async from 'async';

import { updateFormState, openModal } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation, search, sort_by, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, checkboxEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter, timeFilter, datetimeFilter, dateAgoFilter, arrayFilter } from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';
import { SelectAsync, LocalSelect } from '../components/utilcomponents';
import MapProps from '../components/map-properties';
import DetailedInfoWindow from '../components/details/mapdetailsview';

export default class MapForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			boundLatLng: {},
			markers: {},
			firstTime: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getEmployeeLocations = this.getEmployeeLocations.bind(this);
		this.initializeMap = this.initializeMap.bind(this);
		this.markerListenerFn = this.markerListenerFn.bind(this);
		this.renderInfoWindowFn = this.renderInfoWindowFn.bind(this);
		this.showDetailsChange = this.showDetailsChange.bind(this);
		this.mapViewOnChange = this.mapViewOnChange.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		setTimeout(() => {
			this.setState({
				map : new google.maps.Map(document.getElementById('map'), MapProps.MapOptions),
				bounds : new google.maps.LatLngBounds()
			}, () => {
				this.getEmployeeLocations();
			});
		}, 0);
	}

	componentWillReceiveProps(nextprops) {
		if(nextprops.resource.refreshcount != this.props.resource.refreshcount)
			this.getEmployeeLocations();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	getEmployeeLocations () {
		this.updateLoaderFlag(true);

		let sc_engArray = [];

		this.props.resource.engineerDetails.forEach((item, index) => {
			if(!(item.allvalue.latitude && item.allvalue.longitude))
				return null;

			sc_engArray.push({
				position: {
					lat : item.allvalue.latitude,
					lng : item.allvalue.longitude
				},
				markerType: 'engineer',
				...item,
				indexVal: index
			});
		});

		this.onLoad(sc_engArray);
	}

	onLoad(sc_engArray) {

		let serviceCallArray = arrayFilter((this.props.resource.openServiceCallDetails || []), this.props.resource.servicecallsearch, ['customerid_name', 'installationaddress', 'complaintcategoryid_name', 'servicecallno']);

		serviceCallArray.forEach((item, index) => {
			if(!(item.installationaddressid_latitude && item.installationaddressid_longitude))
				return null;

			sc_engArray.push({
				position: {
					lat : item.installationaddressid_latitude,
					lng : item.installationaddressid_longitude
				},
				markerType: 'servicecall',
				...item,
				indexVal: index
			});
		});

		this.setState({
			sc_engArray: [...sc_engArray]
		}, () => {
			this.initializeMap();
		});

		this.updateLoaderFlag(false);
	}

	renderInfoWindowFn (item) {
		let infocontent = '';

		infocontent = `<div style="font-family: Open Sans, sans-serif; font-weight: 600; font-size: 14px;margin-bottom: 4px;"><b>${item.customerid ? item.customerid_name : item.userid_displayname}</b></div>
				<div style="font-family: Open Sans, sans-serif;font-size:12px;">`;

		if(item.markerType == 'servicecall') {
			infocontent += `<div style="background-color: #e4e4e4; padding: 4px; border-radius: 0.25rem; display: inline-block; margin-right: 10px; margin-bottom: 4px;">${item.servicecallno}</div>`;
			if(item.complaintcategoryid_name)
				infocontent += `<div style=background-color: #e4e4e4; padding: 4px; border-radius: 0.25rem; display: inline-block;">${item.complaintcategoryid_name}</div>`;

			this.props.resource.serviceAllocationDetails.forEach(svccall => {
				if(item.id == svccall.servicecallid) {
					infocontent += `<div style="margin-bottom: 4px; font-weight: 600;"><span class="fa fa-user-o" style="margin-right: 5px;"></span>${svccall.assignedto ? svccall.assignedto_displayname : 'Not Assigned'}</div>`
					infocontent += `<div style="display: inline-block; background-color: #e4e4e4; margin-bottom: 4px; padding: 2px;"><span>${timeFilter(svccall.start)}</span> - <span>${timeFilter(svccall.end)}</span></div>`
				}
			});
		}

		if(item.markerType == 'engineer') {
			this.props.resource.serviceAllocationDetails.forEach(svccall => {
				if(item.id == svccall.assignedto) {
					infocontent += `<div style="display: inline-block; background-color: #e4e4e4; margin-bottom: 4px; padding: 2px;"><span>${timeFilter(svccall.start)}</span> - <span>${timeFilter(svccall.end)}</span></div>`
				}
			});
		}

		return infocontent;
	}

	initializeMap () {
		this.updateLoaderFlag(true);

		let { bounds, map, markers } = this.state;

		let boundLatLng = {};

		let servicecallIcon = MapProps.getIcons().servicecallMarkerIcon;
		let engineerIcon = MapProps.getIcons().engineerMarkerIcon;

		this.state.sc_engArray.forEach((item, i) => {

			if(!markers[`${item.markerType}_${item.id}`]) {
				markers[`${item.markerType}_${item.id}`] = {};
				markers[`${item.markerType}_${item.id}`].marker = new google.maps.Marker({
					position: item.position,
					map: map,
					titleinfo : this.renderInfoWindowFn(item),
					animation: google.maps.Animation.DROP,
					icon : item.markerType == 'servicecall' ? servicecallIcon : engineerIcon,
					label: ``,
					id : i
				});
			} else {
				markers[`${item.markerType}_${item.id}`].marker.titleinfo = this.renderInfoWindowFn(item);
				markers[`${item.markerType}_${item.id}`].marker.setPosition(item.position);
			}

			bounds.extend(markers[`${item.markerType}_${item.id}`].marker.position);

			this.markerListenerFn(markers[`${item.markerType}_${item.id}`], i, 'customerLocations');
		});

		if(this.state.firstTime)
			map.fitBounds(bounds);

		this.setState({ markers, firstTime: false });
	}

	markerListenerFn (markerObj, i, param) {
		let { map } = this.state;
		if(!markerObj.infoWindow) {
			markerObj.infoWindow = new google.maps.InfoWindow;
			/*google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
					console.log(infoWindow);
					infoWindow.setContent('<div>'+this.titleinfo+'</div>');
					infoWindow.open(map, this);
				}
			})(marker, i));*/

			google.maps.event.addListener(markerObj.infoWindow, 'domready', ()=> {
				var iwOuter = $('.gm-style-iw');
				iwOuter.addClass('gs-gm-style-iw');

				var iwOuterArrow = $('.gm-style-iw-t');
				iwOuterArrow.addClass('gs-gm-style-iw-t');

				var iwBackground = iwOuter.prev();
				var closeicon = iwOuter.next();

				iwBackground.children(':nth-child(2)').css({'display' : 'none'});
				var arrowEle = $('.gm-style-iw').prev().children(':nth-child(3)');

				arrowEle.css({'display' : 'none'});
				//arrowEle.children(':nth-child(1)').css({'width' : '13px'});
				//arrowEle.children(':nth-child(2)').css({'left' : '7px', 'width' : '13px', 'height' : '18px'});
				//arrowEle.children(':nth-child(2)').children(':nth-child(1)').css({'width' : '8px', 'height' : '26px'});
				iwBackground.children(':nth-child(1)').css({'display' : 'none'});

				iwBackground.children(':nth-child(4)').css({'display' : 'none'});
				closeicon.css({'display' : 'none'});
				$('button.gm-ui-hover-effect').css({'display' : 'none'});
			});
			if(this.props.resource.showdetails)
				markerObj.infoWindow.open(map, markerObj.marker);
		}

		markerObj.infoWindow.setContent('<div>'+markerObj.marker.titleinfo+'</div>');
	}

	showDetailsChange(value) {
		let { markers, map } = this.state;

		Object.keys(markers).forEach(key => {
			let markerObj = markers[key];

			if(value)
				markerObj.infoWindow.open(map, markerObj.marker);
			else
				markerObj.infoWindow.close();
		});
	}

	mapViewOnChange(value) {
		let { markers, map } = this.state;

		let viewmarkertypes = ['servicecall', 'engineer'];

		if(value == 'Calls Only')
			viewmarkertypes = ['servicecall'];
		if(value == 'Engineers Only')
			viewmarkertypes = ['engineer'];

		Object.keys(markers).forEach(key => {
			let markerObj = markers[key];

			let markertype = key.split('_')[0];

			if(viewmarkertypes.includes(markertype)) {
				markerObj.marker.setVisible(true);
				markerObj.infoWindow.open(map, markerObj.marker);
			} else {
				markerObj.marker.setVisible(false);
				markerObj.infoWindow.close();
			}
		});
	}

	render() {
		const mapStyle = {
			width: '100%',
			height: $(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight()  - 20
		};

		return(
			<div className="row no-gutters">
				{ (!this.props.app.feature.licensedTrackingUserCount) ? <div className="col-md-12 col-sm-12 col-xs-12 margintop-50">
					<div className="col-md-8 offset-md-2 col-sm-12 col-xs-12 alert alert-warning text-center">You have no access to view map details !!</div>
				</div> : null}

				<div className="col-md-12">
					<div className="svceng_assign_rpt_map_floating_panel">
						<div style={{position: 'relative', display: 'flex', alignItems: 'center', padding: '5px', backgroundColor: '#fff', marginRight: '20px'}}>
							<span className="marginleft-10 marginright-10">Show on Map</span>
							<div style={{width: '200px'}}>
								<Field name={'mapviewfilter'} props={{options: ["Calls and Engineers", "Calls Only", "Engineers Only"], onChange: this.mapViewOnChange, required: true}} component={localSelectEle} validate={[stringNewValidation({title : 'mapviewfilter', required: true})]} />
							</div>
						</div>
						<div style={{position: 'relative', display: 'flex', alignItems: 'center', padding: '5px', backgroundColor: '#fff'}}>
							<span className="mr-3 ml-3">Show Details</span>
							<Field name={'showdetails'} classname="mb-0" props={{onChange: this.showDetailsChange}} component={checkboxEle} />
						</div>
					</div>
				</div>
				<div className="col-md-12" style={{paddingLeft: '15px'}}>
					<div id="map" style={mapStyle}></div>
				</div>
			</div>
		);
	};
};