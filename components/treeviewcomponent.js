import React, { Component } from 'react';
import { currencyFilter, dateFilter} from '../utils/filter';

export default class TreeViewComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.onLoad = this.onLoad.bind(this);
		this.toggleRow = this.toggleRow.bind(this);
		this.renderGroupHeaderRow = this.renderGroupHeaderRow.bind(this);
		this.renderGroupBodyRow = this.renderGroupBodyRow.bind(this);
		this.listbodyRef = this.listbodyRef.bind(this);
		this.renderTbody = this.renderTbody.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let treeviewdata = [];
		let displayIndex = 0;
		this.props.data.forEach((item, index) => {
			displayIndex++;
			item.displayIndent = 0;
			item.index = displayIndex;
			item.haschild = item.children && item.children.length > 0 ? true : false;
			treeviewdata.push(item);

			if(item.children && item.children.length > 0) {
				recursivefn(item);
			}
		});

		function recursivefn(item) {
			item.children.forEach((childitem, childindex) => {
				displayIndex++;
				childitem.displayIndent = item.displayIndent + 1;
				childitem.rootindex = item.index;
				childitem.index = displayIndex;
				childitem.haschild = childitem.children && childitem.children.length > 0 ? true : false;
				treeviewdata.push(childitem);

				if(childitem.children && childitem.children.length > 0) {
					recursivefn(childitem);
				}
			});
		}
		this.setState({ treeviewdata });

		this.props.updateFormState(this.props.form, {
			flatTreeViewData: treeviewdata
		});
	}

	toggleRow(item) {
		let treeviewdata = [...this.state.treeviewdata];

		item.toggled = item.toggled ? false : true;

		recursiveHideFn(item.index);

		function recursiveHideFn (index) {
			for (var i = 0; i < treeviewdata.length; i++) {
				if (index == treeviewdata[i].rootindex) {
					treeviewdata[i].hideRow = item.toggled;
					recursiveHideFn(treeviewdata[i].index)
				}
			}
		}

		this.setState({ treeviewdata });
	}

	renderGroupHeaderRow() {
		let thclassName = this.props.groupArray.length == 0 ? 'groupthname hide' : 'groupthname';
		return (
			<thead>
				<tr className="parentRow">
					{this.props.groupArray.map((group, keyindex) => {
						if(group.displayname)
							return (
								<td className="text-center align-middle" key={keyindex} style={{width: '175px'}}>{group.displayname}</td>
							);
						if(!group.min && group.max)
							return (
								<td className={`${thclassName} text-center align-middle`} key={keyindex} style={{width: '175px'}}>Till {dateFilter(group.max)}</td>
							);
						return (
							<td className={`${thclassName} text-center align-middle`} key={keyindex} style={{width: '175px'}}>{dateFilter(group.min)}<br />to<br />{dateFilter(group.max)}</td>
						);
					})}
					{this.props.hideCumulative ? null : <td className={`${thclassName} text-center align-middle`} style={{width: '175px'}}>Cumulative</td>}
				</tr>
			</thead>
		);
	}

	renderGroupBodyRow(item) {
		if(this.props.groupArray.length == 0)
			return null;

		return this.props.groupArray.map((group, keyindex) => {
			return (
				<td className={`text-right ${item.haschild ? '' : 'gs-tree-hover-td'}`} key={keyindex} style={{width: '175px'}} onClick={() => { item.clickfn(item, group) }}>{item.groupObj[group.name].balance >= 0 ? currencyFilter(item.groupObj[group.name].balance, this.props.app.defaultCurrency, this.props.app) : `(${currencyFilter((-1 * item.groupObj[group.name].balance), this.props.app.defaultCurrency, this.props.app)})`}</td>
			);
		});
	}

	listbodyRef(node) {
		if(node) {
			node.addEventListener('scroll', (evt) => {
				this.refs.listheader.scrollLeft = evt.srcElement.scrollLeft;
				this.refs.listfirstbody.scrollTop = evt.srcElement.scrollTop;
			})
		}
	}

	renderTbody(param) {
		return this.state.treeviewdata.map((item, keyindex) => {
			return (
				<tr key={keyindex} className={`${item.hideRow ? 'hide' : 'treerow-active'} ${item.displayIndent == 0 ? 'parentRow' : ''}`} style={{marginBottom: param && keyindex == this.state.treeviewdata.length - 1 ? '13px' : null}}>
					{param ? <td style={{paddingLeft: `${item.displayIndent > 0 ? 27 * item.displayIndent : 12}px`, paddingRight:'2px'}} className={`${item.haschild ? 'gs-tree-hover-td' : 'gs-tree-hover-td'}`} onClick={() => {item.haschild ? this.toggleRow(item) : item.clickfn(item)}}>
						{item.haschild ? <div className="float-left pr-2" style={{width: '5%'}}>
							<a style={{color: '#848484'}}>
								<i className={`fa fa-${item.toggled ? 'caret-right' : 'caret-down'}`}></i>
							</a>
						</div> : null}
						<div className="float-left" style={{width: `93%`}}>{item.displayname}</div>
					</td> : null}
					{param ? null : this.renderGroupBodyRow(item)}
					{this.props.hideCumulative || param ? null : <td className={`text-right ${item.haschild ? '' : 'gs-tree-hover-td'}`} style={{width: '175px'}} onClick={() => { item.clickfn(item) }}>{item.balance >= 0 ?currencyFilter(item.balance, this.props.app.defaultCurrency, this.props.app) : `(${currencyFilter((-1 * item.balance), this.props.app.defaultCurrency, this.props.app)})`}</td>}
				</tr>
			)
		})
	}

	render() {
		let tbody_div_style = {
			//width: `100%`
		};
		if($(window).outerHeight() - $('.report-header').outerHeight() - $('.navbar').outerHeight() - $('.gs-tree-table-header-container').outerHeight() - 44) {
			//tbody_div_style.height = `${$(window).outerHeight() - $('.report-header').outerHeight() - $('.navbar').outerHeight() - $('.gs-tree-table-header-container').outerHeight() - 44}px`;
			tbody_div_style.maxHeight = `${$(window).outerHeight() - $('.report-header').outerHeight() - $('.navbar').outerHeight() - $('.gs-tree-table-header-container').outerHeight() - 44}px`;
		}

		let thclassName = this.props.groupArray.length == 0 ? 'align-middle hide' : 'align-middle';

		return (
			<div>
				<div className="gs-tree-parent-table-div">
					<div className={`gs-tree-table-header-first-container`} ref="listheader" style={{maxWidth: '40%'}}>
						<table className={`table table-sm gs-table-sm gs-tree-table gs-tree-table-header`} style={{marginBottom:0, border: '0px'}}>
							<thead>
								<tr className="parentRow">
									<td className={thclassName} style={{height: `${$(".groupthname").outerHeight() - 3}px`}}><span style={{marginLeft: '18px'}}>Account</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div className={`gs-tree-table-header-container`} ref="listheader" style={{maxWidth: '60%'}}>
						<table className={`table table-sm gs-table-sm gs-tree-table gs-tree-table-header`} style={{marginBottom:0, border: '0px'}}>
							{this.renderGroupHeaderRow()}
						</table>
					</div>
				</div>
				<div className="gs-tree-parent-table-div">
					<div className="gs-tree-table-body-first-container gs-scrollbar bg-white" ref="listfirstbody" style={tbody_div_style}>
						<table className="table table-bordered table-sm gs-table-sm gs-tree-table gs-tree-table-body" style={tbody_div_style}>
							<tbody>
								{this.renderTbody(true)}
							</tbody>
						</table>
					</div>
					<div className="gs-tree-table-body-container gs-scrollbar bg-white" ref={this.listbodyRef} style={tbody_div_style}>
						<table className="table table-bordered table-sm gs-table-sm gs-tree-table gs-tree-table-body">
							<tbody>
								{this.renderTbody(false)}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);	
	}
}