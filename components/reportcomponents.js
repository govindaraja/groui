import React, { Component } from 'react';
import { connect } from 'react-redux';
import './create-class';
import './prop-types';
import ReactDataGrid from 'react-data-grid';

import { LocalSelect, DateElement, NumberElement } from '../components/utilcomponents';
import Loadingcontainer from '../components/loadingcontainer';
import { currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter, discountFilter, numberFilter, timeDurationFilter, multiSelectAutoFilter, itemmasterDisplaynamefilter } from '../utils/filter';
import { search } from '../utils/utils';
import { Field } from 'redux-form';
import { checkboxEle } from '../components/formelements';
import { v1 as uuidv1 } from 'uuid';
import { generateExcel } from '../utils/services';
import UAParser from 'ua-parser-js';

var parser = new UAParser();

export class Reactuigrid extends Component {
	constructor(props) {
		super(props);
		this.state = {
			forceRefresh: false,
			expanded: {},
			tempWindow: null
		};
		//this.handleFilterChange();

		this.getColumns = this.getColumns.bind(this);
		this.rowGetter = this.rowGetter.bind(this);
		this.getRowsCount = this.getRowsCount.bind(this);
		this.onClearFilters = this.onClearFilters.bind(this);
		this.handleGridSort = this.handleGridSort.bind(this);
		this.handleFilterChange = this.handleFilterChange.bind(this);
		this.columnResize = this.columnResize.bind(this);
		this.aggregateValues = this.aggregateValues.bind(this);
		this.getVisibleRows = this.getVisibleRows.bind(this);
		this.columnFilterFunction = this.columnFilterFunction.bind(this);
		this.refresh = this.refresh.bind(this);
		this.checkCondition = this.checkCondition.bind(this);
		this.handleGridRowsUpdated = this.handleGridRowsUpdated.bind(this);
		this.onCellExpand = this.onCellExpand.bind(this);
		this.getSubRowDetails = this.getSubRowDetails.bind(this);
		this.updateSubRowDetails = this.updateSubRowDetails.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
		this.sortRows = this.sortRows.bind(this);
		this.showSelectedItemsCount = this.showSelectedItemsCount.bind(this);

		this.mapView = this.mapView.bind(this);
	}

	componentWillReceiveProps(nextProps, prevProps) {
		if(nextProps.gridprops.originalRows && (!this.props.gridprops || !this.props.gridprops.originalRows || (this.props.gridprops.originalRows.length != nextProps.gridprops.originalRows.length))) {
			this.handleFilterChange();
		}
	}

	handleGridSort (sortColumn, sortDirection) {
		let sortObj = Object.assign({}, this.props.gridprops.sortObj);
		if(sortDirection == 'NONE')
			delete sortObj[sortColumn];
		else
			sortObj[sortColumn] = sortDirection;

		this.props.updateFormState(this.props.form, {
			sortObj: sortObj
		});
		this.props.updateReportFilter(this.props.form, {
			sortObj: sortObj
		});
	}

	onClearFilters () {
		this.props.updateFormState(this.props.form, {
			filters: {}
		});
	}

	getVisibleRows() {
		return search(this.props.gridprops.originalRows || [], this.props.gridprops.filterObj);
	}

	checkCondition(condition, type, item) {
		let app = this.props.app;
		let feature = this.props.app.feature;
		let user = this.props.app.user;
		let resource = this.props.gridprops;
		let report = this.props.report;
		if(type == 'if')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 || (['string', 'boolean'].indexOf(typeof(condition)) >= 0 && eval(`try{${condition}}catch(e){}`));
		if(type == 'disabled')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);
		if(type == 'value') {
			if(typeof(condition) == 'string') {
				if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
					return eval(`try{${condition}}catch(e){}`);	
				else
					return condition;
			}
			return condition;
		}
	}

	refresh() {
		setTimeout(() => {
			this.handleFilterChange({
				filterTerm: uuidv1(),
				column: {
					key: '_dontcheck'
				}
			});
		}, 0);
	}

	forceRefresh() {
		this.setState({
			forceRefresh: true
		});
		setTimeout(() => {
			this.setState({
				forceRefresh: false
			});
		}, 0);
	}

	handleFilterChange (filter) {
		let newFilters = Object.assign({}, this.props.gridprops.filters);
		let newFilterObj = Object.assign({}, this.props.gridprops.filterObj);
		if(filter) {
			if (filter.filterTerm)
				newFilters[filter.column.key] = {filterTerm : filter.filterTerm};
			else
				delete newFilters[filter.column.key];
		}
		if(filter) {
			if (filter.filterTerm)
				newFilterObj[filter.column.key] = filter.filterTerm;
			else
				delete newFilterObj[filter.column.key];
		}

		this.props.updateFormState(this.props.form, {
			filters: newFilters,
			filterObj: newFilterObj,
			//columns: this.aggregateValues()
		});
		this.props.updateReportFilter(this.props.form, {
			filters: newFilters,
			filterObj: newFilterObj,
		});
	}

	aggregateValues() {
		let { columns } = this.props.gridprops;
		var filteredRows = search(this.props.gridprops.originalRows || [], this.props.gridprops.filterObj);

		filteredRows.forEach((row, index) => {
			columns.forEach((col) => {
				if(['sum'].indexOf(col.footertype) >= 0) {
					if(index == 0)
						col.footerAggValue = 0;
					if(col.footertype == 'sum' && typeof(row[col.key]) == 'number') {
						col.footerAggValue += row[col.key];
						col.footerCurrencyid = row.currencyid;
					}
				}
			});
		});
		return columns;
	}

	getColumns() {
		let { columns } = this.props.gridprops;
		var filteredRows = search(this.props.gridprops.originalRows || [], this.props.gridprops.filterObj);
		let columnArray = columns.filter(this.columnFilterFunction);
		if(this.state.forceRefresh)
			columnArray.push({
				key: '_temp',
				width: 20
			});
		return columnArray.map((a) => {
			let tempObj = {
				sortable: false
			};
			if(a.format) {
				switch (a.format) {
					case 'anchortag':
						tempObj.formatter = <AnchortagFormatter transactionname={a.transactionname} transactionid={a.transactionid} openTransaction= {this.props.openTransaction}/>;
						break;
					case 'currency':
						tempObj.formatter = <CurrencyFormatter />
						break;
					case 'defaultcurrency':
						tempObj.formatter = <DefaultCurrencyFormatter />
						break;
					case 'date':
						tempObj.formatter = <DateFormatter />
						break;
					case 'datetime':
						tempObj.formatter = <DateTimeFormatter />
						break;
					case 'time':
						tempObj.formatter = <TimeFormatter />
						break;
					case 'boolean':
						tempObj.formatter = <BooleanFormatter />
						break;
					case 'discount':
						tempObj.formatter = <DiscountFormatter />
						break;
					case 'number':
						tempObj.formatter = <NumberFormatter />
						break;
					case 'capacityfield':
						tempObj.formatter = <CapacityFormatter />
						break;
					case 'itemmasterdisplayname':
						tempObj.formatter = <itemmasterDisplaynameFormatter />
						break;
					case 'useragent':
						tempObj.formatter = <UserAgentFormatter />
						break;
					case 'button':
						tempObj.formatter = <ButtonFormatter showfield={a.showfield} btnname={a.buttonname} onClick={a.onClick} disabled={a.disabled} checkCondition={this.checkCondition} />
						break;
					case 'select':
						tempObj.formatter = <SelectFormatter options={a.selectoptions} onChange={a.onChange} disabled={a.disabled} checkCondition={this.checkCondition} />
						break;
					case 'dateinput':
						tempObj.formatter = <DateInputFormatter onChange={a.onChange} disabled={a.disabled} checkCondition={this.checkCondition} />
						break;
					case 'textinput':
						tempObj.formatter = <TextInputFormatter onChange={a.onChange} disabled={a.disabled} checkCondition={this.checkCondition} />
						break;
					case 'numberinput':
						tempObj.formatter = <NumberInputFormatter onChange={a.onChange} disabled={a.disabled} checkCondition={this.checkCondition} />
						break;
					case 'checkbox':
						let onChange = this.checkCondition(a.onChange, 'value');
						tempObj.formatter = <CheckboxFormatter showfield={a.showfield} gridprops={this.props.gridprops} onChange={onChange} checkCondition={this.checkCondition} />
						break;
					case 'taxFilter':
						tempObj.formatter = <TaxFormatter />;
						break;
					case 'multiSelectAutoFilter':
						tempObj.formatter = <MultiSelectAutoFormatter />
						break;
					case 'timeDuration':
						tempObj.formatter = <TimeDurationFormatter />
						break;
					case 'location':
						tempObj.formatter = <LocationFormatter mapView={this.mapView} showfield={a.showfield} fieldname={a.fieldname} fieldformat={a.fieldformat} locationobj={a.locationobj} disabled={a.disabled} checkCondition={this.checkCondition} />
						break;
					case 'array':
						tempObj.formatter = <ArrayFormatter />
						break;
					default:
						tempObj.formatter = <TextFormatter />
				}
			} else {
				tempObj.formatter = <TextFormatter />
			}
			let headerFormatter = a.headerformat ? (a.headerformat == 'checkbox' ? <HeaderCheckBox filteredRows = {filteredRows} checkboxHeaderOnChange={this.props.checkboxHeaderOnChange} /> : null) : <CustomHeaderCell column={{...a}} sortDirection={this.props.gridprops.sortObj ? (this.props.gridprops.sortObj[a.key] ? this.props.gridprops.sortObj[a.key] : 'NONE'): 'NONE'} onSort={this.handleGridSort} />;

			return {
				...a,
				headerRenderer: headerFormatter,
				filterable : this.isFilterableColumn(a),
				resizable : true,
				sortable: true,
				width: a.width > 0 ? a.width : 100,
				...tempObj,
				getRowMetaData: (row) => (row),
				filterRenderer: (props) => <CustomFilter column={props.column} initialValue={this.props.gridprops.filters && this.props.gridprops.filters[props.column.key] ? this.props.gridprops.filters[props.column.key].filterTerm: ''} onChange={(value) => props.onChange({filterTerm: value, column: props.column})} />
			}
		});
	}

	isFilterableColumn(col) {
		if(col.fieldformat == 'date' || col.fieldformat == 'datetime' || col.fieldformat == 'time')
			return false;

		if(col.format)
			return ['date', 'datetime', 'button', 'checkbox', 'time', 'dateinput'].indexOf(col.format) >= 0 ? false : true;
		return true;
	}

	rowGetter (index) {
		let rows = search(this.props.gridprops.originalRows || [], this.props.gridprops.filterObj);
		rows = this.sortRows(rows);
		if(rows.length == 0)
			rows.push({
				'_noResultFound' : true
			});

		return rows[index];
	}

	sortRows(rows) {
		let startNoSortArray = [];
		let endNoSortArray = [];
		let notItemFound = 0;
		for(var i=0;i<rows.length;i++) {
			if(!rows[i]['_restrictFromSorting']) {
				break;
			} else {
				notItemFound++;
				startNoSortArray.push(rows[i]);
			}
		}
		if(notItemFound > 0)
			rows.splice(0, notItemFound);

		notItemFound = 0;
		for(var i=rows.length-1; i>-1; i--) {
			if(!rows[i]['_restrictFromSorting']) {
				break;
			} else {
				notItemFound++;
				endNoSortArray.push(rows[i]);
			}
		}
		if(notItemFound > -1)
			rows.splice(rows.length-notItemFound, notItemFound);

		if(this.props.gridprops.sortObj) {
			for(var prop in this.props.gridprops.sortObj) {
				rows = rows.sort((a, b) => {
					let aprop = a[prop] == null ? '' : a[prop];
					let bprop = b[prop] == null ? '' : b[prop];
					aprop = typeof(aprop) == 'string' ? aprop.toLowerCase() : aprop;
					bprop = typeof(bprop) == 'string' ? bprop.toLowerCase() : bprop;
					if(this.props.gridprops.sortObj[prop] == 'ASC')
						return aprop > bprop ? 1 : -1;
					else
						return aprop < bprop ? 1 : -1;
				});
			}
		}
		return [...startNoSortArray, ...rows, ...endNoSortArray];
	}

	getRowsCount() {
		let filteredResults = search(this.props.gridprops.originalRows || [], this.props.gridprops.filterObj);
		if(filteredResults.length == 0)
			filteredResults.push({});

		return filteredResults.length;
	}

	getFilteredRowsCount() {
		let totalRows = search(this.props.gridprops.originalRows || [], {}).length;
		let filteredRows = search(this.props.gridprops.originalRows || [], this.props.gridprops.filterObj).length;
		if(totalRows != filteredRows)
			return `${filteredRows} (filtered) of ${totalRows}`;

		return `${filteredRows}`;
	}

	showSelectedItemsCount() {
		let columnArray = this.getColumns();
		if(columnArray.length > 0 && columnArray[0].format == 'checkbox') {
			let rows = search(this.props.gridprops.originalRows || [], {});
			let selectedItemsCount = 0;
			rows.forEach((row) => {
				if(row[columnArray[0].key])
					selectedItemsCount++;
			});
			if(selectedItemsCount > 0)
				return <div style={{float:'left'}}>{`${selectedItemsCount} out of ${rows.length} entries selected`}</div>
		}
		return null;
	}

	columnFilterFunction(col) {
		return col.hidden !== true && col.if !== false;
	}

	columnResize(index, width) {
		let columns = JSON.parse(JSON.stringify([...this.props.gridprops.columns]));
		columns.forEach((col, index) => {
			col._originalIndex = index;
		});
		let columnArray = columns.filter(this.columnFilterFunction);
		let _originalIndex = columnArray[index]._originalIndex
		columns[_originalIndex].width = width;
		this.props.updateFormState(this.props.form, {
			columns: columns
		});
		this.props.updateReportFilter(this.props.form, {
			columns: columns
		});
	}

	handleGridRowsUpdated = ({ fromRow, toRow, updated }) => {
		var filteredRows = this.props.gridprops.originalRows;

		for (let i = fromRow; i <= toRow; i++) {
			let rowToUpdate = filteredRows[i];
			for(var prop in rowToUpdate) {
				if(updated[prop] && updated[prop] != rowToUpdate[prop]) {
					rowToUpdate[prop] = updated[prop];
				}
			}
			filteredRows[i] = rowToUpdate;
		}
		this.props.updateReportFilter(this.props.form, {
			originalRows: filteredRows
		});
	}

	getSubRowDetails = (rowItem) => {
		let isExpanded = this.state.expanded[rowItem.name] ? this.state.expanded[rowItem.name] : false;
		return {
			group: rowItem.children && rowItem.children.length > 0,
			expanded: isExpanded,
			children: rowItem.children,
			field: this.props.treeCellExpand,
			treeDepth: rowItem.treeDepth || 0,
			siblingIndex: rowItem.siblingIndex,
			numberSiblings: rowItem.numberSiblings
		};
	}

	onCellExpand = (args) => {
		let rows = this.props.gridprops.originalRows;
		let rowKey = args.rowData[this.props.treeCellExpand];
		let rowIndex = rows.indexOf(args.rowData);
		let subRows = args.expandArgs.children;


		let expanded = Object.assign({}, this.state.expanded);
		if (expanded && !expanded[rowKey]) {
			expanded[rowKey] = true;
			this.updateSubRowDetails(subRows, args.rowData.treeDepth);
			rows.splice(rowIndex + 1, 0, ...subRows);
		} else if (expanded[rowKey]) {
			expanded[rowKey] = false;
			rows.splice(rowIndex + 1, subRows.length);
		}

		this.setState({ expanded: expanded });
		this.props.updateReportFilter(this.props.form, {
			originalRows: rows
		});
	};

	updateSubRowDetails = (subRows, parentTreeDepth) => {
		let treeDepth = parentTreeDepth || 0;
		subRows.forEach((sr, i) => {
			sr.treeDepth = treeDepth + 1;
			sr.siblingIndex = i;
			sr.numberSiblings = subRows.length;
		});
	}

	getFooter() {
		//return null;
		let { columns } = this.props.gridprops;
		let columnArray = columns.filter(this.columnFilterFunction);
		let aggArray = [];
		columnArray.forEach((col, index) => {
			aggArray.push({});			
		});
		var filteredRows = search(this.props.gridprops.originalRows || [], this.props.gridprops.filterObj);

		filteredRows.forEach((row, index) => {
			columnArray.forEach((col, colindex) => {
				if(['sum'].indexOf(col.footertype) >= 0) {
					if(index == 0)
						aggArray[colindex].footerAggValue = 0;
					if(col.footertype == 'sum' && typeof(row[col.key]) == 'number' && !row['_restrictFromAggregation']) {
						aggArray[colindex].footerAggValue += row[col.key];
					}
				}

				if(col.footertype == 'custom')
					aggArray[colindex].footerAggValue = col.customfootervalue;
			});
		});
		columnArray.forEach((col, colindex) => {
			if(['calculated'].indexOf(col.footertype) >= 0) {
				let calculatedFn = this.checkCondition(col.footerCalculation, 'value');
				if(calculatedFn)
					aggArray[colindex].footerAggValue = calculatedFn(columnArray, aggArray);
			}
		});
		columnArray.forEach((col, colindex) => {
			if(['sum'].indexOf(col.footertype) >= 0) {
				if (col.footerformat && col.footerformat == 'timeDuration')
					aggArray[colindex].footerAggValue = timeDurationFilter(aggArray[colindex].footerAggValue);
				else {
					if(aggArray[colindex].footerAggValue !== null && typeof(aggArray[colindex].footerAggValue) == 'number' && aggArray[colindex].footerAggValue > 0)
						aggArray[colindex].footerAggValue = Number(aggArray[colindex].footerAggValue.toFixed(this.props.app.roundOffPrecision));
					aggArray[colindex].footerAggValue = currencyFilter(aggArray[colindex].footerAggValue, 'qty', this.props.app);
				}
			}
		});
		if($(".react-grid-HeaderRow").children().length > 0) {
			let headerArray = $(".react-grid-HeaderRow")[0].children[0].children;
			let returnArray = [];
			let lastWidth = 0;
			let leftWidth = 0;
			for(var i = 0; i < columnArray.length; i++) {
				if(!columnArray[i].locked) {
					let style = {
						height: $(headerArray[i]).height(),
						width: columnArray[i].width > 0 ? columnArray[i].width : 100,
						marginLeft: `${leftWidth}px`,
						float: 'left',
						padding: '8px',
						textAlign: 'right',
						overflow: 'hidden',
						whiteSpace: 'nowrap',
						textOverflow: 'ellipsis',
					};
					leftWidth = 0;
					//lastWidth += $(headerArray[i]).outerWidth();
					lastWidth += style.width;

					returnArray.push(
						<div key={i} className="border" style={style}>
							{['sum', 'custom', 'calculated'].indexOf(columnArray[i].footertype) >= 0 ? aggArray[i].footerAggValue : null}
						</div>
					);
				} else {
					leftWidth = leftWidth + (columnArray[i].width > 0 ? columnArray[i].width : 100);
				}
			}
			let lockedArray = [];
			leftWidth = 27;
			for(var i = 0; i < columnArray.length; i++) {
				if(columnArray[i].locked) {
					let style = {
						position: 'absolute',
						width: columnArray[i].width > 0 ? columnArray[i].width : 100,
						height: $(headerArray[i]).height(),
						left: `${leftWidth}px`,
						contain: 'layout',
						backgroundColor: '#f9f9f9',
						// height: $(headerArray[i]).height(),
						// width: columnArray[i].width > 0 ? columnArray[i].width : 100,
						// float: 'left',
						// padding: '8px',
						// textAlign: 'right'
					};
					leftWidth = leftWidth + style.width;
					//lastWidth += $(headerArray[i]).outerWidth();
					lastWidth += style.width;

					lockedArray.push(
						<div key={i} className="border" style={style}>
							{['sum', 'custom'].indexOf(columnArray[i].footertype) >= 0 ? aggArray[i].footerAggValue : null}
						</div>
					);
				}
			}
			returnArray = [...returnArray, ...lockedArray.reverse()];
			$("#footerInnerCont").width(lastWidth+ 'px');
			return returnArray;
		}
		setTimeout(() => {
			var target = $("#footercont")[0];
			$(".react-grid-Canvas").scroll(function() {
				//target.scrollTop = this.scrollTop;
				target.scrollLeft = this.scrollLeft;
			});
			var targeta = $(".react-grid-Canvas")[0];
			$("#footercont").scroll(function() {
				//targeta.scrollTop = this.scrollTop;
				targeta.scrollLeft = this.scrollLeft;
			});
		}, 1000);
		return null;
	}

	exportExcel () {
		let rows = search(this.props.gridprops.originalRows || [], this.props.gridprops.filterObj);
		rows = this.sortRows(rows);
		generateExcel(this.props.excelname, this.getColumns(), rows, this.props.app, () => {});
	}

	mapView (item) {
		let { tempWindow } = this.state;

		if (tempWindow) {
			tempWindow.close();
			tempWindow = null
		}

		let latlng = `${item.latitude},${item.longitude}`;

		tempWindow = window.open(`https://www.google.com/maps/?q=${latlng}`);

		tempWindow.focus();

		this.setState({
			tempWindow
		});
	}

	render() {
		let firstHeight, secondHeight;

		if(this.props.height > 0) {
			firstHeight = this.props.height - ($('.report-filtertag-container').outerHeight() || 0) - ($('#footeroutercont').outerHeight() || 0) - ($('#footerinfocont').outerHeight() || 0) - ($('.gs-report-pagination').outerHeight() || 0);
			secondHeight = this.props.height - ($('.report-filtertag-container').outerHeight() || 0) - ($('#footeroutercont').outerHeight() || 0) - ($('#footerinfocont').outerHeight() || 0);
		} else {
			firstHeight = $(window).outerHeight() - $('.navbar').outerHeight() - ($('.report-header').outerHeight() || 0) - ($('.report-filtertag-container').outerHeight() || 0) - ($('#footeroutercont').outerHeight() || 0) - ($('#footerinfocont').outerHeight() || 0) - ($('.gs-report-pagination').outerHeight() || 0);
			secondHeight = $(window).outerHeight() - $('.navbar').outerHeight() - ($('.report-header').outerHeight() || 0) - ($('.report-filtertag-container').outerHeight() || 0) - ($('#footeroutercont').outerHeight() || 0) - ($('#footerinfocont').outerHeight() || 0);
		}

		return (
			<div className="col-md-12 paddingright-0 paddingleft-26" style={{display: !this.props.gridprops.originalRows || this.props.gridprops.originalRows.length == 0 ? 'none' : ''}}>
				<Loadingcontainer isloading={this.state.forceRefresh}></Loadingcontainer>
				<div style={{width: '100%', height: `${firstHeight}px`}}>
				<ReactDataGrid
					ref="grid"
					enableCellSelect={true}
					onGridSort={this.handleGridSort}
					columns={this.getColumns()}
					rowGetter={this.rowGetter}
					rowsCount={this.getRowsCount()}
					cellNavigationMode="changeRow"
					toolbar={<CustomToolbar/>}
					onAddFilter={this.handleFilterChange}
					onClearFilters={this.onClearFilters}
					headerRowHeight={60}
					onColumnResize={this.columnResize}
					onGridRowsUpdated={this.handleGridRowsUpdated}
					onCellExpand={this.onCellExpand}
					getSubRowDetails={this.getSubRowDetails}
					minHeight={secondHeight} />
				</div>
				<div id="footeroutercont" className="border" style={{width: $(".react-grid-Container").outerWidth() ? $(".react-grid-Container").outerWidth() : '1410px', overflow: 'hidden'}}>
					<div id="footercont" style={{width: $(".react-grid-Canvas").outerWidth() ? $(".react-grid-Canvas").outerWidth() : '1410px', 'overflow': 'hidden', 'backgroundColor': '#f9f9f9'}}>
						<div id="footerInnerCont" style={{width: '1400px'}}>
							{this.getFooter()}
						</div>
					</div>
				</div>
				<div id="footerinfocont" className="border" style={{width: '100%', height: '45px', backgroundColor: '#f9f9f9', padding: '10px'}}>
					{this.showSelectedItemsCount()}
					<div className="pull-right">Total Entries : {this.getFilteredRowsCount()}</div>
				</div>	
			</div>
		);
	}

}


export const CurrencyFormatter = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let show_value = currencyFilter(this.props.value, this.props.dependentValues.currencyid, this.props.app);
		return (
			<div title={show_value}>{show_value}</div>
		);
	}
});

export const DefaultCurrencyFormatter = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let show_value = currencyFilter(this.props.value, this.props.app.defaultCurrency, this.props.app);
		return (
			<div title={show_value}>{show_value}</div>
		);
	}
});

export class UserAgentFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues.client == 'Growsmart') {
			var resultPar = parser.setUA(this.props.value).getResult();

			if(resultPar && resultPar.browser && !resultPar.browser.name)
				return this.props.value;

			let resultString = `${resultPar.browser.name} - ${resultPar.browser.major} at ${resultPar.os.name} - ${resultPar.os.version}`;
			// console.log(resultPar.getBrowser());
			// console.log(resultPar.getDevice());
			// console.log(resultPar.getOS());
			// console.log(resultPar.getResult());
			return resultString;
		}

		return this.props.value;
	}
};

export const DiscountFormatter = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let show_value = discountFilter(this.props.value, this.props.dependentValues.currencyid, this.props.dependentValues.discountmode, this.props.app);
		return (
			<div title={show_value}>{show_value}</div>
		);
	}
});

export class DateFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let show_value = dateFilter(this.props.value);
		return (
			<span title={show_value}>{show_value}</span>
		);
	}
}

export class DateTimeFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let show_value = datetimeFilter(this.props.value);
		return (
			<span title={show_value}>{show_value}</span>
		);
	}
}

export class TimeFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let show_value = timeFilter(this.props.value);
		return (
			<span title={show_value}>{show_value}</span>
		);
	}
}

export class TimeDurationFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if (this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);

		let show_value = timeDurationFilter(this.props.value);

		return (
			<span title={show_value}>{show_value}</span>
		);
	}
};

export class BooleanFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		return (
			<span>{booleanfilter(this.props.value)}</span>
		);
	}
}

export class TextFormatter extends Component {
	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		return (
			<span title={this.props.value}>{this.props.value}</span>
		);
	}
};

export const NumberFormatter = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let show_value = currencyFilter(this.props.value, 'qty', this.props.app);
		return (
			<span title={show_value}>{show_value}</span>
		);
	}
});

export class AnchortagFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		return (
			<span className="gs-anchor" titile={this.props.value} onClick={() => this.props.openTransaction(this.props.dependentValues, this.props.transactionname, this.props.transactionid)}>{this.props.value}</span>
		);
	}
}

export class ArrayFormatter extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		if (this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);

		let show_value = multiSelectAutoFilter(this.props.value);
		return (
			<span title={show_value}>{show_value}</span>
		);
	}
};

class CustomToolbar extends Component {
	componentDidMount() {
		this.props.onToggleFilter()
	}

	render() {
		return (<div></div>)
	}
}

export const TaxFormatter = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);

		let show_value = taxFilter(this.props.value, this.props.app.taxObject)
		return (
			<div title={show_value}>{show_value}</div>
		);
	}
});

export const CapacityFormatter = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let show_value = this.props.dependentValues.capacityfield && this.props.dependentValues.capacity ? itemmasterDisplaynamefilter(this.props.dependentValues.capacityfield, this.props.app) : '';
		return (
			<div title={show_value}>{this.props.value} {show_value}</div>
		);
	}
});

export const itemmasterDisplaynameFormatter = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let show_value = this.props.dependentValues.capacityfield && this.props.dependentValues.capacity ? itemmasterDisplaynamefilter(this.props.dependentValues.capacityfield, this.props.app) : '';
		return (
			<div title={show_value}>{show_value}</div>
		);
	}
});

export const MultiSelectAutoFormatter = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let show_value = multiSelectAutoFilter(this.props.value)
		return (
			<div title={show_value}>{show_value}</div>
		);
	}
});

class CustomFilter extends Component {
	constructor(props) {
		super(props);

		this.state = {};
		
	}

	render() {
		let inputKey = 'header-filter-' + this.props.column.key;
		return (
			<input type="text" className="form-control" value={this.state.value || this.props.initialValue} onChange={(e) => {this.setState({value: e.target.value});this.props.onChange(e.target.value)}} />
		)
	}
}

export class ButtonFormatter extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {

		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);

		let btnname = this.props.checkCondition(this.props.btnname, 'value', this.props.dependentValues);

		let onClick = this.props.checkCondition(this.props.onClick, 'value', this.props.dependentValues);

		let disabled = this.props.checkCondition(this.props.disabled, 'disabled', this.props.dependentValues);

		let showfield = this.props.checkCondition(this.props.showfield, 'if', this.props.dependentValues);

		if(!(!this.props.showfield || showfield))
			return null;

		return (
			<button type="button" className={`btn btn-sm ${this.props.column.buttonclass ? this.props.column.buttonclass : 'btn-info'}`} disabled={disabled} onClick={() => onClick(this.props.dependentValues)}>{btnname}</button>
		);
	}
}

export class SelectFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value : this.props.value
		};
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.value != this.state.value)
			this.setState({
				value: nextProps.value
			});
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);

		let options = this.props.checkCondition(this.props.options, 'value', this.props.dependentValues);

		let onChange = this.props.checkCondition(this.props.onChange, 'value', this.props.dependentValues);

		let disabled = this.props.checkCondition(this.props.disabled, 'disabled', this.props.dependentValues);

		let onChangeFun = (value) => {
			onChange(value, this.props.dependentValues);
		};

		return (
			<LocalSelect options={options} value={this.state.value} onChange={(value) => onChangeFun(value)} disabled={disabled} />
		);
	}
}

export class DateInputFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value : this.props.value
		};
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.value != this.state.value)
			this.setState({
				value: nextProps.value
			});
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);

		let onChange = this.props.checkCondition(this.props.onChange, 'value', this.props.dependentValues);

		let disabled = this.props.checkCondition(this.props.disabled, 'disabled', this.props.dependentValues);

		let onChangeFun = (value) => {
			onChange(value, this.props.dependentValues);
		};

		return (
			<DateElement className={`form-control`} value={this.state.value} onChange={(value) => onChangeFun(value)} disabled={disabled} />
		);
	}
}

export class NumberInputFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value : this.props.value
		};
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.value != this.state.value)
			this.setState({
				value: nextProps.value
			});
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);

		let onChange = this.props.checkCondition(this.props.onChange, 'value', this.props.dependentValues);

		let disabled = this.props.checkCondition(this.props.disabled, 'disabled', this.props.dependentValues);

		let onChangeFun = (value) => {
			onChange(value, this.props.dependentValues);
		};

		return (
			<NumberElement className={`form-control`} value={this.state.value} onChange={(value) => onChangeFun(value)} disabled={disabled} />
		);
	}
}

export class TextInputFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value : this.props.value
		};
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.value != this.state.value)
			this.setState({
				value: nextProps.value
			});
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);

		let onChange = this.props.checkCondition(this.props.onChange, 'value', this.props.dependentValues);

		let disabled = this.props.checkCondition(this.props.disabled, 'disabled', this.props.dependentValues);

		let onChangeFun = (value) => {
			onChange(value, this.props.dependentValues);
		};

		return (
			<input type="text" className={`form-control`} value={this.state.value ? this.state.value : ''} onChange={(evt) => onChangeFun(evt.target.value)} disabled={disabled} />
		);
	}
}

class CheckboxFormatter extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value : this.props.value
		};
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.value != this.state.value) {
			this.setState({
				value: nextProps.value
			});
		}
	}

	render() {
		if(this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);
		let item = this.props.gridprops.originalRows[this.props.rowIdx];
		let showfield = this.props.checkCondition(this.props.showfield, 'if', this.props.dependentValues);
		let onChangeFun = (event) => {
			this.props.onChange(event.target.checked, this.props.dependentValues);
		};

		return (
			<div>
				{!this.props.showfield || showfield ? 
					<input type="checkbox" className={`form-check-input`} checked={this.state.value} onChange={onChangeFun} /> : null}
			</div>
		)
	}
}

class CustomRowFormatter extends Component {
	render() {
		let usefulPropertyFromGrid = this.props.propertyToPassToCellAndRow;
		return <ReactDataGrid.Row {...this.props} forceUpdate={true} cellRenderer={CellRenderer} />;
	}
}

class CellRenderer extends Component {
	render() {
		return <ReactDataGrid.Cell {...this.props} forceUpdate={true} />;
	}
}


export class HeaderCheckBox extends Component {
	render() {
		return (
			<Field name={`isallchecked`} props={{onChange: (value)=>{this.props.checkboxHeaderOnChange(value, this.props.filteredRows)}}} component={checkboxEle} />
		)
	}
}


export class CustomHeaderCell extends Component {
	constructor(props) {
		super(props);

		this.state = {
			sortDirection: this.props.sortDirection
		};

		this.onClick = this.onClick.bind(this);
	}

	onClick() {
		let sortDirection = this.state.sortDirection == 'NONE' ? 'ASC' : (this.state.sortDirection == 'ASC' ? 'DESC' : 'NONE');
		this.setState({
			sortDirection
		});
		this.props.onSort(this.props.column.key, sortDirection);
	}

	render() {
		return (
			<div className="react-grid-HeaderCell-sortable react-grid-HeaderCell-sortable--ascending" style={{cursor: 'pointer'}} onClick={this.onClick}>
				{this.state.sortDirection == 'ASC' ? <span className="pull-right">▲</span> : null}
				{this.state.sortDirection == 'DESC' ? <span className="pull-right">▼</span> : null}
				{this.props.column.name}
			</div>
		)
	}
}

export class LocationFormatter extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		if (this.props.dependentValues._noResultFound)
			return (
				<span>No result Found</span>
			);

		let fieldname = this.props.checkCondition(this.props.fieldname, 'value', this.props.dependentValues);

		let locationobj = this.props.checkCondition(this.props.locationobj, 'value', this.props.dependentValues);

		let disabled = this.props.checkCondition(this.props.disabled, 'disabled', this.props.dependentValues);

		let showfield = this.props.checkCondition(this.props.showfield, 'if', this.props.dependentValues);

		let show_value = '';

		if (fieldname && this.props.dependentValues[fieldname]) {
			if (this.props.fieldformat == 'datetime')
				show_value = datetimeFilter(this.props.dependentValues[fieldname]);

			if (this.props.fieldformat == 'time')
				show_value = timeFilter(this.props.dependentValues[fieldname]);
		}

		let loc = this.props.dependentValues[locationobj];

		return (
			<div>{show_value}
				{(!this.props.showfield || showfield) && loc ? <button
					type = 'button'
					className = 'btn btn-sm btn-outline-dark'
					disabled = {disabled}
					style = {{float: 'right'}}
					onClick = {()=> {this.props.mapView(loc)}}
				>
					<span className = 'fa fa-globe'></span>
				</button>: null}
			</div>
		);
	}
}