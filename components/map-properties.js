export default {
	MapOptions: {
		streetViewControl: false,
		mapTypeControl: false,
		styles: [
			{
				"featureType": "road.highway",
				"elementType": "geometry.fill",
				"stylers": [{
					"color": "#ffffff"
				}]
			}, {
				"featureType": "road.arterial",
				"elementType": "labels",
				"stylers": [{
					"visibility": "on"
				}]
			}, {
				"featureType": "road.highway",
				"elementType": "labels",
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "road.local",
				"elementType": "labels",
				"stylers": [{
					"visibility": "on"
				}]
			}, {
				"featureType": "transit.station.airport",
				"elementType": "labels",
				"stylers": [{
					"saturation": -100
				}]
			}, {
				"featureType": "transit.station.bus",
				"elementType": "labels",
				"stylers": [{
					"visibility": "on"
				}]
			}, {
				"featureType": "transit.station.bus",
				"elementType": "labels.icon",
				"stylers": [{
					"saturation": -95
				}]
			}, {
				"featureType": "transit.station.rail",
				"elementType": "labels",
				"stylers": [{
					"visibility": "on"
				}]
			}, {
				"featureType": "transit.station.rail",
				"elementType": "labels.icon",
				"stylers": [{
					"saturation": -95
				}, {
					"lightness": -15
				}]
			}, {
				"featureType": "transit.station.rail",
				"elementType": "labels.text.fill",
				"stylers": [{
					"saturation": -100
				}]
			}, {
				"featureType": "poi",
				"stylers": [{
					"saturation": -100
				}]
			}, {
				"featureType": "poi.attraction",
				"stylers": [{
					"saturation": -100
				}]
			}, {
				"featureType": "poi.attraction",
				"elementType": "geometry.fill",
				"stylers": [{
					"saturation": -100
				}, {
					"lightness": -15
				}]
			}, {
				"featureType": "poi.attraction",
				"elementType": "labels.icon",
				"stylers": [{
					"saturation": -100
				}, {
					"lightness": -5
				}]
			}, {
				"featureType": "poi.business",
				"stylers": [{
					"visibility": "on"
				}]
			}, {
				"featureType": "poi.business",
				"elementType": "labels.icon",
				"stylers": [{
					"saturation": -100
				}]
			}, {
				"featureType": "poi.government",
				"elementType": "labels.icon",
				"stylers": [{
					"saturation": -100
				}]
			}, {
				"featureType": "poi.medical",
				"stylers": [{
					"saturation": -100
				}]
			}, {
				"featureType": "poi.medical",
				"elementType": "labels.icon",
				"stylers": [{
					"lightness": -5
				}]
			}, {
				"featureType": "poi.park",
				"elementType": "labels.icon",
				"stylers": [{
					"saturation": -100
				}, {
					"lightness": 65
				}]
			}, {
				"featureType": "poi.place_of_worship",
				"elementType": "labels.icon",
				"stylers": [{
					"saturation": -100
				}]
			}, {
				"featureType": "poi.school",
				"elementType": "labels.icon",
				"stylers": [{
					"saturation": -100
				}]
			}, {
				"featureType": "poi.sports_complex",
				"elementType": "labels.icon",
				"stylers": [{
					"saturation": -100
				}]
			}
		]
	},
	getIcons: () => {
		return {
			customerDetailsIcon : {
				url : '/images/clustermarker.png',
				scaledSize : new google.maps.Size(75, 25),
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(35, 9)
			},
			customerIcon : {
				url : '/images/customermarker.png',
				scaledSize : new google.maps.Size(22, 30),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(16, 11)
			},
			salespersonIcon : {
				url : '/images/salespersonmarker.png',
				scaledSize : new google.maps.Size(22, 30),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(16, 11)
			},
			openSVCIcon : {
				url : '/images/mapmarker-red.png',
				scaledSize : new google.maps.Size(22, 30),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(16, 11)
			},
			assignedSVCIcon : {
				url : '/images/assignedcall.png',
				scaledSize : new google.maps.Size(22, 30),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(16, 11)
			},
			violetIcon : {
				url : '/images/mapmarker-violet.png',
				scaledSize : new google.maps.Size(22, 30),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(16, 11)
			},
			dcIcon : {
				url : '/images/dcmarker.png',
				scaledSize : new google.maps.Size(22, 30),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(16, 11)
			},
			dcRouteIcon : {
				url : '/images/labelmarker.png',//
				scaledSize : new google.maps.Size(20, 20),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(10, 10)
			},
			dcRouteStartIcon : {
				url : '/images/dispatchstart.png',//
				scaledSize : new google.maps.Size(25, 25),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(10, 10)
			},
			dcRouteEndIcon : {
				url : '/images/dispatchend.png',//
				scaledSize : new google.maps.Size(25, 25),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(10, 10)
			},
			attendanceIcon : {
				url : '/images/mapmarker-blue.png',
				scaledSize : new google.maps.Size(22, 30),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker',
				labelOrigin: new google.maps.Point(16, 11)
			},
			checkInIcon : {
				url : '/images/checkin.png',
				scaledSize : new google.maps.Size(25, 25),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker',
				labelOrigin: new google.maps.Point(12, 12)
			},
			checkOutIcon : {
				url : '/images/checkout.png',
				scaledSize : new google.maps.Size(25, 25),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker',
				labelOrigin: new google.maps.Point(12, 12)
			},
			attendanceCheckInIcon : {
				url : '/images/attendancecheckin.png',
				scaledSize : new google.maps.Size(105, 45),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(5,45),
				className : 'mapMarker',
				labelOrigin: new google.maps.Point(63, 23)
			},
			attendanceCheckOutIcon : {
				url : '/images/attendancecheckout.png',
				scaledSize : new google.maps.Size(105, 45),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(110, 35),
				className : 'mapMarker',
				labelOrigin: new google.maps.Point(43, 23)
			},
			servicecallMarkerIcon : {
				url : '/images/servicecall-marker-icon.svg',
				scaledSize : new google.maps.Size(32, 30),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(16, 11)
			},
			engineerMarkerIcon : {
				url : '/images/engineer-marker-icon.svg',
				scaledSize : new google.maps.Size(32, 30),
				//fillOpacity: 0.8,
				origin : new google.maps.Point(0,0),
				anchor : new google.maps.Point(0, 0),
				className : 'mapMarker hoverClass',
				labelOrigin: new google.maps.Point(16, 11)
			}
		}
	}
}
