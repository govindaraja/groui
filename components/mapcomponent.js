import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import { updateFormState, openModal } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation, search, sort_by, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter, currencyFilter, timeFilter, datetimeFilter, dateAgoFilter } from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import async from 'async';
import { SelectAsync, LocalSelect } from '../components/utilcomponents';
import MapProps from '../components/map-properties';
import RenderInfoWindow from '../components/details/mapinfowindow';
import DetailedInfoWindow from '../components/details/mapdetailsview';

export default class MapForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isSummaryActive: true,
			showEmployees: this.props.rptparam == 'servicecallallotmentreport' ? true : false,
			showAssignedCalls: this.props.rptparam == 'servicecallallotmentreport' ? true : false,
			reference_id: null,
			markers : [],
			customermarkers : [],
			availableMarkers : [],
			AssignedSVCMarkers : [],
			AssignedCalls : [],
			waypoints : [],
			directionDisplayArray : [],
			directionsResults: [],
			locationdate : new Date(new Date().setHours(0, 0, 0, 0)),
			customerLocations: [],
			employeeLocations : [],
			errMsg : "",
			boundLatLng: {},
			search: {
				name: '',
				customer: '',
				customerid_name: ''
			},
			tabNames: {
				'leaditemsreport': {
					'summary': 'Summary',
					'details': 'Lead Details',
					'resourceName': 'leads'
				},
				'collectionstatusreport': {
					'summary': 'Summary',
					'details': 'Customer Details',
					'resourceName': 'collectionstatus'
				},
				'contractitemsreport': {
					'summary': 'Summary',
					'details': 'Contract Details',
					'resourceName': 'contracts'
				}
			}
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.initializeMap = this.initializeMap.bind(this);
		this.getAssignedCalls = this.getAssignedCalls.bind(this);
		this.markerClickFn = this.markerClickFn.bind(this);
		this.elementClickFn = this.elementClickFn.bind(this);
		this.updateShowDetailsFlag = this.updateShowDetailsFlag.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.tabOnChange = this.tabOnChange.bind(this);
		this.engineerCB = this.engineerCB.bind(this);
		this.defineAddresses = this.defineAddresses.bind(this);
		this.plotRoute = this.plotRoute.bind(this);
		this.routeMap = this.routeMap.bind(this);
		this.renderSearch = this.renderSearch.bind(this);
		this.renderTabView = this.renderTabView.bind(this);
		this.renderNormalView = this.renderNormalView.bind(this);
		this.markerListenerFn = this.markerListenerFn.bind(this);
		this.renderInfoWindowFn = this.renderInfoWindowFn.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let { customerLocations, availableMarkers, AssignedCalls, errMsg } = this.state;

		customerLocations = [];
		availableMarkers = [];
		AssignedCalls = [];

		for (let i = 0; i < this.props.latlngArray.length; i++) {
			if (this.props.latlngArray[i].latitude && this.props.latlngArray[i].longitude) {
				if(['leaditemsreport', 'deliveryrouteplannerreport', 'collectionstatusreport', 'contractitemsreport'].indexOf(this.props.rptparam) >= 0)
					customerLocations.push(this.props.latlngArray[i]);

				if (this.props.rptparam == 'servicecallallotmentreport') {
					if (this.props.latlngArray[i].status == 'Assigned')
						AssignedCalls.push(this.props.latlngArray[i])
					else
						customerLocations.push(this.props.latlngArray[i]);
				}
			}
		}

		if (['leaditemsreport', 'deliveryrouteplannerreport', 'collectionstatusreport', 'servicecallallotmentreport', 'contractitemsreport'].indexOf(this.props.rptparam) >= 0) {
			for (let i = 0; i < customerLocations.length; i++) {
				availableMarkers.push({
					position: {
						lat : customerLocations[i].latitude,
						lng : customerLocations[i].longitude
					},
					...customerLocations[i],
					indexVal: i
				})
			}
		}

		if(customerLocations.length == 0)
			errMsg = "No results found for your search!";

		setTimeout(() => {
			if(this.props.app.feature.licensedTrackingUserCount > 0)
				this.setState({
					map : new google.maps.Map(document.getElementById('map'),MapProps.MapOptions),
					bounds : new google.maps.LatLngBounds(),
					stepDisplay : new google.maps.InfoWindow,
					availableMarkers: [...availableMarkers],
					customerLocations: [...customerLocations],
					AssignedCalls: [...AssignedCalls],
					errMsg
				}, () => {
					if (this.state.showEmployees)
						this.getReportData();

					this.initializeMap();
				});
		}, 0);

		this.updateLoaderFlag(false);
	}

	tabOnChange = (param) => {
		let isSummaryActive = false, isDetailsActive = false;

		if (param == 'summary' && this.props.rptparam == 'deliveryrouteplannerreport') {
			isSummaryActive = false;
			isDetailsActive = true;
		}

		if (param == 'summary' && this.props.rptparam != 'deliveryrouteplannerreport')
			isSummaryActive = true;
		else if (param == 'details')
			isDetailsActive = true;

		this.setState({ isSummaryActive, isDetailsActive });
	};

	renderInfoWindowFn (item, param) {
		return RenderInfoWindow(item, param, this.props.rptparam, this.props.app);
	}

	getReportData () {
		this.updateLoaderFlag(true);

		let { markers, map, employeeLocations, showEmployees, bounds } = this.state;

		employeeLocations = [];

		markers.forEach((item) => {
			item.setMap(null);
		});

		markers = [];

		let errEmpMsg = "";

		if (showEmployees) {
			axios.get(`/api/query/employeelocationsquery?locationdate=${this.state.locationdate}`).then((response) => {
				if (response.data.message == 'success') {
					let tempEmployeeLocations = [];

					for (let i = 0; i < response.data.main.length; i++) {
						if (this.props.rptparam == 'servicecallallotmentreport') {
							if (response.data.main[i].latitude && response.data.main[i].longitude && response.data.main[i].isengineer)
								tempEmployeeLocations.push(response.data.main[i]);
						} else {
							if (response.data.main[i].latitude && response.data.main[i].longitude)
								tempEmployeeLocations.push(response.data.main[i]);
						}
					}

					if (this.props.app.user.roleid.indexOf(2) >= 0){
						for (let i = 0; i < tempEmployeeLocations.length; i++) {
							if (this.props.app.user.id == tempEmployeeLocations[i].userid) {
								employeeLocations.push(tempEmployeeLocations[i]);
								break;
							}
						}
					} else
						employeeLocations = [...tempEmployeeLocations];

					if(employeeLocations.length == 0) {
						if(this.props.app.user.roleid.indexOf(2) >= 0)
							errEmpMsg = "There is no location for this employee on this date";
						else
							errEmpMsg = "There is no result for show employees on this date";

						this.updateLoaderFlag(false);
						return this.props.openModal(modalService.infoMethod({
							header : "Error",
							body : errEmpMsg,
							btnArray : ["Ok"]
						}));
					} else {
						employeeLocations.forEach((item, i) => {
							let position = {
								lat : item.latitude,
								lng : item.longitude
							};

							let marker = new google.maps.Marker({
								position : position,
								map : map,
								titleinfo : this.renderInfoWindowFn(item, 'employeeLocations'),
								locationdate : new Date(item.locationdate).toLocaleString(),
								animation : google.maps.Animation.DROP,
								icon : MapProps.getIcons().salespersonIcon,
								id : i,
								isUser : true
							});

							markers.push(marker);

							bounds.extend(marker.position);

							this.markerListenerFn(marker, i, 'employeeLocations');
						});
					}

					this.setState({
						markers,
						employeeLocations
					});
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}

				this.updateLoaderFlag(false);
			});
		} else {
			this.updateLoaderFlag(false);

			this.setState({
				markers,
				employeeLocations
			});
		}
	}

	markerClickFn (marker) {
		let { reference_id } = this.state;

		if (['servicecallallotmentreport', 'deliveryrouteplannerreport'].indexOf(this.props.rptparam) >= 0)
			this.checkboxOnChange(true, marker.id);
		else
			this.setState({
				reference_id: marker.reference_id,
				isSummaryActive: false,
				isDetailsActive: true
			});
	}

	getAssignedCalls () {
		let { AssignedSVCMarkers, map, showAssignedCalls, bounds, AssignedCalls, customermarkers } = this.state;

		AssignedSVCMarkers.forEach((item) => {
			item.setMap(null);
		});

		AssignedSVCMarkers = [];

		if (showAssignedCalls) {
			AssignedCalls.forEach((item, i) => {
				let position = {
					lat : item.latitude,
					lng : item.longitude
				};

				let marker = new google.maps.Marker({
					position: position,
					map: map,
					titleinfo : this.renderInfoWindowFn(item, 'customerLocations'),
					animation: google.maps.Animation.DROP,
					icon : MapProps.getIcons().assignedSVCIcon,
					id : i,
					isUser : false
				});

				AssignedSVCMarkers.push(marker);

				bounds.extend(marker.position);

				this.markerListenerFn(marker, i, 'AssignedCalls');
			});

			if(customermarkers.length == 0)
				map.fitBounds(bounds);

			this.setState({
				AssignedSVCMarkers
			});
		} else
			this.setState({
				AssignedSVCMarkers
			});
	}

	initializeMap () {
		this.updateLoaderFlag(true);

		let { errMsg, customermarkers, map, markerCluster, waypoints, boundLatLng, bounds } = this.state;

		boundLatLng = {};

		customermarkers.forEach((item) => {
			item.setMap(null);
		});

		if(markerCluster)
			markerCluster.clearMarkers();

		customermarkers = [];
		waypoints = [];

		this.state.customerLocations.forEach((item, i) => {
			if (item.latitude != null && item.longitude != null) {
				let position = {
					lat : item.latitude,
					lng : item.longitude
				};

				let tempIcon, showLabel;

				if (this.props.rptparam == 'servicecallallotmentreport') {
					tempIcon = MapProps.getIcons().openSVCIcon;
					showLabel = "";
				}

				if(['leaditemsreport', 'contractitemsreport'].includes(this.props.rptparam)) {
					tempIcon = MapProps.getIcons().customerIcon;
					showLabel = "";
				}
				if(this.props.rptparam == 'collectionstatusreport') {
					tempIcon = MapProps.getIcons().customerDetailsIcon;

					let kFormatAmt = item.outstandingamount > 999 ? (item.outstandingamount%1000==0 ? item.outstandingamount/1000 : (item.outstandingamount/1000).toFixed(2)): item.outstandingamount;

					showLabel = {
						text : `${currencyFilter(kFormatAmt, this.props.app.defaultCurrency, this.props.app)}K`,
						color : 'white',
						fontWeight: 'bold',
						fontSize: '13px'
					};
				}
				if (this.props.rptparam == 'deliveryrouteplannerreport') {
					tempIcon = item.ischecked ? MapProps.getIcons().dcIcon : MapProps.getIcons().openSVCIcon;
					showLabel = "";
				}

				let marker = new google.maps.Marker({
					position: position,
					map: map,
					titleinfo : this.renderInfoWindowFn(item, 'customerLocations'),
					animation: google.maps.Animation.DROP,
					icon : tempIcon,
					label: showLabel,
					id : i,
					isUser : false,
					MyValue : this.props.rptparam == 'collectionstatusreport' ? item.outstandingamount : 0,
					reference_id: ['leaditemsreport', 'contractitemsreport'].includes(this.props.rptparam) ? item.id : (this.props.rptparam == 'collectionstatusreport' ? item.partnerid : null)
				});

				customermarkers.push(marker);

				bounds.extend(marker.position);

				this.markerListenerFn(marker, i, 'customerLocations');
			}
		});

		if(customermarkers.length > 0)
			map.fitBounds(bounds);

		let mcOptions = {
			styles: [{
				height: 33,
				width: 120,
				url: "/images/clustermarker1.png",
				textSize: 13,
				textColor: "white"
			}],
			zoomOnClick:true,
			maxZoom:18
		}

		let mcOptions1 = {
			styles: [{
				height: 41,
				width: 41,
				url: "/images/cluster.png",
				textSize: 14,
				textColor: "#444444"
			}],
			zoomOnClick:true,
			maxZoom:18
		}

		let markerCluster1 = (this.props.rptparam == 'collectionstatusreport') ? new MarkerClusterer(map, customermarkers, mcOptions) : new MarkerClusterer(map, customermarkers, mcOptions1);

		if (this.props.rptparam == 'collectionstatusreport') {
			markerCluster1.setCalculator( (customermarkers, numStyles) => {
				var index = 0;
				var title = "";
				var count = customermarkers.length;

				var valueToSum=0;

				for(var m=0;m<customermarkers.length;m++){
					valueToSum+=Number(customermarkers[m].MyValue);
				}

				var dv = valueToSum;

				while (dv !== 0) {
					dv = parseInt(dv / 10, 10);
					index++;
				}

				index = Math.min(index, numStyles);

				let kFormatAmt = valueToSum > 999 ? (valueToSum%1000==0 ? valueToSum/1000 : (valueToSum/1000).toFixed(2)): valueToSum;

				return {
					text: `(${count}) : ${currencyFilter(kFormatAmt, this.props.app.defaultCurrency, this.props.app)}K`,
					index: index,
					title: currencyFilter(valueToSum, this.props.app.defaultCurrency, this.props.app)
				};
			});
		}

		map.addListener('bounds_changed', ()=>{
			this.setState({
				boundLatLng: map.getBounds()
			});
		});

		this.setState({
			customermarkers,
			markerCluster : markerCluster1,
			waypoints
		}, ()=>{
			if (this.state.showAssignedCalls)
				this.getAssignedCalls();

			if (errMsg) {
				this.updateLoaderFlag(false);
				return this.props.openModal(modalService.infoMethod({
					header : "Error",
					body : errMsg,
					btnArray : ["Ok"]
				}));
			}
		});
	}

	markerListenerFn (marker, i, param) {
		let { map, stepDisplay } = this.state;
		if(stepDisplay) {
			google.maps.event.addListener(stepDisplay, 'domready', ()=> {
				var iwOuter = $('.gm-style-iw');
				iwOuter.addClass('gs-gm-style-iw');

				var iwOuterArrow = $('.gm-style-iw-t');
				iwOuterArrow.addClass('gs-gm-style-iw-t');

				var iwBackground = iwOuter.prev();
				var closeicon = iwOuter.next();

				iwBackground.children(':nth-child(2)').css({'display' : 'none'});
				var arrowEle = $('.gm-style-iw').prev().children(':nth-child(3)');

				arrowEle.css({'display' : 'none'});
				//arrowEle.children(':nth-child(1)').css({'width' : '13px'});
				//arrowEle.children(':nth-child(2)').css({'left' : '7px', 'width' : '13px', 'height' : '18px'});
				//arrowEle.children(':nth-child(2)').children(':nth-child(1)').css({'width' : '8px', 'height' : '26px'});
				iwBackground.children(':nth-child(1)').css({'display' : 'none'});

				iwBackground.children(':nth-child(4)').css({'display' : 'none'});
				closeicon.css({'display' : 'none'});
				$('button.gm-ui-hover-effect').css({'display' : 'none'});
			});
		}
		if (param == 'customerLocations')
			marker.addListener('click', ()=> {
				this.setState({
					reference_id: null,
					isSummaryActive: true,
					isDetailsActive: false
				}, () => {
					this.markerClickFn(marker);
					marker.setAnimation(google.maps.Animation.BOUNCE);
				});
			});
		else
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					stepDisplay.setContent('<div>'+this.titleinfo+'</div>');
					stepDisplay.open(map, this);
				}
			})(marker, i));

		google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
			return function() {
				stepDisplay.setContent('<div>'+this.titleinfo+'</div>');
				stepDisplay.open(map, this);
			}
		})(marker, i));

		google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
			return function() {
				stepDisplay.close();
				this.setAnimation(null);
			}
		})(marker, i));
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	filterLatLng (availableMarkers) {
		let { boundLatLng, map, bounds } = this.state;
		let x = [];

		if (!boundLatLng) {
			let a = bounds.extend({
				lat: 38.195311938720536,
				lng: 73.3534302228104
			});

			map.fitBounds(a);
			map.setZoom(3);

			boundLatLng = map.getBounds();
		}

		if(Object.keys(boundLatLng ? boundLatLng : {}).length > 0){
			availableMarkers.forEach((item) => {
				if (['servicecallallotmentreport', 'deliveryrouteplannerreport'].indexOf(this.props.rptparam, ) >= 0) {
					if (item.ischecked || boundLatLng.contains(item.position))
						x.push(item);
				} else {
					if (boundLatLng.contains(item.position))
						x.push(item);
				}
			});
		} else
			x = [...availableMarkers];

		return x;
	}

	elementClickFn (indexVal) {
		let { customermarkers } = this.state;

		this.setState({
			isSummaryActive: false,
			isDetailsActive: true
		}, () => {
			google.maps.event.trigger(customermarkers[indexVal],'click');
		});
	}

	updateShowDetailsFlag () {
		let { availableMarkers, waypoints, map, directionDisplayArray, directionsResults } = this.state;

		availableMarkers.forEach((item)=>{
			item.ischecked = false;
		});

		directionDisplayArray.forEach((item) => {
			item.setMap(null);
		});

		this.setState({
			availableMarkers,
			waypoints: [],
			directionDisplayArray: [],
			totalDistanceTravelled: 0,
			directionsResults: [],
			isSummaryActive: true,
			isDetailsActive: false
		}, () => {
			this.initializeMap();
		});
	}

	inputonChange(value, field) {
		let { search } = this.state;
		search[field] = value;
		this.setState({ search });
	}

	checkboxOnChange (checked, indexVal) {
		let { availableMarkers, customermarkers } = this.state;

		for (let i = 0; i < availableMarkers.length; i++) {
			if (availableMarkers[i].indexVal == indexVal) {
				availableMarkers[i].ischecked = (checked && availableMarkers[i].ischecked) ? !checked : checked;

				if (this.props.rptparam == 'deliveryrouteplannerreport')
					customermarkers[i].setIcon(availableMarkers[i].ischecked ? MapProps.getIcons().dcIcon : MapProps.getIcons().openSVCIcon);

				if(availableMarkers[i].ischecked)
					customermarkers[i].setAnimation(google.maps.Animation.BOUNCE);
				else
					customermarkers[i].setAnimation(null);

				break;
			}
		}

		this.setState({
			availableMarkers
		});
	}

	engineerCB (value) {
		let tempObj = {};

		if (value) {
			for(let i = 0; i < this.props.resource.engineerArray.length; i++) {
				if (this.props.resource.engineerArray[i].id == value) {
					tempObj['engineerid'] = value;
					tempObj['engineerid_displayname'] = this.props.resource.engineerArray[i].displayname;
					tempObj['salesperson'] = this.props.resource.engineerArray[i].userid;
					tempObj['engineerid_userid'] = this.props.resource.engineerArray[i].userid;
					break;
				}
			}
		} else
			tempObj = {
				engineerid: null,
				engineerid_displayname: null,
				salesperson: null,
				engineerid_userid: null
			}

		this.setState({
			...tempObj
		})
	}

	defineAddresses (points) {
		if (points.length > 23)
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Please choose only 23 deliverynotes to dispatch !!!",
				btnArray : ["Ok"]
			}));

		let { startDeliveryAddress, endDeliveryAddress, errMsg } = this.state;
		let x = [...points], geoAddress = [];

		errMsg = "";

		let geocoder = new google.maps.Geocoder();

		if (startDeliveryAddress)
			geoAddress.push({
				address: startDeliveryAddress,
				index: 0
			});

		if (endDeliveryAddress)
			geoAddress.push({
				address: endDeliveryAddress,
				index: 1
			});

		async.eachSeries(geoAddress, (item, eachCB) => {
			geocoder.geocode( { 'address': item.address}, (results, status)=> {
				if (status == 'OK') {
					if (item.index == 0)
						x.unshift({
							deliveryaddress: item.address,
							position: {
								lat : results[0].geometry.location.lat(),
								lng : results[0].geometry.location.lng()
							}
						});

					if (item.index == 1)
						x.push({
							deliveryaddress: item.address,
							position: {
								lat : results[0].geometry.location.lat(),
								lng : results[0].geometry.location.lng()
							}
						});
				}
				else
					errMsg = 'Geocode was not successful for the following reason: ' + status;

				eachCB(null);
			});
		}, () => {
			if (errMsg) {
				return this.props.openModal(modalService.infoMethod({
					header : "Error",
					body : errMsg,
					btnArray : ["Ok"]
				}));
			} else
				this.plotRoute(x);
		});
	}

	plotRoute (points) {
		let { waypoints, customermarkers, map, directionDisplayArray, markerCluster } = this.state;
		let directionsService = new google.maps.DirectionsService;

		customermarkers.map((item) => {
			item.setMap(null);
		});

		directionDisplayArray.forEach((item) => {
			item.setMap(null);
		});

		if(markerCluster)
			markerCluster.clearMarkers();

		directionDisplayArray = [];
		waypoints = [];
		customermarkers = [];

		points.forEach((item, i) => {
			waypoints.push({
				location : item.position,
				stopover: true,
				id : item.id,
				customerid_name: item.customerid_name,
				deliveryaddress: item.deliveryaddress,
				salesperson_displayname: item.salesperson_displayname,
				deliverynotenumber: item.deliverynotenumber,
				deliverynotedate: item.deliverynotedate,
				customerid: item.customerid
			});
		})

		let tempWayPoints = [], FinalWayPoints = {}, x = [], marker_ids = [], originalWayPoints = [];

		x = waypoints;

		if(x.length > 0) {

			x.forEach((v) => {
				marker_ids.push(v.id);
				originalWayPoints.push({...v});

				delete v.id;
				delete v.customerid_name;
				delete v.deliveryaddress;
				delete v.salesperson_displayname;
				delete v.deliverynotenumber;
				delete v.deliverynotedate;
				delete v.customerid;
			});

			let origin, destination;

			origin = x[0].location;
			destination = x[x.length-1].location;

			for (let i = 1; i < x.length-1; i++) {
				tempWayPoints.push(x[i]);
			}

			FinalWayPoints = {
				origin : origin,
				destination : destination,
				waypoints : tempWayPoints
			};
		}

		directionsService.route({
			origin: FinalWayPoints.origin,
			destination: FinalWayPoints.destination,
			travelMode: 'DRIVING',
			waypoints : FinalWayPoints.waypoints,
			optimizeWaypoints: true,
			provideRouteAlternatives : true
		}, (response, status)=> {
			if (status === 'OK') {
				this.setState({
					customermarkers,
					waypoints,
					directionsService,
					directionDisplayArray,
					isSummaryActive: false,
					isDetailsActive: true,
					markerCluster
				}, () => {
					this.routeMap(response, originalWayPoints, marker_ids);
				});
			}
			else
				console.log('Directions request failed due to ' + status);
		});
	};

	routeMap (response, originalWayPoints, marker_ids) {
		let { customermarkers, map, directionDisplayArray, directionsResults, totalDistanceTravelled, bounds } = this.state;

		totalDistanceTravelled = 0;

		directionsResults = [];

		let labelIndex = 0;
		let totalDistance = 0;

		let directionsDisplay = new google.maps.DirectionsRenderer({
			map: map,
			suppressMarkers: true,
			polylineOptions : {
				strokeColor : '#3F4B6D'
			}
			//preserveViewport : true	-the viewport is left unchanged, unless the map's center and zoom were never set.
		});

		directionsDisplay.setDirections(response);
		directionDisplayArray.push(directionsDisplay);

		let waypoint_order = response.routes[0].waypoint_order;
		let z = [], tempZ = [];

		if (originalWayPoints.length == 1)
			z = [...originalWayPoints];
		else {
			z.push(originalWayPoints[0]);

			for (let j = 1; j < originalWayPoints.length-1; j++) {
				tempZ.push(originalWayPoints[j])
			}

			for (let j = 0; j < waypoint_order.length; j++){
				z.push(tempZ[waypoint_order[j]]);
			}

			z.push(originalWayPoints[originalWayPoints.length-1]);
		}

		let dcTempIcon, showLabel;
		for (let j = 0; j < z.length; j++) {
			if (j == 0){
				dcTempIcon = MapProps.getIcons().dcRouteStartIcon;
				showLabel = "";
			} else if (j == z.length -1) {
				dcTempIcon = MapProps.getIcons().dcRouteEndIcon;
				showLabel = "";
			} else {
				dcTempIcon = MapProps.getIcons().dcRouteIcon;
				showLabel = {
					text : (j).toString(),
					color : 'white',
					fontWeight: 'bold',
					fontSize: '13px'
				}
			}

			let marker = new google.maps.Marker({
				position: z[j].location,
				map: map,
				titleinfo : this.renderInfoWindowFn(z[j], 'customerLocations'),
				animation: google.maps.Animation.DROP,
				icon : dcTempIcon,
				label: showLabel,
				id : z[j].id,
				isUser : false
			});

			customermarkers.push(marker);

			bounds.extend(marker.position);

			this.markerListenerFn(marker, j, 'plotRoute');
		}

		for(let i = 0; i < response.routes[0].legs.length; i++) {
			let tempObj = {
				deliveryaddress: z[i].deliveryaddress,
				deliverynotenumber: z[i].deliverynotenumber,
				deliverynotedate: z[i].deliverynotedate,
				customerid_name : z[i].customerid_name,
				salesperson_displayname : z[i].salesperson_displayname,
				customerid : z[i].customerid,
				duration : response.routes[0].legs[i].duration.text,
				distance : response.routes[0].legs[i].distance.text,
				duration_value : response.routes[0].legs[i].duration.value,
				distance_value : response.routes[0].legs[i].distance.value,
				slno : i+1
			};

			totalDistance += tempObj.distance_value;

			directionsResults.push(tempObj);
		}

		if (originalWayPoints.length > 1)
			directionsResults.push({
				deliveryaddress: z[z.length-1].deliveryaddress,
				deliverynotenumber: z[z.length-1].deliverynotenumber,
				deliverynotedate: z[z.length-1].deliverynotedate,
				customerid_name : z[z.length-1].customerid_name,
				salesperson_displayname : z[z.length-1].salesperson_displayname,
				customerid : z[z.length-1].customerid,
				duration : 0,
				distance : 0,
				duration_value : 0,
				distance_value : 0,
				slno : directionsResults.length+1
			});

		totalDistanceTravelled += totalDistance/1000;
		totalDistanceTravelled = Number((totalDistanceTravelled).toFixed(2));

		this.setState({
			directionDisplayArray,
			totalDistanceTravelled,
			directionsResults,
			customermarkers
		})
	}

	printFn () {
		let reportWindow = window.open('', 'Delivery Dispatch', 'width=600,height=600');
		reportWindow.document.open();

		let pvtRenderArea = document.getElementsByClassName('printDoc')[0];

		let htmlToPrint = '<html><head><link rel="stylesheet" href="../../css/reactcustom.css" media="print" type="text/css"></link><style type="text/css">.timeline-centered {position: relative;margin-bottom: 20px;margin-top: 5px;} .timeline-centered .timeline-entry {position: relative;margin-top: 5px;margin-left: 20px;margin-bottom: 10px;clear: both;} .timeline-centered .timeline-entry .timeline-entry-inner {position: relative;margin-left: -20px;} .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time {position: absolute;left: -100px;text-align: right;padding: 10px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;} .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon {background: #fff;color: #fff;display: block;width: 20px;height: 20px;-webkit-background-clip: padding-box;-moz-background-clip: padding;background-clip: padding-box;-webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;text-align: center;background-color: #d3d3d3;line-height: 20px;font-size: 13px;float: left;} .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label {position: relative;min-height: 30px;margin-left: 35px;-webkit-background-clip: padding-box;-moz-background-clip: padding;background-clip: padding-box;-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px;} .timeline-line {position: absolute;width: 1px;display: block;background: #d2d2d2;top: 0px;bottom: 0px;margin-left: 10px;} .timeline-centered .timeline-line::after {bottom: -4px;} .timeline-centered .timeline-line::after {content: "";position: absolute;left: -3px;width: 8px;height: 8px;display: block;border-radius: 50%;background: #d3d3d3;} .margintop-20 {margin-top:20px;} div { font-family: "Open Sans", sans-serif;}';

		htmlToPrint += '</style></head><body><div class="printDiv"><p><b> Delivery Dispatch </b></p>' + pvtRenderArea.innerHTML + '</div></body></html>';

		reportWindow.document.write(htmlToPrint);

		if(reportWindow.document.getElementsByClassName("main-svg").length > 0) {
			reportWindow.document.getElementsByClassName("main-svg")[0].style.position = 'absolute';
			reportWindow.document.getElementsByClassName("main-svg")[0].style.top = '0px';
			reportWindow.document.getElementsByClassName("main-svg")[0].style.left = '0px';
			reportWindow.document.getElementsByClassName("main-svg")[1].style.position = 'absolute';
			reportWindow.document.getElementsByClassName("main-svg")[1].style.top = '0px';
			reportWindow.document.getElementsByClassName("main-svg")[1].style.left = '0px';
		}

		reportWindow.print();
		reportWindow.close();
	}

	renderSearch (title, searchValue, selectedMarkers) {
		return (
			<div  className="reportcontent--map-sidebar-title" style={{width: '100%', display: 'flex', flexWrap:'wrap'}}>
				<div className="col-md-10 form-group"><span className="font-16">{title}</span></div>
				<div className="col-md-2  form-group float-right">
					<a onClick={() => {this.setState({showSearch : !this.state.showSearch})}} ><span className="fa fa-search" style={{color: `${this.state.showSearch ? '#D8D8D8' : ''}`}}></span></a>
				</div>
				{selectedMarkers.length > 0 ? <div className="col-md-12 form-group">Selected : {selectedMarkers.length}</div>:null}
				{ this.state.showSearch ? <div className="col-md-12 form-group"><input type="text" className="form-control" name={searchValue} value={this.state.search.group} placeholder="Search by Customer Name" onChange={(evt) =>{this.inputonChange(evt.target.value, searchValue)}}/></div> : null }
			</div>
		);
	}

	renderTableBody (item, index, customermarkers) {
		return (
			<tr key={index}
				onClick={()=>{if(['servicecallallotmentreport', 'deliveryrouteplannerreport'].indexOf(this.props.rptparam) == -1){this.elementClickFn(item.indexVal)}}}
				onMouseOver={()=>{
					google.maps.event.trigger(customermarkers[item.indexVal],'mouseover');
				}}
				onMouseOut={()=>{
					google.maps.event.trigger(customermarkers[item.indexVal],'mouseout');}}>
				<td className={`${['servicecallallotmentreport', 'deliveryrouteplannerreport'].indexOf(this.props.rptparam) >= 0 ? 'text-center': 'text-left'}`}
					style={{verticalAlign: `${['servicecallallotmentreport', 'deliveryrouteplannerreport'].indexOf(this.props.rptparam) >= 0 ? 'baseline' : ''}`}}>
					{this.props.rptparam == 'leaditemsreport' ? item.customer : null}
					{this.props.rptparam == 'contractitemsreport' ? item.customer : null}
					{this.props.rptparam == 'collectionstatusreport' ? item.name : null}
					{this.props.rptparam == 'servicecallallotmentreport' ? <input type="checkbox" checked={item.ischecked} onChange={(evt) => this.checkboxOnChange(evt.target.checked, item.indexVal)}/> : null}
					{this.props.rptparam == 'deliveryrouteplannerreport' ? <input type="checkbox" checked={item.ischecked} onChange={(evt) => this.checkboxOnChange(evt.target.checked, item.indexVal)}/> : null}
				</td>
				<td className={`${['collectionstatusreport'].indexOf(this.props.rptparam) >= 0 ? 'text-right': 'text-left'}`}
					style={{wordWrap: `${['servicecallallotmentreport'].indexOf(this.props.rptparam) >= 0 ? 'break-word' : ''}`}}>
					{this.props.rptparam == 'leaditemsreport' ? <a href={`#/details/leads/${item.id}`}>{item.leadno}</a> : null}
					{this.props.rptparam == 'contractitemsreport' ? <a href={`#/details/contracts/${item.id}`}>{item.contractno}</a> : null}
					{this.props.rptparam == 'collectionstatusreport' ? currencyFilter(item.outstandingamount, this.props.app.defaultCurrency, this.props.app) : null}
					{this.props.rptparam == 'servicecallallotmentreport' ? <div>
						<span>{item.customerid_name}</span>
						<a className="marginleft-5 badge btn gs-form-btn-secondary" href={`#/details/servicecalls/${item.id}`}>{item.servicecallno}</a>
					</div> : null}
					{this.props.rptparam == 'deliveryrouteplannerreport' ? <div>
						<b>{item.customerid_name}</b>
						<a className="marginleft-5 badge btn gs-form-btn-secondary" href={`#/details/deliverynotes/${item.id}`}>{item.deliverynotenumber}</a>
						<div className="small">{item.deliveryaddress}</div>
					</div> : null}
				</td>
			</tr>
		)
	}

	renderTabView (selectedMarkers, markersFound, totalOutStanding) {
		let { customermarkers, availableMarkers } = this.state;

		return (
			<div className="gs-md-12">
				<div className="tab-content" style={{height: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - $('.reportcontent--map-sidebar-listtag').outerHeight() - 50}px`, maxHeight: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - $('.reportcontent--map-sidebar-listtag').outerHeight() - 50}px`, overflow: 'hidden auto'}}>
					<div className={`tab-pane fade ${this.state.isSummaryActive ? 'show active' : ''}`}>
						{this.props.rptparam == 'leaditemsreport' && markersFound ? <div className="col-md-12">
								<div className="row">{this.renderSearch('Lead Details', 'customer', selectedMarkers)}</div>
								<table className="table gs-table-borderless table-sm">
									<thead>
										<tr>
											<th className="text-left gs-uppercase font-10" style={{color: '#aaa'}}>Customer Name</th>
											<th className="text-left gs-uppercase font-10" style={{color: '#aaa'}}>Lead No</th>
										</tr>
									</thead>
									<tbody>
										{search(this.filterLatLng(availableMarkers), this.state.search).map((item, index) => this.renderTableBody(item, index, customermarkers))}
									</tbody>
								</table>
							</div> : null}
						{this.props.rptparam == 'contractitemsreport' && markersFound ? <div className="col-md-12">
								<div className="row">{this.renderSearch('Contract Details', 'customer', selectedMarkers)}</div>
								<table className="table gs-table-borderless table-sm">
									<thead>
										<tr>
											<th className="text-left gs-uppercase font-10" style={{color: '#aaa'}}>Customer Name</th>
											<th className="text-left gs-uppercase font-10" style={{color: '#aaa'}}>Contract No</th>
										</tr>
									</thead>
									<tbody>
										{search(this.filterLatLng(availableMarkers), this.state.search).map((item, index) => this.renderTableBody(item, index, customermarkers))}
									</tbody>
								</table>
							</div> : null}
						{this.props.rptparam == 'collectionstatusreport' && markersFound ? <div style={{width: '100%', display: 'flex', flexWrap:'wrap'}}>
						    {this.renderSearch('Collection Details', 'name', selectedMarkers)}
						    <div className="col-md-12" style={{height: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - 5}`, overflow: 'hidden auto'}}>
						        <table className="table gs-table-borderless table-hover table-sm">
						            <thead>
						                <tr>
						                    <th className="text-left gs-uppercase font-10" style={{color: '#aaa'}}>Customer Name</th>
						                    <th className="text-right gs-uppercase font-10" style={{color: '#aaa'}}>Outstanding Amount</th>
						                </tr>
						            </thead>
						            <tbody>
						                {search(this.filterLatLng(availableMarkers), this.state.search).map((item, index) => this.renderTableBody(item, index, customermarkers))}
						            </tbody>
						        </table>
						    </div>
						    <div className="reportcontent--map-sidebar-footer" style={{width: '100%', zIndex: '100', position: 'absolute', bottom: '0px', color: '#1A88FF', padding: '10px'}}>
						        <span className = "marginleft-10" style={{fontSize: '13px'}}>Total Outstanding Amount</span>
						        <span className="float-right">{currencyFilter(totalOutStanding, this.props.app.defaultCurrency, this.props.app)}</span>
						    </div>
						</div> : null}
					</div>
					<div className={`tab-pane fade ${this.state.isDetailsActive ? 'show active' : ''}`}>
						{['leaditemsreport', 'collectionstatusreport', 'contractitemsreport'].indexOf(this.props.rptparam) >= 0 && this.state.reference_id ? <DetailedInfoWindow
								form = {this.props.form}
								history = {this.props.history}
								rptparam = {this.props.rptparam}
								resourceName = {this.state.tabNames[this.props.rptparam].resourceName}
								reference_id = {this.state.reference_id}
								app = {this.props.app}/> : <div className={`margintop-50 ${['leaditemsreport', 'collectionstatusreport', 'contractitemsreport'].indexOf(this.props.rptparam) >= 0 ? 'show' : 'hide'}`}>
							<div className="col-md-8 offset-md-2 col-sm-12 col-xs-12 text-center">Click customer name or marker to view details !!!</div>
						</div>}
					</div>
				</div>
			</div>
		);
	}

	renderNormalView (selectedMarkers, markersFound) {
		let { customermarkers, availableMarkers } = this.state;

		return (
			<div className="col-md-12">
				{this.state.isSummaryActive ? <div style={{width: '100%', display: 'flex', flexWrap:'wrap'}}>
					{this.props.rptparam == 'servicecallallotmentreport' && markersFound ? <div style={{width: '100%', display: 'flex', flexWrap:'wrap', marginTop: '10px'}}>
						{this.renderSearch('Service Call Details', 'customerid_name', selectedMarkers)}
						<div className="reportcontent--map-sidebar-searchinput" style={{width: '100%', display: 'flex', flexWrap:'wrap'}}>
							<div className="row col-md-12 col-sm-12">
								<div className="form-group col-md-4">
									<label className="labelclass">Assign to</label>
								</div>
								<div className="form-group col-md-8">
									<LocalSelect options={this.props.resource.engineerArray} label={'namewithcount'} valuename={'id'} value={this.state.engineerid} onChange={(value) => this.engineerCB(value)}/>
								</div>
							</div>
							{(this.state.engineerid && !this.state.engineerid_userid)? <div className="row col-md-12 col-sm-12">
								<div className="form-group col-md-4">
									<label className="labelclass">Service Person</label>
								</div>
								<div className="form-group col-md-8">
									<SelectAsync resource={"users"} fields={"id,displayname"} label={'displayname'} valuename={'id'} filter={`users.issalesperson=true`} value={this.state.salesperson} onChange={(value) => this.setState({salesperson: value})}/>
								</div>
							</div> : null }
						</div>
						<div className="col-md-12" style={{height: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - $('.reportcontent--map-sidebar-searchinput').outerHeight() - $('.reportcontent--map-sidebar-title').outerHeight() - $('.reportcontent--map-sidebar-footer').outerHeight() - (selectedMarkers.length > 0 ? 30 : 0) - (this.state.showSearch ? 75 : 20)}px`, maxHeight: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - $('.reportcontent--map-sidebar-searchinput').outerHeight() - $('.reportcontent--map-sidebar-title').outerHeight() - $('.reportcontent--map-sidebar-footer').outerHeight() - (selectedMarkers.length > 0 ? 30 : 0) - (this.state.showSearch ? 75 : 20)}px`, overflow: 'hidden auto'}}>
							<table className="table gs-table-borderless table-sm">
								<tbody>
									{search(this.filterLatLng(availableMarkers), this.state.search).map((item, index) => this.renderTableBody(item, index, customermarkers))}
								</tbody>
							</table>
						</div>
						<div className="text-center reportcontent--map-sidebar-footer" style={{width: 'calc(100% - 15px)', zIndex: '100', position: 'absolute', bottom: '0px'}}>
							<button type="button" onClick={() => {this.props.callback(this.state);}} className="btn btn-width gs-btn-success btn-sm margintop-25" style={{borderRadius: "20px", width: '75%', height: '40px'}}  disabled={!this.state.engineerid || !this.state.salesperson}>Assign
							</button>
						</div>
					</div> : null}
					{this.props.rptparam == 'deliveryrouteplannerreport' && markersFound ? <div style={{width: '100%', display: 'flex', flexWrap:'wrap', marginTop: '10px'}}>
						{this.renderSearch('Delivery Note Details', 'customerid_name', selectedMarkers)}
						<div className="reportcontent--map-sidebar-searchinput" style={{width: '100%', display: 'flex', flexWrap:'wrap'}}>
							<div className="col-md-12 form-group">
								<input type="text" className="form-control"  name="startDeliveryAddress" value={this.state.startDeliveryAddress} placeholder="Start Addess (optional)" onChange={(evt)=>this.setState({startDeliveryAddress:evt.target.value})}/>
							</div>
							<div className="col-md-12 form-group">
								<input type="text" className="form-control"  name="endDeliveryAddress" value={this.state.endDeliveryAddress} placeholder="End Addess (optional)" onChange={(evt)=>this.setState({endDeliveryAddress: evt.target.value})}/>
							</div>
						</div>
						<div className="col-md-12" style={{height: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - $('.reportcontent--map-sidebar-searchinput').outerHeight() - $('.reportcontent--map-sidebar-title').outerHeight() - $('.reportcontent--map-sidebar-footer').outerHeight() - (selectedMarkers.length > 0 ? 30 : 0) - (this.state.showSearch ? 75 : 20)}px`, maxHeight: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - $('.reportcontent--map-sidebar-searchinput').outerHeight() - $('.reportcontent--map-sidebar-title').outerHeight() - $('.reportcontent--map-sidebar-footer').outerHeight() - (selectedMarkers.length > 0 ? 30 : 0) - (this.state.showSearch ? 75 : 20)}px`, overflow: 'hidden auto'}}>
							<table className="table gs-table-borderless table-sm">
								<tbody>
									{search(this.filterLatLng(availableMarkers), this.state.search).map((item, index) => this.renderTableBody(item, index, customermarkers))}
								</tbody>
							</table>
						</div>
						<div className="text-center reportcontent--map-sidebar-footer" style={{width: 'calc(100% - 15px)', zIndex: '100', position: 'absolute', bottom: '0px'}}>
							<button type="button" onClick={() => {this.defineAddresses(selectedMarkers)}} className="btn btn-width gs-btn-success btn-sm margintop-25" style={{borderRadius: "20px", width: '75%', height: '40px'}} disabled={selectedMarkers.length == 0}>Route
							</button>
						</div>
					</div> : null}
				</div> : null}
				{this.state.isDetailsActive ? <div style={{width: '100%', display: 'flex', flexWrap:'wrap'}}>
					<div style={{width: '100%', display: 'flex', flexWrap:'wrap'}}>
						<div className="form-group reportcontent--map-sidebar-title" style={{width: '100%', display: 'flex', flexWrap:'wrap'}}>
							<div>
								<span className="fa fa-arrow-left" onClick={() => {this.updateShowDetailsFlag()}}></span>
							</div>
							<div className="col-md-11"><b>Delivery Route</b></div>
						</div>
						<div className="printDoc main-svg" style={{height: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - $('.reportcontent--map-sidebar-title').outerHeight() - $('.reportcontent--map-sidebar-footer').outerHeight() - 90}px`, maxHeight: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - $('.reportcontent--map-sidebar-title').outerHeight() - $('.reportcontent--map-sidebar-footer').outerHeight() - 90}px`, overflow: 'hidden auto'}}>
							{this.state.totalDistanceTravelled > 0 ? <div className="form-group">
								<span> Total Distance: </span>
								<b>{this.state.totalDistanceTravelled} KMS</b>
							</div> : null}
							<div className="timeline-centered">
								<div className="timeline-line"></div>
								{this.state.directionsResults.map((activityItem, activityIndex)=> {
									return (
										<div className="timeline-entry" key={activityIndex}>
											<div className="timeline-entry-inner">
												{(activityIndex == 0 || activityIndex == this.state.directionsResults.length-1) ? <div style={{marginLeft: '7px',width: '8px', height: '8px', border: `${activityIndex == 0 ? '1px solid #4CD964' : '1px solid #FF3B30'}`, borderRadius: '50%',backgroundColor: `${activityIndex == 0 ? '#4CD964' : '#FF3B30'}`, boxShadow: `${activityIndex == 0 ? '0px 1px 3px 6px #d6f3d6' : '0px 1px 3px 6px #fbd7d7'}`, float: 'left'}}></div> : <div className="timeline-icon" style={{backgroundColor: `${(activityIndex == 0 || activityIndex == this.state.directionsResults.length-1) ? '' : '#53be9d'}`}}>{activityIndex}</div>}
												<div className="timeline-label">
													<div>
														<div><b>{activityItem.customerid_name}</b></div>
														<div>{activityItem.deliveryaddress}</div>
														<div className="small">
															{activityItem.deliverynotenumber ? <div>Deliverynote No : <b>{activityItem.deliverynotenumber}</b></div> : null}
															{activityItem.deliverynotedate ? <div>Deliverynote Date : <b>{dateFilter(activityItem.deliverynotedate)}</b></div> : null}
														</div>
														{(activityItem.duration_value > 0 || activityItem.distance_value > 0) ? <div className="small margintop-20">
															{activityItem.duration_value > 0 ? <span>Duration : {activityItem.duration}; </span> : null}
															{activityItem.distance_value > 0 ? <span>Distance : {activityItem.distance}</span> : null}
														</div>: null}
													</div>
												</div>
											</div>
										</div>
									);
								})}
							</div>
						</div>
						<div className="text-center reportcontent--map-sidebar-footer'" style={{width: 'calc(100% - 15px)', zIndex: '100', position: 'absolute', bottom: '0px'}}>
							<button
								type="button"
								onClick={() => {this.printFn();}}
								className="btn btn-width gs-btn-success btn-sm"
								style={{borderRadius: "20px"}}
								disabled={selectedMarkers.length == 0}>Print
							</button>
							{ checkActionVerbAccess(this.props.app, 'deliverynotes', 'Dispatch') ? <button
								type="button"
								onClick={() => {this.props.callback(selectedMarkers);}}
								className="btn btn-width gs-btn-success btn-sm"
								style={{borderRadius: "20px"}}
								disabled={selectedMarkers.length == 0}>Dispatch
							</button> : null}
						</div>
					</div>
				</div> : null}
			</div>
		);
	}

	render() {
		const mapStyle = {
			width: '100%',
			height: $(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - 5
		}

		let { customermarkers, availableMarkers } = this.state;
		let selectedMarkers = [], markersFound = false, totalOutStanding = 0;

		if (['servicecallallotmentreport', 'deliveryrouteplannerreport'].indexOf(this.props.rptparam) >= 0 && availableMarkers.length > 0)
			availableMarkers.forEach((item) => {
				if (item.ischecked) {
					if (this.props.rptparam == 'servicecallallotmentreport')
						selectedMarkers.push(item.id);
					else
						selectedMarkers.push(item);
				}
			});

		if (this.props.rptparam == 'collectionstatusreport' && availableMarkers && this.filterLatLng(availableMarkers).length > 0)
			this.filterLatLng(availableMarkers).forEach((item) => {
				totalOutStanding += item.outstandingamount;
			});

		if (availableMarkers)
			markersFound = this.state.isSummaryActive ? this.filterLatLng(availableMarkers).length > 0 : true;

		return(
			<div>
				{ (!this.props.app.feature.licensedTrackingUserCount) ? <div className="col-md-12 col-sm-12 col-xs-12 margintop-50">
					<div className="col-md-8 offset-md-2 col-sm-12 col-xs-12 alert alert-warning text-center">You have no access to view map details !!</div>
				</div> : <div className="row">
					<div className="col-md-12">
						{this.props.rptparam != 'deliveryrouteplannerreport' ? <div className="row floating-panel">
							<div style={{position: 'relative', display: 'flex', top: '5px'}}>
								<span className="marginright-10">Show Employees</span>
								<div>
									<label className="gs-checkbox-switch">
										<input className="form-check-input" type="checkbox" checked={this.state.showEmployees} onChange={() => this.setState({
												showEmployees: !this.state.showEmployees
											}, ()=>{
												this.getReportData();
											})} />
										<span className="gs-checkbox-switch-slider"></span>
									</label>
								</div>
							</div>
							{this.props.rptparam == 'servicecallallotmentreport' ? <div style={{position: 'relative', display: 'flex', top: '5px'}}>
								<span className="marginright-10 marginleft-10">Show Assigned Calls</span>
								<div>
									<label className="gs-checkbox-switch">
										<input className="form-check-input" type="checkbox" checked={this.state.showAssignedCalls} onChange={() => this.setState({
												showAssignedCalls: !this.state.showAssignedCalls
											}, ()=>{
												this.getAssignedCalls();
											})} />
										<span className="gs-checkbox-switch-slider"></span>
									</label>
								</div>
							</div> : null}
						</div>: null}
					</div>
					<div className="col-md-9" style={{paddingLeft: '50px'}}>
						<div id="map" style={mapStyle}></div>
					</div>
					{markersFound ? <div className="col-md-3 paddingleft-0">
						<div className="row bg-white" style={{height:`${$(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - 5}px`}}>
							{['leaditemsreport', 'collectionstatusreport', 'contractitemsreport'].indexOf(this.props.rptparam) >= 0 ? <div className="col-md-12 reportcontent--map-sidebar-listtag form-group">
								<ul className="nav nav-tabs gs-home-navtabs">
									<li className={`nav-item`} onClick={() => this.tabOnChange('summary')} style={{width:'50%'}}>
										<span className={`nav-link bg-transparent ${this.state.isSummaryActive ? 'active' : ''}`}>{this.state.tabNames[this.props.rptparam].summary}</span>
									</li>
									<li className={`nav-item`} onClick={() => this.tabOnChange('details')} style={{width:'50%'}}>
										<span className={`nav-link bg-transparent ${this.state.isDetailsActive ? 'active' : ''}`}>{this.state.tabNames[this.props.rptparam].details}</span>
									</li>
								</ul>
							</div>: null}
							{['leaditemsreport', 'collectionstatusreport', 'contractitemsreport'].indexOf(this.props.rptparam) >= 0 ? this.renderTabView(selectedMarkers, markersFound, totalOutStanding) : this.renderNormalView(selectedMarkers, markersFound)}
						</div>
					</div> : <div className="col-md-3 margintop-50">
						<div className="col-md-8 offset-md-2 col-sm-12 col-xs-12 text-center">No Markers Found!!</div>
					</div>}
				</div> }
			</div>
		);
	};
};