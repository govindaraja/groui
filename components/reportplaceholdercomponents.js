import React, { Component } from 'react';

export default class ReportPlaceholderComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let { reportdata, isreportbuilder, isRestrictAccess } = this.props;

		let heightVal = ($('.report-header').outerHeight() || 0)+($('.main-navbar').outerHeight() || 0)+($('.report-filtertag-container').outerHeight() || 0) + 5;

		if (isRestrictAccess) {
			return (
				<div className="" ref="reportsearchmessage" style={{height: `calc(100vh - ${($('.report-header').outerHeight()+$('.main-navbar').outerHeight())}px`}}>
					<div className="d-flex flex-column align-items-center justify-content-center h-100">
						<img src="/images/dashboard_noaccess.png" className="report-error-image" style={{width:"80px", height:"80px" }}/>

						<div className="report-error-text mt-3">Sorry, You have no access to view this report</div>
					</div>
				</div>
			);
		} else {
			return (
				<>
					{
						reportdata == null ? <div className="d-flex flex-column align-items-center justify-content-center" style={{height: `calc(100vh - ${heightVal}px`}}>
							<div ref="reportsearchmessage" className="text-center" style={{paddingLeft: `${isreportbuilder ? 'auto' : '20%'}`}}>
								<img src="/images/empty-placeholder.png" className="report-error-image" width="150" height="150" />

								<div className="report-error-text">Please enter search criteria and click Go!</div>
							</div>
						</div> : null
					}

					{
						reportdata && reportdata.length == 0 ? <div className="d-flex flex-column align-items-center justify-content-center" style={{height: `calc(100vh - ${heightVal}px`}}>
							<div className="text-center" ref="reportsearchmessage" style={{paddingLeft: `${isreportbuilder ? 'auto' : '20%'}`}}>
								<img src="/images/no-search-results-placeholder.png" className="report-error-image" width="150" height="150" />

								<div className="report-error-text">No results found for your search!</div>
							</div>
						</div> : null
					}
				</>
			);
		}
	}

}