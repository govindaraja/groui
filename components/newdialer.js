import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import {updatePageJSONState, updateFormState, updateAppState } from '../actions/actions';
import axios from 'axios';
import { Link, withRouter, matchPath } from 'react-router-dom';
import { commonMethods, modalService, pageValidation } from '../utils/services';
import routes from '../routeconfig';
import { Timer } from './utilcomponents';
let config = require('../config');

class Dialer extends Component {

	constructor(props) {
		super(props);
		this.state = {
			value: '',
			callto: this.props.app.callto,
			extensionstatus : this.props.app.extensionstatus ? this.props.app.extensionstatus : false,
			dialerposition : localStorage.getItem('dialerposition') ? localStorage.getItem('dialerposition') : 'right'
		};

		this.connection = null;
		this.createWebScoket = this.createWebScoket.bind(this);
		
		this.dialerPopUp = this.dialerPopUp.bind(this);
		this.createContacts = this.createContacts.bind(this);
		this.createLeads = this.createLeads.bind(this);
		this.createServiceCalls = this.createServiceCalls.bind(this);
		this.createActivity = this.createActivity.bind(this);
		this.getTagDetails = this.getTagDetails.bind(this);
		this.createNotes = this.createNotes.bind(this);
		this.closeDialer = this.closeDialer.bind(this);
		this.changeDialerPosition = this.changeDialerPosition.bind(this);
		this.getCustomerDetails = this.getCustomerDetails.bind(this);
		this.updateField = this.updateField.bind(this);
		this.notesOnChange = this.notesOnChange.bind(this);
		this.taggingObj = {};
	}

	componentWillMount(){
		this.createWebScoket();
	}

	notesOnChange(value, callId) {
		let { calldetails } = this.props.app;
		calldetails[callId].notes = value;

		this.props.updateAppState('calldetails', calldetails);
	}

	createNotes(callId) {
		let { calldetails } = this.props.app;
		if(this.props.app.calldetails[callId].createnote && this.taggingObj.id > 0 && this.taggingObj.pagename) {
			var tempData = {
				actionverb : 'Save',
				data : {
					parentid : this.taggingObj.id,
					parentresource : this.taggingObj.pagename,
					notes : this.props.app.calldetails[callId].notes,
					isrestricted : false,
					callid : callId
				}
			};

			axios({
				method : 'post',
				data : tempData,
				url : '/api/notes'
			}).then((response)=> {

				if (response.data.message == 'success') {
					this.updateCallhistory(callId,calldetails);
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}else{
			this.updateCallhistory(callId,calldetails);
		}
	}

	updateCallhistory(callId,calldetails) {

		let tempData = {
			notes : this.props.app.calldetails[callId].notes,
			callid : callId
		};
		axios({
			method : 'post',
			data : tempData,
			url : '/api/common/methods/saveDialerNotes'
		}).then((response)=> {
			calldetails[callId]['disableNote'] = true ;
			this.props.updateAppState('calldetails', calldetails);

			let data = {
				"event": "disableNote",
				"extn": this.props.app.user.extension,
				"callId":callId
			};

			this.connection.send(JSON.stringify(data));

			if (response.data.message != 'success') {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	createContacts(callId) {
		let tempContactObj = {
			param: 'Dialer',
			mobile : this.props.app.calldetails[callId].contactnumber
		};
		this.props.history.push({pathname: '/createContact', params: {...tempContactObj}});
	}

	createLeads(callId) {
		if(this.props.app.calldetails[callId].customerdetails){
			let tempObj = {
				...this.props.app.calldetails[callId].customerdetails,
			};
			if(!tempObj.customerid && tempObj.contactid)
				tempObj.tempcontactid = this.props.app.calldetails[callId].customerdetails.contactid;
			this.props.history.push({pathname: '/createLead', params: {param: 'Dialer', ...tempObj}});
		}
	}

	createServiceCalls(callId) {
		this.props.history.push({pathname: '/createServiceCall', params: {param: 'Dialer', ...this.props.app.calldetails[callId].customerdetails}});
	}

	createActivity(callId) {
		let tempObj = {
			...this.props.app.calldetails[callId].customerdetails,
			callId : callId,
			startdatetime : this.props.app.calldetails[callId].starttime,
			enddatetime : this.props.app.calldetails[callId].endtime,
		};
		this.props.history.push({pathname: '/createActivity', params: {param: 'Dialer',showrelatedtosection:true, ...tempObj}});
	}

	changeDialerPosition(){
		let position = (this.state.dialerposition == 'right') ? 'left' : 'right';
		localStorage.setItem('dialerposition',position);
		this.setState({dialerposition : position});
	}

	getTagDetails(){
		this.taggingObj = {};
		for(var prop in routes) {
			let matchedRoute = matchPath(this.props.location.pathname, {
				path: prop,
				exact: true,
				strict: false
			});
			if(matchedRoute) {
				if(routes[prop].pageoptions && routes[prop].pageoptions.type == 'Details' && matchedRoute.params && matchedRoute.params.id > 0 && this.props.pagejson[routes[prop].pageoptions.title] && this.props.pagejson[routes[prop].pageoptions.title].Details) {
					this.taggingObj = {
						pagename: routes[prop].pageoptions.title,
						displayname : this.props.pagejson[routes[prop].pageoptions.title].Details.pagename,
						id: matchedRoute.params.id
					};
				}
			}
		}
	}

	getCustomerDetails(data) {
		let { calldetails } = this.props.app;
		axios.get(`/api/query/dialerdetailsquery?contactno=${data.customerNo}`).then((response) => {
			if (response.data.message == 'success') {
				if(response.data.main && response.data.main.length >0) {
					calldetails[data.callId].customerdetails = {
						... response.data.main[0],
						contactnumber : data.customerNo
					}
				} else {
					calldetails[data.callId].customerdetails = {
						contactnumber : data.customerNo
					}
				}
			} else {
				calldetails[data.callId].customerdetails = {
					contactnumber : data.customerNo
				}
			}
			this.props.updateAppState('calldetails', calldetails);
		});
	}

	updateField(item, field, value) {
		item[field] = value;
		this.props.updateAppState('calldetails', this.props.app.calldetails);
	}

	closeDialer(callid) {
		let data = {
			"event": "closeDialer",
			"extn": this.props.app.user.extension,
			"callId":callid
		};

		this.connection.send(JSON.stringify(data));
	}

	createWebScoket() {
		this.connection = new WebSocket(`wss://${window.location.host}/dialersocket?extn=${this.props.app.user.extension}`);
		let { calldetails } = this.props.app;
		
		this.connection.onmessage = (msg) => {
			let data = JSON.parse(msg.data);
			let message = data.current;
			if(message.event == 'initiated' && message.callType == 'outbound') {

				for(let prop in calldetails) {
					if(calldetails[prop].starttime)
						calldetails[prop].status = "Completed"
					else
						delete calldetails[prop];
				}

				calldetails[message.callId] = {
					callId : message.callId,
					status : "Connecting",
					customerdetails : {},
					events : [message]
				}
			}

			if(message.event == 'ringing') {
				if(calldetails[message.callId]) {
					calldetails[message.callId].status = (message.callType=='outbound') ? "Ringing" : "Incoming Call";
					calldetails[message.callId].events.push(message);
				} else {
					calldetails[message.callId] = {
						callId : message.callId,
						status : (message.callType=='outbound') ? "Ringing" : "Incoming Call",
						customerdetails : {},
						events : [message]
					}
				}
			}

			if(message.event == 'answered') {
				if(calldetails[message.callId]) {
					calldetails[message.callId].status = "In-progress";
					calldetails[message.callId].starttime = message.eventTimeStamp;
					calldetails[message.callId].events.push(message);
				} else {
					calldetails[message.callId] = {
						callId : message.callId,
						status : 'In-progress',
						starttime : message.eventTimeStamp,
						customerdetails : {},
						events : [message]
					}
				}
			}

			if(message.event == 'completed') {
				if(calldetails[message.callId]) {
					calldetails[message.callId].status = "Completed";
					calldetails[message.callId].endtime =  message.eventTimeStamp;
					calldetails[message.callId].events.push(message);
				} else {
					calldetails[message.callId] = {
						callId : message.callId,
						status : 'Completed',
						endtime : message.eventTimeStamp,
						customerdetails : {},
						events : [message]
					}
				}
				if(data.history && data.history.length > 0) {
					data.history.forEach((item,index)=> {
						if(item.event == 'answered') {
							calldetails[message.callId].starttime = item.eventTimeStamp;
						}
						if(item.event == 'completed') {
							calldetails[message.callId].endtime = item.eventTimeStamp;
						}
					});
				}

				if(!calldetails[message.callId].starttime) {
					delete calldetails[message.callId];
				}
			}

			if(message.event == 'no-answer') {
				if(calldetails[message.callId]) {
					calldetails[message.callId].status = "No-answer";
					calldetails[message.callId].events.push(message);
				} else {
					calldetails[message.callId] = {
						callId : message.callId,
						status : 'No-answer',
						customerdetails : {},
						events : [message]
					}
				}

				if(message.callType == 'inbound'){
					delete calldetails[message.callId];
				}
			}

			if(message.event == 'failed') {
				if(calldetails[message.callId]) {
					calldetails[message.callId].status = "Failed";
					calldetails[message.callId].events.push(message);
				} else {
					calldetails[message.callId] = {
						callId : message.callId,
						status : 'Failed',
						customerdetails : {},
						events : [message]
					}
				}
			}

			if(message.event == 'busy') {
				if(calldetails[message.callId]) {
					calldetails[message.callId].status = "Busy";
					calldetails[message.callId].events.push(message);
				} else {
					calldetails[message.callId] = {
						callId : message.callId,
						status : 'Busy',
						customerdetails : {},
						events : [message]
					}
				}
			}

			if(message.event == 'closeDialer' || message.event == 'abandoned') {
				delete calldetails[message.callId];

				let tempcallidArr = [];
				for (var prop in calldetails) {
					tempcallidArr.push(prop);
				}

				if(tempcallidArr.length > 0)
					calldetails[tempcallidArr[tempcallidArr.length-1]].isactive = true;
			} else {
				for (var prop in calldetails) {
					calldetails[prop].isactive = false;
					if(prop == message.callId) {
						calldetails[prop].isactive = true;
					}
				}
			}

			if(message.event == 'disableNote'){
				if(calldetails[message.callId])
					calldetails[message.callId].disableNote = true;
			}

			this.props.updateAppState('calldetails', calldetails);

			setTimeout(() => {
				if(((message.event == 'initiated' && message.callType == "outbound") || (message.event == 'ringing' && message.callType == "inbound") || (calldetails[message.callId] && Object.keys(calldetails[message.callId].customerdetails).length==0)) && message.event != 'closeDialer')
					this.getCustomerDetails(message);
			}, 0);
		};

		this.connection.onclose = () => {
			console.log("Websocket Connection Closed........................");
			this.connection = null;
			setTimeout(()=>{this.createWebScoket()}, 1000);
		};

		this.connection.onerror = (error) => {
			console.log("Websocket Connection Error........................");
			console.log("Connection Error - ",error);
		};
	}


	dialerPopUp() {
		if(Object.keys(this.props.app.calldetails).length ==0)
			return null;

		this.getTagDetails();

		return(
			Object.keys(this.props.app.calldetails).map((call,index)=>{
				let calldetails = this.props.app.calldetails[call];
				return (
					<div className={`gs-dialer-container ${this.state.dialerposition == 'right' ? 'gs-dialer-container-right' : 'gs-dialer-container-left'}`} key={index}>
						<div className={`gs-dialer-wrapper`}>
							{
								<div className="gs-dialer-container-closebtn">
									<span className="fa fa-close" onClick={()=>{this.closeDialer(calldetails.callId)}}></span>
								</div>
							}
							<div className="gs-dialer-container-arrow">
								{
									(this.state.dialerposition == 'right') ?
										<span className="fa fa-arrow-left" onClick={this.changeDialerPosition}></span>
									: null	
								}
								{
									(this.state.dialerposition == 'left') ?
										<span className="fa fa-arrow-right" onClick={this.changeDialerPosition}></span>
									: null	
								}
							</div>
							<div className="gs-dialer-dialerinfo">
								<div className="gs-dialer-text18 gs-dialer-customername" title={calldetails.customerdetails.name}>
									<span style={{'cursor': 'pointer'}} onClick={()=>{
									this.props.history.replace(`/details/partners/${calldetails.customerdetails.customerid}`);}}><b>{calldetails.customerdetails.name}</b></span>
								</div>
								<div className="gs-dialer-text14 float-left w-100">
									<span style={{'cursor': 'pointer'}} onClick={()=>{
									this.props.history.replace(`/details/contacts/${calldetails.customerdetails.contactid}`);}}>{calldetails.customerdetails.contactname}</span>
								</div>
								<div className="gs-dialer-text12 float-left w-100">{calldetails.customerdetails.contactnumber}</div>

								<div className={`gs-dialer-text11  w-100 float-left ${calldetails.status == 'Completed' ? 'gs-dialer-disconnect-color' : 'gs-dialer-oncall-color'}`} style={{'display': 'flex', 'alignItems': 'center'}}>
									<b>{calldetails.status}</b>
									{calldetails.starttime ? <span className={`gs-dialer-callstatus-texticon ${calldetails.status == 'Completed' ? 'gs-dialer-callstatus-disconnect-bgcolor' : 'gs-dialer-callstatus-oncall-bgcolor'}`}></span> : null}
									{
										((calldetails.status == 'In-progress' || calldetails.status == 'Completed') && calldetails.starttime) ?
											<Timer starttime={calldetails.starttime} callendtime = {calldetails.endtime} />
										: null
									}
								</div>
							</div>
							<div className="d-block w-100 float-left"><hr style={{'borderTop': '1px solid rgb(151, 151, 151)'}}/></div>
							<div className="gs-dialer-actionbtn-container w-100">
								{
									((calldetails.status == 'In-progress' || calldetails.status == 'Completed') && calldetails.starttime)?
										<div className="gs-dialer-wrapper margintop-10" style={{background: "transparent"}}>
											{
												(this.taggingObj && this.taggingObj.id) ? 
													<div style={{'color': '#fff', 'whiteSpace': 'nowrap', "textOverlow" : "ellipsis", "overflow" : "hidden"}}>
														<input type='checkbox' value = {calldetails.createnote} onClick={(e)=>{ this.updateField(calldetails, 'createnote', e.target.checked)}} disabled={calldetails.disableNote}/> Add note to  <span title={document.getElementById("pagetitle").innerHTML}>{document.getElementById("pagetitle").innerHTML}</span>
													</div>
												: null
											}
											<div>
												<textarea className="form-control" style={{'background':'rgba(0, 0, 0, 0.10)','color':'#fff'}} rows={2} value = {calldetails.notes} onChange={(e)=>{this.notesOnChange( e.target.value, calldetails.callId);}} disabled={calldetails.disableNote} />
											</div>
											<div className="gs-dialer-actionbtn-container">
												{
													(!calldetails.disableNote && calldetails.status
													 == 'Completed')?
														<div className="gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color" onClick={()=>{this.createNotes(calldetails.callId)}}>
															Save
														</div>
													:null
												}
											</div>
										</div>
									:null
								}
							</div>
							{calldetails.status == 'Completed' && calldetails.starttime ? <div>
								<div style={{'background':'rgb(151, 151, 151)'}}><hr/></div>
								<div className="gs-dialer-actionbtn-container" style={{'justifyContent' : 'center'}}>
									<div className={`gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color`} onClick={()=>{this.createActivity(calldetails.callId)}}>LOG AS ACTIVITY</div>
								</div>
							</div> : null}
						</div>
						{
							(calldetails.addNotes && !calldetails.disableNote)?
								<div className="gs-dialer-wrapper margintop-10">
									{
										(this.taggingObj && this.taggingObj.id) ? 
											<div style={{'color': '#fff', 'whiteSpace': 'nowrap', "textOverlow" : "ellipsis", "overflow" : "hidden"}}>
												<input type='checkbox' value = {calldetails.createnote} onClick={(e)=>{ this.updateField(calldetails, 'createnote', e.target.checked)}} disabled={calldetails.disableNote}/> Add note to  <span title={document.getElementById("pagetitle").innerHTML}>{document.getElementById("pagetitle").innerHTML}</span>
											</div>
										: null
									}
									<div>
										<textarea className="form-control" style={{'background':'rgba(0, 0, 0, 0.10)','color':'#fff'}} rows={5} value = {calldetails.notes} onChange={(e)=>{this.notesOnChange(e.target.value, calldetails.callId);}} />
									</div>
									<div className="gs-dialer-actionbtn-container">
										<div className="gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color" onClick={()=>{this.setState({'addNotes':false});}}>
											Cancel
										</div>
										{
											(calldetails.status == 'Completed')?
												<div className="gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color" onClick={()=>{this.createNotes(calldetails,'')}}>
													Save
												</div>
											:null
										}
									</div>
								</div>
							:null
						}
						{
							((calldetails.status == 'Completed' || calldetails.status == 'In-progress') && calldetails.starttime) ? 
								<div className="gs-dialer-wrapper margintop-10" style={{padding: '0px'}}>
									<div className="gs-dialer-actionbtn-container">
										{!calldetails.customerdetails.contactid ? <div className="gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color text-center gs-dialer-actionbtn-width" onClick={()=>{this.createContacts(calldetails.callId)}}>
											<span className="fa fa-plus"></span> CONTACT
										</div> : null}
										<div className="gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color text-center gs-dialer-actionbtn-width" onClick={()=>{this.createLeads(calldetails.callId)}}>
											<span className="fa fa-plus"></span> LEAD
										</div>
										<div className={`gs-dialer-btn gs-btn-transparent text-center gs-dialer-actionbtn-width ${(!calldetails.customerdetails.customerid) ? 'gs-dialer-callbtn-inactive' :'gs-dialer-actionbtn-color'}`} onClick={()=>{this.createServiceCalls(calldetails.callId)}}>
											<span className="fa fa-plus"></span> SERVICE CALL
										</div>
									</div>
								</div>				 
							: null
						}
					</div>
				)
			})
		)
	}

	render() {
		return (
			<div>
				{
					(this.connection) ?<div>{this.dialerPopUp()}</div> : null
				}
			</div>
		);
	}
}

export default withRouter(Dialer);
