import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import {updatePageJSONState, updateFormState, updateAppState } from '../actions/actions';
import axios from 'axios';
import { Link, withRouter, matchPath } from 'react-router-dom';
import { commonMethods, modalService, pageValidation } from '../utils/services';
import routes from '../routeconfig';
import { Timer } from './utilcomponents';
import CallTransferModal from './details/calltransfermodal';

class Dialer extends Component {

	constructor(props) {
		super(props);
		this.state = {
			value: '',
			callto: this.props.app.callto,
			callstarttime : null,
			extensionstatus : this.props.app.extensionstatus ? this.props.app.extensionstatus : false,
			notes : null,
			addNotes : false,
			disableNote : false,
			stoptimer: false,
			dialerposition : localStorage.getItem('dialerposition') ? localStorage.getItem('dialerposition') : 'right',
			createNote : false,
			pathname : null,
			contactnumber : null
		};

		this.connection = null;
		this.createWebScoket = this.createWebScoket.bind(this);
		this.hangUP = this.hangUP.bind(this);
		this.returnToQueue = this.returnToQueue.bind(this);
		this.holdCall = this.holdCall.bind(this);
		this.unholdCall = this.unholdCall.bind(this);
		this.answerCall = this.answerCall.bind(this);
		this.dialerPopUp = this.dialerPopUp.bind(this);
		this.createContacts = this.createContacts.bind(this);
		this.createLeads = this.createLeads.bind(this);
		this.createServiceCalls = this.createServiceCalls.bind(this);
		this.createActivity = this.createActivity.bind(this);
		this.getTagDetails = this.getTagDetails.bind(this);
		this.createNotes = this.createNotes.bind(this);
		this.updateUserDialerStatus = this.updateUserDialerStatus.bind(this);
		this.closeDialer = this.closeDialer.bind(this);
		this.changeDialerPosition = this.changeDialerPosition.bind(this);
		this.getCustomerDetails = this.getCustomerDetails.bind(this);
		this.getExtensionDetails = this.getExtensionDetails.bind(this);
		this.callTransfer = this.callTransfer.bind(this);
		this.showBrowserExceptionModal = this.showBrowserExceptionModal.bind(this);
		this.taggingObj = {};
	}

	componentWillMount(){
		this.createWebScoket();
	}

	componentWillReceiveProps(nextProps) {
		if(['Incoming','Incall','On Hold','Connecting'].indexOf(this.props.app.callstatus) ==-1 && nextProps.app.callto && nextProps.app.callto != this.state.callto) {
			this.setState({
				callto: nextProps.app.callto
			}, () => {
				this.initiateCallFromTransaction(this.state.callto);
			});
		}
		
		if(nextProps.app.extensionstatus &&  nextProps.app.extensionstatus != this.state.extensionstatus){
			this.setState({
				extensionstatus : nextProps.app.extensionstatus
			});

			var data = {
			    "type": "setExtnStatus",
			    "extn": this.props.app.user.extension,
			    "allowincoming": this.props.app.user.allowincoming,
			    "agentname" : this.props.app.user.displayname,
			    "status": nextProps.app.extensionstatus ? "Available" : "Not Available"
			};
			if(this.connection && this.connection.readyState == 1){
				this.connection.send(JSON.stringify(data));	
			}else{
				this.props.openModal({
	 			render: (closeModal) => {
	 				return (this.showBrowserExceptionModal(closeModal))}, 
	 			className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		 	});	
			}
		}

		if(this.state.contactnumber && this.state.pathname != nextProps.location.pathname) {
			this.setState({pathname : nextProps.location.pathname});
			for(var prop in routes) {
				let matchedRoute = matchPath(nextProps.location.pathname, {
					path: prop,
					exact: true,
					strict: false
				});

				if(matchedRoute) {
					if(routes[prop].pageoptions && routes[prop].pageoptions.type == 'Details' && ['contacts','partners'].indexOf(routes[prop].pageoptions.title) >=0) {
						this.getCustomerDetails(this.state.contactnumber);
					}
				}
			}
		}
	}


	initiateCallFromTransaction(number) {
		console.log("initiateCallFromTransaction--------",number);
		var data = {
			type : "initiateCall",
    		extn : this.props.app.user.extension,
    		numberToCall : number
		};
		if(this.connection && this.connection.readyState == 1){
			this.connection.send(JSON.stringify(data));
		}else{
			this.props.openModal(modalService['infoMethod']({
				header : 'Error',
				body : 'Unable to connect with Dialer. Please contact admin.',
				btnArray : ['Ok']
			}));
		}
	}

	callTransfer(number) {
		var data = {
			type : "callTransfer",
    		fromextn : this.props.app.user.extension,
    		toextn : number,
    		contactnumber : this.state.contactnumber,
    		callid : this.state.callid
		};
		if(this.connection && this.connection.readyState == 1){
			this.connection.send(JSON.stringify(data));
		}else{
			this.props.openModal(modalService['infoMethod']({
				header : 'Error',
				body : 'Unable to connect with Dialer. Please contact admin.',
				btnArray : ['Ok']
			}));
		}
	}

	createNotes(){
		this.setState({addNotes:false});
		var data = {
			type : "disableNote",
    		extn : this.props.app.user.extension,
    		note : this.state.notes,
    		callid : this.state.callid
		};
		this.connection.send(JSON.stringify(data));
		if(this.state.createnote && this.taggingObj.id > 0 && this.taggingObj.pagename){
			var tempData = {
				actionverb : 'Save',
				data : {
					parentid : this.taggingObj.id,
					parentresource : this.taggingObj.pagename,
					notes : this.state.notes,
					isrestricted : false,
					callid : this.state.callid
				}
			};

			axios({
				method : 'post',
				data : tempData,
				url : '/api/notes'
			}).then((response)=> {
				if (response.data.message != 'success') {
					console.log(response);
				}
			});
		}
	}

	createContacts() {
		let tempContactObj = {
			param: 'Dialer',
			mobile : this.state.call.mobile
		};
		this.props.history.push({pathname: '/createContact', params: {...tempContactObj}});
	}

	createLeads() {
		if(this.state.call && (this.state.call.customerid || (!this.state.call.customerid && !this.state.call.contactid))){
			this.props.history.push({pathname: '/createLead', params: {param: 'Dialer', ...this.state.call}});
		}

		if(this.state.call && !this.state.call.customerid && this.state.call.contactid){
			let tempObj = this.state.call;
			tempObj.tempcontactid = this.state.call.contactid;
			this.props.history.push({pathname: '/createLead', params: {param: 'Dialer', ...tempObj}});
		}

		if(this.state.call && !this.state.call.customerid && !this.state.call.contactid){
			let tempObj = this.state.call;
			this.props.history.push({pathname: '/createLead', params: {param: 'Dialer', ...tempObj}});
		}
		
	}

	createServiceCalls() {
		this.props.history.push({pathname: '/createServiceCall', params: {param: 'Dialer', ...this.state.call}});
	}

	createActivity() {
		this.props.history.push({pathname: '/createSalesActivity', params: {param: 'Dialer',showrelatedtosection:true, ...this.state.call, startdatetime: this.state.callstarttime, enddatetime: this.state.callendtime}});
	}

	changeDialerPosition(){
		let position = (this.state.dialerposition == 'right') ? 'left' : 'right';
		localStorage.setItem('dialerposition',position);
		this.setState({dialerposition : position});
	}

	getTagDetails(){
		this.taggingObj = {};
		for(var prop in routes) {
			let matchedRoute = matchPath(this.props.location.pathname, {
				path: prop,
				exact: true,
				strict: false
			});
			if(matchedRoute) {
				if(routes[prop].pageoptions && routes[prop].pageoptions.type == 'Details' && matchedRoute.params && matchedRoute.params.id > 0 && this.props.pagejson[routes[prop].pageoptions.title] && this.props.pagejson[routes[prop].pageoptions.title].Details) {
					this.taggingObj = {
						pagename: routes[prop].pageoptions.title,
						displayname : this.props.pagejson[routes[prop].pageoptions.title].Details.pagename,
						id: matchedRoute.params.id
					};
				}
			}
		}
	}

	showBrowserExceptionModal(closeModal){
		return (
			<div>
				<div className="react-modal-header">
					<h5>Unable to connect with dialer</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<p>If you are using dialer in this computer for the first time, pls click on this <a target='_blank' href={`https://${this.props.app.feature.dialerWebSocket.ip}:${this.props.app.feature.dialerWebSocket.port}/test`} onClick={()=>{return closeModal();}}>link</a> and accept the browser warning</p>
				</div>
				<div className="react-modal-footer text-right">
					<button type="button" className="btn btn-sm btn-primary" onClick={closeModal}>Ok</button>
				</div>
			</div> 
		);
	}

	updateUserDialerStatus(param) {
		var data = {
		    "type": "setExtnStatus",
		    "extn": this.props.app.user.extension,
		    "allowincoming": this.props.app.user.allowincoming,
		    "agentname" : this.props.app.user.displayname,
		    "status": param ? "Available" : "Not Available"
		};
		
		if(this.connection && this.connection.readyState == 1) {
			this.props.updateAppState('extensionstatus',(param == 'Available') ? true : false);
			this.setState({
				extensionstatus : param
			});
			this.connection.send(JSON.stringify(data));
		}else{
			this.props.openModal({
	 			render: (closeModal) => {
	 				return (this.showBrowserExceptionModal(closeModal))}, 
	 			className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		 	});			
		}
	}

	getCustomerDetails(number){
		axios.get(`/api/query/dialerdetailsquery?contactno=${number}`).then((response) => {
	 		if (response.data.message == 'success') {
	 			if(response.data.main && response.data.main.length >0){
					this.setState({'call' : response.data.main[0],contactnumber : number});
	 			}else{
	 				this.setState({'call' : {
	 					mobile : number
	 				},contactnumber : number});
	 			}
			} else {
	 			this.setState({'call' : {
 					mobile : number
 				},contactnumber : number});
			}
		});
	}

	getExtensionDetails(){
		var data = {
			type : "getAllExtension",
    		extn : this.props.app.user.extension
		};
		this.connection.send(JSON.stringify(data));
	}

	dialerPopUp(){
		if(!this.state.call)
			return null;

		this.getTagDetails();

		return(
			<div id="date-body-cont"  className={`${this.state.dialerposition == 'right' ? 'gs-dialer-container' : 'gs-dialer-container-left'}`}>
				<div className="gs-dialer-wrapper">
					{
						(this.props.app.callstatus == 'Disconnected') ?
							<div className="gs-dialer-container-closebtn">
								<span disabled={(this.props.app.callstatus == 'Incall' || this.props.app.callstatus == 'On Hold') ? true : false} className="fa fa-close" onClick={this.closeDialer}></span>
							</div>
						: null
					}
					<div className="gs-dialer-container-arrow">
						{
							(this.state.dialerposition == 'right') ?
								<span class="fa fa-arrow-left" onClick={this.changeDialerPosition}></span>
							: null	
						}
						{
							(this.state.dialerposition == 'left') ?
								<span class="fa fa-arrow-right" onClick={this.changeDialerPosition}></span>
							: null	
						}
					</div>
					<div className="gs-dialer-dialerinfo">
						<div className="gs-dialer-text18 gs-dialer-customername" title={this.state.call.name}>
							<span style={{'cursor': 'pointer'}} onClick={()=>{
							this.props.history.replace(`/details/partners/${this.state.call.customerid}`);}}><b>{this.state.call.name}</b></span>
						</div>
						<div className="gs-dialer-text14 float-left w-100">
							<span style={{'cursor': 'pointer'}} onClick={()=>{
							this.props.history.replace(`/details/contacts/${this.state.call.contactid}`);}}>{this.state.call.contactname}</span>
						</div>
						<div className="gs-dialer-text12 float-left w-100">{this.state.contactnumber}</div>

						<div className={`gs-dialer-text11  w-100 float-left ${this.props.app.callstatus == 'Disconnected' ? 'gs-dialer-disconnect-color' : (this.props.app.callstatus == 'On Hold' ? 'gs-dialer-onhold-color' : 'gs-dialer-oncall-color')}`} style={{'display': 'flex', 'alignItems': 'center'}}>
							<b>{this.props.app.callstatus} {this.props.app.callstatus == 'Incoming' ? ` call... ${this.state.incomingmessage ? `(${this.state.incomingmessage})` :''}` : ''}</b>
							{this.state.callstarttime ? <span className={`gs-dialer-callstatus-texticon ${this.props.app.callstatus == 'Disconnected' ? 'gs-dialer-callstatus-disconnect-bgcolor' : (this.props.app.callstatus == 'On Hold' ? 'gs-dialer-callstatus-onhold-bgcolor' : 'gs-dialer-callstatus-oncall-bgcolor')}`}></span> : null}
							{
								(this.props.app.callstatus == 'Incall' || this.props.app.callstatus == 'On Hold' || this.props.app.callstatus == 'Disconnected' || this.props.app.callstatus == 'Call Transfering' ) ?
									<Timer starttime={this.state.callstarttime} callendtime = {this.state.callendtime} />
								: null
							}
						</div>
					</div>
					<div className="d-block w-100 float-left"><hr style={{'borderTop': '1px solid rgb(151, 151, 151)'}}/></div>
					<div className="gs-dialer-actionbtn-container w-100">
						<div className={`gs-dialer-callbtn gs-btn-transparent ${(this.props.app.callstatus == 'Incoming' || this.props.app.callstatus == 'On Hold' || this.props.app.callstatus == 'Disconnected' || this.props.app.callstatus == 'Call Transfering') ? 'gs-dialer-callbtn-inactive' : ''}`} onClick={this.hangUP}>
							<div className={`gs-dialer-callbtn-icon-container ${(this.props.app.callstatus == 'Incoming' || this.props.app.callstatus == 'On Hold' || this.props.app.callstatus == 'Disconnected' || this.props.app.callstatus == 'Call Transfering') ? '' : 'gs-dialer-callbtn-icon-endcall'}`}>
								<span style={{'color':`${(this.props.app.callstatus == 'Incoming' || this.props.app.callstatus == 'On Hold' || this.props.app.callstatus == 'Disconnected' || this.props.app.callstatus == 'Call Transfering') ? '#ffffff4f' : 'rgb(255, 59, 48)'}`}} className="fa fa-2x fa-phone"></span>
							</div>
							<span style={{'fontSize':'10px','color':'#fff'}}>End Call</span>
						</div>
						{
							(this.props.app.callstatus != 'On Hold') ? 
								<div className={`gs-dialer-callbtn gs-btn-transparent ${this.props.app.callstatus != 'Incall' ? 'gs-dialer-callbtn-inactive' : ''}`} onClick={this.holdCall} >
									<div className={`gs-dialer-callbtn-icon-container ${this.props.app.callstatus != 'Incall' ? '' : 'gs-dialer-callbtn-icon-holdcall'}`}>
										<span style={{'color':`${(this.props.app.callstatus != 'Incall') ? '#ffffff4f' : 'rgb(97, 173, 255)'}`}} className="fa fa-2x fa-pause"></span>
									</div>
									<span style={{'fontSize':'10px','color':'#fff'}}>Hold Call</span>
								</div>
							: null
						}
						{
							(this.props.app.callstatus == 'On Hold') ? 
								<div className={`gs-dialer-callbtn gs-btn-transparent ${this.props.app.callstatus != 'On Hold' ? 'gs-dialer-callbtn-inactive' : ''}`} onClick={this.unholdCall} >
									<div className={`gs-dialer-callbtn-icon-container ${this.props.app.callstatus != 'On Hold' ? '' : 'gs-dialer-callbtn-icon-holdcall'}`}>
										<span style={{'color':`${(this.props.app.callstatus != 'On Hold') ? '#ffffff4f' : 'rgb(97, 173, 255)'}`}} className="fa fa-2x fa-play"></span>
									</div>
									<span style={{'fontSize':'10px','color':'#fff'}}>Unhold Call</span>
								</div>
							: null
						}

						<div className={`gs-dialer-callbtn gs-btn-transparent ${(this.props.app.callstatus == 'Incall' || this.props.app.callstatus == 'On Hold' || this.props.app.callstatus == 'Disconnected' || this.props.app.callstatus == 'Connecting' || this.props.app.callstatus == 'Call Transfering') ? 'gs-dialer-callbtn-inactive' : ''}`} onClick={this.returnToQueue}>
							<div className={`gs-dialer-callbtn-icon-container ${(this.props.app.callstatus == 'Incall' || this.props.app.callstatus == 'On Hold' || this.props.app.callstatus == 'Disconnected' || this.props.app.callstatus == 'Connecting' || this.props.app.callstatus == 'Call Transfering') ? '' : 'gs-dialer-callbtn-icon-queuecall'}`}>
								<span style={{'color':`${(this.props.app.callstatus == 'Incall' || this.props.app.callstatus == 'On Hold' || this.props.app.callstatus == 'Disconnected' || this.props.app.callstatus == 'Connecting' || this.props.app.callstatus == 'Call Transfering') ? '#ffffff4f' : 'rgb(255, 236, 0)'}`}} className="fa fa-2x fa-repeat"></span>
							</div>
							<span style={{'fontSize':'10px','color':'#fff'}}>Return To Queue</span>
						</div>

						<div className={`gs-dialer-callbtn gs-btn-transparent ${(this.props.app.callstatus == 'Incoming' || this.props.app.callstatus == 'Connecting' || this.state.disableNote) ? 'gs-dialer-callbtn-inactive' : ''}`} onClick={()=>{this.setState({'addNotes':!this.state.addNotes})}}>
							<div className={`gs-dialer-callbtn-icon-container ${(this.props.app.callstatus == 'Connecting' || this.props.app.callstatus == 'Incoming' || this.state.disableNote) ? '' : 'gs-dialer-callbtn-icon-addnote'}`}>
								<span style={{'color':`${(this.props.app.callstatus == 'Connecting' || this.props.app.callstatus == 'Incoming' || this.state.disableNote) ? '#ffffff4f' : 'rgb(0, 231, 144)'}`}} className="fa fa-2x fa-sticky-note"></span>
							</div>
							<span style={{'fontSize':'10px','color':'#fff'}}>Add Note</span>
						</div>

						<div className={`gs-dialer-callbtn gs-btn-transparent ${(['Incoming','Disconnected','Call Transfering','Connecting'].indexOf(this.props.app.callstatus)>=0) ? 'gs-dialer-callbtn-inactive' : ''}`} onClick={this.getExtensionDetails}>
							<div className={`gs-dialer-callbtn-icon-container ${['Incoming','Disconnected','Call Transfering','Connecting'].indexOf(this.props.app.callstatus)>=0 ? '' : 'gs-dialer-callbtn-icon-queuecall'}`}>
								<span style={{'color':`${(['Incoming','Disconnected','Call Transfering','Connecting'].indexOf(this.props.app.callstatus)>=0) ? '#ffffff4f' : 'rgb(255, 236, 0)'}`}} className="fa fa-2x fa-angle-double-right"></span>
							</div>
							<span style={{'fontSize':'10px','color':'#fff'}}>Transfer</span>
						</div>

					</div>
					{this.props.app.callstatus == 'Disconnected' ? <div>
						<div style={{'background':'rgb(151, 151, 151)'}}><hr/></div>
						<div className="gs-dialer-actionbtn-container" style={{'justifyContent' : 'center'}}>
							<div className={`gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color ${this.props.app.callstatus == 'Connecting' ? 'gs-dialer-callbtn-inactive' : ''}`} onClick={this.createActivity}>LOG AS ACTIVITY</div>
						</div>
					</div> : null}
				</div>
				{
					(this.state.addNotes && !this.state.disableNote)?
						<div className="gs-dialer-wrapper margintop-10">
							{
								(this.taggingObj && this.taggingObj.id) ? 
									<div style={{'color': '#fff', 'whiteSpace': 'nowrap', "textOverlow" : "ellipsis", "overflow" : "hidden"}}>
										<input type='checkbox' value = {this.state.createnote} onClick={(e)=>{this.setState({createnote  : e.target.checked})}}/> Add note to  <span title={document.getElementById("pagetitle").innerHTML}>{document.getElementById("pagetitle").innerHTML}</span>
									</div>
								: null
							}
							<div>
								<textarea className="form-control" style={{'background':'rgba(0, 0, 0, 0.10)','color':'#fff'}} rows={5} value = {this.state.notes} onChange={(e)=>{this.setState({notes: e.target.value});}} />
							</div>
							<div className="gs-dialer-actionbtn-container">
								<div className="gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color" onClick={()=>{this.setState({'addNotes':false});}}>
									Cancel
								</div>
								{
									(this.props.app.callstatus == 'Disconnected')?
										<div className="gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color" onClick={this.createNotes}>
											Save
										</div>
									:null
								}
							</div>
						</div>
					:null
				}
				{
					(this.props.app.callstatus != 'Connecting' && this.props.app.callstatus != 'Incoming') ? 
						<div className="gs-dialer-wrapper margintop-10" style={{padding: '0px'}}>
							<div className="gs-dialer-actionbtn-container">
								{!this.state.call.contactid ? <div className="gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color text-center gs-dialer-actionbtn-width" onClick={this.createContacts}>
									<span className="fa fa-plus"></span> CONTACT
								</div> : null}
								<div className="gs-dialer-btn gs-btn-transparent gs-dialer-actionbtn-color text-center gs-dialer-actionbtn-width" onClick={this.createLeads}>
									<span className="fa fa-plus"></span> LEAD
								</div>
								<div className={`gs-dialer-btn gs-btn-transparent text-center gs-dialer-actionbtn-width ${(!this.state.call.customerid) ? 'gs-dialer-callbtn-inactive' :'gs-dialer-actionbtn-color'}`} onClick={this.createServiceCalls}>
									<span className="fa fa-plus"></span> SERVICE CALL
								</div>

							</div>
						</div>				 
					: null
				}
			</div>
		)
	}

	createWebScoket(){
		
		this.connection = new WebSocket(`wss://${this.props.app.feature.dialerWebSocket.ip}:${this.props.app.feature.dialerWebSocket.port}?extn=${this.props.app.user.extension}`);
		
		this.connection.onopen = () => {
		  	console.log("Client Websocket connect");
			var data = {
			    "type": "getExtnStatus",
			    "extn": this.props.app.user.extension
			};

			this.connection.send(JSON.stringify(data));
		};
		
		this.connection.onmessage = (msg) => {
			 var message = JSON.parse(msg.data);

			 console.log(message.type,message);

			 if(message.type == 'setExtnStatus'){
			 	this.props.updateAppState('extensionstatus',(message.status=='Available') ? true : false);
			 }

			 if(message.type == 'getExtnStatus'){
			 	this.props.updateAppState('extensionstatus',(message.status=='Available') ? true : false);
			 }

			 if(message.type == 'closeDialer'){
			 	this.props.updateAppState('showcallpopup',false);
			 	this.props.updateAppState('callstatus',null);
			 	this.props.updateAppState('callid',null);
			 	this.setState({callstarttime:null,callendtime:null,callid:null,contactnumber:null,call:null,notes:null,callto:null,incomingmessage:null});
			 }
			 
			 
			 if(message.type == 'hangupCall' || message.type == 'returnToQueue'){
				this.props.updateAppState('callstatus',null);
			 	if(this.props.app.callstatus != 'Incall')
					this.props.updateAppState('showcallpopup',false);
			 }

			 if(message.type == 'holdCall' || message.type == 'unholdCall'){
			 	this.props.updateAppState('callstatus',(message.type == 'holdCall')? 'On Hold' : 'Incall');
			 }

			 if(message.type == 'showIncomingCall'){
			 	this.props.updateAppState('callstatus','Incoming');
			 	this.setState({disableNote:false});
			 	this.setState({incomingmessage:message.message});
			 	this.getCustomerDetails(message.callerNo);
			 	this.props.updateAppState('showcallpopup',true);
			 }

			 if(message.type == 'callInitiated'){
			 	this.props.updateAppState('showcallpopup',true);
				this.props.updateAppState('callstatus','Connecting');
				this.setState({disableNote:false});
				this.getCustomerDetails(message.numberToCall);
			 }

			 if(message.type == 'callTransfer'){
				this.props.updateAppState('callstatus','Call Transfering');
			 }


			 if(message.type == 'callDisconnect'){
			 	if(!this.props.app.showcallpopup){
			 		this.props.updateAppState('showcallpopup',true);
					this.setState({'call' : {
	 					mobile:this.props.app.callto
	 				}});
			 	}
				
				this.props.updateAppState('callto','');
				this.props.updateAppState('callstatus','Disconnected');

				if(!this.state.callstarttime){
					this.setState({'callstarttime':new Date(),'callendtime':new Date(),'callto':''});
				}else{
					this.setState({'callendtime':new Date(),'callto':''});
				}
			 }

			 if(message.type == 'callAnswered'){
				this.setState({'callstarttime':new Date(),'callid' : message.callid, callendtime: null});
			 	this.props.updateAppState('callstatus','Incall');
			 	this.props.updateAppState('callid',message.callid);	
			 }

			 if(message.type == 'agentNotAnswer'){
				this.props.updateAppState('callstatus','Incall');
				this.props.openModal(modalService['infoMethod']({
					header : 'Info',
					body : `${message.toextn.agentname} did not accept the call. Please try forwarding again, or choose a different Agent`,
					btnArray : ['Ok']
				}));
			 }

			 if(message.type == 'disableNote'){
			 	this.setState({disableNote : true});
			 }

			 if(message.type == 'getAllExtension'){

			 	this.props.openModal({
		 			render: (closeModal) => {
		 				return <CallTransferModal extensions = {message.extensions}  callTransfer={this.callTransfer} closeModal={closeModal} />
		 			}, 
		 			className: {content: 'react-modal-custom-class-60', overlay: 'react-modal-overlay-custom-class'}
			 	});
			 }
		};


		this.connection.onclose = () => {
	        console.log("Websocket Connection Closed........................");
	        this.connection = null;
	        setTimeout(()=>{this.createWebScoket()}, 1000);
	    };

	   this.connection.onerror = (error) => {
	    	console.log("Websocket Connection Error........................");
	        console.log("Connection Error - ",error);
	    };
	}


	closeDialer(){
		var data = {
		    "type": "closeDialer",
		    "extn": this.props.app.user.extension
		};

		this.connection.send(JSON.stringify(data));
	}

	hangUP(){
		var data = {
			type : "hangupCall",
    		extn : this.props.app.user.extension
		};
		this.connection.send(JSON.stringify(data));
	}

	returnToQueue(){
		this.props.updateAppState('showcallpopup',false);
		var data = {
			type : "returnToQueue",
    		extn : this.props.app.user.extension
		};
		this.connection.send(JSON.stringify(data));
	}

	holdCall(){
		var data = {
			type : "holdCall",
    		extn : this.props.app.user.extension
		};
		this.connection.send(JSON.stringify(data));
	}

	unholdCall(){
		var data = {
			type : "unholdCall",
    		extn : this.props.app.user.extension
		};
		this.connection.send(JSON.stringify(data));
	}

	answerCall(){
		var data = {
			type : "answerCall",
    		extn : this.props.app.user.extension
		};
		this.connection.send(JSON.stringify(data));
	}

	render() {
		return (
			<div>
				{
					(this.connection && this.props.app.showcallpopup) ?<div>{this.dialerPopUp()}</div> : null
				}
			</div>
		);
	}
}

export default withRouter(Dialer);
