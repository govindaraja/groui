import React, { Component } from 'react';
import axios from 'axios';
import ReportBuilderForm from '../reportbuilder/reportbuilder';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	};

	render() {
		let { resource, reportid, dashboardid } = this.props;

		if (!reportid)
			return null;

		let keyObj = {
			key: `/reportbuilder/${reportid}`
		};

		let match = {
			path: `/reportbuilder/:id`,
			url: `/reportbuilder/${reportid}`,
			params: {
				id: reportid
			}
		};

		return (
			<ReportBuilderForm
				reportid = {reportid}
				dashboardid = {dashboardid}
				{...keyObj}
				match = {match}
				history={this.props.history}
				openModal = {this.props.openModal}
				isDashboard = {true}
				maximizeWidget = {this.props.maximizeWidget}
				isMaximize = {this.props.isMaximize}
				isCreatePage = {this.props.isCreatePage}
				styleProps = {this.props.styleProps}
			/>
		);
	}
}