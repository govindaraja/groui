import React, { Component } from 'react';
import axios from 'axios';
import moment from 'moment';
import { Field } from 'redux-form';
import { DateEle } from '../formelements';
import { dateValidation } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { search,checkActionVerbAccess } from '../../utils/utils';

export default class extends Component {
	constructor(props) {
		super(props);
		this.addBillingSchedule = this.addBillingSchedule.bind(this);
		this.deleteBillingSchedule = this.deleteBillingSchedule.bind(this);
	};

	addBillingSchedule () {
		this.props.array.push('contractbillingschedules', {});
	}

	deleteBillingSchedule (item,index) {
		this.props.array.splice('contractbillingschedules', index, 1);
	}

	render() {
		if(this.props.resource.contractbillingschedules.length == 0)
			return null;
		
		let suspendaccess = checkActionVerbAccess(this.props.app, 'contractbillingschedules', 'Suspend') ;
		let resource = this.props.resource;

		if(resource.tempflag){
			return null;
		}

		return (
			<div className="row col-md-12 col-sm-12 col-xs-12">
				<div className="col-md-6">
					<table className="table table-bordered">
						<thead>
							<tr>
								<th className="text-center" colSpan="3">{this.props.resource.billingschedule} - Schedule Date</th>
							</tr>
						</thead>
						<tbody>
							{ this.props.resource.contractbillingschedules.map((item, index) => {
								return (
									<tr key={index}>
										<td>
											<Field name={`contractbillingschedules[${index}].scheduledate`}  props = {{ required : true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateValidation]}/>
										</td>
										<td>
											<Field name={`contractbillingschedules[${index}].scheduleenddate`}  props = {{ required : true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateValidation]}/>
										</td>
										<td>
										{
											(!resource.id || ['Draft','Submitted','Revise','Open'].indexOf(resource.status) >=0) ? 
											<button type="button" className="btn gs-form-btn-danger btn-sm btndisable" onClick={()=>this.deleteBillingSchedule(item,index)}>
												<span className="fa fa-trash-o"></span>
											</button> : null
										}
										{	
											((resource.resourceName == 'contracts' && ['Approved','Sent To Customer'].indexOf(resource.status) >=0)) ? 
											((item.invoiceid>0) ? 'Invoiced' : ((item.issuspended) ? 'Suspended' : 'Active')) : null
										}
										</td>
									</tr>
								);
							})}
						</tbody>
					</table>
					<button type="button" className="btn gs-btn-info float-left btn-sm btndisable marginbottom-5" onClick={this.addBillingSchedule}>
						<span className="fa fa-plus"></span>Add
					</button>
				</div>
			</div>
		);
	}
}


class SuspendRemarksModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.suspendSchedule = this.suspendSchedule.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
	}

	suspendSchedule() {
		this.props.callback(this.state.remarks);
		this.props.closeModal();
	}

	inputOnChange(evt) {
		this.setState({
			remarks: evt.target.value
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Remarks for Suspend</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-6 col-sm-12 offset-md-1">
							<textarea className={`form-control ${!this.state.remarks ? 'errorinput' : ''}`} value={this.state.remarks} onChange={this.inputOnChange} required />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.suspendSchedule} disabled={!this.state.remarks}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}