import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { LocalSelect, AutoSelect } from '../utilcomponents';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: {},
			materialReqitemArr: []
		};

		this.onLoad = this.onLoad.bind(this);
		this.addItemreqitem = this.addItemreqitem.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let materialReqitemArr = [];
		var queryString = `/api/query/getmaterialrequirementitemquery?projectid=${this.props.projectid}`;
		axios.get(queryString).then((response)=> {
			if (response.data.message == 'success') {
				let materialReqitemArr = response.data.main;
				for (var i = 0; i < this.props.itemrequestitems.length; i++) {
					for (var j = 0; j < materialReqitemArr.length; j++) {
						if (this.props.itemrequestitems[i].itemid == materialReqitemArr[j].itemid)
							materialReqitemArr[j].checked = true;
					}
				}
				this.setState({
					materialReqitemArr
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	addItemreqitem(id, valueobj) {
		let tempItemreqItem = [];
		for (var i = 0; i < this.state.materialReqitemArr.length; i++) {
			if (this.state.materialReqitemArr[i].checked == true)
				tempItemreqItem.push(this.state.materialReqitemArr[i]);
		}
		this.props.callback(tempItemreqItem);
		this.props.closeModal();
	}

	inputonChange(value) {
		let { search } = this.state;
		search.itemid_name = value;
		this.setState({search});
	}

	checkboxOnChange(value, item) {
		item.checked = value;
		this.setState({
			materialReqitemArr: this.state.materialReqitemArr
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Material Requirement Item Details</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12 col-sm-12 align-self-center">
							<label className="labelclass">Item Name</label>
							<input type="text" className="form-control" value={this.state.search.itemid_name || ''} placeholder="Search by item name" onChange={(evt)=>{this.inputonChange(evt.target.value)}}/>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered" >
								<thead>
									<tr>
										<th></th>
										<th>Item Name</th>
										<th>Quantity</th>
										<th>UOM</th>
										<th>Requested Quantity</th>
									</tr>
								</thead>
								<tbody>
									{search(this.state.materialReqitemArr, this.state.search).map((item, index)=> {
										return (
											<tr key={index}>
												<td className="text-center"><input type="checkbox" checked={item.checked} onChange={(evt)=>{this.checkboxOnChange(evt.target.checked, item)}}/></td>
												<td>{item.itemid_name}</td>
												<td className="text-center">{item.quantity}</td>
												<td className="text-center">{item.uomid_name}</td>
												<td className="text-center">{item.requestedqty}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addItemreqitem}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
