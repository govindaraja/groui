import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import XLSX from 'xlsx';

import { commonMethods, modalService } from '../../utils/services';
import Loadingcontainer from '../loadingcontainer';
import { generateExcelforImport } from  '../../import/excelexport';
import { uomFilter } from '../../utils/filter';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			errors: []
		};

		this.exportExcel = this.exportExcel.bind(this);
		this.fileOnChange = this.fileOnChange.bind(this);
		this.importDataFromSheet = this.importDataFromSheet.bind(this);
		this.openErrorModal = this.openErrorModal.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.checkProjectItemExist = this.checkProjectItemExist.bind(this);
		this.generateData = this.generateData.bind(this);
		this.getProjectItemDetails = this.getProjectItemDetails.bind(this);
		this.validateItem = this.validateItem.bind(this);
		this.getItemCategoryMaster = this.getItemCategoryMaster.bind(this);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	checkProjectItemExist() {
		this.updateLoaderFlag(true);
		let errors = [];
		let resourceArray = [...this.props.resource.estimationitems];

		/*resourceArray.forEach((item) => {
			let itemrefnoprop = item.boqitemsid > 0 ? 'boqitemsid_internalrefno' : (item.boqquoteitemsid > 0 ? 'boqquoteitemsid_internalrefno' : 'internalrefno');

			if(!item[`${itemrefnoprop}`]) {
				errors.push(`Internal Reference no is required for item ${item.boqitemid_name}`);
			}
		});*/

		if(errors.length > 0) {
			return this.setState({
				errors: errors
			}, () => {
				this.openErrorModal();
			});
		}

		if(resourceArray.length > 0) {
			let message = {
				header : 'Warning',
				body : 'You already have items added in this project estimation. If you continue with the import, it will remove all existing items and new items will be added.',
				btnArray : ['Ok','Cancel']
			};
			return this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if(param) {
					$("#projectimportexcelfile").click();
				}
				this.updateLoaderFlag(false);
			}));
		} else {
			$("#projectimportexcelfile").click();
			this.updateLoaderFlag(false);
		}
	}

	fileOnChange(files) {
		this.updateLoaderFlag(true);
		if(files.length > 0) {
			let excelfile = files[0];
			let extensionName = excelfile.name.slice(excelfile.name.lastIndexOf('.')+1, excelfile.name.length);
			let sheetName = 'Project Estimation Items';

			if (['xlsm'].indexOf(extensionName) == -1) {
				return this.setState({
					errors: ["Invalid file format. Please ensure that you are importing the correct file"]
				}, () => {
					this.openErrorModal();
				});
			}

			let reader = new FileReader();
			reader.readAsBinaryString(excelfile);
			reader.onload = (e) => {
				let data = e.target.result;
				let workbook = XLSX.read(data, {type: 'binary'});

				this.setState({
					file: excelfile,
					filename: excelfile.name,
					workbook: workbook,
					errors: [],
					isValid: false,
					importfileurl : ""
				}, () => {
					this.getItemCategoryMaster();
				});
			}
		} else {
			this.setState({
				file: null,
				filename: null,
				sheets: null,
				isValid: false,
				importfileurl: ""
			}, () => {
				this.openErrorModal()
			});
		}
	}

	openErrorModal() {
		this.updateLoaderFlag(false);
		return this.props.openModal({
			render: (closeModal) => {
				return <ErrorAlertModal
					errors = {this.state.errors}
					closeModal = {closeModal} />
			}, className: {
				content: 'react-modal-custom-class',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	exportExcel() {
		this.updateLoaderFlag(true);
		axios.get(`/api/common/methods/downloadprojectrelatedexcel?resourcename=projectestimations&estimationid=${this.props.resource.id}&transactionno=${this.props.resource.estimationno}&projectno=${this.props.resource.projectid ? this.props.resource.projectid_projectno : this.props.resource.projectquoteid_projectquoteno}`).then((response) => {

			if(response.data.message != 'success') {
				this.updateLoaderFlag(false);
				let apiResponse = commonMethods.apiResult(response);
				return this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			let win = window.open(`/print/${response.data.main.filename}?url=${response.data.main.fileurl}`, '_blank');
			if (!win) {
				this.props.openModal(modalService['infoMethod']({
					header : 'Warning',
					body : "Popup Blocker is enabled! Please add this site to your exception list.",
					btnName : ['Ok']
				}));
			}
			this.updateLoaderFlag(false);
		});
	}

	getItemCategoryMaster() {
		let itemcategorymasterObj = {};

		axios.get('/api/itemcategorymaster?field=id,name&filtercondition=').then((response) => {
			if(response.data.message == 'success') {

				response.data.main.forEach(item => {
					itemcategorymasterObj[item.name] = item;
				});
				this.setState({ itemcategorymasterObj }, () => {
					this.importDataFromSheet();
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				this.updateLoaderFlag(false);
			}
		});
	}

	importDataFromSheet() {
		let uomObj = {};
		let errors = [], tempItemnameArr = [];
		let { workbook, itemcategorymasterObj } = this.state;
		let internalrefnoObj = {};
		let sheetName = 'Project Estimation Items';

		for(var prop in this.props.app.uomObj) {
			uomObj[this.props.app.uomObj[prop].name] = this.props.app.uomObj[prop];
		}

		if(!workbook.Sheets[`${sheetName}`]) {
			this.setState({
				errors: [`Sheet name should be ${sheetName}`]
			});
			this.fileOnChange([]);
			return false;
		}

		let sheetItemArray = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {header: 1});

		let estimationitemsArray = [];

		for (var i = 3; i < sheetItemArray.length; i++) {
			let tempobj = {};
			for (var j = 0; j < sheetItemArray[i].length; j++) {
				tempobj[sheetItemArray[2][j]] = sheetItemArray[i][j];
			}
			estimationitemsArray.push(tempobj);
		}

		if(estimationitemsArray.length == 0) {
			this.setState({
				errors: [`${sheetName} sheet must contains atleast one item`]
			});
			this.fileOnChange([]);
			return false;
		}

		estimationitemsArray.forEach((item, index) => {

			if(Object.keys(item).length == 0 || item['Type'] == 'Section')
				return;

			let rowIndex = index + 4;
			item.rowIndex = rowIndex; 

			tempItemnameArr.push(item["Item Name"]);

			if(!item['Internal Ref No'])
				errors.push(`Internal Ref No is mandatory for row - ${rowIndex}`);
			else {
				if(!internalrefnoObj[item['Internal Ref No']])
					internalrefnoObj[item['Internal Ref No']] = [];

				internalrefnoObj[item['Internal Ref No']].push(item);
			}

			if(typeof(item['Item Name']) != 'string') {
				errors.push(`Item Name is mandatory for row - ${rowIndex}`);
			}

			['Qty', 'Rate'].forEach((field) => {
				if(item[field]) {
					item[field] = Number(item[field]);

					if(isNaN(item[field]))
						errors.push(`${field} must contains the number value for row - ${rowIndex}`);
				} else {
					if(field == 'Qty' && (item['Qty'] == null || item['Qty'] == '' || item['Qty'] == undefined) && item['Rate Only'] == 'Yes' && this.props.resource.projectquoteid) 
						return;

					errors.push(`${field} is mandatory for row - ${rowIndex}`);
				}
			});

			if(item['UOM'] && uomObj[item['UOM']]) {
				item.uomid = uomObj[item['UOM']].id;
				item.uomid_name = item['UOM'];
			} else {
				errors.push(`Invalid UOM for row - ${rowIndex}`);
			}

			if(item['Approved Makes']) {
				item.approvedmakes = [];
				let tempMakeArr = item['Approved Makes'].split(',');
				tempMakeArr.forEach(categoryitem => {
					if(itemcategorymasterObj[categoryitem]) {
						item.approvedmakes.push(itemcategorymasterObj[categoryitem].id);
					}
				});
			}
		});

		if(errors.length > 0 || this.state.errors.length > 0) {
			this.setState({
				errors: errors
			});
			this.fileOnChange([]);
			return false;
		}

		axios({
			method : 'post',
			data : {
				actionverb : 'Read',
				data : {
					itemarray: tempItemnameArr,
					type: 'name'
				}
			},
			url : '/api/common/methods/getprojectimportitemdetails'
		}).then((response) => {
			if(response.data.message == 'success') {
				let itemmasterObj = {};
				response.data.main.forEach(item => itemmasterObj[item.name] = item);

				this.getProjectItemDetails(internalrefnoObj, itemmasterObj);
			} else {
				this.setState({
					errors: response.data.errors
				});
				this.fileOnChange([]);
			}
		});
	}

	getProjectItemDetails(internalrefnoObj, itemmasterObj) {
		let estimationidArr = [], projectitemdetailsArr = [];

		let queryStr = this.props.resource.projectquoteid ? `/api/projectquoteitems?field=id,clientrefno,internalrefno,itemid,quantity,uomid,uomconversionfactor,uomconversiontype,description,specification,billinguomid,usebillinguom,billingconversionfactor,billingconversiontype,billingquantity,billingrate,uomconversionfactor,uomconversiontype,displayorder,israteonly,itemmaster/name/itemid,itemmaster/itemtype/itemid,itemmaster/lastpurchasecost/itemid,itemmaster/defaultpurchasecost/itemid,itemmaster/usebillinguom/itemid,uom/name/uomid&filtercondition=projectquoteitems.itemtype='Item' and projectquoteitems.parentid=${this.props.resource.projectquoteid}` : `/api/projectitems?field=id,clientrefno,internalrefno,itemid,quantity,uomid,uomconversionfactor,uomconversiontype,description,specification,billinguomid,usebillinguom,billingconversionfactor,billingconversiontype,billingquantity,billingrate,uomconversionfactor,uomconversiontype,displayorder,itemmaster/name/itemid,itemmaster/itemtype/itemid,itemmaster/lastpurchasecost/itemid,itemmaster/defaultpurchasecost/itemid,itemmaster/usebillinguom/itemid,uom/name/uomid&filtercondition=projectitems.itemtype='Item' and projectitems.parentid=${this.props.resource.projectid}`;

		axios.get(`${queryStr}`).then((response) => {
			if (response.data.message == 'success') {
				projectitemdetailsArr = response.data.main;
				this.validateItem(internalrefnoObj, itemmasterObj, projectitemdetailsArr);
			}
		});
	}

	validateItem (internalrefnoObj, itemmasterObj, projectitemdetailsArr) {
		let errors = [], projectIntRefObj = {};
		let newItemsArray = [];

		projectitemdetailsArr.forEach((item) => {
			if(item.internalrefno)
				projectIntRefObj[item.internalrefno] = item
		});

		Object.keys(internalrefnoObj).forEach(intrefno => {

			let intGroupArr = internalrefnoObj[intrefno];

			if(intrefno != 'Overhead' && !projectIntRefObj[intrefno])
				return errors.push(`Invalid internal ref no ${intrefno} for rows - ${intGroupArr.map(item => item.rowIndex).join()}`);

			if(this.props.resource.projectid > 0 && intrefno != 'Overhead' && (intGroupArr[0]['Rate Only'] != '' && intGroupArr[0]['Rate Only'] != null && intGroupArr[0]['Rate Only'] != undefined)) 
				return errors.push(`For Project Estimation against project should not have value in Rate Only field`);

			if(intrefno != 'Overhead' && this.props.resource.projectquoteid > 0 && projectIntRefObj[intrefno].israteonly) {
				if(intGroupArr.length > 1)
					return errors.push(`This Internal Ref No ${intrefno} Item is Rate Only item so that it cannot be changed to Detailed Estimation`);
				else if(intGroupArr.length == 1 && projectIntRefObj[intrefno].itemid != itemmasterObj[intGroupArr[0]['Item Name']].itemid) {
					return errors.push(`This Internal Ref No ${intrefno} Item is Rate Only item so that it cannot be changed to Detailed Estimation`);
				}
			}

			if(intrefno != 'Overhead' && this.props.resource.projectquoteid > 0 && intGroupArr[0]['Rate Only'] && intGroupArr[0]['Rate Only'].toLowerCase() == 'yes' && !projectIntRefObj[intrefno].israteonly)
				return errors.push(`This Internal Ref No ${intrefno} Item is not Rate Only item and it cannot be changed`);

			if(intrefno != 'Overhead' && projectIntRefObj[intrefno].itemid_itemtype == 'Product') {
				let errorProductFound = false;
				intGroupArr.sort((a,b) => {
					let avalue = a['Item Name'] == projectIntRefObj[intrefno].itemid_name ? 1 : 2;
					let bvalue = b['Item Name'] == projectIntRefObj[intrefno].itemid_name ? 1 : 2;

					return avalue - bvalue;
				});

				if(intGroupArr[0]['Item Name'] != projectIntRefObj[intrefno].itemid_name) {
					errors.push(`This BOQ item's estimation type is Detailed, so this BOQ item should be added as a child for internal reference no - ${intrefno}`);
					errorProductFound = true;
				} else {
					let productitemcount = 0;
					let duplicaterowNo = [];
					intGroupArr.forEach((proditem) => {
						if(proditem['Item Name'] == projectIntRefObj[intrefno].itemid_name) {
							productitemcount++;
							if(productitemcount > 1)
								duplicaterowNo.push(proditem.rowIndex);
						}
					});

					if(productitemcount > 1) {
						errors.push(`Product Item "${projectIntRefObj[intrefno].itemid_name}" must be added once for the internal reference ${intrefno}. Please remove duplicate item in row - ${duplicaterowNo.join()}`);
						errorProductFound = true;
					}

					if(intGroupArr[0].uomid != projectIntRefObj[intrefno].uomid) {
						errors.push(`For Product Item UOM is mismatch with parent transaction uom for row - ${intGroupArr[0].rowIndex}`);
						errorProductFound = true;
					}
					if(intGroupArr[0]['Qty'] != projectIntRefObj[intrefno].quantity) {
						errors.push(`For Product Item Quantity is mismatch with parent transaction quantity for row - ${intGroupArr[0].rowIndex}`);
						errorProductFound = true;
					}
				}
				if(errorProductFound)
					return false;
			}

			intGroupArr.forEach((boqitem, boqindex) => {
				if(!itemmasterObj[boqitem['Item Name']])
					return errors.push(`Invalid Item Name for row - ${boqitem.rowIndex}`);

				if(itemmasterObj[boqitem['Item Name']].itemtype == 'Expense')
					return errors.push(`Expense item should not be added for row - ${boqitem.rowIndex}`);

				let itemObj = itemmasterObj[boqitem['Item Name']];
				let boqfieldname = this.props.resource.projectquoteid ? 'boqquoteitemsid' : 'boqitemsid';
				let tempObj = {
					[boqfieldname]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].id : null,
					[`${boqfieldname}_quantity`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].quantity : null,
					[`${boqfieldname}_description`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].description : null,
					[`${boqfieldname}_specification`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].specification : null,
					[`${boqfieldname}_uomid`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].uomid : null,
					[`${boqfieldname}_clientrefno`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].clientrefno : null,
					[`${boqfieldname}_internalrefno`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].internalrefno : null,
					[`${boqfieldname}_estimatedqty`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].quantity : null,
					[`${boqfieldname}_displayorder`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].displayorder : null,
					[`${boqfieldname}_israteonly`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].israteonly : null,
					[`boqitemid`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].itemid : null,
					[`boqitemid_name`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].itemid_name : null,
					[`boqitemid_itemtype`]: intrefno != 'Overhead' ? projectIntRefObj[intrefno].itemid_itemtype : null,
					internalrefno: boqitem['Internal Ref No'],
					itemid: itemObj.id,
					itemid_name: itemObj.name,
					itemid_keepstock: itemObj.keepstock,
					itemid_issaleskit: itemObj.issaleskit,
					itemid_itemtype: itemObj.itemtype,
					description: boqitem['Description'] ? boqitem['Description'] : itemObj.description,
					uomid: boqitem.uomid,
					uomid_name: boqitem.uomid_name,
					itemid_usebillinguom: itemObj.itemid_usebillinguom,
					itemid_isuseforinternallabour: itemObj.isuseforinternallabour,
					usebillinguom: itemObj.usebillinguom,
					rate: boqitem['Rate'],
					quantity: intrefno != 'Overhead' && this.props.resource.projectquoteid > 0 && projectIntRefObj[intrefno].israteonly ? 0 : boqitem['Qty'],
					approvedmakes: boqitem.approvedmakes
				};

				itemObj.alternateuomObj = {};
				itemObj.alternateuoms.forEach(uom => itemObj.alternateuomObj[uom.id] = uom);

				if(!itemObj.alternateuomObj[boqitem.uomid]) {
					return errors.push(`Invalid UOM for row - ${boqitem.rowIndex}`);
				}

				if(intrefno != 'Overhead' && (projectIntRefObj[intrefno].itemid_itemtype == 'Product' && (intGroupArr.length == 1 || (intGroupArr.length > 1 && boqindex == 0)))) {
					tempObj.uomconversiontype = projectIntRefObj[intrefno].uomconversiontype;
					tempObj.uomconversionfactor = projectIntRefObj[intrefno].uomconversionfactor;
					tempObj.alternateuom = tempObj.uomconversionfactor ? true : false;

					if(tempObj.usebillinguom) {
						tempObj.billinguomid = projectIntRefObj[intrefno].billinguomid;
						tempObj.billingconversiontype= projectIntRefObj[intrefno].billingconversiontype;
						tempObj.billingconversionfactor = projectIntRefObj[intrefno].billingconversionfactor;
						tempObj.billingrate = Number(((tempObj.rate / tempObj.billingconversionfactor) * (tempObj.uomconversionfactor ? tempObj.uomconversionfactor : 1)).toFixed(this.props.app.roundOffPrecision));
						tempObj.billingquantity = projectIntRefObj[intrefno].billingquantity;
					}
				} else {
					tempObj.alternateuom = itemObj.alternateuomObj[boqitem.uomid].alternateuom;
					tempObj.uomconversiontype = itemObj.alternateuomObj[boqitem.uomid].conversiontype;
					tempObj.uomconversionfactor = itemObj.alternateuomObj[boqitem.uomid].alternateuom ? itemObj.alternateuomObj[boqitem.uomid].conversionrate : null;

					if(tempObj.usebillinguom) {
						tempObj.billinguomid = tempObj.uomid;
						tempObj.billingconversiontype= tempObj.alternateuom ? tempObj.uomconversiontype : 'Fixed';
						tempObj.billingconversionfactor = tempObj.alternateuom ? tempObj.uomconversionfactor : 1;
						tempObj.billingrate = tempObj.rate;
						tempObj.billingquantity = tempObj.quantity;
					}
				}

				newItemsArray.push(tempObj);
			});
		});

		if(errors.length > 0) {
			this.setState({errors});
			this.fileOnChange([]);
		} else {
			this.generateData(newItemsArray);
		}
	}

	generateData(newItemsArray) {

		let uniqueItemsArr = [];
		let uniqueItemObj = {};
		let overheaditems = [], estimationitems = [];
		let projectitemprop = this.props.resource.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';

		newItemsArray.forEach((item) => {
			if(item[projectitemprop] > 0) {
				if(!uniqueItemObj[item[projectitemprop]]) {
					uniqueItemObj[item[projectitemprop]] = {
						[`${projectitemprop}`]: item[projectitemprop],
						[`${projectitemprop}_displayorder`]: item[`${projectitemprop}_displayorder`],
						items: []
					};
				}
				uniqueItemObj[item[projectitemprop]].items.push(item);
			} else {
				item.internalrefno = 'Overhead';
				overheaditems.push(item);
			}
		});

		uniqueItemsArr = Object.keys(uniqueItemObj).map(prop => uniqueItemObj[prop]);

		//uniqueItemsArr.sort((a, b) => a[`${projectitemprop}_displayorder`] - b[`${projectitemprop}_displayorder`] );

		uniqueItemsArr.forEach((item) => {
			let tempParentobj = {
				...item.items[0],
				estimationtype : item.items.length > 1 || (item.items[0].itemid != item.items[0].boqitemid) ? 'Detailed' : 'Simple',
				isparent: true
			};

			estimationitems.push(tempParentobj);

			if(tempParentobj.estimationtype == 'Detailed') {
				item.items.forEach((propitem) => {
					propitem.isparent = false;
					estimationitems.push(propitem);
				});
			}
		});

		estimationitems.push({
			isoverhead: true,
			internalrefno: 'Overhead',
			rate: 0
		});
		estimationitems.push(...overheaditems);

		this.props.updateFormState(this.props.form, {
			[`estimationitems`] : estimationitems
		});

		setTimeout(() => {
			this.props.computeFinalRate();
		}, 100);

		this.props.openModal(modalService['infoMethod']({
			header : 'Success',
			body : `Items are imported successfully`,
			btnArray : ['Ok']
		}));
		this.updateLoaderFlag(false);
	}

	render() {
		return (
			<div className="float-left">
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>

				<input type="file" accept={'.xlsm'} id="projectimportexcelfile" className="form-control hide" value={this.state.importfileurl || ''} onChange={(evt) => {this.fileOnChange(evt.target.files)}} />
				<button type="button" className="btn btn-sm gs-form-btn-success btn-width" onClick={this.checkProjectItemExist} title={`Import Estimation Items from excel`} style={{marginRight: '8px'}}><i className="fa fa-download"></i>Import</button>

				<button type="button" className="btn btn-sm gs-form-btn-success btn-width" onClick={this.exportExcel} title={`Export the sample excel`}><i className="fa fa-upload"></i>Download Template</button>
			</div>
		);
	}
});

export class ErrorAlertModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.renderError = this.renderError.bind(this);
	}

	renderError() {
		return this.props.errors.map((error, index) => {
			return(
				<div className="form-group col-md-12" key={index}>
					<div className="alert alert-info"  style={{overflowWrap: 'break-word'}}>{error}</div>
				</div>
			);
		})
	}

	render() {
		if(this.props.errors.length == 0)
			return null;

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Warning</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							{this.renderError()}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
