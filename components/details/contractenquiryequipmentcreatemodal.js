import React, { Component } from 'react';
import axios from 'axios';

import { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { AutoMultiSelect } from '../utilcomponents';

import Loadingcontainer from '../loadingcontainer';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			equipmentArray: [],
			loaderflag: false
		};
		this.onLoad = this.onLoad.bind(this);
		this.add = this.add.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.createEquipment = this.createEquipment.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.equipmentidOnChange = this.equipmentidOnChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let equipmentArray = [];
		let tempResourceObj = JSON.parse(JSON.stringify(this.props.resource));

		tempResourceObj.contractenquiryitems.forEach(item => {
			if(!item.equipmentid && item.itemid > 0) {
				item.param = 'contractenquiries';
				item.partnerid = this.props.resource.customerid;
				item.type = 'Equipment';
				item.tempqty = item.capacityfield ? item.qtyinnos : item.quantity;
				item.equipmentidArr = [];
				equipmentArray.push(item);
			}
		});

		this.setState({ equipmentArray });
	}

	createEquipment(item, index) {
		let itemmasterCustomField = this.props.getCustomFields('itemmaster', 'string', false, this.props.app);

		axios.get(`/api/itemmaster?field=id,description,${(itemmasterCustomField ? ','+itemmasterCustomField : '' )}&filtercondition=itemmaster.id=${item.itemid}`).then((response) => {
			if(response.data.message == 'success') {
				item.description = response.data.main[0].description;
				response.data.main.forEach(itemmasterprop => {
					for(var prop in itemmasterprop) {
						if(prop != 'id' && prop.includes('zzz') && !item[prop]) {
							item[prop] = (item['capacity'] > 0 && item['capacityfield'] == prop) ? item['capacity'] : itemmasterprop[prop];
						}
					}
				});

				this.props.createOrEdit('/createEquipment', null, item, (valueObj) => {
					this.updateLoaderFlag(true);
					let equipmentArray = [...this.state.equipmentArray];
					equipmentArray[index].equipmentidArr = equipmentArray[index].equipmentidArr ? equipmentArray[index].equipmentidArr : [];
					equipmentArray[index].equipmentidArr.push(valueObj.id);
					equipmentArray[index].isOpen = true;
					this.setState({ equipmentArray }, () => {
						let equipmentArray = [...this.state.equipmentArray];
						equipmentArray[index].isOpen = false;
						this.setState({ equipmentArray });
						this.updateLoaderFlag(false);
					});
				});
			}
		});
	}

	equipmentidOnChange(value, item) {
		if(item.tempqty >= value.length) {
			item.equipmentidArr = value;
			this.setState({ equipmentArray: this.state.equipmentArray });
		}
	}

	add() {
		let tempEquipmentArray = [], tempEquipmentidArr = [], errorArray = [];
		this.state.equipmentArray.forEach((item) => {
			if(item.equipmentidArr.length != item.tempqty) {
				errorArray.push(`Pick ${item.tempqty} equipments for item "${item.itemid_name}"`);
			}
			item.equipmentidArr.forEach(equipitem => {
				item.equipmentid = equipitem;
				tempEquipmentArray.push(JSON.parse(JSON.stringify(item)));
				tempEquipmentidArr.push(equipitem);
			});
		});

		if(errorArray.length > 0) {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : errorArray,
				btnArray : ['Ok']
			}));
		}

		let equipmentCustomField = this.props.getCustomFields('equipments', 'string', false, this.props.app);

		axios.get(`/api/equipments?field=id,displayname,partnerid,serialno,remarks,itemid,invoiceno,invoicedate,location${(equipmentCustomField ? ','+equipmentCustomField : '' )}&filtercondition=equipments.id IN (${tempEquipmentidArr})`).then((response) => {
			if(response.data.message == 'success') {
				tempEquipmentArray.forEach(equipitem => {
					response.data.main.forEach(item => {
						if(equipitem.equipmentid == item.id) {
							equipitem.equipmentid_displayname = item.displayname;
							equipitem.equipmentid_partnerid = item.partnerid;
							equipitem.equipmentid_itemid = item.itemid;
							equipitem.equipmentid_serialno = item.serialno;
							equipitem.equipmentid_remarks = item.remarks;
							equipitem.equipmentid_invoiceno = item.invoiceno;
							equipitem.equipmentid_invoicedate = item.invoicedate;
							equipitem.equipmentid_location = item.location;

							for(var prop in item) {
								if(prop.indexOf('zzz') >= 0) {
									equipitem[`equipmentid_${prop}`] = item[prop];
								}
							}
						}
					});
				});
				this.props.callback(tempEquipmentArray);
				this.props.closeModal();
			}
		});
	}

	closeModal() {
		this.props.closeModal();
	}

	render() {
		if(this.state.equipmentArray.length == 0)
			return this.closeModal();

		let disableBtn = false;
		this.state.equipmentArray.forEach((item) => {
			if(item.equipmentidArr.length != item.tempqty) {
				disableBtn = true;
			}
		});

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Contract Equipments</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
					<div className="row">
						<div className="col-md-12 col-sm-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th className='text-center' style={{width:'35%'}}>Item Name</th>
										<th className='text-center' style={{width:'10%'}}>Quantity</th>
										<th className='text-center'>Pick Equipment</th>
										<th style={{width:'10%'}}></th>
									</tr>
								</thead>
								<tbody>
									{this.state.equipmentArray.map((item, index) => {
										return (
											<tr key={index}>
												<td>{item.itemid_name}</td>
												<td className="text-center">{item.tempqty}</td>
												<td>
													{item.isOpen ? null : <AutoMultiSelect resource={'equipments'} fields={'id,displayname'} displaylabel={'displayname'} label={'displayname'} valuename={'id'} filter={`equipments.partnerid=${item.partnerid} and equipments.itemid=${item.itemid}`} value={item.equipmentidArr}  onChange={(value) => this.equipmentidOnChange(value, item)}  className={`${item.tempqty != item.equipmentidArr.length ? 'errorinput' : ''}`}   />}
												</td>
												<td className="text-center"><button type="button" className="btn btn-sm gs-btn-success" onClick={()=>this.createEquipment(item, index)} disabled={item.tempqty == item.equipmentidArr.length}><i className="fa fa-plus"></i>Equipment</button></td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}>Cancel</button>
								<button type="button" className={`btn btn-sm gs-btn-success btn-width`} onClick={this.add} disabled={disableBtn}><i className="fa fa-plus"></i>Contract</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
