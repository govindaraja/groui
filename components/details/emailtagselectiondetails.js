import React, { Component } from 'react';
import { connect } from 'react-redux';


export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			resourceFields : []
		};

		this.copyToClipBoard = this.copyToClipBoard.bind(this);
		this.filterFunction = this.filterFunction.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
	}

	componentWillMount() {
		this.state.resourceFields = this.props.resource.resourceFields;
	}

	componentWillReceiveProps(nextProps) {
		this.state.resourceFields = nextProps.resource.resourceFields
	}

	filterFunction(array, search) {
		let tempArr = [];

		if(search == '' || search == null || search == undefined)
			return array;

		let filterArray = [];

		array.forEach((a) => {
			let string_a = (`${a.field}`).toLowerCase();
			let searchString = search.toLowerCase();
			if(string_a.indexOf(searchString) >= 0)
				filterArray.push(a);
		});

		return filterArray;
	}

	copyToClipBoard(param) {
		param.copied = true;
		this.setState({
			resourceFields: this.state.resourceFields
		}, () => {
			setTimeout(() => {
				param.copied = false;
				this.setState({
					resourceFields: this.state.resourceFields
				});
			}, 1500);
		});

		const el = document.createElement('textarea');
		el.value = `{{${param.field}}}`;
		document.body.appendChild(el);
		el.select();
		document.execCommand('copy');
		document.body.removeChild(el);
	}

	iconOnChange(searchField) {
		let { resourceFields, showadd } = this.state;
		showadd = !showadd;

		resourceFields.forEach((item) => {
			if(item.field == searchField) {
				item.showadd = showadd;
				return true;
			}
		});

		this.setState({
			resourceFields,
			showadd
		});
	}

	inputonChange(evt) {
		let { search } = this.state;
		search = evt.target.value;

		this.setState({search});
	}

	render() {
		const fieldli = {
			listStyle : 'outside none none',
			borderRadius : '5px',
			position : 'relative',
			display : 'block',
			padding : '6px 15px',
			backgroundColor : 'rgb(255,255,255)',
			border : '1px solid rgb(221,221,221)',
			marginBottom : '10px',
			cursor : 'pointer'
		};

		if(!this.state.resourceFields || this.state.resourceFields.length == 0)
			return null;
		return (
			<div className="col-md-12 col-sm-12">
				<div className="row">
					<div className="col-md-12 form-group">
						<input type="text" className="form-control" value={this.state.search || ''} placeholder="Search Fields..." onChange={(evt) =>{this.inputonChange(evt)}}/>
					</div>
				</div>
				<div className="row">
					<div className="col-md-12" style={{height: '300px', maxHeight: '300px', overflowY: 'scroll'}}>
						<ul style={{paddingLeft:'0px'}}>
							{this.filterFunction(this.state.resourceFields, this.state.search).map((secprop, index) => {
								return <li className="emailtemplate-tag" key={index} onClick={(evt) => {this.copyToClipBoard(secprop);evt.stopPropagation();}} >
									<a>{secprop.field}</a>
									<div className="float-right">
										<span className={`${secprop.copied ? 'gs-print-tag-copied' : 'gs-print-tag-copy bg-light'}`}><span>{secprop.copied ? <i className="fa fa-check marginright-5"></i> : ''}</span>{secprop.copied ? 'Tag Copied' : 'Click to Copy'}</span>
										<div style={{width: '15px', float: 'left'}}>
											<i className={`${secprop.formatted ? 'show' : 'hide'} fa fa-star`} style={{fontSize:'10px'}}></i>
											<a className={`${(secprop.additionalFields && secprop.additionalFields.length > 0) ? 'show' : 'hide'} float-right`} onClick={(evt) => {this.iconOnChange(secprop.field);evt.stopPropagation();}}>
													<span className={`${(!secprop.showadd && secprop.additionalFields && secprop.additionalFields.length > 0) ? 'show' : 'hide'} fa fa-chevron-down`}></span>
													<span className={`${(secprop.showadd && secprop.additionalFields && secprop.additionalFields.length > 0) ? 'show' : 'hide'} fa fa-chevron-up`}></span>
											</a>
										</div>
									</div>
									{(secprop.showadd && secprop.additionalFields && secprop.additionalFields.length > 0) ? <ul style={{paddingLeft:'0px',marginTop:'10px'}}>
										{secprop.additionalFields.map((thirdprop, thirdindex) => {
											return (
													<li style={fieldli} key={thirdindex} onClick={(evt) => {this.copyToClipBoard(thirdprop);evt.stopPropagation();}}>
														<a>{thirdprop.field}</a>
														<div className="float-right">
															<span className={`${thirdprop.copied ? 'gs-print-tag-copied' : 'gs-print-tag-copy bg-light'}`}><span>{thirdprop.copied ? <i className="fa fa-check marginright-5"></i> : ''}</span>{thirdprop.copied ? 'Tag Copied' : 'Click to Copy'}</span>
														</div>
													</li>
												);
											})}
									</ul> : null}
								</li>
							})}
						</ul>
					</div>
				</div>
			</div>
		);
	}
});
