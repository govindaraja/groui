import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Field, FieldArray, reduxForm } from 'redux-form';
import axios from 'axios';

import { updateFormState } from '../../actions/actions';
import { ChildElement } from '../../containers/details';
import { commonMethods, modalService, taxEngine } from '../../utils/services';

class EstimatedCostBreakupDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.updateCost = this.updateCost.bind(this);
	}

	updateCost(value) {
		this.props.closeModal();
	}

	render() {
		let disableclass = this.props.resource && ['Approved'].indexOf(this.props.resource.status) >= 0 ? 'disablediv' : '';

		return (
			<div className={`react-outer-modal ${disableclass}`}>
				<div className="react-modal-header">
					<h5 className="modal-title">Estimation Details(Overheads)</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12">
							<div className="card marginbottom-15 borderradius-0 gs-card">
								<div className="card-header borderradius-0 card-header-custom gs-card-header">Materials</div>
								<div className="card-body" style={{paddingTop : '5px'}}>
									<div className="row responsive-form-element">
										<FieldArray name={`overheadobj.productitems`} json={this.props.pagejson.productitems} getFieldString={this.props.getFieldString} resource={this.props.resource} checkCondition={this.props.checkCondition} array={this.props.array} app={this.props.app} updateFormState={this.props.updateFormState} form={this.props.form} component={ChildElement} />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<div className="card marginbottom-15 borderradius-0 gs-card">
								<div className="card-header borderradius-0 card-header-custom gs-card-header">Sub Contract</div>
								<div className="card-body" style={{paddingTop : '5px'}}>
									<div className="row responsive-form-element">
										<FieldArray name={`overheadobj.subcontractitems`} json={this.props.pagejson.subcontractitems} getFieldString={this.props.getFieldString} resource={this.props.resource} checkCondition={this.props.checkCondition} array={this.props.array} app={this.props.app} updateFormState={this.props.updateFormState} form={this.props.form} component={ChildElement}  />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div className="row">
						<div className="col-md-12">
							<div className="card marginbottom-15 borderradius-0 gs-card">
								<div className="card-header borderradius-0 card-header-custom gs-card-header">Internal Labour</div>
								<div className="card-body" style={{paddingTop : '5px'}}>
									<div className="row responsive-form-element">
										<FieldArray name={`overheadobj.labouritems`} json={this.props.pagejson.labouritems} getFieldString={this.props.getFieldString} resource={this.props.resource} checkCondition={this.props.checkCondition} array={this.props.array} app={this.props.app} updateFormState={this.props.updateFormState} form={this.props.form} component={ChildElement} />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.updateCost}><i className="fa fa-check-square-o"></i>Done</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default EstimatedCostBreakupDetails;