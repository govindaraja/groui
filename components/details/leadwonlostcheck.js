import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			quoteArray: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.openQuote = this.openQuote.bind(this);
		this.save = this.save.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		if (this.props.actionVerb == 'Won') {
			axios.get(`/api/quotes?field=id,quoteno&filtercondition=quotes.leadid=${this.props.resource.id}`).then((response)=> {
				if (response.data.message == 'success') {
					this.setState({quoteArray: response.data.main});
				} else {
					var apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}
	}

	openQuote () {
		this.props.closeModal();
	}

	save() {
		this.props.closeModal();
		this.props.callback();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Mark Lead as {this.props.actionVerb}</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						{this.props.actionVerb=="Lost" ? <div className="col-md-12">
							<p>This will also mark the quotations as lost. Continue ?</p>
						</div> : null }

						{this.props.actionVerb=="Won" ? <div className="col-md-12 col-sm-12 col-xs-12">
							<p>This lead has quotations created. Please open the corresponding quotation and mark it as won</p>
							{this.state.quoteArray.map((item, index)=> {
								return (<div className='col-md-4 col-sm-6 col-xs-12' key={index}>
										<a href={`#/details/quotes/${item.id}`} onClick={this.openQuote}>{item.quoteno}</a>
									</div>
								);
							})}
						</div> : null }
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								{this.props.actionVerb == "Lost" ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.save}><i className="fa fa-check-square-o"></i>Ok</button> : null }
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
