import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { currencyFilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			profitarray: [],
			costarray: []
		};
		this.getProfitAnalysis = this.getProfitAnalysis.bind(this);
	}

	componentWillMount() {
		this.callCalculate();
	}

	callCalculate() {
		this.getProfitAnalysis((itemArray, overheadcost) => {
			this.calculateProfit(itemArray, overheadcost);
		});
	}

	calculateProfit(boqArray, overheadcost) {
		boqArray.sort((a, b) => a.childids.length - b.childids.length);

		let boqObj = {};
		boqArray.forEach(item => boqObj[item.id] = item);
		let revenue = 0, cost = 0, profit, profitpercentage;

		boqArray.forEach(item => {
			if(item.itemtype != 'Section') {
				revenue += (item.amount || 0);
				cost += (item.costamount || 0);

				item.cost = item.cost ? item.cost : 0;
				item.profit = item.rate - item.cost;
				item.profitpercentage = Number(((item.profit / item.rate) * 100).toFixed(2));

				return null;
			}

			item.costamount = 0;
			item.amount = 0;
			item.cost = 0;
			item.rate = 0;

			item.childids.forEach(id => {
				if(boqObj[id].itemtype != 'Section') {
					item.cost += (boqObj[id].cost || 0);
					item.rate += (boqObj[id].rate || 0);
				}
			});

			item.costamount = Number(item.costamount.toFixed(this.props.app.roundOffPrecision));
			item.amount = Number(item.amount.toFixed(this.props.app.roundOffPrecision));
			cost +=  Number(item.costamount.toFixed(this.props.app.roundOffPrecision));
			item.profit = item.rate - item.cost;
			item.profitpercentage = item.rate > 0 ? Number(((item.profit / item.rate) * 100).toFixed(2)) : null;
		});

		boqArray.sort((a, b) => a.displayindex - b.displayindex);

		if(cost && revenue) {
			profit = revenue - (cost + overheadcost);
			profitpercentage = Number(((profit / revenue) * 100).toFixed(2));
		}

		this.setState({
			profitarray: boqArray,
			revenue,
			cost,
			profit,
			profitpercentage,
			overheadcost
		});
	}

	getProfitAnalysis(callback) {
		let itemArray = [],
		itemDetailArray = [],
		totaloverheadcost = 0;

		let itemprop = this.props.resourcename == 'projectquotes' ? 'boqquoteitemsid' : 'boqitemsid';
		let itemqtyprop = this.props.resourcename == 'projects' ? 'boqitemsid_estimatedqty' : 'boqquoteitemsid_estimatedqty'

		let resourceitems = [...this.props.resourceitems];
		let boqObj = {};

		resourceitems.forEach((item, itemindex) => {
			let temprate = item.rate ? item.finalratelc : 0;

			boqObj[item.id] = {
				id: item.id,
				clientrefno: item.clientrefno,
				internalrefno: item.internalrefno,
				itemtype: item.itemtype,
				itemid: item.itemid ? item.itemid : null,
				itemid_name: item.itemtype == 'Section' ? item.name : item.itemid_name,
				itemid_itemtype: item.itemtype == 'Section' ? null : item.itemid_itemtype,
				rate: temprate,
				cost: 0,
				amount: item.itemid ? temprate * item.quantity : 0,
				qty: item.itemid ? item.quantity : null,
				estimatedqty: item.itemid > 0 ? item.estimatedqty : null,
				israteonly: item.itemid > 0 ? item.israteonly : false,
				usebillinguom: item.usebillinguom,
				uomconversionfactor: item.uomconversionfactor,
				uomconversiontype: item.uomconversiontype,
				billingconversionfactor: item.billingconversionfactor,
				billingconversiontype: item.billingconversiontype,
				billinguomid: item.billinguomid,
				billingquantity: item.billingquantity,
				billingrate: item.billingrate,
				estimatedrate: null,
				estimatedamount: null,
				displayindex: itemindex,
				childids: [],
				estimationitemsArr: []
			};
		});

		axios.get(`/api/projectestimationitems?&field=id,boqquoteitemsid,boqitemsid,boqitemid,itemid,description,specification,quantity,rate,amount,projectquoteitems/internalrefno/boqquoteitemsid,projectquoteitems/clientrefno/boqquoteitemsid,projectitems/internalrefno/boqitemsid,projectitems/clientrefno/boqitemsid,itemmaster/name/boqitemid,itemmaster/defaultpurchasecost/itemid,itemmaster/lastpurchasecost/itemid,itemmaster/itemtype/boqitemid,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/issaleskit/itemid,itemmaster/uomgroupid/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/itemtype/itemid,uomconversiontype,uomconversionfactor,uomid,uom/name/uomid,billinguomid,usebillinguom,billingconversionfactor,billingconversiontype,billingquantity,activitytypeid,timesheetactivitytypes/name/activitytypeid,remarks,projectestimations/projectquoteid/parentid,projectestimations/currencyid/parentid,projectquoteitems/israteonly/boqquoteitemsid&filtercondition=projectestimations_parentid.status != 'Cancelled' and ${this.props.resourcename == 'projectquotes' ? `projectestimations_parentid.projectquoteid=${this.props.resourceid}` : `projectestimations_parentid.projectid = ${this.props.resourceid}`}`).then((response)=> {

			if(response.data.message != 'success') {
				let apiResponse = commonMethods.apiResult(response);
				return modalService[apiResponse.methodName](apiResponse.message);
			}

			if(response.data.main.length > 0) {

				response.data.main.forEach(item => {
					if(!boqObj[item[itemprop]]) {
						totaloverheadcost += item.amount || 0;
						return null;
					}

					boqObj[item[itemprop]].estimationitemsArr.push(item);
				});

				Object.keys(boqObj).forEach(prop => {
					let item = boqObj[prop];

					if(item.itemtype == 'Section') {
						item.cost = 0;
						item.costamount = 0;
						return null;
					}

					let estimatedcost = 0;
					let totalEstAmount = 0;

					item.estimationitemsArr.forEach(secitem => {
						totalEstAmount += Number(((secitem.boqquoteitemsid_israteonly ? 1 : secitem.quantity) * (secitem.rate || 0)).toFixed(this.props.app.roundOffPrecision));
					});

					item.cost = Number((totalEstAmount / (item.israteonly ? 1 : item.estimatedqty)).toFixed(this.props.app.roundOffPrecision));

					item.costamount = Number((item.cost * item.estimatedqty).toFixed(this.props.app.roundOffPrecision));
				});

				let boqArray = Object.keys(boqObj).map(prop => boqObj[prop]).sort((a,b) => a.displayindex - b.displayindex);

				boqArray.forEach((item, itemindex) => {
					boqArray.forEach((secitem, secindex) => {
						if(secindex != itemindex && secitem.internalrefno.indexOf(`${item.internalrefno}.`) === 0)
							item.childids.push(secitem.id);
					});
				});

				callback(boqArray, totaloverheadcost);
			}
		});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Profitability Analysis</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<table className="table">
								<thead>
									<tr>
										<th style={{width: '12%'}}>Internal Ref No</th>
										<th style={{width: '12%'}}>Client Ref No</th>
										<th>Section/Item Name</th>
										<th className="text-right" style={{width: '15%'}}>Selling Price</th>
										<th className="text-right" style={{width: '15%'}}>Cost Price</th>
										<th className="text-right" style={{width: '12%'}}>Gross Margin %</th>
									</tr>
								</thead>
								<tbody>
									 {this.state.profitarray.map((item, index) => {
										return (<tr key={index}>
											<td>{item.internalrefno}</td>
											<td>{item.clientrefno}</td>
											<td>{item.itemid_name}</td>
											<td className="text-right">{currencyFilter(item.rate, null, this.props.app)}</td>
											<td className="text-right">{currencyFilter(item.cost, null, this.props.app)}</td>
											<td className="text-right">{item.israteonly ? '' : item.profitpercentage}</td>
										</tr>)
									 })}
								</tbody>
							</table>
						</div>
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="col-md-8 pull-right bgColor-gray borderradius-6">
								<table className="width-100 lineheight-2">
									<tbody>
										<tr>
											<td className="text-right">Total Revenue</td>
											<td>:</td>
											<td className="text-right">{currencyFilter(this.state.revenue, null, this.props.app)}</td>
										</tr>
										{this.state.cost ? <tr>
											<td className="text-right">Total Estimated Cost</td>
											<td>:</td>
											<td className="text-right">{currencyFilter(this.state.cost, null, this.props.app)}</td>
										</tr> : null }
										{this.state.overheadcost ? <tr>
											<td className="text-right">Total Estimated Overhead Cost</td>
											<td>:</td>
											<td className="text-right">{currencyFilter(this.state.overheadcost, null, this.props.app)}</td>
										</tr> : null}
										{this.state.profit ? <tr>
											<td className="text-right">Gross Margin</td>
											<td>:</td>
											<td className="text-right">{currencyFilter(this.state.profit, null, this.props.app)}</td>
										</tr> : null }
										{this.state.profitpercentage ? <tr>
											<td className="text-right">Gross Margin %</td>
											<td>:</td>
											<td className="text-right">{this.state.profitpercentage}</td>
										</tr> : null }
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
