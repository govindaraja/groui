import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { updateAppState } from '../../actions/actions';
import { commonMethods, modalService } from '../../utils/services';
import { currencyFilter } from '../../utils/filter';


export default connect((state) => {
	return {
		app: state.app,
		fullstate: state
	}
}, { updateAppState })(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			projectprofitdetails: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.onLoad = this.onLoad.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		this.props.updateLoaderFlag(true);
		let { projectprofitdetails } = this.state;

		axios.get(`/api/query/projectprofitdetailquery?projectid=${this.props.resource.id}`).then((response) => {
			if (response.data.message == 'success') {
				projectprofitdetails = response.data.main;
				for(var i = 0; i < projectprofitdetails.length; i++) {
					if(projectprofitdetails[i].resource != 'projects') {
						projectprofitdetails[i].percent = Number(((projectprofitdetails[i].finaltotal/this.props.resource.finaltotal)*100).toFixed(this.props.app.roundOffPrecision))
					}
				}

				projectprofitdetails.sort((a, b) => {
					return a.displayorder - b.displayorder;
				});
				this.setState({projectprofitdetails});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.props.updateLoaderFlag(false);
		});
	}

	openList(param) {
		let filterArray = [];

		let tempobj = {
			...this.props.app.listFilterObj,
			[param.resource]: []
		};

		if(param.resource == 'salesinvoices') {
			filterArray.push({
				"fieldname" : "status",
				"operator" : "In",
				"value" : ['Approved','Sent To Customer']
			});
			filterArray.push({
				"fieldname" : "projectid",
				"operator" : "Equal",
				"value" : this.props.resource.id,
				"displayValue": this.props.resource.projectname
			});
		} else if(param.resource == 'expenserequests') {
			filterArray.push({
				"fieldname" : "status",
				"operator" : "In",
				"value" : ['Approved','Disbursed']
			});
			filterArray.push({
				"fieldname" : "projectid",
				"operator" : "Equal",
				"value" : this.props.resource.id,
				"displayValue": this.props.resource.projectname
			});
		} else if(param.resource == 'workorders') {
			filterArray.push({
				"fieldname" : "status",
				"operator" : "In",
				"value" : ['Approved','Sent To Supplier']
			});
			filterArray.push({
				"fieldname" : "projectid",
				"operator" : "Equal",
				"value" : this.props.resource.id,
				"displayValue": this.props.resource.projectname
			});
		} else if(param.resource == 'purchaseorders') {
			filterArray.push({
				"fieldname" : "status",
				"operator" : "In",
				"value" : ['Approved','Sent To Supplier']
			});
			filterArray.push({
				"fieldname" : "projectid",
				"operator" : "Equal",
				"value" : this.props.resource.id,
				"displayValue": this.props.resource.projectname
			});
		} else if(param.resource == 'itemrequests') {
			filterArray.push({
				"fieldname" : "status",
				"operator" : "Equal",
				"value" : "Approved"
			});
			filterArray.push({
				"fieldname" : "projectid",
				"operator" : "Equal",
				"value" : this.props.resource.id,
				"displayValue": this.props.resource.projectname
			});
			filterArray.push({
				"fieldname" : "issuedpercent",
				"operator" : "GreaterThan",
				"value" : 0,
				"displayValue": this.props.resource.issuedpercent
			});
		} else if(param.resource == 'journalvouchers') {
			filterArray.push({
				"fieldname" : "status",
				"operator" : "In",
				"value" : ['Approved', 'Sent To Customer']
			});
			filterArray.push({
				"fieldname" : "projectid",
				"operator" : "Equal",
				"value" : this.props.resource.id,
				"displayValue": this.props.resource.projectname
			});
		}

		tempobj[param.resource == 'journalvouchers' ? 'receiptvouchers' : param.resource] = filterArray;
		this.props.updateAppState('listFilterObj', tempobj);
		this.props.history.push(`/list/${param.resource == 'journalvouchers' ? 'receiptvouchers' : param.resource}`);
		this.props.closeModal();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Project Details</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll bg-gray">
					{this.state.projectprofitdetails.length==0 ? <div className="row">
						<div className="col-md-12">
							<div className="alert alert-warning text-center" >There is no project related details</div>
						</div>
					</div> : null }
					{this.state.projectprofitdetails.length > 0 ? <div className="row">
						<div className="col-md-12 col-sm-12">
							<table className="table">
								<tbody>
									<tr>
										<td className="bg-white text-center" colSpan="3">
											{this.state.projectprofitdetails.map((item, index) => {
												if(item.resource == 'projects') {
													return (
														<div key={index}>
															<div className="titledetail">
																<a className="cursor-ptr anchor-menu">{item.displayname} </a>
															</div>
															<div  className="amtdetail">
																<span className="text-amtsuccess">{currencyFilter(item.finaltotal, item.currencyid, this.props.app)}</span>
															</div>
														</div>
													);
												}
											})}
										</td>
									</tr>
									<tr>
										<td colSpan="3" className="bg-gray"></td>
									</tr>
									<tr>
										<td className="bg-white">
											{this.state.projectprofitdetails.map((item, index) => {
												if(item.resource=='salesinvoices' || item.resource=='journalvouchers') {
													return (
														<div  key={index} className="tilecards">
															<div className="titledetail">
																<a className="cursor-ptr anchor-menu" onClick={() => this.openList(item)}>{item.displayname} </a>    
																{item.resource!='projects' ? <span className="label-gray label-circle" title={`Total ${item.resource} raised`}>{item.count}</span> : null }
															</div>
															<div  className="amtdetail">
																<span className="text-amtsuccess">{currencyFilter(item.finaltotal, item.currencyid, this.props.app)}</span>
															</div>
															{item.resource!='projects' ? <div className="titledetail">
																<span className="font-14">{item.percent} % of Project value</span>
															</div> : null }
														</div>
													);
												}
											})}
										</td>
										<td className="bg-gray">
										</td>
										<td className="bg-white">
											{this.state.projectprofitdetails.map((item, index) => {
												if(item.resource!='salesinvoices' && item.resource!='journalvouchers' && item.resource!='projects') {
													return (
														<div key={index} className="tilecards">
															<div className="titledetail">
																<a className="cursor-ptr anchor-menu" onClick={() => this.openList(item)}>{item.displayname} </a>   
															<span className="label-gray label-circle" title={`Total ${item.resource} raised`}>{item.count}</span>
															</div>
															<div className="amtdetail">
																<span className="text-amtdanger">{currencyFilter(item.finaltotal, item.currencyid, this.props.app)}</span>
															</div>
															<div className="titledetail">
																<span className="font-14">{item.percent} % of Project value</span>
															</div>
														</div>
													);
												}
											})}
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div> : null }
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
