import React, { Component } from 'react';
import axios from 'axios';
import { Field } from 'redux-form';

import { commonMethods, modalService } from '../../utils/services';
import { LocalSelect } from '../utilcomponents';
import { textareaEle, Buttongroup } from '../formelements';
import { stringNewValidation } from '../../utils/utils';


export default class pricelistversioncreatemodal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			versionnos: []
		};
		this.getVersionNumbers = this.getVersionNumbers.bind(this);
		this.priceVersionAction = this.priceVersionAction.bind(this);
	}

	getVersionNumbers(existingplid) {
		axios.get(`/api/pricelistversions?field=id,versionno&filtercondition=pricelistversions.status='Approved' and pricelistversions.parentid=${existingplid}`).then((response) => {
			if (response.data.message == 'success') {
				this.setState({
					existingplid: existingplid,
					versionnos: response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	priceVersionAction () {
		let tempObj = {...this.state};
		tempObj.parentid = this.props.resource.id;
		tempObj.parentresource = 'pricelists';
		tempObj.currencyid = this.props.resource.currencyid;
		
		let tempData = {
			actionverb : 'Save',
			data : tempObj
		};

		axios({
			method : 'post',
			data : tempData,
			url : '/api/pricelistversions'
		}).then((response) => {
			if (response.data.message == 'success') {
				this.props.closeModal();
				this.props.callback(response.data.main.id);
			}

			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
		});
	}

	render() {
		let saveButtonDisabled = !this.state.remarks || !this.state.pricelistversion;
		if(this.state.pricelistversion == 'Copy Existing')
			saveButtonDisabled = saveButtonDisabled || !this.state.existingplid || !this.state.priceversionid;
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Add Price List Version</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-4">
							<label>Description</label>
						</div>
						<div className="form-group col-md-8">
							<textarea className={`form-control ${!this.state.remarks ? 'errorinput' : ''}`} value={this.state.remarks} onChange={(event) => this.setState({remarks: event.target.value})} />
						</div>
					</div>
					<div className="row">
						<div className="form-group col-md-4">
							<label>Price List Version</label>
						</div>
						<div className="form-group col-md-8">
							<Buttongroup  buttons={[{"title": "Create New", "value": "Create New"}, {"title": "Copy Existing", "value": "Copy Existing"}]} buttonclass={"btn-secondary btn-sm"} onChange={(value) => {this.setState({pricelistversion: value})}} value={this.state.pricelistversion} required={true}  />
						</div>
					</div>
					{this.state.pricelistversion == 'Copy Existing' ? <div className="row">
						<div className="form-group col-md-4">
							<label>Existing Price List</label>
						</div>
						<div className="form-group col-md-8">
							<LocalSelect options={this.props.resource.pricelistids} className={`${!this.state.existingplid ? 'errorinput' : ''}`} valuename="id" label="name" value={this.state.existingplid} onChange={(value)=>{this.getVersionNumbers(value)}} required />
						</div>
					</div> : null }
					{this.state.pricelistversion == 'Copy Existing' ? <div className="row">
						<div className="form-group col-md-4">
							<label>Price List Version</label>
						</div>
						<div className="form-group col-md-8">
							<LocalSelect options={this.state.versionnos} className={`${!this.state.priceversionid ? 'errorinput' : ''}`} valuename="id" label="versionno" value={this.state.priceversionid} onChange={(value)=>{this.setState({priceversionid: value});}} required />
						</div>
					</div> : null }
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width"  onClick={()=>this.priceVersionAction()} disabled={saveButtonDisabled}><i className="fa fa-save"></i>Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
