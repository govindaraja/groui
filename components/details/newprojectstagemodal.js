import React, { Component } from 'react';

import { SelectAsync } from '../utilcomponents';

export default class newprojectstagemodal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.save = this.save.bind(this);
	}

	save (stageid) {
		this.props.closeModal();
		this.props.callback(stageid);
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">New Project Stage</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-8 offset-md-2">
							<SelectAsync resource={"projectstages"} fields={"id,name"} filter={`projectstages.id!=${this.props.parentresource.id}`} value={this.state.stageid} className={`${!this.state.stageid ? 'errorinput' : ''}`} required={true} onChange={(value)=>{this.setState({stageid: value});}} />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width"  onClick={()=>this.save(this.state.stageid)} disabled={!this.state.stageid}><i className="fa fa-print"></i>Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
