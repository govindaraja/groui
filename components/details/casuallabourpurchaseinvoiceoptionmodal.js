import React, { Component } from 'react';
import { connect } from 'react-redux';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			contractorid : null
		};

		this.save = this.save.bind(this);
		this.radioBtnOnChange = this.radioBtnOnChange.bind(this);
	}

	radioBtnOnChange(evt, contractitem) {
		this.setState({
			contractorid: contractitem.contractorid
		});
	}

	save () {
		this.props.callback(this.state.contractorid);
		this.props.closeModal();
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Purchase Invoice Preference</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12 form-group">
							<span>Please choose contractor to create purchase invoice</span>
						</div>
						<div className="col-md-12">
							{this.props.contractoridArr.map((contractitem, index) => {
								return (
									<div className="form-check" key={index}>
										<input className="form-check-input" type="radio" checked={this.state.contractorid == contractitem.contractorid} onChange={(evt) => this.radioBtnOnChange(evt, contractitem)} />
										<label htmlFor="schedulelevel">
											{contractitem.contractorid_displayname}
										</label>
									</div>
								);
							})}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" disabled={!this.state.contractorid} className="btn btn-sm gs-btn-success btn-width" onClick={this.save}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
