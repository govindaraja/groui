import React, { Component } from 'react';
import moment from 'moment';

import { DateElement } from '../utilcomponents';

export default class AccountingFormQuickFillDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			quickfill: {
				seriesnumber: "",
				formnumber: "",
				formdate: "",
			}
		};
		this.inputOnChange = this.inputOnChange.bind(this);
		this.updateForm = this.updateForm.bind(this);
	}

	inputOnChange(value, field) {
		let { quickfill } = this.state;
		quickfill[field] = value;
		this.setState({ quickfill });
	}

	updateForm() {
		this.props.callback(this.state.quickfill);
		this.props.closeModal();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Quick Fill Details</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-10 offset-md-2 col-sm-12">
							<div className="row">
								<div className="col-md-3 col-sm-6 col-xs-12">
									<label className="labelclass">Series Number</label>
									<input className="form-control" value={this.state.quickfill.seriesnumber} onChange={(evt) => this.inputOnChange(evt.target.value, 'seriesnumber')} />
								</div>
								<div className="col-md-3 col-sm-6 col-xs-12">
									<label className="labelclass">Form Number</label>
									<input className={`form-control ${((this.state.quickfill.seriesnumber || this.state.quickfill.formdate) && !this.state.quickfill.formnumber) ? 'errorinput' : ''}`} value={this.state.quickfill.formnumber} onChange={(evt) => this.inputOnChange(evt.target.value, 'formnumber')} required={this.state.quickfill.seriesnumber || this.state.quickfill.formdate} />
								</div>
								<div className="col-md-3 col-sm-6 col-xs-12">
									<label className="labelclass">Form Date</label>
									<DateElement className="form-control" value={this.state.quickfill.formdate} onChange={(val) => this.inputOnChange(val, 'formdate')} />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.updateForm} disabled={((this.state.quickfill.seriesnumber || this.state.quickfill.formdate) && !this.state.quickfill.formnumber)} ><i className="fa fa-check"></i>Update</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
