import React, { Component } from 'react';
import axios from 'axios';
import { Field } from 'redux-form';
import { checkboxEle } from '../formelements';
import { search, sort_by } from '../../utils/utils';
import { accessMenuResource } from '../../utils/services';

class RoleMapping extends Component {
	constructor(props) {
		super(props);

		this.roleClick = this.roleClick.bind(this);
	};

	roleClick(role, roleindex) {
		this.props.openModal({
			render: (closeModal) => {
				return <RoleDetails
						   role = { role }
						   closeModal={ closeModal }
						   openModal = { this.props.openModal }
						   app = { this.props.app } />
			},
			className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	renderModule(prop, module, index) {
		return (
			<div className="col-md-6 col-sm-8" key={index}>
				<div className="card" style={{marginTop:'10px'}}>
					<div className="card-header">{prop}</div>
					<div className="card-body">
						<div style={{overflowY:'auto',maxHeight:'200px'}}>
							<table className="table" style={{marginBottom:'0px'}}>
								<tbody>
									{this.renderRoles(module)}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		);
	}

	renderRoles(roles) {
		return roles.map((role, index) => {
			return (
				<tr key={index}>
					<td>
						<label style={{marginTop: '2px',marginBottom: '2px',display:'inline-block'}}>
							<input type="checkbox" className="form-check-input" checked={this.props.selectedroleids.indexOf(role.id) >= 0 ? true : false} onChange={(evt) => this.props.onRoleChange(evt.target.checked, role)}/>{role.name || role.rolename}
						</label>
					</td>
					{ this.props.resource.resourceName == 'users' ? <td>
						<button type="button" onClick={() => this.roleClick(role, index)} style={{marginLeft: '5px'}} className = "btn btn-sm btn-outline-light text-dark pull-right" rel="tooltip" title="Details"><span style={{padding: '2px'}} className="fa fa-info"></span></button>
					</td> : null }
				</tr>
			);
		});
	}

	render() {

		let moduleObject = {
			Sales: [],
			Project: [],
			Purchase: [],
			Stock: [],
			Accounts: [],
			Service: [],
			HR: [],
			Admin: [],
			Others: []
		};

		this.props.roleArray.forEach((item) => {
			if(moduleObject[item.module])
				moduleObject[item.module].push(item);
			else
				moduleObject['Others'].push(item);
		});

		return (
			<div className="row col-md-12 col-sm-12 col-xs-12">
				{ this.props.resource.resourceName == 'users' ? <div className="row col-md-12 col-sm-12 col-xs-12">
					<div className="col-md-12 col-sm-12 col-xs-12 pull-right">
						<a onClick={() => this.props.onRoleChange('deselectall')} className='btn btn-sm btn-outline-light text-dark pull-right'>De-Select All</a>
						<a onClick={() => this.props.onRoleChange('selectall')} className='btn btn-sm btn-outline-light text-dark pull-right' style={{marginRight:'10px'}}>Select All</a>
					</div>
				</div>: null}

				{Object.keys(moduleObject).map((prop, index) => {
					let module = moduleObject[prop];
					return this.renderModule(prop, module, index);
				})}
			</div>
		);
	}
};

class RoleDetails extends Component {
	constructor(props) {
		super(props);

		this.state = {
			search : {
				displayName : '',
				group : '',
				subgroup : ''
			},
			resourceArray : []
		};

		this.onLoad = this.onLoad.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
	};

	componentWillMount() {
		this.onLoad();
	}

	inputonChange(value, field) {
		let { search } = this.state;
		search[field] = value;
		this.setState({ search });
	}

	onLoad() {

		let resourceArray = [];

		for (let prop in this.props.app.myResources) {
			if(!this.props.app.myResources[prop].hideInPermission && accessMenuResource.checkFeatureAccess(this.props.app.myResources[prop], this.props.app)) {
				let actionverbArray = [];
				for(let actionverb in this.props.app.myResources[prop].authorization) {
					if(this.props.app.myResources[prop].authorization[actionverb].indexOf(this.props.role.id) >= 0 || this.props.app.myResources[prop].authorization[actionverb].indexOf(this.props.role.id.toString()) >= 0) {
						if(actionverb == 'Read') {
							if(!this.props.app.myResources[prop].ignoreReadPermission)
								actionverbArray.push(actionverb);
						} else {
							actionverbArray.push(actionverb);
						}
					}
				}

				if(actionverbArray.length > 0)
					resourceArray.push({
						group  : this.props.app.myResources[prop].group,
						subgroup  : this.props.app.myResources[prop].subgroup,
						resourceName : this.props.app.myResources[prop].resourceName,
						displayName : this.props.app.myResources[prop].displayName,
						actionverbArray : actionverbArray
					});
			}
		}

		resourceArray = sort_by(resourceArray, ['group', 'subgroup', 'displayName'])

		this.setState({resourceArray});
	}

	render() {

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Resources Access to {this.props.role.rolename} </h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					{this.state.resourceArray.length > 0 ?
						<div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								<table className="table table-bordered">
									<thead>
										<tr>
											<th className='text-center' style={{width:'30%'}}>Name
												<input type="text" className="form-control" name="displayName" value={this.state.search.displayName} placeholder="Search by Resource Name" onChange={(evt) =>{this.inputonChange(evt.target.value, 'displayName')}}/>
											</th>
											<th className='text-center' style={{width:'18%'}}>Group
												<input type="text" className="form-control" name="group" value={this.state.search.group} placeholder="Search by Group" onChange={(evt) =>{this.inputonChange(evt.target.value, 'group')}}/>
											</th>
											<th className='text-center' style={{width:'22%'}}>Sub Group
												<input type="text" className="form-control" name="subgroup" value={this.state.search.subgroup} placeholder="Search by Sub Group" onChange={(evt) =>{this.inputonChange(evt.target.value, 'subgroup')}}/>
											</th>
											<th className='text-center' style={{width:'30%'}}>Action Verb</th>
										</tr>
									</thead>
									<tbody>
										{search(this.state.resourceArray, this.state.search).map((item, index) => {
											return (
												<tr key={index}>
													<td className="text-center">{item.displayName}</td>
													<td className="text-center">{item.group}</td>
													<td className="text-center">{item.subgroup}</td>
													<td className="text-left">{item.actionverbArray.join(', ')}</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</div> : <div className="row col-md-12 col-sm-12 col-xs-12">
							<div className="col-md-12 col-sm-12 col-xs-12 alert alert-light text-center">
								No Data Found !!!
							</div>
						</div>
					}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
};

export default RoleMapping;
