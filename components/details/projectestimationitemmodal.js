import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { LocalSelect, AutoSelect, NumberElement } from '../utilcomponents';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tabClasses: ["show active", ""],
			search: {},
			pendingprojectestimationitemArr: [],
			projectestimationitemArr: [],
			showdescription: false
		};

		this.onLoad = this.onLoad.bind(this);
		this.addItemreqitem = this.addItemreqitem.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
		this.quantityOnChange = this.quantityOnChange.bind(this);
		this.getTabClass = this.getTabClass.bind(this);
		this.getTabPaneClass = this.getTabPaneClass.bind(this);
		this.setActiveTab = this.setActiveTab.bind(this);
		this.descToggleOnChange = this.descToggleOnChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let projectestimationitemArr = [], pendingprojectestimationitemArr = [];
		var queryString = `/api/query/getprojectestimationitemquery?projectid=${this.props.projectid}`;
		axios.get(queryString).then((response)=> {
			if (response.data.message == 'success') {
				let projectestimationitemArr = response.data.main;

				for (var j = 0; j < projectestimationitemArr.length; j++) {
					let remainingqty = Number((projectestimationitemArr[j].estimatedqty - projectestimationitemArr[j].requestedqty).toFixed(this.props.app.roundOffPrecisionStock));
					if(remainingqty > 0)
						projectestimationitemArr[j].quantity = remainingqty;
					else
						projectestimationitemArr[j].quantity = null;
				}
				for (var i = 0; i < this.props.itemrequestitems.length; i++) {
					for (var j = 0; j < projectestimationitemArr.length; j++) {
						if (this.props.itemrequestitems[i].projectestimationitemsid == projectestimationitemArr[j].projectestimationitemsid) {
							projectestimationitemArr[j].checked = true;
							projectestimationitemArr[j].quantity = this.props.itemrequestitems[i].quantity;
						}
					}
				}
				for (var j = 0; j < projectestimationitemArr.length; j++) {
					if (projectestimationitemArr[j].quantity > 0) {
						pendingprojectestimationitemArr.push(projectestimationitemArr[j]);
					}
				}

				this.setState({
					projectestimationitemArr,
					pendingprojectestimationitemArr
				}, () => {
					this.setActiveTab(1);
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	addItemreqitem() {
		let tempItemreqItem = [], errArr = [];

		for (var i = 0; i < this.state.projectestimationitemArr.length; i++) {
			if (this.state.projectestimationitemArr[i].checked == true && this.state.projectestimationitemArr[i].quantity <= 0) {
				errArr.push(`Item : "${this.state.projectestimationitemArr[i].itemid_name}" quantity should be greater than 0`);
			}
			tempItemreqItem.push(this.state.projectestimationitemArr[i]);
		}

		if(errArr.length > 0) {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : errArr,
				btnArray : ['Ok']
			}));
		} else {
			this.props.callback(tempItemreqItem);
			this.props.closeModal();
		}
	}

	inputonChange(value) {
		let { search } = this.state;
		search.itemid_name = value;
		this.setState({search});
	}

	checkboxOnChange(value, item) {
		item.checked = value;
		this.setState({
			projectestimationitemArr: this.state.projectestimationitemArr,
			pendingprojectestimationitemArr: this.state.pendingprojectestimationitemArr
		});
	}

	checkallonChange(value, itemarr) {
		this.state[itemarr].forEach((item) => {
			item.checked = value;
		});
		this.setState({	[itemarr] : this.state[itemarr], checkall : value });
	}

	quantityOnChange(value, item) {
		item.quantity = value > 0 ? Number(value) : "";
		this.setState({
			projectestimationitemArr: this.state.projectestimationitemArr,
			pendingprojectestimationitemArr: this.state.pendingprojectestimationitemArr
		});
	}

	getTabClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	getTabPaneClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	setActiveTab(tabNum) {
		let { tabClasses } = this.state;
		tabClasses =  ["", ""]
		tabClasses[tabNum] = "show active";
		this.setState({tabClasses}, ()=>{
			this.getTabPaneClass(tabNum);
		});
	}

	descToggleOnChange() {
		this.setState({showdescription: !this.state.showdescription});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Project Estimation Item Details</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					{this.state.projectestimationitemArr.length > 0 ? <div className="row">
						<div className="form-group col-md-8 col-sm-8 align-self-center">
							<label className="labelclass">Item Name</label>
							<input type="text" className="form-control" value={this.state.search.itemid_name || ''} placeholder="Search by item name" onChange={(evt)=>{this.inputonChange(evt.target.value)}}/>
						</div>
						<div className="form-group col-md-4 col-sm-4">
							<div>Show Description</div>
							<label className="gs-checkbox-switch">
								<input className="form-check-input" type="checkbox" checked={this.state.showdescription} onChange={() => this.descToggleOnChange()} />
								<span className="gs-checkbox-switch-slider"></span>
							</label>
						</div>
					</div> : null}
					<div className="row">
						<div className="col-md-12">
							<ul className="nav nav-tabs">
								<li className="nav-item">
									<a className={`nav-link ${this.getTabClass(1)}`} onClick={()=>this.setActiveTab(1)}>Request Pending Items</a>
								</li>
								<li className="nav-item">
									<a className={`nav-link ${this.getTabClass(2)}`} onClick={()=>this.setActiveTab(2)} >All Items</a>
								</li>
							</ul>
							<div className="tab-content">
								<div className={`tab-pane fade ${this.getTabPaneClass(1)}`} role="tabpanel">
									<div className="row margintop-25">
										<div className="col-md-12 col-sm-12 col-xs-12">
											{this.state.pendingprojectestimationitemArr.length > 0 ? <table className="table table-bordered" >
												<thead>
													<tr>
														<th><input type="checkbox" checked={this.state.checkall || false} onChange={(evt)=>{this.checkallonChange(evt.target.checked, 'pendingprojectestimationitemArr')}}/></th>
														<th>BOQ Internal Ref No</th>
														<th>BOQ Item Name</th>
														<th>Item Name</th>
														<th>Estimated Qty</th>
														<th>So Far Requested Qty</th>
														<th>Quantity</th>
													</tr>
												</thead>
												<tbody>
													{search(this.state.pendingprojectestimationitemArr, this.state.search).map((item, index)=> {
														return (
															<tr key={index}>
																<td className="text-center"><input type="checkbox" checked={item.checked || false} onChange={(evt)=>{this.checkboxOnChange(evt.target.checked, item)}}/></td>
																<td>{item.boqitemsid_internalrefno}</td>
																<td>{item.boqitemid_name}<div className={`font-12 ${this.state.showdescription ? 'show' : 'hide'}`}>{item.boqitemsid_description}</div></td>
																<td>{item.itemid_name}<div className={`font-12 ${this.state.showdescription ? 'show' : 'hide'}`}>{item.description}</div></td>
																<td className="text-center">{item.estimatedqty} {item.uomid_name}</td>
																<td className="text-center">{item.requestedqty}</td>
																<td className="text-center">
																	<NumberElement className={`form-control`} value={item.quantity} onChange={(value) => this.quantityOnChange(value, item)} />
																</td>
															</tr>
														);
													})}
												</tbody>
											</table> : <div className="alert alert-info text-center">There is no request pending items</div>}
										</div>
									</div>
								</div>
								<div className={`tab-pane fade ${this.getTabPaneClass(2)}`} role="tabpanel">
									<div className="row margintop-25">
										<div className="col-md-12 col-sm-12 col-xs-12">
											<table className="table table-bordered">
												<thead>
													<tr>
														<th><input type="checkbox" checked={this.state.checkall || false} onChange={(evt)=>{this.checkallonChange(evt.target.checked, 'projectestimationitemArr')}}/></th>
														<th>BOQ Internal Ref No</th>
														<th>BOQ Item Name</th>
														<th>Item Name</th>
														<th>Estimated Qty</th>
														<th>So Far Requested Qty</th>
														<th>Quantity</th>
													</tr>
												</thead>
												<tbody>
													{search(this.state.projectestimationitemArr, this.state.search).map((item, index)=> {
														return (
															<tr key={index}>
																<td className="text-center"><input type="checkbox" checked={item.checked || false} onChange={(evt)=>{this.checkboxOnChange(evt.target.checked, item)}}/></td>
																<td>{item.boqitemsid_internalrefno}</td>
																<td>{item.boqitemid_name}<div className={`font-12 ${this.state.showdescription ? 'show' : 'hide'}`}>{item.boqitemsid_description}</div></td>
																<td>{item.itemid_name}<div className={`font-12 ${this.state.showdescription ? 'show' : 'hide'}`}>{item.description}</div></td>
																<td className="text-center">{item.estimatedqty} {item.uomid_name}</td>
																<td className="text-center">{item.requestedqty}</td>
																<td className="text-center">
																	<NumberElement className={`form-control`} value={item.quantity} onChange={(value) => this.quantityOnChange(value, item)} />
																</td>
															</tr>
														);
													})}
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addItemreqitem}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
