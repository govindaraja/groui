import React, { Component } from 'react';
import axios from 'axios';
import { AutoSelect } from '../../components/utilcomponents';
import { numberValidation } from '../../utils/utils';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			template : this.props.resource,
			transactionid: null
		};

		this.selectOnChange = this.selectOnChange.bind(this);
		this.previewTemplate = this.previewTemplate.bind(this);
	};

	selectOnChange(value, valueObj) {
		this.setState({
			transactionid: value
		});
	}

	previewTemplate() {
		this.props.closeModal();
		
		if(this.state.transactionid > 0)
			this.props.callback({
				resourceName : this.state.template.resource,
				templateid: this.state.template.id,
				id : this.state.transactionid
			});
	};

	render() {
		let resourceObj = {
			"quotes" : {
				"fields" : "id,quoteno",
				"value" : "id",
				"label" : "quoteno"
			},
			"orders" : {
				"fields" : "id,orderno",
				"value" : "id",
				"label" : "orderno"
			},
			"deliverynotes" : {
				"fields" : "id,deliverynotenumber",
				"value" : "id",
				"label" : "deliverynotenumber"
			},
			"salesinvoices" : {
				"fields" : "id,invoiceno",
				"value" : "id",
				"label" : "invoiceno"
			},
			"journals" : {
				"fields" : "id,voucherno",
				"value" : "id",
				"label" : "voucherno"
			},
			"creditnotes" : {
				"fields" : "id,voucherno",
				"value" : "id",
				"label" : "voucherno"
			},
			"debitnotes" : {
				"fields" : "id,voucherno",
				"value" : "id",
				"label" : "voucherno"
			},
			"paymentvouchers" : {
				"fields" : "id,voucherno",
				"value" : "id",
				"label" : "voucherno"
			},
			"receiptvouchers" : {
				"fields" : "id,voucherno",
				"value" : "id",
				"label" : "voucherno"
			},
			"rfq" : {
				"fields" : "id,rfqno",
				"value" : "id",
				"label" : "rfqno"
			},
			"supplierquotation" : {
				"fields" : "id,supplierquoteno",
				"value" : "id",
				"label" : "supplierquoteno"
			},
			"purchaseorders" : {
				"fields" : "id,ponumber",
				"value" : "id",
				"label" : "ponumber"
			},
			"receiptnotes" : {
				"fields" : "id,receiptnotenumber",
				"value" : "id",
				"label" : "receiptnotenumber"
			},
			"purchaseinvoices" : {
				"fields" : "id,invoiceno",
				"value" : "id",
				"label" : "invoiceno"
			},
			"contracts" : {
				"fields" : "id,contractno",
				"value" : "id",
				"label" : "contractno"
			},
			"contractenquiries" : {
				"fields" : "id,contractenquiryno",
				"value" : "id",
				"label" : "contractenquiryno"
			},
			"servicecalls" : {
				"fields" : "id,servicecallno",
				"value" : "id",
				"label" : "servicecallno"
			},
			"estimations" : {
				"fields" : "id,estimationno",
				"value" : "id",
				"label" : "estimationno"
			},
			"itemrequests" : {
				"fields" : "id,itemrequestno",
				"value" : "id",
				"label" : "itemrequestno"
			},
			"servicereports" : {
				"fields" : "id,servicereportno",
				"value" : "id",
				"label" : "servicereportno"
			},
			"projectquotes" : {
				"fields" : "id,quoteno",
				"value" : "id",
				"label" : "quoteno"
			},
			"projects" : {
				"fields" : "id,projectno",
				"value" : "id",
				"label" : "projectno"
			},
			"workorders" : {
				"fields" : "id,wonumber",
				"value" : "id",
				"label" : "wonumber"
			},
			"stockreservations" : {
				"fields" : "id,stockreservationno",
				"value" : "id",
				"label" : "stockreservationno"
			},
			"expenserequests" : {
				"fields" : "id,expenserequestno",
				"value" : "id",
				"label" : "expenserequestno"
			},
			"payrolls" : {
				"fields" : "id,payrollno",
				"value" : "id",
				"label" : "payrollno"
			},
			"bom" : {
				"fields" : "id,name",
				"value" : "id",
				"label" : "name"
			},
			"productionorders" : {
				"fields" : "id,orderno",
				"value" : "id",
				"label" : "orderno"
			},
			"projectestimations" : {
				"fields" : "id,estimationno",
				"value" : "id",
				"label" : "estimationno"
			},
			"milestoneprogress" : {
				"fields" : "id,progressno",
				"value" : "id",
				"label" : "progressno"
			},
			"proformainvoices" : {
				"fields" : "id,invoiceno",
				"value" : "id",
				"label" : "invoiceno"
			}
		};

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Preview {this.state.template.printgalleryid ? this.state.template.printgalleryid_displayname : this.state.template.name}</h5>
				</div>
				<div className="react-modal-body">
					<div className="row" style={{height:'100px'}}>
						<div className="col-md-6 offset-md-3">
							<label className="labelclass">Select Transaction No</label>
							<AutoSelect value={this.state.transactionid} resource={this.state.template.resource} fields={resourceObj[this.state.template.resource].fields} label={resourceObj[this.state.template.resource].label} onChange={(transactionid) => this.setState({transactionid})} required={true}/>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className={`btn btn-sm gs-btn-success btn-width`} onClick={this.previewTemplate} disabled={!this.state.transactionid}><i className="fa fa-print"></i>Print</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}