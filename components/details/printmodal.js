import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { LocalSelect } from '../utilcomponents';

export default class printmodal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showmodal: true,
			templateArr: [],
			qz : (this.props.app.feature.useRawPrinting) ? require('../../utils/rawprinting') : null
		}
		this.onLoad = this.onLoad.bind(this);
		this.printTemplate = this.printTemplate.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad () {
		var templatefilter = `templates.resource='${this.props.resourcename}' and templates.companies @> ARRAY[${this.props.companyid}]`;
		if(this.props.templatetype)
			templatefilter += ` and templates.templatetype='${this.props.templatetype}'`;
		else
			templatefilter += ` and templates.templatetype<>'Raw Print'`

		templatefilter = encodeURIComponent(templatefilter);
		var queryString = `/api/templates?pagelength=100&field=id,url,name,filename,companies&skip=0&filtercondition=${templatefilter}`;
		axios.get(queryString).then((response) => {
			if(response.data.message == 'success') {
				response.data.main.sort((a, b) => {
					return (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : (a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 0));
				});
				this.setState({templateArr: response.data.main});
				if (response.data.main.length == 1) {
					this.printTemplate(response.data.main[0].id);
				}
			}
		});
	}

	printTemplate (templateid) {
		this.setState({showmodal: false});
		if (this.props.callback) {
			this.props.callback(templateid);
		} else {
			var tempData = {
				actionverb : 'Print',
				data : this.props.resourcename == 'customerstatements' || this.props.resourcename == 'contacts' ? {
					id : this.props.id,
					templateid : templateid,
					...this.props.resource
				} : {
					id : (this.props.amendparentid)? this.props.amendparentid : this.props.id,
					templateid : templateid,
					versionNo: (this.props.amendparentid)? this.props.id : null
				}
			};

			let urlName = '';

			if(['balancesheet', 'profitloss'].indexOf(this.props.resourcename) >= 0)
				urlName = `${this.props.url}&templateid=${templateid}`;
			else
				urlName = this.props.resourcename;

			axios({
				method : (['balancesheet', 'profitloss'].indexOf(this.props.resourcename) >= 0) ? 'get' : 'post',
				data : tempData,
				url : '/api/' + urlName
			}).then((response) => {
				if (response.data.message) {
					this.setState({showmodal: true});
					var apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				} else {
					if(response.data.rawtemplate){
						let qz = this.state.qz;
						qz.printers.getDefault().then(function(printer) {
							console.log(printer);
							var data = [];
						    let config = qz.configs.create(printer);
						    if(response.data.language)
						    	data.push({type : 'raw',data : '',options: { language: response.data.language}});
						    data.push(response.data.rawtemplate);
						    console.log(data);
						    qz.print(config, data);
						});
					}else{
						var win = window.open("/print/"+response.data.filename+"?url=" + response.data.url, '_blank');
						if (!win) {
							this.props.openModal(modalService['infoMethod']({
								header : 'Warning',
								body : "Popup Blocker is enabled! Please add this site to your exception list.",
								btnName : ['Ok']
							}));
						}
					}
				}
				this.props.closeModal();
			});

		}
	}

	render() {
		if(!this.state.showmodal)
			return (
				<div className="text-center">
					<div className="alert alert-warning marginbottom-0">Printing in progress. Please wait...</div>
				</div>
			);
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Print {this.props.resourcename}</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-4">
							<label>Choose Template</label>
						</div>
						<div className="form-group col-md-8">
							<LocalSelect options={this.state.templateArr} valuename="id" label="name" value={this.state.templateid} onChange={(value)=>{this.setState({templateid: value});}} required />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width"  onClick={()=>this.printTemplate(this.state.templateid)} disabled={!this.state.templateid}><i className="fa fa-print"></i>Print</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
