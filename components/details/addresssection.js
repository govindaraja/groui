import React, { Component } from 'react';

export default class extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="col-md-12" style={{maxHeight:'290px', overflowY:'scroll'}}>
				<div className="list-group">
					{this.props.addresses.map((address, index) => {
						return (
							<li key={index} className="list-group-item" onClick={()=>{this.props.getAddressDetails(address.id)}}>
								{address.displayaddress ? <h6 className="list-group-item-heading">{address.title}<span className={`float-right badge ${address.isactive ? 'badge-success' : 'badge-warning'}`}>{address.isactive ? "Active" : "Inactive"}</span></h6> : null }
								{address.displayaddress ? <span className="list-group-item-text">{address.displayaddress}</span> : null }
							</li>
						);
					})}
				</div>
				<div className="col-md-12 col-sm-12 col-xs-12">
					<button type='button' onClick={this.props.addNewAddress} className="btn btn-sm gs-btn-info float-left"><span className="fa fa-plus"></span> Address</button>
				</div>
			</div>
		);
	}
}
