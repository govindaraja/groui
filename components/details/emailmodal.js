import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import axios from 'axios';

import { updateFormState } from '../../actions/actions';
import { commonMethods, modalService } from '../../utils/services';
import { requiredValidation, stringNewValidation, emailNewValidation, checkActionVerbAccess } from '../../utils/utils';
import { checkboxEle, localSelectEle, InputEle, richtextEle, dateTimeEle, selectAsyncEle, emailEle } from '../formelements';
import { datetimeFilter } from '../../utils/filter';
import DocumentuploadModal from '../../containers/documentuploadmodal';

class emailForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showemailtemplate: false,
			printTemplateName: [],
			emailTemplateName: [],
			emailaccountsArr: []
		}
		this.onLoad = this.onLoad.bind(this);
		this.showemailtemplatefn = this.showemailtemplatefn.bind(this);
		this.getEmailAccounts = this.getEmailAccounts.bind(this);
		this.checkboxonchange = this.checkboxonchange.bind(this);
		this.openLibraryUploadModal = this.openLibraryUploadModal.bind(this);
		this.emailaccountOnChange = this.emailaccountOnChange.bind(this);
		this.checkboxactivityonchange = this.checkboxactivityonchange.bind(this);
		this.activitytypeCallback = this.activitytypeCallback.bind(this);
		this.emailTemplateonChange = this.emailTemplateonChange.bind(this);
		this.sendemail = this.sendemail.bind(this);
		this.selectReplyto = this.selectReplyto.bind(this);
		this.removeReplyToLink = this.removeReplyToLink.bind(this);
	}

	componentWillMount() {
		let initializeObj = this.props.initializeObj ? this.props.initializeObj : {}
		let tempObj = {
			...initializeObj
		};
		tempObj.library = [];
		tempObj.documents = [];
		tempObj.extradocuments = [];
		tempObj.firsttime = this.props.firsttimefalse==true ? false : true;
		tempObj.sendemail = true;
		this.props.initialize(tempObj);
		setTimeout(()=>{this.onLoad();}, 0);
	}

	onLoad () {
		if(this.props.parentresource.companyid) {
			let printTemplateName = [];

			let printtemplatefilter = `templates.resource='${this.props.resourcename}' and templates.companies @> ARRAY[${this.props.parentresource.companyid}]`;
			printtemplatefilter = encodeURIComponent(printtemplatefilter);

			let printqueryString = `/api/templates?pagelength=100&field=id,name&skip=0&filtercondition=${printtemplatefilter}`;

			axios.get(printqueryString).then((response) => {
				if(response.data.message == 'success') {
					printTemplateName = response.data.main.sort((a, b)=> {
						return (a.name < b.name) ? -1 : (a.name > b.name) ? 1 : 0;
					});
					this.props.updateFormState(this.props.form, {
						templateid: printTemplateName.length == 1 ? printTemplateName[0].id : null
					});
					this.setState({printTemplateName});
				}
				this.getEmailTemplate();
			});
		}
	}

	getEmailTemplate() {
		let emailTemplateName = [];

		var emailtemplatefilter = `emailtemplates.resourcename='${this.props.resourcename}' and emailtemplates.companies @> ARRAY[${this.props.parentresource.companyid}]`;
		emailtemplatefilter = encodeURIComponent(emailtemplatefilter);

		var emailqueryString = `/api/emailtemplates?pagelength=100&field=id,title&skip=0&filtercondition=${emailtemplatefilter}`;
		axios.get(emailqueryString).then((response) => {
			if(response.data.message == 'success') {
				emailTemplateName = response.data.main.sort((a, b)=> {
					return (a.title < b.title) ? -1 : (a.title > b.title) ? 1 : 0;
				});
				this.props.updateFormState(this.props.form, {
					emailtemplateid: emailTemplateName.length == 1 && !this.props.activityemail ? emailTemplateName[0].id : null
				});
				this.setState({emailTemplateName});
			}
			this.getEmailAccounts();
		});
	}

	getEmailAccounts() {
		let emailaccountsArr = [];

		let emailaccountQueryStr = `/api/emailaccounts?pagelength=100&field=id,name,email,verified&filtercondition=${encodeURIComponent(`emailaccounts.isoutgoing = true and emailaccounts.userids @> ARRAY[${this.props.app.user.id}]`)}`;

		axios.get(emailaccountQueryStr).then((response) => {
			if(response.data.message == 'success') {
				emailaccountsArr = response.data.main;

				for(var i = 0; i < emailaccountsArr.length; i++) {
					emailaccountsArr[i].displayname = `${emailaccountsArr[i].name } <${emailaccountsArr[i].email}>`;

					if(!emailaccountsArr[i].verified)
						emailaccountsArr[i].displayname += ` [${!emailaccountsArr[i].verified ? 'Not Verified' : ''}]`;
				}
				if(emailaccountsArr.length == 1) {
					this.props.updateFormState(this.props.form, {
						emailaccountid: emailaccountsArr[0].id,
						fromaddress: emailaccountsArr[0].email
					});
				}
				this.setState({emailaccountsArr});
			}
		});
	}

	checkboxonchange(value) {
		this.props.updateFormState(this.props.form, {
			sendemail: value,
			templateid: null
		});
	}

	checkboxactivityonchange(value) {
		this.props.updateFormState(this.props.form, {
			logactivity: value
		});
	}

	emailaccountOnChange(id, valueobj) {
		for(var i=0;i<this.state.emailaccountsArr.length;i++) {
			if(this.state.emailaccountsArr[i].id == id) {
				this.props.updateFormState(this.props.form, {
					fromaddress: this.state.emailaccountsArr[i].email
				});
				break;
			}
		}
	}

	openLibraryUploadModal () {
		let tempArr = [];

		if(this.props.app.myResources[this.props.resourcename]) {
			for(var prop in this.props.app.myResources[this.props.resourcename].fields) {
				if(this.props.app.myResources[this.props.resourcename].fields[prop]['islink']) {
					if(this.props.parentresource[prop] && Object.keys(this.props.parentresource[prop]).length > 0) {
						tempArr.push(this.props.parentresource[prop]);
					}
				}
			}


			if(this.props.app.myResources[this.props.resourcename].childResource.length > 0) {
				for(var i=0;i<this.props.app.myResources[this.props.resourcename].childResource.length;i++) {
					var childresourcename = this.props.app.myResources[this.props.resourcename].childResource[i].name;

					if(this.props.app.myResources[childresourcename] && this.props.app.myResources[childresourcename].fields) {
						for(var itemprop in this.props.app.myResources[childresourcename].fields) {
							if(this.props.app.myResources[childresourcename].fields[itemprop]['islink']) {
								for(var k=0;k<this.props.parentresource[childresourcename].length;k++) {
									if(this.props.parentresource[childresourcename][k][itemprop] && Object.keys(this.props.parentresource[childresourcename][k][itemprop]).length > 0) {
										tempArr.push(this.props.parentresource[childresourcename][k][itemprop]);
									}
								}
							}

							if(this.props.app.myResources[childresourcename].fields[itemprop]['foreignKeyOptions'] && this.props.app.myResources[childresourcename].fields[itemprop]['foreignKeyOptions'].additionalField.length) {
								for(var j=0;j<this.props.app.myResources[childresourcename].fields[itemprop]['foreignKeyOptions'].additionalField.length;j++) {
									var tempresourcename = this.props.app.myResources[childresourcename].fields[itemprop]['foreignKeyOptions'].resource;

									var tempfieldname = this.props.app.myResources[childresourcename].fields[itemprop]['foreignKeyOptions'].additionalField[j].split('.')[1];

									if(this.props.app.myResources[tempresourcename].fields[tempfieldname] && this.props.app.myResources[tempresourcename].fields[tempfieldname].islink) {

										for(var k=0;k<this.props.parentresource[childresourcename].length;k++) {
											if(this.props.parentresource[childresourcename][k][itemprop+"_"+tempfieldname] && Object.keys(this.props.parentresource[childresourcename][k][itemprop+"_"+tempfieldname]).length > 0) {
												tempArr.push(this.props.parentresource[childresourcename][k][itemprop+"_"+tempfieldname]);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		setTimeout(() => {
			this.props.openModal({render: (closeModal) => {return <DocumentuploadModal parentresource={this.props.resourcename} parentid={this.props.resourcename == 'customerstatements' ? this.props.parentresource.partnerid : this.props.parentresource.id} app={this.props.app} resource={this.props.resource} multiple={true} localdoc={tempArr} hidelocaldoc={false} closeModal={closeModal} openModal={this.props.openModal} change={this.props.change} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
		}, 0);
	}

	getfiletype(fileParam) {
		if (fileParam) {
			var filetype = fileParam.name.split('.')[fileParam.name.split('.').length-1];
			if (filetype == 'txt' || filetype =='csv' || filetype == 'rtf') {
				return 'fa-file-text-o';
			}
			else if (filetype == 'gzip' || filetype == 'zip' || filetype == 'rar' || filetype == 'tar') {
				return 'fa-file-zip-o';
			}
			else if (filetype == 'pdf') {
				return 'fa-file-pdf-o';
			}
			else if (filetype == 'jpg' || filetype == 'png' || filetype == 'gif' || filetype == 'jpeg' || filetype == 'ico' || filetype == 'bmp') {
				return 'fa-file-image-o';
			}
			else if (filetype == 'doc' || filetype == 'docx' || filetype == 'dot' || filetype == 'dotx') {
				return 'fa-file-word-o';
			}
			else if (filetype == 'xlsx' || filetype == 'xls') {
				return 'fa-file-excel-o';
			}
			else if (filetype == 'pptx' || filetype == 'pptm') {
				return 'fa-file-powerpoint-o';
			}
			else {
				return "fa-file";
			}
		}
		return "fa-file";
	}

	getDestinationIcon (destination) {
		if(['googledrive', 'dropbox'].indexOf(destination) >= 0) {
			if (destination == 'googledrive') {
				return 'fa-google';
			}
			else if (destination == 'dropbox') {
				return 'fa-dropbox';
			}
		} else
			return "fa-file";
	}

	deleteLibraryFile (item) {
		for (var i = 0; i < this.props.resource.library.length; i++) {
			if (this.props.resource.library[i].referenceid == item.referenceid && this.props.resource.library[i].referencetype == item.referencetype) {
				this.props.array.remove('library', i);
				break;
			}
		}
	}

	activitytypeCallback(id, valueobj) {
		this.props.updateFormState(this.props.form, {
			activitytypeid_name: valueobj.name
		});
	}

	showemailtemplatefn() {
		this.setState({
			showemailtemplate: !this.state.showemailtemplate
		});
	}

	emailTemplateonChange () {
		if(!this.props.resource.emailtemplateid)
			return null;

		this.setState({templateloading: true});
		var tempData = {
			emailtemplateid : this.props.resource.emailtemplateid,
			parentresource : this.props.resourcename,
			parentid : this.props.parentresource.id
		};

		axios({
			method : 'post',
			data: tempData,
			url : '/api/common/methods/getemailtemplate'
		}).then((response) => {
			if(response.data.message == 'success') {
				let tempobj = {};
				if(!this.props.resource.inreplyto) {
					tempobj.subject = response.data.main.subject;
					tempobj.cc = response.data.main.cc;
					tempobj.bcc = response.data.main.bcc;
				}
				tempobj.body = response.data.main.body;
				this.props.updateFormState(this.props.form, tempobj);
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.setState({templateloading: false});
		});
	}

	sendemail() {
		if((this.props.resource.firsttime || this.props.activityemail) && this.props.resource.sendemail) {
			this.props.array.removeAll('extradocuments');

			for (var i = 0; i < this.props.resource.library.length; i++) {
				if(this.props.resource.library[i].referencetype == 'libraries') {
					if(['googledrive', 'dropbox'].indexOf(this.props.resource.library[i].destination) >= 0) {
						this.props.array.push('extradocuments', {
							referencetype : this.props.resource.library[i].referencetype,
							referenceid : this.props.resource.library[i].referenceid,
							name : this.props.resource.library[i].name,
							destination : this.props.resource.library[i].destination,
							fileurl : this.props.resource.library[i].fileurl
						});
					} else {
						this.props.array.push('extradocuments', {
							referencetype : this.props.resource.library[i].referencetype,
							referenceid : this.props.resource.library[i].referenceid,
							name : this.props.resource.library[i].name,
							destination : this.props.resource.library[i].destination,
							fileurl : `/documents/libraries/${this.props.resource.library[i].referenceid}/${this.props.resource.library[i].name}`
						});
					}
				} else {
					if(['googledrive', 'dropbox'].indexOf(this.props.resource.library[i].destination) >= 0) {
						this.props.array.push('extradocuments', {
							referencetype : this.props.resource.library[i].referencetype,
							referenceid : this.props.resource.library[i].referenceid,
							name : this.props.resource.library[i].name,
							destination : this.props.resource.library[i].destination,
							fileurl : this.props.resource.library[i].fileurl
						});
					} else {
						this.props.array.push('extradocuments', {
							referencetype : this.props.resource.library[i].referencetype,
							referenceid : this.props.resource.library[i].referenceid,
							name : this.props.resource.library[i].name,
							destination : this.props.resource.library[i].destination,
							fileurl : `/documents/documents/${this.props.resource.library[i].referenceid}/${this.props.resource.library[i].name}`
						});
					}
				}
			}
		}
		this.setState({loading: true});
		setTimeout(()=> {
			this.props.callback(this.props.resource, (obj, param) => {
				this.setState({loading: false});
				if(!param) {
					if(this.state.emailaccountsArr.length == 1) {
						obj.emailaccountid = this.state.emailaccountsArr[0].id;
						obj.fromaddress = this.state.emailaccountsArr[0].email;
					}
					this.props.initialize(obj);
				} else
					this.props.closeModal();
			});
		}, 0);
	}

	selectReplyto() {
		this.props.openModal({render: (closeModal) => {
			return <ReplyToDetailsModal resourceid={this.props.resource.id} parentresource={this.props.resourcename} app={this.props.app} openModal={this.props.openModal} closeModal={closeModal} callback={(result) => {
					this.props.updateFormState(this.props.form, {
						inreplyto: result.msgid,
						inreplyto_subject: result.subject
					});
			}} />
		}, className: {
			content: 'react-modal-custom-class',
			overlay: 'react-modal-overlay-custom-class'
		}, confirmModal: true});
	}

	removeReplyToLink() {
		this.props.updateFormState(this.props.form, {
			inreplyto : null,
			inreplyto_subject: null
		});
	}

	render() {
		if(!this.props.resource)
			return null;

		let { resource } = this.props;
		let checkboxfieldProps = {
			onChange: this.checkboxonchange
		};
		let checkboxactivityfieldProps = {
			onChange: this.checkboxactivityonchange
		}
		let printlocalselectprops = {
			options: this.state.printTemplateName,
			label: 'name',
			valuename: 'id'
		};
		let emaillocalselectprops = {
			options: this.state.emailTemplateName,
			label: 'title',
			valuename: 'id'
		};
		let emailacclocalselectprops = {
			options: this.state.emailaccountsArr,
			label: 'displayname',
			valuename: 'id',
			onChange: this.emailaccountOnChange
		};
		let emailtemplatelocalselectprops = {
			options: this.state.emailTemplateName,
			label: 'title',
			valuename: 'id',
			onChange: this.emailTemplateonChange
		};
		let selectasyncfieldProps = {
			resource: "activitytypes",
			fields: "id,name",
			onChange : this.activitytypeCallback
		};

		let notVerifiedMail = false;
		this.state.emailaccountsArr.some((mail) => {
			if(mail.id == this.props.resource.emailaccountid && !mail.verified) {
				notVerifiedMail = true;
				return true;
			}
		});

		return (
			<form>
				<div className="react-outer-modal">
					<div className="react-modal-header">
						<div style={{display: 'table-cell'}}><h5 className="modal-title">Send Email</h5></div>
						<div style={{display: 'table-cell', paddingLeft: '50px', minWidth: '300px'}} className={`${!this.props.activityemail ? 'hide' : ''}`}>
							<div onClick={this.showemailtemplatefn} className={`gs-anchor ${this.state.showemailtemplate ? 'hide' : ''}`}>Choose email template (Optional)</div>
							{this.state.showemailtemplate ? <Field name={'emailtemplateid'} props={emailtemplatelocalselectprops} component={localSelectEle} /> : null }
						</div>
					</div>
					<div className="react-modal-body react-modal-body-scroll">
						<div className="row">
							<div className="col-md-12">
								{this.state.loading && this.props.resource.firsttime && this.props.resource.sendemail ? <div className="alert alert-warning" >Please wait... Preview is loading....</div> : null}
								{this.state.loading && !this.props.resource.firsttime ? <div className="alert alert-warning" >Sending mail.... Please wait... </div> : null}
								{this.state.templateloading ? <div className="alert alert-warning" >Please wait... Template is loading....</div> : null }
							</div>
						</div>
						<div className="row">
							{resource.firsttime ? <div  className="col-md-12">
								<div className="row">
									<div className="col-md-8">
										<div className="row">
											<label className="col-sm-5 col-form-label">Send Email</label>
											<div className="form-group col-sm-7">
												<Field name={'sendemail'} props={checkboxfieldProps} component={checkboxEle} />
											</div>
										</div>
										{resource.sendemail ? <div className="row">
											<label className="col-sm-5 col-form-label">Choose Print Template</label>
											<div className="form-group col-sm-7">
												<Field name={'templateid'} props={printlocalselectprops} component={localSelectEle} validate={[requiredValidation]} />
											</div>
										</div> : null }
										{resource.sendemail ? <div className="row">
											<label className="col-sm-5 col-form-label">Choose Email Template</label>
											<div className="form-group col-sm-7">
												<Field name={'emailtemplateid'} props={emaillocalselectprops} component={localSelectEle} validate={[requiredValidation]} />
											</div>
										</div> : null }
										{(resource.sendemail && checkActionVerbAccess(this.props.app, 'libraries', 'Read')) ? <div className="row">
											<label className="col-sm-5 col-form-label">Attach Additional Documents</label>
											<div className="form-group col-sm-7">
												<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.openLibraryUploadModal} disabled={this.props.invalid || this.state.loading}><i className="fa fa-upload"></i>Upload Files</button>
											</div>
										</div> : null }
										{(resource.sendemail && checkActionVerbAccess(this.props.app, 'libraries', 'Read')) ? <div className="row">
											{resource.library.length==0 ? <div className="col-md-12 text-center">!-- Library Documents not found --!</div> : null }
										</div> : null }
									</div>
								</div>
								{resource.sendemail && resource.library.length>0 ? <div className="row">
									<div className="col-md-12">
										<div className="row">
											{resource.library.map((item, index) => {
												return (
													<div key={index} className="form-group col-md-4 hover-btn" rel='tooltip' title={item.name}>
														<div className="row">
															<div className="col-md-10 overflowtxt" style={{paddingRight:'0px'}}>
																<a href={item.fileurl} target="_blank">
																	{['googledrive', 'dropbox'].indexOf(item.destination) == -1 ? <span className={`fa ${this.getfiletype(item)}`} style={{cursor:'pointer', marginRight: '5px'}} rel="tooltip" title={item.name}></span> : null }
																	{['googledrive', 'dropbox'].indexOf(item.destination) >= 0 ? <span className={`fa ${this.getDestinationIcon(item.destination)}`} style={{cursor:'pointer', marginRight: '5px'}}></span> : null }
																	{item.name}
																</a>
															</div>
															<div className="col-md-2">
																<button type="button" onClick={()=>this.deleteLibraryFile(item)} className="btn btn-sm gs-form-btn-danger delete-btn">
																	<span className="fa fa-trash-o"></span>
																</button>
															</div>
														</div>
													</div>
												);
											})}
										</div>
									</div>
								</div> : null }
							</div> : null }
							{!resource.firsttime ? <div  className="col-md-12">
								<div className="row">
									<div className="form-group col-md-6">
										<div className="input-group input-group-sm">
											<div className="input-group-prepend">
												<span className="input-group-text">From</span>
											</div>
											<div style={{flex: '1'}}>
												<Field name={'emailaccountid'} props={emailacclocalselectprops} component={localSelectEle} validate={[requiredValidation]} />
											</div>
										</div>
									</div>
									<div className="form-group col-md-6">
										<div className="input-group input-group-sm">
											<div className="input-group-prepend">
												<span className="input-group-text">To</span>
											</div>
											<Field name={'toaddress'} component={emailEle} validate={[emailNewValidation({
												required: true
											})]} />
										</div>
									</div>
								</div>
								<div className="row">
									<div className="form-group col-md-6">
										<div className="input-group input-group-sm">
											<div className="input-group-prepend">
												<span className="input-group-text">Cc</span>
											</div>
											<Field name={'cc'} component={emailEle}  validate={[emailNewValidation()]} />
										</div>
									</div>
									<div className="form-group col-md-6">
										<div className="input-group input-group-sm">
											<div className="input-group-prepend">
												<span className="input-group-text">Bcc</span>
											</div>
											<Field name={'bcc'} component={emailEle}  validate={[emailNewValidation()]} />
										</div>
									</div>
								</div>
								<div className="row">
									<div className="form-group col-md-12">
										<div className="input-group input-group-sm">
											<div className="input-group-prepend">
												<span className="input-group-text">Subject</span>
											</div>
											<Field name={'subject'} component={InputEle} validate={[stringNewValidation({
												required: true
											})]} />
										</div>
									</div>
								</div>
								<div className="row">
									<div className="form-group col-md-8">
										<Field name={'body'} component={richtextEle} />
									</div>
									<div className="col-md-4">
										{!this.props.activityemail ? <div className="form-group input-group input-group-sm col-md-4">
											<div className="input-group-prepend">
												<span className="input-group-text">Main Attachments : </span>
											</div>
										</div> : null }
										
										{!this.props.activityemail && this.props.resource.url ? <div className="form-group col-md-4">
											<div className="col-md-12">
												<a href={`print/${this.props.resource.url.lastIndexOf('/') >= 0 ? this.props.resource.url.substr(this.props.resource.url.lastIndexOf('/')+1, this.props.resource.url.length-1) : 'getfile'}?url=${this.props.resource.url}`} target="_blank" >{this.props.resource.url.split('/')[this.props.resource.url.split('/').length-1]}</a>
											</div>
										</div> : null }
										<div className="form-group input-group input-group-sm col-md-4">
											<div className="input-group-prepend">
												<span className="input-group-text">Additional Attachments : </span>
											</div>
										</div>
										<div className="form-group col-md-12">
											{(this.props.activityemail && checkActionVerbAccess(this.props.app, 'libraries', 'Read')) ? <button type="button" className="btn btn-sm btn-secondary" onClick={this.openLibraryUploadModal} disabled={this.state.loading}><i className="fa fa-upload"></i>Upload Files</button> : null }
											{this.props.resource.extradocuments.length==0 && !this.props.activityemail ? <div>!-- No Document was selected --!</div> : null }
											{!this.props.activityemail ? this.props.resource.extradocuments.map((doc, docindex) => {
												return (
													<div className="form-group marginbottom-10" key={docindex}>
														{['googledrive', 'dropbox'].indexOf(doc.destination) == -1 ? <a href={doc.fileurl} target="_blank">{doc.name}</a> : null }
													</div>
												);
											}) : null}

											{this.props.activityemail && resource.sendemail && resource.library.length>0 ? <div className="row">						
												{resource.library.map((item, index) => {
													return (
														<div key={index} className="form-group col-md-12 hover-btn" rel='tooltip' title={item.name}>
															<div className="row">
																<div className="col-md-10 overflowtxt" style={{paddingRight:'0px'}}>
																	<a href={item.fileurl} target="_blank">
																		{['googledrive', 'dropbox'].indexOf(item.destination) == -1 ? <span className={`fa ${this.getfiletype(item)}`} style={{cursor:'pointer', marginRight: '5px'}} rel="tooltip" title={item.name}></span> : null }
																		{['googledrive', 'dropbox'].indexOf(item.destination) >= 0 ? <span className={`fa ${this.getDestinationIcon(item.destination)}`} style={{cursor:'pointer', marginRight: '5px'}}></span> : null }
																		{item.name}
																	</a>
																</div>
																<div className="col-md-2">
																	<button type="button" onClick={()=>this.deleteLibraryFile(item)} className="btn btn-sm gs-form-btn-danger delete-btn">
																		<span className="fa fa-trash-o"></span>
																	</button>
																</div>
															</div>
														</div>
													);
												})}
											</div> : null }
										</div>
									</div>
								</div>
							</div> : null }
						</div>
						{!resource.firsttime ? <div className="row">
							<div className="form-group col-md-2">
								<label>In Reply To : </label>
								<a onClick={this.selectReplyto}><span className={`${!resource.inreplyto ? 'show' : 'hide'}`}><i className="fa fa-plus"></i>Add</span><span className={`${resource.inreplyto ? 'show' : 'hide'}`}>Change</span></a>
							</div>
							<div className="form-group col-md-3">
								<span>{resource.inreplyto_subject}</span>
								{resource.inreplyto_subject ? <a tooltip="remove in reply to" onClick={this.removeReplyToLink} style={{marginLeft: '10px'}}>x</a> : null }
							</div>
						</div> : null }

						{this.props.activityemail && false ? <div className="row">
							{this.props.resourcename == 'quotes' || this.props.resourcename=='leads' || this.props.resourcename=='projectquotes' ? <div className="col-md-12">
								<div className="row">
									<div className="form-group col-md-2">
										<label>Log as activity ? </label>
										<Field name={'logactivity'} props={checkboxactivityfieldProps} component={checkboxEle} />
									</div>
									{resource.logactivity ? <div className="form-group col-md-3">
										<label className="labelclass">Activity Type</label>
										<Field name={'activitytypeid'} props={selectasyncfieldProps} component={selectAsyncEle}  validate={[requiredValidation]}/>
									</div> : null }
									{resource.logactivity ? <div className="form-group col-md-3">
										<label className="labelclass">Start Time</label>
										<Field name={'startdatetime'} component={dateTimeEle}/>
									</div> : null }
									{resource.logactivity ? <div className="form-group col-md-3">
										<label className="labelclass">End Time</label>
										<Field name={'enddatetime'}  component={dateTimeEle} />
									</div> : null }
								</div>
							</div> : null }
						</div> : null }
					</div>
					<div className="react-modal-footer">
						<div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								<div className="muted credit text-center">
									<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
									{resource.firsttime ? <button type="button" className="btn btn-sm gs-btn-success btn-width" disabled={this.state.loading || this.props.invalid} onClick={this.sendemail}><i className="fa fa-check"></i>OK</button> : null }
									{!resource.firsttime ? <button type="button" className="btn btn-sm gs-btn-success btn-width" disabled={this.state.loading || this.props.invalid || notVerifiedMail} onClick={this.sendemail}><i className="fa fa-envelope"></i>Send</button> : null }
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		);
	}
}

emailForm = connect(
	(state, props) => {
	  let formName = 'emailForm';
	  return {
		  app : state.app,
		  pagejson : state.pagejson,
		  form : formName,
		  resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
		  formData : state.form,
		  fullstate : state
	  }
	}, {updateFormState}
)(reduxForm()(emailForm));

export default emailForm;




export class ReplyToDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			clientemaildetails: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.selectreplyoption = this.selectreplyoption.bind(this);
		this.renderAvailableReplytoEmail = this.renderAvailableReplytoEmail.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let clientemaildetails = [];
		axios.get(`/api/query/getclientemailquery?resourceid=${this.props.resourceid}&relatedresource=${this.props.parentresource}&userid=${this.props.app.user.id}`).then((response) => {
			if(response.data.message == 'success') {
				clientemaildetails = response.data.main;
				this.setState({ clientemaildetails });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	renderAvailableReplytoEmail() {
		return this.state.clientemaildetails.map((item, index) => {
			return (
				<div key={index} onClick={() => this.selectreplyoption(item)} style={{cursor: 'pointer'}}>
					{item.inreplyto ? <i rel="tooltip" title={`${item.subject}`} className="fa fa-reply marginright-10"></i> : null}
					{!item.inreplyto ? <i rel="tooltip" title={`${item.subject}`} className="fa fa-envelope marginright-10"></i> : null}
					<span>{item.subject}</span>
					<br></br>
					<span className="text-muted" style={{fontSize: '13px', marginLeft: '10px'}}>From: {item.frommail}</span>
					<span className="text-muted" style={{fontSize: '13px', marginLeft: '10px'}}>To: {item.tomail}</span>
					<span className="text-muted" style={{fontSize: '13px', marginLeft: '10px'}}>On: {datetimeFilter(item.date)}</span>
					{this.state.clientemaildetails.length-1 > index ? <hr className="marginbottom-8 margintop-8 w-100" style={{display: 'inline-block'}}/> : null}
				</div>
			);
		});
	}


	selectreplyoption(item) {
		this.props.callback(item);
		this.props.closeModal();
	}

	render() {
		if(this.state.clientemaildetails.length == 0)
			return null;

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Choose In Reply To Email</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							{this.renderAvailableReplytoEmail()}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}