import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { currencyFilter, dateFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect, DateElement } from '../utilcomponents';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			projectEstimationItemArray: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.addProjectEstimationItem = this.addProjectEstimationItem.bind(this);
		this.checkboxonChange = this.checkboxonChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let projectEstimationItemArray = [];

		axios.get(`/api/projectestimationitems?field=id,itemid,boqitemsid,boqitemid,description,quantity,uomid,activitytypeid,remarks,itemmaster/name/boqitemid,itemmaster/name/itemid,projectitems/internalrefno/boqitemsid,projectitems/clientrefno/boqitemsid,uom/name/uomid,timesheetactivitytypes/name/activitytypeid&sortstring=id&filtercondition=projectestimations_parentid.status='Approved' and projectestimations_parentid.projectid=${this.props.resource.projectid}`).then((response) => {
			if (response.data.message == 'success') {
				projectEstimationItemArray = response.data.main;

				projectEstimationItemArray.forEach((estimationitem) => {
					estimationitem.projectestimationitemsid = estimationitem.id;

					if(!estimationitem.boqitemsid)
						estimationitem.boqitemsid_internalrefno = 'Overhead';

					if(this.props.item.projectestimationitemsid == estimationitem.projectestimationitemsid) {
						estimationitem.checked = true;
					}
				});
				
				this.setState({ projectEstimationItemArray });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	checkboxonChange(item, value) {
		item.checked = value;
		this.setState({
			projectEstimationItemArray: this.state.projectEstimationItemArray
		});
	}

	addProjectEstimationItem () {
		let itemdetailsarr = [];
		this.state.projectEstimationItemArray.forEach((item) => {
			if(item.checked) {
				itemdetailsarr.push(item);
			}
		});
		if(itemdetailsarr.length == 0 || itemdetailsarr.length > 1) {
			this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				body : 'Please choose only one item',
				btnArray : ['Ok']
			}));
		} else {
			this.props.updateFormState(this.props.form, {
					[`${this.props.itemstr}.projectestimationitemsid`] : itemdetailsarr[0].projectestimationitemsid,
					[`${this.props.itemstr}.boqitemsid`] : itemdetailsarr[0].boqitemsid,
					[`${this.props.itemstr}.boqitemsid_itemid`] : itemdetailsarr[0].itemid,
					[`${this.props.itemstr}.boqitemsid_internalrefno`] : itemdetailsarr[0].boqitemsid_internalrefno
				});
			setTimeout(() => {
				this.props.closeModal();
			}, 0);
		}
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Project Estimation Item Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th style={{width:'5%'}}></th>
										<th className="text-center">BOQ Details</th>
										<th className="text-center">Item Name</th>
										<th className="text-center">Estimated Qty</th>
										<th className="text-center">UOM</th>
										<th className="text-center">Remarks</th>
									</tr>
								</thead>
								<tbody>
									{this.state.projectEstimationItemArray.map((item, index) => {
										return (
											<tr key={index}>
												<td><input type="checkbox" onChange={(e) => this.checkboxonChange(item, e.target.checked)} checked={item.checked || false} /></td>
												<td>
													<div>
														<span className="text-muted">Internal Ref No </span>
														<span>: {item.boqitemsid_internalrefno}</span>
													</div>
													{item.boqitemsid_internalrefno != 'Overhead' ? <div>
														<span className="text-muted">Client Ref No </span>
														<span>: {item.boqitemsid_clientrefno}</span>
													</div> : null}
													{item.boqitemsid_internalrefno != 'Overhead' ? <div>
														<span className="text-muted">BOQ Item Name </span>
														<span>: {item.boqitemid_name}</span>
													</div> : null}
												</td>
												<td>{item.activitytypeid > 0 ? item.activitytypeid_name : item.itemid_name}</td>
												<td className="text-center">{item.quantity}</td>

												<td className="text-center">{item.uomid_name}</td>
												<td>{item.remarks}</td>
											</tr>
										)
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addProjectEstimationItem}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
