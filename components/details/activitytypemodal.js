import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService } from '../../utils/services';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			activitytypeArr: []
		};

		this.onLoad = this.onLoad.bind(this);
		this.renderActivityType = this.renderActivityType.bind(this);
		this.selectOnChange = this.selectOnChange.bind(this);
	};

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let uniqueCategoryObj = {}, activitytypeArr = [],filterstring='';
		if(this.props.resource.tempactivitycategory){
			filterstring = `activitytypes.category ='${this.props.resource.tempactivitycategory}'`
		}

		if(!this.props.resource.tempactivitycategory) {
			filterstring = `activitytypes.category !='Service Allocation' and activitytypes.category !='Payment Followup'`
		}

		if(this.props.resource.relatedactivityid > 0) {
			filterstring = `activitytypes.category = 'General' AND activitytypes.trackduration=true`;
		}

		axios.get(`/api/activitytypes?field=id,name,category,entrymode,planningtype,trackduration,triptracking,showduedate,startswithdriving,trackdrivingdistance,allowsubactivity,planningduration&filtercondition=${filterstring}`).then((response) => {
			if(response.data.message == 'success') {
				response.data.main.forEach(item => {
					if(!uniqueCategoryObj[item.category])
						uniqueCategoryObj[item.category] = [];

					uniqueCategoryObj[item.category].push(item);
				});

				for(var prop in uniqueCategoryObj) {
					uniqueCategoryObj[prop].sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
 
					activitytypeArr.push({
						category: prop,
						array: uniqueCategoryObj[prop]
					});
				}
				this.setState({ activitytypeArr });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	renderActivityType(item) {
		return item.array.map((type, index) => {
			return (
				<li key={index} onClick={()=> this.selectOnChange(type)} style={{borderBottomRightRadius: 'unset', borderBottomLeftRadius: 'unset',cursor: 'pointer', textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', padding: '7px 24px', borderBottom: '1px solid rgba(216, 216, 216, 0.5)'}}>{type.name}</li>
			);
		});
	}

	selectOnChange(value) {
		this.props.closeModal();
		this.props.callback(value);
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header" style={{padding: '18px 25px'}}>
					<div className="d-flex flex-row justify-content-between align-items-center">
						<div className="modal-title" style={{fontSize: '18px', color: '#00BF9A'}}>Choose Activity Type</div>
						<div style={{lineHeight: '0', display: 'flex', marginRight: '15px'}}>
							<span onClick={()=>this.selectOnChange()} style={{fontSize: '2rem', cursor: 'pointer'}}><span aria-hidden="true">&times;</span></span>
						</div>
					</div>
				</div>
				<div className="react-modal-body react-modal-body-scroll" style={{padding: '0px'}}>
					{
						this.state.activitytypeArr.map((item, index) => {
							return (
								<div className="row no-gutters" key={index}>
									<div className="col-md-12">
										<ul className="list-group">
											<li style={{backgroundColor: '#f5f5f5', borderTopLeftRadius: 'unset', borderTopRightRadius: 'unset', textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', padding: '7px 24px', borderBottom: '1px solid rgba(216, 216, 216, 0.5)'}}>{item.category}</li>
											{this.renderActivityType(item)}
										</ul>
									</div>
								</div>
							)
						})
					}
				</div>
			</div>
		);
	}
}