import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import * as utils from '../../utils/utils';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { LocalSelect, AutoSelect } from '../utilcomponents';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: {},
			productionbomitemArr: []
		};

		this.onLoad = this.onLoad.bind(this);
		this.addProductionRawMaterialItem = this.addProductionRawMaterialItem.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
		this.quantityOnChange = this.quantityOnChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let productionbomitemArr = [];
		var queryString = `/api/query/getproductionrawmaterialitemquery?resourcename=productionorders&productionorderid=${this.props.resource.productionorderid}`;
		axios.get(queryString).then((response)=> {
			if (response.data.message == 'success') {
				let productionbomitemArr = response.data.main;
				
				for (var i = 0; i < this.props.resource.itemrequestitems.length; i++) {
					for (var j = 0; j < productionbomitemArr.length; j++) {
						if (this.props.resource.itemrequestitems[i].productionbomitemsid == productionbomitemArr[j].id) {
							productionbomitemArr[j].checked = true;
						}
					}
				}

				this.setState({
					productionbomitemArr
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	addProductionRawMaterialItem () {
		let itemrequestitems = [...this.props.resource.itemrequestitems];
		let itemrequestitemidArr = [];
		let index = 0;
		itemrequestitems.forEach((item) => {
			if(index < item.index)
				index = item.index;
		});
		index = index + 1;
		let checkedFound = false;
		itemrequestitems.forEach((reqitem) => {
			itemrequestitemidArr.push(reqitem.itemid);
		});
		this.state.productionbomitemArr.forEach((item) => {
			if(item.checked) {
				checkedFound = true;
				if(itemrequestitemidArr.indexOf(item.itemid) == -1) {
					let tempObj = {
						index: (index+1),
						requireddate: this.props.resource.requireddate || null,
						warehouseid: this.props.resource.warehouseid || null 
					};
					utils.assign(tempObj, item, ['itemid', 'itemid_name', 'itemid_issaleskit', '', '', 'itemid_hasaddons', '', 'description', 'quantity', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'itemid_keepstock','', '','remarks', '', '', '', '', '', '', {'productionbomitemsid' : 'id'}]);


					this.props.customFieldsOperation('productionbomitems',tempObj, item, 'itemrequestitems');
					itemrequestitems.push(tempObj);
					index++;
				}
			}
		});

		if(!checkedFound) {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				body : 'Please select atleast one item',
				btnArray : ['Ok']
			}));
		} else {
			this.props.updateFormState(this.props.form, { itemrequestitems });
			setTimeout(() => {
				this.props.closeModal();
			}, 0);
		}
	}

	inputonChange(value) {
		let { search } = this.state;
		search.itemid_name = value;
		this.setState({search});
	}

	checkboxOnChange(item, value) {
		item.checked = value;
		this.setState({
			productionbomitemArr: this.state.productionbomitemArr
		});
	}

	checkallonChange(value, itemarr) {
		this.state.productionbomitemArr.forEach((item) => {
			item.checked = value;
		});
		this.setState({	productionbomitemArr : this.state.productionbomitemArr, checkall : value });
	}

	quantityOnChange(value, item) {
		item.quantity = value > 0 ? Number(value) : "";
		this.setState({
			productionbomitemArr: this.state.productionbomitemArr
		});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Production Raw Material Item Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-3 form-group">
							<input type="text" className="form-control" name="location" value={this.state.search.itemid_name || ''} placeholder="Search By Item Name" onChange={(evt) =>{this.inputonChange(evt.target.value)}}/>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th style={{width: '6%', textAlign: 'center'}}><input type="checkbox" checked={this.state.checkall || false} onChange={(evt)=>{this.checkallonChange(evt.target.checked)}}/></th>
										<th className="text-center">Item Name</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">UOM</th>
									</tr>
								</thead>
								<tbody>
									{search(this.state.productionbomitemArr, this.state.search).map((item, index) => {
										return (
											<tr key={index}>
												<td className="text-center"><input type="checkbox" onChange={(e) => this.checkboxOnChange(item, e.target.checked)} checked={item.checked} /></td>
												<td>{item.itemid_name}</td>
												<td className="text-center">{item.quantity}</td>
												<td className="text-center">{item.uomid_name}</td>
											</tr>
										)
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addProductionRawMaterialItem}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
