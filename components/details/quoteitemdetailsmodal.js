import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import * as utils from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			quotedetails: null,
			quoteitemdetails: [],
			quotekititemdetails: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.updatecheckbox = this.updatecheckbox.bind(this);
		this.getQuoteItemdetails = this.getQuoteItemdetails.bind(this);
		this.getQuoteKitItemdetails = this.getQuoteKitItemdetails.bind(this);
		this.addQuoteitem = this.addQuoteitem.bind(this);
	}

	componentWillMount() {
		if(this.props.resource.quoteno) {
			this.onLoad();
		}
	}

	onLoad() {
		let quotedetails = [];

		axios.get(`/api/quotes?&field=id,quoteno,customerreference,customerid,orderid&filtercondition=quotes.customerid=${this.props.resource.customerid} and quotes.id != ${this.props.resource.quoteno}  and quotes.status='Won' and quotes.orderid is null`).then((response) => {
			if (response.data.message == 'success') {
				quotedetails = response.data.main;
				for(var i = 0; i < quotedetails.length; i++) {
					quotedetails[i].checked = false;
				}

				this.setState({quotedetails}, () => {
					this.getQuoteItemdetails();
				});
			}
		});
	}

	getQuoteItemdetails() {
		let quoteitemdetails = [];

		let filterStr = "";
		for(var i = 0; i < this.state.quotedetails.length; i++) {
			filterStr += this.state.quotedetails[i].id +",";
		}
		filterStr = filterStr.substr(0, filterStr.length-1);

		if(filterStr != "") {
			axios.get(`/api/quoteitems?&field=id,itemid,description,uomid,parentid,quantity,rate,discountquantity,discountmode,amount,taxid,taxdetails,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/quantitytype/itemid,itemmaster/keepstock/itemid,itemmaster/hasaddons/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,itemmaster/usebillinguom/itemid,uom/name/uomid,itemnamevariationid,itemnamevariations/name/itemnamevariationid,uomconversionfactor,uomconversiontype,baseitemrate,addonrate,displaygroup,actualsize,size,number,drawingno,index,remarks,deliverydate,usebillinguom,billinguomid,billingrate,billingquantity,billingconversiontype,billingconversionfactor&filtercondition=quoteitems.parentid in (${filterStr})`).then((response) => {
				if (response.data.message == 'success') {
					quoteitemdetails = response.data.main;

					this.setState({quoteitemdetails}, () => {
						this.getQuoteKitItemdetails(filterStr);
					});
				}
			});
		}
	}

	getQuoteKitItemdetails(filterStr) {
		let quotekititemdetails = [];

		axios.get(`/api/kititemquotedetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid,rate&filtercondition=kititemquotedetails.parentid in (${filterStr})`).then((response) => {
			if (response.data.message == 'success') {
				quotekititemdetails = response.data.main;
				this.setState({quotekititemdetails});
			}
		});
	}

	addQuoteitem () {
		let orderitems = [...this.props.resource.orderitems];
		let kititemorderdetails = [...this.props.resource.kititemorderdetails];
		let index = 0;
		orderitems.forEach((item) => {
			if(index < item.index)
				index = item.index;
		});
		index = index + 1;
		this.state.quotedetails.forEach((quote) => {
			if(quote.checked) {
				this.state.quoteitemdetails.forEach((item) => {
					if(item.parentid == quote.id) {
						let tempObj = {
							index: (index+1),
						};
						utils.assign(tempObj, item, [{'quoteitemid' : 'id'}, 'itemid', 'itemid_name', 'itemid_issaleskit', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'description', 'baseitemrate', 'addonrate', 'rate', 'quantity', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'discountmode', 'drawingno', 'discountquantity', 'itemid_keepstock', 'itemid_quantitytype', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'warehouseid', 'displaygroup', 'quoteid', {'quote_index':'index'}, 'remarks', 'deliverydate', 'itemid_usebillinguom', 'usebillinguom', 'billinguomid', 'billingrate', 'billingquantity', 'billingconversiontype', 'billingconversionfactor']);

						tempObj['warehouseid'] = this.props.resource.warehouseid;

						this.props.customFieldsOperation('quoteitems',tempObj, item,'orderitems');

						this.state.quotekititemdetails.forEach((kititem) => {
							if(kititem.parentid == quote.id && item.index == kititem.rootindex) {
								let tempKitObj = {
									rootindex: tempObj.index
								};
								utils.assign(tempKitObj, kititem, ['itemid', 'itemid_name', 'parent', 'uomid', 'uomid_name', 'quantity', 'conversion', 'rate', 'ratelc', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'warehouseid', 'index', 'parentindex']);
								kititemorderdetails.push(tempKitObj);
							}
						});
						orderitems.push(tempObj);
						index++;
					}
				});
			}
		});
		this.props.updateFormState(this.props.form, { orderitems, kititemorderdetails });
		setTimeout(() => {
			this.props.callback();
			this.props.closeModal();
		}, 0);
	}

	updatecheckbox(item, field, value) {
		item[field] = value;
		this.setState({
			quotedetails: this.state.quotedetails
		});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Add Items from Quotation</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						{ this.state.quotedetails && this.state.quotedetails.length > 0 ? <div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th className="text-center" style={{width: '3%'}}></th>
										<th className="text-center">Quote No</th>
										<th className="text-center">Customer Reference</th>
									</tr>
								</thead>
								<tbody>
									{this.state.quotedetails.map((item, index)=> {
										return (
											<tr key={index}>
												<td><input type="checkbox" onChange={(e) => this.updatecheckbox(item, 'checked', e.target.checked)}  checked={item.checked} /></td>
												<td>{item.quoteno}</td>
												<td>{item.customerreference}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div> : null }
						{ this.state.quotedetails && this.state.quotedetails.length == 0 ? <div className="col-md-12">
							<div className="alert alert-info text-center">Sorry, there is no Won quotation for this customer</div>
						</div> : null}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addQuoteitem}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
