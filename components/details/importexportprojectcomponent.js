import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import async from 'async';

import { commonMethods, modalService } from '../../utils/services';
import Loadingcontainer from '../loadingcontainer';
import ChildImport from '../../import/childimport';
import * as filter from '../../utils/filter';
import { XLSXReader } from '../../utils/excelutils';
import { LocalSelect, SelectAsync, DateElement } from '../utilcomponents';

export default class BOQImportModal extends Component {
	constructor(props) {
		super(props);

		let initialArray = [...this.props.resourcename == 'projectitems' ? this.props.resource.projectitems : this.props.resource.projectquoteitems];

		this.state = {
			initialArray,
			errors: []
		};
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.validate = this.validate.bind(this);
		this.parentValidation = this.parentValidation.bind(this);
		this.importCB = this.importCB.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.fileOnChange = this.fileOnChange.bind(this);
		this.importDataFromSheet = this.importDataFromSheet.bind(this);
		this.openErrorModal = this.openErrorModal.bind(this);
		this.generateData = this.generateData.bind(this);
		this.getTaxDetails = this.getTaxDetails.bind(this);
		this.checkProjectItemExist = this.checkProjectItemExist.bind(this);
		this.getMilestoneTemplateDetails = this.getMilestoneTemplateDetails.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.getTaxDetails();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag})
	}

	getTaxDetails() {
		let taxcodeObj = {};
		axios.get(`/api/taxcodemaster?field=id,taxcode&filtercondition=taxcodemaster.type='Sales'`).then((response) => {
			if (response.data.message == 'success') {
				taxcodeObj.taxcodemaster = {};

				response.data.main.forEach((item) => {
					taxcodeObj.taxcodemaster[item.taxcode] = item
				});
				this.setState({
					taxcodeObj
				}, () => {
					this.getMilestoneTemplateDetails();
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getMilestoneTemplateDetails() {
		let milestonetemplateObj = {};
		axios.get(`/api/milestonetemplates?field=id,name&filtercondition=`).then((response) => {
			if (response.data.message == 'success') {
				milestonetemplateObj = {};

				response.data.main.forEach((item) => {
					milestonetemplateObj[item.name] = item;
				});
				this.setState({
					milestonetemplateObj
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	checkProjectItemExist() {
		let resourceArray = [...this.props.resourcename == 'projectitems' ? this.props.resource.projectitems : this.props.resource.projectquoteitems];

		if(resourceArray.length > 0) {
			let message = {
				header : 'Warning',
				body : 'You already have items added in this project. If you continue with the import, new items will be added along with existing items. Please remove if there are any duplicates.',
				btnArray : ['Ok','Cancel']
			};
			this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if(param) {
					$("#projectimportexcelfile").click();
				}
			}));
		} else {
			$("#projectimportexcelfile").click();
		}
	}

	fileOnChange(files) {
		this.updateLoaderFlag(true);
		if(files.length > 0) {
			let excelfile = files[0];
			let extensionName = excelfile.name.slice(excelfile.name.lastIndexOf('.')+1, excelfile.name.length);
			if (['xls','xlsx'].indexOf(extensionName) == -1) {
				return this.setState({
					errors: ["Invalid file format. Please ensure that you are importing the correct file"]
				}, () => {
					this.openErrorModal();
				});
			}

			XLSXReader(excelfile, true, true, (data) => {
				this.setState({
					file: excelfile,
					filename: excelfile.name,
					sheets: data.sheets,
					errors: [],
					isValid: false,
					importfileurl : ""
				}, () => {
					this.importDataFromSheet();
				});
			});
			this.updateLoaderFlag(false);
		} else {
			this.setState({
				file: null,
				filename: null,
				sheets: null,
				isValid: false,
				importfileurl: ""
			}, () => {
				this.openErrorModal()
			});
			this.updateLoaderFlag(false);
		}
	}

	openErrorModal() {
		return this.props.openModal({
			render: (closeModal) => {
				return <ErrorAlertModal
					errors = {this.state.errors}
					closeModal = {closeModal} />
			}, className: {
				content: 'react-modal-custom-class',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	importDataFromSheet() {
		let boqitemObj = {}, uomObj = {};
		let errors = [];
		let { sheets, taxcodeObj, milestonetemplateObj } = this.state;
		let sheetName = this.props.resourcename == 'projectitems' ? 'Project Items' : 'BOQ Items';

		for(var prop in this.props.app.uomObj) {
			uomObj[this.props.app.uomObj[prop].name] = this.props.app.uomObj[prop];
		}

		if(!sheets[`${sheetName}`]) {
			this.setState({
				errors: [`Sheet name should be ${sheetName}`]
			});
			this.fileOnChange([]);
			return false;
		} else if(sheets[`${sheetName}`] && sheets[`${sheetName}`].length == 0) {
			this.setState({
				errors: [`${sheetName} sheet must contains atleast one item`]
			});
			this.fileOnChange([]);
			return false;
		}

		let boqitemsArray = sheets[`${sheetName}`];
		let internalrefArr = [];
		let tempItemnameArr = [], clientRefNoArr = [], internalRefNoArr = [];

		this.props.resource[this.props.resourcename].forEach((item) => {
			if(!internalRefNoArr.includes(item.internalrefno)) {
				internalRefNoArr.push(item.internalrefno);
			}
		});

		boqitemsArray.forEach((item, index) => {
			for(var prop in item) {
				item[prop.replace(/\s+/g, '')] = item[prop];
			}

			let rowIndex = item.__rowNum__ + 1;
			item['DummyField'] = `Item${index+1}`;

			if(item['Item/Section*'] == 'Item') {
				tempItemnameArr.push(item["Name*"]);
			}

			['Item/Section*','Name*'].forEach((field) => {
				if(typeof(item[field]) != 'string')
					errors.push(`${field} is mandatory & must be a string for row - ${rowIndex}`);
			});

			if(item['Internal Ref No']) {
				let tempinternalRefnoArr = item['Internal Ref No'].split('.');
				if(tempinternalRefnoArr.length > 5) {
					errors.push(`Internal Ref No format should be only 5 digit for row - ${rowIndex}`);
				}

				for(var i = 0 ; i < tempinternalRefnoArr.length; i++) {
					if (isNaN(tempinternalRefnoArr[i])) {
						errors.push(`Internal Ref No format should be only number for row - ${rowIndex}`);
						break;
					}
				}

				if(internalRefNoArr.indexOf(item['Internal Ref No']) == -1) {
					internalRefNoArr.push(item['Internal Ref No']);
				} else {
					errors.push(`Internal Ref no must be unique. Ref No: ${item['Internal Ref No']} in row - ${rowIndex} is duplicated`);
				}
			}

			if(typeof(item['Split Rate']) != 'boolean')
				item['Split Rate'] = item['Split Rate'] === "TRUE" ? true : (item['Split Rate'] === "FALSE" ? false : null);

			if(item['Item/Section*'] == 'Item' && item['Split Rate'] != true && item['Split Rate'] != false)
				item['Split Rate'] = false;

			if(item['Item/Section*'] == 'Item' && typeof(item['Uom*']) != 'string') {
				errors.push(`Uom* is mandatory & must be a string for row - ${rowIndex}`);
			}

			['Quantity*', 'Material Rate', 'Labour Rate', 'Rate'].forEach((field) => {
				if(item[field]) {
					item[field] = Number(item[field]);
					if(isNaN(item[field]))
						errors.push(`${field} must contains the number value for row - ${rowIndex}`);
				} else {
					if(item[field] == '')
						item[field] = null;
				}
			});

			[{name: 'Material Tax', field: 'materialtaxid'}, {name: 'Labour Tax', field: 'labourtaxid'}, {name:'Tax', field: 'taxid'}].forEach((fieldObj) => {
				if(item[fieldObj.name]) {
					item[fieldObj.field] = [];
					item[fieldObj.name].split(',').forEach((taxitem) => {
						if(!taxcodeObj.taxcodemaster[taxitem.trim()])
							errors.push(`Invalid ${fieldObj.name} for row - ${rowIndex}`);
						else{
							item[fieldObj.field].push(taxcodeObj.taxcodemaster[taxitem.trim()].id);
						}
					});
				}
			});

			if(!this.props.resource.ismilestonerequired && item['Milestone Template']) {
				errors.push(`Milestone Template should be empty for row - ${rowIndex}`);
			}

			if(item['Milestone Template'] && this.props.resource.ismilestonerequired) {
				if(!milestonetemplateObj[item['Milestone Template'].trim()])
					errors.push(`Invalid Milestone Template for row - ${rowIndex}`);
				else {
					item.milestonetemplateid = milestonetemplateObj[item['Milestone Template'].trim()].id;
					item.milestonetemplateid_name = milestonetemplateObj[item['Milestone Template'].trim()].name;
				}
			}

			if(item['Uom*']) {
				if(uomObj[item['Uom*']]) {
					item.uomid = uomObj[item['Uom*']].id;
					item.uomid_name = item['Uom*'];
				} else {
					errors.push(`Invalid UOM for row - ${rowIndex}`);
				}
			}

			if(item['Delivery Date']) {
				if(new Date(item['Delivery Date']) == 'Invalid Date') {
					errors.push(`Invalid Delivery Date for row - ${rowIndex}`);
				}
			}

			if(item['Item/Section*'] && ['Section', 'Item'].indexOf(item['Item/Section*']) == -1)
				errors.push(`Item/Section must be Section or Item for row - ${rowIndex}`);
		});

		if(errors.length > 0 || this.state.errors.length > 0) {
			this.setState({
				errors: errors ? errors : this.state.errors
			});
			this.fileOnChange([]);
			return false;
		} else {
			errors = [];
			axios({
				method : 'post',
				data : {
					actionverb : 'Read',
					data : {
						itemarray:tempItemnameArr,
						type: 'name'
					}
				},
				url : '/api/common/methods/getprojectimportitemdetails'
			}).then((response) => {
				if(response.data.message == 'success') {
					let itemmasterArr = response.data.main;

					boqitemsArray.forEach((boqitem, boqindex) => {
						let boqRowIndex = boqindex + 2;
						boqitem.index = boqindex + 2;
						let notFound = false;
						itemmasterArr.forEach((item, index) => {
							if(boqitem['Name*'] == item.name) {
								boqitem.itemid = item.id;
								boqitem.itemid_name = item.name;
								boqitem["Item's Keepstock"] = item.keepstock;
								boqitem["Item's Is Saleskit"] = item.issaleskit;
								boqitem["Item's Item Type"] = item.itemtype;
								boqitem['Split Rate'] = item.itemtype == 'Project' ? boqitem['Split Rate'] : false;
								boqitem['Rate'] = boqitem['Split Rate'] === false ? (boqitem['Rate'] ? boqitem['Rate'] : item.recommendedsellingprice) : 0;
								boqitem['Description'] = boqitem['Description'] ? boqitem['Description'] : item.description;
								boqitem["Alternate UOM"] = item.alternateuom;
								boqitem["UOM Conversion Type"] = item.uomconversiontype;
								boqitem["UOM Conversion Factor"] = item.uomconversionfactor;
								boqitem["Item's Use Billing UOM"] = item.itemid_usebillinguom;
								boqitem["Use Billing UOM"] = item.usebillinguom;
								boqitem["Billing UOM"] = item.billinguomid;
								boqitem["Billing Conversion Type"] = item.billingconversiontype;
								boqitem["Billing Conversion Factor"] = item.billingconversionfactor;

								if(item.usebillinguom) {
									boqitem["Billing Rate"] = boqitem['Rate'];
									boqitem['Rate'] = Number(((boqitem["Billing Rate"] / (boqitem["UOM Conversion Factor"] ? boqitem["UOM Conversion Factor"] : 1)) * boqitem["Billing Conversion Factor"]).toFixed(this.props.app.roundOffPrecision));

									boqitem["Billing Quantity"] = Number(((boqitem["Quantity*"] / (boqitem["UOM Conversion Factor"] ? boqitem["UOM Conversion Factor"] : 1)) * boqitem["Billing Conversion Factor"]).toFixed(this.props.app.roundOffPrecisionStock));
								}
								notFound = true;
								if(boqitem.uomid != item.salesuomid) {
									errors.push(`Invalid UOM Name for row - ${boqRowIndex}`);
								}
							}
						});

						boqitem['Material Rate'] = boqitem['Split Rate'] === true ? boqitem['Material Rate'] : 0;
						boqitem['Labour Rate'] = boqitem['Split Rate'] === true ? boqitem['Labour Rate'] : 0;
						boqitem['Material Tax'] = boqitem['Split Rate'] === true ? boqitem['Material Tax'] : null;
						boqitem['Labour Tax'] = boqitem['Split Rate'] === true ? boqitem['Labour Tax'] : null;
						boqitem['Tax'] = boqitem['Split Rate'] === false ? boqitem['Tax'] : null;
						boqitem.materialtaxid = boqitem['Split Rate'] === true ? boqitem.materialtaxid : null;
						boqitem.labourtaxid = boqitem['Split Rate'] === true ? boqitem.labourtaxid : null;
						boqitem.taxid = boqitem['Split Rate'] === false ? boqitem.taxid : null;

						if(boqitem['Item/Section*'] === "Section") {
							boqitem['Uom*'] = null;
							boqitem.uomid = null;
							boqitem.uomid_name = null;
							boqitem['Delivery Date'] = null;
							boqitem['Remarks'] = null;
							boqitem.milestonetemplateid = null;
							boqitem.milestonetemplateid_name = null;
						} else {
							boqitem['Name*'] = null;
						}

						if(!notFound && boqitem['Item/Section*'] == 'Item') {
							errors.push(`Invalid Item Name for row - ${boqRowIndex}`);
						} else {
							boqitemObj[' ' +boqitem['DummyField']] = JSON.parse(JSON.stringify(boqitem));
						}
					});
					if(errors.length > 0) {
						this.setState({
							errors: errors
						});
						this.fileOnChange([]);
					} else {
						this.generateData(boqitemObj);
					}
				} else {
					this.setState({
						errors: response.data.errors
					});
					this.fileOnChange([]);
				}
			});
		}
	}

	generateData(boqitemObj) {
		let itempropertyArray = [{
				"name" : "Client Ref No",
				"column" : "clientrefno"
			}, {
				"name" : "Internal Ref No",
				"column" : "internalrefno"
			}, {
				"name" : "Item/Section*",
				"column" : "itemtype"
			}, {
				"name" : "Name*",
				"column" : "name"
			}, {
				"name" : "Description",
				"column" : "description"
			}, {
				"name" : "Specification",
				"column" : "specification",
			}, {
				"name" : "Item Id",
				"column" : "itemid",
				"type" : "fk",
				"columnname" : "itemid_name"
			}, {
				"name" : "Item's Keepstock",
				"column" : "itemid_keepstock"
			}, {
				"name" : "Item's Is Saleskit",
				"column" : "itemid_issaleskit"
			}, {
				"name" : "Item's Item Type",
				"column" : "itemid_itemtype"
			}, {
				"name" : "Item's Use Billing UOM",
				"column" : "itemid_usebillinguom"
			}, {
				"name" : "Use Billing UOM",
				"column" : "usebillinguom"
			}, {
				"name" : "Billing UOM",
				"column" : "billinguomid"
			}, {
				"name" : "Billing Conversion Type",
				"column" : "billingconversiontype"
			}, {
				"name" : "Billing Conversion Factor",
				"column" : "billingconversionfactor"
			}, {
				"name" : "Billing Rate",
				"column" : "billingrate"
			}, {
				"name" : "Billing Quantity",
				"column" : "billingquantity"
			}, {
				"name" : "Quantity*",
				"column" : "quantity"
			}, {
				"name" : "Uom*",
				"column" : "uomid",
				"type" : "fk",
				"columnname" : "uomid_name"
			}, {
				"name" : "Alternate UOM",
				"column" : "alternateuom"
			}, {
				"name" : "UOM Conversion Type",
				"column" : "uomconversiontype"
			}, {
				"name" : "UOM Conversion Factor",
				"column" : "uomconversionfactor"
			}, {
				"name" : "Split Rate",
				"column" : "splitrate"
			}, {
				"name" : "Material Rate",
				"column" : "materialrate"
			}, {
				"name" : "Labour Rate",
				"column" : "labourrate"
			}, {
				"name" : "Rate",
				"column" : "rate"
			}, {
				"name" : "Material Tax",
				"column" : "materialtaxid",
				"type" : "fk",
				"columnname" : "materialtaxid_name"
			}, {
				"name" : "Labour Tax",
				"column" : "labourtaxid",
				"type" : "fk",
				"columnname" : "labourtaxid_name"
			}, {
				"name" : "Tax",
				"column" : "taxid",
				"type" : "fk",
				"columnname" : "taxid_name"
			}, {
				"name" : "Delivery Date",
				"column" : "deliverydate"
			}, {
				"name" : "Milestone Template",
				"column" : "milestonetemplateid",
				"type" : "fk",
				"columnname" : "milestonetemplateid_name"
			}, {
				"name" : "Remarks",
				"column" : "remarks"
			}
		];

		let dataArray = [...this.props.resourcename == 'projectitems' ? this.props.resource.projectitems : this.props.resource.projectquoteitems];

		for(let prop in boqitemObj) {
			let tempObj = {};
			let index = 0;
			for (var i = 0; i < dataArray.length; i++) {
				if (dataArray[i].index > index)
					index = dataArray[i].index;
			}
			tempObj.index = index + 1;
			itempropertyArray.forEach((field) => {
				if(field.type) {
					tempObj[field.column] = boqitemObj[prop][field.column];
					tempObj[field.columnname] = boqitemObj[prop][field.columnname];
				} else
					tempObj[field.column] = boqitemObj[prop][field.name];
			});
			dataArray.push(JSON.parse(JSON.stringify(tempObj)));
		};

		this.setState({
			initialArray: dataArray
		});
	}

	formatItemForEdit(item) {
		if(item.itemid_itemtype != 'Project')
			item.splitrate = null;
		
		item.taxid = item.taxid && item.taxid.length > 0 ? item.taxid[0] : null;
		item.materialtaxid = item.materialtaxid && item.materialtaxid.length > 0 ? item.materialtaxid[0] : null;
		item.labourtaxid = item.labourtaxid && item.labourtaxid.length > 0 ? item.labourtaxid[0] : null;
		item.olditemtype = item.itemtype;

		if(!item.splitrate) {
			item.materialrate = null;
			item.labourrate = null;
		} else {
			if(item.materialrate > 0 || item.labourrate > 0)
				item.rate = null;
			
			item.taxid = [];
		}

		if(item.israteonly)
			item.quantity = null;

		if(item.usebillinguom)
			item.rate = null;

		if(item.id > 0)
			item._restrictDelete = true;

		return;
	}

	validate(param) {
		this.updateLoaderFlag(true);
		this.refs.childImport.validate(param);
		this.updateLoaderFlag(false);
	}

	parentValidation(dataArray, cellArray, validCB) {
		if(dataArray.length == 0)
			return validCB(dataArray, cellArray);

		let tempItemIdArr = [];

		dataArray.forEach((item) => {
			tempItemIdArr.push(item.itemid);
		});

		axios({
			method : 'post',
			data : {
				actionverb : 'Read',
				data : {
					itemarray: tempItemIdArr,
					type: 'id'
				}
			},
			url : '/api/common/methods/getprojectimportitemdetails'
		}).then((response) => {
			if(response.data.message == 'success') {
				let resultitemArr = response.data.main;
				let resultitemObj = {};
				resultitemArr.forEach((item)=> {
					resultitemObj[item.id] = {
						...item
					};
				});

				dataArray.forEach((dataItemObj) => {
					cellArray[dataItemObj._refRowNo][0] = [];
					if(resultitemObj[dataItemObj.itemid]) {
						dataItemObj.itemid_name = resultitemObj[dataItemObj.itemid].name;
						dataItemObj.itemid_keepstock = resultitemObj[dataItemObj.itemid].keepstock;
						dataItemObj.itemid_issaleskit = resultitemObj[dataItemObj.itemid].issaleskit;
						dataItemObj.itemid_itemtype = resultitemObj[dataItemObj.itemid].itemtype;
						dataItemObj.itemid_usebillinguom = resultitemObj[dataItemObj.itemid].itemid_usebillinguom;
						dataItemObj.description = dataItemObj.description ? dataItemObj.description : resultitemObj[dataItemObj.itemid].description;
						dataItemObj.discountmode = dataItemObj.discountmode ? dataItemObj.discountmode : 'Percentage';

						if(resultitemObj[dataItemObj.itemid].itemtype != 'Project' && (dataItemObj.splitrate === true || dataItemObj.splitrate === false)) {
							cellArray[dataItemObj._refRowNo][0].push(`Split Rate should be empty. (Only for project item, it should have value)`);
						}

						if(resultitemObj[dataItemObj.itemid].itemtype == 'Project' && (!dataItemObj.hasOwnProperty('splitrate') || dataItemObj.splitrate === null || dataItemObj.splitrate === undefined)) {
							cellArray[dataItemObj._refRowNo][0].push(`Split Rate is required. (Only for project item, it is required)`);
						}

						let uomFound = false;
						resultitemObj[dataItemObj.itemid].alternateuoms.forEach((uomitem)=> {
							if(uomitem.id == dataItemObj.uomid) {
								uomFound = true;
								dataItemObj.uomid_name = uomitem.name;
								dataItemObj.uomconversiontype = uomitem.conversiontype;

								if(dataItemObj.uomconversiontype && !dataItemObj.uomconversionfactor) {
									cellArray[dataItemObj._refRowNo][0].push(`UOM Conversion factor is required`);
								}

								if(dataItemObj.uomconversiontype == 'Fixed' && dataItemObj.uomconversionfactor && dataItemObj.uomconversionfactor != uomitem.conversionrate) {
									cellArray[dataItemObj._refRowNo][0].push(`UOM Conversion factor should be only ${uomitem.conversionrate}`);
								}
							}
						});

						if(!uomFound)
							cellArray[dataItemObj._refRowNo][0].push('UOM is invalid');

						if(resultitemObj[dataItemObj.itemid].usebillinguom) {
							dataItemObj.usebillinguom = resultitemObj[dataItemObj.itemid].usebillinguom;

							if(!dataItemObj.billinguomid) {
								cellArray[dataItemObj._refRowNo][0].push('Billing UOM is Required.');
							} else {
								let billinguomFound = false;

								resultitemObj[dataItemObj.itemid].alternateuoms.forEach((uomitem) => {
									if(uomitem.id == dataItemObj.billinguomid) {
										billinguomFound = true;
										dataItemObj.billinguomid_name = uomitem.name;
										dataItemObj.billingconversiontype = uomitem.conversiontype;

										if(dataItemObj.billingconversiontype && !dataItemObj.billingconversionfactor) {
											cellArray[dataItemObj._refRowNo][0].push(`Billing Conversion factor is required`);
										}

										if(dataItemObj.billingconversiontype == 'Fixed' && dataItemObj.billingconversionfactor &&  dataItemObj.billingconversionfactor != uomitem.conversionrate) {
											cellArray[dataItemObj._refRowNo][0].push(`Billing Conversion factor should be only ${uomitem.conversionrate}`);
										}
									}
								});

								if(!billinguomFound) {
									cellArray[dataItemObj._refRowNo][0].push('Billing UOM is invalid');
								}
							}

							if(dataItemObj.billingrate && dataItemObj.billingconversionfactor) {
								dataItemObj.rate = Number(((dataItemObj.billingrate / (dataItemObj.uomconversionfactor ? dataItemObj.uomconversionfactor : 1)) * dataItemObj.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
							}

							if(dataItemObj.quantity && dataItemObj.billingconversionfactor) {
								dataItemObj.billingquantity = Number(((dataItemObj.quantity / (dataItemObj.uomconversionfactor ? dataItemObj.uomconversionfactor : 1)) * dataItemObj.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
							}
						} else {
							if(dataItemObj.billinguomid) {
								cellArray[dataItemObj._refRowNo][0].push('Billing UOM should not have a value. For more info, please check the field help.');
							}
						}
						
						if(!dataItemObj.uomconversiontype && dataItemObj.uomconversionfactor)
							cellArray[dataItemObj._refRowNo][0].push('UOM Conversion factor should not have a value. For more info, please check the field help.');
					}

					if(dataItemObj.internalrefno) {
						let tempinternalRefnoArr = dataItemObj.internalrefno.split('.');
						if(tempinternalRefnoArr.length > 5) {
							cellArray[dataItemObj._refRowNo][0].push('Internal Ref No format should be only 5 digit');
						}

						for(var i = 0 ; i < tempinternalRefnoArr.length; i++) {
							if (isNaN(tempinternalRefnoArr[i])) {
								cellArray[dataItemObj._refRowNo][0].push('Internal Ref No format should be only number');
								break;
							}
						}
					}
					
					if(dataItemObj.olditemtype && dataItemObj.olditemtype != dataItemObj.itemtype)
						cellArray[dataItemObj._refRowNo][0].push(`Item Type cannot be changed`);

					if(dataItemObj.deleteRow == "Yes" && dataItemObj.estimatedqty > 0)
						cellArray[dataItemObj._refRowNo][0].push(`Item "${dataItemObj.itemid_name}" is already estimated. Please remove 'delete this' option`);
				});

				validCB(dataArray, cellArray);
			} else {
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	importCB(dataArray) {
		this.updateLoaderFlag(true);
		dataArray.forEach((item, itemindex) => {
			delete item._refRowNo;
			item.index = itemindex + 1;
			item.taxid = item.taxid ? [item.taxid] : [];
			item.materialtaxid = item.materialtaxid ? [item.materialtaxid] : [];
			item.labourtaxid = item.labourtaxid ? [item.labourtaxid] : [];
			item.quantity = item.israteonly ? 0 : item.quantity;
		});
		this.props.updateFormState(this.props.form, {
			[`${this.props.resourcename}`] : dataArray,
			[`${this.props.resourcename == 'projectitems' ? 'kititemprojectdetails' : 'kititemprojectquotedetails'}`] : []
		});
		setTimeout(() => {
			this.props.onLoadKit(0, []);
			this.props.getimportMilestoneItems();
			this.props.computeFinalRate();
			this.updateLoaderFlag(false);
			this.props.closeModal();
		}, 0);
	}
	
	exportExcel() {
		this.refs.childImport.exportExcel();
	}
	
	closeModal() {
		this.updateLoaderFlag(true);
		this.props.openModal(modalService['confirmMethod']({
			header : 'Alert',
			body : 'Changes you made in this grid will not be updated to transaction. Would you like to continue?',
			btnArray : ['Yes', 'No']
		}, (param1)=> {
			if(param1) {
				this.props.closeModal();
			}
			this.updateLoaderFlag(false);
		}));
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<div className="modal-title d-flex justify-content-between">
						<div className="d-flex">
							<h5>Add/Edit BOQ</h5>
						</div>
						<div className="d-flex">
							<button type="button" className="btn btn-sm gs-form-btn-success" onClick={this.exportExcel}><i className="fa fa-upload"></i>Export</button>
						</div>
					</div>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-12">
							<ChildImport initialArray={this.state.initialArray} childjson={this.props.childjson} resource={this.props.resource} app={this.props.app} ref="childImport" openModal={this.props.openModal} parentValidation={this.parentValidation} importCB={this.importCB} formatItemForEdit={this.formatItemForEdit} height={$(window).height() - 200} importname = {this.props.resourcename == 'projectitems' ? 'Project Items' : 'Project Quote Items'} needDelete={true} />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.closeModal} disabled={this.state.loaderflag}><i className="fa fa-times"></i>Close</button>
								{this.props.resource.status != 'Approved' && this.props.resource.status != 'Completed' && this.props.resource.status != 'Cancelled' ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={() => this.validate(false)} disabled={this.state.loaderflag}><i className="fa fa-check"></i>Validate</button> : null}
								{this.props.resource.status != 'Approved' && this.props.resource.status != 'Completed' && this.props.resource.status != 'Cancelled' ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={() => this.validate(true)} disabled={this.state.loaderflag}><i className="fa fa-check"></i>Done</button> : null}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export class ErrorAlertModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.renderError = this.renderError.bind(this);
	}

	renderError() {
		return this.props.errors.map((error, index) => {
			return(
				<div className="form-group col-md-12" key={index}>
					<div className="alert alert-info">{error}</div>
				</div>
			);
		})
	}

	render() {
		if(this.props.errors.length == 0)
			return null;

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Warning</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							{this.renderError()}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}