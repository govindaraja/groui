import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService } from '../../utils/services';
import Loadingcontainer from '../loadingcontainer';

export default class extends Component {
	constructor (props) {
		super (props);

		this.state = {
			loaderflag : false,
			autogenpassword : true,
			password : '',
			changepwdinnextlogin : true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.updatePwdDetails = this.updatePwdDetails.bind(this);
		this.save = this.save.bind(this);
	}

	updatePwdDetails (field, value) {
		this.setState({
			[field] : value
		});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	close () {
		this.props.callback();
		this.props.closeModal();
	}

	save(actionverb) {
		this.updateLoaderFlag(true);
		let { resource } = this.props;
		resource.autogenpassword = this.state.autogenpassword;
		resource.changepwdinnextlogin = this.state.changepwdinnextlogin;
		resource.password = this.state.password;

		axios({
			method : 'post',
			data : {
				actionverb : actionverb,
				data : resource
			},
			url : '/api/users'
		}).then((response) => {
			if (response.data.message == 'success') {
				let tempObj = {};

				tempObj.password = response.data.main.password;
				tempObj.changepwdinnextlogin = response.data.main.changepwdinnextlogin;
				tempObj.modified = response.data.main.modified;
				tempObj.generatedpassword = response.data.main.generatedpassword;
				tempObj.resetPwdFlag = true;

				this.props.updateFormState(this.props.form, tempObj);

				this.props.callback(this.state);
				this.props.closeModal();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	render () {
		if (!this.props.resource)
			return null;

		return (
			<div className="react-outer-modal">
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header" style={{padding: '16px 22px'}}>
					<h6 className="modal-title gs-text-color">Reset password</h6>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-12 col-sm-12 col-xs-12">
							<div className="row">
								<div className="col-md-10">Auto Generate Password</div>
								<label className="gs-checkbox-switch">
									<input className="form-check-input" type="checkbox" checked={this.state.autogenpassword} onChange={(evt) => this.updatePwdDetails('autogenpassword', evt.target.checked)} />
									<span className="gs-checkbox-switch-slider"></span>
								</label>
							</div>
						</div>
						{!this.state.autogenpassword ? <div className="form-group col-md-12 col-sm-12 col-xs-12">
							<div className="row">
								<div className="col-md-12 col-sm-12 col-xs-12">
									<input className={`form-control ${!this.state.autogenpassword && !this.state.password ? 'errorinput' : ''}`} type="password" name="password" value={this.state.password} placeholder="Enter password" onChange={(evt) => {this.updatePwdDetails('password',evt.target.value)}} />
								</div>
							</div>
						</div> : null}
						<div className="form-group col-md-12 col-sm-12 col-xs-12">
							<div className="row">
								<div className="col-md-10">Must change password in next login</div>
								<label className="gs-checkbox-switch">
									<input className="form-check-input" type="checkbox" checked={this.state.changepwdinnextlogin} onChange={(evt) => this.updatePwdDetails('changepwdinnextlogin', evt.target.checked)} />
									<span className="gs-checkbox-switch-slider"></span>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={() => this.close()}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={() => this.save('Reset Password')}><i className="fa fa-check"></i>{this.state.autogenpassword ? 'Ok' : 'RESET'}</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}