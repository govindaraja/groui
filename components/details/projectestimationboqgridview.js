import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import async from 'async';

import { commonMethods, modalService } from '../../utils/services';
import Loadingcontainer from '../loadingcontainer';
import ChildImport from '../../import/childimport';
import * as filter from '../../utils/filter';
import { LocalSelect, SelectAsync, DateElement } from '../utilcomponents';
import BOQGridEditView from './projectestimationboqgridview';

export default class BOQImportModal extends Component {
	constructor(props) {
		super(props);

		let initialArray = [];
		this.setInitialArray(initialArray);

		this.state = {
			initialArray,
			errors: []
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.validate = this.validate.bind(this);
		this.parentValidation = this.parentValidation.bind(this);
		this.importCB = this.importCB.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.formatItemForEdit = this.formatItemForEdit.bind(this);
		this.checkdeleteestimationitem = this.checkdeleteestimationitem.bind(this);
	}

	setInitialArray(initialArray) {
		let itemprop = this.props.resource.projectid > 0 ? 'boqitemsid' : 'boqquoteitemsid';
		let temparray = [];

		if(initialArray.length > 0) {
			initialArray.forEach((item, index) => {
				item.refno = item.boqitemsid > 0 ? item.boqitemsid_internalrefno : (item.boqquoteitemsid > 0 ? item.boqquoteitemsid_internalrefno : item.internalrefno);
				item.index = index;
			});
			return null;
		}

		let boqObj = {};

		this.props.resource.estimationitems.forEach((item, index) => {
			item.refno = item.boqitemsid > 0 ? item.boqitemsid_internalrefno : (item.boqquoteitemsid > 0 ? item.boqquoteitemsid_internalrefno : item.internalrefno);
			item.index = index;

			if(!boqObj[item[itemprop]] && item[itemprop] > 0) {
				boqObj[item[itemprop]] = {
					[`${itemprop}`] : item[itemprop],
					refno: item[`${itemprop}_internalrefno`],
					boqitemname: item.boqitemid_name,
					boqitemitemtype: item.boqitemid_itemtype,
					boqquantity: item[`${itemprop}_quantity`],
					boquomid: item[`${itemprop}_uomid`],
					estimationtype: 'Simple',
					isparent: true,
					childitems: []
				}
			}

			if(boqObj[item[itemprop]] && (!item.isparent || item.isparent && (item.estimationtype == 'Simple'))) {

				if(item.itemid == item.boqitemid && item.boqitemid_itemtype == 'Product') {
					item.olditemid = item.itemid;
					item.oldquantity = item.quantity;
					item.olduomid = item.uomid;
					item.olduomconversionfactor = item.uomconversionfactor;
					item.oldbillinguomid = item.billinguomid;
					item.oldbillingconversionfactor = item.billingconversionfactor;
					item.oldbillingquantity = item.billingquantity;
				}

				boqObj[item[itemprop]].childitems.push(item);
			}
		});

		let boqArray = Object.keys(boqObj).map(prop => boqObj[prop]);

		boqArray.push({
			refno: 'Overhead',
			childitems: []
		});

		this.props.resource.estimationitems.forEach(item => {
			if(!item[itemprop] && !item.isoverhead) {
				boqArray[boqArray.length-1].childitems.push(item);
			}
		});

		boqArray.forEach(item => {
			if(item.childitems.length > 0) {
				item.childitems.forEach((estitem, estindex) => {
					let tempObj = {};

					if(initialArray.length == 0 || (initialArray[initialArray.length-1].refno != item.refno)) {
						tempObj = item;
					}

					for(var prop in estitem) {
						tempObj[prop] = estitem[prop];
					}
					initialArray.push(tempObj);
				});
			}
		});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag})
	}

	formatItemForEdit(item) {
		/*if(item.itemid_itemtype != 'Project')
			item.splitrate = null;

		if(!item.splitrate) {
			item.materialrate = null;
			item.labourrate = null;
		} else {
			if(item.materialrate > 0 || item.labourrate > 0)
				item.rate = null;
		}

		if(item.usebillinguom)
			item.rate = null;

		if(item.boqitemid == item.itemid || item.id > 0)
			item._restrictDelete = true;*/

		/*if(!item.isparent) {
			item.boqitemsid_estimatedqty = null;
			item.boqquoteitemsid_estimatedqty = null;
		}*/

		if(item.boqquoteitemsid_israteonly) {
			item.boqquantity = null;
			item.quantity = null;
			item.billingquantity = null;
		}

		if(item.usebillinguom || item.isoverhead)
			item.rate = null;

		if((item.boqitemid == item.itemid) || item.isoverhead || item.id > 0) {
			item._restrictDelete = true;
		}

		return;
	}

	validate(param) {
		this.updateLoaderFlag(true);
		this.refs.childImport.validate(param);
		this.updateLoaderFlag(false);
	}

	parentValidation(dataArray, cellArray, validCB) {
		let tempItemIdArr = [];

		dataArray.forEach((item) => {
			tempItemIdArr.push(item.itemid);
		});

		axios({
			method : 'post',
			data : {
				actionverb : 'Read',
				data : {
					itemarray: tempItemIdArr,
					type: 'id'
				}
			},
			url : '/api/common/methods/getprojectimportitemdetails'
		}).then((response) => {
			if(response.data.message == 'success') {
				let resultitemArr = response.data.main;
				let resultitemObj = {};
				resultitemArr.forEach((item)=> {
					resultitemObj[item.id] = {
						...item
					};
				});
				
				let internalrefnoArr = [], estimationidObj = {}, boqitemidobj = {}, productitemObj = {}, internalrefObj = {};
				let itemprop = this.props.resource.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';

				dataArray.forEach((dataobj) => {
					if(!internalrefObj[dataobj.refno])
						internalrefObj[dataobj.refno] = 1;
					else
						internalrefObj[dataobj.refno] += 1;
				});

				this.props.resource.estimationitems.forEach((item) => {
					internalrefnoArr.push(item[`${itemprop}_internalrefno`]);

					dataArray.forEach((dataobj) => {
						if(dataobj.refno && dataobj.refno != 'Overhead' && dataobj.refno == item[`${itemprop}_internalrefno`]) {
							dataobj[itemprop] = item[itemprop];
							dataobj.boqitemid = item.boqitemid;
							dataobj.boqitemid_name = item.boqitemid_name;
							dataobj.boqitemid_itemtype = item.boqitemid_itemtype;
							dataobj[`${itemprop}_quantity`] = item[`${itemprop}_quantity`];
							dataobj[`${itemprop}_description`] = item[`${itemprop}_description`];
							dataobj[`${itemprop}_specification`] = item[`${itemprop}_specification`];
							dataobj[`${itemprop}_uomid`] = item[`${itemprop}_uomid`];
							dataobj[`${itemprop}_uomconversionfactor`] = item[`${itemprop}_uomconversionfactor`];
							dataobj[`${itemprop}_clientrefno`] = item[`${itemprop}_clientrefno`];
							dataobj[`${itemprop}_internalrefno`] = item[`${itemprop}_internalrefno`];
							dataobj[`${itemprop}_estimatedqty`] = item[`${itemprop}_estimatedqty`];
							dataobj[`${itemprop}_displayorder`] = item[`${itemprop}_displayorder`];
							dataobj[`${itemprop}_israteonly`] = item[`${itemprop}_israteonly`];
						}
					});
				});

				dataArray.forEach((dataItemObj) => {
					cellArray[dataItemObj._refRowNo][0] = [];

					if(!dataItemObj.refno)
						cellArray[dataItemObj._refRowNo][0].push(`Internal Reference No is required`);

					if(dataItemObj[itemprop] > 0 && dataItemObj.boqitemid_itemtype == 'Product' && dataItemObj.itemid == dataItemObj.boqitemid) {
						if(!boqitemidobj[dataItemObj.refno]) {
							boqitemidobj[dataItemObj.refno] = {
								[`${dataItemObj.itemid}`]: 1
							};
						} else {
							if(!boqitemidobj[dataItemObj.refno][`${dataItemObj.itemid}`])
								boqitemidobj[dataItemObj.refno] = {
									[`${dataItemObj.itemid}`]: 1
								};
							else
								boqitemidobj[dataItemObj.refno][`${dataItemObj.itemid}`] = boqitemidobj[dataItemObj.refno][`${dataItemObj.itemid}`]+1;
						}

						if(boqitemidobj[dataItemObj.refno][`${dataItemObj.itemid}`] > 1) {
							cellArray[dataItemObj._refRowNo][0].push(`This Item cannot be duplicated, please remove this item`);
						}
					}

					if(resultitemObj[dataItemObj.itemid]) {
						dataItemObj.itemid_name = resultitemObj[dataItemObj.itemid].name;
						dataItemObj.itemid_keepstock = resultitemObj[dataItemObj.itemid].keepstock;
						dataItemObj.itemid_issaleskit = resultitemObj[dataItemObj.itemid].issaleskit;
						dataItemObj.itemid_itemtype = resultitemObj[dataItemObj.itemid].itemtype;
						dataItemObj.itemid_usebillinguom = resultitemObj[dataItemObj.itemid].itemid_usebillinguom;
						dataItemObj.itemid_isuseforinternallabour = resultitemObj[dataItemObj.itemid].isuseforinternallabour;
						dataItemObj.description = dataItemObj.description ? dataItemObj.description : resultitemObj[dataItemObj.itemid].description;

						let uomFound = false;
						resultitemObj[dataItemObj.itemid].alternateuoms.forEach((uomitem)=> {
							if(uomitem.id == dataItemObj.uomid) {
								uomFound = true;
								dataItemObj.uomid_name = uomitem.name;
								dataItemObj.uomconversiontype = uomitem.conversiontype;

								if(dataItemObj.uomconversiontype && !dataItemObj.uomconversionfactor) {
									cellArray[dataItemObj._refRowNo][0].push(`UOM Conversion factor is required`);
								}

								if(dataItemObj.uomconversiontype == 'Fixed' && dataItemObj.uomconversionfactor && dataItemObj.uomconversionfactor != uomitem.conversionrate) {
									cellArray[dataItemObj._refRowNo][0].push(`UOM Conversion factor should be only ${uomitem.conversionrate}`);
								}
							}
						});

						if(!uomFound)
							cellArray[dataItemObj._refRowNo][0].push('UOM is invalid');

						if(resultitemObj[dataItemObj.itemid].usebillinguom) {
							dataItemObj.usebillinguom = resultitemObj[dataItemObj.itemid].usebillinguom;

							if(!dataItemObj.billinguomid) {
								cellArray[dataItemObj._refRowNo][0].push('Billing UOM is Required.');
							} else {
								let billinguomFound = false;

								resultitemObj[dataItemObj.itemid].alternateuoms.forEach((uomitem) => {
									if(uomitem.id == dataItemObj.billinguomid) {
										billinguomFound = true;
										dataItemObj.billinguomid_name = uomitem.name;
										dataItemObj.billingconversiontype = uomitem.conversiontype;

										if(dataItemObj.billingconversiontype && !dataItemObj.billingconversionfactor) {
											cellArray[dataItemObj._refRowNo][0].push(`Billing Conversion factor is required`);
										}

										if(dataItemObj.billingconversiontype == 'Fixed' && dataItemObj.billingconversionfactor &&  dataItemObj.billingconversionfactor != uomitem.conversionrate) {
											cellArray[dataItemObj._refRowNo][0].push(`Billing Conversion factor should be only ${uomitem.conversionrate}`);
										}
									}
								});

								if(!billinguomFound) {
									cellArray[dataItemObj._refRowNo][0].push('Billing UOM is invalid');
								}
							}

							if(dataItemObj.billingrate && dataItemObj.billingconversionfactor) {
								dataItemObj.rate = Number(((dataItemObj.billingrate / (dataItemObj.uomconversionfactor ? dataItemObj.uomconversionfactor : 1)) * dataItemObj.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
							}

							if(dataItemObj.quantity && dataItemObj.billingconversionfactor) {
								dataItemObj.billingquantity = Number(((dataItemObj.quantity / (dataItemObj.uomconversionfactor ? dataItemObj.uomconversionfactor : 1)) * dataItemObj.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock));
							}
						} else {
							if(dataItemObj.billinguomid) {
								cellArray[dataItemObj._refRowNo][0].push('Billing UOM should not have a value. For more info, please check the field help.');
							}

							if(dataItemObj.billingrate) {
								cellArray[dataItemObj._refRowNo][0].push('Billing Rate should not have a value. For more info, please check the field help.');
							}

							if(dataItemObj.billingconversionfactor) {
								cellArray[dataItemObj._refRowNo][0].push('Billing Conversion Factor should not have a value. For more info, please check the field help.');
							}
						}
						
						if(!dataItemObj.uomconversiontype && dataItemObj.uomconversionfactor)
							cellArray[dataItemObj._refRowNo][0].push('UOM Conversion factor should not have a value. For more info, please check the field help.');
					}
					
					if(internalrefnoArr.indexOf(dataItemObj.refno) == -1 && dataItemObj.refno != 'Overhead') {
						cellArray[dataItemObj._refRowNo][0].push('BOQ ref no is invalid. For more info, please check the field help.');
					}
				});

				let tempboqObj = {};
				let initialArray =[];

				dataArray.forEach((item, index) => {
					if(!tempboqObj[item[itemprop]] && item[itemprop] > 0) {
						tempboqObj[item[itemprop]] = {
							[`${itemprop}`] : item[itemprop],
							refno: item[`${itemprop}_internalrefno`],
							boqitemid: item.boqitemid,
							boqitemname: item.boqitemid_name,
							boqitemitemtype: item.boqitemid_itemtype,
							boqquantity: item[`${itemprop}_quantity`],
							boquomid: item[`${itemprop}_uomid`],
							_estimatedqty: item[`${itemprop}_estimatedqty`],
							boqquoteitemsid: item.boqquoteitemsid ? item.boqquoteitemsid : null,
							boqquoteitemsid_israteonly: item.boqquoteitemsid ? item.boqquoteitemsid_israteonly : false,
							deleteRow: item.deleteRow,
							_refRowNo: item._refRowNo,
							childitems: []
						}
					}

					if(tempboqObj[item[itemprop]] && (!item.isparent || item.isparent && (item.estimationtype == 'Simple'))) {
						tempboqObj[item[itemprop]].childitems.push(item);
					}
				});

				let boqArray = Object.keys(tempboqObj).map(prop => tempboqObj[prop]);

				boqArray.push({
					refno: 'Overhead',
					childitems: []
				});

				dataArray.forEach(item => {
					if(!item[itemprop] && !item.isoverhead) {
						boqArray[boqArray.length-1].childitems.push(item);
					}
				});

				boqArray.forEach(item => {
					if(item.deleteRow == 'Yes') {
						item.childitems.forEach((dataitem) => {
							dataitem.deleteRow = "Yes";
							if(dataitem.id > 0) {
								estimationidObj[dataitem.id] = dataitem._refRowNo;
							}
						});
					}
					if(item[itemprop] && item.boqitemitemtype == 'Product' && !boqitemidobj[item.refno]) {
						cellArray[item._refRowNo][0].push(`This BOQ item's estimation type is Detailed, this BOQ item should be added as a child.`);
					}

					if(item.boqquoteitemsid && item.boqquoteitemsid_israteonly && internalrefObj[item.refno] > 1) {
						cellArray[item._refRowNo][0].push(`This BOQ item is rate only item, so that you cannot changed to Detailed`);
					}

					if(item[itemprop]) {
						item.childitems.forEach(dataitem => {
							if(dataitem.itemid == item.boqitemid && item.boqitemitemtype == 'Product') {
								/*if(dataitem.quantity != dataitem[`${itemprop}_quantity`])
									cellArray[dataitem._refRowNo][0].push(`Item's Quantity should not be changed`);*/

								if(dataitem.uomid != dataitem[`${itemprop}_uomid`])
									cellArray[dataitem._refRowNo][0].push(`Item's UOM should not be changed`);

								if(dataitem.uomconversionfactor && dataitem.uomconversionfactor !=  dataitem[`${itemprop}_uomconversionfactor`])
									cellArray[dataitem._refRowNo][0].push(`Item's UOM Conversion Factor should not be changed`);

								if(dataitem.billinguomid && dataitem.billinguomid !=  dataitem[`${itemprop}_billinguomid`])
									cellArray[dataitem._refRowNo][0].push(`Item's Billing UOM should not be changed`);

								/*if(dataitem.billingquantity && dataitem.billingquantity !=  dataitem[`${itemprop}_billingquantity`])
									cellArray[dataitem._refRowNo][0].push(`Item's Billing Quantity should not be changed`);*/

								if(dataitem.billingconversionfactor && dataitem.billingconversionfactor !=  dataitem[`${itemprop}_billingconversionfactor`])
									cellArray[dataitem._refRowNo][0].push(`Item's Billing Conversion Factor should not be changed`);

								if(dataitem.deleteRow == 'Yes') {
									cellArray[dataitem._refRowNo][0].push(`Product Item cannot be deleted. Please remove 'Yes' in Delete Row option`);
								}
							}

							/*if(item._estimatedqty != dataitem[`${itemprop}_estimatedqty`]) {
								cellArray[item._refRowNo][0].push(`This BOQ item's estimated quantity should be same for all child item.`);
							}*/
						});
					}
				});

				boqArray.forEach(item => {
					if(item.childitems.length > 0) {
						item.childitems.forEach((estitem, estindex) => {
							let tempObj = {};

							if(initialArray.length == 0 || (initialArray[initialArray.length-1].refno != item.refno)) {
								tempObj = item;
							}

							for(var prop in estitem) {
								tempObj[prop] = estitem[prop];
							}
							initialArray.push(tempObj);
						});
					}
				});

				if(Object.keys(estimationidObj).length > 0)
					this.checkdeleteestimationitem(estimationidObj, initialArray, cellArray, validCB);
				else
					validCB(initialArray, cellArray);
			} else {
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}
	
	checkdeleteestimationitem(estimationidObj, dataArray, cellArray, validCB) {
		axios.get(`/api/query/projectestimationitemdeletecheckquery?projectestimationitemsid=${Object.keys(estimationidObj).join(',')}`).then((response)=> {
			if(response.data.message != 'failure') {
				for(var prop in estimationidObj) {
					if(response.data.main[prop] && response.data.main[prop].length > 0)
						cellArray[estimationidObj[prop]][0].push(`${response.data.message} (${response.data.main[prop].join(', ')})`);
				}
				validCB(dataArray, cellArray);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	importCB(dataArray) {
		this.updateLoaderFlag(true);

		let estimationitems = [];
		let itemprop = this.props.resource.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';
		let uniqueItemsArr = [];
		let uniqueItemObj = {};
		let overheaditems = [];

		dataArray.forEach((item) => {
			if(item[itemprop] > 0) {
				if(!uniqueItemObj[item[itemprop]]) {
					uniqueItemObj[item[itemprop]] = {
						[`${itemprop}`]: item[itemprop],
						[`${itemprop}_displayorder`]: item[`${itemprop}_displayorder`],
						items: []
					};
				}
				if(itemprop == 'boqquoteitemsid') {
					item.quantity = item.boqquoteitemsid_israteonly ? 0 : item.quantity;
				}

				uniqueItemObj[item[itemprop]].items.push(item);
			} else {
				item.internalrefno = 'Overhead';
				overheaditems.push(item);
			}
		});

		uniqueItemsArr = Object.keys(uniqueItemObj).map(prop => uniqueItemObj[prop]);

		uniqueItemsArr.forEach((item) => {
			let tempParentobj = {
				...item.items[0],
				estimationtype : item.items.length > 1 || (item.items[0].itemid != item.items[0].boqitemid) ? 'Detailed' : 'Simple',
				isparent: true
			};

			estimationitems.push(tempParentobj);

			if(tempParentobj.estimationtype == 'Detailed') {
				item.items.forEach((propitem) => {
					propitem.isparent = false;
					estimationitems.push(propitem);
				});
			}
		});

		estimationitems.push({
			isoverhead: true,
			internalrefno: 'Overhead',
			rate: 0
		});
		estimationitems.push(...overheaditems);

		this.props.updateFormState(this.props.form, {
			[`${this.props.resourcename}`] : estimationitems
		});

		setTimeout(() => {
			this.props.computeFinalRate();
			this.updateLoaderFlag(false);
			this.props.closeModal();
		}, 0);
	}
	
	exportExcel() {
		this.refs.childImport.exportExcel();
	}
	
	closeModal() {
		this.updateLoaderFlag(true);
		this.props.openModal(modalService['confirmMethod']({
			header : 'Alert',
			body : 'Changes you made in this grid will not be updated to transaction. Would you like to continue?',
			btnArray : ['Yes', 'No']
		}, (param1)=> {
			if(param1) {
				this.props.closeModal();
			}
			this.updateLoaderFlag(false);
		}));
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<div className="modal-title d-flex justify-content-between">
						<div className="d-flex">
							<h5>Add/Edit Estimation Items</h5>
						</div>
						<div className="d-flex">
							<button type="button" className="btn btn-sm gs-form-btn-success" onClick={this.exportExcel}><i className="fa fa-upload"></i>Export</button>
						</div>
					</div>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-12">
							<ChildImport initialArray={this.state.initialArray} childjson={this.props.childjson} resource={this.props.resource} app={this.props.app} ref="childImport" openModal={this.props.openModal} parentValidation={this.parentValidation} importCB={this.importCB} formatItemForEdit={this.formatItemForEdit} height={$(window).height() - 200} importname = {'Project Estimation Items'} needDelete={true} />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.closeModal} disabled={this.state.loaderflag}><i className="fa fa-times"></i>Close</button>
								{this.props.resource.status != 'Approved' && this.props.resource.status != 'Cancelled' ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={() => this.validate(false)} disabled={this.state.loaderflag}><i className="fa fa-check"></i>Validate</button> : null}
								{this.props.resource.status != 'Approved' && this.props.resource.status != 'Cancelled' ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={() => this.validate(true)} disabled={this.state.loaderflag}><i className="fa fa-check"></i>Done</button> : null}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export class ErrorAlertModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.renderError = this.renderError.bind(this);
	}

	renderError() {
		return this.props.errors.map((error, index) => {
			return(
				<div className="form-group col-md-12" key={index}>
					<div className="alert alert-info">{error}</div>
				</div>
			);
		})
	}

	render() {
		if(this.props.errors.length == 0)
			return null;

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Warning</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							{this.renderError()}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}