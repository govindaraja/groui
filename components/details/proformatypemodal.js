import React, { Component } from 'react';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {};

		this.selectOnChange = this.selectOnChange.bind(this);
		this.closeTypeModal = this.closeTypeModal.bind(this);
	};

	selectOnChange(value) {
		this.setState({
			type: value
		});
	}

	closeTypeModal(param) {
		this.props.closeModal();
		this.props.callback(`${param ? this.state.type : ''}`);
	};

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title text-center">Select Proforma Type</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-8 offset-md-2 text-center expensereqtypemodal">
							<div className={`border rounded float-left p-2 reqtype ${this.state.type == 'Advance' ? 'active' : ''}`} onClick={()=>this.selectOnChange('Advance')}>
								Advance
							</div>
							<div className={`border rounded float-right p-2 reqtype ${this.state.type == 'Completion' ? 'active' : ''}`} onClick={()=>this.selectOnChange('Completion')}>
								Completion
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={() => this.closeTypeModal(false)}><i className="fa fa-times"></i>Close</button>
								<button type="button" className={`btn btn-sm gs-btn-success btn-width`} onClick={() => this.closeTypeModal(true)} disabled={!this.state.type}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}