import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';

export default class extends Component {
	constructor(props) {
		super(props);
		let pendinginvoicearr = this.props.array.map((item) => {
			return {
				...item,
				checked: false
			}
		});
		this.state = {
			pendinginvoicearr: pendinginvoicearr
		};

		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	checkboxOnChange(value, item) {
		item.checked = value;
		this.setState({
			pendinginvoicearr: this.state.pendinginvoicearr
		});
	}

	closeModal(param) {
		if (param) {
			var contractidarray = [];
			let contractObj = {};
			for (var i = 0; i < this.state.pendinginvoicearr.length; i++) {
				if (this.state.pendinginvoicearr[i].checked) {
					contractidarray.push(this.state.pendinginvoicearr[i].id);
					contractObj[this.state.pendinginvoicearr[i].id] = this.state.pendinginvoicearr[i];
				}
			}
			this.props.callback(contractidarray, contractObj);
		}
		this.props.closeModal();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Other Contracts</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							<p>There are other contracts for this customer that are not invoiced. Please choose if you would like to add these contracts to the same invoice</p>
						</div>
						<div className="col-md-12">
							<table className="table table-bordered">
								<tbody>
									{this.state.pendinginvoicearr.map((item, index) => {
										return ( <tr key={index}>
											<td style={{width:'15%',textAlign:'center'}}>
												<input type="checkbox" checked={item.checked} onChange={(evt) =>{this.checkboxOnChange(evt.target.checked, item)}} />
											</td>
											<td>{item.contractno}</td>
										</tr>);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={()=>this.closeModal(true)}><i className="fa fa-check"></i>Choose</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
