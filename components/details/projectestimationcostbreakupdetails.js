import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Field, FieldArray, reduxForm } from 'redux-form';
import axios from 'axios';

import { updateFormState } from '../../actions/actions';
import { ChildElement, checkCondition } from '../../containers/details';
import { commonMethods, modalService, taxEngine } from '../../utils/services';
import { dateFilter, currencyFilter, uomFilter } from '../../utils/filter';
import * as filter from '../../utils/filter';
import { InputEle, NumberEle, SpanEle, DateEle, textareaEle, selectAsyncEle, localSelectEle, autoSelectEle, checkboxEle } from '../formelements';
import { numberValidation, dateValidation, multiSelectValidation, requiredValidation, stringNewValidation, numberNewValidation, dateNewValidation, emailNewValidation } from '../../utils/utils';
import { Init, AutoSelect, SelectAsync } from '../utilcomponents';

class EstimatedCostBreakupDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		
		this.templateOnChange = this.templateOnChange.bind(this);
		this.getTemplateItems = this.getTemplateItems.bind(this);
		this.openNavLink = this.openNavLink.bind(this);
		this.createEstimationtemplate = this.createEstimationtemplate.bind(this);
	}

	createEstimationtemplate() {
		let tempchilditem = this.props.selector(this.props.fullstate, this.props.itemstr);
		let tempObj = {
			boqitemid : tempchilditem.boqitemid,
			productitems : tempchilditem.productitems,
			subcontractitems : tempchilditem.subcontractitems,
			labouritems : tempchilditem.labouritems
		};
		this.props.createOrEdit("/createProjectEstimationTemplate", null, tempObj, (valueObj) => {
			this.setState({
				templateid : valueObj.id
			});
		});
	}

	templateOnChange(value) {
		let tempchilditem = this.props.selector(this.props.fullstate, this.props.itemstr);

		if((tempchilditem.productitems.length > 0 || tempchilditem.subcontractitems.length > 0 || tempchilditem.labouritems.length > 0) && value) {
			this.props.openModal({
				render: (closeModal) => {
					return <TemplateConfirmModal callback={(param)=> {
						this.setState({
							templateid : value
						}, () => {
							this.getTemplateItems(param);
						});
					}} closeModal={closeModal} />
				}, className: {
					content: 'react-modal-custom-class',
					overlay: 'react-modal-overlay-custom-class'
				}
			});
		} else if(value) {
			this.setState({
				templateid : value
			}, () => {
				this.getTemplateItems();
			});
		} else {
			this.setState({
				templateid : value
			});
		}
	}

	getTemplateItems(param) {
		let tempchilditem = this.props.selector(this.props.fullstate, this.props.itemstr);
		let productitems = tempchilditem.productitems,
		subcontractitems = tempchilditem.subcontractitems,
		labouritems = tempchilditem.labouritems;
		if(param) {
			productitems = [];
			subcontractitems = [];
			labouritems = [];
		}

		axios.get(`/api/projectestimationtemplateitems?field=itemid,description,quantity,uomid,activitytypeid,itemmaster/name/itemid,itemmaster/itemtype/itemid,itemmaster/lastpurchasecost/itemid,itemmaster/defaultpurchasecost/itemid,timesheetactivitytypes/name/activitytypeid&filtercondition=projectestimationtemplateitems.parentid=${this.state.templateid}`).then((response)=> {
			if (response.data.message == 'success') {
				let templateitemArr = response.data.main;
				templateitemArr.forEach((item) => {
					item.boqitemid = tempchilditem.boqitemid;
					item.boqitemid_name = tempchilditem.boqitemid_name;
					item.boqitemid_itemtype = tempchilditem.boqitemid_itemtype;
					item.boqitemid_defaultpurchasecost = tempchilditem.boqitemid_defaultpurchasecost;
					item.boqitemid_lastpurchasecost = tempchilditem.boqitemid_lastpurchasecost;

					if(tempchilditem.boqquoteitemsid) {
						item.boqquoteitemsid = tempchilditem.boqquoteitemsid;
						item.boqquoteitemsid_quantity = tempchilditem.boqquoteitemsid_quantity;
						item.boqquoteitemsid_estimatedqty = tempchilditem.boqquoteitemsid_estimatedqty;
						item.boqquoteitemsid_description = tempchilditem.boqquoteitemsid_description;
						item.boqquoteitemsid_uomid = tempchilditem.boqquoteitemsid_uomid;
						item.boqquoteitemsid_clientrefno = tempchilditem.boqquoteitemsid_clientrefno;
						item.boqquoteitemsid_internalrefno = tempchilditem.boqquoteitemsid_internalrefno;
					} else {
						item.boqitemsid = tempchilditem.boqitemsid;
						item.boqitemsid_quantity = tempchilditem.boqitemsid_quantity;
						item.boqitemsid_estimatedqty = tempchilditem.boqitemsid_estimatedqty;
						item.boqitemsid_description = tempchilditem.boqitemsid_description;
						item.boqitemsid_uomid = tempchilditem.boqitemsid_uomid;
						item.boqitemsid_clientrefno = tempchilditem.boqitemsid_clientrefno;
						item.boqitemsid_internalrefno = tempchilditem.boqitemsid_internalrefno;
					}

					if(item.itemid_itemtype == 'Product') {
						productitems.push(item);
					}
					if(tempchilditem.boqitemid_itemtype == 'Product' && item.itemid_itemtype == 'Service') {
						subcontractitems.push(item);
					}
					if(tempchilditem.boqitemid_itemtype == 'Project' && item.itemid_itemtype == 'Project' && tempchilditem.boqitemid == item.itemid) {
						subcontractitems.push(item);
					}
					if(tempchilditem.boqitemid_itemtype == 'Service' && item.itemid_itemtype == 'Service' && tempchilditem.boqitemid == item.itemid) {
						subcontractitems.push(item);
					}
					if(item.activitytypeid) {
						labouritems.push(item);
					}
				});

				this.props.updateFormState(this.props.form, {
					[`${this.props.itemstr}.productitems`] : productitems,
					[`${this.props.itemstr}.subcontractitems`] : subcontractitems,
					[`${this.props.itemstr}.labouritems`] : labouritems,
				});
				this.props.openModal(modalService["infoMethod"]({
					header : "Success",
					body : 'Template items added.',
					btnArray : ["Ok"]
				}));
			}
		});
	}

	openNavLink(param) {
		let index = this.props.itemstr.split('[')[1].split(']')[0];
		let tempindex = Number(index);
		if(param == 'previous')
			tempindex -= 1;
		if(param == 'next')
			tempindex += 1;

		let item  = this.props.resource.estimationitems[tempindex] ? this.props.resource.estimationitems[tempindex] : null;
		let itemstr = this.props.itemstr.replace(index, tempindex);
		
		if(Object.keys(item).length > 0) {
			this.props.callback(item, itemstr);
			this.props.closeModal();	
		} else {
			this.props.openModal(modalService["infoMethod"]({
				header : "Error",
				body : `No more ${param} item have Detailed Breakup`,
				btnArray : ["Ok"],
			}));
		}		
	}

	render() {
		let index = this.props.itemstr.split('[')[1].split(']')[0];
		let tempchilditem = this.props.selector(this.props.fullstate, this.props.itemstr);
		let disableclass = this.props.resource && ['Approved'].indexOf(this.props.resource.status) >= 0 ? 'disablediv' : '';

		return (
			<div className={`react-outer-modal ${disableclass}`}>
				<div className="react-modal-header" style={{display: 'flex'}}>
				<div style={{flexDirection: 'column', width: '50%'}}>
					<div className="modal-title float-left marginright-15 font-18">Estimation Details</div>
					{index > 0 ? <button type="button" className="btn btn-sm gs-btn-light" onClick={() => this.openNavLink('previous')} rel="tooltip" title="Go Previous"><span className="fa fa-chevron-left"></span></button> : null}
 					{(index < this.props.resource.estimationitems.length-1) ? <button type="button" className="btn btn-sm gs-btn-light" onClick={() => this.openNavLink('next')} rel="tooltip" title="Go Next"><span className="fa fa-chevron-right"></span></button> : null }
				</div>
					{tempchilditem.estimationtype == 'Detailed' ? <div style={{flexDirection: 'column', width: '45%'}}>
						<div style={{float: 'left', width: '30%'}}>Choose Template : </div>
						<div style={{float: 'left', width: '50%', marginRight: '10px'}}>
							<SelectAsync resource="projectestimationtemplate" fields="id,name" filter={`projectestimationtemplate.boqitemid=${tempchilditem.boqitemid}`} value={this.state.templateid || null} onChange={(value)=>{this.templateOnChange(value)}} />
						</div>
						<button type="button" className="btn btn-sm gs-btn-success btnhide" onClick={this.createEstimationtemplate}><span className="fa fa-plus"></span></button>
					</div> : null}
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered">
								<tbody>
									<tr>
										<td style={{width: '25%'}}>
											<span>Internal Ref. No</span>
											<br></br>
											<span>{`${tempchilditem.boqquoteitemsid ? tempchilditem.boqquoteitemsid_internalrefno || '' : tempchilditem.boqitemsid_internalrefno || ''}`}</span>
										</td>
										<td style={{width: '25%'}}>
											<span>Client Ref. No</span>
											<br></br>
											<span>{`${tempchilditem.boqquoteitemsid ? tempchilditem.boqquoteitemsid_clientrefno || '' : tempchilditem.boqitemsid_clientrefno || ''}`}</span>
										</td>
										<td style={{width: '30%'}}>
											<span>Item Name</span>
											<br></br>
											<span>{`${tempchilditem.boqitemid_name}`}</span>
										</td>
										<td>
											<div className="text-ellipsis">
												<span>Item Description</span>
												<br></br>
												<span>{`${tempchilditem.boqquoteitemsid ? tempchilditem.boqquoteitemsid_description : tempchilditem.boqitemsid_description}`}</span>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div className="text-ellipsis">
												<span>Specification</span>
												<div dangerouslySetInnerHTML={{ __html: tempchilditem.boqquoteitemsid ? (tempchilditem.boqquoteitemsid_specification ? tempchilditem.boqquoteitemsid_specification : '') : (tempchilditem.boqitemsid_specification ? tempchilditem.boqitemsid_specification : '')}} ></div>
											</div>
										</td>
										<td>
											<span>Quantity</span>
											<br></br>
											<span>{`${tempchilditem.boqquoteitemsid ? tempchilditem.boqquoteitemsid_quantity : tempchilditem.boqitemsid_quantity} ${uomFilter(tempchilditem.boqquoteitemsid_uomid ? tempchilditem.boqquoteitemsid_uomid : tempchilditem.boqitemsid_uomid, this.props.app.uomObj)}`}</span>
										</td>
										{tempchilditem.usebillinguom ? <td>
											<span>Billing Quantity</span>
											<br></br>
											<span>{`${tempchilditem.billingquantity} ${uomFilter(tempchilditem.billinguomid, this.props.app.uomObj)}`}</span>
										</td> : null}
										{!this.props.restrictestimationcommercials ? <td>
											<span>Default Purchase Cost</span>
											<br></br>
											<span>{tempchilditem.boqitemid_defaultpurchasecost}</span>
										</td> : null}
										{!tempchilditem.usebillinguom && !this.props.restrictestimationcommercials ? <td>
											<span>Last Purchase Cost</span>
											<br></br>
											<span>{tempchilditem.boqitemid_lastpurchasecost}</span>
										</td> : null}
										{tempchilditem.boqitemsid && this.props.restrictestimationcommercials ? <td>
											<span>Estimated Quantity</span>
											<br></br>
											<Field name={`${this.props.itemstr}.boqitemsid_estimatedqty`} props={{onChange : ()=>{this.props.computeFinalRate()}}} component={NumberEle} />
										</td> : null}
										{tempchilditem.boqquoteitemsid && this.props.restrictestimationcommercials ? <td>
											<span>Estimated Quantity</span>
											<br></br>
											<Field name={`${this.props.itemstr}.boqquoteitemsid_estimatedqty`} props={{onChange : ()=>{this.props.computeFinalRate()}}} component={NumberEle} />
										</td> : null}
									</tr>
									<tr>
										{tempchilditem.usebillinguom && !this.props.restrictestimationcommercials ? <td>
											<span>Last Purchase Cost</span>
											<br></br>
											<span>{tempchilditem.boqitemid_lastpurchasecost}</span>
										</td> : null}
										{tempchilditem.boqitemsid && !this.props.restrictestimationcommercials ? <td>
											<span>Estimated Quantity</span>
											<br></br>
											<Field name={`${this.props.itemstr}.boqitemsid_estimatedqty`} props={{onChange : ()=>{this.props.computeFinalRate()}}} component={NumberEle} />
										</td> : null}
										{tempchilditem.boqquoteitemsid && !this.props.restrictestimationcommercials ? <td>
											<span>Estimated Quantity</span>
											<br></br>
											<Field name={`${this.props.itemstr}.boqquoteitemsid_estimatedqty`} props={{onChange : ()=>{this.props.computeFinalRate()}}} component={NumberEle} />
										</td> : null}
										{tempchilditem.usebillinguom && !this.props.restrictestimationcommercials ? <td>
											<span>Estimated Billing Cost (Per Unit)</span>
											<br></br>
											<Field name={`${this.props.itemstr}.billingrate`} props={{disabled: tempchilditem.estimationtype == 'Detailed', onChange : ()=>{this.props.estimationBillingRateOnChange(this.props.itemstr)}}} component={NumberEle} />
										</td> : null}
										{!this.props.restrictestimationcommercials ? <td>
											<span>Estimated Cost (Per Unit)</span>
											<br></br>
											<Field name={`${this.props.itemstr}.rate`} props={{disabled: (tempchilditem.estimationtype == 'Detailed' || tempchilditem.usebillinguom), onChange : ()=>{this.props.computeFinalRate()}}} component={NumberEle} />
										</td> : null}
										{!tempchilditem.usebillinguom && !this.props.restrictestimationcommercials ? <td colSpan="2">
											<span>Estimated Amount</span>
											<br></br>
											<Field name={`${this.props.itemstr}.amount`} props={{disabled:true}} component={NumberEle} />
										</td> : null}
									</tr>
									<tr>
										{tempchilditem.usebillinguom && !this.props.restrictestimationcommercials ? <td>
											<span>Estimated Amount</span>
											<br></br>
											<Field name={`${this.props.itemstr}.amount`} props={{disabled:true}} component={NumberEle} />
										</td> : null}
										<td colSpan={`${tempchilditem.usebillinguom ? 2 : 3}`}>
											<span>Remarks</span>
											<br></br>
											<Field name={`${this.props.itemstr}.remarks`} props={{}} component={textareaEle} />
										</td>
										<td>
											<span>Estimation Type</span>
											<br></br>
											<Field name={`${this.props.itemstr}.estimationtype`} props={{
													options : ["Simple", "Detailed"],
													onChange: (value) => {this.props.estimationtypeonChange(this.props.itemstr)},
													required: true
											}} component={localSelectEle} validate={[stringNewValidation({required: true})]}/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					{tempchilditem.estimationtype == 'Detailed' ? <div className="row">
						<div className="col-md-12">
							<div className="card marginbottom-15 borderradius-0 gs-card">
								<div className="card-header borderradius-0 card-header-custom gs-card-header">Materials</div>
								<div className="card-body" style={{paddingTop : '5px'}}>
									<div className="row responsive-form-element">
										<FieldArray name={`${this.props.itemstr}.productitems`} json={this.props.pagejson.productitems} getFieldString={this.props.getFieldString} resource={this.props.resource} checkCondition={this.props.checkCondition} array={this.props.array} app={this.props.app} updateFormState={this.props.updateFormState} form={this.props.form} component={ChildElement} parentitemstr={this.props.itemstr} computeFinalRate={this.props.computeFinalRate} />
									</div>
								</div>
							</div>
						</div>
					</div> : null}
					{tempchilditem.estimationtype == 'Detailed' ? <div className="row">
						<div className="col-md-12">
							<div className="card marginbottom-15 borderradius-0 gs-card">
								<div className="card-header borderradius-0 card-header-custom gs-card-header">Sub Contract</div>
								<div className="card-body" style={{paddingTop : '5px'}}>
									<div className="row responsive-form-element">
										<FieldArray name={`${this.props.itemstr}.subcontractitems`} json={this.props.pagejson.subcontractitems} getFieldString={this.props.getFieldString} resource={this.props.resource} checkCondition={this.props.checkCondition} array={this.props.array} app={this.props.app} updateFormState={this.props.updateFormState} form={this.props.form} component={ChildElement} parentitemstr={this.props.itemstr} computeFinalRate={this.props.computeFinalRate} />
									</div>
								</div>
							</div>
						</div>
					</div> : null}
					{tempchilditem.estimationtype == 'Detailed' ? <div className="row">
						<div className="col-md-12">
							<div className="card marginbottom-15 borderradius-0 gs-card">
								<div className="card-header borderradius-0 card-header-custom gs-card-header">Internal Labour</div>
								<div className="card-body" style={{paddingTop : '5px'}}>
									<div className="row responsive-form-element">
										<FieldArray name={`${this.props.itemstr}.labouritems`} json={this.props.pagejson.labouritems} getFieldString={this.props.getFieldString} resource={this.props.resource} checkCondition={this.props.checkCondition} array={this.props.array} app={this.props.app} updateFormState={this.props.updateFormState} form={this.props.form} component={ChildElement} parentitemstr={this.props.itemstr} computeFinalRate={this.props.computeFinalRate} />
									</div>
								</div>
							</div>
						</div>
					</div> : null}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.props.closeModal}><i className="fa fa-check-square-o"></i>Done</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export class TemplateConfirmModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			confirmloader: false
		};
		this.cancel = this.cancel.bind(this);
	}

	cancel (result) {
		this.setState({confirmloader: true}, ()=>{
			this.props.callback(result);
			this.props.closeModal();
		});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5>Confirm</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<p>Estimation items already exist. What would you like to do?</p>
				</div>
				<div className="react-modal-footer text-right">
					<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}>Cancel</button>
					<button type="button" className="btn btn-sm gs-btn-warning btn-width" disabled={this.state.confirmloader} onClick={()=>this.cancel(true)}>Replace existing Items</button>
					<button type="button" className="btn btn-sm gs-btn-success btn-width" disabled={this.state.confirmloader} onClick={()=>this.cancel(false)}>Add to existing items</button>
				</div>
			</div>
		);
	}
}

export default EstimatedCostBreakupDetails;