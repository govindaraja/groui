import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter, currencyFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { LocalSelect, AutoSelect } from '../utilcomponents';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tabClasses: ["show active", ""],
			search: {},
			pendingprojectItemArr: [],
			projectItemArr: []
		};

		this.onLoad = this.onLoad.bind(this);
		this.addProjectItem = this.addProjectItem.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
		this.quantityOnChange = this.quantityOnChange.bind(this);
		this.getTabClass = this.getTabClass.bind(this);
		this.getTabPaneClass = this.getTabPaneClass.bind(this);
		this.setActiveTab = this.setActiveTab.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let projectItemArr = [], pendingprojectItemArr = [];
		let projectCustomFields = this.props.getCustomFields('projectitems','string');

		axios.get(`/api/projectitems?field=id,itemid,itemmaster/name/itemid,itemmaster/itemtype/itemid,description,splitrate,labourrate,materialrate,rate,uomid,uom/name/uomid,quantity,deliveredqty,invoicedqty,discountmode,discountquantity,labourtaxid,materialtaxid,taxid,taxdetails,amount,finalratelc,ratelc,amountwithtax,displayorder,uomconversiontype,uomconversionfactor,itemmaster/usebillinguom/itemid,usebillinguom,billinguomid,billingrate,billingquantity,billingconversiontype,billingconversionfactor,internalrefno,clientrefno,projects/currencyid/parentid,index${projectCustomFields ? (','+projectCustomFields) : ''}&sortstring=projectitems.displayorder&filtercondition=projectitems.itemtype='Item' and projectitems.parentid=${this.props.projectid}`).then((response) => {
			if (response.data.message == 'success') {
				let projectItemArr = response.data.main;
				projectItemArr.forEach((item) => {
					item.invoicedqty = item.invoicedqty || 0;
					item.deliveredqty = item.deliveredqty || 0;
					if (item.itemid_itemtype == 'Product' && item.deliveredqty > 0) {
						if(item.invoicedqty < item.deliveredqty) {
							item.pendinginvoiceqty = item.deliveredqty - item.invoicedqty;
							item.qty = item.pendinginvoiceqty;
						}
					}
					if(item.itemid_itemtype != 'Product') {
						item.pendinginvoiceqty = item.quantity - item.invoicedqty;
						item.qty = item.pendinginvoiceqty;
					}
				});
				
				for (var i = 0; i < this.props.salesinvoiceitems.length; i++) {
					for (var j = 0; j < projectItemArr.length; j++) {
						if (this.props.salesinvoiceitems[i].sourceid == projectItemArr[j].id) {
							projectItemArr[j].checked = true;
						}
					}
				}

				projectItemArr.forEach((item) => {
					if (item.pendinginvoiceqty > 0) {
						pendingprojectItemArr.push(item);
					}
				});

				this.setState({
					projectItemArr,
					pendingprojectItemArr
				}, () => {
					this.setActiveTab(1);
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	addProjectItem(id, valueobj) {
		let tempProjectItem = [], errArr = [];
		let itemFound = false;

		for (var i = 0; i < this.state.projectItemArr.length; i++) {
			if (this.state.projectItemArr[i].checked == true) {
				itemFound = true;
				if(this.state.projectItemArr[i].quantity <= 0) {
					errArr.push(`Item : "${this.state.projectItemArr[i].itemid_name}" quantity should be greater than 0`);
				}
				tempProjectItem.push(this.state.projectItemArr[i]);
			}
		}
		if(!itemFound) {
			errArr.push("Please choose atleast one item");
		}
		
		if(errArr.length > 0) {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : errArr,
				btnArray : ['Ok']
			}));
		} else {
			this.props.callback(tempProjectItem);
			this.props.closeModal();
		}
	}

	inputonChange(value, itemstr) {
		let { search } = this.state;
		search[itemstr] = value;
		this.setState({search});
	}

	checkboxOnChange(value, item) {
		item.checked = value;
		this.setState({
			projectItemArr: this.state.projectItemArr,
			pendingprojectItemArr: this.state.pendingprojectItemArr
		});
	}

	checkallonChange(value, itemarr) {
		this.state[itemarr].forEach((item) => {
			item.checked = value;
		});
		this.setState({	[itemarr] : this.state[itemarr], checkall : value });
	}

	quantityOnChange(value, item) {
		item.qty = value > 0 ? Number(value) : "";
		this.setState({
			projectItemArr: this.state.projectItemArr,
			pendingprojectItemArr: this.state.pendingprojectItemArr
		});
	}

	getTabClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	getTabPaneClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	setActiveTab(tabNum) {
		let { tabClasses } = this.state;
		tabClasses =  ["", ""]
		tabClasses[tabNum] = "show active";
		this.setState({tabClasses}, ()=>{
			this.getTabPaneClass(tabNum);
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Project Item Details</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					{this.state.projectItemArr.length > 0 ? <div className="row">
						<div className="form-group col-md-3">
							<label className="labelclass">Ref No</label>
							<input type="text" className="form-control" value={this.state.search.internalrefno || ''} placeholder="Search by ref no" onChange={(evt)=>{this.inputonChange(evt.target.value, 'internalrefno')}}/>
						</div>
						<div className="form-group col-md-3">
							<label className="labelclass">Item Name</label>
							<input type="text" className="form-control" value={this.state.search.itemid_name || ''} placeholder="Search by item name" onChange={(evt)=>{this.inputonChange(evt.target.value, 'itemid_name')}}/>
						</div>
						<div className="form-group col-md-3">
							<label className="labelclass">Description</label>
							<input type="text" className="form-control" value={this.state.search.description || ''} placeholder="Search by description" onChange={(evt)=>{this.inputonChange(evt.target.value, 'description')}}/>
						</div>
					</div> : null}
					{this.state.projectItemArr.length > 0 ? <div className="row">
						<div className="col-md-12">
							<ul className="nav nav-tabs">
								<li className="nav-item">
									<a className={`nav-link ${this.getTabClass(1)}`} onClick={()=>this.setActiveTab(1)}>Invoice Pending Items</a>
								</li>
								<li className="nav-item">
									<a className={`nav-link ${this.getTabClass(2)}`} onClick={()=>this.setActiveTab(2)} >All Items</a>
								</li>
							</ul>
							<div className="tab-content">
								<div className={`tab-pane fade ${this.getTabPaneClass(1)}`} role="tabpanel">
									<div className="row margintop-25">
										<div className="col-md-12 col-sm-12 col-xs-12">
											{this.state.pendingprojectItemArr.length > 0 ? <table className="table table-bordered" >
												<thead>
													<tr>
														<th className="text-center"><input type="checkbox" checked={this.state.checkall || false} onChange={(evt)=>{this.checkallonChange(evt.target.checked, 'pendingprojectItemArr')}}/></th>
														<th className="text-center" style={{width: '10%'}}>Ref No</th>
														<th className="text-center" style={{width: '20%'}}>Item Name</th>
														<th className="text-center" style={{width: '20%'}}>Description</th>
														<th className="text-center">Not Invoiced Qty</th>
														<th className="text-center">Amount</th>
														<th className="text-center">Quantity</th>
													</tr>
												</thead>
												<tbody>
													{search(this.state.pendingprojectItemArr, this.state.search).map((item, index)=> {
														return (
															<tr key={index}>
																<td className="text-center"><input type="checkbox" checked={item.checked || false} onChange={(evt)=>{this.checkboxOnChange(evt.target.checked, item)}}/></td>
																<td>{item.internalrefno}</td>
																<td>{item.itemid_name}</td>
																<td>{item.description}</td>
																<td className="text-center">{item.pendinginvoiceqty} {item.uomid_name}</td>
																<td className="text-center">{currencyFilter(item.amount, item.parentid_currencyid, this.props.app)}</td>
																<td className="text-center">
																	<input type="number" className="form-control" value={item.qty} onChange={(evt) => this.quantityOnChange(evt.target.value,item)} />
																</td>
															</tr>
														);
													})}
												</tbody>
											</table> : <div className="alert alert-info text-center">There is no invoice pending items</div>}
										</div>
									</div>
								</div>
								<div className={`tab-pane fade ${this.getTabPaneClass(2)}`} role="tabpanel">
									<div className="row margintop-25">
										<div className="col-md-12 col-sm-12 col-xs-12">
											<table className="table table-bordered">
												<thead>
													<tr>
														<th className="text-center"><input type="checkbox" checked={this.state.checkall || false} onChange={(evt)=>{this.checkallonChange(evt.target.checked, 'projectItemArr')}}/></th>
														<th className="text-center" style={{width: '10%'}}>Ref No</th>
														<th className="text-center" style={{width: '20%'}}>Item Name</th>
														<th className="text-center" style={{width: '20%'}}>Description</th>
														<th className="text-center">BOQ Qty</th>
														<th className="text-center">Amount</th>
														<th className="text-center">Invoiced Qty</th>
														<th className="text-center">Quantity</th>
													</tr>
												</thead>
												<tbody>
													{search(this.state.projectItemArr, this.state.search).map((item, index)=> {
														return (
															<tr key={index}>
																<td className="text-center"><input type="checkbox" checked={item.checked || false} onChange={(evt)=>{this.checkboxOnChange(evt.target.checked, item)}}/></td>
																<td>{item.internalrefno}</td>
																<td>{item.itemid_name}</td>
																<td>{item.description}</td>
																<td className="text-center">{item.quantity} {item.uomid_name}</td>
																<td className="text-center">{currencyFilter(item.amount, item.parentid_currencyid, this.props.app)}</td>
																<td className="text-center">{item.invoicedqty} {item.uomid_name}</td>
																<td className="text-center">
																	<input type="number" className="form-control" value={item.qty} onChange={(evt) => this.quantityOnChange(evt.target.value,item)} />
																</td>
															</tr>
														);
													})}
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> : <div className="row"><div className="col-md-12 alert alert-info text-center">Please wait...</div></div>}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addProjectItem}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
