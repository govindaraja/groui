import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect } from '../utilcomponents';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			includeitem: {
				quantity: 1
			},
			childitems: []
		};
		this.callbackProduct = this.callbackProduct.bind(this);
		this.getItemDetails = this.getItemDetails.bind(this);
		this.openItemAddOn = this.openItemAddOn.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.addItem = this.addItem.bind(this);
	}

	callbackProduct(value, valueobj) {
		this.props.updateLoaderFlag(true);
		let { includeitem } = this.state;
		includeitem.itemid =  value;
		includeitem.itemid_name = valueobj.name;

		this.setState({
			includeitem,
			childitems: []
		}, () => {
			if(this.props.param == 'addon')
				this.openItemAddOn();
			else
				this.getItemDetails();
		});
	}

	getItemDetails () {
		let { includeitem, childitems } = this.state;
		let promise = commonMethods.getItemDetails(includeitem.itemid, this.props.resource.partnerid, null, 'sales');
		promise.then((returnObject) => {
			if (returnObject.itemid_issaleskit) {
				for (var i = 0; i < returnObject.kititems.length; i++) {
					returnObject.kititems[i].rootindex = 0;
					returnObject.kititems[i].checked = returnObject.kititems[i].ismandatoryoption ? true : false;
					childitems.push(returnObject.kititems[i]);
				}
			}
			includeitem.itemid_name = returnObject.itemid_name;
			this.setState({
				includeitem,
				childitems
			});
		}, (reason) => {});
		this.props.updateLoaderFlag(false);
	}

	openItemAddOn (item) {
		this.props.updateLoaderFlag(true);
		let { includeitem, childitems } = this.state;

		axios.get(`/api/query/getaddonitemquery?itemid=${includeitem.itemid}&pricelistid=${this.props.resource.pricelistid}`).then((response) => {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.main.length; i++) {
					response.data.main[i].checked = response.data.main[i].ismandatoryoption ? true : false;
					childitems.push(response.data.main[i]);
				}
				this.setState({childitems});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.props.updateLoaderFlag(false);
		});
	}

	addItem () {
		let { childitems } = this.state;
		for (var i = 0; i < childitems.length; i++) {
			let itemFound = false;
			for (var j = 0; j < this.props.resource.stockreservationitems.length; j++) {
				if (childitems[i].itemid == this.props.resource.stockreservationitems[j].itemid && childitems[i].checked) {
					this.props.resource.stockreservationitems[j].quantity = this.props.resource.stockreservationitems[j].quantity + (childitems[i].quantity * this.state.includeitem.quantity);
					itemFound = true;
				}
			}
			if (!itemFound && childitems[i].checked && childitems[i].itemid_keepstock) {
				this.props.array.push('stockreservationitems', {
					itemid : childitems[i].itemid,
					itemid_name : childitems[i].itemid_name,
					description : childitems[i].description,
					uomid : childitems[i].uomid,
					uomid_name : childitems[i].uomid_name,
					quantity :  (childitems[i].quantity * this.state.includeitem.quantity),
					remarks : this.props.param == 'addon' ? 'Addon item for '+this.state.includeitem.itemid_name : 'Sales Kit for '+this.state.includeitem.itemid_name
				});
			}
		}
		this.props.closeModal();
	}

	checkboxOnChange(value, index) {
		let { childitems } = this.state;
		childitems[index].checked = value;
		this.setState({childitems});
	}

	inputonChange(value) {
		let { includeitem } = this.state;
		includeitem.quantity = value;
		this.setState({includeitem});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">
						{this.props.param == 'addon' ? <span>Include Item Addon</span> : <span>Include Kit Item</span>}
					</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-8 offset-md-2">
							<div className="row">
								<div className="col-md-6 form-group">
									<label className="labelclass">Item Name</label>
									<AutoSelect resource={'itemmaster'} fields={'id,name,recommendedsellingprice,description'} value={this.state.includeitem.itemid} label={'name'} valuename={'id'} filter={this.props.param == 'addon' ? 'itemmaster.hasaddons=true' : 'itemmaster.issaleskit=true'} onChange={this.callbackProduct} />
								</div>
								<div className="col-md-6 form-group">
									<label className="labelclass">Quantity</label>
									<input type="text" className="form-control" value={this.state.includeitem.quantity} onChange={(evt) =>{this.inputonChange(evt.target.value)}}/>
								</div>
							</div>
						</div>
					</div>
					{this.state.childitems.length > 0 ? <div className="row">
						<div className="col-md-12 col-sm-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th></th>
										<th>Item Name</th>
										<th>Description</th>
										<th>UOM</th>
									</tr>
								</thead>
								<tbody>
									{this.state.childitems.map((item, index) => {
										return (
											<tr key={index}>
												<td className="text-center"><input type="checkbox" checked={item.checked || false} onChange={(evt) =>{this.checkboxOnChange(evt.target.checked, index)}}/></td>
												<td>{item.itemid_name}</td>
												<td>{item.description}</td>
												<td>{item.uomid_name}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div> : null }
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								{this.state.childitems.length > 0 ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addItem}><i className="fa fa-check-square-o"></i>Add</button> : null }
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
