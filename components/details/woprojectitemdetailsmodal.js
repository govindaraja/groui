import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: {},
			projectItemArray: [],
			orderItemArray: [],
			showdescription: false
		};
		this.onLoad = this.onLoad.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.addProjectItem = this.addProjectItem.bind(this);
		this.descToggleOnChange = this.descToggleOnChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let { projectItemArray, orderItemArray } = this.state;

		if(this.props.sourcename == 'Projects') {
			axios.get(`/api/projectitems?field=id,itemid,uomid,itemmaster/name/itemid,itemmaster/itemtype/itemid,description,itemmaster/keepstock/itemid,uomid,uom/name/uomid,quantity,rate,discount,amount,discountquantity,discountmode,splitrate,materialrate,labourrate,uomgroupid,itemmaster/size/itemid,itemmaster/quantitytype/itemid,itemnamevariationid,number,uomconversionfactor,uomconversiontype,alternateuom,itemmaster/itemcategorymasterid/itemid,itemmaster/itemgroupid/itemid&filtercondition=projectitems.parentresource='projects' and projectitems.itemtype='Item' and itemmaster_itemid.itemtype in ('Service', 'Project') and itemmaster_itemid.allowpurchase and projectitems.parentid=${this.props.resource.projectid}`).then((response) => {
				if (response.data.message == 'success') {
					projectItemArray = response.data.main;
					projectItemArray.forEach((item) => {
						item.boqitemsid = item.id;
						delete item.id;
					});

					for (var i = 0; i < this.props.resource.workorderitems.length; i++) {
						for (var j = 0; j < projectItemArray.length; j++) {
							if (this.props.resource.workorderitems[i].boqitemsid == projectItemArray[j].boqitemsid) {
								projectItemArray[j].checked = true;
							}
						}
					}
					this.setState({projectItemArray});
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		} else {
			axios.get(`/api/orderitems?field=itemid,uomid,itemmaster/name/itemid,itemmaster/itemtype/itemid,description,itemmaster/keepstock/itemid,uomid,uom/name/uomid,quantity,rate,discount,amount,discountquantity,discountmode,uomgroupid,itemmaster/size/itemid,itemmaster/quantitytype/itemid,itemnamevariationid,number,uomconversionfactor,uomconversiontype,alternateuom,itemmaster/itemcategorymasterid/itemid,itemmaster/itemgroupid/itemid&filtercondition=orderitems.parentresource='orders' and itemmaster_itemid.allowpurchase and itemmaster_itemid.itemtype in ('Service','Project') and orderitems.parentid=${this.props.resource.orderid}`).then((response) => {
				if (response.data.message == 'success') {
					orderItemArray = response.data.main;
					for (var i = 0; i < this.props.resource.workorderitems.length; i++) {
						for (var j = 0; j < orderItemArray.length; j++) {
							if (this.props.resource.workorderitems[i].itemid == orderItemArray[j].itemid)
								orderItemArray[j].checked = true;
						}
					}
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}
	}

	inputonChange(value) {
		let { search } = this.state;
		search.itemid_name = value;
		this.setState({search});
	}

	checkboxOnChange(item, value) {
		item.checked = value;
		this.setState({
			projectItemArray: this.state.projectItemArray,
			orderItemArray: this.state.orderItemArray,
		});
	}

	addProjectItem () {
		let itemArray = [], errArr = [];
		if(this.props.sourcename == 'Projects') {
			for (var i = 0; i < this.state.projectItemArray.length; i++) {
				if (this.state.projectItemArray[i].checked && this.state.projectItemArray[i].quantity <= 0) {
					errArr.push(`Item : "${this.state.projectItemArray[i].itemid_name}" quantity should be greater than 0`);
				}
				itemArray.push(this.state.projectItemArray[i]);
			}
		} else {
			for (var i = 0; i < this.state.orderItemArray.length; i++) {
				if (this.state.orderItemArray[i].checked)
					itemArray.push(this.state.orderItemArray[i][i]);
			}
		}

		if(errArr.length > 0) {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : errArr,
				btnArray : ['Ok']
			}));
		}

		this.props.callback(itemArray);
		this.props.closeModal();
	}

	descToggleOnChange() {
		this.setState({showdescription: !this.state.showdescription});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Project Item Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-3 form-group">
							<input type="text" className="form-control" name="location" value={this.state.search.itemid_name || ''} placeholder="Search By Item Name" onChange={(evt) =>{this.inputonChange(evt.target.value)}}/>
						</div>
						{this.props.sourcename == 'Projects' ? <div className="form-group col-md-4 col-sm-4">
							<div>Show Description</div>
							<label className="gs-checkbox-switch">
								<input className="form-check-input" type="checkbox" checked={this.state.showdescription} onChange={() => this.descToggleOnChange()} />
								<span className="gs-checkbox-switch-slider"></span>
							</label>
						</div> : null}
					</div>
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th></th>
										<th className="text-center">Item Name</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">UOM</th>
									</tr>
								</thead>
								<tbody>
									{this.props.sourcename == 'Projects' ? search(this.state.projectItemArray, this.state.search).map((item, index) => {
										return (
											<tr key={index}>
												<td><input type="checkbox" onChange={(e) => this.checkboxOnChange(item, e.target.checked)} checked={item.checked} /></td>
												<td>{item.itemid_name}<div className={`font-12 ${this.state.showdescription ? 'show' : 'hide'}`}>{item.description}</div></td>
												<td className="text-center">{item.quantity}</td>
												<td className="text-center">{item.uomid_name}</td>
											</tr>
										)
									}) : null}
									{this.props.sourcename == 'Orders' ? search(this.state.orderItemArray, this.state.search).map((item, index) => {
										return (
											<tr key={index}>
												<td><input type="checkbox" onChange={(e) => this.checkboxOnChange(item, e.target.checked)} checked={item.checked} /></td>
												<td>{item.itemid_name}</td>
												<td className="text-center">{item.quantity}</td>
												<td className="text-center">{item.uomid_name}</td>
											</tr>
										)
									}) : null}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addProjectItem}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
