import React, { Component } from 'react';
import axios from 'axios';
import { search,checkActionVerbAccess } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect,DateElement } from '../utilcomponents';
import { dateFilter } from '../../utils/filter';
import {Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { selectAsyncEle,NumberEle,DateEle } from '../formelements';
import { numberNewValidation,dateNewValidation } from '../../utils/utils';
import {currencyFilter} from '../../utils/filter';

import Loadingcontainer from '../loadingcontainer';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state={
			contractbillingschedules : JSON.parse(JSON.stringify(this.props.resource.contractbillingschedules))
		};
		this.updateBillingSchedules = this.updateBillingSchedules.bind(this);
		this.addBillingSchedule = this.addBillingSchedule.bind(this);
		this.deleteBillingSchedule = this.deleteBillingSchedule.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.valueOnChange = this.valueOnChange.bind(this);
	};

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	addBillingSchedule(){
		let contractbillingschedules = [...this.state.contractbillingschedules];
		contractbillingschedules.push({
			scheduledate : null,
			scheduleenddate : null,
			parentid : this.props.resource.id,
			parentresource : 'contracts'
		});
		this.setState({contractbillingschedules});
	}

	deleteBillingSchedule(item,index){
		let contractbillingschedules = [...this.state.contractbillingschedules];
		contractbillingschedules.splice(index, 1);
		this.setState({contractbillingschedules});
	}

	valueOnChange(field,item,value){
		item[field] = value;
		let contractbillingschedules = this.state.contractbillingschedules;
		this.setState({contractbillingschedules});
	}

	updateBillingSchedules() {
		this.updateLoaderFlag(true);
		this.props.callback(this.props.action,this.state.contractbillingschedules, null, (success) => {
			this.updateLoaderFlag(false);
			if(success)
				this.props.closeModal();
		});
	}

	render () {
		
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Update Billing Schedules</h5>
				</div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th className="text-center">Start</th>
										<th className="text-center">End</th>
										<th className="text-center">Status</th>
										<th className="text-center">Suspend Remarks</th>
										<th className="text-center"></th>
									</tr>
								</thead>
								<tbody>
									{this.state.contractbillingschedules.map((item, index) => {
										return (<tr key={index}>
											<td className="text-center" style={{width: '20%'}}>
												<DateElement className="form-control" value={item.scheduledate} required={true} disabled={item.invoiceid > 0 || item.issuspended} onChange={(val,itemstr) => {this.valueOnChange('scheduledate',item,val);}}/>
												
											</td>
											<td className="text-center"  style={{width: '20%'}}>
												<DateElement className="form-control" value={item.scheduleenddate} required={true} disabled={item.invoiceid > 0 || item.issuspended} onChange={(val,itemstr) => {this.valueOnChange('scheduleenddate',item,val);}}/>
											</td>
											<td className="text-center"  style={{width: '20%'}}>{(item.id > 0) ? ((item.invoiceid>0) ? 'Invoiced' : ((item.issuspended) ? 'Suspended' : 'Active')) : ''}</td>
											<td className="text-center" style={{width: '35%'}}>
											{
												(item.issuspended) ? item.suspendremarks : null
											}
											</td>
											<td className="text-center"  style={{width: '5%'}}>
												{
													(!item.invoiceid) ?
													<button type="button" className="btn gs-form-btn-danger btn-sm" onClick={()=>this.deleteBillingSchedule(item,index)}>
														<span className="fa fa-trash-o"></span>
													</button> : null
												} 
											</td>
										</tr>);
									})}
								</tbody>
							</table>
							<button type="button" className="btn btn-sm gs-btn-primary btn-width" onClick={this.addBillingSchedule}><i className="fa fa-plus"></i>Add</button>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}>Cancel</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.updateBillingSchedules} disabled={false}>Update</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}