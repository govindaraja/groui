import React, { Component } from 'react';

import { commonMethods, modalService } from '../../utils/services';
import { RichText } from '../formelements';

export default class RichTextModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			
		};
		this.richtextOnChange = this.richtextOnChange.bind(this);
	}

	componentWillMount() {
		this.state.richtextvalue = this.props.input.value;
	}

	richtextOnChange(value) {
		this.setState({
			richtextvalue : value
		}, () => {
			setTimeout(() => {
				this.props.input.onChange(value);
			}, 0);
		})
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Edit {this.props.title}</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12">
							<RichText value={this.state.richtextvalue} onChange={this.richtextOnChange} required={this.props.required} disabled={this.props.disabled} />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>

							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
