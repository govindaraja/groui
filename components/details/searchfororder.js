import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService } from '../../utils/services';
import { LocalSelect, AutoSelect } from '../utilcomponents';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search : {
				type : this.props.type,
				searchby : 'invoice'
			},
			resultArray : []
		};
		this.inputonChange = this.inputonChange.bind(this);
		this.salesInvoiceOnChange = this.salesInvoiceOnChange.bind(this);
		this.purchaseInvoiceOnChange = this.purchaseInvoiceOnChange.bind(this);
		this.deliveryOnChange = this.deliveryOnChange.bind(this);
		this.receiptOnChange = this.receiptOnChange.bind(this);
		this.searchSales = this.searchSales.bind(this);
		this.searchPurchase = this.searchPurchase.bind(this);
		this.chooseOrder = this.chooseOrder.bind(this);
	}

	inputonChange(item) {
		this.state.resultArray = [];
		this.state.search.searchby = item;

		this.setState({
			search: this.state.search
		});
	}

	salesInvoiceOnChange(id, valueobj) {
		this.state.search.salesinvoiceid = id;
		this.state.resultArray = [];

		this.setState({
			search: this.state.search
		});
	}

	purchaseInvoiceOnChange(id, valueobj) {
		this.state.resultArray = [];
		this.state.search.purchaseinvoiceid = id;

		this.setState({
			search: this.state.search
		});
	}

	deliveryOnChange(id, valueobj) {
		this.state.resultArray = [];
		this.state.search.deliveryid = id;

		this.setState({
			search: this.state.search
		});
	}

	receiptOnChange(id, valueobj) {
		this.state.resultArray = [];
		this.state.search.receiptid = id;

		this.setState({
			search: this.state.search
		});
	}

	searchSales () {
		let resultArray = [];

		axios.get(`/api/${this.state.search.searchby == 'invoice' ? 'salesinvoiceitems' : 'deliverynoteitems'}?field=id,itemid,itemmaster/name/itemid,quantity,uomid,uom/name/uomid,orderitemsid,orderitems/parentid/orderitemsid&skip=0&filtercondition=${this.state.search.searchby == 'invoice' ? 'salesinvoiceitems' : 'deliverynoteitems'}.parentid=${this.state.search.searchby == 'invoice' ? this.state.search.salesinvoiceid : this.state.search.deliveryid}`).then((response) => {
			if (response.data.message == 'success') {
				let tempOrderID = [];
				for (let i = 0; i < response.data.main.length; i++) {
					if (response.data.main[i].orderitemsid_parentid > 0) {
						if (tempOrderID.indexOf(response.data.main[i].orderitemsid_parentid) == -1) {
							tempOrderID.push(response.data.main[i].orderitemsid_parentid)
							resultArray.push({
								orderid: response.data.main[i].orderitemsid_parentid,
								items: [JSON.parse(JSON.stringify(response.data.main[i]))]
							});
						} else {
							resultArray[tempOrderID.indexOf(response.data.main[i].orderitemsid_parentid)].items.push(JSON.parse(JSON.stringify(response.data.main[i])));
						}
					}
				}
				if(tempOrderID.length > 0)
					this.getOrderDetails(tempOrderID, resultArray);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getOrderDetails(tempOrderID, resultArray) {
		axios.get(`/api/orders?field=id,orderno,partners/name/customerid,customerid,companyid,companymaster/name/companyid,currencyid,currencyexchangerate,defaultcostcenter,warehouseid,pricelistid&filtercondition=orders.id in (${tempOrderID.join()})`).then((secresponse) => {
			if (secresponse.data.message == 'success') {
				for (let k = 0; k < secresponse.data.main.length; k++) {
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].id = secresponse.data.main[k].id;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].orderno = secresponse.data.main[k].orderno;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].customerid_name = secresponse.data.main[k].customerid_name;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].customerid = secresponse.data.main[k].customerid;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].companyid = secresponse.data.main[k].companyid;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].companyid_name = secresponse.data.main[k].companyid_name;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].currencyid = secresponse.data.main[k].currencyid;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].currencyexchangerate = secresponse.data.main[k].currencyexchangerate;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].defaultcostcenter = secresponse.data.main[k].defaultcostcenter;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].warehouseid = secresponse.data.main[k].warehouseid;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].pricelistid = secresponse.data.main[k].pricelistid;
				}

				this.setState({resultArray});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	searchPurchase () {
		let resultArray = [];

		axios.get(`/api/${this.state.search.searchby == 'invoice' ? 'purchaseinvoiceitems' : 'receiptnoteitems'}?field=id,itemid,itemmaster/name/itemid,quantity,uomid,uom/name/uomid,purchaseorderitemsid,purchaseorderitems/parentid/purchaseorderitemsid&skip=0&filtercondition=${this.state.search.searchby == 'invoice' ? 'purchaseinvoiceitems' : 'receiptnoteitems'}.parentid=${this.state.search.searchby == 'invoice' ? this.state.search.purchaseinvoiceid : this.state.search.receiptid}`).then((response) => {
			if (response.data.message == 'success') {
				let tempOrderID = [];
				for (let i = 0; i < response.data.main.length; i++) {
					if (response.data.main[i].purchaseorderitemsid_parentid > 0) {
						if (tempOrderID.indexOf(response.data.main[i].purchaseorderitemsid_parentid) == -1) {
							tempOrderID.push(response.data.main[i].purchaseorderitemsid_parentid)
							resultArray.push({
								orderid: response.data.main[i].purchaseorderitemsid_parentid,
								items: [JSON.parse(JSON.stringify(response.data.main[i]))]
							});
						} else
							resultArray[tempOrderID.indexOf(response.data.main[i].purchaseorderitemsid_parentid)].items.push(JSON.parse(JSON.stringify(response.data.main[i])));
					}
				}
				if(tempOrderID.length > 0)
					this.getPurchaseOrderDetails(tempOrderID, resultArray);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getPurchaseOrderDetails(tempOrderID, resultArray) {
		axios.get(`/api/purchaseorders?field=id,ponumber,partners/name/partnerid,partnerid,companyid,companymaster/name/companyid,currencyid,currencyexchangerate,defaultcostcenter,warehouseid,pricelistid&filtercondition=purchaseorders.id in (${tempOrderID.join()})`).then((secresponse) => {
			if (secresponse.data.message == 'success') {
				for (let k = 0; k < secresponse.data.main.length; k++) {
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].id = secresponse.data.main[k].id;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].ponumber = secresponse.data.main[k].ponumber;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].partnerid_name = secresponse.data.main[k].partnerid_name;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].partnerid = secresponse.data.main[k].partnerid;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].companyid = secresponse.data.main[k].companyid;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].companyid_name = secresponse.data.main[k].companyid_name;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].currencyid = secresponse.data.main[k].currencyid;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].currencyexchangerate = secresponse.data.main[k].currencyexchangerate;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].defaultcostcenter = secresponse.data.main[k].defaultcostcenter;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].warehouseid = secresponse.data.main[k].warehouseid;
					resultArray[tempOrderID.indexOf(secresponse.data.main[k].id)].pricelistid = secresponse.data.main[k].pricelistid;
				}

				this.setState({resultArray});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	chooseOrder(item) {
		this.props.closeModal();
		this.props.callback(item);
	}

	render() {
		let searchByErrorClass = this.state.search.searchby ? '' : 'errorinput';
		let salesinvoiceidErrorClass = this.state.search.salesinvoiceid > 0 ? '' : 'errorinput';
		let deliveryidErrorClass = this.state.search.deliveryid > 0 ? '' : 'errorinput';
		let purchaseinvoiceidErrorClass = this.state.search.purchaseinvoiceid > 0 ? '' : 'errorinput';
		let receiptidErrorClass = this.state.search.receiptid > 0 ? '' : 'errorinput';
		let salesdisabled = (searchByErrorClass || (this.state.search.searchby == 'invoice' && salesinvoiceidErrorClass) || (this.state.search.searchby == 'delivery' && deliveryidErrorClass)) ? true : false;
		let purchasedisabled = (searchByErrorClass || (this.state.search.searchby == 'invoice' && purchaseinvoiceidErrorClass) || (this.state.search.searchby == 'receipt' && receiptidErrorClass)) ? true : false;
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Search For {this.state.search.type} Order</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						{ this.state.search.type == "Sales" ? 
							<div className="row col-md-12 col-sm-12 col-xs-12">
								<div className="form-group col-md-3 col-sm-6">
									<label className="labelclass">Search By</label>
									<LocalSelect options={ [{value:'invoice', displayname:'By Invoice No'}, {value:'delivery', displayname:'By Delivery Note No'}] } label="displayname" valuename="value" value={this.state.search.searchby} onChange={(value) => this.inputonChange(value)} className={searchByErrorClass} required/>
								</div>
								{ this.state.search.searchby == 'invoice' ? <div className="form-group col-md-3 col-sm-6">
									<label className="labelclass">Invoice No</label>
									<AutoSelect resource={'salesinvoices'} fields={'id,invoiceno'} value={this.state.search.salesinvoiceid} label={'invoiceno'} valuename={'id'} filter={`salesinvoices.status in ('Approved','Sent To Customer')`} onChange={this.salesInvoiceOnChange} className={salesinvoiceidErrorClass} required/>
								</div> : null
								}
								{ this.state.search.searchby == 'delivery' ? <div className="form-group col-md-3 col-sm-6">
									<label className="labelclass">Delivery Note No</label>
									<AutoSelect resource={'deliverynotes'} fields={'id,deliverynotenumber'} value={this.state.search.deliveryid} label={'deliverynotenumber'} valuename={'id'} filter={`deliverynotes.status in ('Approved','Dispatched')`} onChange={this.deliveryOnChange} className={deliveryidErrorClass} required/>
								</div> : null
								}
								<div className="form-group col-md-3 col-sm-6">
									<button type="button" className="btn btn-sm gs-btn-info btn-width" onClick={this.searchSales} disabled={salesdisabled} ><i className="fa fa-search"></i>Search</button>
								</div>
							</div> : <div className="row col-md-12 col-sm-12 col-xs-12">
								<div className="form-group col-md-3 col-sm-6">
									<label className="labelclass">Search By</label>
									<LocalSelect options={ [{value:'invoice', displayname:'By Invoice No'}, {value:'receipt', displayname:'By Receipt Note No'}] } label="displayname" valuename="value" value={this.state.search.searchby} onChange={(value) => this.inputonChange(value)} className={searchByErrorClass} required/>
								</div>
								{ this.state.search.searchby == 'invoice' ? <div className="form-group col-md-3 col-sm-6">
									<label className="labelclass">Invoice No</label>
									<AutoSelect resource={'purchaseinvoices'} fields={'id,invoiceno'} value={this.state.search.purchaseinvoiceid} label={'invoiceno'} valuename={'id'} filter={`purchaseinvoices.status in ('Approved')`} onChange={this.purchaseInvoiceOnChange} className={purchaseinvoiceidErrorClass} required/>
								</div> : null
								}
								{ this.state.search.searchby == 'receipt' ? <div className="form-group col-md-3 col-sm-6">
									<label className="labelclass">Receipt Note No</label>
									<AutoSelect resource={'receiptnotes'} fields={'id,receiptnotenumber'} value={this.state.search.receiptid} label={'receiptnotenumber'} valuename={'id'} filter={`receiptnotes.status in ('Approved','Received')`} onChange={this.receiptOnChange} className={receiptidErrorClass} required/>
								</div> : null
								}
								<div className="form-group col-md-3 col-sm-6">
									<button type="button" className="btn btn-sm gs-btn-info btn-width" onClick={this.searchPurchase} disabled={purchasedisabled}><i className="fa fa-search"></i>Search</button>
								</div>
							</div>
						}
					</div>
					<div className="row">
						<div className="col-md-12">
							{ this.state.resultArray.length == 0 ? <div className="alert alert-warning text-center">No Items Found</div> : <div className="col-md-12">{ this.state.resultArray.map((item, index)=> {
									return (
										<div className="col-md-12" key={index}>
											<div className="col-md-12">
												{this.state.search.type == 'Sales' ? <h5>Order No : <a style={{color: 'blue',cursor:'pointer'}} onClick={()=>{this.chooseOrder(item)}}>{item.orderno}</a>, Customer : {item.customerid_name}</h5>: null}
												{this.state.search.type == 'Purchase' ? <h5>PO No : <a style={{color: 'blue',cursor:'pointer'}} onClick={()=>{this.chooseOrder(item)}}>{item.ponumber}</a>, Supplier : {item.partnerid_name}</h5>: null}
											</div>
											<table className="table table-bordered">
												<thead>
													<tr>
														<th className='text-center'>Item Name</th>
														<th className='text-center'>UOM</th>
														<th className='text-center'>Quantity</th>
													</tr>
												</thead>
												<tbody>
													{item.items.map((secitem, seindex)=> {
														return (
															<tr key={seindex}>
																<td>{secitem.itemid_name}</td>
																<td className="text-center">{secitem.uomid_name}</td>
																<td className="text-center">{secitem.quantity}</td>
															</tr>
														);
													})}
												</tbody>
											</table>
										</div>
									);
								})}
								</div>
							}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
