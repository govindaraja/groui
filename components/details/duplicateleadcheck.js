import React, { Component } from 'react';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);

		this.openLead = this.openLead.bind(this);
		this.save = this.save.bind(this);
	}

	openLead (id) {
		//this.props.closeModal();
		let win = window.open(`/spa#/details/leads/${id}`, '_blank');
		win.focus();
	}

	save() {
		this.props.callback();
		this.props.closeModal();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Duplicate Lead Alert</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12">
							<p>Active lead(s) matching with the given mobile / email found. </p>
						</div>
						<div className="col-md-12 col-sm-12 col-xs-12">
							<table className="table table-bordered table-condensed table-hover table-sm">
								<thead>
									<tr>
										<th className="text-center">Lead No</th>
										<th className="text-center">Lead Date</th>
										<th className="text-center">Sales Person</th>
										<th className="text-center">Status</th>
									</tr>
								</thead>
								<tbody>
									{this.props.array.map((item, index) => {
										return (<tr key={index} onClick={()=>{this.openLead(item.id)}}>
											<td className="text-center">{item.leadno}</td>
											<td className="text-center">{dateFilter(item.created)}</td>
											<td>{item.salesperson_displayname}</td>
											<td className="text-center">{item.status}</td>
										</tr>);
									})}
								</tbody>
							</table>
							<p>Please ensure that you are not creating a duplicate lead. You can click on a Lead to view details.</p>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.save}><i className="fa fa-check-square-o"></i>Ok!</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
