import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { currencyFilter, dateFilter, taxFilter } from '../../utils/filter';


export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			quoteorderArray: [],
			quoteorderitemsArray: [],
			quoteitemsArray: [],
			orderitemsArray: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.renderOrderitem = this.renderOrderitem.bind(this);
		this.renderQuoteitem = this.renderQuoteitem.bind(this);
		this.renderQuoteOrderitem = this.renderQuoteOrderitem.bind(this);
		this.close = this.close.bind(this);
	}

	componentWillMount() {
		if(this.props.resource.quoteno) {
			this.onLoad();
		}
	}

	onLoad() {
		let quoteorderArray = [], quoteorderitemsArray = [], quoteitemsArray = [], orderitemsArray = [];
		let addressdiff = false, freightdiff = false, modeofdespatchdiff = false, creditdiff = false;
		let quoteobj = {};

		axios.get(`/api/quotes/${this.props.resource.quoteno}`).then((response) => {
			if (response.data.message == 'success') {
				quoteobj = response.data.main;

				if (quoteobj.deliveryaddress == this.props.resource.deliveryaddress) {
					addressdiff = true;
				}
				if (quoteobj.freight == this.props.resource.freight) {
					freightdiff = true;
				}
				if (quoteobj.modeofdespatch == this.props.resource.modeofdespatch) {
					modeofdespatchdiff = true;
				}
				if (quoteobj.creditperiod == this.props.resource.creditperiod) {
					creditdiff = true;
				}

				quoteorderArray.push({
					priority : 1,
					name : "Delivery Address",
					diff : addressdiff,
					order : this.props.resource.deliveryaddress,
					quote : quoteobj.deliveryaddress
				}, {
					priority : 2,
					name : "Freight",
					diff : freightdiff,
					order : this.props.resource.freight,
					quote : quoteobj.freight
				}, {
					priority : 3,
					name : "Mode Of Dispatch",
					diff : modeofdespatchdiff,
					order : this.props.resource.modeofdespatch,
					quote : quoteobj.modeofdespatch
				}, {
					priority : 4,
					name : "Credit Period",
					diff : creditdiff,
					order : this.props.resource.creditperiod,
					quote : quoteobj.creditperiod
				});

				for (var i = 0; i < quoteobj.quoteitems.length; i++) {
					for (var j = 0; j < this.props.resource.orderitems.length; j++) {
						if (quoteobj.quoteitems[i].id == this.props.resource.orderitems[j].quoteitemid) {
							let itemdiff = false;
							if (quoteobj.quoteitems[i].description == this.props.resource.orderitems[j].description && quoteobj.quoteitems[i].rate == this.props.resource.orderitems[j].rate && quoteobj.quoteitems[i].quantity == this.props.resource.orderitems[j].quantity && quoteobj.quoteitems[i].uomid_name == this.props.resource.orderitems[j].uomid_name && quoteobj.quoteitems[i].amount == this.props.resource.orderitems[j].amount && new Date(quoteobj.quoteitems[i].deliverydate).toDateString() == new Date(this.props.resource.orderitems[j].deliverydate).toDateString() && quoteobj.quoteitems[i].discountquantity == this.props.resource.orderitems[j].discountquantity && quoteobj.quoteitems[i].discountmode == this.props.resource.orderitems[j].discountmode && quoteobj.currencyid == this.props.resource.currencyid) {
								if (arraysEqual(quoteobj.quoteitems[i].taxid, this.props.resource.orderitems[j].taxid)) {
									itemdiff = false;
								} else {
									itemdiff = true;
								}
							} else {
								itemdiff = true;
							}

							quoteorderitemsArray.push({
								itemid_name : quoteobj.quoteitems[i].itemid_name,
								diff : itemdiff,
								quote : {
									name : "Quote",
									description : quoteobj.quoteitems[i].description,
									rate : quoteobj.quoteitems[i].rate,
									quantity : quoteobj.quoteitems[i].quantity,
									uomid_name : quoteobj.quoteitems[i].uomid_name,
									discountquantity : quoteobj.quoteitems[i].discountquantity,
									discountmode : quoteobj.quoteitems[i].discountmode,
									taxid : quoteobj.quoteitems[i].taxid,
									amount : quoteobj.quoteitems[i].amount,
									deliverydate : quoteobj.quoteitems[i].deliverydate,
									currencyid : quoteobj.currencyid
								},
								order : {
									name : "Order",
									description : this.props.resource.orderitems[j].description,
									rate : this.props.resource.orderitems[j].rate,
									quantity : this.props.resource.orderitems[j].quantity,
									uomid_name : this.props.resource.orderitems[j].uomid_name,
									discountquantity : this.props.resource.orderitems[j].discountquantity,
									discountmode : this.props.resource.orderitems[j].discountmode,
									taxid : this.props.resource.orderitems[j].taxid,
									amount : this.props.resource.orderitems[j].amount,
									deliverydate : this.props.resource.orderitems[j].deliverydate,
									currencyid : this.props.resource.currencyid
								}
							});
						}
					}
				}

				function arraysEqual(arr1, arr2) {
					if (arr1.length !== arr2.length)
						return false;
					for (var i = arr1.length; i--; ) {
						if (arr1[i] !== arr2[i])
							return false;
					}
					return true;
				}

				let quoteitemidArray = quoteobj.quoteitems.map((x) => {
					return x.itemid;
				});
				let orderitemidArray = this.props.resource.orderitems.map((x) => {
					return x.itemid;
				});

				this.props.resource.orderitems.forEach((item) => {
					if (quoteitemidArray.indexOf(item.itemid) == -1) {
						orderitemsArray.push({
							itemid_name : item.itemid_name,
							order : {
								name : "Order",
								description : item.description,
								rate : item.rate,
								quantity : item.quantity,
								uomid_name : item.uomid_name,
								discountquantity : item.discountquantity,
								discountmode : item.discountmode,
								taxid : item.taxid,
								amount : item.amount,
								deliverydate : item.deliverydate,
								currencyid : this.props.resource.currencyid
							}
						});
					}
				});

				quoteobj.quoteitems.forEach((item) => {
					if (orderitemidArray.indexOf(item.itemid) == -1) {
						quoteitemsArray.push({
							itemid_name : item.itemid_name,
							quote : {
								name : "Quote",
								description : item.description,
								rate : item.rate,
								quantity : item.quantity,
								uomid_name : item.uomid_name,
								discountquantity : item.discountquantity,
								discountmode : item.discountmode,
								taxid : item.taxid,
								amount : item.amount,
								deliverydate : item.deliverydate,
								currencyid : quoteobj.currencyid
							}
						});
					}
				});

				this.setState({quoteorderArray, quoteorderitemsArray, orderitemsArray, quoteitemsArray});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	renderQuoteOrderitem() {
		return this.state.quoteorderitemsArray.map((item, index)=> {
			return (
				<tbody key={index}>
					<tr>
						<td rowSpan="2">{item.itemid_name} {item.diff ? <span style={{color:'red'}}><i className="fa fa-exclamation-circle"></i></span> : null }</td>
						<td>{item.quote.name}</td>
						<td>{item.quote.description}</td>
						<td>{item.quote.quantity} {item.quote.uomid_name}</td>
						<td>{currencyFilter(item.quote.rate, item.quote.currencyid, this.props.app)}</td>
						<td>
							{item.quote.discountmode != "Percentage" && item.quote.discountquantity ? <span>{currencyFilter(item.quote.discountquantity, item.quote.currencyid, this.props.app)}</span> : null }
							{item.quote.discountmode == "Percentage" && item.quote.discountquantity ? <span>{item.quote.discountquantity} %</span> : null }
						</td>
						<td>{taxFilter(item.quote.taxid, this.props.app.taxObject)}</td>
						<td>{currencyFilter(item.quote.amount, item.quote.currencyid, this.props.app)}</td>
						<td>{dateFilter(item.quote.deliverydate)}</td>
					</tr>
					<tr>
						<td>{item.order.name}</td>
						<td>{item.order.description}</td>
						<td>{item.order.quantity} {item.order.uomid_name}</td>
						<td>{currencyFilter(item.order.rate, item.order.currencyid, this.props.app)}</td>
						<td>
							{item.order.discountmode != "Percentage" && item.order.discountquantity ? <span>{currencyFilter(item.order.discountquantity, item.order.currencyid, this.props.app)}</span> : null }
							{item.order.discountmode == "Percentage" && item.order.discountquantity ? <span>{item.order.discountquantity} %</span> : null }
						</td>
						<td>{taxFilter(item.order.taxid, this.props.app.taxObject)}</td>
						<td>{currencyFilter(item.order.amount, item.order.currencyid, this.props.app)}</td>
						<td>{dateFilter(item.order.deliverydate)}</td>
					</tr>
				</tbody>
			);
		});
	}

	renderOrderitem() {
		this.state.orderitemsArray.map((item, index)=> {
			return (
				<tbody key={index}>
					<tr>
						<td>{item.itemid_name}</td>
						<td>{item.order.name}</td>
						<td>{item.order.description}</td>
						<td>{item.order.quantity} {item.order.uomid_name}</td>
						<td>{currencyFilter(item.order.rate, item.order.currencyid, this.props.app)}</td>
						<td>
							{item.order.discountmode != "Percentage" && item.order.discountquantity ? <span>{currencyFilter(item.order.discountquantity, item.order.currencyid, this.props.app)}</span> : null }
							{item.order.discountmode == "Percentage" && item.order.discountquantity ? <span>{item.order.discountquantity} %</span> : null }
						</td>
						<td>{taxFilter(item.order.taxid, this.props.app.taxObject)}</td>
						<td>{currencyFilter(item.order.amount, item.order.currencyid, this.props.app)}</td>
						<td>{dateFilter(item.order.deliverydate)}</td>
					</tr>
				</tbody>
			);
		});
	}

	renderQuoteitem() {
		this.state.quoteitemsArray.map((item, index)=> {
			return (
				<tbody key={index}>
					<tr>
						<td>{item.itemid_name}</td>
						<td>{item.quote.name}</td>
						<td>{item.quote.description}</td>
						<td>{item.quote.quantity} {item.quote.uomid_name}</td>
						<td>{currencyFilter(item.quote.rate, item.quote.currencyid, this.props.app)}</td>
						<td>
							{item.quote.discountmode != "Percentage" && item.quote.discountquantity ? <span>{currencyFilter(item.quote.discountquantity, item.quote.currencyid, this.props.app)}</span> : null }
							{item.quote.discountmode == "Percentage" && item.quote.discountquantity ? <span>{item.quote.discountquantity} %</span> : null }
						</td>
						<td>{taxFilter(item.quote.taxid, this.props.app.taxObject)}</td>
						<td>{currencyFilter(item.quote.amount, item.quote.currencyid, this.props.app)}</td>
						<td>{dateFilter(item.quote.deliverydate)}</td>
					</tr>
				</tbody>
			);
		});
	}

	close() {
		this.props.closeModal();
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Compare Quote</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12">
							<div className="card">
								<div className="card-header bg-info">Quote and Order Details</div>
								<div className="card-body">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												<th className="text-center"></th>
												<th className="text-center">Quotation</th>
												<th className="text-center">Order</th>
											</tr>
										</thead>
										<tbody>
											{this.state.quoteorderArray.map((item, index)=> {
												return (
													<tr key={index}>
														<td>{item.name} 
														{!item.diff ? <span style={{color:'red'}}><i className="fa fa-exclamation-circle"></i></span> : null }
														</td>
														<td>{item.quote}</td>
														<td>{item.order}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<div className="card">
								<div className="card-header bg-info">Quote and Order Item Details</div>
								<div className="card-body">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												<th className="text-center">Item Name</th>
												<th className="text-center"></th>
												<th className="text-center">Description</th>
												<th className="text-center">Quantity</th>
												<th className="text-center">Rate</th>
												<th className="text-center">Discount</th>
												<th className="text-center">Taxes</th>
												<th className="text-center">Total</th>
												<th className="text-center">Delivery Date</th>
											</tr>
										</thead>
										{this.renderQuoteOrderitem()}

										{this.renderOrderitem()}

										{this.renderQuoteitem()}

									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
