import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect, SelectAsync, NumberElement, LocalSelect, DateElement } from '../utilcomponents';
import { dateFilter, currencyFilter } from '../../utils/filter';
import Loadingcontainer from '../loadingcontainer';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			btnclicked: false,
			loaderflag: false,
			voucher: {
				adjustfor: 'Partner',
				companyid: this.props.companyid,
				partnerid: this.props.partnerid,
				partnerid_name: this.props.partnerid_name,
				partnerid: this.props.employeeid,
				partnerid_name: this.props.employeeid_displayname,
				currencyid: this.props.currencyid,
			},
			advanceArray: [],
			invoiceArray: [],
			voucherTypes: {
				'Sales Invoice' : 'salesinvoices',
				'Purchase Invoice' : 'purchaseinvoices',
				'Journal Voucher' : 'journalvouchers',
				'Receipt' : 'receiptvouchers',
				'Payment' : 'paymentvouchers',
				'Credit Note' : 'creditnote',
				'Debit Note' : 'debitnote',
				'Expense Request' : 'expenserequests',
				'Pay Roll' : 'payrolls'
			}
		};
		this.getData = this.getData.bind(this);
		this.setData = this.setData.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.updateamount = this.updateamount.bind(this);
		this.deleteItem = this.deleteItem.bind(this);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	getData() {
		this.updateLoaderFlag(true);
		let advanceArray = [], invoiceArray = [];
		axios.get(`/api/query/advanceinvoicequery?companyid=${this.state.voucher.companyid}&partnerid=${this.state.voucher.partnerid}&employeeid=${this.state.voucher.employeeid}&transactiontype=${this.state.voucher.type}&currencyid=${this.state.voucher.currencyid}`).then((response) => {
			if(response.data.message == 'success') {
				//advanceArray = response.data.main.advanceArray;
				//invoiceArray = response.data.main.invoiceArray;
				response.data.main.invoiceArray.forEach((item) => {
					let voucherDate = item.xsalesinvoiceid_invoicedate || item.xpurchaseinvoiceid_invoicedate || item.xjournalvoucherid_voucherdate || item.xexpenserequestid_expensedate || item.xpayrollid_payrolldate;
					let uptodate = this.state.voucher.creditupto;
					if(!uptodate || (uptodate && new Date(uptodate).setHours(0,0,0,0) >= new Date(voucherDate).setHours(0,0,0,0)))
						invoiceArray.push(item);
				});
				response.data.main.advanceArray.forEach((item) => {
					let voucherDate = item.xsalesinvoiceid_invoicedate || item.xpurchaseinvoiceid_invoicedate || item.xjournalvoucherid_voucherdate || item.xexpenserequestid_expensedate || item.xpayrollid_payrolldate;
					let uptodate = this.state.voucher.debitupto;
					if(!uptodate || (uptodate && new Date(uptodate).setHours(0,0,0,0) >= new Date(voucherDate).setHours(0,0,0,0)))
						advanceArray.push(item);
				});

				let advanceAmount = 0, invoiceAmount = 0;
				if(advanceArray.length > 0 && invoiceArray.length > 0) {
					for(var i = 0; i < advanceArray.length; i++)
						advanceAmount += advanceArray[i].balance;
					for(var j = 0; j < invoiceArray.length; j++)
						invoiceAmount += invoiceArray[j].balance;
					advanceAmount = Number(advanceAmount.toFixed(this.props.app.roundOffPrecision));
					invoiceAmount = Number(invoiceAmount.toFixed(this.props.app.roundOffPrecision));
					if(advanceAmount == invoiceAmount) {
						for(var k = 0; k < advanceArray.length; k++)
							advanceArray[k].amounttoall = advanceArray[k].balance;
						for(var l = 0; l < invoiceArray.length; l++)
							invoiceArray[l].amounttoall = invoiceArray[l].balance;
					} else if(advanceAmount > invoiceAmount) {
						for(var m = 0; m < invoiceArray.length; m++)
							invoiceArray[m].amounttoall = invoiceArray[m].balance;
						
						for(var n = 0; n < advanceArray.length; n++) {
							if(invoiceAmount > 0) {
								if(advanceArray[n].balance >= invoiceAmount) {
									advanceArray[n].amounttoall = invoiceAmount;
									invoiceAmount = 0;
								} else {
									advanceArray[n].amounttoall = advanceArray[n].balance;
									invoiceAmount -= advanceArray[n].balance;
									invoiceAmount = Number(invoiceAmount.toFixed(this.props.app.roundOffPrecision));
								}
							}
						}
					} else {
						for(var p = 0; p < advanceArray.length; p++)
							advanceArray[p].amounttoall = advanceArray[p].balance;
						for(var q = 0; q < invoiceArray.length; q++) {
							if(advanceAmount >0) {
								if(invoiceArray[q].balance >= advanceAmount) {
									invoiceArray[q].amounttoall = advanceAmount;
									advanceAmount = 0;
								} else {
									invoiceArray[q].amounttoall = invoiceArray[q].balance;
									advanceAmount -= invoiceArray[q].balance;
									advanceAmount = Number(advanceAmount.toFixed(this.props.app.feature.roundOffPrecision));
								}
							}
						}
					}
				}
				this.setState({
					invoiceArray,
					advanceArray,
					btnclicked: true
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
	
	setData () {
		let array = [];
		let { advanceArray, invoiceArray } = this.state;
		for(var j = 0; j < advanceArray.length; j++) {
			array.push({
				accountid : advanceArray[j].accountid,
				debit : advanceArray[j].amounttoall,
				againstreference : this.state.voucherTypes[advanceArray[j].vouchertype],
				partnerid : advanceArray[j].partnerid,
				partnerid_name : this.state.voucher.partnerid_name,
				employeeid : advanceArray[j].employeeid,
				employeeid_displayname : this.state.voucher.employeeid_displayname,
				xsalesinvoiceid : advanceArray[j].xsalesinvoiceid ? advanceArray[j].xsalesinvoiceid : null,
				xsalesinvoiceid_invoiceno : advanceArray[j].xsalesinvoiceid ? advanceArray[j].xsalesinvoiceid_invoiceno : null,
				xsalesinvoiceid_invoicedate : advanceArray[j].xsalesinvoiceid ? advanceArray[j].xsalesinvoiceid_invoicedate : null,
				xpurchaseinvoiceid : advanceArray[j].xpurchaseinvoiceid ? advanceArray[j].xpurchaseinvoiceid : null,
				xpurchaseinvoiceid_invoiceno : advanceArray[j].xpurchaseinvoiceid ? advanceArray[j].xpurchaseinvoiceid_invoiceno : null,
				xpurchaseinvoiceid_invoicedate : advanceArray[j].xpurchaseinvoiceid ? advanceArray[j].xpurchaseinvoiceid_invoicedate : null,
				xjournalvoucherid : advanceArray[j].xjournalvoucherid ? advanceArray[j].xjournalvoucherid : null,
				xjournalvoucherid_voucherno : advanceArray[j].xjournalvoucherid ? advanceArray[j].xjournalvoucherid_voucherno : null,
				xjournalvoucherid_voucherdate : advanceArray[j].xjournalvoucherid ? advanceArray[j].xjournalvoucherid_voucherdate : null,
				xexpenserequestid : advanceArray[j].xexpenserequestid ? advanceArray[j].xexpenserequestid : null,
				xexpenserequestid_expenserequestno : advanceArray[j].xexpenserequestid ? advanceArray[j].xexpenserequestid_expenserequestno : null,
				xexpenserequestid_expensedate : advanceArray[j].xexpenserequestid ? advanceArray[j].xexpenserequestid_expensedate : null,
				xpayrollid : advanceArray[j].xpayrollid ? advanceArray[j].xpayrollid : null,
				xpayrollid_payrollno : advanceArray[j].xpayrollid ? advanceArray[j].xpayrollid_payrollno : null,
				xpayrollid_payrolldate : advanceArray[j].xpayrollid ? advanceArray[j].xpayrollid_payrolldate : null,
				accountid_name : advanceArray[j].accountid_name,
				accountid_type : advanceArray[j].accountid_type,
				accountid_accountgroup : advanceArray[j].accountid_accountgroup,
				accountid_createfollowup : advanceArray[j].accountid_createfollowup
			});
		}

		for(var j = 0; j < invoiceArray.length; j++) {
			array.push({
				accountid : invoiceArray[j].accountid,
				credit : invoiceArray[j].amounttoall,
				againstreference : this.state.voucherTypes[invoiceArray[j].vouchertype],
				partnerid : invoiceArray[j].partnerid,
				partnerid_name : this.state.voucher.partnerid_name,
				employeeid : invoiceArray[j].employeeid,
				employeeid_displayname : this.state.voucher.employeeid_displayname,
				xsalesinvoiceid : invoiceArray[j].xsalesinvoiceid ? invoiceArray[j].xsalesinvoiceid : null,
				xsalesinvoiceid_invoiceno : invoiceArray[j].xsalesinvoiceid ? invoiceArray[j].xsalesinvoiceid_invoiceno : null,
				xsalesinvoiceid_invoicedate : invoiceArray[j].xsalesinvoiceid ? invoiceArray[j].xsalesinvoiceid_invoicedate : null,
				xpurchaseinvoiceid : invoiceArray[j].xpurchaseinvoiceid ? invoiceArray[j].xpurchaseinvoiceid : null,
				xpurchaseinvoiceid_invoiceno : invoiceArray[j].xpurchaseinvoiceid ? invoiceArray[j].xpurchaseinvoiceid_invoiceno : null,
				xpurchaseinvoiceid_invoicedate : invoiceArray[j].xpurchaseinvoiceid ? invoiceArray[j].xpurchaseinvoiceid_invoicedate : null,
				xjournalvoucherid : invoiceArray[j].xjournalvoucherid ? invoiceArray[j].xjournalvoucherid : null,
				xjournalvoucherid_voucherno : invoiceArray[j].xjournalvoucherid ? invoiceArray[j].xjournalvoucherid_voucherno : null,
				xjournalvoucherid_voucherdate : invoiceArray[j].xjournalvoucherid ? invoiceArray[j].xjournalvoucherid_voucherdate : null,
				xexpenserequestid : invoiceArray[j].xexpenserequestid ? invoiceArray[j].xexpenserequestid : null,
				xexpenserequestid_expenserequestno : invoiceArray[j].xexpenserequestid ? invoiceArray[j].xexpenserequestid_expenserequestno : null,
				xexpenserequestid_expensedate : invoiceArray[j].xexpenserequestid ? invoiceArray[j].xexpenserequestid_expensedate : null,
				xpayrollid : invoiceArray[j].xpayrollid ? invoiceArray[j].xpayrollid : null,
				xpayrollid_payrollno : invoiceArray[j].xpayrollid ? invoiceArray[j].xpayrollid_payrollno : null,
				xpayrollid_payrolldate : invoiceArray[j].xpayrollid ? invoiceArray[j].xpayrollid_payrolldate : null,
				accountid_name : invoiceArray[j].accountid_name,
				accountid_type : invoiceArray[j].accountid_type,
				accountid_accountgroup : invoiceArray[j].accountid_accountgroup,
				accountid_createfollowup : invoiceArray[j].accountid_createfollowup
			});
		}
		this.props.callback(this.state.voucher.companyid, this.state.voucher.partnerid, array);
		this.props.closeModal();
	}

	inputonChange(value, valueobj, param) {
		let voucher = {
			...this.state.voucher
		};
		if(param == 'partnerid') {
			voucher.partnerid = value;
			voucher.partnerid_name = valueobj.name;
		}
		if(param == 'employeeid') {
			voucher.employeeid = value;
			voucher.employeeid_displayname = valueobj.displayname;
		}
		if(param == 'adjustfor') {
			voucher.adjustfor = value;
			voucher.partnerid = null;
			voucher.partnerid_name = null;
			voucher.employeeid = null;
			voucher.employeeid_displayname = null;
		}
		if(param == 'creditupto') {
			voucher.creditupto = value;
		}
		if(param == 'debitupto') {
			voucher.debitupto = value;
		}
		this.setState({
			voucher
		});
	}

	updateamount(value, item) {
		item.amounttoall = value;
		this.setState({
			invoiceArray: this.state.invoiceArray, 
			advanceArray : this.state.advanceArray
		});
	}

	deleteItem(index, param) {
		let { invoiceArray, advanceArray } = this.state;
		if(param == 'invoice') {
			invoiceArray.splice(index, 1);
			this.setState({invoiceArray});
		} else {
			advanceArray.splice(index, 1);
			this.setState({advanceArray});
		}
	}

	render() {
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Advances and Invoices</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-2 form-group">
							<label className="labelclass">Adjust For</label>
							<LocalSelect options={["Partner", 'Employee']} value={this.state.voucher.adjustfor} onChange={(value, valueobj) => this.inputonChange(value, valueobj, 'adjustfor')} required={true} />
						</div>
						{this.state.voucher.adjustfor == 'Partner' ? <div className="col-md-3 form-group">
							<label className="labelclass">Partner</label>
							<AutoSelect resource="partners" fields="id,name,displayname" label="displayname" value={this.state.voucher.partnerid} onChange={(value, valueobj) => this.inputonChange(value, valueobj, 'partnerid')} required={true} />
						</div> : null}
						{this.state.voucher.adjustfor == 'Employee' ? <div className="col-md-3 form-group">
							<label className="labelclass">Employee</label>
							<AutoSelect resource="employees" fields="id,displayname" label="displayname" value={this.state.voucher.employeeid} onChange={(value, valueobj) => this.inputonChange(value, valueobj, 'employeeid')} required={true} />
						</div> : null}
						<div className="col-md-2 form-group">
							<label className="labelclass">Credit Entries Upto</label>
							<DateElement className={`form-control`} value={this.state.voucher.creditupto} onChange={(value, valueobj) => this.inputonChange(value, valueobj, 'creditupto')} />
						</div>
						<div className="col-md-2 form-group">
							<label className="labelclass">Debit Entries Upto</label>
							<DateElement className={`form-control`} value={this.state.voucher.debitupto} onChange={(value, valueobj) => this.inputonChange(value, valueobj, 'debitupto')} />
						</div>
						<div className="form-group col-md-2 col-sm-3">
							<button type="button" className="btn gs-btn-success btn-sm margintop-25" onClick={this.getData} disabled={!this.state.voucher.partnerid && !this.state.voucher.employeeid} ><i className="fa fa-search"></i>Go</button>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							{(this.state.voucher.partnerid || this.state.voucher.employeeid) && this.state.invoiceArray.length == 0 && this.state.btnclicked ? <div className="alert alert-warning text-center">There is no credit balance entries for your selection</div> : null}
							{(this.state.voucher.partnerid || this.state.voucher.employeeid) && this.state.invoiceArray.length > 0 ? <table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th colSpan="7">Credit Balance Entries</th>
									</tr>
									<tr>
										<th className="text-center">Voucher Type</th>
										<th className="text-center">Voucher No</th>
										<th className="text-center">Voucher Date</th>
										<th className="text-center">Account</th>
										<th className="text-center">Outstanding Amount</th>
										<th className="text-center">Amount to Allocate</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									{this.state.invoiceArray.map((item, index) => {
										return (
											<tr key={index}>
												<td>{item.vouchertype}</td>
												<td>{item.xsalesinvoiceid_invoiceno || item.xpurchaseinvoiceid_invoiceno || item.xjournalvoucherid_voucherno || item.xexpenserequestid_expenserequestno || item.xpayrollid_payrollno}</td>
												<td>{dateFilter(item.xsalesinvoiceid_invoicedate || item.xpurchaseinvoiceid_invoicedate || item.xjournalvoucherid_voucherdate || item.xexpenserequestid_expensedate || item.xpayrollid_payrolldate)}</td>
												<td>{item.accountid_name}</td>
												<td>{item.balance}</td>
												<td>
													<NumberElement className="form-control" value={item.amounttoall} onChange={(value) => this.updateamount(value, item)} />
												</td>
												<td>
													<button type="button" className="btn gs-form-btn-danger btn-sm" onClick={() => this.deleteItem(index, 'invoice')} ><span className="fa fa-trash-o"></span></button>
												</td>
											</tr>
										);
									})}
								</tbody>
							</table> : null}
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							{(this.state.voucher.partnerid || this.state.voucher.employeeid) && this.state.advanceArray.length == 0 && this.state.btnclicked ? <div className="alert alert-warning text-center">There is no debit balance entries for your selection</div> : null}
							{(this.state.voucher.partnerid || this.state.voucher.employeeid) && this.state.advanceArray.length > 0 ? <table className="table table-bordered">
								<thead>
									<tr>
										<th colSpan="7">Debit Balance Entries</th>
									</tr>
									<tr>
										<th>Voucher Type</th>
										<th>Vouher No</th>
										<th>Voucher Date</th>
										<th>Account</th>
										<th>Outstanding Amount</th>
										<th>Amount to Allocate</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									{this.state.advanceArray.map((item, index) => {
										return (
											<tr key={index}>
												<td>{item.vouchertype}</td>
												<td>{item.xsalesinvoiceid_invoiceno || item.xpurchaseinvoiceid_invoiceno || item.xjournalvoucherid_voucherno || item.xexpenserequestid_expenserequestno || item.xpayrollid_payrollno}</td>
												<td>{dateFilter(item.xsalesinvoiceid_invoicedate || item.xpurchaseinvoiceid_invoicedate || item.xjournalvoucherid_voucherdate || item.xexpenserequestid_expensedate || item.xpayrollid_payrolldate)}</td>
												<td>{item.accountid_name}</td>
												<td>{item.balance}</td>
												<td><NumberElement className="form-control" value={item.amounttoall} onChange={(value) => this.updateamount(value, item)} /></td>
												<td><button type="button" className="btn gs-form-btn-danger btn-sm" onClick={() => this.deleteItem(index, 'advance')} ><span className="fa fa-trash-o"></span></button></td>
											</tr>
										);
									})}
								</tbody>
							</table> : null }
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.setData} disabled={this.state.invoiceArray.length == 0 || this.state.advanceArray.length == 0}><i className="fa fa-check"></i>Create</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
