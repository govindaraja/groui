import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import utils, { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { NumberElement } from '../utilcomponents';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: {},
			uniqueObj: {},
			tabClasses: ["show active"]
		};
		this.onLoad = this.onLoad.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.addProjectItem = this.addProjectItem.bind(this);
		this.quantityOnChange = this.quantityOnChange.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getTabClass = this.getTabClass.bind(this);
		this.setActiveTab = this.setActiveTab.bind(this);
		this.renderStage = this.renderStage.bind(this);
		this.renderTabContent = this.renderTabContent.bind(this);
		this.searchFn = this.searchFn.bind(this);
		this.sortStages = this.sortStages.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let uniqueObj = {};
		this.updateLoaderFlag(true);

		axios.get(`/api/query/getboqmilestonestatusquery?projectid=${this.props.resource.projectid}`).then((response) => {
			if (response.data.message == 'success') {
				let projectItemArray = response.data.main;

				projectItemArray.forEach((item) => {
					item.quantity = '';
					if(!uniqueObj[item.milestonetemplateid]) {
						uniqueObj[item.milestonetemplateid] = {
							milestonetemplateid_name: item.milestonetemplateid_name,
							stages: {},
							boqItems: {},
							boqMileObj: {}
						};
					}

					if(!uniqueObj[item.milestonetemplateid].stages[item.milestonestageid]) {
						uniqueObj[item.milestonetemplateid].stages[item.milestonestageid] = {
							displayorder: Object.keys(uniqueObj[item.milestonetemplateid].stages).length,
							milestonestageid_name: item.milestonestageid_name
						};
					}
					if(!uniqueObj[item.milestonetemplateid].boqItems[item.boqid]) {
						uniqueObj[item.milestonetemplateid].boqItems[item.boqid] = {
							boqid: item.boqid,
							boqid_internalrefno: item.boqid_internalrefno,
							boqid_clientrefno: item.boqid_clientrefno,
							itemid: item.itemid,
							itemid_name: item.itemid_name,
							boqid_description: item.boqid_description,
							boqid_quantity: item.boqid_quantity,
							uomid_name: item.uomid_name
						};
					}
					uniqueObj[item.milestonetemplateid].boqMileObj[`${item.boqid}_${item.milestonestageid}`] = item;
				});

				this.props.resource.milestoneprogressitems.forEach((progressitem) => {
					Object.keys(uniqueObj).forEach((templateitem) => {
						if(uniqueObj[templateitem].boqMileObj[`${progressitem.boqid}_${progressitem.milestonestageid}`]) {
							uniqueObj[templateitem].boqMileObj[`${progressitem.boqid}_${progressitem.milestonestageid}`].quantity = progressitem.quantity;

							uniqueObj[templateitem].boqItems[progressitem.boqid].checked = true;
						}
					});
				});

				this.setState({ uniqueObj }, ()=> {
					this.setActiveTab(1);
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	searchFn(boqItemObj, searchObj) {
		let tempObj = {};
		Object.keys(boqItemObj).forEach((a) => {
			let filtered = true;
			for(var prop in searchObj) {
				if(searchObj[prop] != null && searchObj[prop] != '' && searchObj[prop] != undefined) {
					let searchstring = '' + searchObj[prop] + '';
					let isDateObject = false;
					if(searchObj[prop] instanceof Date) {
						searchstring = '' + searchObj[prop].toDateString() + '';
						isDateObject = true;
					}
					if((boqItemObj[a][prop] != null && boqItemObj[a][prop] != '' && boqItemObj[a][prop] != undefined) || typeof(boqItemObj[a][prop]) == 'boolean') {
						let originalstring = '' + boqItemObj[a][prop] + '';

						if(isDateObject && new Date(boqItemObj[a][prop]) != 'Invalid Date')
							originalstring = '' + new Date(boqItemObj[a][prop]).toDateString() + '';

						if(typeof(boqItemObj[a][prop]) == 'boolean') {
							originalstring = boqItemObj[a][prop] ? 'true' : 'false';
							searchstring = ['y', 'ye', 'yes', 'e', 'es', 's'].includes(searchstring.toLowerCase()) ? 'true' : (['n', 'no', 'o'].includes(searchstring.toLowerCase()) ? 'false' : searchstring);
						}

						if(originalstring.toLowerCase().indexOf(searchstring.toLowerCase()) == -1)
							filtered = false;
					} else {
						filtered = false;
					}
				}
			}
			if(filtered)
				tempObj[a] = boqItemObj[a];
		});
		return tempObj;
	}

	inputonChange(value, field) {
		let { search } = this.state;
		search[field] = value;
		this.setState({search});
	}

	checkallonChange(value, milestonetemplateid) {
		let { uniqueObj } = this.state;

		Object.keys(uniqueObj).forEach((templateitem) => {
			if(milestonetemplateid == templateitem) {
				uniqueObj[templateitem].checked = value;
				Object.keys(uniqueObj[templateitem].boqItems).forEach((item) => {
					uniqueObj[templateitem].boqItems[item].checked = value;
				});
			}
		});
		this.setState({	uniqueObj });
	}

	checkboxOnChange(value, item) {
		item.checked = value;
		this.setState({
			uniqueObj: this.state.uniqueObj
		});
	}

	getTabClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	getTabPaneClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	setActiveTab(tabNum) {
		let { tabClasses } = this.state;
		tabClasses =  ["", ""]
		tabClasses[tabNum] = "show active";
		this.setState({tabClasses}, ()=>{
			this.getTabPaneClass(tabNum);
		});
	}

	quantityOnChange(value, item) {
		item.quantity = value;

		this.setState({
			uniqueObj: this.state.uniqueObj
		});
	}

	addProjectItem () {
		let { uniqueObj } = this.state;
		let errArr = [], progressitemsArr = [];
		let ischeckedfound = false, invalidqty = false;
		let tempidObj = {};

		Object.keys(uniqueObj).forEach((templateitem) => {
			tempidObj[templateitem] = {
				boq: {}
			};
			Object.keys(uniqueObj[templateitem].boqItems).forEach((boqitem) => {
				Object.keys(uniqueObj[templateitem].stages).forEach((stageitem, stageindex) => {

					let itemstr = `${boqitem}_${stageitem}`;
					uniqueObj[templateitem].boqMileObj[itemstr].checked = false;

					if(uniqueObj[templateitem].boqItems[boqitem].checked) {

						tempidObj[templateitem].boq[boqitem] = tempidObj[templateitem].boq[boqitem] ? tempidObj[templateitem].boq[boqitem] : false;

						if(Number(uniqueObj[templateitem].boqMileObj[itemstr].quantity) > 0) {
							tempidObj[templateitem].boq[boqitem] = true;
							uniqueObj[templateitem].boqMileObj[itemstr].checked = true;
						}
					}

					progressitemsArr.push(uniqueObj[templateitem].boqMileObj[itemstr]);
				});
			});
		});

		for(var prop in tempidObj) {
			if(Object.keys(tempidObj[prop].boq).length > 0) {
				ischeckedfound = true;
				for(var boqprop in tempidObj[prop].boq) {
					if(!tempidObj[prop].boq[boqprop]) {
						invalidqty = true;
					}
				}
			}
		}

		if(!ischeckedfound)
			errArr.push("Please choose atleast one item");

		if(invalidqty)
			errArr.push("Please pick quantity for any one of the stage for choosed items");

		if(errArr.length > 0)
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : errArr,
				btnArray : ['Ok']
			}));

		this.props.callback(progressitemsArr);
		this.props.closeModal();
	}

	sortStages(stageObj) {
		return Object.keys(stageObj).map(prop => {
			return {displayorder: stageObj[prop].displayorder, prop: prop}
		}).sort((a, b) => a.displayorder-b.displayorder).map(a => a.prop);
	}

	renderStage(item, templateObj) {
		return this.sortStages(templateObj.stages).map((stageitem, stageindex) => {
			let secitem = templateObj.boqMileObj[`${item.boqid}_${stageitem}`];
			return([
				<td className="text-center" key={stageindex+1}>{secitem ? secitem.completedqty : '-'}</td>,
				<td className="text-center" key={stageindex+2}>{secitem ? <NumberElement className="form-control" value={secitem.quantity} onChange={(value) => this.quantityOnChange(value, secitem)} disabled = {secitem.boqid_quantity == secitem.completedqty} /> : '-'} </td>
			]);
		});
	}

	renderTabContent(uniqueObj) {
		return Object.keys(uniqueObj).map((templateObj, templateindex) => {
			return (
				<div className={`tab-pane fade ${this.getTabPaneClass(templateindex+1)}`} role="tabpanel" key={templateindex}>
					<div className="row margintop-25">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="table-responsive">
								<table className="table table-bordered table-condensed">
									<thead>
										<tr>
											<th className="text-center">
												<input type="checkbox" checked={uniqueObj[templateObj].checked || false} onChange={(evt)=>{this.checkallonChange(evt.target.checked, templateObj)}}/>
											</th>
											<th className="text-center" style={{width: '7%'}}>Ref No</th>
											<th className="text-center" style={{width: '20%'}}>Item Name</th>
											<th className="text-center" style={{width: '7%'}}>BOQ Qty</th>
											{
												this.sortStages(uniqueObj[templateObj].stages).map((stageitem, stageindex) => {
													return(
														<th colSpan="2" className="text-center" key={stageindex}>{uniqueObj[templateObj].stages[stageitem].milestonestageid_name}</th>
													);
												})
											}
										</tr>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											{
												this.sortStages(uniqueObj[templateObj].stages).map((stageitem, stageindex) => {
													return([
														<th className="text-center" key={stageindex+1}>So far Completed</th>,
														<th className="text-center" key={stageindex+2}>Current Qty</th>
													]);
												})
											}
										</tr>
									</thead>
									<tbody>
										{Object.keys(this.searchFn(uniqueObj[templateObj].boqItems, this.state.search)).map((boqid, index) => {
											let item = uniqueObj[templateObj].boqItems[boqid];
											return (
												<tr key={index}>
													<td className="text-center">
														<input type="checkbox" checked={item.checked || false} onChange={(evt)=>{this.checkboxOnChange(evt.target.checked, item)}}/>
													</td>
													<td>{item.boqid_internalrefno}</td>
													<td><div>{item.itemid_name}</div><div className="text-ellipsis-line-2 font-13 text-muted">{item.boqid_description}</div></td>
													<td>{item.boqid_quantity} {item.uomid_name}</td>
													{
														this.renderStage(item, uniqueObj[templateObj])
													}
												</tr>
											)
										})}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			)
		});
	}

	render() {
		let { uniqueObj } = this.state;
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Project Item Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-3 form-group">
							<input type="text" className="form-control"  value={this.state.search.boqid_internalrefno || ''} placeholder="Search By Ref No" onChange={(evt) =>{this.inputonChange(evt.target.value, 'boqid_internalrefno')}}/>
						</div>
						<div className="col-md-3 form-group">
							<input type="text" className="form-control"  value={this.state.search.itemid_name || ''} placeholder="Search By BOQ Item" onChange={(evt) =>{this.inputonChange(evt.target.value, 'itemid_name')}}/>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<ul className="nav nav-tabs">
								{
									Object.keys(uniqueObj).map((templateObj, templateindex) => {
										return (
											<li className="nav-item" key={templateindex}>
												<a className={`nav-link ${this.getTabClass(templateindex+1)}`} onClick={()=>this.setActiveTab(templateindex+1)}>{uniqueObj[templateObj].milestonetemplateid_name}</a>
											</li>
										)
									})
								}
							</ul>
							<div className="tab-content">
								{this.renderTabContent(uniqueObj)}
							</div>
							{this.state.loaderflag ? <div className="text-center alert alert-info">Please wait ...</div> : null}
							{!this.state.loaderflag && Object.keys(uniqueObj).length == 0 ? <div className="text-center alert alert-warning">There is no approved project items</div> : null}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								{Object.keys(this.state.uniqueObj).length > 0 ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addProjectItem}><i className="fa fa-check"></i>Ok</button> : null}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});