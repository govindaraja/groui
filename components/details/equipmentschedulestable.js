import React, { Component } from 'react';
import axios from 'axios';
import { dateFilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			equipmentScheduleArray : []
		};

		this.onLoad = this.onLoad.bind(this);
	};

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		axios.get(`/api/equipmentschedules?field=id,equipmentscheduledate,donedate,status,suspendremarks,remarks&filtercondition=equipmentschedules.contractitemsid=${this.props.item.id}`).then((response) => {
			if (response.data.message == 'success') {
				this.setState({
					equipmentScheduleArray : response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Equipment Schedules</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<table className="table table-bordered table-condensed table-sm">
								<thead>
									<tr>
										<th className="text-center" style={{whiteSpace:'nowrap'}}>Schedule Date</th>
										<th className="text-center" style={{whiteSpace:'nowrap'}}>Done Date</th>
										<th className="text-center" style={{whiteSpace:'nowrap'}}>Status</th>
										<th className="text-center">Remarks</th>
										<th className="text-center">Suspended Remarks</th>
									</tr>
								</thead>
								<tbody>
									{this.state.equipmentScheduleArray.map((item, index) => {
										return (<tr key={index}>
											<td className="text-center">{dateFilter(item.equipmentscheduledate)}</td>
											<td className="text-center">{dateFilter(item.donedate)}</td>
											<td className="text-center">{item.status}</td>
											<td>{item.remarks}</td>
											<td>{item.suspendremarks}</td>
										</tr>);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
