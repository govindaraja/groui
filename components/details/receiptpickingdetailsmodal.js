import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import {Field, Fields, FieldArray, formValueSelector  } from 'redux-form';

import { commonMethods, modalService } from '../../utils/services';
import { currencyFilter, dateFilter, uomFilter } from '../../utils/filter';

import { InputEle, NumberEle, SpanEle, DateEle, textareaEle, selectAsyncEle, localSelectEle, autoSelectEle, richtextEle, qtyEle, emailEle, checkboxEle, ContactEle, DisplayGroupEle, RateInline, StockInline, AddressEle, autosuggestEle, ButtongroupEle, RateField, DiscountField, TimepickerEle } from '../formelements';
import {numberValidation, dateValidation, multiSelectValidation, requiredValidation } from '../../utils/utils';
import {ChildEditModal} from '../utilcomponents';
import moment from 'moment';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			headArray: [],
			selectedItem: {}
		};

		this.selector = formValueSelector(this.props.form);
		this.onLoad = this.onLoad.bind(this);
		this.renderTableHeader = this.renderTableHeader.bind(this);
		this.addNewRow = this.addNewRow.bind(this);
		this.deleteRow = this.deleteRow.bind(this);
		this.batchcallback = this.batchcallback.bind(this);
		this.locationcallback = this.locationcallback.bind(this);
		this.pickserial = this.pickserial.bind(this);
	}

	componentWillMount() {
		let {resource, itemstr} = this.props;
		let item = this.selector(this.props.fullstate, itemstr);

		let tempQuantity = item.quantity;
		if(item.itemid_hasserial && item.uomconversiontype && item.uomconversionfactor)
			tempQuantity = Number((item.quantity / item.uomconversionfactor).toFixed(0));

		if (!item.pickingdetails || !item.pickingdetails.details) {
			this.props.updateFormState(this.props.form, {
				[`${itemstr}.pickingdetails`]: {
					details : [{
						edit : true,
						quantity : tempQuantity,
						warehouseid : item.warehouseid
					}]
				}
			});
		} else if(item.pickingdetails.details.length == 0) {
			this.props.updateFormState(this.props.form, {
				[`${itemstr}.pickingdetails`]: {
					details : [{
						edit : true,
						quantity : tempQuantity,
						warehouseid : item.warehouseid
					}]
				}
			});
		}

		let headArray = [];
		if(item.itemid_hasbatch) {
			headArray.push('Batch');
			headArray.push('Manufacturing Date');
			headArray.push('Expiry Date');
		}

		if(this.props.app.useSublocation)
			headArray.push('Location');

		headArray.push('Qty');

		if(item.itemid_hasserial)
			headArray.push('Serial Number');

		this.setState({
			headArray
		}, () => {
			this.onLoad(item);
		});
	}

	onLoad(item) {
		let queryString = `/api/itemmaster?pagelength=includeinactive=true&field=id,name,isactive,description,isautogenerateserialno,serialnoformat,serialnocurrentvalue&filtercondition=itemmaster.id=${item.itemid}`;

		axios.get(`${queryString}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.updateFormState(this.props.form, {
					[`${this.props.itemstr}.itemid_isautogenerateserialno`]: response.data.main[0].isautogenerateserialno,
					[`${this.props.itemstr}.itemid_serialnoformat`]: response.data.main[0].serialnoformat,
					[`${this.props.itemstr}.itemid_serialnocurrentvalue`]: response.data.main[0].serialnocurrentvalue,
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	renderTableHeader() {
		return this.state.headArray.map((a, index)=> {
			return <th key={index} className="text-center">{a}</th>
		});
	}

	addNewRow() {
		let {resource, itemstr} = this.props;
		let item = this.selector(this.props.fullstate, itemstr);
		let details = [...item.pickingdetails.details];
		details.push({
			edit : true,
			warehouseid : item.warehouseid
		});

		this.props.updateFormState(this.props.form, {
			[`${itemstr}.pickingdetails.details`] : details
		});
	}

	deleteRow(rowindex) {
		let {resource, itemstr} = this.props;
		let item = this.selector(this.props.fullstate, itemstr);
		let details = [...item.pickingdetails.details];
		details.splice(rowindex, 1);

		this.props.updateFormState(this.props.form, {
			[`${itemstr}.pickingdetails.details`] : details
		});
	}

	batchcallback(value, valueobj, member) {
		let tempObj = {};
		tempObj[`${member}.expirydate`] = valueobj.expirydate;
		tempObj[`${member}.manufacturingdate`] = valueobj.manufacturingdate;
		tempObj[`${member}.itembatchid_batchnumber`] = valueobj.batchnumber;
		this.props.updateFormState(this.props.form, tempObj);
	}

	locationcallback(value, valueobj, member) {
		let tempObj = {};
		tempObj[`${member}.stocklocationid_fullname`] = valueobj.fullname;
		this.props.updateFormState(this.props.form, tempObj);
	}

	pickserial(member) {
		let rowitem = this.selector(this.props.fullstate, member);
		this.props.openModal({render: (closeModal) => {return <SerialnopickingdetailsModal closeModal={closeModal} rowitem={rowitem} member={member} form={this.props.form} updateFormState={this.props.updateFormState} />}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}});
	}

	render() {
		let {resource, itemstr} = this.props;
		let item = this.selector(this.props.fullstate, itemstr);
		let childarray = item.pickingdetails ? (item.pickingdetails.details ? item.pickingdetails.details : []) : [];
		let statusparam = resource.status == 'Approved' || resource.status == 'Received' || resource.status == 'Cancelled';
		let showSerial = item.itemid_hasserial && ['Purchase Orders'].indexOf(resource.ispurchaseorder) >= 0 && !statusparam && item.itemid_isautogenerateserialno;
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Picking Details</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="form-group col-md-2 col-sm-6">                     
							<label className="labelclass">Receiptnote Item ID</label>                  
							<input type="number" className="form-control" value={item.id} disabled></input>	
						</div>
						<div className="form-group col-md-3 col-sm-6">                     
							<label className="labelclass">Item Name</label> 
							<input type="text" className="form-control" value={item.itemid_name} disabled></input>
						</div>
						<div className="form-group col-md-3 col-sm-6">                     
							<label className="labelclass">Warehouse</label>                  
							<Field name={`${itemstr}.warehouseid`} props={{
								resource : 'stocklocations',
								fields: 'id,name',
								filter: 'stocklocations.companyid='+resource.companyid+' and stocklocations.isparent and stocklocations.parentid is null',
								disabled: true,
								required : true
							}} component={selectAsyncEle}/>
						</div>
						{resource.ispurchaseorder != 'Others' ? <div className="form-group col-md-2 col-sm-6">
							<label className="labelclass">Incoming Quantity</label>
							<input type="number" className="form-control" value={item.incomingqty} disabled></input>
						</div> : null }
						<div className="form-group col-md-2 col-sm-6">
							<label className="labelclass">Accepted Quantity</label>
							<input type="number" className="form-control" value={item.quantity} required disabled></input>
						</div>
					</div>

					<div className="row">
						{showSerial ? <div className="form-group col-md-12 col-sm-12 col-xs-12 alert alert-warning">Serial Nos will be generated automatically when Receipt Note is Approved</div> : <div className="col-md-12 col-sm-12 col-xs-12">
							<table className="table">
								<thead className="thead-light">
									<tr>
										{this.renderTableHeader()}
										<th className="text-center editBtnwidth"></th>
									</tr>
								</thead>
								<tbody>
									{childarray.map((rowitem, rowindex) => {
										let member = `${itemstr}.pickingdetails.details[${rowindex}]`;
										return (
											<tr key={rowindex}>
												{item.itemid_hasbatch ? <td style={{width: '25%'}}>
													<Field name={`${member}.itembatchid`} props={{
														resource : 'itembatches',
														fields: 'id,itemid,expirydate,manufacturingdate,batchnumber',
														label: 'batchnumber',
														filter: `itembatches.itemid=${item.itemid}`,
														required: true,
														showadd: true,
														createParams: {
															itemid: item.itemid,
															manufacturingdate: this.props.resource.receiptnotedate ? this.props.resource.receiptnotedate : null
														},
														createOrEdit: this.props.createOrEdit,
														disabled: statusparam,
														onChange: (value, valueObj) => this.batchcallback(value, valueObj, member)
													}} component={autoSelectEle} validate={[numberValidation]}/>

												</td> : null }
												{item.itemid_hasbatch ? <td style={{width: '20%'}}>
													<Field name={`${member}.manufacturingdate`} props={{disabled: true}} component={DateEle} />
												</td> : null }
												{item.itemid_hasbatch ? <td style={{width: '20%'}}>
													<Field name={`${member}.expirydate`} props={{disabled: true}} component={DateEle} />
												</td> : null }
												{this.props.app.useSublocation ? <td style={{width: '20%'}}>
													<Field name={`${member}.stocklocationid`} props={{
														resource : 'stocklocations',
														fields: 'id,parentid,fullname',
														label: 'fullname',
														filter: `stocklocations.isparent=false and stocklocations.warehouseid=${item.warehouseid}`,
														required: true,
														disabled: statusparam,
														onChange: (value, valueObj) => this.locationcallback(value, valueObj, member)
													}} component={autoSelectEle} validate={[numberValidation]} />
												</td> : null }
												<td style={{width: '10%'}}>
													<Field name={`${member}.quantity`} props={{disabled : statusparam, required: true }} component={NumberEle} validate={[numberValidation]}/>
												</td>
												{item.itemid_hasserial ? <td>
													<Field name={`${member}.serialArray`} props={{required : true, disabled : statusparam}} component={textareaEle} validate={[requiredValidation]}/>
													{statusparam ? null : <button type="button" className="btn btn-sm gs-btn-info btn-width" onClick={()=>this.pickserial(member)}><i className="fa fa-check"></i>Generate</button>}
												</td> : null }
												<td className="text-center"><button type='button' className='btn gs-form-btn-danger btn-sm btndisable' onClick={() => this.deleteRow(rowindex)} disabled={statusparam}><span className='fa fa-trash-o'></span></button></td>
											</tr>
										)
									})}
								</tbody>
							</table>
							<button type="button" className="btn btn-sm gs-btn-info btn-width btnhide" onClick={this.addNewRow} disabled={statusparam}><i className="fa fa-plus"></i>Add</button>
						</div>}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
};

class SerialnopickingdetailsModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			format : '',
			currentValue: null
		};
		
		this.generateSerial = this.generateSerial.bind(this);
	}

	generateSerial() {
		let count = this.props.rowitem.quantity;
		let serialArray = [];
		let serialno = Number(this.state.currentValue);
		for (var i = 1; i <= count; i++) {
			serialno ++;
			serialArray.push(`${this.state.format}${serialno}`);
		}
		this.props.updateFormState(this.props.form, {
			[`${this.props.member}.serialArray`]: serialArray.join(),
		});
		this.props.closeModal();
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Serial Number Generation</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-3">
							<label className="label-media-sm">Format</label>
						</div>
						<div className="form-group col-md-9">
							<input type="text" value={this.state.format} className={`form-control ${!this.state.format ? 'errorinput' : ''}`} onChange={(evt) => this.setState({format : evt.target.value})} />
						</div>
						<div className="col-md-3">
							<label className="label-media-sm">Current Value</label>
						</div>
						<div className="form-group col-md-9">
							<input type="number" value={this.state.currentValue || ''} className={`form-control ${!this.state.currentValue ? 'errorinput' : ''}`}  onChange={(evt) => this.setState({currentValue : evt.target.value})} />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" disabled={!this.state.format || !this.state.currentValue} onClick={()=>this.generateSerial()}><i className="fa fa-check"></i>Generate</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
