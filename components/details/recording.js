import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService } from '../../utils/services';

class RecordingplayModal extends Component {
	constructor(props) {
		super(props);
		this.state ={
			url : null
		};
		this.getrecording = this.getrecording.bind(this);
	}

	componentWillMount(){
		this.getrecording();
	}

	getrecording(){
		axios({
			method : 'GET',
			url : `/api/common/methods/getRecording?callid=${this.props.callid}`
		}).then((response)=> {
			if (response.data.message == 'success') {
				this.setState({
					url  : response.data.main.url
				});

				let urlProtocol = response.data.main.url.split(':')[0];
				if(urlProtocol != 'https'){
					window.open(response.data.main.url,'_blank');
					this.props.closeModal();
				}
			} else {
				this.props.closeModal();
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header"></div>
				<div className="react-modal-body text-center">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
						{
							(this.state.url) ? 
								<audio src={this.state.url} controls controlsList="nodownload" autoPlay/>
							: <div className="alert alert-warning text-center">
									Loading ...
								</div>
						}	
			        	</div>
			        </div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default RecordingplayModal;