import React, { Component } from 'react';
import axios from 'axios';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			reason : ''
		};

		this.onLoad = this.onLoad.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.expireItems = this.expireItems.bind(this);
	};

	componentWillMount () {
		this.onLoad();
	}

	onLoad() {
		let tempObj = {};
		this.props.resource.contractitems.forEach((item, index) => {
			if(item.contractstatus == 'Active')
				tempObj[`contractitems[${index}].checked`] = true;
		});

		this.props.updateFormState(this.props.form, tempObj);
 	}

	inputonChange (value) {

		this.props.updateFormState(this.props.form, {
			comment : value
		});

		this.setState({ reason : value });
	}

	checkboxOnChange (value, index) {
		this.props.updateFormState(this.props.form, {
			[`contractitems[${index}].checked`] : value
		});
	}

	expireItems() {
		this.props.closeModal();
		this.props.callback();
	}

	render () {
		let buttondisabled = false, checkedCount = 0;

		this.props.resource.contractitems.forEach((item) => {
			if(item.contractstatus == 'Active' && item.checked)
				checkedCount += 1;
		});

		buttondisabled = checkedCount == 0 || !this.state.reason;

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Expire Contracts</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<table className="table table-bordered table-condensed table-sm">
								<thead>
									<tr>
										<th></th>
										<th className="text-center">Equipment</th>
										<th className="text-center">Serial No</th>
										<th className="text-center">Contract Type</th>
									</tr>
								</thead>
								<tbody>
									{this.props.resource.contractitems.map((item, index) => {
										if(item.contractstatus == 'Active') {
											return (<tr key={index}>
												<td className="text-center"><input type="checkbox" checked={item.checked} onChange={(evt) => this.checkboxOnChange(evt.target.checked, index)}/></td>
												<td>{item.description}</td>
												<td className="text-center">{item.equipmentid_serialno}</td>
												<td className="text-center">{item.contracttypeid_name}</td>
											</tr>);
										}
										return null;
									})}
								</tbody>
							</table>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="react-modal-title"><b> Reson for Expiry</b></div>
							<div className="react-modal-body">
								<textarea className={`form-control ${this.state.reason == null || this.state.reason == "" || this.state.reason == undefined ? 'errorinput' : ''}`} value={this.state.reason} onChange={(evt)=>{this.inputonChange(evt.target.value)}} required="true"/>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<p> This action cannot be reversed. Are you sure you want to expire the equipment or contract?</p>
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.expireItems} disabled={buttondisabled}><i className="fa fa-check"></i>Expire</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
