import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.openResource = this.openResource.bind(this);
		this.close = this.close.bind(this);
	}

	openResource () {
		this.props.closeModal();
	}

	close(param) {
		this.props.closeModal();
		this.props.callback(param);
	}

	render() {
		if(!this.props.resourcedetails)
			return null;
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Alert - Unapproved Transactions Found</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							<p>There are unapproved transactions found related to this transaction. Please review them below.</p>
						</div>
						<div className="col-md-12 col-sm-12">
							<table className="table table-bordered table-sm">
								<thead>
									<tr>
										<th className="text-center">Transaction No</th>
										<th className="text-center">Date</th>
										<th className="text-center">Status</th>
									</tr>
								</thead>
								<tbody>
									{this.props.resourcedetails.map((data, index)=> {
										return (<tr key={index}>
												<td className="text-center"><a href={`#/details/${data.resourcename}/${data.id}`} onClick={this.openResource}>{data.resourceno}</a></td>
												<td>{dateFilter(data.resourcedate)}</td>
												<td className="text-center">{data.status}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
							<p>Would you like to continue creating a new transaction ?</p>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={()=>this.close(false)}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={()=>this.close(true)}><i className="fa fa-check-square-o"></i>Yes, Continue</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
