import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import StockdetailsModal from '../../containers/stockdetails';
import { NumberElement } from '../utilcomponents';
import { dateFilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			displaygroupArray : [],
			itemaddonArray: [],
			rateDisabled: this.props.checkCondition('disabled', this.props.app.feature.addonRateDisabled),
			checkall: false,
			selectedItem: this.getSelectedItem(this.props)
		};
		this.close = this.close.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			selectedItem : this.getSelectedItem(nextProps)
		});
	}

	componentWillMount() {
		if(['Approved', 'Sent To Customer', 'Won', 'Lost'].indexOf(this.props.resource.status) >= 0) {
			let itemaddonArray = [];
			let kititems = this.props.resource[this.props.kit];
			for (var i = 0; i < kititems.length; i++) {
				if (kititems[i].rootindex == this.state.selectedItem.index)
					itemaddonArray.push(kititems[i]);
			}
			this.setState({
				itemaddonArray
			});
		} else {
			this.onLoad();
		}
	}

	onLoad() {
		var queryString = "/api/query/getaddonitemquery?itemid=" + this.state.selectedItem.itemid+"&pricelistid="+this.props.resource.pricelistid;
		axios.get(queryString).then((response) => {
			if (response.data.message == 'success') {
				let addonArray = response.data.main;
				var displaygroupArray = [];
				let kititems = this.props.resource[this.props.kit];
				for (var i = 0; i < addonArray.length; i++) {
					addonArray[i].conversion = addonArray[i].quantity;
					addonArray[i].addonitemratelc = addonArray[i].addonitemrate ? addonArray[i].addonitemrate : 0;
					addonArray[i].addonitemrate = commonMethods.getRate(addonArray[i].addonitemratelc, this.props.resource.currencyid, this.props.resource.currencyexchangerate, this.props.resource.pricelistid, this.props.app);
					if(addonArray[i].ismandatoryoption) {
						var itemFound = false;
						for (var j = 0; j < kititems.length; j++) {
							if (kititems[j].itemid == addonArray[i].itemid && kititems[j].rootindex == this.state.selectedItem.index) {
								itemFound = true;
								break;
							}
						}
						if(!itemFound) {
							addonArray[i].check = true;
							this.checkboxonChange(addonArray[i]);
						}
					}
					var itemFound = false;
					for (var j = 0; j < displaygroupArray.length; j++) {
						if (addonArray[i].displaygroup == displaygroupArray[j].name) {
							displaygroupArray[j].itemaddon.push(addonArray[i]);
							itemFound = true;
						}
					}
					if (!itemFound)
						displaygroupArray.push({
							name : response.data.main[i].displaygroup,
							itemaddon : [addonArray[i]]
						});
				}
				for (var i = 0; i < kititems.length; i++) {
					for (var j = 0; j < displaygroupArray.length; j++) {
						for (var k = 0; k < displaygroupArray[j].itemaddon.length; k++) {
							if (kititems[i].itemid == displaygroupArray[j].itemaddon[k].itemid && kititems[i].rootindex == this.state.selectedItem.index) {
								displaygroupArray[j].itemaddon[k].conversion = kititems[i].conversion;
								displaygroupArray[j].itemaddon[k].addonitemrate = kititems[i].rate;
								displaygroupArray[j].itemaddon[k].check = true;
							}
						}
					}
				}
				this.setState({
					displaygroupArray
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				modalService[apiResponse.methodName](apiResponse.message);
			}
		});
	}

	updateAddOnField(item, field, value) {
		item[field] = value;
		this.setState({
			displaygroupArray: this.state.displaygroupArray
		}, () => {
			this.checkboxonChange(item);
		});
	}

	checkboxonChange(item) {
		var tempObj = {};
		let kititems = this.props.resource[this.props.kit];
		if (item.check && item.conversion > 0 && item.addonitemrate >= 0) {
			var itemFound = false;
			for (var i = 0; i < kititems.length; i++) {
				if (item.itemid == kititems[i].itemid && kititems[i].rootindex == this.state.selectedItem.index) {
					itemFound = true;
					tempObj[`${this.props.kit}[${i}].conversion`] = item.conversion;
					if(this.state.selectedItem.quantity > 0)
						tempObj[`${this.props.kit}[${i}].quantity`] = item.conversion * this.state.selectedItem.quantity;
					else
						tempObj[`${this.props.kit}[${i}].quantity`] = null;
					tempObj[`${this.props.kit}[${i}].rate`] = item.addonitemrate;
					tempObj[`${this.props.kit}[${i}].addondisplaygroup`] = item.displaygroup;
					tempObj[`${this.props.kit}[${i}].addondisplayorder`] = item.displayorder;
					break;
				}
			}
			if(!itemFound) {
				var index = 0;
				for (var i = 0; i < kititems.length; i++) {
					if (kititems[i].index > index)
						index = kititems[i].index;
				}
				var tempChiObj = {
					itemid : item.itemid,
					itemid_name : item.itemid_name,
					itemid_issaleskit : item.itemid_issaleskit,
					itemid_keepstock : item.itemid_keepstock,
					index : index + 1,
					uomid : item.uomid,
					uomid_name : item.uomid_name,
					rate : item.addonitemrate,
					rootindex : this.state.selectedItem.index,
					parent : this.state.selectedItem.itemid_name,
					conversion : item.conversion,
					addondisplaygroup: item.displaygroup,
					addondisplayorder: item.displayorder
				};
				if(this.state.selectedItem.quantity > 0)
					tempChiObj.quantity = item.conversion * this.state.selectedItem.quantity;
				else
					tempChiObj.quantity = null;
				this.props.array.push(this.props.kit, tempChiObj);
			}
		} else {
			for (var i = 0; i < kititems.length; i++) {
				if (item.itemid == kititems[i].itemid && kititems[i].rootindex == this.state.selectedItem.index) {
					this.props.array.remove(this.props.kit, i);
					break;
				}
			}
		}
		this.props.updateFormState(this.props.form, tempObj);
		setTimeout(() => {
			this.calculateRate();	
		}, 0);
	}

	checkAllAddOnField(value) {
		let displaygroupArray = [...this.state.displaygroupArray];
		let kititems = [...this.props.resource[this.props.kit]];
		var tempObj = {};

		displaygroupArray.forEach((displaygroup) => {
			displaygroup.itemaddon.map((item) => {
				if(!item.ismandatoryoption) {
					item.check = value;
					if(item.check && item.conversion > 0 && item.addonitemrate >= 0) {
						var itemFound = false;
						for (var i = 0; i < kititems.length; i++) {
							if (item.itemid == kititems[i].itemid && kititems[i].rootindex == this.state.selectedItem.index) {
								itemFound = true;
								tempObj[`${this.props.kit}[${i}].conversion`] = item.conversion;
								if(this.state.selectedItem.quantity > 0)
									tempObj[`${this.props.kit}[${i}].quantity`] = item.conversion * this.state.selectedItem.quantity;
								else
									tempObj[`${this.props.kit}[${i}].quantity`] = null;

								tempObj[`${this.props.kit}[${i}].rate`] = item.addonitemrate;
								tempObj[`${this.props.kit}[${i}].addondisplaygroup`] = item.displaygroup;
								tempObj[`${this.props.kit}[${i}].addondisplayorder`] = item.displayorder;
								break;
							}
						}
						if(!itemFound) {
							var index = 0;
							for (var i = 0; i < kititems.length; i++) {
								if (kititems[i].index > index)
									index = kititems[i].index;
							}
							var tempChiObj = {
								itemid : item.itemid,
								itemid_name : item.itemid_name,
								itemid_issaleskit : item.itemid_issaleskit,
								itemid_keepstock : item.itemid_keepstock,
								index : index + 1,
								uomid : item.uomid,
								uomid_name : item.uomid_name,
								rate : item.addonitemrate,
								rootindex : this.state.selectedItem.index,
								parent : this.state.selectedItem.itemid_name,
								conversion : item.conversion,
								addondisplaygroup: item.displaygroup,
								addondisplayorder: item.displayorder
							};
							if(this.state.selectedItem.quantity > 0)
								tempChiObj.quantity = item.conversion * this.state.selectedItem.quantity;
							else
								tempChiObj.quantity = null;
							kititems.push(tempChiObj);
						}
					} else {
						for (var i = 0; i < kititems.length; i++) {
							if (item.itemid == kititems[i].itemid && kititems[i].rootindex == this.state.selectedItem.index) {
								kititems.splice(i, 1);
								break;
							}
						}
					}
				}
			});
		});

		this.setState({
			displaygroupArray: this.state.displaygroupArray,
			checkall: value
		});
		tempObj[this.props.kit] = [...kititems];
		this.props.updateFormState(this.props.form, tempObj);
		setTimeout(() => {
			this.calculateRate();
		}, 0);
	}

	calculateRate() {
		let tempObj = {};
		tempObj[`${this.props.itemstr}.addonrate`] = 0;
		tempObj[`${this.props.itemstr}.rate`] = 0;
		let kititems = this.props.resource[this.props.kit];
		for (var i = 0; i < kititems.length; i++) {
			if(kititems[i].rootindex == this.state.selectedItem.index) {
				tempObj[`${this.props.itemstr}.addonrate`] += (kititems[i].rate * kititems[i].conversion);
			}
		}

		tempObj[`${this.props.itemstr}.rate`] = tempObj[`${this.props.itemstr}.addonrate`] + (this.state.selectedItem.baseitemrate || 0);
		this.props.updateFormState(this.props.form, tempObj);
		this.props.computeFinalRate();
	}

	getSelectedItem(props) {
		let selectedItem = {};
		for(var i = 0; i < props.resource[props.child].length; i++) {
			if(props.resource[props.child][i].index == props.index)
				selectedItem = props.resource[props.child][i];
		}
		return selectedItem;
	}

	renderAddon(displaygroup) {
		return displaygroup.itemaddon.map((addonitem, seckey) => {
			return <tr key={seckey} className={`${addonitem.check ? 'itemcheck' : ''}`}>
				<td className="text-center">
					<input  type="checkbox" onChange={(e) => this.updateAddOnField(addonitem, 'check', e.target.checked)}  checked={addonitem.check} disabled={addonitem.ismandatoryoption} />
				</td>
				<td className="text-center">{addonitem.itemid_name}</td>
				<td className="text-center">
					<NumberElement className={`form-control ${addonitem.conversion > 0 ? '' : 'errorinput'}`} value={addonitem.conversion} onChange={(value) => this.updateAddOnField(addonitem, 'conversion', value)} />
				</td>
				<td className="text-center">{addonitem.uomid_name}</td>
				<td className="text-center">
					<NumberElement className={`form-control ${addonitem.addonitemrate >= 0 ? '' : 'errorinput'}`} value={addonitem.addonitemrate} onChange={(value) => this.updateAddOnField(addonitem, 'addonitemrate', value)} disabled={this.state.rateDisabled} />
				</td>
				{this.props.resource.currencyid != this.props.app.defaultCurrency && this.props.resource.currencyid ? <td className="text-center">
					<NumberElement className={`form-control ${addonitem.addonitemratelc >= 0 ? '' : 'errorinput'}`} value={addonitem.addonitemratelc} disabled />
				</td> : null}
				<td className="text-center">
					{addonitem.itemid_keepstock ? <button type='button' className='btn btn-sm gs-form-btn-primary' onClick={() => this.props.openStockDetails(addonitem)}><span className='fa fa-shopping-cart'></span></button> : null}
				</td>
			</tr>
		});
	}

	close() {
		this.props.callback();
		this.props.closeModal();
	}

	render() {
		let {selectedItem, displaygroupArray, itemaddonArray} = this.state;
		let buttondisabled = false;
		displaygroupArray.map((a) => {
			a.itemaddon.map((b) => {
				if(b.check) {
					if(!(b.conversion > 0) || !(b.addonitemrate >= 0)) {
						buttondisabled = true;
					}
				}
			});
		});

		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Please choose add-ons</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Item Name</label>
							<input type="text" className="form-control" value={selectedItem.itemid_name} disabled />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">{`Base Item Rate (${this.props.app.currency[this.props.resource.currencyid].symbol})`}</label>
							<NumberElement className="form-control" value={selectedItem.baseitemrate} disabled />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">{`Add On Rate (${this.props.app.currency[this.props.resource.currencyid].symbol})`}</label>
							<NumberElement className="form-control" value={selectedItem.addonrate} disabled />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">{`Total Rate (${this.props.app.currency[this.props.resource.currencyid].symbol})`}</label>
							<NumberElement className="form-control" value={selectedItem.rate} disabled />
						</div>
						{ selectedItem.itemid_addonhelptext ? <div className="col-sm-12">
							<div className="alert alert-info text-center">{selectedItem.itemid_addonhelptext}</div>
						</div> : null }
						<div className="col-md-12">
							{displaygroupArray.length > 0 ? <table className="table table-bordered tablefixed">
								<thead>
									<tr>
										<th className="text-center">
											<input  type="checkbox" onChange={(e) => this.checkAllAddOnField(e.target.checked)}  checked={this.state.checkall} />
										</th>
										<th className="text-center" style={{width:'35%'}}>Item Name</th>
										<th className="text-center" style={{width:'15%'}}>Quantity</th>
										<th className="text-center" style={{width:'15%'}}>UOM</th>
										<th className="text-center" style={{width:'20%'}}>{`Rate (${this.props.app.currency[this.props.resource.currencyid].symbol})`}</th>
										{this.props.resource.currencyid != this.props.app.defaultCurrency && this.props.resource.currencyid ? <th className="text-center" style={{width:'20%'}}>{`Rate (${this.props.app.currency[this.props.app.defaultCurrency].symbol})`}</th> : null}
										<th className="text-center" style={{width:'10%'}}></th>
									</tr>
								</thead>
								{displaygroupArray.map((displaygroup, key) => {
									return <tbody key={key}>
										<tr>
											<td colSpan="6"><b>{displaygroup.name}</b></td>
										</tr>
										{this.renderAddon(displaygroup)}
									</tbody>
								})}
							</table> : null}
							{itemaddonArray.length > 0 ? <table className="table table-bordered tablefixed">
								<thead>
									<tr>
										<th className="text-center">Item Name</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">UOM</th>
										<th className="text-center">Rate</th>
									</tr>
								</thead>
								<tbody>
									{itemaddonArray.map((addonitem, key) => {
										return <tr key={key}>
											<td className="text-center">{addonitem.itemid_name}</td>
											<td className="text-center">{addonitem.conversion}</td>
											<td className="text-center">{addonitem.uomid_name}</td>
											<td className="text-center">{addonitem.rate}</td>
										</tr>
									})}
								</tbody>
							</table> : null}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close} disabled={buttondisabled}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
