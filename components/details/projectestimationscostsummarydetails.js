import React, { Component } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';

import {currencyFilter} from '../../utils/filter';
import ProjectEstimationCategorywiseCostDetailsModal from './projectestimationcategorywisecostdetailsmodal';

export const ProjectEstimationCostSummaryDetails = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			totalboqcost: 0,
			totaloverheadcost: 0,
			boqobj: {
				materialcost: 0,
				subcontractcost: 0,
				labourcost: 0
			},
			overheadobj: {
				materialcost: 0,
				subcontractcost: 0,
				labourcost: 0
			},
		};
		this.onLoad = this.onLoad.bind(this);
		this.showCostdetails = this.showCostdetails.bind(this);
	}

	componentWillMount() {
		if(this.props.resource && this.props.resource.projectestimationitems && this.props.resource.projectestimationitems.length > 0)
			this.onLoad();
	}

	componentWillReceiveProps(nextProps) {

		if(nextProps.resource.estimationitems.length >0){
			let totalboqcost = 0, totaloverheadcost = 0;
			let boqobj = {
				materialcost: 0,
				subcontractcost: 0,
				labourcost: 0
			};
			let overheadobj = {
				materialcost: 0,
				subcontractcost: 0,
				labourcost: 0
			};
			let itemprop = this.props.resource.projectquoteid> 0 ? 'boqquoteitemsid' : 'boqitemsid';
			nextProps.resource.estimationitems.forEach((item, index) => {
				if(item[itemprop] && (!item.estimationtype || item.estimationtype != 'Detailed')) {

					if(item.itemid_itemtype == 'Product')
						boqobj.materialcost += item.amount || 0;

					if(((item.itemid_itemtype == 'Service' && !item.itemid_isuseforinternallabour) || item.itemid_itemtype == 'Project'))
						boqobj.subcontractcost += item.amount || 0;

					if(item.itemid_itemtype == 'Service' && item.itemid_isuseforinternallabour)
						boqobj.labourcost += item.amount;

					totalboqcost += item.amount || 0;
				}
				if(!item[itemprop]) {
					if(item.itemid_itemtype == 'Product')
						overheadobj.materialcost += item.amount;

					if((item.itemid_itemtype == 'Service' && !item.itemid_isuseforinternallabour) || item.itemid_itemtype == 'Project')
						overheadobj.subcontractcost += item.amount;

					if(item.itemid_itemtype == 'Service' && item.itemid_isuseforinternallabour)
						overheadobj.labourcost += item.amount;

					totaloverheadcost += item.amount || 0;
				}
			});
			this.setState({
				totalboqcost : totalboqcost,
				totaloverheadcost : totaloverheadcost,
				boqobj : boqobj,
				overheadobj : overheadobj
			});
		}
	}

	onLoad() {
		let { totalboqcost, totaloverheadcost, boqobj, overheadobj } = this.state;
		let itemprop = this.props.resource.projectquoteid> 0 ? 'boqquoteitemsid' : 'boqitemsid';

		this.props.resource.projectestimationitems.forEach((item) => {
			if(item[itemprop] && (!item.estimationtype || item.estimationtype != 'Detailed' && item[itemprop])) {
				if(item.itemid_itemtype == 'Product')
					boqobj.materialcost += item.amount || 0;

				if(((item.itemid_itemtype == 'Service' && !item.itemid_isuseforinternallabour) || item.itemid_itemtype == 'Project'))
					boqobj.subcontractcost += item.amount || 0;

				if(item.itemid_itemtype == 'Service' && item.itemid_isuseforinternallabour)
					boqobj.labourcost += item.amount;

				totalboqcost += item.amount || 0;
			}

			if(!item[itemprop]) {
				if(item.itemid_itemtype == 'Product')
					overheadobj.materialcost += item.amount;

				if((item.itemid_itemtype == 'Service' && !item.itemid_isuseforinternallabour) || item.itemid_itemtype == 'Project')
					overheadobj.subcontractcost += item.amount;

				if(item.itemid_itemtype == 'Service' && item.itemid_isuseforinternallabour)
					overheadobj.labourcost += item.amount;

				totaloverheadcost += item.amount;
			}
		});
		this.setState({
			totalboqcost,
			totaloverheadcost,
			boqobj,
			overheadobj
		});
	}

	showCostdetails(param, overhead) {
		return this.props.openModal({
			render: (closeModal) => {
				return <ProjectEstimationCategorywiseCostDetailsModal
						resource = {this.props.resource} 
						param = {param} 
						overhead = {overhead} 
						app = {this.props.app} 
						closeModal = {closeModal} />
			}, className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			}
		});	
	}

	render() {
		if(!this.props.resource)
			return null;
		return (
			<div className="col-md-12 col-sm-12">
				<table className="table table-bordered">
					<thead>
						<tr>
							<th className="text-center">
								<div><a onClick={() => this.showCostdetails()}>BOQ Cost</a></div>
								<div>{currencyFilter(this.state.totalboqcost, this.props.resource.currencyid, this.props.app)}</div>
							</th>
							<th className="text-center">
								<div><a onClick={() => this.showCostdetails(null, true)}>Overhead Cost</a></div>
								<div>{currencyFilter(this.state.totaloverheadcost, this.props.resource.currencyid, this.props.app)}</div>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<table border="0" style={{width: '100%', border: 'none'}}>
									<tbody>
										<tr>
											<td><a onClick={() => this.showCostdetails('material', false)}>Material Cost</a></td>
											<td className="text-right">{currencyFilter(this.state.boqobj.materialcost, this.props.resource.currencyid, this.props.app)}</td>
										</tr>
										<tr>
											<td><a onClick={() => this.showCostdetails('subcontract', false)}>Sub Contract Cost</a></td>
											<td className="text-right">{currencyFilter(this.state.boqobj.subcontractcost, this.props.resource.currencyid, this.props.app)}</td>
										</tr>
										<tr>
											<td><a onClick={() => this.showCostdetails('labour', false)}>Internal Labour Cost</a></td>
											<td className="text-right">{currencyFilter(this.state.boqobj.labourcost, this.props.resource.currencyid, this.props.app)}</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td>
								<table border="0" style={{width: '100%', border: 'none'}}>
									<tbody>
										<tr>
											<td><a onClick={() => this.showCostdetails('material', true)}>Material Cost</a></td>
											<td className="text-right">{currencyFilter(this.state.overheadobj.materialcost, this.props.resource.currencyid, this.props.app)}</td>
										</tr>
										<tr>
											<td><a onClick={() => this.showCostdetails('subcontract', true)}>Sub Contract Cost</a></td>
											<td className="text-right">{currencyFilter(this.state.overheadobj.subcontractcost, this.props.resource.currencyid, this.props.app)}</td>
										</tr>
										<tr>
											<td><a onClick={() => this.showCostdetails('labour', true)}>Internal Labour Cost</a></td>
											<td className="text-right">{currencyFilter(this.state.overheadobj.labourcost, this.props.resource.currencyid, this.props.app)}</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
});
