import React, { Component } from 'react';
import ReactGantt from 'gantt-for-react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { updateFormState } from '../../actions/actions';
import { commonMethods, modalService } from '../../utils/services';
import Loadingcontainer from '../../components/loadingcontainer';
import { numberNewValidation, dateNewValidation } from '../../utils/utils';
import { dateFilter, datetimeFilter } from '../../utils/filter';

class ProjectSchedules extends Component {
	constructor(props) {
		super(props);

		this.state = {
			scrollOffsets: {'Month': 1},
			loaderflag : false
		};

		this.openActivityModal = this.openActivityModal.bind(this);
		this.updateItem = this.updateItem.bind(this);
	}

	updateItem (valueObj) {
		let itemFound = false,
			projectschedules = [...this.props.resource.projectschedules];

		for (let i = 0; i < projectschedules.length; i++) {
			if (valueObj.id == projectschedules[i].id) {
				projectschedules[i] = valueObj;
				itemFound = true;
			}
		}

		if(!itemFound)
			projectschedules.push(valueObj);

		this.props.updateFormState(this.props.form, {
			projectschedules
		});
	}

	openActivityModal(item) {
		if(item.transactionid > 0)
			this.props.createOrEdit('/details/projectschedules/:id', item.transactionid, {}, (valueObj) => {this.updateItem(valueObj);});
		else
			this.props.createOrEdit('/createProjectSchedules', null, item, (valueObj) => {this.updateItem(valueObj);});
	}

	render() {
		if(!this.props.resource)
			return null;

		let tasks = [];
		this.props.resource.projectschedules.forEach((item) => {
			tasks.push({
				name : item.activity,
				transactionid : item.id,
				id : `${item.id}`,
				parentid : item.parentid,
				parentresource : item.parentresource,
				modified : item.modified,
				progress : item.percentcomplete ? item.percentcomplete : 0,
				percentcomplete : item.percentcomplete,
				startdate : item.startdate,
				enddate : item.enddate,
				activity : item.activity,
				start : new Date(item.startdate).toISOString(),
				end : new Date(item.enddate).toISOString()
			});
		});

		return (
			<div className="row col-md-12 col-sm-12 col-xs-12">
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row col-md-12 col-sm-12 col-xs-12">
					<div className='col-md-12 col-sm-12 col-xs-12'>
						<button type="button" onClick={() => {
								this.openActivityModal({
									parentid : this.props.resource.id,
									parentresource : 'projects',
								})
							}} className="btn btn-width btn-sm gs-btn-outline-success" disabled={this.props.resource.status == 'Cancelled'}><i className="fa fa-plus"></i>Activity
						</button>
					</div>
					{ tasks.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12 margintop-10">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div style={{overflow: 'scroll', maxHeight: '250px'}}>
								<ReactGantt tasks={tasks} viewMode={'Day'} scrollOffsets={this.state.scrollOffsets} onClick={this.openActivityModal} onDateChange={() => this.setState({refresh: true})}  onProgressChange={() => this.setState({refresh: true})} />
							</div>
						</div>
					</div> : null}
				</div>
			</div>
		);
	}
}

export default ProjectSchedules;