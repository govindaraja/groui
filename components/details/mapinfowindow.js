import { dateFilter, currencyFilter, timeFilter, datetimeFilter, dateAgoFilter, timeDurationFilter } from '../../utils/filter';

export default function (item, param, rptparam, app) {
	let infocontent;

	if (rptparam == 'Employee Locations Report') {
		switch(param) {
			case 'employeeLocations': {
				infocontent = `<div style="font-family: Open Sans, sans-serif;"><b>${item.userid_displayname}</b></div>
					<div style="font-family: Open Sans, sans-serif;font-size:10px;">`;

				if (item.ishistory)
					infocontent += `<span style="color:gray;">${datetimeFilter(item.locationdate)}</span>`;
				else
					infocontent += `<span style="color:gray;">${dateAgoFilter(item.locationdate)}</span>`;

				infocontent += `</div>`;

				break;
			}
			case 'loginlatlngarray': {
				infocontent = `<div style="font-family: Open Sans, sans-serif;"><b>${item.userid_displayname}</b></div>
					<div style="font-family: Open Sans, sans-serif;font-size:10px;">
						<b>In-Time : </b>
						<span>${timeFilter(item.intime)}</span>
					</div>`;

				break;
			}
			case 'logoutlatlngarray': {
				infocontent = `<div style="font-family: Open Sans, sans-serif;"><b>${item.userid_displayname}</b></div>
					<div style="font-family: Open Sans, sans-serif;font-size:10px;">
						<b>Out-Time : </b>
						<span>${timeFilter(item.outtime)}</span>
					</div>`;

				break;
			}
			case 'customerLocations': {
				infocontent = `<div style="font-family: Open Sans, sans-serif;font-size:15px;font-weight:bold;">
					${item.name}
				</div>
				<div style="font-family: Open Sans, sans-serif;font-size:13px;">`;

				if(item.orderdate)
					infocontent += `<div>Last Order Date : ${dateFilter(item.orderdate)}</div>`;

				if(item.nextfollowup)
					infocontent += `<div>Last Meeting Date : ${dateFilter(item.nextfollowup)}</div>`;

				if(item.outstandingamount)
					infocontent += `<div>Due Amount : ${currencyFilter(item.outstandingamount, app.defaultCurrency, app)}</div>`;

				if(item.paymentduedate)
					infocontent += `<div>Payment Due Date : ${dateFilter(item.paymentduedate)}</div>`;


				infocontent += `</div>`;

				break;
			}
			default: {
				infocontent = `<div style="font-family: Open Sans, sans-serif;font-size:11px; color: #808080; margin-bottom: 5px;">
						${item.referencetype}
					</div>`;

				infocontent += `<div style="font-family: Open Sans, sans-serif;font-size:15px;font-weight:bold;margin-bottom: 5px;">
						${item.partnerid_name ? item.partnerid_name : ''}</div>`;

				if (item.referencename)
					infocontent += `<div style="font-family: Open Sans, sans-serif;font-size:13px;margin-bottom: 5px;">${item.referencename}</div>`;

				infocontent += `<div style="font-family: Open Sans, sans-serif;font-size:14px;">
					<div style="margin-bottom: 5px;">
						<span class="badge badge-pill gs-form-btn-success"><i class="fa fa-calendar-check-o marginright-5"></i>${timeFilter(item.startdatetime)}</span>`;

				if (item.enddatetime)
					infocontent += `<span class="marginleft-5 badge badge-pill gs-form-btn-danger"><i class="fa fa-calendar-times-o marginright-5"></i>${timeFilter(item.enddatetime)}</span>`;

				infocontent += `</div>`;

				infocontent += `<div><span class="marginleft-5 badge btn gs-form-btn-secondary">${timeDurationFilter(item.timespent, true)}</span></div>`;

				infocontent += `</div>`;

				break;
			}
	  	}
	} else {
		if (param == 'employeeLocations') {
			infocontent = `<div style="font-family: Open Sans, sans-serif;"><b>${item.userid_displayname}</b></div>
				<div style="font-family: Open Sans, sans-serif;font-size:10px;">`;

			if (new Date(item.locationdate).setHours(0,0,0,0) != new Date().setHours(0, 0, 0, 0))
				infocontent += `<span style="color:gray;">${datetimeFilter(item.locationdate)}</span>`;
			else
				infocontent += `<span style="color:gray;">${dateAgoFilter(item.locationdate)}</span>`;

			if (rptparam == 'servicecallallotmentreport')
				infocontent += `<div>Assigned Calls <span class="marginleft-5 badge badge-pill gs-form-btn-primary" style="font-size: 12px">${item.callcount}</span></div>`;

			infocontent += `</div>`;
		} else {
			switch(rptparam) {
				case 'leaditemsreport': {
					infocontent = `<div style="font-family: Open Sans, sans-serif;font-size:15px;font-weight:bold;">
							${item.customer}
							<span class="marginleft-5 badge btn gs-form-btn-secondary">${item.leadno}</span>
						</div>
						<div style="font-family: Open Sans, sans-serif;font-size:13px;">`;

					if (item.potentialrevenue)
						infocontent += `<div>${currencyFilter(item.potentialrevenue, app.defaultCurrency, app)}</div>`;

					infocontent += `<div>`;

					if (item.status)
						infocontent += `<span class="marginleft-5 badge badge-pill gs-form-btn-warning">${item.status}</span>`;

					if (item.salesperson)
						infocontent += `<span class="marginleft-5 badge badge-pill gs-form-btn-primary">${item.salesperson}</span>`;

					infocontent += `</div></div>`;

					break;
				}
				case 'servicecallallotmentreport': {
					infocontent = `<div style="font-family: Open Sans, sans-serif;font-size:15px;font-weight:bold;">
							${item.customerid_name}<span class="marginleft-5 badge btn gs-form-btn-secondary">${item.servicecallno}</span>
						</div>`;

					infocontent += `<div style="font-family: Open Sans, sans-serif;font-size:13px;">
						<div style="width: 200px;word-wrap: break-word;">${item.billingaddress}</div>
						<div>
							<span class="badge badge-pill gs-form-btn-primary"><i class="fa fa-calendar marginright-5"></i>${dateFilter(item.servicecalldate)}</span>
							<span class="marginleft-5 badge badge-pill gs-form-btn-warning"><i class="fa fa-wrench marginright-5"></i>${item.calltype}</span>`;

					if (item.equipmentcount > 0)
						infocontent += `<span class="marginleft-5 badge badge-pill gs-form-btn-success">${item.equipmentcount} Equipments</span>`;

					infocontent += `</div>`;

					if (item.status == 'Assigned')
						infocontent += `<div>Engineer : <span class="marginleft-5 badge badge-pill gs-form-btn-primary">${item.engineerid_name}</span></div>`;

					infocontent += `</div>`;

					break;
				}
				case 'deliveryrouteplannerreport': {
					infocontent = `<div style="font-family: Open Sans, sans-serif;font-size:15px;font-weight:bold;">
						${item.customerid_name ? item.customerid_name : ''}<span class="marginleft-5 badge btn gs-form-btn-secondary">${item.deliverynotenumber ? item.deliverynotenumber : ''}</span>
						</div>`;

					infocontent += `<div style="font-family: Open Sans, sans-serif;font-size:13px;">
						<div style="width: 200px;word-wrap: break-word;">${item.deliveryaddress}</div>`;

					infocontent += `<div>`;

					if(item.deliverynotedate)
						infocontent += `<span class="badge badge-pill gs-form-btn-primary"><i class="fa fa-calendar marginright-5"></i>${dateFilter(item.deliverynotedate)}</span>`;

					if(item.salesperson_displayname)
						infocontent += `<span class="marginleft-5 badge badge-pill gs-form-btn-success">${item.salesperson_displayname}</span>`;

					infocontent += `</div></div>`;

					break;
				}
				case 'collectionstatusreport': {
					infocontent = `<div style="font-family: Open Sans, sans-serif;font-size:15px;font-weight:bold;">
						${item.name}
					</div>
					<div style="font-family: Open Sans, sans-serif;font-size:13px;">
						<div>${currencyFilter(item.outstandingamount, app.defaultCurrency, app)}</div>`;

					if(item.collectionrep)
						infocontent += `<div>${item.collectionrep_displayname}</div>`;

					infocontent += `</div>`;

					break;
				}
				case 'contractitemsreport': {
					infocontent = `<div style="font-family: Open Sans, sans-serif;font-size:15px;font-weight:bold;">
							${item.customer}<span class="marginleft-5 badge btn gs-form-btn-secondary">${item.contractno}</span>
						</div>`;

					infocontent += `<div style="font-family: Open Sans, sans-serif;font-size:13px;">
						<div style="width: 200px;word-wrap: break-word;margin-bottom: 5px;">${item.installationaddress}</div>`;

					infocontent += `<div style="margin-bottom: 5px;">
							<span class="badge badge-pill gs-form-btn-success"><i class="fa fa-calendar-check-o marginright-5"></i>${dateFilter(item.startdate)}</span>
							<span class="marginleft-5 badge badge-pill gs-form-btn-danger"><i class="fa fa-calendar-times-o marginright-5"></i>${dateFilter(item.expiredate)}</span>
						</div>`;

					infocontent += `<div style="margin-bottom: 5px;">
						<span class="badge badge-pill gs-form-btn-warning"><i class="fa fa-wrench marginright-5"></i>${item.contractstatus}</span>
						</div>`;

					if (item.equipmentcount > 1)
						infocontent += `<div>
							<span class="badge badge-pill gs-form-btn-success">${item.equipmentcount} Equipments</span>
						</div>`;

					infocontent += `</div>`;

					break;
				}
		  	}
		}
	}

	return infocontent;
}