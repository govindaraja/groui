import React, { Component } from 'react';
import axios from 'axios';

export default class EmployeeLeaveBalanceDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showhidesection: false,
			currentleavetype: 0,
			leavebalancedetails: []
		};

		this.onLoad = this.onLoad.bind(this);
		this.showhideLeaveBalance = this.showhideLeaveBalance.bind(this);
		this.renderLeaveType = this.renderLeaveType.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let currentleavetype = 0,
			leavebalancedetails = []

		axios.get(`/api/query/getemployeeleavebalancequery?employeeid=${this.props.resource.employeeid}`).then((response)=> {
			if (response.data.message == 'success') {
				response.data.main.forEach((item) => {
					if(this.props.resource.leavetypeid == item.leavetypeid) {
						currentleavetype = item.noofdays;
					} else {
						leavebalancedetails.push(item);
					}
				});
				this.setState({ currentleavetype, leavebalancedetails });
			}
		});
	}

	showhideLeaveBalance() {
		this.setState({ showhidesection : !this.state.showhidesection });
	}

	renderLeaveType() {
		return (
			<div className="row">
				{this.state.leavebalancedetails.map((item, index) => {
					return (
						<div className="col-md-2 form-group text-center" key={index}>
							<div className="text-muted font-13 mb-2">{item.leavetypeid_name}</div>
							<div className="fontWeight font-18">{item.noofdays}</div>
						</div>
					)
				})}
			</div>
		);
	}

	render() {
		return (
			<div className="col-lg-12 col-md-12 col-sm-12">
				<div className="leavereq-leavebalance-card">
					<div className="leavereq-leavebalance-card-header">
						<div className="float-left">
							<span className="fa fa-info-circle text-primary mr-1"></span> {this.props.resource.employeeid_displayname} has <span className="text-danger">{this.state.currentleavetype} {this.props.resource.leavetypeid_name}</span> remaining</div>
						<div className="float-right">
							<div className="leavereq-leavebalance-anchor-tag" onClick={()=>this.showhideLeaveBalance()}>{!this.state.showhidesection ? 'View other leave balances' : 'Collapse'}</div>
						</div>
					</div>

					<div className={`leavereq-leavebalance-card-body ${this.state.showhidesection ? 'd-block' : 'd-none'}`}>
							{this.renderLeaveType()}
					</div>
				</div>
			</div>
		);
	}
}