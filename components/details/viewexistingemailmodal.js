import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { datetimeFilter } from '../../utils/filter';

export default class emailForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderFlag: false
		};
		this.onLoad = this.onLoad.bind(this);
		this.replyemail = this.replyemail.bind(this);
	}

	componentWillMount() {
		this.setState( {
			emaildetails: this.props.emailobj
		}, () => {
			this.onLoad();
		});
	}

	onLoad() {
		this.setState({loaderFlag: true});
        let tempData = {
            clientemailid: this.state.emaildetails.clientemailid,
            userid: this.props.app.user.id
        };

		axios({
			method : 'post',
			data : tempData,
			url : '/api/common/methods/getemailbody'
		}).then((response) => {
			if (response.data.message == 'success') {
				let { emaildetails } = this.state;
				emaildetails.msgbody = response.data.main;
				$('#existingemailbody').html(response.data.main);
				this.setState({	emaildetails });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.setState({loaderFlag: false});
		});
	}

	replyemail() {
		let { emaildetails } = this.state;
		emaildetails.subject = emaildetails.subject.indexOf('Re: ') == 0 ? emaildetails.subject : `Re: ${emaildetails.subject}`;

		if(emaildetails.boxname.toLowerCase().indexOf('inbox') > -1)
			emaildetails.tomail = emaildetails.frommail;

		emaildetails.inreplyto = emaildetails.msgid;
		emaildetails.inreplyto_subject = emaildetails.subject;
		this.props.callback(emaildetails);
		this.props.closeModal();
	}

	render() {
		let { emaildetails } = this.state;
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<div className="float-left w-100">
						<div className="float-left" style={{fontWeight: '600', fontSize: '18px'}}>{emaildetails.subject}</div>
						<div className="float-right">{datetimeFilter(emaildetails.emaileddate)}</div>
					</div>
					<div><b>From</b>: {emaildetails.frommail}</div>
					<div><b>To</b>: {emaildetails.tomail}</div>
					{emaildetails.cc ? <div><b>Cc</b>: {emaildetails.cc}</div> : null}
					{emaildetails.bcc ? <div><b>Bcc</b>: {emaildetails.bcc}</div> : null}
				</div>
				<div className="react-modal-body react-modal-body-scroll-view-email">
					<div className="row">
						{this.state.loaderFlag ? <div className="col-md-12 text-center">
							<div className="alert alert-warning" >Please wait... Preview is loading....</div>
						</div> : null}
						<div className="col-md-12" id="existingemailbody"></div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								{!this.props.ndtemail ? <button type="button" className="btn btn-sm gs-btn-success btn-width" disabled={this.state.loaderFlag || !emaildetails.msgbody} onClick={this.replyemail}><i className="fa fa-check"></i>Reply</button> : null}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}