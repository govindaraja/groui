import React, { Component } from 'react';
import axios from 'axios';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {};
		this.cancel = this.cancel.bind(this);
	};

	cancel(param) {
		this.props.closeModal();
		this.props.callback(param);
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-12 text-center">
							<img src="/images/nextactivity-icon.svg" />
						</div>
						<div className="col-md-12 form-group text-center"><span className="semi-bold">Create a follow-on activity?</span></div>
						<div className="col-md-12 text-center">
							<button type="button" className="btn btn-sm btn-width btn-secondary" onClick={()=>this.cancel(false)}>No</button>
							<button type="button" className="btn btn-sm btn-width gs-btn-success" onClick={()=>this.cancel(true)}>Yes</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}