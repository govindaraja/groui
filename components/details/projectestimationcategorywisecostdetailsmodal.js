import React, { Component } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';

import { currencyFilter, uomFilter } from '../../utils/filter';

class ProjectEstimationCategorywiseCostDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			materialitems: [],
			subcontractitems: [],
			labouritems: []
		};
		this.onLoad = this.onLoad.bind(this);
	}

	componentWillMount() {
		if(this.props.resource && this.props.resource.projectestimationitems && this.props.resource.projectestimationitems.length > 0)
			this.onLoad();
	}

	onLoad() {
		let materialitems = [], subcontractitems = [], labouritems = [];
		let itemprop = this.props.resource.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';

		this.props.resource.estimationitems.forEach((item) => {
			if(item[itemprop] && !this.props.overhead && (!item.estimationtype || item.estimationtype != 'Detailed')) {
				item.internalrefno = item.boqquoteitemsid ? item.boqquoteitemsid_internalrefno : item.boqitemsid_internalrefno;
				item.clientrefno = item.boqquoteitemsid ? item.boqquoteitemsid_clientrefno : item.boqitemsid_clientrefno;

				if(item.itemid_itemtype == 'Product')
					materialitems.push(item);

				if((item.itemid_itemtype == 'Service' && !item.itemid_isuseforinternallabour) || item.itemid_itemtype == 'Project')
					subcontractitems.push(item);

				if(item.itemid_itemtype == 'Service' && item.itemid_isuseforinternallabour)
					labouritems.push(item);
			}

			if(!item[itemprop] && this.props.overhead) {
				if(item.itemid_itemtype == 'Product')
					materialitems.push(item);

				if((item.itemid_itemtype == 'Service' && !item.itemid_isuseforinternallabour) || item.itemid_itemtype == 'Project')
					subcontractitems.push(item);

				if(item.itemid_itemtype == 'Service' && item.itemid_isuseforinternallabour)
					labouritems.push(item);
			}
		});

		if(this.props.param == 'material') {
			this.setState({
				materialitems
			});
		} else if(this.props.param == 'subcontract') {
			this.setState({
				subcontractitems
			});
		} else if(this.props.param == 'labour') {
			this.setState({
				labouritems
			});
		} else {
			this.setState({
				materialitems,
				subcontractitems,
				labouritems
			});
		}
	}

	render() {
		let estimationcategorytypeObj = {
			material: {
				displayName: 'Material',
				arrayName: 'materialitems'
			},
			subcontract: {
				displayName: 'Sub Contract',
				arrayName: 'subcontractitems'
			},
			labour: {
				displayName: 'Labour',
				arrayName: 'labouritems'
			}
		};

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">{`${this.props.overhead ? `Overhead ${estimationcategorytypeObj[this.props.param] ? estimationcategorytypeObj[this.props.param].displayName : ''} Cost Details` : `BOQ ${estimationcategorytypeObj[this.props.param] ? estimationcategorytypeObj[this.props.param].displayName : ''} Cost Details`}`}</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					{this.state.materialitems.length > 0 ? <div className="row">
						<div className="col-md-12 col-sm-12">
							<table className="table table-bordered table-sm">
								<thead>
									<tr>
										<th colSpan="6">Materials</th>
									</tr>
									<tr>
										{!this.props.overhead ? <th className="text-center" style={{width: '15%'}}>Ref No</th> : null}
										<th className="text-center"  style={{width: '25%'}}>Item Name</th>
										<th className="text-center"  style={{width: '30%'}}>Description</th>
										<th className="text-center"  style={{width: '10%'}}>Quantity</th>
										<th className="text-center">Estimated Cost</th>
									</tr>
								</thead>
								<tbody>
									{this.state.materialitems.map((item, index) => {
										return (
											<tr key={index}>
												{!this.props.overhead ? <td className="text-center">
													<span>{item.internalrefno}</span>
													{item.clientrefno ? <br></br> : null} 
													{item.clientrefno ? <span>({item.clientrefno})</span> : null}
												</td> : null}
												<td>{item.itemid_name}</td>
												<td><div className="text-ellipsis">{item.description}</div></td>
												<td className="text-center">{`${item.quantity} ${uomFilter(item.uomid, this.props.app.uomObj)}`}</td>
												<td className="text-right">{currencyFilter(item.amount, this.props.resource.currencyid, this.props.app)}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div> : null}
					{this.state.subcontractitems.length > 0 ? <div className="row">
						<div className="col-md-12 col-sm-12">
							<table className="table table-bordered table-sm">
								<thead>
									<tr>
										<th colSpan="6">Sub Contract</th>
									</tr>
									<tr>
										{!this.props.overhead ? <th className="text-center"  style={{width: '15%'}}>Ref No</th> : null}
										<th className="text-center" style={{width: '25%'}}>Item Name</th>
										<th className="text-center" style={{width: '30%'}}>Description</th>
										<th className="text-center" style={{width: '10%'}}>Quantity</th>
										<th className="text-center">Estimated Cost</th>
									</tr>
								</thead>
								<tbody>
									{this.state.subcontractitems.map((item, index) => {
										return (
											<tr key={index}>
												{!this.props.overhead ? <td className="text-center">
													<span>{item.internalrefno}</span>
													{item.clientrefno ? <br></br> : null} 
													{item.clientrefno ? <span>({item.clientrefno})</span> : null}
												</td> : null}
												<td>{item.itemid_name}</td>
												<td><div className="text-ellipsis">{item.description}</div></td>
												<td className="text-center">{`${item.quantity} ${uomFilter(item.uomid, this.props.app.uomObj)}`}</td>
												<td className="text-right">{currencyFilter(item.amount, this.props.resource.currencyid, this.props.app)}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div> : null}
					{this.state.labouritems.length > 0 ? <div className="row">
						<div className="col-md-12 col-sm-12">
							<table className="table table-bordered table-sm">
								<thead>
									<tr>
										<th colSpan="5">Internal Labour</th>
									</tr>
									<tr>
										{!this.props.overhead ? <th className="text-center"  style={{width: '15%'}}>Ref No</th> : null}
										<th className="text-center" style={{width: '25%'}}>Item Name</th>
										<th className="text-center" style={{width: '30%'}}>Description</th>
										<th className="text-center" style={{width: '10%'}}>Quantity</th>
										<th className="text-center">Estimated Cost</th>
									</tr>
								</thead>
								<tbody>
									{this.state.labouritems.map((item, index) => {
										return (
											<tr key={index}>
												{!this.props.overhead ? <td className="text-center">
													<span>{item.internalrefno}</span>
													{item.clientrefno ? <br></br> : null} 
													{item.clientrefno ? <span>({item.clientrefno})</span> : null}
												</td> : null}
												<td>{item.itemid_name}</td>
												<td><div className="text-ellipsis">{item.description}</div></td>
												<td className="text-center">{`${item.quantity} ${uomFilter(item.uomid, this.props.app.uomObj)}`}</td>
												<td className="text-right">{currencyFilter(item.amount, this.props.resource.currencyid, this.props.app)}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div> : null}
					{this.props.param && this.state[`${estimationcategorytypeObj[this.props.param].arrayName}`].length == 0 ? <div className="row">
						<div className="col-md-12 col-sm-12">
							<div className="alert alert-info text-center">{`There is no ${estimationcategorytypeObj[this.props.param].displayName} details`}</div>
						</div>
					</div> : null}
					{!this.props.param && this.state.materialitems.length == 0 && this.state.subcontractitems.length == 0 && this.state.labouritems.length == 0 ? <div className="row">
						<div className="col-md-12 col-sm-12">
							<div className="alert alert-info text-center">{`There is no ${this.props.overhead ? 'Overhead' : 'BOQ'} details`}</div>
						</div>
					</div> : null}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ProjectEstimationCategorywiseCostDetailsModal;
