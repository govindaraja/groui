import React, { Component } from 'react';
import {connect} from 'react-redux';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);

		this.renderThead = this.renderThead.bind(this);
		this.renderTbody = this.renderTbody.bind(this);
		this.renderSupplierQuote = this.renderSupplierQuote.bind(this);
		this.openSupplier = this.openSupplier.bind(this);
		this.close = this.close.bind(this);
	}

	renderThead() {
		return this.props.supplierquotes.map((head, index) => {
			return (
				<th key={index}>{head.partnerid_name}
					<br></br><a rel="tooltip" title={`Open ${head.supplierquoteno}`} onClick={()=>this.openSupplier(head)}>{head.supplierquoteno} </a>
				</th>
			);
		});
	}

	renderTbody() {
		return this.props.rfqitems.map((item, index) => {
			return (
				<tr key={index}>
					<td>{item.itemid_name}</td>
					<td style={{wordBreak: 'break-all'}}>{item.description}</td>
					<td>{item.quantity}&nbsp;&nbsp;{item.uomid_name}</td>
					{this.renderSupplierQuote(item)}
				</tr>
			);
		});
	}

	renderSupplierQuote(item) {
		return this.props.supplierquotes.map((quote, index) => {
			return (
				<td key={index}>
					{quote.supplierquotationitems[item.itemid] ? <span>{quote.supplierquotationitems[item.itemid].quantity} {quote.supplierquotationitems[item.itemid].uomid_name}
					<br></br>{quote.supplierquotationitems[item.itemid].amount/quote.supplierquotationitems[item.itemid].quantity}</span> : null}
				</td>
			);
		});
	}

	openSupplier(item) {
		this.props.closeModal();
		this.props.history.push(`/details/supplierquotation/${item.id}`);
	}

	close() {
		this.props.closeModal();
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Analyse Supplier Quotations</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th>Item</th>
										<th style={{width: '30%'}}>Description</th>
										<th>Quantity</th>
										{this.renderThead()}
									</tr>
								</thead>
								<tbody>
									{this.renderTbody()}
									<tr>
										<td colSpan="3">Final Amount</td>
										{this.props.supplierquotes.map((item, index)=> {
											return (
												<td key={index}>
													{item.finaltotal}
												</td>
											);
										})}
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
