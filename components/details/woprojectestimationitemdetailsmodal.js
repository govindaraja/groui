import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { currencyFilter, dateFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect, DateElement } from '../utilcomponents';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			includeoverhead : true,
			search: {},
			overheadArr : [],
			projectEstimationItemArray: [],
			showdescription: false
		};
		this.onLoad = this.onLoad.bind(this);
		this.overheadonChange = this.overheadonChange.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.addProjectEstimationItem = this.addProjectEstimationItem.bind(this);
		this.checkboxonChange = this.checkboxonChange.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
		this.contractorOnChange = this.contractorOnChange.bind(this);
		this.dateOnChange = this.dateOnChange.bind(this);
		this.quantityOnChange = this.quantityOnChange.bind(this);
		this.descToggleOnChange = this.descToggleOnChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let { projectEstimationItemArray } = this.state,
			filterString = [];

		if (this.props.resource.projectid)
			filterString.push(`projectid=${this.props.resource.projectid}`);
		
		filterString.push(`param=${this.props.param}`)

		axios.get(`/api/query/getworkorderprojectestimateitemsquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				projectEstimationItemArray = response.data.main;
				let overheadArr = [];

				for(var i=0;i<projectEstimationItemArray.length;i++) {
					if(!projectEstimationItemArray[i].boqitemsid)
						overheadArr.push(projectEstimationItemArray[i]);
				}

				for (var i = 0; i < this.props.resource[this.props.itemarray].length; i++) {
					for (var j = 0; j < projectEstimationItemArray.length; j++) {
						if (this.props.resource[this.props.itemarray][i].projectestimationitemsid == projectEstimationItemArray[j].projectestimationitemsid) {
							projectEstimationItemArray[j].checked = true;
							projectEstimationItemArray[j].quantity = this.props.resource[this.props.itemarray][i].quantity;
							projectEstimationItemArray[j].duedate = this.props.resource[this.props.itemarray][i].duedate;
							projectEstimationItemArray[j].dateofcompletion = this.props.resource[this.props.itemarray][i].dateofcompletion;
						}
					}
				}
				this.setState({ projectEstimationItemArray, overheadArr });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	inputonChange(value) {
		let { search } = this.state;
		search.itemid_name = value;
		this.setState({search});
	}

	checkboxonChange(item, value) {
		item.checked = value;
		this.setState({
			projectEstimationItemArray: this.state.projectEstimationItemArray
		});
	}

	checkallonChange(value) {
		let errorArr = [];
		let projectEstimationItemArray = [...this.state.projectEstimationItemArray];
		projectEstimationItemArray.forEach((item) => {
			item.checked = value;
		});
		if(errorArr.length > 0) {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : errorArr,
				btnArray : ['Ok']
			}));
		} else {
			this.setState({	projectEstimationItemArray : projectEstimationItemArray });
		}
	}

	addProjectEstimationItem () {
		let errorArr = [];
		let itemdetailsarr = [...this.props.resource[this.props.itemarray]];
		this.state.projectEstimationItemArray.forEach((item, index) => {
			if(item.checked) {
				if(!item.contractorid && this.props.itemarray == 'workprogressitems') {
					errorArr.push(`Please choose contractor in row ${index+1}`)
				}
				else if(item.quantity <= 0) {
					errorArr.push(`Quantity must be greater than 0 in row ${index+1}`)
				} else {
					itemdetailsarr.push(item);
				}
			}
		});
		if(errorArr.length > 0) {
			this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : errorArr,
				btnArray : ['Ok']
			}));
		} else if(itemdetailsarr.length > 0) {
			this.props.callback(itemdetailsarr);
			this.props.closeModal();
		} else {
			this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				body : 'Please choose atleast one item',
				btnArray : ['Ok']
			}));
		}
	}

	overheadonChange(checked) {
		this.state.includeoverhead = checked;
		if(checked) {
			this.state.overheadArr.forEach((item) => {
				this.state.projectEstimationItemArray.push(item);
			});
			this.setState({
				includeoverhead: this.state.includeoverhead,
				projectEstimationItemArray : this.state.projectEstimationItemArray
			});
		} else {
			let projectEstimationItemArray = [];
			this.state.projectEstimationItemArray.forEach((item) => {
				if(item.boqitemsid) {
					projectEstimationItemArray.push(item);
				}
			});
			this.setState({
				includeoverhead: this.state.includeoverhead,
				projectEstimationItemArray
			});
		}
	}

	contractorOnChange(value, valueobj, item) {
		item.contractorid = value;
		item.contractorid_displayname = valueobj.displayname;
		this.setState({
			projectEstimationItemArray: this.state.projectEstimationItemArray
		});	
	}

	dateOnChange(value, item) {
		item.duedate = value;
		item.dateofcompletion = value;
		this.setState({
			projectEstimationItemArray: this.state.projectEstimationItemArray
		});	
	}

	quantityOnChange(value, item) {
		item.quantity = value > 0 ? Number(value) : "";
		this.setState({
			projectEstimationItemArray: this.state.projectEstimationItemArray
		});
	}

	descToggleOnChange() {
		this.setState({showdescription: !this.state.showdescription});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Project Estimation Item Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-3 form-group">
							<input type="text" className="form-control" name="location" value={this.state.search.itemid_name || ''} placeholder="Search By Item Name" onChange={(evt) =>{this.inputonChange(evt.target.value)}}/>
						</div>
						<div className="col-md-4 form-group">
							<div>Include Overhead</div>
							<input type="checkbox" checked={this.state.includeoverhead} onChange={(evt) =>{this.overheadonChange(evt.target.checked)}}/>
						</div>
						<div className="form-group col-md-4 col-sm-4">
							<div>Show Description</div>
							<label className="gs-checkbox-switch">
								<input className="form-check-input" type="checkbox" checked={this.state.showdescription} onChange={() => this.descToggleOnChange()} />
								<span className="gs-checkbox-switch-slider"></span>
							</label>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th className="text-center" style={{width:'5%'}}><input type="checkbox" checked={this.state.checkall} onChange={(evt)=>{this.checkallonChange(evt.target.checked)}}/></th>
										<th className="text-center">BOQ Details</th>
										<th className="text-center">Item Name</th>
										<th className="text-center">Estimated Qty</th>
										<th className="text-center">UOM</th>
										<th className="text-center">So far WO Qty</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">Due Date</th>
										<th className="text-center">Remarks</th>
									</tr>
								</thead>
								<tbody>
									{search(this.state.projectEstimationItemArray, this.state.search).map((item, index) => {
										return (
											<tr key={index}>
												<td className="text-center"><input type="checkbox" onChange={(e) => this.checkboxonChange(item, e.target.checked)} checked={item.checked || false} /></td>
												<td>
													<div>
														<span className="text-muted">Internal Ref No </span>
														<span>: {item.internalrefno}</span>
													</div>
													<div>
														<span className="text-muted">Client Ref No </span>
														<span>: {item.clientrefno}</span>
													</div>
													<div>
														<span className="text-muted">BOQ Item Name </span>
														<span>: {item.boqitemid_name}</span>
													</div>
													{item.boqitemsid_description ? <div className={`${this.state.showdescription ? 'show' : 'hide'}`}>
														<span className="text-muted">BOQ Description </span>
														<span className="font-12">: {item.boqitemsid_description}</span>
													</div> : null}
												</td>
												<td>{item.itemid_name}<div className={`font-12 ${this.state.showdescription ? 'show' : 'hide'}`}>{item.description}</div></td>
												<td className="text-center">{item.estimatedqty}</td>

												<td className="text-center">{item.uomid_name}</td>
												<td className="text-center">{item.wocompletedqty}</td>
												<td className="text-center">
													<input type="number" className="form-control" value={item.quantity} min="1" onChange={(evt) => this.quantityOnChange(evt.target.value,item)} />
												</td>
												<td className="text-center">
													<DateElement className={`form-control`} value={item.duedate} onChange={(val) => this.dateOnChange(val, item)} />
												</td>
												<td>{item.remarks}</td>
											</tr>
										)
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addProjectEstimationItem}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
