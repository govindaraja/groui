import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService } from '../../utils/services';
import {getPageJson, getPageOptions, getFormName, checkPageCreateMode, getControllerName, checkAccess, checkActionVerbAccess, numberValidation, dateValidation, multiSelectValidation, requiredValidation, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation } from '../../utils/utils';
import { currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../../utils/filter';
import htmlToText from 'html-to-text';
import { diffChars, diffLines, diffSentences } from 'diff';
import Loadingcontainer from '../loadingcontainer';

const diffType = ['selectasync', 'selectauto', 'date', 'select', 'text','autosuggest', 'checkbox', 'textarea', 'number', 'rate', 'quantity', 'richtext', 'address','contact', 'discount', 'additionalcharges', 'repeatabletable', 'email', 'phone', 'datetime', 'displaygroup', 'time'];
const childType = ['additionalcharges', 'repeatabletable'];
const additionalChargesObj = {
	body:[{
		"title" : "Additional Charge",
		"model" : "additionalchargesid",
		"type" : "selectauto",
		"required": true
	}, {
		"title" : "Description",
		"model" : "description",
		"type" : "textarea",
		"required" : true
	}, {
		"title" : "Default Percentage",
		"model" : "defaultpercentage",
		"type" : "number"
	}, {
		"title" : "Amount",
		"model" : "amount",
		"type" : "number",
		"required" : true
	}, {
		"title" : "Tax",
		"model" : "taxcodeid",
		"type" : "select",
		"placeholder" : "Select Tax",
		"label" : "taxcode",
		"value" : "id",
		"localoptions" : "{[...app.salestaxarray, ...app.purchasetaxarray]}",
		"change" : "{controller.computeFinalRate}"
	}]
};
 
class AmendCompareModel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			amendvalue : null,
			loaderflag: false,
			pagejson: JSON.parse(JSON.stringify(this.props.pagejson))
		};

		this.onload = this.onload.bind(this);
	}
	
	componentWillMount() {
		this.onload();
	}
	
	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	onload () {
		this.updateLoaderFlag(true);
		let {amendvalue} = this.state;
 		axios.get(`/api/common/methods/getAmendments?resourcename=${this.props.resourcename}&id=${this.props.resource.id}`).then((response) => {
			if (response.data.message == 'success') {
				axios.get(`/api/common/methods/getAmendments?resourcename=${this.props.resourcename}&versionid=${response.data.main[0].id}`).then((response) => {
					if (response.data.message == 'success') {
						this.changeDataBasedOnResource(response.data.main);
 						this.setState({
							amendvalue : {...response.data.main}
						}, () => {
							axios.get(`/api/${this.props.resourcename}/${this.props.resource.id}`).then((secresponse) => {
								if (secresponse.data.message == 'success') {
									this.changeDataBasedOnResource(secresponse.data.main);
									this.setState({
										currentresource : {...secresponse.data.main}
									}, () => {
										this.generateDiff();
									});
								} else {
									this.updateLoaderFlag(false);
									let apiResponse = commonMethods.apiResult(secresponse);
									this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
								}
							});
						});
					} else {
						let apiResponse = commonMethods.apiResult(response);
						this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
					}
				});
			} else {
				this.updateLoaderFlag(false);
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	generateDiff() {
		let sectionArr = [];
		this.state.pagejson.sections.forEach((section, index) => {
			let changeFields = this.getChangeFields(section.fields);
			if(changeFields.length == 0) {
				return null;
			}
			sectionArr.push ({
				title: section.title,
				changeFields: changeFields
			});
		});
		this.setState({
			sectionArr: sectionArr
		});
		this.updateLoaderFlag(false);
	}

	changeDataBasedOnResource(resourceObj) {
		if(this.props.resourcename == 'estimations') {
			resourceObj.componentitems = [];
			resourceObj.serviceitems = [];
			resourceObj.expenseitems = [];

			resourceObj.estimationitems.forEach((item) => {
				if(item.itemid_itemtype == 'Product')
					resourceObj.componentitems.push(item);
				if(item.itemid_itemtype == 'Service')
					resourceObj.serviceitems.push(item);
				if(item.itemid_itemtype == 'Expense')
					resourceObj.expenseitems.push(item);
			});
		}
		if(this.props.resourcename == 'projectestimations') {
			resourceObj.estimationitems = [];
			resourceObj.overheadobj = {
				productitems: [],
				subcontractitems: [],
				labouritems: []
			};

			let itemprop = resourceObj.projectquoteid > 0 ? 'boqquoteitemsid' : 'boqitemsid';
			let uniqueItemsObj = {};

			resourceObj.projectestimationitems.forEach((item) => {
				if(item[itemprop] > 0) {
					if(uniqueItemsObj[item[itemprop]])
						uniqueItemsObj[item[itemprop]].push(item);
					else
						uniqueItemsObj[item[itemprop]] = [item];
				} else {
					if(item.itemid_itemtype == 'Product')
						resourceObj.overheadobj.productitems.push(item);
					else if(item.itemid_itemtype == 'Project' || item.itemid_itemtype == 'Service')
						resourceObj.overheadobj.subcontractitems.push(item);
					else if(item.activitytypeid)
						resourceObj.overheadobj.labouritems.push(item);
				}
			});

			for(var prop in uniqueItemsObj) {
				let tempChildobj;
				if(uniqueItemsObj[prop].length > 1) {
					tempChildobj = {
						...uniqueItemsObj[prop][0],
						estimationtype : 'Detailed',
						productitems: [],
						subcontractitems: [],
						labouritems: []
					};
				} else {
					tempChildobj = {
						...uniqueItemsObj[prop][0],
						estimationtype : uniqueItemsObj[prop][0].itemid != uniqueItemsObj[prop][0].boqitemid ? 'Detailed' : 'Simple',
						productitems: [],
						subcontractitems: [],
						labouritems: []
					};
				}
				if(tempChildobj.estimationtype == 'Detailed') {
					uniqueItemsObj[prop].forEach((propitem) => {
						if(propitem.itemid_itemtype == 'Product')
							tempChildobj.productitems.push(propitem);
						else if(propitem.itemid_itemtype == 'Project' || propitem.itemid_itemtype == 'Service')
							tempChildobj.subcontractitems.push(propitem);
						else if(propitem.activitytypeid)
							tempChildobj.labouritems.push(propitem);
					});
				}
				resourceObj.estimationitems.push(tempChildobj);
			}

			resourceObj.estimationitems.forEach((item) => {
				let itemqty = item.boqitemsid ? item.boqitemsid_estimatedqty : item.boqquoteitemsid_estimatedqty;
				let tempamount = 0;
				if(item.estimationtype == 'Detailed') {
					item.productitems.forEach((productitem) => {
						tempamount += Number(productitem.amount);
					});
					item.subcontractitems.forEach((subcontractitem) => {
						tempamount += Number(subcontractitem.amount);
					});
					item.labouritems.forEach((labouritem) => {
						tempamount += Number(labouritem.amount);
					});
					let temprate =  Number((tempamount / itemqty).toFixed(this.props.app.roundOffPrecision));
					if(item.usebillinguom) {
						item.billingrate = temprate;
						item.rate = Number(((item.billingrate / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
					} else {
						item.rate = temprate;
					}
					item.amount = Number((item.rate * itemqty).toFixed(this.props.app.roundOffPrecision));
				}
			});
		}
	}
	
	getValue(isOld, model, childvalue) {
		let resource = childvalue ? childvalue : (isOld ? this.state.amendvalue : this.state.currentresource);
		
		let modelname = `resource.${model}`;
		
		return eval (`try{${modelname}}catch(e){}`);
	}

	getValueChild(getvaluetype, field, childvalue) {
		let childName;
		if(field.type == 'additionalcharges')
			childName = field.type
		else
			childName = field.json

		return this.getValue(getvaluetype, childName, childvalue);
	}

	getValueAddress(getvaluetype, field, childvalue) {
		let select_address = this.props.checkCondition('value', field.model);
		return this.getValue(getvaluetype, select_address[1], childvalue);
	}
		
	getValueContact(getvaluetype, field, childvalue) {
		let select_contact = this.props.checkCondition('value', field.model);

		let tempvalue = ``;
		let tempconname = this.getValue(getvaluetype, select_contact[1], childvalue);
		let tempconmobile = this.getValue(getvaluetype, select_contact[2], childvalue);
		let tempconphone = this.getValue(getvaluetype, select_contact[3], childvalue);
		let tempconmemail = this.getValue(getvaluetype, select_contact[4], childvalue);
		tempvalue += `Name: ${tempconname ? tempconname : ''}\n`;
		tempvalue += `Mobile: ${tempconmobile ? tempconmobile : ''}\n`;
		tempvalue += `Phone: ${tempconphone ? tempconphone : ''}\n`;
		tempvalue += `Email: ${tempconmemail ? tempconmemail : ''}`;

		return tempvalue;
	}

	getValueDiscount(getvaluetype, field, childvalue) {
		let select_discount = ['discountmode', 'discountquantity'];

		let tempvalue = ``;
		let tempdiscountmode = this.getValue(getvaluetype, select_discount[0], childvalue);
		let tempdiscountquantity = this.getValue(getvaluetype, select_discount[1], childvalue);
		let currencyid = this.getValue(getvaluetype, 'currencyid', childvalue)

		if(tempdiscountquantity !== null) {
			if(tempdiscountmode == 'Percentage')
				tempvalue += `${currencyFilter(tempdiscountquantity, 'qty', this.props.app)} %`;
			else
				tempvalue += `${currencyFilter(tempdiscountquantity, currencyid, this.props.app)}`;

			return tempvalue;
		}
		return tempvalue;
	}

	getValueModel(field, type, childvalue) {
		let getvaluetype = type == 'oldValue' ? true : false;

		if(field.type == 'address')
			return this.getValueAddress(getvaluetype, field, childvalue);

		if(field.type == 'contact')
			return this.getValueContact(getvaluetype, field, childvalue);

		if(field.type == 'discount')
			return this.getValueDiscount(getvaluetype, field, childvalue);

		if(childType.includes(field.type))
			return this.getValueChild(getvaluetype, field, childvalue);

		return this.getValue(getvaluetype, field.model, childvalue);
	}

	getValue_DN(field, type, tempfieldObj, childvalue) {
		let dnvalue = null;
		let getvaluetype = type == 'oldValue' ? true : false;

		if(field.type == 'date')
			dnvalue = dateFilter(tempfieldObj[type]);

		if(field.type == 'datetime')
			dnvalue = datetimeFilter(tempfieldObj[type]);

		if(field.type == 'time')
			dnvalue = timeFilter(tempfieldObj[type]);

		if(field.type == 'checkbox')
			dnvalue = booleanfilter(tempfieldObj[type]);

		if(field.type == 'quantity' || field.type == 'rate' ||  field.type == 'number')
			dnvalue = currencyFilter(tempfieldObj[type], 'qty', this.props.app);

		if(field.type == 'checkbox')
			dnvalue = booleanfilter(tempfieldObj[type]);

		if(field.type == 'selectasync' || field.type == 'selectauto')
			dnvalue = this.getValue(getvaluetype,`${field.model}_${field.displaylabel ? field.displaylabel : (field.label ? field.label : 'name')}`, childvalue);

		if(field.type == 'select') {
			let select_localOptions = this.props.checkCondition('value', field.localoptions);
			let select_labelkey = field.label ? field.label : 'name';
			let select_valuekey = field.value ? field.value : 'id';
			if(select_localOptions && select_localOptions.length > 0) {
				let select_localOptionsObj = {};

				if(typeof(select_localOptions[0]) == 'string') {
					select_localOptions.forEach((optitem) => {
						select_localOptionsObj[optitem] = optitem;
					});
				} else {
					select_localOptions.forEach((optitem) => {
						select_localOptionsObj[optitem[select_valuekey]] = optitem[select_labelkey];
					});
				}
				if(field.multiselect == true) {
					dnvalue = tempfieldObj[type].map((eachvalue)=> {
						return select_localOptionsObj[eachvalue] ? select_localOptionsObj[eachvalue] : null;
					}).join(', ');
				} else {
					dnvalue = select_localOptionsObj[tempfieldObj[type]] ? select_localOptionsObj[tempfieldObj[type]] : null;
				}
			}
		}
		if(field.type == 'richtext') {
			let htmltext = htmlToText.fromString(tempfieldObj[type], {});
			dnvalue = htmltext;
		}

		tempfieldObj[`${type}_dn`] = dnvalue !== null ? dnvalue : tempfieldObj[`${type}_dn`];
	}

	getChangeFields(fields) {
		let changeFields = [];

		fields.forEach((field,index)=> {
						
			if(!diffType.includes(field.type))
				return null;

			let oldValue = this.getValueModel(field, 'oldValue');
			let currentValue = this.getValueModel(field, 'currentValue');

			if(childType.includes(field.type)) {
				let childDiff = this.getChildDiff(field, oldValue, currentValue);
				if(childDiff) {
					let tempChifieldObj = {
						fieldObj : field,
						diff: childDiff
					};
					changeFields.push(tempChifieldObj);
				}
				return null;
			}
			
			if(this.checkValueChanged(oldValue, currentValue, field))
				return null;
		
			let display_title = this.props.checkCondition('value', field.title);
			let tempfieldObj = {
				display_title: display_title,
				fieldObj : field,
				oldValue : oldValue,
				currentValue : currentValue,
				oldValue_dn: oldValue,
				currentValue_dn: currentValue
			};

			if(this.checkValueIsNull(tempfieldObj.oldValue, field))
				tempfieldObj.oldValue_dn = '-';
			else
				this.getValue_DN(field, 'oldValue', tempfieldObj);

			if(this.checkValueIsNull(tempfieldObj.currentValue, field))
				tempfieldObj.currentValue_dn = '-';
			else
				this.getValue_DN(field, 'currentValue', tempfieldObj);

			changeFields.push(tempfieldObj);
		});
		return changeFields;		
	}

	checkValueChanged(oldValue, currentValue, field) {
		if(field.type == 'select' && field.multiselect) {
			let oldTempValue = oldValue ? oldValue : [];
			let currentTempValue = currentValue ? currentValue : [];

			if(oldTempValue.sort().join(',') == currentTempValue.sort().join(','))
				return true;

			return false;
		}
		if(field.type == 'date') {
			let oldTempValue = oldValue ? dateFilter(oldValue) : '';
			let currentTempValue = currentValue ? dateFilter(currentValue) : '';

			if(oldTempValue == currentTempValue)
				return true;

			return false;
		}
		if(field.type == 'datetime') {
			let oldTempValue = oldValue ? datetimeFilter(oldValue) : '';
			let currentTempValue = currentValue ? datetimeFilter(currentValue) : '';

			if(oldTempValue == currentTempValue)
				return true;

			return false;
		}
		if(field.type == 'time') {
			let oldTempValue = oldValue ? timeFilter(oldValue) : '';
			let currentTempValue = currentValue ? timeFilter(currentValue) : '';

			if(oldTempValue == currentTempValue)
				return true;

			return false;
		}

		let oldTempValue = oldValue === '' ? null : oldValue;
		let currentTempValue = currentValue === '' ? null : currentValue;

		return oldTempValue == currentTempValue;
	}

	checkValueIsNull(value, field) {
		if(field.type == 'select' && field.multiselect) {
			let tempValue = value ? value : [];
			
			if(tempValue.length == 0)
				return true;

			return false;
		}

		return value === null || value === undefined || value === '';
	}

	getChildDiff(field, oldValue, currentValue) {
		let childJSON;
		if(field.type == 'additionalcharges')
			childJSON = JSON.parse(JSON.stringify(additionalChargesObj));
		else
			childJSON = JSON.parse(JSON.stringify(this.state.pagejson[field.json]));

		oldValue = oldValue ? oldValue : [];
		currentValue = currentValue ? currentValue : [];

		let changesArray = [];

		let ovObj = {};
		oldValue.forEach(ov => {
			ovObj[ov.amendparentid] = ov;
		});

		let cvObj = {};
		currentValue.forEach(cv => {
			cvObj[cv.id] = cv;
			if(ovObj[cv.id]) {
				let childChangedFields = this.getChildChangeFields(childJSON, 'change', ovObj[cv.id], cv);

				let filterChangedFilter = childChangedFields.filter(a => a.changed);

				if(filterChangedFilter.length > 0) {
					changesArray.push({
						param: 'change',
						display_param: 'Changed',
						c_value: cv,
						o_value: ovObj[cv.id],
						childChangedFields: childChangedFields
					});
				}
			} else {
				changesArray.push({
					param: 'add',
					display_param: 'Added',
					c_value: cv,
					o_value: null,
					childChangedFields: this.getChildChangeFields(childJSON, 'add', null, cv)
				});
			}
		});

		oldValue.forEach(ov => {
			if(!cvObj[ov.amendparentid])
				changesArray.push({
					param: 'delete',
					display_param: 'Removed',
					c_value: null,
					o_value: ov,
					childChangedFields: this.getChildChangeFields(childJSON, 'delete', ov, null)
				});
		});

		changesArray.forEach(changeditem => {
			changeditem.displayorder = changeditem.c_value ? changeditem.c_value.displayorder : changeditem.o_value.displayorder
		});

		changesArray.sort((a, b) => {
			return a.displayorder - b.displayorder;
		});

		if(changesArray.length > 0)
			return changesArray;
		else
			return null;
	}

	getChildChangeFields(childJSON, childparam, o_value, c_value) {
		let changeFields = [];

		childJSON.body.forEach((field,index)=> {

			if(!diffType.includes(field.type))
				return null;

			if(['ratelc', 'amountlc'].includes(field.model) && this.state.amendvalue.currencyid == this.props.app.defaultCurrency && this.state.currentresource.currencyid == this.props.app.defaultCurrency)
				return null;

			let oldValue = null, currentValue = null;

			if(['delete', 'change'].includes(childparam))
				oldValue = this.getValueModel(field, 'oldValue', o_value);

			if(['add', 'change'].includes(childparam))
				currentValue = this.getValueModel(field, 'currentValue', c_value);

			let display_title = this.props.checkCondition('value', field.title);
			let tempfieldObj = {
				display_title: display_title,
				fieldObj : field,
				changed: !this.checkValueChanged(oldValue, currentValue, field),
				oldValue : oldValue,
				currentValue : currentValue,
				oldValue_dn: oldValue,
				currentValue_dn: currentValue
			};

			if(this.checkValueIsNull(tempfieldObj.oldValue, field))
				tempfieldObj.oldValue_dn = '-';
			else
				this.getValue_DN(field, 'oldValue', tempfieldObj, o_value);

			if(this.checkValueIsNull(tempfieldObj.currentValue, field))
				tempfieldObj.currentValue_dn = '-';
			else
				this.getValue_DN(field, 'currentValue', tempfieldObj, c_value);

			changeFields.push(tempfieldObj);
		});
		return changeFields;
	}
	
	renderSection() {
		/*let sectionArr = [];
		this.state.pagejson.sections.forEach((section, index) => {
			let changeFields = this.getChangeFields(section.fields);
			if(changeFields.length == 0) {
				return null;
			}
			sectionArr.push (
				<div key={index}>
					<div className="card-header gs-amend-section-title">{section.title}</div>
					<ul className="list-group list-group-flush">
						{changeFields.map((field, fieldindex) => <li className="list-group-item" key={fieldindex} style={{padding:'8px 20px 8px 20px'}}>{this.renderDiff(field)}</li>)}
					</ul>
				</div>				
			)								
		});
		return sectionArr;*/
		return this.state.sectionArr.map((section, index) => {
			return (
				<div key={index}>
					<div className="card-header gs-amend-section-title">{section.title}</div>
					<ul className="list-group list-group-flush">
						{section.changeFields.map((field, fieldindex) => <li className="list-group-item" key={fieldindex} style={{padding:'8px 20px 8px 20px'}}>{this.renderDiff(field)}</li>)}
					</ul>
				</div>
			);
		});
	}

	renderNoChanges() {
		return (
			<div>
				<div className="col-md-12 col-sm-12 col-xs-12 alert alert-success text-center">
						No version changes found
				</div>
			</div>
		);
	}

	renderDiff(field, childParam, childParamType) {

		if(childType.includes(field.fieldObj.type)) {
			return this.renderChildDiff(field);
		}

		if(field.fieldObj.type == 'richtext') {
			if(childParam)
				return this.renderRichtext(field, childParam, childParamType);

			return (
				<div className="row">
					<div className="col-md-12">{this.renderRichtext(field)}</div>
				</div>
			);
		}

		if(field.fieldObj.type == 'address' || field.fieldObj.type == 'contact' || field.fieldObj.type == 'textarea') {
			if(childParam) {
				return (
					<div className = "flex">
						{field.changed && ['delete', 'change'].includes(childParamType) ? <div className="gs-compare-red">
						{field.oldValue_dn.split('\n').map((splitpart, splitindex) => <span key={splitindex}>{splitpart}{splitindex < (field.oldValue_dn.split('\n').length - 1) ? <br /> : null}</span>)}
						</div> : null}
						{['add', 'change'].includes(childParamType) ? <div className={`gs-compare-${field.changed ? 'green' : 'black'}`}>{field.currentValue_dn.split('\n').map((splitpart, splitindex) => <span key={splitindex}>{splitpart}{splitindex < (field.currentValue_dn.split('\n').length - 1) ? <br /> : null}</span>)}</div> : null}
					</div>
				);
			}
			return (
				<div className = "row">
					<div className="col-md-4">{field.display_title} </div>
					<div className="col-md-4 gs-compare-red">
						{field.oldValue_dn.split('\n').map((splitpart, splitindex) => <span key={splitindex}>{splitpart}{splitindex < (field.oldValue_dn.split('\n').length - 1) ? <br /> : null}</span>)}
					</div>
					<div className="col-md-4 gs-compare-green">{field.currentValue_dn.split('\n').map((splitpart, splitindex) => <span key={splitindex}>{splitpart}{splitindex < (field.currentValue_dn.split('\n').length - 1) ? <br /> : null}</span>)}</div>
				</div>
			);
		}

		if(childParam) {
			return (
				<div className="flex">
					{field.changed && ['delete', 'change'].includes(childParamType) ? <div className="gs-compare-red">{field.oldValue_dn}</div> : null}
					{['add', 'change'].includes(childParamType) ? <div className={`gs-compare-${field.changed ? 'green' : 'black'}`}>{field.currentValue_dn}</div> : null}
				</div>
			);
		}

		return (
			<div className = "row">
				<div className="col-md-4">{field.display_title} </div>
				<div className="col-md-4 gs-compare-red">{field.oldValue_dn}</div>
				<div className="col-md-4 gs-compare-green">{field.currentValue_dn}</div>
			</div>
		);
	}

	renderChildDiff(field) {
		let getTDStyle = (fie) => {
			let style = {
				width: '150px',
				color: '#959595'
			};
			if(['text', 'selectasync', 'selectauto'].includes(fie.fieldObj.type))
				style.width = '200px';

			if(['textarea'].includes(fie.fieldObj.type))
				style.width = '300px';

			if(['number', 'checkbox', 'date'].includes(fie.fieldObj.type))
				style.width = '100px';

			return style;
		};
		let renderTableHead = () => {
			let tdArray = [];
			tdArray.push(<td key="first" style={{width: '100px'}}></td>);
			field.diff[0].childChangedFields.forEach((fie, fieindex) => {
				let tdStyle = getTDStyle(fie);
				tdArray.push(
					<td key={fieindex} style={tdStyle}>{fie.display_title}</td>
				);
			});
			return tdArray;
		};
		let renderTableBody = () => {
			return field.diff.map((childrow, childrowindex) => {
				return (
					<tr key={childrowindex}>
						{renderTableBodyFields(childrow)}
					</tr>
				);
			});
		};
		let renderTableBodyFields = (childrow) => {
			let tdArray = [];
			tdArray.push(<td key="first" style={{width: '100px'}}>{childrow.display_param}</td>);
			childrow.childChangedFields.forEach((fie, fieindex) => {
				let tdStyle = getTDStyle(fie);
				tdArray.push(
					<td key={fieindex} style={tdStyle}>
						{this.renderDiff(fie, true, childrow.param)}
					</td>
				);
			});
			return tdArray;
		};
		return (
			<div className="row">
				<div className="col-md-12">
					<div className="table-responsive">
						<table className="table gs-amend-child-table">
							<thead>
								<tr>
									{renderTableHead()}
								</tr>
							</thead>
							<tbody>
								{renderTableBody()}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
	
	renderRichtext(field, childParam, childParamType) {
		var textDiffArr = diffChars(field.oldValue_dn, field.currentValue_dn);

		if(childParam && ['add', 'delete'].includes(childParamType)) {
			return textDiffArr.map((part, partindex) => {
				return <span key={partindex} className={`gs-compare-${childParamType == 'add' ? 'green' : 'red'}`}>{part.value}</span>;
			});
		}

		return textDiffArr.map((part, partindex) => {
			return <span key={partindex} style={{color : part.added ? '#1cbc9c' : part.removed ? '#ff617c' : '#0c0c0c',backgroundColor : part.added ? '#d7f0d780' : part.removed ? '#ecccd773' : 'none'}}>{part.value}</span>;
		});
	}
	
	render() {
   		return (
			<div className="col-md-12 col-sm-12" >
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="col-md-12">
						<div style={{width:"100%"}}>
							<div style={{float:'left',width:'90%',paddingTop:'20px',paddingLeft:'10px'}}>
								<h5 style={{color:'#1cbc9c',fontSize:'24px'}}>Change In This Version</h5>
								<p><span style={{color:'#1cbc9c'}}>Text in Green </span>- Current Version, <span style={{color:'#ef4371'}}>Text in Red</span> - Previous Version</p>
							</div>
							<div style={{width:'10%',paddingTop:'25px',     display: 'flex', flexDirection: 'row-reverse', paddingRight: '15px'}}>
								<div style={{fontWeight:'100', fontSize: '30px', color: '#959595', cursor: 'pointer'}} onClick={this.props.closeModal}>x</div>
							</div>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-12" style={{maxHeight:'550px', overflowY:'auto',paddingBottom:'15px'}}>
						{this.state.sectionArr ? (this.state.sectionArr.length == 0 ? this.renderNoChanges() : <div className="card">{this.renderSection()}</div>) : null}
					</div>
				</div>
			</div>
		);
	}
}

export default AmendCompareModel;