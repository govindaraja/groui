import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect, DateElement } from '../utilcomponents';
import { search } from '../../utils/utils';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showallitem: false,
			includeoverhead : true,
			search : {},
			workorderitemdetails: [],
			overheadArr: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.addQuoteitem = this.addQuoteitem.bind(this);
		this.filterOnChange = this.filterOnChange.bind(this);
		this.overheadonChange = this.overheadonChange.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
		this.quantityOnChange = this.quantityOnChange.bind(this);
		this.showAllItem = this.showAllItem.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let workorderitemdetails = [], overheadArr = [];

		axios.get(`/api/query/getworkorderitemdetailsquery?projectid=${this.props.resource.projectid}`).then((response) => {
			if (response.data.message == 'success') {
				let tempworkorderitemdetails = response.data.main;

				for(var i = 0; i < tempworkorderitemdetails.length; i++) {
					tempworkorderitemdetails[i].checked = false;
				}

				for(var i=0;i<tempworkorderitemdetails.length;i++) {
					if(!tempworkorderitemdetails[i].boqitemsid)
						overheadArr.push(tempworkorderitemdetails[i]);
				}

				if(!this.state.showallitem) {
					for (var j = 0; j < tempworkorderitemdetails.length; j++) {
						if (tempworkorderitemdetails[j].quantity > 0) {
							workorderitemdetails.push(tempworkorderitemdetails[j]);
						}
					}
				} else {
					workorderitemdetails = tempworkorderitemdetails;
				}

				for (var i = 0; i < this.props.resource.workprogressitems.length; i++) {
					for (var j = 0; j < workorderitemdetails.length; j++) {
						if (this.props.resource.workprogressitems[i].workorderitemsid == workorderitemdetails[j].workorderitemsid) {
							workorderitemdetails[j].checked = true;
							workorderitemdetails[j].quantity = this.props.resource.workprogressitems[i].quantity
						}
					}
				}

				this.setState({ workorderitemdetails, overheadArr });
			}
		});
	}

	addQuoteitem () {
		let workprogressitems = [...this.props.resource.workprogressitems];

		for(var i=0;i<this.state.workorderitemdetails.length;i++) {
			let itemFound = false;
			if(this.state.workorderitemdetails[i].checked) {
				for(var j=0;j<workprogressitems.length;j++) {
					if(this.state.workorderitemdetails[i].workorderitemsid == workprogressitems[j].workorderitemsid) {
						workprogressitems[j].quantity = this.state.workorderitemdetails[i].quantity;
						itemFound = true;
						break;
					}
				}
				if(!itemFound) {
					workprogressitems.push(this.state.workorderitemdetails[i]);
				}
			}
		}
		if(workprogressitems.length > 0) {
			this.props.updateFormState(this.props.form, { workprogressitems });
			this.props.computeFinalRate();
			setTimeout(() => {
				this.props.closeModal();
			}, 0);
		} else {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				body : 'Please choose atleast one item',
				btnArray : ['Ok']
			}));
		}
	}

	checkboxOnChange(item, field, value) {
		item[field] = value;
		this.setState({
			workorderitemdetails: this.state.workorderitemdetails
		});
	}

	filterOnChange(id, valueobj, param) {
		this.state.search[param] = id;
		this.setState({
			search: this.state.search
		});
	}

	overheadonChange(checked) {
		this.state.includeoverhead = checked;
		if(checked) {
			this.state.overheadArr.forEach((item) => {
				this.state.workorderitemdetails.push(item);
			});
			this.setState({
				includeoverhead: this.state.includeoverhead,
				workorderitemdetails : this.state.workorderitemdetails
			});
		} else {
			let workorderitemdetails = [];
			this.state.workorderitemdetails.forEach((item) => {
				if(item.boqitemsid) {
					workorderitemdetails.push(item);
				}
			});
			this.setState({
				includeoverhead: this.state.includeoverhead,
				workorderitemdetails : workorderitemdetails
			});
		}
	}

	checkallonChange(value) {
		let workorderitemdetails = [...this.state.workorderitemdetails];
		workorderitemdetails.forEach((item, index) => {
			item.checked = value;
		});
		this.setState({	workorderitemdetails : workorderitemdetails });
	}

	quantityOnChange(value, item) {
		item.quantity = Number(value);
		this.setState({
			workorderitemdetails: this.state.workorderitemdetails
		});
	}

	showAllItem(checked) {
		this.setState({
			showallitem : checked
		}, () => {
			this.onLoad();
		})
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Items from Work Order</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Work Order No</label>
							<AutoSelect resource={'workorders'} fields={'id,wonumber,projectid'} value={this.state.search.workorderid} label={'wonumber'} valuename={'id'} filter={`workorders.projectid = ${this.props.resource.projectid} and workorders.status in ('Approved', 'Sent To Supplier')`} onChange={(value, valueobj) => this.filterOnChange(value, valueobj, 'workorderid')} />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Contractor</label>
							<AutoSelect resource={'partners'} fields={'id,name,displayname'} value={this.state.search.contractorid} label={'displayname'} valuename={'id'} filter={`partners.issupplier`} onChange={(value, valueobj) => this.filterOnChange(value, valueobj, 'contractorid')} />
						</div>
						<div className="col-md-3 form-group">
							<div>Include Overhead</div>
							<input type="checkbox" checked={this.state.includeoverhead} onChange={(evt) =>{this.overheadonChange(evt.target.checked)}}/>
						</div>
						<div className="col-md-3 form-group">
							<div>Show All Items</div>
							<label className="gs-checkbox-switch">
								<input className="form-check-input" type="checkbox" checked={this.state.showallitem} onChange={(evt) => this.showAllItem(evt.target.checked)} />
								<span className="gs-checkbox-switch-slider"></span>
							</label>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th className="text-center" style={{width:'5%'}}><input type="checkbox" checked={this.state.checkall} onChange={(evt)=>{this.checkallonChange(evt.target.checked)}}/></th>
										<th className="text-center">BOQ Details</th>
										<th className="text-center">Item Name</th>
										<th className="text-center">Description</th>
										<th className="text-center">Contractor</th>
										<th className="text-center">WO Quantity</th>
										<th className="text-center">So far Completed Qty</th>
										<th className="text-center">Current Completion Qty</th>
										<th className="text-center">Remarks</th>
									</tr>
								</thead>
								<tbody>
									{search(this.state.workorderitemdetails, this.state.search).map((item, index) => {
										return (
											<tr key={index}>
												<td className="text-center"><input type="checkbox" onChange={(e) => this.checkboxOnChange(item, 'checked', e.target.checked)}  checked={item.checked} /></td>
												<td>
													<div>
														<span className="text-muted">Internal Ref No </span>
														<span>: {item.boqitemsid_internalrefno}</span>
													</div>
													<div>
														<span className="text-muted">Client Ref No </span>
														<span>: {item.boqitemsid_clientrefno}</span>
													</div>
													<div>
														<span className="text-muted">BOQ Item Name </span>
														<span>: {item.projectitemsitemid_name}</span>
													</div>
												</td>
												<td>{item.itemid_name}</td>
												<td>{item.description}</td>
												<td>{item.contractorid_displayname}</td>
												<td>{item.woqty} {item.uomid_name}</td>
												<td>{item.completedqty}</td>
												<td>
													<input type="number" className="form-control" value={item.quantity} onChange={(evt) => this.quantityOnChange(evt.target.value,item)} />
												</td>
												<td>{item.remarks}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addQuoteitem}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
