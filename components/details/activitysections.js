import React, { Component } from 'react';
import { Field } from 'redux-form';
import moment from 'moment';

import { SelectAsync, TimeAgo } from '../utilcomponents';
import { textareaEle, dateTimeEle, selectAsyncEle, localSelectEle, ButtongroupEle } from '../formelements';
import { numberValidation, dateValidation, requiredValidation } from '../../utils/utils';
import { dateFilter, timeFilter } from '../../utils/filter';

export class Newactivitysection extends Component {
	constructor(props) {
		super(props);
		this.activitystatusonChange = this.activitystatusonChange.bind(this);
	}

	componentWillMount() {
		if(!this.props.resource.activitystatus)
			this.props.change('activitystatus', 'Completed');
	}

	activitystatusonChange() {
		this.props.change('nextactivity', {});
	}

	render() {
		if(!this.props.resource)
			return null;
		let activitytypeRequired, notesRequired;
		let activitytypeProps = {
			resource: "activitytypes",
			fields: "id,name"
		};
		let localselectprops = {
			options: [{name : null, displayname: 'None'},{name : '5min', displayname: '5 Min'},{name : '10min', displayname: '10 Min'},{name : '15min', displayname: '15 Min'},{name : '30min', displayname: '30 Min'},{name : '1hour', displayname: '1 Hour'},{name : '2hours', displayname: '2 Hours'},{name : '1day', displayname: '1 Day'},{name : '2days', displayname: '2 Days'}],
			label: 'displayname',
			valuename: 'name'
		};
		let btngroupprops = {
			buttons : [{title: 'Completed', value: 'Completed'}, {title:'Planned', value: 'Planned'}],
			buttonclass : 'btn-secondary btn-sm',
			disabled: this.props.resource.activitystatus == 'Completed' && this.props.resource.id,
			onChange: ()=>{this.activitystatusonChange()}
		}

		if(this.props.resource.activitystatus == 'Planned') {
			notesRequired = [requiredValidation];
		}
		
		return (
			<div className="col-md-12">
				<div className="row">
					<label className="col-sm-4 col-form-label">Activity Status</label>
					<div className="form-group col-sm-8">
						<Field name="activitystatus" props={btngroupprops} component={ButtongroupEle} />
					</div>
				</div>
				<div className="row">
					<label className="col-sm-4 col-form-label">Activity Type</label>
					<div className="form-group col-sm-8">
						<Field name="activitytypeid" props={activitytypeProps} component={selectAsyncEle} validate={[requiredValidation]}/>
					</div>
				</div>
				<div className="row">
					<label className="col-sm-4 col-form-label">Start Time</label>
					<div className="form-group col-sm-8">
						<Field name="startdatetime" component={dateTimeEle} validate={[requiredValidation]}/>
					</div>
				</div>
				<div className="row">
					<label className="col-sm-4 col-form-label">End Time</label>
					<div className="form-group col-sm-8">
						<Field name="enddatetime" component={dateTimeEle}/>
					</div>
				</div>
				{this.props.resource.activitystatus == 'Planned' ? <div className="row">
					<label className="col-sm-4 col-form-label">Reminder</label>
					<div className="form-group col-sm-8">
						<Field name="reminder" props={localselectprops} component={localSelectEle}/>
					</div>
				</div> : null }
				{this.props.resource.activitystatus == 'Planned' ? <div className="row">
					<label className="col-sm-4 col-form-label">Notes</label>
					<div className="form-group col-sm-8">
						<Field name="notes" component={textareaEle} validate={notesRequired}/>
					</div>
				</div> : null }
				{this.props.resource.activitystatus == 'Completed' ? <div className="row">
					<label className="col-sm-4 col-form-label">Outcome</label>
					<div className="form-group col-sm-8">
						<Field name="description" component={textareaEle} validate={[requiredValidation]}/>
					</div>
				</div> : null }
			</div>
		);
	}
}

export class Nextactivitysection extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.notesOnFocusfn = this.notesOnFocusfn.bind(this);
		this.activityOnChange = this.activityOnChange.bind(this);
	}

	activityOnChange(id, valueobj) {
		this.props.change('nextactivity.activitytypeid_name', (id ? valueobj.name : null));
	}

	notesOnFocusfn() {
		if(!this.props.resource.nextactivity.notes) {
			this.props.change('nextactivity.notes', this.props.resource.nextactivity.activitytypeid_name);
		}
	}

	render() {
		let activitytypeRequired, startdatetimeRequired, notesRequired;
		let activitytypeProps = {
			resource: "activitytypes",
			fields: "id,name",
			onChange: (value, valueobj)=>{this.activityOnChange(value,valueobj)}
		};
		let localselectprops = {
			options: [{name : null, displayname: 'None'},{name : '5min', displayname: '5 Min'},{name : '10min', displayname: '10 Min'},{name : '15min', displayname: '15 Min'},{name : '30min', displayname: '30 Min'},{name : '1hour', displayname: '1 Hour'},{name : '2hours', displayname: '2 Hours'},{name : '1day', displayname: '1 Day'},{name : '2days', displayname: '2 Days'}],
			label: 'displayname',
			valuename: 'name'
		};

		if(this.props.resource.nextactivity) {
			if(this.props.resource.nextactivity.startdatetime) {
				activitytypeRequired = [numberValidation];
			}
			if(this.props.resource.nextactivity.activitytypeid) {
				startdatetimeRequired = [dateValidation];
			}
			if(this.props.resource.nextactivity.activitytypeid || this.props.resource.nextactivity.startdatetime) {
				notesRequired = [requiredValidation];
			}
		}

		return (
			<div className="col-md-12">
				<div className="row">
					<label className="col-sm-4 col-form-label">Activity Type</label>
					<div className="form-group col-sm-8">
						<Field name="activitytypeid" props={activitytypeProps} component={selectAsyncEle} validate={activitytypeRequired}/>
					</div>
				</div>
				<div className="row">
					<label className="col-sm-4 col-form-label">Start Time</label>
					<div className="form-group col-sm-8">
						<Field name="startdatetime" component={dateTimeEle} validate={startdatetimeRequired}/>
					</div>
				</div>
				<div className="row">
					<label className="col-sm-4 col-form-label">End Time</label>
					<div className="form-group col-sm-8">
						<Field name="enddatetime" component={dateTimeEle}/>
					</div>
				</div>
				<div className="row">
					<label className="col-sm-4 col-form-label">Reminder</label>
					<div className="form-group col-sm-8">
						<Field name="reminder" props={localselectprops} component={localSelectEle}/>
					</div>
				</div>
				<div className="row">
					<label className="col-sm-4 col-form-label">Notes</label>
					<div className="form-group col-sm-8">
						<Field name="notes" props={{onFocus:()=>{this.notesOnFocusfn()}}} component={textareaEle} validate={notesRequired}/>
					</div>
				</div>
			</div>
		);
	}
}


export class Activityhistorysection extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let { activityhistorydetails } = this.props.resource;
		if(activityhistorydetails.length == 0)
			return null;
		return (
			<div className="col-md-12 col-sm-12 col-xs-12" style={{maxHeight:'300px',overflowY:'auto'}}>
				{activityhistorydetails.map((item, index)=> {
					return (
						<div className="row" key={index}>
							<div className="col-md-12">
								{item.activitytypeid_icontext ? <i rel="tooltip" title={item.activitytypeid_name} className={`fa fa-${item.activitytypeid_icontext} marginright-10`}></i> : null}
								<span>{item.activitytypeid_name} - </span>
								{item.description || item.notes ? <span style={{marginLeft:'10px', marginRight:'10px'}}><span className={`${item.activitystatus != 'Completed' ? 'hidesection' : ''}`}>Outcome</span><span className={`${item.activitystatus != 'Planned' ? 'hidesection' : ''}`}>Notes</span> : {item.description ? item.description : item.notes}</span> : null }
								<span className={`badge ${item.activitystatus=='Planned' ? 'badge-info' : 'badge-secondary'}`}>{item.activitystatus}</span>
								<br></br>
								<span className="text-muted" style={{fontSize:'13px'}}><span>Start Date</span> : {dateFilter(item.startdatetime)} @ {timeFilter(item.startdatetime)}</span>
								{item.enddatetime ? <span className="text-muted" style={{fontSize:'13px',marginLeft:'10px'}}><span>End Date</span> : {dateFilter(item.enddatetime)} @ {timeFilter(item.enddatetime)}</span>: null }
								<span className="text-muted" style={{fontSize:'13px',marginLeft:'10px'}}>Created by {item.createdby_displayname}  </span>
								<span className="text-muted" style={{fontSize:'13px'}}> On {dateFilter(item.created)} @ {timeFilter(item.created)}</span>
								<hr className="marginbottom-8 margintop-8" style={{display:'inline-block',width: '100%'}}/>
							</div>
						</div>
					);
				})}
				{this.props.state.showactivity ? <div className="col-md-12 text-center" style={{backgroundColor: '#f8f8f8', border: '1px solid #efefef'}}>
					<span onClick={this.props.controller.getActivities} className="btn showActivity">Show More</span>
				</div> : null }
			</div>
		);
	}
}


export class Newactivityhistorysection extends Component {
	constructor(props) {
		super(props);

		this.showstatus = this.showstatus.bind(this);
	}

	showstatus(activity){
		let statusObj={
			"Open" :{
				statusClass : "gs-bg-info semi-bold"
			},
			"Planned" : {
				statusClass : "gs-bg-info semi-bold"
			},
			"Inprogress" : {
				statusClass : "gs-bg-warning semi-bold"
			},
			"Completed" : {
				statusClass : "gs-bg-success semi-bold"
			},
			"Cancelled" : {
				statusClass : "gs-bg-danger semi-bold"
			}
		};

		return <span className={`font-12 ${statusObj[activity.status] ? statusObj[activity.status].statusClass : 'gs-bg-success text-success'}`} style={{"borderRadius": "2px","textAlign": "center","padding": "3px 5px"}}>{activity.status}  {(activity.status == 'Planned' && activity.plannedstarttime) ? `for ${moment(activity.plannedstarttime).format('DD MMM YYYY, hh:mm A')}` : (activity.status == 'Completed' && activity.enddatetime ? <TimeAgo date={activity.enddatetime} /> : null)}</span>
	}

	render() {
		let { activityhistorydetails } = this.props.resource;
		if(activityhistorydetails.length == 0)
			return <div className="col-md-12 col-sm-12 col-xs-12 text-center">No activities found</div>;

		return (
			<div className="col-md-12 col-sm-12 col-xs-12">
				<div  style={{maxHeight:'300px',overflow:'hidden auto'}}>
					<div className="activity-timeline-centered">
						<div className="activity-timeline-line"></div>
						{
							activityhistorydetails.map((activity, index)=> {
								let showcreatedby = activity.assignedto != activity.createdby ? true : false;
								return (
									<div className="activity-timeline-entry" key={index}>
										<div className="activity-timeline-entry-inner">
											<div className="activity-timeline-icon"></div>
												{
													(!activity.clientemailid) ?
														<div className="activity-timeline-label" onClick={()=>{this.props.openactivity(activity)}}>
															<div className=" row marginbottom-5">
																<div className="semi-bold float-left marginright-10">{activity.activitytypeid_name}</div>
																<div>{this.showstatus(activity)}</div>
															</div>
															{activity.status != 'Completed' && activity.description ? <div className="row marginbottom-5">{activity.description}</div> : null}
															{activity.status == 'Completed' ? <div className="row marginbottom-5">{activity.completionremarks}</div> : null}
															<div className="row">
																<div className="float-left"> 
																	<span className="font-12" style={{"color":"gray"}}>Assigned to</span> 
																	<span> {activity.assignedto_displayname}</span>
																</div>
																<div className={`${!showcreatedby ? 'hide':''}`}>
																	<span className="font-12 ml-2" style={{"color":"gray"}}> Created by </span> 
																	<span> {activity.createdby_displayname}</span>
																</div>
															</div>
														</div>
													: null
												}
												{
													(activity.clientemailid) ?
														<div className="activity-timeline-label" onClick={()=>{this.props.openemail(activity)}}>
															<div className=" row marginbottom-5">
																<div className="semi-bold float-left marginright-10">{activity.subject}</div>
															</div>
															<div className="row marginbottom-5">
																<div>{activity.body}</div>
															</div>
															<div className="row">
																{activity.inreplyto ? <div><span className="font-12" style={{"color":"gray"}}>Received from </span> <span> {activity.tomail} </span></div> : null }
																{!activity.inreplyto ? <div><span className="font-12" style={{"color":"gray"}}> Sent by</span> <span> {activity.frommail} </span></div> : null }

																<div className='float-right marginleft-10 font-12' style={{"color":"gray"}}><TimeAgo date={activity.emaileddate} /></div>
															</div>
														</div>
													: null
												}
										</div>
									</div>
								);
							})
						}
					</div>
				</div>
				{this.props.resource.showactivity ? <div className="col-md-12 text-center" style={{backgroundColor: '#f8f8f8', border: '1px solid #efefef'}}>
					<span onClick={()=>this.props.getActivities('reload')} className="btn showActivity">Show More</span>
				</div> : null }
			</div>
		);
	}
}