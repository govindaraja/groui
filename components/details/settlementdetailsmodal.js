import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter, currencyFilter } from '../../utils/filter';

export default class SettlementDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.onLoad = this.onLoad.bind(this);
		this.openRefLink = this.openRefLink.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		axios.get(`/api/query/settlementdetailsquery?relatedresource=${this.props.relatedresource}&relatedid=${this.props.item.relatedid}&partnerid=${this.props.item.partnerid}&employeeid=${this.props.item.employeeid}&accountid=${this.props.item.accountid}`).then((response) => {
			if (response.data.message == 'success') {
				let settle = response.data.main;
				settle.partnerid = this.props.item.partnerid;
				settle.partnerid_name = this.props.item.partnerid_name;
				settle.employeeid = this.props.item.employeeid;
				settle.employeeid_displayname = this.props.item.employeeid_displayname;
				let totalCredit = 0, totalDebit = 0;
				if(settle.voucherref == 'Dr')
					totalDebit += settle.voucheramount;
				else
					totalCredit += settle.voucheramount;
				settle.settlementarray.forEach((item) => {
					if(item.settleref == 'Dr')
						totalDebit += item.settleamount;
					else
						totalCredit += item.settleamount;
				});
				if(totalCredit - totalDebit > 0) {
					settle.outstanding = totalCredit - totalDebit;
					settle.outstandingref = "Cr";
				} else {
					settle.outstanding = totalDebit - totalCredit;
					settle.outstandingref = "Dr";
				}

				this.setState({settle});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	openRefLink(item) {
		if(item.vouchertype == 'Sales Invoice')
			this.props.history.push(`/details/salesinvoices/${item.relatedid}`);
		if(item.vouchertype == 'Purchase Invoice')
			this.props.history.push(`/details/purchaseinvoices/${item.relatedid}`);
		if(item.vouchertype == 'Receipt')
			this.props.history.push(`/details/receiptvouchers/${item.relatedid}`);
		if(item.vouchertype == 'Payment')
			this.props.history.push(`/details/paymentvouchers/${item.relatedid}`);
		if(item.vouchertype == 'Journal Voucher')
			this.props.history.push(`/details/journalvouchers/${item.relatedid}`);
		if(item.vouchertype == 'Credit Note')
			this.props.history.push(`/details/creditnotes/${item.relatedid}`);
		if(item.vouchertype == 'Debit Note')
			this.props.history.push(`/details/debitnotes/${item.relatedid}`);
		if(item.vouchertype == 'Pay Roll')
			this.props.history.push(`/details/payrolls/${item.relatedid}`);

		this.props.closeModal();
	}

	render() {
		if(!this.state.settle)
			return null;
		let { settle } = this.state;
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Settlement Details for {settle.voucherno} ({settle.vouchertype})</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-3 col-sm-6 col-xs-12">
							<label className="labelclass">{settle.partnerid > 0 ? 'Partner' : 'Employee'}</label>
							<span className="form-control un-editable" disabled>
								{settle.partnerid > 0 ? settle.partnerid_name : settle.employeeid_displayname}
							</span>
						</div>
						<div className="form-group col-md-3 col-sm-6 col-xs-12">
							<label className="labelclass">Voucher Type</label>
							<span className="form-control un-editable" disabled>
								{settle.vouchertype}
							</span>
						</div>
						<div className="form-group col-md-3 col-sm-6 col-xs-12">
							<label className="labelclass">Voucher No</label>
							<span className="form-control un-editable" disabled>
								{settle.voucherno}
							</span>
						</div>
						<div className="form-group col-md-3 col-sm-6 col-xs-12">
							<label className="labelclass">Voucher Date</label>
							<span className="form-control un-editable" disabled>
								{dateFilter(settle.postingdate)}
							</span>
						</div>
						<div className="form-group col-md-3 col-sm-6 col-xs-12">
							<label className="labelclass">Voucher Amount</label>
							<span className="form-control un-editable" disabled>
								{currencyFilter(settle.voucheramount, settle.currencyid, this.props.app)} {settle.voucherref}
							</span>
						</div>
						<div className="form-group col-md-3 col-sm-6 col-xs-12">
							<label className="labelclass">Outstanding Amount</label>
							<span className="form-control un-editable" disabled>
								{currencyFilter(settle.outstanding, settle.currencyid, this.props.app)} {settle.outstandingref}
							</span>
						</div>
					</div>
					<div className="row">
						{settle.settlementarray.length > 0 ? <div className="col-md-12">
							<table className="table table-bordered table-hover table-condensed">
								<thead>
									<tr>
										<th className="text-center">Voucher Date</th>
										<th className="text-center">Voucher Type</th>
										<th className="text-center">Voucher No</th>
										<th className="text-center">Settle Amount</th>
									</tr>
								</thead>
								<tbody>
									{settle.settlementarray.map((item, index) => {
										return (
											<tr key={index}>
												<td className='text-center'>{dateFilter(item.postingdate)}</td>
												<td className='text-center'>{item.vouchertype}</td>
												<td className='text-center'><div className="gs-anchor" onClick={() => this.openRefLink(item)}>{item.voucherno}</div></td>
												<td className='text-center'>{currencyFilter(item.settleamount, this.props.app.defaultCurrency, this.props.app)} {item.settleref}</td>
											</tr>
										);
									})}
								</tbody>
							</table> 
						</div> : <div className="col-md-6 offset-md-3 alert alert-info text-center">No Settlements found!!!!</div>}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
