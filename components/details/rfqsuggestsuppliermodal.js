import React, { Component } from 'react';
import {connect} from 'react-redux';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			vendorDetails: [...this.props.vendorDetails]
		};
		this.renderTbody = this.renderTbody.bind(this);
		this.getvendor = this.getvendor.bind(this);
		this.checkboxChange = this.checkboxChange.bind(this);
	}

	renderTbody() {
		return this.state.vendorDetails.map((item, index) => {
			return (
				<tr key={index}>
					<td><input type="checkbox" checked={item.check || false} onChange={(evt)=>this.checkboxChange(evt.target.checked, index)} /></td>
					<td>{item.partnerid_name}</td>
					<td>{item.count}</td>
				</tr>
			);
		});
	}

	checkboxChange (checked, index) {
		let { vendorDetails } = this.state;
		vendorDetails[index].check = checked;
		this.setState({ vendorDetails });
	}

	getvendor() {
		this.props.closeModal();
		this.props.callback(this.state.vendorDetails);
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Supplier Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th></th>
										<th className="text-center">Supplier</th>
										<th className="text-center">Order Count</th>
									</tr>
								</thead>
								<tbody>
									{this.renderTbody()}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.getvendor}><i className="fa fa-check"></i>Choose</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
