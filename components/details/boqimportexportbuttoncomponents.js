import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import XLSX from 'xlsx';

import { commonMethods, modalService } from '../../utils/services';
import Loadingcontainer from '../loadingcontainer';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			errors: []
		};

		this.exportExcel = this.exportExcel.bind(this);
		this.fileOnChange = this.fileOnChange.bind(this);
		this.importDataFromSheet = this.importDataFromSheet.bind(this);
		this.openErrorModal = this.openErrorModal.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.checkProjectItemExist = this.checkProjectItemExist.bind(this);
		this.generateData = this.generateData.bind(this);
		this.getMilestoneTemplate = this.getMilestoneTemplate.bind(this);
		this.getItemCategoryMaster = this.getItemCategoryMaster.bind(this);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	checkProjectItemExist() {
		this.updateLoaderFlag(true);

		let resourceArray = [...this.props.resourcename == 'projects' ? this.props.resource.projectitems : this.props.resource.projectquoteitems];

		if(resourceArray.length > 0) {
			let message = {
				header : 'Warning',
				body : `You already have items added in this ${this.props.resourcename == 'projects' ? 'Project' : 'Project Quotation'}. If you continue with the import, new items will be added along with existing items. Please remove if there are any duplicates.`,
				btnArray : ['Ok','Cancel']
			};
			return this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if(param) {
					$("#projectimportexcelfile").click();
				}
				this.updateLoaderFlag(false);
			}));
		} else {
			$("#projectimportexcelfile").click();
			this.updateLoaderFlag(false);
		}
	}

	fileOnChange(files) {
		this.updateLoaderFlag(true);
		if(files.length > 0) {
			let excelfile = files[0];
			let extensionName = excelfile.name.slice(excelfile.name.lastIndexOf('.')+1, excelfile.name.length);
			let sheetName = 'BOQ Items';

			if (['xlsm'].indexOf(extensionName) == -1) {
				return this.setState({
					errors: ["Invalid file format. Please ensure that you are importing the correct file"]
				}, () => {
					this.openErrorModal();
				});
			}

			let reader = new FileReader();
			reader.readAsBinaryString(excelfile);
			reader.onload = (e) => {
				let data = e.target.result;
				let workbook = XLSX.read(data, {type: 'binary'});

				this.setState({
					file: excelfile,
					filename: excelfile.name,
					workbook: workbook,
					errors: [],
					isValid: false,
					importfileurl : ""
				}, () => {
					this.getMilestoneTemplate();
				});
			}
		} else {
			this.setState({
				file: null,
				filename: null,
				workbook: null,
				isValid: false,
				importfileurl: ""
			}, () => {
				this.openErrorModal();
			});
		}
	}

	openErrorModal() {
		this.updateLoaderFlag(false);
		return this.props.openModal({
			render: (closeModal) => {
				return <ErrorAlertModal
					errors = {this.state.errors}
					closeModal = {closeModal} />
			}, className: {
				content: 'react-modal-custom-class',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	exportExcel() {
		this.updateLoaderFlag(true);
		axios.get(`/api/common/methods/downloadprojectrelatedexcel?resourcename=projects&transactionno=${this.props.resourcename == 'projects' ? this.props.resource.projectno : this.props.resource.quoteno}`).then((response) => {

			if(response.data.message != 'success') {
				this.updateLoaderFlag(false);
				let apiResponse = commonMethods.apiResult(response);
				return this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

			}

			let win = window.open(`/print/${response.data.main.filename}?url=${response.data.main.fileurl}`, '_blank');
			if (!win) {
				this.props.openModal(modalService['infoMethod']({
					header : 'Warning',
					body : "Popup Blocker is enabled! Please add this site to your exception list.",
					btnName : ['Ok']
				}));
			}
			this.updateLoaderFlag(false);
		});
	}

	getMilestoneTemplate() {
		let milestonetemplateObj = {};

		axios.get('/api/milestonetemplates?field=id,name&filtercondition=').then((response) => {
			if(response.data.message == 'success') {

				response.data.main.forEach(item => {
					milestonetemplateObj[item.name] = item;
				});
				this.setState({ milestonetemplateObj }, () => {
					this.getItemCategoryMaster();
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				this.updateLoaderFlag(false);
			}
		});
	}

	getItemCategoryMaster() {
		let itemcategorymasterObj = {};

		axios.get('/api/itemcategorymaster?field=id,name&filtercondition=').then((response) => {
			if(response.data.message == 'success') {

				response.data.main.forEach(item => {
					itemcategorymasterObj[item.name] = item;
				});
				this.setState({ itemcategorymasterObj }, () => {
					this.getTaxDetails();
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				this.updateLoaderFlag(false);
			}
		});
	}

	getTaxDetails() {
		let taxcodeObj = {};
		axios.get(`/api/taxcodemaster?field=id,taxcode&filtercondition=taxcodemaster.type='Sales'`).then((response) => {
			if (response.data.message == 'success') {
				taxcodeObj.taxcodemaster = {};

				response.data.main.forEach((item) => {
					taxcodeObj.taxcodemaster[item.taxcode] = item
				});
				this.setState({ taxcodeObj }, () => {
					this.importDataFromSheet();
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	importDataFromSheet() {
		let boqitemObj = {}, uomObj = {};
		let errors = [];
		let { workbook, milestonetemplateObj, itemcategorymasterObj, taxcodeObj } = this.state;
		let sheetName = 'BOQ Items';

		for(var prop in this.props.app.uomObj) {
			uomObj[this.props.app.uomObj[prop].name] = this.props.app.uomObj[prop];
		}

		if(!workbook.Sheets[`${sheetName}`]) {
			this.setState({
				errors: [`Sheet name should be ${sheetName}`]
			});
			this.fileOnChange([]);
			return false;
		}

		let sheetItemArray = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {header: 1});
		let boqitemsArray = [];

		for (var i = 2; i < sheetItemArray.length; i++) {
			let tempobj = {};
			for (var j = 0; j < sheetItemArray[i].length; j++) {
				tempobj[sheetItemArray[1][j]] = sheetItemArray[i][j];
			}
			boqitemsArray.push(tempobj);
		}

		if(boqitemsArray.length == 0) {
			this.setState({
				errors: [`${sheetName} sheet must contains atleast one item`]
			});
			this.fileOnChange([]);
			return false;
		}

		let internalrefArr = [];
		let tempItemnameArr = [], clientRefNoArr = [], internalRefNoArr = [];
		let childitemname = this.props.resourcename == 'projects' ? 'projectitems' : 'projectquoteitems';

		boqitemsArray.forEach((item, index) => {
			if(Object.keys(item).length == 0)
				return;

			let rowIndex = index + 3;

			if(!['Section', 'Item'].includes(item['Type']))
				errors.push(`Type must be Section or Item for row - ${rowIndex}`);

			if(item['Type'] == 'Item')
				tempItemnameArr.push(item["Item Name"]);

			['Description'].forEach((field) => {
				if(!item[field] || typeof(item[field]) != 'string')
					errors.push(`${field} is mandatory for row - ${rowIndex}`);
			});

			if(item['Internal Ref No']) {
				item['Internal Ref No'] = `${item['Internal Ref No']}`
			} else {
				errors.push(`Internal Ref No is mandatory for row - ${rowIndex}`);
			}

			if(item['Type'] == 'Item') {
				if(item['UOM']) {
					if(uomObj[item['UOM']]) {
						item.uomid = uomObj[item['UOM']].id;
						item.uomid_name = item['UOM'];
					} else {
						errors.push(`Invalid UOM for row - ${rowIndex}`);
					}
				} else {
					errors.push(`UOM is mandatory for row - ${rowIndex}`);
				}

				if(item['Milestone Template']) {
					if(milestonetemplateObj[item['Milestone Template']] && this.props.resource.ismilestonerequired) {
						item.milestonetemplateid = milestonetemplateObj[item['Milestone Template']].id;
						item.milestonetemplateid_name = item['Milestone Template'];
					}
				} else {
					if(this.props.resource.ismilestonerequired) {
						errors.push(`Milestone Template is mandatory for row - ${rowIndex}`);
					}
				}

				if(this.props.app.feature.useRateOnlyItems && this.props.resourcename == 'projectquotes' && item['Rate Only'] && !['Yes', 'No'].includes(item['Rate Only'])) {
					errors.push(`Rate Only must be Yes or No for row - ${rowIndex}`);
				}

				if(item['Quantity']) {
					item['Quantity'] = Number(item['Quantity']);

					if(isNaN(item['Quantity']))
						errors.push(`Quantity must contains the number value for row - ${rowIndex}`);
				} else {
					if((item['Rate Only'] == 'No' || !item['Rate Only']) || !this.props.app.feature.useRateOnlyItems)
						errors.push(`Quantity is mandatory for row - ${rowIndex}`);
				}

				if(item['Rate']) {
					item['Rate'] = Number(item['Rate']);

					if(isNaN(item['Rate']))
						errors.push(`Rate must contains the number value for row - ${rowIndex}`);
				} else {
					errors.push(`Rate is mandatory for row - ${rowIndex}`);
				}

				if(item['Approved Makes']) {
					item.approvedmakes = [];
					let tempMakeArr = item['Approved Makes'].split(',');
					tempMakeArr.forEach(categoryitem => {
						if(itemcategorymasterObj[categoryitem]) {
							item.approvedmakes.push(itemcategorymasterObj[categoryitem].id);
						}
					});
				}

				if(item['Tax']) {
					item.taxid = [];
					item['Tax'].split(',').forEach((taxitem) => {
						if(!taxcodeObj.taxcodemaster[taxitem.trim()])
							errors.push(`Invalid Tax for row - ${rowIndex}`);
						else{
							item.taxid.push(taxcodeObj.taxcodemaster[taxitem.trim()].id);
						}
					});
				}
			}
		});

		if(errors.length > 0) {
			this.setState({
				errors: errors
			});
			this.fileOnChange([]);
			return false;
		} else {
			errors = [];
			axios({
				method : 'post',
				data : {
					actionverb : 'Read',
					data : {
						itemarray:tempItemnameArr,
						type: 'name'
					}
				},
				url : '/api/common/methods/getprojectimportitemdetails'
			}).then((response) => {
				if(response.data.message == 'success') {
					let itemmasterObj = {};
					response.data.main.forEach((item) => itemmasterObj[item.name] = item);

					let newItemsArray = [];

					boqitemsArray.forEach((boqitem, boqindex) => {
						if(Object.keys(boqitem).length == 0)
							return;

						let boqRowIndex = boqindex + 3;

						if(boqitem['Type'] === "Section") {
							return newItemsArray.push({
								itemtype: 'Section',
								name: boqitem['Description'],
								internalrefno: boqitem['Internal Ref No'],
								clientrefno: boqitem['Client Ref No']
							});
						}

						if(!itemmasterObj[boqitem['Item Name']]) {
							return errors.push(`Invalid Item Name for row - ${boqRowIndex}`);
						}

						if(itemmasterObj[boqitem['Item Name']].itemtype == 'Expense') {
							return errors.push(`Expense item should not be added in BOQ for row - ${boqRowIndex}`);
						}

						let itemObj = itemmasterObj[boqitem['Item Name']];
						let tempObj = {
							itemtype: 'Item',
							internalrefno: boqitem['Internal Ref No'],
							clientrefno: boqitem['Client Ref No'],
							itemid: itemObj.id,
							itemid_name: itemObj.name,
							itemid_keepstock: itemObj.keepstock,
							itemid_issaleskit: itemObj.issaleskit,
							itemid_itemtype: itemObj.itemtype,
							splitrate: false,
							description: boqitem['Description'] ? boqitem['Description'] : itemObj.description,
							uomid: boqitem.uomid,
							uomid_name: boqitem.uomid_name,
							itemid_usebillinguom: itemObj.itemid_usebillinguom,
							usebillinguom: itemObj.usebillinguom,
							taxid: boqitem.taxid ? boqitem.taxid : (this.props.resource.taxid && this.props.resource.taxid.length > 0 ? this.props.resource.taxid : itemObj.taxid),
							rate: boqitem['Rate'],
							discountmode: 'Percentage',
							quantity: boqitem['Quantity'],
							milestonetemplateid: boqitem.milestonetemplateid,
							milestonetemplateid_name:  boqitem.milestonetemplateid_name,
							approvedmakes: boqitem.approvedmakes
						};
						itemObj.alternateuomObj = {};
						itemObj.alternateuoms.forEach(uom => itemObj.alternateuomObj[uom.id] = uom);

						if(!itemObj.alternateuomObj[boqitem.uomid]) {
							return errors.push(`Invalid UOM for row - ${boqRowIndex}`);
						}

						tempObj.alternateuom = itemObj.alternateuomObj[boqitem.uomid].alternateuom;
						tempObj.uomconversiontype = itemObj.alternateuomObj[boqitem.uomid].conversiontype;
						tempObj.uomconversionfactor = itemObj.alternateuomObj[boqitem.uomid].alternateuom ? itemObj.alternateuomObj[boqitem.uomid].conversionrate : null;

						if(tempObj.usebillinguom) {
							tempObj.billinguomid = tempObj.uomid;
							tempObj.billingconversiontype= tempObj.alternateuom ? tempObj.uomconversiontype : 'Fixed';
							tempObj.billingconversionfactor = tempObj.alternateuom ? tempObj.uomconversionfactor : 1;
							tempObj.billingrate = tempObj.rate;
							tempObj.billingquantity = tempObj.quantity;
						}

						if(this.props.app.feature.useRateOnlyItems && this.props.resourcename == 'projectquotes') {
							tempObj.quantity = boqitem['Rate Only'] == 'Yes' ? 0 : tempObj.quantity;
							tempObj.israteonly = boqitem['Rate Only'] == 'Yes' ? true : false;
							tempObj.billingquantity = boqitem['Rate Only'] == 'Yes' ? 0 : tempObj.billingquantity;
						}

						newItemsArray.push(tempObj);
					});

					if(errors.length > 0) {
						this.setState({
							errors: errors
						});
						this.fileOnChange([]);
					} else {
						this.generateData(newItemsArray);
					}
				} else {
					this.setState({
						errors: response.data.errors
					});
					this.fileOnChange([]);
				}
			});
		}
	}

	generateData(newItemsArray) {
		let childitemname = this.props.resourcename == 'projects' ? 'projectitems' : 'projectquoteitems';
		let childkititemname = this.props.resourcename == 'projects' ? 'kititemprojectdetails' : 'kititemprojectquotedetails';
		let dataArray = [...this.props.resource[childitemname]];

		let index = 0;
		for (var i = 0; i < dataArray.length; i++) {
			if (dataArray[i].index > index)
				index = dataArray[i].index;
		}

		newItemsArray.forEach(item => {
			index++;
			item.index = index;
			dataArray.push(JSON.parse(JSON.stringify(item)));
		});

		this.props.updateFormState(this.props.form, {
			[childitemname] : dataArray,
			[childkititemname] : []
		});

		setTimeout(() => {
			this.props.onLoadKit(0, this.props.resource[this.props.resourcename == 'projects' ? 'kititemprojectdetails' : 'kititemprojectquotedetails']);
			this.props.getimportMilestoneItems();
			this.props.computeFinalRate();
		}, 100);

		this.props.openModal(modalService['infoMethod']({
			header : 'Success',
			body : `Items are imported successfully`,
			btnArray : ['Ok']
		}));

		this.updateLoaderFlag(false);
	}

	render() {
		return (
			<div className="float-left">
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>

				<input type="file" accept={'.xlsm'} id="projectimportexcelfile" className="form-control hide" value={this.state.importfileurl || ''} onChange={(evt) => {this.fileOnChange(evt.target.files)}} />
				<button type="button" className="btn btn-sm gs-form-btn-success btn-width" onClick={this.checkProjectItemExist} title={`Import ${this.props.resourcename == 'projectquotes' ? 'Project Quote Items' : 'Project Items'} from excel`} style={{marginRight: '8px'}}><i className="fa fa-download"></i>Import</button>

				<button type="button" className="btn btn-sm gs-form-btn-success btn-width" onClick={this.exportExcel} title={`Export the sample excel`}><i className="fa fa-upload"></i>Download Template</button>
			</div>
		);
	}
});

export class ErrorAlertModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.renderError = this.renderError.bind(this);
	}

	renderError() {
		return this.props.errors.map((error, index) => {
			return(
				<div className="form-group col-md-12" key={index}>
					<div className="alert alert-info" style={{overflowWrap: 'break-word'}}>{error}</div>
				</div>
			);
		})
	}

	render() {
		if(this.props.errors.length == 0)
			return null;

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Warning</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							{this.renderError()}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
