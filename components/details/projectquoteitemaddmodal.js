import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import * as utils from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			checkall : false,
			quoteitemdetails: [],
			quotekititemdetails: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.updatecheckbox = this.updatecheckbox.bind(this);
		this.addQuoteitem = this.addQuoteitem.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
	}

	componentWillMount() {
		if(this.props.resource.projectquoteid || this.props.resource.projectid) {
			this.onLoad();
		}
	}

	onLoad() {
		let quoteitemdetails = [];
		let estimationidArr = [];
		let itemprop = this.props.resource.projectquoteid ? 'boqquoteitemsid' : 'boqitemsid';
		for(var i = 0; i < this.props.resource.estimationitems.length; i++) {
			if(this.props.resource.estimationitems[i][`${itemprop}`])
				estimationidArr.push(this.props.resource.estimationitems[i][`${itemprop}`]);
		}

		let queryStr = this.props.resource.projectquoteid ? `/api/query/getprojectitemdetailsquery?projectquoteid=${this.props.resource.projectquoteid}&boqquoteitemsid=${estimationidArr}` : `/api/query/getprojectitemdetailsquery?projectid=${this.props.resource.projectid}&boqitemsid=${estimationidArr}`

		axios.get(`${queryStr}`).then((response) => {
			if (response.data.message == 'success') {
				quoteitemdetails = response.data.main;
				for(var i = 0; i < quoteitemdetails.length; i++) {
					quoteitemdetails[i].checked = false;
				}

				this.setState({quoteitemdetails});
			}
		});
	}

	addQuoteitem () {
		let estimationitems = [...this.props.resource.estimationitems];
		let index = 0;
		estimationitems.forEach((item) => {
			if(index < item.index)
				index = item.index;
		});
		index = index + 1;
		this.state.quoteitemdetails.forEach((item) => {
			if(item.checked) {
				let tempObj = {
					index: (index+1),
					estimationtype: 'Simple',
					isparent: true
				};
				if(this.props.resource.projectquoteid) {
					utils.assign(tempObj, item, [{'boqquoteitemsid' : 'id'}, {'boqitemid' : 'itemid'}, 'itemid','itemid_name', 'itemid_itemtype', {'boqitemid_name' : 'itemid_name'}, {'boqitemid_itemtype' : 'itemid_itemtype'}, {'boqquoteitemsid_specification' : 'specification'}, 'specification', {'boqquoteitemsid_description' : 'description'}, 'description', {'boqquoteitemsid_israteonly' : 'israteonly'}, {'boqquoteitemsid_quantity' : 'quantity'}, 'quantity', {'boqquoteitemsid_estimatedqty' : 'quantity'}, {'boqquoteitemsid_uomid' : 'uomid'}, 'uomid', {'boqquoteitemsid_internalrefno' : 'internalrefno'}, {'boqquoteitemsid_clientrefno' : 'clientrefno'}, 'uomconversionfactor', 'uomconversiontype', {'boqitemid_defaultpurchasecost' : 'itemid_defaultpurchasecost'}, {'boqitemid_lastpurchasecost' : 'itemid_lastpurchasecost'}, 'billinguomid', 'usebillinguom','billingconversionfactor','billingconversiontype','billingquantity','billingrate','approvedmakes', {'boqquoteitemsid_billinguomid' : 'billinguomid'}, {'boqquoteitemsid_billingconversionfactor' : 'billingconversionfactor'}, {'boqquoteitemsid_billingconversiontype' : 'billingconversiontype'}, {'boqquoteitemsid_billingquantity' : 'billingquantity'}, {'boqquoteitemsid_uomconversionfactor' : 'uomconversionfactor'}, {'boqquoteitemsid_uomconversiontype' : 'uomconversiontype'}]);

					this.props.customFieldsOperation('projectquoteitems', tempObj, item, 'estimationitems');
				}

				if(this.props.resource.projectid) {
					utils.assign(tempObj, item, [{'boqitemsid' : 'id'}, {'boqitemid' : 'itemid'}, 'itemid','itemid_name', 'itemid_itemtype', {'boqitemid_name' : 'itemid_name'}, {'boqitemid_itemtype' : 'itemid_itemtype'}, {'boqitemsid_specification' : 'specification'}, 'specification', {'boqitemsid_description' : 'description'}, 'description', {'boqitemsid_quantity' : 'quantity'}, 'quantity', {'boqitemsid_estimatedqty' : 'quantity'}, {'boqitemsid_uomid' : 'uomid'}, 'uomid', {'boqitemsid_internalrefno' : 'internalrefno'}, {'boqitemsid_clientrefno' : 'clientrefno'}, 'uomconversionfactor', 'uomconversiontype', {'boqitemid_defaultpurchasecost' : 'itemid_defaultpurchasecost'}, {'boqitemid_lastpurchasecost' : 'itemid_lastpurchasecost'}, 'billinguomid', 'usebillinguom','billingconversionfactor','billingconversiontype','billingquantity','billingrate','approvedmakes', {'boqitemsid_billinguomid' : 'billinguomid'}, {'boqitemsid_billingconversionfactor' : 'billingconversionfactor'}, {'boqitemsid_billingconversiontype' : 'billingconversiontype'}, {'boqitemsid_billingquantity' : 'billingquantity'}, {'boqitemsid_uomconversionfactor' : 'uomconversionfactor'}, {'boqitemsid_uomconversiontype' : 'uomconversiontype'}]);

					this.props.customFieldsOperation('projectitems', tempObj, item, 'estimationitems');
				}

				if(item.usebillinguom) {
					tempObj.rate = Number((((tempObj.billingrate || 0) / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
				}

				estimationitems.push(tempObj);
				index++;
			}
		});
		this.props.updateFormState(this.props.form, { estimationitems });
		setTimeout(() => {
			this.props.computeFinalRate();
			this.props.closeModal();
		}, 0);
	}

	updatecheckbox(item, field, value) {
		item[field] = value;
		this.setState({
			quoteitemdetails: this.state.quoteitemdetails
		});
	}

	checkallonChange(value) {

		this.state.quoteitemdetails.forEach((item) => {
			item.checked = value;
		});
		this.setState({
			checkall : value,
			quoteitemdetails: this.state.quoteitemdetails
		});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Add Items from Project {this.props.resource.projectquoteid ? 'Quotation' : ''}</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12">
							{this.state.quoteitemdetails.length > 0 ? <table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th className="text-center" style={{width: '5%'}}><input type="checkbox" checked={this.state.checkall || false} onChange={(evt)=>{this.checkallonChange(evt.target.checked)}}/></th>
										<th className="text-center">Internal Ref No</th>
										<th className="text-center">Item Name</th>
										<th className="text-center">Quantity</th>
									</tr>
								</thead>
								<tbody>
									{this.state.quoteitemdetails.map((item, index)=> {
										return (
											<tr key={index}>
												<td className="text-center"><input type="checkbox" onChange={(e) => this.updatecheckbox(item, 'checked', e.target.checked)}  checked={item.checked} /></td>
												<td>{item.internalrefno}</td>
												<td>{item.itemid_name}</td>
												<td className="text-center">{item.israteonly ? 'RO' : item.quantity} {item.uomid_name}</td>
											</tr>
										);
									})}
								</tbody>
							</table> : <div className="alert alert-info text-center">All items from this Project {this.props.resource.projectquoteid ? 'Quotation' : ''} have already been added in estimation</div>}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								{this.state.quoteitemdetails.length > 0 ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addQuoteitem}><i className="fa fa-check"></i>Ok</button> : null}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
