import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { currencyFilter, dateFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { NumberElement } from '../utilcomponents';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			totalclaimamount: 0,
			availableadvance: 0,
			unusedadvance: 0,
			settlementArr: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.checkboxonChange = this.checkboxonChange.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
		this.creditOnChange = this.creditOnChange.bind(this);
		this.addSettlementItem = this.addSettlementItem.bind(this);
		this.calculateTotal = this.calculateTotal.bind(this);
		this.autosettle = this.autosettle.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let settlementArr = [];
		let availableadvance = 0, totalclaimamount = 0, unusedadvance = 0;

		axios.get(`/api/query/getemployeeoutstandingsquery?type=Receipt&companyid=${this.props.resource.companyid}&currencyid=${this.props.resource.currencyid}&employeeid=${this.props.resource.employeeid}`).then((response) => {
			if (response.data.message == 'success') {
				settlementArr = response.data.main;

				settlementArr.sort((a, b)=> {
					a = new Date(a.xexpenserequestid_expensedate) || new Date(a.xjournalvoucherid_voucherdate);
					b = new Date(b.xexpenserequestid_expensedate) || new Date(b.xjournalvoucherid_voucherdate);
					return a - b;
				});

				settlementArr.forEach((item)=> {
					item.credit = item.balance;
					availableadvance += Number(item.balance);
					if(item.xexpenserequestid)
						item.xexpenserequestid_outstandingamount = item.balance;
					if(item.xpayrollid)
						item.xpayrollid_outstandingamount = item.balance;
				});

				if(this.props.resource.expenserequestsettlementitems.length > 0) {
					totalclaimamount = 0;

					this.props.resource.expenserequestsettlementitems.forEach((settlementitem)=> {
						settlementArr.forEach((item)=> {

							if((item.xexpenserequestid > 0 && (item.xexpenserequestid == settlementitem.xexpenserequestid)) || (item.xjournalvoucherid > 0 && (item.xjournalvoucherid == settlementitem.xjournalvoucherid))) {
								item.credit = settlementitem.credit;
								totalclaimamount += Number(settlementitem.credit);
								item.checked = true;
							}
						});
					});
				}

				unusedadvance = Number((availableadvance - totalclaimamount).toFixed(this.props.app.roundOffPrecision));

				this.setState({ settlementArr, availableadvance, totalclaimamount, unusedadvance });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	checkboxonChange(item, value) {
		item.checked = value;
		this.setState({
			settlementArr: this.state.settlementArr
		}, () => {
			this.calculateTotal();
		});
	}

	checkallonChange (value) {
		let { settlementArr } = this.state;
		settlementArr.forEach((item) => {
			item.checked = value;
		});

		this.setState({
			settlementArr: this.state.settlementArr,
			checkall : value
		}, () => {
			this.calculateTotal();
		});
	}

	creditOnChange(value, item) {
		item.credit = value;
		this.setState({
			settlementArr: this.state.settlementArr
		}, () => {
			this.calculateTotal();
		});
	}

	calculateTotal() {
		let availableadvance = 0, totalclaimamount = 0, unusedadvance = 0;
		this.state.settlementArr.forEach((item)=> {
			availableadvance += Number(item.balance);
			if(item.checked) {
				totalclaimamount += Number(item.credit);
			}
		});
		unusedadvance = Number((availableadvance - totalclaimamount).toFixed(this.props.app.roundOffPrecision));
		this.setState({ availableadvance, totalclaimamount, unusedadvance });
	}

	autosettle() {
		let totalapprovalamount = 0, settlementamount = 0;
		let { settlementArr } = this.state;

		this.props.resource.expenserequestitems.forEach((item) => {
			if(this.props.app.user.roleid.includes(18))
				totalapprovalamount += item.amountrequested || 0;
			else
				totalapprovalamount += item.amountapproved || 0;
		});

		if(totalapprovalamount > 0) {
			settlementArr.forEach((item) => {
				settlementamount += item.balance;
			});
		}

		settlementamount = Number(settlementamount.toFixed(this.props.app.roundOffPrecision));
		totalapprovalamount = Number(totalapprovalamount.toFixed(this.props.app.roundOffPrecision));

		if(totalapprovalamount > 0 && settlementamount > 0) {

			if(totalapprovalamount == settlementamount) {
				for(var k = 0; k < settlementArr.length; k++) {
					settlementArr[k].credit = settlementArr[k].balance;
					settlementArr[k].checked = true;
				}
			} else if(settlementamount > totalapprovalamount) {
				for(var n = 0; n < settlementArr.length; n++) {
					if(totalapprovalamount > 0) {
						if(settlementArr[n].balance >= totalapprovalamount) {
							settlementArr[n].credit = totalapprovalamount;
							settlementArr[n].checked = true;
							totalapprovalamount = 0;
						} else {
							settlementArr[n].credit = settlementArr[n].balance;
							settlementArr[n].checked = true;
							totalapprovalamount -= settlementArr[n].balance;
							totalapprovalamount = Number(totalapprovalamount.toFixed(this.props.app.roundOffPrecision));
						}
					} else {
						settlementArr[n].checked = false;
					}
				}
			} else {
				for(var p = 0; p < settlementArr.length; p++) {
					settlementArr[p].credit = settlementArr[p].balance;
					settlementArr[p].checked = true;
				}
			}
		}

		this.setState({settlementArr}, () => {
			this.calculateTotal();
		});
	}

	addSettlementItem () {
		let itemdetailsarr = [];
		this.state.settlementArr.forEach((item) => {
			if(item.checked) {
				itemdetailsarr.push(item);
			}
		});
		if(itemdetailsarr.length == 0) {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				body : 'Please choose atlease only one item',
				btnArray : ['Ok']
			}));
		}

		this.props.callback(this.state.settlementArr);
		setTimeout(()=> {this.props.closeModal()}, 0);
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Settlement Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-4 form-group">
							<div className="float-left" style={{marginRight: '15px'}}>Total Amount Claimed : </div>
							<div className="float-left font-weight-bold">{currencyFilter(this.state.totalclaimamount, this.props.resource.currencyid, this.props.app)}</div>
						</div>
						<div className="col-md-4 form-group">
							<div className="float-left" style={{marginRight: '15px'}}>Advances Available : </div>
							<div className="float-left font-weight-bold">{currencyFilter(this.state.availableadvance, this.props.resource.currencyid, this.props.app)}</div>
						</div>
						<div className="col-md-4 form-group">
							<div className="float-left" style={{marginRight: '15px'}}>Unused Advances : </div>
							<div className="float-left font-weight-bold">{currencyFilter(this.state.unusedadvance, this.props.resource.currencyid, this.props.app)}</div>
						</div>
						<div className="col-md-4 offset-md-4 text-center form-group">
							<button type="button" className="btn btn-sm btn-primary" onClick={()=>this.autosettle()}><i className="fa fa-check"></i>Auto Settle</button>
						</div>
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th style={{width:'5%'}}><input type="checkbox" checked={this.state.checkall || false} onChange={(evt) => this.checkallonChange(evt.target.checked)}/></th>
										<th className="text-center">Req No</th>
										<th className="text-center" style={{width:'15%'}}>Date</th>
										<th className="text-center">Remarks</th>
										<th className="text-center">Balance</th>
										<th className="text-center" style={{width:'25%'}}>Settlement Amount</th>
									</tr>
								</thead>
								<tbody>
									{this.state.settlementArr.map((item, index) => {
										return (
											<tr key={index}>
												<td><input type="checkbox" onChange={(e) => this.checkboxonChange(item, e.target.checked)} checked={item.checked || false} /></td>
												<td className="text-center">{item.xexpenserequestid ? item.xexpenserequestid_expenserequestno : item.xjournalvoucherid_voucherno}</td>
												<td className="text-center">{(item.xexpenserequestid ? dateFilter(item.xexpenserequestid_expensedate) : dateFilter(item.xjournalvoucherid_voucherdate))}</td>
												<td>{item.xexpenserequestid_remarks}</td>
												<td className="text-center">{currencyFilter(item.balance, this.props.resource.currencyid, this.props.app)}</td>
												<td>
													<NumberElement className="form-control" value={item.credit} onChange={(value) => this.creditOnChange(value, item)} />
												</td>
											</tr>
										)
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addSettlementItem}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});