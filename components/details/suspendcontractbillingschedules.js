import React, { Component } from 'react';
import axios from 'axios';
import { search,checkActionVerbAccess } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect,DateElement } from '../utilcomponents';
import { dateFilter } from '../../utils/filter';
import {Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { selectAsyncEle,NumberEle,DateEle } from '../formelements';
import { numberNewValidation,dateNewValidation } from '../../utils/utils';
import {currencyFilter} from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state={
			contractbillingschedules : JSON.parse(JSON.stringify(this.props.resource.contractbillingschedules))
		};
		this.updateBillingSchedules = this.updateBillingSchedules.bind(this);
		this.valueOnChange = this.valueOnChange.bind(this);
		this.openSuspendRemarkModal = this.openSuspendRemarkModal.bind(this);
	};

	valueOnChange(field,item,value){
		item[field] = value;
		let contractbillingschedules = this.state.contractbillingschedules;
		this.setState({contractbillingschedules});
	}

	updateBillingSchedules(remarks) {
		this.props.closeModal();
		this.props.callback(this.props.action, this.state.contractbillingschedules, remarks, () => {});
	}

	checkboxOnChange(value){
		this.state.contractbillingschedules.forEach((item,index)=>{
			if(!item.invoiceid && !item.issuspended ){
				item.checked = value;
			}
		});

		this.setState({
			contractbillingschedules : this.state.contractbillingschedules
		});
	}
	
	openSuspendRemarkModal (item,index) {
		var scheduleFound = false;
		this.state.contractbillingschedules.forEach((schedule)=>{
			if(schedule.checked)
				scheduleFound = true;
		});

		if(!scheduleFound){
			return this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					bodyArray : ["Please select atleast one schedule"],
					btnArray : ['Ok']
				}));
		}
		
		this.props.openModal({
			render: (closeModal) => {
				return <SuspendRemarksModal resource={this.props.resource} callback={(remarks) => {
					this.updateBillingSchedules(remarks);
				}} app={this.props.app} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-30', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	render () {
		
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Suspend Billing Schedules</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12 paddingbottom-7" style={{textAlign : "center",fontSize : "larger",fontWeight : 600}}>{this.props.resource.billingschedule} - Schedule Date</div>
						<div className="col-md-12 col-sm-12 col-xs-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th className="text-center" style={{width:'10%'}}><input  type="checkbox" onChange={(e) => {this.checkboxOnChange(e.target.checked);}}  checked={this.state.checkall} /></th>
										<th className="text-center">Start</th>
										<th className="text-center">End</th>
										<th className="text-center">Status</th>
									</tr>
								</thead>
								<tbody>
									{this.state.contractbillingschedules.map((item, index) => {
										return (<tr key={index}>
											<td className="text-center" style={{width: '5%'}}>
												{
													(!item.invoiceid && !item.issuspended) ? <input  type="checkbox" onChange={(e) => this.valueOnChange('checked',item, e.target.checked)}  checked={item.checked} /> : null
												}
											</td>
											<td className="text-center" style={{width: '20%'}}>
												<DateElement className="form-control" value={item.scheduledate} required={true} disabled={true} onChange={(val,itemstr) => {this.valueOnChange('scheduledate',item,val);}}/>
												
											</td>
											<td className="text-center" style={{width: '20%'}}>
												<DateElement className="form-control" value={item.scheduleenddate} required={true} disabled={true} onChange={(val,itemstr) => {this.valueOnChange('scheduleenddate',item,val);}}/>
											</td>
											<td className="text-center"  style={{width: '20%'}}>{(item.invoiceid>0) ? 'Invoiced' : ((item.issuspended) ? 'Suspended' : 'Active')}</td>
										</tr>);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}>Cancel</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.openSuspendRemarkModal} disabled={false}>Suspend</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class SuspendRemarksModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.suspendSchedule = this.suspendSchedule.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
	}

	suspendSchedule() {
		this.props.callback(this.state.remarks);
		this.props.closeModal();
	}

	inputOnChange(evt) {
		this.setState({
			remarks: evt.target.value
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header borderbottom-0">
					<h5 className="modal-title">Remarks for Suspend</h5>
				</div>
				<div className="react-modal-body">
					<div className="row justify-content-center">
						<div className="col-md-12 col-sm-12">
							<textarea className={`form-control ${!this.state.remarks ? 'errorinput' : ''}`} value={this.state.remarks} onChange={this.inputOnChange} required />
						</div>
					</div>
				</div>
				<div className="react-modal-footer text-center bordertop-0">
					<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
					<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.suspendSchedule} disabled={!this.state.remarks}><i className="fa fa-check-square-o"></i>Ok</button>
				</div>
			</div>
		);
	}
}