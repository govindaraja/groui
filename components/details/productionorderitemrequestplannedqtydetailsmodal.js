import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { currencyFilter } from '../../utils/filter';


export default connect((state) => {
	return {
		app: state.app,
		fullstate: state
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			productionfinisheditems : this.props.resourcename == 'workorders' ? this.props.resource.workorderinitems : this.props.resource.productionfinisheditems
		};

		this.createitemrequest = this.createitemrequest.bind(this);
	}

	createitemrequest() {
		if(this.state.productionfinisheditems.filter(item=>item.selected).length < 1){
			let message = {
				header : 'Warning',
				body : ["Please select atleast one finished product"],
				btnArray : ['Ok']
			};
			return this.props.openModal(modalService.infoMethod(message));
		}
		let tempObj = {...this.props.resource};
		tempObj.param = this.props.resourcename == 'workorders' ? 'Job Work' : 'Production Orders';
		let childresourcename = this.props.resourcename == 'workorders' ? 'workorderoutitems' : 'productionbomitems';
		tempObj[`${childresourcename}`] = [];

		for(let i=0;i<this.state.productionfinisheditems.length;i++){
			if(this.state.productionfinisheditems[i].selected){
				if(!this.state.productionfinisheditems[i].plannedqty || this.state.productionfinisheditems[i].plannedqty <= 0) {
					let message = {
						header : 'Warning',
						body : ["Please enter planned quantity more than zero"],
						btnArray : ['Ok']
					};
					return this.props.openModal(modalService.infoMethod(message));
				}
				if(this.state.productionfinisheditems[i].plannedqty > this.state.productionfinisheditems[i].quantity) {
					let message = {
						header : 'Warning',
						body : ["Item Request Quantity should not be more than Production Order Quantity"],
						btnArray : ['Ok']
					};
					return this.props.openModal(modalService.infoMethod(message));
				}
				
				this.props.resource[`${childresourcename}`].filter(rmitem=>(rmitem.specificitemid == this.state.productionfinisheditems[i].itemid || (rmitem.applicablefor=='All Items' && this.state.productionfinisheditems[i].valuationfactor>0))).forEach((rmitem,rmindex)=>{
					
					if(rmitem.applicablefor == 'All Items'){

						if(tempObj[`${childresourcename}`].filter(item=>item.id == rmitem.id).length>0){
							return null;	
						}
						tempObj[`${childresourcename}`].push(rmitem);
					}else{
						rmitem.quantity = Number((rmitem.conversion * this.state.productionfinisheditems[i].plannedqty).toFixed(this.props.app.roundOffPrecisionStock));
						tempObj[`${childresourcename}`].push(rmitem);
					}
				});
			}
		}
		
		this.props.history.push({pathname: '/createItemRequest', params: tempObj});
		this.props.closeModal();
		
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Finished Products</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12">
						
							{(this.state.productionfinisheditems && this.state.productionfinisheditems.length>0) ? <table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th></th>
										<th className="text-center">Item</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">Planned Quantity</th>
									</tr>
								</thead>
								<tbody>
									{this.state.productionfinisheditems.map((item, index) => {
										return (
											<tr key={index}>
												<td>
													<input type="checkbox" value={item.selected} onChange={(evt) =>{item.selected=evt.target.checked;this.setState({productionfinisheditems : this.state.productionfinisheditems});}}/>
												</td>
												<td>{item.itemid_name}</td>
												<td>{item.quantity}</td>
												<td>
													<input className="form-control" type="number" value={item.plannedqty} onChange={(evt) =>{item.plannedqty=evt.target.value;this.setState({productionfinisheditems : this.state.productionfinisheditems});}}/>
												</td>
												
											</tr>
										);
									})}
								</tbody>
							</table> : null}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.createitemrequest} disabled={false}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
