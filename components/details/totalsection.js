import React, { Component } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';

import { localSelectEle } from '../formelements';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, orderByTax } from '../../utils/filter';
import { requiredValidation } from '../../utils/utils';

export const Totalsection = connect((state) =>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let defaultCurrenySymbol = this.props.resource.currencyid ? this.props.app.currency[this.props.app.defaultCurrency].symbol : "";
		let resourceCurrenySymbol = this.props.resource.currencyid ? this.props.app.currency[this.props.resource.currencyid].symbol : "";


		return (
			<div className="col-md-12 col-sm-12">
				<table className="float-right lineheight-2">
					<tbody>
						<tr>
							<td align="right">Total Amount :</td>
							<td align="right" style={{whiteSpace: 'nowrap', minWidth: '120px'}}>{currencyFilter(this.props.resource.basictotal, this.props.resource.currencyid, this.props.app)}</td>
						</tr>
						{(this.props.resource.taxdetails && Object.keys(this.props.resource.taxdetails).length > 0) ? orderByTax(this.props.resource.taxdetails, 'displayOrder', false).map((tax, taxindex) => {
							return (
								<tr key={taxindex}>
									<td align="right">{tax.description} on {currencyFilter(tax.amount, this.props.resource.currencyid, this.props.app)} :</td>
									<td align="right" style={{whiteSpace: 'nowrap'}}>{currencyFilter(tax.taxamount, this.props.resource.currencyid, this.props.app)}</td>
								</tr>
							);
						}) : null}
						{this.props.resource.additionalcharges && this.props.resource.additionalcharges.map((charge, chargeindex)=>{
							return (
								<tr key={chargeindex} ng-show="feature.useAdditionalCosts">
									<td align="right">{charge.description} :</td> 
									<td align="right" style={{whiteSpace: 'nowrap'}}>{currencyFilter(charge.amount, this.props.resource.currencyid, this.props.app)}</td>
								</tr>
							);
						})}
						<tr>
							<td align="right">Amount With Tax :</td>
							<td align="right" style={{whiteSpace: 'nowrap'}}>{currencyFilter(this.props.resource.totalaftertax, this.props.resource.currencyid, this.props.app)}</td>
						</tr>
						<tr>
							<td align="right">Round off method :</td>
							<td style={{width: '30%', whiteSpace: 'nowrap'}}>
								<div style={{flex:'1'}}>
									<Field name={'roundoffmethod'} props={{
										classname: 'total_section_select',
										options: [{label:'None', value:'none'},{label:'Up', value:'up'},{label:'Down', value:'down'},{label:'Even', value:'even'}],
										label: 'label',
										valuename: 'value',
										required: true,
										onChange : this.props.computeFinalRate
									}} component={localSelectEle} validate={[requiredValidation]} />
								</div>
							</td>
						</tr>
						<tr>
							<td align="right">Round off value: </td>
							<td align="right" style={{whiteSpace: 'nowrap'}}>{currencyFilter(this.props.resource.roundoffvalue, this.props.resource.currencyid, this.props.app)}</td>
						</tr>
						<tr>
							<td align="right"><b className="font-16">{`Final Amount (${resourceCurrenySymbol}) :`}</b></td>
							<td align="right" className="font-16" style={{whiteSpace: 'nowrap'}}><b>{currencyFilter(this.props.resource.finaltotal, this.props.resource.currencyid, this.props.app)}</b></td>
						</tr>
						{this.props.resource.currencyid != this.props.app.defaultCurrency  && this.props.resource.currencyid ? <tr>
							<td align="right"><b className="font-16">{`Final Amount (${defaultCurrenySymbol}) :`}</b></td>
							<td align="right" className="font-16" style={{whiteSpace: 'nowrap'}}><b>{currencyFilter(this.props.resource.finaltotallc, this.props.app.defaultCurrency, this.props.app)}</b></td>
						</tr> : null }
					</tbody>
				</table>
			</div>
		);
	}
});
