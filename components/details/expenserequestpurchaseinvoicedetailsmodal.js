import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { LocalSelect, AutoSelect } from '../utilcomponents';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			expenserequestitemArr: this.props.expenserequestitemArr
		};

		this.createPurchaseInvoice = this.createPurchaseInvoice.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
	}

	createPurchaseInvoice() {
		let tempExpreqItem = [], errArr = [];
		let itemCount = 0;

		this.state.expenserequestitemArr.forEach((item)=> {
			if (item.checked == true) {
				itemCount++;
				tempExpreqItem.push(item);
			}
		});

		if(itemCount > 1)
			errArr.push("Please select only one item");
		if(itemCount == 0)
			errArr.push("Please select one item");

		if(errArr.length > 0) {
			return this.props.openModal(modalService['infoMethod']({
				header: 'Warning',
				bodyArray: errArr,
				btnArray: ['Ok']
			}));
		} else {
			this.props.callback(tempExpreqItem);
			this.props.closeModal();
		}
	}

	checkboxOnChange(value, item) {
		item.checked = value;
		this.setState({
			expenserequestitemArr: this.state.expenserequestitemArr
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Create Purchase Invoice Details</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12">
							{this.state.expenserequestitemArr.length > 0 ? <table className="table table-bordered" >
								<thead>
									<tr>
										<th></th>
										<th>Expense Category</th>
									</tr>
								</thead>
								<tbody>
									{this.state.expenserequestitemArr.map((item, index)=> {
										return (
											<tr key={index}>
												<td className="text-center"><input type="checkbox" checked={item.checked || false} onChange={(evt)=>{this.checkboxOnChange(evt.target.checked, item)}}/></td>
												<td className="text-center">{item.expensecategoryid_name}</td>
											</tr>
										);
									})}
								</tbody>
							</table> : <div className="alert alert-info text-center">There is no  pending purchase invoice items</div>}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.createPurchaseInvoice}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
