import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { currencyFilter } from '../../utils/filter';
import Loadingcontainer from '../loadingcontainer';

import { LocalSelect } from '../utilcomponents';

export const ItemrequestprofitanalyseModal = connect((state)=>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			basedon : this.props.app.feature.runInCRMMode ? 'defaultcost' : (this.props.app.feature.quoteprofitabilitybasedon ? this.props.app.feature.quoteprofitabilitybasedon : 'averagecost'),
			loaderflag: true,
			profitanalyse : {},
			profitarray: [],
			costarray: [],
			showMessage : false
		};
		this.onLoad = this.onLoad.bind(this);
		this.onLoadEstimation = this.onLoadEstimation.bind(this);
		this.onLoadServicecall = this.onLoadServicecall.bind(this);
		this.getStockdetails = this.getStockdetails.bind(this);
		this.basedonOnChange = this.basedonOnChange.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
	}

	componentWillMount() {
		if(this.props.param == 'estimations')
			this.onLoadEstimation();
		else if(this.props.param == 'itemrequests')
			this.onLoad();
		else
			this.onLoadServicecall();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	onLoadServicecall() {
		let servicecallarr = [];
		axios.get(`/api/query/getservicecallitemquery?servicecallid=${this.props.resource.id}`).then((response)=> {
			if (response.data.message == 'success') {
				servicecallarr = response.data.main;
				for (var i = 0; i < servicecallarr.length; i++) {
					if (this.props.app.uomObj[servicecallarr[i].uomid]) {
						servicecallarr[i].uomconversionrate = this.props.app.uomObj[servicecallarr[i].uomid].conversionrate;
					}
				}
				this.getStockdetails(servicecallarr);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	onLoadEstimation() {
		if(this.props.resource.estimationitems && this.props.resource.estimationitems.length > 0){
			let estimationitemarray = this.props.resource.estimationitems;

			for (var i = 0; i < estimationitemarray.length; i++) {
				if (estimationitemarray[i].rate) {
					if (estimationitemarray[i].discountquantity) {
						if (estimationitemarray[i].discountmode == 'Percentage') {
							estimationitemarray[i].rateafterdiscount = estimationitemarray[i].rate - (estimationitemarray[i].rate * (estimationitemarray[i].discountquantity / 100));
						} else {
							estimationitemarray[i].rateafterdiscount = estimationitemarray[i].rate - estimationitemarray[i].discountquantity;
						}
					} else {
						estimationitemarray[i].rateafterdiscount = estimationitemarray[i].rate;
					}
				}

				if (this.props.app.uomObj[estimationitemarray[i].uomid]) {
					estimationitemarray[i].uomconversionrate = this.props.app.uomObj[estimationitemarray[i].uomid].conversionrate;
				}
			}
			this.getStockdetails(estimationitemarray);
		}else{
			this.setState({showMessage : true});
		}
	}

	onLoad() {
		let itemidarr = [], itemrequestitemidarr = [];
		for (var i = 0; i < this.props.resource.itemrequestitems.length; i++) {
			itemidarr.push(this.props.resource.itemrequestitems[i].itemid);
		}
		for (var i = 0; i < this.props.resource.itemrequestitems.length; i++) {
			itemrequestitemidarr.push(this.props.resource.itemrequestitems[i].id);
		}

		if (itemidarr.length > 0) {
			axios.get(`/api/query/getservicereportitemquery?itemid=${itemidarr.toString()}&itemrequestitemid=${itemrequestitemidarr.toString()}`).then((response) => {
				if (response.data.message == 'success') {
					if(response.data.main.length > 0){
						let servicereportarray = response.data.main;
				
						for (var i = 0; i < servicereportarray.length; i++) {
							if (servicereportarray[i].rate) {
								if (servicereportarray[i].discountquantity) {
									if (servicereportarray[i].discountmode == 'Percentage') {
										servicereportarray[i].rateafterdiscount = servicereportarray[i].rate - (servicereportarray[i].rate * (servicereportarray[i].discountquantity / 100));
									} else {
										servicereportarray[i].rateafterdiscount = servicereportarray[i].rate - servicereportarray[i].discountquantity;
									}
								} else {
									servicereportarray[i].rateafterdiscount = servicereportarray[i].rate;
								}
							}

							if (this.props.app.uomObj[servicereportarray[i].uomid]) {
								servicereportarray[i].uomconversionrate = this.props.app.uomObj[servicereportarray[i].uomid].conversionrate;
							}
						}
						this.getStockdetails(servicereportarray);
					}else{
						this.setState({showMessage : true});
						this.updateLoaderFlag(false);
					}
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}
	}

	basedonOnChange(value) {
		this.updateLoaderFlag(true);
		this.setState({
			basedon : value
		}, () => {
			this.getStockdetails(this.state.itemarray);
		});
	}

	getStockdetails(itemarray) {
		this.setState({ itemarray });

		let itemidArray = itemarray.map(item => item.itemid);

		let requrl = ``;

		if(this.state.basedon == 'averagecost')
			requrl = `/api/query/stockquery?itemid=${itemidArray}`;

		if(this.state.basedon == 'averagelandingcost')
			requrl = `/api/query/getreceiptitemratequery?itemid=${itemidArray}`;

		if(this.state.basedon == 'defaultcost')
			requrl = `/api/itemmaster?field=id,defaultpurchasecost,purchaseuomid,stockuomid,landingcostmultiple&filtercondition=itemmaster.id IN (${itemidArray.join()})`;

		axios.get(requrl).then((response) => {
			if(this.state.basedon == 'defaultcost') {
				response.data.main.forEach(item => {
					item.itemid = item.id
					item.marate = item.defaultpurchasecost;
					item.onhandqty =  1;
				});
			}
			this.calculateProfit(itemarray, response.data.main);
		});
	}

	calculateProfit(itemarray, resultarray) {
		let profitanalyse = {}, profitarray = [];

		for (var j = 0; j < itemarray.length; j++) {
				let temptotalqty = 0, temptotalvalue = 0, costPrice = null;

			for (var i = 0; i < resultarray.length; i++) {
				if (itemarray[j].itemid == resultarray[i].itemid) {
					resultarray[i].onhandqty = resultarray[i].onhandqty ? resultarray[i].onhandqty : 0;
					resultarray[i].marate = resultarray[i].marate ? resultarray[i].marate : 0;
					if (itemarray[j].uomid != resultarray[i].itemid_stockuomid) {
						if (this.props.app.uomObj[resultarray[i].itemid_stockuomid]) {
							resultarray[i].marate = (resultarray[i].marate / itemarray[j].uomconversionrate) * this.props.app.uomObj[resultarray[i].itemid_stockuomid].conversionrate;
						}
					}

					temptotalvalue += resultarray[i].onhandqty * resultarray[i].marate;
					temptotalqty += resultarray[i].onhandqty;

					if (temptotalqty > 0 && temptotalvalue > 0)
						costPrice = temptotalvalue / temptotalqty;
				}
			}

			profitarray.push({
				itemid : itemarray[j].itemid,
				itemid_name : itemarray[j].itemid_name,
				salesprice : itemarray[j].rateafterdiscount,
				costprice : costPrice,
				finalamount : itemarray[j].amount,
				qty : itemarray[j].quantity
			});
		}

		let temptotalcost = 0, temptotalrevenue = 0, tempgrossprofit = 0, costnotFound = false;

		for (var i = 0; i < profitarray.length; i++) {
			temptotalrevenue += profitarray[i].finalamount;
			profitanalyse.totalrevenue = temptotalrevenue;

			if (profitarray[i].costprice) {
				let tempgrossprofitpercentage = (profitarray[i].salesprice - profitarray[i].costprice) / profitarray[i].salesprice * 100;
				profitarray[i].grossprofitpercentage = profitarray[i].salesprice > 0 ? Number(tempgrossprofitpercentage.toFixed(2)) : null;
			} else {
				costnotFound = true;
			}
		}

		if (costnotFound == false) {
			for (var j = 0; j < profitarray.length; j++) {
				temptotalcost += profitarray[j].costprice * profitarray[j].qty;
				profitanalyse.totalcost = temptotalcost;
				tempgrossprofit = temptotalrevenue - temptotalcost;
				profitanalyse.grossprofit = tempgrossprofit;
				let temptotalgrossprofit = (tempgrossprofit / temptotalrevenue) * 100;
				profitanalyse.totalgrossprofit = temptotalrevenue > 0 ? Number(temptotalgrossprofit.toFixed(2)) : null;
			}
		}
		this.setState({profitanalyse, profitarray});
		this.updateLoaderFlag(false);
	}

	renderBasedOn() {
		if(this.props.app.feature.runInCRMMode)
			return null;
		return (
			<div className="col-md-8 form-group">
				<label className="labelclass">Base On</label>
				<LocalSelect options={[{label: 'Average Cost', value: 'averagecost'}, {label: 'Default Cost', value: 'defaultcost'}, {label: 'Average Landing Cost', value: 'averagelandingcost'}]} label="label" valuename="value" value={this.state.basedon} onChange={this.basedonOnChange} disabled={this.state.showMessage} />
			</div>
		);
	}

	render() {
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Profitability Analysis</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
				<div className="row justify-content-center">
					{this.renderBasedOn()}
				</div>
				{	
					(this.state.profitarray.length > 0) ?
						<div className="row">
								<div className="col-md-12 col-sm-12 col-xs-12">
									<table className="table">
										<thead>
											<tr>
												<th className="text-center">Item</th>
												<th className="text-right">Selling Price</th>
												<th className="text-right">Cost Price</th>
												<th className="text-right">Gross Margin %</th>
											</tr>
										</thead>
										<tbody>
											 {this.state.profitarray.map((item, index) => {
												return (<tr key={index}>
													<td>{item.itemid_name}</td>
													<td className="text-right">{currencyFilter(item.salesprice, null, this.props.app)}</td>
													<td className="text-right">{currencyFilter(item.costprice, null, this.props.app)}</td>
													<td className="text-right">{item.grossprofitpercentage}</td>
												</tr>)
											 })}
										</tbody>
									</table>
								</div>
								<div className="col-md-12 col-sm-12 col-xs-12">
									<div className="col-md-8 pull-right bgColor-gray borderradius-6">
										<table className="width-100 lineheight-2">
											<tbody>
												<tr>
													<td className="text-right">Total Revenue</td>
													<td>:</td>
													<td className="text-right">{currencyFilter(this.state.profitanalyse.totalrevenue, null, this.props.app)}</td>
												</tr>
												{this.state.profitanalyse.totalcost ? <tr>
													<td className="text-right">Total Cost</td>
													<td>:</td>
													<td className="text-right">{currencyFilter(this.state.profitanalyse.totalcost, null, this.props.app)}</td>
												</tr> : null }
												{this.state.profitanalyse.grossprofit ? <tr>
													<td className="text-right">Gross Margin</td>
													<td>:</td>
													<td className="text-right">{currencyFilter(this.state.profitanalyse.grossprofit, null, this.props.app)}</td>
												</tr> : null }
												{this.state.profitanalyse.totalgrossprofit ? <tr>
													<td className="text-right">Gross Margin %</td>
													<td>:</td>
													<td className="text-right">{this.state.profitanalyse.totalgrossprofit}</td>
												</tr> : null }
											</tbody>
										</table>
									</div>
								</div> 
						</div>
					: null
				}
				{
					(this.state.showMessage) ? <div className="row"><div className="col-md-8 offset-md-2"><div className="alert alert-warning text-center">Data not available. Please check after using items in  Service Report.</div></div></div> : null
				}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
