import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter, currencyFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { LocalSelect, AutoSelect, DateElement, NumberElement } from '../utilcomponents';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tabClasses: ["show active", ""],
			search: {},
			completeddate: this.props.invoicedate,
			projectItemArr: [],
			uniqueObj: {}
		};

		this.onLoad = this.onLoad.bind(this);
		this.addProjectItem = this.addProjectItem.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
		this.quantityOnChange = this.quantityOnChange.bind(this);
		this.getTabClass = this.getTabClass.bind(this);
		this.getTabPaneClass = this.getTabPaneClass.bind(this);
		this.setActiveTab = this.setActiveTab.bind(this);
		this.dateOnChange = this.dateOnChange.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.renderTabContent = this.renderTabContent.bind(this);
		this.renderStage = this.renderStage.bind(this);
		this.searchFn = this.searchFn.bind(this);
		this.autofillcurrentqty = this.autofillcurrentqty.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		this.updateLoaderFlag(true);
		let projectItemArr = [], uniqueProjectTemplateObj = {};
		let uniqueObj = {};
		let projectCustomFields = this.props.getCustomFields('projectitems','string');

		axios.get(`/api/query/getmilestoneprogressitemquery?projectid=${this.props.projectid}&completeddate=${this.state.completeddate}`).then((response) => {
			if (response.data.message == 'success') {
				let projectItemArray = response.data.main;

				projectItemArray.forEach((item) => {
					item.qty = '';
					if(!uniqueObj[item.milestonetemplateid]) {
						uniqueObj[item.milestonetemplateid] = {
							milestonetemplateid_name: item.milestonetemplateid_name,
							stages: {},
							boqItems: {},
							boqMileObj: {}
						};
					}
					if(!uniqueObj[item.milestonetemplateid].stages[item.milestonestageid]) {
						uniqueObj[item.milestonetemplateid].stages[item.milestonestageid] = {
							milestonestageid_name: item.milestonestageid_name
						};
					}
					if(!uniqueObj[item.milestonetemplateid].boqItems[item.boqid]) {
						uniqueObj[item.milestonetemplateid].boqItems[item.boqid] = {
							boqid: item.boqid,
							boqid_internalrefno: item.boqid_internalrefno,
							boqid_clientrefno: item.boqid_clientrefno,
							itemid: item.itemid,
							itemid_name: item.itemid_name,
							boqid_description: item.boqid_description,
							boqid_quantity: item.boqid_quantity,
							uomid_name: item.uomid_name
						};
					}
					uniqueObj[item.milestonetemplateid].boqMileObj[`${item.boqid}_${item.milestonestageid}`] = item;
				});

				this.props.salesinvoiceitems.forEach((invitem) => {
					Object.keys(uniqueObj).forEach((templateitem) => {
						if(uniqueObj[templateitem].boqMileObj[`${invitem.boqid}_${invitem.milestonestageid}`]) {
							uniqueObj[templateitem].boqMileObj[`${invitem.boqid}_${invitem.milestonestageid}`].qty = invitem.quantity;

							uniqueObj[templateitem].boqItems[invitem.boqid].checked = true;
						}
					});
				});

				this.setState({ uniqueObj }, ()=> {
					this.setActiveTab(1);
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	searchFn(boqItemObj, searchObj) {
		let tempObj = {};
		Object.keys(boqItemObj).forEach((a) => {
			let filtered = true;
			for(var prop in searchObj) {
				if(searchObj[prop] != null && searchObj[prop] != '' && searchObj[prop] != undefined) {
					console.log()
					let searchstring = '' + searchObj[prop] + '';
					let isDateObject = false;
					if(searchObj[prop] instanceof Date) {
						searchstring = '' + searchObj[prop].toDateString() + '';
						isDateObject = true;
					}
					if((boqItemObj[a][prop] != null && boqItemObj[a][prop] != '' && boqItemObj[a][prop] != undefined) || typeof(boqItemObj[a][prop]) == 'boolean') {
						let originalstring = '' + boqItemObj[a][prop] + '';

						if(isDateObject && new Date(boqItemObj[a][prop]) != 'Invalid Date')
							originalstring = '' + new Date(boqItemObj[a][prop]).toDateString() + '';

						if(typeof(boqItemObj[a][prop]) == 'boolean') {
							originalstring = boqItemObj[a][prop] ? 'true' : 'false';
							searchstring = ['y', 'ye', 'yes', 'e', 'es', 's'].includes(searchstring.toLowerCase()) ? 'true' : (['n', 'no', 'o'].includes(searchstring.toLowerCase()) ? 'false' : searchstring);
						}

						if(originalstring.toLowerCase().indexOf(searchstring.toLowerCase()) == -1)
							filtered = false;
					} else {
						filtered = false;
					}
				}
			}
			if(filtered)
				tempObj[a] = boqItemObj[a];
		});
		return tempObj;
	}

	inputonChange(value, itemstr) {
		let { search } = this.state;
		search[itemstr] = value;
		this.setState({search});
	}

	dateOnChange(value) {
		this.setState({ completeddate: value}, () => {
			this.onLoad();
		});
	}

	checkboxOnChange(value, item) {
		item.checked = value;
		this.setState({
			uniqueObj: this.state.uniqueObj
		});
	}

	checkallonChange(value, milestonetemplateid) {
		let { uniqueObj } = this.state;

		Object.keys(uniqueObj).forEach((templateitem) => {
			if(milestonetemplateid == templateitem) {
				uniqueObj[templateitem].checked = value;
				Object.keys(uniqueObj[templateitem].boqItems).forEach((item) => {
					uniqueObj[templateitem].boqItems[item].checked = value;
				});
			}
		});
		this.setState({	uniqueObj });
	}

	quantityOnChange(value, item) {
		item.qty = value;

		this.setState({
			uniqueObj: this.state.uniqueObj
		});
	}

	getTabClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	getTabPaneClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	setActiveTab(tabNum) {
		let { tabClasses } = this.state;
		tabClasses =  ["", ""]
		tabClasses[tabNum] = "show active";
		this.setState({tabClasses}, ()=>{
			this.getTabPaneClass(tabNum);
		});
	}

	addProjectItem(id, valueobj) {
		let { uniqueObj } = this.state;
		let errArr = [], invoiceitemsArr = [];
		let ischeckedfound = false, invalidqty = false;
		let tempidObj = {};

		Object.keys(uniqueObj).forEach((templateitem) => {
			tempidObj[templateitem] = {
				boq: {}
			};
			Object.keys(uniqueObj[templateitem].boqItems).forEach((boqitem) => {
				Object.keys(uniqueObj[templateitem].stages).forEach((stageitem, stageindex) => {

					let itemstr = `${boqitem}_${stageitem}`;

					if(!uniqueObj[templateitem].boqMileObj[itemstr])
						return null;

					uniqueObj[templateitem].boqMileObj[itemstr].checked = false;

					if(uniqueObj[templateitem].boqItems[boqitem].checked) {

						tempidObj[templateitem].boq[boqitem] = tempidObj[templateitem].boq[boqitem] ? tempidObj[templateitem].boq[boqitem] : false;

						if(Number(uniqueObj[templateitem].boqMileObj[itemstr].qty) > 0) {
							tempidObj[templateitem].boq[boqitem] = true;
							uniqueObj[templateitem].boqMileObj[itemstr].checked = true;
						}
					}

					invoiceitemsArr.push(uniqueObj[templateitem].boqMileObj[itemstr]);
				});
			});
		});

		for(var prop in tempidObj) {
			if(Object.keys(tempidObj[prop].boq).length > 0) {
				ischeckedfound = true;
				for(var boqprop in tempidObj[prop].boq) {
					if(!tempidObj[prop].boq[boqprop]) {
						invalidqty = true;
					}
				}
			}
		}

		if(!ischeckedfound)
			errArr.push("Please choose atleast one item");

		if(invalidqty)
			errArr.push("Please pick quantity for any one of the stage for choosed items");

		if(errArr.length > 0)
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : errArr,
				btnArray : ['Ok']
			}));

		this.props.callback(invoiceitemsArr);
		this.props.closeModal();
	}

	autofillcurrentqty() {
		let { uniqueObj } = this.state;

		let isSelected = false;
		Object.keys(uniqueObj).forEach((templateitem) => {
			Object.keys(uniqueObj[templateitem].boqItems).forEach((boqitem) => {
				Object.keys(uniqueObj[templateitem].stages).forEach((stageitem, stageindex) => {

					let itemstr = `${boqitem}_${stageitem}`;

					if(!uniqueObj[templateitem].boqMileObj[itemstr])
						return null;

					uniqueObj[templateitem].boqMileObj[itemstr].checked = false;

					if(uniqueObj[templateitem].boqItems[boqitem].checked) {
						isSelected = true;

						if(!uniqueObj[templateitem].boqMileObj[itemstr].qty && (uniqueObj[templateitem].boqMileObj[itemstr].completedqty > uniqueObj[templateitem].boqMileObj[itemstr].invoicedqty)) {
							uniqueObj[templateitem].boqMileObj[itemstr].qty = uniqueObj[templateitem].boqMileObj[itemstr].completedqty - uniqueObj[templateitem].boqMileObj[itemstr].invoicedqty;
						}
					}
				});
			});
		});

		if(!isSelected)
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : ['Please select at-least one item to apply auto-fill current quantity'],
				btnArray : ['Ok']
			}));

		this.setState({ uniqueObj });
	}

	renderStage(item, templateObj) {
		return Object.keys(templateObj.stages).map((stageitem, stageindex) => {
			let secitem = templateObj.boqMileObj[`${item.boqid}_${stageitem}`];
			return([
				<td className="text-center" key={stageindex}>{secitem ? secitem.completedqty : '-'}</td>,
				<td className="text-center" key={stageindex+1}>{secitem ? secitem.invoicedqty : '-'}</td>,
				<td className="text-center" key={stageindex+2}>{secitem ? <NumberElement className="form-control" value={secitem.qty} onChange={(value) => this.quantityOnChange(value, secitem)} disabled = {secitem.invoicedqty == secitem.completedqty} /> : '-'} </td>
			]);
		});
	}

	renderTabContent(uniqueObj) {
		return Object.keys(uniqueObj).map((templateObj, templateindex) => {
			return (
				<div className={`tab-pane fade ${this.getTabPaneClass(templateindex+1)}`} role="tabpanel" key={templateindex}>
					<div className="row margintop-25">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="table-responsive">
								<table className="table table-bordered table-condensed">
									<thead>
										<tr>
											<th className="text-center">
												<input type="checkbox" checked={uniqueObj[templateObj].checked || false} onChange={(evt)=>{this.checkallonChange(evt.target.checked, templateObj)}}/>
											</th>
											<th className="text-center" style={{width: '7%'}}>Ref No</th>
											<th className="text-center" style={{width: '20%'}}>Item Name</th>
											<th className="text-center" style={{width: '7%'}}>BOQ Qty</th>
											{
												Object.keys(uniqueObj[templateObj].stages).map((stageitem, stageindex) => {
													return(
														<th colSpan="3" className="text-center" key={stageindex}>{uniqueObj[templateObj].stages[stageitem].milestonestageid_name}</th>
													);
												})
											}
										</tr>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											{
												Object.keys(uniqueObj[templateObj].stages).map((stageitem, stageindex) => {
													return([
														<th className="text-center" key={stageindex}>Completed</th>,
														<th className="text-center" key={stageindex+1}>So far Invoiced</th>,
														<th className="text-center" key={stageindex+2}>Current Qty</th>
													]);
												})
											}
										</tr>
									</thead>
									<tbody>
										{Object.keys(this.searchFn(uniqueObj[templateObj].boqItems, this.state.search)).map((boqid, index) => {
											let item = uniqueObj[templateObj].boqItems[boqid];
											return (
												<tr key={index}>
													<td className="text-center">
														<input type="checkbox" onChange={(e) => this.checkboxOnChange(e.target.checked, item)} checked={item.checked || false} />
													</td>
													<td>{item.boqid_internalrefno}</td>
													<td><div>{item.itemid_name}</div><div className="text-ellipsis-line-2 font-13 text-muted">{item.boqid_description}</div></td>
													<td>{item.boqid_quantity} {item.uomid_name}</td>
													{
														this.renderStage(item, uniqueObj[templateObj])
													}
												</tr>
											)
										})}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			)
		});
	}

	render() {
		let { uniqueObj } = this.state;
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Project Item Details</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="form-group col-md-3">
							<label className="labelclass">Ref No</label>
							<input type="text" className="form-control" value={this.state.search.boqid_internalrefno || ''} placeholder="Search by ref no" onChange={(evt)=>{this.inputonChange(evt.target.value, 'boqid_internalrefno')}}/>
						</div>
						<div className="form-group col-md-3">
							<label className="labelclass">Item Name</label>
							<input type="text" className="form-control" value={this.state.search.itemid_name || ''} placeholder="Search by item name" onChange={(evt)=>{this.inputonChange(evt.target.value, 'itemid_name')}}/>
						</div>
						<div className="form-group col-md-3">
							<label className="labelclass">Date</label>
							<DateElement className="form-control" value={this.state.completeddate} onChange={(val) => this.dateOnChange(val)} />
						</div>
						<div className="form-group col-md-3 d-flex align-items-end">
							<button type="button" className="btn btn-sm gs-btn-primary" onClick={this.autofillcurrentqty}>Auto Fill Current Qty</button>
						</div>
					</div>
					{Object.keys(uniqueObj).length > 0 ? <div className="row">
						<div className="col-md-12">
							<ul className="nav nav-tabs">
								{
									Object.keys(uniqueObj).map((templateObj, templateindex) => {
										return (
											<li className="nav-item" key={templateindex}>
												<a className={`nav-link ${this.getTabClass(templateindex+1)}`}  onClick={()=>this.setActiveTab(templateindex+1)}>{uniqueObj[templateObj].milestonetemplateid_name}</a>
											</li>
										)
									})
								}
							</ul>
							<div className="tab-content">
								{this.renderTabContent(uniqueObj)}
							</div>
						</div>
					</div> : null}

					{this.state.loaderflag ? <div className="row"><div className="col-md-12 alert alert-info text-center">Please wait...</div></div> : null}
					{!this.state.loaderflag && Object.keys(uniqueObj).length == 0 ? <div className="row"><div className="col-md-12 alert alert-info text-center">There no items needs to be invoiced</div></div> : null}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addProjectItem}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
