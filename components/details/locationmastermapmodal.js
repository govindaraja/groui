import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';
import { LocalSelect, AutoSelect } from '../utilcomponents';
import { checkActionVerbAccess } from '../../utils/utils';
import MapProps from '../map-properties';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.onLoad = this.onLoad.bind(this);
		this.getLatlngfromAddress = this.getLatlngfromAddress.bind(this);
		this.updateLatlng = this.updateLatlng.bind(this);
	}

	componentWillMount() {
		setTimeout(this.onLoad, 1000);
	}

	componentWillReceiveProps() {
		setTimeout(this.onLoad, 1000);
	}

	onLoad() {
		let templat = this.props.resource.latitude ? this.props.resource.latitude : 13.0827;
		let templng = this.props.resource.longitude ? this.props.resource.longitude : 80.2707;
		let myLatlng = new google.maps.LatLng(templat, templng);

		let map = new google.maps.Map(document.getElementById('map'), {
			zoom: 14,
			center: myLatlng,
			fullscreenControl: false
		});
		let geocoder = new google.maps.Geocoder();

		google.maps.event.addListener(map, 'click', (event)=> {
			let { marker } = this.state;
			let latLng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
			marker.setPosition(latLng);
			this.setState({
				marker,
				latitude: event.latLng.lat(),
				longitude: event.latLng.lng()
			});
		});

		this.setState({ map, geocoder }, ()=> {
			if(this.props.resource.address && !this.props.resource.latitude && !this.props.resource.longitude)
				this.getLatlngfromAddress(geocoder, map);
			else {
				let marker = new google.maps.Marker({
				    position: myLatlng,
				    zoom: 14
				});
				marker.setMap(map);
				this.setState({ marker, latitude: this.props.resource.latitude, longitude: this.props.resource.longitude});
			}
		});
	}

	getLatlngfromAddress(geocoder, map) {
		geocoder.geocode({'address': this.props.resource.address}, (results, status) => {
			if (status === 'OK') {
				map.setCenter(results[0].geometry.location);
				let marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});

				this.setState({
					marker,
					latitude: results[0].geometry.location.lat(),
					longitude: results[0].geometry.location.lng()
				});
			}
		});
	}

	updateLatlng() {
		this.props.updateFormState(this.props.form, {
			latitude: this.state.latitude, 
			longitude: this.state.longitude
		});
		this.props.closeModal();
	}

	render() {
		const mapStyle = {
			width: '100%',
			height: $('.react-outer-modal').outerHeight() - $('.react-modal-footer').outerHeight()
		}

		return (
			<div className="react-outer-modal">
				<div className="react-modal-body-scroll">
					<div className="row no-gutters">
						<div className="col-md-12">
							<div id="map" style={mapStyle}></div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-8 col-sm-8 col-xs-12">
							<span className="text-muted">Address: </span>
							<span>{this.props.resource.address}</span>
						</div>
						<div className="col-md-4 col-sm-4 col-xs-12 text-right">
							<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Cancel</button>
							<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.updateLatlng}><i className="fa fa-check"></i>Ok</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
