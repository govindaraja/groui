import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { itemmasterDisplaynamefilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			covereditemsArray: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.renderQty = this.renderQty.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		axios.get(`/api/query/getcontractcovereditemquery?contractid=${this.props.item.contractid}&facilityid=${this.props.item.equipmentid}`).then(response => {
			if(response.data.message == 'success') {
				this.setState({ covereditemsArray: response.data.main });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	renderQty(item) {
		if(item.capacityfield && item.capacity > 0)
			return (
				<div>
					<span style={{color:'gray'}}>{item.qtyinnos ? item.qtyinnos : 1} * </span>
					<span style={{color:'gray'}}>{item.capacity ? item.capacity : 1}</span>
					<span> = {item.quantity} {`${item.capacityfield ? `${itemmasterDisplaynamefilter(item.capacityfield, this.props.app)}` : ''}`}</span>
				</div>
			);
		else
			return <span>{item.quantity} {item.uomid_name}</span>
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">{`Covered Items of ${this.props.item.equipmentid_displayname} `}</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12 col-sm-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th className='text-center' style={{width:'35%'}}>Item</th>
										<th className='text-center'>Description</th>
										<th className='text-center' style={{width:'20%'}}>Quantity</th>
									</tr>
								</thead>
								<tbody>
									{this.state.covereditemsArray.map((item, index) => {
										return (
											<tr key={index}>
												<td>{item.itemid_name}</td>
												<td className="text-center">{item.description}</td>
												<td>{this.renderQty(item)}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
