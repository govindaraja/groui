import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { currencyFilter, dateFilter, uomFilter } from '../../utils/filter';

import { LocalSelect, SelectAsync, DateElement } from '../utilcomponents';
import Loadingcontainer from '../loadingcontainer';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderflag: true
		};

		this.onLoad = this.onLoad.bind(this);
		this.saveEquipment = this.saveEquipment.bind(this);
		this.updateField = this.updateField.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let resource = {
			...JSON.parse(JSON.stringify(this.props.resource)),
			isinstallationapplicable: true,
			iswarrantyapplicable: true,
			equipmentitems: [],
			contractstartdate: new Date(this.props.resource.deliverynotedate),
			contractcreatemode: 'One contract for each contract type'
		};
		let showkititem = false
		axios.get(`/api/contracttypes?field=id,name,schedule,duration,type&filtercondition=`).then((response) => {
			if (response.data.message == 'success')  {
				let contracttypeObj = {};
				response.data.main.forEach((item) => {
					contracttypeObj[item.id] = item;
				});

				resource.deliverynoteitems.forEach((item) => {
					if (contracttypeObj[item.itemid_warrantycontracttypeid])
						item.itemid_contracttype = contracttypeObj[item.itemid_warrantycontracttypeid].name
				});

				resource.deliverynoteitems.forEach((item) => {
					if (item.itemid_isequipment && !item.itemid_issaleskit && !item.itemid_hasaddons && (item.itemid_iswarrantyapplicable || item.itemid_isinstallationapplicable)) {

						let pickingArray = item.pickingdetails && item.pickingdetails.details ? item.pickingdetails.details : [];

						for(var i=0;i<item.stockqty;i++) {
							resource.equipmentitems.push({
								itemid : item.itemid,
								itemid_name : item.itemid_name,
								description : item.description,
								orderitemsid : item.orderitemsid,
								isequipment : item.itemid_isequipment,
								itemid_contracttype : item.itemid_contracttype,
								iswarrantyapplicable : item.itemid_iswarrantyapplicable,
								isinstallationapplicable : item.itemid_isinstallationapplicable,
								contracttypeid : item.itemid_warrantycontracttypeid,
								warrantycontractbasedon : item.itemid_warrantycontractbasedon,
								serialno : (pickingArray.length == item.stockqty) ? pickingArray[i].serialno : null
							});
						}
					} else if(item.itemid_isequipment && (item.itemid_issaleskit || item.itemid_hasaddons) && (item.itemid_iswarrantyapplicable || item.itemid_isinstallationapplicable)) {
						showkititem = true;
						let serialNoArray = [];
						let totalSerialNoStr = '';
						let pickingArray = item.pickingdetails && item.pickingdetails.details ? item.pickingdetails.details : [];
						if(pickingArray.length > 0 && pickingArray[0].serialno) {
							let tempObj = {
								id : item.parentid,
								itemid : item.itemid,
								itemid_name : item.itemid_name,
								conversion : 1,
								serialnoarr : pickingArray.map((serial) => serial.serialno)
							};
							totalSerialNoStr += `${tempObj.itemid_name} : ${tempObj.serialnoarr.join()}\n`;
							serialNoArray.push(tempObj);
						}
						resource.kititemdeliverydetails.forEach((kititem) => {
							if (item.index == kititem.rootindex && !kititem.parentindex) {
								if(kititem.pickingdetails && kititem.pickingdetails.details && kititem.pickingdetails.details.length > 0 && kititem.pickingdetails.details[0].serialno) {
									let tempObj = {
										id : kititem.parentid,
										itemid : kititem.itemid,
										itemid_name : kititem.itemid_name,
										conversion : kititem.conversion,
										serialnoarr : kititem.pickingdetails.details.map((serial) => serial.serialno)
									};
									totalSerialNoStr += `${tempObj.itemid_name} : ${tempObj.serialnoarr.join()}\n`;
									serialNoArray.push(tempObj);
								}
							}
						});

						for(var i=0;i<item.quantity;i++) {
							let serialnoStrArr = [];
							serialNoArray.forEach((serial) => {
								for (var t = (i * serial.conversion); t < ((i * serial.conversion) + serial.conversion); t++)
									serialnoStrArr.push(serial.serialnoarr[t]);
							});
							resource.equipmentitems.push({
								itemid : item.itemid,
								itemid_name : item.itemid_name,
								description : item.description,
								orderitemsid : item.orderitemsid,
								isequipment : item.itemid_isequipment,
								itemid_contracttype : item.itemid_contracttype,
								iswarrantyapplicable : item.itemid_iswarrantyapplicable,
								isinstallationapplicable : item.itemid_isinstallationapplicable,
								contracttypeid : item.itemid_warrantycontracttypeid,
								warrantycontractbasedon : item.itemid_warrantycontractbasedon,
								serialno : serialnoStrArr.join(),
								totalserialno : totalSerialNoStr
							});
						}
					}
				});
				this.setState({
					resource,
					showkititem
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	saveEquipment (actionverb) {
		this.updateLoaderFlag(true);
		axios({
			method : 'post',
			data : {
				actionverb : actionverb,
				data : this.state.resource
			},
			url : '/api/deliverynotes'
		}).then((response) => {
			if (response.data.message == 'success') {
				let createdarray = [];
				for (var i = 0; i < response.data.main.equipmentArray.length; i++)
					createdarray.push(`Equipment - ${response.data.main.equipmentArray[i].displayname} and Serialno - ${response.data.main.equipmentArray[i].serialno}`);

				for (var i = 0; i < response.data.main.contractArray.length; i++)
					createdarray.push(`Contract  - ${response.data.main.contractArray[i].contractno}`);

				for (var i = 0; i < response.data.main.servicecallArray.length; i++)
					createdarray.push(`Service Call  - ${response.data.main.servicecallArray[i].servicecallno}`);

				this.props.updateFormState(this.props.form, {
					equipmentwizardstatus : response.data.main.main.equipmentwizardstatus
				});

				var message = {
					header : 'Success!',
					body : "The following records are created for this delivery note",
					bodyArray : createdarray,
					btnArray : ['Ok'],
					btnTitle : ""
				};
				this.props.openModal(modalService['infoMethod'](message));
				this.props.closeModal();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	updateField(value, fieldname)  {
		let { resource } = this.state;
		resource[fieldname] = value
		this.setState({resource});
	}

	updateChildField(item, value, fieldname)  {
		item[fieldname] = value;
		this.setState({
			resource: this.state.resource
		});
	}

	render() {
		let btndisabled = false;
		if(!this.state.resource)
			return null;

		if(this.state.resource.isinstallationapplicable && !this.state.resource.servicecallnumberingseriesmasterid)
			btndisabled = true;

		if(this.state.resource.iswarrantyapplicable && !this.state.resource.contractnumberingseriesmasterid)
			btndisabled = true;

		if(!this.state.resource.contractcreatemode || !this.state.resource.contractstartdate)
			btndisabled = true;

		if(this.state.resource.equipmentitems && this.state.resource.equipmentitems.length > 0) {
			for(var i = 0; i < this.state.resource.equipmentitems.length; i++) {
				if(!this.state.resource.equipmentitems[i].serialno) {
					btndisabled = true;
					break;
				}
			}
		}
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Contract/Installation Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Delivery Note No</label>
							<span className="form-control un-editable">{this.state.resource.deliverynotenumber}</span>
						</div>
						<div className="form-group col-md-3 col-sm-6" style={{marginTop:'25px'}}>
							<label className="form-check-label">
								<input type="checkbox" className="form-check-input" checked={this.state.resource.isinstallationapplicable || false} onChange={(evt) => this.updateField(evt.target.checked, 'isinstallationapplicable')} />Installation Applicable
							</label>
						</div>
						<div className="form-group col-md-3 col-sm-6" style={{marginTop:'25px'}}>
							<label className="form-check-label">
								<input type="checkbox" className="form-check-input" checked={this.state.resource.iswarrantyapplicable || false} onChange={(evt) => this.updateField(evt.target.checked, 'iswarrantyapplicable')} />Warranty Applicable
							</label>
						</div>
						{this.state.resource.isinstallationapplicable ? <div className="form-group col-md-3 col-sm-6">
							<label className='labelclass'>Service Call Series Type</label>
							<SelectAsync className={`${!this.state.resource.servicecallnumberingseriesmasterid ? 'errorinput' : ''}`} resource="numberingseriesmaster" fields="id,name,format,isdefault,currentvalue" filter="numberingseriesmaster.resource='servicecalls'" value={this.state.resource.servicecallnumberingseriesmasterid || null} onChange={(value)=>{this.updateField(value, 'servicecallnumberingseriesmasterid')}} required={true} />
						</div> : null }
						{this.state.resource.iswarrantyapplicable ? <div className="form-group col-md-3 col-sm-6">
							<label className='labelclass'>Contract Series Type</label>
							<SelectAsync className={`${!this.state.resource.contractnumberingseriesmasterid ? 'errorinput' : ''}`} resource="numberingseriesmaster" fields="id,name,format,isdefault,currentvalue" filter="numberingseriesmaster.resource='contracts'" value={this.state.resource.contractnumberingseriesmasterid || null} onChange={(value)=>{this.updateField(value, 'contractnumberingseriesmasterid')}} required={true} />
						</div> : null }
						<div className="form-group col-md-4 col-sm-6">
							<label className="labelclass">Contract Creation Mode</label>
							<LocalSelect className={`${!this.state.resource.contractcreatemode ? 'errorinput' : ''}`} options={["One contract for each contract type", "One contract for each duration"]} value={this.state.resource.contractcreatemode || null} onChange={(value)=>{this.updateField(value, 'contractcreatemode');}}  required={true} />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Contract Start Date</label>
							<DateElement className={`form-control ${!this.state.resource.contractstartdate ? 'errorinput' : ''}`} value={this.state.resource.contractstartdate} onChange={(val) => this.updateField(val, 'contractstartdate')} required={true} />
						</div>
					</div>

					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th className="text-center" style={{width: '20%'}}>Item Name</th>
										<th className="text-center" style={{width: '10%'}}>Installation Applicable</th>
										<th className="text-center" style={{width: '10%'}}>Warranty Applicable</th>
										<th className="text-center" style={{width: '15%'}}>Contract Type</th>
										<th className="text-center" style={{width: '20%'}}>Serial No</th>
										{this.state.showkititem ? <th className="text-center" style={{width: '20%'}}>Serial Nos</th> : null }
									</tr>
								</thead>
								<tbody>
									{this.state.resource.equipmentitems.map((item, index)=> {
										return (
											<tr key={index}>
												<td>{item.itemid_name}</td>
												<td className="text-center">{item.isinstallationapplicable==true ? 'Yes' : 'No'}</td>
												<td className="text-center">{item.iswarrantyapplicable==true ? 'Yes' : 'No'}</td>
												<td className="text-center">{item.itemid_contracttype}</td>
												<td>
													<textarea className={`form-control ${item.serialno == null || item.serialno == "" || item.serialno == undefined ? 'errorinput' : ''}`} value={item.serialno} placeholder="Enter Serial No" onChange={(e) => this.updateChildField(item, e.target.value, 'serialno')} required="true"/>
												</td>
												{this.state.showkititem ? <td className="text-center" style={{whiteSpace: 'initial'}}>{item.totalserialno}</td> : null }
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={()=>{this.saveEquipment('Create Equipment')}} disabled={btndisabled}><i className="fa fa-check"></i>Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
