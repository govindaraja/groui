import React, { Component } from 'react';
import { connect } from 'react-redux';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			totalweight: 0,
			totalvolume: 0
		};
	}

	componentWillMount() {
		let { totalweight, totalvolume } = this.state;
		for (var i = 0; i < this.props.analyseitemArray.length; i++) {
			totalweight += this.props.analyseitemArray[i].itemid_totalweight;
			totalvolume += this.props.analyseitemArray[i].itemid_totalvolume;
		}

		this.setState({totalweight, totalvolume});
	}

	close() {
		this.props.closeModal();
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Analyse Item Deatils</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-hover table-condensed">
								<thead>
								   <tr>
									  <th className="text-center">Item Name</th>
									  <th className="text-center">Total Weight</th>
									  <th className="text-center">Total Volume</th>
								   </tr>
								</thead>
								<tbody>
									{this.props.analyseitemArray.map((data, index)=> {
										return (
											<tr key={index}>
											   <td className="text-center">{data.itemid_name}</td>
											   <td className="text-center">{data.itemid_totalweight || 0}</td>
											   <td className="text-center">{data.itemid_totalvolume || 0}</td>
											</tr>
										);
									})}
									<tr>
										<td className="text-right"><b>Total</b></td>
										<td className="text-center"><b>{this.state.totalweight}</b></td>
										<td className="text-center"><b>{this.state.totalvolume}</b></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
