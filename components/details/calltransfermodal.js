import React, { Component } from 'react';
import {connect} from 'react-redux';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Call Transfer</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
						{
							(this.props.extensions && this.props.extensions.length > 0) ?
								<div className="table-responsive">
									<table className="table table-condensed">
										<thead>
											<tr>
												<th className="text-center">Name</th>
												<th className="text-center">Extension</th>
												<th className="text-center"></th>
											</tr>
										</thead>
										<tbody>
											{this.props.extensions.map((extension, key) => {
												return <tr key={key}>
													<td className="text-center">{extension.agentname}</td>
													<td className="text-center">{extension.extension}</td>
													<td className="text-center"><button type="button" className="btn btn-sm btn-default" onClick={()=>{this.props.closeModal();this.props.callTransfer(extension.extension)}}>Transfer</button></td>
												</tr>
											})}
										</tbody>
									</table>
								</div>
							: <div className="col-md-12 col-sm-12 col-xs-12 alert alert-warning text-center">
									No agents are currently available!
								</div>
						}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});