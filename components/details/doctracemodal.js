import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import { commonMethods, modalService } from '../../utils/services';
import { checkActionVerbAccess } from '../../utils/utils';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			parentresource: this.props.parentresource,
			parentid: this.props.parentid,
			removeResource : ['journalvouchers', 'competativequotes', 'landingcosts', 'stocktransfer', 'contacts']
		};
		this.onLoad();
		this.onLoad = this.onLoad.bind(this);
		this.openDocument = this.openDocument.bind(this);
	}

	onLoad() {
		axios.get(`/api/trace/${this.state.parentresource}/${this.state.parentid}`).then((res) => {
			if(res.data.message == 'success') {
				this.setState({
					documenttraceObj: res.data.main
				});
			} else {
				var apiResponse = commonMethods.apiResult(res);
				modalService[apiResponse.methodName](apiResponse.message);
			}
		});
	}

	openDocument(resourcename, displayname, id) {
		this.props.closeModal();
		let url = '';
		if(resourcename == 'journalvouchers') {
			if(displayname == 'Receipt Voucher' || displayname == 'Advance Receipt Voucher')
				url = `/details/receiptvouchers/${id}`;
			else if(displayname == 'Credit Notes')
				url = `/details/creditnotes/${id}`;
			else if(displayname == 'Debit Notes')
				url = `/details/debitnotes/${id}`;
			else if(displayname == 'Journal Voucher')
				url = `/details/journalvouchers/${id}`;
			else if(displayname == 'Payment Voucher' || displayname == 'Advance Payment Voucher')
				url = `/details/paymentvouchers/${id}`;
		} else if(resourcename == 'salesinvoices') {
			if(displayname == 'Contract Invoice')
				url = `/details/contractinvoices/${id}`;
			else if(displayname == 'Service Invoice')
				url = `/details/serviceinvoices/${id}`;
			else
				url = `/details/salesinvoices/${id}`;
		} else {
			url = `/details/${resourcename}/${id}`;
		}
		if(url != '')
			this.props.history.push(url);
	}

	trackDoc(parentresource, parentid) {
		this.setState({
			parentresource,
			parentid,
			documenttraceObj: null
		}, () => {
			this.onLoad();
		})
	}

	renderRow(index) {
		if(!this.state.documenttraceObj)
			return index == 1 ? 'loading' : null;

		let documenttraceObj = this.state.documenttraceObj;

		if(index > 1 && (!documenttraceObj.after[`row${index}`] || documenttraceObj.after[`row${index}`].length == 0))
			return null;

		return (
			<div className="float-left" style={{width:'100%', minWidth:'160%'}}>
				{index > 1 ? <hr></hr> : null}
				{index == 1 && documenttraceObj.before.length > 0 ?  this.renderBefore(documenttraceObj.before) : null}
				{index == 1 || documenttraceObj.after[`row${index}`].length > 0 ?  this.renderCurrent(documenttraceObj.current, index) : null}
				{documenttraceObj.after[`row${index}`].length > 0 ?  this.renderAfter(documenttraceObj.after[`row${index}`], index) : null}
			</div>
		)
	}

	renderBefore(beforeObj) {
		return beforeObj.map((item, index) => {
			let resourcename = item.resourcename;
			if(item.resourcename == 'journalvouchers') {
				if(item.displayname == 'Receipt Voucher' || item.displayname == 'Advance Receipt Voucher')
					resourcename = `receiptvouchers`;
				else if(item.displayname == 'Credit Notes')
					resourcename = `creditnotes`;
				else if(item.displayname == 'Debit Notes')
					resourcename = `debitnotes`;
				else if(item.displayname == 'Journal Voucher')
					resourcename = `journalvouchers`;
				else if(item.displayname == 'Payment Voucher' || item.displayname == 'Advance Payment Voucher')
					resourcename = `paymentvouchers`;
			}
			return (
				<div key={index}>
				<div className="cards float-left">
					{item.details.map((data, dataindex) => {
						return (
							<div className="card trace-card" key={dataindex}>
								<div className="card-body trace-card-padding">
									<div className="card-title" style={{marginBottom:'5px'}}>{item.displayname}</div>
									<div className="font-16 semi-bold-custom">{data.f2}</div>
									<span className="gs-text-color">{data.f3}</span>
								</div>
								{
									(checkActionVerbAccess(this.props.app,resourcename,'Menu')) ? 
										<div className="card-footer trace-card-footer-padding text-muted">
											<div className="row">
												<div className={`${this.state.removeResource.indexOf(item.resourcename) == -1 ? 'col-md-6 trace-btn-border paddingright-0' : 'col-md-12'}`}>
													<div className="trace-btn" onClick={() => this.openDocument(item.resourcename,item.displayname,data.f1)}><span className="fa fa-file-text-o marginright-5"></span>Open This</div>
												</div>
												{this.state.removeResource.indexOf(item.resourcename) == -1 ? <div className="col-md-6 paddingleft-0">
													<div className="trace-btn" onClick={() => this.trackDoc(item.resourcename, data.f1)}><span className="fa fa-share-alt marginright-5"></span>Trace This</div>
												</div> : null}
											</div>
										</div>
									: null
								}
							</div>
						)
					})}
				</div>
				<div className="trace-icon"><span className="fa fa-angle-double-left fa-2x"></span></div>
				</div>
			);
		});
	}

	renderCurrent(currentObj, rootindex) {
		return currentObj.map((item, index) => {
			return (
				<div className="cards float-left" key={index}>
					{item.details.map((data, dataindex) => {
						return (
							<div key={dataindex} className="card" style={{borderColor: '#1dbb99', borderWidth: '2px'}}>
								<div className="card-body trace-card-padding">
									<div className="card-title" style={{marginBottom:'5px'}}>{item.displayname}</div>
									<div className="font-16 semi-bold-custom">{data.f2}</div>
									<span className="gs-text-color">{data.f3}</span>
								</div>
							</div>
						)
					})}
				</div>
			);
		});
	}

	renderAfter(afterObj) {
		return afterObj.map((item, index) => {
			let resourcename = item.resourcename;
			if(item.resourcename == 'journalvouchers') {
				if(item.displayname == 'Receipt Voucher' || item.displayname == 'Advance Receipt Voucher')
					resourcename = `receiptvouchers`;
				else if(item.displayname == 'Credit Notes')
					resourcename = `creditnotes`;
				else if(item.displayname == 'Debit Notes')
					resourcename = `debitnotes`;
				else if(item.displayname == 'Journal Voucher')
					resourcename = `journalvouchers`;
				else if(item.displayname == 'Payment Voucher' || item.displayname == 'Advance Payment Voucher')
					resourcename = `paymentvouchers`;
			}
			return (
				<div key={index}>
				<div className="trace-icon"><span className="fa fa-angle-double-right fa-2x"></span></div>
				<div className="cards float-left">
					{item.details.map((data, dataindex) => {
						return (
							<div className="card trace-card" key={dataindex}>
								<div className="card-body trace-card-padding">
									<div className="card-title" style={{marginBottom:'5px'}}>{item.displayname}</div>
									<div className="font-16 semi-bold-custom">{data.f2}</div>
									<span className="gs-text-color">{data.f3}</span>
								</div>
								{
									(checkActionVerbAccess(this.props.app,resourcename,'Menu')) ? 
										<div className="card-footer trace-card-footer-padding text-muted">
											<div className="row">
												<div className={`${this.state.removeResource.indexOf(item.resourcename) == -1 ? 'col-md-6 trace-btn-border paddingright-0' : 'col-md-12'}`}>
													<div className="trace-btn" onClick={() => this.openDocument(item.resourcename,item.displayname,data.f1)}><span className="fa fa-file-text-o marginright-5"></span>Open This</div>
												</div>
												{this.state.removeResource.indexOf(item.resourcename) == -1 ? <div className="col-md-6 paddingleft-0">
													<div className="trace-btn" onClick={() => this.trackDoc(item.resourcename, data.f1)}><span className="fa fa-share-alt marginright-5"></span>Trace This</div>
												</div> : ''}
											</div>
										</div>
									: null 
								}
							</div>
						)
					})}
				</div>
				</div>
			);
		});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title gs-text-color">Document Trace</h5>
				</div>
				<div className="react-modal-body modalmaxheight" style={{minHeight: '300px'}}>
					{this.renderRow(1)}
					{this.state.documenttraceObj && this.state.documenttraceObj.row2 && this.state.documenttraceObj.row2.length > 0 ? <div className="split-line"></div> : null}
					{this.renderRow(2)}
					{this.state.documenttraceObj && this.state.documenttraceObj.row3 && this.state.documenttraceObj.row3.length > 0 ? <div className="split-line"></div> : null}
					{this.renderRow(3)}
					{this.state.documenttraceObj && this.state.documenttraceObj.row4 && this.state.documenttraceObj.row4.length > 0 ? <div className="split-line"></div> : null}
					{this.renderRow(4)}
					{this.state.documenttraceObj && this.state.documenttraceObj.row5 && this.state.documenttraceObj.row5.length > 0 ? <div className="split-line"></div> : null}
					{this.renderRow(5)}
					{this.state.documenttraceObj && this.state.documenttraceObj.row6 && this.state.documenttraceObj.row6.length > 0 ? <div className="split-line"></div> : null}
					{this.renderRow(6)}
					{this.state.documenttraceObj && this.state.documenttraceObj.row7 && this.state.documenttraceObj.row7.length > 0 ? <div className="split-line"></div> : null}
					{this.renderRow(7)}
					{this.state.documenttraceObj && this.state.documenttraceObj.row8 && this.state.documenttraceObj.row8.length > 0 ? <div className="split-line"></div> : null}
					{this.renderRow(8)}
					{this.state.documenttraceObj && this.state.documenttraceObj.row9 && this.state.documenttraceObj.row9.length > 0 ? <div className="split-line"></div> : null}
					{this.renderRow(9)}

				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
