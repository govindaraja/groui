import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { currencyFilter, dateFilter } from '../../utils/filter';
import { commonMethods, modalService, checkAccountBalance } from '../../utils/services';
import { SelectAsync, NumberElement } from '../utilcomponents';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			expenserequestsettlementitems: null
		};
		this.onLoad = this.onLoad.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
		this.componentOnChange = this.componentOnChange.bind(this);
		this.addComponent = this.addComponent.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {

		let expenserequestsettlementitems = [];

		if (this.props.resource.employeeid && this.props.resource.currencyid) {
			axios.get(`/api/query/getemployeeoutstandingsquery?type=Receipt&companyid=${this.props.resource.companyid}&currencyid=${this.props.resource.currencyid}&employeeid=${this.props.resource.employeeid}`).then((response) => {
				if (response.data.message == 'success') {
					for (var i = 0; i < response.data.main.length; i++)
						response.data.main[i].credit = response.data.main[i].balance;

					let payrollitems = [...this.props.resource.payrollitems];

					payrollitems.forEach((item) => {
						if (item.type == 'Recovery') {
							response.data.main.forEach((recoveryItem) => {
								if ((item.xexpenserequestid > 0 && item.xexpenserequestid == recoveryItem.xexpenserequestid) || (item.xjournalvoucherid > 0 && item.xjournalvoucherid == recoveryItem.xjournalvoucherid)) {
									recoveryItem.checked = true;
									recoveryItem.credit = item.value;
									recoveryItem.componentid = item.componentid;
									recoveryItem.componentid_name = item.componentid_name,
									recoveryItem.type = item.type;
									recoveryItem.componentid_calculationtype = item.componentid_calculationtype;
									recoveryItem.tdsapplicable = item.tdsapplicable;
									recoveryItem.taxexempt = item.taxexempt;
									recoveryItem.pfapplicable = item.pfapplicable;
									recoveryItem.esiapplicable = item.esiapplicable;
									recoveryItem.componentid_accountid = item.componentid_accountid;
									recoveryItem.componentid_roundoffmethod = item.componentid_roundoffmethod;
									recoveryItem.accountid = item.accountid;
								}
							});
						}
					});

					this.setState({
						expenserequestsettlementitems: response.data.main
					});
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}
	}

	inputOnChange(value, field, index) {
		let { expenserequestsettlementitems } = this.state;

		expenserequestsettlementitems[index][field] = value;

		this.setState({
			expenserequestsettlementitems
		})
	}

	componentOnChange (value, valueobj, index) {
		let { expenserequestsettlementitems } = this.state;

		expenserequestsettlementitems[index].componentid = value;
		expenserequestsettlementitems[index].componentid_name = valueobj.name;
		expenserequestsettlementitems[index].type = valueobj.type;
		expenserequestsettlementitems[index].componentid_calculationtype = valueobj.calculationtype;
		expenserequestsettlementitems[index].tdsapplicable = false;
		expenserequestsettlementitems[index].taxexempt = false;
		expenserequestsettlementitems[index].pfapplicable = false;
		expenserequestsettlementitems[index].esiapplicable = false;
		expenserequestsettlementitems[index].componentid_accountid = valueobj.accountid;
		expenserequestsettlementitems[index].componentid_roundoffmethod = valueobj.roundoffmethod;
		expenserequestsettlementitems[index].value = null;
		expenserequestsettlementitems[index].defaultvalue = null;
		expenserequestsettlementitems[index].ismanuallyoverride = false;

		this.setState({
			expenserequestsettlementitems
		});
	}

	addComponent () {
		let payrollitems = [...this.props.resource.payrollitems];

		this.state.expenserequestsettlementitems.forEach((item) => {
			if (item.checked) {
				let itemFound = false;

				for (let i = 0; i < payrollitems.length; i++) {
					if(payrollitems[i].type == 'Recovery' && ((item.xexpenserequestid > 0 && item.xexpenserequestid == payrollitems[i].xexpenserequestid) || (item.xjournalvoucherid > 0 && item.xjournalvoucherid == payrollitems[i].xjournalvoucherid))) {
						payrollitems[i].componentid = item.componentid;
						payrollitems[i].componentid_name = item.componentid_name;
						payrollitems[i].value = item.credit;
						payrollitems[i].defaultvalue = item.credit;

						itemFound = true;
						break;
					}
				}

				if (!itemFound) {
					payrollitems.push({
						componentid: item.componentid,
						componentid_name: item.componentid_name,
						type: item.type,
						componentid_calculationtype: item.componentid_calculationtype,
						tdsapplicable: item.tdsapplicable,
						taxexempt: item.taxexempt,
						pfapplicable: item.pfapplicable,
						esiapplicable: item.esiapplicable,
						componentid_accountid: item.componentid_accountid,
						componentid_roundoffmethod: item.componentid_roundoffmethod,
						value: item.credit,
						defaultvalue: item.credit,
						ismanuallyoverride: item.ismanuallyoverride,
						accountid: item.accountid,
						xexpenserequestid: item.xexpenserequestid,
						xexpenserequestid_expensedate: item.xexpenserequestid_expensedate,
						xexpenserequestid_expenserequestno: item.xexpenserequestid_expenserequestno,
						xjournalvoucherid: item.xjournalvoucherid,
						xjournalvoucherid_voucherdate: item.xjournalvoucherid_voucherdate,
						xjournalvoucherid_voucherno: item.xjournalvoucherid_voucherno,
						xpayrollid: item.xpayrollid,
						xpayrollid_payrolldate: item.xpayrollid_payrolldate,
						xpayrollid_payrollno: item.xpayrollid_payrollno
					});
				}
			} else {
				for (let i = 0; i < payrollitems.length; i++) {
					if(payrollitems[i].type == 'Recovery' && ((item.xexpenserequestid > 0 && item.xexpenserequestid == payrollitems[i].xexpenserequestid) || (item.xjournalvoucherid > 0 && item.xjournalvoucherid == payrollitems[i].xjournalvoucherid))) {
						payrollitems.splice(i, 1);
						break;
					}
				}
			}
		});

		this.props.callback(payrollitems);
		this.props.closeModal();
	}

	render() {
		let isValid = false;
		let { expenserequestsettlementitems } = this.state;

		if (expenserequestsettlementitems && expenserequestsettlementitems.length > 0) {
			for (let i = 0; i < expenserequestsettlementitems.length; i++) {
				if (expenserequestsettlementitems[i].checked && !expenserequestsettlementitems[i].componentid || !expenserequestsettlementitems[i].credit) {
					isValid = true;
					break;
				}
			}
		}

		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Add Recovery Payroll Components</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						{(expenserequestsettlementitems && expenserequestsettlementitems.length > 0) ? <div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th className="text-center"></th>
										<th className="text-center">Voucher Type</th>
										<th className="text-center">Voucher No</th>
										<th className="text-center" style={{whiteSpace:'nowrap'}}>Voucher Date</th>
										<th className="text-center" style={{whiteSpace:'nowrap'}}>Balance</th>
										<th className="text-center" style={{width:'250px'}}>Component</th>
										<th className="text-center" style={{width:'200px', whiteSpace:'nowrap'}}>{`Amount (${this.props.app.currency[this.props.resource.currencyid].symbol})`}</th>
									</tr>
								</thead>
								<tbody>
									{expenserequestsettlementitems.map((item, index)=> {
										return (
											<tr key={index}>
												<td><input type="checkbox" onChange={(evt) => this.inputOnChange(evt.target.checked, 'checked', index)}  checked={item.checked || false} /></td>
												<td>{item.vouchertype}</td>
												<td>{item.xexpenserequestid ? item.xexpenserequestid_expenserequestno : item.xjournalvoucherid_voucherno}</td>
												<td>{(item.xexpenserequestid ? dateFilter(item.xexpenserequestid_expensedate) : dateFilter(item.xjournalvoucherid_voucherdate))}</td>
												<td>{currencyFilter(item.balance, this.props.resource.currencyid, this.props.app)}</td>
												<td>
													<SelectAsync className={`${item.checked && !item.componentid ? 'errorinput' : ''}`} resource="payrollcomponents" fields="id,name,type,category,statutorytype,accountid,accrualaccountid,calculationtype,tdsapplicable,taxexempt,pfapplicable,esiapplicable,utilitymoduleid,roundoffmethod" filter="payrollcomponents.type = 'Recovery'" value={item.componentid || null} onChange={(value, valueobj)=>{this.componentOnChange(value, valueobj, index)}} required={true} />
												</td>
												<td>
													<NumberElement className={`form-control ${item.checked && !item.credit ? 'errorinput' : ''}`} value={item.credit} onChange={(value) => this.inputOnChange(value, 'credit', index)} required={true} />
												</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div> : null }
						{ expenserequestsettlementitems && expenserequestsettlementitems.length == 0 ? <div className="col-md-12">
							<div className="alert alert-info text-center">Sorry, there is no Recovery Items for this employee</div>
						</div> : null}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addComponent} disabled={isValid}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});