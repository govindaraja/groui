import React, { Component } from 'react';
import axios from 'axios';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			search: {},
			resourceFields : this.props.state.resourceFields,
			resource:  this.props.resource.resource,
			tabActive: this.props.resource.resource == 'contacts' ? 'Contacts' : 'Main',
			showadd : false
		};

		this.inputonChange = this.inputonChange.bind(this);
		this.iconOnChange = this.iconOnChange.bind(this);
		this.copyToClipBoard = this.copyToClipBoard.bind(this);
		this.selectkeyvalue = this.selectkeyvalue.bind(this);
	};

	componentWillReceiveProps(nextprops) {
		this.setState({
			resourceFields:  nextprops.state.resourceFields
		});
		if(nextprops.resource.resource != this.state.resource) {
			this.setState({
				resource:  nextprops.resource.resource,
				tabActive: nextprops.resource.resource == 'contacts' ? 'Contacts' : 'Main',
			});
		}
	}

	inputonChange(evt, keyvalue) {
		let { search } = this.state;
		search[evt.target.name] = evt.target.value;

		this.setState({search});
	}

	copyToClipBoard(param) {
		param.copied = true;
		this.setState({ resourceFields:  this.state.resourceFields }, () => {
			setTimeout(() => {
				param.copied = false;
				this.setState({ resourceFields:  this.state.resourceFields });
			}, 2000);
		});
		let copyElement = document.createElement("textarea");
		copyElement.value = `{{${param.field}}}`;
		document.body.appendChild(copyElement);
		copyElement.select();

		document.execCommand('copy');
		document.body.removeChild(copyElement);
	}

	filterFunction(array, search) {
		let tempArr = [];

		if(search == '' || search == null || search == undefined)
			return array;

		let filterArray = [];

		array.forEach((a) => {
			let string_a = (`${a.field}`).toLowerCase();
			let searchString = search.toLowerCase();
			if(string_a.indexOf(searchString) >= 0)
				filterArray.push(a);
		});

		return filterArray;
	}

	iconOnChange(searchField, keyvalue) {
		let { resourceFields, showadd } = this.state;

		showadd = !showadd;

		for(let i = 0; i < resourceFields[keyvalue].length; i++) {
			if(resourceFields[keyvalue][i].field == searchField) {
				resourceFields[keyvalue][i].showadd = showadd;
				break;
			}
		}

		this.setState({
			resourceFields,
			showadd
		});
	}

	selectkeyvalue(keyvalue) {
		this.setState({
			tabActive: keyvalue
		});
	}

	render() {

		let { tabActive } = this.state;

		if(Object.keys(this.state.resourceFields).length == 0)
			return null;

		return (
			<div className="col-md-12">
				{ (this.props.resource.printmodules && this.props.resource.printmodules.length > 0 && this.props.state.moduleObj) ?
					(
						this.props.resource.printmodules.map((module, index) => {
							if(!this.props.state.moduleObj[module])
								return null;
							return <div className="col-md-12 col-sm-12 col-xs-12" key={index}>
								<blockquote style={{backgroundColor : 'rgb(248,249,250)', borderRadius : '10px', padding : '10px'}}>
									<b>{this.props.state.moduleObj[module].name}</b> : {this.props.state.moduleObj[module].description}
								</blockquote>
							</div>
						})
					)
				: null
				}
				<div className="row print-column">
					<div className="col-md-3" style={{padding:'0px'}}>
						<ul className="nav flex-column gs-print-tab">
							{ Object.keys(this.state.resourceFields).map((keyvalue, index) => {
								return <li className="nav-item" key={index}><a className={`nav-link ${tabActive == keyvalue ? 'active'  : 'gs-print-tab-inactive'} gs-print-navlinkitem`}  onClick={()=>this.selectkeyvalue(keyvalue)}>{keyvalue}</a></li>
							})}
						</ul>
					</div>
					<div className="col-md-9">
						<div className="tab-content">
							{ Object.keys(this.state.resourceFields).map((keyvalue, keyindex) => {
								return (<div className={`tab-pane fade ${tabActive == keyvalue ? 'active show'  : ''}`} key={keyindex}>
									<div style={{padding:'12px 25px 12px 12px'}}>
										<input type="text" className="form-control" name={keyvalue} value={this.state.search[keyvalue] || ''} placeholder="Search columns..." onChange={(evt) =>{this.inputonChange(evt, keyvalue)}}/>
									</div>
									<div className="col-md-12 tablerepeatdiv">
										<ul style={{paddingLeft:'0px'}}>		{this.filterFunction(this.state.resourceFields[keyvalue],this.state.search[keyvalue]).map((secprop, fieldindex) => { 
												return <li className="fieldli" key={fieldindex} onClick={(evt) => {this.copyToClipBoard(secprop);evt.stopPropagation();}}>
													<a data-toggle="tooltip" title="Click to Copy">{secprop.field}</a>
													<span className={`${secprop.copied ? 'show' : 'hide'} pull-right fa fa-check gs-print-tab-copied` }>Tag copied</span>
													<span className={`${secprop.field ? (secprop.copied ? 'hide' : 'show' ) :'' } pull-right gs-print-tab-copy`}>Copy tag</span>
													<a className={`${(secprop.additionalFields && secprop.additionalFields.length > 0) ? 'show' : 'hide'} pull-right`} onClick={(evt) => {this.iconOnChange(secprop.field, keyvalue);evt.stopPropagation();}}><span className={`${(!secprop.showadd && secprop.additionalFields && secprop.additionalFields.length > 0) ? 'show' : 'hide'} fa fa-chevron-down`} style={{marginRight:'5px'}}></span><span className={`${(secprop.showadd && secprop.additionalFields && secprop.additionalFields.length > 0) ? 'show' : 'hide'} fa fa-chevron-up`}></span></a>
													{(secprop.showadd && secprop.additionalFields && secprop.additionalFields.length > 0) ? <ul className="select-column-itemlevel">
														{secprop.additionalFields.map((thirdprop, thirdindex) => {
															return <li className="fieldlis" key={thirdindex} onClick={(evt) => {this.copyToClipBoard(thirdprop);evt.stopPropagation();}}><a data-toggle="tooltip" title="Click to Copy" >{thirdprop.field}</a><span className={`${thirdprop.copied ? 'show' : 'hide'} pull-right fa fa-check gs-print-tab-copied`}>Tag copied</span><span className={`${thirdprop.field ? (thirdprop.copied ? 'hide' : 'show' ) :'' } pull-right gs-print-tab-copy`}>Copy tag</span>
															</li>
															})}
													</ul> : null}
												</li>
											})}
										</ul>
									</div>
								</div>)
							})}
						</div>
					</div>
				</div>
			</div>
		);
	}
}