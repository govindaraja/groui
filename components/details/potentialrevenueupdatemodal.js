import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pipelinevalue: null
		};
		this.save = this.save.bind(this);
	}

	componentWillMount() {
		this.state.pipelinevalue = this.props.leadobj.potentialrevenue;
		this.state.leadcurrencyid = this.props.leadobj.currencyid;
		this.state.quoteflag = this.props.leadobj.quoteflag;
		this.state.currencydiffer = this.props.leadobj.currencydiffer;
	}

	save() {
		this.props.closeModal();
		this.props.callback(this.state.pipelinevalue);
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Update Pipeline value</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="row">
						<div className="col-md-12">
							<p className={`${!this.state.quoteflag && !this.state.currencydiffer ? 'show' : 'hide'}`}>You have updated the Lead value. Please enter the new pipeline value in</p>
							<p className={`${this.state.quoteflag && !this.state.currencydiffer ? 'show' : 'hide'}`}>You have updated the value of the quote. Please update the Potential Revenue for the lead in  </p>
							<p className={`${!this.state.quoteflag && this.state.currencydiffer ? 'show' : 'hide'}`}>Your quotation currency differs from lead currency. Please update pipeline value in </p>
						</div>
						<div className="col-md-12 col-sm-12 align-self-center">
							<div className="input-group mb-3">
								 <div className="input-group-prepend">
									<span className="input-group-text">{this.props.app.currency[this.state.leadcurrencyid].name}</span>
								</div>
								<input type="number" className="form-control" value={this.state.pipelinevalue} onChange={(e)=>{this.setState({pipelinevalue: e.target.value});}} required/>
							</div>

						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.save}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
