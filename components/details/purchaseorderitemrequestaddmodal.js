import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { LocalSelect, AutoSelect, NumberElement } from '../utilcomponents';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tabClasses: ["show active", ""],
			search: {},
			pendingItemRequestItemsArray: [],
			itemRequestItemsArray: []
		};

		this.onLoad = this.onLoad.bind(this);
		this.addItemreqitem = this.addItemreqitem.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
		this.quantityOnChange = this.quantityOnChange.bind(this);
		this.renderTable = this.renderTable.bind(this);
		this.getTabClass = this.getTabClass.bind(this);
		this.getTabPaneClass = this.getTabPaneClass.bind(this);
		this.setActiveTab = this.setActiveTab.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		axios.get(`api/query/projectpurchaseplannerquery?querytype=search&show=all&projectid=${this.props.resource.projectid}`).then((response) => {
			if (response.data.message == 'success') {
				let itemRequestItemsArray = response.data.main;
				let pendingItemRequestItemsArray = [];

				itemRequestItemsArray.forEach(item => {
					if(item.suggestedqty > 0)
						pendingItemRequestItemsArray.push(item);
				});

				for (var i = 0; i < this.props.resource[this.props.jsonname].length; i++) {
					for (var j = 0; j < itemRequestItemsArray.length; j++) {
						if (this.props.resource[this.props.jsonname][i].itemrequestitemsid == itemRequestItemsArray[j].itemrequestitemsid)
							itemRequestItemsArray[j].checked = true;
					}
				}

				this.setState({
					itemRequestItemsArray,
					pendingItemRequestItemsArray
				}, () => {
					this.setActiveTab(1);
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	addItemreqitem() {
		let errArr = [];

		/*this.state.itemRequestItemsArray.forEach(item => {
			if(item.checked && item.suggestedqty <= 0)
				errArr.push(`Item : "${item.itemid_name}" quantity should be greater than 0`);
		});*/

		if(errArr.length > 0) {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : errArr,
				btnArray : ['Ok']
			}));
		} else {
			this.props.callback(this.state.itemRequestItemsArray);
			this.props.closeModal();
		}
	}

	inputonChange(value) {
		let { search } = this.state;
		search.itemid_name = value;
		this.setState({search});
	}

	checkboxOnChange(value, item) {
		item.checked = value;

		this.setState({
			itemRequestItemsArray: this.state.itemRequestItemsArray,
			pendingItemRequestItemsArray: this.state.pendingItemRequestItemsArray
		});
	}

	checkallonChange(value, itemarr) {
		this.state[itemarr].forEach((item) => {
			item.checked = value;
		});
		this.setState({	[itemarr] : this.state[itemarr], checkall : value });
	}

	quantityOnChange(value, item) {
		item.suggestedqty = value;

		this.setState({
			itemRequestItemsArray: this.state.itemRequestItemsArray,
			pendingItemRequestItemsArray: this.state.pendingItemRequestItemsArray
		});
	}

	getTabClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	getTabPaneClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	setActiveTab(tabNum) {
		let { tabClasses } = this.state;
		tabClasses =  ["", ""]
		tabClasses[tabNum] = "show active";
		this.setState({tabClasses}, ()=>{
			this.getTabPaneClass(tabNum);
		});
	}

	renderTable(array, arrayname) {
		return (
			<table className="table table-bordered" >
				<thead>
					<tr>
						<th><input type="checkbox" checked={this.state.checkall || false} onChange={(evt)=>{this.checkallonChange(evt.target.checked, arrayname)}}/></th>
						<th>BOQ Internal Ref No</th>
						<th>Item Name</th>
						<th>Qty Requested</th>
						<th>PO Inprogress</th>
						<th>PO Raised</th>
						<th>PO Received</th>
						<th>Suggested Quantity</th>
					</tr>
				</thead>
				<tbody>
					{search(array, this.state.search).map((item, index) => {
						return (
							<tr key={index}>
								<td className="text-center"><input type="checkbox" checked={item.checked || false} onChange={(evt)=>{this.checkboxOnChange(evt.target.checked, item)}}/></td>
								<td>{item.boqitemsid_internalrefno}</td>
								<td>{item.itemid_name}</td>
								<td className="text-center">{item.qtyrequest} {item.uomid_name}</td>
								<td className="text-center">{item.poinprogress}</td>
								<td className="text-center">{item.poraised}</td>
								<td className="text-center">{item.poreceived}</td>
								<td className="text-center">{item.suggestedqty === 0 ? null : item.suggestedqty}</td>
								{/*<td className="text-center">
									<NumberElement className={`form-control`} value={item.suggestedqty} onChange={(value) => this.quantityOnChange(value, item)} />
								</td>*/}
							</tr>
						);
					})}
				</tbody>
			</table>
		);
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Item Request Details</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					{this.state.itemRequestItemsArray.length > 0 ? <div className="row">
						<div className="form-group col-md-12 col-sm-12 align-self-center">
							<label className="labelclass">Item Name</label>
							<input type="text" className="form-control" value={this.state.search.itemid_name || ''} placeholder="Search by item name" onChange={(evt)=>{this.inputonChange(evt.target.value)}}/>
						</div>
					</div> : null}
					<div className="row">
						<div className="col-md-12">
							<ul className="nav nav-tabs">
								<li className="nav-item">
									<a className={`nav-link ${this.getTabClass(1)}`} onClick={()=>this.setActiveTab(1)}>Pending Items</a>
								</li>
								<li className="nav-item">
									<a className={`nav-link ${this.getTabClass(2)}`} onClick={()=>this.setActiveTab(2)} >All Items</a>
								</li>
							</ul>
							<div className="tab-content">
								<div className={`tab-pane fade ${this.getTabPaneClass(1)}`} role="tabpanel">
									<div className="row margintop-25">
										<div className="col-md-12 col-sm-12 col-xs-12">
											{this.state.pendingItemRequestItemsArray.length > 0 ? this.renderTable(this.state.pendingItemRequestItemsArray, 'pendingItemRequestItemsArray') : <div className="alert alert-info text-center">There is no request pending items</div>}
										</div>
									</div>
								</div>
								<div className={`tab-pane fade ${this.getTabPaneClass(2)}`} role="tabpanel">
									<div className="row margintop-25">
										<div className="col-md-12 col-sm-12 col-xs-12">
											{this.renderTable(this.state.itemRequestItemsArray, 'itemRequestItemsArray')}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addItemreqitem}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});