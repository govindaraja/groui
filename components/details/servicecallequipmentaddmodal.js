import React, { Component } from 'react';
import axios from 'axios';

import { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect } from '../utilcomponents';
import { dateFilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: {
				equipmentaddressid: this.props.resource.installationaddressid ? this.props.resource.installationaddressid : null
			},
			equipmentArray: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.selectAllEquipment = this.selectAllEquipment.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.addressonchange = this.addressonchange.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.add = this.add.bind(this);
		this.addEquipment = this.addEquipment.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let equipmentArray = [];
		if(!this.props.resource.customerid)
			return null;

		if(this.props.resourcename != 'servicereports'){
			axios.get(`/api/query/equipmentpickingquery?customerid=${this.props.resource.customerid}`).then((response) => {
				if (response.data.message == 'success') {
					this.props.resource[this.props.childresourcename].map((a) => {
						response.data.main.map((b) => {
							if(a.equipmentid == b.id)
								b.checked = true;
							if(a.oldcontracttypeid && a.oldcontracttypeid.length > 0)
								b.disabled = true;
						});
					});

					response.data.main.forEach(item => {
						if(['contractenquiries', 'contracts'].includes(this.props.resourcename)) {
							if(item.type == 'Equipment' && this.props.resource.contractfor == 'Equipment')
								equipmentArray.push(item);
							else if(item.type == 'Facility' && (this.props.resource.contractfor == 'Facility' || this.props.resource.contractfor == 'O & M'))
								equipmentArray.push(item);
						} else
							equipmentArray.push(item);
					});

					this.setState({ equipmentArray });
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}else{
			let equipmentCustomField = this.props.getCustomFields('equipmentitems', 'string', false, this.props.app);
			
			axios.get(`/api/equipmentitems?field=id,equipmentid,equipments/displayname/equipmentid,equipments/description/equipmentid,equipments/serialno/equipmentid,equipments/location/equipmentid,equipments/remarks/equipmentid,equipments/equipmentaddressid/equipmentid,equipments/equipmentaddress/equipmentid,equipments/installeddate/equipmentid,equipments/invoicedate/equipmentid,contractid,contracts/contractno/contractid,contracts/startdate/contractid,contracts/expiredate/contractid,contracttypeid,contracttypes/name/contracttypeid,scheduleid${(equipmentCustomField ? ','+equipmentCustomField : '' )}&filtercondition=equipmentitems.parentresource = 'servicecalls' and equipmentitems.parentid=${this.props.resource.servicecallid}`).then((response)=> {
				if (response.data.message == 'success') {
					response.data.main.map((b) => {
						b.id = b.equipmentid;
						b.displayname = b.equipmentid_displayname;
						b.serialno = b.equipmentid_serialno;
						b.location = b.equipmentid_location;
						b.remarks = b.equipmentid_remarks;
						b.equipmentaddress = b.equipmentid_equipmentaddress;
						b.equipmentaddressid = b.equipmentid_equipmentaddressid;
						b.installeddate = b.equipmentid_installeddate;
						b.invoicedate = b.equipmentid_invoicedate;
						b.contractno = b.contractid_contractno;
						b.startdate = b.contractid_startdate;
						b.expiredate = b.contractid_expiredate;

						this.props.resource[this.props.childresourcename].map((a) => {
							if(a.equipmentid == b.equipmentid)
								b.checked = true;
						});

					});

					this.setState({
						equipmentArray: response.data.main
					});	
					
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}	
	}

	addressonchange(id, valueobj){
		this.state.search.equipmentaddressid = id;
		this.setState({
			search: this.state.search
		});
	}

	selectAllEquipment(value) {
		let { equipmentArray } = this.state;
		let searchEquipment = search(this.state.equipmentArray, this.state.search);

		if(searchEquipment.length == equipmentArray)
			equipmentArray.map((item) => {
				item.checked = value;
			});
		else {
			equipmentArray.map((item) => {
				searchEquipment.map((searchItem) => {
					if(item.id == searchItem.id)
						item.checked = value;
				});
			});
		}

		this.setState({equipmentArray});
	}

	checkboxOnChange(value, item) {
		let { equipmentArray } = this.state;

		for(let i = 0; i < equipmentArray.length; i++) {
			if(equipmentArray[i].id == item.id) {
				equipmentArray[i].checked = value;
				break;
			}
		}

		this.setState({equipmentArray});
	}

	inputonChange(evt) {
		let { search } = this.state;
		search[evt.target.name] = evt.target.value;
		this.setState({search});
	}

	add() {
		if(this.props.resourcename != 'servicecalls')
			return this.addEquipment();

		let tempContractArr = [];
		for (let i = 0; i < this.state.equipmentArray.length; i++) {
			if(this.state.equipmentArray[i].checked && tempContractArr.indexOf(this.state.equipmentArray[i].contractid) == -1) {
				tempContractArr.push(this.state.equipmentArray[i].contractid);
			}
		}

		if(tempContractArr.length > 1) {
			let message = {
				header : 'Different Contract Type !!',
				body : 'You are adding Equipments with different Contracts. We recommend creating separate Service Call for each Contract. \n\n Would you like to continue anyway ? ',
				btnArray : ['Yes, Continue', 'No']
			};

			this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if(param) {
					this.addEquipment();
				}
			}));
		} else {
			this.addEquipment();
		}
	}

	addEquipment() {
		let childItems = [...this.props.resource[this.props.childresourcename]];
		let serviceCallObj = {};

		for (let i = 0; i < this.state.equipmentArray.length; i++) {
			let itemFound = false;
			for (let j = 0; j < childItems.length; j++) {
				if (this.state.equipmentArray[i].id == childItems[j].equipmentid) {
					itemFound = true;
					if (!this.state.equipmentArray[i].checked)
						childItems.splice(j, 1);
				}
			}

			if (!itemFound && this.state.equipmentArray[i].checked) {

				let tempObj;

				if(this.props.resourcename == 'contractenquiries') {
					tempObj = {
						equipmentid : this.state.equipmentArray[i].id,
						equipmentid_serialno : this.state.equipmentArray[i].serialno,
						equipmentid_type : this.state.equipmentArray[i].type,
						itemid : this.state.equipmentArray[i].itemid,
						equipmentid_displayname : this.state.equipmentArray[i].displayname,
						description : this.state.equipmentArray[i].description,
						itemid : this.state.equipmentArray[i].itemid,
						rate : (this.props.resource.newcontracttypeid && this.props.resource.newcontracttypeid.length == 1) ? (this.props.contractObj[this.props.resource.newcontracttypeid[0]].type == 'Warranty' ? 0 : this.props.contractObj[this.props.resource.newcontracttypeid[0]].rate) : null,
						newcontracttypeid : (this.props.resource.newcontracttypeid && this.props.resource.newcontracttypeid.length == 1) ? this.props.resource.newcontracttypeid[0] : null,
						oldcontracttypeid : (!this.props.resource.contractid && this.props.resource.oldcontracttypeid && this.props.resource.oldcontracttypeid.length == 1) ? this.props.resource.oldcontracttypeid[0] : null,
						taxid : (this.props.resource.taxid && this.props.resource.taxid.length > 0)  ? this.props.resource.taxid : [],
						quantity: 1
					}
					this.props.customFieldsOperation('equipments', tempObj, this.state.equipmentArray[i], 'contractenquiryitems');

					tempObj.taxid = tempObj.taxid.length == 0 ? this.props.defaultHSNCodeTax(tempObj) : tempObj.taxid;

					if(this.props.app.feature.useCapacityInContractPricing && this.state.equipmentArray[i].itemid_capacityfield) {
						tempObj.capacityfield = this.state.equipmentArray[i].itemid_capacityfield;
						tempObj.capacity = this.state.equipmentArray[i][this.state.equipmentArray[i].itemid_capacityfield];
						tempObj.quantity = this.state.equipmentArray[i][this.state.equipmentArray[i].itemid_capacityfield] ? this.state.equipmentArray[i][this.state.equipmentArray[i].itemid_capacityfield] : 1;
					}
				} else if (this.props.resourcename == 'contracts') {
					tempObj = {
						equipmentid : this.state.equipmentArray[i].id,
						equipmentid_itemid : this.state.equipmentArray[i].itemid,
						equipmentid_displayname : this.state.equipmentArray[i].displayname,
						equipmentid_type : this.state.equipmentArray[i].type,
						description : this.state.equipmentArray[i].description,
						equipmentid_serialno : this.state.equipmentArray[i].serialno,
						equipmentid_invoiceno : this.state.equipmentArray[i].invoiceno,
						rate : (this.props.resource.contracttypeid && this.props.resource.contracttypeid.length == 1) ? (this.props.contractObj[this.props.resource.contracttypeid[0]].type == 'Warranty' ? 0 : this.props.contractObj[this.props.resource.contracttypeid[0]].rate) : null,
						equipmentid_invoicedate : this.state.equipmentArray[i].invoicedate,
						contracttypeid : (this.props.resource.contracttypeid && this.props.resource.contracttypeid.length == 1) ? this.props.resource.contracttypeid[0] : null,
						taxid : (this.props.resource.taxid && this.props.resource.taxid.length > 0)  ? this.props.resource.taxid : [],
						quantity: 1
					}

					this.props.customFieldsOperation('equipments', tempObj, this.state.equipmentArray[i], 'contractitems');

					tempObj.taxid = tempObj.taxid.length == 0 ? this.props.defaultHSNCodeTax(tempObj) : tempObj.taxid;

					if(this.props.app.feature.useCapacityInContractPricing && this.state.equipmentArray[i].itemid_capacityfield) {
						tempObj.capacityfield = this.state.equipmentArray[i].itemid_capacityfield;
						tempObj.capacity = this.state.equipmentArray[i][this.state.equipmentArray[i].itemid_capacityfield];
						tempObj.quantity = this.state.equipmentArray[i][this.state.equipmentArray[i].itemid_capacityfield] ? this.state.equipmentArray[i][this.state.equipmentArray[i].itemid_capacityfield] : 1;
					}

				} else if(this.props.resourcename == 'servicecalls') {
					tempObj = {
						equipmentid : this.state.equipmentArray[i].id,
						equipmentid_displayname : this.state.equipmentArray[i].displayname,
						equipmentid_type : this.state.equipmentArray[i].type,
						equipmentid_description : this.state.equipmentArray[i].description,
						equipmentid_serialno : this.state.equipmentArray[i].serialno,
						contracttypeid : this.state.equipmentArray[i].contracttypeid,
						contracttypeid_name : this.state.equipmentArray[i].contracttypeid_name,
						contractid : this.state.equipmentArray[i].contractid,
						contractid_contractno : this.state.equipmentArray[i].contractno,
						contractid_startdate : this.state.equipmentArray[i].startdate,
						contractid_expiredate : this.state.equipmentArray[i].expiredate,
						equipmentid_invoicedate : this.state.equipmentArray[i].invoicedate,
						equipmentid_installeddate : this.state.equipmentArray[i].installeddate
					}

					this.props.customFieldsOperation('equipments', tempObj, this.state.equipmentArray[i], 'equipmentitems');
				} else if(this.props.resourcename == 'servicereports') {
					tempObj = {
						equipmentid : this.state.equipmentArray[i].equipmentid,
						equipmentid_displayname : this.state.equipmentArray[i].equipmentid_displayname,
						equipmentid_type : this.state.equipmentArray[i].type,
						equipmentid_serialno : this.state.equipmentArray[i].equipmentid_serialno,
						equipmentid_description : this.state.equipmentArray[i].equipmentid_description,
						contracttypeid : this.state.equipmentArray[i].contracttypeid,
						contracttypeid_name : this.state.equipmentArray[i].contracttypeid_name,
						contractid : this.state.equipmentArray[i].contractid,
						contractid_contractno : this.state.equipmentArray[i].contractid_contractno,
						scheduleid : this.state.equipmentArray[i].scheduleid
					}

					this.props.customFieldsOperation('equipments', tempObj, this.state.equipmentArray[i], 'equipmentitems');
				}

				childItems.push(tempObj);
			}
			if (this.state.equipmentArray[i].checked && this.props.resourcename == 'servicecalls') {

				if (!this.props.resource.installationaddressid) {
					if (this.state.equipmentArray[i].installationaddressid > 0) {
						serviceCallObj['installationaddressid'] = this.state.equipmentArray[i].installationaddressid;
						serviceCallObj['installationaddress'] = this.state.equipmentArray[i].installationaddress;
					} else if (this.state.equipmentArray[i].equipmentaddressid > 0) {
						serviceCallObj['installationaddressid'] = this.state.equipmentArray[i].equipmentaddressid;
						serviceCallObj['installationaddress'] = this.state.equipmentArray[i].equipmentaddress;
					} else if (this.state.equipmentArray[i].primaryaddressid > 0) {
						serviceCallObj['installationaddressid'] = this.state.equipmentArray[i].primaryaddressid;
						serviceCallObj['installationaddress'] = this.state.equipmentArray[i].primaryaddress;
					}
				}

				if (!this.props.resource.addressid) {
					if (this.state.equipmentArray[i].billingaddressid > 0) {
						serviceCallObj['addressid'] = this.state.equipmentArray[i].billingaddressid;
						serviceCallObj['address'] = this.state.equipmentArray[i].billingaddress;
					} else if (this.state.equipmentArray[i].primaryaddressid > 0) {
						serviceCallObj['addressid'] = this.state.equipmentArray[i].primaryaddressid;
						serviceCallObj['address'] = this.state.equipmentArray[i].primaryaddress;
					}
				}

				if (!this.props.resource.contactid) {
					if (this.state.equipmentArray[i].contactid > 0) {
						serviceCallObj['contactid'] = this.state.equipmentArray[i].contactid;
						serviceCallObj['contactperson'] = this.state.equipmentArray[i].contactperson;
						serviceCallObj['email'] = this.state.equipmentArray[i].email;
						serviceCallObj['phone'] = this.state.equipmentArray[i].phone;
						serviceCallObj['mobile'] = this.state.equipmentArray[i].mobile;
					} else if (this.state.equipmentArray[i].primarycontactid > 0) {
						serviceCallObj['contactid'] = this.state.equipmentArray[i].primarycontactid;
						serviceCallObj['contactperson'] = this.state.equipmentArray[i].primarycontactperson;
						serviceCallObj['email'] = this.state.equipmentArray[i].primaryemail;
						serviceCallObj['phone'] = this.state.equipmentArray[i].primaryphone;
						serviceCallObj['mobile'] = this.state.equipmentArray[i].primarymobile;
					}
				}

				if (!this.props.resource.installationcontactid) {
					if (this.state.equipmentArray[i].installationcontactid > 0) {
						serviceCallObj['installationcontactid'] = this.state.equipmentArray[i].installationcontactid;
						serviceCallObj['installationcontactperson'] = this.state.equipmentArray[i].installationcontactperson;
						serviceCallObj['installationemail'] = this.state.equipmentArray[i].installationemail;
						serviceCallObj['installationphone'] = this.state.equipmentArray[i].installationphone;
						serviceCallObj['installationmobile'] = this.state.equipmentArray[i].installationmobile;
					} else if (this.state.equipmentArray[i].primarycontactid > 0) {
						serviceCallObj['installationcontactid'] = this.state.equipmentArray[i].primarycontactid;
						serviceCallObj['installationcontactperson'] = this.state.equipmentArray[i].primarycontactperson;
						serviceCallObj['installationemail'] = this.state.equipmentArray[i].primaryemail;
						serviceCallObj['installationphone'] = this.state.equipmentArray[i].primaryphone;
						serviceCallObj['installationmobile'] = this.state.equipmentArray[i].primarymobile;
					}
				}
			}
		}

		let tempUpdateObj = {
			[this.props.childresourcename]: childItems,
			...serviceCallObj
		};

		this.props.updateFormState(this.props.form, tempUpdateObj);

		if(this.props.resourcename == 'contractenquiries' || this.props.resourcename == 'contracts')
			this.props.computeFinalRate();

		this.props.closeModal();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Pick Equipments</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className={`col-md-12 align-self-center text-center ${!this.props.resource.customerid ? 'show' : 'hide'}`}>
							<div className="alert alert-primary">Please select Customer, then add Equipments</div>
						</div>
					</div>
					<div className={`${this.props.resource.customerid ? 'show' : 'hide'}`}>
						<div className="row">
							<div className="col-md-3 form-group">
								<label className="labelclass">Installation Address</label>
								<AutoSelect resource={'addresses'} fields={'id,displayaddress'} value={this.state.search.equipmentaddressid} label={'displayaddress'} valuename={'id'} filter={`addresses.parentresource = 'partners' and addresses.parentid = ${this.props.resource.customerid}`} onChange={this.addressonchange} />
							</div>
							{this.props.resource.contractfor == 'Facility' || this.props.resource.contractfor == 'O & M' ? null : <div className="col-md-3 form-group">
								<label className="labelclass">Serial No</label>
								<input type="text" className="form-control" name="serialno" value={this.state.search.serialno || ''} placeholder="Search by serial no" onChange={(evt) =>{this.inputonChange(evt)}}/>
							</div>}
							{this.props.resource.contractfor == 'Facility' || this.props.resource.contractfor == 'O & M' ? null : <div className="col-md-3 form-group">
								<label className="labelclass">Location</label>
								<input type="text" className="form-control" name="location" value={this.state.search.location || ''} placeholder="Search by location" onChange={(evt) =>{this.inputonChange(evt)}}/>
							</div>}
							<div className="col-md-3 form-group">
								<label className="labelclass">Remarks</label>
								<input type="text" className="form-control" name="remarks" value={this.state.search.remarks || ''} placeholder="Search by remarks" onChange={(evt) =>{this.inputonChange(evt)}}/>
							</div>
						</div>

						<div className="row">
							<div className="col-md-12 col-sm-12">
								<table className="table table-bordered">
									<thead>
										<tr>
											{ ['contractenquiries', 'contracts','servicecalls','servicereports'].indexOf(this.props.resourcename) >= 0 ? <th style={{width:'10px'}} className="text-center"><input type="checkbox" onChange={(evt) =>{this.selectAllEquipment(evt.target.checked)}} /></th> : <th style={{width:'10px'}}></th>}
											<th className='text-center' style={{width:'150px'}}>Equipment</th>
											{this.props.resource.contractfor == 'Facility' || this.props.resource.contractfor == 'O & M' ? null : <th className='text-center' style={{width:'100px'}}>Serial No</th>}
											<th className='text-center' style={{width:'200px'}}>Contract Details</th>
											{this.props.resource.contractfor == 'Facility' || this.props.resource.contractfor == 'O & M' ? null : <th className='text-center' style={{width:'100px'}}>Location</th>}
											<th className='text-center' style={{width:'150px'}}>Remarks</th>
											<th className='text-center' style={{width:'150px'}}>Installation Address</th>
											<th className='text-center' style={{width:'150px'}}>Installed Date</th>
											{this.props.resource.contractfor == 'Facility' || this.props.resource.contractfor == 'O & M' ? null : <th className='text-center' style={{width:'150px'}}>Invoice Date</th>}
										</tr>
									</thead>
									<tbody>
										{search(this.state.equipmentArray, this.state.search).map((item, index) => {
											return (
												<tr key={index}>
													<td className="text-center"><input type="checkbox" checked={item.checked || false} disabled={item.disabled} onChange={(evt) =>{this.checkboxOnChange(evt.target.checked, item)}}/></td>
													<td>{item.displayname}</td>
													{this.props.resource.contractfor == 'Facility' || this.props.resource.contractfor == 'O & M' ? null : <td className="text-center">{item.serialno}</td>}
													<td>Contract Type : {item.contracttypeid_name}<br></br>Contract No : {item.contractno}<br></br>Start Date : {dateFilter(item.startdate)}<br></br>Expire Date :{dateFilter(item.expiredate)} </td>
													{this.props.resource.contractfor == 'Facility' || this.props.resource.contractfor == 'O & M' ? null : <td className="text-center">{item.location}</td>}
													<td className="text-center">{item.remarks}</td>
													<td>{item.equipmentaddress}</td>
													<td>{dateFilter(item.installeddate)}</td>
													{this.props.resource.contractfor == 'Facility' || this.props.resource.contractfor == 'O & M' ? null : <td>{dateFilter(item.invoicedate)}</td>}
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className={`btn btn-sm gs-btn-success btn-width ${this.props.resource.customerid ? 'show' : 'hide'}`} onClick={this.add}><i className="fa fa-check-square-o"></i>Add</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
