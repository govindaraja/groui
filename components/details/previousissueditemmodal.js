import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import Loadingcontainer from '../loadingcontainer';
import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';
import { AutoSelect, DateElement } from '../utilcomponents';
import moment from 'moment';
import { search } from '../../utils/utils';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			hasserial: false,
			equipmentissueditemrequestitems: [],
			search : {},
			filter : {
				itemid : this.props.item.itemid,
				itemid_name : this.props.item.itemid_name,
				fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 6)).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0,0,0,0))
			}
		};
		this.onLoad = this.onLoad.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.itemOnChange = this.itemOnChange.bind(this);
		this.dateOnChange = this.dateOnChange.bind(this);
		this.getData = this.getData.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let equipmentissueditemrequestitems = [],
		hasserial,
		fromdate = new Date(this.state.filter.fromdate).toDateString(),
		todate = new Date(this.state.filter.todate).toDateString();

		var queryString =  this.props.servicecallid > 0 ? `/api/query/equipmentitemrequestitemquery?equipmentid=${this.props.item.equipmentid}&customerid=${this.props.customerid}&parentid=${this.props.item.parentid}` : `/api/query/equipmentitemrequestitemquery?equipmentid=${this.props.item.equipmentid}&engineerid=${this.props.engineerid}&parentid=${this.props.item.parentid}`;

		queryString += `&fromdate='${fromdate}'&todate='${todate}'`;
		queryString += `&itemid=${this.state.filter.itemid}`

		axios.get(queryString).then((response)=> {
			if (response.data.message == 'success') {
				equipmentissueditemrequestitems = response.data.main;
				hasserial = response.data.hasserial ? true : false;
				this.setState({equipmentissueditemrequestitems, hasserial});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	itemOnChange(value, valueobj) {
		let { filter } = this.state;
		filter.itemid = value;
		filter.itemid_name = valueobj.name;
		this.setState({ filter });
	}

	dateOnChange (value, field) {
		let { filter } = this.state;
		filter[field] = value;
		this.setState({ filter });
	}

	getData () {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	render() {
		let { filter } = this.state;

		return (
			<div className="react-outer-modal">
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Previous Issued Item Details</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-3 form-group">
							<label className="labelclass">From Date</label>
							<DateElement className={`form-control ${!this.state.filter.fromdate ? 'errorinput' : ''}`} value={this.state.filter.fromdate} onChange={(value) => this.dateOnChange(value, 'fromdate')}  placeholder="From Date" />
						</div>
						<div className="col-md-3 form-group">
							<label className="labelclass">To Date</label>
							<DateElement className={`form-control ${!this.state.filter.todate ? 'errorinput' : ''}`} value={this.state.filter.todate} onChange={(value) => this.dateOnChange(value, 'todate')}  placeholder="To Date" min = {this.state.filter.fromdate ? moment(this.state.filter.fromdate) : null} />
						</div>
						<div className="col-md-3 form-group">
							<label className="labelclass">Item Name</label>
							<AutoSelect resource="itemmaster" fields="id,name,displayname" label="name" value={this.state.filter.itemid} onChange={(value, valueobj) => this.itemOnChange(value, valueobj)} />
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<button type="button" className="btn gs-btn-success btn-sm margintop-25" onClick={this.getData} disabled={!this.state.filter.fromdate || !this.state.filter.todate} ><i className="fa fa-search"></i>Go</button>
						</div>
					</div>
					<div className="row">
						{this.state.equipmentissueditemrequestitems.length == 0 ? <div className="col-md-12 col-sm-12 align-self-center">
							<div className="alert alert-warning">There is no previous issued item details</div>
						</div> : <div className="col-md-12">
							{this.state.hasserial ? <table className="table table-bordered" >
								<thead>
									<tr>
										<th className="text-center">Serial No</th>
										<th className="text-center">Issue Date</th>
										<th className="text-center">Warranty Expiry Date</th>
										<th className="text-center">Item Request No</th>
										<th className="text-center">Item Request Date</th>
										{this.props.servicecallid > 0 ? <th className="text-center">Equipment</th> : null}
										<th className="text-center">Item Name</th>
										{this.props.servicecallid > 0 ? <th className="text-center">Free or Chargeable</th> : null}
									</tr>
								</thead>
								<tbody>
									{this.state.equipmentissueditemrequestitems.map((item, index)=> {
										return (
											<tr key={index}>
												<td className="text-center">{item.serialno}</td>
												<td>{dateFilter(item.deliverynotedate)}</td>
												<td className="text-center">{dateFilter(item.expirydate)}</td>
												<td className="text-center">{item.itemrequestno}</td>
												<td className="text-center">{dateFilter(item.itemrequestdate)}</td>
												{this.props.servicecallid > 0 ? <td className="text-center">{item.equipmentid_displayname}</td> : null}
												<td className="text-center">{item.itemid_name}</td>
												{this.props.servicecallid > 0 ? <td className="text-center">{item.freeorchargeable}</td> : null}
											</tr>
										);
									})}
								</tbody>
							</table> : <table className="table table-bordered" >
								<thead>
									<tr>
										<th className="text-center">Item Request No</th>
										<th className="text-center">Item Request Date</th>
										{this.props.servicecallid > 0 ? <th className="text-center">Equipment</th> : null}
										<th className="text-center">Item Name<input type="text" className="form-control" value={this.state.search.itemid_name || ''} placeholder="Inline Search By Item" onChange={(evt) =>{this.setState({search: {...this.state.search, itemid_name: evt.target.value}})}}/></th>
										{this.props.servicecallid > 0 ? <th className="text-center">Free or Chargeable</th> : null}
										<th className="text-center">Requested Qty</th>
										<th className="text-center">Issued Qty</th>
										<th className="text-center">Consumed Qty</th>
										<th className="text-center">Returned Qty</th>
									</tr>
								</thead>
								<tbody>
									{search(this.state.equipmentissueditemrequestitems, this.state.search).map((item, index)=> {
										return (
											<tr key={index}>
												<td className="text-center">{item.itemrequestno}</td>
												<td>{dateFilter(item.itemrequestdate)}</td>
												{this.props.servicecallid > 0 ? <td className="text-center">{item.equipmentid_displayname}</td> : null}
												<td className="text-center">{item.itemid_name}</td>
												{this.props.servicecallid > 0 ? <td className="text-center">{item.freeorchargeable}</td> : null}
												<td className="text-center">{item.quantity}</td>
												<td className="text-center">{item.deliveredqty}</td>
												<td className="text-center">{item.usedqty}</td>
												<td className="text-center">{item.returnedqty}</td>
											</tr>
										);
									})}
								</tbody>
							</table>}
						</div>}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
