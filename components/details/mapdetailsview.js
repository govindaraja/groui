import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../formelements';
import { commonMethods, modalService } from '../../utils/services';
import { dateFilter, currencyFilter, timeFilter, datetimeFilter, dateAgoFilter } from '../../utils/filter';

export default class DetailedInfoWindow extends Component {
	constructor(props) {
		super(props);

		this.state = {
			collectionstatusDetails: [],
			paymentPerformance: [],
			collectionActivity: [],
			collectionrep:{},
			toalOutstanding: 0,
			toalOutstandingRef: '',
			[`${this.props.resourceName}Obj`]: {}
		};

		this.onLoad = this.onLoad.bind(this);
		this.getCollcetionDetails = this.getCollcetionDetails.bind(this);
		this.getCollectionActivities = this.getCollectionActivities.bind(this);
		this.getResourceDetails = this.getResourceDetails.bind(this);
		this.openLink = this.openLink.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({
			loaderflag
		});
	}

	onLoad () {
		if (this.props.rptparam == 'collectionstatusreport') {
			this.updateLoaderFlag(true);
			let { collectionrep } = this.state;

			collectionrep = {};

			axios.get(`/api/partners?field=id,name,displayname,users/displayname/collectionrep,users/email/collectionrep,users/contactno/collectionrep,users/imageurl/collectionrep&filtercondition=partners.id=${this.props.reference_id}`).then((response) => {
				if (response.data.message == 'success') {
					collectionrep = response.data.main[0];

					this.setState({
						collectionrep
					}, () => {
						this.getCollcetionDetails();
					});
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
				this.updateLoaderFlag(false);
			});
		}

		if (['leaditemsreport', 'contractitemsreport'].includes(this.props.rptparam))
			this.getResourceDetails();
	}

	getCollcetionDetails () {
		this.updateLoaderFlag(true);
		let { collectionstatusDetails, toalOutstanding, toalOutstandingRef } = this.state;

		collectionstatusDetails = [];

		let filterString = [`fromdate=${new Date(new Date(new Date().setMonth(new Date().getMonth() - 3)).setHours(0, 0, 0, 0))}`,`todate=${new Date(new Date().setHours(0, 0, 0, 0))}`,`companyid=${this.props.app.user.selectedcompanyid}`,`partnerid=${this.props.reference_id}`];

		axios.get(`/api/query/customerstatementsquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				collectionstatusDetails = response.data.main.result;
				let totalOutCredit = 0;
				let totalOutDebit = 0;
				toalOutstanding = 0;

				for(var i = 0; i < collectionstatusDetails.length; i++) {
					if(collectionstatusDetails[i].balcredit > 0)
						totalOutCredit += collectionstatusDetails[i].balcredit;
					if(collectionstatusDetails[i].baldebit > 0)
						totalOutDebit += collectionstatusDetails[i].baldebit;
				}
				if(totalOutCredit - totalOutDebit > 0) {
					toalOutstanding = totalOutCredit - totalOutDebit;
					toalOutstandingRef = "Cr";
				} else {
					toalOutstanding = totalOutDebit - totalOutCredit;
					toalOutstandingRef = "Dr";
				}

				this.setState({
					collectionstatusDetails: response.data.main.result,
					paymentPerformance: response.data.main.paymentperformance,
					toalOutstandingRef,
					toalOutstanding
				}, () => {
					this.getCollectionActivities();
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getCollectionActivities = (param) => {
		this.updateLoaderFlag(true);

		let { collectionActivity } = this.state;

		if (param == 'reload')
			collectionActivity = [];

		axios.get(`/api/collectionactivities?field=id,created,users/displayname/createdby,users/displayname/modifiedby,description,activitytype,followuptype,nextfollowup,completedon&pagelength=3&skip=${collectionActivity.length}&filtercondition=collectionactivities.parentresource='partners' and collectionactivities.parentid=${this.props.reference_id}`).then((response) => {
			if (response.data.message == 'success') {
				for(var i = 0; i < response.data.main.length; i++)
					collectionActivity.push(response.data.main[i]);

				this.setState({
					collectionActivity
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getResourceDetails () {
		this.updateLoaderFlag(true);

		axios.get(`/api/${this.props.resourceName}/${this.props.reference_id}`).then((response)=> {
			if (response.data.message == 'success')
				this.setState({
					[`${this.props.resourceName}Obj`]: response.data.main
				})
			else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	openLink = (item) => {
		let link = '';
		if(item.vouchertype == 'Sales Invoice')
			link = `/details/salesinvoices/${item.relatedid}`;
		if(item.vouchertype == 'Purchase Invoice')
			link = `/details/purchaseinvoices/${item.relatedid}`;
		if(item.vouchertype == 'Receipt')
			link = `/details/receiptvouchers/${item.relatedid}`;
		if(item.vouchertype == 'Payment')
			link = `/details/paymentvouchers/${item.relatedid}`;
		if(item.vouchertype == 'Journal Voucher')
			link = `/details/journalvouchers/${item.relatedid}`;
		if(item.vouchertype == 'Credit Note')
			link = `/details/creditnotes/${item.relatedid}`;
		if(item.vouchertype == 'Debit Note')
			link = `/details/debitnotes/${item.relatedid}`;

		this.props.history.push(link);
	};

	render() {
		let { collectionstatusDetails, collectionrep, toalOutstanding, collectionActivity} = this.state;

		let resourceObj = this.state[`${this.props.resourceName}Obj`];

		return (
			 <div>
			{this.props.rptparam == 'collectionstatusreport' && collectionstatusDetails.length > 0 ? <div className="col-md-12">
					<div className="form-group">
						<div>
							<b className="font-14">{collectionrep.name}</b>
						</div>
						<div>{currencyFilter(toalOutstanding, null, this.props.app)}</div>
						<div>{collectionrep.collectionrep_displayname}</div>
					</div>
					<div className="gs-md-12 form-group">
						<table className="table gs-table-borderless table-sm">
							<thead>
								<tr>
									<th className="text-left gs-uppercase font-10" style={{color: '#aaa'}}>Voucher No</th>
									<th className="text-right gs-uppercase font-10" style={{color: '#aaa'}}>Amount Overdue</th>
								</tr>
							</thead>
							<tbody>
								{collectionstatusDetails.map((outstandingItem, outstandingIndex)=>{
									return (
										<tr key={outstandingIndex}>
											<td>
												<a style={{color: 'inherit'}} onClick={() => this.openLink(outstandingItem)}>{outstandingItem.displayvoucherno}</a>
												<span className="marginleft-5 badge gs-form-btn-secondary">{outstandingItem.overduebydays} days</span>
											</td>
											<td className="text-right">{currencyFilter(outstandingItem.balanceamount, outstandingItem.currencyid, this.props.app)}</td>
										</tr>
									)
								})}
							</tbody>
						</table>
					</div>
					{collectionActivity.length > 0 ? <div className="form-group">Activity</div> : null}
					{collectionActivity.length > 0 ? <div className="col-md-12">
						<ul>
						{collectionActivity.map((activityItem, activityIndex)=>{
							return (
								<li key={activityIndex}>
									{activityItem.activitytype} - {activityItem.description}
									{activityItem.nextfollowup ? <span className="badge badge-pill gs-form-btn-primary"><i className="fa fa-calendar marginright-5"></i>{dateFilter(activityItem.nextfollowup)}</span> : null}
									{activityItem.nextfollowup ? <span className="marginleft-5 badge badge-pill gs-form-btn-warning"><i className="fa fa-clock-o marginright-5"></i>{timeFilter(activityItem.nextfollowup)}</span> : null}
								</li>
							)
						})}
						</ul>
					</div> : null}
				</div>: null}
				{this.props.rptparam == 'leaditemsreport' && Object.keys(resourceObj).length > 0 ? <div className="col-md-12">
					<div className="form-group">
						<div>
							<b className="font-14">{resourceObj.customerid_legalname}</b>
							{resourceObj.priority ? <span className="marginleft-5 badge gs-form-btn-secondary">{resourceObj.priority}</span> : null}
							{resourceObj.status ? <span className="marginleft-5 badge gs-form-btn-primary">{resourceObj.status}</span> : null}
						</div>
						<div>{currencyFilter(resourceObj.potentialrevenue, null, this.props.app)}</div>
						<div>{resourceObj.salesperson_displayname}</div>
						{resourceObj.duedate ? <div className="margintop-10">
							Expected Closure Date: {dateFilter(resourceObj.duedate)}
						</div> : null}
					</div>
					<div className="form-group"> Contact Details</div>
					<div className="col-md-12 margintop-10">
						<div className="float-left" style={{width: '8%'}}>
							<div className={`gs-bullet-circle gs-text-color`}></div>
						</div>
						<div className="float-left" style={{width: '92%'}}>
							<div><b>{resourceObj.contactid_title ? resourceObj.contactid_title : null} {resourceObj.contactid_name}</b></div>
							{resourceObj.email ? <div>Email: {resourceObj.email}</div> : null}
							{resourceObj.mobile ? <div>Mobile: {resourceObj.mobile}</div> : null}
							{resourceObj.phone ? <div>Phone: {resourceObj.phone}</div> : null}
						</div>
					</div>
					{resourceObj.billingaddressid ? <div className="form-group"> Billing Address</div> : null}
					{resourceObj.billingaddressid ?	<div className="col-md-12">
						<div className="float-left" style={{width: '8%'}}>
							<div className={`gs-bullet-circle gs-text-color`}></div>
						</div>
						<div className="float-left" style={{width: '92%'}}>
							{resourceObj.billingaddress}
						</div>
					</div> : null}
					{resourceObj.deliveryaddressid ? <div className="form-group"> Delivery Address</div> : null}
					{resourceObj.deliveryaddressid ? <div className="col-md-12">
						<div className="float-left" style={{width: '8%'}}>
							<div className={`gs-bullet-circle gs-text-color`}></div>
						</div>
						<div className="float-left" style={{width: '92%'}}>
							{resourceObj.deliveryaddress}
						</div>
					</div> : null}
				</div>: null}

				{this.props.rptparam == 'contractitemsreport' && Object.keys(resourceObj).length > 0 ? <div className="col-md-12">
					<div className="form-group">
						<div>
							<b className="font-14">{resourceObj.customerid_legalname}</b>
							{resourceObj.contractstatus ? <span className="marginleft-5 badge gs-form-btn-secondary">{resourceObj.contractstatus	}</span> : null}
							{resourceObj.status ? <span className="marginleft-5 badge gs-form-btn-primary">{resourceObj.status}</span> : null}
						</div>
						<div>{resourceObj.salesperson_displayname}</div>
						<div className="margintop-10">
							Start Date:
							<span className="marginleft-5 badge badge-pill gs-form-btn-success"><i className="fa fa-calendar-check-o marginright-5"></i>{dateFilter(resourceObj.startdate)}</span>
						</div>
						<div className="margintop-10">
							Expire Date:
							<span className="marginleft-5 badge badge-pill gs-form-btn-danger"><i className="fa fa-calendar-times-o marginright-5"></i>{dateFilter(resourceObj.expiredate)}</span>
						</div>
					</div>

					{ resourceObj.installationcontactid ? <div className="form-group"> Installation Contact Details</div> : null}
					{ resourceObj.installationcontactid ? <div className="col-md-12">
						<div className="float-left" style={{width: '8%'}}>
							<div className={`gs-bullet-circle gs-text-color`}></div>
						</div>
						<div className="float-left marginbottom-10" style={{width: '92%'}}>
							<div><b>{resourceObj.installationcontactid_title ? resourceObj.installationcontactid_title : null} {resourceObj.installationcontactid_name}</b></div>
							{resourceObj.installationemail ? <div>Email: {resourceObj.installationemail}</div> : null}
							{resourceObj.installationmobile ? <div>Mobile: {resourceObj.installationmobile}</div> : null}
							{resourceObj.installationphone ? <div>Phone: {resourceObj.installationphone}</div> : null}
						</div>
					</div> : null}

					{ resourceObj.contactid ? <div className="form-group"> Contact Details</div> : null}
					{ resourceObj.contactid ? <div className="col-md-12">
						<div className="float-left" style={{width: '8%'}}>
							<div className={`gs-bullet-circle gs-text-color`}></div>
						</div>
						<div className="float-left marginbottom-10" style={{width: '92%'}}>
							<div><b>{resourceObj.contactid_title ? resourceObj.contactid_title : null} {resourceObj.contactid_name}</b></div>
							{resourceObj.email ? <div>Email: {resourceObj.email}</div> : null}
							{resourceObj.mobile ? <div>Mobile: {resourceObj.mobile}</div> : null}
							{resourceObj.phone ? <div>Phone: {resourceObj.phone}</div> : null}
						</div>
					</div> : null}

					{resourceObj.installationaddressid ? <div className="form-group"> Installation Address</div> : null}
					{resourceObj.installationaddressid ? <div className="col-md-12">
						<div className="float-left" style={{width: '8%'}}>
							<div className={`gs-bullet-circle gs-text-color`}></div>
						</div>
						<div className="float-left marginbottom-10" style={{width: '92%'}}>
							{resourceObj.installationaddress}
						</div>
					</div> : null}

					{resourceObj.billingaddressid ? <div className="form-group"> Billing Address</div> : null}
					{resourceObj.billingaddressid ?	<div className="col-md-12">
						<div className="float-left" style={{width: '8%'}}>
							<div className={`gs-bullet-circle gs-text-color`}></div>
						</div>
						<div className="float-left marginbottom-10" style={{width: '92%'}}>
							{resourceObj.billingaddress}
						</div>
					</div> : null}

					{ resourceObj.contractitems && resourceObj.contractitems.length > 0 ? <div className="gs-md-12 form-group">
						<div className="marginbottom-10">
							<b>Equipment Details:</b>
						</div>
						<table className="table gs-table-borderless table-sm">
							<thead>
								<tr>
									<th className="text-left gs-uppercase font-10" style={{color: '#aaa'}}>Equipment Name</th>
									<th className="text-right gs-uppercase font-10" style={{color: '#aaa'}}>Status</th>
								</tr>
							</thead>
							<tbody>
								{resourceObj.contractitems.map((item, index)=>{
									return (
										<tr key={index}>
											<td>
												{item.equipmentid_displayname}
												<span className="marginleft-5 badge gs-form-btn-secondary">{item.equipmentid_serialno}</span>
											</td>
											<td className="text-right">{item.contractstatus}</td>
										</tr>
									)
								})}
							</tbody>
						</table>
					</div> : null}

				</div>: null}
			</div>
		)
	}
}