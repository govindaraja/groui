import React, { Component } from 'react';
import axios from 'axios';

import { search,checkActionVerbAccess } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect } from '../utilcomponents';
import { dateFilter } from '../../utils/filter';
import {Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import { selectAsyncEle,NumberEle,DateEle, costcenterAutoMultiSelectEle } from '../formelements';

import { numberNewValidation,dateNewValidation, multiSelectNewValidation } from '../../utils/utils';
import {currencyFilter} from '../../utils/filter';


export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			roundOffPrecision : this.props.app.feature.roundOffPrecision || 2,
			resourceObj : {
				productionorders : {
					title : 'Production Order',
					transactionno : 'orderno',
					receiptnotefield : 'productionorderid',
					receiptnoteispurchaseorder : 'Production Order'
				},
				workorders : {
					title : 'Work Order',
					transactionno : 'wonumber',
					receiptnotefield : 'workorderid',
					receiptnoteispurchaseorder : 'Job Work Return'
				}
			}
		};
		this.onLoad = this.onLoad.bind(this);
		this.calculateTotal = this.calculateTotal.bind(this);
		this.confirm = this.confirm.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	calculateTotal(){
		let totalmaterialcost=0,totalvaluationcost=0,totalamount=0;
		this.props.resource.confirmvaluation.receiptnoteitems.forEach((item,index)=>{
			item.valuationrate = Number(((item.valuationcost+item.materialcost)/item.quantity).toFixed(this.state.roundOffPrecision));
			item.rate = Number(((item.valuationcost+item.materialcost)/item.quantity).toFixed(this.state.roundOffPrecision));
			item.amount = Number((item.valuationrate*item.quantity).toFixed(this.state.roundOffPrecision));
			totalmaterialcost += item.materialcost;
			totalvaluationcost += item.valuationcost ;
			totalamount += item.amount;
		});
		totalmaterialcost = Number((totalmaterialcost).toFixed(this.state.roundOffPrecision));
 		totalvaluationcost = Number((totalvaluationcost).toFixed(this.state.roundOffPrecision));
 		totalamount = Number((totalamount).toFixed(this.state.roundOffPrecision));
		this.props.resource.confirmvaluation.roundoff = Number((totalamount-totalmaterialcost-totalvaluationcost).toFixed(this.state.roundOffPrecision));
		this.setState({
			totalmaterialcost,totalvaluationcost,totalamount
		});
	}

	onLoad() {
		if(!this.props.resource.valuationdone){
			let confirmvaluation = {
				receiptnoteitems :  [],
				othercostitems :  [],
				roundoff :  0,
				totalmaterialcost :  0,
				defaultcostcenter :  null,
				numberingseriesmasterid : null,
			};
			
			axios.get(`/api/query/getproductionfinalvaluation?resourcename=${this.props.resourceName}&relatedid=${this.props.resource.id}`).then((response) => {
				if (response.data.message == 'success') {
					let receiptnoteitems = [...response.data.main.receiptnoteitems];
					let availableReceiptItems={};
					
					confirmvaluation.receiptnoteitems.forEach((item,index)=>{
						availableReceiptItems[item.id] = item;
					});
					
					receiptnoteitems.forEach((item,index)=>{
						if(!availableReceiptItems[item.id]){
							item.materialcost = Number((item.rate*item.quantity).toFixed(this.state.roundOffPrecision));
							item.valuationcost = 0;
							item.amount = item.materialcost;								
							confirmvaluation.receiptnoteitems.push(item);
						}
					});
					
					confirmvaluation['totalmaterialcost'] = response.data.main.totalissued;
					this.props.updateFormState(this.props.form, {
						confirmvaluation 
					});
					this.calculateTotal();
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}else{
			this.calculateTotal();
		}
	}

	confirm(){

		if(Number((this.state.totalmaterialcost).toFixed(this.state.roundOffPrecision)) != Number((this.props.resource.confirmvaluation.totalmaterialcost).toFixed(this.state.roundOffPrecision))){
			return this.props.openModal(modalService['infoMethod']({
						header : 'Error',
					body : `Total of Material Cost in "Material Valuation Cost" does not match with Total Material Cost.`,
					btnArray : ['Ok']
				})); 
		}
		let valuationcost = 0,negativeValueFound = false;
		this.props.resource.confirmvaluation.othercostitems.forEach((otheritem,otherindex)=>{
			valuationcost += otheritem.amount;
			if(otheritem.amount <= 0)
				negativeValueFound=true;
		});

		if(negativeValueFound)
			return this.props.openModal(modalService['infoMethod']({
				header : 'Error',
				body : `Other Costs should be greater than zero.`,
				btnArray : ['Ok']
			})); 

		if(Number((this.state.totalvaluationcost).toFixed(this.state.roundOffPrecision)) != Number((valuationcost).toFixed(this.state.roundOffPrecision))){
			return this.props.openModal(modalService['infoMethod']({
						header : 'Error',
					body : `Total of Valuation Cost in "Material Valuation Cost" does not match with total of "Other costs" section.`,
					btnArray : ['Ok']
				})); 
		}

		this.props.closeModal();
		this.props.save('Confirm Valuation')
	}

	render() {
		if(!this.props.resource){
			return null;
		}
		let confirmaccess = checkActionVerbAccess(this.props.app, this.props.resourceName, 'Confirm Valuation') ;
		let reconfirmaccess = checkActionVerbAccess(this.props.app, this.props.resourceName, 'Reopen Valuation');
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title gs-text-color semi-bold-custom">{this.state.resourceObj[this.props.resourceName].title} - Valuation</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
				{
					(!this.props.resource.valuationdone || (this.props.resource.valuationdone && !this.props.resource.confirmvaluation.valuationdonemanually) ) ? 
					<div><div className="row">
						<div className="col-md-12 col-sm-12">
							<div className="col-md-9 col-sm-9 float-left">
								<div className='row'>		
									<div className="col-md-12 col-sm-12 form-group">
										{this.state.resourceObj[this.props.resourceName].title} no. {this.props.resource[this.state.resourceObj[this.props.resourceName].transactionno]}
									</div>
									<div className="col-md-4 col-sm-12">
										<Field name={`confirmvaluation.defaultcostcenter`} props={{
											required : true,
											placeholder : 'Select Cost Center',
											disabled : this.props.resource.valuationdone
										}} component={costcenterAutoMultiSelectEle}  validate={[multiSelectNewValidation({required: true})]} />
									</div>
									<div className="col-md-4 col-sm-12">
												<Field name={`confirmvaluation.numberingseriesmasterid`} props={{
													resource : 'numberingseriesmaster',
													fields: 'id,name',
													required : true,
													placeholder : 'Select Numbering Series',
													disabled : this.props.resource.valuationdone,
													filter : "numberingseriesmaster.resource='journals'"
												}} component={selectAsyncEle}  validate={[numberNewValidation({required: true})]} />
									</div>
									<div className="col-md-4 col-sm-12">
										<Field 
											name={`confirmvaluation.voucherdate`} 
											props={{
												required : false,
												placeholder : 'Select Voucher Date',
												disabled : this.props.resource.valuationdone
											}}
											component={DateEle}
											validate={dateNewValidation({required: false})} />
									</div>	
								</div>
							</div>
							<div className="col-md-3 col-sm-3 float-right">
								<table className="float-right lineheight-2">
									<tbody>
										<tr>
											<td align="right">Total Material Cost :</td>
											<td align="right" className="fontWeight" style={{whiteSpace: 'nowrap'}}>{currencyFilter(this.props.resource.confirmvaluation.totalmaterialcost, this.props.resource.currencyid, this.props.app)}</td>
										</tr>
										<tr>
											<td align="right">Rounding off :</td>
											<td align="right" className="fontWeight" style={{whiteSpace: 'nowrap'}}>{currencyFilter(this.props.resource.confirmvaluation.roundoff, this.props.resource.currencyid, this.props.app)}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div className="row margintop-10">
						<div className="col-md-12 col-sm-12">
							<div className="bg-gray padding-10 fontWeight"> Material Valuation Cost </div>
							<div className="table-responsive">
								<table className="table gs-table gs-item-table-bordered">
									<thead>
										<tr>
											<th className='text-center' style={{width:'150px'}}>RN no.</th>
											<th className='text-center' style={{width:'100px'}}>Material</th>
											<th className='text-center' style={{width:'200px'}}>Quantity</th>
											<th className='text-center' style={{width:'100px'}}>Material Cost</th>
											<th className='text-center' style={{width:'150px'}}>Valuation Cost</th>
											<th className='text-center' style={{width:'150px'}}>Rate</th>
											<th className='text-center' style={{width:'150px'}}>Amount</th>
										</tr>
									</thead>
									<tbody>
										{
											(this.props.resource.confirmvaluation.receiptnoteitems) ? 
											this.props.resource.confirmvaluation.receiptnoteitems.map((item, index) => {
											return (
												<tr key={index}>
													<td className="text-center">{item.parentid_receiptnotenumber}</td>
													<td> {item.itemid_name} </td>
													<td className="text-center">{item.quantity}</td>
													<td className="text-center">
														<Field name={`confirmvaluation.receiptnoteitems[${index}].materialcost`} props={{
															required : true,
															onChange : this.calculateTotal,
															disabled : this.props.resource.valuationdone
														}} component={NumberEle} validate={[numberNewValidation({required: true})]} />
													</td>
													<td>
													<Field name={`confirmvaluation.receiptnoteitems[${index}].valuationcost`} props={{
															required : true,
															onChange : this.calculateTotal,
															disabled : this.props.resource.valuationdone
														}} component={NumberEle} validate={[numberNewValidation({required: true})]} />
													</td>
													<td>{currencyFilter(item.valuationrate,this.props.resource.currecyid,this.props.app)}</td>
													<td>{currencyFilter(item.amount,this.props.resource.currecyid,this.props.app)}</td>
												</tr>
											);
											}) : null
										}
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td className="fontWeight">{currencyFilter(this.state.totalmaterialcost,this.props.resource.currecyid,this.props.app)}</td>
											<td className="fontWeight">{currencyFilter(this.state.totalvaluationcost,this.props.resource.currecyid,this.props.app)}</td>
											<td></td>
											<td className="fontWeight">{currencyFilter(this.state.totalamount,this.props.resource.currecyid,this.props.app)}</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
						<div className="col-md-12 col-sm-12">
							<div className="bg-gray padding-10 fontWeight"> Other Costs </div>
							<FieldArray name='confirmvaluation.othercostitems' updateFormState={this.props.updateFormState} form={this.props.form} resource={this.props.resource} calculateTotal={this.calculateTotal} component={OtherCostElement} app={this.props.app} />
						</div>
					</div></div> : <div className="col-md-12 col-sm-12 col-xs-12 alert alert-warning text-center">
									Valuation Confirmation not available for old {this.state.resourceObj[this.props.resourceName].title}s. If you want to change the valuation, please reopen the {this.state.resourceObj[this.props.resourceName].title}.
								</div>
				}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center float-right">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								{
								(confirmaccess && !this.props.resource.valuationdone && this.props.resource.confirmvaluation && this.props.resource.confirmvaluation.receiptnoteitems && this.props.resource.confirmvaluation.receiptnoteitems.length>0) ? 
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={()=>{this.confirm()}} disabled={this.props.invalid}><i className="fa fa-check-square-o"></i>Save</button> : null
								}
								{
								(reconfirmaccess && this.props.resource.valuationdone) ?
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={()=>{this.props.closeModal();this.props.save('Reopen Valuation')}} disabled={false}><i className="fa fa-check"></i>Reopen Valuation</button> : null
								}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}


class OtherCostElement extends Component {
	constructor(props) {
		super(props);
		this.state = {
			totalothercost:0
		}
		this.addNewRow = this.addNewRow.bind(this);
		this.deleteRow = this.deleteRow.bind(this);
		this.onChangeOtherCost = this.onChangeOtherCost.bind(this);
		this.applyOtherCost = this.applyOtherCost.bind(this);
	}

	addNewRow() {
		this.props.fields.push({});
	}

	deleteRow(index) {
		this.props.fields.remove(index);
		setTimeout(() => {
			this.onChangeOtherCost();
		}, 0);
	}
	componentWillMount(){
		setTimeout(() => {
			if(this.props.resource.valuationdone)
				this.onChangeOtherCost();
		}, 0);
	}

	onChangeOtherCost(){
		let totalothercost = 0;
		let othercostitems = this.props.resource.confirmvaluation.othercostitems || [];
		othercostitems.forEach((item,index)=>{
			totalothercost += item.amount;
		});
		this.setState({totalothercost});
	}

	applyOtherCost(param){
		let roundOffPrecision = this.props.app.feature.roundOffPrecision || 2;
		let totalmaterialcost=0,totalquantity=0;
		let confirmvaluation = this.props.resource.confirmvaluation;

		confirmvaluation.receiptnoteitems.forEach((item,index)=>{
			totalquantity += item.quantity;
			totalmaterialcost += item.materialcost;
		});
		
		confirmvaluation.receiptnoteitems.forEach((item,index)=>{
			if(param == 'cost'){
				item.valuationcost = Number(((this.state.totalothercost * ((item.materialcost*100)/totalmaterialcost))/100).toFixed(roundOffPrecision));
			}else{
				item.valuationcost = Number(((this.state.totalothercost * ((item.quantity*100)/totalquantity))/100).toFixed(roundOffPrecision)) ;
			}
			item.valuationrate = Number(((item.valuationcost+item.materialcost)/item.quantity).toFixed(roundOffPrecision));
			item.amount = Number((item.valuationrate*item.quantity).toFixed(roundOffPrecision));
		});
		
		this.props.updateFormState(this.props.form,{
			confirmvaluation
		});

		this.props.calculateTotal();
	}

	render() {
		return (
			<div className="row">
				<div className="col-md-12 col-sm-12">
					<div className="table-responsive">
						<table className="table gs-table gs-item-table-bordered">
							<thead>
								<tr>
									<th className="text-center" style={{width:'5%'}}>S.no.</th>
									<th>Account</th>
									<th style={{width:'30%'}}>Amount</th>
									<th style={{width:'10%'}}></th>
								</tr>
							</thead>
							<tbody>
								 {this.props.fields.map((member, index) => {
									return (
										<tr key={index}>
											<td className="text-center">{index+1}</td>
											<td>
											<div style={{width:'400px'}}>
												<Field name={`${member}.accountid`}  props={{
													resource : 'accounts',
													fields: 'id,name',
													required : true,
													disabled : this.props.resource.valuationdone
												}} component={selectAsyncEle}  validate={[numberNewValidation({required: true})]}/>
											</div>
												
											</td>
											<td>
											<div style={{width:'200px',float:'rigth'}}>
												<Field name={`${member}.amount`} props={{
													required : true,
													onChange : this.onChangeOtherCost,
													disabled : this.props.resource.valuationdone
												}} component={NumberEle} validate={[numberNewValidation({required: true,min: 1})]} />
											</div>
											</td>
											<td>
												<button type='button' className={`btn gs-form-btn-danger btn-sm`} disabled={this.props.resource.valuationdone} onClick={() => this.deleteRow(index)}><span className='fa fa-trash-o'></span></button>
											</td>
										</tr>
									)
								 })}
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<td></td>
									<td className="fontWeight">{currencyFilter(this.state.totalothercost,this.props.resource.currecyid,this.props.app)}</td>
								</tr>
							</tfoot>
						</table>
						<div>
							<div className="float-left">
								<button type="button" className="btn btn-sm gs-form-btn-primary btn-width" disabled={this.props.resource.valuationdone} onClick={this.addNewRow}><i className="fa fa-plus"></i>Add Other Cost</button>
							</div>
							<div className="float-right">
								<button type="button" className="btn btn-sm gs-form-btn-primary btn-width" disabled={this.props.resource.valuationdone} onClick={()=>{this.applyOtherCost('cost')}}>Apply By Cost</button>
								<button type="button" className="btn btn-sm gs-form-btn-primary btn-width" disabled={this.props.resource.valuationdone} onClick={()=>{this.applyOtherCost('quanity')}}>Apply By Quantity</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}