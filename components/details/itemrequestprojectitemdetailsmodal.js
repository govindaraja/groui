import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import * as utils from '../../utils/utils';
import { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: {},
			projectItemArray: [],
			projectkititemdetails: [],
			showdescription: false
		};
		this.onLoad = this.onLoad.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.getKitItemdetails = this.getKitItemdetails.bind(this);
		this.addProjectItem = this.addProjectItem.bind(this);
		this.checkallonChange = this.checkallonChange.bind(this);
		this.descToggleOnChange = this.descToggleOnChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let { projectItemArray } = this.state;
		var projectCustomFields = this.props.getCustomFields('projectitems','string');

		axios.get(`/api/projectitems?field=id,itemid,uomid,itemmaster/name/itemid,itemmaster/itemtype/itemid,description,specification,itemmaster/keepstock/itemid,uomid,uom/name/uomid,quantity,rate,discount,amount,discountquantity,discountmode,splitrate,materialrate,labourrate,uomgroupid,itemmaster/size/itemid,itemmaster/quantitytype/itemid,itemnamevariationid,number,uomconversionfactor,uomconversiontype,alternateuom,itemmaster/itemcategorymasterid/itemid,itemmaster/itemgroupid/itemid,itemmaster/usebillinguom/itemid,usebillinguom,billinguomid,billingrate,billingquantity,billingconversiontype,billingconversionfactor,index,approvedmakes${projectCustomFields ? (','+projectCustomFields) : ''}&filtercondition=projectitems.parentresource='projects' and projectitems.itemtype='Item' and itemmaster_itemid.itemtype in ('Product') and itemmaster_itemid.allowpurchase and projectitems.parentid=${this.props.resource.projectid}`).then((response) => {
			if (response.data.message == 'success') {
				projectItemArray = response.data.main;
				for (var i = 0; i < this.props.resource.itemrequestitems.length; i++) {
					for (var j = 0; j < projectItemArray.length; j++) {
						if (this.props.resource.itemrequestitems[i].boqitemsid == projectItemArray[j].id)
							projectItemArray[j].checked = true;
					}
				}
				this.setState({projectItemArray}, () => {
					this.getKitItemdetails();
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getKitItemdetails() {
		let projectkititemdetails = [];

		axios.get(`/api/kititemprojectdetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid,rate&filtercondition=kititemprojectdetails.parentid = ${this.props.resource.projectid}`).then((response) => {
			if (response.data.message == 'success') {
				this.setState({projectkititemdetails : response.data.main});
			}
		});
	}

	inputonChange(value) {
		let { search } = this.state;
		search.itemid_name = value;
		this.setState({search});
	}

	checkboxOnChange(item, value) {
		item.checked = value;
		this.setState({
			projectItemArray: this.state.projectItemArray
		});
	}

	checkallonChange(value, itemarr) {
		let { projectItemArray } = this.state;
		projectItemArray.forEach((item) => {
			item.checked = value;
		});
		this.setState({	projectItemArray, checkall : value });
	}

	addProjectItem () {
		let itemrequestitems = [...this.props.resource.itemrequestitems];
		let kititemitemrequestdetails = [...this.props.resource.kititemitemrequestdetails];
		let itemrequestitemidArr = [];
		let index = 0;
		itemrequestitems.forEach((item) => {
			if(index < item.index)
				index = item.index;
		});
		index = index + 1;
		let checkedFound = false;
		itemrequestitems.forEach((reqitem) => {
			itemrequestitemidArr.push(reqitem.boqitemsid);
		});
		this.state.projectItemArray.forEach((item) => {
			let itemFound = false;
			itemrequestitems.forEach((reqitem, reqindex) => {
				if (reqitem.boqitemsid == item.id) {
					itemFound = true;
					if (!item.checked)
						itemrequestitems.splice(reqindex, 1);
				}
			});

			if(!itemFound && item.checked) {
				checkedFound = true;
				if(itemrequestitemidArr.indexOf(item.id) == -1) {
					let tempObj = {
						index: (index+1),
						requireddate: this.props.resource.requireddate || null,
						warehouseid: this.props.resource.warehouseid || null 
					};
					utils.assign(tempObj, item, ['itemid', 'itemid_name', 'itemid_issaleskit', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'specification', 'description', 'quantity', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'itemid_keepstock','uomconversionfactor', 'uomconversiontype','remarks', 'usebillinguom', 'billingquantity', 'billinguomid', 'billingconversiontype', 'billingconversionfactor', 'itemid_usebillinguom', {'boqitemsid' : 'id'},'approvedmakes']);


					this.props.customFieldsOperation('projectitems',tempObj, item, 'itemrequestitems');

					this.state.projectkititemdetails.forEach((kititem) => {
						if(kititem.parentid == this.props.resource.projectid && item.index == kititem.rootindex) {
							let tempKitObj = {
								rootindex: tempObj.index
							};
							utils.assign(tempKitObj, kititem, ['itemid', 'itemid_name', 'parent', 'uomid', 'uomid_name', 'quantity', 'conversion', 'rate', 'ratelc', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'warehouseid', 'index', 'parentindex']);
							kititemitemrequestdetails.push(tempKitObj);
						}
					});
					itemrequestitems.push(tempObj);
					index++;
				}
			}
		});

		this.props.updateFormState(this.props.form, { itemrequestitems, kititemitemrequestdetails });
		setTimeout(() => {
			this.props.closeModal();
		}, 0);
	}

	descToggleOnChange() {
		this.setState({showdescription: !this.state.showdescription});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Project Item Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-4 form-group">
							<input type="text" className="form-control" name="location" value={this.state.search.itemid_name || ''} placeholder="Search By Item Name" onChange={(evt) =>{this.inputonChange(evt.target.value)}}/>
						</div>
						<div className=" col-md-4 form-group">
							<div>Show Description</div>
							<label className="gs-checkbox-switch">
								<input className="form-check-input" type="checkbox" checked={this.state.showdescription} onChange={() => this.descToggleOnChange()} />
								<span className="gs-checkbox-switch-slider"></span>
							</label>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th style={{width: '6%', textAlign: 'center'}}><input type="checkbox" checked={this.state.checkall || false} onChange={(evt)=>{this.checkallonChange(evt.target.checked)}}/></th>
										<th className="text-center">Item Name</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">UOM</th>
									</tr>
								</thead>
								<tbody>
									{search(this.state.projectItemArray, this.state.search).map((item, index) => {
										return (
											<tr key={index}>
												<td className="text-center"><input type="checkbox" onChange={(e) => this.checkboxOnChange(item, e.target.checked)} checked={item.checked} /></td>
												<td>{item.itemid_name}<div className={`font-12 ${this.state.showdescription ? 'show' : 'hide'}`}>{item.description}</div></td>
												<td className="text-center">{item.quantity}</td>
												<td className="text-center">{item.uomid_name}</td>
											</tr>
										)
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addProjectItem}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});