import React, { Component } from 'react';
import {connect} from 'react-redux';

import ActivitySection from '../../containers/activitysection';

export default class KanbanActivityModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Activities</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<ActivitySection 
						parentid = {this.props.param.id}
						resourcename = {'leads'}
						leadid = {this.props.param.id}
						customerid = {this.props.param.customerid}
						contactid = {this.props.param.contactid}
						showrelatedto = {true}
						openModal = {this.props.openModal}
						createOrEdit = {this.props.createOrEdit} 
						/>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={()=> {this.props.callback();this.props.closeModal();}}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
};
