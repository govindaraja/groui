import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService } from '../../utils/services';
import printtemplate from '../../printtemplates';
import { search } from '../../utils/utils';

import Loadingcontainer from '../loadingcontainer';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderflag: false,
			printtemplateArray: printtemplate.printtemplateArray,
			templatename: null,
			search: {}
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.searchOnChange = this.searchOnChange.bind(this);
		this.selectOnChange = this.selectOnChange.bind(this);
		this.createPrintTemplate = this.createPrintTemplate.bind(this);
	};

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	searchOnChange(value) {
		let { search } = this.state;
		search.resource = value;
		this.setState({search});
	}

	selectOnChange(valueObj) {
		this.setState({
			templatename: valueObj.templatename
		});
	}

	createPrintTemplate() {
		this.updateLoaderFlag(true);
		axios({
			method : 'post',
			data : {...this.state},
			url : '/api/common/methods/insertprinttemplate'
		}).then((response) => {
			if(response.data.message == 'success') {
				this.props.callback(response.data.main.templateid);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.props.closeModal();
			this.updateLoaderFlag(false);
		});
	};

	render() {
		return (
			<div className="react-outer-modal">
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Choose Print Template</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-6 offset-md-3 form-group">
							<input type="text" className="form-control" value={this.state.search.resource || ''} placeholder="Search by resource" onChange={(evt) =>{this.searchOnChange(evt.target.value)}}/>
						</div>
					</div>
					<div className="row">
						{
							search(this.state.printtemplateArray, this.state.search).map((template, index) => {
								return (
									<div className="col-md-4 form-group" key={index}>
										<div className={`list-group-item printtemplate-gallery-list ${this.state.templatename == template.templatename ? 'activeclass' : ''}`}  onClick={()=> this.selectOnChange(template)}>
											{template.name}
										</div>
									</div>
								);
							})
						}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className={`btn btn-sm gs-btn-success btn-width`} onClick={this.createPrintTemplate} disabled={!this.state.templatename}><i className="fa fa-print"></i>Create</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}