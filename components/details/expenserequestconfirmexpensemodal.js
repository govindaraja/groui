import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import Loadingcontainer from '../loadingcontainer';

import { commonMethods, modalService } from '../../utils/services';
import { currencyFilter } from '../../utils/filter';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderflag: true,
			confirmexpensedetails: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.confirmExpense = this.confirmExpense.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	onLoad() {
		let { expenserequestitems, expenserequestsettlementitems } = this.props.resource;
		let accountidArr = [], confirmexpensedetails = [];

		expenserequestitems.forEach((item) => {
			if(item.xpurchaseinvoiceid_payableaccountid > 0)
				accountidArr.push(item.xpurchaseinvoiceid_payableaccountid);

			confirmexpensedetails.push({
				accountid : item.xpurchaseinvoiceid_payableaccountid ? item.xpurchaseinvoiceid_payableaccountid : item.accountid,

				accountid_name : item.xpurchaseinvoiceid_payableaccountid ? '' : item.accountid_name,
				credit : item.amountapproved,
				debit : null,
				againstreference: item.xpurchaseinvoiceid ? item.xpurchaseinvoiceid_invoiceno : ''
			});
		});

		expenserequestsettlementitems.forEach((item) => {
			confirmexpensedetails.push({
				accountid : item.accountid,
				accountid_name : item.accountid_name,
				credit : null,
				debit : item.credit,
				againstreference: item.xexpenserequestid ? item.xexpenserequestid_expenserequestno : (item.xjournalvoucherid ? item.xjournalvoucherid_voucherno : ''),

			});
		});

		if(accountidArr.length > 0) {
			axios.get(`/api/accounts?field=id,name&filtercondition=accounts.id IN (${accountidArr})`).then((response) => {
				if (response.data.message == 'success') {
					for (var i = 0; i < confirmexpensedetails.length; i++) {
						for (var j = 0; j < response.data.main.length; j++) {
							if (confirmexpensedetails[i].accountid == response.data.main[j].id)
								confirmexpensedetails[i].accountid_name = response.data.main[j].name;
						}
					}
					this.setState({ confirmexpensedetails });
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
				this.updateLoaderFlag(false);
			});
		} else {
			this.updateLoaderFlag(false);
			this.setState({ confirmexpensedetails });
		}
	}

	confirmExpense() {
		this.props.callback();
		this.props.closeModal();
	}

	render() {
		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Confirm Expense</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th className="text-center">Account</th>
										<th className="text-center">Credit</th>
										<th className="text-center">Debit</th>
										<th className="text-center">Against Reference</th>
									</tr>
								</thead>
								<tbody>
									{this.state.confirmexpensedetails.map((item, index) => {
										return (
											<tr key={index}>
												<td>{item.accountid_name}</td>
												<td>{currencyFilter(item.credit, this.props.resource.currencyid, this.props.app)}</td>
												<td>{currencyFilter(item.debit, this.props.resource.currencyid, this.props.app)}</td>
												<td>{item.againstreference}</td>
											</tr>
										)
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.confirmExpense}><i className="fa fa-check"></i>Confirm</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});