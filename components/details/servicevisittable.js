
import React, { Component } from 'react';

import { datetimeFilter } from '../../utils/filter';;

export default class ServiceVisitTable extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		if(!this.props.resource || !this.props.resource.servicevisits || (this.props.resource.servicevisits && this.props.resource.servicevisits.length == 0))
			return null;

		return (
			<div className="table-responsive">
				<table className="table gs-table gs-item-table-bordered">
					<thead>
						<tr>
							<th className="text-center" style={{width:'7%'}}>S.No</th>
							<th className="text-center" style={{width:'25%'}}>Engineer</th>
							<th className="text-center" style={{width:'25%'}}>Planned Start</th>
							<th className="text-center" style={{width:'25%'}}>Planned End</th>
							<th className="text-center" style={{width:'18%'}}>Status</th>
						</tr>
					</thead>
					<tbody>
						 {this.props.resource.servicevisits.map((item, index) => {
							return (
								<tr key={index} onClick={()=>this.props.openServiceVisit(item)} style={{cursor: 'pointer'}}>
									<td className="text-center">{index+1}</td>
									<td className="text-center">{item.assignedto_displayname}</td>
									<td className="text-center">{datetimeFilter(item.plannedstarttime)}</td>
									<td className="text-center">{datetimeFilter(item.plannedendtime)}</td>
									<td className="text-center">{item.status}</td>
								</tr>
							)
						 })}
					</tbody>
				</table>
			</div>
		);
	}
}