import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { currencyFilter } from '../../utils/filter';

export const ServicecallprofitanalyseModal = connect((state)=>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			profitanalyse : {},
			profitarray: [],
			costarray: [],
			showMessage : false
		};
		this.onLoad = this.onLoad.bind(this);
		this.getStockdetails = this.getStockdetails.bind(this);
		this.renderTablebody = this.renderTablebody.bind(this);
		this.showprofitability = this.showprofitability.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		axios.get(`/api/query/getservicecallitemquery?servicecallid=${this.props.resource.id}`).then((response) => {
			if (response.data.message == 'success') {
				if(response.data.main.length > 0){
					let servicecallarr = response.data.main;
					for (var i = 0; i < servicecallarr.length; i++) {
						if (this.props.app.uomObj[servicecallarr[i].uomid]) {
							servicecallarr[i].uomconversionrate = this.props.app.uomObj[servicecallarr[i].uomid].conversionrate;
						}
					}
					this.setState({showMessage : !(this.showprofitability(servicecallarr))});
					this.getStockdetails(servicecallarr);
				}
				else{
					this.setState({showMessage : true});
				}
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	getStockdetails(servicecallarr) {
		let { profitanalyse, profitarray } = this.state;
		var itemidArray = [];
		servicecallarr.forEach((item) => {
			itemidArray.push(item.itemid);
		});

		axios.get(`/api/query/stockquery?itemid=${itemidArray}`).then((response) => {
			for (var j = 0; j < servicecallarr.length; j++) {
				let temptotalqty = 0, temptotalvalue = 0, costPrice = null;

				for (var i = 0; i < response.data.main.length; i++) {
					if (servicecallarr[j].itemid == response.data.main[i].itemid) {
						response.data.main[i].onhandqty = response.data.main[i].onhandqty ? response.data.main[i].onhandqty : 0;
						response.data.main[i].marate = response.data.main[i].marate ? response.data.main[i].marate : 0;
						if (servicecallarr[j].uomid != response.data.main[i].itemid_stockuomid) {
							if (this.props.app.uomObj[response.data.main[i].itemid_stockuomid]) {
								response.data.main[i].marate = (response.data.main[i].marate / servicecallarr[j].uomconversionrate) * this.props.app.uomObj[response.data.main[i].itemid_stockuomid].conversionrate;
							}
						}

						temptotalvalue += response.data.main[i].onhandqty * response.data.main[i].marate;
						temptotalqty += response.data.main[i].onhandqty;

						if (temptotalqty > 0 && temptotalvalue > 0)
							costPrice = temptotalvalue / temptotalqty;
					}
				}

				profitarray.push({
					type : servicecallarr[j].type,
					servicereportno : servicecallarr[j].servicereportno,
					itemid : servicecallarr[j].itemid,
					itemid_name : servicecallarr[j].itemid_name,
					uomid_name : servicecallarr[j].uomid_name,
					salesprice : servicecallarr[j].rateafterdiscount,
					costprice : costPrice,
					finalamount : servicecallarr[j].amount,
					qty : servicecallarr[j].quantity
				});
			}

			let temptotalcost = 0, temptotalrevenue = 0, tempgrossprofit = 0, costnotFound = false;

			for (var i = 0; i < profitarray.length; i++) {
				temptotalrevenue += profitarray[i].finalamount;
				profitanalyse.totalrevenue = temptotalrevenue;

				if (profitarray[i].costprice) {
					let tempgrossprofitpercentage = (profitarray[i].salesprice - profitarray[i].costprice) / profitarray[i].costprice * 100;
					profitarray[i].grossprofitpercentage = Number(tempgrossprofitpercentage.toFixed(2));
				} else {
					costnotFound = true;
				}
			}

			if (costnotFound == false) {
				for (var j = 0; j < profitarray.length; j++) {
					temptotalcost += profitarray[j].costprice * profitarray[j].qty;
					profitanalyse.totalcost = temptotalcost;
					tempgrossprofit = temptotalrevenue - temptotalcost;
					profitanalyse.grossprofit = tempgrossprofit;
					let temptotalgrossprofit = (tempgrossprofit / temptotalcost) * 100;
					profitanalyse.totalgrossprofit = Number(temptotalgrossprofit.toFixed(2));
				}
			}
			this.setState({profitanalyse, profitarray});
		});
	}

	renderTablebody(filter) {
		return this.state.profitarray.map((item, index)=> {
			if(item.type == filter) {
				return (
					<tr key={index}>
						<td className="text-center">{item.servicereportno}</td>
						<td className="text-center">{item.itemid_name}</td>
						<td className="text-center">{item.qty}</td>
						<td className="text-center">{item.uomid_name}</td>
						<td className="text-right">{currencyFilter(item.salesprice, item.currencyid, this.props.app)}</td>
						<td className="text-right">{currencyFilter(item.costprice, item.currencyid, this.props.app)}</td>
						<td className="text-right">{item.grossprofitpercentage}</td>
					</tr>
				);
			}
		});
	}

	showprofitability(array){
		var itemFound = false;
		for(var i=0;i<array.length;i++){
			if(array[i].type == 'servicereport'){
				itemFound = true;
				break;
			}
		}
		return itemFound;
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Profitability Analysis</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
					{	
						(this.state.profitarray.length > 0 && this.showprofitability(this.state.profitarray)) ?
							<div className="form-group col-md-12">
								<div className="card">
									<div className="card-header">Service Report Details</div>
									<div className="card-body">
										<div className="row">
											<div className="col-md-12">
												<table className="table table-bordered">
													<thead>
														<tr>
															<th className="text-center">Service Report No</th>
															<th className="text-center">Item Name</th>									
															<th className="text-center">Qty</th>
															<th className="text-center">UOM</th>
															<th className="text-center">Selling Price</th>
															<th className="text-center">Cost Price</th>
															<th className="text-center">Gross Profit %</th>
														</tr>
													</thead>
													<tbody>
														{this.renderTablebody('servicereport')}
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						: null
					}
					{
						(this.state.showMessage) ? <div className="col-md-8 offset-md-2"><div className="alert alert-warning text-center">Data not available. Please check after creating Service Report with spares.</div></div> : null
					}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
