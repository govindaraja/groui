import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { formValueSelector } from 'redux-form';

import { commonMethods, modalService } from '../../utils/services';
import { search } from '../../utils/utils';
import { currencyFilter, dateFilter, uomFilter } from '../../utils/filter';
import { SelectAsync, DateElement } from '../utilcomponents';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: {},
			checkAll: false,
			cumulativePicking: false,
			hasserial: false,
			hasbatch: false,
			availableStock: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.selector = formValueSelector(this.props.form);
		this.quantityonChange = this.quantityonChange.bind(this);
		this.checkboxonChange = this.checkboxonChange.bind(this);
		this.updatesearchField = this.updatesearchField.bind(this);
	}

	componentWillMount() {
		let {resource, itemstr} = this.props;
		let item = this.selector(this.props.fullstate, itemstr);

		if(this.props.resource.status != 'Approved' && this.props.resource.status != 'Dispatched' && this.props.resource.status != 'Cancelled') {
			if (!item.pickingdetails || !item.pickingdetails.details) {
				this.props.updateFormState(this.props.form, {
					[`${itemstr}.pickingdetails`]: {
						details : []
					}
				});
			} else if(item.pickingdetails.details.length == 0) {
				this.props.updateFormState(this.props.form, {
					[`${itemstr}.pickingdetails`]: {
						details : []
					}
				});
			}
			setTimeout(() => {
				this.onLoad();
			}, 0);
		}
	}

	onLoad() {
		let {resource, itemstr} = this.props;
		let item = this.selector(this.props.fullstate, itemstr);

		if(!item.itemid || !item.warehouseid)
			return null;

		axios.get(`/api/query/stocksplitupquery?itemid=${item.itemid}&warehouseid=${item.warehouseid}&uomid=${item.uomid}&uomconversiontype=${item.uomconversiontype}&uomconversionfactor=${item.uomconversionfactor}`).then((response) => {
			if (response.data.message == 'success')  {
				let removeIDArray = [];
				let availableStock = response.data.stockarray;
				let hasserial = response.data.hasserial;
				let hasbatch = response.data.hasbatch;
				let pickedarray = [...item.pickingdetails.details];
				if(this.props.app.useSublocation || response.data.hasserial || response.data.hasbatch) {
					if(response.data.hasserial) {
						pickedarray.forEach((rowitem) => {
							let itemFound = false;
							availableStock.forEach((resrow) => {
								if(rowitem.id == resrow.id) {
									itemFound = true;
									resrow.check = true;
									resrow.pickingremarks = rowitem.pickingremarks;
								}
							});
							if(!itemFound)
								removeIDArray.push(rowitem.id)
						});

						removeIDArray.forEach((id) => {
							for(var j = 0; j < pickedarray.length; j++) {
								if(pickedarray[j].id == id) {
									pickedarray.splice(j, 1);
									break;
								}
							}
						});
					} else {
						pickedarray.forEach((rowitem) => {
							let itemFound = false;
							availableStock.forEach((resrow) => {
								if(rowitem.itemid == resrow.itemid && rowitem.warehouseid == resrow.warehouseid && rowitem.stocklocationid == resrow.stocklocationid && rowitem.itembatchid == resrow.itembatchid) {
									itemFound = true;
									resrow.pickedQuantity = rowitem.quantity;
									resrow.pickingremarks = rowitem.pickingremarks;
								}
							});
							if(!itemFound)
								removeIDArray.push(rowitem.id)
						});

						removeIDArray.forEach((id) => {
							for(var j = 0; j < pickedarray.length; j++) {
								if(pickedarray[j].id == id) {
									pickedarray.splice(j, 1);
									break;
								}
							}
						});
					}
				}

				this.props.updateFormState(this.props.form, {
					[`${itemstr}.pickingdetails.details`]: pickedarray
				});
				this.setState({
					availableStock,
					hasserial,
					hasbatch
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	quantityonChange (item, value) {
		item.pickedQuantity = value;
		let {resource, itemstr} = this.props;
		let member = `${itemstr}.pickingdetails.details`;
		let pickedarray = this.selector(this.props.fullstate, member);

		if(Number(item.pickedQuantity) > 0) {
			let itemFound = false;
			for(var i = 0; i < pickedarray.length; i++) {
				if(item.itemid == pickedarray[i].itemid && item.warehouseid == pickedarray[i].warehouseid && item.stocklocationid == pickedarray[i].stocklocationid && item.itembatchid == pickedarray[i].itembatchid) {
					itemFound = true;
					pickedarray[i].quantity = Number(item.pickedQuantity);
					break;
				}
			}
			if(!itemFound) {
				pickedarray.push({
					id: item.id,
					warehouseid: item.warehouseid,
					warehouseid_name: item.warehouseid_name,
					stocklocationid: item.stocklocationid,
					stocklocationid_name: item.stocklocationid_name,
					itembatchid: item.itembatchid,
					itembatchid_batchnumber: item.itembatchid_batchnumber,
					itembatchid_expirydate: item.itembatchid_expirydate,
					itembatchid_manufacturingdate: item.itembatchid_manufacturingdate,
					itemid: item.itemid,
					itemid_name: item.itemid_name,
					itemid_stockuomid: item.itemid_stockuomid,
					pickingremarks: item.pickingremarks,
					quantity: Number(item.pickedQuantity)
				});
			}
		} else {
			for(var i = 0; i < pickedarray.length; i++) {
				if(item.itemid == pickedarray[i].itemid && item.warehouseid == pickedarray[i].warehouseid && item.stocklocationid == pickedarray[i].stocklocationid && item.itembatchid == pickedarray[i].itembatchid) {
					pickedarray.splice(i, 1);
					break;
				}
			}
		}
		this.props.updateFormState(this.props.form, {
			[`${member}`]: pickedarray
		});
		this.setState({
			availableStock: this.state.availableStock
		});
	}

	remarksonChange (item, value) {
		item.pickingremarks = value;
		let {resource, itemstr} = this.props;
		let member = `${itemstr}.pickingdetails.details`;
		let pickedarray = this.selector(this.props.fullstate, member);

		for(var i = 0; i < pickedarray.length; i++) {
			if(item.id == pickedarray[i].id) {
				pickedarray[i].pickingremarks = value;
				break;
			}
		}
		this.props.updateFormState(this.props.form, {
			[`${member}`]: pickedarray
		});
		this.setState({
			availableStock: this.state.availableStock
		});
	}

	getCount() {
		let count = 0;
		let member = `${this.props.itemstr}.pickingdetails.details`;
		let pickedarray  = this.selector(this.props.fullstate, member);

		if(!pickedarray)
			return count;

		for(var i = 0; i < pickedarray.length;i++) 
			count += pickedarray[i].serialno ? 1 : pickedarray[i].quantity;

		return count;
	}

	checkboxonChange (item, value) {
		item.check = value;
		let {resource, itemstr} = this.props;
		let member = `${itemstr}.pickingdetails.details`;
		let pickedarray = this.selector(this.props.fullstate, member);

		if(item.check) {
			pickedarray.push(item);
		} else {
			for(var i = 0; i < pickedarray.length; i++) {
				if(item.id == pickedarray[i].id) {
					pickedarray.splice(i, 1);
					break;
				}
			}
		}

		this.props.updateFormState(this.props.form, {
			[`${member}`]: pickedarray
		});
		this.setState({
			availableStock: this.state.availableStock
		});
	}

	checkAll (value) {
		let {resource, itemstr} = this.props;
		let member = `${itemstr}.pickingdetails.details`;
		let pickedarray = [];

		if(!this.state.cumulativePicking) {
			this.state.availableStock.forEach((item) => {
				item.check = false;
			});
		}
		search(this.state.availableStock,this.state.search).forEach((item) => {
			if(value) {
				item.check = true;
			} else {
				item.check = false;
			}
		});

		this.state.availableStock.forEach(item => {
			if(item.check)
				pickedarray.push(item);
		});

		this.props.updateFormState(this.props.form, {
			[`${member}`]: pickedarray
		});
		this.setState({
			checkAll: value,
			availableStock: this.state.availableStock
		});
	}

	updatesearchField(field, value) {
		let { search } = this.state;
		search[field] = value;
		this.setState({search});
	}

	render() {
		let availableStock = this.state.availableStock;
		let item = this.selector(this.props.fullstate, this.props.itemstr);
		let unShowMsg = !this.state.hasbatch && !this.state.hasserial && !this.props.app.useSublocation;
		let statusparam = ['Approved', 'Dispatched', 'Cancelled'].indexOf(this.props.resource.status) >= 0;

		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Pick Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Item Name</label>
							<input type="text" className="form-control" value={item.itemid_name} disabled></input>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Quantity</label>
							<input type="number" className="form-control" value={item.quantity} disabled></input>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Warehouse</label>
							<SelectAsync resource={"stocklocations"} fields={"id,name"} filter={`stocklocations.companyid=${this.props.resource.companyid} and stocklocations.isparent and stocklocations.parentid is null`} value={item.warehouseid} className={`${!item.warehouseid ? 'errorinput' : ''}`} required={true} disabled={true} />
						</div>
						{!statusparam && this.state.hasserial ? <div className="form-group col-md-3 col-sm-6">
							<label className="labelclass margintop-25">Cumulative Picking  <input  type="checkbox" onChange={(e) => this.setState({cumulativePicking: e.target.checked})} checked={this.state.cumulativePicking ? true : false} /></label>
						</div> : null}
					</div>
					{!statusparam && unShowMsg ? <div className="row">
						<div className="form-group col-md-12 col-sm-12 col-xs-12 alert alert-warning">No need to Pick the Stock</div>
						</div> : <div className="row">
						{!statusparam && this.state.hasserial ? <div className="form-group col-md-3 col-sm-6">
							<input type="text" className="form-control"  onChange={(e) =>{this.updatesearchField('serialno', e.target.value)}} value={this.state.search.serialno || ""} placeholder='Search by Serial No' />
						</div> : null}
						{this.props.app.useSublocation ? <div className="form-group col-md-3 col-sm-6">
							<input type="text" className="form-control" onChange={(e) =>{this.updatesearchField('stocklocationid_name', e.target.value)}} value={this.state.search.stocklocationid_name || ""} placeholder='Search by Sub Location' />
						</div> : null }
						{!statusparam && this.state.hasbatch ? <div className="form-group col-md-3 col-sm-6">
							<input type="text" className="form-control" onChange={(e) => this.updatesearchField('itembatchid_batchnumber', e.target.value)} value={this.state.search.itembatchid_batchnumber || ""} placeholder='Search by Batch Number' />
						</div> : null}
						{!statusparam ? <div className="form-group col-md-3 col-sm-6">
							<input type="text" className="form-control" onChange={(e) => this.updatesearchField('invoiceno', e.target.value)} value={this.state.search.invoiceno || ""} placeholder='Search by Invoice No' />
						</div> : null}
						{!statusparam ? <div className="form-group col-md-3 col-sm-6">
							<DateElement className="form-control" onChange={(value) => this.updatesearchField('invoicedate', value)} value={this.state.search.invoicedate} placeholder='Search by Invoice Date' />
						</div> : null}
						{!statusparam ? <div className="form-group col-md-3 col-sm-6"><p><span className="badge badge-secondary">{this.getCount()}</span>  Items picked</p></div> : null}
					</div>}
					{!statusparam && !unShowMsg && !this.state.hasserial ? <div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										{this.props.app.useSublocation ? <th className="text-center">Location</th> : null }
										{this.state.hasbatch ? <th className="text-center">Batch No</th> : null }
										{this.state.hasbatch ? <th className="text-center">Manufacturing Date</th> : null }
										{this.state.hasbatch ? <th className="text-center">Expiry Date</th> : null }
										<th className="text-center">Quantity</th>
										<th className="text-center"></th>
										<th className="text-center">Remarks</th>
									</tr>
								</thead>
								<tbody>
									{search(availableStock,this.state.search).map((rowitem, index)=> {
										return (
											<tr key={index}>
												{this.props.app.useSublocation ? <td className="text-center">{rowitem.stocklocationid_name}</td> : null }
												{this.state.hasbatch ? <td className="text-center">{rowitem.itembatchid_batchnumber}</td> : null }
												{this.state.hasbatch ? <td className="text-center">{dateFilter(rowitem.itembatchid_manufacturingdate)}</td> : null }
												{this.state.hasbatch ? <td className="text-center">{dateFilter(rowitem.itembatchid_expirydate)}</td> : null }
												<td className="text-center">{rowitem.quantity}</td>
												<td className="text-center">
													<input className="form-control" type="number" onChange={(e) => this.quantityonChange(rowitem, e.target.value)} value={rowitem.pickedQuantity || ''} placeholder="Enter Quantity" />
												</td>
												<td className="text-center">
													<input className="form-control" type="text" onChange={(e) => this.remarksonChange(rowitem, e.target.value)} value={rowitem.pickingremarks || ''} disabled={!(rowitem.pickedQuantity > 0)} placeholder="Enter Remarks" />
												</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div> : null }
					{!statusparam && !unShowMsg && this.state.hasserial ? <div className="row">
						<div className="col-md-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th className="text-center"><input  type="checkbox" onChange={(e) => this.checkAll(e.target.checked)} checked={this.state.checkAll ? true : false} /></th>
										<th className="text-center">Serial No</th>
										{this.props.app.useSublocation ? <th className="text-center">Location</th> : null }
										{this.state.hasbatch ? <th className="text-center">Batch No</th> : null }
										{this.state.hasbatch ? <th className="text-center">Manufacturing Date</th> : null }
										{this.state.hasbatch ? <th className="text-center">Expiry Date</th> : null }
										<th className="text-center">Invoice No</th>
										<th className="text-center">Invoice Date</th>
										<th className="text-center">Serial No Remarks</th>
										<th className="text-center">Remarks</th>
									</tr>
								</thead>
								<tbody>
									{search(availableStock,this.state.search).map((rowitem, index)=> {
										return (
											<tr key={index}>
												<td className="text-center">
													<input  type="checkbox" onChange={(e) => this.checkboxonChange(rowitem, e.target.checked)} checked={rowitem.check ? true : false} />
												</td>
												<td className="text-center">{rowitem.serialno}</td>
												{this.props.app.useSublocation ? <td className="text-center">{rowitem.stocklocationid_name}</td> : null}
												{this.state.hasbatch ? <td className="text-center">{rowitem.itembatchid_batchnumber}</td> : null }
												{this.state.hasbatch ? <td className="text-center">{dateFilter(rowitem.itembatchid_manufacturingdate)}</td> : null }
												{this.state.hasbatch ? <td className="text-center">{dateFilter(rowitem.itembatchid_expirydate)}</td> : null }
												<td className="text-center">{rowitem.invoiceno}</td>
												<td className="text-center">{dateFilter(rowitem.invoicedate)}</td>
												<td className="text-center">{rowitem.remarks}</td>
												<td className="text-center">
													<input className="form-control" type="text" onChange={(e) => this.remarksonChange(rowitem, e.target.value)} value={rowitem.pickingremarks || ''} disabled={rowitem.check ? false : true} placeholder="Enter Remarks" />
												</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div> : null }
					{statusparam ? <div className="row">
						<div className="col-md-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										{item.itemid_hasserial ? <th className="text-center">Serial No</th> : null}
										{this.props.app.useSublocation ? <th className="text-center">Location</th> : null }
										{item.itemid_hasbatch ? <th className="text-center">Batch No</th> : null}
										{item.itemid_hasbatch ? <th className="text-center">Manufacturing Date</th> : null}
										{item.itemid_hasbatch ? <th className="text-center">Expiry Date</th> : null}
										{!item.itemid_hasserial ? <th className="text-center">Quantity</th> : null}
										{item.itemid_hasserial ? <th className="text-center">Invoice No</th> : null}
										{item.itemid_hasserial ? <th className="text-center">Invoice Date</th> : null}
										{item.itemid_hasserial ? <th className="text-center">Serial No Remarks</th> : null}
										<th className="text-center">Remarks</th>
									</tr>
								</thead>
								<tbody>
									{item.pickingdetails.details.map((rowitem, index)=> {
										return (
											<tr key={index}>
												{item.itemid_hasserial ? <td className="text-center">{rowitem.serialno}</td> : null}
												{this.props.app.useSublocation ? <td className="text-center">{rowitem.stocklocationid_name}</td> : null }
												{item.itemid_hasbatch ? <td className="text-center">{rowitem.itembatchid_batchnumber}</td> : null }
												{item.itemid_hasbatch ? <td className="text-center">{dateFilter(rowitem.itembatchid_manufacturingdate)}</td> : null }
												{item.itemid_hasbatch ? <td className="text-center">{dateFilter(rowitem.itembatchid_expirydate)}</td> : null }
												{!item.itemid_hasserial ? <td className="text-center">{rowitem.quantity}</td> : null}
												{item.itemid_hasserial ? <td className="text-center">{rowitem.invoiceno}</td> : null}
												{item.itemid_hasserial ? <td className="text-center">{dateFilter(rowitem.invoicedate)}</td> : null}
												{item.itemid_hasserial ? <td className="text-center">{rowitem.remarks}</td> : null}
												<td className="text-center">{rowitem.pickingremarks}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div> : null}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
};
