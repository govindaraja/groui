
import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { uomFilter, currencyFilter } from '../../utils/filter';
import { NumberElement } from '../utilcomponents';

export default class ProjectMarkupModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			boqArray: [],
			itemDetailArray: [],
			childname: this.props.resourcename == 'projects' ? 'projectitems' : 'projectquoteitems',
			itemprop: this.props.resourcename == 'projects' ? 'boqitemsid' : 'boqquoteitemsid',
			itemqtyprop: this.props.resourcename == 'projects' ? 'boqitemsid_estimatedqty' : 'boqquoteitemsid_estimatedqty'
		};

		this.onLoad = this.onLoad.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.markuppercentOnChange = this.markuppercentOnChange.bind(this);
		this.applyMarkupPercent = this.applyMarkupPercent.bind(this);
		this.calculateRate = this.calculateRate.bind(this);
		this.selectAll = this.selectAll.bind(this);
		this.applyMarkupOnRateField = this.applyMarkupOnRateField.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let {childname, itemprop, itemqtyprop} = this.state;

		let boqObj = {};
		let resourceitems = [...this.props.resource[childname]];

		resourceitems.forEach((item, itemindex) => {
			if(!item.id)
				return null;

			boqObj[item.id] = {
				id: item.id,
				clientrefno: item.clientrefno,
				internalrefno: item.internalrefno,
				itemtype: item.itemtype,
				itemid: item.itemid ? item.itemid : null,
				itemid_name: item.itemtype == 'Section' ? item.name : item.itemid_name,
				itemid_itemtype: item.itemtype == 'Section' ? null : item.itemid_itemtype,
				description: item.itemtype == 'Section' ? null : item.description,
				quantity: item.itemid ? item.quantity : null,
				israteonly: item.israteonly ? true : false,
				uomid: item.uomid,
				rate: item.rate ? item.rate : null,
				amount: item.amount ? item.amount : 0,
				estimatedqty: item.itemid > 0 ? item.estimatedqty : null,
				estimatedrate: null,
				estimatedamount: null,
				displayindex: itemindex,
				childids: [],
				estimationitemsArr: []
			};
		});

		axios.get(`/api/projectestimationitems?&field=id,boqquoteitemsid,boqitemsid,boqitemid,itemid,description,specification,quantity,rate,amount,uomconversiontype,uomconversionfactor,uomid,billinguomid,usebillinguom,billingconversionfactor,billingconversiontype,billingquantity,activitytypeid,projectitems/estimatedqty/boqitemsid,projectquoteitems/estimatedqty/boqquoteitemsid,projectquoteitems/israteonly/boqquoteitemsid&filtercondition=projectestimations_parentid.status != 'Cancelled' and ${this.props.resourcename == 'projectquotes' ? `projectestimations_parentid.projectquoteid=${this.props.resource.id}` : `projectestimations_parentid.projectid = ${this.props.resource.id}`}`).then((response)=> {

			if(response.data.message != 'success') {
				let apiResponse = commonMethods.apiResult(response);
				return modalService[apiResponse.methodName](apiResponse.message);
			}

			if(response.data.main.length == 0) {
				this.props.closeModal();
				return this.props.openModal(modalService['infoMethod']({
					header : 'Warning',
					body : `Sorry, there is no estimation for this ${this.props.resourcename == 'projects' ? 'project' : 'project quotation'}. Cannot do markup`,
					btnArray : ['Ok']
				}));
			}

			let totaloverheadcost = 0;

			response.data.main.forEach(item => {
				if(!boqObj[item[itemprop]]) {
					totaloverheadcost += item.amount || 0;
					return null;
				}

				boqObj[item[itemprop]].estimatedqty = item[itemqtyprop] ? item[itemqtyprop] : 0;
				boqObj[item[itemprop]].estimationitemsArr.push(item);
			});

			Object.keys(boqObj).forEach(prop => {
				let item = boqObj[prop];

				if(item.itemtype == 'Section')
					return null;

				if(!(item.estimatedqty > 0) && !item.israteonly)
					return null;

				let estimatedcost = 0;
				let totalEstAmount = 0;

				item.estimationitemsArr.forEach(secitem => {
					totalEstAmount += Number(((secitem.boqquoteitemsid_israteonly ? 1 : secitem.quantity) * (secitem.rate || 0)).toFixed(this.props.app.roundOffPrecision));
				});

				item.costrate = Number((totalEstAmount / (item.israteonly ? 1 : item.estimatedqty)).toFixed(this.props.app.roundOffPrecision));
				item.costamount = Number((item.costrate * item.quantity).toFixed(this.props.app.roundOffPrecision));

				item.markuppercent = Number((((item.rate - item.costrate) / item.costrate) * 100).toFixed(this.props.app.roundOffPrecision));
			});

			let boqArray = Object.keys(boqObj).map(prop => boqObj[prop]).sort((a,b) => a.displayindex - b.displayindex);

			boqArray.forEach((item, itemindex) => {
				boqArray.forEach((secitem, secindex) => {
					if(secindex != itemindex && secitem.internalrefno.indexOf(`${item.internalrefno}.`) === 0)
						item.childids.push(secitem.id);
				});
			});

			let updateObj = { boqObj, boqArray, totaloverheadcost };

			this.calculateRate(updateObj);

			this.setState({ ...updateObj });
		});
	}

	calculateRate(updateObj) {
		updateObj.boqArray.sort((a, b) => a.childids.length - b.childids.length);

		let boqObj = {};
		updateObj.boqArray.forEach(item => boqObj[item.id] = item);

		let notestimateitemFound = false;

		updateObj.totalestimatedcost = updateObj.totaloverheadcost ? updateObj.totaloverheadcost : (this.state.totaloverheadcost ? this.state.totaloverheadcost : 0);
		updateObj.totalrevenue = 0;
		updateObj.grossprofit = null;
		updateObj.markuppercent = null;
		updateObj.grossprofitpercent = null;

		updateObj.boqArray.forEach(item => {
			if(item.itemtype != 'Section') {
				updateObj.totalestimatedcost += (item.costamount || 0);
				updateObj.totalrevenue += (item.amount || 0);

				if(!(item.estimatedqty > 0))
					notestimateitemFound = true;

				return null;
			}

			item.costamount = 0;
			item.amount = 0;

			item.childids.forEach(id => {
				if(boqObj[id].itemtype != 'Section') {
					item.costamount += (boqObj[id].costamount || 0);
					item.amount += (boqObj[id].amount || 0);
				}
			});

			item.costamount = Number(item.costamount.toFixed(this.props.app.roundOffPrecision));
			item.amount = Number(item.amount.toFixed(this.props.app.roundOffPrecision));

			item.markuppercent = item.costamount > 0 ? Number((((item.amount - item.costamount) / item.costamount) * 100).toFixed(this.props.app.roundOffPrecision)) : null;
		});

		if(!notestimateitemFound) {
			updateObj.grossprofit = updateObj.totalrevenue - updateObj.totalestimatedcost;
			updateObj.markuppercent = updateObj.totalestimatedcost > 0 ? Number(((updateObj.grossprofit / updateObj.totalestimatedcost) * 100).toFixed(this.props.app.roundOffPrecision)) : '';
			updateObj.grossprofitpercent = updateObj.totalrevenue > 0 ? Number(((updateObj.grossprofit / updateObj.totalrevenue) * 100).toFixed(this.props.app.roundOffPrecision)) : '';
		}

		updateObj.boqArray.sort((a, b) => a.displayindex - b.displayindex);
	}

	selectAll(param) {
		let boqArray = [...this.state.boqArray];

		boqArray.forEach((item) => {
			if(item.itemtype == 'Section' || item.estimatedqty > 0 || item.israteonly)
				item.checked = param;
		});

		this.setState({ boqArray });
	}

	checkboxOnChange(value, item) {
		[item.id, ...item.childids].forEach(chidlitem => {
			this.state.boqArray.forEach(boqitem => {
				if(boqitem.id == chidlitem && (boqitem.estimatedqty > 0 || boqitem.itemtype == 'Section' || boqitem.israteonly)) {
					boqitem.checked = value;
				}
			});
		});
		this.setState({ boqArray: this.state.boqArray });
	}

	markuppercentOnChange(value) {
		this.setState({ markuppercent: value });
	}

	applyMarkupPercent() {
		let boqArray = [...this.state.boqArray];
		let checkfound = false;

		boqArray.forEach((item) => {
			if(item.itemtype != 'Section' && item.checked) {
				item.markuppercent = this.state.markuppercent;
				item.rate = Number((item.costrate + (item.costrate * (item.markuppercent / 100))).toFixed(this.props.app.roundOffPrecision));
				item.amount = Number((item.quantity * item.rate).toFixed(this.props.app.roundOffPrecision));
				checkfound = true;
			}
		});

		if(!checkfound) {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				body : 'Please select atleast one item.',
				btnArray : ['Ok']
			}));
		}

		let updateObj = { boqArray };

		this.calculateRate(updateObj);

		this.setState({ ...updateObj });
	}

	itemLevelOnChange(value, item, param) {
		item[param] = value;

		if(param == 'markuppercent') {
			item.rate = Number((item.costrate + (item.costrate * (item.markuppercent / 100))).toFixed(this.props.app.roundOffPrecision));
			item.amount = Number((item.quantity * item.rate).toFixed(this.props.app.roundOffPrecision));
		} else {
			item.amount = Number((item.quantity * item.rate).toFixed(this.props.app.roundOffPrecision));
			item.markuppercent = Number((((item.rate - item.costrate) / item.costrate) * 100).toFixed(this.props.app.roundOffPrecision))
		}

		let boqArray = [...this.state.boqArray];
		let updateObj = { boqArray };

		this.calculateRate(updateObj);

		this.setState({ ...updateObj });
	}

	applyMarkupOnRateField() {
		let tempObj = {};

		let resourceitems = [...this.props.resource[this.state.childname]];

		let boqObj = {};
		this.state.boqArray.forEach(item => boqObj[item.id] = item);

		resourceitems.forEach((item, itemindex) => {
			if(item.itemtype == 'Section')
				return null;

			let tempitemStr = `${this.state.childname}[${itemindex}]`;

			tempObj[`${tempitemStr}.rate`] = boqObj[item.id].rate;
			tempObj[`${tempitemStr}.discountquantity`] = null;

			if(item.usebillinguom) {
				tempObj[`${tempitemStr}.billingrate`] = Number(((boqObj[item.id].rate / item.billingconversionfactor) * (item.uomconversionfactor ? item.uomconversionfactor : 1)).toFixed(this.props.app.roundOffPrecision));
			}
		});

		this.props.updateFormState(this.props.form, tempObj);
		this.props.computeFinalRate();
		this.props.closeModal();
		this.props.openModal(modalService['infoMethod']({
			header : 'Success',
			body : 'Selling price set successfully for selected items',
			btnArray : ['Ok']
		}));
	}

	render() {
		if(this.state.boqArray.length == 0)
			return null;

		let modalbodyHeight = $(window).height() - ($('.react-modal-header').outerHeight() > 0 ? ($('.react-modal-header').outerHeight() + $('.react-modal-header').outerHeight()) : 100) - 70;

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title gs-text-color">Add Markup</h5>
				</div>
				<div className="react-modal-body" style={{minHeight: '350px', maxHeight: `${modalbodyHeight}px`, overflowY:'scroll'}}>
					<div className="row">
						<div className="col-md-6 col-sm-6 col-xs-6">
							<div className="custom-anchor-tag float-left" onClick={() => this.selectAll(true)}>Select all</div>
							<div className="custom-anchor-tag float-left ml-3" onClick={() => this.selectAll(false)}>Clear</div>
						</div>
						<div className="col-md-6 col-sm-6 col-xs-6">
							<div className="row">
								<div className="col-md-8 offset-md-4 form-group">
									<div className="row">
										<div className="col-md-5 text-right">
											<label>Markup %</label>
										</div>
										<div className="col-md-4">
											<NumberElement className={`form-control`} value={this.state.markuppercent} onChange={(value) => this.markuppercentOnChange(value)} />
										</div>
										<div className="col-md-3">
											<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={()=>this.applyMarkupPercent()} disabled={!this.state.markuppercent}>Apply</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<table className="table">
								<thead>
									<tr>
										<th className="text-center"></th>
										<th style={{width: '8%'}}>Ref No</th>
										<th>Item Name</th>
										<th className="text-center" style={{width: '10%'}}>Quantity</th>
										<th className="text-center" style={{width: '10%'}}>Est.Cost Rate</th>
										<th className="text-right" style={{width: '12%'}}>Est. Amount</th>
										<th className="text-center" style={{width: '10%'}}>% Markup</th>
										<th className="text-center" style={{width: '10%'}}>Rate</th>
										<th className="text-right" style={{width: '15%'}}>Amount</th>
									</tr>
								</thead>
								<tbody>
									 {this.state.boqArray.map((item, index) => {
										return (
											<tr key={index} style={{backgroundColor: item.itemtype == 'Section' ? '#f0f0f0' : ''}}>
												<td>
													{item.itemtype == 'Section' || item.estimatedqty > 0 || item.israteonly ? <input type="checkbox" checked={item.checked || false} onChange={(evt)=>{this.checkboxOnChange(evt.target.checked, item)}}/> : null}
												</td>
												<td>{item.internalrefno}</td>
												<td>
													<div className={item.itemtype == 'Section' ?'text-ellipsis-line-2' : ''}>{item.itemid_name}</div>
													<div className="text-ellipsis-line-2 font-13 text-muted">{item.description}</div>
												</td>
												<td className="text-center">{item.israteonly ? 'RO' : item.quantity} {`${uomFilter(item.uomid, this.props.app.uomObj)}`}</td>
												<td className="text-right">{currencyFilter(item.costrate, this.props.resource.currencyid, this.props.app)}</td>
												<td className="text-right">{currencyFilter(item.costamount, this.props.resource.currencyid, this.props.app)}</td>
												<td className="text-center">
													{item.itemtype != 'Section' && (item.estimatedqty > 0 || item.israteonly) ? <NumberElement className={`form-control text-center`} value={item.markuppercent} onChange={(value) => this.itemLevelOnChange(value, item, 'markuppercent')} /> : item.markuppercent}
												</td>
												<td className="text-right">
													{item.itemtype != 'Section' && (item.estimatedqty > 0 || item.israteonly) ? <NumberElement className={`form-control text-right`} value={item.rate} onChange={(value) => this.itemLevelOnChange(value, item, 'rate')} /> : currencyFilter(item.rate, this.props.resource.currencyid, this.props.app)}
												</td>
												<td className="text-right">{currencyFilter(item.amount, this.props.resource.currencyid, this.props.app)}</td>
											</tr>
										)
									 })}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row no-gutters">
						<div className="col-md-7 col-sm-7 col-xs-7 form-group" style={{borderBottom: '1px solid #CCCCCC', paddingBottom: '10px'}}>
							<div className="row">
								<div className="col-md-3 semi-bold-custom text-right">Total Estimated Cost</div>
								<div className="col-md-3 semi-bold-custom font-16"  style={{color: '#61adff'}}>{currencyFilter(this.state.totalestimatedcost, this.props.resource.currencyid, this.props.app)}</div>
								<div className="col-md-3 semi-bold-custom text-right">Total Revenue</div>
								<div className="col-md-3 semi-bold-custom font-16"  style={{color: '#61adff'}}>{currencyFilter(this.state.totalrevenue, this.props.resource.currencyid, this.props.app)}</div>
							</div>
						</div>
						<div className="col-md-5 col-sm-5 col-xs-5 form-group"  style={{borderBottom: '1px solid #CCCCCC', paddingBottom: '10px'}}>
							<div className="row">
								<div className="col-md-3 semi-bold-custom text-right">Gross Margin</div>
								<div className="col-md-4 semi-bold-custom font-16"  style={{color: '#61adff'}}>{currencyFilter(this.state.grossprofit, this.props.resource.currencyid, this.props.app)}</div>
								<div className="col-md-3 semi-bold-custom text-right">Markup %</div>
								<div className="col-md-2 semi-bold-custom font-16" style={{color: '#61adff'}}>{this.state.markuppercent}</div>
								<div className="col-md-3 offset-md-7 semi-bold-custom text-right">Gross Margin %</div>
								<div className="col-md-2 semi-bold-custom font-16" style={{color: '#61adff'}}>{this.state.grossprofitpercent}</div>
							</div>
						</div>
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Cancel</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.applyMarkupOnRateField}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}