import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { dateFilter, currencyFilter, uomFilter } from '../../utils/filter';

class ProjectEstimatedCostBreakupOverviewDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			itemObj : {}
		};
		this.onLoad = this.onLoad.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let resourceitems = [...this.props.resourceitems];
		let tempItemObj = resourceitems[this.props.index];

		axios.get(`/api/projectestimationitems?&field=id,boqquoteitemsid,boqitemsid,projectitems/estimatedqty/boqitemsid,projectquoteitems/estimatedqty/boqquoteitemsid,boqitemid,itemid,description,specification,quantity,rate,amount,itemmaster/name/boqitemid,itemmaster/defaultpurchasecost/itemid,itemmaster/lastpurchasecost/itemid,itemmaster/itemtype/boqitemid,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/issaleskit/itemid,itemmaster/uomgroupid/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/itemtype/itemid,itemmaster/isuseforinternallabour/itemid,uomconversiontype,uomconversionfactor,uomid,uom/name/uomid,billingquantity,billinguomid,billingconversionfactor,billingconversiontype,billingrate,usebillinguom,activitytypeid,timesheetactivitytypes/name/activitytypeid,remarks,projectestimations/projectquoteid/parentid,projectestimations/currencyid/parentid&filtercondition=projectestimations_parentid.status != 'Cancelled' and ${this.props.resourcename == 'projectquotes' ? `projectestimationitems.boqquoteitemsid = ` : `projectestimationitems.boqitemsid=`}${tempItemObj.id}`).then((response)=> {
			if(response.data.message == 'success') {
				if(response.data.main.length > 0) {
					let estimationitemsArr = response.data.main;
					let tempChildobj = {
						boqquoteitemsid : tempItemObj.id,
						boqitemsid : tempItemObj.id,
						internalrefno : tempItemObj.internalrefno,
						clientrefno : tempItemObj.clientrefno,
						itemid : tempItemObj.itemid,
						itemid_name : tempItemObj.itemid_name,
						itemid_itemtype : tempItemObj.itemid_itemtype,
						itemid_keepstock : tempItemObj.itemid_keepstock,
						itemid_issaleskit : tempItemObj.itemid_issaleskit,
						itemid_isuseforinternallabour : tempItemObj.itemid_isuseforinternallabour,
						quantity : tempItemObj.quantity,
						israteonly : tempItemObj.israteonly,
						uomid : tempItemObj.uomid,
						uomid_name : tempItemObj.uomid_name,
						usebillinguom : tempItemObj.usebillinguom,
						billingquantity : tempItemObj.billingquantity,
						billinguomid : tempItemObj.billinguomid,
						billingconversionfactor : tempItemObj.billingconversionfactor,
						billingconversiontype : tempItemObj.billingconversiontype,
						billingrate : tempItemObj.billingrate,
						uomconversionfactor : tempItemObj.uomconversionfactor,
						uomconversiontype : tempItemObj.uomconversiontype,
						description : tempItemObj.description,
						specification : tempItemObj.specification,
						remarks : tempItemObj.remarks,
						rate : tempItemObj.rate,
						amount : tempItemObj.amount,
						estimatedqty : tempItemObj.estimatedqty,
						itemid_defaultpurchasecost : tempItemObj.itemid_defaultpurchasecost,
						itemid_lastpurchasecost : tempItemObj.itemid_lastpurchasecost,
						estimationtype : 'Simple',
						productitems: [],
						subcontractitems: [],
						labouritems: []
					};

					let itemprop = this.props.resourcename == 'projectquotes' ? 'boqquoteitemsid' : 'boqitemsid';
					let uniqueItemsObj = {};

					estimationitemsArr.forEach((item) => {
						if(item[itemprop] > 0) {
							if(uniqueItemsObj[item[itemprop]])
								uniqueItemsObj[item[itemprop]].push(item);
							else
								uniqueItemsObj[item[itemprop]] = [item];
						}
					});

					for(var prop in uniqueItemsObj) {
						if(uniqueItemsObj[prop].length > 1) {
							tempChildobj.estimationtype = 'Detailed';
							tempChildobj.productitems = [];
							tempChildobj.subcontractitems = [];
							tempChildobj.labouritems = [];
						} else {
							tempChildobj.estimationtype = uniqueItemsObj[prop][0].itemid != uniqueItemsObj[prop][0].boqitemid ? 'Detailed' : 'Simple';
							tempChildobj.rate = uniqueItemsObj[prop][0].rate;
							tempChildobj.billingrate = uniqueItemsObj[prop][0].billingrate;
							tempChildobj.usebillinguom = uniqueItemsObj[prop][0].itemid == uniqueItemsObj[prop][0].boqitemid ? uniqueItemsObj[prop][0].usebillinguom : tempChildobj.usebillinguom,
							tempChildobj.billingquantity = uniqueItemsObj[prop][0].itemid == uniqueItemsObj[prop][0].boqitemid ? uniqueItemsObj[prop][0].billingquantity : tempChildobj.billingquantity,
							tempChildobj.billinguomid = uniqueItemsObj[prop][0].itemid == uniqueItemsObj[prop][0].boqitemid ? uniqueItemsObj[prop][0].billinguomid : tempChildobj.billinguomid,
							tempChildobj.billingconversionfactor = uniqueItemsObj[prop][0].itemid == uniqueItemsObj[prop][0].boqitemid ? uniqueItemsObj[prop][0].billingconversionfactor : tempChildobj.billingconversionfactor,
							tempChildobj.billingconversiontype = uniqueItemsObj[prop][0].itemid == uniqueItemsObj[prop][0].boqitemid ? uniqueItemsObj[prop][0].billingconversiontype : tempChildobj.billingconversiontype,
							tempChildobj.billingrate = uniqueItemsObj[prop][0].itemid == uniqueItemsObj[prop][0].boqitemid ? uniqueItemsObj[prop][0].billingrate : tempChildobj.billingrate,
							tempChildobj.productitems = [];
							tempChildobj.subcontractitems = [];
							tempChildobj.labouritems = [];
						}
						if(tempChildobj.estimationtype == 'Detailed') {
							uniqueItemsObj[prop].forEach((propitem) => {
								if(propitem.itemid_itemtype == 'Product')
									tempChildobj.productitems.push(propitem);
								else if(propitem.itemid_itemtype == 'Project' || (propitem.itemid_itemtype == 'Service' && !propitem.itemid_isuseforinternallabour))
									tempChildobj.subcontractitems.push(propitem);
								else if(propitem.itemid_itemtype == 'Service' && propitem.itemid_isuseforinternallabour)
									tempChildobj.labouritems.push(propitem);
							});
						}
					}

					let itemqty = tempChildobj.estimatedqty;
					let temprate = 0;
					if(tempChildobj.estimationtype == 'Detailed') {
						tempChildobj.productitems.forEach((productitem) => {
							temprate += Number((productitem.amount / itemqty).toFixed(this.props.app.roundOffPrecision));
						});
						tempChildobj.subcontractitems.forEach((subcontractitem) => {
							temprate += Number((subcontractitem.amount / itemqty).toFixed(this.props.app.roundOffPrecision));
						});
						tempChildobj.labouritems.forEach((labouritem) => {
							temprate += Number((labouritem.amount / itemqty).toFixed(this.props.app.roundOffPrecision));
						});

						if(tempChildobj.usebillinguom) {
							tempChildobj.billingrate = temprate;
							tempChildobj.rate = Number(((tempChildobj.billingrate / (tempChildobj.uomconversionfactor ? tempChildobj.uomconversionfactor : 1)) * tempChildobj.billingconversionfactor).toFixed(this.props.app.roundOffPrecision));
						} else {
							tempChildobj.rate = temprate;
						}
						tempChildobj.amount = Number((tempChildobj.rate * itemqty).toFixed(this.props.app.roundOffPrecision));
					}

					let restrictestimationcommercials = false;
					this.props.app.feature.restrictprojectestimationrole.some((restrictrole) => {
						if(this.props.app.user.roleid.indexOf(restrictrole) > -1) {
							restrictestimationcommercials = true;
							return true;
						}
					});

					this.setState({
						itemObj : tempChildobj,
						restrictestimationcommercials
					});
				}
			}
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Estimation Details</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					{Object.keys(this.state.itemObj).length > 0 ? <div className="row">
						<div className="col-md-12">
							<table className="table table-bordered">
								<tbody>
									<tr>
										<td style={{width: '25%'}}>
											<span className="text-muted">Internal Ref. No</span>
											<br></br>
											<span>{this.state.itemObj.internalrefno}</span>
										</td>
										<td style={{width: '20%'}}>
											<span className="text-muted">Client Ref. No</span>
											<br></br>
											<span>{this.state.itemObj.clientrefno}</span>
										</td>
										<td style={{width: '25%'}}>
											<span className="text-muted">Item Name</span>
											<br></br>
											<span>{this.state.itemObj.itemid_name}</span>
										</td>
										<td>
											<div className="text-ellipsis">
												<span className="text-muted">Item Description</span>
												<br></br>
												<span>{this.state.itemObj.description}</span>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div className="text-ellipsis">
												<span className="text-muted">Specification</span>
												<br></br>
												<div dangerouslySetInnerHTML={{ __html: this.state.itemObj.specification ? this.state.itemObj.specification : ''}} ></div>
											</div>
										</td>
										<td>
											<span className="text-muted">Quantity</span>
											<br></br>
											<span>{`${this.state.itemObj.israteonly ? 'RO' : this.state.itemObj.quantity} ${uomFilter(this.state.itemObj.uomid, this.props.app.uomObj)}`}</span>
										</td>
										{this.state.itemObj.usebillinguom ? <td>
											<span className="text-muted">Billing Quantity</span>
											<br></br>
											<span>{`${this.state.itemObj.billingquantity} ${uomFilter(this.state.itemObj.billinguomid, this.props.app.uomObj)}`}</span>
										</td> : null}
										{!this.state.restrictestimationcommercials ? <td>
											<span className="text-muted">Default Purchase Cost</span>
											<br></br>
											<span>{`${currencyFilter(this.state.itemObj.itemid_defaultpurchasecost, this.state.itemObj.parentid_currencyid, this.props.app)}`}</span>
										</td> : null}
										{!this.state.itemObj.usebillinguom && !this.state.restrictestimationcommercials ? <td>
											<span className="text-muted">Last Purchase Cost</span>
											<br></br>
											<span>{`${currencyFilter(this.state.itemObj.itemid_lastpurchasecost, this.state.itemObj.parentid_currencyid, this.props.app)}`}</span>
										</td> : null}
										{this.state.restrictestimationcommercials ? <td colspan="2">
											<span>Estimated Quantity</span>
											<br></br>
											<span>{`${this.state.itemObj.estimatedqty} ${uomFilter(this.state.itemObj.uomid, this.props.app.uomObj)}`}</span>
										</td> : null}
									</tr>
									<tr>
										{this.state.itemObj.usebillinguom && !this.state.restrictestimationcommercials ? <td>
											<span className="text-muted">Last Purchase Cost</span>
											<br></br>
											<span>{`${currencyFilter(this.state.itemObj.itemid_lastpurchasecost, this.state.itemObj.parentid_currencyid, this.props.app)}`}</span>
										</td> : null}
										{!this.state.restrictestimationcommercials ? <td>
											<span>Estimated Quantity</span>
											<br></br>
											<span>{`${this.state.itemObj.estimatedqty} ${uomFilter(this.state.itemObj.uomid, this.props.app.uomObj)}`}</span>
										</td> : null}
										{!this.state.restrictestimationcommercials ? <td>
											<span className="text-muted">Estimated Cost (Per Unit)</span>
											<br></br>
											<span>{currencyFilter(this.state.itemObj.rate, this.state.itemObj.parentid_currencyid, this.props.app)}</span>
										</td> : null}
										<td colSpan={`${this.state.restrictestimationcommercials ? 4 : (this.state.itemObj.usebillinguom ? 1 : 2)}`}>
											<span className="text-muted">Remarks</span>
											<br></br>
											<span>{this.state.itemObj.remarks}</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						{this.state.itemObj.productitems.length > 0 ? <div className="col-md-12 col-sm-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th colSpan="7" className="gs-text-color">Materials</th>
									</tr>
									<tr>
										<th className="text-center">Item Name</th>
										{!this.state.restrictestimationcommercials ? <th className="text-center">Default Purchase Cost</th> : null}
										{!this.state.restrictestimationcommercials ? <th className="text-center">Last Purchase Cost</th> : null}
										<th className="text-center">Quantity</th>
										{!this.state.restrictestimationcommercials ? <th className="text-center">Estimated Cost (Per Unit)</th> : null}
										{!this.state.restrictestimationcommercials ? <th className="text-center">Estimated Amount</th> : null}
									</tr>
								</thead>
								<tbody>
									{this.state.itemObj.productitems.map((item, index) => {
										return (
											<tr key={index}>
												<td>{item.itemid_name}</td>
												{!this.state.restrictestimationcommercials ? <td>{currencyFilter(item.itemid_defaultpurchasecost, this.state.itemObj.parentid_currencyid, this.props.app)}</td> : null}
												{!this.state.restrictestimationcommercials ? <td>{currencyFilter(item.itemid_lastpurchasecost, this.state.itemObj.parentid_currencyid, this.props.app)}</td> : null}
												<td className="text-center">{`${item.quantity} ${uomFilter(item.uomid, this.props.app.uomObj)}`}</td>
												{!this.state.restrictestimationcommercials ? <td className="text-right">{currencyFilter(item.rate, this.state.itemObj.parentid_currencyid, this.props.app)}</td> : null}
												{!this.state.restrictestimationcommercials ? <td className="text-right">{currencyFilter(item.amount, this.state.itemObj.parentid_currencyid, this.props.app)}</td> : null}
											</tr>
										);
									})}
								</tbody>
							</table>
						</div> : null}
						{this.state.itemObj.subcontractitems.length > 0 ? <div className="col-md-12 col-sm-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th colSpan="7" className="gs-text-color">Sub Contract</th>
									</tr>
									<tr>
										<th className="text-center">Item Name</th>
										{!this.state.restrictestimationcommercials ? <th className="text-center">Default Purchase Cost</th> : null}
										{!this.state.restrictestimationcommercials ? <th className="text-center">Last Purchase Cost</th> : null}
										<th className="text-center">Quantity</th>
										{!this.state.restrictestimationcommercials ? <th className="text-center">Estimated Cost (Per Unit)</th> : null}
										{!this.state.restrictestimationcommercials ? <th className="text-center">Estimated Amount</th> : null}
									</tr>
								</thead>
								<tbody>
									{this.state.itemObj.subcontractitems.map((item, index) => {
										return (
											<tr key={index}>
												<td>{item.itemid_name}</td>
												{!this.state.restrictestimationcommercials ? <td>{currencyFilter(item.itemid_defaultpurchasecost, this.state.itemObj.parentid_currencyid, this.props.app)}</td> : null}
												{!this.state.restrictestimationcommercials ? <td>{currencyFilter(item.itemid_lastpurchasecost, this.state.itemObj.parentid_currencyid, this.props.app)}</td> : null}
												<td className="text-center">{`${item.quantity} ${uomFilter(item.uomid, this.props.app.uomObj)}`}</td>
												{!this.state.restrictestimationcommercials ? <td className="text-right">{currencyFilter(item.rate, this.state.itemObj.parentid_currencyid, this.props.app)}</td> : null}
												{!this.state.restrictestimationcommercials ? <td className="text-right">{currencyFilter(item.amount, this.state.itemObj.parentid_currencyid, this.props.app)}</td> : null}
											</tr>
										);
									})}
								</tbody>
							</table>
						</div> : null}
						{this.state.itemObj.labouritems.length > 0 ? <div className="col-md-12 col-sm-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th colSpan="5" className="gs-text-color">Internal Labour</th>
									</tr>
									<tr>
										<th className="text-center">Item Name</th>
										<th className="text-center">Quantity</th>
										{!this.state.restrictestimationcommercials ? <th className="text-center">Estimated Cost (Per Hour)</th> : null}
										{!this.state.restrictestimationcommercials ? <th className="text-center">Estimated Amount</th> : null}
									</tr>
								</thead>
								<tbody>
									{this.state.itemObj.labouritems.map((item, index) => {
										return (
											<tr key={index}>
												<td>{item.itemid_name}</td>
												<td className="text-center">{`${item.quantity} ${uomFilter(item.uomid, this.props.app.uomObj)}`}</td>
												{!this.state.restrictestimationcommercials ? <td className="text-right">{currencyFilter(item.rate, this.state.itemObj.parentid_currencyid, this.props.app)}</td> : null}
												{!this.state.restrictestimationcommercials ? <td className="text-right">{currencyFilter(item.amount, this.state.itemObj.parentid_currencyid, this.props.app)}</td> : null}
											</tr>
										);
									})}
								</tbody>
							</table>
						</div> : null}
					</div> : <div className="row">
						<div className="col-md-12 col-sm-12">
							<div className="alert alert-info text-center">Estimation is not available for this item yet</div>
						</div>
					</div>}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ProjectEstimatedCostBreakupOverviewDetails;