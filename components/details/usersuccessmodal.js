import React, { Component } from 'react';

export default class extends Component {
	constructor (props) {
		super (props);

		this.state = {};

		this.copyPassword = this.copyPassword.bind(this);
	}

	copyPassword (param) {
		let copyElement = document.createElement("textarea");
		copyElement.value = `${param}`;
		document.body.appendChild(copyElement);
		copyElement.select();

		document.execCommand('copy');
		document.body.removeChild(copyElement);
		this.props.closeModal();
	}

	render () {
		if (!this.props.resource)
			return null;

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header d-flex justify-content-between" style={{padding: '16px 22px'}}>
					<div className="flex-column">
						<h6 className="modal-title gs-text-color">{!this.props.resource.resetPwdFlag ? 'New user created successfully' : 'Success'}</h6>
					</div>
					<div className="flex-column">
						<button type="button" className="close" aria-label="Close" onClick={this.props.closeModal}><span aria-hidden="true">&times;</span></button>
					</div>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12 text-center form-group">
							<span>Password has been generated successfully!</span>
						</div>
						{this.props.resource.autogenpassword ? <div className="col-md-8 offset-md-2 form-group">
							<div className="row no-gutters">
								<div className="col-md-6">
									<input text="text" className="form-control text-center" style={{backgroundColor : '#f7f7f9'}} value={this.props.resource.generatedpassword} disabled></input>
								</div>
								<div className="col-md-6" style={{textAlign : 'right'}}>
									<button type="button" className="btn btn-sm gs-btn-outline-success btn-width" onClick={(evt) => {this.copyPassword(this.props.resource.generatedpassword);evt.stopPropagation();}}>Copy password</button>
								</div>
							</div>
						</div> : null}
						{!this.props.resource.resetPwdFlag ? <div className="col-md-12 col-sm-12 col-xs-12 text-center form-group">
							<span>Please select Roles for new user.</span>
						</div> : null}
					</div>
				</div>
			</div>
		);
	}
}