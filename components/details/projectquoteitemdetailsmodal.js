import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import * as utils from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			quotedetails: null,
			quoteitemdetails: [],
			quotekititemdetails: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.updatecheckbox = this.updatecheckbox.bind(this);
		this.getQuoteItemdetails = this.getQuoteItemdetails.bind(this);
		this.getQuoteKitItemdetails = this.getQuoteKitItemdetails.bind(this);
		this.addQuoteitem = this.addQuoteitem.bind(this);
	}

	componentWillMount() {
		if(this.props.resource.projectquoteid) {
			this.onLoad();
		}
	}

	onLoad() {
		let quotedetails = [];

		axios.get(`/api/projectquotes?&field=id,quoteno,customerid,customerreference&filtercondition=projectquotes.customerid=${this.props.resource.customerid} and projectquotes.id != ${this.props.resource.projectquoteid}  and projectquotes.status='Won'`).then((response) => {
			if (response.data.message == 'success') {
				quotedetails = response.data.main;
				for(var i = 0; i < quotedetails.length; i++) {
					quotedetails[i].checked = false;
				}

				this.setState({quotedetails}, () => {
					this.getQuoteItemdetails();
				});
			}
		});
	}

	getQuoteItemdetails() {
		let quoteitemdetails = [];

		let filterStr = "";
		for(var i = 0; i < this.state.quotedetails.length; i++) {
			filterStr += this.state.quotedetails[i].id +",";
		}
		filterStr = filterStr.substr(0, filterStr.length-1);

		if(filterStr != "") {
			axios.get(`/api/projectquoteitems?&field=id,name,internalrefno,itemtype,clientrefno,itemid,description,uomid,parentid,quantity,rate,discountquantity,discountmode,amount,taxid,taxdetails,itemmaster/name/itemid,itemmaster/itemtype/itemid,itemmaster/keepstock/itemid,itemmaster/quantitytype/itemid,itemmaster/keepstock/itemid,itemmaster/hasaddons/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid,itemnamevariationid,itemnamevariations/name/itemnamevariationid,uomconversionfactor,uomconversiontype,baseitemrate,addonrate,displaygroup,actualsize,size,number,drawingno,index,remarks,deliverydate,approvedmakes&filtercondition=projectquoteitems.parentid in (${filterStr})`).then((response) => {
				if (response.data.message == 'success') {
					quoteitemdetails = response.data.main;

					this.setState({quoteitemdetails}, () => {
						this.getQuoteKitItemdetails(filterStr);
					});
				}
			});
		}
	}

	getQuoteKitItemdetails(filterStr) {
		let quotekititemdetails = [];

		axios.get(`/api/kititemprojectquotedetails?&field=itemid,parent,uomid,parentid,quantity,conversion,rootindex,index,parentindex,itemmaster/name/itemid,itemmaster/keepstock/itemid,itemmaster/hasserial/itemid,itemmaster/hasbatch/itemid,itemmaster/issaleskit/itemid,uom/name/uomid,rate&filtercondition=kititemprojectquotedetails.parentid in (${filterStr})`).then((response) => {
			if (response.data.message == 'success') {
				quotekititemdetails = response.data.main;
				this.setState({quotekititemdetails});
			}
		});
	}

	addQuoteitem () {
		let projectitems = [...this.props.resource.projectitems];
		let kititemprojectdetails = [...this.props.resource.kititemprojectdetails];
		let index = 0;
		projectitems.forEach((item) => {
			if(index < item.index)
				index = item.index;
		});
		index = index + 1;
		let checkedFound = false;
		this.state.quotedetails.forEach((quote) => {
			if(quote.checked) {
				checkedFound = true;
				this.state.quoteitemdetails.forEach((item) => {
					if(item.parentid == quote.id) {
						let tempObj = {
							index: (index+1),
						};
						utils.assign(tempObj, item, [{'boqquoteitemsid' : 'id'}, 'itemid', 'itemid_name','itemid_itemtype', 'itemid_issaleskit', 'itemnamevariationid', 'itemnamevariationid_name', 'itemid_hasaddons', 'description', 'baseitemrate', 'addonrate', 'rate', 'quantity', 'uomid', 'uomid_name', 'itemid_uomgroupid', 'discountmode', 'drawingno', 'discountquantity', 'itemid_keepstock', 'itemid_quantitytype', 'taxid', 'uomconversionfactor', 'uomconversiontype', 'displaygroup', 'quoteid', {'quote_index':'index'}, 'remarks', 'deliverydate', 'internalrefno', 'clientrefno', 'itemtype', 'name','approvedmakes']);


						this.props.customFieldsOperation('projectquoteitems',tempObj, item,'projectitems');

						this.state.quotekititemdetails.forEach((kititem) => {
							if(kititem.parentid == quote.id && item.index == kititem.rootindex) {
								let tempKitObj = {
									rootindex: tempObj.index
								};
								utils.assign(tempKitObj, kititem, ['itemid', 'itemid_name', 'parent', 'uomid', 'uomid_name', 'quantity', 'conversion', 'rate', 'ratelc', 'itemid_issaleskit', 'itemid_hasserial', 'itemid_hasbatch', 'itemid_keepstock', 'warehouseid', 'index', 'parentindex']);
								kititemprojectdetails.push(tempKitObj);
							}
						});
						projectitems.push(tempObj);
						index++;
					}
				});
			}
		});

		if(!checkedFound) {
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				body : 'Please select atleast one quotation',
				btnArray : ['Ok']
			}));
		} else {
			this.props.updateFormState(this.props.form, { projectitems, kititemprojectdetails });
			setTimeout(() => {
				this.props.callback();
				this.props.closeModal();
			}, 0);
		}
	}

	updatecheckbox(item, field, value) {
		item[field] = value;
		this.setState({
			quotedetails: this.state.quotedetails
		});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Add Items from Project Quotation</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						{ this.state.quotedetails && this.state.quotedetails.length > 0 ? <div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th className="text-center"></th>
										<th className="text-center">Quote No</th>
										<th className="text-center">Customer Reference</th>
									</tr>
								</thead>
								<tbody>
									{this.state.quotedetails.map((item, index)=> {
										return (
											<tr key={index}>
												<td><input type="checkbox" onChange={(e) => this.updatecheckbox(item, 'checked', e.target.checked)}  checked={item.checked} /></td>
												<td>{item.quoteno}</td>
												<td>{item.customerreference}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div> : null }
						{ this.state.quotedetails && this.state.quotedetails.length == 0 ? <div className="col-md-12">
							<div className="alert alert-info text-center">No Pending items from Project Quotations!</div>
						</div> : null}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								{this.state.quotedetails && this.state.quotedetails.length > 0 ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addQuoteitem}><i className="fa fa-check"></i>Ok</button> : null}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
