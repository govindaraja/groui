import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService } from '../../utils/services';
import { NumberElement,SelectAsync } from '../utilcomponents';

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			orderitems : [],
			checkall : false
		};
		this.close = this.close.bind(this);
		this.create = this.create.bind(this);
		this.renderTableRows = this.renderTableRows.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.disabled = this.disabled.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
	}

	componentDidMount(){
		this.onLoad();	
	}

	disabled(){
		if(!this.state.orderitems || this.state.orderitems.length == 0){
			return true;
		}

		for(var i=0;i<this.state.orderitems.length;i++){
			if(this.state.orderitems[i].checked && (!this.state.orderitems[i].pendingqty || this.state.orderitems[i].pendingqty <= 0)){
				return true;
			}
		}
	}

	checkboxOnChange(value){
		this.state.orderitems.forEach((item,index)=>{
			item.checked = value; 
		});

		this.setState({
			orderitems : this.state.orderitems,
			checkall :value
		});
	}

	onLoad() {
		var queryString = `/api/query/getpendingproductionitemquery?orderid=${this.props.resource.id}`;
		axios.get(queryString).then((response) => {
			if (response.data.message == 'success') {
				this.setState({
					orderitems : response.data.main.map((orderitem,index) =>{
						let itemObj={}
						for(var i=0;i<this.props.resource.orderitems.length;i++){
							if(orderitem.id == this.props.resource.orderitems[i].id){
								itemObj={
									checked : false,
									includeaddon : true,
									...this.props.resource.orderitems[i],
									producedqty : orderitem.producedqty,
									pendingqty : orderitem.pendingqty > 0 ? orderitem.pendingqty : 0,
									bomid : orderitem.bomid
								}
								break;
							}
						}
						return itemObj;
					})
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				modalService[apiResponse.methodName](apiResponse.message);
			}
		});
	}

	updateAddOnField(item, field, value) {
		item[field] = value;
		this.setState({
			orderitems: this.state.orderitems
		});
	}	

	close() {
		this.props.closeModal();
	}

	create(){
		let itemArray = [];
		this.state.orderitems.forEach((item,index)=>{
			if(item.checked){
				itemArray.push(item.itemid);
			}
		});
		
		if(itemArray.length == 0)
			return this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					bodyArray : ["Please select atleast one Production Item"],
					btnArray : ['Ok']
				}));
		if(itemArray.length != new Set(itemArray).size)
			return this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					bodyArray : ["You can't add the same items under FInished Products."],
					btnArray : ['Ok']
				}));
		
		this.props.productionCallback(this.state.orderitems);
		this.props.closeModal();
	}

	renderTableRows(){
		return this.state.orderitems.map((orderitem, index) => {
			return <tr key={index} className={`${orderitem.checked ? 'itemcheck' : ''}`}>
						<td className="text-center">
							<input  type="checkbox" onChange={(e) => this.updateAddOnField(orderitem, 'checked', e.target.checked)}  checked={orderitem.checked} />
						</td>
						<td className="text-center">{orderitem.itemid_name}</td>
						<td className="text-center">{orderitem.quantity}</td>
						<td className="text-center">{orderitem.producedqty}</td>
						<td className="text-center">
							<NumberElement className={`form-control ${(orderitem.pendingqty <= 0 || !orderitem.pendingqty) &&  orderitem.checked ? 'errorinput' : ''}`} value={orderitem.pendingqty} onChange={(value) => this.updateAddOnField(orderitem, 'pendingqty', value)} />
						</td>
						<td className="text-center">
							<SelectAsync resource="bom" fields="id,name,isdefault" filter={`bom.itemid=${orderitem.itemid} and bom.status ='Approved'`} value={orderitem.bomid} onChange={(value)=>{this.updateAddOnField(orderitem, 'bomid', value)}} />
						</td>
						{
						  (this.props.app.feature.useItemAddOns) ?
						  	<td className={`text-center ${this.props.app.feature.useItemAddOns ? '' :'hidebtn'}`}>
								<input  type="checkbox" onChange={(e) => this.updateAddOnField(orderitem, 'includeaddon', e.target.checked)}  checked={orderitem.includeaddon} />
							</td>
							: null
						}
						
					</tr>
		});					
	}

	render() {
		
		let {orderitems} = this.state;
		
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Please Choose Production Item</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-12">
							{orderitems.length > 0 ? <table className="table table-bordered tablefixed">
								<thead>
									<tr>
										<th className="text-center" style={{width:'10%'}}><input  type="checkbox" onChange={(e) => {this.checkboxOnChange(e.target.checked);}}  checked={this.state.checkall} /></th>
										<th className="text-center">Item Name</th>
										<th className="text-center">Ordered Qty</th>
										<th className="text-center">So Far Ordered(Production) Qty</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">BOM</th>
										{
										  (this.props.app.feature.useItemAddOns) ?
										  	<th className={`text-center ${this.props.app.feature.useItemAddOns ? '' :'hidebtn'}`}>Include Add-Ons</th>
											: null
										}
									</tr>
								</thead>
								{
									this.renderTableRows()
								}
							</table> : <div className="col-md-12 col-sm-12 col-xs-12 alert alert-warning text-center">
									No items available for production !
								</div>}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" disabled={this.disabled()} onClick={this.create}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
