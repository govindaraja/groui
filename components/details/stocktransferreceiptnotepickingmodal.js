import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect } from '../utilcomponents';
import { uomFilter } from '../../utils/filter';
import Loadingcontainer from '../loadingcontainer';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search : {},
			loaderflag: false,
			receiptnoteItemArray : []
		};
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.receiptOnChange = this.receiptOnChange.bind(this);
		this.add = this.add.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	receiptOnChange(receiptid) {
		this.updateLoaderFlag(true);
		let receiptnoteItemArray = [];
		let receiptnoteCustomFields = this.props.getCustomFields('receiptnoteitems','string');

		axios.get(`/api/receiptnoteitems?field=itemid,uomid,itemmaster/name/itemid,description,uom/name/uomid,stockqty,warehouseid,itemmaster/issaleskit/itemid,itemmaster/hasaddons/itemid,itemmaster/keepstock/itemid,itemmaster/stockuomid/itemid,displayorder${receiptnoteCustomFields ? (','+receiptnoteCustomFields) : ''}&filtercondition=receiptnoteitems.parentid=${receiptid}`).then((response) => {
			if (response.data.message == 'success') {

				receiptnoteItemArray = response.data.main.filter((item) => !item.itemid_issaleskit && !item.itemid_hasaddons && item.itemid_keepstock);

				receiptnoteItemArray.forEach((item) => {
					item.quantity = item.stockqty;
					item.uomid = item.itemid_stockuomid;
				});

				for (var i = 0; i < this.props.resource.stocktransferitems.length; i++) {
					for (var j = 0; j < receiptnoteItemArray.length; j++) {
						if (this.props.resource.stocktransferitems[i].itemid == receiptnoteItemArray[j].itemid)
							receiptnoteItemArray[j].checked = true;
					}
				}

				receiptnoteItemArray.sort((a, b) => {
					return a.displayorder - b.displayorder;
				});

				this.setState({
					receiptid,
					receiptnoteItemArray
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	checkboxOnChange(item, value) {
		if(item === 'all') {
			let receiptnoteItemArray = [...this.state.receiptnoteItemArray];
			receiptnoteItemArray.forEach((item) => item.checked = value ? true : false);
			this.setState({
				checkall : value ? true : false,
				receiptnoteItemArray
			});
		} else {
			item.checked = value;
			this.setState({
				receiptnoteItemArray: this.state.receiptnoteItemArray
			});	
		}
	}

	add () {
		this.updateLoaderFlag(true);
		let stocktransferitems = [...this.props.resource.stocktransferitems];
		let itemArray  = [...this.state.receiptnoteItemArray];
		let warehouseidArray = [];

		itemArray.forEach((item) => {
			if(!item.checked)
				return true;

			if(warehouseidArray.indexOf(item.warehouseid) == -1)
				warehouseidArray.push(item.warehouseid);

			let itemFound = false;
			for(let i=0;i<stocktransferitems.length;i++) {
				if(item.itemid == stocktransferitems[i].itemid) {
					itemFound = true;
					stocktransferitems[i].quantity = item.quantity;
					break;
				}
			}

			if(!itemFound) {
				let tempObj = {
					itemid: item.itemid,
					itemid_name: item.itemid_name,
					quantity: item.quantity,
					uomid: item.uomid,
					uomid_name: uomFilter(item.uomid, this.props.app.uomObj),
					description: item.description
				};

				this.props.customFieldsOperation('receiptnoteitems', tempObj, item, 'stocktransferitems');

				stocktransferitems.push(tempObj);
			}
		});

		this.props.updateFormState(this.props.form, {
			stocktransferitems,
			sourcewarehouseid : warehouseidArray.length == 1 ? warehouseidArray[0] : this.props.resource.sourcewarehouseid
		});
		this.props.closeModal();
		this.updateLoaderFlag(false);
	}

	render() {
		let receiptidErrorClass = this.state.receiptid > 0 ? '' : 'errorinput';

		return (
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Receipt Note Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-3 form-group">
							<label className="labelclass">Receipt Note No</label>
							<AutoSelect resource={'receiptnotes'} fields={'id,receiptnotenumber'} value={this.state.receiptid} label={'receiptnotenumber'} valuename={'id'} filter={`receiptnotes.status in ('Approved','Received')`} onChange={(value) => this.receiptOnChange(value)} className={receiptidErrorClass} required />
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							{this.state.receiptnoteItemArray.length > 0 ? <table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th><input type="checkbox" onChange={(e) => this.checkboxOnChange('all', e.target.checked)} checked={this.state.checkall || false} /></th>
										<th className="text-center">Item Name</th>
										<th className="text-center">UOM</th>
										<th className="text-center">Quantity</th>
									</tr>
								</thead>
								<tbody>
									{search(this.state.receiptnoteItemArray, this.state.search).map((item, index) => {
										return (
											<tr key={index}>
												<td><input type="checkbox" onChange={(e) => this.checkboxOnChange(item, e.target.checked)} checked={item.checked || false} /></td>
												<td>{item.itemid_name}</td>
												<td className="text-center">{item.uomid_name}</td>
												<td className="text-center">{item.quantity}</td>
											</tr>
										)
									})}
								</tbody>
							</table> : (this.state.receiptid > 0 ? <div className="alert alert-warning text-center">No Items Found</div> : null)}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.add} disabled={this.state.receiptnoteItemArray.length==0}><i className="fa fa-plus"></i>Add</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});