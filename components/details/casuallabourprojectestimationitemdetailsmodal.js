import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { currencyFilter, dateFilter } from '../../utils/filter';
import { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { AutoSelect, DateElement } from '../utilcomponents';

export default connect((state) => {
	return {
		app: state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			projectEstimationItemArray: [],
			showdescription: false
		};
		this.onLoad = this.onLoad.bind(this);
		this.addProjectEstimationItem = this.addProjectEstimationItem.bind(this);
		this.checkboxonChange = this.checkboxonChange.bind(this);
		this.descToggleOnChange = this.descToggleOnChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let projectEstimationItemArray = [];

		axios.get(`/api/query/getworkorderprojectestimateitemsquery?projectid=${this.props.resource.projectid}&param=workprogress`).then((response) => {
			if (response.data.message == 'success') {
				projectEstimationItemArray = response.data.main;

				projectEstimationItemArray.forEach((estimationitem) => {
					if(this.props.item.projectestimationitemsid == estimationitem.projectestimationitemsid) {
						estimationitem.checked = true;
					}
				});
				
				this.setState({ projectEstimationItemArray });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	checkboxonChange(item, value) {
		item.checked = value;
		this.setState({
			projectEstimationItemArray: this.state.projectEstimationItemArray
		});
	}

	addProjectEstimationItem () {
		let itemdetailsarr = [];
		this.state.projectEstimationItemArray.forEach((item) => {
			if(item.checked) {
				itemdetailsarr.push(item);
			}
		});
		if(itemdetailsarr.length == 0 || itemdetailsarr.length > 1) {
			this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				body : 'Please choose only one item',
				btnArray : ['Ok']
			}));
		} else {
			this.props.updateFormState(this.props.form, {
					[`${this.props.itemstr}.projectestimationitemsid`] : itemdetailsarr[0].projectestimationitemsid,
					[`${this.props.itemstr}.boqitemsid`] : itemdetailsarr[0].boqitemsid,
					[`${this.props.itemstr}.boqitemsid_itemid`] : itemdetailsarr[0].boqitemsid_itemid,
					[`${this.props.itemstr}.boqitemsid_internalrefno`] : itemdetailsarr[0].internalrefno
				});
			setTimeout(() => {
				this.props.closeModal();
			}, 0);
		}
	}

	descToggleOnChange() {
		this.setState({showdescription: !this.state.showdescription});
	}

	render() {
		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Project Estimation Item Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="form-group col-md-4 col-sm-4">
							<label className="float-left">Show Description</label>
							<label className="gs-checkbox-switch">
								<input className="form-check-input" type="checkbox" checked={this.state.showdescription} onChange={() => this.descToggleOnChange()} />
								<span className="gs-checkbox-switch-slider"></span>
							</label>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th style={{width:'5%'}}></th>
										<th className="text-center">BOQ Details</th>
										<th className="text-center">Item Name</th>
										<th className="text-center">Estimated Qty</th>
										<th className="text-center">UOM</th>
										<th className="text-center">Remarks</th>
									</tr>
								</thead>
								<tbody>
									{this.state.projectEstimationItemArray.map((item, index) => {
										return (
											<tr key={index}>
												<td><input type="checkbox" onChange={(e) => this.checkboxonChange(item, e.target.checked)} checked={item.checked || false} /></td>
												<td>
													<div>
														<span className="text-muted">Internal Ref No </span>
														<span>: {item.internalrefno}</span>
													</div>
													{item.internalrefno != 'Overhead' ? <div>
														<span className="text-muted">Client Ref No </span>
														<span>: {item.clientrefno}</span>
													</div> : null}
													{item.internalrefno != 'Overhead' ? <div>
														<span className="text-muted">BOQ Item Name </span>
														<span>: {item.boqitemid_name}</span>
													</div> : null}
													{item.boqitemsid_description ? <div className={`${this.state.showdescription ? 'show' : 'hide'}`}>
														<span className="text-muted">BOQ Description </span>
														<span className="font-12">: {item.boqitemsid_description}</span>
													</div> : null}
												</td>
												<td>{item.itemid_name}<div className={`font-12 ${this.state.showdescription ? 'show' : 'hide'}`}>{item.description}</div></td>
												<td className="text-center">{item.estimatedqty}</td>

												<td className="text-center">{item.uomid_name}</td>
												<td>{item.remarks}</td>
											</tr>
										)
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addProjectEstimationItem}><i className="fa fa-check"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
