import React, { Component } from 'react';
import axios from 'axios';
import {connect} from 'react-redux';

import { search } from '../../utils/utils';
import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';
import { LocalSelect, AutoSelect } from '../utilcomponents';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			itemfrom:  'Item Request',
			componentdetails: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.quantityonChange = this.quantityonChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let componentdetails = [];
		axios.get(`/api/query/itemrequestitempickingquery?servicecallid=${this.props.resource.servicecallid}&engineerid=${this.props.resource.engineerid}&contractid=${this.props.resource.contractid}&equipmentid=${this.props.resource.equipmentid}&pricelistid=${this.props.resource.pricelistid}`).then((response) => {
			if (response.data.message == 'success') {
				for (var j = 0; j < this.props.resource.componentitems.length; j++) {
					for (var i = 0; i < response.data.main.length; i++) {
						if (this.props.resource.componentitems[j].itemrequestitemsid > 0 && response.data.main[i].itemrequestitemsid == this.props.resource.componentitems[j].itemrequestitemsid)
							response.data.main[i].quantityUsed = this.props.resource.componentitems[j].quantity;
						if(this.props.resource.componentitems[j].bufferstockitemsid > 0 && response.data.main[i].bufferstockitemsid == this.props.resource.componentitems[j].bufferstockitemsid)
							response.data.main[i].quantityUsed = this.props.resource.componentitems[j].quantity;
					}
				}
				componentdetails = response.data.main;
				this.setState({componentdetails});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	quantityonChange(value, index) {
		let componentdetails = [...this.state.componentdetails];
		componentdetails[index].quantityUsed = value;
		this.setState({
			componentdetails
		});
		let item = componentdetails[index];
		if (value) {
			let itemFound = false;
			for (var i = 0; i < this.props.resource.componentitems.length; i++) {
				let tempupdateObj = {};
				if(item.itemrequestitemsid > 0 && item.itemrequestitemsid == this.props.resource.componentitems[i].itemrequestitemsid) {
					itemFound = true;
					tempupdateObj[`componentitems[${i}].quantity`] = value;
					tempupdateObj[`componentitems[${i}].billingquantity`] = item.usebillinguom ? (Number(((value / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;

					this.props.updateFormState(this.props.form, tempupdateObj);
					break;
				}
				if(item.bufferstockitemsid > 0 && item.bufferstockitemsid == this.props.resource.componentitems[i].bufferstockitemsid) {
					itemFound = true;
					tempupdateObj[`componentitems[${i}].quantity`] = value;
					tempupdateObj[`componentitems[${i}].billingquantity`] = item.usebillinguom ? (Number(((value / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0;
					this.props.updateFormState(this.props.form, tempupdateObj);
					break;
				}
			}
			if (!itemFound) {
				let tempObj = {
					itemfrom : item.itemfrom,
					itemrequestitemsid : item.itemrequestitemsid,
					bufferstockitemsid : item.bufferstockitemsid,
					itemid : item.itemid,
					itemid_name : item.itemname,
					itemid_itemtype : item.itemtype,
					rate : item.freeorchargeable=='Free' ? 0 : item.rate,
					discountmode : item.discountmode,
					discountquantity : item.discountquantity,
					freeorchargeable : item.freeorchargeable,
					uomid : item.uomid,
					uomid_name : item.uomname,
					description : item.description,
					quantity : value,
					equipmentid : item.itemrequestitemsid > 0 ? item.equipmentid : null,
					contractid : item.itemrequestitemsid > 0 ? item.contractid : null,
					contractid_contractno : item.itemrequestitemsid > 0 ? item.contractid_contractno : null,
					contracttypeid : item.contracttypeid > 0 ? item.contracttypeid : null,
					taxid : item.taxid,
					alternateuom : item.uomconversiontype ? true : false,
					uomconversionfactor : item.uomconversionfactor,
					uomconversiontype : item.uomconversiontype,
					itemid_usebillinguom: item.itemid_usebillinguom,
					usebillinguom: item.usebillinguom,
					billinguomid: item.billinguomid,
					billingquantity: item.usebillinguom ? (Number(((value / (item.uomconversionfactor ? item.uomconversionfactor : 1)) * item.billingconversionfactor).toFixed(this.props.app.roundOffPrecisionStock))) : 0,
					billingrate: item.estimationbillingrate,
					billingconversiontype: item.billingconversiontype,
					billingconversionfactor: item.billingconversionfactor
				};

				this.props.customFieldsOperation('itemrequestitems', tempObj, item, 'servicereportitems');
				this.props.array.push('componentitems', tempObj);
			}
		} else {
			for (var i = 0; i < this.props.resource.componentitems.length; i++) {
				if (item.itemrequestitemsid > 0 && item.itemrequestitemsid == this.props.resource.componentitems[i].itemrequestitemsid) {
					this.props.array.remove('componentitems', i);
					break;
				}
				if (item.bufferstockitemsid > 0 && item.bufferstockitemsid == this.props.resource.componentitems[i].bufferstockitemsid) {
					this.props.array.remove('componentitems', i);
					break;
				}
			}
		}
		this.props.callback();
	}

	render() {

		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Item Request Item</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					<div className="row">
						<div className="col-md-3 form-group">
							<label className="labelclass">Component From</label>
							<LocalSelect options={['Item Request', 'Buffer Stock']} value={this.state.itemfrom} onChange={(itemfrom) => this.setState({itemfrom})} required={true} />
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							{this.state.componentdetails.length == 0 ? 
								<div className="alert alert-warning text-center">There is no item request items or buffer stock or items already consumed</div> : <table className="table table-bordered">
								<thead>
									<tr>
										<th className='text-center'>Item Name</th>
										<th className='text-center'>UOM</th>
										<th className='text-center'>Quantity Used</th>
										<th className='text-center'>Quantity Available</th>
										<th className='text-center'>Qty to be Used</th>
									</tr>
								</thead>
								<tbody>
									{this.state.componentdetails.map((item, index)=> {
										if((this.state.itemfrom == 'Item Request' && item.bufferstockitemsid > 0) || (this.state.itemfrom == 'Buffer Stock' && item.itemrequestitemsid > 0))
											return null;
										return (
											<tr key={index}>
												<td>{item.itemname}</td>
												<td className="text-center">{item.uomname}</td>
												<td className="text-center">{item.usedqty}</td>
												<td className="text-center">{item.remainingqty}</td>
												<td className="text-center" style={{width: '20%'}}><input type="number" className="form-control" value={item.quantityUsed} onChange={(evt)=>{this.quantityonChange(Number(evt.target.value), index)}} /></td>
											</tr>
										);
									})}
								</tbody>
							</table> }
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
