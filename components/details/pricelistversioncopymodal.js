import React, { Component } from 'react';

export default class pricelistversioncopymodal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.save = this.save.bind(this);
	}

	save () {
		this.props.callback(this.state.description);
		this.props.closeModal();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Copy Price List Version</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-4">
							<label>Description</label>
						</div>
						<div className="form-group col-md-8">
							<textarea className={`form-control ${!this.state.description ? 'errorinput' : ''}`} value={this.state.description} onChange={(evt) =>{ this.setState({description: evt.target.value});}}/>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width"  onClick={()=>this.save()} disabled={!this.state.description}><i className="fa fa-save"></i>Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
