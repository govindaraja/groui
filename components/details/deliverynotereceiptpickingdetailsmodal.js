import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { formValueSelector } from 'redux-form';

import { commonMethods, modalService } from '../../utils/services';
import { currencyFilter, dateFilter, uomFilter } from '../../utils/filter';
import { SelectAsync } from '../utilcomponents';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			item: {},
			search: {},
			availableReceipts: []
		};

		this.onLoad = this.onLoad.bind(this);
		this.selector = formValueSelector(this.props.form);
		this.quantityonChange = this.quantityonChange.bind(this);
		this.getCount = this.getCount.bind(this);
	}

	componentWillMount() {
		let {resource, itemstr} = this.props;
		let item = this.selector(this.props.fullstate, itemstr);
		if(this.props.resource.status != 'Approved' && this.props.resource.status != 'Dispatched' && this.props.resource.status != 'Cancelled') {
			if (!item.receiptpickingdetails || !item.receiptpickingdetails.details) {
				this.props.updateFormState(this.props.form, {
					[`${itemstr}.receiptpickingdetails`]: {
						details : []
					}
				});
			}
			setTimeout(() => {
				this.onLoad();
			}, 0);
		}
	}

	onLoad() {
		let {resource, itemstr} = this.props;
		let item = this.selector(this.props.fullstate, itemstr);

		if(!item.itemid || !item.warehouseid)
			return null;

		let tempsourceresource = item.sourceresource;
		let tempsourceid = item.sourceid;

		if(this.props.kititemparam) {
			for(var i = 0; i < this.props.resource.deliverynoteitems.length; i++) {
				if(this.props.resource.deliverynoteitems[i].index == item.rootindex) {
					tempsourceresource = this.props.resource.deliverynoteitems[i].sourceresource;
					tempsourceid = this.props.resource.deliverynoteitems[i].sourceid;
				}
			}
		}
		axios.get(`/api/query/availablereceiptsfordeliveryquery?itemid=${item.itemid}&warehouseid=${item.warehouseid}&sourceresource=${tempsourceresource}&sourceid=${tempsourceid}&uomid=${item.uomid}&uomconversiontype=${item.uomconversiontype}&uomconversionfactor=${item.uomconversionfactor}`).then((response) => {
			if (response.data.message == 'success') {
				let removeIDArray = [];
				let availableReceipts = response.data.main;
				let pickedarray = [...item.receiptpickingdetails.details];

				pickedarray.forEach((rowitem) => {
					let itemFound = false;
					availableReceipts.forEach((resrow) => {
						if(resrow.sourceresource == rowitem.sourceresource && resrow.sourceid == rowitem.sourceid) {
							itemFound = true;
							resrow.pickedQuantity = rowitem.quantity
						}
					});
					if(!itemFound)
						removeIDArray.push({
							sourceresource : rowitem.sourceresource, 
							sourceid : rowitem.sourceid
						})
				});
				removeIDArray.forEach((removeitem) => {
					for(var j = 0; j < pickedarray.length; j++) {
						if(removeitem.sourceresource == pickedarray[j].sourceresource && removeitem.sourceid == pickedarray[j].sourceid) {
							pickedarray.splice(j, 1);
							break;
						}
					}
				});
				this.props.updateFormState(this.props.form, {
					[`${itemstr}.receiptpickingdetails.details`]: pickedarray
				});
				this.setState({
					availableReceipts
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	quantityonChange(item, value) {
		item.pickedQuantity = value;
		let {resource, itemstr} = this.props;
		let member = `${itemstr}.receiptpickingdetails.details`;
		let pickedarray = this.selector(this.props.fullstate, member);

		if(Number(item.pickedQuantity) > 0) {
			let itemFound = false;
			for(var i = 0; i < pickedarray.length; i++) {
				if(item.sourceresource == pickedarray[i].sourceresource && item.sourceid == pickedarray[i].sourceid) {
					itemFound = true;
					pickedarray[i].quantity = Number(item.pickedQuantity);
					break;
				}
			}
			if(!itemFound) {
				pickedarray.push({
					sourceresource : item.sourceresource,
					sourceid : item.sourceid,
					receiptnotenumber : item.receiptnotenumber,
					receiptnotedate : item.receiptnotedate,
					reference : item.reference,
					referencedate : item.referencedate,
					quantity : Number(item.pickedQuantity)
				});
			}
		} else {
			for(var i = 0; i < pickedarray.length; i++) {
				if(item.sourceresource == pickedarray[i].sourceresource && item.sourceid == pickedarray[i].sourceid) {
					pickedarray.splice(i, 1);
					break;
				}
			}
		}
		this.props.updateFormState(this.props.form, {
			[`${member}`]: pickedarray
		});
		this.setState({
			availableStock: this.state.availableStock
		});
	}

	getCount() {
		let count = 0;
		let member = `${this.props.itemstr}.receiptpickingdetails.details`;
		let pickedarray  = this.selector(this.props.fullstate, member);

		if(!pickedarray)
			return count;

		for(var i = 0; i < pickedarray.length;i++)
			count += pickedarray[i].quantity;

		return Number(count.toFixed(this.props.app.roundOffPrecisionStock));
	}

	render() {
		let availableReceipts = this.state.availableStock;
		let item = this.selector(this.props.fullstate, this.props.itemstr);
		let statusparam = ['Approved', 'Dispatched', 'Cancelled'].indexOf(this.props.resource.status) >= 0;

		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Manual Receipt Note Selection</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Item Name</label>
							<input type="text" className="form-control" value={item.itemid_name} disabled></input>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Quantity</label>
							<input type="number" className="form-control" value={item.quantity} disabled></input>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							<label className="labelclass">Warehouse</label>
							<SelectAsync resource={"stocklocations"} fields={"id,name"} filter={`stocklocations.companyid=${this.props.resource.companyid} and stocklocations.isparent and stocklocations.parentid is null`} value={item.warehouseid} className={`${!item.warehouseid ? 'errorinput' : ''}`} required={true} disabled={true} />
						</div>
					</div>
					{!statusparam ? <div className="row">
						<div className="col-md-12">
							<p className="float-right"><span className="badge badge-secondary">{this.getCount()}</span>  Items selected</p>
							<table className="table table-bordered table-condensed">
								<thead>
								   <tr>
									<th className="text-center">Receipt Note No</th>
									<th className="text-center">Receipt Note Date</th>
									<th className="text-center">PO No</th>
									<th className="text-center">PO Date</th>
									<th className="text-center">SO/Project Details</th>
									<th className="text-center">Available Qty</th>
									<th className="text-center"></th>
								   </tr>
								</thead>
								<tbody>
									{this.state.availableReceipts.map((rowitem, index) => {
										return (
											<tr key={index}>
												<td className="text-center">{rowitem.receiptnotenumber}</td>
												<td className="text-center">{dateFilter(rowitem.receiptnotedate)}</td>
												<td className="text-center">{rowitem.pono}</td>
												<td className="text-center">{dateFilter(rowitem.podate)}</td>
												<td className="text-center">{rowitem.referenceno}<br></br> {rowitem.referencepartnerid_name}</td>
												<td className="text-center">{rowitem.quantity}</td>
												<td className="text-center">
													<input type="number" className="form-control" onChange={(e) => this.quantityonChange(rowitem, e.target.value)} value={rowitem.pickedQuantity || ''} placeholder="Enter Quantity" />
												</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div> : <div className="row">
						<div className="col-md-12">
							<table className="table table-bordered table-condensed">
								<thead>
									<tr>
										<th className="text-center">Receipt Note No</th>
										<th className="text-center">Receipt Note Date</th>
										<th className="text-center">PO No</th>
										<th className="text-center">PO Date</th>
										<th className="text-center">SO/Project Details</th>
										<th className="text-center">Quantity</th>
									</tr>
								</thead>
								<tbody>
									{item.receiptpickingdetails.details.map((rowitem, index) => {
										return (
											<tr key={index}>
												<td className="text-center">{rowitem.receiptnotenumber}</td>
												<td className="text-center">{dateFilter(rowitem.receiptnotedate)}</td>
												<td className="text-center">{rowitem.pono}</td>
												<td className="text-center">{dateFilter(rowitem.podate)}</td>
												<td className="text-center">{rowitem.referenceno}<br></br> {rowitem.referencepartnerid_name}</td>
												<td className="text-center">{rowitem.quantity}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
};
