import React, { Component } from 'react';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { CostCenterAutoMultiSelect, NumberElement, SelectAsync  } from '../utilcomponents';
import { currencyFilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);

		let costCenterArr = this.props.item && this.props.item.costcenter ? JSON.parse(JSON.stringify(this.props.item.costcenter)) : [];

		let costCategoryArray = [],costcategoryObj = {};

		costCenterArr.forEach(item => {
			if(!costcategoryObj[item.categoryid])
				costcategoryObj[item.categoryid] = {
					id : item.categoryid,
					name : item.categoryid_name,
					items : []
				};
		});

		Object.keys(costcategoryObj).forEach((category)=>{
			costCategoryArray.push(costcategoryObj[category]);
		});

		this.state = {
			costCenterArr,
			costCategoryArray,
			disableitem: ['Approved', 'Sent To Customer','Cancelled'].includes(this.props.resource.status),
			costcentercategoryid: null,
			costcentercategories : [],
			costcenter : []
		};

		this.onload = this.onload.bind(this);
		this.addCostCenter = this.addCostCenter.bind(this);
		this.updateCostCenter = this.updateCostCenter.bind(this);
		this.deleteRow = this.deleteRow.bind(this);
		this.updateItem = this.updateItem.bind(this);
		this.checkRequired = this.checkRequired.bind(this);
		this.renderCategoryItems = this.renderCategoryItems.bind(this);
		this.getTotalAmount = this.getTotalAmount.bind(this);
		this.close = this.close.bind(this);
	}

	componentWillMount() {
		this.onload();
	}

	onload() {
		let costcentercategories = this.state.disableitem ? JSON.parse(JSON.stringify(this.state.costCategoryArray)) : JSON.parse(JSON.stringify(this.props.app.costcentercategories));

		costcentercategories = costcentercategories.sort((a, b) => a.id - b.id);

		costcentercategories.forEach((category) => {
			category.items = [];

			this.state.costCenterArr.forEach((item) => {
				if(item.categoryid == category.id)
					category.items.push(item);
			});
		});
		
		this.setState({
			costcentercategories
		});
	}

	addCostCenter(category){
		if(category && category.items) {
			category.items.push({
				costcenterid : null,
				costcenterid_name : null,
				amount : null,
				categoryid : category.id,
				categoryid_name : category.name
			});
		}

		this.setState({ costcentercategories: this.state.costcentercategories });
	}

	updateCostCenter(costcategoryindex,costcenterindex,field,value) {
		var costcentercategories = this.state.costcentercategories;

		if(field == 'costcenterid')
			costcentercategories[costcategoryindex].items[costcenterindex] = value;
		else
			costcentercategories[costcategoryindex].items[costcenterindex][field] = value;

		this.setState({ costcentercategories});
	}

	deleteRow(costcategoryindex,costcenterindex) {
		let costcentercategories = [...this.state.costcentercategories];
		costcentercategories[costcategoryindex].items.splice(costcenterindex,1);
		this.setState({ costcentercategories })
	}

	checkRequired() {
		let formRequired = false;

		this.state.costcentercategories.forEach((category)=>{
			category.items.forEach((item)=>{
				if(!item.costcenterid || !item.amount){
					formRequired = true;
				}
			});
		});
		return formRequired;
	}

	updateItem() {
		if(this.state.disableitem)
			return this.props.closeModal();

		let costCategoryObj = {}, errorArr = [], costcenter = [];

		let costcentercategories = [...this.state.costcentercategories];

		costcentercategories.forEach((category) => {
			category.amount = this.getTotalAmount(category);
			let costCenterArr = [];

			if((category.items || []).length == 0)
				return null;

			category.items.forEach((item)=>{
				if(costCenterArr.indexOf(item.costcenterid) >= 0){
					errorArr.push(`Duplicate Cost Center in Cost Category "${category.name}"`);
				}
				costCenterArr.push(item.costcenterid);
				costcenter.push(item);
			});

			if(category.amount != this.props.item[`${this.props.amountstr}`])
				errorArr.push(`The allocated amount for category "${category.name}" does not match the total amount.`);
		});

		if(errorArr.length > 0)
			return this.props.openModal(modalService['infoMethod']({
				header : 'Warning',
				bodyArray : errorArr,
				btnArray : ['Ok']
			}));

		this.props.updateFormState(this.props.form, {
			[`${this.props.itemstr}.costcenter`] : costcenter
		});

		setTimeout(this.props.closeModal(), 0);
	}

	close() {
		this.props.closeModal();
	}

	getTotalAmount(category) {
		let amount = 0;
		(category.items || []).forEach(item => {
			amount += (item.amount > 0 ? item.amount : 0);
		});

		return Number(amount.toFixed(this.props.app.roundOffPrecision));
	}

	renderCategoryItems(category, index) {
		return category.items.map((item, itemkey) => {
			return (
				<div className="row" key={itemkey}>
					<div className="col-md-5 col-sm-5 form-group">
						{(!item.costcenterid) ?<CostCenterAutoMultiSelect value={item} onChange={(value) => {this.updateCostCenter(index,itemkey,'costcenterid',value)}}  restrictToSingleSelect={true} categoryFilter={category.id} /> : item.costcenterid_name}
					</div>
					<div className="offset-md-4 col-md-2 col-sm-2 form-group">
						<NumberElement className={`form-control ${!item.amount ? 'errorinput' : ''}`} value={item.amount} onChange={(value) => this.updateCostCenter(index,itemkey,'amount',value)} required={true} disabled={this.state.disableitem} />
					</div>
					<div className={`col-md-1 col-sm-1 ${(this.state.disableitem) ? '' : 'gs-tree-hover-td'}`}>
						{(!this.state.disableitem) ? <button type="button" class="close" style={{'fontSize': '30px'}} onClick={()=>{this.deleteRow(index,itemkey)}}>&times;</button> : null}
					</div>
				</div>
			)
		});
	}

	render() {

		let costcentercategories = this.state.costcentercategories;
		
		return (
			<div>
				<div className="react-modal-header">
					<div className="d-flex justify-content-between">
						<div className="flex-column">
							<h6 className="modal-title semi-bold" style ={{color :'#1cbc9c'}}>Cost Center Details</h6>
						</div>
						<div className="flex-column semi-bold" style ={{paddingRight :'24px'}}>
							Total Cost: {currencyFilter(this.props.item[`${this.props.amountstr}`], this.props.resource.currencyid, this.props.app)}
						</div>
					</div>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12 col-sm-12"> 
							<div className="card" style={{'borderRadius' : '0px'}}>
								<div className="card-body">
									<div className="row" style={{"fontSize": "14px"}}>
										<div className="col-md-9 col-sm-9 semi-bold">Cost Center Category</div>
										<div className="col-md-2 col-sm-2 semi-bold paddingleft-26">Amount</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					{costcentercategories.map((category,index)=>{
						return (
							<div className="row" key={index}>
								<div className="col-md-12 col-sm-12"> 
									<div className="card" style={{'borderRadius' : '0px'}}>
										<div className="card-header">
											<div className="row">
												<div className="col-md-9 col-sm-9 semi-bold">{category.name}</div>
												<div className="col-md-2 col-sm-2 text-gray paddingleft-26">{this.getTotalAmount(category)}</div>
												<div className="col-md-1 col-sm-1"></div>
											</div>
										</div>
										<div className="card-body">
											{this.renderCategoryItems(category, index)}
											{(!this.state.disableitem) ? <div className="row" style={{"marginLeft": "3px"}}>
												<button type="button" className="btn btn-sm gs-form-btn-primary" onClick={()=>{this.addCostCenter(category)}}><i className="fa fa-plus"></i>Cost Center</button>
											</div> : null}
										</div>
									</div>
								</div>
							</div>
						);
					})}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close}>Cancel</button>
								{
									(!this.state.disableitem) ? <button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.updateItem} disabled={this.checkRequired()}>Ok</button> : null 
								}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}