import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';
import { LocalSelect, AutoSelect } from '../utilcomponents';
import { checkActionVerbAccess } from '../../utils/utils';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showAlert: false,
			addressArray: [],
			partnerAddressArray: []
		};
		this.onLoad = this.onLoad.bind(this);
		this.onSelectAddress = this.onSelectAddress.bind(this);
		this.editFn = this.editFn.bind(this);
		this.createAddress = this.createAddress.bind(this);
		this.getProjectOrderAddress = this.getProjectOrderAddress.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	componentWillReceiveProps(newprops) {
		this.onLoad();
	}

	onLoad() {
		if(this.props.parentresource && this.props.parentid) {
			var queryString =`/api/addresses?field=id,title,displayaddress,firstline,streetid,areaid,cityid,stateid,countryid,streets/name/streetid,areas/name/areaid,areas/pincode/areaid,cities/name/cityid,states/name/stateid,countries/name/countryid,state,gstin&filtercondition=addresses.parentresource='${this.props.parentresource}' and addresses.parentid=${this.props.parentid}`;

			axios.get(queryString).then((response) => {
				if(response.data.message == 'success') {
					if(this.props.app.feature.useMasterForAddresses) {
						for (var i=0;i<response.data.main.length;i++) {
							var addressData = '';
							addressData = (response.data.main[i].firstline) ? addressData + (response.data.main[i].firstline) : addressData + '';
							addressData = (response.data.main[i].streetid_name) ? addressData + ',' + (response.data.main[i].streetid_name) : addressData + '';
							addressData = (response.data.main[i].areaid_name) ? addressData + ',\n' + (response.data.main[i].areaid_name) : addressData + '';
							addressData = (response.data.main[i].cityid_name) ? addressData + ',' + (response.data.main[i].cityid_name) : addressData + '';
							addressData = (response.data.main[i].stateid_name) ? addressData + ',\n' + (response.data.main[i].stateid_name) : addressData + '';
							addressData = (response.data.main[i].areaid_pincode) ? addressData + ',' + (response.data.main[i].areaid_pincode) : addressData + '';
							response.data.main[i].displayaddress = (addressData!='')?addressData:response.data.main[i].displayaddress;
						}
					}

					response.data.main.forEach((item) => {
						item.isselected = (this.props.resource && this.props.resource[this.props.model[0]] == item.id) ? true : false;
					});

					this.setState({addressArray: response.data.main}, () => {
						if(this.props.orderid) {
							this.getProjectOrderAddress('orders', this.props.orderid);
						} else if(this.props.projectid) {
							this.getProjectOrderAddress('projects', this.props.projectid);
						}
					});
				}
			});
		} else {
			this.setState({showAlert: true});
		}
	}

	getProjectOrderAddress(resource, param) {
		let partnerAddressArray = [];
		axios.get(`/api/${resource}?field=id,deliveryaddress,deliveryaddressid&filtercondition=${resource}.id=${param}`).then((response) => {
			if(response.data.message == 'success') {
				response.data.main.forEach((item) => {
					if(item.deliveryaddress) {
						partnerAddressArray.push({
							id: item.deliveryaddressid,
							displayaddress: item.deliveryaddress,
							isselected: (this.props.resource && this.props.resource[this.props.model[0]] == item.deliveryaddressid) ? true : false
						});
					}
				});
				this.setState({ partnerAddressArray });
			}
		});
	}

	createAddress() {
		let connectingObj = {
			parentresource: this.props.parentresource,
			parentid: this.props.parentid
		};
		this.props.createOrEdit('/createAddress', null, connectingObj, (valueObj) => {
			this.props.onChange(valueObj);
			this.props.closeModal();
		});
	}


	onSelectAddress(selecteditem) {
		this.props.onChange(selecteditem);
		this.props.closeModal();
	}

	editFn(item) {
		this.props.createOrEdit('/details/address/:id', item.id, {}, (valueObj) => {
			if(item.isselected) {
				this.props.onChange(valueObj);
				this.props.closeModal();
			}
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Select Address</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll">
					{this.state.showAlert ? <div className="row">
						<div className="col-md-12"><div className="alert alert-warning">Please choose partner or company</div></div>
					</div> : null }
					{!this.state.showAlert ? <div className="row">
						{checkActionVerbAccess(this.props.app, 'addresses', 'Save') ? <div className="form-group col-md-3" >
							<div className="card">
								<div className="card-body text-center custom-text-color" style={{padding: '0.75rem', cursor: 'pointer', height: '140px'}} onClick={() => this.createAddress()}>
									<div style={{margin: '0 auto', position: 'absolute', top: '25%', right: 0, bottom: 0, left: 0}}>
										<span className="fa fa-plus fa-2x"></span>
										<br></br>
										<span>Add new address</span>
									</div>
								</div>
							</div>
						</div> : null}
						{this.state.addressArray.map((item, index) => {
							return (<div className="form-group col-md-3" key={index}>
									<div className={`card ${item.isselected ? 'card-active' : ''}`}>
										<div onClick={()=>this.onSelectAddress(item)} className="card-body address-card-body">
											<div className="marginbottom-5">
												<span className="custom-muted-text">{item.title}</span>
												<a className="float-right custom-muted-text marginright-5" onClick={(evt)=>{evt.stopPropagation();this.editFn(item);}} ><span className="fa fa-edit"></span></a>
											</div>
											<span style={{whiteSpace: 'pre-line'}}>{item.displayaddress}</span>
										</div>
										<div style={{height: '25px'}}>
											<div className={`${item.isselected ? 'card-selected' : 'hide'}`}>
												<span className="fa fa-check icon-bar"></span>
											</div>
										</div>
									</div>
								</div>
							);
						})}
						{this.state.partnerAddressArray.length > 0 ? <div className="col-md-12">
							<div className="row">
								<div className="col-md-12">
									<div className="card">
										<div className="card-header">{`${this.props.projectid ? 'Project' : 'Order'} Address`}</div>
										<div className="card-body">
											<div className="row">
												{this.state.partnerAddressArray.map((item, index) => {
													return (<div className="col-md-3" key={index}>
															<div className={`card ${item.isselected ? 'card-active' : ''}`}>
																<div onClick={()=>this.onSelectAddress(item)} className="card-body address-card-body">
																	<div className="marginbottom-5">
																		<span className="custom-muted-text">{item.title}</span>
																		<a className="float-right custom-muted-text marginright-5" onClick={(evt)=>{evt.stopPropagation();this.editFn(item);}} ><span className="fa fa-edit"></span></a>
																	</div>
																	<span style={{whiteSpace: 'pre-line'}}>{item.displayaddress}</span>
																</div>
																<div style={{height: '25px'}}>
																	<div className={`${item.isselected ? 'card-selected' : 'hide'}`}>
																		<span className="fa fa-check icon-bar"></span>
																	</div>
																</div>
															</div>
														</div>
													);
												})}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> : null}
					</div> : null }
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
