import React, { Component } from 'react';
import axios from 'axios';
import { commonMethods, modalService } from '../../utils/services';
import StockdetailsModal from '../../containers/stockdetails';
import { NumberElement } from '../utilcomponents';
import { dateFilter } from '../../utils/filter';

export default class extends Component {
	constructor(props) {
		super(props);

		this.state = {
			estimationItemsArray: [],
			selectedItem : this.getSelectedItem(this.props),
			showdescription: false
		};
		this.close = this.close.bind(this);
		this.descToggleOnChange = this.descToggleOnChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			selectedItem : this.getSelectedItem(nextProps)
		});
	}

	componentWillMount() {
		if(['Approved', 'Sent To Customer'].indexOf(this.props.resource.status) >= 0) {
			this.setState({
				disableitem : true
			});
		}
		setTimeout(() => {
			this.onLoad();
		}, 0);
	}

	onLoad() {
		axios.get(`/api/query/getworkorderprojectestimateitemsquery?projectid=${this.props.resource.projectid}&param=purchase`).then((response) => {
			if (response.data.message == 'success') {
				let estimationItemsArray = response.data.main;
				estimationItemsArray.sort(function(a, b) {
				    return a.parentid - b.parentid || a.displayorder - b.displayorder;
				});
				for(let i=0;i<estimationItemsArray.length;i++){
					if(estimationItemsArray[i].projectestimationitemsid == this.state.selectedItem.estimationitemid){
						estimationItemsArray[i].check = true;
						break;
					}
				}
				this.setState({
					estimationItemsArray
				});
			} else {
				var apiResponse = commonMethods.apiResult(response);
				modalService[apiResponse.methodName](apiResponse.message);
			}
		});
	}

	updateAddOnField(item, field, value) {
		item[field] = value;
		this.props.updateFormState(this.props.form, {
			[`${this.props.itemstr}.estimationitemid`]: value ? item.projectestimationitemsid : null,
			[`${this.props.itemstr}.estimationitemid_itemid`]: value ? item.itemid : null
		});
		this.setState({
			estimationItemsArray: this.state.estimationItemsArray
		});
	}

	getSelectedItem(props) {
		let selectedItem = {};
		for(var i = 0; i < props.resource[props.child].length; i++) {
			if(props.resource[props.child][i].index == props.index)
				selectedItem = props.resource[props.child][i];
		}
		return selectedItem;
	}

	descToggleOnChange() {
		this.setState({showdescription: !this.state.showdescription});
	}

	renderAddon() {
		return this.state.estimationItemsArray.map((estimationitem, key) => {
			return <tr key={key} className={`${estimationitem.check ? 'itemcheck' : ''}`}>
				<td className="text-center">
					<input  type="checkbox" onChange={(e) => this.updateAddOnField(estimationitem, 'check', e.target.checked)}  checked={estimationitem.check} disabled={this.state.disableitem} />
				</td>
				<td><div>
					<span className="text-muted">Internal Ref No </span>
					<span>: {estimationitem.internalrefno}</span>
				</div>
				{estimationitem.internalrefno != 'Overhead' ? <div>
					<span className="text-muted">Client Ref No </span>
					<span>: {estimationitem.clientrefno}</span>
				</div> : null}
				{estimationitem.internalrefno != 'Overhead' ? <div>
					<span className="text-muted">BOQ Item Name </span>
					<span>: {estimationitem.boqitemid_name}</span>
				</div> : null}
				{estimationitem.boqitemsid_description ? <div className={`${this.state.showdescription ? 'show' : 'hide'}`}>
					<span className="text-muted">BOQ Description </span>
					<span className="font-12">: {estimationitem.boqitemsid_description}</span>
				</div> : null}</td>
				<td>{estimationitem.itemid_name}<div className={`font-12 ${this.state.showdescription ? 'show' : 'hide'}`}>{estimationitem.description}</div></td>
				<td className="text-center">{estimationitem.estimatedqty}</td>
				<td className="text-center">{estimationitem.uomid_name}</td>
				<td className="text-center">{estimationitem.remarks}</td>
			</tr>
		});
	}

	close() {

		let selectItemCount = 0;
		this.state.estimationItemsArray.forEach((item)=>{
			if(item.check)
				selectItemCount++;
		});

		if(selectItemCount > 1){
			let message = {
				header : 'Error',
				body : ["Only one Item can be selected"],
				btnArray : ['Ok']
			};
			return this.props.openModal(modalService.infoMethod(message));
		}

		this.props.closeModal();
	}

	render() {
		let {estimationItemsArray} = this.state;

		return (
			<div>
				<div className="react-modal-header">
					<h5 className="modal-title">Project Estimation Item Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="form-group col-md-4 col-sm-4">
							<label className="float-left">Show Description</label>
							<label className="gs-checkbox-switch">
								<input className="form-check-input" type="checkbox" checked={this.state.showdescription} onChange={() => this.descToggleOnChange()} />
								<span className="gs-checkbox-switch-slider"></span>
							</label>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							{estimationItemsArray.length > 0 ? <table className="table table-bordered tablefixed">
								<thead>
									<tr>
										<th style={{width:'5%'}}></th>
										<th className="text-center">BOQ Details</th>
										<th className="text-center">Item Name</th>
										<th className="text-center">Estimated Qty</th>
										<th className="text-center">UOM</th>
										<th className="text-center">Remarks</th>
									</tr>
								</thead>
								<tbody>
									{this.renderAddon()}
								</tbody>
							</table> : <div className="col-md-12 col-sm-12 col-xs-12 alert alert-warning text-center">
									No Estimation Items Found For This Project!
								</div>}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
