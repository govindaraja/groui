import React, { Component } from 'react';
import axios from 'axios';
import moment from 'moment';
import { Field } from 'redux-form';
import { DateEle, InputEle } from '../formelements';
import { dateValidation } from '../../utils/utils';

export default class extends Component {
	constructor(props) {
		super(props);
		this.addContractSchedule = this.addContractSchedule.bind(this);
		this.deleteContractSchedule = this.deleteContractSchedule.bind(this);
	};

	addContractSchedule (keyvalue) {
		this.props.array.push(`schedules.array[${keyvalue}]`, {});
	}

	deleteContractSchedule (keyvalue, index) {
		this.props.array.splice(`schedules.array[${keyvalue}]`, index, 1);
	}

	render() {
		if(Object.keys(this.props.resource.schedules.array).length == 0)
			return null;

		return (
			<div className="row col-md-12 col-sm-12 col-xs-12">
				{ Object.keys(this.props.resource.schedules.array).map((keyvalue) => {
					return (
					<div className="col-md-6" key={keyvalue}>
						<table className="table table-bordered">
							<thead>
								<tr>
									<th className="text-center" colSpan="3"> {this.props.contractState.contractObj ? this.props.contractState.contractObj[keyvalue].name : ''} - Schedule Date</th>
								</tr>
							</thead>
							<tbody>
								{ this.props.resource.schedules.array[keyvalue].map((item, schindex) => {
									return (
										<tr key={schindex}>
											<td>
												<Field name={`schedules.array[${keyvalue}][${schindex}].scheduledate`}  props = {{ required : true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateValidation]}/>
											</td>
											<td>
												<Field name={`schedules.array[${keyvalue}][${schindex}].remarks`} component={InputEle} />
											</td>
											<td>
												<button type="button" className="btn gs-form-btn-danger btn-sm btndisable" onClick={()=>this.deleteContractSchedule(keyvalue, schindex)}>
													<span className="fa fa-trash-o"></span>
												</button>
											</td>
										</tr>
									);
								})}
							</tbody>
						</table>
						<button type="button" className="btn gs-btn-info pull-left btn-sm btndisable marginbottom-5" onClick={()=>this.addContractSchedule(keyvalue)}>
							<span className="fa fa-plus"></span>Add
						</button>
					</div>)
				})}
			</div>
		);
	}
}
