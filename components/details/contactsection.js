import React, { Component } from 'react';

export default class extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="col-md-12" style={{maxHeight:'290px',overflowY:'scroll'}}>
				<div className="list-group">
					{this.props.contacts.map((contact, index) => {
						return (
							<li key={index} className="list-group-item" onClick={()=>{this.props.getContactDetails(contact.id)}}>
								{contact.name ? <div>
									<label><b>Name&nbsp;:&nbsp;</b></label>
									<span>{contact.name}</span>&nbsp;&nbsp;&nbsp;
									<span className={`float-right badge ${contact.isactive ? 'badge-success' : 'badge-warning'}`}>{contact.isactive ? "Active" : "Inactive"}</span>
								</div> : null }
								{contact.designation ? <div> 
									<label><b>Designation&nbsp;:&nbsp;</b></label>
									<span>{contact.designation}</span>
								</div> : null }
								{contact.mobile ? <div>
									<label><b>Mobile&nbsp;:&nbsp;</b></label>
									<span>{contact.mobile}</span>&nbsp;&nbsp;&nbsp;
								</div> : null }
								{contact.phone ? <div>
									<label><b>Phone&nbsp;:&nbsp;</b></label>
									<span>{contact.phone}</span>
								</div> : null }
								{contact.email ? <div>
									<label><b>E-mail&nbsp;:&nbsp;</b></label>
									<span>{contact.email}</span>
								</div> : null }
							</li>
						);
					})}
				</div>
				<div className="col-md-12 col-sm-12 col-xs-12">
					<button type='button' onClick={this.props.addNewContact} className="btn btn-sm gs-btn-info float-left"><span className="fa fa-plus"></span> Contact</button>
				</div>
			</div>
		);
	}
}
