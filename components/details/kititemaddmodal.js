import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';

import { commonMethods, modalService } from '../../utils/services';
import { dateFilter } from '../../utils/filter';
import { LocalSelect, AutoSelect, NumberElement } from '../utilcomponents';

export default connect((state) => {
	return {
		app : state.app
	}
})(class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			itemObj: {
				conversion: 1
			},
			parentArray: [],
		};
		this.onLoad = this.onLoad.bind(this);
		this.addItem = this.addItem.bind(this);
		this.callbackProduct = this.callbackProduct.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let parentArray = [];

		for (var i = 0; i < this.props.resource[this.props.child].length; i++) {
			if (this.props.resource[this.props.child][i].itemid_issaleskit)
				parentArray.push({
					itemid : this.props.resource[this.props.child][i].itemid,
					rootindex: this.props.resource[this.props.child][i].index,
					name : this.props.resource[this.props.child][i].itemid_name,
					displayname: this.props.resource[this.props.child][i].index,
					parent: this.props.resource[this.props.child][i].itemid_name
				});
		}

		for (var i = 0; i < this.props.resource[this.props.kit].length; i++) {
			if (this.props.resource[this.props.kit][i].itemid_issaleskit)
				parentArray.push({
					itemid : this.props.resource[this.props.kit][i].itemid,
					rootindex : this.props.resource[this.props.kit][i].rootindex,
					parentindex : this.props.resource[this.props.kit][i].index,
					name : this.props.resource[this.props.kit][i].itemid_name,
					displayname: `${this.props.resource[this.props.kit][i].rootindex}_${this.props.resource[this.props.kit][i].index}`,
					parent: `${this.props.resource[this.props.kit][i].parent}/${this.props.resource[this.props.kit][i].itemid_name}`
				});
		}
		this.setState({parentArray});
	}

	callbackProduct(id, valueobj) {
		let { itemObj } = this.state;
		itemObj.itemid = id;
		itemObj.itemid_name = valueobj.name;
		itemObj.itemid_issaleskit = valueobj.issaleskit;
		itemObj.itemid_hasbatch = valueobj.hasbatch;
		itemObj.itemid_hasserial = valueobj.hasserial;
		itemObj.itemid_keepstock = valueobj.keepstock;
		itemObj.uomid = valueobj.stockuomid;
		itemObj.uomid_name = valueobj.stockuomid_name;
		itemObj.itemid_uomgroupid = valueobj.uomgroupid;
		itemObj.conversion = itemObj.conversion ? itemObj.conversion : 1;
		this.setState({itemObj});
	};

	inputonChange(value, param) {
		let  { itemObj } = this.state;
		itemObj[param] = value;
		this.setState({itemObj});
	}

	parentitemChange(value, valueobj) {
		let  { itemObj } = this.state;
		itemObj.parentitemid = value;
		itemObj.parentitemidobj = value ? valueobj : null;
		this.setState({itemObj});
	}

	addItem () {
		let { itemObj } = this.state;
		var index = 0;
		for (var i = 0; i < this.props.resource[this.props.kit].length; i++) {
			if (this.props.resource[this.props.kit][i].index > index)
				index = this.props.resource[this.props.kit][i].index;
		}
		var tempObj = {
			itemid : itemObj.itemid,
			itemid_name : itemObj.itemid_name,
			itemid_issaleskit : itemObj.itemid_issaleskit,
			itemid_hasbatch : itemObj.itemid_hasbatch,
			itemid_keepstock : itemObj.itemid_keepstock,
			itemid_hasserial : itemObj.itemid_hasserial,
			itemid : itemObj.itemid,
			index : index + 1,
			rootindex: itemObj.parentitemidobj.rootindex,
			parentindex: itemObj.parentitemidobj.parentindex >= 0 ? itemObj.parentitemidobj.parentindex : null,
			parent: itemObj.parentitemidobj.parent,
			conversion : itemObj.conversion,
			uomid : itemObj.uomid,
			uomid_name : itemObj.uomid_name,
		};
		/*for (var i = 0; i < this.props.resource[this.props.child].length; i++) {
			if (this.props.resource[this.props.child][i].itemid == itemObj.parentitemid) {
				tempObj.rootindex = this.props.resource[this.props.child][i].index;
				tempObj.parent = this.props.resource[this.props.child][i].itemid_name;
				break;
			}
		}
		for (var i = 0; i < this.props.resource[this.props.kit].length; i++) {
			if (this.props.resource[this.props.kit][i].itemid == itemObj.parentitemid) {
				tempObj.rootindex = this.props.resource[this.props.kit][i].rootindex;
				tempObj.parent = this.props.resource[this.props.kit][i].parent + "/" + this.props.resource[this.props.kit][i].itemid_name;
				tempObj.parentindex = this.props.resource[this.props.kit][i].index;
				break;
			}
		}*/
		this.props.array.push(this.props.kit, tempObj);
		setTimeout(() => {
			this.props.callback();
			this.props.closeModal();
		}, 0);
	}

	render() {
		let errorObj = {};
		let { itemObj } = this.state;
		let buttondisabled = false;
		if(!(itemObj.itemid > 0))
			errorObj.itemid = 'errorinput', buttondisabled = true;
		if(!(itemObj.parentitemid))
			errorObj.parentitemid = 'errorinput', buttondisabled = true;
		if(!(itemObj.uomid > 0))
			errorObj.uomid = 'errorinput', buttondisabled = true;
		if(!(itemObj.conversion > 0))
			errorObj.conversion = 'errorinput', buttondisabled = true;


		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Add Kit Item</h5>
				</div>
				<div className="react-modal-body react-modal-body-scroll-wt-height">
					<div className="form-group row">
						<label className="col-sm-3 col-form-label">Item Name</label>
						<div className="col-sm-9">
							<AutoSelect className={errorObj.itemid} resource={'itemmaster'} fields={`id,name,issaleskit,keepstock,hasbatch,hasserial,stockuomid,uomgroupid,uom/name/stockuomid`} value={itemObj.itemid} label={'name'} valuename={'id'} filter={'itemmaster.allowsales and itemmaster.issaleskit=false'} onChange={this.callbackProduct} required />
							</div>
					</div>
					<div className="form-group row">
						<label className="col-sm-3 col-form-label">Parent</label>
						<div className="col-sm-9">
							<LocalSelect className={errorObj.parentitemid} options={this.state.parentArray} label={'parent'} valuename={'displayname'} value={itemObj.parentitemid} onChange={(value, valueobj) => this.parentitemChange(value, valueobj)}/>
						</div>
					</div>
					<div className="form-group row">
						<label className="col-sm-3 col-form-label">UOM</label>
						<div className="col-sm-9">
							<input type="text" className={`form-control ${errorObj.uomid}`} value={itemObj.uomid_name} disabled />
						</div>
					</div>
					<div className="form-group row">
						<label className="col-sm-3 col-form-label">Conversion</label>
						<div className="col-sm-9">
							<NumberElement className={`form-control ${errorObj.conversion}`} value={itemObj.conversion} onChange={(value) => this.inputonChange(value, 'conversion')} />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.addItem} disabled={buttondisabled} ><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
