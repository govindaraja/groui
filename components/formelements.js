import React, { Component } from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';
import moment from 'moment';
import axios from 'axios';
import { Field } from 'redux-form';
import { v1 as uuidv1 }  from 'uuid';

//import 'react-select/dist/react-select.css';

import { LocalSelect, SelectAsync, AutoSelect, DisplayGroup, AutoSuggest, DateElement, NumberElement, DateTimeElement, TimeElement, AutoMultiSelect, CostCenterAutoMultiSelect } from './utilcomponents';
import { stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, emailNewValidation, checkActionVerbAccess } from '../utils/utils';
import { currencyFilter } from '../utils/filter';
import AddressModal from '../components/details/addressmodal';
import DocumentuploadModal from '../containers/documentuploadmodal';
import { commonMethods,modalService } from '../utils/services';
import RichTextModal from '../components/details/richtextmodal';
import LocationmastermapModal from '../components/details/locationmastermapmodal';

export const InputEle= function(field) {
	var { onChange, onFocus, onBlur } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value) => {let inputvalue = value.target.value;field.input.onChange(value);setTimeout(()=>{onChange(inputvalue);},0);} : field.input.onChange;

	var onBlurFun = (typeof(onBlur) == 'function') ? (value) => {let inputvalue = value.target.value;field.input.onBlur(value);setTimeout(()=>{onBlur(inputvalue);},0);} : field.input.onBlur;

	var onFocusFun = (typeof(onFocus) == 'function') ? (value) => {let inputvalue = value.target.value;field.input.onFocus(value);setTimeout(()=>{onFocus(inputvalue);},0);} : field.input.onFocus;

	return (
		<input type="text" className={`form-control ${field.classname} ${field.meta.error ? 'errorinput' : ''}`} {...field.input} placeholder={field.placeholder} onChange={onChangeFun} onBlur={onBlurFun} onFocus={onFocusFun} required={field.required} disabled={field.disabled} autoComplete="off" ></input>
	);
}

export const NumberEle= function(field) {
	var { onChange, onFocus, onBlur } = field;

	var onChangeFun = (value) => {
		field.input.onChange(value);
		if(typeof(onChange) == 'function') {
			setTimeout(()=>{onChange(value);},0);
		}
	};

	/*return (
		<input type="number" className={`form-control  ${field.classname} ${field.meta.error ? 'errorinput' : ''}`} {...field.input} placeholder={field.placeholder} onChange={onChangeFun} required={field.required} disabled={field.disabled} autoComplete="off"></input>
	);*/
	return(
		<NumberElement className={`form-control  ${field.classname} ${field.meta.error ? 'errorinput' : ''}`} placeholder={field.placeholder} value={field.input.value} onChange={onChangeFun} required={field.required} disabled={field.disabled} />
	);
}

export const SpanEle= function(field) {
	return (
		<input type="text" className="form-control" value={field.input.name} disabled={true} />
	);
}

export const PhoneElement = connect((state)=>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.onCallTo = this.onCallTo.bind(this);
	}

	onCallTo(number) {
		if(this.props.app.feature.enableDialerIntegration && this.props.app.user.extension){
			if(!this.props.app.feature.dialerOutboundURL){
				return this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Click to call API not configured. Please reach out to support for help.',
					btnArray : ['Ok']
				}));
			}
			
			axios.get(`/telephony/clicktocall?extn=${this.props.app.user.extension}&destinationNo=${number}`).then((response) => {
				if(response.data.message == "success") {
					this.props.openModal(modalService['infoMethod']({
						header : 'Info',
						body : response.data.main[0],
						btnArray : ['Ok']
					}));
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}else{
			window.open(`tel:${number}`);
		}
	}

	render() {
		return (
			<div className="input-group mb-3">
				<Field name={this.props.model} props={{disabled:this.props.disabled, required: this.props.required}} component={InputEle} validate={[stringNewValidation({
					required: this.props.required ? true : false,
					model: this.props.title
				})]}/>
				<div className="input-group-append">
					<button className="btn btn-sm gs-btn-call" type="button" onClick={() => this.onCallTo(this.props.resource[this.props.model])}><span className="fa fa-phone-square"></span></button>
				</div>
			</div>
		);
	}
});

export const DateEle= function(field) {
	var { onChange, onFocus, onBlur, min, max } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value) => {field.input.onChange(value);setTimeout(()=>{onChange(value);},0);} : field.input.onChange;

	var onFocusFun = (typeof(onFocus) == 'function') ? (value) => {field.input.onFocus(value);setTimeout(()=>{onFocus(value);},0);} : (value) => field.input.onFocus(value);

	var onBlurFun = (typeof(onBlur) == 'function') ? (value) => {field.input.onBlur(value);setTimeout(()=>{onBlur(value);},0);} : (value) => field.input.onBlur(value);

	let minDate = field.min ? moment(field.min) : undefined;
	let maxDate = field.max ? moment(field.max) : undefined;

	/*return (
		<DatePicker isClearable={field.disabled ? false : true} className={`form-control ${field.classname} ${field.meta.error ? 'errorinput' : ''}`} {...field.input} selected={field.input.value ? moment(field.input.value, "DD-MMM-YYYY") : null} onChange={onChangeFun} onFocus={onFocusFun} required={field.required} disabled={field.disabled} minDate = {minDate} maxDate={maxDate} showMonthDropdown showYearDropdown dropdownMode="select" placeholderText={field.placeholder} autoComplete="off" />
	);*/
	return (
		<DateElement
			className={`form-control ${field.classname} ${field.meta.error ? 'errorinput' : ''}`}
			value={field.input.value}
			onChange={onChangeFun}
			onFocus={onFocusFun}
			onBlur={onBlurFun}
			required={field.required}
			disabled={field.disabled}
			min = {minDate}
			max={maxDate}
			placeholder={field.placeholder} />
	);
}

export const TimepickerEle = function(field) {
	var { onChange, onFocus, onBlur } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value) => {field.input.onChange(value);setTimeout(()=>{onChange(value);},0);} : field.input.onChange;

	return (
		<TimeElement
			className={`${field.classname} ${field.meta.error ? 'errorinput' : ''}`}
			value={field.input.value}
			onChange={onChangeFun}
			required={field.required}
			disabled={field.disabled}
			placeholder={field.placeholder} />
	);
}

export const textareaEle= function(field) {
	var { onFocus } = field;

	var onFocusFun = (typeof(onFocus) == 'function') ? (value) => {let inputvalue = value.target.value;field.input.onFocus(value);setTimeout(()=>{onFocus(inputvalue);},0);} : field.input.onFocus;

	return (
		<textarea className={`form-control ${field.classname} ${field.meta.error ? 'errorinput' : ''}`} {...field.input} onFocus={onFocusFun} required={field.required} readOnly={field.disabled || field.readonly} title={field.input.value} rows="3" autoComplete="off" />
	);
}

export const checkboxEle= function(field) {
	var { onChange } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value) => {let checkedvalue = value.target.checked;field.input.onChange(value);setTimeout(()=>{onChange(checkedvalue);},0);} : field.input.onChange;

	return (
		<label className={`gs-checkbox-switch ${field.classname}`}>
			<input type="checkbox" className={`form-check-input ${field.classname} ${field.meta.error ? 'errorinput' : ''}`} checked={field.input.value} onChange={onChangeFun} required={field.required} disabled={field.disabled} />
			<span className="gs-checkbox-switch-slider"></span>
		</label>
	);
}

export const emailEle= function(field) {
	var { onChange, onFocus, onBlur } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value) => {field.input.onChange(value);setTimeout(()=>{onChange(value);},0);} : field.input.onChange;

	var onBlurFun = (typeof(onBlur) == 'function') ? (value) => {field.input.onBlur(value);setTimeout(()=>{onBlur(value);},0);} : field.input.onBlur;

	return (
		<input type="text" className={`form-control ${field.classname} ${field.meta.error ? 'errorinput' : ''}`} {...field.input} placeholder={field.placeholder} onChange={onChangeFun} onBlur={onBlurFun} required={field.required} disabled={field.disabled} autoComplete="off" ></input>
	);
}


export const passwordEle= function(field) {
	var { onChange, onFocus, onBlur } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value) => {field.input.onChange(value);setTimeout(()=>{onChange(value);},0);} : field.input.onChange;

	var onBlurFun = (typeof(onBlur) == 'function') ? (value) => {field.input.onBlur(value);setTimeout(()=>{onBlur(value);},0);} : field.input.onBlur;

	return (
		<input type="password" className={`form-control ${field.classname} ${field.meta.error ? 'errorinput' : ''}`} {...field.input} placeholder={field.placeholder} onChange={onChangeFun} onBlur={onBlurFun} required={field.required} disabled={field.disabled} autoComplete="off" ></input>
	);
}

export const localSelectEle= function(field) {
	var { options, label, valuename, onChange, onFocus, onBlur, multiselect } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value, valueObj) => {field.input.onChange(value);setTimeout(()=>{onChange(value, valueObj);},0);} : field.input.onChange;
	var onFocusfunction = (typeof(onFocus) == 'function') ? (value) => {field.input.onFocus(value);onFocus(value);} : field.input.onFocus;
	var onBlurfunction = (typeof(onBlur) == 'function') ? (value) => {field.input.onBlur(value);onBlur(value);} : field.input.onBlur;

	return (
		<LocalSelect options={options} label={label} valuename={valuename} {...field.input} onChange={onChangeFun} onFocus={onFocusfunction} onBlur={onBlurfunction} placeholder={field.placeholder} multiselect={field.multiselect} className={`${field.classname} ${field.meta.error ? 'errorinput' : ''}`} required={field.required} disabled={field.disabled} />
	);
}

export const selectAsyncEle= function(field) {
	var { resource, fields, displaylabel, label, valuename, filter, refresh, onChange, onFocus, onBlur, createParamFlag, defaultValueUpdateFn, createOrEdit, createCallback, showadd } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value, valueObj) => {field.input.onChange(value);setTimeout(()=>{onChange(value, valueObj);},0);} : field.input.onChange;
	var onFocusfunction = (typeof(onFocus) == 'function') ? (value) => {field.input.onFocus(value);onFocus(value);} : field.input.onFocus;
	var onBlurfunction = (typeof(onBlur) == 'function') ? (value) => {field.input.onBlur(value);onBlur(value);} : field.input.onBlur;

	return (
		<SelectAsync resource={resource} fields={fields} displaylabel={displaylabel} label={label} valuename={valuename} filter={filter} createParamFlag={createParamFlag} defaultValueUpdateFn={defaultValueUpdateFn} createOrEdit={createOrEdit} showadd={showadd} createCallback={createCallback} {...field.input} onChange={onChangeFun} onFocus={onFocusfunction} onBlur={onBlurfunction} placeholder={field.placeholder} className={`${field.classname} ${field.meta.error ? 'errorinput' : ''}`} required={field.required} disabled={field.disabled} refresh={refresh} />
	);
}

export const autoSelectEle= function(field) {
	var { resource, fields, displaylabel, label, valuename, filter, onChange, onFocus, onBlur, createOrEdit, showadd, showedit, createParams, resourceobj, transactionresourcename } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value, valueObj) => {field.input.onChange(value);setTimeout(()=>{onChange(value, valueObj);},0);} : field.input.onChange;
	var onFocusfunction = (typeof(onFocus) == 'function') ? (value) => {field.input.onFocus(value);onFocus(value);} : field.input.onFocus;
	var onBlurfunction = (typeof(onBlur) == 'function') ? (value) => {field.input.onBlur(value);onBlur(value);} : field.input.onBlur;

	return (
		<AutoSelect resourceobj={resourceobj} transactionresourcename={transactionresourcename} resource={resource} fields={fields} displaylabel={displaylabel} label={label} valuename={valuename} filter={filter} createOrEdit={createOrEdit} showadd={showadd} showedit={showedit} createParams={createParams} {...field.input} onChange={onChangeFun} onFocus={onFocusfunction} onBlur={onBlurfunction} placeholder={field.placeholder} className={`${field.classname} ${field.meta.error ? 'errorinput' : ''}`}  required={field.required} disabled={field.disabled} />
	);
}

export const autoMultiSelectEle= function(field) {
	var { resource, fields, displaylabel, label, valuename, filter, onChange, onFocus, onBlur, createOrEdit, showadd, showedit, createParams, resourceobj, transactionresourcename } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value, valueObj) => {field.input.onChange(value);setTimeout(()=>{onChange(value, valueObj);},0);} : field.input.onChange;
	var onFocusfunction = (typeof(onFocus) == 'function') ? (value) => {field.input.onFocus(value);onFocus(value);} : field.input.onFocus;
	var onBlurfunction = (typeof(onBlur) == 'function') ? (value) => {field.input.onBlur(value);onBlur(value);} : field.input.onBlur;

	return (
		<AutoMultiSelect resourceobj={resourceobj} transactionresourcename={transactionresourcename} resource={resource} fields={fields} displaylabel={displaylabel} label={label} valuename={valuename} filter={filter} createOrEdit={createOrEdit} showadd={showadd} showedit={showedit} createParams={createParams} {...field.input} onChange={onChangeFun} onFocus={onFocusfunction} onBlur={onBlurfunction} placeholder={field.placeholder} className={`${field.classname} ${field.meta.error ? 'errorinput' : ''}`}  required={field.required} disabled={field.disabled} />
	);
}

export const autosuggestEle= function(field) {
	var { resource, onChange } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value) => {field.input.onChange(value);setTimeout(()=>{onChange(value);},0);} : field.input.onChange;

	return (
		<AutoSuggest resource={resource} field={field.field} address_country={field.address_country} feedbackContactArray={field.feedbackContactArray} {...field.input} placeholder={field.placeholder} required={field.required} disabled={field.disabled} onChange={onChangeFun} className={`${field.classname} ${field.meta.error ? 'errorinput' : ''}`} />
	);
}

export const dateTimeEle= function(field) {
	var { onChange, onFocus, onBlur, min, max } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value) => {field.input.onChange(value);setTimeout(()=>{onChange(value);},0);} : (value) => field.input.onChange(value);
	var onFocusFun = (typeof(onFocus) == 'function') ? (value) => {field.input.onFocus(value);setTimeout(()=>{onFocus(value);},0);} : (value) => field.input.onFocus(value);
	var onBlurFun = (typeof(onBlur) == 'function') ? (value) => {
		if(moment(value).isValid()) {
			field.input.onBlur(value);setTimeout(()=>{onBlur(value);},0);
		}
	} : (value) => {
		if(moment(value).isValid()) {
			field.input.onBlur(value);
		}
	}

	let minDate = field.min ? moment(field.min) : undefined;
	let maxDate = field.max ? moment(field.max) : undefined;

	/*return (
		<DateTime className={`${field.classname} ${field.meta.error ? 'errorinput' : ''}`} {...field.input} value={moment(field.input.value)} dateFormat="DD-MMM-YYYY" timeFormat="hh:mm A" onChange={onChangeFun} onFocus={onFocusFun} onBlur={onBlurFun} placeholder={field.placeholder} disabled={field.disabled} required={field.required} autoComplete="off"/>
	);*/

	return (
		<DateTimeElement
			className={`${field.classname} ${field.meta.error ? 'errorinput' : ''}`}
			value={field.input.value}
			onChange={onChangeFun}
			onFocus={onFocusFun}
			onBlur={onBlurFun}
			required={field.required}
			disabled={field.disabled}
			placeholder={field.placeholder}
			min = {minDate}
			max={maxDate} />
	);
}

export const alertinfoEle= function(field) {
	return (
		<div className="col-md-12 col-sm-12 col-xs-12" >
			<div className={`${field.messageclass} col-md-12 col-sm-12 col-xs-12 alert viewalert`}>{field.message}</div>
		</div>
	);
}

export class RichText extends Component {
	constructor(props) {
		super(props);
		this.state = {
			richtextUUID : uuidv1()
		};
	}
	
	componentDidMount() {
		$(`#${this.state.richtextUUID}`).wysiwyg({
			toolbarSelector: `[data-role=editor-toolbar-${this.state.richtextUUID}]`
		});
		if(this.props.value)
			$(`#${this.state.richtextUUID}`).html(this.props.value);
		$(`#${this.state.richtextUUID}`).on('change', () => {
			this.props.onChange($(`#${this.state.richtextUUID}`).html());
		});
		$(`#${this.state.richtextUUID}`).attr('contenteditable', this.props.disabled ? false : true);
	}
	
	componentWillReceiveProps(nextProps) {
		if($(`#${this.state.richtextUUID}`).html() != (nextProps.value ? $("<div>"+nextProps.value+"</div>").html() : "")) {
			nextProps.onChange(nextProps.value ? nextProps.value : "");
			$(`#${this.state.richtextUUID}`).html(nextProps.value ? nextProps.value : "")
		}
		$(`#${this.state.richtextUUID}`).attr('contenteditable', nextProps.disabled ? false : true);
	}

	render() {
		return (
			<div style={{width: '100%', float: 'left'}}>
				<div className={`btn-toolbar richtext-toolbar ${this.props.disabled ? 'disabled' : ''}`} data-role={`editor-toolbar-${this.state.richtextUUID}`} data-target={`#${this.state.richtextUUID}`}>
					<div className="btn-group">
						<a className="btn btn-sm btn-light" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><span className="fa fa-bold"></span></a>
						<a className="btn btn-sm btn-light" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><span className="fa fa-italic"></span></a>
						<a className="btn btn-sm btn-light" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><span className="fa fa-underline"></span></a>
					</div>
					<div className="btn-group">
						<a className="btn btn-sm btn-light" data-edit="insertunorderedlist" title="Bullet list"><span className="fa fa-list-ul"></span></a>
						<a className="btn btn-sm btn-light" data-edit="insertorderedlist" title="Number list"><span className="fa fa-list-ol"></span></a>
						<a className="btn btn-sm btn-light" data-edit="outdent" title="Reduce indent (Shift+Tab)"><span className="fa fa-outdent"></span></a>
						<a className="btn btn-sm btn-light" data-edit="indent" title="Indent (Tab)"><span className="fa fa-indent"></span></a>
					</div>
					<div className="btn-group">
						<a className="btn btn-sm btn-light" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><span className="fa fa-align-left"></span></a>
						<a className="btn btn-sm btn-light" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><span className="fa fa-align-center"></span></a>
						<a className="btn btn-sm btn-light" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><span className="fa fa-align-right"></span></a>
						<a className="btn btn-sm btn-light" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><span className="fa fa-align-justify"></span></a>
					</div>
					<div className="btn-group">
						<a className="btn btn-sm btn-light" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><span className="fa fa-undo"></span></a>
						<a className="btn btn-sm btn-light" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><span className="fa fa-repeat"></span></a>
					</div>
				</div>
				<div id={`${this.state.richtextUUID}`} className={`richtext-editor ${this.props.required && this.props.value=="" ? 'errorinput' : ''} ${this.props.disabled ? 'disabled' : ''}`} disabled={this.props.disabled} ></div>
			</div>
		);
	}
}

export class richtextEle extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (<RichText value={this.props.input.value} onChange={this.props.input.onChange} required={this.props.required} disabled={this.props.disabled} />);
	}
}

export const qtyEle= function(field) {
	var { onChange, btnclick } = field;

	var onChangeFun = (value) => {
		field.input.onChange(value);
		if(typeof(onChange) == 'function') {
			setTimeout(()=>{onChange(value);},0);
		}
	}

	return (
		<div className="input-group">
			<NumberElement className={`form-control marginbottom-0 ${field.meta.error ? 'errorinput' : ''}`} placeholder={field.placeholder} value={field.input.value} onChange={onChangeFun} required={field.required} disabled={field.disabled} />
			{field.showstockbutton ? <span className="input-group-append">
				<button type="button" className="btn btn-sm gs-form-btn-secondary ndtclass" title="Check Stock Details" onClick={btnclick}><span className="fa fa-shopping-cart"></span></button>
			</span> : null}
		</div>
	);
}

export const StockInline = function(field) {
	var { btnclick } = field;

	return (
		<button type="button" className="btn btn-sm gs-form-btn-primary ndtclass" title="Check Stock Details" onClick={btnclick}><span className="fa fa-shopping-cart"></span></button>
	);
}

export const RateInline = function(field) {
	var { btnclick } = field;

	return (
		<button type="button" className="btn btn-sm gs-form-btn-secondary ndtclass" title="Analyse Item Rate" onClick={btnclick}><span className="fa fa-history"></span></button>
	);
}

export class discountmodeEle extends Component {
	constructor(props) {
		super(props);
		if(['Percentage', 'Rupees'].indexOf(this.props.input.value) == -1)
			this.props.input.onChange('Percentage');
		this.onChange = this.onChange.bind(this);
	}

	componentWillReceiveProps(newprops) {
		if(['Percentage', 'Rupees'].indexOf(newprops.input.value) == -1)
			newprops.input.onChange('Percentage');
	}

	onChange(value, field) {
		this.props.input.onChange(value);
		if(typeof(this.props.onChange) == 'function')
			setTimeout(()=>{this.props.onChange();},0);
	}

	render() {
		return (
			<span className="input-group-append">
				{this.props.input.value == 'Percentage' ? <button className="btn btn-sm gs-form-btn-secondary" type="button"  onClick={(value) => this.onChange('Rupees')} disabled={this.props.discountmodedisabled || this.props.disabled}>%</button> : ''}
				{this.props.input.value == 'Rupees' ? <button className="btn btn-sm gs-form-btn-secondary" type="button" onClick={(value) => this.onChange('Percentage')} disabled={this.props.discountmodedisabled || this.props.disabled}>{this.props.currencytitle}</button> : ''}
			</span>
		);
	}
}

export const ButtongroupEle = function(field) {
	var { onChange, buttons, buttonclass } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value, valueObj) => {field.input.onChange(value);setTimeout(()=>{onChange(value, valueObj);},0);} : field.input.onChange;

	return (
		<Buttongroup  buttons={buttons} buttonclass={buttonclass} onChange={onChangeFun} value={field.input.value} className={`${field.meta.error ? 'errorinput' : ''}`} required={field.required} disabled={field.disabled} />
	);
}

export const ExpenseRequestSummary = function(field) {
	return (
		<div className="col-md-12 col-sm-12">
			<table className="float-right lineheight-2">
				<tbody>
				<tr>
					<td align="right">Claims Requested :</td>
					<td align="right">{currencyFilter(field.resource.amountrequested, field.resource.currencyid, field.app)}</td>
					</tr>
					{checkActionVerbAccess(field.app, 'expenserequests', 'Approve') || checkActionVerbAccess(field.app, 'expenserequests', 'Confirm Expense') ? <tr>
					<td align="right">Claims Approved :</td>
					<td align="right">{currencyFilter(field.resource.amountapproved, field.resource.currencyid, field.app)}</td>
					</tr> : null}
				<tr>
					<td align="right">Advances Settled :</td>
					<td align="right">{currencyFilter(field.resource.settledamount, field.resource.currencyid, field.app)}</td>
					</tr>
				{checkActionVerbAccess(field.app, 'expenserequests', 'Approve') || checkActionVerbAccess(field.app, 'expenserequests', 'Confirm Expense') ? <tr>
					<td align="right">Reimbursement Amount :</td>
					<td align="right">{currencyFilter(field.resource.amount, field.resource.currencyid, field.app)}</td>
					</tr> : null}
				</tbody>
			</table>
		</div>
	);
}

export class Buttongroup extends Component {
	constructor(props) {
		super(props);

		this.toggleSelect = this.toggleSelect.bind(this);
	}

	toggleSelect(newValue) {
		this.props.onChange(newValue);
	}

	render() {
		let value = this.props.value;
		return (
			<div className={`btn-group ${this.props.className}`}>
				{ this.props.buttons.map((button, i) => {
					return (<button type="button" key={i} className={`btn btn-sm gs-btn-light ${this.props.buttonclass} ${(button.value == value) ? ' active' : ''}`} onClick={()=>this.toggleSelect(button.value)} disabled={this.props.disabled}>
							{button.icon ? <i className = {`fa ${button.icon} marginright-5`}></i> : null}
							{button.title}
						</button>
					);
				})}
			</div>
		);
	}
}

export const DisplayGroupEle= function(field) {
	var { onChange, onFocus, onBlur, child } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value, valueObj) => {field.input.onChange(value);setTimeout(()=>{onChange(value, valueObj);},0);} : field.input.onChange;
	var onFocusfunction = (typeof(onFocus) == 'function') ? (value) => {field.input.onFocus(value);onFocus(value);} : field.input.onFocus;
	var onBlurfunction = (typeof(onBlur) == 'function') ? (value) => {field.input.onBlur(value);onBlur(value);} : field.input.onBlur;

	return (
		<DisplayGroup child={child} onChange={onChangeFun} onFocus={onFocusfunction} onBlur={onBlurfunction} placeholder={field.placeholder} value={field.input.value} className={`${field.meta.error ? 'errorinput' : ''}`}  required={field.required} disabled={field.disabled} />
	);
}

export class contactelement extends Component {
	constructor(props) {
		super(props);
		this.state = {
			refresh: false
		}
		this.createFn = this.createFn.bind(this);
		this.editFn = this.editFn.bind(this);
		this.onChangeFun = this.onChangeFun.bind(this);
	}

	onChangeFun(value, valueObj) {
		this.props.input.onChange(value);
		if(typeof(this.props.onChange) == 'function') {
			setTimeout(()=>{this.props.onChange(value, valueObj);},0);
		}
	}

	editFn() {
		this.props.createOrEdit('/details/contacts/:id', this.props.input.value, {}, (valueObj) => {this.setState({refresh: !this.state.refresh});this.onChangeFun(valueObj.id, valueObj);});		
	}

	createFn() {
		var connectingObj = {
			parentresource: this.props.parentresource,
			parentid: this.props.parentid
		};
		this.props.createOrEdit('/createContact', null, connectingObj, (valueObj) => {this.setState({refresh: !this.state.refresh});this.onChangeFun(valueObj.id, valueObj);});
	}

	render() {
		return (
			<div className="row">
				<div className="col-md-9">
					<div className="card-title">
						<SelectAsync refresh={this.state.refresh} className={`${this.props.meta.error ? 'errorinput' : ''}`} {...this.props.input} resource={'contacts'} fields={'id,name,isprimary,mobile,email,phone,designation,organization'} filter={`contacts.parentresource='${this.props.parentresource}' and contacts.parentid=${this.props.parentid}`} onChange={this.onChangeFun} />
					</div>
				</div>
				<div className="col-md-3 text-right" style={{paddingLeft: '0px'}}>
					{this.props.input.value ? <a className="btn btn-sm btn-outline-primary btndisable" onClick={this.editFn} style={{marginRight: '5px'}}><span className="fa fa-edit"></span></a> : null }
					<a className="btn btn-sm btn-outline-success btndisable" onClick={this.createFn}><span className="fa fa-plus"></span></a>
				</div>
			</div>
		);
	}
}

export const ContactEle = connect((state)=>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			refresh: false
		};
		this.editContact = this.editContact.bind(this);
		this.createContact = this.createContact.bind(this);
		this.onCallTo = this.onCallTo.bind(this);
	}

	editContact() {
		this.props.createOrEdit('/details/contacts/:id', this.props.resource[this.props.model[0]], {}, (valueObj) => {
			this.setState({refresh: !this.state.refresh});
			this.props.onChange(valueObj.id, valueObj);
		});
	}

	createContact() {
		var connectingObj = {
			parentresource: this.props.parentresource,
			parentid: this.props.parentid
		};
		this.props.createOrEdit('/createContact', null, connectingObj, (valueObj) => {this.setState({refresh: !this.state.refresh});this.props.updateFormState(this.props.form, {[this.props.model[0]] : valueObj.id});this.props.onChange(valueObj.id, valueObj);});
	}

	onCallTo(number) {
		if(this.props.app.feature.enableDialerIntegration && this.props.app.user.extension){
			if(!this.props.app.feature.dialerOutboundURL){
				return this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Click to call API not configured. Please reach out to support for help.',
					btnArray : ['Ok']
				}));
			}
			
			axios.get(`/telephony/clicktocall?extn=${this.props.app.user.extension}&destinationNo=${number}`).then((response) => {
				if(response.data.message == "success") {
					this.props.openModal(modalService['infoMethod']({
						header : 'Info',
						body : response.data.main[0],
						btnArray : ['Ok']
					}));
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			});
		}else{
			window.open(`tel:${number}`);
		}	
	}

	render() {
		let tempvalidate = [numberNewValidation({required: this.props.required ? true : false,
				model: 'Contact'})];
		let contactidname = this.props.model[0];
		let contactperson = this.props.model[1];
		let mobile = this.props.model[2];
		let phone = this.props.model[3];
		let email = this.props.model[4];
		let inputfieldprops = {
			disabled: true
		};
		let emailValidation = [emailNewValidation({required: this.props.emailrequired ? true : false,
				model: 'Email'})]
		let mobileValidation = [stringNewValidation({required: this.props.mobilerequired ? true : false,
				model: 'Mobile'})]
		let phoneValidation = [stringNewValidation({required: this.props.phonerequired ? true : false,
				model: 'Phone'})]

		let contactfieldProps = {
			resource: 'contacts',
			fields: 'id,name,isprimary,mobile,email,phone,designation,organization,displayname',
			filter: this.props.filter ? this.props.filter : (!this.props.parentresource && !this.props.parentid ? `contacts.parentid is null` :  `contacts.parentresource='${this.props.parentresource}' and contacts.parentid=${this.props.parentid}`),
			required: this.props.required,
			disabled: this.props.disabled,
			showadd: true,
			createCallback: this.createContact,
			createOrEdit: this.props.createOrEdit,
			refresh: this.state.refresh,
			onChange: this.props.onChange,
			classname: 'gs-select-input-group'
		};

		let autoselectneeded = !this.props.filter && !this.props.parentresource && !this.props.parentid ? true : false;
		if(autoselectneeded) {
			contactfieldProps.displaylabel = "name";
			contactfieldProps.label = "displayname";
		}

		return (
			<div className="d-flex flex-wrap">
				<div className="input-group">
					{autoselectneeded ? <Field name={contactidname} props={contactfieldProps} component={autoSelectEle} validate={tempvalidate} /> : <Field name={contactidname} props={contactfieldProps} component={selectAsyncEle} validate={tempvalidate} />}
					{this.props.resource && this.props.resource[contactidname] ? <span className="input-group-append">
						<button type="button" className="btn btn-sm gs-form-btn-secondary btndisable" onClick={this.editContact}><span className="fa fa-edit"></span></button>
					</span> : null }
				</div>
				<div className="input-group">
					<Field name={email} props={inputfieldprops} component={InputEle} validate={emailValidation} />
				</div>
				<div className="input-group">
					<Field name={mobile} props={inputfieldprops} component={InputEle} validate={mobileValidation} />
					<div className="input-group-append">
						<button type='button' className="btn btn-sm gs-btn-call" onClick={() => this.onCallTo(this.props.resource[this.props.model[2]])}><span className="fa fa-phone"></span></button>
					</div>
				</div>
				<div className="input-group">
					<Field name={phone} props={inputfieldprops} component={InputEle} validate={phoneValidation} />
					<div className="input-group-append">
						<button type='button' className="btn btn-sm gs-btn-call" onClick={() => this.onCallTo(this.props.resource[this.props.model[3]])}><span className="fa fa-phone"></span></button>
					</div>
				</div>
			</div>
		);
	}
});

export const AddressEle = connect((state)=>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.openAddress = this.openAddress.bind(this);
	}

	openAddress() {
		this.props.openModal({
			render: (closeModal) => {
				return <AddressModal {...this.props} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'},
			confirmModal : true
		});
	}

	render() {
		let tempvalidate = [stringNewValidation({required: this.props.required ? true : false})];
		let addressid = this.props.model[0];
		let address = this.props.model[1];
		let addressProps = {
			...this.props,
			openAddress: this.openAddress,
			readonly: this.props.app.feature.addressMasterRequired ? 'readonly' : false
		};

		return (
			<div>
				<label className="labelclass">{this.props.temptitle}</label>
				<Field name={address} props={addressProps} component={addressTextareaEle} validate={tempvalidate} />
				{!this.props.disabled ? <div className="gs-address-pick">
					<button type="button" className="btn btn-sm gs-form-btn-primary" onClick={this.openAddress} disabled={this.props.disabled}><i className="fa fa-external-link"></i>Pick address</button>
				</div> : null}
			</div>
		);
	}
});

export class addressTextareaEle extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.onFocusFun = this.onFocusFun.bind(this);
		this.onChangefn = this.onChangefn.bind(this);
	}
	
	onFocusFun(evt) {
 		if(!this.props.disabled && this.props.readonly) {
 			this.props.openAddress();
 			this.refs.textarearef.blur();
 		}
	}
	
	onChangefn(evt) {
		this.props.input.onChange(evt.target.value);
	}

	render() {
		return (
			<textarea ref="textarearef" className={`form-control ${this.props.classname} ${this.props.meta.error ? 'errorinput' : ''}`} value={this.props.input.value} onFocus={this.onFocusFun} required={this.props.required} title={this.props.input.value} rows="3" readOnly={this.props.readonly || this.props.disabled} onChange={this.onChangefn} autoComplete="off" />
		);
	}
}

export class DiscountField extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let discqtyField = this.props.model[0];
		let discmodeField = this.props.model[1];
		let tempvalidate = [numberNewValidation({required: this.props.required ? true : false})];

		return (
			<div className="input-group">
				<Field name={discqtyField} props={this.props} component={NumberEle} validate={tempvalidate}/>
				<Field name={discmodeField} props={this.props} component={discountmodeEle} />
			</div>
		);
	}
}

export class RateField extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let options = {
			required: this.props.required ? true : false
		};
		if(this.props.min != null && this.props.min != '')
			options.min = this.props.min;
		if(this.props.max != null && this.props.max != '')
			options.max = this.props.max;
		let tempvalidate = [numberNewValidation(options)];

		return (
			<div className="input-group">
				<Field name={this.props.name} props={this.props} component={NumberEle} validate={tempvalidate} />
				{this.props.showanalyseratebutton ? <span className="input-group-append">
					<button type="button" className="btn btn-sm gs-form-btn-secondary ndtclass" onClick={this.props.btnclick}><span className="fa fa-history"></span></button>
				</span> : null}
			</div>
		);
	}
}

export class ReportDateRangeField extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.changePeriod = this.changePeriod.bind(this);
	}

	changePeriod(value) {
		let todate = (value != "Custom") ? new Date() : this.props.resource.todate;
		let fromdate = new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)));
		if (value == "Last week")
			fromdate = new Date(new Date().setDate(new Date().getDate() - 7));
		else if (value == "Last 30 days")
			fromdate = new Date(new Date().setDate(new Date().getDate() - 30));
		else if (value == "Last month")
			fromdate = new Date(new Date().setMonth(new Date().getMonth() - 1));
		else if (value == "Last 3 months")
			fromdate = new Date(new Date().setMonth(new Date().getMonth() - 3));
		else if (value == "Last 6 months")
			fromdate = new Date(new Date().setMonth(new Date().getMonth() - 6));
		else if (value == "Last 1 year")
			fromdate = new Date(new Date().setFullYear(new Date().getFullYear() - 1));

		fromdate = new Date(fromdate.setDate(fromdate.getDate() + 1));

		this.props.updateFormState(this.props.form, {
			fromdate,
			todate
		});
	}

	render() {
		let periodEleModal = this.props.model[0];
		let fromdateEleModal = this.props.model[1];
		let todateEleModal = this.props.model[2];
		return (
			<div style={{width: '100%'}}>
				<div className="form-group col-md-12 col-sm-12">
					<label className="labelclass">Period</label>
					<Field name={periodEleModal} props={{options: [{value: "Last week", label: "Last 7 Days"}, {value: "Last month", label: "Last Month"}, {value: "Last 3 months", label: "Last Quarter"}, {value: "Last 30 days", label: "Last 30 Days"}, {value: "Last 6 months", label: "Last 6 Months"}, {value: "Last 1 year", label: "Last 1 year"}, {value: "Custom", label: "Custom"}], label:"label", valuename: "value", onChange: (value)=>{this.changePeriod(value)}}} component={localSelectEle} />
				</div>
				<div className="form-group col-md-12 col-sm-12">
					<label className="labelclass">From Date</label>
					<Field name={fromdateEleModal} props={{required: true, disabled: this.props.resource.period != 'Custom'}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: this.props.required ? this.props.required : true, title : 'From Date'})]}/>
				</div>
				<div className="form-group col-md-12 col-sm-12">
					<label className="labelclass">To Date</label>
					<Field name={todateEleModal} props={{required: true, disabled: this.props.resource.period != 'Custom'}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: this.props.required ? this.props.required : true, title : 'To Date', min : '{resource.fromdate}'})]}/>
				</div>
			</div>
		);
	}
}


export class DocumentLinkEle extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.openDocumentDetails = this.openDocumentDetails.bind(this);
	}

	openDocumentDetails() {
		let resourceObj = {
			library:[],
			...this.props.input.value
		};
		this.props.openModal({render: (closeModal) => {return <DocumentuploadModal {...this.props} resource={resourceObj} multiple={false} localdoc={[]} hidelocaldoc={false} closeModal={closeModal} callback={(value)=>{
		this.props.input.onChange(value);
	}} />}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
	}

	render() {
		return (
			<div className="input-group mb-3">
				{this.props.input.value ? <a className="form-control ndtclass" href={this.props.input.value.fileurl} target="_blank" style={{overflow: 'hidden', textOverflow: 'ellipsis', backgroundColor: 'rgb(245, 245, 245)', boxShadow: 'none', wordBreak: 'break-all'}}>{this.props.input.value.name}</a> : <input type="text" className={`form-control ${this.props.required ? 'errorinput' : ''}`} required={this.props.required} disabled/>}
				<div className="input-group-append">
					<button className={`btn btn-sm btn-secondary ${this.props.classname}`}  type="button" onClick={() => {this.openDocumentDetails()}}><span className="fa fa-folder-open"></span></button>
				</div>
			</div>
		);
	}
}

export class BIReportSelectField extends Component {
	constructor(props) {
		super(props);
		this.state = {
			refresh: false
		}
		this.createBIFn = this.createBIFn.bind(this);
		this.editBIFn = this.editBIFn.bind(this);
	}

	editBIFn() {
		let connectingObj = {...this.props.parentobj};
		connectingObj.reportname = this.props.reportname;
		this.props.createOrEdit('/details/biconfigurations/:id', this.props.parentobj.configid, connectingObj, (value) => {this.setState({refresh: !this.state.refresh});this.props.updateFormState(this.props.form, {configid: value});});
	}

	createBIFn() {
		let connectingObj = {...this.props.parentobj};
		connectingObj.reportname = this.props.reportname;
		this.props.createOrEdit('/createBIconfig', null, connectingObj, (value) => {this.setState({refresh: !this.state.refresh});this.props.updateFormState(this.props.form, {configid: value});});
	}

	render() {
		return (
			<div className="input-group">
				<Field name={'configid'} props={{resource: "biconfigurations", fields: "id,name,description,config", filter: `biconfigurations.reportname='${this.props.reportname}' and biconfigurations.resourcename='${this.props.parentobj.resource}'`, onChange: this.props.onChangeFn, required: true, refresh: this.state.refresh, classname: 'gs-select-input-group'}} component={selectAsyncEle} validate={[numberNewValidation({required: true, model: 'Report Name'})]} />
				<span className="input-group-append">
					<button type="button" className="btn btn-sm btn-primary" disabled={!this.props.parentobj.configid} onClick={this.editBIFn}><span className="fa fa-edit"></span></button>
					<button type="button" className="btn btn-sm btn-success" onClick={this.createBIFn}><span className="fa fa-plus"></span></button>
				</span>
			</div>
		);
	}
}

export class UUIDEle extends Component {
	constructor(props) {
		super(props);

		this.copyUUID = this.copyUUID.bind(this);
		this.resetUUID = this.resetUUID.bind(this);
	}

	resetUUID = () => {
		let uuidvalue = uuidv1();
		this.props.updateFormState(this.props.form, {[this.props.model]: uuidvalue});
	}

	copyUUID = () => {
		if(window.confirm(`Do you want to copy ${this.props.resource[this.props.model]} ?`))
			commonMethods.copyToClipBoard(this.props.resource[this.props.model]);
	}

	render() {
		return (
			<div className="input-group mb-3">
				<Field name={this.props.model} classname={!this.props.resource[this.props.model] ? 'errorinput' : ''} props={{disabled:true, required: this.props.required}} component={InputEle} />
				<div className="input-group-append">
					<button type="button" className="btn btn-sm gs-btn-secondary" title="Copy to Clipboard" onClick={this.copyUUID}><span className="fa fa-copy"></span></button>
					<button type="button" className="btn btn-sm gs-btn-secondary" title="Regenerate" onClick={this.resetUUID}><span className="fa fa-refresh"></span></button>
				</div>
			</div>
		);
	}
}

export class ContractEle extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let contractno = `${this.props.itemstr}.${this.props.model[0]}`;
		let contractstartdate = `${this.props.itemstr}.${this.props.model[1]}`;
		let contractexpiredate = `${this.props.itemstr}.${this.props.model[2]}`;
		let contracttypename = `${this.props.itemstr}.${this.props.model[3]}`;

		let inputfieldprops = {
			disabled: true
		};

		return (
			<div className="input-group">
				<div className="input-group">
					<Field name={contractno} props={inputfieldprops} component={InputEle}/>
				</div>
				<div className="input-group">
					<div style={{width:'40%', float: 'left'}}>
						<Field name={contractstartdate} props={inputfieldprops} component={DateEle}/>
					</div>
					<div style={{width:'10%', float: 'left', backgroundColor: '#fbfbfd'}}>-</div>
					<div style={{width:'50%', float: 'left'}}>
						<Field name={contractexpiredate} props={inputfieldprops} component={DateEle}/>
					</div>
				</div>
				{this.props.model[3] ? <div className="input-group">
					<Field name={contracttypename} props={inputfieldprops} component={InputEle}/>
				</div> : null}
			</div>
		);
	}
}

export class customrichtextEle extends Component {
	constructor(props) {
		super(props);
		this.state = {
			customrichtextUUID : uuidv1()
		};
		this.openRichtextModal = this.openRichtextModal.bind(this);
	}

	componentDidMount() {
		if(this.props.input.value) {
			$(`#${this.state.customrichtextUUID}`).html(this.props.input.value);
		}
	}

	componentWillReceiveProps(nextProps) {
		if($(`#${this.state.customrichtextUUID}`).html() != (nextProps.input.value ? nextProps.input.value  : "")) {
			$(`#${this.state.customrichtextUUID}`).html(nextProps.input.value ? nextProps.input.value : "");
			nextProps.input.onChange($(`#${this.state.customrichtextUUID}`).html());
		}
	}

	openRichtextModal() {
		this.props.openModal({
			render: (closeModal) => {
				return <RichTextModal {...this.props} closeModal={closeModal} />
			}, className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	render() {
		return (
			<div id={`${this.state.customrichtextUUID}`} className={`customrichtext ${this.props.required && this.props.value=="" ? 'errorinput' : ''} ${this.props.disabled ? 'disabled' : ''}`} required={this.props.required} onClick={this.openRichtextModal} title="Click here to edit/view"></div>
		);
	}
}

export class EmailAttachmentDownloadEle extends Component {
	constructor(props) {
		super(props);

		this.renderEmailAttachment = this.renderEmailAttachment.bind(this);
	}

	renderEmailAttachment() {
		return this.props.resource.emailattachmentdetails.map((attachment) => {
			return (
				<div className="col-lg-12 col-md-12 col-sm-12 col-12">
					<a href={attachment.url} target="_blank">{attachment.name}</a>
				</div>
			);
		});
	}

	render() {
		return (
			<div className="w-100 float-left form-group">
				<div className="row">
					{this.props.resource.emailattachmentdetails && this.props.resource.emailattachmentdetails.length > 0 ? this.renderEmailAttachment() : <div className="col-lg-12 col-md-12 col-sm-12 col-12"><i>No Attachment found!</i></div>}
				</div>
			</div>
		);
	}
}

export const LocationAddressEle = connect((state)=>{
	return {app: state.app}
}) (class extends Component {
	constructor(props) {
		super(props);
		this.openLocationMapModal = this.openLocationMapModal.bind(this);
	}

	openLocationMapModal() {
		this.props.openModal({
			render: (closeModal) => {
				return <LocationmastermapModal {...this.props} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-60', overlay: 'react-modal-overlay-custom-class'},
			confirmModal : true
		});
	}

	render() {
		let tempvalidate = [stringNewValidation({required: this.props.required ? true : false})];
		let addressProps = {
			...this.props,
			openLocationMapModal: this.openLocationMapModal
		};

		return (
			<div>
				<label className="labelclass">{this.props.temptitle}</label>
				<Field name={this.props.model} props={addressProps} component={addressTextareaEle} validate={tempvalidate} />
				<div className="gs-address-pick">
					<button type="button" className="btn btn-sm gs-form-btn-primary" onClick={this.openLocationMapModal} disabled={!this.props.resource.address}>Mark location on map <i className="fa fa-map-marker"></i></button>
				</div>
			</div>
		);
	}
});

export const costcenterAutoMultiSelectEle = function(field) {
	var { label, valuename, onChange, onFocus, onBlur, restrictToSingleSelect, categoryFilter, showUncategorized } = field;

	var onChangeFun = (typeof(onChange) == 'function') ? (value, valueObj) => {field.input.onChange(value);setTimeout(()=>{onChange(value, valueObj);},0);} : field.input.onChange;
	var onFocusfunction = (typeof(onFocus) == 'function') ? (value) => {field.input.onFocus(value);onFocus(value);} : field.input.onFocus;
	var onBlurfunction = (typeof(onBlur) == 'function') ? (value) => {field.input.onBlur(value);onBlur(value);} : field.input.onBlur;

	return (
		<CostCenterAutoMultiSelect  label={label} valuename={valuename}  {...field.input} onChange={onChangeFun} onFocus={onFocusfunction} onBlur={onBlurfunction} placeholder={field.placeholder} className={`${field.classname} ${field.meta.error ? 'errorinput' : ''}`}  required={field.required} disabled={field.disabled} restrictToSingleSelect={restrictToSingleSelect} categoryFilter={categoryFilter} showUncategorized={showUncategorized} />
	);
}
