import React, { Component } from 'react';

export default class Loadingcontainer extends Component {
	render() {
		return(
			<div className={this.props.isloading ? 'showloader' : 'hideloader'}>
				<div className="loader-overlay" style={{position: this.props.isAbsolute ? 'absolute' : 'fixed'}}></div>
				<svg className="circular" viewBox="25 25 50 50" style={{position: this.props.isAbsolute ? 'absolute' : 'fixed'}}><circle className="path" cx="50" cy="50" r="20" fill="none" strokeWidth="3" strokeMiterlimit="10"/></svg>
			</div>
		);
	};
}

