export default function(state = {}, action) {
	switch(action.type) {
		case 'GS_App_Update': {
			let newState = JSON.parse(JSON.stringify(state));
			if(typeof(action.payload.prop) == 'string')
				newState[action.payload.prop] = action.payload.value;
			else {
				for(var prop in action.payload.prop)
					newState[prop] = action.payload.prop[prop];
			}
			return newState;
		}
  	}
	return state;
}