export default function(state = {}, action) {
	switch(action.type) {
		case 'GS_LIST_Update': {
			//var newState = JSON.parse(JSON.stringify(state));
			state[action.payload.prop] = action.payload.value;
			return JSON.parse(JSON.stringify(state));
		}
		case 'GS_LIST_View_Update': {
			var newState = JSON.parse(JSON.stringify(state));
			if(newState[action.payload.prop]) {
				newState[action.payload.prop].currentview = action.payload.value;
			}
			return newState;
		}
  	}
	return state;
}
