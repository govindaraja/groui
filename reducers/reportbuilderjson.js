export default function(state = {}, action) {
	switch(action.type) { 
		case 'GS_ReportBuilderJSON_Update': {
			var newState = JSON.parse(JSON.stringify(state));
			if(!newState[action.payload.title])
				newState[action.payload.title] = {};
			newState[action.payload.title] = action.payload.value;
			return newState;
		}
  	}
	return state;
}
