import { v1 as uuidv1 } from 'uuid';

export default function(state = [], action) {
	switch(action.type) {
		case 'GS_Modal_Open': {
			action.payload.id = uuidv1();
			action.payload.isOpen = true;
			return [...state, action.payload];
		}
		case 'GS_Modal_Close': {
			let key = -1;
			state.map((item, index) => {
				if(item.id == action.payload.id)
					key = index;
			});
			if(state.length > -1)
				state.splice(key, 1);
			return [...state];
		}
  	}
	return state;
}
