import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import app from './app';
import pagejson from './pagejson';
import list from './list';
import modal from './modal';
import reportjson from './reportjson';
import reportbuilderjson from './reportbuilderjson';
import reportfilter from './reportfilter';

const rootReducer = combineReducers({
	app: app,
	form: formReducer,
	pagejson : pagejson,
	list: list,
	modal: modal,
	reportjson: reportjson,
	reportbuilderjson: reportbuilderjson,
	reportfilter: reportfilter
});

export default rootReducer;
