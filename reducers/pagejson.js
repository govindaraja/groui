export default function(state = {}, action) {
	switch(action.type) {
		case 'GS_PageJSON_Update': {
			var newState = JSON.parse(JSON.stringify(state));
			if(!newState[action.payload.title])
				newState[action.payload.title] = {};
			newState[action.payload.title][action.payload.type] = action.payload.value;
			return newState;
		}
		case 'GS_PageJSON_View_Update': {
			var newState = JSON.parse(JSON.stringify(state));
			if(newState[action.payload.title] && newState[action.payload.title][action.payload.type]) {
				let itemFound = false;
				let pagejson = newState[action.payload.title][action.payload.type];
				for(var i=0; i < pagejson.views.length; i++) {
					if(pagejson.views[i].id == action.payload.value.id) {
						if(action.payload.value.deleteParam)
							pagejson.views.splice(i, 1);
						else
							pagejson.views[i] = action.payload.value;
						itemFound = true;
						break;
					}
				}
				if(!itemFound && !action.payload.value.deleteParam)
					pagejson.views.splice(pagejson.views.length-1, 0, action.payload.value);
			}
			return newState;
		}
  	}
	return state;
}