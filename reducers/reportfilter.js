export default function(state = {}, action) {
	switch(action.type) {
		case 'GS_Report_Filter_Update': {
			if(!state[action.payload.prop])
				state[action.payload.prop] = {};

			for(var prop in action.payload.value)
				state[action.payload.prop][prop] = action.payload.value[prop];

			return JSON.parse(JSON.stringify(state));
		}
		case 'GS_Report_Filter_Reset_By_Report': {
			let payloadPropArray = action.payload.prop.split('_');

			if(action.payload.prop.split('_').length == 2)
				delete state[action.payload.prop];

			for(var propvalue in state) {
				let propvalueArray = propvalue.split('_');
				if(propvalueArray.length == 3 && payloadPropArray[0] == propvalueArray[0] && payloadPropArray[1] == propvalueArray[2])
					delete state[propvalue];

				if(propvalueArray.length == 3 && payloadPropArray[0] == propvalueArray[0] && propvalueArray[2] == 'drilldown' && payloadPropArray[1] == propvalueArray[1])
					delete state[propvalue];
			}

			return JSON.parse(JSON.stringify(state));
		}
		case 'GS_Report_Filter_Reset': {
			return JSON.parse(JSON.stringify({}));
		}
  	}
	return state;
}
