import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import ReactGantt from 'gantt-for-react';
import ReactPaginate from 'react-paginate';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { stringNewValidation, numberNewValidation } from '../utils/utils';
import { localSelectEle, autoSelectEle } from '../components/formelements';
import { AutoSelect, DateTimeElement, ChildEditModal } from '../components/utilcomponents';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class ProductionScheduleReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.openActivityModal = this.openActivityModal.bind(this);
		this.updateProdcutionSchedule = this.updateProdcutionSchedule.bind(this);
		this.renderPagination = this.renderPagination.bind(this);
		this.handlePaginationClick = this.handlePaginationClick.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				viewmode : 'Month',
				skip : 0,
				totalcount : 0,
				scrollOffsets: {
					'Day': 10,
					'Week': 2,
					'Month': 1
				}
			};
		}

		this.props.initialize(tempObj);

		setTimeout(() => {
			if(this.props.reportdata)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);

		let filterString = [];
		['operatorid', 'workcenterid', 'processid'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`productionschedule.${item}=${this.props.resource[item]}`);
			}
		});

		if (this.props.resource.productionorderid) {
			filterString.push(`productionschedule.parentid=${this.props.resource.productionorderid}`);
		}
	
		axios.get(`/api/productionschedule?&field=id,processid,workcenterid,setuptime,processtime,parentid,parentresource,operatorid,displayorder,modified,plannedstarttime,plannedendtime,processes/name/processid,workcenters/name/workcenterid,employees/displayname/operatorid,productionorders/orderno/parentid&pagelength=10&skip=${(this.props.resource.skip * 10)}&sortstring=productionschedule.id&filtercondition=productionschedule.plannedstarttime is not null and productionschedule.plannedendtime is not null and productionschedule.status='Pending' ${filterString.length > 0 ? ' and '+filterString.join(' and ') : ''}`).then((response) => {
			if (response.data.message == 'success') {
				let data = [];

				response.data.main = response.data.main.sort((a, b) => {
					return (a.parentid > b.parentid ? 1 : (a.parentid < b.parentid ? -1 : 0));
				})
				for(var i= 0; i < response.data.main.length; i++ ) {
					let itemFound = false;
					let name = `${response.data.main[i].parentid} - ${response.data.main[i].parentid_orderno}`;
					for(var j = 0; j < data.length; j++) {
						if(name == data[j].id) {
							//let childname = response.data.main[i].parentid_orderno+"/"+response.data.main[i].processid_name +" - "+response.data.main[i].workcenterid_name + (response.data.main[i].operatorid_displayname ? " - "+response.data.main[i].operatorid_displayname : "");
							let minDate = data[j].start;
							let maxDate = data[j].end;
							if(new Date(response.data.main[i].plannedstarttime) < new Date(minDate)) {
								minDate = new Date(response.data.main[i].plannedstarttime).toISOString()
							}
							if(new Date(response.data.main[i].plannedendtime) > new Date(maxDate)) {
								maxDate = new Date(response.data.main[i].plannedendtime).toISOString()
							}
							data[j].start = minDate;
							data[j].end = maxDate;
							//data[j].dependencies.push(childname);
							itemFound = true;
							break;
						}
					}
					if(!itemFound) {
						//let childname = response.data.main[i].parentid_orderno+"/"+response.data.main[i].processid_name +" - "+response.data.main[i].workcenterid_name + (response.data.main[i].operatorid_displayname ? " - "+response.data.main[i].operatorid_displayname : "");
						data.push({
							id : name,
							transactionid : response.data.main[i].id,
							name : response.data.main[i].parentid_orderno,
							parentid : response.data.main[i].parentid,
							start : new Date(response.data.main[i].plannedstarttime).toISOString(),
							end : new Date(response.data.main[i].plannedendtime).toISOString(),
							progress : 0,
							isparent: true,
							custom_class: 'gantt_parent',
							//dependencies : [childname]
							dependencies : []
						});
					}
					let childname = response.data.main[i].parentid_orderno+"/"+response.data.main[i].processid_name +" - "+response.data.main[i].workcenterid_name + (response.data.main[i].operatorid_displayname ? " - "+response.data.main[i].operatorid_displayname : "");
					data.push({
						name : childname,
						start : new Date(response.data.main[i].plannedstarttime).toISOString(),
						end : new Date(response.data.main[i].plannedendtime).toISOString(),
						id : childname,
						transactionid : response.data.main[i].id,
						parentid : response.data.main[i].parentid,
						parentresource : response.data.main[i].parentresource,
						modified : response.data.main[i].modified,
						workcenterid : response.data.main[i].workcenterid,
						workcenterid_name : response.data.main[i].workcenterid_name,
						processid : response.data.main[i].processid,
						processid_name : response.data.main[i].processid_name,
						progress : 0,
						isparent: false,
						dependencies: []
					});
				}

				/*for(var i= 0; i < response.data.main.length; i++ ) {
					let name = `${response.data.main[i].parentid} - ${response.data.main[i].parentid_orderno}`;
					let childname = response.data.main[i].parentid_orderno+"/"+response.data.main[i].processid_name +" - "+response.data.main[i].workcenterid_name + (response.data.main[i].operatorid_displayname ? " - "+response.data.main[i].operatorid_displayname : "");
					data.push({
						name : childname,
						start : new Date(response.data.main[i].plannedstarttime).toISOString(),
						end : new Date(response.data.main[i].plannedendtime).toISOString(),
						id : childname,
						transactionid : response.data.main[i].id,
						parentid : response.data.main[i].parentid,
						parentresource : response.data.main[i].parentresource,
						modified : response.data.main[i].modified,
						workcenterid : response.data.main[i].workcenterid,
						workcenterid_name : response.data.main[i].workcenterid_name,
						processid : response.data.main[i].processid,
						processid_name : response.data.main[i].processid_name,
						progress : 0,
						isparent: false,
						dependencies: [name]
					});
				}*/

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					totalcount: response.data.count,
					data
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			viewmode : 'Day',
			skip : 0,
			totalcount : 0,
			operatorid : null,
			workcenterid : null,
			processid : null,
			productionorderid : null,
			scrollOffsets: {
				'Day': 10,
				'Week': 2,
				'Month': 1
			},
			originalRows: null,
			data : null
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	updateProdcutionSchedule (obj) {
		this.updateLoaderFlag(true);
		let tempData = {
			actionverb : 'Save',
			data : obj
		};
		axios({
			method : 'post',
			data : tempData,
			url : '/api/productionschedule'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			if (response.data.message == 'success') {
				this.getReportData();
			}
			this.updateLoaderFlag(false);
		});
	}

	openActivityModal(item) {
		let data = null;
		this.props.resource.originalRows.forEach((scheduleitem) => {
			if (item.transactionid == scheduleitem.id) {
				data = scheduleitem;
			}
		});
		if(!item.isparent && data != null) {
			this.props.openModal({
				render: (closeModal) => {
					return <ProductionScheduleDetailsModal data={data} closeModal={closeModal} openModal={this.props.openModal} callback={(paramvalue) => {
						this.getReportData();
					}} />
				}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
			});
		}
	}

	renderPagination() {
		if(this.props.resource && Math.ceil(this.props.resource.totalcount / 10) > 1)
			return <ReactPaginate previousLabel={<span>&laquo;</span>}
				nextLabel={<span>&raquo;</span>}
				breakLabel={<a></a>}
				breakClassName={"break-me"}
				pageCount={Math.ceil(this.props.resource.totalcount / 10)}
				marginPagesDisplayed={0}
				pageRangeDisplayed={5}
				onPageChange={this.handlePaginationClick}
				containerClassName={"list-pagination"}
				subContainerClassName={"pages pagination"}
				activeClassName={"active"}
				forcePage={this.props.resource.skip} />
		return null;
	}

	handlePaginationClick(obj) {
		this.props.updateFormState(this.props.form, {
			skip : obj.selected
		});
		setTimeout(() => {
			this.getReportData();
		}, 0);
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Production Schedule Report</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">View</label>
										<Field name={'viewmode'} props={{options: ["Month", "Week", "Day"], required: true}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'View'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Production Order</label>
										<Field name={'productionorderid'} props={{resource: "productionorders", fields: "id,orderno", label: "orderno"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Work Center</label>
										<Field name={'workcenterid'} props={{resource: "workcenters", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Process</label>
										<Field name={'processid'} props={{resource: "processes", fields: "id,name"}} component={autoSelectEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							<div style={{overflow: 'auto', marginLeft: '30px'}}>
								{this.props.resource.data && this.props.resource.data.length > 0 ? <ReactGantt tasks={this.props.resource.data} viewMode={this.props.resource.viewmode} scrollOffsets={this.props.resource.scrollOffsets} onClick={this.openActivityModal} onDateChange={() => this.setState({refresh: true})}  onProgressChange={() => this.setState({refresh: true})} /> : null }
							</div>
							<div className="margintop-10 paddingleft-30">{this.renderPagination()}</div>

						</div>
					</div>
				</form>
			</>
		);
	};
}


class ProductionScheduleDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			schedule: {}
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.close = this.close.bind(this);
		this.saveSchedule = this.saveSchedule.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
	}

	componentWillMount() {
		this.state.schedule = this.props.data;
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	saveSchedule() {
		this.updateLoaderFlag(true);
		let tempData = {
			actionverb : 'Save',
			data : this.state.schedule
		};
		axios({
			method : 'post',
			data : tempData,
			url : '/api/productionschedule'
		}).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			if (response.data.message == 'success') {
				this.props.callback(response.data.main);
				this.props.closeModal();
			}
			this.updateLoaderFlag(false);
		});
	}

	inputonChange(value, field) {
		let { schedule } = this.state;
		schedule[field] = value
		this.setState({ schedule });
	}

	close() {
		this.props.closeModal();
	}

	render() {
		let { schedule } = this.state;
		return (
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title gs-text-color">Project Schedule</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-3 col-sm-3 col-xs-12">
							<label className="label-media-sm">Process</label>
						</div>
						<div className="form-group col-md-9 col-sm-9 col-xs-12">
							<AutoSelect value={schedule.processid} resource={'processes'} fields={'id,name'} disabled={true}/>
						</div>
					</div>
					<div className="row">
						<div className="col-md-3 col-sm-3 col-xs-12">
							<label className="label-media-sm">Work Center</label>
						</div>
						<div className="form-group col-md-9 col-sm-9 col-xs-12">
							<AutoSelect value={schedule.workcenterid} resource={'workcenters'} fields={'id,name'} disabled={true}/>
						</div>
					</div>
					<div className="row">
						<div className="col-md-3 col-sm-3 col-xs-12">
							<label className="label-media-sm">Operator</label>
						</div>
						<div className="form-group col-md-9 col-sm-9 col-xs-12">
							<AutoSelect value={schedule.operatorid} resource={'employees'} fields={'id,name,displayname'} label={'displayname'} filter={'employees.isoperator=true'} onChange={(value, valueobj) => this.inputonChange(value, 'operatorid')} />
						</div>
					</div>
					<div className="row">
						<div className="col-md-3 col-sm-3 col-xs-12">
							<label className="label-media-sm">Planned Start Date</label>
						</div>
						<div className="form-group col-md-9 col-sm-9 col-xs-12">
							<DateTimeElement className={`${!schedule.plannedstarttime ? 'errorinput' : ''}`}  value={schedule.plannedstarttime} onChange={(value) => this.inputonChange(value, 'plannedstarttime')} required={true}/>
						</div>
					</div>
					<div className="row">
						<div className="col-md-3 col-sm-3 col-xs-12">
							<label className="label-media-sm">Planned End Date</label>
						</div>
						<div className="form-group col-md-9 col-sm-9 col-xs-12">
							<DateTimeElement className={`${!schedule.plannedendtime ? 'errorinput' : ''}`}  value={schedule.plannedendtime} onChange={(value) => this.inputonChange(value, 'plannedendtime')} required={true}/>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="muted credit text-center">
							<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close}><i className="fa fa-times"></i>Close</button>
							<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.saveSchedule} disabled={!schedule.plannedendtime || !schedule.plannedstarttime}><i className="fa fa-check"></i>Save</button>
						</div>
					</div>
				</div>
			</>
		);
	}
}

ProductionScheduleReportForm = connect(
	(state, props) => {
		let formName = 'productionschedulereport';
		return {
			app : state.app,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ProductionScheduleReportForm));

export default ProductionScheduleReportForm;
