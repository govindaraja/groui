import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, Fields, FieldArray, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import ReactPaginate from 'react-paginate';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { InputEle, DateEle, selectAsyncEle, localSelectEle, autoSelectEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { currencyFilter, dateFilter } from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import AccountingFormQuickFillDetailsModal from '../components/details/accountingformsquickfilldetailsmodal';

import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class AccountingFormsReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.saveAccountingForms = this.saveAccountingForms.bind(this);
		this.openQuickFillModal = this.openQuickFillModal.bind(this);
		this.updateAccountingForm = this.updateAccountingForm.bind(this);
		this.renderPagination = this.renderPagination.bind(this);
		this.handlePaginationClick = this.handlePaginationClick.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {
			skip : 0,
			totalCount : 0,
			show : 'Pending'
		};

		this.props.initialize(tempObj);

		setTimeout(() => {
			this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('resultArray');

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['partnerid', 'formid', 'type', 'fromdate', 'todate', 'show'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});

		axios.get(`/api/query/accountingformsquery?${filterString.join('&')}&pagelength=10&skip=${(this.props.resource.skip * 10)}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.updateFormState(this.props.form, {
					resultArray: response.data.main,
					totalCount: response.data.count
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);

				if(this.props.resource.resultArray.length > 0)
					this.updateToggleState(false);

			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			partnerid : '', 
			formid: '',
			type: '',
			fromdate: '',
			todate: '',
			show: '',
			skip : 0,
			totalCount: 0,
			resultArray: []
		};
		this.props.updateFormState(this.props.form, tempObj);
		setTimeout(() => {
			this.getReportData();
		}, 0);
	}

	openQuickFillModal(){
		this.props.openModal({
			render: (closeModal) => {
				return <AccountingFormQuickFillDetailsModal callback={(valObj) => this.updateAccountingForm(valObj)} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	updateAccountingForm(valObj) {
		let { resultArray } = this.props.resource;
		resultArray.forEach((item) => {
			item.seriesnumber = valObj.seriesnumber;
			item.formnumber = valObj.formnumber;
			item.formdate = valObj.formdate;
		});
		this.props.updateFormState(this.props.form, { resultArray });
	}

	saveAccountingForms () {
		this.updateLoaderFlag(true);
		axios({
			method : 'post',
			data : {
				actionverb : 'Save',
				data : this.props.resource.resultArray
			},
			url : '/api/accountingforms'
		}).then((response) => {
			if (response.data.message == 'success') {
				this.getReportData();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	handlePaginationClick(obj) {
		this.props.updateFormState(this.props.form, {
			skip : obj.selected
		});
		setTimeout(() => {
			this.getReportData();
		}, 0);
	}

	renderPagination() {
		if(this.props.resource && Math.ceil(this.props.resource.totalCount / 10) > 1)
			return <ReactPaginate previousLabel={<span>&laquo;</span>}
				nextLabel={<span>&raquo;</span>}
				breakLabel={<a></a>}
				breakClassName={"break-me"}
				pageCount={Math.ceil(this.props.resource.totalCount / 10)}
				marginPagesDisplayed={0}
				pageRangeDisplayed={5}
				onPageChange={this.handlePaginationClick}
				containerClassName={"list-pagination"}
				subContainerClassName={"pages pagination"}
				activeClassName={"active"}
				forcePage={this.props.resource.skip} />
		return null;
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Forms to Issue / Receive</div>
							<div className="pull-right margintop-10">
								<button type="button" onClick={() => {
										this.openQuickFillModal()
									}} className="btn btn-width btn-sm gs-btn-outline-success"><i className="fa fa-edit"></i>Quick Fill</button>
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Form</label>
										<Field name={'salesperson'} props={{resource: "accountingformsmaster", fields: "id,formname", label: "formname", filter: "accountingformsmaster.isdeleted=false"}} component={selectAsyncEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Partner</label>
										<Field name={'partnerid'} props={{resource: "partners", fields: "id,name,displayname", label: "displayname", displaylabel: "name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Type</label>
										<Field name={'type'} props={{options: [{value: "Receive", label: "Receive"}, {value: "Issue", label: "Issue"}], label:"label", valuename: "value"}} component={localSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: [{value: "Pending", label: "Pending"}, {value: "All", label: "All"}], label:"label", valuename: "value"}} component={localSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()
										}} className="btn btn-width btn-sm gs-btn-success margintop-25"><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()
										}} className="btn btn-width btn-sm gs-btn-info margintop-25"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.resultArray} />
							{this.props.resource.resultArray && this.props.resource.resultArray.length > 0 ? <div className="row marginleft-30 margintop-20">
									<div className="col-md-12 bg-white">
										<FieldArray name={'resultArray'} resource={this.props.resource} array={this.props.array} app={this.props.app} component={accountingformTableElement} saveAccountingForms={this.saveAccountingForms} />
									</div>
									<div className="margintop-10 paddingleft-30">{this.renderPagination()}</div>
								</div> : null}
						</div>
					</div>
				</form>
			</>
		);
	};
}

class accountingformTableElement extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let resource = this.props.resource;
		return (
			<div className="table-responsive margintop-20">
				<table className="table table-bordered table-hover table-sm">
					<thead>
						<tr>
							<th className='text-center'>Partners</th>
							<th className='text-center'>Voucher Number</th>
							<th className='text-center'>Voucher Type</th>
							<th className='text-center'>Voucher Date</th>
							<th className='text-center'>Amount</th>
							<th className='text-center'>Form Name</th>
							<th className='text-center'>Type</th>
							<th className='text-center' style={{width: '12%'}}>Series Number</th>
							<th className='text-center' style={{width: '12%'}}>Form Number</th>
							<th className='text-center' style={{width: '12%'}}>Form Date</th>
						</tr>
					</thead>
					<tbody>
						{this.props.fields.map((member, index) => {
							let itemObj = eval(`try{resource.${member}}catch(e){}`);
							return (
								<tr key={index}>
									<td>{itemObj.partnername}</td>
									<td className='text-center'>{itemObj.voucherno}</td>
									<td className='text-center'>{itemObj.vouchertype}</td>
									<td className='text-center'>{dateFilter(itemObj.voucherdate)}</td>
									<td className='text-right'>{currencyFilter(itemObj.finaltotal, this.props.app.defaultCurrency, this.props.app)}</td>
									<td className='text-center'>{itemObj.formname}</td>
									<td className='text-center'>{itemObj.type}</td>
									<td>
										<Field type="text" name={`${member}.seriesnumber`} props={{}} component={InputEle} />
									</td>
									<td>
										<Field type="text" name={`${member}.formnumber`} props={{required: itemObj.seriesnumber || itemObj.formdate}} component={InputEle} validate={[stringNewValidation({required: (itemObj.seriesnumber || itemObj.formdate) ? true : false, title: 'Form Number'})]} />
									</td>
									<td>
										<Field name={`${member}.formdate`} props={{}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} />
									</td>
								</tr>
							)
						})}
					</tbody>
				</table>
				<button type="button" className="btn btn-sm gs-btn-success float-right" disabled={this.props.valid} onClick={this.props.saveAccountingForms}><i className="fa fa-save"></i>Save</button>
			</div>
		);
	}
}



AccountingFormsReportForm = connect(
	(state, props) => {
		let formName = 'accountingformsreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(AccountingFormsReportForm));

export default AccountingFormsReportForm;
