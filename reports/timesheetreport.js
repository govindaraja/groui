import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class TimesheetReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true,
			resultArray : [],
			summaryArray : [],
			summaryColumns : [{
					name : 'Employee',
					key : 'employeename',
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Date',
					key : 'seriesdate',
					format : "date",
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Submitted Hours',
					key : 'submittedhours',
					cellClass : 'text-center',
					width : 200
				}
			],
			detailsColumns : [{
					name : 'Date',
					key : 'series',
					format : 'date',
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Activity Type',
					key : 'activitytype',
					format: 'anchortag',
					transactionname : 'timesheets',
					transactionid : 'timesheetid',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Employee',
					key : 'employeename',
					cellClass : 'text-center',
					width : 150
				}, {
					name : 'Start Date',
					key : 'startdatetime',
					format : "datetime",
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'End Date',
					key : 'enddatetime',
					format : "datetime",
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Duration',
					key : 'duration',
					cellClass : 'text-center',
					width : 120
				}, {
					name : 'Remarks',
					key : 'remarks',
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Approval No',
					key : 'approvalno',
					format: 'anchortag',
					transactionname : 'timesheetapproval',
					transactionid : 'timesheetapproval_id',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Status',
					key : 'status',
					cellClass : 'text-center',
					width : 150
				}
			]
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.showOnChange = this.showOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let { summaryColumns, detailsColumns } = this.state;
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				employeeid : null,
				filters : {},
				hiddencols : [],
				columns : [],
				show : 'Summary'
			};

		customfieldAssign(detailsColumns, null, 'timesheets', this.props.app.myResources);

		this.setState({detailsColumns});

		if(!this.props.reportdata)
			this.setEmployee (tempObj, true);
		else
			this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	setEmployee (tempObj, isInit) {
		axios.get(`/api/employees?&field=id,displayname&filtercondition=employees.userid=${this.props.app.user.id}`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.length > 0)
					tempObj['employeeid'] = response.data.main[0].id;
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			if(isInit)
				this.props.initialize(tempObj);
			else
				this.props.updateFormState(this.props.form, tempObj);

			this.updateLoaderFlag(false);
		});
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];

		['fromdate', 'todate', 'employeeid'].forEach((item) => {
			if(this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/gettimesheetdetailsquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.summaryArray.length; i++) {
					for(var prop in response.data.summaryArray[i]) {
						response.data.summaryArray[i][prop] = response.data.summaryArray[i][prop] == null ? "" : response.data.summaryArray[i][prop];
					}
				}
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: this.props.resource.show == 'Summary' ? response.data.summaryArray : response.data.main,
					columns: this.props.resource.show == 'Summary' ? this.state.summaryColumns : this.state.detailsColumns,
					hiddencols
				});

				this.setState({
					summaryArray : response.data.summaryArray,
					resultArray : response.data.main
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let { summaryArray, resultArray } = this.state;

		summaryArray = [];
		resultArray = [];

		this.setState({ summaryArray, resultArray });

		let tempObj = {
			fromdate : null,
			todate : null,
			show : null,
			employeeid : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};

		this.setEmployee (tempObj, false);
	}

	showOnChange() {
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('columns');

		let tempObj = {
			originalRows: this.props.resource.show == 'Summary' ? this.state.summaryArray : this.state.resultArray,
			rows: this.props.resource.show == 'Summary' ? this.state.summaryArray : this.state.resultArray,
			columns: this.props.resource.show == 'Summary' ? this.state.summaryColumns : this.state.detailsColumns,
			filters: {}
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Timesheet Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required:  true, title : 'From Date'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									{this.props.app.user.roleid.indexOf(18) == -1 ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Employee</label>
										<Field name={'employeeid'} props={{resource: "employees", fields: "id,displayname", label: "displayname"}} component={autoSelectEle} />
									</div> : null}
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: ["Summary", "Details"], required: true, onChange: () => {this.showOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Show'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname={`${this.props.resource.show == 'Summary' ? 'Timesheet Report - Summary' : 'Timesheet Report - Details'}`} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : <div className={`marginTop-5 align-self-center text-center ${this.props.reportdata ? 'show' : 'hide'}`}>
								<i>No Data found !!!</i>
							</div> }
						</div>
					</div>
				</form>
			</>
		);
	};
}

TimesheetReportForm = connect(
	(state, props) => {
		let formName = 'timesheetreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(TimesheetReportForm));

export default TimesheetReportForm;
