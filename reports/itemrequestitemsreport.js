import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, BIReportSelectField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class ItemRequestItemsReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true,
			itemrequestItemColumns: [{
				"name" : "Item Request No",
				"key" : "itemrequestno",
				"format" : "anchortag",
				"transactionname" : "itemrequests",
				"transactionid" : "id",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Item Request Date",
				"key" : "itemrequestdate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Item Request For",
				"key" : "itemrequestfor",
				"cellClass" : "text-center",
				"width" : 200
			}, {
				"name" : "Transaction No",
				"key" : "transactiondetails",
				"format": "anchortag",
				"transactionname" : "relatedresource",
				"transactionid" : "relatedid",
				"cellClass" : "text-center",
				"width" : 200
			}, {
				"name" : "Customer",
				"key" : "customer",
				"width" : 250
			}, {
				"name" : "Engineer",
				"key" : "engineer",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Item Name",
				"key" : "itemname",
				"cellClass" : "text-center",
				"width" : 250
			}, {
				"name" : "Quantity",
				"key" : "quantity",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "UOM",
				"key" : "uom",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Description",
				"key" : "description",
				"width" : 200
			}, {
				"name" : "Warehouse",
				"key" : "warehouseid_name",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Required Date",
				"key" : "requireddate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Item Category",
				"key" : "itemcategorymasterid_name",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Item Group",
				"key" : "itemgroupid_fullname",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Customer Group",
				"key" : "customergroupid_name",
				"cellClass" : "text-center",
				"width" : 160
			}, {
				"name" : "Delivered Quantity",
				"key" : "deliveredqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Returned Quantity",
				"key" : "returnedqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Consumed Quantity",
				"key" : "usedqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Cost Rate",
				"key" : "costrate",
				"format" : "currency",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Cost Value",
				"key" : "costvalue",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Free/Chargeable",
				"key" : "freeorchargeable",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Remarks",
				"key" : "requestitems_remarks",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Status",
				"key" : "status",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Sales Person",
				"key" : "salesperson",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Numbering Series",
				"key" : "numberingseriesmasterid_name",
				"width" : 180
			}, {
				"name" : "Service Call Type",
				"key" : "calltype",
				"width" : 180
			}]
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		let { itemrequestItemColumns } = this.state;

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				show : 'All',
				status : ["Approved"],
				filters: {},
				hiddencols: [],
				columns: []
			};

		customfieldAssign(itemrequestItemColumns, null, 'itemrequests', this.props.app.myResources);
		customfieldAssign(itemrequestItemColumns, null, 'itemrequestitems', this.props.app.myResources);

		this.setState({itemrequestItemColumns});

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let { itemrequestItemColumns } = this.state;

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['companyid', 'fromdate', 'todate', 'status', 'customerid', 'itemid', 'itemcategoryid', 'itemgroupid', 'customergroupid', 'engineerid', 'numberingseriesmasterid', 'show'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/itemrequestitemsreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns: itemrequestItemColumns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			status : null,
			show : null,
			customergroupid : null,
			itemid : null,
			itemgroupid : null,
			itemcategoryid : null,
			customerid : null,
			numberingseriesmasterid : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		if(transactionname == 'relatedresource') {
			transactionname = data.servicecallid ? 'servicecalls' : (data.projectid ? 'projects' : 'productionorders');
			transactionid = data.servicecallid ? 'servicecallid' : (data.projectid ? 'projectid' : 'productionorderid');
		}
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Item Request Items Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Status</label>
										<Field name={'status'} props={{options: ["Draft", "Submitted", "Revise", "Approved", "Cancelled"], multiselect: true}} component={localSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer</label>
										<Field name={'customerid'} props={{resource: "partners", fields: "id,name", label: "name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Name</label>
										<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Group</label>
										<Field name={'itemgroupid'} props={{resource: "itemgroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer Group</label>
										<Field name={'customergroupid'} props={{resource: "customergroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Engineer</label>
										<Field name={'engineerid'} props={{resource: "users", fields: "id,displayname", label: "displayname", filter: "users.isengineer=true"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Numbering Series</label>
										<Field name={'numberingseriesmasterid'} props={{resource: "numberingseriesmaster", fields: "id,name,format,isdefault,currentvalue", filter: "numberingseriesmaster.resource='itemrequests'"}} component={selectAsyncEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: ["All", "Pending Delivery"], required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Show'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Item Request Items Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

ItemRequestItemsReportForm = connect(
	(state, props) => {
		let formName = 'itemrequestitemsreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ItemRequestItemsReportForm));

export default ItemRequestItemsReportForm;
