import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import { updateFormState, updateReportFilter } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, checkboxEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter, currencyFilter, timeFilter, dateAgoFilter, timeDurationFilter } from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import async from 'async';
import MapProps from '../components/map-properties';
import RenderInfoWindow from '../components/details/mapinfowindow';

class EmployeeLocationsReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true,
			showIntervals : false,
			markers : [],
			customermarkers : [],
			waypoints : [],
			showallmarkers : false,
			loginlatlngarray: [],
			logoutlatlngarray: [],
			loginmarker: [],
			logoutmarker: [],
			errMsg : [],
			overallTimelineArray: [],
			activityTimelineCheckIn: [],
			activityTimelineCheckOut: [],
			servicereportTimelineCheckIn: [],
			servicereportTimelineCheckOut: [],
			activityTimelineCheckInMarker: [],
			activityTimelineCheckOutMarker: [],
			servicereportTimelineCheckInMarker: [],
			servicereportTimelineCheckOutMarker: [],
			showoverview: false,
			userObj: {}
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.onLoadCustomers = this.onLoadCustomers.bind(this);
		this.initializeMap = this.initializeMap.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		//this.checkboxOnChange = this.checkboxOnChange.bind(this);
		//this.attenCheckinoutFn = this.attenCheckinoutFn.bind(this);
		this.markerListenerFn = this.markerListenerFn.bind(this);
		this.renderInfoWindowFn = this.renderInfoWindowFn.bind(this);
		this.activityTimelineMarkers = this.activityTimelineMarkers.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		setTimeout(this.onLoad, 2000);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				locationdate : new Date(new Date().setHours(0, 0, 0, 0)),
				totalDistanceTravelled : 0,
				usersCurrentLocations : [],
				directionDisplayArray : [],
				customerLocations : [],
				employeeLocations : [],
				locationOffArray : []
			}

		this.setState({
			map : new google.maps.Map(document.getElementById('map'),MapProps.MapOptions),
			//directionsService : new google.maps.DirectionsService,
			directionsService : [],
			stepDisplay : new google.maps.InfoWindow
		}, () => {
			this.props.initialize(tempObj);
		});

		setTimeout(() => {
			this.getReportData();
		}, 0);

		this.updateLoaderFlag(false);
	}

	/*checkboxOnChange(value) {
		let { showallmarkers } = this.state;

		showallmarkers = value;

		this.setState({showallmarkers}, ()=> {
			this.initializeMap();
		});
	}

	attenCheckinoutFn () {
		this.updateLoaderFlag(true);

		let { showAttenCheckinout, loginlatlngarray, logoutlatlngarray, errMsg } = this.state;

		loginlatlngarray = [];
		logoutlatlngarray = [];
		errMsg = "";

		if (showAttenCheckinout) {

			let filterArray = [];

			if(this.props.resource.userid)
				filterArray.push(`userid=${this.props.resource.userid}`);

			if(this.props.resource.locationdate)
				filterArray.push(`locationdate=${this.props.resource.locationdate}`);

			filterArray.push(`attendancelatlng=true`)

			axios.get(`/api/query/employeelocationsquery?${filterArray.join('&')}`).then((response) => {
				if (response.data.message == 'success') {
					loginlatlngarray = response.data.loginlatlngarray;
					logoutlatlngarray = response.data.logoutlatlngarray;

					if(loginlatlngarray.length == 0)
						errMsg = "There is no attendance login result for this date";

					if(logoutlatlngarray.length == 0)
						errMsg = "There is no attendance logout result for this date";


					this.setState({
						errMsg,
						loginlatlngarray,
						logoutlatlngarray
					}, () => {
						this.initializeMap();
					});
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}

				this.updateLoaderFlag(false);
			});
		} else {
			this.setState({
				errMsg,
				loginlatlngarray,
				logoutlatlngarray
			}, () => {
				this.initializeMap();
			});
		}
	}*/

	getReportData () {
		this.updateLoaderFlag(true);

		let locationOffArray = [],
			usersCurrentLocations = [],
			totalDistanceTravelled = 0,
			employeeLocations = [],
			directionDisplayArray = this.props.resource.directionDisplayArray;

		let { errMsg, waypoints, markers, map, loginmarker, logoutmarker, overallTimelineArray, activityTimelineCheckIn, activityTimelineCheckOut, servicereportTimelineCheckIn, servicereportTimelineCheckOut, activityTimelineCheckInMarker, activityTimelineCheckOutMarker, servicereportTimelineCheckInMarker, servicereportTimelineCheckOutMarker, loginlatlngarray, logoutlatlngarray, userObj } = this.state;

		markers.forEach((item) => {
			item.setMap(null);
		});

		loginmarker.forEach((item) => {
			item.setMap(null);
		});

		logoutmarker.forEach((item) => {
			item.setMap(null);
		});

		['activityTimelineCheckInMarker', 'activityTimelineCheckOutMarker', 'servicereportTimelineCheckInMarker', 'servicereportTimelineCheckOutMarker'].forEach((stateProp) => {
			this.state[stateProp].forEach((item) => {
				item.setMap(null);
			});
		});


		loginmarker = [];
		logoutmarker = [];
		markers = [];
		waypoints = [];
		overallTimelineArray = [],
		activityTimelineCheckIn = [];
		activityTimelineCheckOut = [];
		servicereportTimelineCheckIn = [];
		servicereportTimelineCheckOut = [];
		loginlatlngarray = [];
		logoutlatlngarray = [];

		activityTimelineCheckInMarker = [];
		activityTimelineCheckOutMarker = [];
		servicereportTimelineCheckInMarker = [];
		servicereportTimelineCheckOutMarker = [];

		this.props.resource.directionDisplayArray.forEach((item) => {
			item.setMap(null);
		});

		directionDisplayArray = [];
		errMsg = "";

		let filterArray = [`locationdate=${this.props.resource.locationdate}`];

		if (this.props.resource.userid)
			filterArray.push(`userid=${this.props.resource.userid}`, `isUserFlag=true`);

		axios.get(`/api/query/employeelocationsquery?${filterArray.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				employeeLocations = response.data.main;

				if (this.props.resource.userid) {
					overallTimelineArray = response.data.overallTimelineArray;
					activityTimelineCheckIn = response.data.activityTimelineCheckIn;
					activityTimelineCheckOut = response.data.activityTimelineCheckOut;
					servicereportTimelineCheckIn = response.data.servicereportTimelineCheckIn;
					servicereportTimelineCheckOut = response.data.servicereportTimelineCheckOut;
					loginlatlngarray = response.data.loginlatlngarray;
					logoutlatlngarray = response.data.logoutlatlngarray;
				}

				employeeLocations.sort(function(a, b) {
					return (new Date(a.locationdate).getTime() < new Date(b.locationdate).getTime()) ? -1 : (new Date(a.locationdate).getTime() > new Date(b.locationdate).getTime()) ? 1 : 0;
				});

				if(employeeLocations.length == 0) {
					if(!this.props.resource.userid && this.props.resource.locationdate)
						errMsg = "There is no result for this date";
					else if(this.props.resource.userid && userObj && userObj.trackerstatus == 'Allowed' && this.props.resource.locationdate)
						errMsg = "There is no location for this employee in this date";
				} else {
					if(this.props.resource.userid) {
						employeeLocations.forEach((item) => {
							if(item.islocationon == false)
								locationOffArray.push(timeFilter(item.locationdate));
						});
					}
				}

				this.setState({
					errMsg,
					waypoints,
					markers,
					loginmarker,
					logoutmarker,
					loginlatlngarray,
					logoutlatlngarray,
					overallTimelineArray,
					activityTimelineCheckIn,
					activityTimelineCheckOut,
					servicereportTimelineCheckIn,
					servicereportTimelineCheckOut,
					activityTimelineCheckInMarker,
					activityTimelineCheckOutMarker,
					servicereportTimelineCheckInMarker,
					servicereportTimelineCheckOutMarker
				});

				this.props.updateFormState(this.props.form, {
					employeeLocations,
					customerLocations: [],
					locationOffArray,
					directionDisplayArray,
					usersCurrentLocations,
					totalDistanceTravelled
				});

				this.props.updateReportFilter('employeelocationsreport', this.props.resource);

				if(employeeLocations.length > 0)
					this.updateToggleState(false);

				if(this.props.resource.showcustomerlocation) {
					setTimeout(this.onLoadCustomers, 0);
				} else {
					this.updateToggleState(false);
					this.initializeMap();
				}
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	onLoadCustomers () {
		this.updateLoaderFlag(true);
		let { errMsg } = this.state;

		let customerLocations = [];

		axios.get(`/api/query/customermapquery`).then((response) => {
			if (response.data.message == 'success') {
				if (this.props.resource.customerid)
					response.data.main.map((item) => {
						if(item.id == this.props.resource.customerid)
							customerLocations.push(item)
					});
				else
					customerLocations = response.data.main;

				if(customerLocations.length > 0) {
					customerLocations.map((item) => {
						if (item.paymentduedate) {
							let currentdate = new Date().getDate();
							let duedate = new Date(item.paymentduedate).getDate();

							if (currentdate - duedate >= 90)
								item.className = "above90";

							if (currentdate - duedate >= 30)
								item.className = "above30";

							if (currentdate - duedate <= 30)
								item.className = "below30";

						} else
							item.className = "";


						if (item.orderdate) {
							let currentorderdate = new Date().getDate();
							let orderdate = new Date(item.orderdate).getDate();

							if (currentorderdate - orderdate >= 180)
								item.orderClass = "above180";

							if (currentorderdate - orderdate >= 90)
								item.orderClass = "above90";

							if (currentorderdate - orderdate <= 30)
								item.orderClass = "below30";

						} else
							item.orderClass = '';


						if (item.nextfollowup) {
							let currentfollowupdate = new Date().getDate();
							let followupdate = new Date(item.nextfollowup).getDate();

							if (currentfollowupdate - followupdate >= 90)
								item.followupClass = "above90";

							if (currentfollowupdate - followupdate >= 30)
								item.followupClass = "above30";

							if (currentfollowupdate - followupdate <= 30)
								item.followupClass = "below30";

						} else
							item.followupClass = '';
					});
				} else {
					if(this.props.resource.customerid)
						errMsg = 'No data found for this search';
				}

				this.setState({ errMsg });

				this.props.updateFormState(this.props.form, {
					customerLocations
				});

				if(customerLocations.length > 0 && this.state.filterToggleOpen)
					this.updateToggleState(false);

				this.initializeMap();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	renderInfoWindowFn (item, param) {
		return RenderInfoWindow(item, param, 'Employee Locations Report', this.props.app);;
	}

	initializeMap () {
		this.updateLoaderFlag(true);

		let { errMsg, waypoints, markers, customermarkers, map, directionsService, stepDisplay, showallmarkers, markerCluster, loginlatlngarray, logoutlatlngarray, loginmarker, logoutmarker, activityTimelineCheckIn, activityTimelineCheckOut, servicereportTimelineCheckIn, servicereportTimelineCheckOut, activityTimelineCheckInMarker, activityTimelineCheckOutMarker, servicereportTimelineCheckInMarker, servicereportTimelineCheckOutMarker, overallTimelineArray } = this.state,
			totalDistanceTravelled = 0,
			usersCurrentLocations = [],
			directionDisplayArray = [],
			marker_ids = [];

		let polylineDisplayArray = [],
			lineArray = [];

		markers.forEach((item) => {
			item.setMap(null);
		});

		customermarkers.forEach((item) => {
			item.setMap(null);
		});

		loginmarker.forEach((item) => {
			item.setMap(null);
		});

		logoutmarker.forEach((item) => {
			item.setMap(null);
		});

		['activityTimelineCheckInMarker', 'activityTimelineCheckOutMarker', 'servicereportTimelineCheckInMarker', 'servicereportTimelineCheckOutMarker'].forEach((stateProp) => {
			this.state[stateProp].forEach((item) => {
				item.setMap(null);
			});
		});

		if (directionsService.length > 0)
			directionsService.forEach((item) => {
				item.setMap(null);
			});

		if(markerCluster)
			markerCluster.clearMarkers();

		this.props.resource.directionDisplayArray.forEach((item) => {
			item.setMap(null);
		});

		markers = [];
		waypoints = [];
		customermarkers = [];
		loginmarker = [];
		logoutmarker = [];
		directionsService = [];

		activityTimelineCheckInMarker = [];
		activityTimelineCheckOutMarker = [];
		servicereportTimelineCheckInMarker = [];
		servicereportTimelineCheckOutMarker = [];

		this.setState({
			markers,
			customermarkers,
			waypoints,
			loginmarker,
			logoutmarker,
			directionsService,
			activityTimelineCheckInMarker,
			activityTimelineCheckOutMarker,
			servicereportTimelineCheckInMarker,
			servicereportTimelineCheckOutMarker
		});

		let bounds = new google.maps.LatLngBounds();

		this.props.resource.employeeLocations.forEach((item, i) => {
			if (item.latitude != null && item.longitude != null) {
				let mapIcon = this.props.resource.userid ? MapProps.getIcons().dcRouteIcon : MapProps.getIcons().salespersonIcon;

				let position = {
					lat : item.latitude,
					lng : item.longitude
				};

				let marker = new google.maps.Marker({
					position : position,
					map : map,
					titleinfo : this.renderInfoWindowFn(item, 'employeeLocations'),
					locationdate : new Date(item.locationdate),
					animation : google.maps.Animation.DROP,
					icon : mapIcon,
					id : i,
					isUser : true,
					locationobj: item.locationobj ? item.locationobj : {
						latitude: item.latitude,
						longitude: item.longitude,
						speed: 0,
						accuracy: 0,
						locationdate: new Date(item.locationdate)
					},
					trackerfrequency: item.userid_trackerfrequency,
					latlng: position
				});

				markers.push(marker);

				if(!this.props.resource.userid) {
					usersCurrentLocations.push({
						position : position,
						titleinfo : this.renderInfoWindowFn(item, 'employeeLocations'),
						locationdate : new Date(item.locationdate),
						id : i
					});
				}

				bounds.extend(marker.position);

				this.markerListenerFn(marker, i);

				/*if(this.props.resource.userid) {
					waypoints.push({
						location : position,
						stopover: true,
						id : i
					})
				}*/

				if(this.props.resource.userid) {
					waypoints.push(position);
				}
			}
		});

		if (loginlatlngarray.length > 0 || logoutlatlngarray.length > 0) {

			loginlatlngarray.forEach((item, i) => {

				let marker = new google.maps.Marker({
					position : item.position,
					map : map,
					titleinfo : this.renderInfoWindowFn(item, 'loginlatlngarray'),
					locationdate : new Date(item.locationdate),
					animation : google.maps.Animation.DROP,
					icon : MapProps.getIcons().attendanceCheckInIcon,
					id : i,
					isUser : true
				});

				marker.setLabel({
					text: timeFilter(item.intime).toString(),
					color: '#3b99ff',
					fontWeight: 'bold',
					fontSize: '13px'
				});

				loginmarker.push(marker);

				bounds.extend(marker.position);

				this.markerListenerFn(marker, i);
			});

			logoutlatlngarray.forEach((item, i) => {
				let marker = new google.maps.Marker({
					position : item.position,
					map : map,
					titleinfo : this.renderInfoWindowFn(item, 'logoutlatlngarray'),
					locationdate : new Date(item.locationdate),
					animation : google.maps.Animation.DROP,
					icon : MapProps.getIcons().attendanceCheckOutIcon,
					id : i,
					isUser : true
				});

				marker.setLabel({
					text: timeFilter(item.outtime).toString(),
					color: '#3b99ff',
					fontWeight: 'bold',
					fontSize: '13px'
				});

				logoutmarker.push(marker);

				bounds.extend(marker.position);

				this.markerListenerFn(marker, i);
			});
		} else
			this.updateLoaderFlag(false);

		this.props.resource.customerLocations.forEach((item, i) => {
			if (item.latitude != null && item.longitude != null) {
				let position = {
					lat : item.latitude,
					lng : item.longitude
				};

				let marker = new google.maps.Marker({
					position: position,
					map: map,
					titleinfo : this.renderInfoWindowFn(item, 'customerLocations'),
					animation: google.maps.Animation.DROP,
					icon : MapProps.getIcons().customerIcon,
					id : i,
					isUser : false
				});

				customermarkers.push(marker);

				bounds.extend(marker.position);

				this.markerListenerFn(marker, i);
			}
		});

		let timeLineArray = [];

		if (activityTimelineCheckIn.length > 0)
			this.activityTimelineMarkers(activityTimelineCheckIn, activityTimelineCheckInMarker, 'activityTimelineCheckIn', map, bounds, true, timeLineArray);

		if (activityTimelineCheckOut.length > 0)
			this.activityTimelineMarkers(activityTimelineCheckOut, activityTimelineCheckOutMarker, 'activityTimelineCheckOut', map, bounds, false, timeLineArray);

		if (servicereportTimelineCheckIn.length > 0)
			this.activityTimelineMarkers(servicereportTimelineCheckIn, servicereportTimelineCheckInMarker, 'servicereportTimelineCheckIn', map, bounds, true, timeLineArray);

		if (servicereportTimelineCheckOut.length > 0)
			this.activityTimelineMarkers(servicereportTimelineCheckOut, servicereportTimelineCheckOutMarker, 'servicereportTimelineCheckOut', map, bounds, false, timeLineArray);

		timeLineArray.sort((a, b) => {
			return (new Date(a.locationdate).getTime() < new Date(b.locationdate).getTime()) ? -1 : (new Date(a.locationdate).getTime() > new Date(b.locationdate).getTime()) ? 1 : 0;
		});

		let validPoints = [],
			totalDistance = 0,
			isTrackerUser = false;

		if(this.props.resource.userid && waypoints.length > 0) {
			markers.map((item) => {
				item.setMap(null);
			});

			let finalMarkersArray = [markers[0]],
				MarkersObj = {},
				markerIndex = 0,
				xx = false,
				frequencyValues = {
					'High': 10,
					'Medium': 25,
					'Low': 100
				},
				markersArray = [];

			markers.forEach((item) => {
				let accuracy = item.locationobj.accuracy ? item.locationobj.accuracy : 0;

				if (accuracy <= 100)
					markersArray.push(item);
			});

			markersArray = (markersArray.length == 0) ? [markers[markers.length-1]] : markersArray;

			function checkUserAccuracy (markersArray) {
				return markersArray.locationobj.accuracy ? markersArray.locationobj.accuracy : 0;
			};

			function checkUserFrequencyDistance (distance, duration, userFrequency) {
				// Actual Distance travelled is more than 3 times of user tracker frequency than the point represented as dotted lines

				let isDotted = false;
				let meterpersecond = 0.8; //Assumed value 

				if ((distance > 3 * userFrequency) && distance <= (duration * meterpersecond))
					isDotted = true;

				/*if (distance > 3 * userFrequency) {
					isDotted = true;
				} else {
					let meterpersecond = 0.8; //Assumed value 

					if (distance <= (duration * meterpersecond))
						isDotted = true;
				}*/

				return isDotted;
			};

			for (let i = 0; i < markersArray.length; i++) {
				let distance = validPoints.length > 0 ? getDistance(validPoints[validPoints.length-1].latlng, markersArray[i].latlng) : (i != 0 ? getDistance(markersArray[i-1].latlng, markersArray[i].latlng) : 0);

				let duration = validPoints.length > 0 ? timeDiffinsecondsFn(validPoints[validPoints.length-1].locationdate, markersArray[i].locationdate) : (i != 0 ? timeDiffinsecondsFn(markersArray[i-1].locationdate, markersArray[i].locationdate) : 0);

				let distancepersecond = duration * markersArray[i].locationobj.speed;

				let userFrequency =  frequencyValues[markersArray[i].trackerfrequency];

				let isDottedLines = checkUserFrequencyDistance(distance, duration, userFrequency);

				let accuracy = checkUserAccuracy(markersArray[i]);

				let assumedSpeed = 83.5; // (i.e 300 KMPH)

				if (i == 0 || (distance > accuracy)) {
					let nextPoint = (i != markersArray.length-1) ? markersArray[i+1] : {};

					let previousPointIsValid = (i == 0) ? true : false;
					let nextPointIsValid = (i == markersArray.length-1) ? true : false;

					if ((duration * assumedSpeed) > distance)
						previousPointIsValid = true;

					if (Object.keys(nextPoint).length > 0) {
						let nextPointDistance = getDistance(nextPoint.latlng, markersArray[i].latlng);
						let nextPointDuration = timeDiffinsecondsFn(nextPoint.locationdate, markersArray[i].locationdate);

						if (i == 0 && (nextPointDuration * assumedSpeed) >= nextPointDistance)
							nextPointIsValid = true;

						if (i != 0 && (nextPointDuration * assumedSpeed) > nextPointDistance)
							nextPointIsValid = true;
					}

					if (previousPointIsValid && nextPointIsValid)
						validPoints.push({
							distance,
							duration,
							distancepersecond,
							userFrequency,
							isDottedLines,
							accuracy,
							...markersArray[i]
						});
				}
			}

			isTrackerUser = validPoints.length > 0 ? true : false;

			validPoints = (validPoints.length == 0) ? [markersArray[markersArray.length-1]] : validPoints;
		}

		if (validPoints.length == 0)
			validPoints = timeLineArray.length > 0 ? timeLineArray : validPoints;

		if (validPoints.length > 0) {
			let tempFalsePoints = [validPoints[0]],
				tempTruePoints = [];

			if (validPoints.length > 1) {
				for (let i = 1; i < validPoints.length; i++) {
					//totalDistance += validPoints[i].distance; Calculate Distance

					if (validPoints[i].isDottedLines) {
						if (tempFalsePoints.length > 0) {
							polylineDisplayArray.push({
								array: tempFalsePoints.map((item) => {return item.latlng}),
								isdotted: false
							});

							tempFalsePoints = [];

							tempTruePoints.push(validPoints[i-1]);
						}

						tempTruePoints.push(validPoints[i]);

						if (i == validPoints.length-1) {
							polylineDisplayArray.push({
								array: tempTruePoints.map((item) => {return item.latlng}),
								isdotted: true
							});
						}
					} else {
						if (tempTruePoints.length > 0) {
							polylineDisplayArray.push({
								array: tempTruePoints.map((item) => {return item.latlng}),
								isdotted: true
							});

							tempTruePoints = [];

							tempFalsePoints.push(validPoints[i-1]);
						}

						tempFalsePoints.push(validPoints[i]);

						if (i == validPoints.length-1) {
							polylineDisplayArray.push({
								array: tempFalsePoints.map((item) => {return item.latlng}),
								isdotted: false
							});
						}
					}
				}
			} else {
				if (timeLineArray.length > 0)
					polylineDisplayArray.push({
						array: timeLineArray.map((item) => {return item.latlng}),
						isdotted: true
					});
				else
					polylineDisplayArray.push({
						array: tempFalsePoints.map((item) => {return item.latlng}),
						isdotted: true
					});
			}

			if (polylineDisplayArray.length > 1 || (polylineDisplayArray.length == 1 && polylineDisplayArray[0].array.length > 1)) {
				polylineDisplayArray.forEach((item) => {
					let lineSymbol = {
						path: 'M 0,-1 0,1',
						strokeOpacity: 1,
						scale: 4
					};

					let dottedPolyOptions = {
						strokeOpacity: 0,
						icons: [{
							icon: lineSymbol,
							offset: '0',
							repeat: '20px'
						}]
					};

					let polyLineObj = new google.maps.Polyline({
						path: item.array,
						geodesic: true,
						strokeColor: '#3F4B6D',
						//strokeOpacity: 4.0,
						//strokeWeight: 2
					});

					if (item.isdotted)
						polyLineObj.setOptions(dottedPolyOptions);

					polyLineObj.setMap(map);

					directionsService.push(polyLineObj);
				});
			} else {
				zoomToObject([validPoints[0].position]);

				if (isTrackerUser)
					routeMap([validPoints[0].id]);
			}

			if (directionsService.length > 0) {
				totalDistance = 0;
				lineArray = [];

				directionsService.forEach((item) => {
					totalDistance += google.maps.geometry.spherical.computeLength(item.getPath());

					lineArray = [...lineArray, ...item.getPath().getArray()]
				});

				zoomToObject(lineArray);

				if (isTrackerUser)
					routeMap(validPoints);
			}

			if (isTrackerUser)
				this.props.updateFormState(this.props.form, {
					usersCurrentLocations,
					directionDisplayArray,
					totalDistanceTravelled: (Number((totalDistance/1000).toFixed(2)))
				});
		}

		function zoomToObject(array){
			let bounds = new google.maps.LatLngBounds();
			let points = array; //obj.getPath().getArray();

			for (let n = 0; n < points.length ; n++){
				bounds.extend(points[n]);
			}

			map.fitBounds(bounds);
		};

		function routeMap (validPoints) {
			let labelIndex = 0;
			let showMarkers = [];
			let validIds = validPoints.map((item) => {return item.id});

			for (let i = 0; i < markers.length; i++) {
				if (showMarkers.length == 0) {
					if (validIds.includes(markers[i].id))
						showMarkers.push(markers[i]);
				} else {
					if (validIds[validIds.length-1] != markers[i].id) {
						if (validIds.includes(markers[i].id)) {
							if (timeDiffFn(showMarkers[showMarkers.length-1].locationdate, markers[i].locationdate))
								showMarkers.push(markers[i]);
						}
					} else {
						showMarkers.push(markers[i]);
						break;
					}
				}
			}

			if (showMarkers.length == 0)
				showMarkers = [markers[0]];

			for (let i = 0; i < showMarkers.length; i++) {
				let marker = showMarkers[i];

				if (showMarkers[i].isUser) {
					labelIndex = labelIndex+1;
					marker.setLabel({
						text: (labelIndex).toString(),
						color: 'white',
						fontWeight: 'bold',
						fontSize: '13px'
					});
				}

				marker.setMap(map);
			}
		};

		function timeDiffFn(a ,b) {
			let timeDiff = Math.abs(new Date(a).getTime() - new Date(b).getTime());
			let diffDays = Math.ceil(timeDiff / (1000*60));

			return diffDays >= 10 ? true : false;
		};

		function timeDiffinsecondsFn(a ,b) {
			var timeDiff = Math.abs(new Date(a).getTime() - new Date(b).getTime());
			var diffDays = Math.ceil(timeDiff / (1000));

			return diffDays;
		};

		/*if(this.props.resource.userid && waypoints.length > 0) {
			markers.map((item) => {
				item.setMap(null);
			});

			let tempWayPoints = [], FinalWayPoints = [], x = [], mp = 0;

			if(waypoints.length > 1) {
				for(let i = 1; i < waypoints.length; i++) {
					if(getDistance(waypoints[i-1].location,waypoints[i].location) > 50) {
						let idFound = false;

						for(let j = 0; j < x.length; j++) {
							if(x[j].id == waypoints[i-1].id)
								idFound = true;
						}

						if(!idFound)
							x.push(waypoints[i-1]);

						x.push(waypoints[i]);

						mp = i;
					} else if (getDistance(waypoints[mp].location,waypoints[i].location) > 50) {
						let idFound = false;

						for(let j = 0; j < x.length; j++) {
							if(x[j].id == waypoints[mp].id)
								idFound = true;
						}

						if(!idFound)
							x.push(waypoints[mp]);

						x.push(waypoints[i]);

						mp = i;
					}
				}

				if(x.length == 0)
					x.push(waypoints[waypoints.length-1]);
			} else
				x = waypoints;

			if(x.length > 0) {

				x.forEach((v) => { marker_ids.push(v.id); delete v.id });

				let i = 0;

				while(i < x.length) {
					if(i < 23) {
						tempWayPoints.push(x[i]);

						if(i == (x.length-1)) {
							FinalWayPoints.push({
								origin : tempWayPoints[0].location,
								destination : tempWayPoints[tempWayPoints.length-1].location,
								waypoints : tempWayPoints
							});
						}
					} else {
						FinalWayPoints.push({
							origin : tempWayPoints[0].location,
							destination : tempWayPoints[tempWayPoints.length-1].location,
							waypoints : tempWayPoints
						});

						tempWayPoints = [];

						i = 0;
						x.splice(0,22);

						tempWayPoints.push(x[i]);

						if(i == (x.length-1)) {
							FinalWayPoints.push({
								origin : tempWayPoints[0].location,
								destination : tempWayPoints[tempWayPoints.length-1].location,
								waypoints : tempWayPoints
							});
						}
					}

					i++;
				};

			}

			async.eachSeries(FinalWayPoints, (waypoint, eachCB) => {
				directionsService.route({
					origin: waypoint.origin,
					destination: waypoint.destination,
					travelMode: 'DRIVING',
					waypoints : waypoint.waypoints,
					optimizeWaypoints: false,
					provideRouteAlternatives : true
				}, (response, status)=> {
					if (status === 'OK') {
						routeMap(response);
						eachCB(null);
					} else
						console.log('Directions request failed due to ' + status);
				});
			}, () => {
				this.props.updateFormState(this.props.form, {
					usersCurrentLocations,
					directionDisplayArray,
					totalDistanceTravelled
				});

				this.updateLoaderFlag(false);
			});
		}

		function routeMap (response) {
			let labelIndex = 0;
			let totalDistance = 0;

			let directionsDisplay = new google.maps.DirectionsRenderer({
				map: map,
				suppressMarkers: true,
				polylineOptions : {
					strokeColor : '#3F4B6D'
				}
				//preserveViewport : true	-the viewport is left unchanged, unless the map's center and zoom were never set.
			});

			directionsDisplay.setDirections(response);
			directionDisplayArray.push(directionsDisplay);

			if(showallmarkers) {
				for(let i = 0; i < markers.length; i++) {
					let marker = markers[i];

					if(markers[i].isUser) {
						labelIndex=labelIndex+1;
						marker.setLabel({
							text : (labelIndex).toString(),
							color : 'white',
							fontWeight: 'bold',
							fontSize: '13px'
						});
					}
					marker.setMap(map);
				}
			} else {
				for(let i = 0; i < markers.length; i++) {
					let marker = markers[i];

					if(markers[i].isUser && marker_ids.indexOf(markers[i].id) >= 0) {
						labelIndex=labelIndex+1;
						marker.setLabel({
							text : (labelIndex).toString(),
							color : 'white',
							fontWeight: 'bold',
							fontSize: '13px'
						});
						marker.setMap(map);
					} else if(!markers[i].isUser)
						marker.setMap(map);
				}
			}

			for(let i = 0; i < response.routes[0].legs.length; i++) {
				let tempObj = {
					start_address : response.routes[0].legs[i].start_address,
					end_address : response.routes[0].legs[i].end_address,
					duration : response.routes[0].legs[i].duration.text,
					distance : response.routes[0].legs[i].distance.text,
					duration_value : response.routes[0].legs[i].duration.value,
					distance_value : (i==0 || i==(response.routes[0].legs.length-1)) ? 0 : response.routes[0].legs[i].distance.value,
					slno : i+1
				};

				totalDistance += tempObj.distance_value;
			}

			totalDistanceTravelled += totalDistance/1000;
			totalDistanceTravelled = Number((totalDistanceTravelled).toFixed(2));
		}*/

		// Calculate distance between two points in google maps V3
		// https://stackoverflow.com/questions/1502590/calculate-distance-between-two-points-in-google-maps-v3
		// If you want to calculate it yourself, then you can use the Haversine formula :

		function rad (x) {
			return x * Math.PI / 180;
		};

		function getDistance (p1, p2) {
			let R = 6378137; // Earth’s mean radius in meter
			let dLat = rad(p2.lat - p1.lat);
			let dLong = rad(p2.lng - p1.lng);
			let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
			let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			let d = R * c;
			return d; // returns the distance in meter
		};

		if ((!this.props.resource.userid || waypoints.length == 0) && timeLineArray.length == 0) {
			if (markers.length > 0 || customermarkers.length > 0)
				map.fitBounds(bounds);
			else {
				let a = bounds.extend({
					lat: 38.195311938720536,
					lng: 73.3534302228104
				});

				map.fitBounds(a);
				map.setZoom(3);
			}
		}

		let mcOptions = {
			styles: [{
				height: 41,
				width: 41,
				url: "/images/cluster.png",
				textSize: 14,
				textColor: "#444444"
			}],
			zoomOnClick:true,
			maxZoom:21
		}

		this.setState({
			markers,
			customermarkers,
			waypoints,
			directionsService,
			markerCluster : new MarkerClusterer(map, customermarkers, mcOptions)
		});

		if (errMsg) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : this.state.errMsg,
				btnArray : ["Ok"]
			}));
		}
	}

	activityTimelineMarkers (array, markerArray, reference, map, bounds, ischeckin, timeLineArray) {
		array.forEach((item, i) => {
			timeLineArray.push({
				latlng: item.position,
				locationdate: new Date(item.locationdate),
				reference,
				ischeckin
			});

			let marker = new google.maps.Marker({
				position: item.position,
				map: map,
				titleinfo: this.renderInfoWindowFn(item, reference),
				locationdate: new Date(item.locationdate),
				animation: google.maps.Animation.DROP,
				icon: ischeckin ? MapProps.getIcons().checkInIcon : MapProps.getIcons().checkOutIcon,
				id: i,
				isUser: true
			});

			marker.setLabel({
				text : (i+1).toString(),
				color : 'white',
				fontWeight: 'bold',
				fontSize: '13px'
			});
			//marker.setMap(map);

			markerArray.push(marker);

			bounds.extend(marker.position);

			this.markerListenerFn(marker, i);
		});

		//map.fitBounds(bounds);
	}

	resetFilter () {
		let { errMsg, markers, customermarkers, waypoints, map, directionsService, userObj } = this.state;
		let directionDisplayArray = this.props.resource.directionDisplayArray;

		markers.forEach((item) => {
			item.setMap(null);
		});

		customermarkers.forEach((item) => {
			item.setMap(null);
		});

		if (directionsService.length > 0) {
			directionsService.forEach((item) => {
				item.setMap(null);
			});
		}

		this.setState({
			errMsg : "",
			markers : [],
			customermarkers : [],
			waypoints : [],
			directionsService: [],
			showoverview: false,
			userObj: {}
		});

		directionDisplayArray.forEach((item) => {
			item.setMap(null);
		});

		directionDisplayArray = [];

		let tempObj = {
			locationdate : new Date(new Date().setHours(0, 0, 0, 0)),
			totalDistanceTravelled : 0,
			usersCurrentLocations : [],
			directionDisplayArray,
			locationOffArray : [],
			customerLocations : [],
			employeeLocations : [],
			userid : null,
			customerid : null
		};

		this.props.updateFormState(this.props.form, tempObj);

		setTimeout(this.getReportData, 0);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	markerListenerFn (marker, i) {
		let { map, stepDisplay } = this.state;

		google.maps.event.addListener(stepDisplay, 'domready', function() {
			var iwOuter = $('.gm-style-iw');
			iwOuter.addClass('gs-gm-style-iw');

			var iwOuterArrow = $('.gm-style-iw-t');
			iwOuterArrow.addClass('gs-gm-style-iw-t');

			var iwBackground = iwOuter.prev();
			var closeicon = iwOuter.next();

			iwBackground.children(':nth-child(2)').css({'display' : 'none'});
			var arrowEle = $('.gm-style-iw').prev().children(':nth-child(3)');

			arrowEle.css({'display' : 'none'});
			//arrowEle.children(':nth-child(1)').css({'width' : '13px'});
			//arrowEle.children(':nth-child(2)').css({'left' : '7px', 'width' : '13px', 'height' : '18px'});
			//arrowEle.children(':nth-child(2)').children(':nth-child(1)').css({'width' : '8px', 'height' : '26px'});
			iwBackground.children(':nth-child(1)').css({'display' : 'none'});

			iwBackground.children(':nth-child(4)').css({'display' : 'none'});
			closeicon.css({'display' : 'none'});
			$('button.gm-ui-hover-effect').css({'display' : 'none'});
		});

		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				stepDisplay.setContent('<div>'+this.titleinfo+'</div>');
				stepDisplay.open(map, this);
			}
		})(marker, i));

		google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
			return function() {
				stepDisplay.setContent('<div>'+this.titleinfo+'</div>');
				stepDisplay.open(map, this);
			}
		})(marker, i));

		google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
			return function() {
				stepDisplay.close();
			}
		})(marker, i));
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		let { showallmarkers, overallTimelineArray } = this.state;

		const mapStyle = {
			width: '100%',
			height: $(window).outerHeight() - $('.navbar').outerHeight() - $('.report-header').outerHeight() - ((this.props.resource && !this.props.resource.totalDistanceTravelled) ? 0 : 25)
		}

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Employee Map</div>
						</div>
						<div className="col-md-12 paddingright-5">
							{this.props.resource ? <ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">User Name</label>
										<Field
											name = {'userid'}
											props = {{
												resource: "users",
												fields: "id,displayname,trackerstatus",
												label: "displayname",
												//filter: "users.trackerstatus='Allowed'",
												required: new Date(this.props.resource.locationdate).setHours(0,0,0,0) != new Date().setHours(0, 0, 0, 0) ? true : false,
												onChange: (value, valueObj) => {this.setState({
													userObj: valueObj
												})}
											}}
											component = {autoSelectEle}
											validate={[numberNewValidation({required: "{new Date(resource.locationdate).setHours(0,0,0,0) != new Date().setHours(0, 0, 0, 0) ? true : false}", model: 'User Name'})]}
											/>
									</div>
									<div className="form-group col-md-12 col-sm-12 d-flex">
										<label className="labelclass mr-1">Show Customer Locations</label>
										<Field name={'showcustomerlocation'} component={checkboxEle}/>
									</div>
									{this.props.resource.showcustomerlocation ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer Name</label>
										<Field
											name = {'customerid'}
											props = {{
												resource: "partners",
												fields: "id,name,displayname",
												displaylabel: 'name',
												label: 'displayname',
												filter: 'partners.iscustomer=true'
											}}
											component = {autoSelectEle} />
									</div> : null}
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Date</label>
										<Field
											name = {'locationdate'}
											props = {{required: true, max: new Date(new Date().setHours(0, 0, 0, 0)),}}
											format = {(value, name) => {
												return value == null ? null : moment(value).format("DD-MMM-YYYY")
											}}
											parse = {(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}}
											component={DateEle}
											validate={[dateNewValidation({required:  true, title : 'Date', max: new Date(new Date().setHours(0, 0, 0, 0))})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter> : null}
							<div className="row">
								<div className="col-md-12 col-xs-12 col-sm-12" style={{paddingLeft : '46px'}}>
									{ this.props.resource && this.props.resource.locationOffArray.length > 0 ? <div className="col-md-10 offset-md-1 col-xs-10 col-sm-10 alert alert-light text-dark">
										Location is off for this employee during the some intervals.
										<a className='marginleft-15' onClick={() => {this.setState({showIntervals : !this.state.showIntervals})}} ><span className={`${this.state.showIntervals ? 'fa fa-lg fa-chevron-circle-down' : 'fa fa-lg fa-chevron-circle-right'}`}></span></a>
										{ this.state.showIntervals ? <p><br />{this.props.resource.locationOffArray.join(', ')}</p> : null }
									</div>: null }
									<div className="col-md-12 col-xs-12 col-sm-12">
										<div className="row">
											{(this.props.resource && this.props.resource.userid && (this.props.resource.totalDistanceTravelled > 0 || this.state.waypoints.length > 0)) ? <div className="col-md-6 offset-md-3">
												<div className="floating-panel">
													<div style={{position: 'relative', display: 'flex'}}>
														<span style={{marginTop:'5px'}}> Total Distance Travelled : <b style={{color: '#3b99ff'}}>{this.props.resource.totalDistanceTravelled} kms</b> (approximately)</span>
													</div>
												</div>
											</div>: null}
										</div>
										{(this.props.resource && this.props.resource.userid && overallTimelineArray.length > 0) ? <div style={{position: 'absolute', right: '90px', bottom: '40px', zIndex: '100', width: '325px', backgroundColor: '#fff', textAlign: 'center', borderRadius: '5px'}}>
											<div className="row no-gutters">
												<div className="col-md-12 gs-uppercase" style={{paddingTop: '2px', color: '#3b99ff', paddingBottom: '6px', fontWeight:'bold', cursor: 'pointer', borderBottom: `${this.state.showoverview ? '1px solid #ddd' : 'none'}`}} onClick={()=>this.setState({showoverview: !this.state.showoverview})}>
													<div className="row no-gutters">
														<div className = {`col-md-12`}>
															<span className = {`${this.state.showoverview ? 'fa fa-caret-down' : 'fa fa-caret-up'}`}></span>
															<div>Activity Overview</div>
														</div>
													</div>
												</div>
												<div className={`col-md-12 mapacitivity-animation ${this.state.showoverview ? 'd-block': 'd-none'}`} style={{height: `${mapStyle.height - 130}px`, maxHeight: `${mapStyle.height - 130}px`, overflowY: 'auto', paddingTop: '10px'}}>
													<div className="row no-gutters">
														<div className="col-md-12">
															<div className="gs-activity-timeline-centered">
																{overallTimelineArray.map((activityItem, activityIndex)=> {
																	return (
																		<div className="gs-activity-timeline-entry" key={activityIndex}>
																			<div className="gs-activity-timeline-entry-inner">
																				<div className="gs-activity-timeline-time">
																					{activityItem.startdatetime ? <div>{timeFilter(activityItem.startdatetime)}</div> : null}
																					{activityItem.enddatetime ? <div>{timeFilter(activityItem.enddatetime)}</div> : null}
																				</div>
																				{(activityIndex == 0 || activityIndex == overallTimelineArray.length-1) ? <div className="gs-activity-timeline-icon1">
																					<div style={{width: '8px', height: '8px',backgroundColor: '#00386c', borderRadius: '50%'}}></div>
																				</div> : <div className="gs-activity-timeline-icon">
																					{activityIndex}
																				</div> }
																				<div className="gs-activity-timeline-label">
																					<div>
																						<span className="gs-uppercase" style={{color: '#3b99ff'}}>{activityItem.referencetype}</span>
																						{activityItem.partnerid_name ? <span> with {activityItem.partnerid_name}</span> : null}
																					</div>
																					{activityItem.timespent > 0 ? <div>
																						<span className=" badge btn gs-form-btn-secondary">{timeDurationFilter(activityItem.timespent, true)}</span>
																					</div>: null}
																				</div>
																			</div>
																		</div>
																	);
																})}
															</div>
														</div>
													</div>
												</div>
											</div>
										</div> : null}
										<div id="map" style={mapStyle}></div>
										{(this.props.resource && this.props.resource.userid && this.props.resource.totalDistanceTravelled > 0) ? <div className="col-md-12 col-xs-12 col-sm-12 text-center">* Showing approximate route based on available data points. May not match exactly with the actual route traveled by the employee.</div> : null}
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</>
		);
	};
}

EmployeeLocationsReportForm = connect(
	(state, props) => {
		let formName = 'employeelocationsreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(EmployeeLocationsReportForm));

export default EmployeeLocationsReportForm;
