import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import { Calendar, momentLocalizer, Navigate } from 'react-big-calendar';
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import "react-big-calendar/lib/addons/dragAndDrop/styles.css";
import 'react-big-calendar/lib/css/react-big-calendar.css';


import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, checkboxEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import CalendarToolbar from '../components/calendartoolbar';
import { dateFilter, datetimeFilter, timeFilter } from '../utils/filter';
import NextActivityConfirmModal from '../components/details/nextactivityalertmodal';

const localizer = momentLocalizer(moment);
const DnDCalendar = withDragAndDrop(Calendar);

class ActivityCalendarForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			displayDragItemInCell: true,
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.getCalendarData = this.getCalendarData.bind(this);
		this.getTitle = this.getTitle.bind(this);
		this.onView = this.onView.bind(this);
		this.onNavigate = this.onNavigate.bind(this);
		this.updateTimes = this.updateTimes.bind(this);
		this.openActivity = this.openActivity.bind(this);
		this.gotoListPage = this.gotoListPage.bind(this);
		this.createActivity = this.createActivity.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata) {
			tempObj = {
				...this.props.reportdata,
				events: []
			};
		} else {
			tempObj = {
				activitiesfor : ['All'],
				showcompleted: false,
				salesperson : this.props.app.user.id,
				fromdate : moment(new Date()).startOf('day')._d,
				todate : moment(new Date()).endOf('day')._d,
				defaultDate: new Date(),
				defaultView: 'day',
				activitycalendar: {},
				events: []
			};
		}

		if(this.props.location.params && tempObj.activitiesfor.indexOf(this.props.location.params.activitiesfor) == -1) {
			tempObj.activitiesfor.push(this.props.location.params.activitiesfor);
		}
		this.props.initialize(tempObj);
		setTimeout(() => {
			this.getCalendarData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	onView(view) {
		if(view == 'day') {
			window.scrollTo($('.rbc-current-time-indicator').css('top'), 0)
		}
		this.props.updateFormState(this.props.form, {
			defaultView: view
		});
		setTimeout(() => {
			this.updateTimes();
		});
	}

	onNavigate(date, view) {
		const new_date = moment(date);
		this.props.updateFormState(this.props.form, {
			defaultDate: new_date
		});
		setTimeout(() => {
			this.updateTimes();
		});
	}

	updateTimes(date = this.props.resource.defaultDate, view = this.props.resource.defaultView) {
		let start, end;
	
		if(view === 'day'){
			start = moment(date).startOf('day');
			end   = moment(date).endOf('day');
		} else if(view === 'week'){
			start = moment(date).startOf('week');
			end   = moment(date).endOf('week');
		} else if(view === 'month') {
			start = moment(date).startOf('month').startOf('week');
			end = moment(date).endOf('month').endOf('week');
		}
		else if(view === 'agenda') {
			start = moment(date).startOf('day');
			end   = moment(date).endOf('day');
			//end   = moment(date).endOf('day').add(1, 'month');
		}

		this.props.updateFormState(this.props.form, {
			fromdate: start._d,
			todate: end._d
		});
		setTimeout(() => {
			this.getCalendarData();
		}, 0);
	}

	getTitle(obj) {
		let title = obj.partnerid_name ? `${obj.partnerid_name} -- ` : '';

		title += `${obj.activitytypeid_name} -- ${datetimeFilter(obj.startdatetime)} -- ${obj.status} `;

		return title;
	}
	
	getCalendarData() {
		//this.updateLoaderFlag(true);
		this.setState({spinnerLoader: true});
		let events = [];
		let year = new Date().getFullYear();
		let month = new Date().getMonth();
		let date = new Date().getDate();

		let filterString = [];

		let activitiesfor = this.props.resource.activitiesfor == 'All' ? ['General', 'Sales Followup', 'Service Allocation', 'Payment Followup'] : [this.props.resource.activitiesfor];

		['fromdate', 'todate', 'salesperson'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});

		filterString.push(`activitiesfor=${activitiesfor}`);

		axios.get(`/api/query/activitycalendarquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.main.length; i++) {
					let showcompleted = !this.props.resource.showcompleted ? response.data.main[i].status != 'Completed'  : true;

					if(showcompleted) {
						let nextFollowupdate = new Date(response.data.main[i]['startdatetime']);
						let nextFollowupEnddate = new Date(response.data.main[i]['enddatetime']);
						events.push({
							id : response.data.main[i].id,
							title : this.getTitle(response.data.main[i]),
							start : nextFollowupdate,
							end : nextFollowupEnddate,
							className : 'label-green',
							allDay : false,
							...response.data.main[i]
						});
					}
				}

				this.props.updateFormState(this.props.form, {
					events
				});
				this.props.updateReportFilter(this.props.form, this.props.resource);
			}
			//this.updateLoaderFlag(false);
			this.setState({spinnerLoader: false});
		});
	}

	openActivity(activity) {
		let tempObj = {};
		tempObj = {
			id: activity.id,
			parentresource: activity.resourcename,
			parentid: activity.parentid,
			partnerid: activity.partnerid,
			contactid: activity.contactid,
			showrelatedtosection: true
		};

		if(this.props.resourcename == 'leads') {
			tempObj.quoteid = activity.quoteid;
			tempObj.leadid = activity.transactionid;
		}
		if(this.props.resourcename == 'quotes') {
			tempObj.leadid = activity.leadid;
			tempObj.quoteid = activity.transactionid;
		}

		this.props.createOrEdit('/details/activities/:id', tempObj.id, tempObj, (responseobj) => {
			this.getCalendarData();
		});
	}

	gotoListPage() {
		this.props.history.goBack();
	}

	createActivity(obj) {
		this.props.history.push({pathname: '/createActivity', params: {assignedto: this.props.resource.salesperson, ...obj}});
	}

	handleDragStart = event => {
		this.setState({ draggedEvent: event });
	}

	dragFromOutsideItem = () => {
		return this.state.draggedEvent
	}

	onDropFromOutside = ({ start, end, allDay }) => {
		const { draggedEvent } = this.state;

		const event = {
			id: draggedEvent.id,
			title: draggedEvent.title,
			start,
			end,
			allDay: allDay,
			...draggedEvent
		};

		this.setState({ draggedEvent: null });
		this.moveEvent({ event, start, end });
	}

	moveEvent = ({ event, start, end }) => {
		const { events } = this.props.resource;

		let eventObj = {};
		const nextEvents = events.map(existingEvent => {
			if(existingEvent.id == event.id && existingEvent.plannedendtime) {
				const { startdatetime, enddatetime, ...tempevent } = existingEvent;
				tempevent.plannedstarttime = start;
				tempevent.plannedendtime = end;
				tempevent.start = start;
				tempevent.end = end;
				eventObj = tempevent;

				return {
					...existingEvent,
					start: start,
					end: end
				};
			}
			return existingEvent;
		});

		this.props.updateFormState(this.props.form, {
			events: nextEvents
		});

		if(Object.keys(eventObj).length == 0)  {
			return;
		}

		this.updatePlannedTime(eventObj);
	}

	updatePlannedTime = (eventObj) => {
		const { events } = this.props.resource;

		axios({
			method : 'post',
			data : {
				actionverb : 'Save',
				data : eventObj
			},
			url : '/api/salesactivities'
		}).then((response)=> {
			if (response.data.message == 'success') {

				const nextEvents = events.map(existingEvent => {
					return existingEvent.id == response.data.main.id
					? {...existingEvent, plannedstarttime: response.data.main.plannedstarttime, plannedendtime: response.data.main.plannedendtime, modified: response.data.main.modified, start: eventObj.start, end: eventObj.end}
					: existingEvent;
				});

				this.props.updateFormState(this.props.form, {
					events: nextEvents
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	handleSelect = ({start, end, action}) => {
		if(['doubleClick', 'select'].includes(action)) {
			this.createActivity({
				plannedstarttime: start,
				plannedendtime: (action == 'select') ? end : null
			});
		}
	}

	event = ({ event }) => {
		if(event.activitytypeid_category == 'General')
			return (
				<div className="d-flex" style={{height: '100%'}}>
					<div className="d-flex flex-column" style={{backgroundColor: '#001f71',width: '3px'}}></div>
					<div className="d-flex flex-column" style={{padding: '2px 5px', width: 'calc(100% - 3px)', textDecoration: `${event.status == 'Completed' ? 'line-through': ''}`, lineHeight: '1.2'}}>
						{event.description ? <div className="nowrap">{event.description}</div> : null}
						{this.props.resource.defaultView != 'month' ? <div className="font-12 nowrap">{event.activitytypeid_name}</div> : null}
					</div>
				</div>	
			);
		if(event.activitytypeid_category == 'Sales Followup')
			return (
				<div className="d-flex" style={{height: '100%'}}>
					<div className="d-flex flex-column" style={{backgroundColor: '#1286ff', width: '3px'}}></div>
					<div className="d-flex flex-column" style={{padding: '2px 5px', width: 'calc(100% - 3px)', textDecoration: `${event.status == 'Completed' ? 'line-through': ''}`, lineHeight: '1.2'}}>
						<div className="nowrap">{event.partnerid_name ? <span>{event.partnerid_name}</span> : null}{event.contactid_name ? <span>{event.partnerid_name ? ' - ': ''}{event.contactid_name}</span> : null}</div>
						{this.props.resource.defaultView != 'month' ? <div className="font-12 nowrap">{event.activitytypeid_name}</div> : null}
					</div>
				</div>	
			);
		if(event.activitytypeid_category == 'Payment Followup')
			return (
				<div className="d-flex" style={{height: '100%'}}>
					<div className="d-flex flex-column" style={{backgroundColor: '#248654c4', width: '3px'}}></div>
					<div className="d-flex flex-column" style={{padding: '2px 5px', width: 'calc(100% - 3px)', textDecoration: `${event.status == 'Completed' ? 'line-through': ''}`, lineHeight: '1.2'}}>
						<div className="nowrap">{event.partnerid_name ? <span>{event.partnerid_name}</span> : null}</div>
						{this.props.resource.defaultView != 'month' ? <div className="font-12 nowrap">{event.activitytypeid_name}</div> : null}
					</div>
				</div>	
			);
		if(event.activitytypeid_category == 'Service Allocation')
			return (
				<div className="d-flex" style={{height: '100%'}}>
					<div className="d-flex flex-column" style={{backgroundColor: '#b050f7', width: '3px'}}></div>
					<div className="d-flex flex-column" style={{padding: '2px 5px', width: 'calc(100% - 3px)', textDecoration: `${event.status == 'Completed' ? 'line-through': ''}`, lineHeight: '1.2'}}>
						<div className="nowrap">{event.partnerid_name ? <span>{event.partnerid_name}</span> : null}</div>
						{this.props.resource.defaultView != 'month' ? <div className="font-12 nowrap">{event.activitytypeid_name}</div> : null}
					</div>
				</div>	
			);
	}

	dragggable = (event) =>{
		if(event.status != 'Planned' || !event.plannedendtime) {
			return false;
		}

		return true;
	}

	render() {
		if(!this.props.resource)
			return null;

		let calendarstyle = {
			height: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.calendar_header_bar').outerHeight() - 15}px`,
			maxHeight: `${$(window).outerHeight() - $('.navbar').outerHeight() - $('.calendar_header_bar').outerHeight() - 15}px`,
			overflowY: 'auto'
		};

		let typeColorObj = {
			'Sales Followup' : '#e2effd',
			'General': '#e6eaf1',
			'Service Allocation': '#f8e6fd',
			'Payment Followup': '#d8fcec'
		};

		let currentTime = new Date();
		if(this.props.resource.events && this.props.resource.events.length > 0) {
			this.props.resource.events.some((event) => {
				if(new Date(event.startdatetime) < currentTime) {
					currentTime = new Date(event.startdatetime);
					return true;
				}
			});
		}

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row calendar_header_bar">
						<div className="col-md-12 form-group bg-white"  style={{borderBottom: '1px solid #ddd'}}>
							<div className="float-left" style={{marginTop: '15px', marginBottom: '15px', fontSize: '18px'}}><a className="affixanchor" onClick={this.gotoListPage}><span className="fa fa-chevron-left marginright-10"></span></a>  Activity Calendar</div>
							<div className="float-right margintop-10 marginbottom-10">
								<button type="button" className={`btn gs-btn-success btn-sm btn-width ${checkActionVerbAccess(this.props.app, 'salesactivities', 'Save') ? '' : 'hide'}`} onClick={this.createActivity}><i className="fa fa-plus"></i>Activity</button>
							</div>
						</div>
					</div>
					<div className="row" style={calendarstyle}>
						<div className="col-md-12 mb-3 py-2 bg-white">
							<div className="row">
								<div className="col-md-4 col-sm-6 col-xs-6">
									<div className="row">
										<div className="col-md-3 d-flex align-items-center justify-content-end">
											<label className="labelclass">Assigned To</label>
										</div>
										<div className="col-md-8">
											<Field name={'salesperson'} props={{resource: "users", fields: "id,displayname", label: "displayname", onChange: this.getCalendarData, required: true}} component={selectAsyncEle}  validate={[numberNewValidation({required:  true, title : 'Assigned To'})]} />
										</div>
									</div>
								</div>
								<div className="col-md-4 col-sm-6 col-xs-6">
									<div className="row">
										<div className="col-md-4 d-flex align-items-center justify-content-end">
											<label className="labelclass">Activities Types</label>
										</div>
										<div className="col-md-8">
											<Field name={'activitiesfor'} props={{options: ['All', 'General', 'Sales Followup', 'Service Allocation', 'Payment Followup'], required: true, onChange: this.getCalendarData}} component={localSelectEle}  validate={[stringNewValidation({required: true, title : 'Show Activities For'})]} />
										</div>
									</div>
								</div>
								<div className="col-md-2 col-sm-6 col-xs-6 d-flex  justify-content-center align-items-center">
									<label className="labelclass" style={{marginBottom: '.5rem'}}>Show Completed</label>
									<Field name={'showcompleted'} props={{onChange: this.getCalendarData}} component={checkboxEle} />
								</div>
								<div className="col-md-2 col-sm-6 col-xs-6">
									{this.state.spinnerLoader ? <span className="fa fa-spinner fa-spin fa-2x fa-fw"></span> : null}
								</div>
							</div>
						</div>
						<div className="col-md-12 bg-white py-2" style={{height: '100%'}}>
							<DnDCalendar localizer={localizer} events={this.props.resource.events}
								views={{ day: true, week: true, month: true,  agenda: CustomAgendaView }}
								defaultView={this.props.resource.defaultView}
								defaultDate={new Date(this.props.resource.defaultDate)}
								showMultiDayTimes
								components = {{
									event: this.event,
									toolbar : CalendarToolbar,
								}}
								onView={this.onView}
								onNavigate={this.onNavigate}
								onSelectEvent={event => this.openActivity(event)}
								draggableAccessor={this.dragggable}
								length={1}
								popup
								onEventDrop={this.moveEvent}
								dragFromOutsideItem={
									this.state.displayDragItemInCell ? this.dragFromOutsideItem : null
								}
								onDropFromOutside={this.onDropFromOutside}
								handleDragStart={this.handleDragStart}
								selectable
								resizable={false}
								onSelectSlot={this.handleSelect}
								eventPropGetter={(event, start, end, isSelected) => {
									return {
										style: {
											color: '#444444',
											backgroundColor: `${typeColorObj[event.activitytypeid_category]}`
										}
									}
								}}
								scrollToTime={currentTime} />
						</div>
					</div>
				</form>
			</>
		);
	};
}

class CustomAgendaView extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let currentdate = new Date(this.props.date).setHours(0, 0, 0, 0);
		let tempEventArr = [];

		let typeColorObj = {
			'Sales Followup' : '#1286ff',
			'General': '#001f71',
			'Service Allocation': '#b050f7',
			'Payment Followup': '#248654c4'
		};

		this.props.events.forEach(date => {
			let startdate = new Date(new Date(date.start).setHours(0, 0, 0, 0)).getTime();

			if(new Date(currentdate).getTime() == startdate)
				tempEventArr.push(date)
		});

		if(this.props.events.length == 0 || tempEventArr.length == 0)
			return <div className="alert alert-info text-center">There is Activities found for this date</div>;

		return (
			<>
				<table className="table" style={{color: '#444444'}}>
					<colgroup>
						<col style={{width: '12%'}} />
						<col style={{width: '3%'}} />
						<col />
					</colgroup>
					<tbody>
						{
							tempEventArr.map((item, index) => {
								return (
									<tr key={index} onClick={()=>this.props.onSelectEvent(item)} style={{cursor: 'pointer'}}>
										<td>
											<span>{timeFilter(item.start)} - {timeFilter(item.end)}</span>
										</td>
										<td>
											<div style={{width: '8px', height: '8px', borderRadius: '50%', display: 'inline-block', backgroundColor: `${typeColorObj[item.activitytypeid_category]}`}}></div>
										</td>
										<td>
											<div className="d-flex">
												<div className="flex-column semi-bold">{item.activitytypeid_name}</div>
												<div className="flex-column" style={{marginLeft: '24px'}}>
													{item.contactid_name ? <span className="">{item.contactid_name}</span> : null}
													{item.partnerid_name ? <span>{item.contactid_name ? ' - ' : ''}{item.partnerid_name}</span> : null}
												</div>
											</div>
										</td>
									</tr>
								)
							})
						}
					</tbody>
				</table>
			</>
		)
	}
}

CustomAgendaView.navigate = (date, action, Agenda) => {
	let previous_day = moment(date).subtract(1, 'days')._d;
	let next_day = moment(date).add(1, 'days')._d;

	switch (action) {
		case Navigate.PREVIOUS:
			return previous_day;

		case Navigate.NEXT:
			return next_day;

		default:
			return date;
	}
}

CustomAgendaView.title = date => {
	return `${moment(date).format('DD-MMM-YYYY')}`;
}

ActivityCalendarForm = connect(
	(state, props) => {
		let formName = 'activitycalendar';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ActivityCalendarForm));

export default ActivityCalendarForm;