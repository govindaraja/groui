import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, checkboxEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class AccountBalanceReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			accountColumns : [{
				"name" : "Account",
				"key" : "accountid_name",
				"format" : "anchortag",
				"cellClass" : "text-center",
				"width" : 200
			}, {
				"name" : "Opening Balance",
				"key" : "openingbalance",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Debit",
				"key" : "debitbalance",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Credit",
				"key" : "creditbalance",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Closing Balance",
				"key" : "closingbalance",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Parent",
				"key" : "parentid_name",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Type",
				"key" : "accountid_type",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Group",
				"key" : "accountid_accountgroup",
				"cellClass" : "text-center",
				"width" : 200
			}],
			partnerColumns : [{
				"name" : "Customer/Supplier",
				"key" : "partnerid_name",
				"cellClass" : "text-center",
				"width" : 200
			}, {
				"name" : "Opening Balance",
				"key" : "openingbalance",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Debit",
				"key" : "debitbalance",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Credit",
				"key" : "creditbalance",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Closing Balance",
				"key" : "closingbalance",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Sales Person",
				"key" : "salesperson_displayname",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Group",
				"key" : "customergroupid_name",
				"cellClass" : "text-center",
				"width" : 200
			}],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.filterOnChange = this.filterOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
			...this.props.reportdata
			};
		else
			tempObj = {
				asondate : new Date(new Date().setHours(0, 0, 0, 0)),
				type : 'Accounts',
				filter : 'As On Date',
				filters: {},
				columns: []
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('originalRows');

		let { accountColumns, partnerColumns } = this.state;

		let filterString = [];
		['fromdate', 'todate', 'filter', 'type', 'asondate'].forEach((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		if (this.props.resource.includereceivableentry)
			filterString.push(`includereceivableentry=yes`);

		if (this.props.resource.includepayableentry)
			filterString.push(`includepayableentry=yes`);

		axios.get(`/api/query/accountbalancequery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				if (this.props.resource.filter == 'Between') {
					response.data.main.forEach((item) => {
						item.openingbalance = Number((item.openbalance).toFixed(this.props.app.roundOffPrecision)) + '  ' + item.openbalancetype;
						item.currentbal = (item.debitbalance || 0) - (item.creditbalance || 0);

						if (item.currentbal > 0)
							item.currentbaltype = 'Dr';
						else
							item.currentbaltype = 'Cr';

						item.currentbal = Math.abs(item.currentbal);

						if (item.currentbaltype == item.openbalancetype) {
							item.closebal = item.currentbal + item.openbalance;
							item.closebaltype = item.currentbaltype;
						} else {
							if (item.openbalance > item.currentbal) {
								item.closebal = item.openbalance - item.currentbal;
								item.closebaltype = item.openbalancetype;
							} else {
								item.closebal = item.currentbal - item.openbalance;
								item.closebaltype = item.currentbaltype;
							}
						}

						item.closingbalance = Number((item.closebal).toFixed(this.props.app.roundOffPrecision)) + '  ' + item.closebaltype;
					});
				}

				accountColumns.forEach((item) => {
					if ((item.key == 'openingbalance' || item.key == 'closingbalance') && this.props.resource.type == 'Accounts')
						item.hidden = this.props.resource.filter == 'Between' ? false : true;
				});

				partnerColumns.forEach((item) => {
					if ((item.key == 'openingbalance' || item.key == 'closingbalance') && this.props.resource.type != 'Accounts')
						item.hidden = this.props.resource.filter == 'Between' ? false : true;
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns: this.props.resource.type == 'Accounts' ? accountColumns : partnerColumns
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);

				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			filter : null,
			asondate : null,
			fromdate : null,
			todate : null,
			type : null,
			includepayableentry : false,
			includereceivableentry : false,
			originalRows: null,
			filters: {},
			columns: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	filterOnChange() {
		let tempObj = {};
		if (this.props.resource.filter == 'Between')
			tempObj.asondate = null;
		else {
			tempObj.fromdate = null;
			tempObj.todate = null;
		}
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data) {
		this.props.history.push({
			pathname: '/generalledgerreport',
			params: {
				fromdate: this.props.resource.filter == 'As On Date' ? new Date(new Date(this.props.resource.asondate).setMonth(new Date(this.props.resource.asondate).getMonth() - 1)) : new Date(this.props.resource.fromdate),
				todate: this.props.resource.filter == 'As On Date' ? new Date(this.props.resource.asondate) : new Date(this.props.resource.todate),
				companyid: this.props.app.user.selectedcompanyid,
				accountid: data.accountid
			}
		});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Account Balances</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Filter</label>
										<Field name={'filter'} props={{options: ["As On Date", "Between"], required: true, onChange: this.filterOnChange}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Filter'})]} />
									</div>
									{this.props.resource.filter == 'As On Date' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">As On Date</label>
										<Field name={'asondate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'As On Date'})]}/>
									</div> : null }
									{this.props.resource.filter == 'Between' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div> : null }
									{this.props.resource.filter == 'Between' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: true,title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div> : null}
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Type</label>
										<Field name={'type'} props={{options: ["Accounts", "Customers", "Suppliers"], required: true}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Type'})]} />
									</div>
									{this.props.resource.type == 'Customers' ? <div className="form-group col-md-12 col-sm-12  d-flex">
										<label className="labelclass">Include Payable Entries</label>
										<Field name={'includepayableentry'} props={{}} component={checkboxEle} />
									</div> : null }
									{this.props.resource.type == 'Suppliers' ? <div className="form-group col-md-12 col-sm-12  d-flex">
										<label className="labelclass">Include Receivable Entries</label>
										<Field name={'includereceivableentry'} props={{}} component={checkboxEle} />
									</div> : null }
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12" >
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname="Accounts Balance" app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

AccountBalanceReportForm = connect(
	(state, props) => {
		let formName = 'accountbalancereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(AccountBalanceReportForm));

export default AccountBalanceReportForm;
