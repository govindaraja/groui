import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, search } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, textareaEle, checkboxEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class StockBatchHistoryReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.itemCB = this.itemCB.bind(this);
		this.itemBatchCB = this.itemBatchCB.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth((new Date().getMonth() - 1))).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Created On",
					"key" : "created",
					"format" : "datetime",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Opening Stock",
					"key" : "openqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Quantity",
					"key" : "qtynow",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Closing Stock",
					"key" : "closingqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Source Type",
					"key" : "resource",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Source",
					"key" : "sourceno",
					"format" : "anchortag",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Remarks",
					"key" : "remarks",
					"cellClass" : "text-center",
					"width" : 300
				}]
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];
		['warehouseid', 'itemid', 'itembatchid', 'locationid', 'fromdate', 'todate'].map((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/stockbatchhistoryquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {

				//Calculate Qty After,Now,Before
				if (response.data.main.length > 0) {
					
					response.data.main[0].openqty = response.data.startqty;
					response.data.main[0].closingqty = Number((response.data.startqty + response.data.main[0].qtynow).toFixed(this.props.app.roundOffPrecisionStock));

					for (let i = 1; i < response.data.main.length; i++) {
						response.data.main[i].openqty = response.data.main[i - 1].closingqty;
						response.data.main[i].closingqty = Number((response.data.main[i].openqty + response.data.main[i].qtynow).toFixed(this.props.app.roundOffPrecisionStock));
					}
				}

				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			warehouseid : null,
			itemid : null,
			itembatchid : null,
			locationid : null,
			itembatchid_description : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data) {
		let link = data.link.replace('#', '');

		this.props.history.push(`${link}`);
	}

	itemCB (value, valueObj) {
		this.props.updateFormState(this.props.form, {
			itembatchid : null,
			itemid_hasbatch : valueObj.hasbatch
		});
	}

	itemBatchCB (value, valueObj) {
		this.props.updateFormState(this.props.form, {
			itembatchid_description : valueObj.description
		});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Stock Batch/Location History Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Warehouse</label>
										<Field name={'warehouseid'} props={{resource: "stocklocations", fields: "id,name", filter: `stocklocations.companyid=${this.props.app.selectedcompanyid} and stocklocations.isparent and stocklocations.parentid is null ${!this.props.resource.includeprojectwarehouse ? ' AND stocklocations.projectid is null' : ''}`}} component={selectAsyncEle} validate={[numberNewValidation({required: true})]}/>
									</div>
									{this.props.app.feature.useProjects ? <div className="form-group col-md-12 col-sm-12 d-flex">
										<label className="labelclass">Include Project Warehouse</label>
										<Field name={'includeprojectwarehouse'} props={{}} component={checkboxEle}/>
									</div>: null }
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item</label>
										<Field
											name={'itemid'}
											props={{
												resource: "itemmaster",
												fields: "id,name,hasbatch,displayname",
												filter:"itemmaster.keepstock",
												label:"displayname",
												displaylabel:"name",
												onChange: (value, valueObj) => this.itemCB(value, valueObj)
											}}
											component={autoSelectEle}
											validate={[numberNewValidation({required: true})]}/>
									</div>
									{this.props.resource.itemid_hasbatch ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Batch Number</label>
										<Field
											name={'itembatchid'}
											props={{
												resource: "itembatches",
												fields: "id,batchnumber,description",
												filter:`itembatches.itemid=${this.props.resource.itemid}`,
												label:"batchnumber",
												onChange: (value, valueObj) => this.itemBatchCB(value, valueObj)
											}}
											component={autoSelectEle}
											validate={[numberNewValidation({required: true})]}/>
									</div> : null}
									{this.props.app.feature.useSubLocations ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Location</label>
										<Field
											name={'locationid'}
											props={{
												resource: "stocklocationquery",
												filter:`warehouseid=${this.props.resource.warehouseid}`,
												usemethod:true
											}}
											component={selectAsyncEle}
											validate={[numberNewValidation({required: true})]}/>
									</div> : null }
									{this.props.resource.itembatchid ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Batch Description</label>
										<Field name={'itembatchid_description'} disabled={true} component={textareaEle}/>
									</div> : null}
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Stock Batch/Location History' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

StockBatchHistoryReportForm = connect(
	(state, props) => {
		let formName = 'stockbatchhistoryreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(StockBatchHistoryReportForm));

export default StockBatchHistoryReportForm;
