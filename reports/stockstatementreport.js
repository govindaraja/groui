import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import axios from 'axios';
import moment from 'moment';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation } from '../utils/utils';
import { selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, checkboxEle } from '../components/formelements';
import { DateElement } from '../components/utilcomponents';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter } from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import { ChildEditModal } from '../components/utilcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class StockStatementReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.callBackGroup = this.callBackGroup.bind(this);
		this.onChangeGroupby = this.onChangeGroupby.bind(this);
		this.getWarehouseDetails = this.getWarehouseDetails.bind(this);
		this.getItemGroups = this.getItemGroups.bind(this);
		this.getItemCategories = this.getItemCategories.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.callBackIncludeProjectwarehouse = this.callBackIncludeProjectwarehouse.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				show : 'All',
				salespurchaseshow : 'All',
				stockallitem : 'stockitem',
				activeitem : 'Active Item',
				groupby : 'None',
				warehouseArray: [],
				warehouseid: [],
				stockArray : [],
				tempstockArray : [],
				itemcategoryArray : [],
				itemgroupArray : [],
				groupFilterArray : [],
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Item Name",
					"key" : "itemname",
					"locked" : true,
					"width" : 250
				}, {
					"name" : "Warehouse",
					"key" : "warehouse",
					"locked" : true,
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "On Hand Qty",
					"key" : "onhandqty",
					"locked" : true,
					"format" : "number",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "UOM",
					"key" : "uomname",
					"cellClass" : "text-center",
					"width" : 100
				}, {
					"name" : "SO Qty",
					"key" : "soqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 120
				}, {
					"name" : "PO Qty",
					"key" : "poqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 120
				}, {
					"name" : "Effective Qty",
					"key" : "effectiveqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 130
				}, {
					"name" : "",
					"format" : "button",
					"onClick" : "{report.btnOnClick}",
					"showfield" : "!item.issaleskit",
					"buttonname" : "Details",
					"cellClass" : "text-center",
					"restrictToExport" : true,
					"width" : 100
				}, {
					"name" : "Reserved Qty",
					"key" : "reservedqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 140
				}, {
					"name" : "Lead Time",
					"key" : "leadtime",
					"cellClass" : "text-right",
					"width" : 120
				}, {
					"name" : "Safety Stock",
					"key" : "safetystock",
					"cellClass" : "text-right",
					"width" : 140
				}, {
					"name" : "Item Group",
					"key" : "itemfullname",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Item Category Name",
					"key" : "itemcategoryname",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Description",
					"key" : "description",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Stock Location",
					"key" : "sublocations",
					"cellClass" : "text-center",
					"if" : this.props.app.feature.useSubLocations,
					"width" : 250
				}]
			};

			customfieldAssign(tempObj.columns, null, 'itemmaster', this.props.app.myResources);
		}

		this.props.initialize(tempObj);

		setTimeout(() => {
			this.getWarehouseDetails();
			this.getItemGroups();
			this.getItemCategories();
		}, 0);

		this.updateLoaderFlag(false);
	}

	getWarehouseDetails() {
		this.updateLoaderFlag(true);

		axios.get(`/api/stocklocations?field=id,name,projectid&filtercondition=stocklocations.isparent AND stocklocations.parentid IS NULL AND stocklocations.projectid is null`).then((response) => {
			if (response.data.message == 'success') {
				if(this.props.reportdata) {
					this.getReportData();
				} else {
					this.props.array.removeAll('warehouseArray');
					this.props.array.removeAll('warehouseid');

					let warehouseid = [];
					for (var i = 0; i < response.data.main.length; i++)
						warehouseid.push(response.data.main[i].id);

					this.props.updateFormState(this.props.form, {
						warehouseid,
						warehouseArray : response.data.main
					});
				}
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getItemGroups() {
		this.updateLoaderFlag(true);

		axios.get(`/api/itemgroups?field=id,groupname,fullname,parentid,isparent`).then((response) => {
			if (response.data.message == 'success') {
				this.props.array.removeAll('itemgroupArray');

				this.props.updateFormState(this.props.form, {
					itemgroupArray : response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getItemCategories() {
		this.updateLoaderFlag(true);

		axios.get(`/api/itemcategorymaster?field=id,name`).then((response) => {
			if (response.data.message == 'success') {
				this.props.array.removeAll('itemcategoryArray');

				this.props.updateFormState(this.props.form, {
					itemcategoryArray : response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	callBackGroup (value, valueObj) {
		let groupFilterArray = [],
			itemgroupArray = this.props.resource.itemgroupArray;

		if(value){
			groupFilterArray.push(value);
			getChildId(value);
		}

		function getChildId(id) {
			itemgroupArray.map((grpItem) => {
				if(grpItem.parentid == id) {
					groupFilterArray.push(grpItem.id);
					getChildId(grpItem.id);
				}
			});
		}

		this.props.updateFormState(this.props.form, { groupFilterArray });
	};

	callBackIncludeProjectwarehouse() {
		this.updateLoaderFlag(true);
		let queryString = `/api/stocklocations?field=id,name,projectid&filtercondition=stocklocations.isparent AND stocklocations.parentid IS NULL `;

		if(!this.props.resource.includeprojectwarehouse) {
			queryString += ` AND stocklocations.projectid is null`;
		}

		axios.get(`${queryString}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.array.removeAll('warehouseArray');
				this.props.array.removeAll('warehouseid');

				let warehouseid = [];
				for (var i = 0; i < response.data.main.length; i++)
					if(response.data.main[i].projectid == null)
						warehouseid.push(response.data.main[i].id);

				this.props.updateFormState(this.props.form, {
					warehouseid,
					warehouseArray : response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('tempstockArray');
		this.props.array.removeAll('stockArray');
		let { columns } = this.props.resource;

		let filterString = [];
		['companyid', 'warehouseid', 'itemid', 'itemcategoryid', 'show', 'stockallitem', 'salespurchaseshow', 'activeitem'].map((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		if(!this.props.resource.itemgroupid)
			this.props.array.removeAll('groupFilterArray');

		if (this.props.resource.groupFilterArray.length > 0)
			filterString.push(`itemgroupid=${this.props.resource.groupFilterArray}`);

		axios.get(`/api/query/stockstatementquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				if(this.props.resource.groupby == 'Item Group') {
					this.getItemGroupStockArray(response.data.main);
				} else if(this.props.resource.groupby == 'Item Category') {
					this.getItemCategoryStockArray(response.data.main);
				} else {
					let hiddencols = [];

					this.props.resource.columns.forEach((item) => {
						if(item.hidden == true)
							hiddencols.push(item.key);
					});
					this.props.updateFormState(this.props.form, {
						originalRows: response.data.main,
						tempstockArray: response.data.main,
						hiddencols
					});
				}

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	onChangeGroupby () {
		if(['Item Group', 'Item Category'].indexOf(this.props.resource.groupby) >= 0)
			this.props.array.removeAll('stockArray');

		if(this.props.resource.groupby == 'Item Group')
			this.getItemGroupStockArray(this.props.resource.tempstockArray);

		if(this.props.resource.groupby == 'Item Category')
			this.getItemCategoryStockArray(this.props.resource.tempstockArray);

	};

	getItemGroupStockArray (array) {
		this.props.resource.itemgroupArray.map((grpitem) => {
			grpitem.items = [];
			grpitem.onhandqty = 0;
			grpitem.poqty = 0;
			grpitem.soqty = 0;
			grpitem.reservedqty = 0;

			array.map((arrItem) => {
				if (grpitem.id == arrItem.itemgroupid)
					grpitem.items.push(arrItem);
			});
		});

		this.props.resource.itemgroupArray.map((grpitem) => {
			grpitem.items.map((childgrpitem) => {
				grpitem.onhandqty += childgrpitem.onhandqty;
				grpitem.poqty += childgrpitem.poqty;
				grpitem.soqty += childgrpitem.soqty;
				grpitem.reservedqty += childgrpitem.reservedqty;
				childgrpitem.effectiveqty = (childgrpitem.onhandqty) + (childgrpitem.poqty) - (childgrpitem.soqty);

				if (grpitem.parentid)
					this.setCount(grpitem.parentid, childgrpitem.onhandqty, childgrpitem.poqty, childgrpitem.soqty, childgrpitem.reservedqty);

				grpitem.effectiveqty += childgrpitem.effectiveqty;
				childgrpitem.onhandqty = Number(Number(childgrpitem.onhandqty).toFixed(this.props.app.roundOffPrecisionStock));
				childgrpitem.poqty = Number(Number(childgrpitem.poqty).toFixed(this.props.app.roundOffPrecisionStock));
				childgrpitem.soqty = Number(Number(childgrpitem.soqty).toFixed(this.props.app.roundOffPrecisionStock));
				childgrpitem.reservedqty = Number(Number(childgrpitem.reservedqty).toFixed(this.props.app.roundOffPrecisionStock));
				childgrpitem.effectiveqty = Number(Number(childgrpitem.effectiveqty).toFixed(this.props.app.roundOffPrecisionStock));
			})
		});

		let nodeArray = [];

		if(this.props.resource.stockallitem == 'stockitem') {
			if (this.props.resource.groupFilterArray.length > 0) {
				this.props.resource.groupFilterArray.map((FilgrpItem) => {
					this.props.resource.itemgroupArray.map((grpItem) => {
						if (FilgrpItem == grpItem.id) {
							if (grpItem.onhandqty > 0) {
								nodeArray.push({
									id : grpItem.id,
									itemname : grpItem.groupname,
									Group : grpItem.groupname,
									parentid : grpItem.parentid,
									isparent : grpItem.isparent,
									items : grpItem.items,
									onhandqty : grpItem.onhandqty.toFixed(this.props.app.roundOffPrecisionStock),
									poqty : grpItem.poqty.toFixed(this.props.app.roundOffPrecisionStock),
									soqty : grpItem.soqty.toFixed(this.props.app.roundOffPrecisionStock),
									reservedqty : grpItem.reservedqty.toFixed(this.props.app.roundOffPrecisionStock),
									effectiveqty : ((grpItem.onhandqty) + (grpItem.poqty) - (grpItem.soqty)).toFixed(this.props.app.roundOffPrecisionStock),
									children : grpItem.items
								});
							}
						}
					});
				});
			} else {
				this.props.resource.itemgroupArray.map((grpItem) => {
					if (grpItem.onhandqty > 0) {
						nodeArray.push({
							id : grpItem.id,
							itemname : grpItem.groupname,
							Group : grpItem.groupname,
							parentid : grpItem.parentid,
							isparent : grpItem.isparent,
							items : grpItem.items,
							onhandqty : grpItem.onhandqty.toFixed(this.props.app.roundOffPrecisionStock),
							poqty : grpItem.poqty.toFixed(this.props.app.roundOffPrecisionStock),
							soqty : grpItem.soqty.toFixed(this.props.app.roundOffPrecisionStock),
							reservedqty : grpItem.reservedqty.toFixed(this.props.app.roundOffPrecisionStock),
							effectiveqty : ((grpItem.onhandqty) + (grpItem.poqty) - (grpItem.soqty)).toFixed(this.props.app.roundOffPrecisionStock),
							children : grpItem.items
						});
					}
				});
			}
		} else {
			if (this.props.resource.groupFilterArray.length > 0) {
				this.props.resource.groupFilterArray.map((FilgrpItem) => {
					this.props.resource.itemgroupArray.map((grpItem) => {
						if(FilgrpItem == grpItem.id) {
							nodeArray.push({
								id : grpItem.id,
								itemname : grpItem.groupname,
								Group : grpItem.groupname,
								parentid : grpItem.parentid,
								isparent : grpItem.isparent,
								items : grpItem.items,
								onhandqty : grpItem.onhandqty.toFixed(this.props.app.roundOffPrecisionStock),
								poqty : grpItem.poqty.toFixed(this.props.app.roundOffPrecisionStock),
								soqty : grpItem.soqty.toFixed(this.props.app.roundOffPrecisionStock),
								reservedqty : grpItem.reservedqty.toFixed(this.props.app.roundOffPrecisionStock),
								effectiveqty : ((grpItem.onhandqty) + (grpItem.poqty) - (grpItem.soqty)).toFixed(this.props.app.roundOffPrecisionStock),
								children : grpItem.items
							});
						}
					});
				});
			} else {
				this.props.resource.itemgroupArray.map((grpItem) => {
					if(grpItem.items.length > 0) {
						nodeArray.push({
							id : grpItem.id,
							itemname : grpItem.groupname,
							Group : grpItem.groupname,
							parentid : grpItem.parentid,
							isparent : grpItem.isparent,
							items : grpItem.items,
							onhandqty : grpItem.onhandqty.toFixed(this.props.app.roundOffPrecisionStock),
							poqty : grpItem.poqty.toFixed(this.props.app.roundOffPrecisionStock),
							soqty : grpItem.soqty.toFixed(this.props.app.roundOffPrecisionStock),
							reservedqty : grpItem.reservedqty.toFixed(this.props.app.roundOffPrecisionStock),
							effectiveqty : ((grpItem.onhandqty) + (grpItem.poqty) - (grpItem.soqty)).toFixed(this.props.app.roundOffPrecisionStock),
							children : grpItem.items
						});
					}
				});
			}
		}


		nodeArray.map((item, index) => {
			if(item.parentid) {
				nodeArray.map((parentitem) => {
					if(parentitem.id == item.parentid) {
						parentitem.children.push(item);
					}
				});
				nodeArray.splice(index, 1);
			}
		});

		let hiddencols = [];

		this.props.resource.columns.forEach((item) => {
			if(item.hidden == true)
				hiddencols.push(item.key);
		});
		this.props.updateFormState(this.props.form, {
			tempstockArray: array,
			originalRows: nodeArray,
			hiddencols
		});
	};

	setCount (parentid, onhandqty, poqty, soqty, reservedqty) {
		let itemgroupArray = this.props.resource.itemgroupArray;

		itemgroupArray.map((item) => {
			if (parentid == item.id) {
				item.onhandqty += onhandqty;
				item.poqty += poqty;
				item.soqty += soqty;
				item.reservedqty += reservedqty;

				if (item.parentid)
					this.setCount(item.parentid, onhandqty, poqty, soqty, reservedqty);

			}
		});

		this.props.updateFormState(this.props.form, { itemgroupArray });
	};

	getItemCategoryStockArray (array) {
		let stockArray = [];

		this.props.resource.itemcategoryArray.map((grpItem) => {
			grpItem.items = [];
			grpItem.onhandqty = 0;
			grpItem.poqty = 0;
			grpItem.soqty = 0;
			grpItem.reservedqty = 0;

			array.map((arrItem) => {
				if (grpItem.id == arrItem.itemcategorymasterid)
					grpItem.items.push(arrItem);
			})
		});

		this.props.resource.itemcategoryArray.map((grpItem) => {
			grpItem.items.map((childgrpitem) => {
				grpItem.onhandqty += childgrpitem.onhandqty;
				grpItem.poqty += childgrpitem.poqty;
				grpItem.soqty += childgrpitem.soqty;
				grpItem.reservedqty += childgrpitem.reservedqty;
				childgrpitem.effectiveqty = (childgrpitem.onhandqty) + (childgrpitem.poqty) - (childgrpitem.soqty);
				grpItem.effectiveqty += childgrpitem.effectiveqty;
				childgrpitem.onhandqty = Number(Number(childgrpitem.onhandqty).toFixed(this.props.app.roundOffPrecisionStock));
				childgrpitem.poqty = Number(Number(childgrpitem.poqty).toFixed(this.props.app.roundOffPrecisionStock));
				childgrpitem.soqty = Number(Number(childgrpitem.soqty).toFixed(this.props.app.roundOffPrecisionStock));
				childgrpitem.reservedqty = Number(Number(childgrpitem.reservedqty).toFixed(this.props.app.roundOffPrecisionStock));
				childgrpitem.effectiveqty = Number(Number(childgrpitem.effectiveqty).toFixed(this.props.app.roundOffPrecisionStock));
			})
		});

		if(this.props.resource.stockallitem == 'stockitem') {
			if(this.props.resource.itemcategoryid > 0) {
				this.props.resource.itemcategoryArray.map((grpItem) => {
					if(this.props.resource.itemcategoryid == grpItem.id) {
						if(grpItem.onhandqty > 0) {
							grpItem.itemname = grpItem.name;
							grpItem.children = grpItem.items;
							stockArray.push(grpItem);
						}
					}
				});
			} else {
				this.props.resource.itemcategoryArray.map((grpItem) => {
					if(grpItem.onhandqty > 0) {
						grpItem.itemname = grpItem.name;
						grpItem.children = grpItem.items;
						stockArray.push(grpItem);
					}
				});
			}
		} else {
			if(this.props.resource.itemcategoryid > 0) {
				this.props.resource.itemcategoryArray.map((grpItem) => {
					if(this.props.resource.itemcategoryid == grpItem.id) {
						grpItem.itemname = grpItem.name;
						grpItem.children = grpItem.items;
						stockArray.push(grpItem);
					}
				});
			} else {
				this.props.resource.itemcategoryArray.map((grpItem) => {
					if(grpItem.children.length > 0) {
						grpItem.itemname = grpItem.name;
						grpItem.children = grpItem.items;
						stockArray.push(grpItem);
					}
				});
			}
		}

		let hiddencols = [];

		this.props.resource.columns.forEach((item) => {
			if(item.hidden == true)
				hiddencols.push(item.key);
		});
		this.props.updateFormState(this.props.form, {
			tempstockArray: array,
			originalRows: stockArray,
			hiddencols
		});
	};

	resetFilter () {
		let tempObj = {
			show : null,
			salespurchaseshow : null,
			stockallitem : null,
			activeitem : null,
			itemid : null,
			itemgroupid : null,
			itemcategoryid : null,
			groupby : null,
			warehouseid: [],
			tempstockArray : [],
			stockArray : [],
			itemcategoryArray : [],
			itemgroupArray : [],
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	btnOnClick(data) {
		this.props.openModal({
			render: (closeModal) => {
				return <ChildEditModal form={this.props.form} history={this.props.history} data={data} app={this.props.app} closeModal={closeModal} openModal={this.props.openModal} getBody={() => {return <StockStatementDetailsModal />}} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Stock Statement</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Warehouse</label>
										<Field name={'warehouseid'} props={{options: this.props.resource.warehouseArray, label: "name", valuename: "id", required: true, multiselect: true}} component={localSelectEle} validate={[numberNewValidation({required: true, model: 'Warehouse'})]} />
									</div>
									{this.props.app.feature.useProjects ? <div className="form-group col-md-12 col-sm-12 d-flex">
										<label className="labelclass mr-2">Include Project Warehouse</label>
										<Field name={'includeprojectwarehouse'} props={{onChange: (value) => this.callBackIncludeProjectwarehouse(value)}} component={checkboxEle}/>
									</div>: null }
									{ this.props.app.feature.useKitItems ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show (Based On Kit)</label>
										<Field name={'show'} props={{options: [{value: "All", label: "All"}, {value: "Non Kit", label: "Non Kit only"}, {value: "Kit", label: "Kit only"}], label: "label", valuename:"value", required: true}} component={localSelectEle} validate={[stringNewValidation({required:true, model: "Show (Based On Kit)"})]}/>
									</div>: null }
									{ this.props.app.feature.useItemAddOns ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show (Based On Item Addons)</label>
										<Field name={'salespurchaseshow'} props={{options: ["All", "Sales Item", "Purchase Item"], required: true}} component={localSelectEle} validate={[stringNewValidation({required:true, model: 'Show (Based On Item Addons)'})]}/>
									</div>: null }
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Name</label>
										<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name,displayname", label:"displayname", displaylabel:"name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Group</label>
										<Field name={'itemgroupid'} props={{resource: "itemgroups", fields: "id,fullname", label: "fullname", onChange: (value, valueObj) => this.callBackGroup(value, valueObj)}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'stockallitem'} props={{options: [{value: "allitem", label: "All Items"}, {value: "stockitem", label: "In Stock Items"}], label: "label", valuename:"value", required: true}} component={localSelectEle} validate={[stringNewValidation({required:true, model: "Show"})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Active</label>
										<Field name={'activeitem'} props={{options: ["All", "Active Item", "Inactive Item"], required: true}} component={localSelectEle} validate={[stringNewValidation({required:true, model: 'Active'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Group By</label>
										<Field name={'groupby'} props={{options:["None", "Item Group", "Item Category"], required: true, onChange: () => {this.onChangeGroupby()}}} component={localSelectEle}  validate={[stringNewValidation({required: true, model: 'Grouping By'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Stock Statement Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} btnOnClick={this.btnOnClick} treeCellExpand="itemname" /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

class StockStatementDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			useSubLocations : false,
			fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 6)).setHours(0, 0, 0, 0)),
			todate : new Date(new Date().setHours(0, 0, 0, 0)),
			companyid : this.props.app.user.selectedcompanyid,
			soqtydetails : [],
			poqtydetails : [],
			reservedqtydetails : [],
			stockByLocationsArray : [],
			serialnoArray : [],
			historyarray : [],
			tabClasses: ["", "", "", "", "", ""],
			selectedItem : this.props.data
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.close = this.close.bind(this);
		this.openRefLink = this.openRefLink.bind(this);
		this.updateField = this.updateField.bind(this);
		this.getTransactionHistroy = this.getTransactionHistroy.bind(this);
		this.setActiveTab = this.setActiveTab.bind(this);
		this.getTabPaneClass = this.getTabPaneClass.bind(this);
		this.getTabClass = this.getTabClass.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		this.updateLoaderFlag(true);
		let { useSubLocations, soqtydetails, poqtydetails, reservedqtydetails, selectedItem } = this.state;

		this.props.app.appSettings.map((item) => {
			if(item.module == "Stock" && item.name == "useSubLocations")
				useSubLocations = item.value.value;
		});

		axios.get(`/api/query/getstockquery?itemid=${selectedItem.itemid}&companyid=${this.state.companyid}&warehouseid=${selectedItem.warehouseid}`).then((response) => {
			if (response.data.message == 'success') {

				response.data.main.map((a)=>{
					if(a.warehouseid == selectedItem.warehouseid) {
						soqtydetails = a.salesOrders.sort((a, b)=>{
							return (a.deliverydate < b.deliverydate) ? -1 : (a.deliverydate > b.deliverydate ? 1 : 0);
						});
						poqtydetails = a.purchaseOrders.sort((a, b)=>{
							return (a.deliverydate < b.deliverydate) ? -1 : (a.deliverydate > b.deliverydate ? 1 : 0);
						});
						reservedqtydetails = a.stockreservations.sort((a, b)=>{
							return (a.expiredate < b.expiredate) ? -1 : (a.expiredate > b.expiredate ? 1 : 0);
						});
					}
				});

				this.setState({
					selectedItem,
					useSubLocations,
					soqtydetails,
					poqtydetails,
					reservedqtydetails
				});

				setTimeout(() => {
					this.setActiveTab(1);
					this.getTransactionHistroy();
					this.getSubLocationDetails();
					this.getSerialNos();
				}, 0);
				this.updateLoaderFlag(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	getSubLocationDetails () {
		this.updateLoaderFlag(true);

		let { useSubLocations, stockByLocationsArray, selectedItem } = this.state;

		if (useSubLocations || selectedItem.itemid_hasbatch) {
			axios.get(`/api/query/stockbybatchquery?warehouseid=${selectedItem.warehouseid}&itemid=${selectedItem.itemid}&activeitem=${'Active Items'}`).then((response) => {
				if (response.data.message == 'success') {
					stockByLocationsArray = response.data.main;

					this.setState({ stockByLocationsArray });
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}

				this.updateLoaderFlag(false);
			});
		}
	}

	getSerialNos () {
		this.updateLoaderFlag(true);

		let { serialnoArray, selectedItem } = this.state;

		if (this.state.selectedItem.itemid_hasserial) {
			axios.get(`/api/itemserialnos?field=id,serialno,stocklocations/name/stocklocationid,itembatchid,itembatches/batchnumber/itembatchid&filtercondition=itemserialnos.status='Available' and itemserialnos.itemid=${selectedItem.itemid} and itemserialnos.warehouseid=${selectedItem.warehouseid}`).then((response) => {
				if (response.data.message == 'success') {
					serialnoArray = response.data.main;

					this.setState({ serialnoArray });
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}

				this.updateLoaderFlag(false);
			});
		}
	}

	getTransactionHistroy () {
		this.updateLoaderFlag(true);

		let { historyarray, selectedItem, fromdate, todate, companyid } = this.state;

		let tempObj = selectedItem;

		tempObj['fromdate'] = fromdate;
		tempObj['todate'] = todate;
		tempObj['companyid'] = companyid;
		tempObj['activeitem'] = 'All';

		let filterString = [];
		['companyid', 'warehouseid', 'itemid', 'fromdate', 'todate', 'activeitem'].map((item) => {
			if (selectedItem[item])
				filterString.push(`${item}=${tempObj[item]}`)
		});

		axios.get(`/api/query/stockhistroyquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				historyarray = response.data.main;

				//Calculate Qty After,Now,Before
				if (historyarray.length > 0) {
					historyarray[0].openqty = response.data.startqty;
					historyarray[0].closingqty = Number((response.data.startqty + historyarray[0].transqty).toFixed(this.props.app.roundOffPrecisionStock));
					historyarray[0].valuebefore = response.data.startvalue;
					historyarray[0].valueafter = Number((response.data.startvalue + historyarray[0].valuenow).toFixed(this.props.app.roundOffPrecision));
				}

				for (let i = 1; i < historyarray.length; i++) {
					historyarray[i].openqty = historyarray[i - 1].closingqty;
					historyarray[i].closingqty = Number((historyarray[i].openqty + historyarray[i].transqty).toFixed(this.props.app.roundOffPrecisionStock));

					historyarray[i].valuebefore = historyarray[i - 1].valueafter;
					historyarray[i].valueafter = Number((historyarray[i].valuebefore + historyarray[i].valuenow).toFixed(this.props.app.roundOffPrecision));
				}

				this.setState({ historyarray });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	updateField(value, fieldname)  {
		let { fromdate, todate, historyarray } = this.state;

		if(fieldname == 'fromdate')
			this.setState({fromdate : value});

		if(fieldname == 'todate')
			this.setState({todate : value});

		if(fieldname == 'reset') {
			this.setState({
				fromdate : null,
				todate : null,
				historyarray : []
			});
		}
	}

	openRefLink (item) {
		this.props.closeModal();

		if(item.reference == 'Order')
			this.props.history.push(`/details/orders/${item.id}`);

		if (item.reference == 'Add On/Kit Order')
			this.props.history.push(`/details/orders/${item.id}`);

		if(item.reference == 'Stock Transfer')
			this.props.history.push(`/details/stocktransfer/${item.id}`);

		if(item.reference == 'Purchase Order')
			this.props.history.push(`/details/purchaseorders/${item.id}`);

		if(item.reference == 'Stock Reservation')
			this.props.history.push(`/details/stockreservations/${item.id}`);

		if(item.relatedresource == 'deliverynotes')
			this.props.history.push(`/details/deliverynotes/${item.relatedid}`);

		if(item.relatedresource == 'receiptnotes')
			this.props.history.push(`/details/receiptnotes/${item.relatedid}`);

		if(item.reference == 'Item Request')
			this.props.history.push(`/details/itemrequests/${item.id}`);
	};

	getTabClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	getTabPaneClass(tabNum) {
		return this.state.tabClasses[tabNum];
	}

	setActiveTab(tabNum) {
		let { tabClasses } = this.state;
		tabClasses =  ["", "", "", "", "", ""]
		tabClasses[tabNum] = "show active";
		this.setState({tabClasses}, ()=>{
			this.getTabPaneClass(tabNum);
		});
	}

	close() {
		this.props.closeModal();
	}

	renderParticularWarehouse() {
		let {useSubLocations, selectedItem, soqtydetails, poqtydetails, reservedqtydetails, historyarray, stockByLocationsArray, serialnoArray } = this.state;
		return (
			<div className="col-md-12 col-sm-12 col-xs-12">
				<ul className="nav nav-tabs">
					<li className="nav-item">
						<span className={`nav-link ${this.getTabClass(1)}`}  onClick={()=>this.setActiveTab(1)} >SO Details</span>
					</li>
					<li className="nav-item">
						<span className={`nav-link ${this.getTabClass(2)}`} onClick={()=>this.setActiveTab(2)} >PO Details</span>
					</li>
					<li className="nav-item">
						<span className={`nav-link ${this.getTabClass(3)}`} onClick={()=>this.setActiveTab(3)} >Stock Reservation Details</span>
					</li>
					<li className="nav-item">
						<span className={`nav-link ${this.getTabClass(4)}`} onClick={()=>this.setActiveTab(4)} >Stock History</span>
					</li>
					{ (useSubLocations || selectedItem.itemid_hasbatch) ? <li className="nav-item">
						<span className={`nav-link ${this.getTabClass(5)}`} onClick={()=>this.setActiveTab(5)} >Batch/Location Details</span>
					</li> : null }
					{ selectedItem.itemid_hasserial ? <li className="nav-item">
						<span className={`nav-link ${this.getTabClass(6)}`} onClick={()=>this.setActiveTab(6)} >Serial No Details</span>
					</li> : null }
				</ul>
				<div className="tab-content">
					<div className={`tab-pane fade ${this.getTabPaneClass(1)}`} role="tabpanel">
						<div className="row margintop-25">
							{soqtydetails.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												<th className="text-center">Resource</th>
												<th className="text-center">Customer</th>
												<th className="text-center">Reference No</th>
												<th className="text-center">Delivery Date</th>
												<th className="text-center">Quantity</th>
											</tr>
										</thead>
										<tbody>
											{soqtydetails.map((order, index) => {
												return (
													<tr key={index}>
														<td className="text-center">{order.reference}</td>
														<td>{order.partnerid_name}</td>
														<td className="text-center"><span onClick={()=>{this.openRefLink(order)}}>{order.transactionno}</span></td>
														<td className="text-center">{dateFilter(order.deliverydate)}</td>
														<td className="text-center">{order.quantity} {order.uomid_name}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</div>
							</div> : <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 alert alert-danger text-center">
									No SO Details found!!!
								</div>
							</div> }
						</div>
					</div>
					<div className={`tab-pane fade ${this.getTabPaneClass(2)}`} role="tabpanel">
						<div className="row margintop-25">
							{poqtydetails.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												<th className="text-center">Resource</th>
												<th className="text-center">Supplier</th>
												<th className="text-center">Reference No</th>
												<th className="text-center">Delivery Date</th>
												<th className="text-center">Quantity</th>
											</tr>
										</thead>
										<tbody>
											{poqtydetails.map((purchaseorder, index) => {
												return (
													<tr key={index}>
														<td className="text-center">{purchaseorder.reference}</td>
														<td>{purchaseorder.partnerid_name}</td>
														<td className="text-center"><span onClick={()=>{this.openRefLink(purchaseorder)}}>{purchaseorder.transactionno}</span></td>
														<td className="text-center">{dateFilter(purchaseorder.deliverydate)}</td>
														<td className="text-center">{purchaseorder.quantity} {purchaseorder.uomid_name}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</div>
							</div> :  <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 alert alert-danger text-center">
									No PO Details found!!!
								</div>
							</div> }
						</div>
					</div>
					<div className={`tab-pane fade ${this.getTabPaneClass(3)}`} role="tabpanel">
						<div className="row margintop-25">
							{reservedqtydetails.length > 0 ?  <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												<th className="text-center">Resource</th>
												<th className="text-center">Customer</th>
												<th className="text-center">Stock Reservation No</th>
												<th className="text-center">Expire Date</th>
												<th className="text-center">Quantity</th>
											</tr>
										</thead>
										<tbody>
											{reservedqtydetails.map((stockreservation, index) => {
												return (
													<tr key={index}>
														<td className="text-center">{stockreservation.reference}</td>
														<td>{stockreservation.customerid_name}</td>
														<td className="text-center"><span onClick={()=>{this.openRefLink(stockreservation)}}>{stockreservation.stockreservationno}</span></td>
														<td className="text-center">{dateFilter(stockreservation.expiredate)}</td>
														<td className="text-center">{stockreservation.quantity} {stockreservation.uomid_name}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</div>
							</div> : <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 alert alert-danger text-center">
									No Stock Reservation Details found!!!
								</div>
							</div> }
						</div>
					</div>
					<div className={`tab-pane fade ${this.getTabPaneClass(4)}`} role="tabpanel">
						<div className="row margintop-10">
							<div className="col-md-12 bg-white form-group">
								<div className="row" style={{margin: '0 auto',padding: '10px'}}>
									<div className="form-group col-md-3 col-sm-6 col-xs-6">
										<label className="labelclass">From Date</label>
										<DateElement className={`form-control ${!this.state.fromdate ? 'errorinput' : ''}`} value={this.state.fromdate} onChange={(val) => this.updateField(val, 'fromdate')} required={true} />
									</div>
									<div className="form-group col-md-3 col-sm-6 col-xs-6">
										<label className="labelclass">To Date</label>
										<DateElement className={`form-control ${!this.state.todate ? 'errorinput' : ''}`} value={this.state.todate} onChange={(val) => this.updateField(val, 'todate')} minDate = {this.state.fromdate ? moment(this.state.fromdate) : null} required={true} />
									</div>
									<div className="form-group col-md-3 col-sm-6 col-xs-6 margintop-25">
										<button type="button" onClick={() => {
											this.getTransactionHistroy()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.state.fromdate || !this.state.todate}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={(value) => {
											this.updateField(null, 'reset')	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</div>
							{historyarray.length > 0 ?  <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												<th className="text-center">Date</th>
												<th className="text-center">Open Qty</th>
												<th className="text-center">Transaction Qty</th>
												<th className="text-center">Closing Qty</th>
												<th className="text-center">Source Type</th>
												<th className="text-center">Source</th>
											</tr>
										</thead>
										<tbody>
											{historyarray.map((history, index) => {
												return (
													<tr key={index}>
														<td className="text-center">{dateFilter(history.postingdate)}</td>
														<td className="text-center">{history.openqty}</td>
														<td className="text-center">{history.transqty}</td>
														<td className="text-center">{history.closingqty}</td>
														<td className="text-center">{history.relatedresource == 'deliverynotes' ? 'Delivery Note' : (history.relatedresource == 'receiptnotes' ? 'Receipt Note' : '')}</td>
														<td className="text-center"><span onClick={()=>{this.openRefLink(history)}}>{history.relatedresource == 'deliverynotes' ? history.deliverynotenumber : (history.relatedresource == 'receiptnotes' ? history.receiptnotenumber : '')}</span></td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</div>
							</div> : <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 alert alert-danger text-center">
									NO Stock History Found!!!
								</div>
							</div> }
						</div>
					</div>
					<div className={`tab-pane fade ${this.getTabPaneClass(5)}`} role="tabpanel">
						<div className="row margintop-25">
							{stockByLocationsArray.length > 0 ?  <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												{ useSubLocations ? <th className="text-center">Location</th> : null }
												{ selectedItem.itemid_hasbatch ? <th className="text-center">Item Batch</th> : null }
												<th className="text-center">Quantity</th>
											</tr>
										</thead>
										<tbody>
											{stockByLocationsArray.map((item, index) => {
												return (
													<tr key={index}>
														{ useSubLocations ? <td className="text-center">{item.stocklocationid_name}</td> : null }
														{ selectedItem.itemid_hasbatch ? <td className="text-center">{item.itembatchid_batchnumber}</td> : null }
														
														<td className="text-center">{item.quantity}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</div>
							</div> : <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 alert alert-danger text-center">
									NO Details Found!!!
								</div>
							</div> }
						</div>
					</div>
					<div className={`tab-pane fade ${this.getTabPaneClass(6)}`} role="tabpanel">
						<div className="row margintop-25">
							{ serialnoArray.length > 0 ?  <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
									<table className="table table-bordered table-condensed">
										<thead>
											<tr>
												{ useSubLocations ? <th className="text-center">Location</th> : null }
												{ selectedItem.itemid_hasbatch ? <th className="text-center">Item Batch</th> : null }
												<th className="text-center">Serial No</th>
											</tr>
										</thead>
										<tbody>
											{ serialnoArray.map((item, index) => {
												return (
													<tr key={index}>
														{ useSubLocations ? <td className="text-center">{item.stocklocationid_name}</td> : null }
														{ selectedItem.itemid_hasbatch ? <td className="text-center">{item.itembatchid_batchnumber}</td> : null }
														
														<td className="text-center">{item.serialno}</td>
													</tr>
												);
											})}
										</tbody>
									</table>
								</div>
							</div> : <div className="col-md-12 col-sm-12 col-xs-12">
								<div className="col-md-12 col-sm-12 col-xs-12 alert alert-danger text-center">
									NO Details Found!!!
								</div>
							</div> }
						</div>
					</div>
				</div>
			</div>
		);
	}

	render() {
		let item = this.props.item;
		let { selectedItem } = this.state;

		return (
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Stock Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="form-group col-md-3 col-sm-6">
							Item Name : <b className="text-primary">{selectedItem.itemname}</b>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							Onhand Qty : <b className="text-success">{selectedItem.onhandqty} {selectedItem.uomname}</b>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							Effective Qty : <b className="text-danger">{selectedItem.effectiveqty} {selectedItem.uomname} </b>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							Reserved Qty : <b className="text-danger">{selectedItem.reservedqty} {selectedItem.uomname}</b>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							SO Qty : <b className="text-danger">{selectedItem.soqty}</b>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							PO Qty : <b className="text-danger">{selectedItem.poqty}</b>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							Warehouse : <b>{selectedItem.warehouse}</b>
						</div>
						{ selectedItem.warehouseid ? this.renderParticularWarehouse() : null}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="muted credit text-center">
							<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.close}><i className="fa fa-times"></i>Close</button>
						</div>
					</div>
				</div>
			</>
		);
	}
}

StockStatementReportForm = connect(
	(state, props) => {
		let formName = 'stockstatementreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(StockStatementReportForm));

export default StockStatementReportForm;
