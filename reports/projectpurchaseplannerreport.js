import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';

import { checkboxEle, DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import StockdetailsModal from '../containers/stockdetails';

class ProjectPurchasePlannerReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.projectCB = this.projectCB.bind(this);
		this.createPO = this.createPO.bind(this);
		this.createStockTransfer = this.createStockTransfer.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.showOnChange = this.showOnChange.bind(this);
		this.checkStockBtnOnClick = this.checkStockBtnOnClick.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "",
					"headerformat" : "checkbox",
					"key" : "ischecked",
					"locked": true,
					"format" : "checkbox",
					"cellClass" : "text-center",
					"onChange" : "{report.checkboxOnChange}",
					"showfield" : "((item.purchasestockdecision && item.suggestedqty > 0) || (!item.purchasestockdecision))",
					"restrictToExport" : true,
					"width" : 100
				}, {
					"name" : "Project Name",
					"key" : "projectid_projectname",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "BOQ Internal Ref No",
					"key" : "boqitemsid_internalrefno",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Item Name",
					"key" : "itemid_name",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Description",
					"key" : "description",
					"cellClass" : "text-center",
					"width" : 300
				},{
					"name" : "Approved Makes",
					"key" : "textapprovedmakes",
					"cellClass" : "text-center",
					"if" : this.props.app.feature.useMakeInTransactions,
					"width" : 250
				}, {
					"name" : "Qty Requested",
					"key" : "qtyrequest",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "UOM",
					"key" : "uomid_name",
					"cellClass" : "text-center",
					"width" : 100
				}, {
					"name" : "",
					"format" : "button",
					"key" : "stockdetails",
					"onClick" : "{report.checkStockBtnOnClick}",
					"buttonname" : "Check Stock",
					"cellClass" : "text-center",
					"restrictToExport" : true,
					"width" : 130
				}, {
					"name" : "Request No",
					"key" : "itemrequestno",
					"format" : "anchortag",
					"transactionname" : "itemrequests",
					"transactionid" : "itemrequestid",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Request Date",
					"key" : "itemrequestdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Require Date",
					"key" : "requireddate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Lead Time",
					"key" : "leadtime",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "",
					"format" : "button",
					"key" : "purchasestockdecision",
					"onClick" : "{report.btnOnClick}",
					"buttonname" : "{`${item.purchasestockdecision ? 'From Purchase' : 'From Stock'}`}",
					"cellClass" : "text-center",
					"restrictToExport" : true,
					"width" : 150
				}, {
					"name" : "PO Inprogress",
					"key" : "poinprogress",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "PO Raised",
					"key" : "poraised",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "PO Received",
					"key" : "poreceived",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "In Stock Qty",
					"key" : "instockqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Suggested Qty",
					"key" : "suggestedqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Purchase Orders",
					"key" : "ponumbers",
					"width" : 180
				}]
			};

		customfieldAssign(tempObj.columns, null, 'itemrequests', this.props.app.myResources);
		customfieldAssign(tempObj.columns, null, 'itemrequestitems', this.props.app.myResources);

		this.props.initialize(tempObj);

		setTimeout(() => {
			if(this.props.reportdata)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];
		['show', 'projectid', 'fromdate', 'todate', 'itemcategoryid'].map((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/projectpurchaseplannerquery?querytype=search&${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			projectid: null,
			itemcategoryid: null,
			fromdate : null,
			todate : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	projectCB (value, valueObj) {
		this.props.updateFormState(this.props.form, {
			projectid_projectname : valueObj.projectname,
			projectid_displayname : valueObj.displayname,
			projectid_projectno : valueObj.projectno,
			projectid_deliveryaddress : valueObj.deliveryaddress,
			projectid_deliveryaddressid :valueObj.deliveryaddressid
		});
	}

	createPO(param) {
		this.updateLoaderFlag(true);

		let itemCheckFound = false,
			purchaseItemArray = [],
			errorArr = [],
			tempObj = this.props.resource;

		let tempItemObj = {}, invalidProjectFound = false;

		for(var i= 0; i < tempObj.originalRows.length; i++) {
			if(tempObj.originalRows[i].ischecked) {
				tempObj.originalRows[i].quantity = tempObj.originalRows[i].suggestedqty;
				tempObj.originalRows[i].uomid = tempObj.originalRows[i].purchaseuomid;
				tempObj.originalRows[i].uomid_name = tempObj.originalRows[i].purchaseuomid_name;

				if (Object.keys(tempItemObj).length > 0) {
					let item = tempObj.originalRows[i];
					if (item.projectid != tempItemObj.projectid) {
						invalidProjectFound = true;
						break;
					}
				} else {
					tempItemObj = tempObj.originalRows[i];
				}

				if(!tempObj.originalRows[i].purchasestockdecision) {
					errorArr.push(`Sorry, you can't create ${param == 'po' ? 'Purchase Order' : 'RFQ'} for 'From Stock' items.`);
				} else if(tempObj.originalRows[i].quantity <= 0) {
					errorArr.push("Item: "+tempObj.originalRows[i].itemid_name+" quantity should be greater than 0");
				}

				purchaseItemArray.push(tempObj.originalRows[i]);
				itemCheckFound = true;
			}
		}

		if (!itemCheckFound || invalidProjectFound) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Warning",
				body : !itemCheckFound ? `Please choose atleast one item to create ${param == 'po' ? 'Purchase Order' : 'RFQ'}.!!!` : "Please select item request item for same project",
				btnArray : ["Ok"]
			}));
		} else if(errorArr.length > 0) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Warning",
				bodyArray : errorArr,
				btnArray : ["Ok"]
			}));
		} else {
			tempObj.projectid = purchaseItemArray[0].projectid;
			tempObj.projectid_projectno = purchaseItemArray[0].projectid_projectno;
			tempObj.projectid_projectname = purchaseItemArray[0].projectid_projectname;
			tempObj.displayaddress = purchaseItemArray[0].displayaddress;
			tempObj.displayaddressid = purchaseItemArray[0].displayaddressid;
			tempObj.warehouseid = purchaseItemArray[0].warehouseid;
			tempObj.projectid_deliveryaddressid = purchaseItemArray[0].deliveryaddressid;
			tempObj.projectid_deliveryaddress = purchaseItemArray[0].deliveryaddress;
			tempObj.projectid_displayname = purchaseItemArray[0].projectid_displayname;
			tempObj.requireddate = purchaseItemArray[0].itemrequestno_requireddate;
			tempObj.defaultcostcenter = purchaseItemArray[0].defaultcostcenter;
			tempObj['purchaseItemArray'] = purchaseItemArray;

			if(param == 'po') {
				commonMethods.customFieldsOperation(this.props.app.myResources, 'itemrequests', tempObj, purchaseItemArray[0], 'purchaseorders');
				this.props.history.push({pathname: '/createPurchaseOrder', params: {...tempObj, companyid: purchaseItemArray[0].companyid, param: 'Project Purchase Planner'}});
			} else {
				commonMethods.customFieldsOperation(this.props.app.myResources, 'itemrequests', tempObj, purchaseItemArray[0], 'rfq');
				this.props.history.push({pathname: '/createRFQ', params: {...tempObj, companyid: purchaseItemArray[0].companyid, purchaseArray: tempObj.purchaseItemArray, param: 'Project Purchase Planner'}});

			}
		}

		this.updateLoaderFlag(false);
	}

	createStockTransfer() {
		this.updateLoaderFlag(true);

		let itemCheckFound = false,
			purchaseItemArray = [],
			errorArr = [],
			tempObj = this.props.resource;

		let tempItemObj = {}, invalidProjectFound = false;

		for(var i= 0; i < tempObj.originalRows.length; i++) {
			if(tempObj.originalRows[i].ischecked) {
				tempObj.originalRows[i].quantity = tempObj.originalRows[i].qtyrequest;

				if (Object.keys(tempItemObj).length > 0) {
					let item = tempObj.originalRows[i];
					if (item.projectid != tempItemObj.projectid) {
						invalidProjectFound = true;
						break;
					}
				} else {
					tempItemObj = tempObj.originalRows[i];
				}

				if(tempObj.originalRows[i].purchasestockdecision)
					errorArr.push("You can add only items with 'From Stock' mode in a Stock Transfer");

				purchaseItemArray.push(tempObj.originalRows[i]);
				itemCheckFound = true;
			}
		}

		if (!itemCheckFound || invalidProjectFound) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Warning",
				body : !itemCheckFound ? "Please choose atleast one item to create Stock Transfer.!!!" : "Please select item request item for same project",
				btnArray : ["Ok"]
			}));
		} else if(errorArr.length > 0) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Warning",
				bodyArray : errorArr,
				btnArray : ["Ok"]
			}));
		} else {
			tempObj.projectid = purchaseItemArray[0].projectid;
			tempObj.projectid_projectno = purchaseItemArray[0].projectid_projectno;
			tempObj.projectid_projectname = purchaseItemArray[0].projectid_projectname;
			tempObj.projectid_warehouseid = purchaseItemArray[0].projectid_warehouseid;
			tempObj.displayaddress = purchaseItemArray[0].displayaddress;
			tempObj.displayaddressid = purchaseItemArray[0].displayaddressid;
			tempObj.warehouseid = purchaseItemArray[0].warehouseid;
			tempObj.projectid_deliveryaddressid = purchaseItemArray[0].deliveryaddressid;
			tempObj.projectid_deliveryaddress = purchaseItemArray[0].deliveryaddress;
			tempObj.projectid_displayname = purchaseItemArray[0].projectid_displayname;
			tempObj.requireddate = purchaseItemArray[0].itemrequestno_requireddate;
			commonMethods.customFieldsOperation(this.props.app.myResources, 'itemrequests', tempObj, purchaseItemArray[0], 'stocktransfer');
			tempObj['purchaseItemArray'] = purchaseItemArray;

			this.props.history.push({pathname: '/createStockTransfer', params: {...tempObj, companyid: purchaseItemArray[0].companyid, param: 'Project Purchase Planner'}});
		}

		this.updateLoaderFlag(false);
	}

	checkboxOnChange(value, item) {
		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		this.refs.grid.refresh();
	}

	checkboxHeaderOnChange(param) {
		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if(param) {
				item.ischecked = true;
			}
		});
		setTimeout(() => {
			this.props.updateFormState(this.props.form, {
				originalRows: this.props.resource.originalRows
			});
		}, 0);
		this.refs.grid.forceRefresh();
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	showOnChange() {
		this.props.updateFormState(this.props.form, {
			projectid : null,
			originalRows : []
		});
	}

	btnOnClick(item) {
		this.updateLoaderFlag(true);
		let checked = !item.purchasestockdecision;
		if(item.suggestedqty >  0 && !checked) {
			this.props.openModal(modalService.infoMethod({
				header : "Warning",
				body : "There is no sufficient quantity for this item '"+item.itemid_name+"' in the warehouse "+item.warehouseid_name,
				btnArray : ["Ok"]
			}));
		}

		axios.get(`/api/query/projectpurchaseplannerquery?querytype=update&purchasestockdecision=${checked}&itemrequestitemsid=${item.itemrequestitemsid}`).then((response) => {
			if (response.data.message == 'success') {
				item.purchasestockdecision = checked;
				this.props.updateFormState(this.props.form, {
					originalRows: this.props.resource.originalRows
				});
				this.refs.grid.refresh();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	checkStockBtnOnClick = (item) => {
		this.props.openModal({render: (closeModal) => {
			return <StockdetailsModal resource = {{}} item={item} closeModal={closeModal} />
		}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
	};

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 form-group bg-white report-header">
							<div className="report-header-title">Project Purchase Planner</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							<div className="report-header-rightpanel">
								{(this.props.resource.originalRows && this.props.resource.originalRows.length>0 && checkActionVerbAccess(this.props.app, 'stocktransfer', 'Save'))? <button type="button" onClick={() => {this.createStockTransfer()}} className="btn btn-width btn-sm gs-btn-outline-primary float-right" rel="tooltip" title="Create Purchase Order" disabled={!this.props.valid}><i className="fa fa-plus"></i>Stock Transfer</button> : null}
								{(this.props.resource.originalRows && this.props.resource.originalRows.length>0 && checkActionVerbAccess(this.props.app, 'purchaseorders', 'Save'))? <button type="button" onClick={() => {this.createPO('po')}} className="btn btn-width btn-sm gs-btn-outline-primary float-right" rel="tooltip" title="Create Purchase Order" disabled={!this.props.valid}><i className="fa fa-plus"></i>PO</button> : null}
								{(this.props.resource.originalRows && this.props.resource.originalRows.length>0 && checkActionVerbAccess(this.props.app, 'rfq', 'Save'))? <button type="button" onClick={() => {this.createPO('rfq')}} className="btn btn-width btn-sm gs-btn-outline-primary float-right" rel="tooltip" title="Create RFQ" disabled={!this.props.valid}><i className="fa fa-plus"></i>RFQ</button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: [{value: "all", label: "Show All"}, {value: "pending", label: "Show Pending"}], label:"label", valuename: "value", required: true, onChange: () => {this.showOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Show'})]} />
									</div>
									{this.props.resource.show == 'all' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Project</label>
										<Field
											name={'projectid'}
											props={{
												resource: "projects",
												fields: "id,projectname,projectno,deliveryaddress,deliveryaddressid,displayname",
												label: "displayname",
												filter: "projects.status='Approved'",
												onChange: (value, valueObj) => this.projectCB(value, valueObj)
											}}
											component={autoSelectEle}
											validate={[numberNewValidation({required: true})]}/>
									</div> : null}
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Required Date(From)</label>
										<Field name={'fromdate'} props={{}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Required Date(To)</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: '{resource.fromdate ? true : false}' ,title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Project Purchase Planner Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} btnOnClick={this.btnOnClick} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

ProjectPurchasePlannerReportForm = connect(
	(state, props) => {
		let formName = 'projectpurchaseplannerreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ProjectPurchasePlannerReportForm));

export default ProjectPurchasePlannerReportForm;
