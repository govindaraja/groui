import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class ReconcileWIP extends Component {
	constructor(props) {
		super(props);
		this.state = {
			reportSummaryColumns: [{
				"name" : "Order No",
				"key" : "orderno",
				"format" : "anchortag",
				"transactionname" : "productionorders",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Order Date",
				"key" : "orderdate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Customer",
				"key" : "customername",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Valuation Done",
				"key" : "valuationdone",
				"format" : "boolean",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Delivery Value",
				"key" : "deliveryvalue",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Receipt Value",
				"key" : "receiptvalue",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Journal Value",
				"key" : "journalvalue",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Difference Value",
				"key" : "diffvalue",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}],
			reportDetailColumns: [{
				"name" : "Order No",
				"key" : "orderno",
				"format" : "anchortag",
				"transactionname" : "productionorders",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Order Date",
				"key" : "orderdate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Customer",
				"key" : "customername",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Transaction Name",
				"key" : "tranname",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Transaction No",
				"key" : "tranno",
				"format" : "anchortag",
				"transactionname" : "relatedresource",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Transaction Value",
				"key" : "value",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let { reportSummaryColumns, reportDetailColumns } = this.state;
		let cdmonemonthdate = new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)));
		let fromDate = new Date(cdmonemonthdate.setDate(cdmonemonthdate.getDate() + 1));
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : fromDate,
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				period : 'Last month',
				show: 'Summary',
				columns: [...this.state.reportSummaryColumns],
				filters: {},
				hiddencols: []
			};

		customfieldAssign(reportSummaryColumns, null, 'productionorders', this.props.app.myResources);
		customfieldAssign(reportDetailColumns, null, 'productionorders', this.props.app.myResources);

		this.setState({ reportSummaryColumns, reportDetailColumns });

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];
		['companyid', 'fromdate', 'todate', 'asondate', 'show'].forEach((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`);
		});

		axios.get(`/api/query/reconcilewip?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let { reportSummaryColumns, reportDetailColumns } = this.state;

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns: this.props.resource.show == 'Summary' ? [...reportSummaryColumns] : [...reportDetailColumns]
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			period : 'Custom',
			asondate: null,
			show : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: [...this.state.reportSummaryColumns]
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname) {
		if(transactionname == 'productionorders')
			return this.props.history.push(`/details/productionorders/${data.id}`);

		this.props.history.push(`/details/${data.relatedresource}/${data.relatedid}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">WIP Reconciliation</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: ["Summary", "Details"], required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Show'})]} />
									</div>
									<ReportDateRangeField model={["period", "fromdate", "todate"]} updateFormState={this.props.updateFormState} form={this.props.form} resource={this.props.resource} />
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show Value As On Date</label>
										<Field name={'asondate'} props={{required: false}} component={DateEle} validate={[dateNewValidation({required: false, model: 'As On date'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12" >
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='WIP Reconciliation' app={this.props.app} ref="grid" report={this}gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

ReconcileWIP = connect(
	(state, props) => {
		let formName = 'reconcilewip';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ReconcileWIP));

export default ReconcileWIP;
