import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import { ChildEditModal } from '../components/utilcomponents';
import { currencyFilter } from '../utils/filter';

class ProjectCostVarianceReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Internal Ref No",
					"key" : "internalrefno",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Client Ref No",
					"key" : "clientrefno",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "BOQ Item Name",
					"key" : "boqitemid_name",
					"width" : 180,
				}, {
					"name" : "BOQ Description",
					"key" : "description",
					"width" : 200
				}, {
					"name" : "Estimation No",
					"key" : "estimationno",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Item Name",
					"key" : "itemid_name",
					"width" : 180
				}, {
					"name" : "Estimated Qty",
					"key" : "estimatedqty",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Estimated Rate",
					"key" : "estimatedrate",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Estimated value",
					"key" : "estimatedvalue",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Actual Qty",
					"key" : "actualqty",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Actual Rate",
					"key" : "actualrate",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Actual value",
					"key" : "actualvalue",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "",
					"format" : "button",
					"onClick" : "{report.btnOnClick}",
					"buttonname" : "Details",
					"cellClass" : "text-center",
					"restrictToExport" : true,
					"width" : 100
				}]
			};
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
	
		axios.get(`/api/query/projectcostvariancereportquery?param=list&projectid=${this.props.resource.projectid}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			projectid : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	btnOnClick(data) {
		this.props.openModal({
			render: (closeModal) => {
				return <ChildEditModal  form={this.props.form} history={this.props.history} data={data} app={this.props.app} closeModal={closeModal} openModal={this.props.openModal} getBody={() => {return <ProjectCostVarianceDetailsModal />}} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Project Cost Variance Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Project</label>
										<Field name={'projectid'} props={{resource: "projects", fields: "id,projectname,projectno,displayname", label: "displayname", filter: "projects.status in ('Approved', 'Completed')", required: true}} component={autoSelectEle} validate={[numberNewValidation({required: true, model: 'Project'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Project Cost Variance Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

class ProjectCostVarianceDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderflag: true,
			detailsarr: null
		};
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		axios.get(`/api/query/projectcostvariancereportquery?param=details&projectid=${this.props.resource.projectid}&projectestimationitemsid=${this.props.data.projectestimationitemsid}&itemid=${this.props.data.itemid}`).then((response) => {
			if (response.data.message == 'success') {
				this.setState({detailsarr: response.data.main});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	render() {
		return (
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="react-modal-header">
					<h5 className="modal-title">Actuals Details</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="col-md-12">
							{!this.state.detailsarr ? null : this.state.detailsarr.length > 0 ? <table className="table table-bordered">
								<thead>
									<tr>
										<th className="text-center">Actual Transaction</th>
										<th className="text-center">Source Transaction</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">Rate</th>
										<th className="text-center">Amount</th>
									</tr>
								</thead>
								<tbody>
									{this.state.detailsarr.map((item, index) => {
										return(
											<tr key={index}>
												<td className="text-center">{item.transactionname} - {item.transactionno}</td>
												<td className="text-center">{item.sourcetranname} - {item.sourcetranno}</td>
												<td className="text-center">{item.quantity}</td>
												<td className="text-right">{currencyFilter(item.rate, item.currencyid, this.props.app)}</td>
												<td className="text-right">{currencyFilter(item.amount, item.currencyid, this.props.app)}</td>
											</tr>
										);
									})}
								</tbody>
							</table> : <div className="alert alert-info text-center">There is no Actual Details</div>}
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="col-md-12 col-sm-12 col-xs-12">
						<div className="muted credit text-center">
							<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
						</div>
					</div>
				</div>
			</>
		);
	}
}

ProjectCostVarianceReportForm = connect(
	(state, props) => {
		let formName = 'projectcostvariancereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ProjectCostVarianceReportForm));

export default ProjectCostVarianceReportForm;
