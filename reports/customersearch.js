import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import ReactPaginate from 'react-paginate';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { InputEle, DateEle, selectAsyncEle, localSelectEle, autoSelectEle, autosuggestEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class CustomerSearchReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			customerSearchColumns: [{
				"name" : "Name",
				"key" : "displayname",
				"format" : "anchortag",
				"cellClass" : "text-center",
				"width" : 250
			}],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.handlePaginationClick = this.handlePaginationClick.bind(this);
		this.renderPagination = this.renderPagination.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				basedon : 'Customer Details',
				skip : 0,
				totalcount : 0,
				filters: {},
				hiddencols: [],
				columns: []
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData (searchFlag) {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let { customerSearchColumns } = this.state;
		let filterArray = [];

		if(this.props.resource.basedon == 'Delivery Note No')
			filterArray = ['basedon', 'deliverynotenumber'];
		else if(this.props.resource.basedon == 'Equipment Serial No')
			filterArray = ['basedon', 'serialno'];
		else if(this.props.resource.basedon == 'Sales Invoice No')
			filterArray = ['basedon', 'invoiceno'];
		else
			filterArray = ['basedon', 'name', 'contactperson', 'phone', 'mobile', 'email', 'addresstype', 'street', 'area', 'city', 'pincode', 'territoryid'];

		let filterString = [];
		filterArray.forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${encodeURIComponent(this.props.resource[item])}`)
			}
		});

		axios.get(`/api/query/customersearchquery?pagelength=500&skip=${(this.props.resource.skip * 500)}&${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {

				let hiddencols = [];
				if(customerSearchColumns.length > 1)
					customerSearchColumns.splice(1, customerSearchColumns.length-1);

				if(this.props.resource.basedon == 'Delivery Note No') {
					let dcArray = [{
						name : 'Delivery Note No',
						key : 'deliverynotenumber',
						cellClass : 'text-center',
						width : 300
					}, {
						name : 'Delivery Note Date',
						key : 'deliverynotedate',
						format : 'date',
						cellClass : 'text-center',
						width : 200
					}, {
						name : 'Status',
						key : 'status',
						cellClass : 'text-center',
						width : 250
					}];
					customerSearchColumns.splice(1, 0, ...dcArray);
				} else if(this.props.resource.basedon == 'Equipment Serial No') {
					let equipArray = [{
						name : 'Serial No',
						key : 'serialno',
						cellClass : 'text-center',
						width : 300
					}, {
						name : 'Equipment Name',
						key : 'equipmentid_displayname',
						cellClass : 'text-center',
						width : 300
					}];
					customerSearchColumns.splice(1, 0, ...equipArray);
				} else if(this.props.resource.basedon == 'Sales Invoice No') {
					let invArray = [{
						name : 'Invoice No',
						key : 'invoiceno',
						cellClass : 'text-center',
						width : 300
					}, {
						name : 'Invoice Date',
						key : 'invoicedate',
						format : 'date',
						cellClass : 'text-center',
						width : 200
					}, {
						name : 'Status',
						key : 'status',
						cellClass : 'text-center',
						width : 250
					}];
					customerSearchColumns.splice(1, 0, ...invArray);
				} else {
					let cusArray = [{
						"name" : "Contact Details",
						"key" : "contactname",
						"width" : 200
					}, {
						"name" : "Email",
						"key" : "email",
						"width" : 200
					}, {
						"name" : "Mobile",
						"key" : "mobile",
						"width" : 180
					}, {
						"name" : "Phone",
						"key" : "phone",
						"width" : 180
					}, {
						"name" : "Territory",
						"key" : "territoryid_territoryname",
						"width" : 150
					}, {
						"name" : "Street",
						"key" : "streetname",
						"if" : this.props.app.feature.useMasterForAddresses,
						"width" : 150
					}, {
						"name" : "Area",
						"key" : "areaname",
						"width" : 150
					}, {
						"name" : "Pin Code",
						"key" : "pincode",
						"width" : 150
					}, {
						"name" : "Address Type",
						"key" : "addresstype",
						"width" : 250
					}, {
						"name" : "Address Details",
						"key" : "addressinfo",
						"width" : 250
					}];
					customerSearchColumns.splice(1, 0, ...cusArray);
				}

				customerSearchColumns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					totalcount: response.data.count,
					columns: customerSearchColumns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);

				if(searchFlag && response.data.main.length == 1)
					this.openTransaction(response.data.main[0]);

			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	renderPagination() {
		if(this.props.resource && Math.ceil(this.props.resource.totalcount / 500) > 1)
			return <ReactPaginate previousLabel={<span>&laquo;</span>}
				nextLabel={<span>&raquo;</span>}
				breakLabel={<a></a>}
				breakClassName={"break-me"}
				pageCount={Math.ceil(this.props.resource.totalcount / 500)}
				marginPagesDisplayed={0}
				pageRangeDisplayed={5}
				onPageChange={this.handlePaginationClick}
				containerClassName={"list-pagination"}
				subContainerClassName={"pages pagination"}
				activeClassName={"active"}
				forcePage={this.props.resource.skip} />
		return null;
	}

	handlePaginationClick(obj) {
		this.props.updateFormState(this.props.form, {
			skip : obj.selected
		});
		setTimeout(() => {
			this.getReportData();
		}, 0);
	}

	resetFilter () {
		let tempObj = {
			basedon : '',
			name: '',
			mobile: '',
			email: '',
			phone: '',
			addresstype : '',
			street : '',
			area: '',
			city: '',
			contactperson: '',
			pincode : '',
			territoryid : null,
			deliverynotenumber: '',
			serialno: '',
			invoiceno: '',
			skip : 0,
			totalcount : 0,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data) {
		this.props.history.push({pathname: '/servicedashboard', params: {customerid: data.id}});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Customer Search</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Filter Basedon</label>
										<Field name={'basedon'} props={{options:["Customer Details", "Sales Invoice No", "Equipment Serial No", "Delivery Note No"], required: true}} component={localSelectEle}  validate={[stringNewValidation({required: true, model: 'Filter Basedon'})]}/>
									</div>
									{this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer Name</label>
										<Field name={'name'} props={{}} component={InputEle} />
									</div> : null}
									{this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Contact Name</label>
										<Field name={'contactperson'} props={{}} component={InputEle} />
									</div> : null}
									{this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Phone</label>
										<Field name={'phone'} props={{}} component={InputEle} />
									</div> : null}
									{this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Mobile</label>
										<Field name={'mobile'} props={{}} component={InputEle} />
									</div> : null}
									{this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Email</label>
										<Field name={'email'} props={{}} component={InputEle} />
									</div> : null}
									{this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Address Type</label>
										<Field name={'addresstype'} props={{
											resource: "addresses", field: "title"
										}} component={autosuggestEle} />
									</div> : null}
									{this.props.app.feature.useMasterForAddresses && this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Street</label>
										<Field name={'street'} props={{resource : "streets", field : "name"}} component={autosuggestEle} />
									</div> : null}
									{!this.props.app.feature.useMasterForAddresses && this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Area</label>
										<Field name={'area'} props={{resource : "addresses", field : "secondline"}} component={autosuggestEle} />
									</div> : null}
									{this.props.app.feature.useMasterForAddresses && this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Area</label>
										<Field name={'area'} props={{resource : "areas", field : "name"}} component={autosuggestEle} />
									</div> : null}
									{!this.props.app.feature.useMasterForAddresses && this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">City</label>
										<Field name={'city'} props={{resource : "addresses", field : "city"}} component={autosuggestEle} />
									</div> : null}
									{this.props.app.feature.useMasterForAddresses && this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">City</label>
										<Field name={'city'} props={{resource : "cities", field : "name"}} component={autosuggestEle} />
									</div> : null}
									{!this.props.app.feature.useMasterForAddresses && this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Pin Code</label>
										<Field name={'pincode'} props={{resource : "addresses", field : "postalcode"}} component={autosuggestEle} />
									</div> : null}
									{this.props.app.feature.useMasterForAddresses && this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Pin Code</label>
										<Field name={'pincode'} props={{resource : "areas", field : "pincode"}} component={autosuggestEle} />
									</div> : null}
									{this.props.resource.basedon == 'Customer Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Territory</label>
										<Field name={'territoryid'} props={{resource: "territories", fields: "id,fullname", label: "fullname", displaylabel: "fullname"}} component={autoSelectEle} />
									</div> : null}
									{this.props.resource.basedon == 'Delivery Note No' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Delivery Note No</label>
										<Field name={'deliverynotenumber'} props={{resource : "deliverynotes", field : "deliverynotenumber"}} component={autosuggestEle} />
									</div> : null}
									{this.props.resource.basedon == 'Equipment Serial No' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Equipment Serial No</label>
										<Field name={'serialno'} props={{resource : "equipments", field : "serialno"}} component={autosuggestEle} />
									</div> : null}
									{this.props.resource.basedon == 'Sales Invoice No' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Sales Invoice No</label>
										<Field name={'invoiceno'} props={{resource : "salesinvoices", field : "invoiceno"}} component={autosuggestEle} />
									</div> : null}
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData(true)	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
							<div className="margintop-10 paddingleft-30">{this.renderPagination()}</div>
						</div>
					</div>
				</form>
			</>
		);
	};
}

CustomerSearchReportForm = connect(
	(state, props) => {
		let formName = 'customersearchreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(CustomerSearchReportForm));

export default CustomerSearchReportForm;
