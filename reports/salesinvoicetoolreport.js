import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { AutoSelect, SelectAsync } from '../components/utilcomponents';
import { search } from '../utils/utils';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';

class SalesInvoicePlannerReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			search: {},
			limit: {
				order: 10,
				deliverynote: 10,
				salesreturn: 10
			},
			orderArray: [],
			deliverynoteArray: [],
			salesreturnArray: [],
			isDeliveryNoteActive: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.tabOnChange = this.tabOnChange.bind(this);
		this.addLimit = this.addLimit.bind(this);
		this.parentCheckboxOnChange = this.parentCheckboxOnChange.bind(this);
		this.itemCheckboxOnChange = this.itemCheckboxOnChange.bind(this);
		this.filterOnchange = this.filterOnchange.bind(this);
		this.createInvoice = this.createInvoice.bind(this);
		this.createCreditnote = this.createCreditnote.bind(this);
		this.searchFilter = this.searchFilter.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag = (loaderflag) => {
		this.setState({loaderflag});
	}

	onLoad = () => {
		this.updateLoaderFlag(true);
		let orderArray = [], deliverynoteArray = [], salesreturnArray = [];

		axios.get(`/api/query/salesinvoicetoolquery`).then((response) => {
			if (response.data.message == 'success') {
				let deliverynoteitemsdata = response.data.main.deliverynotesarr,
				salesreturnitems = response.data.main.salesreturnitemsarr,
				orderitems = response.data.main.orderarr;

				for (var i = 0; i < orderitems.length; i++) {
					let orderitemFound = false;
					for (var j = 0; j < orderArray.length; j++) {
						if (orderArray[j].id == orderitems[i].orderid) {
							orderitemFound = true;
							orderitems[i].number = orderitems[i].quantity;
							orderitems[i].numberinvoiced = orderitems[i].invoicedqty;
							orderitems[i].invoicequantity = orderitems[i].quantity - orderitems[i].invoicedqty;
							orderArray[j].orderitems.push(orderitems[i]);
							break;
						}
					}
					if (!orderitemFound) {
						let tempObject = {
							orderno : orderitems[i].orderno,
							customerid : orderitems[i].customerid,
							customerid_name : orderitems[i].customername,
							invoicefor : "Sales Orders",
							param : "Sales Orders",
							id : orderitems[i].orderid,
							contactid : orderitems[i].contactid,
							companyid : orderitems[i].companyid,
							companyid_name : orderitems[i].companyid_name,
							currencyid : orderitems[i].currencyid,
							currencyid_name : orderitems[i].currencyid_name,
							customerid_suppliercode : orderitems[i].customerid_suppliercode,
							customerid_gstregtype : orderitems[i].customerid_gstregtype,
							customerid_gstin : orderitems[i].customerid_gstin,
							billingaddress : orderitems[i].billingaddress,
							billingaddressid : orderitems[i].billingaddressid,
							billingaddressid_state : orderitems[i].billingaddressid_state,
							billingaddressid_gstin : orderitems[i].billingaddressid_gstin,
							deliveryaddress : orderitems[i].deliveryaddress,
							deliverypolicy : orderitems[i].deliverypolicy,
							orderdate : orderitems[i].orderdate,
							paymentterms : orderitems[i].paymentterms,
							creditperiod : orderitems[i].creditperiod,
							salesperson : orderitems[i].salesperson,
							orderitems : []
						}
						commonMethods.customFieldsOperation(this.props.app.myResources, 'orders', tempObject, orderitems[i],'salesinvoices');

						orderitems[i].number = orderitems[i].quantity;
						orderitems[i].numberinvoiced = orderitems[i].invoicedqty;
						orderitems[i].invoicequantity = orderitems[i].quantity - orderitems[i].invoicedqty;

						tempObject.orderitems.push(orderitems[i]);
						orderArray.push(tempObject);
					}
				}

				for (var i = 0; i < deliverynoteitemsdata.length; i++) {
					let dcitemFound = false;
					for (var j = 0; j < deliverynoteArray.length; j++) {
						if (deliverynoteArray[j].id == deliverynoteitemsdata[i].deliverynoteid) {
							dcitemFound = true;
							deliverynoteitemsdata[i].number = deliverynoteitemsdata[i].quantity;
							deliverynoteitemsdata[i].numberinvoiced = deliverynoteitemsdata[i].invoicedqty;
							deliverynoteitemsdata[i].invoicequantity = deliverynoteitemsdata[i].quantity - deliverynoteitemsdata[i].invoicedqty;
							deliverynoteArray[j].deliverynoteitems.push(deliverynoteitemsdata[i]);
							break;
						}
					}
					if (!dcitemFound) {
						let tempObject = {
							customerid : deliverynoteitemsdata[i].customerid,
							customerid_name : deliverynoteitemsdata[i].customername,
							invoicefor : "Delivery Notes",
							param : "Delivery Notes",
							id : deliverynoteitemsdata[i].deliverynoteid,
							deliverynotenumber : deliverynoteitemsdata[i].deliverynotenumber,
							contactid : deliverynoteitemsdata[i].contactid,
							companyid : deliverynoteitemsdata[i].companyid,
							companyid_name : deliverynoteitemsdata[i].companyid_name,
							currencyid : deliverynoteitemsdata[i].currencyid,
							currencyid_name : deliverynoteitemsdata[i].currencyid_name,
							customerreference : deliverynoteitemsdata[i].customerreference,
							customerid_suppliercode : deliverynoteitemsdata[i].suppliercode,
							customerid_gstregtype : deliverynoteitemsdata[i].customerid_gstregtype,
							customerid_gstin : deliverynoteitemsdata[i].customerid_gstin,
							deliveryaddress : deliverynoteitemsdata[i].deliveryaddress,
							billingaddress : deliverynoteitemsdata[i].billingaddress,
							billingaddressid : deliverynoteitemsdata[i].billingaddressid,
							billingaddressid_state : deliverynoteitemsdata[i].billingaddressid_state,
							billingaddressid_gstin : deliverynoteitemsdata[i].billingaddressid_gstin,
							orderid_tandc : deliverynoteitemsdata[i].tandc,
							orderid : deliverynoteitemsdata[i].orderid,
							salesperson : deliverynoteitemsdata[i].salesperson,
							orderid_ponumber : deliverynoteitemsdata[i].orderid_ponumber,
							orderid_podate : deliverynoteitemsdata[i].orderid_podate,
							orderid_creditperiod : deliverynoteitemsdata[i].orderid_creditperiod,
							deliverypolicy : deliverynoteitemsdata[i].deliverypolicy,
							deliverynotedate : deliverynoteitemsdata[i].deliverynotedate,
							orderid_paymentterms : deliverynoteitemsdata[i].orderid_paymentterms,
							dcorderno : deliverynoteitemsdata[i].dcorderno,
							dcorderdate : deliverynoteitemsdata[i].dcorderdate,
							dcpodate : deliverynoteitemsdata[i].dcpodate,
							ewaybillno : deliverynoteitemsdata[i].ewaybillno,
							deliverynoteitems : []
						}
						commonMethods.customFieldsOperation(this.props.app.myResources, 'deliverynotes', tempObject, deliverynoteitemsdata[i],'salesinvoices');
						
						deliverynoteitemsdata[i].number = deliverynoteitemsdata[i].quantity;
						deliverynoteitemsdata[i].numberinvoiced = deliverynoteitemsdata[i].invoicedqty;
						deliverynoteitemsdata[i].invoicequantity = deliverynoteitemsdata[i].quantity - deliverynoteitemsdata[i].invoicedqty;
						
						tempObject.deliverynoteitems.push(deliverynoteitemsdata[i]);
						deliverynoteArray.push(tempObject);
					}
				}

				for (var i = 0; i < salesreturnitems.length; i++) {
					let itemFound = false;
					for (var j = 0; j < salesreturnArray.length; j++) {
						if (salesreturnArray[j].id == salesreturnitems[i].salesreturnid) {
							itemFound = true;
							salesreturnArray[j].salesreturnitems.push(salesreturnitems[i]);
							break;
						}
					}
					if (!itemFound) {
						let tempObject = {
							againstorder: salesreturnitems[i].againstorder,
							currencyid: salesreturnitems[i].currencyid,
							currencyid_name: salesreturnitems[i].currencyid_name,
							currencyexchangerate: salesreturnitems[i].currencyexchangerate,
							pricelistid: salesreturnitems[i].pricelistid,
							defaultcostcenter: salesreturnitems[i].defaultcostcenter,
							customerid : salesreturnitems[i].customerid,
							customerid_name : salesreturnitems[i].customername,
							id : salesreturnitems[i].salesreturnid,
							companyid : salesreturnitems[i].companyid,
							companyid_name : salesreturnitems[i].companyid_name,
							salesreturnno : salesreturnitems[i].salesreturnno,
							orderid: salesreturnitems[i].orderid,
							salesreturnitems : []
						}
						tempObject.salesreturnitems.push(salesreturnitems[i]);
						salesreturnArray.push(tempObject);
					}
				}

				this.setState({ orderArray, deliverynoteArray, salesreturnArray });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter = () => {
		let { search } = this.state;
		search = {};
		this.setState({ search });
	}

	tabOnChange = (param) => {
		let isOrderActive = false, isDeliveryNoteActive = false, isSalesReturnActive = false;
		if(param == 'order')
			isOrderActive = true;
		else if(param == 'deliverynote')
			isDeliveryNoteActive = true;
		else if(param == 'salesreturn')
			isSalesReturnActive = true;
		
		this.setState({ isOrderActive, isDeliveryNoteActive, isSalesReturnActive });
	};

	addLimit = (param) => {
		this.state.limit[param] += 10;
		this.setState({limit : this.state.limit});
	};

	parentCheckboxOnChange = (checked, param, arrayname, childname) => {
		this.state[arrayname].forEach((data) => {
			if(data.id == param.id) {
				data.check = checked;
				data[childname].forEach((item) => {
					item.check = checked;
				});
			}
		});
		this.setState({ [arrayname] : this.state[arrayname] });
	};

	itemCheckboxOnChange = (checked, param, childname) => {
		param.check = checked;
		this.setState({ [childname] : this.state[childname] });
	};

	searchFilter = (array) => {
		let tempArr = [];
		array.map((a) => {
			let filtered = true;
			for(var prop in this.state.search) {
				if(this.state.search[prop]) {
					if(a[prop]) {
						if(this.state.search[prop] != a[prop]) {
							filtered = false;
						}
					} else {
						filtered = false;
					}
				}
			}
			if(filtered)
				tempArr.push(a);
		});
		return tempArr;
	};

	filterOnchange = (value, field) => {		
		this.state.search[field] = value;

		[{name: "orderArray", childname: 'orderitems'}, {name: "deliverynoteArray", childname: 'deliverynoteitems'}, {name: "salesreturnArray", childname: "salesreturnitems"}].forEach((itemObj) => {
			this.state[itemObj.name].forEach((data) => {
				data.check = false;
				data[itemObj.childname].forEach((item) => {
					item.check = false;
				});
			});
		});

		this.setState({
			search: this.state.search,
			orderArray: this.state.orderArray,
			deliverynoteArray: this.state.deliverynoteArray,
			salesreturnArray: this.state.salesreturnArray
		});
	};

	createInvoice = () => {
		let { orderArray, deliverynoteArray, salesreturnArray } = this.state;
		let tempObj = {}, tempArray = [];
		if (this.state.isOrderActive) {
			let orderitemNotFound = true, errorFound = false;
			for (var i = 0; i < orderArray.length; i++) {
				for (var j = 0; j < orderArray[i].orderitems.length; j++) {
					if (orderArray[i].orderitems[j].check) {
						if (Object.keys(tempObj).length > 0) {
							if (tempObj.customerid != orderArray[i].orderitems[j].customerid || tempObj.companyid != orderArray[i].orderitems[j].companyid || tempObj.deliverypolicy != orderArray[i].orderitems[j].deliverypolicy || tempObj.currencyid != orderArray[i].orderitems[j].currencyid) {
								errorFound = true;
								break;
							}
						} else 
							tempObj = orderArray[i];

						tempArray.push(orderArray[i].orderitems[j]);
					}
				}
				if (errorFound)
					break;
			}
			if (errorFound) {
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please select order item for same customer,company,currency and delivery policy',
					btnName : ['Ok']
				}));
				orderitemNotFound = false;
			}

			if (orderitemNotFound && !errorFound) {
				if (Object.keys(tempObj).length > 0) {
					tempObj.orderid_ponumber = "";
					let tempOrderId = "";
					for (var i = 0; i < tempArray.length; i++) {
						if (tempOrderId != tempArray[i].orderid) {
							tempObj.orderid_ponumber += (tempArray[i].orderid_ponumber) ? (tempArray[i].orderid_ponumber) + ',' : '';
							tempOrderId = tempArray[i].orderid;
						}
					}
					tempObj.orderid_ponumber = tempObj.orderid_ponumber.substr(0, tempObj.orderid_ponumber.length - 1);
					tempObj.orderid_podate = tempArray[0].orderid_podate;
					tempObj.orderitems = tempArray;

					this.props.history.push({pathname: '/createSalesInvoice', params: {...tempObj}});
				} else {
					this.props.openModal(modalService['infoMethod']({
						header : 'Error',
						body : 'Please choose atleast one item to create sales deliverynote',
						btnName : ['Ok']
					}));
				}
			}
		} else if (this.state.isDeliveryNoteActive) {
			let itemNotFound = true, errorFound = false;
			for (var i = 0; i < deliverynoteArray.length; i++) {
				for (var j = 0; j < deliverynoteArray[i].deliverynoteitems.length; j++) {
					if (deliverynoteArray[i].deliverynoteitems[j].check) {
						if (Object.keys(tempObj).length > 0) {
							if (tempObj.customerid != deliverynoteArray[i].deliverynoteitems[j].customerid || tempObj.companyid != deliverynoteArray[i].deliverynoteitems[j].companyid || tempObj.deliverypolicy != deliverynoteArray[i].deliverynoteitems[j].deliverypolicy || tempObj.currencyid != deliverynoteArray[i].deliverynoteitems[j].currencyid) {
								errorFound = true;
								break;
							}
						} else
							tempObj = deliverynoteArray[i];
						 
						tempArray.push(deliverynoteArray[i].deliverynoteitems[j]);
					}
				}
				if (errorFound)
					break;
			}
			if (errorFound) {
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please select delivery note item for same customer,company,currency and delivery policy',
					btnArray : ['Ok']
				}));
				itemNotFound = false;
			}

			if (itemNotFound && !errorFound) {
				if (Object.keys(tempObj).length > 0) {
					tempObj.customerreference = "";
					let ewaybillno = [];
					let invdeliverynoteno = "", invorderno = "", tempDeliveryId = "";
					for (var i = 0; i < tempArray.length; i++) {
						if (tempDeliveryId != tempArray[i].deliverynoteid) {
							if(tempObj.customerreference.indexOf(tempArray[i].customerreference)<0)
								tempObj.customerreference += (tempArray[i].customerreference) ? (tempArray[i].customerreference) + ',' : '';
							if(invorderno.indexOf(tempArray[i].dcorderno)<0)
								invorderno += (tempArray[i].dcorderno) ? (tempArray[i].dcorderno) + ',' : '';
							invdeliverynoteno += tempArray[i].deliverynotenumber + ',';
							tempDeliveryId = tempArray[i].deliverynoteid;
							if(tempArray[i].ewaybillno && ewaybillno.indexOf(tempArray[i].ewaybillno)<0)
								ewaybillno.push(tempArray[i].ewaybillno);
						}
					}
					tempObj.customerreference = tempObj.customerreference.substr(0, tempObj.customerreference.length - 1);
					tempObj.ewaybillno = ewaybillno.join(', ');
					invorderno = invorderno.substr(0, invorderno.length - 1);
					invdeliverynoteno = invdeliverynoteno.substr(0, invdeliverynoteno.length - 1);
					tempObj.dcorderno = invorderno;
					tempObj.deliverynotenumber = invdeliverynoteno;
					tempObj.deliverynoteitems = tempArray;
					this.props.history.push({pathname: '/createSalesInvoice', params: {...tempObj}});
				} else {
					this.props.openModal(modalService['infoMethod']({
						header : 'Error',
						body : 'Please choose atleast one item to create sales deliverynote',
						btnArray : ['Ok']
					}));
				}
			}
		}
	}

	createCreditnote = () => {
		let { salesreturnArray } = this.state;
		let tempObj = {}, tempArray = [];
		if (this.state.isSalesReturnActive) {
			let itemNotFound = true, errorFound = false;
			for (var i = 0; i < salesreturnArray.length; i++) {
				for (var j = 0; j < salesreturnArray[i].salesreturnitems.length; j++) {
					if (salesreturnArray[i].salesreturnitems[j].check) {
						if (Object.keys(tempObj).length > 0) {
							if ((tempObj.id != salesreturnArray[i].salesreturnitems[j].salesreturnid) || tempObj.customerid != salesreturnArray[i].salesreturnitems[j].customerid || tempObj.companyid != salesreturnArray[i].salesreturnitems[j].companyid || tempObj.currencyid != salesreturnArray[i].salesreturnitems[j].currencyid) {
								errorFound = true;
								break;
							}
						} else
							tempObj = salesreturnArray[i];
						   
						 tempArray.push(salesreturnArray[i].salesreturnitems[j]);
					}
				}
				if (errorFound)
					break;
			}
			if (errorFound) {
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please select one sales return at a time',
					btnArray : ['Ok']
				}));
				itemNotFound = false;
			}

			if (itemNotFound && !errorFound) {
				if (Object.keys(tempObj).length > 0) {
					tempObj.salesreturnitems = tempArray;
					tempObj.param = 'Sales Returns';
					this.props.history.push({pathname: '/createCreditNote', params: {...tempObj}});
				} else {
					this.props.openModal(modalService['infoMethod']({
						header : 'Error',
						body : 'Please choose atleast one item to create credit note',
						btnArray : ['Ok']
					}));
				}
			}
		}
	};

	render() {
		let filteredOrderArray = this.searchFilter(this.state.orderArray, this.state.search);
		let filteredDeliveryNoteArray = this.searchFilter(this.state.deliverynoteArray, this.state.search);
		let filteredSalesReturnArray = this.searchFilter(this.state.salesreturnArray, this.state.search);

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="form-group col-md-12 bg-white report-header">
						<div className="report-header-title">Sales Invoice Tool</div>
						<div className="report-header-rightpanel">
							{(this.state.isDeliveryNoteActive || this.state.isOrderActive) ? <button type="button" onClick={this.createInvoice} className="btn btn-sm gs-btn-success btn-width"><i className="fa fa-plus"></i>Invoice</button> : null }
							{this.state.isSalesReturnActive ? <button type="button" onClick={this.createCreditnote} className="btn btn-sm gs-btn-success btn-width"><i className="fa fa-plus"></i>Credit Note</button> : null }
						</div>
					</div>
					<div className="form-group col-md-12 bg-white paddingright-5">
						<div className="row">
							<div className="form-group col-md-3 col-sm-12">
								<label className="labelclass">Partner</label>
								<AutoSelect resource={'partners'} fields={'id,name,displayname'} value={this.state.search.customerid} label={'displayname'} valuename={'id'} onChange={(value, valueobj) => this.filterOnchange(value, 'customerid')} />
							</div>
							<div className="form-group col-md-3 col-sm-12" >
								<button type="button" onClick={() => {
									this.resetFilter()	
								}} className="btn btn-width btn-sm gs-btn-info margintop-25"><i className="fa fa-refresh"></i>Reset</button>
							</div>
						</div>
					</div>

					<div className="form-group col-md-12 bg-white paddingright-5">
						<div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								<ul className="nav nav-tabs">
									<li className={`nav-item nav-link ${this.state.isDeliveryNoteActive ? 'active' : ''}`} onClick={() => this.tabOnChange('deliverynote')}>
										<i className="fa fa-list-alt"></i>
										<span className="marginleft-10">Delivery Notes</span>
										<span className="badge badge-secondary marginleft-10">{filteredDeliveryNoteArray.length}</span>
									</li>
									<li className={`nav-item nav-link ${this.state.isOrderActive ? 'active' : ''}`} onClick={() => this.tabOnChange('order')}>
										<i className="fa fa-thumbs-up"></i>
										<span className="marginleft-10">Orders</span>
										<span className="badge badge-secondary marginleft-10">{filteredOrderArray.length}</span>
									</li>
									<li className={`nav-item nav-link ${this.state.isSalesReturnActive ? 'active' : ''}`} onClick={() => this.tabOnChange('salesreturn')}>
										<i className="fa fa-list-alt"></i>
										<span className="marginleft-10">Sales Returns</span>
										<span className="badge badge-secondary marginleft-10">{filteredSalesReturnArray.length}</span>
									</li>
								</ul>

								<div className="tab-content">
									<div className={`tab-pane fade ${this.state.isDeliveryNoteActive ? 'show active' : ''}`}>
										<div className="row margintop-25">
											{filteredDeliveryNoteArray.map((deliverynotedata, deliverynotedataindex) => {
												if(this.state.limit.deliverynote > deliverynotedataindex) {
													return(
														<div className="form-group col-md-12 col-sm-12 col-xs-12" key={deliverynotedataindex}>
															<div className="card border-info">
																<div className="card-header">
																	<div className="float-left">
																		<span className="marginleft-11">
																			<input type="checkbox" checked={deliverynotedata.check || false} onChange={(evt) => this.parentCheckboxOnChange(evt.target.checked, deliverynotedata, 'deliverynoteArray', 'deliverynoteitems')} />
																		</span>
																		<a href={`#/details/deliverynotes/${deliverynotedata.id}`} className="marginleft-25">{deliverynotedata.deliverynotenumber}</a>
																	</div>
																	<div className="float-left">
																		<span> | Customer :   </span>
																		<span>{deliverynotedata.customerid_name}</span>
																		<span> | Currency : </span>
																		<span>{this.props.app.currency[deliverynotedata.currencyid].name}</span>
																		<label className="badge badge-info marginleft-10">{deliverynotedata.deliverypolicy}</label>
																	</div>
																</div>
																<div className="card-body padding-0">
																	<div className="table-responsive">
																		<table className="table table-sm marginbottom-0">
																			<thead>
																				<tr>
																					<th></th>
																					<th className="text-center">Item</th>
																					<th className="text-center">Delivered Qty</th>
																					<th className="text-center">Invoiced Qty</th>
																					<th className="text-center">Qty to Invoice</th>
																				</tr>
																			</thead>
																			{deliverynotedata.deliverynoteitems ? <tbody>
																				{deliverynotedata.deliverynoteitems.map((deliverynoteitem, deliverynoteitemindex) => {
																					return (
																						<tr key={deliverynoteitemindex}>
																							<td className='text-center' style={{width: '5%'}}><input type="checkbox" checked={deliverynoteitem.check || false} onChange={(evt) => this.itemCheckboxOnChange(evt.target.checked, deliverynoteitem, 'deliverynoteitems')}/></td>
																							<td className="text-center">{deliverynoteitem.itemid_name}</td>
																							<td className="text-center">{deliverynoteitem.number}</td>
																							<td className="text-center">{deliverynoteitem.numberinvoiced || 0}</td>
																							<td className="text-center width-20">{deliverynoteitem.invoicequantity}</td>
																						</tr>
																					)
																				})}
																			</tbody> : null}
																		</table>
																	</div>
																</div>
															</div>
														</div>
													)
												}
											})}

											{filteredDeliveryNoteArray.length > this.state.limit.deliverynote ? <div className="col-md-12 col-sm-12 text-center">
												<button type="button" className="btn btn-sm gs-btn-warning" onClick={() => this.addLimit("deliverynote")}>Show More</button>
											</div> : null }
										</div>
									</div>

									<div className={`tab-pane fade ${this.state.isOrderActive ? 'show active' : ''}`}>
										<div className="row margintop-25">
											{filteredOrderArray.map((orderdata, orderdataindex) => {
												if(this.state.limit.order > orderdataindex) {
													return(
														<div className="form-group col-md-12 col-sm-12 col-xs-12" key={orderdataindex}>
															<div className="card border-info">
																<div className="card-header">
																	<div className="float-left">
																		<span className="marginleft-11">
																			<input type="checkbox" checked={orderdata.check || false} onChange={(evt) => this.parentCheckboxOnChange(evt.target.checked, orderdata, 'orderArray', 'orderitems')} />
																		</span>
																		<a href={`#/details/orders/${orderdata.id}`} className="marginleft-25">{orderdata.orderno}</a>
																	</div>
																	<div className="float-left">
																		<span> | Customer :   </span>
																		<span>{orderdata.customerid_name}</span>
																		<span> | Currency : </span>
																		<span>{this.props.app.currency[orderdata.currencyid].name}</span>
																		<label className="badge badge-info marginleft-10">{orderdata.deliverypolicy}</label>
																	</div>
																</div>
																<div className="card-body padding-0">
																	<div className="table-responsive">
																		<table className="table table-sm marginbottom-0">
																			<thead>
																				<tr>
																					<th></th>
																					<th className="text-center">Item</th>
																					<th className="text-center">Ordered Qty</th>
																					<th className="text-center">Invoiced Qty</th>
																					<th className="text-center">Qty to deliverynote</th>
																				</tr>
																			</thead>
																			{orderdata.orderitems ? <tbody>
																				{orderdata.orderitems.map((orderitem, orderitemindex) => {
																					return (
																						<tr key={orderitemindex}>
																							<td className='text-center' style={{width: '5%'}}><input type="checkbox" checked={orderitem.check || false} onChange={(evt) => this.itemCheckboxOnChange(evt.target.checked, orderitem, 'orderitems')}/></td>
																							<td className="text-center">{orderitem.itemid_name}</td>
																							<td className="text-center">{orderitem.number}</td>
																							<td className="text-center">{orderitem.numberinvoiced || 0}</td>
																							<td className="text-center width-20">{orderitem.invoicequantity}</td>
																						</tr>
																					)
																				})}
																			</tbody> : null}
																		</table>
																	</div>
																</div>
															</div>
														</div>
													)
												}
											})}

											{filteredOrderArray.length > this.state.limit.order ?<div className="col-md-12 col-sm-12 text-center">
												<button type="button" className="btn btn-sm gs-btn-warning" onClick={() => this.addLimit("order")}>Show More</button>
											</div> : null }
										</div>
									</div>

									<div className={`tab-pane fade ${this.state.isSalesReturnActive ? 'show active' : ''}`}>
										<div className="row margintop-25">
											{filteredSalesReturnArray.map((salesreturndata, salesreturndataindex) => {
												if(this.state.limit.salesreturn > salesreturndataindex) {
													return(
														<div className="form-group col-md-12 col-sm-12 col-xs-12" key={salesreturndataindex}>
															<div className="card border-info">
																<div className="card-header">
																	<div className="float-left">
																		<span className="marginleft-11">
																			<input type="checkbox" checked={salesreturndata.check || false} onChange={(evt) => this.parentCheckboxOnChange(evt.target.checked, salesreturndata, 'salesreturnArray', 'salesreturnitems')} />
																		</span>
																		<a href={`#/details/salesreturns/${salesreturndata.id}`} className="marginleft-25">{salesreturndata.salesreturnno}</a>
																	</div>
																	<div className="float-left">
																		<span> | Customer :   </span>
																		<span>{salesreturndata.customerid_name}</span>
																	</div>
																</div>
																<div className="card-body padding-0">
																	<div className="table-responsive">
																		<table className="table table-sm marginbottom-0">
																			<thead>
																				<tr>
																					<th></th>
																					<th className="text-center">Item</th>
																					<th className="text-center">Delivered Qty</th>
																					<th className="text-center">Invoiced Qty</th>
																					<th className="text-center">Qty to deliverynote</th>
																				</tr>
																			</thead>
																			{salesreturndata.salesreturnitems ? <tbody>
																				{salesreturndata.salesreturnitems.map((salesreturnitem, salesreturnitemindex) => {
																					return (
																						<tr key={salesreturnitemindex}>
																							<td className='text-center' style={{width: '5%'}}><input type="checkbox" checked={salesreturnitem.check || false} disabled={true} onChange={(evt) => this.itemCheckboxOnChange(evt.target.checked, salesreturnitem, 'salesreturnitems')}/></td>
																							<td className="text-center">{salesreturnitem.itemid_name}</td>
																							<td className="text-center">{salesreturnitem.qtyreturned}</td>
																							<td className="text-center">{salesreturnitem.qtycredited || 0}</td>
																							<td className="text-center width-20">{salesreturnitem.invoicequantity}</td>
																							
																						</tr>
																					)
																				})}
																			</tbody> : null}
																		</table>
																	</div>
																</div>
															</div>
														</div>
													)
												}
											})}

											{filteredSalesReturnArray.length > this.state.limit.salesreturn ? <div className="col-md-12 col-sm-12 text-center">
												<button type="button" className="btn btn-sm gs-btn-warning" onClick={() => this.addLimit("salesreturn")}>Show More</button>
											</div> : null }
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</>
		);
	};
}



SalesInvoicePlannerReportForm = connect((state) =>{
	return {app: state.app}
}) (SalesInvoicePlannerReportForm);

export default SalesInvoicePlannerReportForm;
