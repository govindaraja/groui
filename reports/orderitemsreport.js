import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, BIReportSelectField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import ReactBIChart from '../components/reportbicomponents';
import { currencyFilter, booleanfilter, taxFilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../utils/filter';

class OrderItemsReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showMoreFilter: false,
			filterToggleOpen: true,
			orderItemColumns: [{
				"name" : "Order No",
				"key" : "orderno",
				"format" : "anchortag",
				"transactionname" : "orders",
				"transactionid" : "id",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Order Date",
				"key" : "orderdate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "PO Number",
				"key" : "ponumber",
				"width" : 250
			}, {
				"name" : "PO Date",
				"key" : "podate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Customer",
				"key" : "customer",
				"width" : 250
			}, {
				"name" : "Territory",
				"key" : "territoryname",
				"width" : 180
			}, {
				"name" : "Item Name",
				"key" : "itemname",
				"width" : 250
			}, {
				"name" : "Order Qty",
				"key" : "quantity",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "Delivered Qty",
				"key" : "deliveredqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "Pending Qty",
				"key" : "pendingdeliveryqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "Invoiced Qty",
				"key" : "invoicedqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "Closed Qty",
				"key" : "closedqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "UOM",
				"key" : "uom",
				"width" : 150
			}, {
				"name" : "Currency",
				"key" : "currencyname",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Currency Exchange Rate",
				"key" : "currencyexchangerate",
				"if" : this.props.app.feature.useMultiCurrency,
				"width" : 180
			}, {
				"name" : "Rate",
				"key" : "rate",
				"format" : "currency",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Discount",
				"key" : "discountquantity",
				"format" : "discount",
				"width" : 150
			}, {
				"name" : "Value Before Discount",
				"key" : "valuebeforediscount",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Discount Value",
				"key" : "discountvalue",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Value Before Tax",
				"key" : "amount",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Pending Delivery Value",
				"key" : "pendingdeliveryvalue",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Pending Delivery Value (Local Currency)",
				"key" : "pendingdeliveryvaluelc",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Pending Invoice Value",
				"key" : "pendinginvoicevalue",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Value (Local Currency)",
				"key" : "amountlc",
				"format" : "defaultcurrency",
				"if" : this.props.app.feature.useMultiCurrency,
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Value After Tax",
				"key" : "amountwithtax",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Delivery Date",
				"key" : "deliverydate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 200
			}, {
				"name" : "Item Description",
				"key" : "description",
				"width" : 300
			}, {
				"name" : "Contact Person",
				"key" : "contactperson",
				"width" : 150
			}, {
				"name" : "Mobile No",
				"key" : "mobile",
				"width" : 150
			}, {
				"name" : "Status",
				"key" : "status",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Sales Person",
				"key" : "salesperson",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Warehouse",
				"key" : "warehouseid_name",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Item Category",
				"key" : "itemcategorymasterid_name",
				"width" : 180
			}, {
				"name" : "Item Group",
				"key" : "itemgroupid_fullname",
				"width" : 180
			}, {
				"name" : "Remarks",
				"key" : "remarks",
				"width" : 180
			}, {
				"name" : "Customer group",
				"key" : "customergroupid_name",
				"width" : 180
			}, {
				"name" : "Industry",
				"key" : "industryid_name",
				"width" : 150
			}, {
				"name" : "Delivery Policy",
				"key" : "deliverypolicy",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Numbering Series",
				"key" : "numberingseriesmasterid_name",
				"width" : 160
			}, {
				"name" : "Tax",
				"key" : "taxid",
				"format" : "taxFilter",
				"width" : 180
			}, {
				"name" : "Customer's Legal Name",
				"key" : "customerid_legalname",
				"width" : 250
			}],
			orderColumns: [{
				"name" : "Order No",
				"key" : "orderno",
				"format" : "anchortag",
				"transactionname" : "orders",
				"transactionid" : "id",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Order Date",
				"key" : "orderdate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Customer",
				"key" : "customer",
				"width" : 250
			}, {
				"name" : "PO Number",
				"key" : "ponumber",
				"width" : 250
			}, {
				"name" : "PO Date",
				"key" : "podate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Sales Person",
				"key" : "salesperson",
				"width" : 150
			}, {
				"name" : "Value Before Tax",
				"key" : "basictotal",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Value (Local Currency)",
				"key" : "basictotallc",
				"format" : "defaultcurrency",
				"if" : this.props.app.feature.useMultiCurrency,
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Value After Tax",
				"key" : "totalaftertax",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Status",
				"key" : "status",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Delivered Percent (%)",
				"key" : "deliveredpercent",
				"format" : "number",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Invoiced Percent (%)",
				"key" : "invoicedpercent",
				"format" : "number",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Territory",
				"key" : "territoryname",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Customer Group",
				"key" : "customergroupid_name",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Delivery Date",
				"key" : "deliverydate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Currency",
				"key" : "currencyname",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Currency Exchange Rate",
				"key" : "currencyexchangerate",
				"if" : this.props.app.feature.useMultiCurrency,
				"width" : 180
			}, {
				"name" : "Price List",
				"key" : "pricelistid_name",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Industry",
				"key" : "industryid_name",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Delivery Policy",
				"key" : "deliverypolicy",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Mobile No",
				"key" : "mobile",
				"width" : 150
			}, {
				"name" : "Numbering Series",
				"key" : "numberingseriesmasterid_name",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Customer's Legal Name",
				"key" : "customerid_legalname",
				"width" : 250
			}]
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateMoreFilter = this.updateMoreFilter.bind(this);
		this.resourceOnChange = this.resourceOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.configOnChange = this.configOnChange.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	updateMoreFilter() {
		this.setState({showMoreFilter : true});
	}

	onLoad() {
		let { orderColumns, orderItemColumns } = this.state;
		let fromdate = new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)));
		let fromDate = new Date(fromdate.setDate(fromdate.getDate() + 1));
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : fromDate,
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				period : 'Last month',
				resource : 'orderitems',
				reporttype : 'Normal',
				show : 'All',
				status : ["Approved"],
				filters: {},
				hiddencols: [],
				columns: [],
				pivotobj: {},
				datefilter : "Transaction Date"
			};

		customfieldAssign(orderColumns, orderItemColumns, 'orders', this.props.app.myResources, true);
		customfieldAssign(orderItemColumns, null, 'orderitems', this.props.app.myResources, true);

		this.setState({orderColumns, orderItemColumns});

		this.props.initialize(tempObj);

		setTimeout(() => {
			if(this.props.reportdata)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let { orderColumns, orderItemColumns } = this.state;

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['companyid', 'fromdate', 'todate', 'itemid', 'itemcategoryid', 'customergroupid', 'resource', 'show', 'reporttype', 'configid', 'team', 'itemgroupid', 'numberingseriesmasterid', 'status', 'datefilter'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/orderitemsreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns: this.props.resource.resource == 'orders' ? orderColumns : orderItemColumns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			period : 'Custom',
			resource : null,
			reporttype : null,
			status : null,
			show : null,
			customergroupid : null,
			itemid : null,
			itemgroupid : null,
			itemcategoryid : null,
			team : null,
			numberingseriesmasterid : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: [],
			pivotobj: {},
			datefilter : null
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	resourceOnChange() {
		this.props.updateFormState(this.props.form, {
			configid : null,
			configid_name : null,
			pivotobj: {},
			originalRows : [],
			columns : []
		});
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	configOnChange = (value, valueobj) => {
		let tempObj = {
			originalRows: [],
			columns : [],
			pivotobj: {}
		};
		for (var prop in valueobj.config) {
			tempObj.pivotobj[prop] = valueobj.config[prop];
		}
		tempObj.configid_name = valueobj.name;
		this.props.updateFormState(this.props.form, tempObj);
	};

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Sales Order Items Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.reporttype != "BI" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
								{this.props.resource.reporttype == "BI" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.pivot.print()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-print"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Report Data</label>
										<Field name={'resource'} props={{options: [{value: "orders", label: "Sales Orders"}, {value: "orderitems", label: "Sales Order Items"}], label:"label", valuename: "value", required: true, onChange: () => {this.resourceOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Report Data'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Report Type</label>
										<Field name={'reporttype'} props={{options: [{value: "Normal", label: "Normal"}, {value: "BI", label: "Analytics"}], label : "label", valuename: "value", required: true, onChange: () => {this.resourceOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Report Type'})]} />
									</div>
									{this.props.resource.reporttype == "BI" ? <div className="form-group col-md-12 col-sm-12">
										<label>Report Name</label>
										<BIReportSelectField parentobj={this.props.resource} reportname='orderitemsreport' onChangeFn={this.configOnChange} updateFormState={this.props.updateFormState} form={this.props.form} createOrEdit={this.props.createOrEdit}  />
									</div> : null}
									<ReportDateRangeField model={["period", "fromdate", "todate"]} updateFormState={this.props.updateFormState} form={this.props.form} resource={this.props.resource} required={"resource.show=='All'"}/>

									{!this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<span className="gs-anchor" onClick={this.updateMoreFilter}>More Filters</span>
									</div> : null }
									{this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Date Filter Based On</label>
										<Field name={'datefilter'} props={{options: ["Transaction Date", "Delivery Date"], required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Date Filter Based On'})]} />
									</div> : null }
									{this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Status</label>
										<Field name={'status'} props={{options: ["Draft", "Submitted", "Revise", "Approved", "Hold", "Cancelled"], multiselect: true}} component={localSelectEle} />
									</div> : null }
									{this.state.showMoreFilter && this.props.resource.resource == "orderitems" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: ["All", "Pending Delivery", "Pending Invoice"], required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Show'})]} />
									</div> : null }
									{this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer Group</label>
										<Field name={'customergroupid'} props={{resource: "customergroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div> : null }
									{this.state.showMoreFilter && this.props.resource.resource == "orderitems" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Name</label>
										<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name"}} component={autoSelectEle} />
									</div> : null }
									{this.state.showMoreFilter && this.props.resource.resource == "orderitems" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Group</label>
										<Field name={'itemgroupid'} props={{resource: "itemgroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div> : null }
									{this.state.showMoreFilter && this.props.resource.resource == "orderitems" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div> : null }
									{this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Team</label>
										<Field name={'team'} props={{resource: "teamstructure", fields: "id,fullname", label: "fullname"}} component={selectAsyncEle} />
									</div> : null }
									{this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Numbering Series</label>
										<Field name={'numberingseriesmasterid'} props={{resource: "numberingseriesmaster", fields: "id,name,format,isdefault,currentvalue", filter: "numberingseriesmaster.resource='orders'"}} component={selectAsyncEle} />
									</div> : null }
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.reporttype != "BI" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname={`${this.props.resource.resource == 'orderitems' ? 'Sales Order Items Report' : 'Sales Order Report'}`} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }

							{this.props.resource.reporttype == "BI" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <div className='report-biheader-title'>{this.props.resource.configid_name}</div> : null }
							{this.props.resource.reporttype == "BI" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && this.props.resource.configid > 0 ? <ReactBIChart chartprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} app={this.props.app} ref="pivot" /> : null}
						</div>
					</div>
				</form>
			</>
		);
	};
}

OrderItemsReportForm = connect(
	(state, props) => {
		let formName = 'orderitemsreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(OrderItemsReportForm));

export default OrderItemsReportForm;
