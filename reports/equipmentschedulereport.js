import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import { AutoSelect, SelectAsync } from '../components/utilcomponents';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class EquipmentScheduleReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.openServiceCallModal = this.openServiceCallModal.bind(this);
		this.saveEquipSchedule = this.saveEquipSchedule.bind(this);
		this.openSuspendRemarksModal = this.openSuspendRemarksModal.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : null,
				todate : null,
				contracttype : 'All',
				status : ['Pending'],
				filters: {},
				hiddencols: [],
				equipmentschedules: [],
				preferences: "",
				columns: [{
					"name" : "",
					"headerformat" : "checkbox",
					"key" : "ischecked",
					"locked" : true,
					"format" : "checkbox",
					"cellClass" : "text-center",
					"onChange" : "{report.checkboxOnChange}",
					"showfield" : "item.status == 'Pending'",
					"restrictToExport" : true,
					"width" : 100
				}, {
					"name" : "Status",
					"key" : "status",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Customer",
					"key" : "customername",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Schedule No",
					"key" : "scheduleno",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Schedule Date",
					"key" : "equipmentscheduledate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Done Date",
					"key" : "donedate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Last Schedule Completed On",
					"key" : "lastcompletedschedules",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 220
				}, {
					"name" : "Contract",
					"key" : "contractno",
					"format" : "anchortag",
					"transactionname" : "contracts",
					"transactionid" : "contractid",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Contract Type",
					"key" : "contracttype",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Expiry Date",
					"key" : "expiredate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Contract Status",
					"key" : "contractstatus",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Equipment",
					"key" : "equipmentname",
					"width" : 200
				}, {
					"name" : "Address Type",
					"key" : "addresstype",
					"width" : 180
				}, {
					"name" : "First Line Address",
					"key" : "firstline",
					"width" : 180
				}, {
					"name" : "Second Line Address",
					"key" : "secondline",
					"width" : 180
				}, {
					"name" : "Street",
					"key" : "street",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "City",
					"key" : "city",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Pin Code",
					"key" : "pincode",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Territory",
					"key" : "territoryname",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Location",
					"key" : "equipmentlocation",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Service Call No",
					"key" : "servicecallno",
					"format" : "anchortag",
					"transactionname" : "servicecalls",
					"transactionid" : "servicecallid",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Remarks",
					"key" : "remarks",
					"width" : 200
				}, {
					"name" : "Suspend Remarks",
					"key" : "suspendremarks",
					"width" : 200
				}]
			};

		this.props.initialize(tempObj);

		setTimeout(() => {
			if(this.props.reportdata)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('equipmentschedules');

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['fromdate', 'todate', 'status', 'contracttype', 'customerid', 'contractstatus'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/equipmentschedulequery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols,
					suspendremarks: "",
					preferences: ""
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			customerid : null,
			contracttype : null,
			status : null,
			preferences: null,
			suspendremarks: null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			contractstatus : null,
			equipmentschedules: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	checkboxOnChange(value, item) {
		let equipmentschedules = [];

		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		setTimeout(() => {
			this.props.resource.originalRows.forEach((rowItem, index) => {
				if(rowItem.ischecked && (rowItem.status != 'Suspended' && rowItem.status != 'Inprogress' && rowItem.status != 'Completed')) {
					equipmentschedules.push(rowItem);
				}
			});
			this.props.updateFormState(this.props.form, {
				equipmentschedules
			});
		}, 0);
		this.refs.grid.refresh();
	}

	checkboxHeaderOnChange(param) {
		let equipmentschedules = [];
		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if(param && (item.status != 'Suspended' && item.status != 'Inprogress' && item.status != 'Completed')) {
				item.ischecked = true;
				equipmentschedules.push(item);
			}
		});
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows,
			equipmentschedules
		});
		this.refs.grid.forceRefresh();
	}

	openServiceCallModal () {
		this.props.openModal({
			render: (closeModal) => {
				return <ServicecallOptionModal resource={this.props.resource} callback={(preferences, numberingseriesmasterid) => {
					this.props.updateFormState(this.props.form, { preferences, numberingseriesmasterid });
					setTimeout(() => {
						this.saveEquipSchedule('Service Call');
					}, 0);
				}} app={this.props.app} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	openSuspendRemarksModal () {
		this.props.openModal({
			render: (closeModal) => {
				return <SuspendRemarksModal resource={this.props.resource} callback={(remarks) => {
					this.props.updateFormState(this.props.form, {
						suspendremarks: remarks
					});
					setTimeout(() => {
						this.saveEquipSchedule('Suspend');
					}, 0);
				}} app={this.props.app} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	saveEquipSchedule (param) {
		this.updateLoaderFlag(true);
		let tempObj = { ...this.props.resource };
		delete tempObj.columns;
		delete tempObj.originalRows;
		delete tempObj.hiddencols;

		if(param == 'Suspend') {
			axios({
				method : 'post',
				data :  {
					actionverb : param,
					data : tempObj
				},
				url : '/api/equipmentschedules'
			}).then((response) => {
				if (response.data.message == 'success') {
					this.getReportData();
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
				this.updateLoaderFlag(false);
			});
		} else {
			axios({
				method : 'post',
				data : {
					actionverb : 'Inprogress',
					data : tempObj
				},
				url : '/api/equipmentschedules'
			}).then((response) => {
				if (response.data.message == 'success') {
					this.props.openModal(modalService['infoMethod']({
						header : 'Success',
						body : 'Service Call No(s) ' + response.data.main.join() + ' created successfully!!!',
						btnArray : ['Ok']
					}));
					this.getReportData();
				} else {
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
				this.updateLoaderFlag(false);
			});
		}
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title float-left">Equipment Schedule</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							{this.props.resource.equipmentschedules.length > 0 ? <div className="report-header-rightpanel">
								<button type="button" className={`btn gs-btn-success btn-sm btn-width ${checkActionVerbAccess(this.props.app, 'equipmentschedules', 'Save') ? '' : 'hide'}`} onClick={this.openSuspendRemarksModal}><i className="fa fa-plus"></i>Suspend</button>
								<button type="button" className={`btn gs-btn-success btn-sm btn-width ${checkActionVerbAccess(this.props.app, 'servicecalls', 'Save') ? '' : 'hide'}`} onClick={this.openServiceCallModal}><i className="fa fa-plus"></i>Service Call</button>
							</div> : null}
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Schedule Status</label>
										<Field name={'status'} props={{options:["Pending", "Inprogress", "Suspended", "Completed"], multiselect: true, required: true}} component={localSelectEle}  validate={[stringNewValidation({required: true, model: 'Schedule Status'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Schedule Date From</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  '{resource.status.length > 1 || resource.status.indexOf("Pending") == -1}',title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Schedule Date To</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: '{resource.status.length > 1 || resource.status.indexOf("Pending") == -1 || resource.fromdate}', title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Contract Type</label>
										<Field name={'contracttype'} props={{options:["All", "AMC", "Warranty"], required: true}} component={localSelectEle}  validate={[stringNewValidation({required: true, model: 'Contract Type'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer</label>
										<Field name={'customerid'} props={{resource: "partners", fields: "id,name,displayname", label: "displayname", displaylabel: "name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Contract Status</label>
										<Field name={'contractstatus'} props={{options:["Active", "Upcoming", "Expired"]}} component={localSelectEle}  validate={[stringNewValidation({model: 'Contract Status'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Equipment Schedule Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

class ServicecallOptionModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			numberingseriesmasterid: null
		};
		this.createServiceCall = this.createServiceCall.bind(this);
		this.radioBtnOnChange = this.radioBtnOnChange.bind(this);
	}

	componentWillMount() {
		this.state.preferences = this.props.resource.preferences;
	}

	createServiceCall() {
		this.props.callback(this.state.preferences, this.state.numberingseriesmasterid);
		this.props.closeModal();
	}

	radioBtnOnChange(evt) {
		this.setState({
			preferences: evt.target.value
		});
	}

	filterOnchange(numberingseriesmasterid) {
		this.setState({
			numberingseriesmasterid
		});	
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">PMS Call Creation Preference</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-12">
							<div className="form-check col-md-3 col-sm-12">
								<label>Choose Series Type</label>
								<SelectAsync 
									resource="numberingseriesmaster" 
									fields="id,name,format,isdefault,currentvalue"
									className={this.state.numberingseriesmasterid ? '' : 'errorinput'}
									value={this.state.numberingseriesmasterid}
									label="name" 
									valuename="id"
									createParamFlag={true}
									filter="numberingseriesmaster.resource='servicecalls'" 
									defaultValueUpdateFn={(value, valueobj) => this.filterOnchange(value, valueobj)}
									onChange={(value, valueobj) => this.filterOnchange(value, valueobj)}
								/>
							</div>
							<div className="form-check">
								<input className="form-check-input" type="radio" id="schedulelevel" value="Schedule Level" checked={this.state.preferences == 'Schedule Level'} onChange={this.radioBtnOnChange} />
								<label htmlFor="schedulelevel">
									Create one call for each schedule
								</label>
							</div>
							<div className="form-check">
								<input className="form-check-input" type="radio" id="customerlevel" value="Customer Level" checked={this.state.preferences == 'Customer Level'} onChange={this.radioBtnOnChange} />
								<label htmlFor="customerlevel">
									Merge schedules at customer level and create one call for each customer
								</label>
							</div>
							<div className="form-check">
								<input className="form-check-input" type="radio" id="customercontractlevel" value="Customer Contract Level" checked={this.state.preferences == 'Customer Contract Level'} onChange={this.radioBtnOnChange} />
								<label htmlFor="customercontractlevel">
									Merge scehdules at customer and contract type level and create one call for each customer and contract type combination
								</label>
							</div>
							<div className="form-check">
								<input className="form-check-input" type="radio" id="customeraddresslevel" value="Customer Address Level" checked={this.state.preferences == 'Customer Address Level'} onChange={this.radioBtnOnChange} />
								<label htmlFor="customeraddresslevel">
									Merge scehdules at customer and address level and create one call for each customer and address combination
								</label>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.createServiceCall} disabled={!this.state.preferences || !this.state.numberingseriesmasterid}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class SuspendRemarksModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.suspendSchedule = this.suspendSchedule.bind(this);
		this.inputOnChange = this.inputOnChange.bind(this);
	}

	suspendSchedule() {
		this.props.callback(this.state.remarks);
		this.props.closeModal();
	}

	inputOnChange(evt) {
		this.setState({
			remarks: evt.target.value
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Remarks for Suspend</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-10 offset-md-1">
							<textarea className={`form-control ${!this.state.remarks ? 'errorinput' : ''}`} value={this.state.remarks} onChange={this.inputOnChange} required />
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={this.suspendSchedule} disabled={!this.state.remarks}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

EquipmentScheduleReportForm = connect(
	(state, props) => {
		let formName = 'equipmentschedulereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(EquipmentScheduleReportForm));

export default EquipmentScheduleReportForm;
