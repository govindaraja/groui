import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateFilterField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class SalesActivityReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				searchbasedon : 'startdatetime',
				filter : 'All',
				status : [],
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Created On",
					"key" : "created",
					"format": "date",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "Created By",
					"key" : "createdby_displayname",
					"width" : 150
				}, {
					"name" : "Assigned To",
					"key" : "assignedto_displayname",
					"width" : 150
				}, {
					"name" : "Activity",
					"key" : "activitytypeid_name",
					"width" : 150
				}, {
					"name" : "Completion Remarks",
					"key" : "completionremarks",
					"width" : 300
				}, {
					"name" : "Status",
					"key" : "status",
					"width" : 150
				}, {
					"name" : "Customer",
					"key" : "customer",
					"width" : 200
				}, {
					"name" : "Territory",
					"key" : "territoryname",
					"width" : 180
				}, {
					"name" : "Contact Person",
					"key" : "contactperson",
					"width" : 200
				}, {
					"name" : "Mobile",
					"key" : "contactmobile",
					"width" : 150
				}, {
					"name" : "Organization",
					"key" : "contactorganization",
					"width" : 150
				}, {
					"name" : "Lead No",
					"key" : "leadno",
					"format" : "anchortag",
					"transactionname" : "leads",
					"transactionid" : "lead_id",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Subject",
					"key" : "subject",
					"width" : 300
				}, {
					"name" : "Stage",
					"key" : "leads_stage",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Priority",
					"key" : "priority",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Leads Status",
					"key" : "leadstatus",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Quote No",
					"key" : "quoteno",
					"format" : "anchortag",
					"transactionname" : "quotes",
					"transactionid" : "quote_id",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Quote Date",
					"key" : "quotedate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Quote Value",
					"key" : "finaltotal",
					"format" : "currency",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Quote Status",
					"key" : "quotestatus",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Contract Enquiry No",
					"key" : "contractenquiryno",
					"format" : "anchortag",
					"transactionname" : "contractenquiries",
					"transactionid" : "contractenquiry_id",
					"if" : this.props.app.feature.useServiceFlow,
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Contract Enquiry Date",
					"key" : "contractenquirydate",
					"format" : "date",
					"if" : this.props.app.feature.useServiceFlow,
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Contract Enquiry Value",
					"key" : "contractenquiryvalue",
					"format" : "currency",
					"if" : this.props.app.feature.useServiceFlow,
					"cellClass" : "text-right",
					"width" : 200
				}, {
					"name" : "Contract Enquiry Status",
					"key" : "contractenquirystatus",
					"if" : this.props.app.feature.useServiceFlow,
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Project Quote No",
					"key" : "projectquoteid_quoteno",
					"format" : "anchortag",
					"transactionname" : "projectquotes",
					"transactionid" : "projectquoteid",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Project Quote Date",
					"key" : "projectquoteid_quotedate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 170
				}, {
					"name" : "Project Quote Value",
					"key" : "projectquoteid_finaltotal",
					"format" : "currency",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Project Quote Status",
					"key" : "projectquoteid_status",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Sales Person",
					"key" : "salesperson_displayname",
					"width" : 150
				}, {
					"name" : "Industry",
					"key" : "industryid_name",
					"width" : 150
				}, {
					"name" : "Planned Start Time",
					"key" : "plannedstarttime",
					"format" : "datetime",
					"cellClass" : "text-center",
					"width" : 230
				}, {
					"name" : "Planned End Time",
					"key" : "plannedendtime",
					"format" : "datetime",
					"cellClass" : "text-center",
					"width" : 230
				}, {
					"name" : "Due Date",
					"key" : "duedate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 230
				}, {
					"name" : "Travel Start Time",
					"key" : "travelstarttime",
					"format" : "datetime",
					"cellClass" : "text-center",
					"width" : 230
				}, {
					"name" : "Travel End Time",
					"key" : "travelendtime",
					"format" : "datetime",
					"cellClass" : "text-center",
					"width" : 230
				}, {
					"name" : "Travel Duration",
					"key" : "travelduration",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "Driving Distance",
					"key" : "drivingdistance",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "Start Date Time",
					"key" : "startdatetime",
					"format" : "location",
					"showfield" : "item.checkindetails",
					"fieldname" : "startdatetime",
					"fieldformat" : "datetime",
					"locationobj" : "checkindetails",
					"cellClass" : "text-center",
					"width" : 230
				}, {
					"name" : "End Date Time",
					"key" : "enddatetime",
					"format" : "location",
					"showfield" : "item.checkoutdetails",
					"fieldname" : "enddatetime",
					"fieldformat" : "datetime",
					"locationobj" : "checkoutdetails",
					"cellClass" : "text-center",
					"width" : 230
				}, {
					"name" : "Completed On",
					"key" : "completedon",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 230
				}, {
					"name" : "Duration",
					"key" : "duration",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "Description",
					"key" : "description",
					"cellClass" : "text-center",
					"width" : 300
				}]
			};

			customfieldAssign(tempObj.columns, null, 'salesactivities', this.props.app.myResources, true);
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['searchbasedon', 'fromdate', 'todate', 'createdby', 'assignedto', 'filter', 'status'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/salesactivityreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.main.length; i++) {
					for(var prop in response.data.main[i]) {
						response.data.main[i][prop] = response.data.main[i][prop] == null ? "" : response.data.main[i][prop];
					}
					response.data.main[i].duration = "";

					if(new Date(response.data.main[i].startdatetime) < new Date(response.data.main[i].enddatetime)) {
						let date1 = new Date(response.data.main[i].startdatetime);
						let date2 = new Date(response.data.main[i].enddatetime);
						let milliseconds = Math.abs(date2.getTime() - date1.getTime());
						let oneSecond = 1000, seconds = 0, minutes = 0,  hours = 0;
						let result;

						if (milliseconds >= 3600000) {
							hours = Math.floor(milliseconds / 3600000);
						}

						milliseconds = hours > 0 ? (milliseconds - hours * 3600000) : milliseconds;

						if (milliseconds >= 60000) {
							minutes = Math.floor(milliseconds / 60000);
						}

						milliseconds = minutes > 0 ? (milliseconds - minutes * 60000) : milliseconds;

						if (milliseconds >= oneSecond) {
							seconds = Math.floor(milliseconds / oneSecond);
						}

						milliseconds = seconds > 0 ? (milliseconds - seconds * oneSecond) : milliseconds;

						if (hours > 0) {
							result = (hours > 9 ? hours : "0" + hours) + ":";
						} else {
							result = "00:";
						}

						if (minutes > 0) {
							result += (minutes > 9 ? minutes : "0" + minutes) + ":";
						} else {
							result += "00:";
						}

						if (seconds > 0) {
							result += (seconds > 9 ? seconds : "0" + seconds);
						} else {
							result += "00";
						}
						response.data.main[i].duration = result;
					}
				}

				let updateColObj = {
					hiddencols: [],
					originalRows: []
				};
				updateColObj.originalRows = response.data.main;

				for(var i = 0; i < this.props.resource.columns.length; i++) {
					if(this.props.resource.columns[i].key == 'leads_stage' || this.props.resource.columns[i].key == 'priority') {
						updateColObj[`columns[${i}].hidden`] = (this.props.resource.filter == 'Lead Activities' || this.props.resource.filter == 'All') ? false : true;
						if(updateColObj[`columns[${i}].hidden`])
							updateColObj.hiddencols.push(this.props.resource.columns[i].key);
					}
				}

				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						updateColObj.hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, updateColObj);

				this.props.updateReportFilter(this.props.form, this.props.resource);

				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			searchbasedon : null,
			filter : null,
			status : null,
			createdby : null,
			assignedto : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Sales Activity Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Search Based On</label>
										<Field name={'searchbasedon'} props={{options:[{value: "startdatetime", label: "Activity Start Date"}, {value: "created", label: "Activity Created Date"}], valuename: 'value', label: 'label'}} component={localSelectEle}  validate={[stringNewValidation({required: true, model: 'searchbasedon'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									{this.props.app.user.roleid.indexOf(2) == -1 ? <div className="form-group col-md-12 col-sm-6 col-xs-6">
										<label className="labelclass">Created By</label>
										<Field name={'createdby'} props={{resource: "users", fields: "id,displayname", label: "displayname"}} component={selectAsyncEle} />
									</div> : null}
									{this.props.app.user.roleid.indexOf(2) == -1 ? <div className="form-group col-md-12 col-sm-6 col-xs-6">
										<label className="labelclass">Assigned To</label>
										<Field name={'assignedto'} props={{resource: "users", fields: "id,displayname", label: "displayname"}} component={selectAsyncEle} />
									</div> : null}
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Filter By</label>
										<Field name={'filter'} props={{options:[{value: "All", label: "All"}, {value: "Contact Activities", label: "Contact Activities"}, {value: "Lead Activities", label: "Lead Activities"}, {value: "Quotation Activities", label: "Quotation Activities"}, {value: "Project Quotation Activities", label: "Project Quotation Activities"}, {value: "Contractenquiry Activities", label: "Contract Enquiry Activities"}, {value: "Other Activities", label: "Other Activities"}], valuename: 'value', label: 'label'}} component={localSelectEle}  validate={[stringNewValidation({required: true, model: 'filter'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Status</label>
										<Field name={'status'} props={{options: ["Open", "Planned", "Inprogress", "Completed"], multiselect: true}} component={localSelectEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12" >
										<button type="button" onClick={() => {
											this.getReportData()
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>

										<button type="button" onClick={() => {
											this.resetFilter()
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Sales Activity Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

SalesActivityReportForm = connect(
	(state, props) => {
		let formName = 'salesactivityreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(SalesActivityReportForm));

export default SalesActivityReportForm;
