import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { search, customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../utils/filter';
import { AutoSelect } from '../components/utilcomponents';

import Loadingcontainer from '../components/loadingcontainer';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class ServiceDashboardForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			equipmentsearch: {},
			tabactive: {
				pendingservicecall: '',
				equipment: '',
				allservicecall: '',
				servicereport: '',
				estimation: '',
				contractenquiry: '',
				contract: '',
				pmsschedule: '',
				invoice: '',
				receipt: ''
			},
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.filterBasedOnEquipments = this.filterBasedOnEquipments.bind(this);
		this.clearEquipmentDetails = this.clearEquipmentDetails.bind(this);
		this.createServiceCall = this.createServiceCall.bind(this);
		this.viewCustomer = this.viewCustomer.bind(this);
		this.openItem = this.openItem.bind(this);
		this.updateTabActive = this.updateTabActive.bind(this);
		this.renderServiceCall = this.renderServiceCall.bind(this);
		this.renderEquipment = this.renderEquipment.bind(this);
		this.renderEstimation = this.renderEstimation.bind(this);
		this.renderContractEnquiry = this.renderContractEnquiry.bind(this);
		this.renderContract = this.renderContract.bind(this);
		this.renderPMSschedule = this.renderPMSschedule.bind(this);
		this.renderInvoice = this.renderInvoice.bind(this);
		this.renderReceipt = this.renderReceipt.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.addressonchange = this.addressonchange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.getServiceDashboardDetails = this.getServiceDashboardDetails.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad = () => {
		let tempObj = {};
		if(this.props.location.params) {
			tempObj.customerid = this.props.location.params.customerid;
		} else if(this.props.reportdata) {
			tempObj = {
				...this.props.reportdata
			};
		}
		this.props.initialize(tempObj);
		this.updateLoaderFlag(false);
		setTimeout(()=> {
			if(this.props.resource.customerid)
				this.getServiceDashboardDetails();
		}, 0)
	}

	getServiceDashboardDetails() {
		this.updateLoaderFlag(true);
		let { tabactive } = this.state;
		axios.get(`/api/query/servicedashboardquery?customerid=${this.props.resource.customerid}`).then((response) => {
			if (response.data.message == 'success') {
				let tempObj = {
					equipmentResultArray: []
				};
				for (var prop in response.data.main)
					tempObj[prop] = response.data.main[prop];

				this.props.initialize(tempObj);
				this.props.updateReportFilter(this.props.form, this.props.resource);
				for(var prop in tabactive) {
					tabactive[prop] = '';
				}
				tabactive.pendingservicecall = 'active';
				this.setState({ tabactive });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateToggleState(false);
			this.updateLoaderFlag(false);
		});
	}

	filterBasedOnEquipments = (item) => {
		this.updateLoaderFlag(true);
		let tempObj = {
			equipmentResultArray: []
		};
		let filterString = '';

		if(this.props.resource.customerid)
			filterString += 'customerid=' + this.props.resource.customerid;
		if(item.id)
			filterString += '&equipmentid=' + item.id;
		axios.get(`/api/query/servicedashboardquery?${filterString}`).then((response) => {
			if (response.data.message == 'success') {
				tempObj.equipmentResultArray = response.data.equipmentResult;

				for(var prop in tempObj.equipmentResultArray)
					tempObj[prop] = tempObj.equipmentResultArray[prop];
				
				this.setState({
					tabactive: {
						pendingservicecall: 'active',
						equipment: '',
						allservicecall: '',
						servicereport: '',
						estimation: '',
						contractenquiry: '',
						contract: '',
						pmsschedule: '',
						invoice: '',
						receipt: ''
					}
				});
				this.props.updateFormState(this.props.form, tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	clearEquipmentDetails = () => {
		this.updateLoaderFlag(true);
		let { tabactive } = this.state;
		let tempObj = {};

		for(var prop in this.props.resource.equipmentResultArray)
			tempObj[prop] = null;

		tempObj.equipmentResultArray = null;
		tempObj.equiptabactive = true;
		
		tabactive = {
			pendingservicecall: '', equipment: 'active', allservicecall: '',servicereport:' ', estimation: '', contractenquiry: '', contract: '', pmsschedule: '', invoice: '', receipt: ''
		};
		this.setState({ tabactive });
		this.props.updateFormState(this.props.form, tempObj);
		this.updateLoaderFlag(false);
	};

	createServiceCall = () => {
		this.props.history.push({pathname: '/createServiceCall', params: {...this.props.resource}});
	}

	viewCustomer = () => {
		this.props.history.push(`/details/partners/${this.props.resource.customerid}`);
	}

	openItem = (resourcename, id) => {
		this.props.history.push(`/details/${resourcename}/${id}`);
	}

	updateTabActive = (param) => {
		let { tabactive } = this.state;
		for(var prop in tabactive) {
			tabactive[prop] = prop == param ? 'active' : '';
		}
		this.setState({ tabactive });
	}

	addressonchange = (id, valueobj) => {
		let { equipmentsearch } = this.state;
		equipmentsearch.equipmentaddressid = id;
		this.setState({ equipmentsearch });
	}

	inputonChange = (evt) => {
		let { equipmentsearch } = this.state;
		equipmentsearch[evt.target.name] = evt.target.value;
		this.setState({ equipmentsearch });
	}

	cancel = () => {
		this.props.history.goBack();
	};

	renderServiceCall = (array, param) => {
		return array.map((servicecallitem, servicecallIndex) => {
			return (
				<tr key={servicecallIndex} onClick={() => this.openItem('servicecalls', servicecallitem.id)}>
					<td className='text-center'>{servicecallitem.servicecallno}</td>
					<td className='text-center'>{servicecallitem.servicecalldate}</td>
					<td>{servicecallitem.subject}</td>
					<td className='text-center'>{servicecallitem.priority}</td>
					<td className='text-center'>{servicecallitem.displayname}</td>
					<td className='text-center'>{servicecallitem.territory}</td>
					<td className='text-center'>{servicecallitem.status}</td>
					<td>{servicecallitem.servicerendered}</td>
				</tr>
			);
		});
	}

	renderServiceReport = () => {
		return this.props.resource.serviceReportArray.map((servicereportItem, servicereportIndex) => {
			return (
				<tr key={servicereportIndex} onClick={() => this.openItem('servicereports', servicereportItem.id)}>
					<td className='text-center'>{servicereportItem.reportno}</td>
					<td className='text-center'>{dateFilter(servicereportItem.servicereportdate)}</td>
					<td className='text-center'>{servicereportItem.servicecallno}</td>
					<td className='text-center'>{servicereportItem.calltype}</td>
					<td className='text-center'>{servicereportItem.engineerid_displayname}</td>
					<td className='text-center'>{servicereportItem.complaintcategoryid_name}</td>
					<td className='text-center'>{servicereportItem.remarks}</td>
					<td className='text-center'>{servicereportItem.status}</td>
					<td className='text-center'>{servicereportItem.servicerendered}</td>
				</tr>
			);
		});	
	};

	renderEquipment = () => {
		return search(this.props.resource.equipmentArray, this.state.equipmentsearch).map((equipItem, equipIndex) => {
			return (<tr key={equipIndex} onClick={() => this.filterBasedOnEquipments(equipItem)}>
					<td className='text-center'>{equipItem.displayname}</td>
					<td className='text-center'>{equipItem.serialno}</td>
					<td>{equipItem.contractid ? <div>Contract Type : {equipItem.contracttypeid_name}<br />Contract No : {equipItem.contractno}<br />Start Date : {dateFilter(equipItem.startdate)}<br />Expire Date : {dateFilter(equipItem.expiredate)}</div> : null}</td>
					<td>{equipItem.remarks}</td>
					<td>{equipItem.location}</td>
					<td>{equipItem.equipmentaddresstype}</td>
					<td>{equipItem.equipmentaddress}</td>
					<td>{booleanfilter(equipItem.equipmentstatus)}</td>
				</tr>
			);
		});
	}

	renderEstimation = () => {
		return this.props.resource.estimationArray.map((estimationItem, estimationIndex) => {
			return (
				<tr key={estimationIndex} onClick={() => this.openItem('estimations', estimationItem.id)}>
					<td className='text-center'>{estimationItem.estimationno}</td>
					<td className='text-center'>{dateFilter(estimationItem.estimationdate)}</td>
					<td className='text-center'>{estimationItem.status}</td>
					<td className='text-center'>{estimationItem.servicecallid_servicecallno}</td>
				</tr>
			);
		});	
	};

	renderContractEnquiry = () => {
		return this.props.resource.contractEnquiryArray.map((enquiryItem, enquiryIndex) => {
			return (
				<tr key={enquiryIndex} onClick={() => this.openItem('contractenquiries',enquiryItem.id)}>
					<td className='text-center'>{enquiryItem.contractenquiryno}</td>
					<td className='text-center'>{dateFilter(enquiryItem.contractenquirydate)}</td>
					<td className='text-center'>{enquiryItem.status}</td>
				</tr>
			);
		});
	};

	renderPMSschedule = () => {
		return this.props.resource.pmsArray.map((scheduleItem, scheduleIndex) => {
			return (
				<tr key={scheduleIndex} >
					<td className='text-center'>{scheduleItem.equipmentname}</td>
					<td className='text-center'>{scheduleItem.contractno}</td>
					<td className='text-center'>{scheduleItem.scheduleno}</td>
					<td className='text-center'>{dateFilter(scheduleItem.equipmentscheduledate)}</td>
					<td className='text-center'>{dateFilter(scheduleItem.donedate)}</td>
					<td className='text-center'>{scheduleItem.status}</td>
					<td>{scheduleItem.remarks}</td>
				</tr>
			);
		});
	};

	renderContract = () => {
		return this.props.resource.contractArray.map((contractItem, contractIndex) => {
			return (
				<tr key={contractIndex} onClick={() => this.openItem('contracts',contractItem.id)}>
					<td className='text-center'>{contractItem.contractno}</td>
					<td className='text-center'>{contractItem.contracttype}</td>
					<td className='text-center'>{dateFilter(contractItem.startdate)}</td>
					<td className='text-center'>{dateFilter(contractItem.expiredate)}</td>
					<td className='text-center'>{contractItem.contractstatus}</td>
					<td className='text-center'>{contractItem.remarks}</td>
				</tr>
			);
		});
	};

	renderInvoice = () => {
		return this.props.resource.invoicesArray.map((invoiceItem, invoiceIndex) => {
			let resourceName = invoiceItem.invoicetype == 'Contract Invoice' ? 'contractinvoices' : (invoiceItem.invoicetype ==  'Service Invoice' ? 'serviceinvoices' : 'salesinvoices');
			return (
				<tr key={invoiceIndex} onClick={() => this.openItem(resourceName, invoiceItem.id)}>
					<td className='text-center'>{invoiceItem.invoicetype}</td>
					<td className='text-center'>{invoiceItem.invoiceno}</td>
					<td className='text-center'>{dateFilter(invoiceItem.invoicedate)}</td>
					<td className='text-right'>{currencyFilter(invoiceItem.finaltotal, invoiceItem.currencyid, this.props.app)}</td>
					<td>{invoiceItem.remarks}</td>
				</tr>
			);
		});
	};

	renderReceipt = () => {
		return this.props.resource.receiptArray.map((receiptItem, receiptIndex) => {
			return (
				<tr key={receiptIndex} onClick={() => this.openItem('receiptvouchers', receiptItem.id)}>
					<td className='text-center'>{receiptItem.voucherno}</td>
					<td className='text-center'>{dateFilter(receiptItem.voucherdate)}</td>
					<td className='text-right'>{currencyFilter(receiptItem.amount, receiptItem.currencyid, this.props.app)}</td>
					<td>{receiptItem.description}</td>
				</tr>
			);
		});
	};

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	resetFilter () {
		let tempObj = {};

		for (var prop in this.props.resource)
			tempObj[prop] = null;

		this.props.updateFormState(this.props.form, tempObj);
	}

	render() {
		if(!this.props.resource)
			return null;

		let { resource } = this.props;
		let { tabactive } = this.state;

		let customerid = this.props.match.params.id;
		let serviceCallArray = [];
		if(resource.equipmentid > 0 && resource.serviceCallArray.length > 0)
			serviceCallArray = resource.serviceCallArray.filter((item) => {
				return item.status != 'Completed';
			});

		let showDetails = resource.equipmentArray || resource.invoicesArray || resource.pendingserviceCallArray || resource.receiptArray;

		return(
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title float-left">Service Dashboard</div>
							{showDetails ? <div className="float-right margintop-10 marginbottom-10">
								<button type="button" className={`btn gs-btn-success btn-sm btn-width ${checkActionVerbAccess(this.props.app, 'servicecalls', 'Save') ? '' : 'hide'}`} onClick={this.createServiceCall}><i className="fa fa-plus"></i>Service Call</button>
							</div> : null}
						</div>
						<div className="col-md-12 bg-white ">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer Name</label>
										<Field name={'customerid'} props={{resource: "partners", fields: "id,name", required: true}} component={autoSelectEle} validate={[numberNewValidation({required: true, model: 'Customer Name'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getServiceDashboardDetails()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>

							{!showDetails ? <ReportPlaceholderComponent reportdata={showDetails} /> : null}
						</div>
						<div className="col-md-12 form-group" style={{paddingLeft: '40px'}}>
							{showDetails ? <div className="card">
								<div className="card-header gs-card-header borderradius-0 card-header-custom">
									Service Details
								</div>
								<div className="card-body">
									<div className="row">
										<div className="col-md-12">
											<div className="form-group col-md-3 col-sm-3 col-xs-3 float-left">
												Customer Name : <span className="gs-anchor" onClick={this.viewCustomer} title="View Customer Details"><b className="text-success">{resource.customerid_displayname}</b></span>
											</div>
											<div className="form-group col-md-3 col-sm-3 col-xs-3 float-left">
												Territory : {resource.territoryid_territoryname}
											</div>
											{resource.equipmentid>0 ? <div className="form-group col-md-6 col-sm-6 col-xs-6 float-left" >
												Selected Equipment : <b className="text-danger">{resource.equipmentid_name}</b>
												<span className="gs-anchor" onClick={this.clearEquipmentDetails}  style={{paddingLeft: '10px'}} title="View All Equipments">View All</span>
											</div>: null}
										</div>
									</div>
									<div className='row margintop-25'>
										<div className="col-md-12 col-sm-12 col-xs-12">
											<ul className="nav nav-tabs">
												<li className="nav-item">
													<span className={`nav-link gs-anchor-no-hover ${tabactive.pendingservicecall}`} onClick={()=>{this.updateTabActive('pendingservicecall')}}>Pending Service Calls</span>
												</li>
												{!resource.equipmentid ? <li className="nav-item">
													<span className={`nav-link gs-anchor-no-hover ${tabactive.equipment}`} onClick={()=>{this.updateTabActive('equipment')}}>Equipments</span>
												</li> : null}
												{resource.equipmentid>0 ? <li className="nav-item">
													<span className={`nav-link gs-anchor-no-hover ${tabactive.allservicecall}`} onClick={()=>{this.updateTabActive('allservicecall')}}>All Service Calls</span>
												</li> : null}
												{resource.equipmentid>0 ? <li className="nav-item">
													<span className={`nav-link gs-anchor-no-hover ${tabactive.servicereport}`} onClick={()=>{this.updateTabActive('servicereport')}}>Service Reports</span>
												</li> : null}
												{resource.equipmentid>0 ? <li className="nav-item">
													<span className={`nav-link gs-anchor-no-hover ${tabactive.estimation}`} onClick={()=>{this.updateTabActive('estimation')}}>Estimations</span>
												</li> : null}
												{resource.equipmentid>0 ? <li className="nav-item">
													<span className={`nav-link gs-anchor-no-hover ${tabactive.contractenquiry}`} onClick={()=>{this.updateTabActive('contractenquiry')}}>Contract Enquiries</span>
												</li> : null}
												{resource.equipmentid>0 ? <li className="nav-item">
													<span className={`nav-link gs-anchor-no-hover ${tabactive.contract}`} onClick={()=>{this.updateTabActive('contract')}}>Contracts</span>
												</li> : null}
												{resource.equipmentid>0 ? <li className="nav-item">
													<span className={`nav-link gs-anchor-no-hover ${tabactive.pmsschedule}`} onClick={()=>{this.updateTabActive('pmsschedule')}}>PMS Schedules</span>
												</li> : null}
												{!resource.equipmentid ? <li className="nav-item">
													<span className={`nav-link gs-anchor-no-hover ${tabactive.invoice}`} onClick={()=>{this.updateTabActive('invoice')}}>Invoices</span>
												</li> : null}
												{!resource.equipmentid ? <li className="nav-item">
													<span className={`nav-link gs-anchor-no-hover ${tabactive.receipt}`} onClick={()=>{this.updateTabActive('receipt')}}>Receipts</span>
												</li> : null}
											</ul>

											<div className="tab-content">
											{tabactive.pendingservicecall ? 
												<div className={`tab-pane fade ${tabactive.pendingservicecall ? 'show active': ''}`}>
													<div className="row margintop-25">
														{(resource.equipmentid > 0 ? serviceCallArray.length>0 : resource.pendingserviceCallArray.length > 0) ? <div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
															<table className="table table-bordered table-hover table-sm">
																<thead>
																	<tr>
																		<th className='text-center'>Service Call No</th>
																		<th className='text-center'>Call Date</th>
																		<th className='text-center'>Subject</th>
																		<th className='text-center'>Priority</th>
																		<th className='text-center'>Assigned To</th>
																		<th className='text-center'>Territory</th>
																		<th className='text-center'>Status</th>
																		<th className='text-center' style={{width:'300px'}}>Service Rendered</th>
																	</tr>
																</thead>
																<tbody>
																	{resource.equipmentid > 0 ? this.renderServiceCall(serviceCallArray) : this.renderServiceCall(resource.pendingserviceCallArray, true)}
																</tbody>
															</table>
														</div> : <div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
															No Pending Service Call Details found!!!
														</div>}
													</div>
												</div> : null}
												{tabactive.equipment ?
												<div className={`tab-pane fade ${tabactive.equipment ? 'show active': ''}`}>
													{resource.equipmentArray.length>0 ? <div className='row margintop-25'>
														<div className="col-md-3 form-group">
															<label className="labelclass">Installation Address</label>
															<div>
																<AutoSelect resource={'addresses'} fields={'id,displayaddress'} value={this.state.equipmentsearch.equipmentaddressid} label={'displayaddress'} valuename={'id'} filter={`addresses.parentresource = 'partners' and addresses.parentid = ${customerid}`} onChange={this.addressonchange} />
															</div>
														</div>
														<div className="col-md-3 form-group">
															<label className="labelclass">Serial No</label>
															<input type="text" className="form-control" name="serialno" value={this.state.equipmentsearch.serialno || ''} placeholder="Search by serial no" onChange={(evt) =>{this.inputonChange(evt)}}/>
														</div>
														<div className="col-md-3 form-group">
															<label className="labelclass">Location</label>
															<input type="text" className="form-control" name="location" value={this.state.equipmentsearch.location || ''} placeholder="Search by location" onChange={(evt) =>{this.inputonChange(evt)}}/>
														</div>
														<div className="col-md-3 form-group">
															<label className="labelclass">Remarks</label>
															<input type="text" className="form-control" name="remarks" value={this.state.equipmentsearch.remarks || ''} placeholder="Search by remarks" onChange={(evt) =>{this.inputonChange(evt)}}/>
														</div>
														<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
															<table className="table table-bordered table-hover table-sm">
																<thead>
																	<tr>
																		<th className='text-center' style={{width:'150px'}}>Equipment Name</th>
																		<th className='text-center' style={{width:'100px'}}>Serial No</th>
																		<th className='text-center' style={{width:'200px'}}>Contract Details</th>
																		<th className='text-center' style={{width:'150px'}}>Remarks</th>
																		<th className='text-center' style={{width:'100px'}}>Location</th>
																		<th className='text-center' style={{width:'100px'}}>Installed Address Type</th>
																		<th className='text-center' style={{width:'150px'}}>Installed Address</th>
																		<th className='text-center' style={{width:'150px'}}>Equipment Status</th>
																	</tr>
																</thead>
																<tbody>
																	{this.renderEquipment()}
																</tbody>
															</table>
														</div>
													</div> : <div className="row margintop-25">
														<div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
															No Equipment Details found!!!
														</div>
													</div>}
												</div>:null}
												{resource.equipmentid>0 && tabactive.allservicecall ? <div className={`tab-pane fade ${tabactive.allservicecall ? 'show active': ''}`}>
													<div className='row margintop-25'>
														{resource.serviceCallArray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
															<table className="table table-bordered table-hover table-sm">
																<thead>
																	<tr>
																		<th className='text-center'>Service Call No</th>
																		<th className='text-center'>Call Date</th>
																		<th className='text-center'>Subject</th>
																		<th className='text-center'>Priority</th>
																		<th className='text-center'>Assigned To</th>
																		<th className='text-center'>Territory</th>
																		<th className='text-center'>Status</th>
																		<th className='text-center' style={{width: '300px'}}>Service Rendered</th>
																	</tr>
																</thead>
																<tbody>
																	{this.renderServiceCall(this.props.resource.serviceCallArray)}
																</tbody>
															</table>
														</div> :<div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
															No Service Call Details found!!!
														</div>}
													</div>
												</div> : null }
												{resource.equipmentid>0 && tabactive.servicereport ? <div className={`tab-pane fade ${tabactive.servicereport ? 'show active': ''}`}>
													<div className='row margintop-25'>
														{resource.serviceCallArray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
															<table className="table table-bordered table-hover table-sm">
																<thead>
																	<tr>
																		<th className='text-center'>Report No</th>
																		<th className='text-center'>Report Date</th>
																		<th className='text-center'>Call No</th>
																		<th className='text-center'>Call Type</th>
																		<th className='text-center'>Technician</th>
																		<th className='text-center'>Complaint</th>
																		<th className='text-center'>Remarks</th>
																		<th className='text-center'>Status</th>
																		<th className='text-center' style={{width: '300px'}}>Service Rendered</th>
																	</tr>
																</thead>
																<tbody>
																	{this.renderServiceReport()}
																</tbody>
															</table>
														</div> :<div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
															No Service Report Details found!!!
														</div>}
													</div>
												</div> : null }
												{resource.equipmentid>0 && tabactive.estimation ? <div className={`tab-pane fade ${tabactive.estimation ? 'show active': ''}`}>
													<div className='row margintop-25'>
														{resource.estimationArray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
															<table className="table table-bordered table-hover table-sm">
																<thead>
																	<tr>
																		<th className='text-center'>Estimation No</th>
																		<th className='text-center'>Date</th>
																		<th className='text-center'>Status</th>
																		<th className='text-center'>Service Call No</th>
																	</tr>
																</thead>
																<tbody>
																	{this.renderEstimation()}
																</tbody>
															</table>
														</div> : <div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
															No Estimation Details found!!!
														</div>}
													</div>
												</div> : null}
												{resource.equipmentid>0 && tabactive.contractenquiry ? <div className={`tab-pane fade ${tabactive.contractenquiry ? 'show active': ''}`}>
													<div className='row margintop-25'>
														{resource.contractEnquiryArray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
															<table className="table table-bordered table-hover table-sm">
																<thead>
																	<tr>
																		<th className='text-center'>Contract Enquiry No</th>
																		<th className='text-center'>Date</th>
																		<th className='text-center'>Status</th>
																	</tr>
																</thead>
																<tbody>
																	{this.renderContractEnquiry()}
																</tbody>
															</table>
														</div> : <div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
															No Contract Enquiry Details found!!!
														</div>}
													</div>
												</div> : null }
												{resource.equipmentid>0 && tabactive.contract ? <div className={`tab-pane fade ${tabactive.contract ? 'show active': ''}`}>
													<div className='row margintop-25'>
														{resource.contractArray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
															<table className="table table-bordered table-hover table-sm">
																<thead>
																	<tr>
																		<th className='text-center'>Contract No</th>
																		<th className='text-center'>Contract Type</th>
																		<th className='text-center'>Start Date</th>
																		<th className='text-center'>Expire Date</th>
																		<th className='text-center'>Contract Status</th>
																		<th className='text-center'>Remarks</th>
																	</tr>
																</thead>
																<tbody>
																	{this.renderContract()}
																</tbody>
															</table>
														</div> : <div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
															No Contract Details found!!!
														</div>}
													</div>
												</div> : null }
												{resource.equipmentid>0 && tabactive.pmsschedule ? <div className={`tab-pane fade ${tabactive.pmsschedule ? 'show active': ''}`}>
													<div className='row margintop-25'>
														{resource.pmsArray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
															<table className="table table-bordered table-hover table-sm">
																<thead>
																	<tr>
																		<th className='text-center'>Equipment Name</th>
																		<th className='text-center'>Contract No</th>
																		<th className='text-center'>Schedule No</th>
																		<th className='text-center'>Schedule Date</th>
																		<th className='text-center'>Done Date</th>
																		<th className='text-center'>Status</th>
																		<th className='text-center'>Remarks</th>
																	</tr>
																</thead>
																<tbody>
																	{this.renderPMSschedule()}
																</tbody>
															</table>
														</div> : <div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
															No Equipment Schedule Details found!!!
														</div>}
													</div>
												</div> : null}
												{tabactive.invoice ?
												<div className={`tab-pane fade ${tabactive.invoice ? 'show active': ''}`}>
													<div className='row margintop-25'>
														{resource.invoicesArray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
															<table className="table table-bordered table-hover table-sm">
																<thead>
																	<tr>
																		<th className='text-center'>Invoice Type</th>
																		<th className='text-center'>Invoice No</th>
																		<th className='text-center'>Invoice Date</th>
																		<th className='text-center'>Amount</th>
																		<th className='text-center'>Remarks</th>
																	</tr>
																</thead>
																<tbody>
																	{this.renderInvoice()}
																</tbody>
															</table>
														</div> : <div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
															No Invoice Details found!!!
														</div>}
													</div>
												</div> :null}
												{tabactive.receipt ?
												<div className={`tab-pane fade ${tabactive.receipt ? 'show active': ''}`}>
													<div className='row margintop-25'>
														{resource.receiptArray.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
															<table className="table table-bordered table-hover table-sm">
																<thead>
																	<tr>
																		<th className='text-center'>Receipt No</th>
																		<th className='text-center'>Receipt Date</th>
																		<th className='text-center'>Amount</th>
																		<th className='text-center'>Remarks</th>
																	</tr>
																</thead>
																<tbody>
																	{this.renderReceipt()}
																</tbody>
															</table>
														</div> : <div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
															No Receipt Details found!!!
														</div>}
													</div>
												</div> :null}
											</div>
										</div>
									</div>
								</div>
							</div> : null}
						</div>
					</div>
					{showDetails ? <div className="row" style={{paddingTop: '40px'}}>
						<div className="col-md-12">
							<div className="muted credit text-center sticky-footer" style={{width : '100%'}}>
								<button type="button" className="btn btn-sm btn-secondary btn-width closebtn" onClick={()=>{this.cancel()}}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div> : null}
				</form>
			</div>
		);
	};
}

ServiceDashboardForm = connect(
	(state, props) => {
		let formName = 'servicedashboard';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ServiceDashboardForm));

export default ServiceDashboardForm;
