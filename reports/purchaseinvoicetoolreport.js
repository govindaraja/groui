import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { AutoSelect, SelectAsync } from '../components/utilcomponents';
import { search } from '../utils/utils';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter } from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';

class PurchaseInvoicePlannerReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			search: {},
			limit: {
				receiptnote : 10,
				purchasereturn : 10
			},
			receiptnoteArray: [],
			purchasereturnArray: [],
			isReceiptNoteActive: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.tabOnChange = this.tabOnChange.bind(this);
		this.addLimit = this.addLimit.bind(this);
		this.parentCheckboxOnChange = this.parentCheckboxOnChange.bind(this);
		this.itemCheckboxOnChange = this.itemCheckboxOnChange.bind(this);
		this.filterOnchange = this.filterOnchange.bind(this);
		this.createInvoice = this.createInvoice.bind(this);
		this.createDebitnote = this.createDebitnote.bind(this);
		this.searchFilter = this.searchFilter.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag = (loaderflag) => {
		this.setState({loaderflag});
	}

	onLoad = () => {
		this.updateLoaderFlag(true);
		let receiptnoteArray = [], purchasereturnArray = [];

		axios.get(`/api/query/purchaseinvoicetoolquery`).then((response) => {
			if (response.data.message == 'success') {
				let receiptnoteitemsarray = response.data.main.receiptnotesarr,
				purchasereturnitemsarray = response.data.main.purchasereturnitemsarr;

				for (var i = 0; i < receiptnoteitemsarray.length; i++) {
					let receiptitemFound = false;
					for (var j = 0; j < receiptnoteArray.length; j++) {
						if (receiptnoteArray[j].receiptnotenumber) {
							if (receiptnoteArray[j].id == receiptnoteitemsarray[i].receiptnoteid) {
								receiptitemFound = true;
								receiptnoteitemsarray[i].number = receiptnoteitemsarray[i].quantity;
								receiptnoteitemsarray[i].numberinvoiced = receiptnoteitemsarray[i].invoicedqty;
								receiptnoteitemsarray[i].invoicequantity = receiptnoteitemsarray[i].quantity - receiptnoteitemsarray[i].invoicedqty;
								receiptnoteArray[j].receiptnoteitems.push(receiptnoteitemsarray[i]);
								break;
							}
						}
					}
					
					if (!receiptitemFound) {
						let tempObject = {
							receiptnotenumber : receiptnoteitemsarray[i].receiptnotenumber,
							partnerid : receiptnoteitemsarray[i].partnerid,
							partnerid_name : receiptnoteitemsarray[i].partnerid_name,
							companyid : receiptnoteitemsarray[i].companyid,
							companyid_name : receiptnoteitemsarray[i].companyid_name,
							currencyid : receiptnoteitemsarray[i].currencyid,
							currencyid_name : receiptnoteitemsarray[i].currencyid_name,
							id : receiptnoteitemsarray[i].receiptnoteid,
							billingaddress : receiptnoteitemsarray[i].billingaddress,
							deliveryaddress : receiptnoteitemsarray[i].deliveryaddress,
							contactid : receiptnoteitemsarray[i].contactid,
							phone : receiptnoteitemsarray[i].phone,
							mobile : receiptnoteitemsarray[i].mobile,
							param : 'Receipt Notes',
							receiptfor : 'Receipt Notes',
							ponumber : receiptnoteitemsarray[i].ponumber,
							receiptnotedate : receiptnoteitemsarray[i].receiptnotedate,
							supplierreference : receiptnoteitemsarray[i].supplierreference,
							ewaybillno : receiptnoteitemsarray[i].ewaybillno,
							receiptnoteitems : []
						};

						receiptnoteitemsarray[i].number = receiptnoteitemsarray[i].quantity;
						receiptnoteitemsarray[i].numberinvoiced = receiptnoteitemsarray[i].invoicedqty;
						receiptnoteitemsarray[i].invoicequantity = receiptnoteitemsarray[i].quantity - receiptnoteitemsarray[i].invoicedqty;
						tempObject.receiptnoteitems.push(receiptnoteitemsarray[i]);

						receiptnoteArray.push(tempObject);
					}
				}

				for (var i = 0; i < purchasereturnitemsarray.length; i++) {
					let itemFound = false
					for (var j = 0; j < purchasereturnArray.length; j++) {
						if (purchasereturnArray[j].id == purchasereturnitemsarray[i].purchasereturnid) {
							itemFound = true;
							purchasereturnArray[j].purchasereturnitems.push(purchasereturnitemsarray[i]);
							break;
						}
					}
					if (!itemFound) {
						let tempObject = {
							againstorder: purchasereturnitemsarray[i].againstorder,
							currencyid: purchasereturnitemsarray[i].currencyid,
							currencyid_name: purchasereturnitemsarray[i].currencyid_name,
							currencyexchangerate: purchasereturnitemsarray[i].currencyexchangerate,
							pricelistid: purchasereturnitemsarray[i].pricelistid,
							defaultcostcenter: purchasereturnitemsarray[i].defaultcostcenter,
							id : purchasereturnitemsarray[i].purchasereturnid,
							companyid : purchasereturnitemsarray[i].companyid,
							companyid_name : purchasereturnitemsarray[i].companyid_name,
							purchaseorderid: purchasereturnitemsarray[i].purchaseorderid,
							partnerid : purchasereturnitemsarray[i].partnerid,
							partnerid_name : purchasereturnitemsarray[i].partnerid_name,
							purchasereturnno : purchasereturnitemsarray[i].purchasereturnno,
							purchasereturnitems : []
						}
						tempObject.purchasereturnitems.push(purchasereturnitemsarray[i]);
						purchasereturnArray.push(tempObject);
					}
				}

				receiptnoteArray.sort((a, b) => {
					return new Date(a.receiptnotedate) - new Date(b.receiptnotedate);
				});

				this.setState({ receiptnoteArray, purchasereturnArray });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	searchFilter = (array) => {
		let tempArr = [];
		array.map((a) => {
			let filtered = true;
			for(var prop in this.state.search) {
				if(this.state.search[prop]) {
					if(a[prop]) {
						if(this.state.search[prop] != a[prop]) {
							filtered = false;
						}
					} else {
						filtered = false;
					}
				}
			}
			if(filtered)
				tempArr.push(a);
		});
		return tempArr;
	};

	resetFilter = () => {
		let { search } = this.state;
		search = {};
		this.setState({ search });
	}

	tabOnChange = (param) => {
		let isReceiptNoteActive = false, isPurchaseReturnActive = false;
		if(param == 'receiptnote')
			isReceiptNoteActive = true;
		else if(param == 'purchasereturn')
			isPurchaseReturnActive = true;
		
		this.setState({ isReceiptNoteActive, isPurchaseReturnActive });
	};

	addLimit = (param) => {
		this.state.limit[param] += 10;
		this.setState({limit : this.state.limit});
	};

	parentCheckboxOnChange = (checked, param, arrayname, childname) => {
		this.state[arrayname].forEach((data) => {
			if(data.id == param.id) {
				data.check = checked;
				data[childname].forEach((item) => {
					item.check = checked;
				});
			}
		});
		this.setState({ [arrayname] : this.state[arrayname] });
	};

	itemCheckboxOnChange = (checked, param, childname) => {
		param.check = checked;
		this.setState({ [childname] : this.state[childname] });
	};

	filterOnchange = (value, field) => {		
		this.state.search[field] = value;

		[{name: "receiptnoteArray", childname: 'receiptnoteitems'}, {name: "purchasereturnArray", childname: "purchasereturnitems"}].forEach((itemObj) => {
			this.state[itemObj.name].forEach((data) => {
				data.check = false;
				data[itemObj.childname].forEach((item) => {
					item.check = false;
				});
			});
		});

		this.setState({
			search: this.state.search,
			receiptnoteArray: this.state.receiptnoteArray,
			purchasereturnArray: this.state.purchasereturnArray
		});
	};

	createInvoice = () => {
		let { receiptnoteArray } = this.state;
		let tempObj = {}, tempArray = [];
		if (this.state.isReceiptNoteActive) {
			let receiptnoteitemNotFound = true, errorFound = false;
			for (var i = 0; i < receiptnoteArray.length; i++) {
				for (var j = 0; j < receiptnoteArray[i].receiptnoteitems.length; j++) {
					if (receiptnoteArray[i].receiptnoteitems[j].check) {
						if (Object.keys(tempObj).length > 0) {
							if (tempObj.partnerid != receiptnoteArray[i].receiptnoteitems[j].partnerid || tempObj.companyid != receiptnoteArray[i].receiptnoteitems[j].companyid|| tempObj.currencyid != receiptnoteArray[i].receiptnoteitems[j].currencyid) {
								errorFound = true;
								break;
							}
						} else
							tempObj = receiptnoteArray[i];
						  
						tempArray.push(receiptnoteArray[i].receiptnoteitems[j]);
					}
				}
				if (errorFound)
					break;
			}
			if (errorFound) {
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please select receiptnote item for same partner or company or currency',
					btnArray : ['Ok']
				}));
				receiptnoteitemNotFound = false;
			}

			if (receiptnoteitemNotFound && !errorFound) {
				if (Object.keys(tempObj).length > 0) {
					let ewaybillno = [], tempReceiptId = "";
					for (var i = 0; i < tempArray.length; i++) {
						if (tempReceiptId != tempArray[i].receiptnoteid) {
							tempReceiptId = tempArray[i].receiptnoteid;
							if(tempArray[i].ewaybillno && ewaybillno.indexOf(tempArray[i].ewaybillno)<0)
								ewaybillno.push(tempArray[i].ewaybillno);
						}
					}
					tempObj.ewaybillno = ewaybillno.join(', ');
					tempObj.receiptnoteitems = tempArray;

					this.props.history.push({pathname: '/createPurchaseInvoice', params: {...tempObj}});
				} else {
					this.props.openModal(modalService['infoMethod']({
						header : 'Error',
						body : 'Please choose atleast one item to create purchase invoice',
						btnArray : ['Ok']
					}));
				}
			}
		}
	}

	createDebitnote = () => {
		let { purchasereturnArray } = this.state;
		let tempObj = {}, tempArray = [];
		if (this.state.isPurchaseReturnActive) {
			let purchasereturnNotFound = true, errorFound = false;
			for (var i = 0; i < purchasereturnArray.length; i++) {
				for (var j = 0; j < purchasereturnArray[i].purchasereturnitems.length; j++) {
					if (purchasereturnArray[i].purchasereturnitems[j].check) {
						if (Object.keys(tempObj).length > 0) {
							if (tempObj.id != purchasereturnArray[i].purchasereturnitems[j].purchasereturnid|| tempObj.partnerid != purchasereturnArray[i].purchasereturnitems[j].partnerid || tempObj.companyid != purchasereturnArray[i].purchasereturnitems[j].companyid || tempObj.currencyid != purchasereturnArray[i].purchasereturnitems[j].currencyid) {
								errorFound = true;
								break;
							}
						} else
							tempObj = purchasereturnArray[i];

						tempArray.push(purchasereturnArray[i].purchasereturnitems[j]);
					}
				}
				if (errorFound)
					break;
			}
			if (errorFound) {
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please select one purchase return at a time',
					btnArray : ['Ok']
				}));
				purchasereturnNotFound = false;
			}

			if (purchasereturnNotFound && !errorFound) {
				if (Object.keys(tempObj).length > 0) {
					tempObj.param = 'Purchase Returns';
					this.props.history.push({pathname: '/createDebitNote', params: tempObj});
				} else {
					this.props.openModal(modalService['infoMethod']({
						header : 'Error',
						body : 'Please choose atleast one item to create debit note',
						btnArray : ['Ok']
					}));
				}
			}
		}
	};

	render() {
		let filteredReceiptNoteArray = this.searchFilter(this.state.receiptnoteArray, this.state.search);
		let filteredPurchaseReturnArray = this.searchFilter(this.state.purchasereturnArray, this.state.search);

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="form-group col-md-12 bg-white report-header">
						<div className="report-header-title">Purchase Invoice Tool</div>
						<div className="report-header-rightpanel">
							{this.state.isReceiptNoteActive ? <button type="button" onClick={this.createInvoice} className="btn btn-sm gs-btn-success btn-width"><i className="fa fa-plus"></i>Invoice</button> : null }
							{this.state.isPurchaseReturnActive ? <button type="button" onClick={this.createDebitnote} className="btn btn-sm gs-btn-success btn-width"><i className="fa fa-plus"></i>Debit Note</button> : null }
						</div>
					</div>
					<div className="form-group col-md-12 bg-white paddingright-5">
						<div className="row">
							<div className="form-group col-md-3 col-sm-12">
								<label className="labelclass">Partner</label>
								<AutoSelect resource={'partners'} fields={'id,name,displayname'} value={this.state.search.partnerid} label={'displayname'} valuename={'id'} onChange={(value, valueobj) => this.filterOnchange(value, 'partnerid')} />
							</div>
							<div className="form-group col-md-3 col-sm-12" >
								<button type="button" onClick={() => {
									this.resetFilter()	
								}} className="btn btn-width btn-sm gs-btn-info margintop-25"><i className="fa fa-refresh"></i>Reset</button>
							</div>
						</div>
					</div>

					<div className="form-group col-md-12 bg-white paddingright-5">
						<div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								<ul className="nav nav-tabs">
									<li className={`nav-item nav-link ${this.state.isReceiptNoteActive ? 'active' : ''}`} onClick={() => this.tabOnChange('receiptnote')}>
										<i className="fa fa-list-alt"></i>
										<span className="marginleft-10">Receipt Notes</span>
										<span className="badge badge-secondary marginleft-10">{filteredReceiptNoteArray.length}</span>
									</li>
									<li className={`nav-item nav-link ${this.state.isPurchaseReturnActive ? 'active' : ''}`} onClick={() => this.tabOnChange('purchasereturn')}>
										<i className="fa fa-list-alt"></i>
										<span className="marginleft-10">Receipt Returns</span>
										<span className="badge badge-secondary marginleft-10">{filteredPurchaseReturnArray.length}</span>
									</li>
								</ul>

								<div className="tab-content">
									<div className={`tab-pane fade ${this.state.isReceiptNoteActive ? 'show active' : ''}`}>
										<div className="row margintop-25">
											{filteredReceiptNoteArray.map((receiptnotedata, receiptnotedataindex) => {
												if(this.state.limit.receiptnote > receiptnotedataindex) {
													return(
														<div className="form-group col-md-12 col-sm-12 col-xs-12" key={receiptnotedataindex}>
															<div className="card border-info">
																<div className="card-header">
																	<div className="float-left">
																		<span className="marginleft-11">
																			<input type="checkbox" checked={receiptnotedata.check || false} onChange={(evt) => this.parentCheckboxOnChange(evt.target.checked, receiptnotedata, 'receiptnoteArray', 'receiptnoteitems')} />
																		</span>
																		<a href={`#/details/receiptnotes/${receiptnotedata.id}`} className="marginleft-25">{receiptnotedata.receiptnotenumber}</a>
																	</div>
																	<div className="float-left">
																		<span> | Date :   </span>
																		<span>{dateFilter(receiptnotedata.receiptnotedate)}</span>
																		<span> | Partner Reference :   </span>
																		<span>{receiptnotedata.supplierreference}</span>
																		<span> | Supplier :   </span>
																		<span>{receiptnotedata.partnerid_name}</span>
																		<span> | Currency : </span>
																		<span>{receiptnotedata.currencyid_name}</span>
																	</div>
																</div>
																<div className="card-body padding-0">
																	<div className="table-responsive">
																		<table className="table table-sm marginbottom-0">
																			<thead>
																				<tr>
																					<th></th>
																					<th className="text-center">Item</th>
																					<th className="text-center">Ordered Qty</th>
																					<th className="text-center">Invoiced Qty</th>
																					<th className="text-center">Qty to Invoice</th>
																				</tr>
																			</thead>
																			{receiptnotedata.receiptnoteitems ? <tbody>
																				{receiptnotedata.receiptnoteitems.map((receiptnoteitem, receiptnoteitemindex) => {
																					return (
																						<tr key={receiptnoteitemindex}>
																							<td className='text-center' style={{width: '5%'}}><input type="checkbox" checked={receiptnoteitem.check || false} onChange={(evt) => this.itemCheckboxOnChange(evt.target.checked, receiptnoteitem, 'receiptnoteitems')}/></td>
																							<td className="text-center">{receiptnoteitem.itemid_name}</td>
																							<td className="text-center">{receiptnoteitem.number}</td>
																							<td className="text-center">{receiptnoteitem.numberinvoiced || 0}</td>
																							<td className="text-center width-20">{receiptnoteitem.invoicequantity}</td>
																						</tr>
																					)
																				})}
																			</tbody> : null}
																		</table>
																	</div>
																</div>
															</div>
														</div>
													)
												}
											})}

											{filteredReceiptNoteArray.length > this.state.limit.receiptnote ? <div className="col-md-12 col-sm-12 text-center">
												<button type="button" className="btn btn-sm gs-btn-warning" onClick={() => this.addLimit("receiptnote")}>Show More</button>
											</div> : null }
										</div>
									</div>

									<div className={`tab-pane fade ${this.state.isPurchaseReturnActive ? 'show active' : ''}`}>
										<div className="row margintop-25">
											{filteredPurchaseReturnArray.map((purchasereturndata, purchasereturndataindex) => {
												if(this.state.limit.purchasereturn > purchasereturndataindex) {
													return(
														<div className="form-group col-md-12 col-sm-12 col-xs-12" key={purchasereturndataindex}>
															<div className="card border-info">
																<div className="card-header">
																	<div className="float-left">
																		<span className="marginleft-11">
																			<input type="checkbox" checked={purchasereturndata.check || false} onChange={(evt) => this.parentCheckboxOnChange(evt.target.checked, purchasereturndata, 'purchasereturnArray', 'purchasereturnitems')} />
																		</span>
																		<a href={`#/details/purchasereturns/${purchasereturndata.id}`} className="marginleft-25">{purchasereturndata.purchasereturnno}</a>
																	</div>
																	<div className="float-left">
																		<span> | Supplier :   </span>
																		<span>{purchasereturndata.partnerid_name}</span>
																	</div>
																</div>
																<div className="card-body padding-0">
																	<div className="table-responsive">
																		<table className="table table-sm marginbottom-0">
																			<thead>
																				<tr>
																					<th></th>
																					<th className="text-center">Item</th>
																					<th className="text-center">Accepted Qty</th>
																					<th className="text-center">Invoiced Qty</th>
																					<th className="text-center">Qty to Invoice</th>
																				</tr>
																			</thead>
																			{purchasereturndata.purchasereturnitems ? <tbody>
																				{purchasereturndata.purchasereturnitems.map((purchasereturnitem, purchasereturnitemindex) => {
																					return (
																						<tr key={purchasereturnitemindex}>
																							<td className='text-center' style={{width: '5%'}}><input type="checkbox" checked={purchasereturnitem.check || false} disabled={true} onChange={(evt) => this.itemCheckboxOnChange(evt.target.checked, purchasereturnitem, 'purchasereturnitems')}/></td>
																							<td className="text-center">{purchasereturnitem.itemid_name}</td>
																							<td className="text-center">{purchasereturnitem.qtyreturned}</td>
																							<td className="text-center">{purchasereturnitem.qtycredited || 0}</td>
																							<td className="text-center width-20">{purchasereturnitem.invoicequantity}</td>
																							
																						</tr>
																					)
																				})}
																			</tbody> : null}
																		</table>
																	</div>
																</div>
															</div>
														</div>
													)
												}
											})}

											{filteredPurchaseReturnArray.length > this.state.limit.purchasereturn ? <div className="col-md-12 col-sm-12 text-center">
												<button type="button" className="btn btn-sm gs-btn-warning" onClick={() => this.addLimit("purchasereturn")}>Show More</button>
											</div> : null }
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</>
		);
	};
}



PurchaseInvoicePlannerReportForm = connect((state) =>{
	return {app: state.app}
}) (PurchaseInvoicePlannerReportForm);

export default PurchaseInvoicePlannerReportForm;
