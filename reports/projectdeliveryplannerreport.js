import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';

import { checkboxEle, DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import StockdetailsModal from '../containers/stockdetails';

class ProjectDeliveryPlannerReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.projectCB = this.projectCB.bind(this);
		this.createDeliveryNote = this.createDeliveryNote.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "",
					"headerformat" : "checkbox",
					"key" : "ischecked",
					"locked": true,
					"format" : "checkbox",
					"cellClass" : "text-center",
					"onChange" : "{report.checkboxOnChange}",
					"restrictToExport" : true,
					"width" : 100
				}, {
					"name" : "BOQ Internal Ref No",
					"key" : "boqitemsid_internalrefno",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Request No",
					"key" : "itemrequestno",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Item Name",
					"key" : "itemid_name",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Description",
					"key" : "description",
					"cellClass" : "text-center",
					"width" : 300
				}, {
					"name" : "Require Date",
					"key" : "requireddate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Qty Requested",
					"key" : "qtyrequest",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "UOM",
					"key" : "uomid_name",
					"cellClass" : "text-center",
					"width" : 100
				}, {
					"name" : "",
					"format" : "button",
					"onClick" : "{report.btnOnClick}",
					"buttonname" : "Check Stock",
					"cellClass" : "text-center",
					"restrictToExport" : true,
					"width" : 100
				}, {
					"name" : "Delivered Qty",
					"key" : "deliveredqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "PO Placed",
					"key" : "poqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "PO Received",
					"key" : "podeliveredqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "To be Delivered Qty",
					"key" : "tobedeliveredqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "In Stock Qty",
					"key" : "instockqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Available Qty",
					"key" : "availableqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}]
			};

		this.props.initialize(tempObj);

		setTimeout(() => {
			if(this.props.reportdata)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];
		['projectid', 'fromdate', 'todate', 'itemcategoryid'].map((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/projectdeliveryplannerquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			projectid: null,
			itemcategoryid: null,
			fromdate : null,
			todate : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	projectCB (value, valueObj) {
		this.props.updateFormState(this.props.form, {
			projectid_projectname : valueObj.projectname,
			projectid_displayname : valueObj.displayname,
			projectid_projectno : valueObj.projectno,
			projectid_deliveryaddress : valueObj.deliveryaddress,
			projectid_deliveryaddressid :valueObj.deliveryaddressid
		});
	}

	createDeliveryNote() {
		this.updateLoaderFlag(true);

		let itemCheckFound = false,
			deliveryItemArray = [],
			tempObj = this.props.resource;

		tempObj.originalRows.map((item) => {
			if(item.ischecked) {
				item.quantity = item.tobedeliveredqty;
				deliveryItemArray.push(item);
				itemCheckFound = true;
			}
		});

		tempObj['itemrequestitems'] = deliveryItemArray;

		if (!itemCheckFound) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Please Choose Atlease One Item to create Delivery Note!!!",
				btnArray : ["Ok"]
			}));
		} else if(deliveryItemArray.length > 100) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "You have chosen more than 100 items. Delivery note cannot be created for more than 100 Items at a time",
				btnArray : ["Ok"]
			}));
		} else {
			this.props.history.push({pathname: '/createDeliveryNote', params: {...tempObj, companyid: deliveryItemArray[0].companyid, param: 'Project Delivery Planner'}});
		}

		this.updateLoaderFlag(false);
	}

	checkboxOnChange(value, item) {
		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		this.refs.grid.refresh();
	}

	checkboxHeaderOnChange(param) {
		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if(param) {
				item.ischecked = true;
			}
		});
		setTimeout(() => {
			this.props.updateFormState(this.props.form, {
				originalRows: this.props.resource.originalRows
			});
		}, 0);
		this.refs.grid.forceRefresh();
	}

	btnOnClick = (item) => {
		this.props.openModal({render: (closeModal) => {
			return <StockdetailsModal resource = {{}} item={item} closeModal={closeModal} />
		}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
	};

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 form-group bg-white report-header">
							<div className="report-header-title">Project Delivery Planner</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							<div className="report-header-rightpanel">
								{(this.props.resource.originalRows && this.props.resource.originalRows.length>0 && checkActionVerbAccess(this.props.app, 'deliverynotes', 'Save'))? <button type="button" onClick={() => {this.createDeliveryNote()}} className="btn btn-width btn-sm gs-btn-outline-primary float-right" rel="tooltip" title="Create Delivery Note" disabled={!this.props.valid}><i className="fa fa-plus"></i>Delivery Note</button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Project</label>
										<Field
											name={'projectid'}
											props={{
												resource: "projects",
												fields: "id,projectname,projectno,deliveryaddress,deliveryaddressid,displayname",
												label: "displayname",
												filter: "projects.status='Approved'",
												onChange: (value, valueObj) => this.projectCB(value, valueObj)
											}}
											component={autoSelectEle}
											validate={[numberNewValidation({required: true})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Required Date(From)</label>
										<Field name={'fromdate'} props={{}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Required Date(To)</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: '{resource.fromdate ? true : false}' ,title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Project Delivery Planner Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} btnOnClick={this.btnOnClick} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

ProjectDeliveryPlannerReportForm = connect(
	(state, props) => {
		let formName = 'projectdeliveryplannerreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ProjectDeliveryPlannerReportForm));

export default ProjectDeliveryPlannerReportForm;
