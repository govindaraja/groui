import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, checkboxEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class StockAsonDateReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.onChangeGroupby = this.onChangeGroupby.bind(this);
		this.getWarehouseDetails = this.getWarehouseDetails.bind(this);
		this.getItemGroups = this.getItemGroups.bind(this);
		this.getItemCategories = this.getItemCategories.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.callBackIncludeProjectwarehouse = this.callBackIncludeProjectwarehouse.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				date : new Date(new Date().setHours(0, 0, 0, 0)),
				reporttype : 'postingdate',
				stockallitem : 'stockitem',
				activeitem : 'Active Item',
				groupby : 'None',
				warehouseArray: [],
				warehouseid: [],
				tempstockArray : [],
				itemcategoryArray : [],
				itemgroupArray : [],
				groupFilterArray : [],
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Item Name",
					"key" : "itemname",
					"locked" : true,
					"width" : 250
				}, {
					"name" : "Description",
					"key" : "description",
					"locked": true,
					"width" : 250
				}, {
					"name" : "Item Category Name",
					"key" : "itemcategoryname",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Item Group",
					"key" : "itemfullname",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Warehouse",
					"key" : "warehouse",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "UOM",
					"key" : "uomname",
					"cellClass" : "text-center",
					"width" : 100
				}, {
					"name" : "On Hand Qty",
					"key" : "onhandqty",
					"format" : "number",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Average Rate",
					"key" : "averagerate",	
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 200
				}, {
					"name" : "Value",
					"key" : "valuenow",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 200
				}]
			};

			customfieldAssign(tempObj.columns, null, 'itemmaster', this.props.app.myResources);

			this.props.initialize(tempObj);
		}

		setTimeout(() => {
			this.getWarehouseDetails();
			this.getItemGroups();
			this.getItemCategories();
		}, 0);

		this.updateLoaderFlag(false);
	}

	getWarehouseDetails() {
		this.updateLoaderFlag(true);

		axios.get(`/api/stocklocations?field=id,name,projectid&filtercondition=stocklocations.isparent AND stocklocations.parentid IS NULL AND stocklocations.projectid is null`).then((response) => {
			if (response.data.message == 'success') {
				if(this.props.reportdata) {
					this.getReportData();
				} else {
					this.props.array.removeAll('warehouseArray');
					this.props.array.removeAll('warehouseid');

					let warehouseid = [];
					for (var i = 0; i < response.data.main.length; i++)
							warehouseid.push(response.data.main[i].id);

					this.props.updateFormState(this.props.form, {
						warehouseid,
						warehouseArray : response.data.main
					});
				}
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	callBackIncludeProjectwarehouse() {
		this.updateLoaderFlag(true);
		let queryString = `/api/stocklocations?field=id,name,projectid&filtercondition=stocklocations.isparent AND stocklocations.parentid IS NULL `;

		if(!this.props.resource.includeprojectwarehouse) {
			queryString += ` AND stocklocations.projectid is null`;
		}

		if(this.props.resource.includeinactivewarehouse)
			queryString += `&includeinactive=true`;

		axios.get(`${queryString}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.array.removeAll('warehouseArray');
				this.props.array.removeAll('warehouseid');

				let warehouseid = [];
				for (var i = 0; i < response.data.main.length; i++)
					if(response.data.main[i].projectid == null)
						warehouseid.push(response.data.main[i].id);

				this.props.updateFormState(this.props.form, {
					warehouseid,
					warehouseArray : response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getItemGroups() {
		this.updateLoaderFlag(true);

		axios.get(`/api/itemgroups?field=id,groupname,fullname,parentid,isparent`).then((response) => {
			if (response.data.message == 'success') {
				this.props.array.removeAll('itemgroupArray');

				this.props.updateFormState(this.props.form, {
					itemgroupArray : response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getItemCategories() {
		this.updateLoaderFlag(true);

		axios.get(`/api/itemcategorymaster?field=id,name`).then((response) => {
			if (response.data.message == 'success') {
				this.props.array.removeAll('itemcategoryArray');

				this.props.updateFormState(this.props.form, {
					itemcategoryArray : response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('tempstockArray');

		let { columns } = this.props.resource;

		let filterString = [];
		['date', 'warehouseid', 'reporttype', 'itemid', 'itemcategoryid', 'itemgroupid', 'stockallitem', 'activeitem', 'exclusivenegativestock'].map((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		if(!this.props.resource.itemgroupid)
			this.props.array.removeAll('groupFilterArray');

		axios.get(`/api/query/stockasondatequery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				if(this.props.resource.groupby == 'Item Group') {
					this.getItemGroupStockArray(response.data.main);
				} else if(this.props.resource.groupby == 'Item Category') {
					this.getItemCategoryStockArray(response.data.main);
				} else {
					let hiddencols = [];

					this.props.resource.columns.forEach((item) => {
						if(item.hidden == true)
							hiddencols.push(item.key);
					});
					this.props.updateFormState(this.props.form, {
						originalRows: response.data.main,
						tempstockArray: response.data.main,
						hiddencols
					});
				}

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	onChangeGroupby () {
		if(this.props.resource.groupby == 'Item Group')
			this.getItemGroupStockArray(this.props.resource.tempstockArray);

		if(this.props.resource.groupby == 'Item Category')
			this.getItemCategoryStockArray(this.props.resource.tempstockArray);

	};

	getItemGroupStockArray (array) {
		let itemgroupArray = JSON.parse(JSON.stringify(this.props.resource.itemgroupArray));

		let indexed_nodes = {};
		let treedata = [];

		itemgroupArray.forEach((item) => {
			let newItem = {
				id: item.id,
				itemname: item.groupname,
				Group: item.groupname,
				isGroup: true,
				parentid: item.parentid,
				isparent: item.isparent,
				children: [],
				onhandqty: 0,
				valuenow: 0
			};

			indexed_nodes[item.id] = newItem;
		});

		itemgroupArray.forEach((item) => {
			let parent_id = item.parentid;

			if (item.parentid === null)
				treedata.push(indexed_nodes[item.id]);
			else
				indexed_nodes[parent_id].children.push(indexed_nodes[item.id]);
		});

		if(this.props.resource.itemgroupid > 0)
			treedata = [indexed_nodes[this.props.resource.itemgroupid]];

		let updateIndexNode = (parentid, arrItem) => {
			if(indexed_nodes[parentid]) {
				indexed_nodes[parentid].onhandqty += arrItem.onhandqty;
				indexed_nodes[parentid].valuenow += arrItem.valuenow;

				if(indexed_nodes[parentid].parentid > 0)
					updateIndexNode(indexed_nodes[parentid].parentid, arrItem);
			}
		}

		array.map((arrItem) => {
			arrItem.onhandqty = Number(arrItem.onhandqty.toFixed(this.props.app.roundOffPrecisionStock));
			arrItem.valuenow = Number(arrItem.valuenow.toFixed(this.props.app.roundOffPrecision));

			if(arrItem.itemgroupid > 0 && indexed_nodes[arrItem.itemgroupid]) {
				indexed_nodes[arrItem.itemgroupid].children.push(arrItem);
				indexed_nodes[arrItem.itemgroupid].onhandqty += arrItem.onhandqty;
				indexed_nodes[arrItem.itemgroupid].valuenow += arrItem.valuenow;

				if(indexed_nodes[arrItem.itemgroupid].parentid) {
					updateIndexNode(indexed_nodes[arrItem.itemgroupid].parentid, arrItem)
				}
			}
		});

		for(var prop in indexed_nodes) {
			indexed_nodes[prop].onhandqty = Number(indexed_nodes[prop].onhandqty.toFixed(this.props.app.roundOffPrecisionStock));
			indexed_nodes[prop].valuenow = Number(indexed_nodes[prop].valuenow.toFixed(this.props.app.roundOffPrecision));
		}

		let hiddencols = [];

		this.props.resource.columns.forEach((item) => {
			if(item.hidden == true)
				hiddencols.push(item.key);
		});
		this.props.updateFormState(this.props.form, {
			tempstockArray: array,
			originalRows: treedata,
			hiddencols
		});
	};

	getItemCategoryStockArray (array) {
		let itemcategoryArray = JSON.parse(JSON.stringify(this.props.resource.itemcategoryArray));

		let indexed_nodes = {};
		let treedata = [];

		itemcategoryArray.forEach((item) => {
			let newItem = {
				id: item.id,
				itemname: item.name,
				Group: item.name,
				isGroup: true,
				children: [],
				onhandqty: 0,
				valuenow: 0
			};

			indexed_nodes[item.id] = newItem;
			treedata.push(indexed_nodes[item.id]);
		});

		if(this.props.resource.itemcategoryid > 0)
			treedata = [indexed_nodes[this.props.resource.itemcategoryid]];

		array.map((arrItem) => {
			if(arrItem.itemcategorymasterid > 0 && indexed_nodes[arrItem.itemcategorymasterid]) {
				indexed_nodes[arrItem.itemcategorymasterid].children.push(arrItem);
				indexed_nodes[arrItem.itemcategorymasterid].onhandqty += arrItem.onhandqty;
				indexed_nodes[arrItem.itemcategorymasterid].valuenow += arrItem.valuenow;
			}
		});

		let hiddencols = [];

		this.props.resource.columns.forEach((item) => {
			if(item.hidden == true)
				hiddencols.push(item.key);
		});
		this.props.updateFormState(this.props.form, {
			tempstockArray: array,
			originalRows: treedata,
			hiddencols
		});
	};

	resetFilter () {
		let tempObj = {
			date : null,
			reporttype : null,
			stockallitem : null,
			activeitem : null,
			groupby : null,
			warehouseid: [],
			tempstockArray : [],
			itemcategoryArray : [],
			itemgroupArray : [],
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Stock Valuation Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Report Type</label>
										<Field name={'reporttype'} props={{options: [{value: "postingdate", label: "By Posting Date"}, {value: "transactiondate", label: "By Transaction Date"}], label: "label", valuename:"value", required: true}} component={localSelectEle} validate={[stringNewValidation({required:true, model: "Report Type"})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Date</label>
										<Field name={'date'} props={{required: true, max: new Date(new Date().setHours(0, 0, 0, 0))}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'Date', max: new Date(new Date().setHours(0, 0, 0, 0))})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12 d-flex">
										<label className="labelclass">Include Inactive Warehouse</label>
										<Field name={'includeinactivewarehouse'} props={{onChange: (value) => this.callBackIncludeProjectwarehouse(this.props.resource.includeprojectwarehouse)}} component={checkboxEle}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Warehouse</label>
										<Field name={'warehouseid'} props={{options: this.props.resource.warehouseArray, label: "name", valuename: "id", required: true, multiselect: true}} component={localSelectEle} validate={[numberNewValidation({required: true, model: 'Warehouse'})]} />
									</div>
									{this.props.app.feature.useProjects ? <div className="form-group col-md-12 col-sm-12 d-flex">
										<label className="labelclass">Include Project Warehouse</label>
										<Field name={'includeprojectwarehouse'} props={{onChange: (value) => this.callBackIncludeProjectwarehouse(value)}} component={checkboxEle}/>
									</div>: null }
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Name</label>
										<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name,displayname", label:"displayname", displaylabel:"name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Group</label>
										<Field name={'itemgroupid'} props={{resource: "itemgroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'stockallitem'} props={{options: [{value: "allitem", label: "All Items"}, {value: "stockitem", label: "In Stock Items"}], label: "label", valuename:"value", required: true}} component={localSelectEle} validate={[stringNewValidation({required:true, model: "Show"})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12 d-flex">
										<label className="labelclass">Exclude Negative Stock</label>
										<Field name={'exclusivenegativestock'} component={checkboxEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Group By</label>
										<Field name={'groupby'} props={{options:["None", "Item Group", "Item Category"], required: true, onChange: () => {this.onChangeGroupby()}}} component={localSelectEle}  validate={[stringNewValidation({required: true, model: 'Grouping By'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Active</label>
										<Field name={'activeitem'} props={{options: ["All", "Active Item", "Inactive Item"], required: true}} component={localSelectEle} validate={[stringNewValidation({required:true, model: 'Active'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid  excelname='Stock As On Date Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} treeCellExpand="itemname" /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

StockAsonDateReportForm = connect(
	(state, props) => {
		let formName = 'stockasondatereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(StockAsonDateReportForm));

export default StockAsonDateReportForm;
