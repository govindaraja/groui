import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

import SettlementDetailsModal from '../components/details/settlementdetailsmodal';

class AccountReceivableReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			invoiceColumns: [{
				"name" : "",
				"headerformat" : "checkbox",
				"key" : "ischecked",
				"locked" : true,
				"format" : "checkbox",
				"cellClass" : "text-center",
				"onChange" : "{report.checkboxOnChange}",
				"restrictToExport" : true,
				"width" : 100
			}, {
				"name" : "Customer",
				"key" : "partnerid_name",
				"format" : "anchortag",
				"transactionname" : "partners",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Sales Person",
				"key" : "salespersonGrp",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Voucher No",
				"key" : "voucherno",
				"format" : "anchortag",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Voucher Date",
				"key" : "voucherdate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Voucher Amount",
				"key" : "voucheramount",
				"format" : "currency",
				"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Outstanding Amount",
				"key" : "balance",
				"format" : "currency",
				"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Exchange Rate",
				"key" : "currencyexchangerate",
				"if" : this.props.app.feature.useMultiCurrency,
				"cellClass" : 'text-right',
				"width" : 150
			}, {
				"name" : "Voucher Amount (" + this.props.app.currency[this.props.app.defaultCurrency].symbol + ")",
				"key" : "voucheramountlc",
				"format" : "defaultcurrency",
				"if" : this.props.app.feature.useMultiCurrency,
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Outstanding Amount (" + this.props.app.currency[this.props.app.defaultCurrency].symbol + ")",
				"key" : "balancelc",
				"format" : "defaultcurrency",
				"if" : this.props.app.feature.useMultiCurrency,
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Remarks",
				"key" : "remarks",
				"width" : 200
			}, {
				"name" : "Due Date",
				"key" : "duedate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Age (In Days)",
				"key" : "age",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Account Name",
				"key" : "accountname",
				"cellClass" : "text-center",
				"width" : 200
			}, {
				"name" : "",
				"format" : "button",
				"buttonname" : "Settlement Details",
				"onClick" : "{report.btnOnClick}",
				"cellClass" : "text-center",
				"restrictToExport" : true,
				"width" : 180
			}, {
				"name" : "Sales Person",
				"key" : "salesperson",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Voucher Type",
				"key" : "vouchertype",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Numbering Series",
				"key" : "numberingseriesmasterid_name",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Territory",
				"key" : "territoryname",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "0-30",
				"key" : "age30",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "31-60",
				"key" : "age60",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "61-90",
				"key" : "age90",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "91-120",
				"key" : "age120",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "121-150",
				"key" : "age150",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "151-180",
				"key" : "age180",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "180 +",
				"key" : "age180above",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Customer Group",
				"key" : "partnerid_customergroupname",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Contact Person",
				"key" : "contactperson",
				"width" : 180
			}, {
				"name" : "Mobile No",
				"key" : "mobile",
				"width" : 150
			}, {
				"name" : "Phone No",
				"key" : "phone",
				"width" : 150
			}, {
				"name" : "PO Number",
				"key" : "invorderno",
				"width" : 150
			}, {
				"name" : "Payment Terms",
				"key" : "paymentterms",
				"width" : 150
			}, {
				"name" : "Legal Name",
				"key" : "partnerid_legalname",
				"cellClass" : "text-center",
				"width" : 180
			}],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.createReceipt = this.createReceipt.bind(this);
		this.createJournal = this.createJournal.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let { invoiceColumns } = this.state;
		let fromdate = new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)));
		let fromDate = new Date(fromdate.setDate(fromdate.getDate() + 1));
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : fromDate,
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				groupby : 'none',
				reportbasedon : 'duedate',
				dueentries : 'all',
				filters: {},
				hiddencols: [],
				columns: []
			};

		customfieldAssign(invoiceColumns, null, 'salesinvoices', this.props.app.myResources);

		this.setState({invoiceColumns});

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let { invoiceColumns } = this.state;

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['dueentries', 'reportbasedon', 'salesperson', 'team', 'customergroupid', 'asondate'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});

		axios.get(`/api/query/accountreceivablequery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {

				for (var i = 0; i < response.data.main.length; i++) {
					response.data.main[i].voucheramountlc = Number((response.data.main[i].voucheramount * (response.data.main[i].currencyexchangerate ? response.data.main[i].currencyexchangerate : 1)).toFixed(this.props.app.roundOffPrecision));

					response.data.main[i].balancelc = Number((response.data.main[i].balance * (response.data.main[i].currencyexchangerate ? response.data.main[i].currencyexchangerate : 1)).toFixed(this.props.app.roundOffPrecision));

					if (response.data.main[i].age < 0)
						response.data.main[i].age = 0;
					if (response.data.main[i].age >= 0 && response.data.main[i].age <= 30)
						response.data.main[i].age30 = response.data.main[i].balancelc;
					else if (response.data.main[i].age > 30 && response.data.main[i].age <= 60)
						response.data.main[i].age60 = response.data.main[i].balancelc;
					else if (response.data.main[i].age > 60 && response.data.main[i].age <= 90)
						response.data.main[i].age90 = response.data.main[i].balancelc;
					else if (response.data.main[i].age > 90 && response.data.main[i].age <= 120)
						response.data.main[i].age120 = response.data.main[i].balancelc;
					else if (response.data.main[i].age > 120 && response.data.main[i].age <= 150)
						response.data.main[i].age150 = response.data.main[i].balancelc;
					else if (response.data.main[i].age > 150 && response.data.main[i].age <= 180)
						response.data.main[i].age180 = response.data.main[i].balancelc;
					else if (response.data.main[i].age > 180)
						response.data.main[i].age180above = response.data.main[i].balancelc;
				}

				if (this.props.resource.groupby == 'customer') {
					let customerObj = {};
					for (var i = 0; i < response.data.main.length; i++) {
						let prop = response.data.main[i].partnerid + '_' + response.data.main[i].currencyid;
						if (customerObj[prop]) {
							customerObj[prop].voucheramount += (response.data.main[i].voucheramount || 0);
							customerObj[prop].voucheramountlc += (response.data.main[i].voucheramountlc || 0);
							customerObj[prop].balance += (response.data.main[i].balance || 0);
							customerObj[prop].balancelc += (response.data.main[i].balancelc || 0);
							customerObj[prop].age30 += (response.data.main[i].age30 || 0);
							customerObj[prop].age60 += (response.data.main[i].age60 || 0);
							customerObj[prop].age90 += (response.data.main[i].age90 || 0);
							customerObj[prop].age120 += (response.data.main[i].age120 || 0);
							customerObj[prop].age150 += (response.data.main[i].age150 || 0);
							customerObj[prop].age180 += (response.data.main[i].age180 || 0);
							customerObj[prop].age180above += (response.data.main[i].age180above || 0);
						} else {
							customerObj[prop] = {
								partnerid : response.data.main[i].partnerid,
								partnerid_name : response.data.main[i].partnerid_name,
								partnerid_customergroupname : response.data.main[i].partnerid_customergroupname,
								territoryname : response.data.main[i].territoryname,
								currencyid : response.data.main[i].currencyid,
								voucheramount : (response.data.main[i].voucheramount || 0),
								voucheramountlc : (response.data.main[i].voucheramountlc || 0),
								balance : (response.data.main[i].balance || 0),
								balancelc : (response.data.main[i].balancelc || 0),
								age30 : (response.data.main[i].age30 || 0),
								age60 : (response.data.main[i].age60 || 0),
								age90 : (response.data.main[i].age90 || 0),
								age120 : (response.data.main[i].age120 || 0),
								age150 : (response.data.main[i].age150 || 0),
								age180 : (response.data.main[i].age180 || 0),
								age180above : (response.data.main[i].age180above || 0)
							};
						}
					}

					response.data.main = [];
					for (var prop in customerObj)
						response.data.main.push(customerObj[prop]);

					let columnArray = ['partnerid_name', 'voucheramount', 'balance', 'age30', 'age60', 'age90', 'age120', 'age150', 'age180', 'age180above', 'partnerid_customergroupname', 'territoryname', 'voucheramountlc', 'balancelc'];

					
					for (var i = 0; i < invoiceColumns.length; i++) {
						invoiceColumns[i].hidden = false;
						if (columnArray.indexOf(invoiceColumns[i].key) == -1)
							invoiceColumns[i].hidden = true;
					}
				} else if (this.props.resource.groupby == 'salesperson') {
					let salesPersonObj = {};
					for (var i = 0; i < response.data.main.length; i++) {
						let prop = response.data.main[i].salespersonid + '_' + response.data.main[i].currencyid;
						if (salesPersonObj[prop]) {
							salesPersonObj[prop].voucheramount += (response.data.main[i].voucheramount || 0);
							salesPersonObj[prop].voucheramountlc += (response.data.main[i].voucheramountlc || 0);
							salesPersonObj[prop].balance += (response.data.main[i].balance || 0);
							salesPersonObj[prop].balancelc += (response.data.main[i].balancelc || 0);
							salesPersonObj[prop].age30 += (response.data.main[i].age30 || 0);
							salesPersonObj[prop].age60 += (response.data.main[i].age60 || 0);
							salesPersonObj[prop].age90 += (response.data.main[i].age90 || 0);
							salesPersonObj[prop].age120 += (response.data.main[i].age120 || 0);
							salesPersonObj[prop].age150 += (response.data.main[i].age150 || 0);
							salesPersonObj[prop].age180 += (response.data.main[i].age180 || 0);
							salesPersonObj[prop].age180above += (response.data.main[i].age180above || 0);
						} else {
							salesPersonObj[prop] = {
								salespersonid : response.data.main[i].salespersonid,
								salespersonGrp : response.data.main[i].salesperson,
								currencyid : response.data.main[i].currencyid,
								voucheramount : (response.data.main[i].voucheramount || 0),
								voucheramountlc : (response.data.main[i].voucheramountlc || 0),
								balance : (response.data.main[i].balance || 0),
								balancelc : (response.data.main[i].balancelc || 0),
								age30 : (response.data.main[i].age30 || 0),
								age60 : (response.data.main[i].age60 || 0),
								age90 : (response.data.main[i].age90 || 0),
								age120 : (response.data.main[i].age120 || 0),
								age150 : (response.data.main[i].age150 || 0),
								age180 : (response.data.main[i].age180 || 0),
								age180above : (response.data.main[i].age180above || 0)
							};
						}
					}

					response.data.main = [];
					for (var prop in salesPersonObj)
						response.data.main.push(salesPersonObj[prop]);

					let columnArray = ['salespersonGrp', 'voucheramount', 'balance', 'age30', 'age60', 'age90', 'age120', 'age150', 'age180', 'age180above', 'voucheramountlc', 'balancelc'];

					for (var i = 0; i < invoiceColumns.length; i++) {
						invoiceColumns[i].hidden = false;
						if (columnArray.indexOf(invoiceColumns[i].key) == -1)
							invoiceColumns[i].hidden = true;
					}
				} else {
					for (var i = 0; i < invoiceColumns.length; i++) {
						invoiceColumns[i].hidden = false;
						if (invoiceColumns[i].key == 'salespersonGrp')
							invoiceColumns[i].hidden = true;
					}
				}

				let hiddencols = [];
				invoiceColumns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns: invoiceColumns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			dueentries : null,
			reportbasedon : null,
			salesperson: null,
			groupby: null,
			team: null,
			customergroupid: null,
			totalinvoiceselected: null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	resourceOnChange() {
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('columns');
	}

	openTransaction(data, transactionname) {
		if(transactionname) {
			let tempObj = {
				fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 3)).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				companyid : this.props.app.user.selectedcompanyid,
				partnerid : data.partnerid
			};
			this.props.history.push({pathname: '/customerstatementreport', params: {...tempObj}});
		} else {
			let link = (data.xsalesinvoiceid > 0) ? `/details/salesinvoices/${data.xsalesinvoiceid}` : `/details/journalvouchers/${data.xjournalvoucherid}`;
			this.props.history.push(link);
		}
	}

	btnOnClick(data) {
		this.props.openModal({
			render: (closeModal) => {
				return <SettlementDetailsModal item={data} relatedresource={data.relatedresource} app={this.props.app} history={this.props.history} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	checkboxHeaderOnChange(param) {
		let checkCount = 0, totaloutstandingamount = 0;
		let filterRows = this.refs.grid.getVisibleRows();
		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if(param) {
				item.ischecked = true;
				totaloutstandingamount += item.balance;
				checkCount ++;
			}
		});

		let totalinvoiceselected = (checkCount > 0) ? (checkCount +" Invoices Selected. Total Outstanding: Rs. " + (totaloutstandingamount.toFixed(2))) : "";
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows,
			totalinvoiceselected
		});
		this.refs.grid.forceRefresh();
	}

	checkboxOnChange(value, item) {
		let tempObj = {}, checkCount = 0, totaloutstandingamount = 0;
		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		setTimeout(() => {
			this.props.resource.originalRows.forEach((rowItem) => {
				if(rowItem.ischecked) {
					totaloutstandingamount += rowItem.balance;
					checkCount ++;
				}
			});
			tempObj.totalinvoiceselected = (checkCount > 0) ? (checkCount +" Invoices Selected. Total Outstanding: Rs. " + (totaloutstandingamount.toFixed(2))) : "";
			this.props.updateFormState(this.props.form, tempObj);
		}, 0);
		this.refs.grid.refresh();
	}

	createReceipt (param) {
		let receiptArray = [], errorFound = false;
		for(var i = 0; i < this.props.resource.originalRows.length; i++) {
			if(this.props.resource.originalRows[i].ischecked) {
				let mismatchCustomer = false, mismatchCurrency = false;
				for(var j = 0; j < receiptArray.length; j++) {
					if(receiptArray[j].currencyid != this.props.resource.originalRows[i].currencyid) {
						mismatchCurrency = true;
						break;
					}
					if(receiptArray[j].partnerid != this.props.resource.originalRows[i].partnerid) {
						mismatchCustomer = true;
						break;
					}
				}
				if(mismatchCustomer) {
					errorFound=true;
					return this.props.openModal(modalService["infoMethod"]({
						header : "Error",
						body : "Partner Name Mismatched",
						btnArray : ["Ok"]
					}));				
				} else if(mismatchCurrency) {
					errorFound=true;
					return this.props.openModal(modalService["infoMethod"]({
						header : "Error",
						body : "Currency Mismatched",
						btnArray : ["Ok"]
					}));				
				} else {
					receiptArray.push(this.props.resource.originalRows[i]);
				}
                	}
		}

		if (!errorFound) {
			if (receiptArray.length > 0) {
				this.props.history.push({pathname: '/createReceiptVoucher', params: {receiptArray : receiptArray, param: 'Accounts Receivable Report'}});
			} else {
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please Choose atleast one item',
					btnArray : ['Ok']
				}));
			}
		}
	}

	createJournal(param) {
		let voucherArray = [], errorFound = false;

		for(var i = 0; i < this.props.resource.originalRows.length; i++) {
			if(this.props.resource.originalRows[i].ischecked) {
				let mismatchCurrency = false;
				if(voucherArray.length > 0) {
					if(voucherArray[0].currencyid != this.props.resource.originalRows[i].currencyid)
						mismatchCurrency=true;
				}
				if(mismatchCurrency) {
					errorFound=true;
					return this.props.openModal(modalService["infoMethod"]({
						header : "Error",
						body : "Currency Mismatched",
						btnArray : ["Ok"]
					}));				
				} else {
					voucherArray.push(this.props.resource.originalRows[i]);
				}
                	}
		}
		
		if(voucherArray.length > 50) {
			errorFound=true;
			return this.props.openModal(modalService["infoMethod"]({
				header : "Error",
				body : "Please Choose only 50 Vouchers",
				btnArray : ["Ok"]
				}));
		}

		if (!errorFound) {
			if (voucherArray.length > 0) {
				this.props.history.push({pathname: '/createJournalVoucher', params: {voucherArray : voucherArray, param: 'Accounts Receivable Report'}});
			} else {
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please Choose atleast one item',
					btnArray : ['Ok']
				}));
			}
		}
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Accounts Receivable</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							{(this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && this.props.resource.groupby == 'none' && (checkActionVerbAccess(this.props.app, 'receiptvouchers', 'Save') || checkActionVerbAccess(this.props.app, 'journals', 'Save'))) ? <div className="report-header-rightpanel">
								{checkActionVerbAccess(this.props.app, 'receiptvouchers', 'Save') ? <button type="button" className="btn gs-btn-success btn-sm btn-width" onClick={this.createReceipt}><i className="fa fa-plus"></i>Receipt</button> : null}
								{checkActionVerbAccess(this.props.app, 'journals', 'Save') ? <button type="button" className="btn gs-btn-success btn-sm btn-width" onClick={this.createJournal}><i className="fa fa-plus"></i>Journal</button> : null}
							</div> : null}
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Entries</label>
										<Field name={'dueentries'} props={{options: [{value: "all", label: "All Entries"}, {value: "overdue", label: "Over Due Entries"}], label:"label", valuename: "value", required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Entries'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Based On</label>
										<Field name={'reportbasedon'} props={{options: [{value: "duedate", label: "Due Date"}, {value: "voucherdate", label: "Voucher Date"}], label:"label", valuename: "value", required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Based On'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">As On Date</label>
										<Field name={'asondate'} props={{required: false}} component={DateEle} validate={[dateNewValidation({required: false, model: 'As On date'})]} />
									</div>
									{this.props.app.user.roleid.indexOf(2) == -1 ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Sales Person</label>
										<Field name={'salesperson'} props={{resource: "users", fields: "id,displayname", label: "displayname", filter: "users.issalesperson=true"}} component={selectAsyncEle} />
									</div> : null }
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Group By</label>
										<Field name={'groupby'} props={{options: [{value: "none", label: "None"}, {value: "customer", label: "Customer"}, {value: "salesperson", label: "Salesperson"}], label:"label", valuename: "value", required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Group By'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Team</label>
										<Field name={'team'} props={{resource: "teamstructure", fields: "id,fullname", label: "fullname"}} component={selectAsyncEle} />
									</div>
									{this.props.resource.groupby != "salesperson" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer Group</label>
										<Field name={'customergroupid'} props={{resource: "customergroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div> : null }
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12" >
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.totalinvoiceselected ? <div className="col-md-12 col-sm-12 paddingleft-30"><span>{this.props.resource.totalinvoiceselected}</span></div> : null}
							{this.props.resource ? <Reactuigrid excelname='Accounts Receivable Report' app={this.props.app} ref="grid" report={this}gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} btnOnClick={this.btnOnClick} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

AccountReceivableReportForm = connect(
	(state, props) => {
		let formName = 'accountreceivablereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(AccountReceivableReportForm));

export default AccountReceivableReportForm;
