import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import XLSX from 'xlsx';
import { saveAs } from 'file-saver';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, costcenterAutoMultiSelectEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { currencyFilter, dateFilter} from '../utils/filter';
import { excel_sheet_from_array_of_arrays, excel_Workbook, excel_s2ab } from '../utils/excelutils';
import Loadingcontainer from '../components/loadingcontainer';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import { Treebeard, decorators } from 'react-treebeard';
import Printmodal from '../components/details/printmodal';
import TreeView from '../components/treeviewcomponent';

class ProfitLossReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.printfunction = this.printfunction.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
		this.costCategoryChange = this.costCategoryChange.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		let fromDate = new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)));

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate :fromDate,
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				companyid : [this.props.app.user.selectedcompanyid],
				companyArray : JSON.parse(JSON.stringify(this.props.app.companyArray)),
				nodeArray : []
			}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);

		let nodeArray = [];

		this.props.updateFormState(this.props.form, {
			nodeArray, groupArray: []
		});

		axios.get(`/api/query/profitlossquery?fromdate=${new Date(this.props.resource.fromdate).toDateString()}&todate=${new Date(this.props.resource.todate).toDateString()}&companyid=${this.props.resource.companyid}&costcategoryid=${this.props.resource.costcategoryid}&filtercostcategoryid=${this.props.resource.filtercostcenterid ? this.props.resource.filtercostcenterid.categoryid : null}&filtercostcenterid=${this.props.resource.filtercostcenterid ? this.props.resource.filtercostcenterid.costcenterid : null}&schedule=${this.props.resource.schedule ? this.props.resource.schedule : ''}`).then((response) => {
			if (response.data.message == 'success') {

				response.data.main.forEach((item) => {
					if(!item.isledger || (item.balance && item.isledger))
						nodeArray.push({
							id : item.id,
							name : item.displayname,
							displayname : item.displayname,
							type : item.type,
							isledger : item.isledger,
							balance : item.balance,
							groupObj: item.groupObj,
							parentid : item.parentid,
							children : []
						});
				});

				this.props.updateFormState(this.props.form, {
					nodeArray, groupArray: response.data.groupArray
				});

				this.props.updateReportFilter('profitlossreport', this.props.resource);

				if(this.props.resource.nodeArray.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	costCategoryChange() {
		this.props.updateFormState(this.props.form, {
			filtercostcenterid: null
		});
	}

	printfunction = () => {
		this.props.openModal({
			render: (closeModal) => {
				let url = `query/profitlossquery?fromdate=${new Date(this.props.resource.fromdate).toDateString()}&todate=${new Date(this.props.resource.todate).toDateString()}&param=print&companyid=${this.props.resource.companyid}`;

				return <Printmodal app={this.props.app} resourcename={'profitloss'} companyid={this.props.resource.companyid}  openModal={this.props.openModal} closeModal={closeModal} url={url}/>
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	exportExcel () {
		let reportData = [[{v : ''}, {v : 'Report Name'}, {v : 'Profit Loss'}],[],[{v : ''}, {v : 'Company Name'}, {v : this.props.app.selectedCompanyDetails.legalname}], [], [], [{v : ''}, {v : 'From Date'}, {v : dateFilter(this.props.resource.fromdate)}, {v : ''}, {v : 'To Date'}, {v : dateFilter(this.props.resource.todate)}], [], []];

		let totalChildLevel = 0;

		this.props.resource.flatTreeViewData.forEach((item) => {
			if(item.displayIndent > totalChildLevel)
				totalChildLevel = item.displayIndent;
		});

		if(this.props.resource.groupArray.length > 0) {
			let tempHeaderArray = [];

			tempHeaderArray.push({
				v: 'Account Name'
			});
			/*for(let i=0; i<(totalChildLevel + 1); i++) {
				tempHeaderArray.push({
					v: ''
				});
			}*/

			this.props.resource.groupArray.forEach(group => {
				let groupDisplayName = group.displayname ? group.displayname : (`${dateFilter(group.min)}\nto\n${dateFilter(group.max)}`);

				tempHeaderArray.push({
					v: groupDisplayName
				});
			});

			tempHeaderArray.push({
				v: 'Cummulative'
			});

			reportData.push(tempHeaderArray);
		}

		this.props.resource.flatTreeViewData.forEach((item, index) => {
			let tempDataArray = [];

			/*for(let i=0; i<item.displayIndent; i++) {
				tempDataArray.push({
					v: ''
				});
			}*/
			//let tabSpace = `	`;
			let tabSpace = `       `;
			let genDisplayName = ``;
			for(let i=0; i<item.displayIndent; i++)
				genDisplayName += tabSpace;

			genDisplayName += item.displayname;

			tempDataArray.push({
				v: genDisplayName
			});
			/*for(let i=item.displayIndent; i<(totalChildLevel + 1); i++) {
				tempDataArray.push({
					v: ''
				});
			}*/
			this.props.resource.groupArray.forEach(group => {
				tempDataArray.push({
					v: item.groupObj[group.name].balance
				});
			});
			tempDataArray.push({
				v: item.balance
			});
			reportData.push(tempDataArray);
			if(index == this.props.resource.flatTreeViewData.length - 2)
				reportData.push([], [], []);
		});

		let wb = {
			SheetNames : [],
			Sheets : {}
		};

		let ws = excel_sheet_from_array_of_arrays(reportData);
		wb.SheetNames.push('Profit Loss');
		wb.Sheets['Profit Loss'] = ws;
		ws['!cols'] = [{
				wch : 15 * totalChildLevel
			}, {
				wch : 20
			}, {
				wch : 20
			}
		];

		for(var j = 0; j < (this.props.resource.groupArray.length + totalChildLevel + 1); j++)
			ws['!cols'].splice(1, 0, {
				wch : 20
			});

		let wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
		saveAs(new Blob([excel_s2ab(wbout)],{type:"application/octet-stream"}), "ProfitLoss_" + (dateFilter(new Date())) + ".xlsx");
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			companyid : [this.props.app.user.selectedcompanyid],
			nodeArray : []
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Profit and Loss Report</div>
							{this.props.resource.nodeArray.length > 0 ? <div className="pull-right margintop-10">
								{!this.props.resource.schedule && !this.props.resource.costcategoryid ? <button type="button" className="btn btn-sm gs-btn-outline-success" onClick={this.printfunction} rel="tooltip" title="Print"><span className="fa fa-print"></span></button> : null}
								<button type="button" className="btn btn-sm gs-btn-outline-primary marginleft-5" onClick={this.exportExcel} rel="tooltip" title="Export to Excel"><span className="fa fa-file-excel-o"></span></button>
							</div> : null}
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Company</label>
										<Field name={'companyid'} props={{options: this.props.resource.companyArray, label: "name", valuename: "id", required: true, multiselect: true}} component={localSelectEle} validate={[numberNewValidation({required: true, model: 'Company'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									{this.props.app.feature.enableCostCenter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Cost Center</label>
										<Field name={'filtercostcenterid'} props={{restrictToSingleSelect: true, showUncategorized: true, disabled: this.props.costcategoryid ? true : false}} component={costcenterAutoMultiSelectEle} />
									</div> : null}
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Compare Period</label>
										<Field name={'schedule'}  props={{options: [{value: "monthly", label: "Monthly"}, {value: "quarterly", label: "Quarterly"}, {value: "halfyearly", label: "Half Yearly"}, {value: "yearly", label: "Yearly"}], label:"label", valuename: "value", disabled: this.props.resource.costcategoryid ? true : false}}  component={localSelectEle} />
									</div>
									{ this.props.app.feature.enableCostCenter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Compare Category</label>
										<Field name={'costcategoryid'} props={{resource: "costcentercategories", fields: "id,name", disabled: (this.props.resource.schedule || this.props.resource.filtercostcenterid) ? true : false}} component={selectAsyncEle} />
									</div> : null }
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.nodeArray} />
							{this.props.resource.nodeArray.length > 0 ? <TreeViewModal resource = {this.props.resource} app = {this.props.app} history = {this.props.history} updateFormState = {this.props.updateFormState} form = {this.props.form} updateLoaderFlag={this.updateLoaderFlag}></TreeViewModal> : null}
						</div>
					</div>
				</form>
			</>
		);
	};
}

class TreeViewModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.onLoad = this.onLoad.bind(this);
		this.openDetails = this.openDetails.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	componentWillReceiveProps() {
		this.onLoad();
	}

	onLoad() {
		let treeViewData = [], netProvision = 0;

		let nodeArray = JSON.parse(JSON.stringify(this.props.resource.nodeArray));
		let indexed_nodes = {};
		let netIncome, netExpense;

		nodeArray.forEach((item) => {
			item.clickfn = this.openDetails;
			indexed_nodes[item.id] = item;
		});

		nodeArray.forEach((item) => {
			let parent_id = item.parentid;

			if (item.parentid === null)
				treeViewData.push(item);
			else
				indexed_nodes[parent_id].children.push(item);
		});

		treeViewData.forEach((item) => {
			if(item.type == 'Income')
				netIncome = item;
			if(item.type == 'Expense')
				netExpense = item;

			if(item.children.length == 0)
				delete item.children;
			else
				recursivefn(item.children);
		});

		netProvision = Number((netIncome.balance - netExpense.balance).toFixed(this.props.app.roundOffPrecision));

		function recursivefn (childArr) {
			for (var j = 0; j < childArr.length; j++) {
				if(childArr[j].children && childArr[j].children.length == 0) {
					if(childArr[j].isledger) {
						delete childArr[j].children;
					} else {
						childArr.splice(j, 1);
						j = j - 1;
					}
				} else {
					recursivefn(childArr[j].children);
					if(childArr[j].children.length == 0) {
						childArr.splice(j, 1);
						j = j - 1;
					}
				}
			}
		}

		let tempProvObj = {
			name: 'Provisional Profit / Loss',
			displayname: 'Provisional Profit / Loss',
			balance: netProvision,
			groupObj: {}
		};

		if(this.props.resource.groupArray.length > 0) {
			this.props.resource.groupArray.forEach(group => {
				tempProvObj.groupObj[group.name] = {
					balance: Number((netIncome.groupObj[group.name].balance - netExpense.groupObj[group.name].balance).toFixed(this.props.app.roundOffPrecision))
				};
			});
		}

		treeViewData.push(tempProvObj);

		this.setState({ treeViewData });
		this.props.updateFormState(this.props.form, {
			treeViewData
		});
	}

	openDetails(item, group){
		this.props.updateLoaderFlag(true);
		if(item.isledger) {
			let tempObj = {
				param: 'Profit Loss',
				fromdate: group && group.min ? group.min : this.props.resource.fromdate,
				todate: group && group.max ? group.max : this.props.resource.todate,
				accountid: item.id,
				companyid: this.props.app.selectedcompanyid
			};
			if(this.props.resource.costcategoryid)
				tempObj.costcategoryid = this.props.resource.costcategoryid;
			if(this.props.resource.filtercostcenterid) {
				tempObj.costcategoryid = this.props.resource.filtercostcenterid.categoryid;
				tempObj.costcenterid = this.props.resource.filtercostcenterid;
			}
			if(group && group.name && this.props.resource.costcategoryid)
				tempObj.costcenterid = {categoryid: this.props.resource.costcategoryid, costcenterid: group.name};

			this.props.history.push({
				pathname: tempObj.costcategoryid || tempObj.costcenterid ? '/generalledgercostsplitupreport' : '/generalledgerreport',
				params: tempObj
			});
		}

		this.props.updateLoaderFlag(false);
	}

	render() {
		let { treeViewData } = this.state;
		return (
			<div className="col-md-10 offset-md-1 col-xs-12 col-sm-12 margintop-20 marginbottom-30">
				<TreeView data={treeViewData} groupArray={this.props.resource.groupArray} app={this.props.app} updateFormState={this.props.updateFormState} form={this.props.form} />
			</div>
		);
	}
}

/*class TreeViewModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			netProvision : 0,
			treedata: [],
			decorators : decorators,
			treeviewStyles : {
				tree: {
					base: {
						listStyle: 'none',
						backgroundColor: '#fff',
						margin: 0,
						padding: 0,
						color: '#333'
					},
					node: {
						base: {
							position: 'relative'
						},
						link: {
							cursor: 'pointer',
							position: 'relative',
							padding: '0px 5px',
							display: 'block'
						},
						activeLink: {
							background: '#fff'
						},
						toggle: {
							base: {
								position: 'relative',
								display: 'inline-block',
								verticalAlign: 'top',
								marginLeft: '-5px',
								height: '24px',
								width: '24px'
							},
							wrapper: {
								position: 'absolute',
								top: '50%',
								left: '50%',
								margin: '-7px 0 0 -7px',
								height: '14px'
							},
							height: 14,
							width: 14,
							arrow: {
								fill: '#333',
								strokeWidth: 0
							}
						},
						header: {
							base: {
								display: 'inline-block',
								verticalAlign: 'top',
								color: '#333'
							},
							connector: {
								width: '2px',
								height: '12px',
								borderLeft: 'solid 2px black',
								borderBottom: 'solid 2px black',
								position: 'absolute',
								top: '0px',
								left: '-21px'
							},
							title: {
								lineHeight: '24px',
								verticalAlign: 'middle'
							}
						},
						subtree: {
							listStyle: 'none',
							paddingLeft: '19px'
						},
						loading: {
							color: '#E2C089'
						}
					}
				}
			}
		};

		this.onLoad = this.onLoad.bind(this);
		this.openDetails = this.openDetails.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	componentWillReceiveProps() {
		this.onLoad();
	}

	onLoad() {
		let treedata = [], decorators = this.state.decorators, netProvision = 0;

		let nodeArray = JSON.parse(JSON.stringify(this.props.resource.nodeArray));
		let indexed_nodes = {};
		let netIncome;
		let netExpense;

		nodeArray.forEach((item) => {
			item['openDetails'] = this.openDetails,
			item['toggled'] = true,
			indexed_nodes[item.id] = item;
		});

		nodeArray.forEach((item) => {
			let parent_id = item.parentid;

			if (item.parentid === null)
				treedata.push(item);
			else
				indexed_nodes[parent_id].children.push(item);
		});

		treedata.forEach((item) => {
			if(item.type == 'Income')
				netIncome = item.balance;
			if(item.type == 'Expense')
				netExpense = item.balance;
			
			if(item.children.length == 0)
				delete item.children;
			else
				recursivefn(item.children);
		});

		netProvision = Number((netIncome - netExpense).toFixed(this.props.app.roundOffPrecision));

		function recursivefn (childArr) {
			for (var j = 0; j < childArr.length; j++) {
				if(childArr[j].children.length == 0) {
					if(childArr[j].isledger) {
						delete childArr[j].children;
					} else {
						childArr.splice(j, 1);
						j = j - 1;
					}
				} else {
					recursivefn(childArr[j].children);
				}
			}
			/*childArr.forEach((item) => {
				if(item.children.length == 0)
					delete item.children;
				else
					recursivefn(item.children);
			});*/
		/*}

		decorators.Container = ({style, node}) => {
			const iconType = node.children && node.children.length > 0 ? (node.toggled ? 'folder-open-o' : 'folder-o') : 'file-text-o';
			const iconClass = `fa fa-${iconType}`;
			const iconStyle = {marginRight: '5px'};

			return (
				<div className="hovercolor" style={style.base} onClick={()=>node.openDetails(node)}>
					<div style={style.title}>
						<i className={iconClass} style={iconStyle}/>
						{node.name}
						<div className="pull-right marginright-15">{currencyFilter(node.balance, this.props.app.defaultCurrency, this.props.app)}</div>
					</div>
				</div>
			);
		};

		this.setState({ treedata, decorators, netProvision });
		this.props.updateFormState(this.props.form, {
			treedata, netProvision
		});
	}

	openDetails(item){
		if(item.isledger)
			this.props.history.push({
				pathname: '/generalledgerreport',
				params: {
					fromdate : this.props.resource.fromdate,
					todate : this.props.resource.todate,
					accountid : item.id,
					costcenterid : this.props.resource.costcenterid,
					companyid : this.props.app.selectedcompanyid,
					param: 'Profit Loss'
				}
			});
	}

	render() {
		let { treedata, treeviewStyles } = this.state;
		return (
			<div className="col-md-12">
				<div className="margintop-20 marginbottom-30 col-md-12 col-xs-12 col-sm-12 bg-white">
					<Treebeard data={this.state.treedata} decorators={this.state.decorators} style={treeviewStyles}/>
					<hr />
					<b>Provisional Profit / Loss</b>
					<b className="pull-right marginright-15">{currencyFilter(this.state.netProvision, this.props.app.defaultCurrency, this.props.app)}</b>
				</div>
			</div>
		);
	}
}*/

ProfitLossReportForm = connect(
	(state, props) => {
		let formName = 'profitlossreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ProfitLossReportForm));

export default ProfitLossReportForm;
