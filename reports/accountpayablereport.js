import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

import SettlementDetailsModal from '../components/details/settlementdetailsmodal';

class AccountPayableReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			invoiceColumns: [{
				"name" : "",
				"headerformat" : "checkbox",
				"key" : "ischecked",
				"locked" : true,
				"format" : "checkbox",
				"cellClass" : "text-center",
				"onChange" : "{report.checkboxOnChange}",
				"restrictToExport" : true,
				"width" : 100
			}, {
				"name" : "Supplier",
				"key" : "supplier",
				"format" : "anchortag",
				"transactionname" : "partners",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Voucher No",
				"key" : "voucherno",
				"format" : "anchortag",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Supplier Invoice Date",
				"key" : "supplierinvoicedate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Posting Date(Voucher Date)",
				"key" : "voucherdate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Partner",
				"key" : "partnerid_name",
				"format" : "anchortag",
				"transactionname" : "customerstatements",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Voucher Amount",
				"key" : "voucheramount",
				"format" : "currency",
				"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Outstanding Amount",
				"key" : "balance",
				"format" : "currency",
				"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Exchange Rate",
				"key" : "currencyexchangerate",
				"if" : this.props.app.feature.useMultiCurrency,
				"cellClass" : 'text-right',
				"width" : 150
			}, {
				"name" : "Voucher Amount (" + this.props.app.currency[this.props.app.defaultCurrency].symbol + ")",
				"key" : "voucheramountlc",
				"format" : "defaultcurrency",
				"if" : this.props.app.feature.useMultiCurrency,
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Outstanding Amount (" + this.props.app.currency[this.props.app.defaultCurrency].symbol + ")",
				"key" : "balancelc",
				"format" : "defaultcurrency",
				"if" : this.props.app.feature.useMultiCurrency,
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "",
				"format" : "button",
				"buttonname" : "Settlement Details",
				"onClick" : "{report.btnOnClick}",
				"cellClass" : "text-center",
				"restrictToExport" : true,
				"width" : 180
			}, {
				"name" : "Due Date",
				"key" : "duedate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Age (In Days)",
				"key" : "age",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Account Name",
				"key" : "accountname",
				"cellClass" : "text-center",
				"width" : 200
			}, {
				"name" : "Voucher Type",
				"key" : "vouchertype",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Numbering Series",
				"key" : "numberingseriesmasterid_name",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "0-30",
				"key" : "age30",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "31-60",
				"key" : "age60",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "61-90",
				"key" : "age90",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "91-180",
				"key" : "age180",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "180 +",
				"key" : "age180above",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.createPayment = this.createPayment.bind(this);
		this.createJournal = this.createJournal.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let { invoiceColumns } = this.state;
		let fromdate = new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)));
		let fromDate = new Date(fromdate.setDate(fromdate.getDate() + 1));
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : fromDate,
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				groupby : 'none',
				reportbasedon : 'duedate',
				dueentries : 'all',
				filters: {},
				hiddencols: [],
				columns: []
			};

		customfieldAssign(invoiceColumns, null, 'purchaseinvoices', this.props.app.myResources);

		this.setState({invoiceColumns});

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let { invoiceColumns } = this.state;

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		axios.get(`/api/query/accountpayablequery?dueentries=${this.props.resource.dueentries}&reportbasedon=${this.props.resource.reportbasedon}&asondate=${this.props.resource.asondate ? this.props.resource.asondate : ''}`).then((response) => {
			if (response.data.message == 'success') {

				for (var i = 0; i < response.data.main.length; i++) {
					response.data.main[i].voucheramountlc = Number((response.data.main[i].voucheramount * (response.data.main[i].currencyexchangerate ? response.data.main[i].currencyexchangerate : 1)).toFixed(this.props.app.roundOffPrecision));

					response.data.main[i].balancelc = Number((response.data.main[i].balance * (response.data.main[i].currencyexchangerate ? response.data.main[i].currencyexchangerate : 1)).toFixed(this.props.app.roundOffPrecision));

					if (response.data.main[i].age < 0)
						response.data.main[i].age = 0;
					if (response.data.main[i].age >= 0 && response.data.main[i].age <= 30)
						response.data.main[i].age30 = response.data.main[i].balancelc;
					else if (response.data.main[i].age > 30 && response.data.main[i].age <= 60)
						response.data.main[i].age60 = response.data.main[i].balancelc;
					else if (response.data.main[i].age > 60 && response.data.main[i].age <= 90)
						response.data.main[i].age90 = response.data.main[i].balancelc;
					else if (response.data.main[i].age > 90 && response.data.main[i].age <= 180)
						response.data.main[i].age180 = response.data.main[i].balancelc;
					else if (response.data.main[i].age > 180)
						response.data.main[i].age180above = response.data.main[i].balancelc;
				}

				if (this.props.resource.groupby == 'supplier') {
					let supplierObj = {};
					for (var i = 0; i < response.data.main.length; i++) {
						let prop = response.data.main[i].partnerid + '_' + response.data.main[i].currencyid;
						if (supplierObj[prop]) {
							supplierObj[prop].voucheramount += (response.data.main[i].voucheramount || 0);
							supplierObj[prop].voucheramountlc += (response.data.main[i].voucheramountlc || 0);
							supplierObj[prop].balance += (response.data.main[i].balance || 0);
							supplierObj[prop].balancelc += (response.data.main[i].balancelc || 0);
							supplierObj[prop].age30 += (response.data.main[i].age30 || 0);
							supplierObj[prop].age60 += (response.data.main[i].age60 || 0);
							supplierObj[prop].age90 += (response.data.main[i].age90 || 0);
							supplierObj[prop].age120 += (response.data.main[i].age120 || 0);
							supplierObj[prop].age150 += (response.data.main[i].age150 || 0);
							supplierObj[prop].age180 += (response.data.main[i].age180 || 0);
							supplierObj[prop].age180above += (response.data.main[i].age180above || 0);
						} else {
							supplierObj[prop] = {
								partnerid : response.data.main[i].partnerid,
								supplier : response.data.main[i].partnerid_name,
								currencyid : response.data.main[i].currencyid,
								voucheramount : (response.data.main[i].voucheramount || 0),
								voucheramountlc : (response.data.main[i].voucheramountlc || 0),
								balance : (response.data.main[i].balance || 0),
								balancelc : (response.data.main[i].balancelc || 0),
								age30 : (response.data.main[i].age30 || 0),
								age60 : (response.data.main[i].age60 || 0),
								age90 : (response.data.main[i].age90 || 0),
								age120 : (response.data.main[i].age120 || 0),
								age150 : (response.data.main[i].age150 || 0),
								age180 : (response.data.main[i].age180 || 0),
								age180above : (response.data.main[i].age180above || 0)
							};
						}
					}

					response.data.main = [];
					for (var prop in supplierObj)
						response.data.main.push(supplierObj[prop]);

					let columnArray = ['supplier', 'voucheramount', 'balance', 'age30', 'age60', 'age90', 'age120', 'age150', 'age180', 'age180above', 'voucheramountlc', 'balancelc'];

					for (var i = 0; i < invoiceColumns.length; i++) {
						invoiceColumns[i].hidden = false;
						if (columnArray.indexOf(invoiceColumns[i].key) == -1)
							invoiceColumns[i].hidden = true;
					}
				} else {
					for (var i = 0; i < invoiceColumns.length; i++) {
						invoiceColumns[i].hidden = false;
						if (invoiceColumns[i].key == 'supplier')
							invoiceColumns[i].hidden = true;
					}
				}

				let hiddencols = [];
				invoiceColumns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns: invoiceColumns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			dueentries : null,
			reportbasedon : null,
			groupby: null,
			totalinvoiceselected: null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	resourceOnChange() {
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('columns');
	}

	openTransaction(data, transactionname) {
		if(transactionname) {
			let tempObj = {
				fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 3)).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				companyid : this.props.app.user.selectedcompanyid,
				partnerid : data.partnerid
			};
			this.props.history.push({pathname: '/customerstatementreport', params: {...tempObj}});
		} else {
			let link = (data.xpurchaseinvoiceid > 0) ? `/details/purchaseinvoices/${data.xpurchaseinvoiceid}` : `/details/journalvouchers/${data.xjournalvoucherid}`;
			this.props.history.push(link);
		}
	}

	btnOnClick(data) {
		this.props.openModal({
			render: (closeModal) => {
				return <SettlementDetailsModal item={data} relatedresource={data.relatedresource} app={this.props.app} history={this.props.history}  closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	checkboxHeaderOnChange(param) {
		let checkCount = 0, totaloutstandingamount = 0;
		let filterRows = this.refs.grid.getVisibleRows();
		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if(param) {
				item.ischecked = true;
				totaloutstandingamount += item.balance;
				checkCount ++;
			}
		});

		let totalinvoiceselected = (checkCount > 0) ? (checkCount +" Invoices Selected. Total Outstanding: Rs. " + (totaloutstandingamount.toFixed(2))) : "";
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows,
			totalinvoiceselected
		});
		this.refs.grid.forceRefresh();
	}

	checkboxOnChange(value, item) {
		let tempObj = {}, checkCount = 0, totaloutstandingamount = 0;
		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		setTimeout(() => {
			this.props.resource.originalRows.forEach((rowItem) => {
				if(rowItem.ischecked) {
					totaloutstandingamount += rowItem.balance;
					checkCount ++;
				}
			});
			tempObj.totalinvoiceselected = (checkCount > 0) ? (checkCount +" Invoices Selected. Total Outstanding: Rs. " + (totaloutstandingamount.toFixed(2))) : "";
			this.props.updateFormState(this.props.form, tempObj);
		}, 0);
		this.refs.grid.refresh();
	}

	createPayment (param) {
		let paymentArray = [], errorFound = false;
		for(var i = 0; i < this.props.resource.originalRows.length; i++) {
			if(this.props.resource.originalRows[i].ischecked) {
				let mismatchCustomer = false, mismatchCurrency = false;
				for(var j = 0; j < paymentArray.length; j++) {
					if(paymentArray[j].currencyid != this.props.resource.originalRows[i].currencyid) {
						mismatchCurrency = true;
						break;
					}
					if(paymentArray[j].partnerid != this.props.resource.originalRows[i].partnerid) {
						mismatchCustomer = true;
						break;
					}
				}
				if(mismatchCustomer) {
					errorFound=true;
					return this.props.openModal(modalService["infoMethod"]({
						header : "Error",
						body : "Partner Name Mismatched",
						btnArray : ["Ok"]
					}));				
				} else if(mismatchCurrency) {
					errorFound=true;
					return this.props.openModal(modalService["infoMethod"]({
						header : "Error",
						body : "Currency Mismatched",
						btnArray : ["Ok"]
					}));				
				} else {
					paymentArray.push(this.props.resource.originalRows[i]);
				}
                	}
		}

		if (!errorFound) {
			if (paymentArray.length > 0) {
				this.props.history.push({pathname: '/createPaymentVoucher', params: {paymentArray : paymentArray, param: 'Accounts Payable Report'}});
			} else {
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please Choose atleast one item',
					btnArray : ['Ok']
				}));
			}
		}
	}

	createJournal(param) {
		let voucherArray = [], errorFound = false;

		for(var i = 0; i < this.props.resource.originalRows.length; i++) {
			if(this.props.resource.originalRows[i].ischecked) {
				let mismatchCurrency = false;
				if(voucherArray.length > 0) {
					if(voucherArray[0].currencyid != this.props.resource.originalRows[i].currencyid)
						mismatchCurrency=true;
				}
				if(mismatchCurrency) {
					errorFound=true;
					return this.props.openModal(modalService["infoMethod"]({
						header : "Error",
						body : "Currency Mismatched",
						btnArray : ["Ok"]
					}));				
				} else {
					voucherArray.push(this.props.resource.originalRows[i]);
				}
                	}
		}
		
		if(voucherArray.length > 50) {
			errorFound=true;
			return this.props.openModal(modalService["infoMethod"]({
				header : "Error",
				body : "Please Choose only 50 Vouchers",
				btnArray : ["Ok"]
				}));
		}

		if (!errorFound) {
			if (voucherArray.length > 0) {
				this.props.history.push({pathname: '/createJournalVoucher', params: {voucherArray : voucherArray, param: 'Accounts Payable Report'}});
			} else {
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please Choose atleast one item',
					btnArray : ['Ok']
				}));
			}
		}
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Accounts Payable</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							{(this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && this.props.resource.groupby == 'none' && (checkActionVerbAccess(this.props.app, 'paymentvouchers', 'Save') || checkActionVerbAccess(this.props.app, 'journals', 'Save'))) ? <div className="report-header-rightpanel">
								{checkActionVerbAccess(this.props.app, 'paymentvouchers', 'Save') ? <button type="button" className="btn gs-btn-success btn-sm btn-width" onClick={this.createPayment}><i className="fa fa-plus"></i>Payment</button> : null}
								{checkActionVerbAccess(this.props.app, 'journals', 'Save') ? <button type="button" className="btn gs-btn-success btn-sm btn-width" onClick={this.createJournal}><i className="fa fa-plus"></i>Journal</button> : null}
							</div> : null}
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Entries</label>
										<Field name={'dueentries'} props={{options: [{value: "all", label: "All Entries"}, {value: "overdue", label: "Over Due Entries"}], label:"label", valuename: "value", required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Entries'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Based On</label>
										<Field name={'reportbasedon'} props={{options: [{value: "duedate", label: "Due Date"}, {value: "supplierinvoicedate", label: "Supplier Invoice Date"}, {value: "voucherdate", label: "Posting Date"}], label:"label", valuename: "value", required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Based On'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">As On Date</label>
										<Field name={'asondate'} props={{required: false}} component={DateEle} validate={[dateNewValidation({required: false, model: 'As On date'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Group By</label>
										<Field name={'groupby'} props={{options: [{value: "none", label: "None"}, {value: "supplier", label: "Supplier"}], label:"label", valuename: "value", required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Group By'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12" >
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.totalinvoiceselected ? <div className="col-md-12 col-sm-12 paddingleft-30"><span>{this.props.resource.totalinvoiceselected}</span></div> : null}
							{this.props.resource ? <Reactuigrid excelname='Accounts Payable Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} btnOnClick={this.btnOnClick} /> : null }
						</div>
					</div>
				</form>
			</div>
		);
	};
}

AccountPayableReportForm = connect(
	(state, props) => {
		let formName = 'accountpayablereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(AccountPayableReportForm));

export default AccountPayableReportForm;
