import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class RevenueRecongnitionReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			columns : [{
					name : "Project",
					key : "projectname",
					format: "anchortag",
					transactionname : "projects",
					transactionid : "id",
					cellClass : "text-center",
					width : 200
				},{
					name : "Project No",
					key : "projectno",
					format: "anchortag",
					transactionname : "projects",
					transactionid : "id",
					cellClass : "text-center",
					width : 200
				},{
					name : 'Original Project Value',
					key : 'originalprojectbasictotal',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 200
				}, {
					name : 'Original Estimated Cost',
					key : 'originalestimationcost',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 200
				}, {
					name : 'Original Estimated Margin',
					key : 'originalestimationmargin',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 200
				},{
					name : 'Original Estimated Margin %',
					key : 'originalestimationmarginpercent',
					cellClass : "text-center",
					width : 200
				},{
					name : 'Revised Project Value',
					key : 'projectbasictotal',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 200
				}, {
					name : 'Revised Estimated Cost',
					key : 'estimationcost',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 200
				}, {
					name : 'Revised Estimated Margin',
					key : 'estimationmargin',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 200
				},{
					name : 'Revised Estimated Margin %',
					key : 'estimationmarginpercent',
					cellClass : "text-center",
					width : 200
				},{
					name : 'Cost Incurred',
					key : 'totalconsumption',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 180
				},{
					name : 'Percent Completed',
					key : 'percentcompleted',
					cellClass : "text-center",
					width : 180
				}, {
					name : 'So far Billed',
					key : 'invoicebasictotal',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 180
				},{
					name : 'Revenue to be Recognised',
					key : 'revenuetoberecongnised',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 200
				}, {
					name : 'Revenue in Excess of Billing',
					key : 'excessbilling',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 230
				},{
					name : 'Billing in Excess of Revenue',
					key : 'shortfallbilling',
					format : "defaultcurrency",
					footertype : "sum",
					cellClass : "text-right",
					width : 230
				}
			],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.showOnChange = this.showOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				asondate : null,
				filters : {},
				hiddencols : [],
				columns : [],
				show : 'Summary'
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];

		['fromdate', 'todate','asondate'].forEach((item) => {
			if(this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/revenuerecognitionreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns:  this.state.columns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			asondate : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	showOnChange() {
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('columns');

		this.props.updateFormState(this.props.form, {
			filters: {}
		});
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}
	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row" style={{marginTop: `${(document.getElementsByClassName('navbar').length > 0 ? document.getElementsByClassName('navbar')[0].clientHeight : '0')}px`}}>
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Revenue Recognition Report (AS7)</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">AS On Date</label>
										<Field name={'asondate'} props={{required: false}} component={DateEle} validate={[dateNewValidation({required: false, model: 'As On date'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Revenue Recognition Report (AS7)' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} openTransaction={this.openTransaction} updateReportFilter={this.props.updateReportFilter} /> : null }
						</div>
					</div>
				</form>
			</div>
		);
	};
}

RevenueRecongnitionReportForm = connect(
	(state, props) => {
		let formName = 'revenuerecongnitionreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(RevenueRecongnitionReportForm));

export default RevenueRecongnitionReportForm;
