import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, search, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

import SettlementDetailsModal from '../components/details/settlementdetailsmodal';

class PayrollReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true,
			payroll_columns: [{
					"name": "Employee Name",
					"key": "displayname",
					"locked": true,
					"width": 200
				}, {
					"name": "Employee Code",
					"key": "employeecode",
					"cellClass" : "text-center",
					"locked": true,
					"width": 120
				}, {
					"name" : "Payroll No",
					"key" : "payrollno",
					"format" : "anchortag",
					"transactionname" : "payrolls",
					"transactionid" : "id",
					"locked": true,
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name": "Payroll Date",
					"key": "payrolldate",
					"format": "date",
					"cellClass": "text-center",
					"width": 150
				}, {
					"name": "Period Start",
					"key": "periodstart",
					"format": "date",
					"cellClass": "text-center",
					"width": 150
				}, {
					"name": "Period End",
					"key": "periodend",
					"format": "date",
					"cellClass": "text-center",
					"width": 150
				}, {
					"name": "Total Pay Days",
					"key": "paydaystotal",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Computed Pay Days",
					"key": "defaultpaydaypaid",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Leave Days Without Pay",
					"key": "lopdays",
					"cellClass": "text-right",
					"width": 150
				}, {
					"name": "Payable Pay Days",
					"key": "paydaypaid",
					"cellClass": "text-right",
					"width": 150
				}
			]
		}

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				filters: {},
				fromdate: new Date(new Date().getFullYear(),new Date().getMonth()-1,1),
				todate: new Date(new Date().getFullYear(),new Date().getMonth(),0),
				status: ["Approved"],
				hiddencols: [],
				columns: []
			};
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData() {
		this.updateLoaderFlag(true);

		let { payroll_columns } = this.state;

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];

		['fromdate', 'todate', 'employeeid', 'locationid', 'departmentid', 'status'].forEach((item) => {
			if(this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/getpayrollreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {

				if (payroll_columns.length > 10)
					payroll_columns.splice(10, payroll_columns.length-1);

				if (response.data.componentEarningArray.length > 0)
					response.data.componentEarningArray.forEach((item) => {
						payroll_columns.push({
							"name": response.data.componentObject[item],
							"key": item,
							"cellClass": "text-right",
							"width": 150
						})
					});

				if (response.data.componentDeductionArray.length > 0)
					response.data.componentDeductionArray.forEach((item) => {
						payroll_columns.push({
							"name": response.data.componentObject[item],
							"key": item,
							"cellClass": "text-right",
							"width": 150
						})
					});

				if (response.data.componentEmployerContributionArray.length > 0)
					response.data.componentEmployerContributionArray.forEach((item) => {
						payroll_columns.push({
							"name": response.data.componentObject[item],
							"key": item,
							"cellClass": "text-right",
							"width": 150
						})
					});

				if (response.data.componentRecoveryArray.length > 0)
					response.data.componentRecoveryArray.forEach((item) => {
						payroll_columns.push({
							"name": response.data.componentObject[item],
							"key": item,
							"cellClass": "text-right",
							"width": 150
						})
					});

				payroll_columns.push({
					"name": "Total Earning",
					"key": "totalearnings",
					"cellClass": "text-right",
					"footertype" : "sum",
					"width": 180
				}, {
					"name": "Total Deduction",
					"key": "totaldeductions",
					"cellClass": "text-right",
					"footertype" : "sum",
					"width": 180
				}, {
					"name": "Total Employer Contributions",
					"key": "totalemployercontributions",
					"cellClass": "text-right",
					"footertype" : "sum",
					"width": 180
				}, {
					"name": "Net Pay",
					"key": "netpay",
					"cellClass": "text-right",
					"footertype" : "sum",
					"width": 200
				}, {
					"name" : "Payroll Batch No",
					"key" : "payrollbatchno",
					"format" : "anchortag",
					"transactionname" : "payrollbatch",
					"transactionid" : "payrollbatchid",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name": "Payroll Batch Date",
					"key": "payrollbatchdate",
					"format": "date",
					"cellClass": "text-center",
					"width": 150
				}, {
					"name": "Payroll Batch Status",
					"key": "status",
					"cellClass": "text-center",
					"width": 200
				}, {
					"name": "Payment Status",
					"key": "payment_status",
					"cellClass": "text-center",
					"width": 200
				}, {
					"name": "Out Standing Amount",
					"key": "outstandingamount",
					"cellClass": "text-right",
					"width": 200,
					"footertype" : "sum"
				}, {
					"name" : "Voucher Type",
					"key" : "journaltype",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Voucher No",
					"key" : "journalno",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Voucher Date",
					"key" : "journaldate",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name": "Voucher Status",
					"key": "journalstatus",
					"cellClass": "text-center",
					"width": 150
				}, {
					"name": "Cost Center",
					"key": "costcenterid_name",
					"cellClass": "text-center",
					"width": 200,
					"if": this.props.app.feature.enableCostCenter
				}, {
					"name": "Pay Structure",
					"key": "paystructureid_name",
					"cellClass": "text-center",
					"width": 200
				}, {
					"name": "Payroll Status",
					"key": "payroll_status",
					"cellClass": "text-center",
					"width": 150
				}, {
					"name": "Location",
					"key": "locationid_name",
					"cellClass": "text-center",
					"width": 180
				}, {
					"name": "Department",
					"key": "departmentid_name",
					"cellClass": "text-center",
					"width": 180
				}, {
					"name": "Bank Name",
					"key": "bankid_name",
					"cellClass": "text-center",
					"width": 180
				}, {
					"name": "Account No",
					"key": "bankid_accountno",
					"cellClass": "text-center",
					"width": 180
				}, {
					"name": "Bank Branch",
					"key": "bankid_branchname",
					"cellClass": "text-center",
					"width": 180
				}, {
					"name": "IFSC Code",
					"key": "bankid_ifsccode",
					"cellClass": "text-center",
					"width": 150
				});


				this.setState({
					payroll_columns
				}, () => {
					this.props.updateFormState(this.props.form, {
						payrollArray: response.data.main,
						componentEarningArray: response.data.componentEarningArray,
						componentDeductionArray: response.data.componentDeductionArray,
						componentEmployerContributionArray: response.data.componentEmployerContributionArray,
						componentRecoveryArray: response.data.componentRecoveryArray,
						originalRows: response.data.main,
						columns: this.state.payroll_columns
					});
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);

				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}
	resetFilter () {
		this.props.updateFormState(this.props.form, {
			fromdate : null,
			todate : null,
			status : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		});
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Payroll Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field
											name={'fromdate'}
											props={{
												required: true
											}}
											format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}}
											parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}}
											component={DateEle}
											validate={[dateNewValidation({required:  true, title : 'From Date'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field
											name={'todate'}
											props={{
												required: true,
												min: this.props.resource.fromdate
											}}
											format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}}
											parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}}
											component={DateEle}
											validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Employee</label>
										<Field
											name={'employeeid'}
											props={{
												resource: "employees",
												fields: "id,displayname",
												label: "displayname"
											}}
											component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Location</label>
										<Field
											name={'locationid'}
											props={{
												resource: "locations",
												fields: "id,name"
											}}
											component={autoSelectEle}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Department</label>
										<Field
											name={'departmentid'}
											props={{
												resource: "departments",
												fields: "id,name"
											}}
											component={autoSelectEle}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Status</label>
										<Field
											name={'status'}
											props={{
												options: ["Draft", "Submitted", "Revise", "Approved", "Cancelled"],
												multiselect: true
											}}
											component={localSelectEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Payroll Details' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

PayrollReportForm = connect(
	(state, props) => {
		let formName = 'payrollreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(PayrollReportForm));

export default PayrollReportForm;
