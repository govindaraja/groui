import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { AutoSelect, SelectAsync, DateElement } from '../components/utilcomponents';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter } from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';

class ReceiptNotePlannerReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			receiptdate: new Date(new Date().setHours(0, 0, 0, 0)),
			search: {},
			limit: {
				purchaseorder : 10,
				salesreturn : 10
			},
			purchaseorderArray: [],
			salesreturnArray: [],
			isPurchaseOrderActive: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.tabOnChange = this.tabOnChange.bind(this);
		this.addLimit = this.addLimit.bind(this);
		this.parentCheckboxOnChange = this.parentCheckboxOnChange.bind(this);
		this.itemCheckboxOnChange = this.itemCheckboxOnChange.bind(this);
		this.filterOnchange = this.filterOnchange.bind(this);
		this.createReceiptnote = this.createReceiptnote.bind(this);
		this.searchFilter = this.searchFilter.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag = (loaderflag) => {
		this.setState({loaderflag});
	}

	onLoad = () => {
		this.updateLoaderFlag(true);
		let purchaseorderArray = [], salesreturnArray = [];

		axios.get(`/api/query/receiptplannerquery`).then((response) => {
			if (response.data.message == 'success') {
				response.data.main.poitems.sort((a, b) => (
					a.displayorder - b.displayorder
				));
				response.data.main.salesreturnitems.sort((a, b) => (
					a.displayorder - b.displayorder
				));
				let poitems = response.data.main.poitems,
				salesreturnitems = response.data.main.salesreturnitems;

				for (var i = 0; i < poitems.length; i++) {
					let itemFound = false;
					for (var j = 0; j < purchaseorderArray.length; j++) {
						if (purchaseorderArray[j].id) {
							if (purchaseorderArray[j].id == poitems[i].poid) {
								if (new Date(purchaseorderArray[j].earlierdate) > new Date(poitems[i].earlierdate))
									purchaseorderArray[j].earlierdate = poitems[i].earlierdate;

								poitems[i].number = poitems[i].quantity;
								poitems[i].numberreceived = poitems[i].deliveredqty;
								poitems[i].receiptnoteqty = poitems[i].quantity - poitems[i].deliveredqty;
								poitems[i].receiptdate = poitems[i].earlierdate;
								poitems[i].deliverydate = poitems[i].earlierdate;
								purchaseorderArray[j].deliverydate = poitems[i].earlierdate;
								purchaseorderArray[j].purchaseorderitems.push(poitems[i]);
								itemFound = true;
								break;
							}
						}
					}

					if (!itemFound) {
						let tempObject = {
							ponumber : poitems[i].ponumber,
							partnerid : poitems[i].partnerid,
							partnerid_name : poitems[i].partnerid_name,
							companyid : poitems[i].companyid,
							companyid_name : poitems[i].companyid_name,
							currencyid : poitems[i].currencyid,
							currencyid_name : poitems[i].currencyid_name,
							id : poitems[i].poid,
							projectid : poitems[i].projectid,
							projectid_projectno : poitems[i].projectid_projectno,
							projectid_projectname : poitems[i].projectid_projectname,
							contactid : poitems[i].contactid,
							phone : poitems[i].phone,
							mobile : poitems[i].mobile,
							email : poitems[i].email,
							suppliercontactid : poitems[i].suppliercontactid,
							supplierphone : poitems[i].supplierphone,
							suppliermobile : poitems[i].suppliermobile,
							supplieremail : poitems[i].supplieremail,
							deliveryaddress : poitems[i].deliveryaddress,
							supplieraddress : poitems[i].supplieraddress,
							supplierreference : poitems[i].supplierreference,
							receiptdate : poitems[i].deliverydate,
							earlierdate : poitems[i].earlierdate,
							param : 'Purchase Orders',
							receiptfor : 'Purchase Orders',
							purchaseorderitems : []
						};

						poitems[i].number = poitems[i].quantity;
						poitems[i].numberreceived = poitems[i].deliveredqty;
						poitems[i].receiptnoteqty = poitems[i].quantity - poitems[i].deliveredqty;
						poitems[i].receiptdate = poitems[i].earlierdate;
						poitems[i].deliverydate = poitems[i].earlierdate;

						tempObject.purchaseorderitems.push(poitems[i]);
						purchaseorderArray.push(tempObject);
					}
				}

				for (var i = 0; i < salesreturnitems.length; i++) {
					let itemFound = false;
					for (var j = 0; j < salesreturnArray.length; j++) {
						if (salesreturnArray[j].id) {
							if (salesreturnArray[j].id == salesreturnitems[i].salesreturnid) {
								if (new Date(salesreturnArray[j].earlierdate) > new Date(salesreturnitems[i].returndate))
									salesreturnArray[j].earlierdate = salesreturnitems[i].returndate;
								
								salesreturnitems[i].number = salesreturnitems[i].qtyauthorized;
								salesreturnitems[i].numberreceived = salesreturnitems[i].qtyreturned;
								salesreturnitems[i].receiptnoteqty = salesreturnitems[i].qtyauthorized - salesreturnitems[i].qtyreturned;
								salesreturnitems[i].deliveredqty = salesreturnitems[i].qtyreturned;
								salesreturnitems[i].quantity = salesreturnitems[i].qtyauthorized;
								salesreturnitems[i].receiptdate = salesreturnitems[i].returndate;

								salesreturnArray[j].salesreturnitems.push(salesreturnitems[i]);
								itemFound = true;
								break;
							}
						}
					}

					if (!itemFound) {
						let tempObject = {
							salesreturnno : salesreturnitems[i].salesreturnno,
							againstorder: salesreturnitems[i].againstorder,
							currencyid: salesreturnitems[i].currencyid,
							currencyexchangerate: salesreturnitems[i].currencyexchangerate,
							pricelistid: salesreturnitems[i].pricelistid,
							defaultcostcenter: salesreturnitems[i].defaultcostcenter,
							customerid : salesreturnitems[i].customerid,
							partnerid : salesreturnitems[i].customerid,
							customerid_name : salesreturnitems[i].customerid_name,
							orderid_currencyid : salesreturnitems[i].orderid_currencyid,
							companyid : salesreturnitems[i].companyid,
							companyid_name : salesreturnitems[i].companyid_name,
							id : salesreturnitems[i].salesreturnid,
							earlierdate : salesreturnitems[i].returndate,
							returndate : salesreturnitems[i].returndate,
							receiptdate : salesreturnitems[i].returndate,
							param : 'Sales Returns',
							salesreturnitems : []
						};

						salesreturnitems[i].number = salesreturnitems[i].qtyauthorized;
						salesreturnitems[i].numberreceived = salesreturnitems[i].qtyreturned;
						salesreturnitems[i].receiptnoteqty = salesreturnitems[i].qtyauthorized - salesreturnitems[i].qtyreturned;
						salesreturnitems[i].deliveredqty = salesreturnitems[i].qtyreturned;
						salesreturnitems[i].quantity = salesreturnitems[i].qtyauthorized;
						salesreturnitems[i].receiptdate = salesreturnitems[i].returndate;

						tempObject.salesreturnitems.push(salesreturnitems[i]);
						salesreturnArray.push(tempObject);
					}
				}

				purchaseorderArray.sort((a, b) => {
					return new Date(a.earlierdate) - new Date(b.earlierdate);
				});

				this.setState({ purchaseorderArray, salesreturnArray });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter = () => {
		let { search } = this.state;
		search = {};
		this.setState({ search });
	}

	tabOnChange = (param) => {
		let isPurchaseOrderActive = false, isSalesReturnActive = false;
		if(param == 'purchaseorder')
			isPurchaseOrderActive = true;
		else if(param == 'salesreturn')
			isSalesReturnActive = true;
		
		this.setState({ isPurchaseOrderActive, isSalesReturnActive });
	};

	addLimit = (param) => {
		this.state.limit[param] += 10;
		this.setState({limit : this.state.limit});
	};

	parentCheckboxOnChange = (checked, param, arrayname, childname) => {
		this.state[arrayname].forEach((data) => {
			if(data.id == param.id) {
				data.check = checked;
				data[childname].forEach((item) => {
					item.check = checked;
				});
			}
		});
		this.setState({ [arrayname] : this.state[arrayname] });
	};

	itemCheckboxOnChange = (checked, param, childname) => {
		param.check = checked;
		this.setState({ [childname] : this.state[childname] });
	};

	filterOnchange = (value, field) => {
		if(field == 'receiptdate' )
			this.state.receiptdate = value;
		else
			this.state.search[field] = value;

		[{name: "purchaseorderArray", childname: 'purchaseorderitems'}, {name: "salesreturnArray", childname: "salesreturnitems"}].forEach((itemObj) => {
			this.state[itemObj.name].forEach((data) => {
				data.check = false;
				data[itemObj.childname].forEach((item) => {
					item.check = false;
				});
			});
		});

		this.setState({
			receiptdate: this.state.receiptdate,
			search: this.state.search,
			purchaseorderArray: this.state.purchaseorderArray,
			salesreturnArray: this.state.salesreturnArray
		});
	};

	searchFilter = (array) => {
		let tempArr = [];
		array.map((a) => {
			let filtered = true;
			for(var prop in this.state.search) {
				if(this.state.search[prop]) {
					if(a[prop]) {
						if(this.state.search[prop] != a[prop]) {
							filtered = false;
						}
					} else {
						filtered = false;
					}
				}
			}
			if(a.receiptdate && this.state.receiptdate) {
				if(new Date(this.state.receiptdate).getTime() < new Date(a.receiptdate).getTime())
					filtered = false;
			}
			if(filtered)
				tempArr.push(a);
		});
		return tempArr;
	};

	createReceiptnote = () => {
		let { purchaseorderArray, salesreturnArray } = this.state;
		if (this.state.isPurchaseOrderActive) {
			let errorFound = false,
			tempObj,
			tempArray = [];
			for (var i = 0; i < purchaseorderArray.length; i++) {
				for (var j = 0; j < purchaseorderArray[i].purchaseorderitems.length; j++) {
					if (purchaseorderArray[i].purchaseorderitems[j].check) {
						if (tempObj) {
							var item = purchaseorderArray[i].purchaseorderitems[j];
							if ((item.partnerid != tempObj.partnerid) || (item.currencyid != tempObj.currencyid)) {
								errorFound = true;
								break;
							}
						} else
							tempObj = purchaseorderArray[i];

						tempArray.push(purchaseorderArray[i].purchaseorderitems[j]);
					}
				}

				if (errorFound)
					break;
			}

			if (errorFound)
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please select purchase order item for same supplier,currency',
					btnArray : ['Ok']
				}));
			else {
				if (tempObj) {
					tempObj.purchaseorderitems = tempArray;
					this.props.history.push({pathname: '/createReceiptNote', params: {...tempObj}});
				} else
					this.props.openModal(modalService['infoMethod']({
						header : 'Error',
						body : 'Please select purchase order item for same customer, company and currency',
						btnArray : ['Ok']
					}));
			}
		} else if (this.state.isSalesReturnActive) {
			let errorFound = false,
			tempObj,
			tempArray = [];
			for (var i = 0; i < salesreturnArray.length; i++) {
				for (var j = 0; j < salesreturnArray[i].salesreturnitems.length; j++) {
					if (salesreturnArray[i].salesreturnitems[j].check) {
						if (tempObj) {
							let item = salesreturnArray[i].salesreturnitems[j];
							if ((tempObj.id != item.salesreturnid) || (tempObj.customerid != item.customerid) || tempObj.currencyid != item.currencyid) {
								errorFound = true;
								break;
							}
						} else
							tempObj = salesreturnArray[i];

						tempArray.push(salesreturnArray[i].salesreturnitems[j]);
					}
				}

				if (errorFound)
					break;
			}

			if (errorFound)
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please select one sales return at a time',
					btnArray : ['Ok']
				}));
			else {
				if (tempObj) {
					tempObj.salesreturnitems = tempArray;
					this.props.history.push({pathname: '/createReceiptNote', params: {...tempObj}});
				} else
					this.props.openModal(modalService['infoMethod']({
						header : 'Error',
						body : 'Please choose atleast one item to create receipt note',
						btnArray : ['Ok']
					}));
			}
		}
	}

	render() {
		let filteredPurchaseOrderArray = this.searchFilter(this.state.purchaseorderArray);
		let filteredSalesReturnArray = this.searchFilter(this.state.salesreturnArray);

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="form-group col-md-12 bg-white report-header">
						<div className="report-header-title">Receipt Note Planner</div>
						<div className="report-header-rightpanel">
							<button type="button" onClick={this.createReceiptnote} className="btn btn-sm gs-btn-success"><i className="fa fa-plus"></i>Receipt Note</button>
						</div>
					</div>
					<div className="form-group col-md-12 bg-white paddingright-5">
						<div className="row">
							<div className="form-group col-md-3 col-sm-12">
								<label className="labelclass">Partner</label>
								<AutoSelect resource={'partners'} fields={'id,name,displayname'} value={this.state.search.partnerid} label={'displayname'} valuename={'id'} onChange={(value, valueobj) => this.filterOnchange(value, 'partnerid')} />
							</div>
							<div className="form-group col-md-3 col-sm-12">
								<label className="labelclass">Receipt Date</label>
								<DateElement className="form-control" value={this.state.receiptdate} onChange={(val) => this.filterOnchange(val, 'receiptdate')} />
							</div>
							<div className="form-group col-md-3 col-sm-12" >
								<button type="button" onClick={() => {
									this.resetFilter()	
								}} className="btn btn-width btn-sm gs-btn-info margintop-25"><i className="fa fa-refresh"></i>Reset</button>
							</div>
						</div>
					</div>

					<div className="form-group col-md-12 bg-white paddingright-5">
						<div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								<ul className="nav nav-tabs">
									<li className={`nav-item nav-link ${this.state.isPurchaseOrderActive ? 'active' : ''}`} onClick={() => this.tabOnChange('purchaseorder')}>
										<i className="fa fa-file-text-o"></i>
										<span className="marginleft-10">Purchase Orders</span>
										<span className="badge badge-secondary marginleft-10">{filteredPurchaseOrderArray.length}</span>
									</li>
									<li className={`nav-item nav-link ${this.state.isSalesReturnActive ? 'active' : ''}`} onClick={() => this.tabOnChange('salesreturn')}>
										<i className="fa fa-file-text-o"></i>
										<span className="marginleft-10">Sales Returns</span>
										<span className="badge badge-secondary marginleft-10">{filteredSalesReturnArray.length}</span>
									</li>
								</ul>

								<div className="tab-content">
									<div className={`tab-pane fade ${this.state.isPurchaseOrderActive ? 'show active' : ''}`}>
										<div className="row margintop-25">
											{filteredPurchaseOrderArray.map((purchaseorderdata, purchaseorderdataindex) => {
												if(this.state.limit.purchaseorder > purchaseorderdataindex) {
													return(
														<div className="form-group col-md-12 col-sm-12 col-xs-12" key={purchaseorderdataindex}>
															<div className="card border-info">
																<div className="card-header">
																	<div className="float-left">
																		<span className="marginleft-11">
																			<input type="checkbox" checked={purchaseorderdata.check || false} onChange={(evt) => this.parentCheckboxOnChange(evt.target.checked, purchaseorderdata, 'purchaseorderArray', 'purchaseorderitems')} />
																		</span>
																		<a href={`#/details/purchaseorders/${purchaseorderdata.id}`} className="marginleft-25">{purchaseorderdata.ponumber}</a>
																	</div>
																	<div className="float-left">
																		<span> | Supplier :   </span>
																		<span>{purchaseorderdata.partnerid_name}</span>
																		<span> | Currency : </span>
																		<span>{this.props.app.currency[purchaseorderdata.currencyid].name}</span>
																	</div>
																</div>
																<div className="card-body padding-0">
																	<div className="table-responsive">
																		<table className="table table-sm marginbottom-0">
																			<thead>
																				<tr>
																					<th></th>
																					<th className="text-center">Item</th>
																					<th className="text-center">Qty Ordered</th>
																					<th className="text-center">Qty Received</th>
																					<th className="text-center">purchaseorder Date</th>
																					<th className="text-center">Receipt Date</th>
																					<th className="text-center">Qty to Receive</th>
																				</tr>
																			</thead>
																			{purchaseorderdata.purchaseorderitems ? <tbody>
																				{purchaseorderdata.purchaseorderitems.map((purchaseorderitem, purchaseorderitemindex) => {
																					return (
																						<tr key={purchaseorderitemindex}>
																							<td className='text-center' style={{width: '5%'}}><input type="checkbox" checked={purchaseorderitem.check || false} onChange={(evt) => this.itemCheckboxOnChange(evt.target.checked, purchaseorderitem, 'purchaseorderitems')}/></td>
																							<td className="text-center">{purchaseorderitem.itemid_name}</td>
																							<td className="text-center">{purchaseorderitem.number}</td>
																							<td className="text-center">{purchaseorderitem.numberreceived || 0}</td>
																							<td className="text-center">{dateFilter(purchaseorderitem.created)}</td>
																							<td className="text-center">{dateFilter(purchaseorderitem.receiptdate)}</td>
																							<td className="text-center width-20">{purchaseorderitem.receiptnoteqty}</td>
																						</tr>
																					)
																				})}
																			</tbody> : null}
																		</table>
																	</div>
																</div>
															</div>
														</div>
													)
												}
											})}

											{filteredPurchaseOrderArray.length > this.state.limit.purchaseorder ?<div className="col-md-12 col-sm-12 text-center">
												<button type="button" className="btn btn-sm gs-btn-warning" onClick={() => this.addLimit("purchaseorder")}>Show More</button>
											</div> : null }
										</div>
									</div>

									<div className={`tab-pane fade ${this.state.isSalesReturnActive ? 'show active' : ''}`}>
										<div className="row margintop-25">
											{filteredSalesReturnArray.map((salesreturndata, salesreturndataindex) => {
												if(this.state.limit.salesreturn > salesreturndataindex) {
													return(
														<div className="form-group col-md-12 col-sm-12 col-xs-12" key={salesreturndataindex}>
															<div className="card border-info">
																<div className="card-header">
																	<div className="float-left">
																		<span className="marginleft-11">
																			<input type="checkbox" checked={salesreturndata.check || false} onChange={(evt) => this.parentCheckboxOnChange(evt.target.checked, salesreturndata, 'salesreturnArray', 'salesreturnitems')} />
																		</span>
																		<a href={`#/details/salesreturns/${salesreturndata.id}`} className="marginleft-25">{salesreturndata.salesreturnno}</a>
																	</div>
																	<div className="float-left">
																		<span> | Customer :   </span>
																		<span>{salesreturndata.customerid_name}</span>
																	</div>
																</div>
																<div className="card-body padding-0">
																	<div className="table-responsive">
																		<table className="table table-sm marginbottom-0">
																			<thead>
																				<tr>
																					<th></th>
																					<th className="text-center">Item</th>
																					<th className="text-center">Qty Ordered</th>
																					<th className="text-center">Qty Received</th>
																					<th className="text-center">purchaseorder Date</th>
																					<th className="text-center">Receipt Date</th>
																					<th className="text-center">Qty to Receive</th>
																				</tr>
																			</thead>
																			{salesreturndata.salesreturnitems ? <tbody>
																				{salesreturndata.salesreturnitems.map((salesreturnitem, salesreturnitemindex) => {
																					return (
																						<tr key={salesreturnitemindex}>
																							<td className='text-center' style={{width: '5%'}}><input type="checkbox" checked={salesreturnitem.check || false} onChange={(evt) => this.itemCheckboxOnChange(evt.target.checked, salesreturnitem, 'salesreturnitems')}/></td>
																							<td className="text-center">{salesreturnitem.itemid_name}</td>
																							<td className="text-center">{salesreturnitem.number}</td>
																							<td className="text-center">{salesreturnitem.numberreceived || 0}</td>
																							<td className="text-center">{dateFilter(salesreturnitem.created)}</td>
																							<td className="text-center">{dateFilter(salesreturnitem.receiptdate)}</td>
																							<td className="text-center width-20">{salesreturnitem.receiptnoteqty}</td>
																						</tr>
																					)
																				})}
																			</tbody> : null}
																		</table>
																	</div>
																</div>
															</div>
														</div>
													)
												}
											})}

											{filteredSalesReturnArray.length > this.state.limit.salesreturn ? <div className="col-md-12 col-sm-12 text-center">
												<button type="button" className="btn btn-sm gs-btn-warning" onClick={() => this.addLimit("salesreturn")}>Show More</button>
											</div> : null }
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</>
		);
	};
}



ReceiptNotePlannerReportForm = connect((state) =>{
	return {app: state.app}
}) (ReceiptNotePlannerReportForm);

export default ReceiptNotePlannerReportForm;
