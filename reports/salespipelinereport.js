import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import { Chart } from 'react-google-charts';

import { updateFormState, updateReportFilter, updateAppState } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, checkboxEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class SalesPipelineReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.rangeOnChange = this.rangeOnChange.bind(this);
		this.typeOnChange = this.typeOnChange.bind(this);
		this.seriesSelected = this.seriesSelected.bind(this);
		this.chartEvents = [{
			eventName: 'select',
			callback: (Chart) => {
				let selectedArr = Chart.chart.getSelection();
				this.updateLoaderFlag(true);
				this.seriesSelected(selectedArr);
			}
		}];
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				reporttype: 'By Stage',
				range: 'Monthly',
				priority: 'All',
				type: 'By Value',
				potentialrevenue: 'With Probability',
				stageArray: [],
				chartObj: {
					data: [],
					rows: [],
					cols: [{
						type: 'string',
						label: 'Stages',
					},{
						type: 'number',
						label: 'Cold',
					},{
						type: 'number',
						label: 'Hot',
					},{
						type: 'number',
						label: 'Warm',
					},{
						type: 'number',
						label: 'No Priority',
					}],
					options: {
						isStacked : true,
						fill : 20,
						bar : {
							"groupWidth" : '65%'
						},
						legend : {
							"position" : 'top',
							"maxLines" : 3
						},
						displayExactValues : true,
						vAxis : {
							title : "Amount / Count",
							gridlines : {
								"count" : 7
							},
							titleTextStyle : {
								"color" : '#6C9696',
								"bold" : true
							}
						},
						hAxis : {
							title : 'Periods / Stages',
							titleTextStyle : {
								"color" : '#6C9696',
								"bold" : true
							}
						},
						scrollbar : {
							"enabled" : true
						},
						formatters : {}
					}
				},
				filters: {}
			};

		this.props.initialize(tempObj);
		this.setState({filterToggleOpen: true});

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('originalRows');
		let { chartObj } = this.props.resource;
		let sortingArray = []

		let filterString = [];
		['type', 'reporttype', 'priority', 'range', 'salesperson', 'territory', 'fromdate', 'todate', 'potentialrevenue'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/salespipelinereportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let resultObj = {}, stageArray = [];
				chartObj.rows = [];

				if(this.props.resource.reporttype == 'By Stage')
					response.data.main.sort((a, b) => {
						return a.index - b.index;
					});

				response.data.main.forEach((item) => {
					if (!resultObj[`${item.id}_${item.id}`]) {
						resultObj[`${item.id}_${item.id}`] = {
							id : item.id,
							name : this.props.resource.reporttype == 'By Stage' ? item.name : item.id,
							startdate : this.props.resource.reporttype == 'By Stage' ? item.name : item.startdate,
							Cold : {
								count : 0,
								value : 0,
								weightedvalue : 0
							},
							Hot : {
								count : 0,
								value : 0,
								weightedvalue : 0
							},
							Warm : {
								count : 0,
								value : 0,
								weightedvalue : 0
							},
							'No Priority' : {
								count : 0,
								value : 0,
								weightedvalue : 0
							}
						}
					}
					resultObj[`${item.id}_${item.id}`][item.priority].count = Number(item.totalcount);
					resultObj[`${item.id}_${item.id}`][item.priority].value = item.totalpotentialrevenue;
					resultObj[`${item.id}_${item.id}`][item.priority].weightedvalue = item.weightedpotentialrevenue;
				});

				for (var prop in resultObj) {
					chartObj.rows.push([
						resultObj[prop].name, 

						this.props.resource.type == 'By Value' ? ((this.props.resource.potentialrevenue == 'With Probability') ? resultObj[prop]['Cold'].weightedvalue : resultObj[prop]['Cold'].value) : resultObj[prop]['Cold'].count, 

						this.props.resource.type == 'By Value' ? ((this.props.resource.potentialrevenue == 'With Probability') ? resultObj[prop]['Hot'].weightedvalue : resultObj[prop]['Hot'].value) : resultObj[prop]['Hot'].count, 

						this.props.resource.type == 'By Value' ? ((this.props.resource.potentialrevenue == 'With Probability') ? resultObj[prop]['Warm'].weightedvalue : resultObj[prop]['Warm'].value) : resultObj[prop]['Warm'].count, 

						this.props.resource.type == 'By Value' ? ((this.props.resource.potentialrevenue == 'With Probability') ? resultObj[prop]['No Priority'].weightedvalue : resultObj[prop]['No Priority'].value) : resultObj[prop]['No Priority'].count

					]);

					stageArray.push([this.props.resource.reporttype == 'By Stage' ? resultObj[prop].id : resultObj[prop].startdate]);
				}

				this.props.updateFormState(this.props.form, {chartObj, stageArray});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(response.data.main.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	rangeOnChange() {
		let tempObj = {};
		if (this.props.resource.reporttype != 'By Timeline') {
			tempObj.range = '';
			tempObj.fromdate = null;
			tempObj.todate = null;
		} else {
			tempObj.potentialrevenue = null;
		}
		this.props.updateFormState(this.props.form, tempObj);
	}

	typeOnChange() {
		if(this.props.resource.type == "By Number") {
			this.props.updateFormState(this.props.form, { potentialrevenue : null });
		}
	}

	seriesSelected(array) {
		let selectedItem = array[0];

		let filterArray = [{
			"fieldname" : "status",
			"operator" : "In",
			"value" : ["Open", "Quotation"]
		}];

		if(this.props.resource.salesperson > 0) {
			filterArray.push({
				"fieldname" : "salesperson",
				"operator" : "Equal",
				"value" : this.props.resource.salesperson
			});
		}

		if(this.props.app.user.roleid.indexOf(2) > -1) {
			filterArray.push({
				"fieldname" : "salesperson",
				"operator" : "Equal",
				"value" : this.props.app.user.id
			});
		}

		if(this.props.resource.territory > 0) {
			filterArray.push({
				"fieldname" : "territoryid",
				"operator" : "Equal",
				"value" : this.props.resource.territory
			});
		}

		if(this.props.resource.reporttype == 'By Stage') {
			filterArray.push({
				"fieldname" : "stageid",
				"operator" : "Equal",
				"value" : this.props.resource.stageArray[selectedItem.row][0] == 0 ? null : this.props.resource.stageArray[selectedItem.row][0]
			});
			filterArray.push({
				"fieldname" : "priority",
				"operator" : "Equal",
				"value" : this.props.resource.chartObj.cols[selectedItem.column].label == 'No Priority' ? null : this.props.resource.chartObj.cols[selectedItem.column].label
			});
		} else {
			let startDate = new Date(this.props.resource.stageArray[selectedItem.row][0]);
			let endDate = new Date(startDate.getFullYear(),startDate.getMonth() +1, 0);
			if(this.props.resource.range == 'Quarterly') {
				endDate = new Date(startDate.getFullYear(),startDate.getMonth()+3, 0);
			}

			filterArray.push({
				"fieldname" : "duedate",
				"operator" : "GreaterThanOrEqual",
				"value" : this.props.resource.fromdate ? (this.props.resource.fromdate > startDate ? this.props.resource.fromdate : startDate) : startDate
			});
			filterArray.push({
				"fieldname" : "duedate",
				"operator" : "LessThanOrEqual",
				"value" : this.props.resource.todate ? (this.props.resource.todate > endDate ? endDate : this.props.resource.todate) : endDate
			});
			filterArray.push({
				"fieldname" : "priority",
				"operator" : "Equal",
				"value" : this.props.resource.chartObj.cols[selectedItem.column].label == 'No Priority' ? null : this.props.resource.chartObj.cols[selectedItem.column].label
			});
		}

		let tempobj = {
			...this.props.app.listFilterObj,
			leads: filterArray
		};
		this.props.updateAppState('listFilterObj', tempobj);
		this.props.history.push('/list/leads');
		this.updateLoaderFlag(false);
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Sales Pipeline Report</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Report Type</label>
										<Field name={'reporttype'} props={{options: ["By Stage", "By Timeline"], onChange: this.rangeOnChange, required: true}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Range'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Priority</label>
										<Field name={'priority'} props={{options: ["All", "Hot", "Warm", "Cold", "No Priority"], required: true}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Range'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Type</label>
										<Field name={'type'} props={{options: ["By Value", "By Number"], onChange: this.typeOnChange, required: true}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Type'})]} />
									</div>
									{this.props.resource.type == 'By Value' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Potential Revenue</label>
										<Field name={'potentialrevenue'} props={{options: ["With Probability", "Without Probability"], required: true}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Potential Revenue'})]} />
									</div> : null}
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Sales Person</label>
										<Field name={'salesperson'} props={{resource: "users", fields: "id,displayname", label: "displayname", filter: "users.issalesperson=true"}} component={selectAsyncEle} />
									</div>
									{this.props.resource.reporttype=="By Timeline" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Range</label>
										<Field name={'range'} props={{options: ["Monthly", "Quarterly"], required: true}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Range'})]} />
									</div> : null }
									{this.props.resource.reporttype=="By Timeline" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({title : 'From Date'})]}/>
									</div> : null }
									{this.props.resource.reporttype=="By Timeline" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div> : null }
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Territory</label>
										<Field name={'territory'} props={{resource: "territories", fields: "id,territoryname,fullname", label: "fullname"}} component={selectAsyncEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="row">
										<div className="col-md-12 col-sm-12 text-center">
											<button type="button" onClick={() => {
												this.getReportData()	
											}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										</div>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.chartObj.rows} />
							{this.props.resource.chartObj.rows.length > 0 ? <div className="col-md-12 paddingright-0 paddingleft-26">
								<div className= "gs-chart-properties">
									<div className="d-flex">
										<label className="gs-chart-properies-label">Stacked</label>
										<Field name={'chartObj.options.isStacked'} component={checkboxEle} />
									</div>
									<div className="d-flex">
										<label className="gs-chart-properies-label">Log Scale</label>
										<Field name={'chartObj.options.vAxis.logScale'} component={checkboxEle} />
									</div>
								</div>
							<Chart
								chartType="ColumnChart"
								columns={this.props.resource.chartObj.cols}
								rows={this.props.resource.chartObj.rows}
								options={this.props.resource.chartObj.options}
								graph_id="ColumnChart"
								width="100%"
								height="550px"
								legend_toggle
								chartEvents={this.chartEvents}
							/></div> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

SalesPipelineReportForm = connect(
	(state, props) => {
		let formName = 'salespipelinereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter, updateAppState }
)(reduxForm()(SalesPipelineReportForm));

export default SalesPipelineReportForm;
