import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { AutoSelect, SelectAsync, DateElement } from '../components/utilcomponents';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter } from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';
import StockdetailsModal from '../containers/stockdetails';

class DeliveryNotePlannerReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			deliverydate: new Date(new Date().setHours(0, 0, 0, 0)),
			search: {},
			limit: {
				order : 10,
				invoice : 10,
				purchasereturn : 10
			},
			orderArray: [],
			invoiceArray: [],
			purchasereturnArray: [],
			deliveryaddressArray: [],
			isOrderActive: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.tabOnChange = this.tabOnChange.bind(this);
		this.addLimit = this.addLimit.bind(this);
		this.parentCheckboxOnChange = this.parentCheckboxOnChange.bind(this);
		this.itemCheckboxOnChange = this.itemCheckboxOnChange.bind(this);
		this.filterOnchange = this.filterOnchange.bind(this);
		this.openStockDetails = this.openStockDetails.bind(this);
		this.createDC = this.createDC.bind(this);
		this.searchFilter = this.searchFilter.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag = (loaderflag) => {
		this.setState({loaderflag});
	}

	onLoad = () => {
		this.updateLoaderFlag(true);
		let orderArray = [], invoiceArray = [], purchasereturnArray = [], deliveryaddressArray = [];

		axios.get(`/api/query/deliveryplannerquery`).then((response) => {
			if (response.data.message == 'success') {
				let orderitems = response.data.main.orderitems,
				salesinvoiceitems = response.data.main.salesinvoiceitems,
				purchasereturnitems = response.data.main.purchasereturnitems;

				for (var i = 0; i < orderitems.length; i++) {
					let itemFound = false;
					for (var j = 0; j < orderArray.length; j++) {
						if (orderArray[j].id && (orderArray[j].id == orderitems[i].orderid)) {
							orderitems[i].number = orderitems[i].quantity;
							orderitems[i].numberdelivered = orderitems[i].deliveredqty;
							orderitems[i].deliverynoteqty = orderitems[i].quantity - orderitems[i].deliveredqty;
							orderitems[i].deliverydate = orderitems[i].earliestdate ? orderitems[i].earliestdate : orderitems[i].deliverydate;
							orderArray[j].orderitems.push(orderitems[i]);
							itemFound = true;
							break;
						}
					}

					if (!itemFound) {
						let tempObject = {
							orderno : orderitems[i].orderid_orderno,
							orderdate : orderitems[i].orderdate,
							customerid : orderitems[i].customerid,
							customerid_name : orderitems[i].customerid_name,
							companyid : orderitems[i].companyid,
							companyid_name : orderitems[i].companyid_name,
							currencyid : orderitems[i].currencyid,
							currencyid_name : orderitems[i].currencyid_name,
							salesperson : orderitems[i].salesperson,
							salesperson_displayname : orderitems[i].salesperson_displayname,
							id : orderitems[i].orderid,
							contactid : orderitems[i].contactid,
							contactperson : orderitems[i].contactperson,
							mobile : orderitems[i].mobile,
							phone : orderitems[i].phone,
							email : orderitems[i].email,
							deliverycontactid : orderitems[i].deliverycontactid,
							deliverycontactperson : orderitems[i].deliverycontactperson,
							deliverycontactmobile : orderitems[i].deliverycontactmobile,
							deliverycontactphone : orderitems[i].deliverycontactphone,
							deliverycontactemail : orderitems[i].deliverycontactemail,
							consigneeid : orderitems[i].consigneeid,
							modeofdespatch : orderitems[i].modeofdespatch,
							deliveryaddress : orderitems[i].deliveryaddress,
							billingaddress : orderitems[i].billingaddress,
							deliverydate : orderitems[i].deliverydate,
							deliverypolicy : orderitems[i].deliverypolicy,
							itemid_keepstock : orderitems[i].itemid_keepstock,
							itemid_issaleskit : orderitems[i].itemid_issaleskit,
							paymentterms : orderitems[i].paymentterms,
							orderitems : []
						};

						commonMethods.customFieldsOperation(this.props.app.myResources, 'orders', tempObject, orderitems[i], 'deliverynotes');

						if (orderitems[i].deliverypolicy)
							tempObject.param = 'Sales Orders for ' + orderitems[i].deliverypolicy;

						orderitems[i].number = orderitems[i].quantity;
						orderitems[i].numberdelivered = orderitems[i].deliveredqty;
						orderitems[i].deliverynoteqty = orderitems[i].quantity - orderitems[i].deliveredqty;
						orderitems[i].deliverydate = orderitems[i].earliestdate ? orderitems[i].earliestdate : orderitems[i].deliverydate;

						deliveryaddressArray.push({
							customerid : orderitems[i].customerid,
							customerid_name : orderitems[i].customerid_name,
							deliverydate : orderitems[i].deliverydate,
							orderno : orderitems[i].orderid_orderno,
							salesperson : orderitems[i].salesperson,
							addressid_latitude : orderitems[i].addressid_latitude,
							addressid_longitude : orderitems[i].addressid_longitude,
							displayadderss : orderitems[i].deliveryaddress
						});
						tempObject.orderitems.push(orderitems[i]);
						orderArray.push(tempObject);
					}
				}

				for (var i = 0; i < salesinvoiceitems.length; i++) {
					let itemFound = false;
					for (var j = 0; j < invoiceArray.length; j++) {
						if (invoiceArray[j].id && invoiceArray[j].id == salesinvoiceitems[i].invoiceid) {
							salesinvoiceitems[i].number = salesinvoiceitems[i].quantity;
							salesinvoiceitems[i].numberdelivered = salesinvoiceitems[i].deliveredqty;
							salesinvoiceitems[i].deliverynoteqty = salesinvoiceitems[i].quantity - salesinvoiceitems[i].deliveredqty;
							salesinvoiceitems[i].deliverydate = salesinvoiceitems[i].orderitemdeliverydate ? salesinvoiceitems[i].orderitemdeliverydate : salesinvoiceitems[i].deliverydate;
							invoiceArray[j].salesinvoiceitems.push(salesinvoiceitems[i]);
							itemFound = true;
							break;
						}
					}

					if (!itemFound) {
						let tempObject = {
							invoiceno : salesinvoiceitems[i].invoiceno,
							invoicedate : salesinvoiceitems[i].invoicedate,
							customerid : salesinvoiceitems[i].customerid,
							customerid_name : salesinvoiceitems[i].customerid_name,
							currencyid : salesinvoiceitems[i].currencyid,
							currencyid_name : salesinvoiceitems[i].currencyid_name,
							companyid : salesinvoiceitems[i].companyid,
							companyid_name : salesinvoiceitems[i].companyid_name,
							salesperson : salesinvoiceitems[i].salesperson,
							salesperson_displayname : salesinvoiceitems[i].salesperson_displayname,
							id : salesinvoiceitems[i].invoiceid,
							deliveryaddressid : salesinvoiceitems[i].deliveryaddressid,
							deliveryaddress : salesinvoiceitems[i].deliveryaddress,
							billingaddressid : salesinvoiceitems[i].billingaddressid,
							billingaddress : salesinvoiceitems[i].billingaddress,
							deliverydate : salesinvoiceitems[i].deliverydate,
							deliverypolicy : salesinvoiceitems[i].deliverypolicy,
							paymentterms : salesinvoiceitems[i].paymentterms,
							param : 'Sales Invoices',
							deliveryfor : 'Sales Invoices',
							itemid_keepstock : salesinvoiceitems[i].itemid_keepstock,
							itemid_issaleskit : salesinvoiceitems[i].itemid_issaleskit,
							invorderno : salesinvoiceitems[i].invorderno,
							invorderdate : salesinvoiceitems[i].invorderdate,
							invpodate : salesinvoiceitems[i].invpodate,
							ewaybillno : salesinvoiceitems[i].ewaybillno,
							contactid : salesinvoiceitems[i].contactid,
							contactperson : salesinvoiceitems[i].contactperson,
							mobile : salesinvoiceitems[i].mobile,
							phone : salesinvoiceitems[i].phone,
							email : salesinvoiceitems[i].email,
							deliverycontactid : salesinvoiceitems[i].deliverycontactid,
							deliverycontactperson : salesinvoiceitems[i].deliverycontactperson,
							deliverycontactmobile : salesinvoiceitems[i].deliverycontactmobile,
							deliverycontactphone : salesinvoiceitems[i].deliverycontactphone,
							deliverycontactemail : salesinvoiceitems[i].deliverycontactemail,
							consigneeid : salesinvoiceitems[i].consigneeid,
							salesinvoiceitems : []
						};

						salesinvoiceitems[i].number = salesinvoiceitems[i].quantity;
						salesinvoiceitems[i].numberdelivered = salesinvoiceitems[i].deliveredqty;
						salesinvoiceitems[i].deliverynoteqty = salesinvoiceitems[i].quantity - salesinvoiceitems[i].deliveredqty;
						salesinvoiceitems[i].deliverydate = salesinvoiceitems[i].orderitemdeliverydate ? salesinvoiceitems[i].orderitemdeliverydate : salesinvoiceitems[i].deliverydate;

						deliveryaddressArray.push({
							customerid : salesinvoiceitems[i].customerid,
							customerid_name : salesinvoiceitems[i].customerid_name,
							deliverydate : salesinvoiceitems[i].deliverydate,
							orderno : salesinvoiceitems[i].orderid_orderno,
							salesperson : salesinvoiceitems[i].salesperson,
							addressid_latitude : salesinvoiceitems[i].addressid_latitude,
							addressid_longitude : salesinvoiceitems[i].addressid_longitude,
							displayadderss : salesinvoiceitems[i].deliveryaddress
						});
						tempObject.salesinvoiceitems.push(salesinvoiceitems[i]);
						invoiceArray.push(tempObject);
					}
				}

				for (var i = 0; i < purchasereturnitems.length; i++) {
					let itemFound = false;
					for (var j = 0; j < purchasereturnArray.length; j++) {
						if (purchasereturnArray[j].id && purchasereturnArray[j].id == purchasereturnitems[i].purchasereturnid) {
							purchasereturnitems[i].number = purchasereturnitems[i].qtyauthorized;
							purchasereturnitems[i].numberdelivered = purchasereturnitems[i].qtyreturned;
							purchasereturnitems[i].deliverynoteqty = purchasereturnitems[i].qtyauthorized - purchasereturnitems[i].qtyreturned;
							purchasereturnitems[i].deliverydate = purchasereturnitems[i].returndate;
							purchasereturnArray[j].purchasereturnitems.push(purchasereturnitems[i]);
							itemFound = true;
							break;
						}
					}

					if (!itemFound) {
						let tempObject = {
							purchasereturnno : purchasereturnitems[i].purchasereturnno,
							againstorder: purchasereturnitems[i].againstorder,
							currencyid: purchasereturnitems[i].currencyid,
							currencyexchangerate: purchasereturnitems[i].currencyexchangerate,
							pricelistid: purchasereturnitems[i].pricelistid,
							defaultcostcenter: purchasereturnitems[i].defaultcostcenter,
							partnerid : purchasereturnitems[i].partnerid,
							customerid : purchasereturnitems[i].customerid,
							partnerid_name : purchasereturnitems[i].partnerid_name,
							id : purchasereturnitems[i].purchasereturnid,
							purchaseorderid_currencyid : purchasereturnitems[i].purchaseorderid_currencyid,
							purchaseorderid_currencyid_name : purchasereturnitems[i].purchaseorderid_currencyid_name,
							deliverydate : purchasereturnitems[i].returndate,
							companyid : purchasereturnitems[i].companyid,
							companyid_name : purchasereturnitems[i].companyid_name,
							returndate : purchasereturnitems[i].returndate,
							param : 'Purchase Returns',
							deliveryfor : 'Purchase Returns',
							purchasereturnitems : []
						};

						purchasereturnitems[i].number = purchasereturnitems[i].qtyauthorized;
						purchasereturnitems[i].numberdelivered = purchasereturnitems[i].qtyreturned;
						purchasereturnitems[i].deliverynoteqty = purchasereturnitems[i].qtyauthorized - purchasereturnitems[i].qtyreturned;
						purchasereturnitems[i].deliverydate = purchasereturnitems[i].returndate;
						deliveryaddressArray.push({
							customerid : purchasereturnitems[i].customerid,
							customerid_name : purchasereturnitems[i].partnerid_name,
							deliverydate : purchasereturnitems[i].deliverydate,
							orderno : purchasereturnitems[i].ponumber,
							addressid_latitude : purchasereturnitems[i].addressid_latitude,
							addressid_longitude : purchasereturnitems[i].addressid_longitude,
							displayadderss : purchasereturnitems[i].deliveryaddress
						});
						tempObject.purchasereturnitems.push(purchasereturnitems[i]);
						purchasereturnArray.push(tempObject);
					}
				}

				orderArray.sort((a, b) => {
					return new Date(a.deliverydate) - new Date(b.deliverydate);
				});

				invoiceArray.sort((a, b) => {
					return new Date(a.deliverydate) - new Date(b.deliverydate);
				});

				this.setState({ orderArray, invoiceArray, purchasereturnArray, deliveryaddressArray });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter = () => {
		let { search } = this.state;
		search = {};
		this.setState({search, deliverydate: null});
	}

	tabOnChange = (param) => {
		let isOrderActive = false, isInvoiceActive = false, isPurchaseReturnActive = false;
		if(param == 'order')
			isOrderActive = true;
		else if(param == 'invoice')
			isInvoiceActive = true;
		else if(param == 'purchasereturn')
			isPurchaseReturnActive = true;

		this.setState({ isOrderActive, isInvoiceActive, isPurchaseReturnActive });
	};

	addLimit = (param) => {
		this.state.limit[param] += 10;
		this.setState({limit : this.state.limit});
	};

	parentCheckboxOnChange = (checked, param, arrayname, childname) => {
		this.state[arrayname].forEach((data) => {
			if(data.id == param.id) {
				data.check = checked;
				data[childname].forEach((item) => {
					item.check = checked;
				});
			}
		});
		this.setState({ [arrayname] : this.state[arrayname] });
	};

	itemCheckboxOnChange = (checked, param, childname) => {
		param.check = checked;
		this.setState({ [childname] : this.state[childname] });
	};

	filterOnchange = (value, field) => {		
		if(field == 'deliverydate' )
			this.state.deliverydate = value;
		else
			this.state.search[field] = value;

		[{name: "orderArray", childname: 'orderitems'}, {name: "invoiceArray", childname: 'salesinvoiceitems'}, {name: "purchasereturnArray", childname: "purchasereturnitems"}].forEach((itemObj) => {
			this.state[itemObj.name].forEach((data) => {
				data.check = false;
				data[itemObj.childname].forEach((item) => {
					item.check = false;
				});
			});
		});

		this.setState({
			deliverydate: this.state.deliverydate,
			search: this.state.search,
			orderArray: this.state.orderArray,
			invoiceArray: this.state.invoiceArray,
			purchasereturnArray: this.state.purchasereturnArray
		});
	};

	openStockDetails = (item) => {
		this.props.openModal({render: (closeModal) => {
			return <StockdetailsModal resource = {{}} item={item} closeModal={closeModal} />
		}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}});
	};

	searchFilter = (array) => {
		let tempArr = [];
		array.map((a) => {
			let filtered = true;
			for(var prop in this.state.search) {
				if(this.state.search[prop]) {
					if(a[prop]) {
						if(this.state.search[prop] != a[prop]) {
							filtered = false;
						}
					} else {
						filtered = false;
					}
				}
			}
			if(a.deliverydate && this.state.deliverydate) {
				if(new Date(this.state.deliverydate).getTime() < new Date(a.deliverydate).getTime())
					filtered = false;
			}
			if(filtered)
				tempArr.push(a);
		});
		return tempArr;
	};

	createDC = () => {
		let { orderArray, invoiceArray, purchasereturnArray } = this.state;
		if (this.state.isOrderActive) {
			let errorFound = false,
			tempObj,
			tempArray = [];
			for (var i = 0; i < orderArray.length; i++) {
				for (var j = 0; j < orderArray[i].orderitems.length; j++) {
					if (orderArray[i].orderitems[j].check) {
						if (tempObj) {
							var item = orderArray[i].orderitems[j];
							if ((item.customerid != tempObj.customerid) || (item.deliverypolicy != tempObj.deliverypolicy) || (item.currencyid != tempObj.currencyid)) {
								errorFound = true;
								break;
							}
						} else
							tempObj = orderArray[i];

						tempArray.push(orderArray[i].orderitems[j]);
					}
				}

				if (errorFound)
					break;
			}

			if (errorFound)
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please select order item for same customer,currency and delivery policy',
					btnArray : ['Ok']
				}));
			else {
				if (tempObj) {
					var uniqueOrder = [];
					tempObj.ponumber = [];
					for (var i = 0; i < tempArray.length; i++) {
						if (uniqueOrder.indexOf(tempArray[i].orderid)) {
							if (tempArray[i].ponumber)
								tempObj.ponumber.push(tempArray[i].ponumber);

							uniqueOrder.push(tempArray[i].orderid);
						} else
							continue;
					}

					tempObj.ponumber = tempObj.ponumber.join();
					tempObj.orderitems = tempArray;

					this.props.history.push({pathname: '/createDeliveryNote', params: {...tempObj}});
				} else
					this.props.openModal(modalService['infoMethod']({
						header : 'Error',
						body : 'Please choose atleast one item to create delivery challan',
						btnArray : ['Ok']
					}));
			}
		} else if (this.state.isInvoiceActive) {
			let errorFound = false,
			tempObj,
			tempArray = [];
			for (var i = 0; i < invoiceArray.length; i++) {
				for (var j = 0; j < invoiceArray[i].salesinvoiceitems.length; j++) {
					if (invoiceArray[i].salesinvoiceitems[j].check) {
						if (tempObj) {
							var item = invoiceArray[i].salesinvoiceitems[j];
							if ((tempObj.customerid != item.customerid) || (tempObj.deliverypolicy != item.deliverypolicy) || (tempObj.currencyid != item.currencyid)) {
								errorFound = true;
								break;
							}
						} else
							tempObj = invoiceArray[i];

						tempArray.push(invoiceArray[i].salesinvoiceitems[j]);
					}
				}

				if (errorFound)
					break;
			}

			if (errorFound)
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please select invoice item for same customer,currency and delivery policy',
					btnArray : ['Ok']
				}));
			else {
				if (tempObj) {
					tempObj.customerreference = [];
					tempObj.invorderno = [];
					tempObj.invoiceno = [];
					tempObj.ewaybillno = [];
					var uniqueInvId = [];
					for (var i = 0; i < tempArray.length; i++) {
						if (uniqueInvId.indexOf(tempArray[i].invoiceid)) {
							if (tempObj.customerreference.indexOf(tempArray[i].customerreference) < 0 && tempArray[i].customerreference)
								tempObj.customerreference.push(tempArray[i].customerreference);
							if (tempObj.invorderno.indexOf(tempArray[i].invorderno) < 0 && tempArray[i].invorderno)
								tempObj.invorderno.push(tempArray[i].invorderno);

							if (tempArray[i].invoiceno)
								tempObj.invoiceno.push(tempArray[i].invoiceno);

							if(tempObj.ewaybillno.indexOf(tempArray[i].ewaybillno)<0 && tempArray[i].ewaybillno)
								tempObj.ewaybillno.push(tempArray[i].ewaybillno);

							uniqueInvId.push(tempArray[i].invoiceid);
						} else
							continue;
					}

					tempObj.customerreference = tempObj.customerreference.join();
					tempObj.invorderno = tempObj.invorderno.join();
					tempObj.invoiceno = tempObj.invoiceno.join();
					tempObj.ewaybillno = tempObj.ewaybillno.join();
					tempObj.salesinvoiceitems = tempArray;
					this.props.history.push({pathname: '/createDeliveryNote', params: {...tempObj, param: 'Sales Invoices'}});
				} else
					this.props.openModal(modalService['infoMethod']({
						header : 'Error',
						body : 'Please choose atleast one item to create delivery challan',
						btnArray : ['Ok']
					}));
			}
		} else if (this.state.isPurchaseReturnActive) {
			let errorFound = false,
			tempObj,
			tempArray = [];
			for (var i = 0; i < purchasereturnArray.length; i++) {
				for (var j = 0; j < purchasereturnArray[i].purchasereturnitems.length; j++) {
					if (purchasereturnArray[i].purchasereturnitems[j].check) {
						if (tempObj) {
							var item = purchasereturnArray[i].purchasereturnitems[j];
							if ((tempObj.id != item.purchasereturnid) || (tempObj.partnerid != item.partnerid) || (tempObj.currencyid != item.currencyid)) {
								errorFound = true;
								break;
							}
						} else
							tempObj = purchasereturnArray[i];

						tempArray.push(purchasereturnArray[i].purchasereturnitems[j]);
					}
				}

				if (errorFound)
					break;
			}

			if (errorFound)
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please select one purchase return  at a time',
					btnArray : ['Ok']
				}));
			else {
				if (tempObj) {
					tempObj.purchasereturnitems = tempArray;
					this.props.history.push({pathname: '/createDeliveryNote', params: {...tempObj, param: 'Purchase Returns'}});
				} else
					this.props.openModal(modalService['infoMethod']({
						header : 'Error',
						body : 'Please choose atleast one item to create delivery challan',
						btnArray : ['Ok']
					}));
			}
		}
	}

	render() {
		let filteredOrderArray = this.searchFilter(this.state.orderArray);
		let filteredInvoiceArray = this.searchFilter(this.state.invoiceArray);
		let filteredPurchaseReturnArray = this.searchFilter(this.state.purchasereturnArray);

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<div className="row">
					<div className="form-group col-md-12 bg-white report-header">
						<div className="report-header-title">Delivery Planner</div>
						<div className="report-header-rightpanel">
							<button type="button" onClick={this.createDC} className="btn btn-sm gs-btn-success"><i className="fa fa-plus"></i>Delivery Note</button>
						</div>
					</div>
					<div className="form-group col-md-12 bg-white paddingright-5">
						<div className="row">
							<div className="form-group col-md-3 col-sm-12">
								<label className="labelclass">Partner</label>
								<AutoSelect resource={'partners'} fields={'id,name,displayname'} value={this.state.search.customerid} label={'displayname'} valuename={'id'} onChange={(value, valueobj) => this.filterOnchange(value, 'customerid')} />
							</div>
							<div className="form-group col-md-3 col-sm-12">
								<label className="labelclass">Delivery Date</label>
								<DateElement className="form-control" value={this.state.deliverydate} onChange={(val) => this.filterOnchange(val, 'deliverydate')} />
							</div>
							<div className="form-group col-md-3 col-sm-12">
								<label className="labelclass">Sales Person</label>
								<SelectAsync resource={'users'} fields={'id,displayname'} value={this.state.search.salesperson} label={'displayname'} valuename={'id'} filter={'users.issalesperson=true'} onChange={(value, valueobj) => this.filterOnchange(value, 'salesperson')} />
							</div>
							<div className="form-group col-md-3 col-sm-12" >
								<button type="button" onClick={() => {
									this.resetFilter()	
								}} className="btn btn-width btn-sm gs-btn-info margintop-25"><i className="fa fa-refresh"></i>Reset</button>
							</div>
						</div>
					</div>

					<div className="form-group col-md-12 bg-white paddingright-5">
						<div className="row">
							<div className="col-md-12 col-sm-12 col-xs-12">
								<ul className="nav nav-tabs">
									<li className={`nav-item nav-link ${this.state.isOrderActive ? 'active' : ''}`} onClick={() => this.tabOnChange('order')}>
										<i className="fa fa-thumbs-up"></i>
										<span className="marginleft-10">Orders</span>
										<span className="badge badge-secondary marginleft-10">{filteredOrderArray.length}</span>
									</li>
									<li className={`nav-item nav-link ${this.state.isInvoiceActive ? 'active' : ''}`} onClick={() => this.tabOnChange('invoice')}>
										<i className="fa fa-list-alt"></i>
										<span className="marginleft-10">Sales Invoices</span>
										<span className="badge badge-secondary marginleft-10">{filteredInvoiceArray.length}</span>
									</li>
									<li className={`nav-item nav-link ${this.state.isPurchaseReturnActive ? 'active' : ''}`} onClick={() => this.tabOnChange('purchasereturn')}>
										<i className="fa fa-list-alt"></i>
										<span className="marginleft-10">Purchase Returns</span>
										<span className="badge badge-secondary marginleft-10">{filteredPurchaseReturnArray.length}</span>
									</li>
								</ul>

								<div className="tab-content">
									<div className={`tab-pane fade ${this.state.isOrderActive ? 'show active' : ''}`}>
										<div className="row margintop-25">
											{filteredOrderArray.map((orderdata, orderdataindex) => {
												if(this.state.limit.order > orderdataindex) {
													return(
														<div className="form-group col-md-12 col-sm-12 col-xs-12" key={orderdataindex}>
															<div className="card border-info">
																<div className="card-header">
																	<div className="float-left">
																		<span className="marginleft-11">
																			<input type="checkbox" checked={orderdata.check || false} onChange={(evt) => this.parentCheckboxOnChange(evt.target.checked, orderdata, 'orderArray', 'orderitems')} />
																		</span>
																		<a href={`#/details/orders/${orderdata.id}`} className="marginleft-25">{orderdata.orderno}</a>
																	</div>
																	<div className="float-left">
																		<span> | Customer :   </span>
																		<span>{orderdata.customerid_name}</span>
																		<span> | Currency : </span>
																		<span>{this.props.app.currency[orderdata.currencyid].name}</span>
																		<span> | Salesperson : </span>
																		<span>{orderdata.salesperson_displayname}</span>
																		<label className="badge badge-info marginleft-10">{orderdata.deliverypolicy}</label>
																	</div>
																	<div className="float-right text-right"><button type="button" className="btn btn-secondary btn-sm" title={`${orderdata.deliveryaddress}`}>Address</button></div>
																</div>
																<div className="card-body padding-0">
																	<div className="table-responsive">
																		<table className="table table-sm marginbottom-0">
																			<thead>
																				<tr>
																					<th></th>
																					<th className="text-center">Item</th>
																					<th className="text-center">Qty Ordered</th>
																					<th className="text-center">Qty Delivered</th>
																					<th className="text-center">Order Date</th>
																					<th className="text-center">Delivery Date</th>
																					<th className="text-center">Qty to Deliver</th>
																					<th className="text-center"></th>
																				</tr>
																			</thead>
																			{orderdata.orderitems ? <tbody>
																				{orderdata.orderitems.map((orderitem, orderitemindex) => {
																					return (
																						<tr key={orderitemindex}>
																							<td className='text-center' style={{width: '5%'}}><input type="checkbox" checked={orderitem.check || false} onChange={(evt) => this.itemCheckboxOnChange(evt.target.checked, orderitem, 'orderitems')}/></td>
																							<td className="text-center">{orderitem.itemid_name}</td>
																							<td className="text-center">{orderitem.number}</td>
																							<td className="text-center">{orderitem.numberdelivered || 0}</td>
																							<td className="text-center">{dateFilter(orderitem.orderdate)}</td>
																							<td className="text-center">{dateFilter(orderitem.deliverydate)}</td>
																							<td className="text-center width-20">{orderitem.deliverynoteqty}</td>
																							<td className="text-center">
																								{orderitem.itemid_keepstock && !orderitem.itemid_issaleskit ? <button type="button" className={`btn btn-sm ${orderitem.onhandqty >= orderitem.stockqy ? 'gs-form-btn-success' : 'gs-form-btn-danger'}`} onClick={() => this.openStockDetails(orderitem)} tooltip="Check Stock Details"><span className="fa fa-shopping-cart"></span></button> : null }
																							</td>
																						</tr>
																					)
																				})}
																			</tbody> : null}
																		</table>
																	</div>
																</div>
															</div>
														</div>
													)
												}
											})}

											{filteredOrderArray.length > this.state.limit.order ?<div className="col-md-12 col-sm-12 text-center">
												<button type="button" className="btn btn-sm gs-btn-warning" onClick={() => this.addLimit("order")}>Show More</button>
											</div> : null }
										</div>
									</div>

									<div className={`tab-pane fade ${this.state.isInvoiceActive ? 'show active' : ''}`}>
										<div className="row margintop-25">
											{filteredInvoiceArray.map((invoicedata, invoicedataindex) => {
												if(this.state.limit.invoice > invoicedataindex) {
													return(
														<div className="form-group col-md-12 col-sm-12 col-xs-12" key={invoicedataindex}>
															<div className="card border-info">
																<div className="card-header">
																	<div className="float-left">
																		<span className="marginleft-11">
																			<input type="checkbox" checked={invoicedata.check || false} onChange={(evt) => this.parentCheckboxOnChange(evt.target.checked, invoicedata, 'invoiceArray', 'salesinvoiceitems')} />
																		</span>
																		<a href={`#/details/salesinvoices/${invoicedata.id}`} className="marginleft-25">{invoicedata.invoiceno}</a>
																	</div>
																	<div className="float-left">
																		<span> | Customer :   </span>
																		<span>{invoicedata.customerid_name}</span>
																		<span> | Currency : </span>
																		<span>{this.props.app.currency[invoicedata.currencyid].name}</span>
																		<span> | Salesperson : </span>
																		<span>{invoicedata.salesperson_displayname}</span>
																		<label className="badge badge-info marginleft-10">{invoicedata.deliverypolicy}</label>
																	</div>
																	<div className="float-right text-right"><button type="button" className="btn btn-secondary btn-sm" title={`${invoicedata.deliveryaddress}`}>Address</button></div>
																</div>
																<div className="card-body padding-0">
																	<div className="table-responsive">
																		<table className="table table-sm marginbottom-0">
																			<thead>
																				<tr>
																					<th></th>
																					<th className="text-center">Item</th>
																					<th className="text-center">Qty Ordered</th>
																					<th className="text-center">Qty Delivered</th>
																					<th className="text-center">Order Date</th>
																					<th className="text-center">Delivery Date</th>
																					<th className="text-center">Qty to Deliver</th>
																					<th className="text-center"></th>
																				</tr>
																			</thead>
																			{invoicedata.salesinvoiceitems ? <tbody>
																				{invoicedata.salesinvoiceitems.map((invoiceitem, invoiceitemindex) => {
																					return (
																						<tr key={invoiceitemindex}>
																							<td className='text-center' style={{width: '5%'}}><input type="checkbox" checked={invoiceitem.check || false} onChange={(evt) => this.itemCheckboxOnChange(evt.target.checked, invoiceitem, 'salesinvoiceitems')}/></td>
																							<td className="text-center">{invoiceitem.itemid_name}</td>
																							<td className="text-center">{invoiceitem.number}</td>
																							<td className="text-center">{invoiceitem.numberdelivered || 0}</td>
																							<td className="text-center">{dateFilter(invoiceitem.orderdate)}</td>
																							<td className="text-center">{dateFilter(invoiceitem.deliverydate)}</td>
																							<td className="text-center width-20">{invoiceitem.deliverynoteqty}</td>
																							<td className="text-center">
																								{invoiceitem.itemid_keepstock && !invoiceitem.itemid_issaleskit ? <button type="button" className={`btn btn-sm ${invoiceitem.onhandqty >= invoiceitem.stockqy ? 'gs-form-btn-success' : 'gs-form-btn-danger'}`} onClick={() => this.openStockDetails(invoiceitem)} tooltip="Check Stock Details"><span className="fa fa-shopping-cart"></span></button> : null }
																							</td>
																						</tr>
																					)
																				})}
																			</tbody> : null}
																		</table>
																	</div>
																</div>
															</div>
														</div>
													)
												}
											})}

											{filteredInvoiceArray.length > this.state.limit.invoice ? <div className="col-md-12 col-sm-12 text-center">
												<button type="button" className="btn btn-sm gs-btn-warning" onClick={() => this.addLimit("invoice")}>Show More</button>
											</div> : null }
										</div>
									</div>

									<div className={`tab-pane fade ${this.state.isPurchaseReturnActive ? 'show active' : ''}`}>
										<div className="row margintop-25">
											{filteredPurchaseReturnArray.map((purchasereturndata, purchasereturndataindex) => {
												if(this.state.limit.purchasereturn > purchasereturndataindex) {
													return(
														<div className="form-group col-md-12 col-sm-12 col-xs-12" key={purchasereturndataindex}>
															<div className="card border-info">
																<div className="card-header">
																	<div className="float-left">
																		<span className="marginleft-11">
																			<input type="checkbox" checked={purchasereturndata.check || false} onChange={(evt) => this.parentCheckboxOnChange(evt.target.checked, purchasereturndata, 'purchasereturnArray', 'purchasereturnitems')} />
																		</span>
																		<a href={`#/details/purchasereturns/${purchasereturndata.id}`} className="marginleft-25">{purchasereturndata.purchasereturnno}</a>
																	</div>
																	<div className="float-left">
																		<span> | Customer :   </span>
																		<span>{purchasereturndata.partnerid_name}</span>
																		<label className="badge badge-info marginleft-10">{purchasereturndata.deliverypolicy}</label>
																	</div>
																	<div className="float-right text-right"><button type="button" className="btn btn-secondary btn-sm" title={`${purchasereturndata.deliveryaddress}`}>Address</button></div>
																</div>
																<div className="card-body padding-0">
																	<div className="table-responsive">
																		<table className="table table-sm marginbottom-0">
																			<thead>
																				<tr>
																					<th></th>
																					<th className="text-center">Item</th>
																					<th className="text-center">Qty Ordered</th>
																					<th className="text-center">Qty Delivered</th>
																					<th className="text-center">Order Date</th>
																					<th className="text-center">Delivery Date</th>
																					<th className="text-center">Qty to Deliver</th>
																					<th className="text-center"></th>
																				</tr>
																			</thead>
																			{purchasereturndata.purchasereturnitems ? <tbody>
																				{purchasereturndata.purchasereturnitems.map((purchasereturnitem, purchasereturnitemindex) => {
																					return (
																						<tr key={purchasereturnitemindex}>
																							<td className='text-center' style={{width: '5%'}}><input type="checkbox" checked={purchasereturnitem.check || false} onChange={(evt) => this.itemCheckboxOnChange(evt.target.checked, purchasereturnitem, 'purchasereturnitems')}/></td>
																							<td className="text-center">{purchasereturnitem.itemid_name}</td>
																							<td className="text-center">{purchasereturnitem.number}</td>
																							<td className="text-center">{purchasereturnitem.numberdelivered || 0}</td>
																							<td className="text-center">{dateFilter(purchasereturnitem.podate)}</td>
																							<td className="text-center">{dateFilter(purchasereturnitem.deliverydate)}</td>
																							<td className="text-center width-20">{purchasereturnitem.deliverynoteqty}</td>
																							<td className="text-center">
																								{purchasereturnitem.itemid_keepstock && !purchasereturnitem.itemid_issaleskit ? <button type="button" className={`btn btn-sm ${purchasereturnitem.onhandqty >= purchasereturnitem.stockqy ? 'gs-form-btn-success' : 'gs-form-btn-danger'}`} onClick={() => this.openStockDetails(purchasereturnitem)} tooltip="Check Stock Details"><span className="fa fa-shopping-cart"></span></button> : null }
																							</td>
																						</tr>
																					)
																				})}
																			</tbody> : null}
																		</table>
																	</div>
																</div>
															</div>
														</div>
													)
												}
											})}

											{filteredPurchaseReturnArray.length > this.state.limit.purchasereturn ? <div className="col-md-12 col-sm-12 text-center">
												<button type="button" className="btn btn-sm gs-btn-warning" onClick={() => this.addLimit("purchasereturn")}>Show More</button>
											</div> : null }
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</>
		);
	};
}



DeliveryNotePlannerReportForm = connect((state) =>{
	return {app: state.app}
}) (DeliveryNotePlannerReportForm);

export default DeliveryNotePlannerReportForm;
