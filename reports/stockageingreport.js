import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, checkboxEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class StockAgeingReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Item Name",
					"key" : "itemname",
					"locked": true,
					"width" : 250
				}, {
					"name" : "Description",
					"key" : "description",
					"locked": true,
					"width" : 250
				}, {
					"name" : "Qty",
					"key" : "qty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "UOM",
					"key" : "uom",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Min Age",
					"key" : "minage",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Max Age",
					"key" : "maxage",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Average Age",
					"key" : "avgage",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Value",
					"key" : "value",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					"width" : 180
				}, {
					"name" : "0-30",
					"key" : "age30",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					"width" : 180
				}, {
					"name" : "0-30 (Qty)",
					"key" : "qty30",
					"format" : "number",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 180
				}, {
					"name" : "31-60",
					"key" : "age60",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					"width" : 180
				}, {
					"name" : "31-60 (Qty)",
					"key" : "qty60",
					"format" : "number",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 180
				}, {
					"name" : "61-90",
					"key" : "age90",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					"width" : 180
				}, {
					"name" : "61-90 (Qty)",
					"key" : "qty90",
					"format" : "number",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 180
				}, {
					"name" : "91-120",
					"key" : "age120",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					"width" : 180
				}, {
					"name" : "91-120 (Qty)",
					"key" : "qty120",
					"format" : "number",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 180
				}, {
					"name" : "121-150",
					"key" : "age150",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					"width" : 180
				}, {
					"name" : "121-150 (Qty)",
					"key" : "qty150",
					"format" : "number",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 180
				}, {
					"name" : "151-180",
					"key" : "age180",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					"width" : 180
				}, {
					"name" : "151-180 (Qty)",
					"key" : "qty180",
					"format" : "number",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 180
				}, {
					"name" : "181-365",
					"key" : "age365",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					"width" : 180
				}, {
					"name" : "181-365 (Qty)",
					"key" : "qty365",
					"format" : "number",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 180
				}, {
					"name" : "365+",
					"key" : "age365above",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					"width" : 180
				}, {
					"name" : "365+ (Qty)",
					"key" : "qty365above",
					"format" : "number",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 180
				}, {
					"name" : "Item Category",
					"key" : "categoryname",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Item Group",
					"key" : "groupname",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "",
					"format" : "button",
					"buttonname" : "Details",
					"onClick" : "{report.btnOnClick}",
					"cellClass" : "text-center",
					"restrictToExport" : true,
					"width" : 100
				}]
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];
		['warehouseid', 'itemid', 'itemcategoryid', 'itemgroupid'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/stockageingreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			warehouseid: null,
			itemid: null,
			itemcategoryid: null,
			itemgroupid: null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	btnOnClick(data) {
		this.props.openModal({
			render: (closeModal) => {
				return <StockItemDetailsModal data={data} warehouseid={this.props.resource.warehouseid} app={this.props.app} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Stock Ageing Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Warehouse</label>
										<Field name={'warehouseid'} props={{resource: "stocklocations", fields: "id,name", filter: `stocklocations.companyid=${this.props.app.selectedcompanyid} and stocklocations.isparent and stocklocations.parentid is null ${!this.props.resource.includeprojectwarehouse ? ' AND stocklocations.projectid is null' : ''}`, required: true}} component={selectAsyncEle}  validate={[numberNewValidation({required: true, model: 'Warehouse'})]} />
									</div>
									{this.props.app.feature.useProjects ? <div className="form-group col-md-12 col-sm-12 d-flex">
										<label className="labelclass">Include Project Warehouse</label>
										<Field name={'includeprojectwarehouse'} props={{}} component={checkboxEle}/>
									</div>: null }
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Name</label>
										<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Group</label>
										<Field name={'itemgroupid'} props={{resource: "itemgroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Stock Ageing Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

class StockItemDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderFlag: false,
			resultArray : []
		};
		this.onLoad = this.onLoad.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		this.setState({loaderFlag: true});
		let resultArray = [];
		axios.get(`/api/query/stockageingreportquery?param=getItemDetails&warehouseid=${this.props.warehouseid}&itemid=${this.props.data.itemid}`).then((response) => {
			if (response.data.message == 'success') {
				resultArray = response.data.main;
				this.setState({resultArray});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.setState({loaderFlag: false});
		});
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Item Details</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-8 md-offset-2">
							<div className="row">
								<div className="form-group col-md-6 col-sm-6 col-xs-12">
									<label className="labelclass">Item Name</label>
									<span className="form-control un-editable" disabled>{this.props.data.itemname}</span>
								</div>
								<div className="form-group col-md-6 col-sm-6 col-xs-12">
									<label className="labelclass">Warehouse</label>
									<span className="form-control un-editable" disabled>{this.props.data.warehouse}</span>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						{this.state.loaderFlag ? <div className="col-md-12 col-sm-12 col-xs-12 alert alert-warning">
							<b>Loading Report... Please Wait...</b>
						</div> : <div className="col-md-12">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th className="text-center">Receipt Note No & Date</th>
										<th className="text-center">Stock Origin Date</th>
										<th className="text-center">Qty</th>
										<th className="text-center">Qty Available</th>
										<th className="text-center">UOM</th>
										<th className="text-center">Age (in Days)</th>
										<th className="text-center">Valuation Rate</th>
										<th className="text-center">Total Value</th>
									</tr>
								</thead>
								<tbody>
									{this.state.resultArray.map((item, index) => {
										return (
											<tr key={index}>
												<td className="text-center">{item.receiptnotenumber} & {dateFilter(item.receiptnotedate)}</td>
												<td className="text-center">{dateFilter(item.stockorigindate)}</td>
												<td className="text-right">
													{item.qty}
												</td>
												<td className="text-right">
													{item.qtyavailable}
												</td>
												<td className="text-center">{item.uom}</td>
												<td className="text-right">{item.age}</td>
												<td className="text-right">
													{currencyFilter(item.valuationrate, item.currencyid, this.props.app)}
												</td>
												<td className="text-right">
													{currencyFilter((item.valuationrate * item.qtyavailable), item.currencyid, this.props.app)}
												</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>}
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

StockAgeingReportForm = connect(
	(state, props) => {
		let formName = 'stockageingreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(StockAgeingReportForm));

export default StockAgeingReportForm;
