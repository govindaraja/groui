import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class PurchasePlannerReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.validatePlanner = this.validatePlanner.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				report : 'purchase',
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "",
					"headerformat" : "checkbox",
					"key" : "ischecked",
					"locked" : true,
					"format" : "checkbox",
					"cellClass" : "text-center",
					"onChange" : "{report.checkboxOnChange}",
					"restrictToExport" : true,
					"width" : 100
				}, {
					"name" : "Item Name",
					"key" : "itemid_name",
					"width" : 250
				}, {
					"name" : "Item Description",
					"key" : "description",
					"width" : 300
				}, {
					"name" : "Item Category",
					"key" : "itemcategorymasterid_name",
					"width" : 200
				}, {
					"name" : "Item Group",
					"key" : "itemgroupid_fullname",
					"width" : 180
				}, {
					"name" : "On Hand Qty",
					"key" : "onhandqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Quantity to Purchase",
					"key" : "quantity",
					"editable" : true,
					"width" : 200
				}, {
					"name" : "Supplier Name",
					"key" : "defaultsupplierid_name",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "SO Qty",
					"key" : "soqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "PO Qty",
					"key" : "poqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Effective Qty",
					"key" : "effectiveqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Safety Qty",
					"key" : "safetystock",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Reorder Qty",
					"key" : "reorderqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "UOM",
					"key" : "uomid_name",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Lead Time",
					"key" : "leadtime",
					"width" : 180
				}]
			};

		this.props.initialize(tempObj);

		setTimeout(() => {
			if(this.props.reportdata)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
	
		axios.get(`/api/query/purchaseplannerquery?warehouseid=${this.props.resource.warehouseid}&itemcategorymasterid=${this.props.resource.itemcategorymasterid}&itemgroupid=${this.props.resource.itemgroupid}&deliveryupto=${this.props.resource.deliveryupto}`).then((response) => {
			if (response.data.message == 'success') {
				let resultArray = this.changeUOM(response.data.main);

				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: resultArray,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	changeUOM(resultArray) {
		let objArray = ['onhandqty', 'soqty', 'poqty', 'effectiveqty', 'safetystock', 'reorderqty'],
		conversionfactor;

		if(resultArray.length > 0) {
			for (var i = 0; i < resultArray.length; i++) {
				if (this.props.resource.report == 'purchase' && resultArray[i].purchaseuomid != resultArray[i].stockuomid) {
					resultArray[i].uomid = resultArray[i].purchaseuomid;
					resultArray[i].conversionfactor = resultArray[i].purchaseuomid_conversionfactor;
					resultArray[i].conversiontype = resultArray[i].purchaseuomid_conversiontype;
					conversionfactor = resultArray[i].purchaseuomid_conversionfactor;
				} else {
					resultArray[i].uomid = resultArray[i].stockuomid;
					resultArray[i].conversionfactor = null;
					resultArray[i].conversiontype = null;
					conversionfactor = 1;
				}

				for (var j = 0; j < objArray.length; j++)
					resultArray[i][objArray[j]] *= conversionfactor;

				resultArray[i].quantity = (resultArray[i].reorderqty > (resultArray[i].safetystock - resultArray[i].effectiveqty)) ? resultArray[i].reorderqty : (resultArray[i].safetystock - resultArray[i].effectiveqty);
		
				resultArray[i].quantity = Number(resultArray[i].quantity.toFixed(this.props.app.roundOffPrecision));
			}
		}
		return resultArray;
	}

	resetFilter () {
		let tempObj = {
			warehouseid : null,
			report : null,
			itemcategorymasterid : null,
			itemgroupid : null,
			deliveryupto : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	checkboxOnChange(value, item) {
		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		this.refs.grid.refresh();
	}

	checkboxHeaderOnChange(param) {
		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = param ? true : false;
		});
		setTimeout(() => {
			this.props.updateFormState(this.props.form, {
				originalRows: this.props.resource.originalRows
			});
		}, 0);
		this.refs.grid.forceRefresh();
	}

	validatePlanner (param) {
		let itemArray = [],
		errorFound = false;
		for (var i = 0; i < this.props.resource.originalRows.length; i++)
			if (this.props.resource.originalRows[i].ischecked)
				itemArray.push(this.props.resource.originalRows[i]);

		for (var i = 0; i < itemArray.length; i++) {
			if (!(itemArray[i].quantity > 0)) {
				errorFound = true;
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : `${itemArray[i].itemid_name} Quantity should greated than zero`,
					btnArray : ['Ok']
				}));
				break;
			}
		}

		if (!errorFound) {
			if (itemArray.length > 0) {
				if (param == 'RFQ')
					this.props.history.push({pathname: '/createRfq', params: {purchaseArray: itemArray, param: 'Purchase Planner'}});
				else {
					let tempObj = {...itemArray[0]};
					tempObj.purchaseItemArray = itemArray;
					this.props.history.push({pathname: '/createPurchaseOrder', params: {...tempObj, param: 'Purchase Planner'}});
				}
			} else
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please Choose atleast one item to purchase',
					btnArray : ['Ok']
				}));
		}
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Purchase Planner</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <div className="report-header-rightpanel">
								{(this.props.app.feature.useSupplierQuotations && checkActionVerbAccess(this.props.app, 'rfq', 'Save')) ? <button type="button" className="btn gs-btn-success btn-sm btn-width" onClick={() => this.validatePlanner('RFQ')}><i className="fa fa-plus"></i>RFQ</button> : null}
								{checkActionVerbAccess(this.props.app, 'purchaseorders', 'Save') ? <button type="button" className="btn gs-btn-success btn-sm btn-width" onClick={() => this.validatePlanner('PO')}><i className="fa fa-plus"></i>PO</button> : null}
							</div> : null}
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Warehouse</label>
										<Field name={'warehouseid'} props={{resource: "stocklocations", fields: "id,name", filter: `stocklocations.companyid=${this.props.app.selectedcompanyid} and stocklocations.isparent and stocklocations.parentid is null`, required: true}} component={selectAsyncEle}  validate={[numberNewValidation({required: true, model: 'Warehouse'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Report UOM</label>
										<Field name={'report'} props={{options:[{value: "purchase", label: "Purchase UOM"}, {value: "stock", label: "Stock UOM"}], label: "label", valuename: "value"}} component={localSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategorymasterid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Group</label>
										<Field name={'itemgroupid'} props={{resource: "itemgroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Delivery Up To</label>
										<Field name={'deliveryupto'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Purchase Planner Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

PurchasePlannerReportForm = connect(
	(state, props) => {
		let formName = 'purchaseplannerreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(PurchasePlannerReportForm));

export default PurchasePlannerReportForm;
