import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class SerialnoHistoryReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.getWarehouseDetails = this.getWarehouseDetails.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				warehouseArray : [],
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Warehouse",
					"key" : "warehouseid_name",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Item Name",
					"key" : "itemserialnoid_itemname",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Serial Number",
					"key" : "itemserialnoid_serialno",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Created On",
					"key" : "created",
					format : "datetime",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Status",
					"key" : "status",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Batch",
					"key" : "itembatchid_batchnumber",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Location",
					"key" : "stocklocationid_name",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Source Type",
					"key" : "resource",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Source",
					"key" : "sourceno",
					"format" : "anchortag",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Remarks",
					"key" : "remarks",
					"cellClass" : "text-center",
					"width" : 300
				}]
			};

		this.props.initialize(tempObj);

		setTimeout(() => {
			this.getWarehouseDetails();
		}, 0);

		this.updateLoaderFlag(false);
	}

	getWarehouseDetails() {
		this.updateLoaderFlag(true);

		axios.get(`/api/stocklocations?pagelength=1000&field=id,name&filtercondition=stocklocations.companyid=${this.props.app.selectedcompanyid}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.array.removeAll('warehouseArray');

				let warehouseArray = [];
				for (var i = 0; i < response.data.main.length; i++)
					warehouseArray.push(response.data.main[i].id);

				this.props.updateFormState(this.props.form, { warehouseArray });

				if(this.props.reportdata)
					this.getReportData();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let { columns } = this.props.resource;

		axios.get(`/api/query/itemserialnohistoryquery?itemid=${this.props.resource.itemid}&itemserialnoid=${this.props.resource.itemserialnoid}`).then((response) => {
			if (response.data.message == 'success') {

				response.data.main.map((item) => {
					if(item.relatedresource == 'receiptnotes') {
						item.resource = 'Receipt Note';
						item.remarks = item.receiptnoteid_description;
						item.sourceno = item.receiptnotenumber;
					}

					if(item.relatedresource == 'deliverynotes') {
						item.resource = 'Delivery Note';
						item.remarks = item.deliverynoteid_description;
						item.sourceno = item.deliverynotenumber;
						item.relatedresource = 'deliverynotes'
					}
				});

				let hiddencols = [];

				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true)
						hiddencols.push(item.key);
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			itemid: null,
			itemserialnoid: null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data) {
		this.props.history.push(`/details/${data.relatedresource}/${data.relatedid}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Serial Number History</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item</label>
										<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name,displayname", label:"displayname", displaylabel:"name", filter:"itemmaster.hasserial", onChange: () => this.props.updateFormState(this.props.form, {itemserialnoid: null})}} component={autoSelectEle} validate={[numberNewValidation({required: true})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Serial Number</label>
										<Field name={'itemserialnoid'} props={{resource: "itemserialnos", fields: "id,serialno", label:"serialno", filter: `itemserialnos.itemid=${this.props.resource.itemid} AND itemserialnos.warehouseid IN (${this.props.resource.warehouseArray})`}} component={autoSelectEle} validate={[numberNewValidation({required: true})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Serial Number History Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} btnOnClick={this.btnOnClick} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

SerialnoHistoryReportForm = connect(
	(state, props) => {
		let formName = 'serialnohistoryreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(SerialnoHistoryReportForm));

export default SerialnoHistoryReportForm;
