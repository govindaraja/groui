import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class SalesProfitabilityReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			profitabilityColumn : [{
				"name" : "Quantity Sold",
				"key" : "quantity",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Sales Rate",
				"key" : "salesrate",
				"format" : "currency",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Total Sales Value",
				"key" : "totalsalesvalue",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Cost Rate",
				"key" : "costrate",
				"format" : "currency",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Total Cost Value",
				"key" : "totalcostvalue",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Gross Margin",
				"key" : "grossprofit",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Gross Margin Percentage",
				"key" : "profitpercentage",
				"format" : "number",
				"footertype" : "calculated",
				"footerCalculation" : "{(colarr, aggarr) => report.getFooterValue('profitpercentage', colarr, aggarr)}",
				"cellClass" : "text-right",
				"width" : 180
			}],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.getFooterValue = this.getFooterValue.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				groupingby : '',
				filters: {},
				hiddencols: [],
				columns: []
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let { profitabilityColumn } = this.state;

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}


		let filterString = [];
		['companyid', 'fromdate', 'todate', 'groupingby'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/salesprofitabilityreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let temp = 1, hiddencols = [], totalCostValue = 0, totalGrossProfit = 0;

				response.data.main.forEach((item) => {
					totalCostValue += item.totalcostvalue;
					totalGrossProfit += item.grossprofit;
				});

				let totalGrossPercentage = Number(((totalGrossProfit/totalCostValue)*100).toFixed(2));

				if(profitabilityColumn.length > 7)
					profitabilityColumn.splice(0, profitabilityColumn.length - 7);

				if(this.props.resource.groupingby == 'Sales Order') {
					let orderColArr = [{
						name : 'Grouping Variable',
						key : 'groupingvariable',
						format : "anchortag",
						transactionname : "orders",
						transactionid : "salesorderid",
						width : 200
					}, {
						name : 'Order Date',
						key : 'orderdate',
						format : 'date',
						cellClass : 'text-center',
						width : 150
					}, {
						name : 'Customer',
						key : 'customerid_name',
						width : 250
					}, {
						name : 'Sales Person',
						key : 'salesperson_displayname',
						width : 150
					}];
					profitabilityColumn.splice(0, 0, ...orderColArr);
				} else if(this.props.resource.groupingby == 'Invoice') {
					let invColArr = [{
						name : 'Grouping Variable',
						key : 'groupingvariable',
						key : 'groupingvariable',
						format : "anchortag",
						transactionname : "salesinvoices",
						transactionid : "salesinvoiceid",
						width : 200
					}, {
						name : 'Invoice Date',
						key : 'invoicedate',
						format : 'date',
						cellClass : 'text-center',
						width : 150
					}, {
						name : 'Order No',
						key : 'orderno',
						format : "anchortag",
						transactionname : "orders",
						transactionid : "salesorderid",
						width : 200
					}, {
						name : 'Customer',
						key : 'customerid_name',
						width : 250
					}, {
						name : 'Sales Person',
						key : 'salesperson_displayname',
						width : 150
					}, {
						name : 'Territory',
						key : 'territoryname',
						cellClass : 'text-center',
						width : 150
					}, {
						name : 'Numbering Series',
						key : 'numberingseriesmasterid_name',
						cellClass : 'text-center',
						width : 160
					}];
					profitabilityColumn.splice(0, 0, ...invColArr);
				} else if(this.props.resource.groupingby == 'Invoice Item') {
					let invItemColArr = [{
						name : 'Grouping Variable',
						key : 'groupingvariable',
						width : 200
					}, {
						name : 'Invoice No',
						key : 'invoiceno',
						format : "anchortag",
						transactionname : "salesinvoices",
						transactionid : "salesinvoiceid",
						cellClass : 'text-center',
						width : 150
					}, {
						name : 'Invoice Date',
						key : 'invoicedate',
						format : 'date',
						cellClass : 'text-center',
						width : 150
					}, {
						name : 'Order No',
						key : 'orderno',
						format : "anchortag",
						transactionname : "orders",
						transactionid : "salesorderid",
						width : 200
					}, {
						name : 'Customer',
						key : 'customerid_name',
						width : 250
					}, {
						name : 'Sales Person',
						key : 'salesperson_displayname',
						width : 150
					}, {
						name : 'Territory',
						key : 'territoryname',
						cellClass : 'text-center',
						width : 150
					}, {
						name : 'Numbering Series',
						key : 'numberingseriesmasterid_name',
						cellClass : 'text-center',
						width : 160
					}, {
						name : '',
						format : 'button',
						buttonname : "Details",
						onClick : "{report.btnOnClick}",
						cellClass : 'text-center',
						restrictToExport : true,
						width : 100
					}];
					profitabilityColumn.splice(0, 0, ...invItemColArr);
				} else if (this.props.resource.groupingby == 'Customer') {
					let customerColArr = [{
						name : 'Grouping Variable',
						key : 'groupingvariable',
						width : 200
					}, {
						name : 'Territory',
						key : 'territoryname',
						cellClass : 'text-center',
						minWidth : 150
					}, {
						name : 'Customer group',
						key : 'customergroupid_name',
						width : 200
					}];
					profitabilityColumn.splice(0, 0, ...customerColArr);
				} else if (this.props.resource.groupingby == 'Item') {
					let itemColArr = [{
						name : 'Grouping Variable',
						key : 'groupingvariable',
						width : 200
					}, {
						name : 'Item Category',
						key : 'itemcategorymasterid_name',
						width : 200
					}, {
						name : 'Item Group',
						key : 'itemgroup_fullname',
						width : 200
					}];
					profitabilityColumn.splice(0, 0, ...itemColArr);
				} else {
					profitabilityColumn.splice(0, 0,{
						name : 'Grouping Variable',
						key : 'groupingvariable',
						width : 200
					});
				}

				profitabilityColumn.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}

					if (item.key == 'profitpercentage')
						item.customfootervalue = currencyFilter(totalGrossPercentage, 'percentage', this.props.app);
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns : profitabilityColumn,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getFooterValue(prop, colarr, aggarr) {
		if(prop == 'profitpercentage') {
			let profitvalue = 0;
			let costvalue = 0;
			colarr.forEach((col, colindex) => {
				if(col.key == 'grossprofit')
					profitvalue = aggarr[colindex].footerAggValue;

				if(col.key == 'totalsalesvalue')
					costvalue = aggarr[colindex].footerAggValue;
			});

			if(costvalue > 0)
				return Number(((profitvalue / costvalue) * 100).toFixed(2));

			return null;
		}
		return null;
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			groupingby: null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	btnOnClick(data) {
		this.props.openModal({
			render: (closeModal) => {
				return <ProfitabilityReceiptDetailsModal data={data} app={this.props.app} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Sales Profitability Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Grouping By</label>
										<Field name={'groupingby'} props={{options:["Sales Order", "Invoice Item", "Invoice", "Sales Person", "Customer", "Item Group", "Item Category", "Territory", "Item"], required: true}} component={localSelectEle}  validate={[stringNewValidation({required: true, model: 'Grouping By'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Sales Profitability Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} btnOnClick={this.btnOnClick} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

class ProfitabilityReceiptDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loaderflag: true
		};
		this.onLoad = this.onLoad.bind(this);
		this.openRefLink = this.openRefLink.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({ loaderflag });
	}

	onLoad() {
		axios.get(`/api/query/salesprofitabilityreportquery?param=Get Receipt Details&invoiceitemid=${this.props.data.id}`).then((response) => {
			if (response.data.message == 'success') {
				this.setState({resultData : response.data.main});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	openRefLink() {
		this.props.history.push(`/details/receiptnotes/${item.receiptnoteid}`);
		this.props.closeModal();
	}

	render() {
		let { resultData } = this.state;
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Receipt Details</h5>
				</div>
				<div className="react-modal-body">
					<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
					{this.state.resultData ? <div className="row">
						{!resultData.keepstock && !resultData.issaleskit ? <div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">This Item does not have "Keep Stock" enabled. Profitability is shown based on the Default Purchase Cost in Item Master {resultData.defaultpurchasecost}</div> : null}
						{(resultData.keepstock || resultData.issaleskit) && resultData.deliveredqty == 0 ? <div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">This item is invoiced, but not yet delivered. Profitability is shown based on the Default Purchase Cost in Item Master {resultData.defaultpurchasecost}</div> : null }
						{(resultData.keepstock || resultData.issaleskit) && resultData.deliveredqty > 0 ? <div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">Cost and Profitability is calculated based on the average cost of all deliveries made against this Sales Order for this Item</div> : null }
						<div className="col-md-12">
							{(resultData.keepstock || resultData.issaleskit) && resultData.deliveredqty > 0 ? <table className="table table-bordered table-hover table-condensed">
								<thead>
									<tr>
										<th className='text-center'>Supplier</th>
										<th className='text-center'>Receipt Note No</th>
										<th className='text-center'>Receipt Note Date</th>
										<th className='text-center'>Valuation Rate</th>
										<th className='text-center'>Quantity</th>
									</tr>
								</thead>
								<tbody>
									{resultData.receiptarray.map((item, index) => {
										return (
											<tr key={index}>
												<td className='text-center'>{item.partnerid_name}</td>
												<td className='text-center'><span onClick={() => this.openRefLink(item)}>{item.receiptnotenumber}</span></td>
												<td className='text-center'>{dateFilter(item.receiptnotedate)}</td>
												<td className='text-center'>{currencyFilter(item.valuationrate, this.props.app.defaultCurrency, this.props.app)}</td>
												<td className='text-center'>{item.quantity}</td>
											</tr>
										);
									})}
								</tbody>
							</table> : null }
						</div>
					</div> : null}
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

SalesProfitabilityReportForm = connect(
	(state, props) => {
		let formName = 'salesprofitabilityreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(SalesProfitabilityReportForm));

export default SalesProfitabilityReportForm;
