import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { InputEle, DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import { currencyFilter, dateFilter} from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import { ChildEditModal } from '../components/utilcomponents';
import ReactPaginate from 'react-paginate';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class BankReconciliationReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openBankModal = this.openBankModal.bind(this);
		this.getBankAccounts = this.getBankAccounts.bind(this);
		this.updateLedger = this.updateLedger.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth((new Date().getMonth() - 1))).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				basedon: 'Voucher Date',
				status : 'Uncleared',
				accountsArray : [],
				resultArray : [],
				bankArray : [],
				filters: {},
				columns : [{
					"name" : "Partner",
					"key" : "partnerid_name",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Cheque No",
					"key" : "instrumentno",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Cheque Date",
					"key" : "instrumentdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Remarks",
					"key" : "remarks",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Debit",
					"key" : "debit",
					"format" : "defaultcurrency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Credit",
					"key" : "credit",
					"format" : "defaultcurrency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Status",
					"format" : "select",
					"selectoptions": ["Cleared", "Bounced"],
					"disabled" : "{item.clearancedate}",
					"onChange" : "{(value) => report.onChangeColumn('tostatus', value, item)}",
					"key" : "tostatus",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Date",
					"format" : "dateinput",
					"disabled" : "{item.clearancedate}",
					"onChange" : "{(value) => report.onChangeColumn('toclear', value, item)}",
					"key" : "toclear",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "",
					"format" : "button",
					"buttonname" : "{item.clearancedate ? 'Edit' : 'Update'}",
					"onClick" : "{() => report.onButtonClick(item)}",
					"disabled": "item.clearancedate ? false : (!item.toclear || !item.tostatus)",
					"cellClass" : "text-center",
					"restrictToExport" : true,
					"width" : 180
				}, {
					"name" : "Voucher Type",
					"key" : "vouchertype",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher No",
					"key" : "voucherno",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher Date",
					"key" : "postingdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Bank",
					"key" : "bankname",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Account",
					"key" : "accountid_name",
					"cellClass" : "text-center",
					"width" : 200
				}]
			}

		this.props.initialize(tempObj);

		setTimeout(() => {
			this.getBankAccounts();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getBankAccounts() {
		this.updateLoaderFlag(true);

		axios.get(`/api/accounts?&field=id,name,displayname&filtercondition=accounts.isledger and accounts.type in ('Asset','Liability') and accounts.accountgroup='Bank'`).then((response) => {
			if (response.data.message == 'success') {
				if(this.props.reportdata) {
					this.getReportData();
				} else {
					this.props.array.removeAll('accountsArray');

					this.props.updateFormState(this.props.form, {
						accountsArray : response.data.main
					});
				}
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	onButtonClick(item) {
		if(item.clearancedate)
			this.openBankModal(item)
		else
			this.updateLedger(item);
	}

	onChangeColumn(key, value, item) {
		item[key] = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		this.refs.grid.refresh();
	}

	getReportData () {
		this.updateLoaderFlag(true);

		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('resultArray');
		this.props.array.removeAll('bankArray');

		let { columns } = this.props.resource;

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];

		if(this.props.resource.fromdate)
			filterString.push(`startdate=${new Date(this.props.resource.fromdate).toDateString()}`);

		if(this.props.resource.todate)
			filterString.push(`enddate=${new Date(this.props.resource.todate).toDateString()}`);

		['status', 'partnerid', 'instrumentno', 'bankid', 'basedon'].forEach((item) => {
			if(this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/bankreconciliationquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				response.data.bankArray.map((item) => {
					item.bankbalance = Number((item.accountbalancedebit - item.accountbalancecredit).toFixed(this.props.app.roundOffPrecision));
					item.ourbalance = Number((item.ourbalancedebit - item.ourbalancecredit).toFixed(this.props.app.roundOffPrecision));
					item.ourbalancewithoutpostdated = Number((item.ourbalancedebitwithoutpost - item.ourbalancecreditwithoutpost).toFixed(this.props.app.roundOffPrecision));
					item.difference = Number((item.bankbalance - item.ourbalance).toFixed(this.props.app.roundOffPrecision));
					item.differencepostdated = Number((item.bankbalance - item.ourbalancewithoutpostdated).toFixed(this.props.app.roundOffPrecision));
				});
				response.data.resultArray.map((item) => {
					item.toclear = item.clearancedate;
					item.tostatus = item.bankstatus;
				});

				this.props.updateFormState(this.props.form, {
					resultArray : response.data.resultArray,
					originalRows : response.data.resultArray,
					bankArray : response.data.bankArray,
					columns
				});

				this.props.updateReportFilter('bankreconciliationreport', this.props.resource);

				if(this.props.resource.bankArray.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	updateLedger(item) {
		this.updateLoaderFlag(true);

		axios.get(`/api/query/bankreconciliationquery?type=update&id=${item.id}&bankstatus=${item.tostatus}&clearancedate=${new Date(item.toclear).toISOString()}&modified=${item.modified}`).then((response) => {
			if (response.data.message == 'success') {
				let resultArray = this.props.resource.resultArray;

				for(let i = 0; i < resultArray.length; i++) {
					if(item.id == resultArray[i].id) {
						resultArray[i].clearancedate = response.data.main.clearancedate;
						resultArray[i].toclear = response.data.main.clearancedate;
						resultArray[i].bankstatus = response.data.main.bankstatus;
						resultArray[i].tostatus = response.data.main.bankstatus;
						resultArray[i].modified = response.data.main.modified;
						break;
					}
				}

				this.props.updateFormState(this.props.form, { resultArray });

				this.getReportData();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	openBankModal(data) {
		let index = -1;
		for(let i=0;i<this.props.resource.resultArray.length;i++) {
			if(data.id == this.props.resource.resultArray[i].id) {
				index = i;
				break;
			}
		}
		if(index == -1)
			return null;
		this.props.openModal({
			render: (closeModal) => {
				return <ChildEditModal form={this.props.form} history={this.props.history} resource={this.props.resource} data={data} index={index} app={this.props.app} updateFormState={this.props.updateFormState} callback = {(item) => this.updateLedger(item)} closeModal={closeModal} getBody={() => {return <BankReconciliationDetailsModal />}} />
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)).setHours(0, 0, 0, 0)),
			todate : new Date(new Date().setHours(0, 0, 0, 0)),
			status : 'Uncleared',
			accountsArray : [],
			resultArray : [],
			bankArray : [],
			filters: {},
			originalRows: null
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Bank Reconciliation Report</div>
						</div>
						<div className="report-header-btnbar">
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12 col-xs-12">
										<label className="labelclass">Based On</label>
										<Field name={'basedon'} props={{options: ["Voucher Date", "Cheque Date"], required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Based On'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12 col-xs-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12 col-xs-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true,title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12 col-xs-12">
										<label className="labelclass">Status</label>
										<Field name={'status'} props={{options: ["Cleared", "Uncleared", "Bounced", "All"], required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Status'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12 col-xs-12">
										<label className="labelclass">Partner</label>
										<Field name={'partnerid'} props={{resource: "partners", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12 col-xs-12">
										<label className="labelclass">Cheque No</label>
										<Field name={'instrumentno'} component={InputEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12 col-xs-12">
										<label className="labelclass">Bank</label>
										<Field name={'bankid'} props={{options: this.props.resource.accountsArray, label: "displayname", valuename: "id", multiselect: true}} component={localSelectEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.resource.fromdate || !this.props.resource.todate || !this.props.resource.status}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<div className="col-md-12 bg-white form-group  marginleft-30">
								{ this.props.resource.bankArray.length > 0 ? <div className="col-md-12 col-xs-12 col-sm-12">
									<div className="row col-md-12 col-xs-12 col-sm-12">
										<div className="col-md-12 col-xs-12 col-sm-12" style={{marginTop:'20px', padding:'0px'}}>
											<table className="table table-bordered">
												<thead>
													<tr>
														<th colSpan="4" className="card-header">Summary Report for Banks
														</th>
													</tr>
													<tr>
														<th className="text-center">Name</th>
														<th className="text-center">Bank Balance</th>
														<th className="text-center">Our Balance</th>
														<th className="text-center">Difference (Bank Balance - Our Balance)</th>
													</tr>
												</thead>
												<tbody>
													{ this.props.resource.bankArray.map((item, index) => {
														return (
															<tr key={index}>
																<td>{item.accountid_name}</td>
																<td className="text-center">{currencyFilter(item.bankbalance, this.props.app.defaultCurrency, this.props.app)}</td>
																<td className="text-center">{currencyFilter(item.ourbalance, this.props.app.defaultCurrency, this.props.app)}</td>
																<td className="text-center">{currencyFilter(item.difference, this.props.app.defaultCurrency, this.props.app)}</td>
															</tr>
														)
													})}
												</tbody>
											</table>
										</div>
									</div>
								</div> : null }
								{this.props.resource ? <Reactuigrid excelname='Bank Reconcilation Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} /> : null }
							</div>
							<ReportPlaceholderComponent reportdata={this.props.resource.resultArray} />
						</div>
					</div>
				</form>
			</>
		);
	};
}

class BankReconciliationDetailsModal extends Component {
	constructor(props) {
		super(props);

		this.state = {};
		this.update = this.update.bind(this);
	}

	componentWillMount() {
		let resultArray = this.props.resource.resultArray;

		resultArray[this.props.index].toclear = resultArray[this.props.index].clearancedate;
		resultArray[this.props.index].tostatus = resultArray[this.props.index].bankstatus;

		this.props.updateFormState(this.props.form, { resultArray });
	}

	update(data) {
		this.props.closeModal();
		this.props.callback(data);
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Edit Clearance Date</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="col-md-12">
							<div className="col-md-12">
								<div className="col-md-3">
									<label className="label-media-sm">Transaction Date</label>
								</div>
								<div className="form-group col-md-8">
									<div className="marginleft-8  margintop-0">
										<Field name={`resultArray[${this.props.index}].toclear`} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}}  component={DateEle}  validate={[dateNewValidation({required:  true, title : 'Transaction Date'})]} />
									</div>
								</div>
							</div>
							<div className="col-md-12">
								<div className="col-md-3">
									<label className="label-media-sm">Status</label>
								</div>
								<div className="form-group col-md-8">
									<div className="marginleft-8  margintop-0">
										<Field
											name={`resultArray[${this.props.index}].tostatus`}
											props={{
												options: ["Cleared", "Bounced", "Uncleared"],
												required: true
											}}
											component={localSelectEle}
											validate={[stringNewValidation({required: true, model: 'Status'})]}/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm gs-btn-info btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
						 		<button type="button" className="btn btn-sm btn-width gs-btn-outline-success" disabled={!this.props.resource.resultArray[this.props.index].toclear || !this.props.resource.resultArray[this.props.index].tostatus} onClick={()=>{this.update(this.props.resource.resultArray[this.props.index])}}><i className="fa fa-save"></i>Update</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

BankReconciliationReportForm = connect(
	(state, props) => {
		let formName = 'bankreconciliationreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(BankReconciliationReportForm));

export default BankReconciliationReportForm;
