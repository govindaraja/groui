import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class SerialnoStatusReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				statusArray : ["Delivered", "Available"],
				status : ["Delivered", "Available"],
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Item Name",
					"key" : "itemid_name",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Warehouse",
					"key" : "warehouseid_name",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Serial No",
					"key" : "serialno",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Status",
					"key" : "status",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "Category",
					"key" : "itemcategorymasterid_name",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Group",
					"key" : "itemgroupid_groupname",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Supplier Name",
					"key" : "supplier",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Receipt Note No",
					"key" : "receiptnotenumber",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Receipt Note Date",
					"key" : "receiptnotedate",
					format : "date",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Purchase Invoice No",
					"key" : "invoiceno",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Purchase Invoice Date",
					"key" : "invoicedate",
					format : "date",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Customer Name",
					"key" : "customer",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Delivery Note No",
					"key" : "deliverynotenumber",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Delivery Note Date",
					"key" : "deliverynotedate",
					format : "date",
					"cellClass" : "text-center",
					"width" : 150
				}]
			};

			customfieldAssign(tempObj.columns, null, 'itemmaster', this.props.app.myResources);

			tempObj.columns.push({
				"name" : "",
				"format" : "button",
				"buttonname" : "Details",
				"onClick" : "{report.btnOnClick}",
				"cellClass" : "text-center",
				"restrictToExport" : true,
				"width" : 100
			});
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let { columns } = this.props.resource;

		let filterString = [];
		['warehouseid', 'itemid', 'fromdate', 'todate', 'itemgroupid', 'itemcategoryid'].map((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		if (this.props.resource.status.length > 0)
			filterString.push(`status=${this.props.resource.status}`);

		axios.get(`/api/query/itemserialnostatusquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];

				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true)
						hiddencols.push(item.key);
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			warehouseid : null,
			fromdate : null,
			todate : null,
			status : [],
			itemid : null,
			itemgroupid : null,
			itemcategoryid : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	btnOnClick(data) {
		this.props.openModal({
			render: (closeModal) => {
				return <SerialHistoryDetailsModal data={data} app={this.props.app} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Serial Number Status</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Warehouse</label>
										<Field name={'warehouseid'} props={{resource: "stocklocations", fields: "id,name", filter: `stocklocations.companyid=${this.props.app.selectedcompanyid} and stocklocations.isparent and stocklocations.parentid is null`}} component={selectAsyncEle} validate={[numberNewValidation({required: true})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Received From</label>
										<Field name={'fromdate'} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Received Till</label>
										<Field name={'todate'} props={{min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({min: '{resource.fromdate}'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Status</label>
										<Field name={'status'} props={{options: this.props.resource.statusArray, multiselect: true}} component={localSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Name</label>
										<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name,displayname", label:"displayname", displaylabel:"name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Group</label>
										<Field name={'itemgroupid'} props={{resource: "itemgroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Serial No Status Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} btnOnClick={this.btnOnClick} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

class SerialHistoryDetailsModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			itemName : this.props.data.itemid_name,
			serialNo : this.props.data.serialno,
			resultArray : []
		};

		this.onLoad = this.onLoad.bind(this);
		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		this.updateLoaderFlag(true);
		let { resultArray } = this.state;

		axios.get(`/api/query/itemserialnohistoryquery?itemserialnoid=${this.props.data.id}&itemid=${this.props.data.itemid}`).then((response) => {
			if (response.data.message == 'success') {
				resultArray = response.data.main;

				resultArray.map((item) => {
					if(item.relatedresource == 'receiptnotes') {
						item.resource = 'Receipt Note';
						item.remarks = item.receiptnoteid_description;
						item.sourceno = item.receiptnotenumber;
					}

					if(item.relatedresource == 'deliverynotes') {
						item.resource = 'Delivery Note';
						item.remarks = item.deliverynoteid_description;
						item.sourceno = item.deliverynotenumber;
					}
				});

				this.setState({ resultArray });
				this.updateLoaderFlag(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	render() {
		let { resultArray, itemName, serialNo } = this.state;
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Serial No History</h5>
				</div>
				<div className="react-modal-body modalmaxheight">
					<div className="row">
						<div className="form-group col-md-3 col-sm-6">
							Item Name : <b className="text-primary">{itemName}</b>
						</div>
						<div className="form-group col-md-3 col-sm-6">
							Serial No : <b className="text-success">{serialNo}</b>
						</div>
						<div className="col-md-12">
							<table className="table table-bordered table-hover table-condensed">
								<thead>
									<tr>
										<th className="text-center">Warehouse</th>
										<th className="text-center">Created On</th>
										<th className="text-center">Status</th>
										<th className="text-center">Batch</th>
										<th className="text-center">Location</th>
										<th className="text-center">Source Type</th>
										<th className="text-center">Source</th>
										<th className="text-center">Remarks</th>
									</tr>
								</thead>
								<tbody>
									{resultArray.map((item, index) => {
										return (
											<tr key={index}>
												<td className='text-center'>{item.warehouseid_name}</td>
												<td className='text-center'>{dateFilter(item.created)}</td>
												<td className='text-center'>{item.status}</td>
												<td className='text-center'>{item.itembatchid_batchnumber}</td>
												<td className='text-center'>{item.stocklocationid_name}</td>
												<td className='text-center'>{item.resource}</td>
												<td className='text-center'>{item.sourceno}</td>
												<td>{item.remarks}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

SerialnoStatusReportForm = connect(
	(state, props) => {
		let formName = 'serialnostatusreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(SerialnoStatusReportForm));

export default SerialnoStatusReportForm;
