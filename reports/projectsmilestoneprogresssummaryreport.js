import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class ProjectMilestoneProgressSummaryReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				asondate : new Date(new Date().setHours(0, 0, 0, 0)),
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Customer",
					"key" : "customerid_name",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Project No",
					"key" : "projectid_projectno",
					"format" : "anchortag",
					"transactionname" : "projects",
					"transactionid" : "projectid",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Project Name",
					"key" : "projectid_projectname",
					"width" : 200
				}, {
					"name" : "Ref No",
					"key" : "boqid_internalrefno",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "BOQ Item Name",
					"key" : "itemid_name",
					"width" : 200
				}, {
					"name" : "BOQ Item Description",
					"key" : "boqid_description",
					"width" : 200
				}, {
					"name" : "BOQ Qty",
					"key" : "boqid_quantity",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "UOM",
					"key" : "uomid_name",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "BOQ Value",
					"key" : "boqid_amount",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}]
			};
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let columns = [...this.props.resource.columns];

		let filterString = [];
		['customerid', 'projectid', 'asondate'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/projectmilestoneprogresssummaryreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [], resultArray = [];
				let itemCount = 0;
				let uniquemilestoneitemObj = {}, uniqueboqObj = {};

				columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
					if(item.stageitem)
						itemCount++;
				});

				for (var i = 0; i < itemCount; i++) {
					for (var j = 0; j < columns.length; j++) {
						if (columns[j].stageitem) {
							columns.splice(j, 1);
							break;
						}
					}
				}

				response.data.main.forEach((item) => {
					item.milestonefinalrate = item.milestonerate;
					if(item.boqid_discountquantity != null && item.boqid_discountquantity != "") {
						if (item.boqid_discountmode == 'Percentage') {
							item.milestonefinalrate = Number((item.milestonerate - (item.milestonerate * (item.boqid_discountquantity / 100))).toFixed(this.props.app.roundOffPrecision));
						} else {
							item.milestonefinalrate = Number((item.milestonerate - Number(((item.boqid_discountquantity) * (item.milestoneitemsid_percentage/100)).toFixed(this.props.app.roundOffPrecision))).toFixed(this.props.app.roundOffPrecision));
						}
					}
					item.tobeinvoicedrate = item.tobeinvoicedqty * item.milestonefinalrate;
				});

				response.data.main.forEach((item) => {
					if(!uniqueboqObj[item.boqid]) {
						uniqueboqObj[item.boqid] = JSON.parse(JSON.stringify(item));

						if (uniqueboqObj[item.boqid].boqid_discountmode == 'Percentage') {
							uniqueboqObj[item.boqid].boqid_finalrate = Number((uniqueboqObj[item.boqid].boqid_rate - (uniqueboqObj[item.boqid].boqid_rate * (uniqueboqObj[item.boqid].boqid_discountquantity / 100))).toFixed(this.props.app.roundOffPrecision));
						} else {
							uniqueboqObj[item.boqid].boqid_finalrate = Number((uniqueboqObj[item.boqid].boqid_rate - uniqueboqObj[item.boqid].boqid_discountquantity).toFixed(this.props.app.roundOffPrecision));
						}

						uniqueboqObj[item.boqid]['boqid_amount'] = uniqueboqObj[item.boqid].boqid_quantity * uniqueboqObj[item.boqid].boqid_finalrate;

						uniqueboqObj[item.boqid]['boqtobeinvoicedamount'] = 0;
					}

					if (!uniquemilestoneitemObj[item.milestoneitemsid])
						uniquemilestoneitemObj[item.milestoneitemsid] = {
							name: item.boqmilestonestatusid_name
						};

					uniqueboqObj[item.boqid][`milestonitem_completed${item.milestoneitemsid}`] = item.completedqty;
					uniqueboqObj[item.boqid][`milestonitem_invoiced${item.milestoneitemsid}`] = item.invoicedqty;
					uniqueboqObj[item.boqid][`milestonitem_tobeinvoicedqty${item.milestoneitemsid}`] = item.tobeinvoicedqty;
					uniqueboqObj[item.boqid]['boqtobeinvoicedamount'] += item.tobeinvoicedrate;
				});

				for(var prop in uniquemilestoneitemObj) {
					columns.push({
						name: `${uniquemilestoneitemObj[prop].name} (Completed Qty)`,
						key: `milestonitem_completed${prop}`,
						cellClass: 'text-center',
						stageitem: true,
						width: 300
					}, {
						name: `${uniquemilestoneitemObj[prop].name} (Invoiced Qty)`,
						key: `milestonitem_invoiced${prop}`,
						cellClass: 'text-center',
						stageitem: true,
						width: 300
					}, {
						name: `${uniquemilestoneitemObj[prop].name} (To be Invoiced Qty)`,
						key: `milestonitem_tobeinvoicedqty${prop}`,
						cellClass: 'text-center',
						stageitem: true,
						width: 300
					});
				}

				columns.push({
					name: `To be Invoiced Amount`,
					key: `boqtobeinvoicedamount`,
					format: "currency",
					footertype: "sum",
					cellClass: "text-right",
					stageitem: true,
					width: 300
				});

				for(var prop in uniqueboqObj) {
					resultArray.push(uniqueboqObj[prop]);
				}

				this.props.updateFormState(this.props.form, {
					originalRows: resultArray,
					hiddencols,
					columns
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			asondate : null,
			customerid : null,
			projectid : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Milestone Progress Summary Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Project Name</label>
										<Field name={'projectid'} props={{resource: "projects", fields: "id,projectname,projectno,displayname", label: "displayname", filter: "projects.status in ('Approved', 'Completed') and projects.ismilestonerequired=true", required: true}} component={autoSelectEle} validate={[numberNewValidation({required:  true, title : 'Project Name'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">As On Date</label>
										<Field name={'asondate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'As On Date'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Milestone Progress Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

ProjectMilestoneProgressSummaryReportForm = connect(
	(state, props) => {
		let formName = 'projectsmilestoneprogresssummaryreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ProjectMilestoneProgressSummaryReportForm));

export default ProjectMilestoneProgressSummaryReportForm;
