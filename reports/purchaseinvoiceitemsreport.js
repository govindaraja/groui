import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import { currencyFilter, booleanfilter, taxFilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../utils/filter';

class PurchaseInvoiceItemsReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	updateMoreFilter() {
		this.setState({showMoreFilter : true});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth((new Date().getMonth() - 1))).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				reportbasedon : 'invoicedate',
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Voucher Type",
					"key" : "vouchertype",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher No",
					"key" : "voucherno",
					"format" : "anchortag",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher Date",
					"key" : "voucherdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Supplier Invoice No",
					"key" : "supplierinvoiceno",
					"format" : "anchortag",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Supplier Invoice Date",
					"key" : "supplierinvoicedate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Supplier Name",
					"key" : "customerid_name",
					"width" : 250
				}, {
					"name" : "Supplier State",
					"key" : "supplierstate",
					"width" : 150
				}, {
					"name" : "Supplier GST Reg Type",
					"key" : "suppliergstregtype",
					"width" : 180
				}, {
					"name" : "Supplier GSTIN",
					"key" : "suppliergstin",
					"width" : 180
				}, {
					"name" : "Item Name",
					"key" : "itemid_name",
					"width" : 200
				}, {
					"name" : "HSN Code",
					"key" : "hsncode",
					"width" : 180
				}, {
					"name" : "Quantity",
					"key" : "quantity",
					"format" : "number",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 180
				}, {
					"name" : "UOM",
					"key" : "uom",
					"width" : 180
				}, {
					"name" : "Currency",
					"key" : "currencyname",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Currency Exchange Rate",
					"key" : "currencyexchangerate",
					"if" : this.props.app.feature.useMultiCurrency,
					"width" : 180
				}, {
					"name" : "Rate",
					"key" : "rate",
					"format" : "currency",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Discount",
					"key" : "discountquantity",
					"format" : "discount",
					"width" : 150
				}, {
					"name" : "Value Before Tax",
					"key" : "amount",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 160
				}, {
					"name" : "Value (Local Currency)",
					"key" : "amountlc",
					"format" : "defaultcurrency",
					"if" : this.props.app.feature.useMultiCurrency,
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 160
				}, {
					"name" : "Value After Tax",
					"key" : "amountwithtax",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 160
				}, {
					"name" : "Description",
					"key" : "description",
					"width" : 300
				}, {
					"name" : "Contact Person",
					"key" : "contactperson",
					"width" : 150
				}, {
					"name" : "Status",
					"key" : "status",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : 'Receipt Note No',
					"key" : 'receiptnotenumber',
					"format" : "anchortag",
					"transactionname" : "receiptnotes",
					"transactionid" : "receiptnoteid",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : 'Receipt Note Date',
					"key" : 'receiptnotedate',
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : 'Work Order No',
					"key" : 'wonumber',
					"format" : "anchortag",
					"transactionname" : "workorders",
					"transactionid" : "woid",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : 'Project',
					"key" : 'projectname',
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Warehouse",
					"key" : "warehouseid_name",
					"width" : 150
				}, {
					"name" : "Item Category",
					"key" : "itemcategorymasterid_name",
					"width" : 180
				}, {
					"name" : "Item Group",
					"key" : "itemgroupid_fullname",
					"width" : 180
				}, {
					"name" : "Commodity Code",
					"key" : "itemid_commoditycode",
					"width" : 180
				}, {
					"name" : "Tariff Code",
					"key" : "itemid_tariffcode",
					"width" : 180
				}, {
					"name" : "Remarks",
					"key" : "remarks",
					"width" : 180
				}, {
					"name" : "Partner group",
					"key" : "customergroupid_name",
					"width" : 180
				}, {
					"name" : "Tax",
					"key" : "taxid",
					"format" : "taxFilter",
					"width" : 180
				}, {
					"name" : "Territory",
					"key" : "territoryname",
					"width" : 180
				}]
			};

			customfieldAssign(tempObj.columns, null, 'purchaseinvoices', this.props.app.myResources, true);
			customfieldAssign(tempObj.columns, null, 'purchaseinvoiceitems', this.props.app.myResources, true);
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['fromdate', 'todate', 'itemid', 'itemcategoryid', 'itemgroupid', 'customergroupid', 'reportbasedon'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});

		axios.get(`/api/query/purchaseinvoiceitemsreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			itemid : null,
			itemgroupid: null,
			itemcategoryid: null,
			customergroupid: null,
			originalRows: null,
			filters: {},
			reportbasedon : null,
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		if(transactionname) {
			this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
		} else {
			if (data.vouchertype == 'Debit Note')
				this.props.history.push(`/details/debitnotes/${data.id}`);
			else
				this.props.history.push(`/details/purchaseinvoices/${data.id}`);
		}
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Purchase Invoice Items Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true,title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Name</label>
										<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Group</label>
										<Field name={'itemgroupid'} props={{resource: "itemgroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Partner Group</label>
										<Field name={'customergroupid'} props={{resource: "customergroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Based On</label>
										<Field name={'reportbasedon'} props={{options: [{value: "invoicedate", label: "Invoice Date"}, {value: "postingdate", label: "Posting Date"}], label: "label", valuename: "value", required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Based On'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Purchase Invoice Items Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

PurchaseInvoiceItemsReportForm = connect(
	(state, props) => {
		let formName = 'purchaseinvoiceitemsreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(PurchaseInvoiceItemsReportForm));

export default PurchaseInvoiceItemsReportForm;
