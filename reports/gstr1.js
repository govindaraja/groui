import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import Loadingcontainer from '../components/loadingcontainer';
import ReportFilter from '../components/reportfiltercomponents';

class GSTR1ReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openLink = this.openLink.bind(this);
		this.downloadCSV = this.downloadCSV.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
				todate: new Date(new Date().getFullYear(), new Date().getMonth()+1, 0)
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		axios.get(`/api/query/gstr1query?fromdate=${this.props.resource.fromdate}&todate=${this.props.resource.todate}`).then((response) => {
			if (response.data.message == 'success') {
				let gstrObj = response.data.main;
				this.props.updateFormState(this.props.form, { gstrObj });
				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(Object.keys(this.props.resource.gstrObj).length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			gstrObj : null
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openLink(data, hsnparam, uqcparam) {
		if(hsnparam) {
			if(data.itemid > 0)
				this.props.history.push(`/details/itemmaster/${data.itemid}`);
			if(data.contracttypeid > 0)
				this.props.history.push(`/details/contracttypes/${data.contracttypeid}`);
			if(data.additionalchargesid > 0)
				this.props.history.push(`/details/additionalchargesmaster/${data.additionalchargesid}`);
			return true;
		}
		if(uqcparam) {
			if(data.uomid > 0)
				this.props.history.push(`/details/uom/${data.uomid}`);
			if(data.contracttypeid > 0)
				this.props.history.push(`/details/contracttypes/${data.contracttypeid}`);
			if(data.additionalchargesid > 0)
				this.props.history.push(`/details/additionalchargesmaster/${data.additionalchargesid}`);
			return true;
		}
		if(data.vouchertype == 'Sales Invoice')
			this.props.history.push(`/details/salesinvoices/${data.voucherid}`);
		if(data.vouchertype == 'Credit Note')
			this.props.history.push(`/details/creditnotes/${data.voucherid}`);
	}

	downloadCSV (param) {
		this.updateLoaderFlag(true);
		let headerArray = [];
		for(var prop in this.props.resource.gstrObj[param].array[0]) {
			if(prop != 'groupingid')
				headerArray.push(prop);
		}

		let allRows = [headerArray];

		for(var i = 0; i< this.props.resource.gstrObj[param].array.length; i++){ 
			let tempArray = [];
			headerArray.map((a) => {
				tempArray.push(this.props.resource.gstrObj[param].array[i][a]);
			});
			allRows.push(tempArray);
		}

		let csvRows = [];

		for(var i=0; i< allRows.length; i++)
			csvRows.push(allRows[i].join(',')+',');

		let csvString = csvRows.join("\n");
		let a = document.createElement('a');
		a.href = 'data:attachment/csv,' + encodeURI(csvString);
		a.target = '_blank';
		a.download = param +'_('+ this.props.resource.fromdate.toLocaleDateString() + ' to ' + this.props.resource.todate.toLocaleDateString() + ')' +'.csv';

		document.body.appendChild(a);
		a.click();
		this.updateLoaderFlag(false);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		let { gstrObj } = this.props.resource;
		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">GSTR-1</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true,title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>								
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className={`btn btn-width btn-sm gs-btn-success ${checkActionVerbAccess(this.props.app, 'gstr1query', 'Read') ? '' : 'hide'}`} disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>

							<div className="row bg-white">
								<div className="col-md-12 form-group paddingleft-45">
									{gstrObj && gstrObj.errors.nogstregtype.length > 0 ? <table className="table table-bordered">
										<thead>
											<tr>
												<th>Invalid GST REG Type</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div>
														{gstrObj.errors.nogstregtype.map((item, index) => {
															return (
																<span key={index} className="badge badge-secondary font-14" onClick={() => this.openLink(item)} style={{margin: '7px'}}>{item.voucherno}</span>
															)
														})}
													</div>
												</td>
											</tr>
										</tbody>
									</table> : null }
									{gstrObj && gstrObj.errors.invalidstate.length > 0 ? <table className="table table-bordered">
										<thead>
											<tr>
												<th>Invalid State</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div>
														{gstrObj.errors.invalidstate.map((item, index) => {
															return (
																<span key={index} className="badge badge-secondary font-14" onClick={() => this.openLink(item)} style={{margin: '7px'}}>{item.voucherno}</span>
															)
														})}
													</div>
												</td>
											</tr>
										</tbody>
									</table> : null }
									{gstrObj && gstrObj.errors.invalidgstin.length > 0 ? <table className="table table-bordered">
										<thead>
											<tr>
												<th>Invalid GST IN</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div>
														{gstrObj.errors.invalidgstin.map((item, index) => {
															return (
																<span key={index} className="badge badge-secondary font-14" onClick={() => this.openLink(item)} style={{margin: '7px'}}>{item.voucherno}</span>
															)
														})}
													</div>
												</td>
											</tr>
										</tbody>
									</table> : null }
									{gstrObj && gstrObj.errors.invalidgstcreditsaleid.length > 0 ? <table className="table table-bordered">
										<thead>
											<tr>
												<th>Invalid GST Credit Sales Details</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div>
														{gstrObj.errors.invalidgstcreditsale.map((item, index) => {
															return (
																<span key={index} className="badge badge-secondary font-14" onClick={() => this.openLink(item)} style={{margin: '7px'}}>{item.voucherno}</span>
															)
														})}
													</div>
												</td>
											</tr>
										</tbody>
									</table> : null }
									{gstrObj && gstrObj.errors.invalidhsncodeid.length > 0 ? <table className="table table-bordered">
										<thead>
											<tr>
												<th>Invalid HSN Code</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div>
														{gstrObj.errors.invalidhsncode.map((item, index) => {
															return (
																<span key={index} className="badge badge-secondary font-14" onClick={() => this.openLink(item, true)} style={{margin: '7px'}}>{item.itemid > 0 ? item.itemid_name : (item.contracttypeid > 0 ? item.contracttypeid_name : item.additionalchargesid_name)}</span>
															)
														})}
													</div>
												</td>
											</tr>
										</tbody>
									</table> : null }
									{gstrObj && gstrObj.errors.invaliduqcid.length > 0 ? <table className="table table-bordered">
										<thead>
											<tr>
												<th>Invalid UQC</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div>
														{gstrObj.errors.invaliduqc.map((item, index) => {
															return (
																<span key={index} className="badge badge-secondary font-14" onClick={() => this.openLink(item, false, true)} style={{margin: '7px'}}>{item.itemid > 0 ? item.uomid_name : (item.contracttypeid > 0 ? item.contracttypeid_name : item.additionalchargesid_name)}</span>
															)
														})}
													</div>
												</td>
											</tr>
										</tbody>
									</table> : null }
								</div>
								<div className="col-md-8 offset-md-2">
									{gstrObj && gstrObj.errors.nogstregtype.length == 0 && gstrObj.errors.invalidstate.length == 0 && gstrObj.errors.invalidgstin.length == 0 && gstrObj.errors.invalidgstcreditsale.length == 0 && gstrObj.errors.invalidhsncode.length == 0 && gstrObj.errors.invaliduqc.length == 0 ? <div className="alert alert-success">
										No Error
									</div> : null }
									{gstrObj ? <div className="alert alert-info">
										{gstrObj.totalSalesinvoices} Sales Invoices Found
										<br></br>
										{gstrObj.totalCreditnotes} Credit Notes Found
									</div> : null }
								</div>

								{gstrObj ? <div className="col-md-8 offset-md-2 margintop-10">
									<table className="table table-bordered">
										<thead>
											<tr>
												<th className="text-center">Title</th>
												<th className="text-center">Voucher Count</th>
												<th className="text-center">Voucher Count</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td className="text-center">B2B Invoices</td>
												<td className="text-center">{gstrObj.b2b.voucherarray.length}</td>
												<td className="text-center"><button type="button" className="btn btn-secondary btn-sm" onClick={() => this.downloadCSV('b2b')} disabled={gstrObj.b2b.array.length == 0}><i className="fa fa-file-excel-o"></i>Download CSV</button></td>
											</tr>
											<tr>
												<td className="text-center">B2C(Large) Invoices</td>
												<td className="text-center">{gstrObj.b2cl.voucherarray.length}</td>
												<td className="text-center"><button type="button" className="btn btn-secondary btn-sm" onClick={() => this.downloadCSV('b2cl')} disabled={gstrObj.b2cl.array.length == 0}><i className="fa fa-file-excel-o"></i>Download CSV</button></td>
											</tr>
											<tr>
												<td className="text-center">B2C(Small) Invoices</td>
												<td className="text-center">{gstrObj.b2cs.voucherarray.length}</td>
												<td className="text-center"><button type="button" className="btn btn-secondary btn-sm" onClick={() => this.downloadCSV('b2cs')} disabled={gstrObj.b2cs.array.length == 0}><i className="fa fa-file-excel-o"></i>Download CSV</button></td>
											</tr>
											<tr>
												<td className="text-center">Credit Notes(Registered)</td>
												<td className="text-center">{gstrObj.cdnr.voucherarray.length}</td>
												<td className="text-center"><button type="button" className="btn btn-secondary btn-sm" onClick={() => this.downloadCSV('cdnr')} disabled={gstrObj.cdnr.array.length == 0}><i className="fa fa-file-excel-o"></i>Download CSV</button></td>
											</tr>
											<tr>
												<td className="text-center">Credit Notes(Unregistered)</td>
												<td className="text-center">{gstrObj.cdnur.voucherarray.length}</td>
												<td className="text-center"><button type="button" className="btn btn-secondary btn-sm" onClick={() => this.downloadCSV('cdnur')} disabled={gstrObj.cdnur.array.length == 0}><i className="fa fa-file-excel-o"></i>Download CSV</button></td>
											</tr>
											<tr>
												<td className="text-center">Exports Invoices</td>
												<td className="text-center">{gstrObj.exp.voucherarray.length}</td>
												<td className="text-center"><button type="button" className="btn btn-secondary btn-sm" onClick={() => this.downloadCSV('exp')} disabled={gstrObj.exp.array.length == 0}><i className="fa fa-file-excel-o"></i>Download CSV</button></td>
											</tr>
											<tr>
												<td className="text-center">HSN Summary</td>
												<td className="text-center">{gstrObj.hsn.voucherarray.length}</td>
												<td className="text-center"><button type="button" className="btn btn-secondary btn-sm" onClick={() => this.downloadCSV('hsn')} disabled={gstrObj.hsn.array.length == 0}><i className="fa fa-file-excel-o"></i>Download CSV</button></td>
											</tr>
										</tbody>
									</table>
								</div> : null}
							</div>
						</div>
					</div>
				</form>
			</>
		);
	};
}

GSTR1ReportForm = connect(
	(state, props) => {
		let formName = 'gstr1report';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(GSTR1ReportForm));

export default GSTR1ReportForm;
