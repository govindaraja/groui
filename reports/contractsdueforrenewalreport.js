import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, checkboxEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class ContractDueForRenewalReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			contractidarray: [],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.createContractEnquiry = this.createContractEnquiry.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				show : 'Pending Contracts',
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "",
					"headerformat" : "checkbox",
					"key" : "ischecked",
					"locked": true,
					"format" : "checkbox",
					"cellClass" : "text-center",
					"onChange" : "{report.checkboxOnChange}",
					"showfield" : "!item.contractenquiryno",
					"restrictToExport" : true,
					"width" : 100
				}, {
					"name" : "Contract No",
					"key" : "contractno",
					"format" : "anchortag",
					"transactionname" : "contracts",
					"transactionid" : "contractid",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Customer Name",
					"key" : "customername",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Type",
					"key" : "contracttype",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Contract Type",
					"key" : "contracttypeid_name",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Expire Date",
					"key" : "expiredate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Renewal Enquiry No",
					"key" : "contractenquiryno",
					"format" : "anchortag",
					"transactionname" : "contractenquiries",
					"transactionid" : "contractenquiryid",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Renewal Contract No",
					"key" : "renewalcontractno",
					"format" : "anchortag",
					"transactionname" : "contracts",
					"transactionid" : "renewalcontractid",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Territory",
					"key" : "territoryname",
					"cellClass" : "text-center",
					"width" : 180
				}]
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}
	
		axios.get(`/api/query/getcontractsdueforrenewalquery?querytype=search&fromdate=${this.props.resource.fromdate}&todate=${this.props.resource.todate}&type=${this.props.resource.type}&show=${this.props.resource.show}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			type : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	checkboxHeaderOnChange(param) {
		let contractidarray = [];
		let filterRows = this.refs.grid.getVisibleRows();
		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if(param && !item.contractenquiryid) {
				item.ischecked = true;
				contractidarray.push(item.contractid);
			}
		});

		this.setState({contractidarray});
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		this.refs.grid.forceRefresh();
	}

	checkboxOnChange(value, item) {
		let contractidarray = [];

		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		setTimeout(() => {
			this.props.resource.originalRows.forEach((rowItem, index) => {
				if(rowItem.ischecked)
					contractidarray.push(rowItem.contractid);
			});
			this.setState({contractidarray});
		}, 0);
		this.refs.grid.refresh();
	}

	createContractEnquiry () {
		this.updateLoaderFlag(true);
		let { contractidarray } = this.state;

		if (contractidarray.length > 15) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Please Choose Only 15 Contracts ",
				btnArray : ["Ok"]
			}));
		}

		axios.get(`/api/query/getcontractsdueforrenewalquery?querytype=create&numberingseriesmasterid=${this.props.resource.numberingseriesmasterid}&contractidarray=${contractidarray.join(",")}`).then((response) => {
			if (response.data.message == 'success') {
				let tempStr = "The following records are created\n\n";
				response.data.main.forEach((item) => {
					tempStr += "Contract Enquiry - " + item.contractenquiryno + "\n";
				});

				this.props.openModal(modalService['infoMethod']({
					header : response.data.message,
					body : tempStr,
					btnArray : ['Ok']
				}));
				if(this.props.reportdata)
					this.getReportData();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Contract Renewal Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							{(this.state.contractidarray.length > 0 && checkActionVerbAccess(this.props.app, 'contractenquiries', 'Save')) ? <div className="report-header-rightpanel">
								<button type="button" className="btn gs-btn-success btn-sm" onClick={this.createContractEnquiry}><i className="fa fa-plus"></i>Contract Enquiry</button>
							</div> : null}
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: ["Pending Contracts", "All Contracts"]}} component={localSelectEle}  validate={[stringNewValidation({required: true, title: 'show'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true,title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Contract Type</label>
										<Field name={'type'} props={{options:["AMC", "Warranty"]}} component={localSelectEle}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Series Type</label>
										<Field name={'numberingseriesmasterid'} props={{resource: "numberingseriesmaster", fields: "id,name,format,isdefault,currentvalue", filter: "numberingseriesmaster.resource='contractenquiries'"}} component={selectAsyncEle} validate={[numberNewValidation({required: true, title : 'Series Type'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Contracts Due For Renewal Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

ContractDueForRenewalReportForm = connect(
	(state, props) => {
		let formName = 'contractdueforrenewalreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ContractDueForRenewalReportForm));

export default ContractDueForRenewalReportForm;
