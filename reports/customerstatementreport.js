import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import XLSX from 'xlsx';
import { saveAs } from 'file-saver';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { search, customfieldAssign, numberNewValidation, dateNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import * as utils from '../utils/utils';
import { currencyFilter, dateFilter, datetimeFilter, timeFilter } from '../utils/filter';
import { AutoSelect, DateTimeElement } from '../components/utilcomponents';
import { excel_sheet_from_array_of_arrays, excel_Workbook, excel_s2ab } from '../utils/excelutils';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import SettlementDetailsModal from '../components/details/settlementdetailsmodal';
import Printmodal from '../components/details/printmodal';
import EmailModal from '../components/details/emailmodal';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import { Newactivityhistorysection } from '../components/details/activitysections';

class CustomerStatementForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			activity: {},
			addActivityShow: false,
			filterToggleOpen: true,
			collectionActivityAccess: utils.checkActionVerbAccess(this.props.app, 'collectionactivities', 'Read'),
			tabactive: {
				statement: '',
				outstanding: '',
				payment: '',
				collectionactivity: '',
				contractenquiry: '',
				contact: '',
				address: ''
			}
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.updateTabActive = this.updateTabActive.bind(this);
		this.inputonChange = this.inputonChange.bind(this);
		this.dateInputonChange = this.dateInputonChange.bind(this);
		this.renderOutstanding = this.renderOutstanding.bind(this);
		this.renderPayment = this.renderPayment.bind(this);
		this.renderContact = this.renderContact.bind(this);
		this.renderAddress = this.renderAddress.bind(this);
		this.getCollectionActivities = this.getCollectionActivities.bind(this);
		this.getCustomerDetails = this.getCustomerDetails.bind(this);
		this.getCustomerStatements = this.getCustomerStatements.bind(this);
		this.addCollectionActivity = this.addCollectionActivity.bind(this);
		this.openLink = this.openLink.bind(this);
		this.openSettlementDetails = this.openSettlementDetails.bind(this);
		this.getContactDetails = this.getContactDetails.bind(this);
		this.getAddressDetails = this.getAddressDetails.bind(this);
		this.printfunction = this.printfunction.bind(this);
		this.emailFn = this.emailFn.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.exportExcel = this.exportExcel.bind(this);
		this.createActivity = this.createActivity.bind(this);
		this.openActivity = this.openActivity.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata && !this.props.location.params)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				fromdate : (this.props.location.params && this.props.location.params.fromdate) 
? this.props.location.params.fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 3)).setHours(0, 0, 0, 0)),
				todate : (this.props.location.params && this.props.location.params.todate) 
? this.props.location.params.todate : new Date(new Date().setHours(0, 0, 0, 0)),
				companyid : (this.props.location.params && this.props.location.params.companyid) 
? this.props.location.params.companyid : this.props.app.selectedcompanyid,
				partnerid : (this.props.location.params && this.props.location.params.partnerid) 
? this.props.location.params.partnerid : "",
				outstandings: [],
				filters: {},
				paymentperformance: [],
				collectionActivity: [],
				activityhistorydetails : [],
				contacts: [],
				addresses: [],
				columns: [{
					"name" : "Sl.No",
					"key" : "index",
					"cellClass" : "text-center",
					"restrictToExport" : true,
					"width" : 130
				}, {
					"name" : "Date",
					"key" : "postingdate",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher Type",
					"key" : "vouchertype",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Voucher No",
					"key" : "displayvoucherno",
					"format" : "anchortag",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Debit",
					"key" : "debit",
					"format" : "defaultcurrency",
					"footertype" : "custom",
					"cellClass" : "text-right",
					"width" : 250
				}, {
					"name" : "Credit",
					"key" : "credit",
					"format" : "defaultcurrency",
					"footertype" : "custom",
					"cellClass" : "text-right",
					"width" : 250
				}, {
					"name" : "Debit in FC",
					"key" : "debitfc",
					"format" : "NumberFormatter",
					"footertype" : "custom",
					"cellClass" : "text-right",
					"if" : this.props.app.feature.useMultiCurrency,
					"width" : 280
				}, {
					"name" : "Credit in FC",
					"key" : "creditfc",
					"format" : "NumberFormatter",
					"footertype" : "custom",
					"cellClass" : "text-right",
					"if" : this.props.app.feature.useMultiCurrency,
					"width" : 280
				}, {
					"name" : "Currency",
					"key" : "currencyid_name",
					"cellClass" : "text-center",
					"if" : this.props.app.feature.useMultiCurrency,
					"width" : 180
				}, {
					"name" : "Currency Exchange Rate",
					"key" : "currencyexchangerate",
					"cellClass" : "text-center",
					"if" : this.props.app.feature.useMultiCurrency,
					"width" : 180
				}, {
					"name" : "Remarks",
					"key" : "remarks",
					"width" : 200
				}]
			};
		}

		this.props.initialize(tempObj);

		setTimeout(() => {
			if(this.props.reportdata || this.props.location.params)
				this.getCustomerDetails();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getCustomerDetails = () => {
		this.updateLoaderFlag(true);
		
		axios.get(`/api/partners/${this.props.resource.partnerid}`).then((response) => {
			if (response.data.message == 'success') {
				let tempObj = {
					customerid : response.data.main.id,
					customerid_address : response.data.main.addressid_displayaddress,
					customerid_name : response.data.main.name,
					customerid_legalname : response.data.main.legalname,
					customerid_collectionrep : response.data.main.collectionrep_displayname,
					customerid_email : response.data.main.contactid_email,
					contacts : response.data.main.contacts,
					addresses : response.data.main.addresses,
					collectionActivity : []
				};
				this.props.updateFormState(this.props.form, tempObj);
				
				setTimeout(() => {
					this.getCustomerStatements();
				}, 0);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getCustomerStatements = () => {
		this.updateLoaderFlag(true);

		let filterString = [];
		['companyid', 'fromdate', 'todate', 'partnerid'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});

		axios.get(`/api/query/customerstatementsquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let tempObj = {
					originalRows : response.data.main.resultArray,
					statementTotalCredit : 0,
					statementTotalDebit : 0,
					statementTotalCreditFC : 0,
					statementTotalDebitFC : 0
				};

				for(i = 0; i < tempObj.originalRows.length; i++) {
					if(tempObj.originalRows[i].postingdate=='OPENING BALANCE' || tempObj.originalRows[i].postingdate=='CLOSING BALANCE') {
						tempObj.originalRows[i].postingdate = tempObj.originalRows[i].postingdate;
					} else {
						tempObj.originalRows[i].postingdate = dateFilter(tempObj.originalRows[i].postingdate);
						tempObj.statementTotalCredit += (tempObj.originalRows[i].credit ? tempObj.originalRows[i].credit : 0);
						tempObj.statementTotalDebit += (tempObj.originalRows[i].debit ? tempObj.originalRows[i].debit : 0);
						tempObj.statementTotalCreditFC += (tempObj.originalRows[i].creditfc ? tempObj.originalRows[i].creditfc : 0);
						tempObj.statementTotalDebitFC += (tempObj.originalRows[i].debitfc ? tempObj.originalRows[i].debitfc : 0);
					}
				}
				tempObj.outstandings = response.data.main.result;
				tempObj.paymentperformance = response.data.main.paymentperformance;
				
				tempObj.originalRows.forEach((item, index) => {
					item.index = index + 1;
				});
				let totalOutCredit = 0;
				let totalOutDebit = 0;
				for(var i = 0; i < tempObj.outstandings.length; i++) {
					if(tempObj.outstandings[i].balcredit > 0)
						totalOutCredit += tempObj.outstandings[i].balcredit;
					if(tempObj.outstandings[i].baldebit > 0)
						totalOutDebit += tempObj.outstandings[i].baldebit;
				}
				if(totalOutCredit - totalOutDebit > 0) {
					tempObj.toalOutstanding = totalOutCredit - totalOutDebit;
					tempObj.toalOutstandingRef = "Cr";
				} else {
					tempObj.toalOutstanding = totalOutDebit - totalOutCredit;
					tempObj.toalOutstandingRef = "Dr";
				}

				this.props.updateFormState(this.props.form, tempObj);

				this.props.resource.columns.forEach((item) => {
					if (item.key == 'debit')
						item.customfootervalue = `Total Debit : ${currencyFilter(this.props.resource.statementTotalDebit, this.props.resource.currencyid, this.props.app)}`;
					if (item.key == 'credit')
						item.customfootervalue = `Total Credit : ${currencyFilter(this.props.resource.statementTotalCredit, this.props.resource.currencyid, this.props.app)}`;
					if (item.key == 'debitfc')
						item.customfootervalue = `Total Debit in FC : ${currencyFilter(this.props.resource.statementTotalDebitFC, 'percentage', this.props.app)}`;
					if (item.key == 'creditfc')
						item.customfootervalue = `Total Credit in FC : ${currencyFilter(this.props.resource.statementTotalCreditFC, 'percentage', this.props.app)}`;
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
				this.getCollectionActivities();
				this.updateTabActive('statement');
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	};

	getCollectionActivities = (param) => {
		if(!this.state.collectionActivityAccess)
			return null;

		this.updateLoaderFlag(true);
		let tempObj = {}, pagelengthString = '';
		if (param == 'reload')
			tempObj.collectionActivity = [];
		else
			tempObj.collectionActivity = this.props.resource.collectionActivity;

		if(!param)
			pagelengthString = `&pagelength=3`;

		tempObj.activityhistorydetails = [];

		axios.get(`/api/salesactivities?field=id,created,modified,description,activitytypeid,activitytypes/name/activitytypeid,duedate,plannedstarttime,plannedendtime,startdatetime,enddatetime,status,createdby,users/displayname/createdby,assignedto,users/displayname/assignedto,checkindetails,checkoutdetails&filtercondition=activitytypes_activitytypeid.category = 'Payment Followup' and (salesactivities.parentresource='partners' and salesactivities.parentid=${this.props.resource.customerid})${pagelengthString}`).then((response) => {
			if (response.data.message == 'success') {
				for(var i = 0; i < response.data.main.length; i++)
					tempObj.collectionActivity.push(response.data.main[i]);

				tempObj.activityhistorydetails = tempObj.collectionActivity;

				tempObj.showactivity = (tempObj.collectionActivity.length < response.data.count) ? true : false;
				this.props.updateFormState(this.props.form, tempObj);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	createActivity() {
		let tempObj = {
			parentresource:  'partners',
			parentid: this.props.resource.customerid,
			partnerid: this.props.resource.customerid,
			tempactivitycategory : 'Payment Followup',
			assignedto: this.props.app.user.issalesperson ? this.props.app.user.id: null
		};
	
		this.props.createOrEdit('/createActivity', null, tempObj, (responseobj) => {
			this.getCollectionActivities('reload');
		});
	}

	openActivity = function(activity) {
		this.props.createOrEdit('/details/activities/:id', activity.id, {}, (responseobj) => {
			this.getCollectionActivities('reload');
		});
	}

	addCollectionActivity = () => {
		let { activity } = this.state;

		activity.parentresource = 'partners';
		activity.parentid = this.props.resource.customerid;

		axios({
			method : 'POST',
			data : {
				actionverb : 'Save',
				data : activity
			},
			url : '/api/collectionactivities'
		}).then((response) => {
			if (response.data.message == 'success') {
				this.setState({activity: {}, addActivityShow: false}, () => {
					this.getCollectionActivities('reload');
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	};

	updateTabActive = (param) => {
		let { tabactive } = this.state;
		for(var prop in tabactive) {
			tabactive[prop] = prop == param ? 'active' : '';
		}
		this.setState({ tabactive });
	}

	dateInputonChange = (value, name) => {
		let { activity } = this.state;
		activity[name] = value;
		this.setState({ activity });
	}

	inputonChange = (evt) => {
		let { activity } = this.state;
		activity[evt.target.name] = evt.target.value;
		this.setState({ activity });
	}

	cancel = () => {
		this.props.history.goBack();
	};

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	renderOutstanding = () => {
		return this.props.resource.outstandings.map((outstandingItem, outstandingIndex) => {
			return (
				<tr key={outstandingIndex}>
					<td className="text-center">{dateFilter(outstandingItem.postingdate)}</td>
					<td className="text-center">{outstandingItem.vouchertype}</td>
					<td className="text-center"><span className="gs-anchor" onClick={() => this.openLink(outstandingItem)}>{outstandingItem.displayvoucherno}</span></td>
					<td className="text-right">{currencyFilter(outstandingItem.voucheramount, this.props.app.defaultcurrency, this.props.app)} {outstandingItem.voucherref}</td>
					<td className="text-right">{currencyFilter(outstandingItem.balanceamount, this.props.app.defaultcurrency, this.props.app)} {outstandingItem.balanceref}</td>
					<td className="text-center">{dateFilter(outstandingItem.duedate)}</td>
					<td className="text-center">{outstandingItem.overduebydays}</td>
					<td className="text-center">{outstandingItem.ageofvoucher}</td>
					<td className="text-center" style={{width:'150px',wordWrap: 'break-word',wordBreak: 'break-all'}}>{outstandingItem.remarks}</td>
					<td className="text-center">
						<button type="button" className="btn btn-secondary btn-sm" onClick={() => this.openSettlementDetails(outstandingItem.relatedresource, outstandingItem.relatedid, outstandingItem.accountid)} title="Settlement Details"><span className="fa fa-list-alt"></span></button>
					</td>
				</tr>
			);
		});
	}

	renderPayment = () => {
		return this.props.resource.paymentperformance.map((paymentItem, paymentIndex) => {
			return (
				<tr key={paymentIndex}>
					<td className="text-center">{dateFilter(paymentItem.postingdate)}</td>
					<td className="text-center">{paymentItem.vouchertype}</td>
					<td className="text-center"><div className="gs-anchor" onClick={() => this.openLink(paymentItem)}>{paymentItem.displayvoucherno}</div></td>
					<td className="text-right">{currencyFilter(paymentItem.voucheramount, paymentItem.currencyid, this.props.app)} {paymentItem.voucherref}</td>
					<td className="text-center">{dateFilter(paymentItem.settlementdate)}</td>
					<td className="text-center">{paymentItem.days}</td>
					<td className="text-center">{dateFilter(paymentItem.duedate)}</td>
					<td className="text-center">{paymentItem.daysafterdue}</td>
					<td className="text-center">
						<button type="button" className="btn btn-secondary btn-sm" onClick={() => this.openSettlementDetails(paymentItem.relatedresource, paymentItem.relatedid, paymentItem.accountid)} rel="tooltip" title="Settlement Details"><span className="fa fa-list-alt"></span></button>
					</td>
				</tr>
			);
		});
	}

	openLink = (item) => {
		let link = '';
		if(item.vouchertype == 'Sales Invoice')
			link = '/details/salesinvoices/'+ item.relatedid;
		if(item.vouchertype == 'Purchase Invoice')
			link = '/details/purchaseinvoices/'+ item.relatedid;
		if(item.vouchertype == 'Receipt')
			link = '/details/receiptvouchers/'+ item.relatedid;
		if(item.vouchertype == 'Payment')
			link = '/details/paymentvouchers/'+ item.relatedid;
		if(item.vouchertype == 'Journal Voucher')
			link = '/details/journalvouchers/'+ item.relatedid;
		if(item.vouchertype == 'Credit Note')
			link = '/details/creditnotes/'+ item.relatedid;
		if(item.vouchertype == 'Debit Note')
			link = '/details/debitnotes/'+ item.relatedid;
		this.props.history.push(link);
	};

	openSettlementDetails = (relatedresource, relatedid, accountid) => {
		let itemObj = {
			relatedid: relatedid,
			partnerid : this.props.resource.customerid,
			accountid : accountid,
			partnerid_name: this.props.resource.customerid_name
		};
		this.props.openModal({
			render: (closeModal) => {
				return <SettlementDetailsModal history={this.props.history} item={itemObj} relatedresource={relatedresource} app={this.props.app} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	};

	renderContact = () => {
		return this.props.resource.contacts.map((contact, index) => {
			return (
				<div key={index} className="col-md-4">
					<div className="gs-card card-body">
						{contact.name ? <div>
							<label><b>Name&nbsp;:&nbsp;</b></label>
							<span>{contact.name}</span>
						</div> : null }
						{contact.designation ? <div> 
							<label><b>Designation&nbsp;:&nbsp;</b></label>
							<span>{contact.designation}</span>
						</div> : null }
						{contact.mobile ? <div>
							<label><b>Mobile&nbsp;:&nbsp;</b></label>
							<span>{contact.mobile}</span>&nbsp;&nbsp;&nbsp;
						</div> : null }
						{contact.phone ? <div>
							<label><b>Phone&nbsp;:&nbsp;</b></label>
							<span>{contact.phone}</span>
						</div> : null }
						{contact.email ? <div>
							<label><b>E-mail&nbsp;:&nbsp;</b></label>
							<span>{contact.email}</span>
						</div> : null }
						<div className="d-flex justify-content-between align-items-start">
							{contact.isprimary ? <span className="badge gs-badge-success">Primary</span> : null}
							{
								(utils.checkActionVerbAccess(this.props.app, 'contacts', 'Menu')) ? <button type="button" className="btn btn-sm btn-secondary float-right" onClick={() => this.getContactDetails(contact.id)}>Details</button> : null
							}
						</div>
					</div>
				</div>
			);
		});
	};

	openTransaction(data) {
		if (data.relatedresource == 'salesinvoices')
			this.props.history.push(`/details/salesinvoices/${data.relatedid}`);
		else if (data.relatedresource == 'purchaseinvoices')
			this.props.history.push(`/details/purchaseinvoices/${data.relatedid}`);
		if (data.relatedresource == 'journalvouchers') {
			if (data.vouchertype == "Payment")
				this.props.history.push(`/details/paymentvouchers/${data.relatedid}`);
			else if (data.vouchertype == "Receipt")
				this.props.history.push(`/details/receiptvouchers/${data.relatedid}`);
			else if (data.vouchertype == "Credit Note")
				this.props.history.push(`/details/creditnotes/${data.relatedid}`);
			else if (data.vouchertype == "Debit Note")
				this.props.history.push(`/details/debitnotes/${data.relatedid}`);
			else if(data.vouchertype == "Journal Voucher")
				this.props.history.push(`/details/journalvouchers/${data.relatedid}`);
		}
	}

	renderAddress = () => {
		return this.props.resource.addresses.map((address, index) => {
			return (
				<div key={index} className="col-md-4">
					<div className="gs-card card-body">
						{address.displayaddress ? <h6 className="list-group-item-heading">{address.title}</h6> : null }
						{address.displayaddress ? <span className="list-group-item-text">{address.displayaddress}</span> : null }
						<div className="text-right" style={{bottom: 4}}>
						{
							(utils.checkActionVerbAccess(this.props.app, 'addresses', 'Menu')) ? <button type="button" className="btn btn-sm btn-secondary" onClick={() => this.getAddressDetails(address.id)}>Details</button> : null
						}
						</div>
					</div>
				</div>
			);
		});
	};

	getContactDetails = (id) => {
		let tempObj = {
			parentresource : 'partners',
			parentid : this.props.resource.id
		};
		this.props.history.push({pathname: `/details/contacts/${id}`, params: tempObj});
	};

	getAddressDetails = (id) => {
		this.props.history.push(`/details/address/${id}`);
	};

	printfunction = () => {
		this.props.openModal({
			render: (closeModal) => {
				return <Printmodal app={this.props.app} resourcename={'customerstatements'} companyid={this.props.resource.companyid} resource={this.props.resource} openModal={this.props.openModal} closeModal={closeModal}/>
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	openEmail = () => {
		this.props.openModal({
			render: (closeModal) => {
				return <EmailModal parentresource={this.props.resource} app={this.props.app} resourcename={'customerstatements'} activityemail={false} callback={this.emailFn} openModal={this.props.openModal} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}, confirmModal: true
		});
	};

	emailFn = (emailObj, callback) => {
		emailObj.partnerid = this.props.resource.partnerid;
		emailObj.companyid = this.props.resource.companyid;
		emailObj.fromdate = this.props.resource.fromdate;
		emailObj.todate = this.props.resource.todate;

		let extradocuments = emailObj.extradocuments;

		axios({
			method : 'post',
			data : {
				actionverb : 'Send Email',
				data : emailObj
			},
			url : '/api/customerstatements'
		}).then((response) => {
			if (response.data.message == 'success') {
				if (emailObj.firsttime) {
					if (emailObj.sendemail) {
						console.log(this);
						console.log('aaa');
						emailObj = response.data.main;
						emailObj.toaddress = this.props.resource.customerid_email;
						emailObj.bcc = response.data.main.bcc;
						emailObj.cc = response.data.main.cc ? (this.props.app.user.email+","+response.data.main.cc) : this.props.app.user.email;
						emailObj.firsttime = false;
						emailObj.extradocuments = extradocuments;
						callback(emailObj, false);						
					} else {
						callback(null, true);
						let apiResponse = commonMethods.apiResult(response);
						this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
					}
				} else {
					callback(null, true);
					let apiResponse = commonMethods.apiResult(response);
					this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
				}
			} else {
				callback(null, true);
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	resetFilter = () => {
		let tempObj = {
			fromdate : null,
			todate : null,
			companyid : this.props.app.selectedcompanyid,
			partnerid : "",
			customerid : "",
			outstandings: [],
			filters: {},
			originalRows: null,
			paymentperformance: [],
			collectionActivity: [],
			contacts: [],
			addresses: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}
	
	exportExcel() {
		let reportData = [
			[{v : ''}, {v : 'Report Name'}, {v : 'Customer Statement Report'}],
			[],
			[{v : ''}, {v : 'Company Name'}, {v : this.props.app.selectedCompanyDetails.legalname}], 
			[], 
			[]
		],
		child_reportData = {},
		widthArray = [],
		child_widthArray = {},
		needFooter = false,
		colDefs = this.props.resource.columns,
		child_colDefs = {
			outstandings : [{
					"name" : "Voucher Date",
					"key" : "postingdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "Voucher Type",
					"key" : "vouchertype",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher No",
					"key" : "displayvoucherno",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher Amount",
					"key" : "voucheramount",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Amount outstanding",
					"key" : "balanceamount",
					"cellClass" : "text-right",
					"footertype" : "custom",
					"width" : 180
				}, {
					"name" : "Due Date",
					"key" : "duedate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Over Due Days",
					"key" : "overduebydays",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Age of Vocuher",
					"key" : "ageofvoucher",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Remarks",
					"key" : "remarks",
					"cellClass" : "text-center",
					"width" : 200
				}
			],
			paymentperformance : [{
					"name" : "Voucher Date",
					"key" : "postingdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "Voucher Type",
					"key" : "vouchertype",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher No",
					"key" : "displayvoucherno",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher Amount",
					"key" : "voucheramount",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Cleared On",
					"key" : "settlementdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Days",
					"key" : "days",
					"cellClass" : "text-center",
					"width" : 50
				}, {
					"name" : "Due On",
					"key" : "duedate",
					"cellClass" : "text-center",
					"format" : "date",
					"width" : 150
				}, {
					"name" : "Days from Due",
					"key" : "daysafterdue",
					"cellClass" : "text-center",
					"width" : 150
				}
			]
		},
		reportName = 'Customer Statement Report',
		ChildSheetsObj = {
			outstandings : "Customer Outstanding",
			paymentperformance : "Payment Performance"
		},
		dataArray = search(this.props.resource.originalRows || [], this.props.resource.filterObj),
		child_dataArray = this.props.resource;

		reportData[4].push({
			v : 'Sl. No'
		});

		widthArray.push({
			wch : 6
		});

		for (let i = 0; i < colDefs.length; i++) {
			if (!colDefs[i].restrictToExport && colDefs[i].hidden != true) {
				reportData[4].push({
					v : colDefs[i].name
				});
				widthArray.push({
					wch : colDefs[i].width ? colDefs[i].width / 10 : 10
				});
				if (['sum', 'custom'].indexOf(colDefs[i].footertype) >= 0) {
					needFooter = true;
				}
			}
		}

		for (let i = 0; i < dataArray.length; i++) {
			let tempData = [];
			tempData.push({
				v : i + 1
			});

			for (let j = 0; j < colDefs.length; j++) {
				if (!colDefs[j].restrictToExport && colDefs[j].hidden != true) {
					let value = '',type = '';

					if ((colDefs[j].format == 'date' || colDefs[j].format == 'dateinput') && dataArray[i][colDefs[j].key]) {
						let tempdate = new Date(dataArray[i][colDefs[j].key]);
						value = new Date(tempdate.setTime(tempdate.getTime()+((330/60)*3600*1000))).toString();
						type='d';
					} else
						value = dataArray[i][colDefs[j].key] || '';

					tempData.push({
						v : value,
						type : type
					});
				}
			}
			reportData.push(tempData);
		}

		if (Object.keys(child_colDefs).length > 0) {
			for (let prop in child_colDefs) {
				child_reportData[prop] = [[]];
				child_widthArray[prop] = [];

				for (let i = 0; i < child_colDefs[prop].length; i++) {
					child_reportData[prop][0].push({
						v : child_colDefs[prop][i].name
					});

					child_widthArray[prop].push({
						wch : child_colDefs[prop][i].width ? child_colDefs[prop][i].width / 10 : 10
					});
				}

				if (child_dataArray[prop] && child_dataArray[prop].length > 0) {
					for (let i = 0; i < child_dataArray[prop].length; i++) {
						let tempData = [];
						
						for (let j = 0; j < child_colDefs[prop].length; j++) {
							if (!child_colDefs[prop][j].restrictToExport && child_colDefs[prop][j].hidden != true) {
								let value = '',type = '';

								if ((child_colDefs[prop][j].format == 'date' || child_colDefs[prop][j].format == 'dateinput') && child_dataArray[prop][i][child_colDefs[prop][j].key]) {
									let tempdate = new Date(child_dataArray[prop][i][child_colDefs[prop][j].key]);
									value = new Date(tempdate.setTime(tempdate.getTime()+((330/60)*3600*1000))).toString();
									type='d';
								} else
									value = child_dataArray[prop][i][child_colDefs[prop][j].key] || '';

								tempData.push({
									v : value,
									type : type
								});
							}
						}
						child_reportData[prop].push(tempData);
					}
				}
			}
			needFooter = true;
		}

		if (needFooter) {
			let tempArray = [], tempChildArray = [];

			tempArray.push({
				v : ''
			});

			tempChildArray.push({
				v : ''
			});

			for (let i = 0; i < colDefs.length; i++) {
				if (!colDefs[i].restrictToExport && colDefs[i].hidden != true) {
					if (['sum', 'custom'].indexOf(colDefs[i].footertype) >= 0) {
						tempArray.push({
							v : colDefs[i].footertype == 'sum' ? Number(colDefs[i].custom_isSumAggregaion_Total.toFixed(this.props.app.roundOffPrecision)) : (!isNaN(colDefs[i].customfootervalue) ? Number(colDefs[i].customfootervalue) : colDefs[i].customfootervalue)
						});
					} else {
						tempArray.push({
							v : '',
						});
					}
				}
			}
			tempArray[1] = {
				v : 'Total'
			};
			reportData.push(tempArray);

			for (let prop in child_colDefs) {
				if (prop == 'outstandings') {
					for (let i=0; i<child_reportData[prop].length; i++) {
						tempChildArray.push({
							v : '',
						});
					}

					if (tempChildArray.length > 4)
						tempChildArray[4] = {
							v : `Total Outstanding : ${currencyFilter(this.props.resource.toalOutstanding, this.props.resource.currencyid, this.props.app)} ${this.props.resource.toalOutstandingRef}`
						}

					child_reportData[prop].push(tempChildArray);
				}
			}
		}

		let wb = {
			SheetNames : [],
			Sheets : {}
		};

		let ws = excel_sheet_from_array_of_arrays(reportData);
		wb.SheetNames.push(reportName);
		wb.Sheets[reportName] = ws;
		ws['!cols'] = widthArray;

		for (let prop in child_reportData) {
			let ws = excel_sheet_from_array_of_arrays(child_reportData[prop]);
			wb.SheetNames.push(ChildSheetsObj[prop]);
			wb.Sheets[ChildSheetsObj[prop]] = ws;
			ws['!cols'] = child_widthArray[prop];
		}

		let wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
		saveAs(new Blob([excel_s2ab(wbout)],{type:"application/octet-stream"}), reportName + "_" + (dateFilter(new Date())) + ".xlsx");
	}

	render() {
		if(!this.props.resource)
			return null;

		let { resource } = this.props;
		let { tabactive } = this.state;

		let customerid = this.props.match.params.id;
		let serviceCallArray = [];
		if(resource.equipmentid > 0 && resource.serviceCallArray.length > 0)
			serviceCallArray = resource.serviceCallArray.filter((item) => {
				return item.status != 'Completed';
			});

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Partner Account Details</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
					</div>
					<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
						<div className="row marginbottom-75">
							<div className="form-group col-md-12 col-sm-12">
								<label className="labelclass">Partner</label>
								<Field name={'partnerid'} props={{resource: "partners", fields: "id,name", required: true}} component={autoSelectEle} validate={[numberNewValidation({required:  true, title : 'Partner'})]} />
							</div>
							<div className="form-group col-md-12 col-sm-12">
								<label className="labelclass">From Date</label>
								<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
							</div>
							<div className="form-group col-md-12 col-sm-12">
								<label className="labelclass">To Date</label>
								<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
							</div>
						</div>
						<div className="reportfilter-search">
							<div className="form-group col-md-12 col-sm-12">
								<button type="button" onClick={() => {
									this.getCustomerDetails()
								}} className="btn btn-width btn-sm gs-btn-success margintop-25" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
								<button type="button" onClick={() => {
									this.resetFilter()
								}} className="btn btn-width btn-sm gs-btn-info margintop-25"><i className="fa fa-refresh"></i>Reset</button>
							</div>
						</div>
					</ReportFilter>
					<div className="row">
						{resource.customerid > 0 ? <div className="col-md-12 bg-white form-group paddingleft-50">
							<div className='row margintop-25'>
								<div className="col-md-12 col-sm-12 col-xs-12">
									<ul className="nav nav-tabs">
										<li className="nav-item">
											<span className={`nav-link gs-anchor-no-hover ${tabactive.statement}`} onClick={()=>{this.updateTabActive('statement')}}>Statement</span>
										</li>
										<li className="nav-item">
											<span className={`nav-link gs-anchor-no-hover ${tabactive.outstanding}`} onClick={()=>{this.updateTabActive('outstanding')}}>Outstandings</span>
										</li>
										<li className="nav-item">
											<span className={`nav-link gs-anchor-no-hover ${tabactive.payment}`} onClick={()=>{this.updateTabActive('payment')}}>Payment Performance</span>
										</li>
										{this.state.collectionActivityAccess ? <li className="nav-item">
											<span className={`nav-link gs-anchor-no-hover ${tabactive.collectionactivity}`} onClick={()=>{this.updateTabActive('collectionactivity')}}>Collection Activities</span>
										</li> : null}
										<li className="nav-item">
											<span className={`nav-link gs-anchor-no-hover ${tabactive.contact}`} onClick={()=>{this.updateTabActive('contact')}}>Contacts</span>
										</li>
										<li className="nav-item">
											<span className={`nav-link gs-anchor-no-hover ${tabactive.address}`} onClick={()=>{this.updateTabActive('address')}}>Addresses</span>
										</li>
									</ul>

									<div className="tab-content">
										<div className={`tab-pane fade ${tabactive.statement ? 'show active': ''}`}>
											<div className="row margintop-25">
												<div className="col-md-12 col-sm-12 col-xs-12">
													{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Customer Statement Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : <div className="alert alert-info">No Data found!!!!</div> }
												</div>
											</div>
										</div>
										<div className={`tab-pane fade ${tabactive.outstanding ? 'show active': ''}`}>
											{resource.outstandings.length>0 ? <div className='row margintop-25'>
												<div className="col-md-12 col-sm-12 col-xs-12 table-responsive">
													<table className="table table-bordered table-sm">
														<thead>
															<tr>
																<th className="text-center">Voucher Date</th>
																<th className="text-center">Voucher Type</th>
																<th className="text-center">Voucher No</th>
																<th className="text-center">Voucher Amount</th>
																<th className="text-center">Amount outstanding</th>
																<th className="text-center">Due Date</th>
																<th className="text-center">Over Due Days</th>
																<th className="text-center">Age of Voucher</th>
																<th className="text-center">Remarks</th>
																<th className="text-center"></th>
															</tr>
														</thead>
														<tbody>
															{this.renderOutstanding()}
														</tbody>
														<tfoot>
															<tr>
																<td className="text-right" colSpan="4">Total Outstanding</td>
																<td className="text-right">
																	{currencyFilter(resource.toalOutstanding, null, this.props.app)} {resource.toalOutstandingRef}
																</td>
																<td className="text-right" colSpan="5"></td>
															</tr>
														</tfoot>
													</table>
												</div>
											</div> : <div className="row margintop-25">
												<div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
													No Outstandings found!!!!
												</div>
											</div>}
										</div>
										<div className={`tab-pane fade ${tabactive.payment ? 'show active': ''}`}>
											<div className='row margintop-25'>
												{resource.paymentperformance.length > 0 ? <div className="col-md-12 col-sm-12 col-xs-12">
													<table className="table table-bordered table-sm">
														<thead>
															<tr>
																<th className="text-center">Voucher Date</th>
																<th className="text-center">Voucher Type</th>
																<th className="text-center">Voucher No</th>
																<th className="text-center">Voucher Amount</th>
																<th className="text-center">Cleared On</th>
																<th className="text-center">Days</th>
																<th className="text-center">Due On</th>
																<th className="text-center">Days from Due</th>
																<th className="text-center"></th>
															</tr>
														</thead>
														<tbody>
															{this.renderPayment()}
														</tbody>
													</table>
												</div> :<div className="col-md-12 col-sm-12 col-xs-12 alert alert-info text-center">
													No Transactions found!!!!
												</div>}
											</div>
										</div>
										{this.state.collectionActivityAccess ? <div className={`tab-pane fade ${tabactive.collectionactivity ? 'show active': ''}`}>
											<div className='row margintop-25'>
												<div className="col-md-12 col-sm-12 col-xs-12">
													<div className="col-md-3 offset-md-4">
														<label className="labelclass">Collection Representative</label>
														<span className="form-control un-editable" disabled>
															{resource.customerid_collectionrep}
														</span>
													</div>
												</div>
											</div>
											<div className="row">
												<div className={`form-group col-md-12 col-sm-12 col-xs-12 ${!this.state.addActivityShow ? 'showsection' : 'hidesection'}`}>
													<div className="row">
														<div className="col-md-8 col-sm-8 col-xs-8">
															{this.props.resource.collectionActivity.length==0 ? <div className="text-center">No item found</div> : null }
														</div>
														<div className="col-md-4 col-sm-4 col-xs-4">
															<button type="button" className="float-right btn btn-sm gs-btn-info" onClick={this.createActivity}>
																<i className="fa fa-plus"></i> Activity
															</button>
														</div>
													</div>
												</div>
											</div>
											<div className='row'>
												<Newactivityhistorysection resource={this.props.resource} getActivities={this.getCollectionActivities} openactivity={this.openActivity}/>
											</div>
										</div> : null}
										<div className={`tab-pane fade ${tabactive.contact ? 'show active': ''}`}>
											<div className='row margintop-25'>
												{this.renderContact()}
											</div>
										</div>
										<div className={`tab-pane fade ${tabactive.address ? 'show active': ''}`}>
											<div className='row margintop-25'>
												{this.renderAddress()}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> : null }
					</div>
					{this.props.resource.customerid > 0 ? <div className="row" style={{paddingTop: '40px'}}>
						<div className="col-md-12">
							<div className="muted credit text-center sticky-footer" style={{width : '100%'}}>
								<button type="button" className="btn btn-sm btn-secondary btn-width closebtn" onClick={()=>{this.cancel()}}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm btn-width gs-btn-success" onClick={this.printfunction}><i className="fa fa-print"></i>Print</button>

								<button type="button" className="btn btn-sm btn-width gs-btn-success" onClick={this.openEmail}><i className="fa fa-envelope-o"></i>Send Mail</button>
							</div>
						</div>
					</div> : null}
				</form>
			</>
		);
	};
}

CustomerStatementForm = connect(
	(state, props) => {
		let formName = 'customerstatementreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(CustomerStatementForm));

export default CustomerStatementForm;
