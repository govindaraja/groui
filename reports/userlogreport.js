import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { autoSelectEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { dateFilter, timeFilter } from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';

class UserLogReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.renderWeekDetails = this.renderWeekDetails.bind(this);
		this.renderTimeline = this.renderTimeline.bind(this);
		this.searchWeek = this.searchWeek.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				fromdate : new Date(moment().startOf('week')._d),
				todate : new Date(moment().endOf('week')._d),
				attendancedetails: []
			};
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
    }


	getReportData () {
		this.updateLoaderFlag(true);

		let filterString = [];
		['fromdate', 'todate', 'userid'].forEach((item) => {
			if (this.props.resource[item]) {
				let filterData = this.props.resource[item];

				if(item == 'fromdate' || item == 'todate')
					filterData = new Date(filterData).toDateString();

				filterString.push(`${item}=${filterData}`);
			}
		});

		if(!this.props.resource.userid) {
			this.updateLoaderFlag(false);
			return this.props.updateFormState(this.props.form, {
				attendancedetails: []
			});
		}
	
		axios.get(`/api/query/userlogreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.updateFormState(this.props.form, {
					attendancedetails: response.data.main
				});
				this.props.updateReportFilter(this.props.form, this.props.resource);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	searchWeek(param) {
		let { fromdate, todate } = this.props.resource;

		let nextMonday, nextSunday;
		if(param == 'prev') {
			let prevdaystoMonday = moment(fromdate).subtract(1, 'weeks').startOf('week');
			let prevdaystoSunday = moment(fromdate).subtract(1, 'weeks').endOf('week');

			nextMonday = prevdaystoMonday.add('days', 1);
			nextSunday = prevdaystoSunday.add('days', 1);
		} else {
			let nextdaystoMonday = moment(fromdate).add(1, 'weeks').startOf('week');
			let nextdaystoSunday = moment(fromdate).add(1, 'weeks').endOf('week');

			nextMonday = nextdaystoMonday.add('days', 1);
			nextSunday = nextdaystoSunday.add('days', 1);
		}

		this.props.updateFormState(this.props.form, {
			fromdate: new Date(new Date(nextMonday).setHours(0, 0, 0, 0)),
			todate : new Date(new Date(nextSunday).setHours(0, 0, 0, 0))
		});
		setTimeout(this.getReportData, 0);
	}

	renderWeekDetails() {
		return this.props.resource.attendancedetails.map((attendance, attendanceindex) => {
			let leaveHolidayDetails = '';

			if(attendance.holidayid_name)
				leaveHolidayDetails = 'Holiday';
			else if(attendance.weekoffholidaydetails == 'WeekOff')
				leaveHolidayDetails = 'Week Off';
			else if(attendance.leavetypeid_name)
				leaveHolidayDetails = 'Leave';
			else if(attendance.absent)
				leaveHolidayDetails = 'Absent';
			else if(attendance.onduty == 'Yes')
				leaveHolidayDetails = 'On Duty';
			else if(!attendance.intime && !attendance.outtime)
				leaveHolidayDetails = 'No Attendance';

			attendance.holidayid_name ? 'Holiday' : (attendance.weekoffholidaydetails == 'WeekOff' ? 'Week Off' : (attendance.leavetypeid_name ? 'Leave' : (attendance.absent ? 'Absent' : (attendance.onduty == 'Yes' ? 'On Duty' : ''))));

			let upcomingDays = new Date(new Date(attendance.date).setHours(0,0,0,0)) > new Date(new Date().setHours(0,0,0,0)) ? true : false;
			let currentDate = new Date(new Date(attendance.date).setHours(0,0,0,0)).getTime() == new Date(new Date().setHours(0,0,0,0)).getTime() ? true : false;

			return (
				<div className="card" key={attendanceindex} style={{borderRadius: '8px', minWidth: '140px', minHeight: '100px', boxShadow: currentDate ? '0px 0px 20px rgba(173, 173, 173, 0.3)' : 'none'}}>
					<div className="card-body d-flex flex-column justify-content-between" style={{backgroundColor: upcomingDays ? '#F3F3F3' : '', padding: '10px'}}>
						<div className="text-center">
							<span className="semi-bold mr-1">{moment(attendance.date).format('DD MMM')}</span>
							<span className="font-12" style={{color: 'rgba(68, 68, 68, 0.5)'}}>{moment(attendance.date).format('ddd')}</span>
						</div>
						<div className="text-center py-2 font-20">
							{attendance.notes.length > 0 ? <div className="d-flex align-items-center justify-content-center" style={{width: '18px', height: '18px', borderRadius: '50%', backgroundColor: '#86DE95', margin: '0 auto', color: '#fff'}}>
								<span className="fa fa-check font-12"></span>
							</div> : (!leaveHolidayDetails && !upcomingDays) ? <div className="d-flex align-items-center justify-content-center" style={{width: '18px', height: '18px', borderRadius: '50%', backgroundColor: '#F2994A', margin: '0 auto', color: '#fff'}}>
								<span className="fa fa-exclamation font-12"></span>
							</div> : null}
							{(leaveHolidayDetails || upcomingDays) && attendance.notes.length == 0 ? <span style={{color: 'rgba(68, 68, 68, 0.5)'}}>--</span> : null}
						</div>
						<div className="text-center">
							{
								leaveHolidayDetails ? 
									<div className="font-12" style={{color: 'rgba(68, 68, 68, 0.5)'}}>{leaveHolidayDetails}</div>
								: (attendance.intime || attendance.outtime) ? 
									<div className="font-12">
										<span>{timeFilter(attendance.intime)}</span> - <span>{timeFilter(attendance.outtime)}</span>
									</div>
									: ''
							}
						</div>
					</div>
				</div>
			);
		});
	}

	renderTimeline() {
		let attendancedetails = [...this.props.resource.attendancedetails].sort((a, b) => (new Date(b.date) - new Date(a.date)));

		return attendancedetails.map((attendance, attendanceindex) => {
			if(new Date(new Date(attendance.date).setHours(0,0,0,0)) > new Date(new Date().setHours(0,0,0,0)))
				return null;

			return (
				<div className="mb-3" key={attendanceindex}>
					<div className="font-18 font-weight-bold mb-3">{dateFilter(attendance.date)}</div>
					{
						attendance.notes.length == 0 ? <div>No Entries</div>
						: <div style={{marginLeft: '75px'}}>
							<ul className="gs-user-log-timeline">
								{this.renderNotesDetails(attendance.notes)}
							</ul>
						</div>
					}
				</div>
			);
		});
	}

	renderNotesDetails(notesDetails) {
		return notesDetails.map((note, noteindex) => {
			return (
				<li className="event" data-date={`${timeFilter(note.created)}`}  key={noteindex}>
					<div style={{color: '#052140'}}>
						<span>{note.transaction_displayname}</span> - <span>{note.transaction_title}</span><span className="badge ml-3" style={{backgroundColor: 'rgba(80, 100, 121, .1)', color: '#506479', fontSize: '12px', padding: '4px 10px', fontWeight: 'normal'}}>{note.action}</span>
					</div>
				</li>
			);
		});
	}

	render() {
		if(!this.props.resource)
			return <Loadingcontainer isloading={true}></Loadingcontainer>;

		let header_height = $('.navbar').outerHeight() + $('.report-header').outerHeight() + 20;
		return(
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row bg-white">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">User Log Report</div>
							<div className="report-header-btnbar" style={{width: '250px'}}>
								<Field
									name={'userid'}
									props={{
										resource: 'users',
										fields: 'id,displayname',
										label: "displayname",
										onChange: (value) => this.getReportData()
									}}
									component={autoSelectEle} />
							</div>
							{this.props.resource.attendancedetails.length > 0 ? <div className="report-header-rightpanel">
								<button type="button" className="btn btn-sm gs-form-badge-primary btn-width" onClick={()=>this.searchWeek('prev')}><span className="fa fa-caret-left mr-1"></span>Previous Week</button>
								<button type="button" className="btn btn-sm gs-form-badge-primary btn-width" onClick={()=>this.searchWeek('next')}>Next Week<span className="fa fa-caret-right ml-1"></span></button>
							</div> : null}
						</div>
						<div className="col-md-12 paddingright-5 mt-3" style={{height: `calc(100vh - ${header_height}px)`, overflow: 'hidden', overflowY: 'auto'}}>
							<div className="row">
								<div className="col-lg-10 offset-lg-1">
									<div className="d-flex flow-row justify-content-between">
										{this.props.resource.attendancedetails.length > 0 ? this.renderWeekDetails() : null}
									</div>
								</div>
								{this.props.resource.attendancedetails.length > 0 ? <div className="col-lg-12"><hr></hr></div> : null}
								<div className="col-lg-10 offset-lg-1">
									<div className="d-flex flex-column" style={{color: '#506479'}}>
										{this.renderTimeline()}
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		);
	};
}

UserLogReportForm = connect(
	(state, props) => {
		let formName = 'userlogreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(UserLogReportForm));

export default UserLogReportForm;
