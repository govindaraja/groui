import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class PurchaseRegisterReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			purchaseregisterColumns: [{
				"name" : "Voucher Type",
				"key" : "vouchertype",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Invoice No",
				"key" : "invoiceno",
				"format" : "anchortag",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Voucher No",
				"key" : "voucherno",
				"format" : "anchortag",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Voucher Date",
				"key" : "voucherdate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Supplier Invoice No",
				"key" : "supplierinvoiceno",
				"format" : "anchortag",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Supplier Invoice Date",
				"key" : "supplierinvoicedate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Supplier Name",
				"key" : "supplierid_name",
				"width" : 200
			}, {
				"name" : "Supplier State",
				"key" : "supplierstate",
				"width" : 200
			}, {
				"name" : "Supplier GST Reg Type",
				"key" : "suppliergstregtype",
				"width" : 200
			}, {
				"name" : "Supplier GSTIN",
				"key" : "suppliergstin",
				"width" : 200
			}, {
				"name" : "Numbering Series",
				"key" : "numberingseries",
				"width" : 180
			}, {
				"name" : "Currency",
				"key" : "currencyname",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Currency Exchange Rate",
				"key" : "currencyexchangerate",
				"width" : 150
			}, {
				"name" : "Total before tax(Transaction Currency)",
				"key" : "valuebeforetax",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Total(Transaction Currency)",
				"key" : "finaltotal",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Total",
				"key" : "total",
				"format" : "defaultcurrency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "TIN Number",
				"key" : "tin",
				"width" : 160
			}, {
				"name" : "Service Tax No",
				"key" : "servicetaxno",
				"width" : 160
			}],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				reportbasedon : 'invoicedate',
				filters: {},
				hiddencols: [],
				columns: []
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let purchaseregisterColumns = [...this.state.purchaseregisterColumns];

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['companyid', 'fromdate', 'todate', 'accountid', 'reportbasedon'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/purchaseregisterquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let tempObj = {originalRows: [], hiddencols: [], columns : []};

				let temp = 16;
				if (purchaseregisterColumns.length > 18) {
					purchaseregisterColumns.splice(16, purchaseregisterColumns.length - 18);
				}
				for (var i = 0; i < response.data.main.accountarray.length; i++) {
					let tempObj = {
						name : response.data.main.accountarray[i].name,
						key : response.data.main.accountarray[i].id.toString(),
						format : 'defaultcurrency',
						footertype : 'sum',
						cellClass : 'text-right',
						width : 210
					};
					purchaseregisterColumns.splice(temp, 0, tempObj);
					temp++;
				}

				purchaseregisterColumns.forEach((item) => {
					if(item.hidden == true) {
						tempObj.hiddencols.push(item.key);
					}
				});

				customfieldAssign(purchaseregisterColumns, null, 'purchaseinvoices', this.props.app.myResources, true);

				tempObj.originalRows = response.data.main.purchaseregister;
				tempObj.columns = purchaseregisterColumns;

				this.props.updateFormState(this.props.form, tempObj);

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			reportbasedon : null,
			accountid : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data) {
		if(data.vouchertype == 'Purchase Invoice'){
			this.props.history.push(`/details/purchaseinvoices/${data.referenceid}`);
		} else {
			this.props.history.push(`/details/debitnotes/${data.referenceid}`);
		}
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Purchase Register Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Account</label>
										<Field name={'accountid'} props={{resource: "accounts", fields: "id,name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Based On</label>
										<Field name={'reportbasedon'} props={{options: [{value: "invoicedate", label: "Invoice Date"}, {value: "postingdate", label: "Posting Date"}], label: "label", valuename: "value", required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Based On'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Purchase Register Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

PurchaseRegisterReportForm = connect(
	(state, props) => {
		let formName = 'purchaseregisterreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(PurchaseRegisterReportForm));

export default PurchaseRegisterReportForm;
