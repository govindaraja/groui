import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class EngineerPerformanceReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.groupbyOnChange = this.groupbyOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth((new Date().getMonth() - 1))).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				groupby : 'Service Person',
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "",
					"key" : "displayname",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Designation",
					"key" : "designation",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Days Present",
					"key" : "workingdays",
					"cellClass" : "text-center",
					"width" : 125
				}, {
					"name" : "Total No Of Calls",
					"key" : "totalcount",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 160
				}, {
					"name" : "AMC Calls",
					"key" : "amc_count",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Warranty Calls",
					"key" : "warranty_count",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Calls without Contract",
					"key" : "nulltype_count",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "PMS Calls",
					"key" : "pms_count",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Service Calls",
					"key" : "service_count",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Installation Calls",
					"key" : "installation_count",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "PMS Equipments",
					"key" : "pmsequipmentcount",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Service Equipments",
					"key" : "serviceequipmentcount",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Installation Equipments",
					"key" : "installationequipmentcount",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Avg First Visit Time (hours)",
					"key" : "avgfirstvisittime",
					"footertype" : "avg",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Total Time Spent (hours)",
					"key" : "totaltimespent",
					"footertype" : "sum",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Avg Time Spent (per call)",
					"key" : "avgtimespent",
					"footertype" : "avg",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Avg Resolution Time (Per Call)",
					"key" : "avgimpactduration",
					"footertype" : "avg",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "First Time Resolution %",
					"key" : "firstrespercent",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Avg Feedback Rating",
					"key" : "avgrating",
					"footertype" : "avg",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "0-4 Hours",
					"key" : "hours4",
					"footertype" : "sum",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "4-8 Hours",
					"key" : "hours8",
					"footertype" : "sum",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "8-12 Hours",
					"key" : "hours12",
					"footertype" : "sum",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "12-16 Hours",
					"key" : "hours16",
					"footertype" : "sum",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "16-20 Hours",
					"key" : "hours20",
					"footertype" : "sum",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "20 Hours Plus",
					"key" : "hours20plus",
					"footertype" : "sum",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 130
				}, {
					"name" : "No of Estimations",
					"key" : "estimation_count",
					"footertype" : "sum",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 160
				}, {
					"name" : "Estimation Value",
					"key" : "estimation_value",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "No Of Service Invoices",
					"key" : "serviceinvoice_count",
					"footertype" : "sum",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 160
				}, {
					"name" : "Service Invoice Value",
					"key" : "serviceinvoice_value",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-center",
					"width" : 180
				}]
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let { columns } = this.props.resource;

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];

		['fromdate', 'todate', 'groupby'].forEach((item) => {
			if(this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});
	
		axios.get(`/api/query/engineerperformancereportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {

				if(columns.length > 29)
					columns.splice(29, columns.length - 29);

				if(this.props.resource.groupby == 'Service Person') {
					for (let i = 0; i < columns.length; i++) {
						if(columns[i].key == 'displayname')
							columns[i].name = 'Service Person';
						if(columns[i].key == 'designation')
							columns[i].hidden = true;
					}

					columns.splice(29, 0, {
							"name" : "No of Contract Enquiries",
							"key" : "contractenquiries_count",
							"footertype" : "sum",
							"cellClass" : "text-center",
							"width" : 160
						}, {
							"name" : "Contract Enquiries Value",
							"key" : "contractenquiries_value",
							"format" : "currency",
							"footertype" : "sum",
							"cellClass" : "text-center",
							"width" : 180
						}, {
							"name" : "No of Contracts",
							"key" : "contracts_count",
							"footertype" : "sum",
							"cellClass" : "text-center",
							"width" : 160
						}, {
							"name" : "Contract Value",
							"key" : "contracts_value",
							"format" : "currency",
							"footertype" : "sum",
							"cellClass" : "text-center",
							"width" : 180
						});
				} else {
					for (let i = 0; i < columns.length; i++) {
						if(columns[i].key == 'displayname')
							columns[i].name = 'Engineer';
						if(columns[i].key == 'designation')
							columns[i].hidden = false;
					}
				}

				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	groupbyOnChange() {
		this.props.array.removeAll('originalRows');

		this.props.updateFormState(this.props.form, {
			filters: {}
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			groupby : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Engineer Performance Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Group By</label>
										<Field name={'groupby'} props={{options: ["Engineer", "Service Person"], required: true, onChange: () => {this.groupbyOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Group By'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Engineer Performance Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

EngineerPerformanceReportForm = connect(
	(state, props) => {
		let formName = 'engineerperformancereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(EngineerPerformanceReportForm));

export default EngineerPerformanceReportForm;
