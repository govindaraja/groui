import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class GeneralLedgerReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.vouchertypeOnChange = this.vouchertypeOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata && !this.props.location.params) {
			tempObj = {
				...this.props.reportdata
			};
		} else {
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth((new Date().getMonth() - 1))).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				filters: {},
				columns: [{
					"name" : "Date",
					"key" : "postingdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Voucher Type",
					"key" : "vouchertype",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher No",
					"key" : "voucherno",
					"format" : "anchortag",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Account",
					"key" : "accountid_displayname",
					"cellClass" : "text-center",
					"width" : 230
				}, {
					"name" : "Debit",
					"key" : "debit",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Credit",
					"key" : "credit",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Partner",
					"key" : "partnerid_name",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Employee",
					"key" : "employeeid_displayname",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Against Voucher",
					"key" : "againstvoucher",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Transaction Remarks",
					"key" : "transactionremarks",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Remarks",
					"key" : "description",
					"width" : 180
				}, {
					"name" : "Instrument No",
					"key" : "instrumentno",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Instrument Date",
					"key" : "instrumentdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Clearance Date",
					"key" : "clearancedate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}]
			};
			if(this.props.location.params) {
				tempObj.fromdate = this.props.location.params.fromdate;
				tempObj.todate = this.props.location.params.todate;
				tempObj.accountid = this.props.location.params.accountid;
			}
		}

		this.props.initialize(tempObj);

		setTimeout(() => {
			if(this.props.reportdata || this.props.location.params)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('originalRows');

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['fromdate', 'todate', 'accountid', 'vouchertype', 'voucherno'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});

		axios.get(`/api/query/generalledgerreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.main.length; i++) {
					for(var prop in response.data.main[i]) {
						response.data.main[i][prop] = response.data.main[i][prop] == null ? "" : response.data.main[i][prop];
					}
				}

				if(this.props.resource.accountid > 0) {
					response.data.main[0]['_restrictFromSorting'] = true;
					response.data.main[0]['_restrictFromAggregation'] = true;
					response.data.main[0]['_restrictFromFiltering'] = true;
					response.data.main[response.data.main.length - 1]['_restrictFromSorting'] = true;
					response.data.main[response.data.main.length - 1]['_restrictFromAggregation'] = true;
					response.data.main[response.data.main.length - 1]['_restrictFromFiltering'] = true;
				}

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);

				if(response.data.main.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			accountid : null,
			vouchertype : null,
			voucherno: null,
			originalRows: null,
			filters: {}
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	vouchertypeOnChange() {
		this.props.upateFormState(this.props.form, {
			voucherno : null
		});
	}

	openTransaction(data) {
		if (data.relatedresource == 'salesinvoices')
			this.props.history.push(`/details/salesinvoices/${data.relatedid}`);
		else if (data.relatedresource == 'purchaseinvoices')
			this.props.history.push(`/details/purchaseinvoices/${data.relatedid}`);
		if (data.relatedresource == 'journalvouchers') {
			if (data.vouchertype == "Payment")
				this.props.history.push(`/details/paymentvouchers/${data.relatedid}`);
			else if (data.vouchertype == "Receipt")
				this.props.history.push(`/details/receiptvouchers/${data.relatedid}`);
			else if (data.vouchertype == "Credit Note")
				this.props.history.push(`/details/creditnotes/${data.relatedid}`);
			else if (data.vouchertype == "Debit Note")
				this.props.history.push(`/details/debitnotes/${data.relatedid}`);
			else if(data.vouchertype == "Journal Voucher")
				this.props.history.push(`/details/journalvouchers/${data.relatedid}`);
		}
		else if (data.relatedresource == 'deliverynotes')
			this.props.history.push(`/details/deliverynotes/${data.relatedid}`);
		else if (data.relatedresource == 'receiptnotes')
			this.props.history.push(`/details/receiptnotes/${data.relatedid}`);
		else if (data.relatedresource == 'landingcosts')
			this.props.history.push(`/details/landingcosts/${data.relatedid}`);
		else if (data.relatedresource == 'financialyears')
			this.props.history.push(`/details/financialyear/${data.relatedid}`);
		else if (data.relatedresource == 'expenserequests')
			this.props.history.push(`/details/expenserequests/${data.relatedid}`);
		else if (data.relatedresource == 'payrolls')
			this.props.history.push(`/details/payrolls/${data.relatedid}`);
		else if (data.relatedresource == 'pos')
			this.props.history.push(`/details/pos/${data.relatedid}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">General Ledger</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true,title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Account</label>
										<Field name={'accountid'} props={{resource: "accounts", fields: "id,displayname", label: "displayname", filter: "accounts.isledger"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Voucher Type</label>
										<Field name={'vouchertype'} props={{options: [{value: "Sales Invoice", label: "Sales Invoice"}, {value: "Purchase Invoice", label: "Purchase Invoice"}, {value: "Delivery Note", label: "Delivery Note"}, {value: "Receipt Note", label: "Receipt Note"}, {value: "Receipt", label: "Receipt"}, {value: "Payment", label: "Payment"}, {value: "Credit Note", label: "Credit Note"}, {value: "Debit Note", label: "Debit Note"}, {value: "Journal Voucher", label: "Journal Voucher"}, {value: "Period Closure", label: "Period Closure"}, {value: "Landing Cost", label: "Landing Cost"}, {value: "Expense Request", label: "Expense Request"}, {value: "Pay Roll", label: "Pay Roll"}], label:"label", valuename: "value", onChange: () => {this.vouchertypeOnChange}}} component={localSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Voucher No</label>
										<Field name={'voucherno'} props={{resource: "generalledger", fields: "id,voucherno", label: "voucherno", valuename: "voucherno", filter: `generalledger.vouchertype='${this.props.resource.vouchertype}'`}} component={autoSelectEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12" >
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className={`btn btn-width btn-sm gs-btn-success ${checkActionVerbAccess(this.props.app, 'generalledger', 'Read') ? '' : 'hide'}`} disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='General Ledger' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

GeneralLedgerReportForm = connect(
	(state, props) => {
		let formName = 'generalledgerreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(GeneralLedgerReportForm));

export default GeneralLedgerReportForm;
