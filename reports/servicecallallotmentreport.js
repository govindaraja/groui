import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { checkboxEle, DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import MapForm from '../components/mapcomponent';

class ServicecallAllotmentReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.engineerCB = this.engineerCB.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.assignEngineer = this.assignEngineer.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.cbEngineer = this.cbEngineer.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	changeView(param) {
		this.props.updateFormState(this.props.form, {viewvalue : param});
		setTimeout(() => {
			this.getReportData();
			this.props.updateReportFilter(this.props.form, this.props.resource);
		}, 0);
	}

	onLoad() {
		let tempObj = {};

		let fromdate = new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)));
		let fromDate = new Date(fromdate.setDate(fromdate.getDate() + 1));

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				sericecallallotmentArray : [],
				engineerArray : [],
				filters: {},
				fromdate : fromDate,
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				calltype : ["Service"],
				viewvalue: 'list',
				//hiddencols: [],
				columns: [{
					"name" : "",
					"headerformat" : "checkbox",
					"key" : "ischecked",
					"locked" : true,
					"format" : "checkbox",
					"cellClass" : "text-center",
					"onChange" : "{report.checkboxOnChange}",
					"restrictToExport" : true,
					"width" : 100
				}, {
					"name" : "Service Call No",
					"key" : "servicecallno",
					"format" : "anchortag",
					"transactionname" : "servicecalls",
					"transactionid" : "id",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Service Call Date",
					"key" : "servicecalldate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Call Type",
					"key" : "calltype",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Customer",
					"key" : "customerid_name",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Equipment",
					"key" : "equipmentname",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Complaint",
					"key" : "complianttypeid_name",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Area",
					"key" : "area",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "City",
					"key" : "city",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Priority",
					"key" : "priority",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Appointment Date",
					"key" : "appointmentdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 150
				}]
			};

		this.props.initialize(tempObj);

		setTimeout(() => {
			//if(this.props.reportdata)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	cbEngineer (value) {
		this.props.updateFormState(this.props.form, {
			engineerid_displayname: value.engineerid_displayname,
			salesperson: value.salesperson,
			engineerid_userid: value.engineerid_userid,
			engineerid: value.engineerid,
			originalRows: [...value.availableMarkers]
		});

		setTimeout(()=>{
			this.assignEngineer();
		},0)
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('engineerArray');
		this.props.array.removeAll('sericecallallotmentArray');

		let filterString = [];

		if (this.props.resource.viewvalue == 'map') {
			if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
				this.updateLoaderFlag(false);
				return this.props.openModal(modalService.infoMethod({
					header : "Error",
					body : "Difference between From Date and Todate can't be more than One Year",
					btnArray : ["Ok"]
				}));
			}

			['fromdate', 'todate', 'territoryid', 'calltype'].forEach((item) => {
				if (this.props.resource[item])
					filterString.push(`${item}=${this.props.resource[item]}`)
			});

			filterString.push(`querytype=load`, `isMapView=true`);
		} else
			filterString.push(`querytype=load`);

		axios.get(`/api/query/servicecallallotmentquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				response.data.main.forEach((item) => {
					item.ischecked = false;
				});

				for ( let i = 0; i < response.data.engineerArray.length; i++) {
					response.data.engineerArray[i].namewithcount = response.data.engineerArray[i].displayname+' - '+response.data.engineerArray[i].count;
				}

				response.data.engineerArray = response.data.engineerArray.sort((a, b) => {
					return (Number(a.count) > Number(b.count)) ? -1 : (Number(a.count) < Number(b.count)) ? 1 : 0;
				});

				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});


				let tempObj = {
					engineerArray : response.data.engineerArray,
					sericecallallotmentArray : response.data.main,
					originalRows: response.data.main
				};

				this.props.updateFormState(this.props.form, tempObj);

				this.props.updateReportFilter(this.props.form, this.props.resource);

				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	engineerCB (value) {
		let tempObj = {};
		for(let i = 0; i < this.props.resource.engineerArray.length; i++) {
			if (this.props.resource.engineerArray[i].id == value) {
				tempObj['engineerid_displayname'] = this.props.resource.engineerArray[i].displayname;
				tempObj['salesperson'] = this.props.resource.engineerArray[i].userid;
				tempObj['engineerid_userid'] = this.props.resource.engineerArray[i].userid;
				break;
			}
		}

		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	assignEngineer() {
		let tempServicecall = [],
			tempStr = "",
			tempObj = this.props.resource;

		tempObj.originalRows.map((item) => {
			if(item.ischecked) {
				tempStr += item.servicecallno+",";
				tempServicecall.push(item.id);
			}
		});

		tempStr = tempStr.substr(0,tempStr.length-1);

		tempObj['tempServicecall'] = tempServicecall;

		if (tempObj.engineerid == 'undefined' || tempObj.engineerid == null) {
			return this.props.openModal(modalService.infoMethod({
				header : "Warning",
				body : "Please select Engineer.",
				btnArray : ["Ok"]
			}));
		} else if (tempObj.salesperson == 'undefined' || tempObj.salesperson == null) {
			return this.props.openModal(modalService.infoMethod({
				header : "Warning",
				body : "Please select Service Person.",
				btnArray : ["Ok"]
			}));
		} else if (tempServicecall.length == 0) {
			return this.props.openModal(modalService.infoMethod({
				header : "Warning",
				body : "Please select alteast one service call to assign.",
				btnArray : ["Ok"]
			}));
		} else {
			let message = {
				header : "Confirm",
				body : `Do you want to assign ${tempObj.engineerid_displayname} to ${tempStr}`,
				btnArray : ['Yes','No']
			};
			this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if(param) {
					this.callbackmodal(param, tempObj);
				}
			}));
		}
	}

	callbackmodal (param, tempObj) {
		if(param){
			this.updateLoaderFlag(true);

			axios.get(`/api/query/servicecallallotmentquery?querytype=update&engineerid=${tempObj.engineerid}&salesperson=${tempObj.salesperson}&servicecallarray=${tempObj.tempServicecall}`).then((response) => {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

				if (response.data.message == 'success')
					this.getReportData();

				this.updateLoaderFlag(false);
			});
		}
	}

	checkboxOnChange(value, item) {
		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		this.refs.grid.refresh();
	}

	checkboxHeaderOnChange(param) {
		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if(param) {
				item.ischecked = true;
			}
		});

		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		this.refs.grid.forceRefresh();
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			territoryid : null,
			calltype : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			sericecallallotmentArray: null,
			engineerArray: null
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<div>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className={`col-md-12 bg-white report-header ${this.props.resource.viewvalue != 'map' ? 'form-group' : ''}`}>
							<div className="report-header-title">Service Call Assignment</div>
							{ this.props.resource.viewvalue == 'list' ? <div className="report-header-btnbar col-md-8 margintop-10">
								<div className="row col-md-12 col-sm-12">
									<div className="form-group col-md-5 col-sm-12">
										<label className="labelclass">Select Engineer</label>
										<Field
											name={'engineerid'}
											props={{
												options: this.props.resource.engineerArray,
												label: "namewithcount",
												valuename: "id",
												onChange: (value) => this.engineerCB(value)
											}}
											component={localSelectEle} />
									</div>
									{(this.props.resource.engineerid && !this.props.resource.engineerid_userid)? <div className="form-group col-md-5 col-sm-12">
										<label className="labelclass">Service Person</label>
										<Field name={'salesperson'} props={{resource: "users", fields: "id,displayname", label: "displayname", filter: "users.issalesperson=true"}} component={selectAsyncEle} />
									</div> : null }
									<div className="form-group col-md-2 col-sm-12"><button type="button" onClick={() => {this.assignEngineer()}} className="btn btn-width gs-btn-outline-success btn-sm margintop-25" disabled={!this.props.valid}><i className="fa fa-check"></i>Assign</button></div>
								</div>
							</div> : null }
							{this.props.app.feature.licensedTrackingUserCount > 0 ? <div className="report-header-rightpanel">
								<ul className="list-pagination">
									<li className={`${this.props.resource.viewvalue == 'list' ? 'active' : ''}`} onClick={() =>{this.changeView('list')}}><a><span className="fa fa-th-list"></span></a></li>
									<li className={`${this.props.resource.viewvalue == 'map' ? 'active' : ''}`} onClick={() =>{this.changeView('map')}}><a><span className="fa fa-globe"></span></a></li>
								</ul>
							</div> : null}
						</div>
						<div className="col-md-12 paddingright-5">
							{this.props.resource.viewvalue == 'map' ? <ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Call Type</label>
										<Field name={'calltype'} props={{options: ["Service",  "Installation", "PMS", "Inspection"], required: true, multiselect: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Call Type'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required:  true, title : 'From Date'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Territory</label>
										<Field name={'territoryid'} props={{resource: "territories", fields: "id,territoryname", label: "territoryname"}} component={autoSelectEle} validate={[numberNewValidation({required: false, model: 'territoryid'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>: null}
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.viewvalue == 'list' && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} /> : null }
							{ this.props.resource.viewvalue == 'map' && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <MapForm app={this.props.app} resource={this.props.resource} openModal={this.props.openModal} history = {this.props.history} updateFormState={this.props.updateFormState} form={this.props.form} rptparam={'servicecallallotmentreport'} latlngArray={this.props.resource.originalRows} callback = {(value) => this.cbEngineer(value)}/> : null }
						</div>
					</div>
				</form>
			</div>
		);
	};
}

ServicecallAllotmentReportForm = connect(
	(state, props) => {
		let formName = 'servicecallallotmentreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ServicecallAllotmentReportForm));

export default ServicecallAllotmentReportForm;
