import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';

import { checkboxEle, DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { AutoSelect, SelectAsync } from '../components/utilcomponents';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class ProjectContractorPaymentReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			contractorpaymentarray : [],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.createPurchaseInvoice = this.createPurchaseInvoice.bind(this);
		this.projectCB = this.projectCB.bind(this);
		this.worktypeCB = this.worktypeCB.bind(this);
		this.getProjectDetails = this.getProjectDetails.bind(this);
		this.showOnChange = this.showOnChange.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata) {
			tempObj = {
				...this.props.reportdata
			};
			this.props.initialize(tempObj)
			setTimeout(() => {
				if(this.props.reportdata)
					this.getReportData();
			}, 0);
			this.updateLoaderFlag(false);
		} else {
			tempObj = {
				projectArray: [],
				workorderArray: [],
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "",
					"headerformat" : "checkbox",
					"key" : "ischecked",
					"locked": true,
					"format" : "checkbox",
					"cellClass" : "text-center",
					"onChange" : "{report.checkboxOnChange}",
					"showfield" : "item.completedqty > item.invoicedqty",
					"restrictToExport" : true,
					"width" : 100
				}, {
					"name" : "Contractor",
					"key" : "partnerid_name",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "Project No",
					"key" : "projectno",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Project Name",
					"key" : "projectname",
					"cellClass" : "text-center",
					"width" : 250
				}, {
					"name" : "BOQ Internal Ref No",
					"key" : "internalrefno",
					"cellClass" : "text-center",
					"width" : 300
				}, {
					"name" : "Estimation Item Name",
					"key" : "estimationitemid_name",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Estimation Item Description",
					"key" : "estimationitemid_description",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Workorder No",
					"key" : "wonumber",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Workprogress No",
					"key" : "workprogressno",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Item Name",
					"key" : "itemid_name",
					"width" : 180
				}, {
					"name" : "Item Description",
					"key" : "description",
					"width" : 180
				}, {
					"name" : "Workorder Qty",
					"key" : "quantity",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "UOM",
					"key" : "uomid_name",
					"cellClass" : "text-center",
					"width" : 100
				}, {
					"name" : "Completed Qty",
					"key" : "completedqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Invoiced Qty",
					"key" : "invoicedqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "To be Invoiced Qty",
					"key" : "tobeinvoicedqty",
					"format" : "number",
					"cellClass" : "text-right",
					"width" : 180
				}]
			};
			this.getProjectDetails(tempObj);
		}
	}

	getProjectDetails(tempObj) {
		axios.get(`/api/projects?field=id,projectno,projectname,displayname&filtercondition=projects.status='Approved'`).then((response) => {
			tempObj.projectArray = response.data.main;
			this.props.initialize(tempObj);
			setTimeout(() => {
				if(this.props.reportdata)
					this.getReportData();
			}, 0);
			this.updateLoaderFlag(false);
		})
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];
		['projectid', 'contractorid', 'worktype', 'workorderid', 'show'].map((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/projectcontractorpaymentquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				let { columns } = this.props.resource;

				if(this.props.resource.worktype && this.props.resource.worktype.length ==1) {
					if(this.props.resource.worktype.indexOf('Work Order') > -1)
						columns.splice(8, 1);
					if(this.props.resource.worktype.indexOf('Casual Labour') > -1)
						columns.splice(9, 1);
				}
				this.props.resource.columns.forEach((item, index) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			projectid: null,
			workorderid: null,
			contractorid : null,
			worktype : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	projectCB (value, valueObj) {
		if(value > 0){
			this.getWorkorderDetails();
		}
		else{
			this.props.updateFormState(this.props.form, {
				workorderArray : []
			});
		}
	}

	getWorkorderDetails() {
		axios.get(`/api/workorders?field=id,wonumber&filtercondition=workorders.status in ('Approved', 'Sent To Supplier') and workorders.projectid in (${this.props.resource.projectid})`).then((response) => {
			this.props.updateFormState(this.props.form, {
				workorderArray : response.data.main
			});
		});
	}

	workorderCB(value, valueObj) {
		this.props.updateFormState(this.props.form, {
			workorderid : null
		});
	}

	worktypeCB() {
		this.props.updateFormState(this.props.form, {
			workorderid : null
		});
	}

	checkboxOnChange(value, item) {
		let contractorpaymentarray = [];
		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		setTimeout(() => {
			this.props.resource.originalRows.forEach((rowItem, index) => {
				if(rowItem.ischecked)
					contractorpaymentarray.push(rowItem.sourceid);
			});
			this.setState({contractorpaymentarray});
		}, 0);
		this.refs.grid.refresh();
	}

	checkboxHeaderOnChange(param) {
		let contractorpaymentarray = [];
		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if(param) {
				item.ischecked = true;
				contractorpaymentarray.push(item.sourceid);
			}
		});
		this.setState({ contractorpaymentarray });
		setTimeout(() => {
			this.props.updateFormState(this.props.form, {
				originalRows: this.props.resource.originalRows
			});
		}, 0);
		this.refs.grid.forceRefresh();
	}	

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	createPurchaseInvoice() {
		this.updateLoaderFlag(true);

		let itemCheckFound = false,
			tempObj = this.props.resource;

		tempObj.originalRows.map((item) => {
			if(item.ischecked) {
				itemCheckFound = true;
			}
		});

		if (!itemCheckFound) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Please Choose Atlease One Item to create Purchase Invoice !!!",
				btnArray : ["Ok"]
			}));
		} else {
			axios.get(`/api/numberingseriesmaster?field=id,name?filtercondition=numberingseriesmaster.resource='purchaseinvoices'`).then((response) =>{
				if(response.data.main.length > 1) {
					this.props.openModal({
						render: (closeModal) => {
							return <NumberingseriesModal callback={(numberingseriesmasterid) => {
								if(!numberingseriesmasterid)
									return this.updateLoaderFlag(false);
								this.props.updateFormState(this.props.form, { numberingseriesmasterid });
								setTimeout(() => {
									this.savePurchaseInvoice(numberingseriesmasterid);
								}, 0);
							}} app={this.props.app} closeModal={closeModal} />
						}, confirmModal: true, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
					});
				} else {
					this.savePurchaseInvoice(response.data.main.id);
				}
			});
		}
	}

	savePurchaseInvoice(numberingseriesmasterid) {
		this.updateLoaderFlag(true);
		let filterString = [];
		['projectid', 'contractorid', 'worktype', 'workorderid'].map((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/projectcontractorpaymentquery?${filterString.join('&')}&querytype=create&numberingseriesmasterid=${numberingseriesmasterid}&invoiceidarray=${this.state.contractorpaymentarray.join(',')}`).then((response) => {
			if (response.data.message == 'success') {
				let tempStr = "The following records are created\n\n";
				response.data.main.forEach((item) => {
					tempStr += "Purchase Invoice - " + item + "\n";
				});

				this.props.openModal(modalService['infoMethod']({
					header : response.data.message,
					body : tempStr,
					btnArray : ['Ok']
				}));
				if(this.props.reportdata)
					this.getReportData();
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	showOnChange() {
        this.props.updateFormState(this.props.form, {
            projectid: null,
            contractorid: null,
            worktype: null,
            originalRows: []
        });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 form-group bg-white report-header">
							<div className="report-header-title">Contractor Payment</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							<div className="report-header-rightpanel">
								{(this.props.resource.originalRows && this.props.resource.originalRows.length>0 && checkActionVerbAccess(this.props.app, 'purchaseinvoices', 'Save'))? <button type="button" onClick={this.createPurchaseInvoice} className="btn btn-width btn-sm gs-btn-outline-primary float-right" rel="tooltip" title="Create Purchase Invoice" disabled={!this.props.valid}><i className="fa fa-plus"></i>Purchase Invoice</button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: [{value: "all", label: "Show All"}, {value: "pending", label: "Pending Invoice"}], label:"label", valuename: "value", required: true, onChange: () => {this.showOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Show'})]} />
									</div>									
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Project No</label>

										<Field name={'projectid'} props={{options: this.props.resource.projectArray, label: "displayname", valuename: "id", multiselect: true, onChange: (value, valueObj) => this.projectCB(value, valueObj)}} component={localSelectEle} validate={[multiSelectNewValidation({required: '{resource.show == "all" ? true : false}' ,title : 'Project No'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Contractor</label>
										<Field
											name={'contractorid'}
											props={{
												resource: "partners",
												fields: "id,displayname",
												label: "displayname",
												filter: "partners.issupplier"
											}}
											component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Work Type</label>
										<Field name={'worktype'} props={{options: ["Work Order", "Casual Labour"], multiselect: true, onChange: (value, valueObj) => this.worktypeCB(value, valueObj)}} component={localSelectEle} />
									</div>
									{this.props.resource.worktype && this.props.resource.worktype.length == 1 && this.props.resource.worktype[0] == 'Work Order' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Work Order</label>
										<Field name={'workorderid'} props={{options: this.props.resource.workorderArray, label: "wonumber", valuename: "id", required: true, multiselect: true}} component={localSelectEle} validate={[numberNewValidation({required: true, model: 'Workorder No'})]} />
									</div> : null}
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Contactor Payment Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} btnOnClick={this.btnOnClick} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

class NumberingseriesModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.filterOnchange = this.filterOnchange.bind(this);
		this.setNumberingSeries = this.setNumberingSeries.bind(this);
	}

	filterOnchange(numberingseriesmasterid) {
		this.setState({
			numberingseriesmasterid
		});	
	}

	setNumberingSeries(closeparam) {
		if(closeparam)
			this.props.callback(null);
		else
			this.props.callback(this.state.numberingseriesmasterid);

		this.props.closeModal();
	}

	render() {
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">Pick Numbering Series</h5>
				</div>
				<div className="react-modal-body">
					<div className="row">
						<div className="form-group col-md-8 offset-md-2">
							<label>Choose Series Type</label>
							<SelectAsync 
								resource="numberingseriesmaster" 
								fields="id,name,format,isdefault,currentvalue"
								className={this.state.numberingseriesmasterid ? '' : 'errorinput'}
								value={this.state.numberingseriesmasterid}
								label="name" 
								valuename="id"
								createParamFlag={true}
								filter="numberingseriesmaster.resource='purchaseinvoices'" 
								defaultValueUpdateFn={(value, valueobj) => this.filterOnchange(value, valueobj)}
								onChange={(value, valueobj) => this.filterOnchange(value, valueobj)}
							/>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={() => this.setNumberingSeries(true)}><i className="fa fa-times"></i>Close</button>
								<button type="button" className="btn btn-sm gs-btn-success btn-width" onClick={() => this.setNumberingSeries(false)} disabled={!this.state.numberingseriesmasterid}><i className="fa fa-check-square-o"></i>Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

ProjectContractorPaymentReportForm = connect(
	(state, props) => {
		let formName = 'projectcontractorpaymentreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ProjectContractorPaymentReportForm));

export default ProjectContractorPaymentReportForm;
