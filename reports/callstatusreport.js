import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import RecordingplayModal from '../components/details/recording';

class CallStatusReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			columns : [{
					name : 'Call ID',
					key : 'callid',
					cellClass : 'text-left',
					width : 180
				},{
					name : 'Call Type',
					key : 'calltype',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'From',
					key : 'fromno',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'To',
					key : 'tono',
					cellClass : 'text-center',
					width : 180
				},{
					name : 'Start Time',
					key : 'starttime',
					format : 'datetime',
					cellClass : 'text-center',
					width : 150
				},{
					name : 'Answer Time',
					key : 'answertime',
					format : 'datetime',
					cellClass : 'text-center',
					width : 150
				},{
					name : 'Hangup Time',
					key : 'hanguptime',
					format : 'datetime',
					cellClass : 'text-center',
					width : 150
				}, {
					name : 'Total Duration',
					key : 'duration',
					cellClass : 'text-center',
					width : 150
				},{
					name : 'Total Duration In Seconds',
					key : 'durationinseconds',
					cellClass : 'text-center',
					footertype : 'sum',
					width : 150
				}, {
					name : 'Status',
					key : 'status',
					cellClass : 'text-center',
					width : 180
				},{
					name : 'Contact',
					key : 'contactid_name',
					cellClass : 'text-center',
					width : 180
				},{
					name : 'Agent',
					key : 'userid_displayname',
					cellClass : 'text-center',
					width : 180
				},{
					name : 'Notes',
					key : 'notes',
					cellClass : 'text-left',
					width : 250
				},{
					name : '',
					format : 'button',
					onClick : '{report.playRecording}',
					showfield : 'item.recordingurl',
					buttonname : 'Recording',
					cellClass : 'text-center',
					restrictToExport : true,
					width : 100
				}
			],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.showOnChange = this.showOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.playRecording = this.playRecording.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	playRecording(data) {
		this.props.openModal({
			render: (closeModal) => {
				return <RecordingplayModal
						callid = {data.callid}
						closeModal = {closeModal} 
						openModal = {this.props.openModal}/>
			},
			className: {
				content: 'react-modal-custom-class-30',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(),
				todate : new Date(),
				userid : null,
				status : ["Abandoned", "Not Answered"],
				filters : {},
				hiddencols : [],
				columns : [],
				show : 'Summary'
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];

		['fromdate', 'todate','userid','status'].forEach((item) => {
			if(this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/callstatusreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns:  this.state.columns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			userid : null,
			status : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	showOnChange() {
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('columns');

		this.props.updateFormState(this.props.form, {
			filters: {}
		});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Call Status Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Status</label>
										<Field name={'status'} props={{options: ["Completed", "Abandoned", "Not Answered"], multiselect: true}} component={localSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Agent</label>
										<Field name={'userid'} props={{resource: "users", fields: "id,displayname,extension", label: "extension",filter:"users.extension is not null"}} component={selectAsyncEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Call Status Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

CallStatusReportForm = connect(
	(state, props) => {
		let formName = 'calhistoryreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(CallStatusReportForm));

export default CallStatusReportForm;
