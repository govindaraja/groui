import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, NumberEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import { currencyFilter, dateFilter} from '../utils/filter';
import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class TargetActualsReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.openSalesTarget = this.openSalesTarget.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.checkForCurrencyFilter = this.checkForCurrencyFilter.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	checkForCurrencyFilter(targetfield) {
		let resource = targetfield.split('-')[0];
		let field = targetfield.split('-')[1];

		if(resource == 'leadscount')
			return false;

		return this.props.app.myResources[resource] && this.props.app.myResources[resource].fields[field] && this.props.app.myResources[resource].fields[field].filterformat == 'currency' ? true : false;
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata) {
			tempObj = {
				...this.props.reportdata
			};
		} else {
			tempObj = {
				month : new Date().getMonth(),
				year : new Date().getFullYear(),
				salesperson : this.props.app.user.id
			}
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);

		this.props.array.removeAll('resultArray');

		axios.get(`/api/query/targetactualsreportquery?month=${this.props.resource.month}&year=${this.props.resource.year}&salesperson=${this.props.resource.salesperson}`).then((response) => {
			if (response.data.message == 'success') {
				response.data.main.map((item) => {
					if(item.periodtype == 'Monthly')
						item.title_period = moment(item.startdate).format("MMM-YYYY");
					else
						item.title_period = moment(item.startdate).format("MMM-YYYY") +' to '+ moment(item.enddate).format("MMM-YYYY");
				});

				this.props.updateFormState(this.props.form, {
					resultArray: response.data.main
				});

				this.props.updateReportFilter('targetactualsreport', this.props.resource);
				if(this.props.resource.resultArray.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	openSalesTarget(data) {
		this.props.openModal({
			render: (closeModal) => {
				return <TargetActualsDetailsModal data={data} app={this.props.app} checkForCurrencyFilter={this.checkForCurrencyFilter} closeModal={closeModal} />
			}, className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	resetFilter () {
		let tempObj = {
			month : null,
			year : null,
			salesperson : null,
			resultArray : null
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Target vs Actuals Report</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									{this.props.app.user.roleid.indexOf(2) == -1 ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Sales Person</label>
										<Field name={'salesperson'} props={{resource: "users", fields: "id,displayname", label: "displayname", filter: "users.issalesperson=true"}} component={selectAsyncEle} validate={[numberNewValidation({required:true, model: "Sales Person"})]}/>
									</div> : null }
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Year</label>
										<Field name={'year'} props={{required : true}} component={NumberEle} validate={[numberNewValidation({required:true, model: "Year"})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Month</label>
										<Field
											name={'month'}
											props={{
												options: [{ value : 0, label : "January" }, { value : 1, label : "February" }, { value : 2, label : "March" }, { value : 3, label : "April" }, { value : 4, label : "May" }, { value : 5, label : "June" }, { value : 6, label : "July" }, { value : 7, label : "August" }, { value : 8, label : "September" }, { value : 9, label : "October" }, { value : 10, label : "November" }, { value : 11, label : "December" }],
												label: "label",
												valuename:"value",
												required: true
											}}
											component={localSelectEle}
											validate={[numberNewValidation({required:true, model: "Month"})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.resultArray} />
							<div className="row">
								<div className="col-md-12 col-xs-12 col-sm-12" style={{paddingLeft:'100px'}}>
									<div className="row col-md-10 col-xs-12 col-sm-12">
										{
											this.props.resource.resultArray && this.props.resource.resultArray.length > 0 ? this.props.resource.resultArray.map((item, index) => {
												return (
													<div className="col-md-6 col-sm-6 col-xs-12" key={index}>
														<div className="card" style={{marginTop:'20px'}}>
															<div className="card-header text-center"><i className={`fa fa-2x fa-${item.baseresource == 'Leads' ? 'bell' : (item.baseresource == 'Orders' ? 'shopping-cart' : 'inr')} pull-left margintop-5`}></i>{item.title}<button type="button" onClick={() => { this.openSalesTarget(item) }} className="btn btn-outline-secondary btn-sm pull-right" disabled={!this.props.valid} title={`Target Split Up by ${item.periodtype == 'Monthly' ? 'Week' : 'Month'}`}><span className="fa fa-table"></span></button><br />({item.title_period})</div>
															<div className="card-body" style={{padding:'0px'}}>
																<div className="col-md-12 row">
																	<div className="col-md-6">
																		<table className="table" style={{whiteSpace:'nowrap'}}>
																			<tbody>
																				<tr>
																					<td>Start On</td>
																					<td>: {dateFilter(item.startdate)}</td>
																				</tr>
																				<tr>
																					<td>End On</td>
																					<td>: {dateFilter(item.enddate)}</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div className="col-md-6 pull-right">
																		<table className="table" style={{whiteSpace:'nowrap'}}>
																			<tbody>
																				<tr>
																					<td>Target</td>
																					<td>: {currencyFilter(item.targetvalue, this.checkForCurrencyFilter(item.targetfield) ? this.props.app.defaultCurrency : 'qty', this.props.app)}</td>
																				</tr>
																				<tr>
																					<td>Achieved</td>
																					<td>: {currencyFilter(item.targetacheived, this.checkForCurrencyFilter(item.targetfield) ? this.props.app.defaultCurrency : 'qty', this.props.app)}</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
																<div className="text-center">
																	<h2 style={{color: (item.periodelapsedpercentage > item.acheivedpercentage) ? 'red' : 'green'}}>{item.acheivedpercentage} %</h2>
																	<h6 style={{marginBottom:'32px'}}>{item.remainingdays>0 ? item.remainingdays : 0} days to go</h6>
																	{(item.baseresource == 'Leads' && item.targetfield != 'leadscount-count') ? <h6 style={{marginTop:'-23px'}}>Pipeline : {currencyFilter(item.pipeline_potentialvalue, this.props.app.defaultCurrency, this.props.app)}</h6> : null}
																</div>
															</div>
														</div>
													</div>
												)
											}) : null
										}
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</>
		);
	};
}

class TargetActualsDetailsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			resultArray: []
		};
		this.onLoad = this.onLoad.bind(this);
	}

	componentWillMount() {
		this.onLoad();
	}

	onLoad() {
		let { resultArray } = this.state;

		axios.get(`/api/query/targetactualsreportquery?salestargetid=${this.props.data.id}`).then((response) => {
			if (response.data.message == 'success') {

				response.data.main.sort((a, b)=>{
					return (new Date(a.splitupenddate) < new Date(b.splitupenddate)) ? -1 : (new Date(a.splitupenddate) > new Date(b.splitupenddate) ? 1 : 0);
				});
				
				resultArray = response.data.main;

				if (resultArray.length > 0) {
					let startDate = new Date(this.props.data.startdate),
					endDate = new Date(this.props.data.enddate),
					diffDays = Math.round(Math.abs((startDate.getTime() - endDate.getTime()) / (24 * 60 * 60 * 1000))) + 1;
					for (let i = 0; i < resultArray.length; i++) {
						let weekStart = new Date(resultArray[i].splitupstartdate),
							weekEnd = new Date(resultArray[i].splitupenddate),
							weekDiffDays = Math.round(Math.abs((weekStart.getTime() - weekEnd.getTime()) / (24 * 60 * 60 * 1000))) + 1;

						if (resultArray[i].periodtype == "Monthly")
							resultArray[i].targetvalue = this.props.data.targetvalue / diffDays * weekDiffDays;
						else if (resultArray[i].periodtype == "Quarterly")
							resultArray[i].targetvalue = this.props.data.targetvalue / 3;
						else if (resultArray[i].periodtype == "Yearly")
							resultArray[i].targetvalue = this.props.data.targetvalue / 12;

						resultArray[i].targetvalue = Math.round(resultArray[i].targetvalue * 100) / 100;

						resultArray[i].targetachievepercent = (resultArray[i].targetacheived / resultArray[i].targetvalue) * 100;

						resultArray[i].targetachievepercent = Math.round(resultArray[i].targetachievepercent * 100) / 100;
						
						if (i == 0) {
							resultArray[i].cumulativetarget = resultArray[i].targetvalue;
							resultArray[i].cumulativeacheived = resultArray[i].targetacheived;
						} else {
							resultArray[i].cumulativetarget = resultArray[i].targetvalue + resultArray[i - 1].cumulativetarget;
							resultArray[i].cumulativetarget = Math.round(resultArray[i].cumulativetarget * 100) / 100;
							resultArray[i].cumulativeacheived = Number(resultArray[i].targetacheived) + Number(resultArray[i - 1].cumulativeacheived);
							resultArray[i].cumulativeacheived = Math.round(resultArray[i].cumulativeacheived * 100) / 100;
						}

						resultArray[i].cumulativepercent = (resultArray[i].cumulativeacheived / resultArray[i].cumulativetarget) * 100;

						resultArray[i].cumulativepercent = Math.round(resultArray[i].cumulativepercent * 100) / 100;
					}
				}
				
				this.setState({ resultArray });
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
		});
	}

	render() {
		let { resultArray } = this.state;
		return (
			<div className="react-outer-modal">
				<div className="react-modal-header">
					<h5 className="modal-title">{this.props.data.title} - ({this.props.data.title_period})</h5>
				</div>
				<div className="react-modal-body" style={{overflowY:'auto',maxHeight:'400px'}}>
					<div className="row">
						<div className="col-md-12">
							<div className="col-md-12 row" style={{padding:'0px'}}>
								<div className="form-group col-md-3 col-sm-6">
									Start On : <b>{dateFilter(this.props.data.startdate)}</b>
								</div>
								<div className="form-group col-md-3 col-sm-6">
									End On : <b>{dateFilter(this.props.data.enddate)}</b>
								</div>
								<div className="form-group col-md-3 col-sm-6">
									Target : <b> {currencyFilter(this.props.data.targetvalue, this.props.checkForCurrencyFilter(this.props.data.targetfield) ? this.props.app.defaultCurrency : 'qty', this.props.app)}</b>
								</div>
								<div className="form-group col-md-3 col-sm-6">
									Achieved : <b>{currencyFilter(this.props.data.targetacheived, this.props.checkForCurrencyFilter(this.props.data.targetfield) ? this.props.app.defaultCurrency : 'qty', this.props.app)}</b>
								</div>
								<div className="col-md-12 text-center">
									<h3 style={{color: (this.props.data.periodelapsedpercentage > this.props.data.acheivedpercentage) ? 'red' : 'green'}}>{this.props.data.acheivedpercentage} %</h3>
									<h6 style={{marginBottom:'32px'}}>{this.props.data.remainingdays>0 ? this.props.data.remainingdays : 0} days to go</h6>
									{(this.props.data.baseresource == 'Leads' && this.props.data.targetfield != 'leadscount-count') ? <h6 style={{marginTop:'-23px'}}>Pipeline : {currencyFilter(this.props.data.pipeline_potentialvalue, this.props.app.defaultCurrency, this.props.app)}</h6> : null}
								</div>
							</div>
						</div>
						<div className="col-md-12">
							<table className="table table-bordered table-hover table-condensed">
								<thead>
									<tr>
										<th colSpan="7" className='text-center'>Split Up by {this.props.data.periodtype == 'Monthly' ? 'Week' : 'Month'}</th>
									</tr>
									<tr>
										<th className='text-center' style={{width:'220px'}}>Period</th>
										<th className='text-center'>Period Target</th>
										<th className='text-center'>Period Achievement</th>
										<th className='text-center'>Period Achievement %</th>
										<th className='text-center'>Cumulative Target</th>
										<th className='text-center'>Cumulative Achievement</th>
										<th className='text-center'>Cumulative Achievement %</th>
									</tr>
								</thead>
								<tbody>
									{resultArray.map((item, index) => {
										return (
											<tr key={index}>
												<td>{item.splituplabel}</td>
												<td className='text-right'>{currencyFilter(item.targetvalue, this.props.checkForCurrencyFilter(item.targetfield) ? this.props.app.defaultCurrency : 'qty', this.props.app)}</td>
												<td className='text-right'>{currencyFilter(item.targetacheived, this.props.checkForCurrencyFilter(item.targetfield) ? this.props.app.defaultCurrency : 'qty', this.props.app)}</td>
												<td className='text-center'>{item.targetachievepercent}</td>
												<td className='text-right'>{currencyFilter(item.cumulativetarget, this.props.checkForCurrencyFilter(item.targetfield) ? this.props.app.defaultCurrency : 'qty', this.props.app)}</td>
												<td className='text-right'>{currencyFilter(item.cumulativeacheived, this.props.checkForCurrencyFilter(item.targetfield) ? this.props.app.defaultCurrency : 'qty', this.props.app)}</td>
												<td className='text-center'>{item.cumulativepercent}</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="react-modal-footer">
					<div className="row">
						<div className="col-md-12 col-sm-12 col-xs-12">
							<div className="muted credit text-center">
								<button type="button" className="btn btn-sm btn-secondary btn-width" onClick={this.props.closeModal}><i className="fa fa-times"></i>Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

TargetActualsReportForm = connect(
	(state, props) => {
		let formName = 'targetactualsreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(TargetActualsReportForm));

export default TargetActualsReportForm;
