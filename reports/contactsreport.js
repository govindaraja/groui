import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import Printmodal from '../components/details/printmodal';
import ReactPaginate from 'react-paginate';

class ContactsReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			contactColumns : [{
					name : '',
					headerformat : 'checkbox',
					key : 'ischecked',
					locked: true,
					format : 'checkbox',
					cellClass : 'text-center',
					onChange : '{report.checkboxOnChange}',
					restrictToExport : true,
					width : 100
				}, {
					name : 'Contact Name',
					key : 'contactname',
					format: 'anchortag',
					transactionname : 'contacts',
					transactionid : 'id',
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Designation',
					key : 'designation',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Email',
					key : 'email',
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Mobile',
					key : 'mobile',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Phone',
					key : 'phone',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Primary',
					key : 'primary',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Source',
					key : 'sourcename',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Territory Name',
					key : 'territoryname',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Industry',
					key : 'industryid_name',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Type',
					key : 'type',
					cellClass : 'text-center',
					width : 150
				}, {
					name : 'Stage',
					key : 'stage',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'First Line',
					key : 'firstline',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Second Line',
					key : 'secondline',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'City',
					key : 'city',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'State',
					key : 'state',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Country',
					key : 'country',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Postal Code',
					key : 'postalcode',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Contact For',
					key : 'contactfor',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Customer / Lead No',
					key : 'customername',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Organization',
					key : 'organization',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Customer Group',
					key : 'customergroup',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Sales Person',
					key : 'salesperson',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Remarks',
					key : 'remarks',
					cellClass : 'text-center',
					width : 300
				}, {
					name : 'Created On',
					key : 'created',
					format : 'date',
					cellClass : 'text-center',
					width : 150
				}
			],
			addressColumns : [{
					name : 'Address For',
					key : 'contactfor',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Customer / Lead No',
					key : 'customername',
					cellClass : 'text-center',
					width : 250
				}, {
					name : 'Customer Group',
					key : 'customergroup',
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Sales Person',
					key : 'salesperson',
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Address Title',
					key : 'title',
					cellClass : 'text-center',
					width : 250
				}, {
					name : 'First Line',
					key : 'firstline',
					cellClass : 'text-center',
					width : 250
				}, {
					name : 'Second Line',
					key : 'secondline',
					cellClass : 'text-center',
					width : 250
				}, {
					name : 'City',
					key : 'city',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'State',
					key : 'state',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Country',
					key : 'country',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Postal Code',
					key : 'postalcode',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Primary',
					key : 'primary',
					cellClass : 'text-center',
					width : 150
				}, {
					name : 'Created On',
					key : 'created',
					format : 'date',
					cellClass : 'text-center',
					width : 150
				}
			],
			contactidarray: [],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.showOnChange = this.showOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.printfunction = this.printfunction.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.handlePaginationClick = this.handlePaginationClick.bind(this);
		this.renderPagination = this.renderPagination.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let { contactColumns, addressColumns } = this.state;
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				companyid : [this.props.app.user.selectedcompanyid],
				companyArray : JSON.parse(JSON.stringify(this.props.app.companyArray)),
				filters : {},
				hiddencols : [],
				columns : [],
				show : 'Contacts',
				skip : 0,
				totalcount : 0
			};

		customfieldAssign(contactColumns, null, 'contacts', this.props.app.myResources);

		this.setState({ contactColumns });

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');

		let filterString;

		if (this.props.resource.show == 'Contacts')
			filterString = `pagelength=500&skip=${(this.props.resource.skip * 500)}&show=${this.props.resource.show}`;
		else
			filterString = `show=${this.props.resource.show}`;

		axios.get(`/api/query/contactsreportquery?${filterString}`).then((response) => {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.main.length; i++) {
					for(var prop in response.data.main[i]) {
						response.data.main[i][prop] = response.data.main[i][prop] == null ? "" : response.data.main[i][prop];
					}
				}

				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns: this.props.resource.show == 'Contacts' ? this.state.contactColumns : this.state.addressColumns,
					hiddencols,
					totalcount: response.data.count
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}
	
	printfunction = () => {
		this.props.openModal({
			render: (closeModal) => {
				let contactidObj = {
					id: this.state.contactidarray[0],
					contactidarray : this.state.contactidarray
				};

				return <Printmodal app={this.props.app} resourcename={'contacts'} companyid={this.props.resource.companyid}  openModal={this.props.openModal} closeModal={closeModal} resource={contactidObj}/>
			}, className: {content: 'react-modal-custom-class', overlay: 'react-modal-overlay-custom-class'}
		});
	}

	resetFilter () {

		let tempObj = {
			show : 'Contacts',
			originalRows: null,
			rows: [],
			filters: {},
			hiddencols: [],
			columns: [],
			skip : 0,
			totalcount : 0
		};

		this.props.updateFormState(this.props.form, tempObj);
	}
	
	checkboxOnChange(value, item) {
		let contactidarray = [];

		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		setTimeout(() => {
			this.props.resource.originalRows.forEach((rowItem, index) => {
				if(rowItem.ischecked)
					contactidarray.push(rowItem.id);
			});
			this.setState({contactidarray});
		}, 0);
		this.refs.grid.refresh();
	}

	checkboxHeaderOnChange(param) {
		let contactidarray = [];
		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = false;
			if(param) {
				item.ischecked = true;
				contactidarray.push(item.id);
			}
		});
		setTimeout(() => {
			this.props.updateFormState(this.props.form, {
				originalRows: this.props.resource.originalRows
			});
			this.setState({contactidarray});
		}, 0);
		this.refs.grid.forceRefresh();
	}

	showOnChange() {
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('columns');

		this.props.updateFormState(this.props.form, {
			filters: {}
		});
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	renderPagination () {
		if (this.props.resource && Math.ceil(this.props.resource.totalcount / 500) > 1)
			return <ReactPaginate previousLabel={<span>&laquo;</span>}
				nextLabel={<span>&raquo;</span>}
				breakLabel={<a></a>}
				breakClassName={"break-me"}
				pageCount={Math.ceil(this.props.resource.totalcount / 500)}
				marginPagesDisplayed={0}
				pageRangeDisplayed={5}
				onPageChange={this.handlePaginationClick}
				containerClassName={"list-pagination"}
				subContainerClassName={"pages pagination"}
				activeClassName={"active"}
				forcePage={this.props.resource.skip} />
		return null;
	}

	handlePaginationClick (obj) {
		this.setState({contactidarray: []}, ()=>{
			this.props.updateFormState(this.props.form, {
				skip : obj.selected,
				isallchecked: false
			});
		});
		setTimeout(() => {
			this.getReportData();
		}, 0);
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Contacts Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && this.state.contactidarray.length > 0 ? <button type="button" className="btn btn-sm gs-btn-outline-success marginleft-15" onClick={this.printfunction} rel="tooltip" title="Print"><span className="fa fa-print"></span></button> : null }
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: ["Contacts", "Addresses"], required: true, onChange: () => {this.showOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Show'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname={`${this.props.resource.show == 'Contacts' ? 'Contact Report' : 'Address Report'}`} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} /> : null }
							{this.props.resource.show == 'Contacts' ? <div className="gs-report-pagination paddingtop-10 paddingleft-30">{this.renderPagination()}</div>: null}
						</div>
					</div>
				</form>
			</>
		);
	};
}

ContactsReportForm = connect(
	(state, props) => {
		let formName = 'contactsreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ContactsReportForm));

export default ContactsReportForm;
