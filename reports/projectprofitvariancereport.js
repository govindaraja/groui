import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class ProjectProfitVarianceReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.projectOnChange = this.projectOnChange.bind(this);
		this.getFooterValue = this.getFooterValue.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Internal Ref No",
					"key" : "internalrefno",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Client Ref No",
					"key" : "clientrefno",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "BOQ Item Name",
					"key" : "itemid_name",
					"width" : 180,
				}, {
					"name" : "BOQ Description",
					"key" : "description",
					"width" : 200
				}, {
					"name" : "Revenue",
					"key" : "revenue",
					"format" : "currency",
					"cellClass" : "text-right",
					"footertype" : "sum",
					"width" : 150
				}, {
					"name" : "Estimated Cost",
					"key" : "estimatedvalue",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Actual Cost",
					"key" : "actualvalue",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Estimated Margin",
					"key" : "estimatedmargin",
					"width" : 180,
					"footertype" : "calculated",
					"footerCalculation" : "{(colarr, aggarr) => report.getFooterValue('estimatedmargin', colarr, aggarr)}"
				}, {
					"name" : "Actual Margin",
					"key" : "actualmargin",
					"width" : 180,
					"footertype" : "calculated",
					"footerCalculation" : "{(colarr, aggarr) => report.getFooterValue('actualmargin', colarr, aggarr)}"
				}]
			};
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
	
		axios.get(`/api/query/projectprofitvariancereportquery?projectid=${this.props.resource.projectid}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			projectid : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	projectOnChange(value, valueobj) {
		this.props.updateFormState(this.props.form, {
			projectid_status : valueobj.status
		});
	}

	getFooterValue(prop, colarr, aggarr) {
		if(prop == 'estimatedmargin' || prop == 'actualmargin') {
			let costvalue = 0;
			let salevalue = 0;
			colarr.forEach((col, colindex) => {
				if(col.key == 'revenue')
					salevalue = aggarr[colindex].footerAggValue;

				if(col.key == 'estimatedvalue' && prop == 'estimatedmargin')
					costvalue = aggarr[colindex].footerAggValue;

				if(col.key == 'actualvalue' && prop == 'actualmargin')
					costvalue = aggarr[colindex].footerAggValue;
			});

			if(costvalue > 0 && salevalue > 0)
				return Number((((salevalue - costvalue) / salevalue) * 100).toFixed(2));

			return null;
		}
		return null;
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Project Profit Variance Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Project</label>
										<Field name={'projectid'} props={{resource: "projects", fields: "id,projectname,projectno,displayname,status", label: "displayname", filter: "projects.status in ('Approved', 'Completed')", required: true, onChange: (value, valueobj) => {this.projectOnChange(value, valueobj)}}} component={autoSelectEle} validate={[numberNewValidation({required: true, model: 'Project'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && this.props.resource.projectid_status != 'Completed' ? <div className="col-md-12 col-sm-12 paddingleft-30"><span>Project is not completed yet. All costs may not be captured yet. Actual Margin shown may be higher due to this.</span></div> : null}
							{this.props.resource ? <Reactuigrid excelname='Project Cost Variance Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

ProjectProfitVarianceReportForm = connect(
	(state, props) => {
		let formName = 'projectprofitvariancereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ProjectProfitVarianceReportForm));

export default ProjectProfitVarianceReportForm;