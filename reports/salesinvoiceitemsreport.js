import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, BIReportSelectField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import ReactBIChart from '../components/reportbicomponents';
import { currencyFilter, booleanfilter, taxFilter, dateFilter, datetimeFilter, timeFilter, uomFilter } from '../utils/filter';

class SalesInvoiceItemsReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showMoreFilter: false,
			filterToggleOpen: true,
			invoiceItemColumns: [{
				"name" : "Voucher Type",
				"key" : "vouchertype",
				"width" : 180
			}, {
				"name" : "Voucher No",
				"key" : "invoiceno",
				"format" : "anchortag",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Voucher Date",
				"key" : "invoicedate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Customer",
				"key" : "customer",
				"width" : 250
			}, {
				"name" : "Customer State",
				"key" : "customerstate",
				"width" : 150
			}, {
				"name" : "Customer GST Reg Type",
				"key" : "customergstregtype",
				"width" : 180
			}, {
				"name" : "Customer GSTIN",
				"key" : "customergstin",
				"width" : 180
			}, {
				"name" : "Item Name",
				"key" : "itemid_name",
				"width" : 200
			}, {
				"name" : "HSN Code",
				"key" : "hsncode",
				"width" : 180
			}, {
				"name" : "Quantity",
				"key" : "quantity",
				"footertype" : "sum",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "UOM",
				"key" : "uom",
				"width" : 180
			}, {
				"name" : "Currency",
				"key" : "currencyname",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Currency Exchange Rate",
				"key" : "currencyexchangerate",
				"if" : this.props.app.feature.useMultiCurrency,
				"width" : 180
			}, {
				"name" : "Rate",
				"key" : "rate",
				"format" : "currency",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Discount",
				"key" : "discountquantity",
				"format" : "discount",
				"width" : 150
			}, {
				"name" : "Value Before Discount",
				"key" : "valuebeforediscount",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Discount Value",
				"key" : "discountvalue",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Value Before Tax",
				"key" : "amount",
				"format" : "currency",
				"cellClass" : "text-right",
				"footertype" : "sum",
				"width" : 160
			}, {
				"name" : "Value (Local Currency)",
				"key" : "amountlc",
				"format" : "defaultcurrency",
				"if" : this.props.app.feature.useMultiCurrency,
				"cellClass" : "text-right",
				"footertype" : "sum",
				"width" : 160
			}, {
				"name" : "Value After Tax",
				"key" : "amountwithtax",
				"format" : "currency",
				"cellClass" : "text-right",
				"footertype" : "sum",
				"width" : 160
			}, {
				"name": "Income Account",
				"key": "incomeaccountid_name",
				"cellClass": "text-center",
				"width": 180
			}, {
				"name" : "Item Description",
				"key" : "description",
				"width" : 300
			}, {
				"name" : "Contact Person",
				"key" : "contactperson",
				"width" : 150
			}, {
				"name" : "Status",
				"key" : "status",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Sales Person",
				"key" : "salesperson_displayname",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Item Category",
				"key" : "itemcategorymasterid_name",
				"width" : 180
			}, {
				"name" : "Item Group",
				"key" : "itemgroupid_fullname",
				"width" : 180
			}, {
				"name" : "Commodity Code",
				"key" : "itemid_commoditycode",
				"width" : 180
			}, {
				"name" : "Tariff Code",
				"key" : "itemid_tariffcode",
				"width" : 180
			}, {
				"name" : "Remarks",
				"key" : "remarks",
				"width" : 180
			}, {
				"name" : "Customer group",
				"key" : "customergroupid_name",
				"width" : 180
			}, {
				"name" : "Territory",
				"key" : "territoryname",
				"width" : 180
			}, {
				"name" : "Industry",
				"key" : "industryid_name",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Numbering Series",
				"key" : "numberingseriesmasterid_name",
				"width" : 160
			}, {
				"name" : "Tax",
				"key" : "taxid",
				"format" : "taxFilter",
				"width" : 180
			}, {
				"name" : "Delivery / Site Address",
				"key" : "deliveryaddress",
				"width" : 200
			}, {
				"name" : "Billing Address",
				"key" : "billingaddress",
				"width" : 200
			}, {
				"name" : "Customer Reference",
				"key" : "customerreference",
				"width" : 180
			}, {
				"name" : "Delivery Note No",
				"key" : "deliverynotenumber",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Delivery Note Date",
				"key" : "deliverynotedate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Delivered Warehouse",
				"key" : "deliveredwarehouse",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : 'Project Name',
				"key" : 'projectid_projectname',
				"if" : this.props.app.feature.useProjects,
				"width" : 250
			}, {
				"name" : 'Project No',
				"key" : 'projectid_projectno',
				"if" : this.props.app.feature.useProjects,
				"width" : 150
			}, {
				"name" : 'Customer Legal Name',
				"key" : 'customerlegalname',
				"width" : 200
			}],
			invoiceColumns: [{
				"name" : "Voucher Type",
				"key" : "vouchertype",
				"width" : 180
			}, {
				"name" : "Voucher No",
				"key" : "invoiceno",
				"format" : "anchortag",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Voucher Date",
				"key" : "invoicedate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Customer",
				"key" : "customer",
				"width" : 250
			}, {
				"name" : "Customer State",
				"key" : "customerstate",
				"width" : 150
			}, {
				"name" : "Customer GST Reg Type",
				"key" : "customergstregtype",
				"width" : 180
			}, {
				"name" : "Customer GSTIN",
				"key" : "customergstin",
				"width" : 180
			}, {
				"name" : "Sales Person",
				"key" : "salesperson_displayname",
				"width" : 150
			}, {
				"name" : "Currency",
				"key" : "currencyname",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Currency Exchange Rate",
				"key" : "currencyexchangerate",
				"if" : this.props.app.feature.useMultiCurrency,
				"width" : 180
			}, {
				"name" : "Value Before Tax",
				"key" : "basictotal",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Value (Local Currency)",
				"key" : "basictotallc",
				"format" : "defaultcurrency",
				"if" : this.props.app.feature.useMultiCurrency,
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Value After Tax",
				"key" : "finaltotal",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Status",
				"key" : "status",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Due Date",
				"key" : "paymentduedate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 160
			}, {
				"name" : "Outstanding Amount",
				"key" : "outstandingamount",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Territory",
				"key" : "territoryname",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Customer Group",
				"key" : "customergroupid_name",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Price List",
				"key" : "pricelistid_name",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Industry",
				"key" : "industryid_name",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Numbering Series",
				"key" : "numberingseriesmasterid_name",
				"cellClass" : "text-center",
				"width" : 150
			}, {
				"name" : "Delivery / Site Address",
				"key" : "deliveryaddress",
				"width" : 180
			}, {
				"name" : "Billing Address",
				"key" : "billingaddress",
				"width" : 180
			}, {
				"name" : "Customer Reference",
				"key" : "customerreference",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : 'Project Name',
				"key" : 'projectid_projectname',
				"if" : this.props.app.feature.useProjects,
				"width" : 250
			}, {
				"name" : 'Project No',
				"key" : 'projectid_projectno',
				"if" : this.props.app.feature.useProjects,
				"width" : 150
			}, {
				"name" : 'Customer Legal Name',
				"key" : 'customerlegalname',
				"width" : 200
			}, {
				"name" : 'Payment Terms',
				"key" : 'paymentterms',
				"cellClass" : "text-center",
				"width" : 200
			}]
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateMoreFilter = this.updateMoreFilter.bind(this);
		this.resourceOnChange = this.resourceOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.configOnChange = this.configOnChange.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	updateMoreFilter() {
		this.setState({showMoreFilter : true});
	}

	onLoad() {
		let { invoiceColumns, invoiceItemColumns } = this.state;
		let fromdate = new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)));
		let fromDate = new Date(fromdate.setDate(fromdate.getDate() + 1));
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : fromDate,
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				period : 'Last month',
				resource : 'salesinvoiceitems',
				reporttype : 'Normal',
				status : ["Approved", "Sent To Customer"],
				filters: {},
				hiddencols: [],
				columns: [],
				pivotobj: {}
			};

		customfieldAssign(invoiceColumns, invoiceItemColumns, 'salesinvoices', this.props.app.myResources, true);
		customfieldAssign(invoiceItemColumns, null, 'salesinvoiceitems', this.props.app.myResources, true);

		this.setState({invoiceColumns, invoiceItemColumns});

		this.props.initialize(tempObj);

		setTimeout(() => {
			if(this.props.reportdata)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let { invoiceColumns, invoiceItemColumns } = this.state;

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['fromdate', 'todate', 'itemid', 'itemcategoryid', 'customerid', 'customergroupid', 'resource', 'reporttype', 'configid', 'salesperson', 'team', 'itemgroupid', 'numberingseriesmasterid', 'status'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});

		axios.get(`/api/query/salesinvoiceitemsreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				invoiceColumns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});
				invoiceItemColumns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns: this.props.resource.resource == 'salesinvoices' ? invoiceColumns : invoiceItemColumns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			period : 'Custom',
			resource : null,
			reporttype : null,
			status : null,
			customerid : null,
			customergroupid : null,
			itemid : null,
			itemgroupid : null,
			itemcategoryid : null,
			numberingseriesmasterid : null,
			team : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: [],
			pivotobj: {}
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	resourceOnChange() {
		this.props.updateFormState(this.props.form, {
			configid : null,
			configid_name : null,
			pivotobj: {},
			originalRows : [],
			columns : []
		});
	}

	openTransaction(data) {
		if (data.vouchertype == 'Service Invoice') {
			this.props.history.push(`/details/serviceinvoices/${data.id}`);
		} else if (data.vouchertype == 'Contract Invoice') {
			this.props.history.push(`/details/contractinvoices/${data.id}`);
		} else if(data.vouchertype == 'Commercial Invoice') {
			this.props.history.push(`/details/salesinvoices/${data.id}`);
		} else {
			this.props.history.push(`/details/creditnotes/${data.id}`);
		}
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	configOnChange = (value, valueobj) => {
		let tempObj = {
			originalRows: [],
			columns : [],
			pivotobj: {}
		};
		for (var prop in valueobj.config) {
			tempObj.pivotobj[prop] = valueobj.config[prop];
		}
		tempObj.configid_name = valueobj.name;
		this.props.updateFormState(this.props.form, tempObj);
	};

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Sales Invoice Items Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.reporttype != "BI" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
								{this.props.resource.reporttype == "BI" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.pivot.print()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-print"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Report Data</label>
										<Field name={'resource'} props={{options: [{value: "salesinvoices", label: "Sales Invoices"}, {value: "salesinvoiceitems", label: "Invoice Items"}], label:"label", valuename: "value", required: true, onChange: () => {this.resourceOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Report Data'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Report Type</label>
										<Field name={'reporttype'} props={{options: [{value: "Normal", label: "Normal"}, {value: "BI", label: "Analytics"}], label : "label", valuename: "value", required: true, onChange: () => {this.resourceOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required:  true, title : 'Report Type'})]} />
									</div>
									{this.props.resource.reporttype == "BI" ? <div className="form-group col-md-12 col-sm-12">
										<label>Report Name</label>
										<BIReportSelectField parentobj={this.props.resource} reportname='salesinvoiceitemsreport' onChangeFn={this.configOnChange} updateFormState={this.props.updateFormState} form={this.props.form} createOrEdit={this.props.createOrEdit}  />
									</div> : null}
									<ReportDateRangeField model={["period", "fromdate", "todate"]} updateFormState={this.props.updateFormState} form={this.props.form} resource={this.props.resource} required={"false"} />
									{!this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<span className="gs-anchor" style={{lineHeight: '5.5'}} onClick={this.updateMoreFilter}>More Filters</span>
									</div> : null }
									{this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Status</label>
										<Field name={'status'} props={{options: ["Draft", "Submitted", "Revise", "Approved", "Sent To Customer", "Cancelled"], multiselect: true}} component={localSelectEle} />
									</div> : null }
									{this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer Name</label>
										<Field name={'customerid'} props={{resource: "partners", fields: "id,name,displayname", label: "displayname", displaylabel: "name"}} component={autoSelectEle} />
									</div> : null }
									{this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer Group</label>
										<Field name={'customergroupid'} props={{resource: "customergroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div> : null }
									{this.state.showMoreFilter && this.props.resource.resource == "salesinvoiceitems" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Name</label>
										<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name"}} component={autoSelectEle} />
									</div> : null }
									{this.state.showMoreFilter && this.props.resource.resource == "salesinvoiceitems" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Group</label>
										<Field name={'itemgroupid'} props={{resource: "itemgroups", fields: "id,fullname", label: "fullname"}} component={autoSelectEle} />
									</div> : null }
									{this.state.showMoreFilter && this.props.resource.resource == "salesinvoiceitems" ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Item Category</label>
										<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name"}} component={autoSelectEle} />
									</div> : null }
									{this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Numbering Series</label>
										<Field name={'numberingseriesmasterid'} props={{resource: "numberingseriesmaster", fields: "id,name,format,isdefault,currentvalue", filter: "numberingseriesmaster.resource='salesinvoices'  or numberingseriesmaster.resource='creditnotes'"}} component={selectAsyncEle} />
									</div> : null }
									{this.state.showMoreFilter ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Team</label>
										<Field name={'team'} props={{resource: "teamstructure", fields: "id,fullname", label: "fullname"}} component={selectAsyncEle} />
									</div> : null }
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.reporttype != "BI" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname={`${this.props.resource.resource == 'salesinvoiceitems' ? 'Sales Invoice Items Report' : 'Sales Invoices Report'}`} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }

							{this.props.resource.reporttype == "BI" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <div className='report-biheader-title'>{this.props.resource.configid_name}</div> : null }
							{this.props.resource.reporttype == "BI" && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && this.props.resource.configid > 0 ? <ReactBIChart chartprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} app={this.props.app} ref="pivot" /> : null}
						</div>
					</div>
				</form>
			</>
		);
	};
}

SalesInvoiceItemsReportForm = connect(
	(state, props) => {
		let formName = 'salesinvoiceitemsreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(SalesInvoiceItemsReportForm));

export default SalesInvoiceItemsReportForm;
