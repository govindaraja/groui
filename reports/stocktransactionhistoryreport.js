import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField, checkboxEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class StockTransactionHistoryReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true,
			summaryColumnsArr : [{
				"name" : "Item Name",
				"key" : "itemname",
				"locked": true,
				"width" : 200
			}, {
				"name" : "Item Category Name",
				"key" : "itemcategorymasterid_name",
				"width" : 200
			}, {
				"name" : "Item Group",
				"key" : "itemgroupid_fullname",
				"width" : 250
			}, {
				"name" : "Lead Time",
				"key" : "leadtime",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "Safety Stock",
				"key" : "safetystock",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "UOM",
				"key" : "uom",
				"width" : 150
			}, {
				"name" : "Opening Qty",
				"key" : "openingqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "Opening Unit Rate",
				"key" : "openingunitrate",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "Opening Value",
				"key" : "openingvalue",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"footertype" : "sum",
				"width" : 180
			}, {
				"name" : "Stock In Qty",
				"key" : "stockinqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Stock In Avg Rate",
				"key" : "stockinavgvalue",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Stock In Value",
				"key" : "stockinvalue",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"footertype" : "sum",
				"width" : 150
			}, {
				"name" : "Stock Out Qty",
				"key" : "stockoutqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Stock Out Avg Rate",
				"key" : "stockoutavgvalue",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"width" : 160
			}, {
				"name" : "Stock Out Value",
				"key" : "stockoutvalue",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"footertype" : "sum",
				"width" : 160
			}, {
				"name" : "Closing Qty",
				"key" : "closingqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Closing Unit Rate",
				"key" : "closingavgvalue",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Closing Value",
				"key" : "closingvalue",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"footertype" : "sum",
				"width" : 180
			}],
			detailsColumnsArr : [{
				"name" : "Warehouse",
				"key" : "warehouse",
				"cellClass" : "text-center",
				"width" : 200
			}, {
				"name" : "Date",
				"key" : "postingdate",
				"format" : "datetime",
				"width" : 200
			}, {
				"name" : "Open Qty",
				"key" : "openqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Transaction Qty",
				"key" : "transqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "Closing Qty",
				"key" : "closingqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 150
			}, {
				"name" : "Source Type",
				"key" : "sourcetype",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Source",
				"key" : "source",
				"format" : "anchortag",
				"width" : 150
			}, {
				"name" : "Partner",
				"key" : "partner",
				"cellClass" : "text-center",
				"width" : 230
			}, {
				"name" : "Remarks",
				"key" : "remarks",
				"width" : 180
			}, {
				"name" : "Value Before",
				"key" : "valuebefore",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Value Now",
				"key" : "valuenow",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"footertype" : "sum",
				"width" : 180
			}, {
				"name" : "Value After",
				"key" : "valueafter",
				"format" : "defaultcurrency",
				"cellClass" : "text-right",
				"width" : 150
			}]
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.showOnChange = this.showOnChange.bind(this);
		this.getWarehouseDetails = this.getWarehouseDetails.bind(this);
		this.itemCallback = this.itemCallback.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.callBackIncludeProjectwarehouse = this.callBackIncludeProjectwarehouse.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth((new Date().getMonth() - 1))).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				filters: {},
				hiddencols: [],
				warehouseArray: [],
				activeitem : 'Active Item',
				warehouseid: [],
				show: 'Summary',
				includeallitems: false,
				reportbasedon: 'postingdate',
				columns: []
			};
		}

		this.props.initialize(tempObj);

		setTimeout(() => {
			this.getWarehouseDetails();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getWarehouseDetails() {
		this.updateLoaderFlag(true);
		axios.get(`/api/stocklocations?field=id,name,projectid&filtercondition=stocklocations.isparent AND stocklocations.parentid IS NULL  AND stocklocations.projectid is null`).then((response) => {
			if (response.data.message == 'success') {
				for(var i = 0; i < response.data.main.length; i++) {
					this.props.array.push('warehouseArray', response.data.main[i]);
				}

				if(this.props.reportdata) {
					this.getReportData();
				} else {
					this.props.array.removeAll('warehouseid');
					let warehouseid = [];
					for (var i = 0; i < response.data.main.length; i++)
							warehouseid.push(response.data.main[i].id);

					this.props.updateFormState(this.props.form, {
						warehouseid
					});
				}
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	callBackIncludeProjectwarehouse() {
		this.updateLoaderFlag(true);
		let queryString = `/api/stocklocations?field=id,name,projectid&filtercondition=stocklocations.isparent AND stocklocations.parentid IS NULL `;

		if(!this.props.resource.includeprojectwarehouse) {
			queryString += ` AND stocklocations.projectid is null`;
		}

		axios.get(`${queryString}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.array.removeAll('warehouseArray');
				this.props.array.removeAll('warehouseid');

				let warehouseid = [];
				for (var i = 0; i < response.data.main.length; i++)
					if(response.data.main[i].projectid == null)
						warehouseid.push(response.data.main[i].id);

				this.props.updateFormState(this.props.form, {
					warehouseid,
					warehouseArray : response.data.main
				});
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['warehouseid', 'itemid', 'fromdate', 'todate', 'reportbasedon', 'show', 'activeitem', 'includeallitems'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/stockhistroyquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let resultArray = response.data.main;

				if(this.props.resource.show == "Details") {
					if (resultArray.length > 0) {
						resultArray[0].openqty = response.data.startqty;
						resultArray[0].closingqty = Number((response.data.startqty + resultArray[0].transqty).toFixed(this.props.app.roundOffPrecisionStock));
						resultArray[0].valuebefore = response.data.startvalue;
						resultArray[0].valueafter = Number((response.data.startvalue + resultArray[0].valuenow).toFixed(this.props.app.roundOffPrecision));
					}

					for (var i = 1; i < resultArray.length; i++) {
						resultArray[i].openqty = resultArray[i - 1].closingqty;
						resultArray[i].closingqty = Number((resultArray[i].openqty + resultArray[i].transqty).toFixed(this.props.app.roundOffPrecisionStock));
						resultArray[i].valuebefore = resultArray[i - 1].valueafter;
						resultArray[i].valueafter = Number((resultArray[i].valuebefore + resultArray[i].valuenow).toFixed(this.props.app.roundOffPrecision));
					}

					for (var i = 0; i < resultArray.length; i++) {
						if (resultArray[i].relatedresource == 'deliverynotes') {
							resultArray[i].sourcetype = 'Delivery Note';
							resultArray[i].source = resultArray[i].deliverynotenumber;
							resultArray[i].remarks = resultArray[i].deliveryremarks;
						} else if (resultArray[i].relatedresource == 'receiptnotes') {
							resultArray[i].sourcetype = 'Receipt Note';
							resultArray[i].source = resultArray[i].receiptnotenumber;
							resultArray[i].remarks = resultArray[i].receiptremarks;
						}
					}
				}

				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						tempObj.hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: resultArray,
					columns: this.props.resource.show == 'Summary' ? this.state.summaryColumnsArr : this.state.detailsColumnsArr,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			warehouseid: [],
			show: null,
			itemid: null,
			reportbasedon: null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			activeitem : null,
			columns: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	itemCallback(value, valueobj) {
		this.props.updateFormState(this.props.form, {
			itemid_name: valueobj.name,
			itemid_hasbatch: valueobj.hasbatch,
			uomid: valueobj.stockuomid,
			uomid_name: valueobj.stockuomid_name
		});
	}

	showOnChange() {
		this.props.array.removeAll('originalRows');
	}

	openTransaction(data) {
		this.props.history.push(`/details/${data.relatedresource}/${data.relatedid}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Stock Transaction History Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: ["Summary", "Details"], required: true, onChange: () => {this.showOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required:true, model: 'Show'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Warehouse</label>
										<Field name={'warehouseid'} props={{options: this.props.resource.warehouseArray, label: "name", valuename: "id", required: true, multiselect: true}} component={localSelectEle} validate={[numberNewValidation({required: true, model: 'Warehouse'})]} />
									</div>
									{this.props.app.feature.useProjects ? <div className="form-group col-md-12 col-sm-12 d-flex">
										<label className="labelclass">Include Project Warehouse</label>
										<Field name={'includeprojectwarehouse'} props={{onChange: (value) => this.callBackIncludeProjectwarehouse(value)}} component={checkboxEle}/>
									</div>: null }
									{this.props.resource.show == 'Summary' ? <div className="form-group col-md-12 col-sm-12 d-flex">
										<label className="labelclass">Include All Items</label>
										<Field name={'includeallitems'} component={checkboxEle}/>
									</div>: null }
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Based On</label>
										<Field name={'reportbasedon'} props={{options: [{value: "postingdate", label: "Posting Date"}, {value: "transactiondate", label: "Transaction Date"}], label: "label", valuename:"value", required: true}} component={localSelectEle} validate={[stringNewValidation({required:true, model: 'Based On'})]}/>
									</div>
									{this.props.resource.show=='Summary' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Active</label>
										<Field name={'activeitem'} props={{options: ["All", "Active Item", "Inactive Item"], required: true}} component={localSelectEle} validate={[stringNewValidation({required:true, model: 'Active'})]}/>
									</div> : null}
									<div className="form-group col-md-12 col-sm-12" style={{marginBottom: '150px'}}>
										<label className="labelclass">Item Name</label>
										<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name,stockuomid,uom/name/stockuomid", onChange: (value, valueobj) => {this.itemCallback(value, valueobj)}, required: this.props.resource.show == 'Details' ? true : false}} component={autoSelectEle} validate={[numberNewValidation({required: "{resource.show == 'Details' ? true : false}", model: 'Item Name'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Stock Transaction History Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

StockTransactionHistoryReportForm = connect(
	(state, props) => {
		let formName = 'stocktransactionhistoryreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(StockTransactionHistoryReportForm));

export default StockTransactionHistoryReportForm;
