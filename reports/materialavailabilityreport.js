import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class MaterialAvailabilityReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			materialAvailabilityColumns : [{
				"name" : "",
				"headerformat" : "checkbox",
				"key" : "ischecked",
				"locked" : true,
				"format" : "checkbox",
				"cellClass" : "text-center",
				"onChange" : "{report.checkboxOnChange}",
				"restrictToExport" : true,
				"showfield" : "item.status == 'Shortage'",
				"width" : 100
			}, {
				"name" : "Prod Order No",
				"key" : "orderno",
				"format" : "anchortag",
				"transactionname" : "productionorders",
				"transactionid" : "productionorderid",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Prod Order Date",
				"key" : "orderdate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Planned Start",
				"key" : "plannedstartdate",
				"format" : "date",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : 'Production Item Name',
				"key" : 'productionitemname',
				"width" : 200
			}, {
				"name" : "Customer",
				"key" : "customer",
				"width" : 180
			}, {
				"name" : "Sales Order No",
				"key" : "salesorderno",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Status",
				"key" : "status",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Item Name",
				"key" : "itemid_name",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Qty Required",
				"key" : "qtyrequired",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Qty Reserved",
				"key" : "qtyreserved",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Qty in PO",
				"key" : "poqty",
				"format" : "number",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Shortage",
				"key" : "shortage",
				"cellClass" : "text-center",
				"width" : 180
			}, {
				"name" : "Default Purchase Cost",
				"key" : "defaultpurchasecost",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}, {
				"name" : "Moving Average",
				"key" : "marate",
				"format" : "currency",
				"footertype" : "sum",
				"cellClass" : "text-right",
				"width" : 180
			}],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.createPO = this.createPO.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let { materialAvailabilityColumns } = this.state;
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				filters: {},
				hiddencols: [],
				columns: []
			};

		customfieldAssign(materialAvailabilityColumns, null, 'productionorders', this.props.app.myResources);

		this.setState({materialAvailabilityColumns});

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let { materialAvailabilityColumns } = this.state;

		axios.get(`/api/query/materialavailabilityreportquery?show=${this.props.resource.show}`).then((response) => {
			if (response.data.message == 'success') {
				materialAvailabilityColumns.forEach(column => {
					if(['itemid_name', 'qtyrequired', 'qtyreserved', 'poqty', 'shortage', 'defaultpurchasecost', 'marate', 'ischecked'].includes(column.key))
						column.if = this.props.resource.show == 'Item Wise' ? true : false;
				});

				let hiddencols = [];
				materialAvailabilityColumns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main.result,
					columns: materialAvailabilityColumns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			show: "",
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	checkboxOnChange(value, item) {
		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		this.refs.grid.refresh();
	}

	checkboxHeaderOnChange(param) {
		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = param && item.status == 'Shortage' ? true : false;
		});
		setTimeout(() => {
			this.props.updateFormState(this.props.form, {
				originalRows: this.props.resource.originalRows
			});
		}, 0);
		this.refs.grid.forceRefresh();
	}

	createPO() {
		let itemArray = [],
		errorFound = false;
		this.props.resource.originalRows.forEach((item) => {
			if (item.ischecked && item.status == 'Shortage') {
				item.quantity = item.shortage;
				itemArray.push(item);
			}
		});

		for (var i = 0; i < itemArray.length; i++) {
			if (!(itemArray[i].quantity > 0)) {
				errorFound = true;
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : `${itemArray[i].productionitemname} Quantity should greater than zero`,
					btnArray : ['Ok']
				}));
				break;
			}
		}

		if (!errorFound) {
			if (itemArray.length > 0) {
				let tempObj = {...itemArray[0]};
				tempObj.purchaseItemArray = itemArray;
				this.props.history.push({pathname: '/createPurchaseOrder', params: {...tempObj, param: 'Purchase Planner'}});
			} else
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please Choose atleast one item to purchase',
					btnArray : ['Ok']
				}));
		}
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Material Availability Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0&& this.props.resource.show == 'Item Wise' ? <div className="report-header-rightpanel">
								{checkActionVerbAccess(this.props.app, 'purchaseorders', 'Save') ? <button type="button" className="btn gs-btn-success btn-sm btn-width" onClick={() => this.createPO()}><i className="fa fa-plus"></i>PO</button> : null}
							</div> : null}
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: ["Summary", "Item Wise"], required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Show'})]} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Material Availability Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

MaterialAvailabilityReportForm = connect(
	(state, props) => {
		let formName = 'materialavailabilityreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(MaterialAvailabilityReportForm));

export default MaterialAvailabilityReportForm;
