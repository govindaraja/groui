import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class ProjectsProfitabilityReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.getFooterValue = this.getFooterValue.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth((new Date().getMonth() - 1))).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Project No",
					"key" : "projectno",
					"format" : "anchortag",
					"transactionname" : "projects",
					"transactionid" : "id",
					"cellClass" : "text-center",
					"width" : 150,
					"locked" : true
				}, {
					"name" : "Project Name",
					"key" : "projectname",
					"width" : 150,
					"locked" : true
				}, {
					"name" : "Project Date",
					"key" : "startdate",
					"width" : 150,
					"cellClass" : "text-center",
					"format" : "date"
				}, {
					"name" : "Customer",
					"key" : "customername",
					"width" : 250
				}, {
					"name" : "Status",
					"key" : "status",
					"cellClass" : "text-center",
					"width" : 200
				}, {
					"name" : "Salesperson",
					"key" : "salesperson",
					"width" : 160
				}, {
					"name" : "Project Value Before Tax",
					"key" : "projectvaluebeforetax",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 200
				}, {
					"name" : "Project Value After Tax",
					"key" : "projectvalueaftertax",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 200
				}, {
					"name" : "Advances Received",
					"key" : "advancereceipt",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Invoiced Amount Before Tax",
					"key" : "invoicevaluebeforetax",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 200
				}, {
					"name" : "Invoiced Amount After Tax",
					"key" : "invoicevalueaftertax",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 200
				}, {
					"name" : "Outstanding Amount",
					"key" : "invoiceoutstandingamount",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 160
				}, {
					"name" : "Received Amount",
					"key" : "amountreceived",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 160
				}, {
					"name" : "PO Issued",
					"key" : "poissuedamount",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "PO Invoiced",
					"key" : "poinvamount",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Materials Consumed",
					"key" : "materialsconsumed",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Work Order Issued",
					"key" : "workorder_issued",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Work Order Invoiced",
					"key" : "workorder_invoiced",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Internal Labour",
					"key" : "labouramount",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Advances to Suppliers",
					"key" : "wopoadvancepayment",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				},{
					"name" : "Other Expenses",
					"key" : "otherexpenses",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Total Cash Inflow",
					"key" : "totalcashinflow",
					"format" : "currency",
					"footertype" : "sum",
					"if" : false,
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Total Cash Outflow",
					"key" : "totalcashoutflow",
					"format" : "currency",
					"footertype" : "sum",
					"if" : false,
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Net Cashflow",
					"key" : "netcashflow",
					"format" : "currency",
					"footertype" : "sum",
					"if" : false,
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Total Income",
					"key" : "totalincome",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Total Expense",
					"key" : "totalexpenses",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Gross Profit",
					"key" : "grossprofit",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 150
				}, {
					"name" : "Gross Profit %",
					"key" : "grossprofitpercentage",
					"format" : "number",
					"cellClass" : "text-center",
					"width" : 150,
					"footertype" : "calculated",
					"footerCalculation" : "{(colarr, aggarr) => report.getFooterValue(colarr, aggarr)}"
				}]
			};

			customfieldAssign(tempObj.columns, null, 'projects', this.props.app.myResources);
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];
		['fromdate', 'todate', 'customerid', 'projectid'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`)
			}
		});
	
		axios.get(`/api/query/projectprofitabilityreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getFooterValue(colarr, aggarr) {
		let costvalue = 0;
		let salevalue = 0;
		colarr.forEach((col, colindex) => {
			if(col.key == 'totalincome')
				salevalue = aggarr[colindex].footerAggValue;

			if(col.key == 'totalexpenses')
				costvalue = aggarr[colindex].footerAggValue;
		});

		if(costvalue > 0 && salevalue > 0)
			return Number((((salevalue - costvalue) / salevalue) * 100).toFixed(2));

		return null;
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			customerid : null,
			projectid : null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Project  Profitability Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Customer Name</label>
										<Field name={'customerid'} props={{resource: "partners", fields: "id,name,displayname", label: "displayname", displaylabel: "name"}} component={autoSelectEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Project</label>
										<Field name={'projectid'} props={{resource: "projects", fields: "id,projectname,projectno,displayname", label: "displayname", filter: "projects.status in ('Approved', 'Completed')"}} component={autoSelectEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname='Project Profitability Report' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

ProjectsProfitabilityReportForm = connect(
	(state, props) => {
		let formName = 'projectsprofitabilityreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(ProjectsProfitabilityReportForm));

export default ProjectsProfitabilityReportForm;
