import React, { Component } from 'react';
import { v1 as uuidv1 } from 'uuid';
import Modal from 'react-modal';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {Field, Fields, FieldArray, reduxForm, formValueSelector, FormSection } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import '../components/create-class';
import '../components/prop-types';
import ReactDataGrid from 'react-data-grid';

import {updatePageJSONState, updateReportJSONState, updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, getPageJson, getReportJson, getPageOptions, getFormName, checkPageCreateMode, getControllerName, checkAccess, checkActionVerbAccess, numberValidation, dateValidation, multiSelectValidation, requiredValidation, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { InputEle, SpanEle, DateEle, textareaEle, selectAsyncEle, localSelectEle, autoSelectEle, richtextEle, qtyEle, emailEle, checkboxEle, ContactEle, DisplayGroupEle, RateInline, StockInline, AddressEle, autosuggestEle, ButtongroupEle, RateField, DiscountField, TimepickerEle, alertinfoEle, PhoneElement, passwordEle, autoMultiSelectEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { LocalSelect, Init } from '../components/utilcomponents';
import { Reactuigrid, CurrencyFormatter, DateFormatter, anchortagFormatter } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class ReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.reportJSONCallback = this.reportJSONCallback.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.checkCondition = this.checkCondition.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		getReportJson(this.props, this.props.match.path.split('/')[1], this.reportJSONCallback);
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	checkCondition(type, condition) {
		let app = this.props.app;
		let feature = this.props.app.feature;
		let user = this.props.app.user;

		let initFn = (tempObj) => tempObj ? this.props.updateFormState(this.props.form, tempObj) : null;
		let changeFn = (tempObj) => tempObj ? this.props.updateFormState(this.props.form, tempObj) : null;
		let resource = this.props.resource;

		if(type == 'if')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 || (['string', 'boolean'].indexOf(typeof(condition)) >= 0 && eval(`try{${condition}}catch(e){}`));
		if(type == 'hide')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? 'hide' : '');
		if(type == 'show')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? '' : (eval(`try{${condition}}catch(e){}`) ? '' : 'hide');
		if(type == 'required')
			return ['string', 'boolean'].indexOf(typeof(condition)) == -1 ? false : (eval(`try{${condition}}catch(e){}`) ? true : false);

		if(type == 'value') {
			if(typeof(condition) == 'string') {
				if(condition.indexOf('{') == 0 && condition.lastIndexOf('}') == condition.length - 1)
					return eval(`try{${condition}}catch(e){}`);
				else
					return condition;
			}
			return condition;
		}
		return null;
	}

	reportJSONCallback(reportjson) {
		let tempObj = {};
		if(this.props.reportdata) {
			this.props.initialize({
				...this.props.reportdata
			});

			setTimeout(this.getReportData, 0);
		} else {
			let tempObj = {
				filters: {},
				hiddencols: [],
				columns: reportjson.columns ? reportjson.columns : [],
				reportfilter: reportjson.filter,
				reportdisplayName: reportjson.displayname,
				reportqueryname: reportjson.queryname
			};

			if(reportjson.customfieldResources && reportjson.customfieldResources.length > 0) {
				let tempKeyFlag = reportjson.keyFlag ? true : false;

				reportjson.customfieldResources.forEach((item) => {
					customfieldAssign(tempObj.columns, null, item, this.props.app.myResources, tempKeyFlag);
				});
			}

			this.props.initialize(tempObj);
		}

		setTimeout(() => {
			if(this.props.resource.reportfilter.length == 0)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];
		this.props.resource.reportfilter.forEach((item) => {
			if (this.props.resource[item.name]) {
				filterString.push(`${item.name}=${this.props.resource[item.name]}`)
			}
		});

		axios.get(`/api/query/${this.props.resource.reportqueryname}?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});
				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols
				});
				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			originalRows: null,
			filters: {}
		};

		this.props.resource.reportfilter.forEach((item) => {
			tempObj[item.name] = null
		});

		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		let transactionName = transactionname;
		let transactionId = transactionid;
		if(transactionname.indexOf('{') == 0 && transactionname.lastIndexOf('}') == transactionname.length - 1)
			transactionName = eval(`try{${transactionname}}catch(e){}`);
		if(transactionid.indexOf('{') == 0 && transactionid.lastIndexOf('}') == transactionid.length - 1)
			transactionId = eval(`try{${transactionid}}catch(e){}`);

		this.props.history.push(`/details/${transactionName}/${data[transactionId]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">{this.props.resource.reportdisplayName}</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							{this.props.resource.reportfilter.length > 0 ? <ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									{this.props.resource.reportfilter.map((filter, index) => {
										let initfn = this.checkCondition('value', filter.init)
										let fieldRequired = this.checkCondition('required', filter.required) ? true : false;
										let showHideClass = `${this.checkCondition('hide', filter.hide)} ${this.checkCondition('show', filter.show)}`;

										if(!this.checkCondition('if', filter.if))
											return null;

										return (
											<Init className={`form-group col-md-12 col-sm-12 col-xs-12 ${showHideClass}`} key={index} initfn={initfn}>
												{filter.name == 'show' ? <div>
													<label className="labelclass">Show</label>
													<Field name={'show'} props={{options: filter.localarray, required : fieldRequired}} component={localSelectEle}  validate={[stringNewValidation({required: fieldRequired, model: 'show'})]}/>
												</div> : null }
												{filter.name == 'fromdate' ? <div>
													<label className="labelclass">From Date</label>
													<Field name={'fromdate'} props={{required: fieldRequired}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  fieldRequired, title : 'From Date'})]}/>
												</div> : null }
												{filter.name == 'todate' ? <div>
													<label className="labelclass">To Date</label>
													<Field name={'todate'} props={{required: fieldRequired, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: fieldRequired, title : 'To Date', min: '{resource.fromdate}'})]}/>
												</div> : null }
												{filter.name == 'asondate' ? <div>
													<label className="labelclass">As on Date</label>
													<Field name={'asondate'} props={{required: fieldRequired}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  fieldRequired, title : 'As On Date'})]}/>
												</div> : null }
												{filter.name == 'salesperson' ? <div>
													<label className="labelclass">Sales Person</label>
													<Field name={'salesperson'} props={{resource: "users", fields: "id,displayname", label: "displayname", filter: "users.issalesperson=true", required : fieldRequired}} component={selectAsyncEle}  validate={[numberNewValidation({required: fieldRequired, model: 'salesperson'})]}/>
												</div> : null }
												{filter.name == 'customerid' ? <div>
													<label className="labelclass">Customer</label>
													<Field name={'customerid'} props={{resource: "partners", fields: "id,name,displayname", displaylabel: 'name', label: 'displayname', required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'customerid'})]}/>
												</div> : null }
												{filter.name == 'partnerid' ? <div>
													<label className="labelclass">Supplier</label>
													<Field name={'partnerid'} props={{resource: "partners", fields: "id,name,displayname", displaylabel: 'name', label: 'displayname', required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'partnerid'})]}/>
												</div> : null }
												{filter.name == 'customergroupid' ? <div>
													<label className="labelclass">Customer Group</label>
													<Field name={'customergroupid'} props={{resource: "customergroups", fields: "id,fullname", label: 'fullname', required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'customergroupid'})]}/>
												</div> : null }
												{filter.name == 'status' ? <div>
													<label className="labelclass">Status</label>
													<Field name={'status'} props={{options: filter.localarray, multiselect: filter.multiselect ? true : false, required : fieldRequired}} component={localSelectEle}  validate={[multiSelectNewValidation({required: fieldRequired, model: 'status'})]}/>
												</div> : null }
												{filter.name == 'itemid' ? <div>
													<label className="labelclass">Item Name</label>
													<Field name={'itemid'} props={{resource: "itemmaster", fields: "id,name", required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'itemid'})]}/>
												</div> : null }
												{filter.name == 'itemgroupid' ? <div>
													<label className="labelclass">Item Group</label>
													<Field name={'itemgroupid'} props={{resource: "itemgroups", fields: "id,fullname", label: "fullname", required: fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'itemgroupid'})]}/>
												</div> : null }
												{filter.name == 'itemcategoryid' ? <div>
													<label className="labelclass">Item Category</label>
													<Field name={'itemcategoryid'} props={{resource: "itemcategorymaster", fields: "id,name", required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'itemcategoryid'})]}/>
												</div> : null }
												{filter.name == 'numberingseriesmasterid' ? <div>
													<label className="labelclass">Numbering Series</label>
													<Field name={'numberingseriesmasterid'} props={{resource: "numberingseriesmaster", fields: "id,name,format,isdefault,currentvalue", filter: "numberingseriesmaster.resource='"+filter.numberingseriesresource+"'", required : fieldRequired}} component={selectAsyncEle}  validate={[numberNewValidation({required: fieldRequired, model: 'numberingseriesmasterid'})]}/>
												</div> : null }
												{filter.name == 'territoryid' ? <div>
													<label className="labelclass">Territory</label>
													<Field name={'territoryid'} props={{resource: "territories", fields: "id,territoryname,fullname", label: "fullname", required : fieldRequired}} component={selectAsyncEle}  validate={[numberNewValidation({required: fieldRequired, model: 'territoryid'})]}/>
												</div> : null }
												{filter.name == 'engineerid' ? <div>
													<label className="labelclass">Engineer</label>
													<Field name={'engineerid'} props={{resource: "users", fields: "id,displayname", label: "displayname", filter: "users.isengineer", required : fieldRequired}} component={autoMultiSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'engineerid'})]}/>
												</div> : null }
												{(filter.name == 'employeeid' && this.props.app.user.roleid.indexOf(18) == -1) ? <div>
													<label className="labelclass">Employee</label>
													<Field name={'employeeid'} props={{resource: "employees", fields: "id,displayname", label: "displayname", required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'employeeid'})]}/>
												</div> : null }
												{filter.name == 'projectid' ? <div>
													<label className="labelclass">Project</label>
													<Field name={'projectid'} props={{resource: "projects", fields: "id,projectname,projectno,displayname", label: "displayname", filter: "projects.status in ('Approved', 'Completed')", required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'projectid'})]}/>
												</div> : null }
												{(filter.name == 'receiptnoteid' && this.props.app.user.roleid.indexOf(18) == -1) ? <div>
													<label className="labelclass">Receipt Note No</label>
													<Field name={'receiptnoteid'} props={{resource: "receiptnotes", fields: "id,receiptnotenumber", label: "receiptnotenumber", filter: "receiptnotes.status in ('Approved','Received')", required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'receiptnoteid'})]}/>
												</div> : null }
												{(filter.name == 'landingcostid' && this.props.app.user.roleid.indexOf(18) == -1) ? <div>
													<label className="labelclass">Landing Cost Number</label>
													<Field name={'landingcostid'} props={{resource: "landingcosts", fields: "id,landingcostno", label: "landingcostno", required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'landingcostid'})]}/>
												</div> : null }
												{(filter.name == 'landingcostmasterid' && this.props.app.user.roleid.indexOf(18) == -1) ? <div>
													<label className="labelclass">Landing Cost Name</label>
													<Field name={'landingcostmasterid'} props={{resource: "landingcostmaster", fields: "id,name", required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'landingcostmasterid'})]}/>
												</div> : null }
												{filter.name == 'contracttype' ? <div>
													<label className="labelclass">Contract Type</label>
													<Field name={'contracttype'} props={{options: filter.localarray, required : fieldRequired}} component={localSelectEle}  validate={[stringNewValidation({required: fieldRequired, model: 'contracttype'})]}/>
												</div> : null }
												{(filter.name == 'contractid' && this.props.app.user.roleid.indexOf(18) == -1) ? <div>
													<label className="labelclass">Contract No</label>
													<Field name={'contractid'} props={{resource: "contracts", fields: "id,contractno", label: "contractno", required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'contractid'})]}/>
												</div> : null }
												{filter.type == 'customselect' ? <div>
													<label className="labelclass">{filter.title}</label>
													<Field name={filter.name} props={{options: filter.localarray, label: filter.label ? filter.label : '', valuename: filter.valuename ? filter.valuename : '', required: fieldRequired}} component={localSelectEle}  validate={[stringNewValidation({required: fieldRequired, model: filter.title})]}/>
												</div> : null }
												{filter.name == 'productionorderid' ? <div>
													<label className="labelclass">Production Order No</label>
													<Field name={'productionorderid'} props={{resource: "productionorders", fields: "id,orderno", label: "orderno", required : fieldRequired}} component={autoSelectEle}  validate={[numberNewValidation({required: fieldRequired, model: 'Production Order No'})]}/>
												</div> : null }
												{filter.name == 'warehouseid' ? <div>
													<label className="labelclass">Warehouse</label>
													<Field name={'warehouseid'} props={{resource: "stocklocations", fields: "id,name", filter: `stocklocations.companyid=${this.props.app.selectedcompanyid} AND stocklocations.isparent AND stocklocations.parentid IS NULL`, required : fieldRequired}} component={selectAsyncEle}  validate={[numberNewValidation({required: fieldRequired, model: 'Warehouse'})]}/>
												</div> : null }
												{filter.name == 'itembatchid' ? <div>
													<label className="labelclass">Item Batch Number</label>
													<Field name={'itembatchid'} props={{resource: "itembatches", fields: "id,batchnumber,description", label:"batchnumber", required : fieldRequired}} component={selectAsyncEle}  validate={[numberNewValidation({required: fieldRequired, model: 'Item Batch Number'})]}/>
												</div> : null }
												{filter.name == 'stocklocationid' ? <div>
													<label className="labelclass">Stock Location</label>
													<Field name={'stocklocationid'} props={{resource: "stocklocationquery", filter:`warehouseid=${this.props.resource.warehouseid}`,required : fieldRequired, usemethod:true}} component={selectAsyncEle}  validate={[numberNewValidation({required: fieldRequired, model: 'Stock Location'})]}/>
												</div> : null }
												{filter.name == 'user' ? <div>
													<label className="labelclass">User</label>
													<Field name={'user'} props={{resource: "users", fields: "id,displayname", label:"displayname", required : fieldRequired}} component={selectAsyncEle} validate={[numberNewValidation({required: fieldRequired, model: 'User'})]}/>
												</div> : null }
											</Init>
										);
									})}
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter> : null}
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname={this.props.resource.reportdisplayName} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

ReportForm = connect(
	(state, props) => {
		let formName = props.match.path.split('/')[1];
		return {
			app : state.app,
			reportjson : state.reportjson,
			form : formName,
			//destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateReportJSONState, updateFormState, updateReportFilter }
)(reduxForm()(ReportForm));

export default ReportForm;
