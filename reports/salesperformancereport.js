import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import {updateAppState} from '../actions/actions';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import { currencyFilter } from '../utils/filter';

class SalesPerformanceReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.openLead = this.openLead.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth((new Date().getMonth() - 1))).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				resultArray : {}
			}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);

		if ((new Date(new Date(this.props.resource.todate).setMonth(new Date(this.props.resource.todate).getMonth() - 12)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and Todate can't be more than One Year",
				btnArray : ["Ok"]
			}));
		}

		let filterString = [];

		['fromdate', 'todate', 'salesperson','territory', 'team'].forEach((item) => {
			if(this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/salesperformancereportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				
				this.props.updateFormState(this.props.form, {
					resultArray: response.data.main
				});

				this.props.updateReportFilter('salesperformancereport', this.props.resource);

				if(Object.keys(this.props.resource.resultArray).length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			filters: {},
			resultArray : {}
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	openLead (param) {
		let filterArray = [];
		let startdate = new Date(this.props.resource.fromdate).toDateString();
		let enddate = new Date(this.props.resource.todate).toDateString();
		
		filterArray.push({
			"fieldname" : param.datefield,
			"operator" : "GreaterThanOrEqual",
			"value" : startdate
		}, {
			"fieldname" : param.datefield,
			"operator" : "LessThanOrEqual",
			"value" : enddate
		});

		if(param.name == 'Won Leads')
			filterArray.push({
				"fieldname" : "status",
				"operator" : "Equal",
				"value" : 'Won'
			});
		else if(param.name == 'Lost Leads')
			filterArray.push({
				"fieldname" : "status",
				"operator" : "Equal",
				"value" : 'Lost'
			});
		else if (param.name == 'Quotations Sent')
			filterArray.push({
				"fieldname" : "status",
				"operator" : "Equal",
				"value" : 'Sent To Customer'
			});
		else if (param.name == 'New Orders')
			filterArray.push({
				"fieldname" : "status",
				"operator" : "In",
				"value" : ['Approved', 'Hold']
			});
		else if (param.name == 'Invoices Created' || param.name == 'Total Collections' || param.name == 'Credit Notes Created')
			filterArray.push({
				"fieldname" : "status",
				"operator" : "In",
				"value" : ['Approved', 'Sent To Customer']
			});
		else if (param.name == 'Project Quotations Sent')
			filterArray.push({
				"fieldname" : "status",
				"operator" : "Equal",
				"value" : 'Sent To Customer'
			});
		else if (param.name == 'New Projects')
			filterArray.push({
				"fieldname" : "status",
				"operator" : "Equal",
				"value" : 'Approved'
			});

		if (this.props.resource.salesperson > 0)
			filterArray.push({
				"fieldname" : "salesperson",
				"operator" : "Equal",
				"value" : this.props.resource.salesperson
			});
		else {
			if (this.props.app.user.roleid.indexOf(2) > -1)
				filterArray.push({
					"fieldname" : "salesperson",
					"operator" : "Equal",
					"value" : this.props.app.user.id
				});
		}

		if (this.props.resource.territory > 0 && param.name != 'New Projects')
			filterArray.push({
				"fieldname" : "territoryid",
				"operator" : "Equal",
				"value" : this.props.resource.territory
			});

		if (this.props.resource.team > 0)
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "We're sorry. Details are not available when a Team filter is applied. Please remove the Team filter to see details.",
				btnArray : ["Ok"]
			}));
		else {
			let routeObj = {
				'New Leads' :  {
					resource : 'leads',
					path : '/list/leads'
				},
				'Won Leads' :  {
					resource : 'leads',
					path : '/list/leads'
				},
				'Lost Leads' :  {
					resource : 'leads',
					path : '/list/leads'
				},
				'Quotations Sent' :  {
					resource : 'quotes',
					path : '/list/quotes'
				},
				'New Orders' :  {
					resource : 'orders',
					path : '/list/orders'
				},
				'Invoices Created' :  {
					resource : 'salesinvoices',
					path : '/list/salesinvoices'
				},
				'Total Collections' :  {
					resource : 'receiptvouchers',
					path : '/list/receiptvouchers'
				},
				'Project Quotations Sent' :  {
					resource : 'projectquotes',
					path : '/list/projectquotes'
				},
				'New Projects' :  {
					resource : 'projects',
					path : '/list/projects'
				},
				'Credit Notes Created' :  {
					resource : 'creditnotes',
					path : '/list/creditnotes'
				}
			};

			let tempobj = {
				...this.props.app.listFilterObj,
				[routeObj[param.name].resource]: filterArray
			};
			this.props.updateAppState('listFilterObj', tempobj);
			this.props.history.push(routeObj[param.name].path);
		}
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Sales Performance Report</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									{this.props.app.user.roleid.indexOf(2) == -1 ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Sales Person</label>
										<Field name={'salesperson'} props={{resource: "users", fields: "id,displayname", label: "displayname", filter: "users.issalesperson=true"}} component={selectAsyncEle} />
									</div> : null }
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Team</label>
										<Field name={'team'} props={{resource: "teamstructure", fields: "id,fullname", label: "fullname"}} component={selectAsyncEle} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Territory</label>
										<Field name={'territory'} props={{resource: "territories", fields: "id,fullname", label: "fullname"}} component={selectAsyncEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<div className="row">
								<div className="col-md-12 col-xs-12 col-sm-12" style={{paddingLeft:'100px'}}>
									<div className="row col-md-10 col-xs-12 col-sm-12">
										{
											(this.props.resource.resultArray.countArray && this.props.resource.resultArray.countArray.length > 0) ? this.props.resource.resultArray.countArray.map((stage, index) => {
												return (
													<div className="col-md-3 col-sm-3 col-xs-6" key={index}>
														<div className={`card ${(stage.name!='Sales Activities' && stage.name!='Collection Activities') ? 'hovercolor' : ''}`} style={{textDecoration:'none',color:'#333',height:'100px',marginTop:'20px'}}>
															<div className="card-body" style={{padding:'0px'}}>
																{(stage.name!='Sales Activities' && stage.name!='Collection Activities') ? <a className="thumbnail padding-20" onClick={() => this.openLead(stage)}>
																	<div className="text-center">
																		<b className="font-20">{stage.totalcount}</b> <span>{stage.name}</span>
																		<br />
																		<span className="text-muted">{currencyFilter(stage.totalrevenue, stage.currencyid, this.props.app)}</span>
																		<br />
																		{(stage.name=='Quotations Sent' && stage.totalcount > 0) ? <span className="text-muted" style={{fontSize: '11px'}}>* Amount before tax</span> : null}
																	</div>
																</a> : <span  className="thumbnail padding-20">
																	<div className="text-center">
																		<b className="font-20">{stage.totalcount}</b> <span>{stage.name}</span>
																		<br />
																		<span className="text-muted">{currencyFilter(stage.totalrevenue, stage.currencyid, this.props.app)}</span>
																	</div>
																</span> }
															</div>
														</div>
													</div>
												)
											}) : null
										}
									</div>
									<div className="row col-md-10 col-xs-12 col-sm-12">
									{ (this.props.resource.resultArray.activityArray && this.props.resource.resultArray.activityArray.length > 0) ? 
										<div className="col-md-6 col-xs-8 col-sm-8">
											<div className="card" style={{marginTop:'20px'}}>
												<div className="card-header"><i className="fa fa-eye"></i> Sales Activitiy Summary</div>
												<div className="card-body">
													<table className="table table-bordered" style={{marginBottom:'0px'}}>
														<thead>
															<tr>
																<th className='text-center'>Activity Type</th>
																<th className='text-center'>Count</th>
															</tr>
														</thead>
														<tbody>
															{this.props.resource.resultArray.activityArray.map((activity, activityindex) => {
																return (
																	<tr key={activityindex}>
																		<td>{activity.activitytypename}</td>
																		<td className='text-center'>{activity.totalcount}</td>
																	</tr>
																)
															})}
														</tbody>
													</table>
												</div>
											</div>
										</div> : null
									}

									{ (this.props.resource.resultArray.leadstagemovementArray && this.props.resource.resultArray.leadstagemovementArray.length > 0) ? 
										<div className="col-md-6 col-xs-8 col-sm-8">
											<div className="card" style={{marginTop:'20px'}}>
												<div className="card-header"><i className="fa fa-bars"></i> Stage Movements</div>
												<div className="card-body">
													<table className="table table-bordered" style={{marginBottom:'0px'}}>
														<thead>
															<tr>
																<th className='text-center'>Stage</th>
																<th className='text-center'>No of Leads</th>
															</tr>
														</thead>
														<tbody>
															{this.props.resource.resultArray.leadstagemovementArray.map((activity, activityindex) => {
																return (
																	<tr key={activityindex}>
																		<td>{activity.stagename}</td>
																		<td className='text-center'>{activity.totalcount}</td>
																	</tr>
																)
															})}
														</tbody>
													</table>
												</div>
											</div>
										</div> : null
									}

									{ (this.props.resource.resultArray.collectionactivityArray && this.props.resource.resultArray.collectionactivityArray.length > 0) ? 
										<div className="col-md-6 col-xs-8 col-sm-8">
											<div className="card" style={{marginTop:'20px'}}>
												<div className="card-header"><i className="fa fa-eye"></i> Collection Activitiy Summary</div>
												<div className="card-body">
													<table className="table table-bordered" style={{marginBottom:'0px'}}>
														<thead>
															<tr>
																<th className='text-center'>Activity Type</th>
																<th className='text-center'>Count</th>
															</tr>
														</thead>
														<tbody>
															{this.props.resource.resultArray.collectionactivityArray.map((activity, activityindex) => {
																return (
																	<tr key={activityindex}>
																		<td>{activity.activitytype}</td>
																		<td className='text-center'>{activity.totalcount}</td>
																	</tr>
																)
															})}
														</tbody>
													</table>
												</div>
											</div>
										</div> : null
									}

									{ (this.props.resource.resultArray.lostreasonArray && this.props.resource.resultArray.lostreasonArray.length > 0) ? 
										<div className="col-md-6 col-xs-8 col-sm-8">
											<div className="card" style={{marginTop:'20px'}}>
												<div className="card-header"><i className="fa fa-thumbs-down"></i> Lost Lead Analysis</div>
												<div className="card-body">
													<table className="table table-bordered" style={{marginBottom:'0px'}}>
														<thead>
															<tr>
																<th className='text-center'>Reason</th>
																<th className='text-center'>Count</th>
															</tr>
														</thead>
														<tbody>
															{this.props.resource.resultArray.lostreasonArray.map((activity, activityindex) => {
																return (
																	<tr key={activityindex}>
																		<td>{activity.reason}</td>
																		<td className='text-center'>{activity.totalcount}</td>
																	</tr>
																)
															})}
														</tbody>
													</table>
												</div>
											</div>
										</div> : null
									}
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</>
		);
	};
}

SalesPerformanceReportForm = connect(
	(state, props) => {
		let formName = 'salesperformancereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter, updateAppState }
)(reduxForm()(SalesPerformanceReportForm));

export default SalesPerformanceReportForm;
