import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import MapForm from '../components/mapcomponent';

class DeliveryRoutePlannerReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.dispatchDelivery = this.dispatchDelivery.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		let fromDate = new Date(new Date(new Date().setDate(new Date().getDate() - 6)));

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				filters: {},
				fromdate : fromDate,
				todate : new Date(new Date().setHours(0, 0, 0, 0))
			};

		this.props.initialize(tempObj);

		setTimeout(() => {
			//if(this.props.reportdata)
				this.getReportData();
		}, 0);
		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('originalRows');

		let filterString = [];

		/*if ((new Date(new Date(this.props.resource.todate).setDate(new Date(this.props.resource.todate).getDate() - 6)).setHours(0, 0, 0, 0)) > new Date(this.props.resource.fromdate).setHours(0, 0, 0, 0)) {
			this.updateLoaderFlag(false);
			return this.props.openModal(modalService.infoMethod({
				header : "Error",
				body : "Difference between From Date and To Date can't be more than One Week",
				btnArray : ["Ok"]
			}));
		}*/

		['fromdate', 'todate', 'territoryid'].forEach((item) => {
			if (this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		filterString.push(`type=load`);

		axios.get(`/api/query/deliveryrouteplannerreportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				response.data.main.forEach((item) => {
					item.ischecked = false;
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);

				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	dispatchDelivery(delivery_array) {
		let tempDeliveryNotes = [];

		delivery_array.map((item) => {
			if(item.ischecked)
				tempDeliveryNotes.push(item.id);
		});

		if (delivery_array.length == 0) {
			return this.props.openModal(modalService.infoMethod({
				header : "Warning",
				body : "Please select alteast one Delivery Note to dispatch",
				btnArray : ["Ok"]
			}));
		} else {
			let message = {
				header : "Confirm",
				body : `Do you want to Dispatch selected Delivery Notes?`,
				btnArray : ['Yes','No']
			};
			this.props.openModal(modalService['confirmMethod'](message, (param) => {
				if(param)
					this.callbackmodal(tempDeliveryNotes);
			}));
		}
	}

	callbackmodal (tempDeliveryNotes) {
		this.updateLoaderFlag(true);

		axios.get(`/api/query/deliveryrouteplannerreportquery?type=update&deliveryArray=${tempDeliveryNotes}`).then((response) => {
			let apiResponse = commonMethods.apiResult(response);
			this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));

			if (response.data.message == 'success')
				this.getReportData();

			this.updateLoaderFlag(false);
		});
	}

	checkboxOnChange(value, item) {
		item.ischecked = value;
		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});
		this.refs.grid.refresh();
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			territoryid : null,
			calltype : null,
			originalRows: null,
			filters: {}
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Delivery Route Planner</div>
						</div>
						{ !this.props.app.feature.licensedTrackingUserCount ? <div className="col-md-12 col-sm-12 col-xs-12 margintop-50">
							<div className="col-md-8 offset-md-2 col-sm-12 col-xs-12 alert alert-warning text-center">You have no access to view map details !!</div>
						</div> : <div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									{false ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required:  true, title : 'From Date'})]} />
									</div>: null}
									{false ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>: null}
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Territory</label>
										<Field name={'territoryid'} props={{resource: "territories", fields: "id,territoryname", label: "territoryname"}} component={autoSelectEle} validate={[numberNewValidation({required: false, model: 'territoryid'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{ this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <MapForm app={this.props.app} resource={this.props.resource} openModal={this.props.openModal} history = {this.props.history} updateFormState={this.props.updateFormState} form={this.props.form} rptparam={'deliveryrouteplannerreport'} latlngArray={this.props.resource.originalRows} callback = {(value) => this.dispatchDelivery(value)}/> : null }
						</div>}
					</div>
				</form>
			</>
		);
	};
}

DeliveryRoutePlannerReportForm = connect(
	(state, props) => {
		let formName = 'deliveryrouteplannerreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(DeliveryRoutePlannerReportForm));

export default DeliveryRoutePlannerReportForm;
