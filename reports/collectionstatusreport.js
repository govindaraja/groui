import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import { currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter, discountFilter, numberFilter } from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import CalendarToolbar from '../components/calendartoolbar';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';
import MapForm from '../components/mapcomponent';

const localizer = momentLocalizer(moment);

class CollectionStatusReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.changeView = this.changeView.bind(this);
		this.getTitle = this.getTitle.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
		this.onView = this.onView.bind(this);
		this.onNavigate = this.onNavigate.bind(this);
		this.calendarOnClick = this.calendarOnClick.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	changeView(param) {
		this.props.updateFormState(this.props.form, {
			viewvalue : param
		});
		setTimeout(() => {
			if (param != 'map')
				this.getReportData();

			this.props.updateReportFilter(this.props.form, this.props.resource);
		}, 0)
	}

	onView(view) {
		this.props.updateFormState(this.props.form, {
			defaultView: view
		});
		setTimeout(() => {
			this.props.updateReportFilter(this.props.form, this.props.resource);
		}, 0);
	}

	onNavigate(date, view) {
		const new_date = moment(date);
		this.props.updateFormState(this.props.form, {
			defaultDate: new_date
		});
		setTimeout(() => {
			this.props.updateReportFilter(this.props.form, this.props.resource);
		}, 0);
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata,
				events: []
			};
		else {
			tempObj = {
				viewvalue: 'list',
				show : 'overdue',
				defaultDate: new Date(),
				defaultView: 'day',
				collectionrep : this.props.app.user.roleid.indexOf(2) > -1 ? this.props.app.user.id : null,
				filters: {},
				hiddencols: [],
				events: [],
				columns: [{
					"name" : "Customer",
					"key" : "name",
					"format" : "anchortag",
					"width" : 180
				}, {
					"name" : "Amount Overdue",
					"key" : "outstandingamount",
					"format" : "currency",
					"footertype" : "sum",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Collection Rep",
					"key" : "collectionrep_displayname",
					"width" : 180
				}, {
					"name" : "Last Action",
					"key" : "lastcollectionaction",
					"width" : 180
				}, {
					"name" : "Last Action Date",
					"key" : "lastactiondate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Next Action Date",
					"key" : "nextactiondate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Sales Person",
					"key" : "salesperson_displayname",
					"width" : 150
				}, {
					"name" : "Territory",
					"key" : "territoryname",
					"cellClass" : "text-center",
					"width" : 180
				}]
			};
			customfieldAssign(tempObj.columns, null, 'partners', this.props.app.myResources, true);
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata || this.props.app.user.roleid.indexOf(2) > -1)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let events = [];
		let filterString = [];

		if(this.props.resource.collectionrep)
			filterString.push(`collectionrep=${this.props.resource.collectionrep}`);
		if(this.props.resource.followupfromdate && this.props.resource.viewvalue != 'calendar')
			filterString.push(`followupfromdate=${this.props.resource.followupfromdate}`);
		if(this.props.resource.followuptodate && this.props.resource.viewvalue != 'calendar')
			filterString.push(`followuptodate=${this.props.resource.followuptodate}`);

		axios.get(`/api/query/collectionstatusquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				for (var i = 0; i < response.data.main.length; i++) {
					let nextFollowupdate = new Date(response.data.main[i]['nextactiondate']);
					events.push({
						partnerid : response.data.main[i].partnerid,
						title : this.getTitle(response.data.main[i]),
						start : nextFollowupdate,
						end : nextFollowupdate,
						className : 'label-green',
						allDay : false
					});
				}

				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					hiddencols,
					events
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	getTitle(obj) {
		let subject = obj.name +' -- '+currencyFilter(obj.outstandingamount, this.props.app.defaultcurrency, this.props.app);
		if (!this.props.resource.collectionrep && obj.collectionrep_displayname)
			subject += " -- " + obj.collectionrep_displayname;
		return subject;
	}

	resetFilter () {
		let tempObj = {
			show : 'overdue',
			originalRows: null,
			filters: {},
			hiddencols: [],
			events: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data) {
		let tempobj = {
			fromdate : new Date(new Date(new Date().setMonth(new Date().getMonth() - 3)).setHours(0, 0, 0, 0)),
			todate : new Date(new Date().setHours(0, 0, 0, 0)),
			companyid : this.props.app.user.selectedcompanyid,
			partnerid : data.partnerid
		};
		this.props.history.push({pathname: '/customerstatementreport', params: {...tempobj}});
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	calendarOnClick() {
		this.props.history.push({pathname: '/activitycalendar', params: {activitiesfor: 'Payment Followup'}});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Collection Status</div>
							<div className="report-header-btnbar">
								{this.props.resource.viewvalue != 'map' && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							<div className="report-header-rightpanel">
								<ul className="list-pagination">
									<li className={`${this.props.resource.viewvalue == 'list' ? 'active' : ''}`} onClick={() =>{this.changeView('list')}}><a><span className="fa fa-th-list"></span></a></li>
									<li className={`${this.props.resource.viewvalue == 'calendar' ? 'active' : ''}`} onClick={this.calendarOnClick}><a><span className="fa fa-calendar"></span></a></li>
									{this.props.app.feature.licensedTrackingUserCount > 0 ? <li className={`${this.props.resource.viewvalue == 'map' ? 'active' : ''}`} onClick={() =>{this.changeView('map')}}><a><span className="fa fa-globe"></span></a></li> : null}
								</ul>
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									{this.props.app.user.roleid.indexOf(2) < 0 ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Collection Representative</label>
										<Field name={'collectionrep'} props={{resource: "users", fields: "id,displayname", label: "displayname"}} component={selectAsyncEle} />
									</div> : null}
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Follow Up From</label>
										<Field name={'followupfromdate'} component={DateEle}  validate={[dateNewValidation({title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Follow Up To</label>
										<Field name={'followuptodate'} component={DateEle} props={{min: this.props.resource.followupfromdate}}  validate={[dateNewValidation({title : 'To Date', min: '{resource.followupfromdate}'})]}/>
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12" >
										<button type="button" onClick={() => {
											this.getReportData()
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />							
							{ this.props.resource && this.props.resource.viewvalue == 'list' ? <Reactuigrid excelname="Collection Status Report" app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
							{ this.props.resource.viewvalue == 'map' && this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <MapForm app={this.props.app} resource={this.props.resource} openModal={this.props.openModal} history = {this.props.history} updateFormState={this.props.updateFormState} form={this.props.form} rptparam={'collectionstatusreport'} latlngArray={this.props.resource.originalRows}/> : null }
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && this.props.resource.viewvalue == 'calendar' ? <div className="col-md-12 margintop-10 paddingleft-30" style={{height: '500px'}}>
								<Calendar localizer={localizer} events={this.props.resource.events} views={["month", "week", "day"]} defaultView={this.props.resource.defaultView} defaultDate={new Date(this.props.resource.defaultDate)} showMultiDayTimes components = {{toolbar : CalendarToolbar}} onView={this.onView} onNavigate={this.onNavigate} onSelectEvent={event => this.openTransaction(event)} />
							</div> : null}
						</div>
					</div>
				</form>
			</>
		);
	};
}

CollectionStatusReportForm = connect(
	(state, props) => {
		let formName = 'collectionstatusreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(CollectionStatusReportForm));

export default CollectionStatusReportForm;
