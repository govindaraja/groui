import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation, search, checkActionVerbAccess } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';
import {currencyFilter, booleanfilter, taxFilter, deliveryreceiptforfilter, dateFilter, datetimeFilter, timeFilter, uomFilter} from '../utils/filter';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

import SettlementDetailsModal from '../components/details/settlementdetailsmodal';

class UnsettledReceiptPaymentReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		}

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.checkboxOnChange = this.checkboxOnChange.bind(this);
		this.checkboxHeaderOnChange = this.checkboxHeaderOnChange.bind(this);
		this.btnOnClick = this.btnOnClick.bind(this);
		this.createJournal = this.createJournal.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};
		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else {
			tempObj = {
				asondate : new Date(new Date().setHours(0, 0, 0, 0)),
				type : "Customer Advances",
				filters: {},
				columns: [{
					name : "",
					headerformat : "checkbox",
					key : "ischecked",
					locked : true,
					format : "checkbox",
					cellClass : "text-center",
					onChange : "{report.checkboxOnChange}",
					restrictToExport : true,
					width : 100
				}, {
					name : "Partner",
					key : "partnerid_name",
					cellClass : "text-center",
					width : 200
				}, {
					name : "Employee",
					key : "employeeid_displayname",
					cellClass : "text-center",
					width : 200
				}, {
					name : "Voucher Type",
					key : "vouchertype",
					cellClass : "text-center",
					width : 180
				}, {
					name : "Voucher No",
					key : "voucherno",
					format : "anchortag",
					cellClass : "text-center",
					width : 180
				}, {
					name : "Voucher Date",
					key : "voucherdate",
					format : "date",
					cellClass : "text-center",
					width : 180
				}, {
					name : "Voucher Amount",
					key : "amount",
					format : "currency",
					footertype : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					cellClass : 'text-right',
					width : 180
				}, {
					name : "Unsettled Amount",
					key : "outstandingamount",
					format : "currency",
					footertype : !this.props.app.feature.useMultiCurrency ? "sum" : "",
					cellClass : 'text-right',
					width : 180
				}, {
					name : 'Exchange Rate',
					key : 'currencyexchangerate',
					if: this.props.app.feature.useMultiCurrency,
					cellClass : 'text-right',
					width : 150
				}, {
					name : 'Voucher Amount (' + this.props.app.currency[this.props.app.defaultCurrency].symbol + ')',
					key : 'amountlc',
					format : 'currency',
					if: this.props.app.feature.useMultiCurrency,
					cellClass : 'text-right',
					footertype : 'sum',
					width : 170
				}, {
					name : 'Unsettled Amount (' + this.props.app.currency[this.props.app.defaultCurrency].symbol + ')',
					key : 'outstandingamountlc',
					format : 'currency',
					if: this.props.app.feature.useMultiCurrency,
					cellClass : 'text-right',
					footertype : 'sum',
					width : 170
				}, {
					name : "Account",
					key : "accountid_name",
					cellClass : "text-center",
					width : 180
				}, {
					name : "Sales Peson",
					key : "salesperson_displayname",
					cellClass : "text-center",
					width : 180
				}, {
					name : "Territory",
					key : "territoryid_territoryname",
					cellClass : "text-center",
					width : 180
				}, {
					name : "Remarks",
					key : "description",
					width : 200
				}]
			};

			customfieldAssign(tempObj.columns, null, 'journalvouchers', this.props.app.myResources);

			tempObj.columns.push({
				name : "",
				format : "button",
				buttonname : "Details",
				cellClass : "text-center",
				onClick : "{report.btnOnClick}",
				restrictToExport : true,
				width : 150
			});
		}

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('originalRows');

		let filterString = [];
		['type', 'asondate'].forEach((item) => {
			if (this.props.resource[item]) {
				filterString.push(`${item}=${this.props.resource[item]}`);
			}
		});

		axios.get(`/api/query/unsettledreceiptspaymentsquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);

				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);

			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}

			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		this.props.updateFormState(this.props.form, {
			type : "",
			asondate : null,
			originalRows: null,
			filters: {}
		});
	}

	openTransaction(data) {
		if(data.vouchertype == 'Receipt')
			this.props.history.push(`/details/receiptvouchers/${data.relatedid}`);
		else if(data.vouchertype == 'Payment')
			this.props.history.push(`/details/paymentvouchers/${data.relatedid}`);
		else if(data.vouchertype == 'Credit Note')
			this.props.history.push(`/details/creditnotes/${data.relatedid}`);
		else if(data.vouchertype == 'Debit Note')
			this.props.history.push(`/details/debitnotes/${data.relatedid}`);
		else if(data.vouchertype == 'Expense Request')
			this.props.history.push(`/details/expenserequests/${data.relatedid}`);
		else if(data.vouchertype == 'Pay Roll')
			this.props.history.push(`/details/payrolls/${data.relatedid}`);
		else
			this.props.history.push(`/details/journalvouchers/${data.relatedid}`);
	}

	btnOnClick(data) {
		this.props.openModal({
			render: (closeModal) => {
				return <SettlementDetailsModal
					item = {data}
					relatedresource = {data.relatedresource ? data.relatedresource : 'journalvouchers'}
					app = {this.props.app}
					history = {this.props.history}
					closeModal = {closeModal} />
			}, className: {
				content: 'react-modal-custom-class-80',
				overlay: 'react-modal-overlay-custom-class'
			}
		});
	}

	checkboxHeaderOnChange(param) {
		let filterRows = this.refs.grid.getVisibleRows();

		filterRows.forEach((item, index) => {
			item.ischecked = param ? true : false;
		});

		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		this.refs.grid.forceRefresh();
	}

	checkboxOnChange(value, item) {
		item.ischecked = value;

		this.props.updateFormState(this.props.form, {
			originalRows: this.props.resource.originalRows
		});

		this.refs.grid.refresh();
	}

	createJournal (param) {
		let voucherArray = [], errorFound = false;

		for(var i = 0; i < this.props.resource.originalRows.length; i++) {
			if(this.props.resource.originalRows[i].ischecked) {
				let mismatchCurrency = false;

				if(voucherArray.length > 0) {
					if(voucherArray[0].currencyid != this.props.resource.originalRows[i].currencyid)
						mismatchCurrency=true;
				}

				if(mismatchCurrency) {
					errorFound=true;
					return this.props.openModal(modalService["infoMethod"]({
						header : "Error",
						body : "Currency Mismatched",
						btnArray : ["Ok"]
					}));				
				} else
					voucherArray.push(this.props.resource.originalRows[i]);
			}
		}

		if(voucherArray.length > 50) {
			errorFound=true;
			return this.props.openModal(modalService["infoMethod"]({
				header : "Error",
				body : "Please Choose only 50 Vouchers",
				btnArray : ["Ok"]
				}));
		}

		if (!errorFound) {
			if (voucherArray.length > 0)
				this.props.history.push({
					pathname: '/createJournalVoucher',
					params: {
						voucherArray : voucherArray,
						param: 'Unsettled Receipt Payment'
					}
				});
			else
				this.props.openModal(modalService['infoMethod']({
					header : 'Error',
					body : 'Please Choose atleast one item',
					btnArray : ['Ok']
				}));
		}
	}

	updateToggleState (filterToggleOpen) {
		this.setState({filterToggleOpen});
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Unsettled Receipts / Payments</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
							{(this.props.resource.originalRows && this.props.resource.originalRows.length > 0 && checkActionVerbAccess(this.props.app, 'journals', 'Save')) ? <div className="report-header-rightpanel">
								<button type="button" className="btn gs-btn-outline-success btn-sm btn-width" onClick={this.createJournal}><i className="fa fa-plus"></i>Journal</button>
							</div> : null}
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Type</label>
										<Field name={'type'} props={{options:["Customer Advances", "Supplier Advances", "Both Customer and Supplier Advances", 'Employee Advances'], required: true}} component={localSelectEle} validate={[stringNewValidation({required: true, title : 'Type'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">As On Date</label>
										<Field name={'asondate'} props={{}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <Reactuigrid excelname='Unsettled Receipts and Payments' app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} checkboxOnChange={this.checkboxOnChange} checkboxHeaderOnChange={this.checkboxHeaderOnChange} btnOnClick={this.btnOnClick} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

UnsettledReceiptPaymentReportForm = connect(
	(state, props) => {
		let formName = 'unsettledreceiptspaymentsreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(UnsettledReceiptPaymentReportForm));

export default UnsettledReceiptPaymentReportForm;
