import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import { currencyFilter } from '../utils/filter';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class CashBookReportForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date(new Date().setMonth((new Date().getMonth() - 1))).setHours(0, 0, 0, 0)),
				todate : new Date(new Date().setHours(0, 0, 0, 0)),
				filters: {},
				hiddencols: [],
				columns: [{
					"name" : "Date",
					"key" : "postingdate",
					"format" : "date",
					"cellClass" : "text-center",
					"width" : 150
				}, {
					"name" : "Voucher Type",
					"key" : "vouchertype",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Voucher No",
					"key" : "voucherno",
					"format" : "anchortag",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Type",
					"key" : "type",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Account",
					"key" : "account",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Opening Balance",
					"key" : "openingbalance",
					"format" : "currency",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Amount",
					"key" : "amount",
					"format" : "currency",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Closing Balance",
					"key" : "closingbalance",
					"format" : "currency",
					"cellClass" : "text-right",
					"width" : 180
				}, {
					"name" : "Remarks",
					"key" : "description",
					"cellClass" : "text-center",
					"width" : 230
				}, {
					"name" : "Customer",
					"key" : "customer",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Project",
					"key" : "projectname",
					"cellClass" : "text-center",
					"width" : 180
				}, {
					"name" : "Employee",
					"key" : "employee",
					"cellClass" : "text-center",
					"width" : 180
				}]
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');
		let cashinhand = "", tempArray = [];

		axios.get(`/api/query/getcashbalancequery?todate=${this.props.resource.todate}&fromdate=${this.props.resource.fromdate}&accountid=${this.props.resource.accountid}`).then((response) => {
			if (response.data.message == 'success') {
				if (response.data.main.ledgerArray.length > 0) {
					cashinhand = response.data.main.closingbalance;
					tempArray = response.data.main.ledgerArray;
					tempArray[0].openingbalance = response.data.main.openingbalance;
					if (tempArray[0].debit > 0) {
						tempArray[0].closingbalance = tempArray[0].openingbalance + tempArray[0].debit;
						tempArray[0].type = 'Receipt';
						tempArray[0].amount = tempArray[0].debit;
					} else {
						tempArray[0].closingbalance = tempArray[0].openingbalance - tempArray[0].credit;
						tempArray[0].type = 'Payment';
						tempArray[0].amount = tempArray[0].credit * (-1);
					}

					for (var i = 1; i < tempArray.length; i++) {
						tempArray[i].openingbalance = tempArray[i - 1].closingbalance;
					
						if (tempArray[i].debit> 0) {
							tempArray[i].closingbalance = tempArray[i].openingbalance + tempArray[i].debit;
							tempArray[i].type = 'Receipt';
							tempArray[i].amount = tempArray[i].debit;
						} else {
							tempArray[i].closingbalance = tempArray[i].openingbalance - tempArray[i].credit;
							tempArray[i].type = 'Payment';
							tempArray[i].amount = tempArray[i].credit * (-1);
						}
					}
				}

				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: tempArray,
					hiddencols,
					cashinhand
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			accountid : null,
			cashinhand: null,
			originalRows: null,
			filters: {},
			hiddencols: []
		};
		this.props.updateFormState(this.props.form, tempObj);
	}

	openTransaction(data) {
		if (data.vouchertype == 'Journal Voucher')
			this.props.history.push(`/details/journalvouchers/${data.relatedid}`);
		else if(data.vouchertype == 'Receipt')
			this.props.history.push(`/details/receiptvouchers/${data.relatedid}`);
		else if(data.vouchertype == 'Payment')
			this.props.history.push(`/details/paymentvouchers/${data.relatedid}`);
		else if(data.relatedresource == 'expenserequests')
			this.props.history.push(`/details/expenserequests/${data.relatedid}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Cash Book</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>

									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Cash Account</label>
										<Field name={'accountid'} props={{resource: "accounts", fields: "id,name", filter: "accounts.type='Asset' and accounts.accountgroup='cash'"}} component={selectAsyncEle} />
									</div>
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12" >
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource.cashinhand ? <div className="col-md-12 col-sm-12 paddingleft-30"><span>Cash In Hand : {currencyFilter(this.props.resource.cashinhand, this.props.app.defaultCurrency, this.props.app)}</span></div> : null}
							{this.props.resource ? <Reactuigrid excelname="Cash Book" app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

CashBookReportForm = connect(
	(state, props) => {
		let formName = 'cashbookreport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(CashBookReportForm));

export default CashBookReportForm;
