import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import axios from 'axios';

import { updateFormState, updateReportFilter } from '../actions/actions';
import { customfieldAssign, stringNewValidation, numberNewValidation, dateNewValidation, multiSelectNewValidation } from '../utils/utils';
import { DateEle, selectAsyncEle, localSelectEle, autoSelectEle, ReportDateRangeField } from '../components/formelements';
import { commonMethods, modalService } from '../utils/services';

import Loadingcontainer from '../components/loadingcontainer';
import { Reactuigrid } from '../components/reportcomponents';
import ReportFilter from '../components/reportfiltercomponents';
import ReportPlaceholderComponent from '../components/reportplaceholdercomponents';

class AttendanceReportForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			summaryColumns : [{
					name : 'Employee Name',
					key : 'displayname',
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Location',
					key : 'locationid_name',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'No of Working Days',
					key : 'noofworkingdays',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Weekoff / Holidays Count',
					key : 'weekoffholidayCount',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Present Days',
					key : 'presentdays',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Total Leaves',
					key : 'leaves',
					cellClass : 'text-center',
					width : 180
				}, {
					name: 'Paid Leaves',
					key: 'paidleaves',
					cellClass: 'text-center',
					width: 150
				}, {
					name: 'LOP Days',
					key: 'lossofpays',
					cellClass: 'text-center',
					width: 150
				}, {
					name : 'No Data',
					key : 'nodata',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Permissions(Hours)',
					key : 'permission',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'On Duty',
					key : 'onduty',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Late In',
					key : 'latein',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Early Out',
					key : 'earlyout',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Required Hours',
					key : 'requiredhours',
					cellClass : 'text-center',
					format : 'number',
					width : 180
				}, {
					name : 'Actual Hours',
					key : 'actualhours',
					cellClass : 'text-center',
					format : 'number',
					width : 180
				}, {
					name : 'Shortage Hours',
					key : 'shortagehours',
					cellClass : 'text-center',
					format : 'number',
					width : 180
				}, {
					name : 'Department',
					key : 'departmentid_name',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Employee Code',
					key : 'employeecode',
					cellClass : 'text-center',
					width : 180
				}
			],
			detailsColumns : [{
					name : 'Date',
					key : 'date',
					format : 'date',
					cellClass : 'text-center',
					width : 150
				}, {
					name : 'Employee Name',
					key : 'displayname',
					cellClass : 'text-center',
					width : 200
				}, {
					name : 'Location',
					key : 'locationid_name',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Employee Code',
					key : 'employeecode',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Department',
					key : 'departmentid_name',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Check-in Location',
					key : 'checkinlocationid_name',
					cellClass : 'text-center',
					width : 180
				}, {
					name: 'In Time',
					key: 'intime',
					format: 'location',
					showfield: 'item.checkindetails',
					fieldname: 'intime',
					fieldformat: 'time',
					locationobj: 'checkindetails',
					cellClass: 'text-center',
					width: 200
				}, {
					name : 'Check-in Inside Geofence',
					key : 'checkininsidegeofence',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Check-out Location',
					key : 'checkoutlocationid_name',
					cellClass : 'text-center',
					width : 180
				}, {
					name: 'Out Time',
					key: 'outtime',
					format: 'location',
					showfield: 'item.checkoutdetails',
					fieldname: 'outtime',
					fieldformat: 'time',
					locationobj: 'checkoutdetails',
					cellClass: 'text-center',
					width: 200
				}, {
					name : 'Check-out Inside Geofence',
					key : 'checkoutinsidegeofence',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Duration',
					key : 'durationtime',
					//format : "datetime",
					cellClass : 'text-center',
					width : 140
				}, {
					name : 'Leaves',
					key : 'leavetype',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Leave Type',
					key : 'leavetypeid_name',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Permissions(Hours)',
					key : 'permission',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'On Duty',
					key : 'onduty',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Weekoff or Holiday',
					key : 'weekoffholidaydetails',
					cellClass : 'text-center',
					width : 180
				}, {
					name : 'Remarks',
					key : 'remarks',
					cellClass : 'text-center',
					width : 200
				}
			],
			filterToggleOpen: true
		};

		this.updateLoaderFlag = this.updateLoaderFlag.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.getReportData = this.getReportData.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.openTransaction = this.openTransaction.bind(this);
		this.showOnChange = this.showOnChange.bind(this);
		this.updateToggleState = this.updateToggleState.bind(this);
	}

	componentWillMount() {
		this.updateLoaderFlag(true);
		this.onLoad();
	}

	updateLoaderFlag(loaderflag) {
		this.setState({loaderflag});
	}

	onLoad() {
		let tempObj = {};

		if(this.props.reportdata)
			tempObj = {
				...this.props.reportdata
			};
		else
			tempObj = {
				fromdate : new Date(new Date().getFullYear(), new Date().getMonth(),1),
				todate : new Date(new Date().getFullYear(), new Date().getMonth()+1,0),
				filters : {},
				hiddencols : [],
				columns : [],
				show : 'Summary'
			};

		this.props.initialize(tempObj);

		if(this.props.reportdata)
			setTimeout(this.getReportData, 0);

		this.updateLoaderFlag(false);
	}

	getReportData () {
		this.updateLoaderFlag(true);
		this.props.array.removeAll('hiddencols');
		this.props.array.removeAll('originalRows');

		let filterString = [];

		['fromdate', 'todate', 'employeeid', 'show'].forEach((item) => {
			if(this.props.resource[item])
				filterString.push(`${item}=${this.props.resource[item]}`)
		});

		axios.get(`/api/query/attendancereportquery?${filterString.join('&')}`).then((response) => {
			if (response.data.message == 'success') {
				let hiddencols = [];
				this.props.resource.columns.forEach((item) => {
					if(item.hidden == true) {
						hiddencols.push(item.key);
					}
				});

				this.props.updateFormState(this.props.form, {
					originalRows: response.data.main,
					columns: this.props.resource.show == 'Summary' ? this.state.summaryColumns : this.state.detailsColumns,
					hiddencols
				});

				this.props.updateReportFilter(this.props.form, this.props.resource);
				if(this.props.resource.originalRows.length > 0)
					this.updateToggleState(false);
			} else {
				let apiResponse = commonMethods.apiResult(response);
				this.props.openModal(modalService[apiResponse.methodName](apiResponse.message));
			}
			this.updateLoaderFlag(false);
		});
	}

	resetFilter () {
		let tempObj = {
			fromdate : null,
			todate : null,
			show : null,
			employeeid : null,
			originalRows: null,
			filters: {},
			hiddencols: [],
			columns: []
		};

		this.props.updateFormState(this.props.form, tempObj);
	}

	showOnChange() {
		this.props.array.removeAll('originalRows');
		this.props.array.removeAll('columns');

		this.props.updateFormState(this.props.form, {
			filters: {}
		});
	}

	openTransaction(data, transactionname, transactionid) {
		this.props.history.push(`/details/${transactionname}/${data[transactionid]}`);
	}

	updateToggleState (filterToggleOpen) {
		this.setState({ filterToggleOpen });
	}

	render() {
		if(!this.props.resource)
			return null;

		return(
			<>
				<Loadingcontainer isloading={this.state.loaderflag}></Loadingcontainer>
				<form>
					<div className="row">
						<div className="col-md-12 bg-white report-header">
							<div className="report-header-title">Attendance Report</div>
							<div className="report-header-btnbar">
								{this.props.resource.originalRows && this.props.resource.originalRows.length > 0 ? <button type="button" onClick={() => this.refs.grid.exportExcel()} className="btn btn-sm btn-outline-secondary"><span className="fa fa-download"></span></button> : null}
							</div>
						</div>
						<div className="col-md-12 paddingright-5">
							<ReportFilter isOpen={this.state.filterToggleOpen} updateToggle={this.updateToggleState} >
								<div className="row marginbottom-75">
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">From Date</label>
										<Field name={'fromdate'} props={{required: true}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle}  validate={[dateNewValidation({required:  true, title : 'From Date'})]} />
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">To Date</label>
										<Field name={'todate'} props={{required: true, min: this.props.resource.fromdate}} format={(value, name) => {return value == null ? null : moment(value).format("DD-MMM-YYYY")}} parse={(value) => {return (value == null || value == '') ? null : moment(value, "DD-MMM-YYYY")._d}} component={DateEle} validate={[dateNewValidation({required: true, title : 'To Date', min: '{resource.fromdate}'})]}/>
									</div>
									<div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Show</label>
										<Field name={'show'} props={{options: ["Summary", "Details"], required: true, onChange: () => {this.showOnChange()}}} component={localSelectEle} validate={[stringNewValidation({required: true, model: 'Show'})]} />
									</div>
									{ this.props.resource.show == 'Details' ? <div className="form-group col-md-12 col-sm-12">
										<label className="labelclass">Employee</label>
										<Field name={'employeeid'} props={{resource: "employees", fields: "id,displayname", label: "displayname"}} component={autoSelectEle} />
									</div> : null}
								</div>
								<div className="reportfilter-search">
									<div className="form-group col-md-12 col-sm-12">
										<button type="button" onClick={() => {
											this.getReportData()	
										}} className="btn btn-width btn-sm gs-btn-success" disabled={!this.props.valid}><i className="fa fa-search"></i>Go</button>
										<button type="button" onClick={() => {
											this.resetFilter()	
										}} className="btn btn-width btn-sm gs-btn-info"><i className="fa fa-refresh"></i>Reset</button>
									</div>
								</div>
							</ReportFilter>
							<ReportPlaceholderComponent reportdata={this.props.resource.originalRows} />
							{this.props.resource ? <Reactuigrid excelname={`${this.props.resource.show == 'Summary' ? 'Attendance Report - Summary' : 'Attendance Report - Details'}`} app={this.props.app} ref="grid" report={this} gridprops={this.props.resource} updateFormState={this.props.updateFormState} form={this.props.form} updateReportFilter={this.props.updateReportFilter} openTransaction={this.openTransaction} /> : null }
						</div>
					</div>
				</form>
			</>
		);
	};
}

AttendanceReportForm = connect(
	(state, props) => {
		let formName = 'attendancereport';
		return {
			app : state.app,
			form : formName,
			destroyOnUnmount: false,
			resource : state.form[formName] ? (state.form[formName].values ? state.form[formName].values : null) : null,
			formData : state.form,
			fullstate : state,
			reportdata: state.reportfilter[formName] ? state.reportfilter[formName] : null
		}
	}, { updateFormState, updateReportFilter }
)(reduxForm()(AttendanceReportForm));

export default AttendanceReportForm;
