var config = require('./config');
var path = require('path');
const webpack = require('webpack');

console.log(__dirname);
module.exports = {
	entry: {
		'appnew' : './app.js'
	},
	output: {
		path: path.join(__dirname, "bundles"),
		publicPath: '/bundles',
		filename: "[name].js"
	},
	module: {
		rules: [{
			exclude: [
				path.resolve(__dirname, "node_modules/")
			],
			loader: "babel-loader",
			options: {
				presets: ["@babel/preset-react", "@babel/preset-env", "@babel/preset-flow"],
				compact: false
			}
		}, {
			test: /\.css$/,
			use: [{ loader: 'style-loader' }, { loader: 'css-loader'}]
		}, {
			test: /\.(png|svg|jpe?g|gif)$/,
			use: ['file-loader']
		}, {
			test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
			use: [{
				loader: 'url-loader',
				options: {
					limit: 100000,
					mimetype: 'application/font-woff',
				}
			}]
		}, {
			test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
			use: [{
				loader: 'url-loader',
				options: {
					limit: 100000,
					mimetype: 'application/font-woff',
				}
			}]
		}, {
			test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
			use: [{
				loader: 'url-loader',
				options: {
					limit: 100000,
					mimetype: 'application/octet-stream',
				}
			}]
		}, {
			test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
			use: ['file-loader']
		}, {
			test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
			use: [{
				loader: 'url-loader',
				options: {
					limit: 100000,
					mimetype: 'image/svg+xml',
				}
			}]
		}, {
			test: require.resolve('snapsvg/dist/snap.svg.js'),
			use: 'imports-loader?this=>window,fix=>module.exports=0'
		}]
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			_: 'lodash'
		})
	],
	resolve: {
		extensions: ['.js', '.jsx']
	},
	node: { fs: 'empty' },
	watchOptions: {
		poll: 3000,
		ignored: ["node_modules"]
	},
	devServer: {
		host : config.reacthost,
		port : config.reactport,
		historyApiFallback: true,
		contentBase: './'
	}
};
