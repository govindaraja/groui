export function updateListState (prop, value) {
	return {
		type: 'GS_LIST_Update',
		payload: {prop, value}
	};
};

export function updateListViewState (prop, value) {
	return {
		type: 'GS_LIST_View_Update',
		payload: {prop, value}
	}
};
