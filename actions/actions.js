import React,{Component} from 'react';
import { change } from 'redux-form';
import { batchActions } from 'redux-batched-actions';
import routes from '../routeconfig';

import DetailForm from '../containers/details';

export function updateAppState (prop, value) {
	return {
		type: 'GS_App_Update',
		payload: {prop, value}
	}
};

export function updatePageJSONState (title, type, value) {
	return {
		type: 'GS_PageJSON_Update',
		payload: {title, type, value}
	}
};

export function updatePageJSONViewState (title, type, value) {
	return {
		type: 'GS_PageJSON_View_Update',
		payload: {title, type, value}
	}
};

export function updateReportJSONState (title, type, value) {
	return {
		type: 'GS_ReportJSON_Update',
		payload: {title, value}
	}
};

export function updateReportBuilderJSONState (title, value) {
	return {
		type: 'GS_ReportBuilderJSON_Update',
		payload: {title, value}
	}
};

export function updateFormState (formname, fieldarray) {
	return (dispatch)=> {
		var actionArr = [];

		for(var prop in fieldarray) {
			actionArr.push(change(formname, prop, fieldarray[prop]== undefined ? null : fieldarray[prop]));
		}
		dispatch(batchActions(actionArr));
	}
};

/*export function openModal (modal) {
	return {
		type: 'GS_Modal_Open',
		payload: modal
	}
};

export function closeModalState (modal) {
	return {
		type: 'GS_Modal_Close',
		payload: modal
	}
};

export function createOrEdit (url, id, params, callback) {
	var param = {
		isModal: true,
		callback: callback,
		match: {
			params: {
				id : id
			},
			path: url
		},
		location: params ? {
			params
		} : {}
	}
	return {
		type: 'GS_Modal_Open',
		payload: {
			render: (closeModal) => {
				return <DetailForm {...param} closeModal={closeModal} />
			},
			className: {content: 'react-modal-custom-class-80', overlay: 'react-modal-overlay-custom-class'}
		}
	}
};*/

export function updateReportFilter (prop, value) {
	return {
		type: 'GS_Report_Filter_Update',
		payload: {prop, value}
	};
};

export function resetReportFilterByReport (prop) {
	return {
		type: 'GS_Report_Filter_Reset_By_Report',
		payload: { prop }
	};
};

export function resetReportFilter () {
	return {
		type: 'GS_Report_Filter_Reset',
		payload: {}
	};
};
